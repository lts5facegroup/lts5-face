/**
 *  @file   test_face_reconstruction_pipeline.cpp
 *  @brief  3D Reconstruction pipeline
 *
 *  @author Christophe Ecabert
 *  @date   30/07/14
 *  Copyright (c) 2014 Christophe Ecabert. All rights reserved.
 */

#include "opencv2/core.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/face_reconstruction/face_synthesis_pipeline.hpp"

int main(int argc, const char * argv[]) {
  // CmdParser
  LTS5::CmdLineParser parser;
  parser.AddArgument("-c",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     ".xml file holding pipeline configuration");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Folder where data are exported");
  parser.AddArgument("-f",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Which frame to process");
  parser.AddArgument("-r",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Reset at every frame");
  // Parse arg
  int err = parser.ParseCmdLine(argc, argv);
  if(!err) {
    // Setup parameters
    std::string config_file;
    std::string output_folder;
    std::string frame_number;
    std::string force_reset;


    parser.HasArgument("-c", &config_file);
    parser.HasArgument("-o", &output_folder);
    parser.HasArgument("-f" , &frame_number);
    parser.HasArgument("-r" , &force_reset);

    if (!config_file.empty()) {
      // Pipeline init parameters
      // --------------------------------------------------
      LTS5::FaceSynthesisPipeline::FaceSynthesisPipelineParameters _pipelineParam;
      _pipelineParam.pipeline_xml_config_file = config_file;
      _pipelineParam.output_folder = output_folder;
      _pipelineParam.output_on_screen = output_folder.empty();
      // Process config
      LTS5::FaceSynthesisPipeline::PipelineProcessConfiguration process_configuration_;
      process_configuration_.has_synthetic_data = false;
      process_configuration_.draw_landmarks = false;
      // Instantiate pipeline
      // --------------------------------------------------
      LTS5::FaceSynthesisPipeline* _pipeline;
      try {
        // Instantiate pipeline
        _pipeline = new LTS5::FaceSynthesisPipeline(_pipelineParam);
      } catch(const LTS5::ProcessError& error) {
        // Something went wrong
        LTS5_LOG_ERROR(error.what());
        return -1;
      }

      // Ready to use
      // --------------------------------------------------
      int n_frame = _pipeline->get_total_number_of_frame();
      if (!frame_number.empty()) {
        int frame = std::atoi(frame_number.c_str());
        if (frame < n_frame) {
          // Set which frame to process
          _pipeline->set_frame_to_process(frame);
          LTS5_LOG_INFO("Process frame : " << frame);
          // Process
          _pipeline->Process(process_configuration_);
        } else {
          LTS5_LOG_ERROR("Error, index exceed maximum number of frame available");
        }
      } else {
        // Loop over all frame
        int cnt = 0;
        /*size_t max_frame = 49;
        double time = 0.0;
        std::vector<double> sample(max_frame + 1);*/
        while (cnt < n_frame || n_frame == -2) {
          LTS5_LOG_INFO("Process frame : " << cnt);
          // Process
          double t0 = cv::getTickCount();
          _pipeline->Process(process_configuration_);
          double t1 = cv::getTickCount() - t0;
          t1 *= (1000.0 / cv::getTickFrequency());
          LTS5_LOG_INFO("\ttime : " << t1 << " ms");

          if (!force_reset.empty()) {
            _pipeline->ResetTrackingState();
          }
          cnt++;
          /*if (cnt > 9) {
            time += t1;
          }
          sample[cnt] = t1;*/
          // Inc cnt
          if (++cnt > 100) break;
        }
        /*LTS5_LOG_INFO("Average processing time : " << (time/(cnt-10)) << " ms");
        cv::Mat data(sample.size(), 1, CV_64FC1, sample.data());
        LTS5::SaveMatToBin("sample.bin",data);*/
      }
      // Release memory
      delete _pipeline;
    }
  }
  return err;
}

