/**
 *  @file   test_morphable_model_pipeline.cpp
 *  @brief  Sample, 3D Morphable Model fitting
 *
 *  @author Christophe Ecabert
 *  @date   13/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <lts5/face_tracker/lbf_tracker.hpp>
#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/model/color_morphable_model.hpp"
#include "lts5/face_reconstruction/mm_fitter.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"


#include "lts5/utils/colormap.hpp"

int main(int argc, const char ** argv) {
  using T = float;
  using MModel = LTS5::MorphableModel<T>;
  using ColorMModel = LTS5::ColorMorphableModel<T>;
  using MMFitter = LTS5::MorphableModelFitter<T>;
  using Camera = typename LTS5::MorphableModelFitter<T>::Camera_t;
  using Mesh = LTS5::Mesh<T, LTS5::Vector3<T>>;

  LTS5::CmdLineParser parser;
  parser.AddArgument("-m",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Morphable Model path");
  parser.AddArgument("-i",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Input image");
  parser.AddArgument("-a",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Image annotation");
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Tracker models \"fdetect;tracker\"");
  int err = parser.ParseCmdLine(argc, argv);
  if (err == 0) {
    std::string model_path, image_path, ann_path, tracker_path;
    parser.HasArgument("-m", &model_path);
    parser.HasArgument("-i", &image_path);
    parser.HasArgument("-a", &ann_path);
    parser.HasArgument("-t", &tracker_path);

    // Load image
    cv::Mat image = cv::imread(image_path);
    // Load model
    MModel* model = new ColorMModel();
    err = model->Load(model_path);

    std::cout << "Ns " << model->get_active_shape_component() << std::endl;


    Mesh m;
    model->Generate(&m);
    int e = m.Save("debug/rand1.ply");
    model->Generate(&m);
    e |= m.Save("debug/rand2.ply");
    model->Generate(&m);
    e |= m.Save("debug/rand3.ply");


    // Create fitter object
    MMFitter* fitter = new MMFitter("SimultaneousForward", model);
    // Load SDM
    std::vector<std::string> part;
    LTS5::SplitString(tracker_path, ";", &part);
    LTS5::SdmTracker tracker;
    err |= tracker.Load(part[1], part[0]);


    if (err == 0) {

      cv::Mat landmarks;
      if (ann_path.empty()) {
        // Track
        tracker.Detect(image, &landmarks);
      } else {
        // Load annotation
        LTS5::LoadPts(ann_path, landmarks);
      }

      // Reconstruction
      const T f = static_cast<T>(std::max(image.cols, image.rows));
      Camera cam(f, image.cols, image.rows);
      const T axis[] = {T(1.0), T(-1.0), T(-1.0)};
      cam.set_axis_inversion(axis);


      T min_x = std::numeric_limits<T>::max();
      T max_x = std::numeric_limits<T>::epsilon();
      T min_y = std::numeric_limits<T>::max();
      T max_y = std::numeric_limits<T>::epsilon();

      int n_pts = std::max(landmarks.rows, landmarks.cols) / 2;
      const auto* ptr = reinterpret_cast<double*>(landmarks.data);
      for (int i = 0; i < n_pts; ++i) {
        const double x = ptr[i];
        const double y = ptr[i + n_pts];

        min_x = x < min_x ? x : min_x;
        max_x = x > max_x ? x : max_x;

        min_y = y < min_y ? y : min_y;
        max_y = y > max_y ? y : max_y;
      }

      cv::Rect bbox(min_x, min_y, max_x - min_x, max_y - min_y);






      LTS5::Mesh<T, LTS5::Vector3<T>> mesh;
      LTS5::MorphableModelFitter<T>::Regularization reg;
      /*reg.data = T(1.0);
      reg.landmark = T(0.001);
      reg.tex = T(0.5); //T(0.0025);
      reg.shape = T(0.008);*/

      reg.data = T(1.0);
      reg.landmark = T(0.005);
      reg.tex = T(0.065); //T(0.075);
      reg.shape = T(0.001);

      fitter->set_regularizer(reg);

      LTS5::MMFitAlgorithm<T>::Stat fit_stats;
      err = fitter->FitWithLandmarks(image,
                                     landmarks,
                                     &cam,
                                     &mesh,
                                     &fit_stats);/*

      int err = fitter->FitWithBoundingBox(image,
                                           bbox,
                                           &cam,
                                           &mesh,
                                           &fit_stats);*/


      fit_stats.Print(std::cout);
      cv::Mat data_point;
      fit_stats.ToMat(&data_point);
      LTS5::SaveMatToBin("debug/stat.bin", data_point);



      Mesh m("BFM/00052_20061024_00526_neutral_face05.ply");
      LTS5::Vector3<T> t = m.Cog();
      t *= T(-1.0);
      m.Translate(t);

      t = mesh.Cog();
      t *= T(-1.0);
      mesh.Translate(t);

      const auto& v_ref = m.get_vertex();
      const auto& v_rec = mesh.get_vertex();
      std::vector<T> err_pts(v_rec.size(), T(0.0));
      T emax = T(0.0);
      for (size_t i = 0; i < v_ref.size(); ++i) {
        T e = (v_ref[i] - v_rec[i]).Norm();
        err_pts[i] = e;
        if (e > emax) {
          emax = e;
        }
      }

      LTS5::Vector3<T> color;
      std::vector<LTS5::Vector3<T>> e_color;
      LTS5::ColorMap<T> map(LTS5::ColorMap<T>::ColorMapType::kJet, T(0.0), emax);
      for (size_t i = 0; i < v_ref.size(); ++i) {
        map.PickColor(err_pts[i], &color);
        e_color.push_back(color);
      }
      mesh.set_vertex_color(e_color);
      mesh.Save("debug/mesh_err.ply");






    } else {
      LTS5_LOG_ERROR("Can not load Morphable Model: " << model_path);
    }
    // Clean up
    delete model;
    delete fitter;
  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
  return err;
}
