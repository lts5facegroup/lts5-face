/**
 *  @file   uv_map_generator.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   17/02/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/face_reconstruction/lscm_parameterizer.hpp"

#include "lts5/utils/file_io.hpp"

int main(int argc, const char * argv[]) {

  // CmdParser
  LTS5::CmdLineParser parser;
  parser.AddArgument("-m",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Input mesh");
  // Parse
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    std::string filename;
    parser.HasArgument("-m", &filename);
    // Define output name
    std::string folder, file, ext, process_filename;
    LTS5::ExtractDirectory(filename, &folder, &file, &ext);
    process_filename = folder + file + "_uv." + ext;

    // Load mesh
    LTS5::Mesh<double> mesh;
    err = mesh.Load(filename);
    if (!err) {
      // Compute UV map
      LTS5::LSCMParameterizer<double> uv_generator;
      uv_generator.Initialize(mesh);
      err = uv_generator.Process(&mesh);
      if (!err) {
        err = mesh.Save(process_filename);
        std::cout << "Finish : " << (!err ? "Success" : "Error") << std::endl;

        /*const auto& tcoord = mesh.get_tex_coord();
        const auto& tri = mesh.get_triangle();
        cv::Mat tc(tcoord.size(), 2, cv::DataType<double>::type, (void*)tcoord.data());
        cv::Mat t(tri.size(), 3, CV_32SC1, (void*)tri.data());
        LTS5::SaveMatToBin("tex.bin",tc);
        LTS5::SaveMatToBin("tri.bin",t);*/
      } else {
        std::cout << "Error while generating UV map" << std::endl;
      }
    } else {
      std::cout << "Error, can not open : " << filename << std::endl;
    }
  } else {
    parser.PrintHelp();
  }
  return err;
}