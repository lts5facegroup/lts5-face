/**
 *  @file   mesh2image_exporter.hpp
 *  @brief  Convert a mesh into an image using offscreen renderer
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 25/08/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_MESH2IMAGE_EXPORTER__
#define __LTS5_MESH2IMAGE_EXPORTER__

#include <stdio.h>
#include <thread>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/ogl/offscreen_renderer.hpp"
#include "lts5/face_reconstruction/base_mesh_exporter.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  Mesh2ImageExporter
 *  @brief  Convert a Mesh object into an Image (cv::Mat) object
 *  @author Christophe Ecabert
 *  @date   25/08/15
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS Mesh2ImageExporter : public BaseMeshExporter {
 public :

#pragma mark -
#pragma mark Initialization
  /**
   *  @name Mesh2ImageExporter
   *  @fn Mesh2ImageExporter()
   *  @brief  Constructor
   */
  Mesh2ImageExporter();

  /**
   *  @name Mesh2ImageExporter
   *  @fn Mesh2ImageExporter(const Mesh2ImageExporter& other) = delete
   *  @brief  Copy constructor
   *  @param[in]    other   Object to copy from
   */
  Mesh2ImageExporter(const Mesh2ImageExporter& other) = delete;

  /**
   * @name  operator=
   * @fn    Mesh2ImageExporter& operator=(const Mesh2ImageExporter& rhs) = delete
   * @brief Assignment operator
   * @param rhs Object to copy from
   * @return    Newly assigned object
   */
  Mesh2ImageExporter& operator=(const Mesh2ImageExporter& rhs) = delete;

  /**
   *  @name ~Mesh2ImageExporter
   *  @fn ~Mesh2ImageExporter() override
   *  @brief  Destructor
   */
  ~Mesh2ImageExporter() override;

  /**
   *  @name Mesh2ImageExporter
   *  @fn Mesh2ImageExporter(const LTS5::Mesh<float>& mesh,
                             const cv::Mat& texture,
                             const int width,
                             const int height)
   *  @brief  Constructor
   *  @param[in]  mesh    Mesh to convert to image
   *  @param[in]  texture Mesh's texture, can be null if not used
   *  @param[in]  width   Image width
   *  @param[in]  height  Image height
   */
  Mesh2ImageExporter(const LTS5::Mesh<float>& mesh,
                     const cv::Mat& texture,
                     const int width,
                     const int height);

  /**
   *  @name Mesh2ImageExporter
   *  @fn Mesh2ImageExporter(const int width,
                             const int height)
   *  @brief  Constructor
   *  @param[in]  width   Image width
   *  @param[in]  height  Image height
   */
  Mesh2ImageExporter(const int width,
                     const int height);

  /**
   *  @name Initialize
   *  @fn void Initialize(const LTS5::Mesh<float>& mesh,
                          const cv::Mat& texture,
                          const int width,
                          const int height) override
   *  @brief  Initialize converter
   *  @param[in]  mesh  Mesh to convert to image
   *  @param[in]  texture Mesh's texture, can be null if not used
   *  @param[in]  width   Image width
   *  @param[in]  height  Image height
   */
  void Initialize(const LTS5::Mesh<float>& mesh,
                  const cv::Mat& texture,
                  const int width,
                  const int height) override;

#pragma mark -
#pragma mark Process

  /**
   * @name  ActiveContext
   * @fn    void ActiveContext() const override
   * @brief Make this context the current one
   */
  void ActiveContext() const override;

  /**
   * @name  DisableContext
   * @fn    void DisableContext() const override
   * @brief Detach from current context
   */
  void DisableContext() const override;

  /**
   *  @name Process
   *  @fn void Process() override
   *  @brief  Convert rendered mesh into image
   */
  void Process() override;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  set_output_name
   * @fn    void set_output_name(const std::string& output) override
   * @brief Set the name to use as output
   * @param output    Output name
   */
  void set_output_name(const std::string& output) override {
    output_name_ = output;
  }

  /**
   *  @name set_mesh
   *  @fn void set_mesh(const LTS5::Mesh<float>& mesh) override
   *  @brief  Link to the mesh to export
   *  @param[in]  mesh  Mesh to export
   */
  void set_mesh(const LTS5::Mesh<float>& mesh) override;

  /**
   *  @name set_texture
   *  @fn void set_texture(const cv::Mat& texture) override
   *  @brief  Set texture of the object
   *  @param[in]  texture Texture object
   *  @throw  Throw #LTS5#ProcessError exception if class is not initialized
   */
  void set_texture(const cv::Mat& texture) override;

  /**
   * @name  get_texture
   * @fn    const OGLTexture* get_texture() const override
   * @brief Provide access to opengl texture object
   * @return    OGLTexture object
   */
  const OGLTexture* get_texture() const  override{
    return texture_;
  }

  /**
   *  @name get_image
   *  @fn cv::Mat get_image() const override
   *  @brief  Provide access to the converted mesh to image
   *  @return Image
   */
  cv::Mat get_image() const override{
    return image_;
  }

  /**
   * @name  get_model_view_transform
   * @fn    const cv::Mat& get_model_view_transform() const
   * @brief Provide Model-View transformation that goes into eye coordinate system
   * @return    Model-View transform [4x4] matrix
   */
  const cv::Mat& get_model_view_transform() const {
    return model_view_;
  }

  /**
   * @name  get_projection_transform
   * @fn    const cv::Mat& get_projection_transform() const
   * @brief Provide projection transformation that goes from eye coordinate into
   *        Normalize Device Coordinate (NDC). (i.e. perspective/ortho proj.)
   * @return    Projection transform [4x4] matrix
   */
  const cv::Mat& get_projection_transform() const {
    return projection_;
  }

  /**
   * @name  get_window_width
   * @fn    const float get_window_width() const
   * @brief Provide window's width
   * @return    window's width
   */
  float get_window_width() const {
    return cam_->get_window_width();
  }

  /**
   * @name  get_window_height
   * @fn    const float get_window_height() const
   * @brief Provide window's height
   * @return    window's height
   */
  float get_window_height() const {
    return cam_->get_window_height();
  }

#pragma mark -
#pragma mark Private

 private :
  /** Mesh */
  Mesh<float>* mesh_;
  /** Texture object */
  OGLTexture* texture_;
  /** Rendering program */
  OGLProgram* program_;
  /** Camera */
  OGLCamera<float>* cam_;
  /** Offscreen renderer */
  OGLOffscreenRenderer<float>* renderer_;
  /** Mesh displayed as image */
  cv::Mat image_;
  /** Model-View transform */
  cv::Mat model_view_;
  /** Projection transform */
  cv::Mat projection_;
  /** Output name */
  std::string output_name_;
};
}  // namespace LTS5
#endif /* __LTS5_MESH2IMAGE_EXPORTER__ */
