/**
 *  @file   contour_extractor.hpp
 *  @brief  Extract chin contour from a 3D mesh
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 03/02/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_CONTOUR_EXTRACTOR__
#define __LTS5_CONTOUR_EXTRACTOR__

#include <vector>
#include <unordered_map>

#include "opencv2/core/core.hpp"
#include "lts5/utils/library_export.hpp"
#include "lts5/face_reconstruction/virtual_camera.hpp"
#include "lts5/face_reconstruction/pipeline_data_type_definition.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  ContourExtractor
 *  @brief  Extract chin contour from a 3D mesh
 *  @author Christophe Ecabert
 *  @date   03/02/2015
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS ContourExtractor {
 private :
  /** Number of lines on chin contour */
  int nb_contour_line_;
  /** How many vertices are consider as potential contour */
  int nb_total_contour_vertex_;
  /** Number of connection between potential candidates and their neighbors */
  int nb_onnected_vertex_;
  /** Number of vertex in the submesh */
  int nb_candidate_vertex_;
  /** Number of view considered during extraction */
  int number_views_;

  /** Array indicating where each lines starts */
  std::vector<int> start_line_idx_;
  /** Mesh index corresponding to the contour candidates */
  std::vector<int> line_vertex_idx_;
  /** Index to the first neighbour for a given vertices */
  std::vector<int> start_connection_idx_;
  /** List of all neighbour */
  std::vector<int> connection_vertex_idx_;
  /** List of all vertices used to compute contour */
  std::vector<int> candidate_vertex_idx_;
  /** Index of the 3d contour for each views */
  std::vector<std::vector<int>> contour_3d_;
  /** Index of the 3d contour for each views that have a match on images.
   Size is <= to contour_3d_.size() */
  std::vector<std::vector<int>> matched_contour_3d_;
  /** Corresponding global indexes */
  std::vector<std::vector<int>> matched_global_idx_contour_3d_;
  /** Position on the image plane that match 3d contour */
  std::vector<std::vector<float>> matched_contour_img_position_;
  /** Mapping between global and local mesh indexes */
  std::unordered_map<int, int> index_map_;

  /** Selection of valid features for theta angle */
  std::vector<std::vector<int>> theta_features_;
  /** Theta angle boundaries */
  std::vector<std::pair<float, float>> theta_bounds_;
  /** Selection of valid features for phi angle */
  std::vector<std::vector<int>> phi_features_;
  /** Phi angle boundaries */
  std::vector<std::pair<float, float>> phi_bounds_;
  /** Feature's index used for computation */
  std::vector<int> valid_features_selection_;
  /*!> 3D shape */
  cv::Mat mesh_;

  using ContourIndexIt = std::vector<std::vector<int>>::iterator;
  using ContourIndexConstIt = std::vector<std::vector<int>>::const_iterator;
  using ContourValueIt = std::vector<std::vector<float>>::iterator;
  using ContourValueConstIt = std::vector<std::vector<float>>::const_iterator;
  using ContourMatrixIt = std::vector<cv::Mat>::iterator;
  using ContourMatrixConstIt = std::vector<cv::Mat>::const_iterator;
  using ContourPointIt = std::vector<cv::Point2f>::iterator;
  using ContourPointConstIt = std::vector<cv::Point2f>::const_iterator;

 public :
  /** Normal on contour points for each view */
  cv::Mat mesh_normal_DBG_;
  /** Normal's at contour point */
  std::vector<cv::Mat> contour_normal_;

  /**
   *  @brief  Configuration flags
   */
  /** Show line search space */
  static const bool  show_contour_search_space_ = false;

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   ContourExtractor
   *  @fn ContourExtractor(const std::string& config_filename)
   *  @brief  Constructor
   *  @param[in]  config_filename     Binary file where configuration
   *                                  is stored
   */
  explicit ContourExtractor(const std::string& config_filename);

  /**
   *  @name   ~ContourExtractor
   *  @fn ~ContourExtractor()
   *  @brief  Destructor
   */
  ~ContourExtractor();

  /**
   *  @name   InitMultiView
   *  @fn void InitMultiView(const int number_view)
   *  @brief  Initialize the extractor for a given number of view
   *  @param[in]  number_view         Number of views
   */
  void InitMultiView(const int number_view);

#pragma mark -
#pragma mark Process

  /**
   *  @name   ExtractContour3D
   *  @fn void ExtractContour3D(const std::vector<cv::Mat>& camera_facing_direction)
   *  @brief  Compute the index of the vertices on the chin contour
   *  @param[in]  camera_facing_direction   Optical axis (Camera Z axis)
   *                                        for each views
   */
  void ExtractContour3D(const std::vector<cv::Mat>& camera_facing_direction);

  /**
   *  @name   MatchContourWithImage
   *  @fn void MatchContourWithImage(const std::vector<VirtualCamera*>& cam_structure,
                        const PPLData::ModelPoseParametersStruct&  model_pose,
                        const std::vector<cv::Mat>& image_contour,
                        const std::vector<cv::Point2f>& face_region_origin)
   *  @brief  Compute the correspondance between 3D contour and the image(s)
   *          for cases with calibrated environment
   *  @param[in]  cam_structure       Camera configuration setup
   *  @param[in]  model_pose          Model rigid transfrom
   *  @param[in]  image_contour       Binary image with contour,
   *                                  i.e. canny output
   *  @param[in]  face_region_origin  Location of the ROI on the image,
   *                                  used to compensate the projection
   */
  void MatchContourWithImage(const std::vector<VirtualCamera*>& cam_structure,
                             const PPLData::ModelPoseParametersStruct&  model_pose,
                             const std::vector<cv::Mat>& image_contour,
                             const std::vector<cv::Point2f>& face_region_origin);

  /**
   *  @name   MatchContourWithImage
   *  @fn void MatchContourWithImage(const PPLData::ModelPoseParametersStruct& model_pose,
                            const std::vector<cv::Mat>& image_contour,
                            const std::vector<cv::Point2f>& face_region_origin,
                            const std::vector<bool>& view_valid)
   *  @brief  Compute the correspondance between 3D contour and the image(s)
   *          for cases with uncalibrated environment
   *  @param[in]  model_pose          Model rigid transfrom
   *  @param[in]  image_contour       Binary image with contour,
   *                                  i.e. canny output
   *  @param[in]  face_region_origin  Location of the ROI on the image,
   *                                  used to compensate the projection
   *  @param[in]  view_valid          Indicate if views are valid
   */
  void MatchContourWithImage(const PPLData::ModelPoseParametersStruct& model_pose,
                             const std::vector<cv::Mat>& image_contour,
                             const std::vector<cv::Point2f>& face_region_origin,
                             const std::vector<bool>& view_valid);

#pragma mark -
#pragma mark Setters / Getters

  /**
   *  @name   set_mesh
   *  @fn void set_mesh(const cv::Mat& mesh)
   *  @brief  Define the mesh to use for the contour extraction
   *  @param  mesh   Mesh
   */
  void set_mesh(const cv::Mat& mesh){ mesh_ = mesh;};

  /**
   *  @name       get_contour_size
   *  @fn int get_contour_size(void) const
   *  @brief      Provide the size of the contour
   *  @return     Contour size
   */
  int get_contour_size(void) const {return nb_contour_line_;};

  /**
   *  @name   get_contour_candidates
   *  @fn const std::vector<int>& get_contour_candidates(void) const
   *  @brief  Provide a list if vertex indexes corresponding to
   *          possible contour candidates
   *  @return Reference to contour candidates
   */
  const std::vector<int>& get_contour_candidates(void) const {
    return candidate_vertex_idx_;
  };

  /**
   *  @name   get_contour_3d
   *  @fn const std::vector< std::vector<int> >& get_contour_3d(void) const
   *  @brief  Provide index corresponding to the 3D contour (chin)
   *  @return Vector of int corresponding to contour
   */
  const std::vector< std::vector<int> >& get_contour_3d(void) const {
    return contour_3d_;
  };

  /**
   *  @name   get_matched_contour
   *  @fn const std::vector< std::vector<int> >& get_matched_contour(void) const
   *  @brief  Provide list of indexes that have corresponding points
   *          on the image plane
   *  @return Vector of int for each view corresponding to the selected
   *          3D points
   */
  const std::vector< std::vector<int> >& get_matched_contour(void) const {
    return matched_contour_3d_;
  };

  /**
   *  @name   get_matched_global_contour
   *  @fn const std::vector< std::vector<int> >& get_matched_global_contour(void) const
   *  @brief  Provide list of indexes (Global, i.e. relative to whole mesh)
   *          that have corresponding points on the image plane
   *  @return Vector of int for each view corresponding to the selected
   *          3D points
   */
  const std::vector<std::vector<int>>& get_matched_global_contour(void) const {
    return matched_global_idx_contour_3d_;
  };

  /**
   *  @name   GetMatchedImagePosition
   *  @fn const std::vector< std::vector<float> >& get_matched_image_position(void) const
   *  @brief  Provide the position on the image plane of the extracted
   *          contour.
   *  @return Vector of float holding the contour position on the image
   *          plane.
   */
  const std::vector< std::vector<float> >& get_matched_image_position(void) const {
    return matched_contour_img_position_;
  };

#pragma mark -
#pragma mark Debug

  /**
   *  @name   Dump3DContourIntoFile
   *  @fn int Dump3DContourIntoFile(const std::string& filename) const
   *  @brief  Dump into a binary file the 3D contour extracted
   *  @param[in]  filename    Where to write output
   *  @return -1 If error
   */
  int Dump3DContourIntoFile(const std::string& filename) const;

  /**
   *  @name   WriteInternalMatToFile
   *  @fn void WriteInternalMatToFile(const std::string& filename) const
   *  @brief  Dump internal matrices into file
   *  @param[in]  filename    Where to export data
   */
  void WriteInternalMatToFile(const std::string& filename) const;

#pragma mark -
#pragma mark Private Functions

  /**
   *  @name   IsInRange
   *  @fn static int IsInRange(const std::vector<std::pair<float, float>>& bounds,
                               const float value)
   *  @brief  Look into constraint vector (i.e. aBoounds) and if value
   *          is included in the range, provide the corresponding index
   *  @param[in]  bounds     Range for a given angle
   *  @param[in]  value      Angle to look for inside the range list
   *  @return     Index where the value is inside the range, -1 otherwise
   */
  static int IsInRange(const std::vector<std::pair<float, float>>& bounds,
                       const float value);

 private :
  /**
   *  @name   InitMatchedContainer
   *  @fn void InitMatchedContainer(void)
   *  @brief  Initiliation of internal container for 3D-2D matching step
   */
  void InitMatchedContainer(void);

  /**
   *  @name   HasFoundMatch
   *  @fn static bool HasFoundMatch(const T value)
   *  @brief  Indicate if the element in the vector has a match in the
   *          contour. No match is indicate by -1
   *  @param[in]  value      Vector value
   *  @return     True if value != -1
   */
  template<class T>
  static bool HasFoundMatch(const T value);

  /**
   *  @name   SetValidFeatures
   *  @fn void SetValidFeatures(const float& theta,const float& phi)
   *  @brief  Provide a List of valid vertex index used to match with images
   *  @param[in]  theta  Head pose, theta (left.right)
   *  @param[in]  phi    Head pose, phi  (up,down)
   */
  void SetValidFeatures(const float& theta,const float& phi);
};
}
#endif /* __LTS5_CONTOUR_EXTRACTOR__ */
