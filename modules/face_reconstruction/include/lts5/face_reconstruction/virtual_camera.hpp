/**
 *  @file   virtual_camera.hpp
 *  @brief  Camera model
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 24/02/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_VIRTUAL_CAMERA__
#define __LTS5_VIRTUAL_CAMERA__

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  VirtualCamera
 *  @brief  Camera parameters
 *  @author Christophe Ecabert
 *  @date   24.02.2014
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS VirtualCamera {
 private :
  /** Indicate if it simulate a camera or not */
  bool real_camera_;
  /** Camera position in world reference frame*/
  float position_[3];
  /** Focal length, x-y*/
  float focal_[2];
  /** Image center points */
  float center_point_[2];
  /** Distortion coefficients */
  float distortion_coefficient_[5];
  /** Intrinsic camera parameters */
  cv::Mat intrinsic_matrix_;
  /** Extrinsic camera parameters */
  cv::Mat extrinsic_matrix_;
  /** Camera orientation in world ref */
  cv::Mat camera_orientation_;
  /** Camera projection matrix */
  cv::Mat projection_matrix_;
  /** Back projection matrix : B = Rcam' * Kinv */
  cv::Mat back_projection_matrix_;

  /**
   *  @name   InitMatrix
   *  @fn void InitMatrix(void)
   *  @brief  Initialize internal matrices such as projection_matrix_,back_projection_matrix_,camera_orientation_,...
   */
  void InitMatrix(void);

  public :

  /**
   *  @name   VirtualCamera
   *  @fn VirtualCamera(void)
   *  @brief  Basic constructor
   */
  VirtualCamera(void);

  /**
   *  @name   VirtualCamera
   *  @fn VirtualCamera(const float x,
                        const float y,
                        const float z)
   *  @brief  Constructor
   *  @param[in]  x      X location
   *  @param[in]  y      Y location
   *  @param[in]  z      Z location
   */
  VirtualCamera(const float x,
                const float y,
                const float z);

  /**
   *  @name   VirtualCamera
   *  @fn VirtualCamera(const float x,
                        const float y,
                        const float z,
                        const float focal_x,
                        const float focal_y)
   *  @brief  Constructor
   *  @param[in] x                  X location
   *  @param[in] y                  Y location
   *  @param[in] z                  Z location
   *  @param[in] focal_x       Camera's lens focal length X
   *  @param[in] focal_y       Camera's lens focal length Y
   */
  VirtualCamera(const float x,
                const float y,
                const float z,
                const float focal_x,
                const float focal_y);

  /**
   *  @name   VirtualCamera
   *  @fn VirtualCamera(const float x,
                        const float y,
                        const float z,
                        const float focal_x,
                        const float focal_y,
                        const float image_center_x,
                        const float image_center_y,
                        bool real_camera = false)
   *  @brief  Constructor
   *  @param[in] x              X location
   *  @param[in] y              Y location
   *  @param[in] z              Z location
   *  @param[in] focal_x   Camera's lens focal length X
   *  @param[in] focal_y   Camera's lens focal length Y
   *  @param[in] image_center_x             Camera center points in x
   *  @param[in] image_center_y             Camera center points in y
   *  @param[in] real_camera        Indicate if it simulate camera of not
   */
  VirtualCamera(const float x,
                const float y,
                const float z,
                const float focal_x,
                const float focal_y,
                const float image_center_x,
                const float image_center_y,
                bool real_camera = false);

  /**
   *  @name   VirtualCamera
   *  @fn VirtualCamera(const std::string& camera_calibration)
   *  @brief  Load calibration information (Intrinsic+Extrinsic) from binary File (*.cal)
   *  @param[in] camera_calibration   Camera calibration parameters
   */
  VirtualCamera(const std::string& camera_calibration);

  /**
   *  @name   ~VirtualCamera
   *  @fn ~VirtualCamera(void)
   *  @brief  Destructor
   */
  ~VirtualCamera(void);

  /**
   *  @name   InitIntrinsicParameters
   *  @fn void InitIntrinsicParameters(void)
   *  @brief  Init intrinsic matrix
   */
  void InitIntrinsicParameters(void);

  /**
   *  @name   InitExtrinsicParameters
   *  @fn void InitExtrinsicParameters(void)
   *  @brief  Init extrinsic matrix with given position, assuming camera is pointing at origin
   */
  void InitExtrinsicParameters(void);

#pragma mark -
#pragma mark Accessors
  /**
   *  @name   SetPosition
   *  @fn void set_position(const float x,const float y, const float z)
   *  @brief  Set the camera position in the world coordinate reference
   *  @param[in] x      X location
   *  @param[in] y      Y location
   *  @param[in] z      Z location
   */
  void set_position(const float x,const float y, const float z) {
    position_[0] = x;
    position_[1] = y;
    position_[2] = z;
  }

  /**
   *  @name   SetFocal
   *  @fn void set_focal(const float focal_x,const float focal_y)
   *  @brief  Set the camera focal length
   *  @param[in] focal_x    Camera's lens focal length in X
   *  @param[in] focal_y    Camera's lens focal length in Y
   */
  void set_focal(const float focal_x,const float focal_y) {
    focal_[0] = focal_x;
    focal_[1] = focal_y;
  }

  /**
   *  @name   setImageCenterPts
   *  @fn void set_center_point(const float image_center_x,const float image_center_y)
   *  @brief  Set image center point
   *  @param[in] image_center_x     Center's x coordinate
   *  @param[in] image_center_y     Center's y coordinate
   */
  void set_center_point(const float image_center_x,const float image_center_y){
    center_point_[0] = image_center_x;
    center_point_[1] = image_center_y;
  }

  /**
   *  @name   setDistortionCoef
   *  @fn void setDistortionCoef(const float k0, const float k1, const float k2,
                                 const float k3, const float k4)
   *  @brief  Set the coefficients of distortion
   *  @param[in] k0     First order
   *  @param[in] k1     Second order
   *  @param[in] k2     third order
   *  @param[in] k3     forth order
   *  @param[in] k4     fifth order
   */
  void setDistortionCoef(const float k0,
                         const float k1,
                         const float k2,
                         const float k3,
                         const float k4) {
    distortion_coefficient_[0] = k0;
    distortion_coefficient_[1] = k1;
    distortion_coefficient_[2] = k2;
    distortion_coefficient_[3] = k3;
    distortion_coefficient_[4] = k4;
  }

  /**
   *  @name   get_position
   *  @fn const float* get_position(void) const
   *  @brief  Gives the position of the camera in world coordinate system
   *  @return Camera position array pointer
   */
  const float* get_position(void) const {
    return &position_[0];
  }

  /**
   *  @name   get_focal
   *  @fn const float* get_focal(void) const
   *  @brief  Gives the camera focal length
   *  @return Camera focal length
   */
  const float* get_focal(void) const {
    return &focal_[0];
  }

  /**
   *  @name   get_center_point
   *  @fn const float* get_center_point(void) const
   *  @brief  Return the image center points
   */
  const float* get_center_point(void) const {
    return &center_point_[0];
  }

  /**
   *  @name   get_distortion_coefficient
   *  @fn const float* get_distortion_coefficient(void) const
   *  @brief  Return reference to camera distortion coefficients
   *  @return Pointer to coefficient's array
   */
  const float* get_distortion_coefficient(void) const {
    return &distortion_coefficient_[0];
  }

  /**
   *  @name   get_intrinsic_matrix
   *  @fn const cv::Mat& get_intrinsic_matrix(void) const
   *  @brief  Give the intrinsic parameters (No deep copy)
   *  @return Intrinsic camera parameters
   */
  const cv::Mat& get_intrinsic_matrix(void) const {
    return intrinsic_matrix_;
  }

  /**
   *  @name   get_extrinsic_matrix
   *  @fn const cv::Mat& get_extrinsic_matrix(void) const
   *  @brief  Give the extrinsic parameters (No deep copy)
   *  @return Extrinsic camera parameters
   */
  const cv::Mat& get_extrinsic_matrix(void) const {
    return extrinsic_matrix_;
  }

  /**
   *  @name   get_projection_matrix
   *  @fn const cv::Mat& get_projection_matrix(void) const
   *  @brief  Provide the projection matrix : i.e. P = K*[R|T]
   *  @return 3x4 projection matrix
   */
  const cv::Mat& get_projection_matrix(void) const {
    return projection_matrix_;
  }

  /**
   *  @name   get_back_projection_matrix
   *  @fn const cv::Mat& get_back_projection_matrix(void) const
   *  @brief  Provide matrix for back projection : i.e. B = R' * Kinv
   *  @return 3x3 back projection matrix
   */
  const cv::Mat& get_back_projection_matrix(void) const {
    return back_projection_matrix_;
  }

  /**
   *  @name   get_camera_orientation
   *  @fn const cv::Mat& get_camera_orientation(void) const
   *  @brief  Provide the camera orientation in the world coordinate
   *  @return 3x3 Matrix representing coordinate system in world reference
   */
  const cv::Mat& get_camera_orientation(void) const {
    return camera_orientation_;
  }

  /**
   *  @name       get_camera_axis_x
   *  @fn cv::Mat get_camera_axis_x(void) const
   *  @brief      Provide camera x axis
   *  @return     x axis
   */
  cv::Mat get_camera_axis_x(void) const {
    return camera_orientation_(cv::Rect(0,0,1,3));
  }

  /**
   *  @name       get_camera_axis_y
   *  @fn cv::Mat get_camera_axis_y(void) const
   *  @brief      Provide camera y axis
   *  @return     y axis
   */
  cv::Mat get_camera_axis_y(void) const {
    return camera_orientation_(cv::Rect(1,0,1,3));
  }

  /**
   *  @name       get_camera_axis_z
   *  @fn cv::Mat get_camera_axis_z(void) const
   *  @brief      Provide camera z axis
   *  @return     z axis
   */
  cv::Mat get_camera_axis_z(void) const {
    return camera_orientation_(cv::Rect(2,0,1,3));
  }
};
}
#endif /* __LTS5_VIRTUAL_CAMERA__ */
