/**
 *  @file   data_generator.hpp
 *  @brief  Generate synthetic data for reconstruction testing
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 09/05/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_DATA_GENERATOR__
#define __LTS5_DATA_GENERATOR__

#include <iostream>
#include <vector>
#include <random>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_reconstruction/virtual_camera.hpp"
#include "lts5/face_reconstruction/face_model_fitter.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  DataGenerator
 *  @brief  Class to generate synthetic landmarks from 3D shape
 *  @author Christophe Ecabert
 *  @date   24/03/15
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS DataGenerator {
 private :
  /** List of possible blendshape */
  std::vector<std::string> blendshape_list_;
  /** Maximum Id */
  int max_identity_;
  /** Maximum Expr */
  int max_expression_;
  /** Eye landmark indexes */
  const int index_eye_vertex_[4] = {36,39,42,45};
  /** Eye distance */
  float eye_distance_;

  /** Number of element inside mesh */
  int number_of_elements_;
  /** Number of vertex inside mesh */
  int number_of_vertex_;
  /** Synthetic mesh */
  Mesh<float>* mesh_;
  /** Synthetic landmarks */
  std::vector<cv::Mat> synthetic_landmarks_;

  /** Random number generator engine */
  std::mt19937_64 random_generator_;
  /** Pose estimated by face tracker */
  float   pose_angle_[3];

  /** Number of views */
  int number_of_view_;
  /** Number of landmarks */
  int number_2D_features_;

  /** Index corresponding to landmarks */
  std::vector<int> index_features_;
  /** Camera setup structure */
  std::vector<LTS5::VirtualCamera*> camera_setup_;

  /** Indicate if generator have been set */
  bool initialized_;
  /** Connectivity build flag */
  bool has_connectivity_;

 public :

  /**
   *  @struct SyntheticDataParameter
   *  @brief  Structure holding parameters need to generate synthetic data from FaceWarehouse database
   */
  struct SyntheticDataParameter {
    /** Path to the root folder where database is stored */
    std::string root_path;
    /** Shape extension type */
    std::string extension_type;
    /** Angle 1 */
    float gamma;
    /** Angle 2 */
    float theta;
    /** Angle 3 */
    float phi;
    /** Translation X */
    float delta_x;
    /** Translation Y */
    float delta_y;
    /** Translation Z */
    float delta_z;
    /** Scale */
    float scale;
    /** Activate noise on landmark */
    bool add_noise;
    /** Noise standard deviation - amount of noise to add */
    float noise_std;
    /** Select blendshape identity */
    int id;
    /** Select the blendshape expression */
    int expr;
    /** Number of view used {1 - 2} */
    int number_views;
    /** Error */
    bool show_error;
  };

  /**
   *  @struct ModelInformation
   *  @brief  Information about model
   */
  struct ModelInformation {
    /** Where coef ground truth are located */
    std::string coef_folder;
    /** Number of vertex */
    int number_elements;
    /** Model dimension (RankId / RANK_EXPRESSION / Id / Expr) */
    cv::Scalar_<int> model_dimension;
  };

  /**
   *  @enum   ParamSelectionEnum
   *  @brief  Weight coefficients selection
   */
  enum ParamSelectionEnum {
    /** Identity selection */
    kId,
    /** Expression selection */
    kExpr,
    /** Gamma angle */
    gamma,
    /** Theta angle */
    theta,
    /** Phi angle */
    phi
  };

  /**
   *  @struct PersonSequenceParamStruct
   *  @brief  Parameters used to generate Id/Expr sequence
   */
  struct PersonSequenceParamStruct {
    /** Parameter selector */
    ParamSelectionEnum variation_type;
    /** Position to start */
    int start;
    /** Position to stop */
    int stop;
    /** Step */
    float step = 1.0;
  };

  /**
   *  @name DataGenerator
   *  @fn explicit DataGenerator(const std::string& config_file);
   *  @brief  Constructor
   *  @param[in]  config_file Path to the config file
   */
  explicit DataGenerator(const std::string& config_file);

  /**
   *  @name   ~DataGenerator
   *  @fn ~DataGenerator(void)
   *  @brief  Destructor
   */
  ~DataGenerator(void);

  /**
   *  @name   LoadLandmarkVerticesIndex
   *  @fn int LoadLandmarkVerticesIndex(const std::string& landmark_selection_file)
   *  @brief  Load the vertex indexes corresponding to the selected landmarks
   *          from config files
   *  @param  landmark_selection_file   Configuration file
   *  @return 0 if everything went fine otherwise -1.
   */
  int LoadLandmarkVerticesIndex(const std::string& landmark_selection_file);

  /**
   *  @name   generateSyntheticData
   *  @fn int GenerateData(const SyntheticDataParameter& data_parameters)
   *  @brief  Function used to generate some synthetic data from FaceWarehouse
   *          database. It basically select one blendshape and apply a known
   *          transformation on it.
   *  @param  data_parameters      Set of parameters used for data synthesis
   */
  int GenerateData(const SyntheticDataParameter& data_parameters);

  /**
   *  @name   get_synthetic_landmarks
   *  @fn const std::vector<cv::Mat>& get_synthetic_landmarks(void) const
   *  @brief  Return synthetic landmarks
   *  @return Vector of landmarks
   */
  const std::vector<cv::Mat>& get_synthetic_landmarks(void) const {
    return synthetic_landmarks_;
  }

  /**
   *  @name get_mesh
   *  @fn Mesh<float>* get_mesh(void) const
   *  @brief  Provide synthetic data mesh
   *  @return Mesh holding synthetic data
   */
  Mesh<float>* get_mesh(void) const {
    return mesh_;
  }

 private :

  /**
   *  @name   ApplyRigidTransformation
   *  @fn void ApplyRigidTransformation(const SyntheticDataParameter& transformation_parameters)
   *  @brief  Apply rigid transform on shape (Rotation - translation)
   *  @param  transformation_parameters    Transformation parameters
   */
  void ApplyRigidTransformation(const SyntheticDataParameter& transformation_parameters);

  /**
   *  @name   ProjectLandmark
   *  @fn void ProjectLandmark(const SyntheticDataParameter& transformation_parameters)
   *  @brief  Project 3D points onto image plane
   *  @param  transformation_parameters    Transformation parameters
   */
  void ProjectLandmark(const SyntheticDataParameter& transformation_parameters);
};
}  // namespace LTS5

#endif /* __LTS5_DATA_GENERATOR__ */
