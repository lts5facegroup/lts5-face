/**
 *  @file   utilities.hpp
 *  @brief  Helper function used in face synthesis pipeline
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 24/02/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_UTILITIES__
#define __LTS5_UTILITIES__

#include <iostream>
#include <vector>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @namespace  Utilities
 *  @brief      System tools like folder searching,...
 */
namespace Utilities {

  /**
   *  @enum RotationDimension
   *  @brief  List of possible rotation
   *  @ingroup face_reconstruction
   */
  enum RotationDimension {
    /** Rotation in 3d space */
    k3d3d,
    /** Rotation from 3d to 2d */
    k3d2d
  };

  /** Conversion from degree to radian */
  const float DegToRad = CV_PI/180.f;
  /** Conversion from radian to degree */
  const float RadToDeg = 180.f/CV_PI;

  /**
   *  @name NumberToString
   *  @fn std::string NumberToString (T number)
   *  @brief Convert number into strings
   *  @ingroup face_reconstruction
   *  @param[in] number      Number to convert
   *  @return Input converted into string
   */
  template<typename T>
  std::string LTS5_EXPORTS NumberToString (T number);

  /**
   *  @name GetFileListing
   *  @fn void GetFileListing(std::vector<std::string> &files,
                              const std::string &dir,
                              const std::string &subdir = "",
                              const std::string &ext = ".jpg")
   *  @brief  Get a list with the name of the whole file into a specific
   *          directory
   *  @param[out] files   Returned value as a vector string
   *  @param[in]  dir     Full name the directory to check
   *  @param[in]  subdir  Subfolder name, initialise with ""
   *  @param[in]  ext     Extension file type, DAFAULT = ".jpg"
   */
  /*void LTS5_EXPORTS GetFileListing(std::vector<std::string> &files,
                                   const std::string &dir,
                                   const std::string &subdir = "",
                                   const std::string &ext = ".jpg");*/

  /**
   *  @name   WriteMatToCsv
   *  @fn int WriteMatToCsv(const std::string &filename,
                            const cv::Mat &matrix)
   *  @brief  Write OpenCV matrix (cv::Mat()) to .csv file
   *  @ingroup face_reconstruction
   *  @param[in]  filename       Name of the created .csv file
   *  @param[in]  matrix            Matrix to write
   *  @return -1 if error
   */
  int LTS5_EXPORTS WriteMatToCsv(const std::string &filename,
                                 const cv::Mat &matrix);

  /**
   *  @name   WriteMatToBin
   *  @fn int WriteMatToBin(const std::string &filename,
                            const cv::Mat &matrix)
   *  @brief  Write OpenCV matrix (cv::Mat()) to .csv file
   *  @ingroup face_reconstruction
   *  @param[in]  filename       Name of the created .csv file
   *  @param[in]  matrix            Matrix to write
   *  @return -1 if error
   */
  int LTS5_EXPORTS WriteMatToBin(const std::string &filename,
                                 const cv::Mat &matrix);

  /**
   *  @name   ReadCsvToMat
   *  @fn int ReadCsvToMat(const std::string& filename, cv::Mat* matrix)
   *  @brief  Fill a matrix with values from an .csv file
   *  @ingroup face_reconstruction
   *  @param[in]  filename   Csv filename
   *  @param[out] matrix        Matrix to fill
   *  @return -1 if error
   */
  int LTS5_EXPORTS ReadCsvToMat(const std::string& filename, cv::Mat* matrix);

  /**
   *  @name   ReadBinToMat
   *  @fn int ReadBinToMat(const std::string &filename, cv::Mat& matrix)
   *  @brief  Fill a matrix with values from an .csv file
   *  @ingroup face_reconstruction
   *  @param[in]  filename   Csv filename
   *  @param[out] matrix        Matrix to fill
   *  @return -1 if error
   */
  int LTS5_EXPORTS ReadBinToMat(const std::string &filename, cv::Mat& matrix);

  /**
   *  @name   SortByNumber
   *  @fn bool SortByNumber(const std::string& A,const std::string& B)
   *  @brief  Sort string by number increasing order
   *  @ingroup face_reconstruction
   *  @param[in]  A   First string
   *  @param[in]  B   Second string
   *  @return True if A < B
   */
  bool LTS5_EXPORTS SortByNumber(const std::string& A,const std::string& B);

  /**
   *  @name   GetRotationMatrix
   *  @fn void GetRotationMatrix(const float gamma,
                              const float theta,
                              const float phi,
                              const Utilities::RotationDimension& rotation_type,
                              const bool is_rad,
                              cv::Mat* rotation)
   *  @brief  Generate rotation matrix for specific angles in degrees : Gamma, Theta, Phi
   *  @ingroup face_reconstruction
   *  @param[in]  gamma          Rotation around Z axis
   *  @param[in]  theta          Rotation around X axis
   *  @param[in]  phi            Rotation around Y axis
   *  @param[in]  rotation_type  Rotation type
   *  @param[in]  is_rad         Indicates if angle are in radians
   *  @param[out] rotation       Rotation matrix
   */
  void LTS5_EXPORTS GetRotationMatrix(const float gamma,
                                      const float theta,
                                      const float phi,
                                      const Utilities::RotationDimension& rotation_type,
                                      const bool is_rad,
                                      cv::Mat* rotation);

  /**
   *  @name   GetRotationAngle
   *  @fn void GetRotationAngle(const cv::Mat& rotation_matrix,
                                float* gamma,
                                float* theta,
                                float* phi)
   *  @brief  Given a rotation matrix, computes the corresponding rotation angle.
   *  @ingroup face_reconstruction
   *  @param[in]  rotation_matrix        Rotation matrix
   *  @param[out] gamma   Gamma angle
   *  @param[out] theta   Theta angle
   *  @param[out] phi     Phi angle
   */
  void LTS5_EXPORTS GetRotationAngle(const cv::Mat& rotation_matrix,
                                     float* gamma,
                                     float* theta,
                                     float* phi);

  /**
   *  @name   MatrixRowReductionEchelonForm
   *  @fn void MatrixRowReductionEchelonForm(cv::Mat* matrix)
   *  @brief  Perform row reduction in form of echelon matrix
   *  @ingroup face_reconstruction
   *  @param[in,out]  matrix  Matrix to reduce
   *  @see    http://rosettacode.org/wiki/Reduced_row_echelon_form#C.2B.2B
   */
  void LTS5_EXPORTS MatrixRowReductionEchelonForm(cv::Mat* matrix);

  /**
   *  @name   SwapMatrixRowsFloat
   *  @fn void SwapMatrixRowsFloat(cv::Mat* matrix,int i, int j)
   *  @brief  Swap the ith row of A with the jth row of A
   *  @ingroup face_reconstruction
   *  @param[in,out]  matrix  Matrix
   *  @param[in]  i   Src row to swap
   *  @param[in]  j   Dst row to swap
   */
  void LTS5_EXPORTS SwapMatrixRowsFloat(cv::Mat* matrix,int i, int j);

  /**
   *  @name   SwapMatrixRowsDouble
   *  @fn void SwapMatrixRowsDouble(cv::Mat* matrix,int i, int j)
   *  @brief  Swap the ith row of A with the jth row of A
   *  @ingroup face_reconstruction
   *  @param[in,out]  matrix  Matrix
   *  @param[in]      i   Src row to swap
   *  @param[in]      j   Dst row to swap
   */
  void LTS5_EXPORTS SwapMatrixRowsDouble(cv::Mat* matrix,int i, int j);

  /**
   *  @name   SubstractRowsByMultipleFloat
   *  @fn void SubstractRowsByMultipleFloat(cv::Mat* matrix, int i, int j,
                                            float multiple)
   *  @brief  Substract multiple mutiplied by row i from row j
   *  @ingroup face_reconstruction
   *  @param[in,out]  matrix      Input matrix
   *  @param[in]  i       Row to multiply
   *  @param[in]  j       Row to substract to
   *  @param[in]  multiple    Value to multiply
   */
  void LTS5_EXPORTS SubstractRowsByMultipleFloat(cv::Mat* matrix,
                                                 int i,
                                                 int j,
                                                 float multiple);

  /**
   *  @name   SubstractRowsByMultipleDouble
   *  @fn void SubstractRowsByMultipleDouble(cv::Mat* matrix, int i, int j,
                                             double multiple);
   *  @brief  Substract multiple mutiplied by row i from row j
   *  @ingroup face_reconstruction
   *  @param[in,out]  matrix      Input matrix
   *  @param[in]  i       Row to multiply
   *  @param[in]  j       Row to substract to
   *  @param[in]  multiple    Value to multiply
   */
  void LTS5_EXPORTS SubstractRowsByMultipleDouble(cv::Mat* matrix,
                                                  int i,
                                                  int j,
                                                  double multiple);

  /**
   *  @name   DivideRowFloat
   *  @fn void DivideRowFloat(cv::Mat* matrix, int i, float value)
   *  @brief  Divide a specific row by value value.
   *  @ingroup face_reconstruction
   *  @param[in,out]  matrix      Input matrix
   *  @param[in]  i       Row to divide
   *  @param[in]  value    Value to divide by.
   */
  void LTS5_EXPORTS DivideRowFloat(cv::Mat* matrix, int i, float value);

  /**
   *  @name   DivideRowDouble
   *  @fn void DivideRowDouble(cv::Mat* matrix, int i, double value)
   *  @brief  Divide a specific row by value value.
   *  @ingroup face_reconstruction
   *  @param[in,out]  matrix  Input matrix
   *  @param[in]  i           Row to divide
   *  @param[in]  value       Value to divide by.
   */
  void LTS5_EXPORTS DivideRowDouble(cv::Mat* matrix, int i, double value);

  /**
   *  @name   LoadIndexListFromFile
   *  @fn void LoadIndexListFromFile(const std::string& filename,
                                     std::vector<int>* index)
   *  @brief  Load a list of index from file
   *  @ingroup face_reconstruction
   *  @param[in]  filename   File where indexes are stored
   *  @param[in]  index      Vector including the list of index
   */
  void LTS5_EXPORTS LoadIndexListFromFile(const std::string& filename,
                                          std::vector<int>* index);

  /**
   *  @name   GetCmdOption
   *  @fn char* GetCmdOption(char ** begin,
                             char ** end,
                             const std::string& option)
   *  @brief  Give the content of a given option
   *  @ingroup face_reconstruction
   *  @param[in]  begin       Begining of the command line args
   *  @param[in]  end         End of the command line args
   *  @param[in]  option      Which option to look for i.e : -e
   *  @return Return the command
   */
  char* LTS5_EXPORTS GetCmdOption(char ** begin,
                                  char ** end,
                                  const std::string& option);

  /**
   *  @name   CmdOptionExists
   *  @fn bool CmdOptionExists(const char** begin,
                               const char** end,
                               const std::string& option)
   *  @brief  Check if the command exist inside the argument from cmd line
   *  @ingroup face_reconstruction
   *  @param[in]  begin       Begining of the command line args
   *  @param[in]  end         End of the command line args
   *  @param[in]  option      Which option to look for i.e : -e
   *  @return Indicate if a command is present or not
   */
  bool LTS5_EXPORTS CmdOptionExists(const char** begin,
                                    const char** end,
                                    const std::string& option);

  /**
   *  @name   JumpToLineInFile
   *  @fn void JumpToLineInFile(std::ifstream& file,
                                int line_number)
   *  @brief  Jump to specific line into a file
   *  @ingroup face_reconstruction
   *  @param[in]  file           File input stream
   *  @param[in]  line_number    Which line to jump
   */
  void LTS5_EXPORTS JumpToLineInFile(std::ifstream& file,
                                     int line_number);

  /**
   *  @name   ImageMedian
   *  @fn float ImageMedian(const cv::Mat& image)
   *  @brief  Compute median value of the image
   *  @param[in]  image      Image input
   *  @return Median value of the image
   */
  float LTS5_EXPORTS ImageMedian(const cv::Mat& image);

  /**
   *  @name   AutomaticCannyThresholdComputation
   *  @fn void AutomaticCannyThresholdComputation(const cv::Mat& image,
                                                    int* lower_bound_thresh,
                                                    int* upper_bound_thresh,
                                                    const float sigma = 0.33f)
   *  @brief  Estimate canny threshold based on image properties
   *  @ingroup face_reconstruction
   *  @param[in]  image          I    nput image
   *  @param[out] lower_bound_thresh  Lower threshold
   *  @param[out] upper_bound_thresh  Upper threshold
   *  @param[in]  sigma               Percentage (+/-)
   */
  void LTS5_EXPORTS AutomaticCannyThresholdComputation(const cv::Mat& image,
                                                       int* lower_bound_thresh,
                                                       int* upper_bound_thresh,
                                                       const float sigma = 0.33f);

  /**
   *  @name   AutomaticCannyEdge
   *  @fn void AutomaticCannyEdge(const cv::Mat& image,
                                  cv::Mat* edges,
                                  const float sigma = 0.33f)
   *  @brief  Extract edge from single channel images
   *  @ingroup face_reconstruction
   *  @param[in]  image      Source image
   *  @param[out] edges      Extracted edges
   *  @param[in]  sigma      Threshold percentages
   *  @see http://www.pyimagesearch.com/2015/04/06/zero-parameter-automatic-canny-edge-detection-with-python-and-opencv/
   */
  void LTS5_EXPORTS AutomaticCannyEdge(const cv::Mat& image,
                                       cv::Mat* edges,
                                       const float sigma = 0.33f);
}
}

#endif /* __LTS5_UTILITIES__ */
