/**
 *  @file   pointgrey_stream.hpp
 *  @brief  PointGrey camera stream
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   01/06/16
 *  Copyright © 2016 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_POINTGREY_STREAM__
#define __LTS5_POINTGREY_STREAM__

#include "lts5/utils/library_export.hpp"
#include "lts5/face_reconstruction/base_stream.hpp"
#ifdef HAS_POINTGREY_
#include "lts5/pointgrey/rig.hpp"
#endif

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  PointGreyStream
 *  @brief  PointGrey camera stream
 *  @author Christophe Ecabert
 *  @date   01/06/2016
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS PointGreyStream : public LTS5::BaseStream {
public:

#pragma mark -
#pragma mark Type definition

  /**
   *  @struct Parameter
   *  @brief  Stream Parameter
   */
  struct Parameter : BaseStream::BaseParameter {
    /** External trigger */
    bool external_trigger;
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name       PointGreyStream
   *  @fn         PointGreyStream(const PointGreyStream::Parameter& params)
   *  @brief      Constructor
   *  @param[in]  params  Stream parameters
   */
  PointGreyStream(const PointGreyStream::Parameter& params);

  /**
   *  @name       PointGreyStream
   *  @fn         PointGreyStream(const PointGreyStream& other) = delete
   *  @brief      Copy Constructor
   *  @param[in]  other Object to copy from
   */
  PointGreyStream(const PointGreyStream& other) = delete;

  /**
   *  @name       operator=
   *  @fn         PointGreyStream& operator=(const PointGreyStream& rhs) = delete
   *  @brief      Assignment operator
   *  @param[in]  rhs Object to assign from
   *  @return     Assigned object
   */
  PointGreyStream& operator=(const PointGreyStream& rhs) = delete;

  /**
   *  @name       ~PointGreyStream
   *  @fn         ~PointGreyStream(void)
   *  @brief      Destructor
   */
  ~PointGreyStream(void);

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Read
   *  @fn int Read(void)
   *  @brief  Read image from the stream
   *  @return -1 if error, 0 otherwise
   */
  int Read(void);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   get_frame_index
   *  @fn     int get_frame_index(void) const
   *  @brief  Provide the index of the current frame
   *  @return Frame index
   */
  int get_frame_index(void) const {
    return n_frame_loaded_;
  }

  /**
   *  @name   get_n_max_frame
   *  @fn     int get_n_max_frame(void) const
   *  @brief  Provide the maximum index of the frame
   *  @return Maximum Frame index
   */
  int get_n_max_frame(void) const {
    return n_max_frame_;
  }

  /**
   *  @name get_frame_name
   *  @fn void get_frame_name(std::string* frame_name, std::string* frame_ext) const
   *  @brief  Provide the name of the current frame according to the media
   *  @param[out] frame_name  Frame name
   *  @param[out] frame_ext   Frame extension
   */
  void get_frame_name(std::string* frame_name, std::string* frame_ext) const;

#pragma mark -
#pragma mark Private

private:
  /** Parameters */
  Parameter params_;
  /** Number of frame already loaded */
  int n_frame_loaded_;
  /** Maximum frame number */
  int n_max_frame_;
  /** Camera */
#ifdef HAS_POINTGREY_
  LTS5::PGCameraRig* caps_;
#endif
};

}  // namespace LTS5
#endif /* __LTS5_POINTGREY_STREAM__ */
