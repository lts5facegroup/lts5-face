/**
 *  @file   external_video_stream.hpp
 *  @brief  External Stream class to handle data importation via
 *          callbacks interface
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   22/07/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_EXTERNAL_VIDEO_STREAM__
#define __LTS5_EXTERNAL_VIDEO_STREAM__

#include "lts5/utils/library_export.hpp"
#include "lts5/face_reconstruction/base_stream.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   ExternalVideoStream
 * @brief   External Stream class to handle data importation via
 *          callbacks interface
 * @author  Christophe Ecabert
 * @date    22/07/2016
 * @ingroup face_reconstruction
 */
class LTS5_EXPORTS ExternalVideoStream : public LTS5::BaseStream {
 public:

#pragma mark -
#pragma mark Type definition

  /**
   * @struct  ICallbacks
   * @brief Callbacks interface needed by the stream
   */
  struct ICallbacks {

    /**
     * @name  ~ICallbacks
     * @fn  virtual ~ICallbacks(void)
     * @brief Destructor
     */
    virtual ~ICallbacks(void) {}

    /**
     * @name  Read
     * @fn  virtual int Read(std::vector<cv::Mat>* buffer) = 0;
     * @brief Load image into a given \p buffer
     * @param[out]  buffer  Buffer where image are loaded
     * @return -1 if error, 0 otherwise
     */
    virtual int Read(std::vector<cv::Mat>* buffer) = 0;
  };

  /**
   *  @struct Parameter
   *  @brief  Stream Parameter
   */
  struct Parameter : BaseStream::BaseParameter {

  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   ExternalVideoStream
   *  @fn ExternalVideoStream(const Parameter& param)
   *  @brief  Constructor
   *  @param[in]  param Stream parameters
   */
  ExternalVideoStream(const Parameter& param);

  /**
   * @name  ExternalVideoStream
   * @fn  ExternalVideoStream(const ExternalVideoStream& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  ExternalVideoStream(const ExternalVideoStream& other) = delete;

  /**
   * @name  operator=
   * @fn  ExternalVideoStream& operator=(const ExternalVideoStream& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return  Assigned object
   */
  ExternalVideoStream& operator=(const ExternalVideoStream& rhs) = delete;

  /**
   * @name  ~ExternalVideoStream
   * @fn  ~ExternalVideoStream(void)
   * @brief Destructor
   */
  ~ExternalVideoStream(void);

  /**
   * @name  InitCallback
   * @fn  void InitCallback(ICallbacks* callback)
   * @brief Define which callbacks use to load data
   * @param[in] callback  Callback to use by the stream
   */
  void InitCallback(ICallbacks* callback);

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Read
   *  @fn int Read(void)
   *  @brief  Read image from the stream
   *  @return -1 if error, 0 otherwise
   */
  int Read(void);

#pragma mark -
#pragma mark Accessors

  /**
 *  @name get_frame_name
 *  @fn void get_frame_name(std::string* frame_name, std::string* frame_ext) const
 *  @brief  Provide the name of the current frame according to the media
 *  @param[out] frame_name  Frame name
 *  @param[out] frame_ext   Frame extension
 */
  void get_frame_name(std::string* frame_name, std::string* frame_ext) const;

#pragma mark -
#pragma mark Private

 private:
  /** Callback */
  ICallbacks* callback_;
  /** Number of frame already loaded */
  int n_frame_loaded_;

};

}  // namepsace LTS5
#endif //__LTS5_EXTERNAL_VIDEO_STREAM__
