/**
 *  @file   video_stream.hpp
 *  @brief  Handle video input
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   01/06/16
 *  Copyright © 2016 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_VIDEO_STREAM__
#define __LTS5_VIDEO_STREAM__

#include <vector>
#include <string>

#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_reconstruction/base_stream.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  VideoStream
 *  @brief  Handle video input
 *  @author Christophe Ecabert
 *  @date   01/06/2016
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS VideoStream : public LTS5::BaseStream {
 public:

#pragma mark -
#pragma mark Type definition

  /**
   *  @struct Parameter
   *  @brief  Stream Parameter
   */
  struct Parameter : BaseStream::BaseParameter {
    /** Video folder */
    std::string root_folder;
    /** Video extension */
    std::string video_ext;
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name       VideoStream
   *  @fn         VideoStream(const VideoStream::Parameter& params)
   *  @brief      Constructor
   *  @param[in]  params  Stream parameters
   */
  VideoStream(const VideoStream::Parameter& params);

  /**
   *  @name       VideoStream
   *  @fn         VideoStream(const VideoStream& other) = delete
   *  @brief      Copy Constructor
   *  @param[in]  other Object to copy from
   */
  VideoStream(const VideoStream& other) = delete;

  /**
   *  @name       operator=
   *  @fn         VideoStream& operator=(const VideoStream& rhs) = delete
   *  @brief      Assignment operator
   *  @param[in]  rhs Object to assign from
   *  @return     Assigned object
   */
  VideoStream& operator=(const VideoStream& rhs) = delete;

  /**
   *  @name       ~VideoStream
   *  @fn         ~VideoStream() override
   *  @brief      Destructor
   */
  ~VideoStream() override;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Read
   *  @fn int Read() override
   *  @brief  Read image from the stream
   *  @return -1 if error, 0 otherwise
   */
  int Read() override;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_frame_to_process
   *  @fn void set_frame_to_process(int frame_index) override
   *  @brief  Set from which frame the pipeline process
   *  @param[in]  frame_index   Frame's index
   */
  void set_frame_to_process(int frame_index) override {
    n_frame_loaded_ = (frame_index > n_max_frame_ ?
                       frame_index :
                       n_max_frame_ - 1);
  }

  /**
   *  @name   get_frame_index
   *  @fn     int get_frame_index() const override
   *  @brief  Provide the index of the current frame
   *  @return Frame index
   */
  int get_frame_index() const override {
    return n_frame_loaded_;
  }

  /**
   *  @name   get_n_max_frame
   *  @fn     int get_n_max_frame() const override
   *  @brief  Provide the maximum index of the frame
   *  @return Maximum Frame index
   */
  int get_n_max_frame() const override {
    return n_max_frame_;
  }

  /**
   *  @name get_frame_name
   *  @fn void get_frame_name(std::string* frame_name,
   *                          std::string* frame_ext) const override
   *  @brief  Provide the name of the current frame according to the media
   *  @param[out] frame_name  Frame name
   *  @param[out] frame_ext   Frame extension
   */
  void get_frame_name(std::string* frame_name,
                      std::string* frame_ext) const override;

#pragma mark -
#pragma mark Private

 private:
  /** Parameters */
  Parameter params_;
  /** Number of frame already loaded */
  int n_frame_loaded_;
  /** Maximum frame number */
  int n_max_frame_;
  /** List of video */
  std::vector<std::string> list_video_;
  /** Capture device */
  std::vector<cv::VideoCapture> caps_;
};

}  // namespace LTS5
#endif /* __LTS5_VIDEO_STREAM__ */
