/**
 *  @file   sim_forward_fit.hpp
 *  @brief  Simultaneous Forward fit (Solve for shape and texture parameters at
 *          the same time)
 *  @ingroup    face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   19/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_SIM_FORWARD_FIT__
#define __LTS5_SIM_FORWARD_FIT__

#include "lts5/utils/library_export.hpp"
#include "lts5/face_reconstruction/fit_algorithm.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   SimultaneousFit
 * @brief   Simultaneous Forward fit (Solve for shape and texture parameters at
 *          the same time)
 * @author  Christophe Ecabert
 * @date    19/09/2017
 * @ingroup face_reconstruction
 * @tparam T Data type
 */
template<typename T>
class LTS5_EXPORTS SimultaneousFit : public MMFitAlgorithm<T> {
 public:

#pragma mark -
#pragma mark Type Definition

  /** Mesh Type */
  using Mesh = typename MMFitAlgorithm<T>::Mesh;
  /** Camera type */
  using Camera_t = typename MMFitAlgorithm<T>::Camera_t;
  /** Vertex type */
  using Vertex = typename MMFitAlgorithm<T>::Vertex;
  /** Statistics type */
  using Stat = typename MMFitAlgorithm<T>::Stat;
  /** Regularization type */
  using Regularization = typename MMFitAlgorithm<T>::Regularization;
  /** ImagePoint type */
  using ImagePoint = typename MMFitAlgorithm<T>::ImagePoint;
  /** Instance Type */
  using InstanceType = typename MorphableModel<T>::InstanceType;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  SimultaneousFit
   * @fn    explicit SimultaneousFit(const MorphableModel<T>* model)
   * @brief Constructor
   * @param[in] model       Model to fit
   */
  explicit SimultaneousFit(const MorphableModel<T>* model);

  /**
   * @name  SimultaneousFit
   * @fn    SimultaneousFit(const SimultaneousFit<T>& other) = delete
   * @brief Copy Constructor
   * @param[in] other   Object to copy from
   */
  SimultaneousFit(const SimultaneousFit<T>& other) = delete;

  /**
   * @name  SimultaneousFit
   * @fn    SimultaneousFit(SimultaneousFit<T>&& other) = delete
   * @brief Move Constructor
   * @param[in] other   Object to move from
   */
  SimultaneousFit(SimultaneousFit<T>&& other) = delete;

  /**
   * @name  operator=
   * @fn    SimultaneousFit<T>& operator=(const SimultaneousFit<T>& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned from
   */
  SimultaneousFit<T>& operator=(const SimultaneousFit<T>& rhs) = delete;

  /**
   * @name  operator=
   * @fn    SimultaneousFit<T>& operator=(SimultaneousFit<T>&& rhs) = delete
   * @brief Move-assignment operator
   * @param[in] rhs Object to move-assign from
   * @return    Newly moved-assign from
   */
  SimultaneousFit<T>& operator=(SimultaneousFit<T>&& rhs) = delete;

  /**
   * @name  SimultaneousFit
   * @fn    ~SimultaneousFit(void)
   * @brief Destructor
   */
  ~SimultaneousFit(void) = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Process
   * @fn    int Process(const cv::Mat& image,
                        const cv::Mat& landmarks,
                        const cv::Mat& shape_p,
                        const cv::Mat& tex_p,
                        const cv::Mat& illu_p,
                        const Regularization& reg,
                        Mesh* mesh,
                        Camera_t* camera,
                        Stat* stat)
   * @brief Estimate the model parameters
   * @param[in] image       Image to fit
   * @param[in] landmarks   Facial landmarks (optional)
   * @param[in] shape_p     Initial shape parameters (optional)
   * @param[in] tex_p       Initial texture parameters (optional)
   * @param[in] illu_p      Initial illumination parameters (optional)
   * @param[in] reg         Regularization factor
   * @param[in, out] mesh   Initial mesh (can be meanshape / texture)
   * @param[in, out] camera Initial camera estimation
   * @param[out] stat       Algorithm statistics (can be nullptr)
   * @return    -2 if numerical error appears, -1 if not converged, 0 otherwise
   */
  int Process(const cv::Mat& image,
              const cv::Mat& landmarks,
              const cv::Mat& shape_p,
              const cv::Mat& tex_p,
              const cv::Mat& illu_p,
              const Regularization& reg,
              Mesh* mesh,
              Camera_t* camera,
              Stat* stat);

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  JacobianData
   * @fn    void JacobianData(const Camera_t& camera,
                    const cv::Mat& shape,
                    const cv::Mat& shape_pc,
                    const cv::Mat& mean_tex,
                    const cv::Mat& tex_pc,
                    const cv::Mat& grad,
                    const cv::Mat& normal,
                    const T reg,
                    cv::Mat* j_data)
   * @brief Compute Jacobian for data term
   * @param[in] camera      Current camera estimation
   * @param[in] shape       Sampled shape   [3N x 1]
   * @param[in] shape_pc    Sampled shape principal component [3N x ns]
   * @param[in] mean_tex    Sampled mean texutre [3N x 1]
   * @param[in] tex_pc      Sampled texture principal component [3N x nt]
   * @param[in] grad        Sampled X/Y gradient  [CN x 2]
   * @param[in] normal      Sampled surface's normals [N x 3]
   * @param[out] j_data     Jacobian for data term  [CN x (ns + nc + nt)]
   */
  void JacobianData(const Camera_t& camera,
                    const cv::Mat& shape,
                    const cv::Mat& shape_pc,
                    const cv::Mat& mean_tex,
                    const cv::Mat& tex_pc,
                    const cv::Mat& grad,
                    const cv::Mat& normal,
                    cv::Mat* j_data);

  /**
   * @name  ComputeUpdate
   * @fn    int ComputeUpdate(const cv::Mat& hessian,
                             const cv::Mat& error,
                             const Regularization& reg,
                             cv::Mat* delta_s,
                             cv::Mat* delta_t,
                             cv::Mat* delta_c,
                             cv::Mat* delta_i)
   * @brief Compute coefficients update given the \p hessian and \p error.
   * @param[in] hessian     Current Hessian estimation
   * @param[in] error       Current SD error estimation
   * @param[in] reg         Regularization factors
   * @param[out] delta_s    Shape update
   * @param[out] delta_t    Texture update
   * @param[out] delta_c    Camera update
   * @param[out] delta_i    Illumination update
   * @return -1 if error, 0 otherwise
   */
  int ComputeUpdate(const cv::Mat& hessian,
                    const cv::Mat& error,
                    const Regularization& reg,
                    cv::Mat* delta_s,
                    cv::Mat* delta_t,
                    cv::Mat* delta_c,
                    cv::Mat* delta_i);

  /**
   * @name  UpdateCamera
   * @fn    void UpdateCamera(const cv::Mat& delta_c, Camera_t* camera)
   * @brief Update current camera estimation
   * @param delta_c Camera update
   * @param camera  Current camera estimation to update
   */
  void UpdateCamera(const cv::Mat& delta_c, Camera_t* camera);

  /** Visible points */
  std::vector<ImagePoint> visible_pts_;
  /** Camera parameters - [f cx cy qx qy qz qw tx ty tz] */
  T cam_p_[10];
};

#pragma mark -
#pragma mark Proxy

/**
 * @class   SimultaneousFitProxy
 * @brief   Registration for Simultaneous Forward Fit
 * @author  Christophe Ecabert
 * @date    21/09/2017
 * @tparam T Data type
 */
template<typename T>
class LTS5_EXPORTS SimultaneousFitProxy : public MMFitAlgorithmProxy<T> {
 public:

  /**
   * @name  SimultaneousFitProxy
   * @fn    SimultaneousFitProxy(void) = default
   * @brief Constructor
   */
  SimultaneousFitProxy(void) = default;

  /**
   * @name  ~SimultaneousFitProxy
   * @fn    ~SimultaneousFitProxy(void) = default
   * @brief Destructor
   */
  ~SimultaneousFitProxy(void) = default;

  /**
   * @name  Create
   * @fn    MMFitAlgorithm<T>* Create(const MorphableModel<T>* model) const
   * @brief Instantiate a specific instance for a given proxy
   * @param[in] model   Morphable model to use for fitting
   * @return    Fitting algorithm
   */
  MMFitAlgorithm<T>* Create(const MorphableModel<T>* model) const;

  /**
   * @name  Name
   * @fn    const char* Name(void) const
   * @brief Indicate the name of that proxy
   * @return    Fit algorithm's name
   */
  const char* Name(void) const;
};

}  // namepsace LTS5
#endif //__LTS5_SIM_FORWARD_FIT__
