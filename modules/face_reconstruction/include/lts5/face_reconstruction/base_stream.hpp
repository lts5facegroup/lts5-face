/**
 *  @file   base_stream.hpp
 *  @brief  Interface template for data streaem
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   01/06/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_BASE_STREAM__
#define __LTS5_BASE_STREAM__

#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/core/cuda.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BaseStream
 *  @brief  Interface template for data streaem
 *  @author Christophe Ecabert
 *  @date   01/06/2016
 *  @ingroup face_reconstruction
 */
class BaseStream {
 public :

#pragma mark -
#pragma mark Type definition

  /**
   *  @enum   StreamType
   *  @brief  List of possible stream
   */
  enum StreamType {
    /** PointGrey Camera */
    kPointGrey = 0,
    /** Images */
    kImage = 1,
    /** Videos */
    kVideo = 2,
    /** External video */
    kExtVideo = 3
  };

  /**
   *  @struct BaseParameter
   *  @brief  Stream parameters
   */
  struct BaseParameter {
    /** Stream type */
    StreamType stream_type;
    /** Number of view */
    int n_view;
  };


  /**
   * @class Buffer
   * @brief Container for frame loader
   */
  class Buffer {
   public:
    /**
     * @name    Buffer
     * @fn  explicit Buffer(const size_t size)
     * @brief   Constructor
     * @param[in]   size    Number of element in the buffer
     */
    explicit Buffer(const size_t size) : host(size),
                                         device(size) {
    }

    /**
     * @name    Upload
     * @fn  void Upload(const size_t element)
     * @brief   upload frame to device memory
     * @param[in] element
     */
    void Upload(const size_t element) {
      if (element < host.size()) {
        device[element].upload(host[element], stream);
      }
    }

    /**
     * @name    Upload
     * @fn  void Upload(void)
     * @brief   upload all frame to device memory
     */
    void Upload(void) {
      for (size_t f = 0; f < host.size(); ++f) {
        device[f].upload(host[f], stream);
      }
    }

    /**
     * @name    WaitCompletion
     * @fn  void WaitCompletion(void)
     * @brief   Wait for upload completion
     */
    void WaitCompletion(void) {
      if (!stream.queryIfComplete()) {
        stream.waitForCompletion();
      }
    }

    /** Host data */
    std::vector<cv::Mat> host;
    /** Device data */
    std::vector<cv::cuda::GpuMat> device;
    /** Cuda stream */
    cv::cuda::Stream stream;
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   BaseStream
   *  @fn BaseStream(const BaseParameter& param)
   *  @brief  Constructor
   *  @param[in]  param Stream parameters
   */
  BaseStream(const BaseParameter& param) : n_view_(param.n_view),
                                           buffer_(n_view_),
                                           image_width_(0),
                                           image_height_(0) {
  }

  /**
   *  @name   ~BaseStream
   *  @fn virtual ~BaseStream(void)
   *  @brief  Destructor
   */
  virtual ~BaseStream(void) {}

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Read
   *  @fn virtual int Read(void) = 0
   *  @brief  Read image from the stream
   *  @return -1 if error, 0 otherwise
   */
  virtual int Read(void) = 0;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name    get_image_width
   *  @fn      virtual int get_image_width(void) const
   *  @brief   Provide image width
   *  @return  Image width
   */
  virtual int get_image_width(void) const {
    return image_width_;
  }

  /**
   *  @name    get_image_height
   *  @fn      virtual int get_image_height(void) const
   *  @brief   Provide image height
   *  @return  Image height
   */
  virtual int get_image_height(void) const {
    return image_height_;
  }

  /**
   *  @name   set_frame_to_process
   *  @fn virtual void set_frame_to_process(int frame_index)
   *  @brief  Set from which frame the pipeline process
   *  @param[in]  frame_index   Frame's index
   */
  virtual void set_frame_to_process(int frame_index) {}

  /**
   *  @name   get_buffer
   *  @fn     virtual const Buffer& get_buffer(void) const
   *  @brief  Give reference to image buffer
   *  @return Image buffer
   */
  virtual const Buffer& get_buffer(void) const {
    return buffer_;
  }

  /**
   *  @name   get_frame_index
   *  @fn     virtual int get_frame_index(void) const
   *  @brief  Provide the index of the current frame
   *  @return Frame index
   */
  virtual int get_frame_index(void) const {
    return -1;
  }

  /**
   *  @name   get_n_max_frame
   *  @fn     virtual int get_n_max_frame(void) const
   *  @brief  Provide the maximum index of the frame
   *  @return Maximum Frame index
   */
  virtual int get_n_max_frame(void) const {
    return -1;
  }

  /**
   *  @name get_frame_name
   *  @fn void get_frame_name(std::string* frame_name, std::string* frame_ext) const
   *  @brief  Provide the name of the current frame according to the media
   *  @param[out] frame_name  Frame name
   *  @param[out] frame_ext   Frame extension
   */
  virtual void get_frame_name(std::string* frame_name,
                              std::string* frame_ext) const {
    *frame_name = "";
    *frame_ext = "";
  }

#pragma mark -
#pragma mark Protected

 protected:
  /** Number of view */
  int n_view_;
  /** Image buffer */
  Buffer buffer_;
  /** Image width */
  int image_width_;
  /** Image height */
  int image_height_;

};

}  // namespace LTS5


#endif /* __LTS5_BASE_STREAM__ */
