/**
 *  @file   lscm_parameterizer.hpp
 *  @brief  The class `LSCMParameterizer` implements the "Least Squares
 *          Conformal Maps (LSCM)" parameterization by :
 *            Bruno Lévy, Sylvain Petitjean, Nicolas Ray, Jérome Maillot
 *          This is a conformal parameterization, i.e. it attempts to preserve
 *          angles.
 *          This is a free border parameterization. No need to map the border
 *          of the surface onto a convex polygon (only two pinned vertices are
 *          needed to ensure a unique solution), but one-to-one mapping
 *          is *not* guaranteed.
 *          Based on OpenNL/CGAL libraries implementation
 *  @ingroup face_reconstruction
 *  @author Christophe Ecabert
 *  @date   13/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_LSCM_PARAMETERIZER__
#define __LTS5_LSCM_PARAMETERIZER__

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/optimization/sparse_linear_solver.hpp"
#include "lts5/geometry/mesh.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class LSCMParameterizer
 *  @brief  The class `LSCMParameterizer` implements the "Least Squares
 *          Conformal Maps (LSCM)" parameterization by :
 *            Bruno Lévy, Sylvain Petitjean, Nicolas Ray, Jérome Maillot
 *          This is a conformal parameterization, i.e. it attempts to preserve
 *          angles.
 *          This is a free border parameterization. No need to map the border
 *          of the surface onto a convex polygon (only two pinned vertices are
 *          needed to ensure a unique solution), but one-to-one mapping
 *          is *not* guaranteed.
 *          Based on OpenNL/CGAL libraries implementation
 *  @ingroup face_reconstruction
 *  @author Christophe Ecabert
 *  @date   13/10/15
 */
template<class T>
class LTS5_EXPORTS LSCMParameterizer {

 public :
#pragma mark -
#pragma mark Initialization
  /**
   *  @name LSCMParameterizer
   *  @fn LSCMParameterizer(void)
   *  @brief  Constructor
   */
  LSCMParameterizer(void);

  /**
   *  @name LSCMParameterizer
   *  @fn explicit LSCMParameterizer(const LTS5::Mesh<T>& mesh)
   *  @brief  Constructor
   *  @param[in]  mesh  Mesh to parameterize
   */
  explicit LSCMParameterizer(const LTS5::Mesh<T>& mesh);

  /**
   *  @name Initialize
   *  @fn void Initialize(const LTS5::Mesh<T>& mesh)
   *  @brief  Initialize LSCMParameterizer with the given mesh
   *  @param[in]  mesh  Mesh to parameterize
   */
  void Initialize(const LTS5::Mesh<T>& mesh);

  /**
   *  @name ~LSCMParameterizer
   *  @fn ~LSCMParameterizer(void)
   *  @brief  Destructor
   */
  ~LSCMParameterizer(void);

#pragma mark -
#pragma mark Process

  /**
   *  @name Process
   *  @fn int Process(const LTS5::Mesh<T>* mesh)
   *  @brief  Compute a one-to-one mapping from a triangular 3D surface mesh
   *          to a piece of the 2D space.
   *          The mapping is linear by pieces (linear in each triangle). The
   *          result is the (u,v) pair image of each vertex of the 3D surface.
   *          Based on OpenNL/CGAL libraries
   *  @param[in,out] mesh Mesh to process
   *  @return 0 if success, -1 otherwise
   *
   *  @note   Outline of the algorithm:
   *          1) Find an initial solution by projecting on a plane.
   *          2) Lock two vertices of the mesh.
   *          3) Copy the initial u,v coordinates to OpenNL.
   *          4) Solve the equation with OpenNL.
   *          5) Copy OpenNL solution to the u,v coordinates.
   */
  int Process(const LTS5::Mesh<T>* mesh);

#pragma mark -
#pragma mark Accessor

  /**
   *  @name set_unwrap
   *  @fn virtual void set_unwrap(const bool unwrap)
   *  @brief  Indicate if unwrapping is needed
   *  @param[in]  unwrap   True when unwrapping
   */
  void set_unwrap(const bool unwrap) {
    solver_->set_unwrap(unwrap);
  }

#pragma mark -
#pragma mark Private

 private :

  /**
   *  @name ParameterizeMeshBorder
   *  @fn void ParameterizeMeshBorder(void)
   *  @brief  Map two extreme vertices of the 3D mesh and mark them as "locked"
   */
  void ParameterizeMeshBorder(void);

  /**
   *  @name InitializeSystemFromMeshBorder
   *  @fn void InitializeSystemFromMeshBorder(void)
   *  @brief  Initialize Ax = B system after two border vertices are locked
   */
  void InitializeSystemFromMeshBorder(void);

  /**
   *  @name
   *  @fn void ProjectTriangle(const LTS5::Vector3<T>& p0,
                               const LTS5::Vector3<T>& p1,
                               const LTS5::Vector3<T>& p2,
                               LTS5::Vector2<T>* z0,
                               LTS5::Vector2<T>* z1,
                               LTS5::Vector2<T>* z2)
   *  @brief  Computes the coordinates of the vertices of a triangle in a
   *          local 2D orthonormal basis of the triangle's plane.
   *  @param[in]  p0  First triangle's vertex (3D)
   *  @param[in]  p1  Second triangle's vertex (3D)
   *  @param[in]  p2  Third triangle's vertex (3D)
   *  @param[out] z0  Corresponding 2D location of p0
   *  @param[out] z1  Corresponding 2D location of p1
   *  @param[out] z2  Corresponding 2D location of p2
   */
  void ProjectTriangle(const LTS5::Vector3<T>& p0,
                       const LTS5::Vector3<T>& p1,
                       const LTS5::Vector3<T>& p2,
                       LTS5::Vector2<T>* z0,
                       LTS5::Vector2<T>* z1,
                       LTS5::Vector2<T>* z2);

  /**
   *  @name SetupTriangleRelation
   *  @fn void SetupTriangleRelation(void)
   *  @brief  Create two lines in the linear sstem per triangle (one fur u,
   *          one for v).
   */
  void SetupTriangleRelation(void);

  /**
   *  @name SetMeshUVFromSystem
   *  @fn void SetMeshUVFromSystem(void)
   *  @brief  Copy X coordinates int the (u,v) pair of each vertex. i.e. Fill
   *          vtkPolyData structure with proper (u,v) pairs.
   */
  void SetMeshUVFromSystem(void);


  /** Flag to indicate if everything is setup */
  bool is_initialized_;
  /** Mesh */
  LTS5::Mesh<T>* mesh_;
  /** Solver */
  SparseLinearSolver<T>* solver_;
  /** Locked vertex indexes */
  int locked_vertex_index_[2];
  /** Number of vertices */
  int n_vertex_;
  /** Direct pointer to vertex array */
  LTS5::Vector3<T>* vertex_ptr_;
  /** texture coordinate */
  LTS5::Vector2<T>* tex_ptr_;

};
}  // namespace LTS5


#endif /* __LTS5_LSCM_PARAMETERIZER__ */
