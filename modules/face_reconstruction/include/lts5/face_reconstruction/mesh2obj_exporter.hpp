/**
 *  @file   mesh2obj_exporter.hpp
 *  @brief  Export a mesh into an .obj file
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 30/07/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_MESH2OBJ_EXPORTER__
#define __LTS5_MESH2OBJ_EXPORTER__

#include <iostream>

#include "lts5/utils/library_export.hpp"
#include "lts5/face_reconstruction/base_mesh_exporter.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class      Mesh2OBJExporter
 *  @brief      Class use to export mesh to .obj file
 *  @author     Christophe Ecabert
 *  @date       30/07/14
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS Mesh2OBJExporter : public BaseMeshExporter {
 public :

#pragma mark -
#pragma mark Initialization
  /**
   *  @name   Mesh2OBJExporter
   *  @fn Mesh2OBJExporter(void)
   *  @brief  Constructor
   */
  Mesh2OBJExporter(void);

  /**
   *  @name   Mesh2OBJExporter
   *  @fn Mesh2OBJExporter(const Mesh2OBJExporter& other) = delete
   *  @brief  Copy Constructor
   */
  Mesh2OBJExporter(const Mesh2OBJExporter& other) = delete;

  /**
   * @name  operator=
   * @fn    Mesh2OBJExporter& operator=(const Mesh2OBJExporter& rhs) = delete
   * @brief Assignment operator
   * @param rhs Object to assign from
   * @return    Newly assigned object
   */
  Mesh2OBJExporter& operator=(const Mesh2OBJExporter& rhs) = delete;

  /**
   *  @name   ~Mesh2OBJExporter
   *  @fn ~Mesh2OBJExporter(void)
   *  @brief  Destructor
   */
  ~Mesh2OBJExporter(void);

#pragma mark -
#pragma mark Process

  /**
   *  @name   Process
   *  @fn void Process(void)
   *  @brief  Export mesh into obj file
   */
  void Process(void);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  set_output_name
   * @fn    void set_output_name(const std::string& output)
   * @brief Set the name to use as output
   * @param output    Output name
   */
  void set_output_name(const std::string& output) {
    output_name_ = output;
  }

  /**
   *  @name   set_mesh
   *  @fn void set_mesh(const LTS5::Mesh<float>& mesh)
   *  @brief  Link the exporter to a given mesh.
   *  @param[in]  mesh   Face mesh to export to .obj file
   */
  void set_mesh(const LTS5::Mesh<float>& mesh);

#pragma mark -
#pragma mark Private

 private :
  /** Mesh to export */
  LTS5::Mesh<float>* mesh_;
  /** output name */
  std::string output_name_;
};
}
#endif /* __LTS5_MESH2OBJ_EXPORTER__ */
