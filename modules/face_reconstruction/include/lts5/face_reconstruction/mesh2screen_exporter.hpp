/**
 *  @file   mesh2screen_exporter.hpp
 *  @brief  Export reconstruction to OpenGL windows
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   04/04/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __MESH2SCREEN_EXPORTER__
#define __MESH2SCREEN_EXPORTER__

#include "lts5/utils/library_export.hpp"
#include "lts5/face_reconstruction/base_mesh_exporter.hpp"
#include "lts5/ogl/renderer.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/ogl/context.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   Mesh2ScreenExporter
 * @brief   Export reconstruction to OpenGL windows
 * @author  Christophe Ecabert
 * @date    04/04/2017
 * @ingroup face_reconstruction
 */
class LTS5_EXPORTS Mesh2ScreenExporter : public BaseMeshExporter {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   *  @name Mesh2ScreenExporter
   *  @fn Mesh2ScreenExporter(void)
   *  @brief  Constructor
   */
  Mesh2ScreenExporter(void);

  /**
   *  @name ~Mesh2ScreenExporter
   *  @fn ~Mesh2ScreenExporter(void) override
   *  @brief  Destructor
   */
  ~Mesh2ScreenExporter(void) override;

  /**
   *  @name Mesh2ScreenExporter
   *  @fn Mesh2ScreenExporter(const Mesh2ScreenExporter& other)
   *  @brief  Copy Constructor
   *  @param[in]    other   Object to copy from
   */
  Mesh2ScreenExporter(const Mesh2ScreenExporter& other) = delete;

  /**
   *  @name operator=
   *  @fn Mesh2ScreenExporter& operator=(const Mesh2ScreenExporter& rhs)
   *  @brief  Assignment Constructor
   *  @param[in]    rhs   Object to assign from
   *  @return newly assigned object
   */
  Mesh2ScreenExporter& operator=(const Mesh2ScreenExporter& rhs) = delete;

  /**
   *  @name Initialize
   *  @fn void Initialize(const LTS5::Mesh<float>& mesh,
                          const cv::Mat& texture,
                          const int width,
                          const int height) override
   *  @brief  Initialize converter
   *  @param[in]  mesh  Mesh to convert to image
   *  @param[in]  texture Mesh's texture, can be null if not used
   *  @param[in]  width   Image width
   *  @param[in]  height  Image height
   */
  void Initialize(const LTS5::Mesh<float>& mesh,
                  const cv::Mat& texture,
                  const int width,
                  const int height) override;

#pragma mark -
#pragma mark Usage

  /**
   * @name  ActiveContext
   * @fn    void ActiveContext(void) const override
   * @brief Make this context the current one
   */
  void ActiveContext(void) const override ;

  /**
   * @name  DisableContext
   * @fn    void DisableContext(void) const override
   * @brief Detach from current context
   */
  void DisableContext(void) const override ;

  /**
   *  @name Process
   *  @fn void Process(void)
   *  @brief  Export mesh according to exporter class
   */
  void Process(void) override;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  set_mesh
   * @fn    void set_mesh(const LTS5::Mesh<float>& mesh) override
   * @brief Set what mesh need to be exported
   * @param mesh    Mesh to export
   */
  void set_mesh(const LTS5::Mesh<float>& mesh) override;

  /**
   * @name  is_initialiazed
   * @fn    bool is_initialiazed(void) const override
   * @brief Indicate if exported is initialized
   * @return    true if init, false otherwise
   */
  bool is_initialiazed(void) const  override {
    return is_initialized_;
  }

  /**
   *  @name set_texture
   *  @fn void set_texture(const vtkTexture* texture) override
   *  @brief  Set texture of the object
   *  @param[in]  texture Texture object
   */
  void set_texture(const cv::Mat& texture) override;

  /**
   * @name  get_texture
   * @fn    const OGLTexture* get_texture(void) const
   * @brief Provide access to opengl texture object
   * @return    OGLTexture object
   */
  const OGLTexture* get_texture(void) const  override {
    return texture_;
  }

#pragma mark -
#pragma mark Private
 private:

  /** OpenGL context */
  OGLContext* ctx_;
  /** Mesh */
  Mesh<float>* mesh_;
  /** Texture object */
  OGLTexture* texture_;
  /** Rendering program */
  OGLProgram* program_;
  /** Camera */
  OGLCamera<float>* cam_;
  /** Renderer */
  OGLRenderer<float>* renderer_;
  /** Indicate if module is initialized or not */
  bool is_initialized_;

};

}  // namepsace LTS5
#endif //__MESH2SCREEN_EXPORTER__
