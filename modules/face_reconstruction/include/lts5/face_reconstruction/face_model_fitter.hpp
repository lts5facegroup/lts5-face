/**
 *  @file   face_model_fitter.hpp
 *  @brief  Fit a 3D face model to a given input
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   10/03/14
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_FACE_MODEL_FITTER__
#define __LTS5_FACE_MODEL_FITTER__

#include <iostream>
#include <vector>
#include <array>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/model/bilinear_face_model.hpp"
#include "lts5/face_reconstruction/virtual_camera.hpp"
#include "lts5/face_reconstruction/contour_extractor.hpp"
#include "lts5/face_reconstruction/pipeline_data_type_definition.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/octree.hpp"

#ifdef HAS_CUDA_
#include "lts5/cuda/face_reconstruction/bilinear_face_model.hpp"
#endif

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  FaceModelFitter
 *  @brief  Object used to fit a Bilinear model based on data provided by user
 *          The usual data are a set of facial landmarks
 *          However this class can be used to generate some synthetic data
 *          [i.e Load blendshape from FaceWarehouse data base and apply
 *          transformation on it (Rotation, translation, scaling)]
 *  @author Christophe Ecabert
 *  @date   10/03/14
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS FaceModelFitter {
 public :

  /**
   *  @enum   ProjectionMode
   *  @brief  List of possible projection model used for reconstruction
   */
  enum ProjectionMode {
    kFullPerspective = 0,
    kWeakPerspective = 1
  };

  /**
   *  @struct FitterParameter
   *  @brief  Structure holding parameters for model fitter class
   */
  struct FitterParameter {
    /** Name's file holding bilinear face model */
    std::string model_filename;
    /** File holding the vertex index corresponding to landmarks features */
    std::string feature_index_filename;
    /** Triangulation file for close meanshape */
    std::string close_triangulation_filename;
    /** How many view are used to fit model */
    int number_of_view;
    /** Type of projection used */
    ProjectionMode  projection_model;
    /** Camera structure setup */
    std::vector<LTS5::VirtualCamera*>* camera_setup;
    /** Configuration for contour extraction */
    std::string extractor_configuration = "";
  };

  /**
   *  @struct FitParameter
   *  @brief  Define parameters used to fit bilinear face model
   */
  struct FitParameter {
    /** Maximum of iteration used to fit face model */
    int max_iteration;
    /** Regularization parameters for identity*/
    float eta_identity;
    /** Aggregation parameters for identity*/
    float gamma_identity;
    /** Regularization parameters for identity*/
    float eta_expression;
    /** Time Regularization parameters for identity */
    float eta_time_expression;
    /** Initialize fit with weights from previous frame */
    bool tracking_enable = false;
    /** Indicate if contour is is used during optimization */
    bool use_contour = false;
    /** Contour extracted from input images */
    std::vector<cv::Mat> image_contour;
    /** Location of the face ROI on the image */
    std::vector<cv::Point2f> face_region_origin;
    /** Vertex face index */
    std::vector<int>* face_vertex_index = NULL;
  };

  /**
   *  @struct FitResult
   *  @brief  Structures holding fitting results
   */
  struct FitResult {
    /** How fit took */
    float fit_time;
    /** Point to point error */
    float pts_to_pts_error;
    /** Gamma angle recovered */
    float gamma;
    /** Theta angle recovered */
    float theta;
    /** Phi angle recovered */
    float phi;
    /** Tx recovered */
    float delta_x;
    /** Ty recovered */
    float delta_y;
    /** Tz recovered */
    float delta_z;
    /** Scale recovered */
    float scale;
  };

  /**
   *  @struct TransformationData
   *  @brief  Structure for passing user-supplied data to the
   *          objective function and its Jacobian
   */
  struct TransformationData {
    /** Number of landmarks */
    int number_landmark;
    /** Number of views */
    int number_of_view;
    /** Shape valid */
    const std::vector<bool>* shape_valid;
    /** Camera setup */
    const std::vector<LTS5::VirtualCamera*>* camera_setup;
    /** Landmarks for each view [v1_{l1,l2,..}, v2{l1,l2,..}, v3{l1,l2,..}] */
    float* landmark;
    /** 3D object points */
    const float*  point_3d;
    /** Pose estimation */
    float estimated_parameter[7];
  };

 private :

  /**
   *  @enum   DataType
   *  @brief  List of possible data selection
   */
  enum DataType {
    /** Vertices - Identity direction */
    kVertId,
    /** Vertices - Expression direction */
    kVertExpr,
    /** Identity direction */
    kIdentity,
    /** Expression direction */
    kExpression,
    /** Transformation */
    kTransformation,
    /** Levenberg-Marquardt */
    kLevMar
  };

  /**
   *  @enum   VertexSelection
   *  @brief  List of possible vertex selection for different function
   */
  enum VertexSelection {
    /** Select only vertices corresponding to landmarks */
    kLandmarks,
    /** Select only vertices corresponding to contour */
    kContour,
    /** Select all vertices */
    kFull
  };

  /**
   * @struct  BlendshapeParameter
   * @brief Structure holding blendshape model parameters for a given view
   */
  struct BlendshapeParameter {
    /** Projection of expression variation - Bi = (Pi * B) */
    cv::Mat Bi;
    /** Residual, landmarks - projection of neutral shape -
     ri = r - ti - Pi Bo */
    cv::Mat ri;
    /** Precomputation of BiT * Bi */
    cv::Mat BiTBi;
    /** Precomputation of BiT * ri */
    cv::Mat BiTri;
    /** Scale */
    float scale;
    /** Regularization */
    float eta;
    /** Regularization - time */
    float eta_time;
  };

  /** Right eye,Left eye, Nose tip, Right mouth, left mouth */
  const int control_landmark_[7] = {36,39,42,45,30,48,54};
  /** Left-Mid / Mid-Right / Left-Right */
  const int camera_pair_index_[6] = {0,1,0,2,1,2};
  /** Distance Obj-Cam */
  const float camera_distance_ = 5.f;
  /** Face model */
  LTS5::BilinearFaceModel* face_model_;
#ifdef HAS_CUDA_
  LTS5::CUDA::BilinearFaceModel* d_face_model_;
#endif
  /** Number of vertices used */
  int number_of_vertex_;
  /** Number of identites in the model */
  int number_identity_;
  /** Number of exoression in the model */
  int number_expression_;
  /** Number of 2D points detected */
  int number_2D_features_;
  /** Number of views used for fitting */
  int number_of_view_;
  /** Corresponding features - vertices indexes */
  std::vector<int> feature_vertex_index_;
  /** Vector holding identity weights */
  cv::Mat identity_weight_;
  /** Identity weight at t-1 */
  cv::Mat identity_weight_tm1_;
  /** Identity weight at t-2 */
  cv::Mat identity_weight_tm2_;
  /** Aggregation identity : matrix M at time t */
  cv::Mat Mt_;
  /** Aggregation identity : matrix M at time t - 1 */
  cv::Mat Mtm1_;
  /** Aggregation identity : vector y at time t */
  cv::Mat Yt_;
  /** Aggregation identity : vector y at time t - 1 */
  cv::Mat Ytm1_;
  /** Aggregation identity : factor at time t */
  float st_;
  /** Aggregation identity : factor at time t - 1*/
  float stm1_;
  /** Vector holding expression weights */
  cv::Mat expression_weight_;
  /** Expression weight at t-1 */
  cv::Mat expression_weight_tm1_;
  /** Expression weight at t-2 */
  cv::Mat expression_weight_tm2_;
  /** Vector holding rigid transformation parameters */
  std::vector<cv::Mat> rigid_transform_weight_;
  /** Average weights for identity */
  cv::Mat average_identity_weight_;
  /** Neutral expression coef */
  cv::Mat neutral_expression_weight_;
  /** MeanShape */
  cv::Mat mean_shape_;
  /** Vertex from Mean shape corresponding to landmarks */
  cv::Mat mean_shape_landmark_;
  /** Norm of the meanshape - scale estimation*/
  float norm_mean_shape_;
  /** Mesh corresponding to mean shape */
  Mesh<float>* mean_shape_mesh_;
  /** Collision Detection Tree */
  OCTree<float>* collision_tree_;
  /** Vector holding the face computed from weights vector and face model */
  cv::Mat face_vertex_;
  /** Vector holding reconstruct vertices corresponding to facial landmarks */
  cv::Mat landmark_vertex_;
  /** Matrix holding temporary result after multiplying face model by
   identity weight vector */
  cv::Mat data_vertex_expr_;
  /** Information about camera setup */
  std::vector<LTS5::VirtualCamera*> camera_configuration_;
  /** Indicate what type of projection model need to be used. */
  ProjectionMode projection_mode_;
  /** Landmark used to fit bilinear model */
  std::vector<cv::Mat> local_landmark_;
  /** Full landmarks set, reference to input */
  const std::vector<cv::Mat>* real_landmark_;
  /** Validity flag */
  std::vector<bool> valid_shape_;
  /** Validity flag for previous iteration */
  std::vector<bool> previous_valid_shape_;
  /** Matrix apllying transformation [scaling - rotation] and select a
   subset of the whole 3D vertices */
  std::vector<cv::Mat> subset_transformation_;
  /** Detected landmarks (2D) shifted by estimated translation */
  std::vector<cv::Mat> shifted_landmark_;
  /** Shift to move principle point to top left corner of the image */
  std::vector<cv::Mat> image_center_shift_;
  /** Selected vertices used for shape reconstruction - identity
   reconstruction */
  cv::Mat subset_vertex_identity_;
  /** Selected vertices used for shape reconstruction - expression
   reconstruction */
  cv::Mat subset_vertex_expression_;
  /** Matrix holding the basis to estimate the rigid transformation */
  cv::Mat rigid_basis_;
  /** Model rigid transformation structures */
  LTS5::PPLData::ModelPoseParametersStruct rigid_trsfrm_;
  /** Flag to indicate the use of contour */
  bool use_contour_ = false;
  /** Module to extract chin contour from 3D mesh */
  LTS5::ContourExtractor* contour_extractor_ = nullptr;
  /** List of vertices considerated as potential contour element */
  std::vector<int> contour_candidate_;
  /** 3d surface generated by model */
  cv::Mat contour_candidate_surface_;
  /** List of 2D points corresponding to contour */
  std::vector<std::vector<float>> contour_image_position_;
  /** Indicates if fitter modules is correctly init */
  bool initialized_;
  /** Tracking state */
  bool is_tracking_;


  /** LBFGS solver buffer */
  float* w_exp_solver_;
  /** Blendshape parameters to use the evaluation of LBFGS */
  std::vector<BlendshapeParameter> bshape_vars_;

  cv::Mat wexp_tm1_;
  cv::Mat wexp_tm2_;

 public:

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   FaceModelFitter
   *  @fn FaceModelFitter()
   *  @brief  Constructor
   */
  FaceModelFitter();

  /**
   *  @name   FaceModelFitter
   *  @fn FaceModelFitter(const FitterParameter& configuration)
   *  @brief  Constructor
   *  @param[in]  configuration  Structure holding fitter's parameters
   */
  explicit FaceModelFitter(const FitterParameter& configuration);

  /**
   *  @name   ~FaceModelFitter
   *  @fn ~FaceModelFitter()
   *  @brief  Destructor
   */
  ~FaceModelFitter();

  /**
   *  @name   IsModuleWorking
   *  @fn bool IsModuleWorking() const
   *  @brief  Return the initialization flag of the fitter. This indicates
   *          if fitter will be working on not.
   *  @return True if module is initialized correctly.
   */
  bool IsModuleWorking() const;

  /**
   *  @name   initVariablesForSynthData
   *  @fn void InitMultiViewVariables(const int number_view)
   *  @brief  Initialise/Reinitialise variable depending on the number
   *          of views used for fitting
   *  @param[in]  number_view    Number of views
   */
  void InitMultiViewVariables(const int number_view);

#pragma mark -
#pragma mark Fit
  /**
   *  @name   FitModel
   *  @fn int FitModel(const std::vector<cv::Mat>& landmark,
                       const FitParameter& fit_config,
                       FitResult* fit_result,
                       const bool save_data = false)
   *  @brief  Methods call to fit bilinear face model on a new entry
   *          (new set of landmarks) [Single view]
   *  @param[in]  landmark      Set of points used to compute model
   *                            coefficients
   *  @param[in]  fit_config    Parameters set used by fitter
   *  @param[out] fit_result    Fit results
   *  @return -1 if error, 0 otherwise
   */
  int FitModel(const std::vector<cv::Mat>& landmark,
               const FitParameter& fit_config,
               FitResult* fit_result);

  /**
   *  @name   ComputeMeshVertices
   *  @fn void ComputeMeshVertices(const VertexSelection& selection,
                                   const bool with_rigid_transformation = false)
   *  @brief  Compute mesh vertices from model parameters
   *  @param[in]  selection                 Indicate what type of vertices
   *                                        has to be reconstruct
   *  @param[in]  with_rigid_transformation Indicates if reconstruction need
   *                                        to take into account the rigid
   *                                        transfromation or only the
   *                                        non-rigid part (i.e generated
   *                                        using only face model)
   */
  void ComputeMeshVertices(const VertexSelection& selection,
                           const bool with_rigid_transformation = false);

  /**
   *  @name ResetTrackingState
   *  @fn void ResetTrackingState(void)
   *  @brief  Reinitialize tracking state
   */
  void ResetTrackingState(void);

#pragma mark -
#pragma mark Accessors
  /**
   *  @name   set_camera_configuration
   *  @fn void set_camera_configuration(const std::vector<LTS5::VirtualCamera*>& camera_configuration)
   *  @brief  Initialize internal camera structure
   *  @param[in]  camera_configuration  New camera structure
   */
  void set_camera_configuration(const std::vector<LTS5::VirtualCamera*>& camera_configuration) {
    camera_configuration_ = camera_configuration;
  }

  /**
   *  @name   set_projection_mode
   *  @fn void set_projection_mode(const ProjectionMode& mode)
   *  @brief  Select the model of projection used to fit model on landmarks
   *  @param[in]  mode  Which model of projection is used
   */
  void set_projection_mode(const ProjectionMode& mode) {
    projection_mode_ = mode;
  }

  /**
   * @name  set_external_mesh_container
   * @fn    void set_external_mesh_container(LTS5::Mesh<float>* mesh)
   * @brief  Setup the container to directly output synthetic reconstruction in
   *        a given \p mesh.
   * @param[in,out] mesh    Where to put reconstructed shape
   */
  void set_external_mesh_container(LTS5::Mesh<float>* mesh);

  /**
   *  @name   get_projection_matrix
   *  @fn const cv::Mat get_projection_matrix(const int view) const
   *  @brief  Return the projection matrix for a given view
   *  @param[in]  view  Selected view
   *  @return Projection matrix P
   */
  const cv::Mat get_projection_matrix(const int view) const;

  /**
   * @name  get_pose
   * @fn  const PPLData::ModelPoseParametersStruct& get_pose(void) const
   * @brief Provide rigid transformation data
   * @return  Rigid transform parameters
   */
  const PPLData::ModelPoseParametersStruct& get_pose(void) const {
    return rigid_trsfrm_;
  }

  /**
   *  @name   get_raw_3d_surface
   *  @fn const cv::Mat get_raw_3d_surface(void) const
   *  @brief  Return the matrix holding the surface's 3D pts
   *  @return 3D pts
   */
  const cv::Mat get_raw_3d_surface(void) const {
    return face_vertex_;
  }

  /**
   *  @name   get_model_size
   *  @fn const cv::Scalar_<int> get_model_size(void) const
   *  @brief  Return model parameters dimension : Id/Expr
   */
  const cv::Scalar_<int> get_model_size(void) const {
    return cv::Scalar_<int>(number_identity_,
                            number_expression_,
                            number_of_vertex_,
                            0);
  }

  /**
   *  @name   get_face_model
   *  @fn const LTS5::BilinearFaceModel* get_face_model(void) const
   *  @brief  Give reference to bilinear face model
   *  @return Pointer to face model
   */
  const LTS5::BilinearFaceModel* get_face_model(void) const {
    return face_model_;
  }

  /**
   * @name  get_vertex_index
   * @fn    const std::vector<int>& get_vertex_index(void) const
   * @brief Provide a list of vertex that correspond to face landmarks
   * @return    List of vertex indices corresponding to face tracker landmarks
   */
  const std::vector<int>& get_vertex_index(void) const {
    return feature_vertex_index_;
  }

#pragma mark -
#pragma mark Private function
  private :

  /**
   *  @name   LoadLandmarkVerticesIndex
   *  @fn int LoadLandmarkVerticesIndex(const std::string& file)
   *  @brief  Load the vertex indexes corresponding to the selected landmarks
   *          from config files
   *  @param  file     Configuration file
   *  @return 0 if everything went fine otherwise -1.
   */
  int LoadLandmarkVerticesIndex(const std::string& file);

  /**
   *  @name   LoadInitParameters
   *  @fn int LoadInitParameters(const std::string& init_param_file,
                                  const std::string& tri_file)
   *  @brief  Load the initial parameters used for fit model (Mean shape -
   *          neutral expression weights)
   *  @param[in]  init_param_file   Init parameters file
   *  @param[in]  tri_file          Triangulation filename
   *  @return     0 if everything went fine otherwise -1.
   */
  int LoadInitParameters(const std::string& init_param_file,
                         const std::string& tri_file);

  /**
   *  @name   InitFirstIteration
   *  @fn void InitFirstIteration(const std::vector<cv::Mat>& landmark,
                                  const FitParameter& fitter_configuration)
   *  @brief  Initialize fitter for reconstruction
   *  @param[in]  landmark    Set of landmarks to use
   *  @param[in]  fitter_configuration       Fitting parameters
   */
  void InitFirstIteration(const std::vector<cv::Mat>& landmark,
                          const FitParameter& fitter_configuration);

  /**
   *  @name   ApplyTransformation
   *  @fn void ApplyTransformation(const int iteration,
                                   const bool skip_contour = false)
   *  @brief  Apply translation on landmarks
   *          Fill projection matrix with information from rotation matrix.
   *  @param[in]  iteration       Current iteration number
   *  @param[in]  skip_contour    Include of not contour
   */
  void ApplyTransformation(const int iteration,
                           const bool skip_contour = false);

  /**
   *  @name   UpdateTransformation
   *  @fn UpdateTransformation(void)
   *  @brief  Update transformation parameters (i.e Rotation,
   *          translation) with the estimation (rigid_transform_weight_)
   */
  void UpdateTransformation(void);

  /**
   *  @name   UpdateRigidBasis
   *  @fn void UpdateRigidBasis(const int current_view, const int iteration)
   *  @brief  Update the bassis used to recover rigid transformation  \n
   *          It will select the proper set of points used to recover the
   *          pose (i.e. Mean shape/Current shape) and \n
   *          apply the rotation according the view
   *  @param[in] current_view    Current view point index
   *  @param[in] iteration       Current iteration
   */
  void UpdateRigidBasis(const int current_view, const int iteration);

  /**
   *  @name   ComputeWeight
   *  @fn void ComputeWeight(const DataType& direction,
                             const float eta,
                             const float eta_time,
                             const int iteration)
   *  @brief  Compute weights for given direction specified as parameters
   *  @param[in] direction      Direction of interest DataType
   *  @param[in] eta            Regularization coef
   *  @param[in] eta_time       Regularization coef for time smoothing
   *  @param[in] iteration      Current iteration
   */
  void ComputeWeight(const DataType& direction,
                     const float eta,
                     const float eta_time,
                     const int iteration);

  /**
   *  @name SolveExpression
   *  @fn void SolveExpression(const int iteration, const float eta,
                               const float eta_time)
   *  @brief  Compute expression weight
   *  @param[in]  iteration Current iteration number
   *  @param[in]  eta       Expression regularization factor
   *  @param[in]  eta_time  Expression regularization factor over time
   */
  void SolveExpression(const int iteration,
                       const float eta,
                       const float eta_time);

  /**
   *  @name SolveBlendshapeExpression
   *  @fn void SolveBlendshapeExpression(const int iteration, const float eta,
                                         const float eta_time)
   *  @brief  Compute expression weight
   *  @param[in]  iteration Current iteration number
   *  @param[in]  eta       Expression regularization factor
   *  @param[in]  eta_time  Expression regularization factor over time
   */
  void SolveBlendshapeExpression(const int iteration,
                                 const float eta,
                                 const float eta_time);

  /**
   *  @name LBFGSEvaluateCallback
   *  @fn   static float LBFGSEvaluateCallback(void* instance,
                                     const float* x,
                                     float* gx,
                                     const int n,
                                     const float step)
   *  @brief  Callback invoked by LBFGS solver to estimate cost function + grad
   *  @param[in] instance   Object linked to the solver
   *  @param[in] x          Current estimation of the parameters
   *  @param[in] gx         Current estimation of gradient
   *  @param[in] n          Number of variables in \p x.
   *  @param[in] step       Step size
   */

  static float LBFGSEvaluateCallback(void* instance,
                                     const float* x,
                                     float* gx,
                                     const int n,
                                     const float step);

  /**
   *  @name SolveIdentity
   *  @fn void SolveIdentity(const int iteration, const float eta,
                              const float gamma)
   *  @brief  Compute identity weight
   *  @param[in]  iteration Current iteration number
   *  @param[in]  eta       Identity regularization factor
   *  @param[in]  gamma     Identity aggregation factor over time
   */
  void SolveIdentity(const int iteration,
                     const float eta,
                     const float gamma);

  /**
   *  @name   SolveLinearLeastSquare
   *  @fn void SolveLinearLeastSquare(const cv::Mat& q_matrix,
                                      const cv::Mat& y_vector,
                                      const float eta,
                                      cv::Mat* weight)
   *  @brief  Solve problem of form min(w) |Qw - y|^2 + rho*|w|^2
   *  @param[in]  q_matrix  System matrix
   *  @param[in]  y_vector  Output vector
   *  @param[in]  eta       Regularizazion coef
   *  @param[out] weight    Result
   */
  void SolveLinearLeastSquare(const cv::Mat& q_matrix,
                              const cv::Mat& y_vector,
                              const float eta,
                              cv::Mat* weight);

  /**
   *  @name   SolveLinearLeastSquare
   *  @fn void SolveLinearLeastSquareOpt(const cv::Mat& q_matrix,
                                         const cv::Mat& y_vector,
                                         const float eta,
                                         cv::Mat* weight)
   *  @brief  Solve problem of form min(w) |Qw - y|^2 + rho*|w|^2
   *  @param[in]  q_matrix  System matrix
   *  @param[in]  y_vector  Output vector
   *  @param[in]  eta       Regularizazion coef
   *  @param[out] weight    Result
   */
  void SolveLinearLeastSquareOpt(const cv::Mat& q_matrix,
                                 const cv::Mat& y_vector,
                                 const float eta,
                                 cv::Mat* weight);

  /**
   *  @name   SolveLinearLeastSquare
   *  @fn void SolveLinearLeastSquare(const cv::Mat& a_matrix,
                                      const cv::Mat& b_matrix,
                                      const int sum_length,
                                      const float eta,
                                      cv::Mat* weight)
   *  @brief  Solve problem of form min(w) Sum {|Qw - y|^2 } + rho*|w|^2
   *  @param[in]  a_matrix      Sum{ Qt*Q }
   *  @param[in]  b_matrix      Sum{ Qt*y }
   *  @param[in]  sum_length    How many elements are in the sum
   *  @param[in]  eta           Regularizazion coef
   *  @param[out] weight        Result - w
   */
  void SolveLinearLeastSquare(const cv::Mat& a_matrix,
                              const cv::Mat& b_matrix,
                              const int sum_length,
                              const float eta,
                              cv::Mat* weight);

  /**
   *  @name   SolveLinearLeastSquareOpt
   *  @fn void SolveLinearLeastSquareOpt(const cv::Mat& a_matrix,
                                          const cv::Mat& b_matrix,
                                          const float cumulate_scale,
                                          const float eta,
                                          cv::Mat* weight)
   *  @brief  Solve problem of form min(w) Sum {| Qw - y |^2 } + rho*|w|^2
   *  @param[in]  a_matrix        Sum{ Qt*Q }
   *  @param[in]  b_matrix        Sum{ Qt*y }
   *  @param[in]  cumulate_scale  Cumulated scale for each views
   *  @param[in]  eta             Regularizazion coef
   *  @param[out] weight          Result - w
   */
  void SolveLinearLeastSquareOpt(const cv::Mat& a_matrix,
                                 const cv::Mat& b_matrix,
                                 const float cumulate_scale,
                                 const float eta,
                                 cv::Mat* weight);

  /**
   *  @name   SolveLinearLeastSquare_v2
   *  @fn void SolveLinearLeastSquareOpt_v2(const cv::Mat& a_matrix,
                                           const cv::Mat& b_matrix,
                                           const float cumulate_scale,
                                           const float eta,
                                           cv::Mat* weight)
   *  @brief  Solve problem of form min(w) Sum {| Qw - y |^2 } + rho*|w|^2
   *  @param[in]  a_matrix        Sum{ Qt*Q }
   *  @param[in]  b_matrix        Sum{ Qt*y }
   *  @param[in]  cumulate_scale  Cumulated scale for each views
   *  @param[in]  eta             Regularizazion coef
   *  @param[out] weight          Result - w
   */
  void SolveLinearLeastSquareOpt_v2(const cv::Mat& a_matrix,
                                    const cv::Mat& b_matrix,
                                    const float cumulate_scale,
                                    const float eta,
                                    cv::Mat* weight);

  /**
   *  @name   SolveLinearLeastSquare_v3
   *  @fn void SolveLinearLeastSquareOpt_v3(const cv::Mat& a_matrix,
                                           const cv::Mat& b_matrix,
                                           const float cumulate_scale,
                                           const float eta,
                                           cv::Mat* weight)
   *  @brief  Solve problem of form min(w) Sum {| Qw - y |^2 } + rho*|w|^2
   *  @param[in]  a_matrix        Sum{ Qt*Q }
   *  @param[in]  b_matrix        Sum{ Qt*y }
   *  @param[in]  cumulate_scale  Cumulated scale for each views
   *  @param[in]  eta             Regularizazion coef
   *  @param[out] weight          Result - w
   */
  void SolveLinearLeastSquareOpt_v3(const cv::Mat& a_matrix,
                                    const cv::Mat& b_matrix,
                                    const float cumulate_scale,
                                    const float eta,
                                    cv::Mat* weight);

  /**
   *  @name   SolveLinearLeastSquare
   *  @fn void SolveLinearLeastSquare(const cv::Mat& q_matrix,
                                      const cv::Mat& y_vector,
                                      cv::Mat* weight)
   *  @brief  Solve problem of form min(w) |Qw - y|^2 using pseudo inverse
   *  @param[in]  q_matrix    System matrix
   *  @param[in]  y_vector    Output vector
   *  @param[out] weight      Result
   */
  void SolveLinearLeastSquare(const cv::Mat& q_matrix,
                              const cv::Mat& y_vector,
                              cv::Mat* weight);

  /**
   *  @name   estimatetRigidTransformWeakPerspective
   *  @fn void EstimatetRigidTransform(const std::vector<cv::Mat>& landmark,
                                      const FitParameter& config,
                                      const int iteration)
   *  @brief  Estimate rigid transformation using Weak perspective camera model
   *  @param[in]  landmark              Facial landmarks for each views
   *  @param[in]  config                Parameters for the current fit
   *  @param[in]  iteration             Iteration number
   */
  void EstimatetRigidTransform(const std::vector<cv::Mat>& landmark,
                               const FitParameter& config,
                               const int iteration);

  /**
   *  @name   ProcessContour
   *  @fn void ProcessContour(const FitParameter& config)
   *  @brief  Extract 3D contour and match it with 2D image
   *  @param[in] config    Fit parameters
   */
  void ProcessContour(const FitParameter& config);

  /**
   *  @name   PickLandmark
   *  @fn void PickLandmark(const int iteration,
                            const std::vector<cv::Mat>& landmark)
   *  @brief  Compute the landmark selection based on ray tracing methods.
   *          If one ray from one landmark to the camera origin is
   *          intercepted by the shape (face) it is discarded.
   *  @param[in] iteration  Current iteration
   *  @param[in] landmark   Current set of landmark
   */
  void PickLandmark(const int iteration,
                    const std::vector<cv::Mat>& landmark);

  /**
   *  @name   EstimateFacePosition
   *  @fn void EstimateFacePosition(TransformationData* transformation)
   *  @brief  Estimate the position of the head in the space by reprojecting
   *          landmarks
   *  @param[in] transformation  3D shape data structure
   */
  void EstimateFacePosition(TransformationData* transformation);

  /**
   *  @name   EstimateFaceSize
   *  @fn void EstimateFaceSize(TransformationData* transformation)
   *  @brief  Estimate the size of the head for single view
   *  @param[in] transformation     3D shape data structure
   */
  void EstimateFaceSize(TransformationData* transformation);

  /**
   *  @name   IsPointVisibleFromCamera
   *  @fn bool IsPointVisibleFromCamera(const int camera_0_index,
                                        const int camera_1_index,
                                        const int point_index)
   *  @brief  Indicates if a landmarks is visible from a pair of cameras
   *  @param[in] camera_0_index    First Camera index
   *  @param[in] camera_1_index    Second Camera index
   *  @param[in] point_index     Landmark index
   *  @return True if visible
   */
  bool IsPointVisibleFromCamera(const int camera_0_index,
                                const int camera_1_index,
                                const int point_index);

  /**
   *  @name   BackProjectLandmark
   *  @fn void BackProjectLandmark(const int camera_0_index,
                                    const int camera_1_index,
                                    const int landmark_index,
                                    cv::Mat* point)
   *  @brief  Back project point by triangulation
   *  @param[in]  camera_0_index  Index0
   *  @param[in]  camera_1_index  Index1
   *  @param[in]  landmark_index  Index2
   *  @param[out] point Backprojected points
   */
  void BackProjectLandmark(const int camera_0_index,
                           const int camera_1_index,
                           const int landmark_index,
                           cv::Mat* point);

  /**
   *  @name   EstimateRigidTransformSolver
   *  @fn void EstimateRigidTransformSolver(TransformationData* transformation)
   *  @brief  Estimate rigid transformation (i.e pose/location) using
   *          non-linear model (Full perspective). Minimization is done
   *          through Levenberg-Marquardt iterative algorithm.
   *  @param[in] transformation    Pointer to structure holding parameters
   *          such as landmark/object points/...
   */
  void EstimateRigidTransformSolver(TransformationData* transformation);
};
}
#endif /* __LTS5_FACE_MODEL_FITTER__ */
