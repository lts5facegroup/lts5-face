/**
 *  @file   fit_algorithm_factory.hpp
 *  @brief  Factory for 3DMM fitting algorithms
 *  @ingroup    face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   21/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_FIT_ALGORITHM_FACTORY__
#define __LTS5_FIT_ALGORITHM_FACTORY__

#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/face_reconstruction/fit_algorithm.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   MMFitAlgorithmFactory
 * @brief   Factory for 3DMM fitting algorithms
 * @author  Christophe Ecabert
 * @date    21/09/2017
 * @tparam T Data type
 */
template<typename T>
class LTS5_EXPORTS MMFitAlgorithmFactory {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  Get
   * @fn    static MMFitAlgorithmFactory<T>& Get(void)
   * @brief Provide single factory instance
   * @return    Factory instance
   */
  static MMFitAlgorithmFactory<T>& Get(void);

  /**
   *  @name   MMFitAlgorithmFactory
   *  @fn     MMFitAlgorithmFactory(const MMFitAlgorithmFactory& other) = delete;
   *  @brief  Copy constructor
   *  @param[in] other  Object to copy from
   */
  MMFitAlgorithmFactory(const MMFitAlgorithmFactory& other) = delete;

  /**
   *  @name operator=
   *  @fn MMFitAlgorithmFactory& operator=(const MMFitAlgorithmFactory& rhs) = delete
   *  @brief  Assignment operator
   *  @param[in]  rhs Object to assign from
   *  @return Newly assigned operator
   */
  MMFitAlgorithmFactory& operator=(const MMFitAlgorithmFactory& rhs) = delete;

  /**
   *  @name   ~MMFitAlgorithmFactory
   *  @fn     ~MMFitAlgorithmFactory(void) = default
   *  @brief  Destructor
   */
  ~MMFitAlgorithmFactory(void) = default;

#pragma mark -
#pragma mark Usage

  /**
   *  @name CreateByName
   *  @fn MMFitAlgorithm<T>* CreateByName(const std::string& name,
                                  const MorphableModel<T>* model)
   *  @brief  Create a spcific fitting algorithm instance
   *  @param[in]  name  Algorithm name
   *  @param[in] model  Morphable model instance to use while fitting
   *  @return Pointer to a fitting instance, or nullptr if name is not known by
   *            the factory
   */
  MMFitAlgorithm<T>* CreateByName(const std::string& name,
                                  const MorphableModel<T>* model);

  /**
   *  @name Register
   *  @fn void Register(const MMFitAlgorithmProxy<T>* algo)
   *  @brief  Register a type of image with a given proxy.
   *  @param[in]  algo  Algorithm to register
   */
  void Register(const MMFitAlgorithmProxy<T>* algo);

  /**
   * @name  Registered
   * @fn    void Registered(std::vector<std::string>* names) const
   * @brief Provide a list of all registered proxy within this factory
   * @param[out] names   List of registered algorithm
   */
  void Registered(std::vector<std::string>* names) const;

#pragma mark -
#pragma mark Private
 private:

  /**
   *  @name   MMFitAlgorithmFactory
   *  @fn     MMFitAlgorithmFactory(void)
   *  @brief  Constructor
   */
  MMFitAlgorithmFactory(void) = default;

  /** List of all unique registered image's proxies */
  std::vector<const MMFitAlgorithmProxy<T>*> proxies_;
};

}  // namepsace LTS5
#endif //__LTS5_FIT_ALGORITHM_FACTORY__
