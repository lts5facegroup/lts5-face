/**
 *  @file   mm_fitter.hpp
 *  @brief  3DMM fitter object
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   21/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_MM_FITTER__
#define __LTS5_MM_FITTER__

#include <string>

#include "lts5/utils/library_export.hpp"
#include "lts5/model/morphable_model.hpp"
#include "lts5/face_reconstruction/fit_algorithm.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   MorphableModelFitter
 * @brief   3DMM fitter object
 * @author  Christophe Ecabert
 * @date    21/09/2017
 * @tparam T Data type
 */
template<typename T>
class LTS5_EXPORTS MorphableModelFitter {
 public:
#pragma mark -
#pragma mark Type Definition
  /** Mesh type */
  using Mesh = typename PCAModel<T>::Mesh;
  /** Camera type */
  using Camera_t = typename MMFitAlgorithm<T>::Camera_t;
  /** Vertex type */
  using Vertex = typename MMFitAlgorithm<T>::Vertex;
  /** Statistics type */
  using Stat = typename MMFitAlgorithm<T>::Stat;
  /** Regularization type */
  using Regularization = typename MMFitAlgorithm<T>::Regularization;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  MorphableModelFitter
   * @fn    MorphableModelFitter(const std::string& fit_algo,
                                 const MorphableModel<T>* model)
   * @brief Constructor
   * @param[in] fit_algo    Fit algorithm's name
   * @param[in] model       Morphable model to use
   */
  MorphableModelFitter(const std::string& fit_algo,
                       const MorphableModel<T>* model);

  /**
   * @name  MorphableModelFitter
   * @fn    MorphableModelFitter(const MorphableModelFitter<T>& other) = delete
   * @brief Copy Constructor
   * @param[in] other  Object to copy from
   */
  MorphableModelFitter(const MorphableModelFitter<T>& other) = delete;

  /**
   * @name  operator=
   * @fn    MorphableModelFitter& operator=(const MorphableModelFitter<T>& rhs) =delete;
   * @brief Copy assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  MorphableModelFitter& operator=(const MorphableModelFitter<T>& rhs) =delete;

  /**
   * @name  MorphableModelFitter
   * @fn    MorphableModelFitter(MorphableModelFitter<T>&& other) = delete
   * @brief Move Constructor
   * @param[in] other  Object to move from
   */
  MorphableModelFitter(MorphableModelFitter<T>&& other) = delete;

  /**
   * @name  operator=
   * @fn    MorphableModelFitter& operator=(MorphableModelFitter<T>&& rhs) =delete;
   * @brief Move assignment operator
   * @param[in] rhs Object to move assign from
   * @return    Newly moved assigned object
   */
  MorphableModelFitter& operator=(MorphableModelFitter<T>&& rhs) =delete;

  /**
   * @name  ~MorphableModelFitter
   * @fn    ~MorphableModelFitter()
   * @brief Destructor
   */
  ~MorphableModelFitter();

#pragma mark -
#pragma mark Usage

  /**
   * @name  FitWithBoundingBox
   * @fn    int FitWithBoundingBox(const cv::Mat& image,
                                   const cv::Rect& bbox,
                                   Camera_t* camera,
                                   Mesh* mesh,
                                   Stat* stats)
   * @brief Fit 3D Model from a given image and bounding box.
   * @param[in] image   Input image
   * @param[in] bbox    Face bounding box
   * @param[out] camera Estimated camera transform
   * @param[out] mesh   Reconstructed mesh
   * @param[out] stats  Optimization statistics, can be nullptr if not used
   * @return    -1 if not converged, 0 otherwise
   */
  int FitWithBoundingBox(const cv::Mat& image,
                         const cv::Rect& bbox,
                         Camera_t* camera,
                         Mesh* mesh,
                         Stat* stats);

  /**
   * @name  FitWithLandmarks
   * @fn    int FitWithLandmarks(const cv::Mat& image,
                                 const cv::Mat& landmarks,
                                 Camera_t* camera,
                                 Mesh* mesh,
                                 Stat* stats)
   * @brief Fit 3D Model from a given image and set of landmarks.
   * @param[in] image       Input image
   * @param[in] landmarks   Set of landmarks
   * @param[out] camera     Estimated camera transform
   * @param[out] mesh       Reconstructed mesh
   * @param[out] stats      Optimization statistics, can be nullptr if not used
   * @return    -1 if not converged, 0 otherwise
   */
  int FitWithLandmarks(const cv::Mat& image,
                       const cv::Mat& landmarks,
                       Camera_t* camera,
                       Mesh* mesh,
                       Stat* stats);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  set_regularizer
   * @fn    void set_regularizer(const Regularization& regularization)
   * @brief Initialize regularizer
   * @param[in] regularization  Regularization factor
   */
  void set_regularizer(const Regularization& regularization) {
    reg_ = regularization;
  }

  /**
   * @name  get_shape_parameter
   * @fn    const cv::Mat& get_shape_parameter() const
   * @brief Provide estimated shape parameters
   * @return    Shape parameters
   */
  const cv::Mat& get_shape_parameter() const {
    return this->algo_->get_shape_parameter();
  }

  /**
   * @name  get_texture_parameter
   * @fn    const cv::Mat& get_texture_parameter() const
   * @brief Provide estimated texture parameters
   * @return    Texture parameters
   */
  const cv::Mat& get_texture_parameter() const {
    return this->algo_->get_texture_parameter();
  }

  /**
   * @name  get_illumination_parameter
   * @fn    const cv::Mat& get_illumination_parameter() const
   * @brief Provide estimated illumination parameters
   * @return    Illumination parameters
   */
  const cv::Mat& get_illumination_parameter() const {
    return this->algo_->get_illumination_parameter();
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  ConvertInput
   * @fn    void ConvertInput(const cv::Mat& image, const cv::Mat& landmarks,
                              const T& diag, const bool& lms_stacked,
                              cv::Mat* patch, cv::Mat* lms)
   * @brief Convert inputs (image + landmarks) into proper format for fitting
   * @param[in] image       Image to normalize
   * @param[in] landmarks   Landmarks from tracker
   * @param[in] diag        Desired main diagonal dimension
   * @param[in] lms_stacked True if lms are in [x,x,x,.. ,y,y,y,...] format,
   *                        False otherwise
   * @param[out] patch      Converted image
   * @param[out] lms        Converted landmarks
   */
  void ConvertInput(const cv::Mat& image,
                    const cv::Mat& landmarks,
                    const T& diag,
                    const bool& lms_stacked,
                    cv::Mat* patch,
                    cv::Mat* lms);

  /**
   * @name  Fit
   * @fn    int Fit(const cv::Mat& image,
                    const cv::Mat& landmarks,
                    const Regularization& reg,
                    Camera_t* camera,
                    Mesh* mesh,
                    Stat* stats)
   * @brief Fit a 3DMM tp a given image and set of landmarks
   * @param[in] image       Image to fit model on
   * @param[in] landmarks   Set of landmarks (can be left empty)
   * @param[in] reg         Regularization parameters
   * @param[in] camera      Estimated camera transform
   * @param[in] mesh        Reconstructed mesh. Can hold initial guess but not
   *                        mandatory
   * @param[in] stats       Fitting statistics, can be nullptr if not used
   * @return    -1 if not converged, 0 otherwise
   */
  int Fit(const cv::Mat& image,
          const cv::Mat& landmarks,
          const Regularization& reg,
          Camera_t* camera,
          Mesh* mesh,
          Stat* stats);

  /**
   * @name  PlaceMeanLandmarks
   * @fn    void PlaceMeanLandmarks(const Camera_t* camera,
                                    const cv::Rect& bbox,
                                    cv::Mat* landmarks) const
   * @brief Place meanshape within a given bbox
   * @param[in] camera  Camera object
   * @param[in] bbox    Bounding box where to place mean shape
   * @param[out] landmarks   Mean landmarks place within bbox
   */
  void PlaceMeanLandmarks(const Camera_t* camera,
                          const cv::Rect& bbox,
                          cv::Mat* landmarks) const;

  /** Fit algorithm */
  MMFitAlgorithm<T>* algo_;
  /** Morphable Model */
  const MorphableModel<T>* model_;
  /** Regularization factor */
  Regularization reg_;
};

}  // namepsace LTS5
#endif //__LTS5_MM_FITTER__
