/**
 *  @file   image_stream.hpp
 *  @brief  Image loading
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   01/06/16
 *  Copyright © 2016 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_IMAGE_STREAM__
#define __LTS5_IMAGE_STREAM__

#include "lts5/utils/library_export.hpp"
#include "lts5/face_reconstruction/base_stream.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  ImageStream
 *  @brief  Image stream implementation
 *  @author Christophe Ecabert
 *  @date   01/06/2016
 */
class LTS5_EXPORTS ImageStream : public LTS5::BaseStream {
 public:
#pragma mark -
#pragma mark Type definition

  struct Parameter : BaseStream::BaseParameter {
    /** Image root folder */
    std::string root_folder;
    /** Image folder */
    std::vector<std::string> image_folder;
    /** Image extension */
    std::string image_ext;
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name       ImageStream
   *  @fn         ImageStream(const ImageStream::Parameter& params)
   *  @brief      Constructor
   *  @param[in]  params  Stream parameters
   */
  ImageStream(const ImageStream::Parameter& params);

  /**
   *  @name       ImageStream
   *  @fn         ImageStream(const ImageStream& other) = delete
   *  @brief      Copy Constructor
   *  @param[in]  other Object to copy from
   */
  ImageStream(const ImageStream& other) = delete;

  /**
   *  @name       operator=
   *  @fn         ImageStream& operator=(const ImageStream& rhs) = delete
   *  @brief      Assignment operator
   *  @param[in]  rhs Object to assign from
   *  @return     Assigned object
   */
  ImageStream& operator=(const ImageStream& rhs) = delete;

  /**
   *  @name       ~ImageStream
   *  @fn         ~ImageStream()
   *  @brief      Destructor
   */
  ~ImageStream() override = default;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Read
   *  @fn int Read() override
   *  @brief  Read image from the stream
   *  @return -1 if error, 0 otherwise
   */
  int Read() override;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_frame_to_process
   *  @fn void set_frame_to_process(int frame_index) override
   *  @brief  Set from which frame the pipeline process
   *  @param[in]  frame_index   Frame's index
   */
  void set_frame_to_process(int frame_index) override {
    n_frame_loaded_ = (frame_index > n_max_frame_ ?
                       frame_index :
                       n_max_frame_ - 1);
  }

  /**
   *  @name   get_frame_index
   *  @fn     int get_frame_index() const
   *  @brief  Provide the index of the current frame
   *  @return Frame index
   */
  int get_frame_index() const override {
    return n_frame_loaded_;
  }

  /**
   *  @name   get_n_max_frame
   *  @fn     int get_n_max_frame() const override
   *  @brief  Provide the maximum index of the frame
   *  @return Maximum Frame index
   */
  int get_n_max_frame() const override {
    return n_max_frame_;
  }

  /**
   *  @name get_frame_name
   *  @fn void get_frame_name(std::string* frame_name,
   *                          std::string* frame_ext) const  override
   *  @brief  Provide the name of the current frame according to the media
   *  @param[out] frame_name  Frame name
   *  @param[out] frame_ext   Frame extension
   */
  void get_frame_name(std::string* frame_name,
                      std::string* frame_ext) const override;

#pragma mark -
#pragma mark Private
 private:
  /** Parameters */
  Parameter params_;
  /** Number of frame already loaded */
  int n_frame_loaded_;
  /** Maximum frame number */
  int n_max_frame_;

  /** List of image */
  std::vector<std::vector<std::string>> list_image_;
  /** First valid index */
  size_t valid_index_;

};

}  // namespace LTS5
#endif /* __LTS5_IMAGE_STREAM__ */
