/**
 *  @file   face_synthesis_pipeline.hpp
 *  @brief  Face reconstruction pipeline
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 04/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_FACE_SYNTHESIS_PIPELINE__
#define __LTS5_FACE_SYNTHESIS_PIPELINE__

#include <iostream>

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/face_reconstruction/base_stream.hpp"
#include "lts5/face_reconstruction/virtual_camera.hpp"
#include "lts5/face_reconstruction/face_model_fitter.hpp"
#include "lts5/face_reconstruction/base_extractor.hpp"
#include "lts5/face_reconstruction/base_mesh_exporter.hpp"
#include "lts5/face_reconstruction/texture_mapper.hpp"
#include "lts5/face_reconstruction/external_video_stream.hpp"

#ifndef HAS_CUDA_
using TexMapper = LTS5::TextureMapper;
#else
#include "lts5/cuda/face_reconstruction/texture_mapper.hpp"
using TexMapper = LTS5::CUDA::TextureMapper;
#endif


#define SHOW_RECONSTRUCTION 1

#pragma mark -
#pragma mark Type forwarding

/** Type forwarding */
namespace tinyxml2 {
  class XMLDocument;
}
/** Exporter type definition */
/*#if SHOW_RECONSTRUCTION
typedef LTS5::Mesh2ScreenExporter MeshExporter ;
#else
typedef LTS5::Mesh2ImageExporter MeshExporter;
#endif*/

#pragma mark -
#pragma mark Synthesis Pipeline

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


/**
 *  @class  FaceSynthesisPipeline
 *  @brief  Modulte holding the whole process chain to synthetize new
 *          images from input frames with the help of Bilinear
 *          3D Morphable Model
 *  @author Christophe Ecabert
 *  @date   04/03/14
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS FaceSynthesisPipeline {

public :

  /**
   *  @struct FaceSynthesisPipelineParameters
   *  @brief  Parameters used to create FaceSynthesisPipeline object
   */
  struct FaceSynthesisPipelineParameters {
    /** Config file for the whole pipeline */
    std::string pipeline_xml_config_file;
    /** Working directory */
    std::string working_directory = "";
    /** Output folder, location where to dump the reconstruction.
     * If empty the reconstruction is displayed on the screen */
    std::string output_folder = "";
    /** Display on screen */
    bool output_on_screen = false;
  };

  /**
   *  @enum   FaceMeshSelection
   *  @brief  Possible mesh selection
   */
  enum FaceMeshSelection {
    /** Reconstruct face shape */
    kModel,
    /** Synthetic input face shape */
    kSynthetic,
    /** Mean shape */
    kMeanShape,
    kDebug
  };

  /**
   *  @struct PipelineProcessConfiguration
   *  @brief  Configuration for current processing
   */
  struct PipelineProcessConfiguration {
    /** Use real or fake data */
    bool has_synthetic_data = true;
    /** Synthetic landmark */
    std::vector<cv::Mat> landmark;
    /** Whether or not to draw landmarks */
    bool draw_landmarks = false;
  };

  /** Fit parameters */
  FaceModelFitter::FitParameter fit_parameter_;
  /** Fitter results */
  FaceModelFitter::FitResult fit_result_;

private :

  /** Working directory */
  std::string working_directory_;

  // Acquisition
  // ----------------------------------------------------------
  /** Acquisition parameters */
  LTS5::BaseStream::BaseParameter* stream_params_ = nullptr;
  /** Acquisition module */
  LTS5::BaseStream* frame_capture_ = nullptr;

  // Face tracking
  // ----------------------------------------------------------
  /** Face tracker configuration */
  LTS5::BaseExtractor::BaseParameter* extractor_config_ = nullptr;
  /** Face tracker module */
  LTS5::BaseExtractor* face_extractor_ = nullptr;
  /** Extractor output */
  LTS5::BaseExtractor::ExtractorData extractor_data_;

  // Camera module
  // ----------------------------------------------------------
  /** Structure holding camera parameters */
  std::vector<LTS5::VirtualCamera*> camera_setup_;

  // Texture mapping
  // ----------------------------------------------------------
  /** Module to map texture on 3D face */
  TexMapper* texture_mapper_ = nullptr;
  /** Texture mapper configuration */
  TextureMapper::TextureMapperConfig texture_mapper_config_;
  /** Projection matrix for each view */
  std::vector<cv::Mat> projection_mat_;

  // Data Export
  // ----------------------------------------------------------
  /** Exporter */
  LTS5::BaseMeshExporter* mesh_exporter_ = nullptr;
  /** Folder where obj file are exported */
  std::string output_folder_;

  // Fit
  // ----------------------------------------------------------
  /** Fitter parameters */
  FaceModelFitter::FitterParameter fitter_configuration_;
  /** Model fitter */
  FaceModelFitter* model_fitter_  = nullptr;
  /** Projection type, i.e weak/full perspective */
  FaceModelFitter::ProjectionMode projection_model_;
  /** Configurtion file for triangulation */
  std::string triangulation_file_;
  /** Face mesh to render */
  LTS5::Mesh<float>* face_mesh_;

  /**
   *  @name   ParseXML
   *  @fn void ParseXML(const tinyxml2::XMLDocument& document)
   *  @brief  Read xml file and fill proper structure to
   *          initialize process pipeline
   *  @param[in]    document    XML document
   */
  void ParseXML(const tinyxml2::XMLDocument& document);

  /**
   *  @name   ReleaseMemory
   *  @fn void ReleaseMemory(void)
   *  @brief  Release dynamically instantiated objects
   */
  void ReleaseMemory(void);

public:

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   FaceSynthesisPipeline
   *  @fn FaceSynthesisPipeline(const FaceSynthesisPipelineParameters& configuration)
   *  @brief  Constructor
   *  @param[in]  configuration      Pipeline parameters
   */
  explicit FaceSynthesisPipeline(const FaceSynthesisPipelineParameters& configuration);

  /**
   *  @name   ~FaceSynthesisPipeline
   *  @fn ~FaceSynthesisPipeline()
   *  @brief  Destructor
   */
  ~FaceSynthesisPipeline();

#pragma mark -
#pragma mark Processing

  /**
   *  @name   Process
   *  @fn int Process(const PipelineProcessConfiguration& process_configuration)
   *  @brief  Start the synthesis for the current frame
   *  @param[in]  process_configuration  Configuration for current fit
   *  @return -1 if error, 0 otherwise
   */
  int Process(const PipelineProcessConfiguration& process_configuration);

  /**
   *  @name ResetTrackingState
   *  @fn void ResetTrackingState(void)
   *  @brief  Reinitialize tracking state
   */
  void ResetTrackingState(void);

  /**
   * @name  TrackFrontalImage
   * @fn    int TrackFrontalImage(cv::Mat* shape)
   * @brief Detect landmark on synthetic frontal image
   * @param[out] shape  Detected landmarks
   * @return    -1 if error, 0 otherwise
   */
  int TrackFrontalImage(cv::Mat* shape);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_frame_to_process
   *  @fn void set_frame_to_process(const int frame_index)
   *  @brief  Indicate from which frame to process
   *  @param[in]  frame_index   Frame's index
   */
  void set_frame_to_process(const int frame_index) {
    frame_capture_->set_frame_to_process(frame_index);
    face_extractor_->set_frame_index(frame_index);
  }

  /**
   *  @name   get_frame_to_process
   *  @fn int get_frame_to_process(void)
   *  @brief  Provide from which frame to process
   *  @return Frame index
   */
  int get_frame_to_process(void) {
    return face_extractor_->get_current_frame_index();
  }

  /*
   *  @name   set_export_folder
   *  @fn void set_export_folder(const std::string& folder)
   *  @brief  Define where file are exported
   *  @param[in]  folder     Output location
   */
  //void set_export_folder(const std::string& folder);

  /**
   *  @name   get_mesh
   *  @fn Mesh<float>* get_mesh(void) const
   *  @brief  Return reconstructed mesh
   *  @return Mesh object
   */
  Mesh<float>* get_mesh(void) const {
    return face_mesh_;
  }

  /**
   *  @name   get_frame
   *  @fn const BaseStream::Buffer& get_frame(void) const
   *  @brief  Provide access to the buffer where input image are stored
   *  @return Reference to image buffer
   */
  const BaseStream::Buffer& get_frame(void) const {
    return frame_capture_->get_buffer();
  }

  /**
   * @name  get_landmarks
   * @fn  const std::vector<cv::Mat>& get_landmarks(void) const
   * @brief Provide sets of landmarks
   * @return  Set of landmarks for each view, can be empty if no landmarks were
   *          detected
   */
  const std::vector<cv::Mat>& get_landmarks(void) const {
    return extractor_data_.landmarks;
  }

  /**
   *  @name   get_current_frame_index
   *  @fn int get_current_frame_index(void) const
   *  @brief  Provide the index of the current frame being processed
   *  @return Frame index
   */
  int get_current_frame_index(void) const {
    return frame_capture_->get_frame_index();
  }

  /**
   *  @name   get_number_of_view
   *  @fn int get_number_of_view(void) const
   *  @brief  Give the number of real camera used for acquiring images
   *  @return Number of cameras
   */
  int get_number_of_view(void) const {
    return stream_params_->n_view;
  }

  /**
   *  @name   get_total_number_of_frame
   *  @fn int get_total_number_of_frame(void) const
   *  @brief  Gives the number of image to process
   *  @return Number of frame
   */
  int get_total_number_of_frame(void) const;

  /**
   *  @name   get_projection_matrix
   *  @fn cv::Mat get_projection_matrix(const int view) const
   *  @brief  Return the projection matrix for a given view
   *  @param[in]  view selection
   *  @return Projection matrix P
   */
  cv::Mat get_projection_matrix(const int view) const {
    return model_fitter_->get_projection_matrix(view);
  }

  /**
   *  @name   get_raw_3d_surface
   *  @fn cv::Mat get_raw_3d_surface(void) const
   *  @brief  Return the matrix holding the surface's 3D pts
   *  @return 3D pts
   */
  cv::Mat get_raw_3d_surface(void) const {
    return model_fitter_->get_raw_3d_surface();
  }

  /**
   *  @name    get_projection_model
   *  @fn FaceModelFitter::ProjectionMode get_projection_model(void) const
   *  @brief   Indicate which projection model is currently used by the
   *           pipeline
   *  @return  ProjectionModelEnum
   */
  FaceModelFitter::ProjectionMode get_projection_model(void) const {
    return projection_model_;
  }

  /**
   *  @name get_frontal_image
   *  @fn cv::Mat get_frontal_image(void) const
   *  @brief  Provide the synthetic frontal image
   *  @return Frontal virtual view
   */
  cv::Mat get_frontal_image(void) const {
    return mesh_exporter_->get_image();
  }

  /**
   * @name  get_pose
   * @fn  const PPLData::ModelPoseParametersStruct& get_pose(void) const
   * @brief Provide rigid transformation data
   * @return  Rigid transform parameters
   */
  const PPLData::ModelPoseParametersStruct& get_pose(void) const {
    return model_fitter_->get_pose();
  }

  /**
   * @name  set_external_input_callback
   * @fn  void set_external_input_callback(ExternalVideoStream::ICallbacks* callbacks)
   * @brief Define the callback to invoke when external capture module is used
   * @param[in] callbacks Interface to call from input capture module
   */
  void set_external_input_callback(ExternalVideoStream::ICallbacks* callbacks) {
    if (stream_params_->stream_type == BaseStream::StreamType::kExtVideo) {
      auto cap = reinterpret_cast<ExternalVideoStream*>(frame_capture_);
      cap->InitCallback(callbacks);
    }
  }
};
}
#endif /* __LTS5_FACE_SYNTHESIS_PIPELINE__ */
