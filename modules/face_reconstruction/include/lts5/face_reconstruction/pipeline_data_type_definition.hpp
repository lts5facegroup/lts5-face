/**
 *  @file   pipeline_data_type_definition.hpp
 *  @brief  Type definition for face synthesis pipeline
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 20/02/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_PIPELINE_DATA_TYPE_DEFINITION__
#define __LTS5_PIPELINE_DATA_TYPE_DEFINITION__

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @namespace  PPLData
 *  @brief      Pipeline definition
 */
namespace PPLData {
  /**
   *  @struct ModelPoseParametersStruct
   *  @brief  Encapsulate parameters linked to rigid transformation
   *  @ingroup face_reconstruction
   */
  struct LTS5_EXPORTS ModelPoseParametersStruct {
    /** Number of views */
    int number_of_view;
    /** Rotation matrix, one for each view */
    std::vector<cv::Mat> rotation;
    /** Translation vector, one for each view */
    std::vector<cv::Mat> translation;
    /** Scales for each views */
    std::vector<float> scale;
    /** Euler angle used in full perspective projection */
    std::vector<float> euler_angle;

    /**
     *  @name   ModelPoseParametersStruct
     *  @brief  Constructor
     *  @fn ModelPoseParametersStruct(void)
     */
    ModelPoseParametersStruct(void) : number_of_view(0),
                                      rotation(0),
                                      translation(0),
                                      scale(0),
                                      euler_angle(0) {
      euler_angle.resize(3, 0.f);
    };

    /**
     *  @name   ModelPoseParametersStruct
     *  @brief  Copy Constructor
     *  @fn ModelPoseParametersStruct(const ModelPoseParametersStruct& other)
     *  @param[in] other  Object to copy from
     */
    ModelPoseParametersStruct(const ModelPoseParametersStruct& other) {
      number_of_view = other.number_of_view;
      scale = other.scale;
      euler_angle = other.euler_angle;
      for (int i = 0; i < number_of_view; ++i) {
        rotation.push_back(other.rotation[i].clone());
        translation.push_back(other.translation[i].clone());
      }
    }

    /**
     *  @name   operator=
     *  @brief  Assignment operator
     *  @fn ModelPoseParametersStruct& operator=(const ModelPoseParametersStruct& rhs)
     *  @param[in] rhs  Object to assign from
     *  @return Newly assigned object
     */
    ModelPoseParametersStruct& operator=(const ModelPoseParametersStruct& rhs) {
      if (this != &rhs) {
        number_of_view = rhs.number_of_view;
        scale = rhs.scale;
        euler_angle = rhs.euler_angle;
        rotation.resize(number_of_view);
        translation.resize(number_of_view);
        for (int i = 0; i < number_of_view; ++i) {
          rhs.rotation[i].copyTo(rotation[i]);
          rhs.translation[i].copyTo(translation[i]);
        }
      }
      return *this;
    }

    /**
     *  @name ~ModelPoseParametersStruct
     *  @fn ~ModelPoseParametersStruct(void)
     *  @brief  Destructor
     */
    ~ModelPoseParametersStruct(void) {
      rotation.clear();
      translation.clear();
      scale.clear();
      euler_angle.clear();
    }

    /**
     *  @name   InitStruct
     *  @fn void InitStruct(int number_of_view)
     *  @brief  Initialize internal structure, i.e. vector(s)
     *  @param[in]  number_of_view  Number of views
     */
    void InitStruct(int number_of_view) {

      if(number_of_view != this->number_of_view) {
        // View
        this->number_of_view = number_of_view;
        // CLear
        scale.clear();
        rotation.clear();
        translation.clear();
        // Init scalar
        scale = std::vector<float>(number_of_view,0.f);
        // Init matrix
        for(int i = 0 ; i < number_of_view; i++) {
          rotation.push_back(cv::Mat::zeros(3,3,CV_32FC1));
          translation.push_back(cv::Mat::zeros(3,1,CV_32FC1));
        }
      }
    };

    bool CheckTransform(void) {
      bool valid = true;
      for (size_t i = 0; i < scale.size(); ++i) {
        if (translation[i].at<float>(0) != -1.f) {
          // Scale =
          bool s_ok = (scale[i] > 1.f);
          // Rotation
          auto det = cv::determinant(rotation[i]);
          bool r_ok = std::abs(det - 1.0) < 0.1;
          // Stack result
          valid &= s_ok & r_ok;
        }
      }
      return valid;
    }
  };

  /**
   *  @enum ErrorSelectionEnum
   *  @brief  List of possible error recorded through process
   *  @ingroup face_reconstruction
   */
  enum ErrorSelectionEnum {
    /** Point to point error */
    pts_to_pts_error = 0,
    /** Projection error for ID */
    kProjId = 1,
    /** Projection error for expression */
    kProjExpr = 2,
    /** Id's weight norm */
    kNormId = 3,
    /** Expr's weight norm */
    kNormExpr = 4,
  };

  /**
   *  @struct EtaErrorStruct
   *  @brief  Hold process error during eta estimation
   *  @ingroup face_reconstruction
   */
  struct LTS5_EXPORTS ProcessError {
    /** Internal counter increment everytime a value is added */
    int counter;
    /** Matrix hold error information */
    cv::Mat error_data;

    /**
     *  @name   InitErrorStructure
     *  @fn void InitErrorStructure(int number_of_run = 100)
     *  @brief  Initialize internal structure
     *  @param[in]  number_of_run  Number of experiment to record
     */
    void InitErrorStructure(int number_of_run = 100) {
      // Init matrix
      error_data.create(number_of_run, 5, CV_32FC1);
      counter = 0;
    }

    /**
     *  @name   ClearError
     *  @brief  Reset error matrix
     *  @fn void ClearError(void)
     */
    void ClearError(void) {
      counter = 0;
      error_data = 0.f;
    }

    /**
     *  @name   AddElement
     *  @fn void AddElement(const ErrorSelectionEnum& error_type,
                            const float value)
     *  @brief  Add a new element at current error buffer location
     *  @param[in]  error_type    Error type
     *  @param[in]  value      Error value
     */
    void AddElement(const ErrorSelectionEnum& error_type,
                    const float value) {
      // Add at current position
      error_data.at<float>(counter,error_type) = value;
    }

    /**
     *  @name   NextIteration
     *  @fn void NextIteration(void)
     *  @brief  Jump to next row into error buffer
     */
    void NextIteration(void) {
      counter++;
    }
  };
}
}
#endif /* __LTS5_PIPELINE_DATA_TYPE_DEFINITION__ */
