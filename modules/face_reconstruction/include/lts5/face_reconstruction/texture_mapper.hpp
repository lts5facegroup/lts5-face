/**
 *  @file   "lts5/face_reconstruction/texture_mapper.hpp"
 *  @brief  Generate texture map from a 3D mesh and image
 *          Based on :
 *          Real-time Facial Animation with Image-based Dynamic Avatars
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 29/08/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_TEXTURE_MAPPER__
#define __LTS5_TEXTURE_MAPPER__

#include <atomic>

#include "opencv2/core/core.hpp"


#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/face_reconstruction/base_stream.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  TextureMapper
 *  @brief  Map texture from each view to the 3D reconstruction
 *  @author Christophe Ecabert
 *  @date   29/08/14
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS TextureMapper {
 public :

#pragma mark -
#pragma mark Type definition

  /**
   *  @enum  MappingType
   *  @brief  List the possible type of texture mapping
   */
  enum MappingType {
    /** Project vertex on image and interpolate texture */
    kProjection,
    /** Create texture map without unwrapping the mesh */
    kTextureMap,
    /** Create texture map and unwrap the mesh */
    kUnwrapTextureMap
  };

  /**
   *  @struct TextureMapperConfig
   *  @brief  TextureMapper configuration
   */
  struct TextureMapperConfig {
    /** Mapping type */
    MappingType mapping_type;
    /** Path to control points config file */
    std::string control_pts_config_file;
    /** Texture map size */
    int texture_map_size;
    /** Mesh downsampling factor */
    int d_factor;
    /** Per-vertex weight importance controle */
    float w_n;
    /** Per-pixel weight importance controle */
    float w_u;
    /** Maximum per-vertex weight to consider while
     * computing per-pixel weight */
    int per_vertex_thresh;
    /** Gamma correction - exponent */
    float gamma_exp;
    /** Gamma correction - scale */
    float gamma_scale;


    /**
     *  @name TextureMapperConfig
     *  @fn TextureMapperConfig(void)
     *  @brief  Constructor
     */
    TextureMapperConfig(void) {
      mapping_type = MappingType::kProjection;
      control_pts_config_file = "";
      texture_map_size = 512;
      d_factor = 10;
      w_n = 5;
      w_u = 5;
      per_vertex_thresh = 100;
      gamma_exp = 1.f;
      gamma_scale = 1.f;
    }
  };

#pragma mark -
#pragma mark Constructor/Destructor

  /**
   *  @name   TextureMapper
   *  @fn TextureMapper(void)
   *  @brief  Constructor
   */
  TextureMapper(void);

  /**
   *  @name   TextureMapper
   *  @fn explicit TextureMapper(const TextureMapperConfig& config)
   *  @brief  Constructor
   *  @param[in]  config  Mapper configuration
   */
  explicit TextureMapper(const TextureMapperConfig& config);

  /**
   *  @name   TextureMapper
   *  @fn TextureMapper(const TextureMapper& other) = delete
   *  @brief  Copy Constructor
   *  @param[in]  other  Object to copy from
   */
  TextureMapper(const TextureMapper& other) = delete;

  /**
   * @name  operator=
   * @fn    TextureMapper& operator=(const TextureMapper& rhs) = delete
   * @brief Assignment operator
   * @param rhs Object to assign from
   * @return    Newly assigned object
   */
  TextureMapper& operator=(const TextureMapper& rhs) = delete;

  /**
   *  @name   ~TextureMapper
   *  @fn ~TextureMapper(void)
   *  @brief  Destructor
   */
  ~TextureMapper(void);

  /**
   *  @name Load
   *  @fn int Load(const std::string& file)
   *  @brief  Load mapper configuration
   *  @param[in] file File holding configuration
   */
  int Load(const std::string& file);

  /**
   *  @name Initialize
   *  @fn void Initialize(LTS5::Mesh<float>* face_mesh)
   *  @brief  Initialize texture mapper module
   *  @param[in,out]  face_mesh Mesh used to map texture on
   */
  void Initialize(LTS5::Mesh<float>* face_mesh);

#pragma mark -
#pragma mark Process

  /**
   *  @name Process
   *  @fn void Process(const BaseStream::Buffer& images,
                       const std::vector<cv::Mat>& projection_mat,
                       const std::vector<cv::Mat>& landmarks,
                       LTS5::Mesh<float>* face_mesh)
   *  @brief  Map texture for a given set of images
   *  @param[in]  images          Set of images used to map texture
   *  @param[in]  projection_mat  Projection matrix for each views
   *  @param[in]  landmarks       List of landmarks detected by the face tracker
   *  @param[in,out]  face_mesh   Reconstructed mesh
   *  @return -1 if error, 0 otherwise
   */
  int Process(const BaseStream::Buffer& images,
              const std::vector<cv::Mat>& projection_mat,
              const std::vector<cv::Mat>& landmarks,
              LTS5::Mesh<float>* face_mesh);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name get_mapper_configuration
   *  @fn TextureMapperConfig& get_mapper_configuration(void)
   *  @brief  Provide mapper configuration
   *  @return Mapper configuration
   */
  TextureMapperConfig& get_mapper_configuration(void) {
    return mapper_config_;
  }

  /**
   *  @name set_mapper_configuration
   *  @fn void set_mapper_configuration(const TextureMapperConfig& config)
   *  @brief  Set a new configuration
   *  @param[in]  config  Configuration to set
   */
  void set_mapper_configuration(const TextureMapperConfig& config) {
    mapper_config_ = config;
    texture_map_.create(mapper_config_.texture_map_size,
                        mapper_config_.texture_map_size,
                        CV_8UC1);
  }

  /**
   *  @name get_texture_map
   *  @fn const cv::Mat& get_texture_map(void) const
   *  @brief  Provide access to the generated texture map
   *  @return Texutre map
   */
  const cv::Mat& get_texture_map(void) const {
    return texture_map_;
  }

#pragma mark -
#pragma mark Private
 private :

  /** Vertex type */
  using Vertex = Mesh<float>::Vertex;
  /** Normal type */
  using Normal = Mesh<float>::Normal;
  /** Edge type */
  using Edge = Mesh<float>::Edge;
  /** Texture coordinate type */
  using TCoord = Mesh<float>::TCoord;
  /** Triangle type */
  using Triangle = Mesh<float>::Triangle;
  /** 2D Vector */
  using Vec2 = Vector2<float>;
  /** 3D Vector */
  using Vec3 = Vector3<float>;
  /** Camera-Normal angle treshold */
  //constexpr static float angle_thr = 0.35f;
  constexpr static float angle_thr = 0.35f;
  /** Frontal Normal angle treshold */
  constexpr static float front_angle_thr = 0.1f;

  /**
   * @struct    TexMapCorrespondance
   * @brief     Correspondance for one vertex, low->high res
   *            texture map triangulation
   */
  struct TexMapCorrespondance {
    /** High resolution triangle index */
    int tri_index;
    /** Barycentric coordinate - u */
    float u;
    /** Barycentric coordinate - v */
    float v;
    /** Barycentric coordinate - w */
    float w;
  };


  /** Indicate if module is initialized */
  bool  is_initialized_;
  /** Flig indicate if some pixel are set in the texture map */
  std::atomic<bool> has_some_pixel_;
  /** Mapper configuration */
  TextureMapperConfig mapper_config_;
  /** Index of the control points for image warping */
  std::vector<int> control_pts_index_;
  /** Interpolated vertex */
  std::vector<Vec3> sampled_vertex_;
  /** Src points */
  std::vector<std::vector<Vec2>> image_pts_;
  /** Texture coordinate */
  std::vector<TCoord> tex_coordinate_;
  /** Target points, scaled texture coordinate */
  std::vector<TCoord> target_pts_;
  /** Target points, scaled texture coordinate for low resolution mesh */
  std::vector<TCoord> low_res_target_pts_;
  /** Texture map bounding box */
  AABB<float> tex_bbox_;
  /** Low resolution triangulation */
  std::vector<Triangle> low_res_triangulation_;
  /** Connectivity */
  std::vector<std::vector<int>> low_res_connectivity_;
  /** Low resultion triangulation to high resolution correspondance */
  std::vector<TexMapCorrespondance> interpolation_map_;
  /** Triangulation */
  std::vector<Triangle> triangle_list_;
  /** Extra triangle for closing eye + mouth */
  std::vector<Triangle> extra_triangle_list_;
  /** Vertex visibility by view */
  cv::Mat vertex_view_visibility_;
  /** Vertex weight by view */
  cv::Mat vertex_view_weight_;
  /** Vertex visibility in frontal frame */
  cv::Mat vertex_visibility_;
  /** Triangle map */
  cv::Mat triangle_map_;
  /** Triangle closest vertex weight */
  std::vector<std::vector<int>> triangle_vertex_weight_selection_;
  /** Texture map */
  cv::Mat texture_map_;

  /**
   *  @name ProjectionMat2RotationMat
   *  @fn void ProjectionMat2RotationMat(const cv::Mat& proj_mat, cv::Mat* rot_mat)
   *  @brief  Extract rotation from projection matrix
   *  @param[in]  proj_mat  Projection matrix
   *  @param[out] rot_mat   Rotation matrix
   */
  void ProjectionMat2RotationMat(const cv::Mat& proj_mat, cv::Mat* rot_mat);

  /**
   *  @name SetupControlPoints
   *  @fn void SetupControlPoints(const LTS5::Mesh<float>* mesh,
                             const std::vector<cv::Mat>& proj_mat)
   *  @brief  Setup control points onto image plane, either from face tracker
   *          detection or by projecting 3d pts
   *  @param[in]  mesh      Where control points are stored
   *  @param[in]  proj_mat  List of projection matrix
   */
  void SetupControlPoints(const LTS5::Mesh<float>* mesh,
                          const std::vector<cv::Mat>& proj_mat);

  /**
   * @name  ComputeTriangleMap
   * @fn    void ComputeTriangleMap(void)
   * @brief Compute a map where each pixel has the triangle number that belong
   *        to it in the texture map
   */
  void ComputeTriangleMap(void);

  /**
   * @name  DownsampleTextureMap
   * @fn void DownsampleTextureMap(const LTS5::Mesh<float>* mesh);
   * @brief Uniformly downsample the texture map and compute triangle close
   *        neighbors
   * @param[in] mesh    Mesh holding texture coordinate.
   */
  void DownsampleTextureMap(const LTS5::Mesh<float>* mesh);

  /**
   * @name  ComputeVertexVisibilityAndWeight
   * @fn    void ComputeVertexVisibilityAndWeight(const LTS5::Mesh<float>* mesh,
                                        const std::vector<cv::Mat>& proj_mat)
   * @brief In one pass compute the following coefficients :
   *        vertex visibility by view
   *        vertex visibility in the frontal view
   *        vertex weight for a given view
   * @param[in] mesh                    Reconstructed mesh
   * @param[in] proj_mat                Projection matrix for each views
   */
  void ComputeVertexVisibilityAndWeight(const LTS5::Mesh<float>* mesh,
                                        const std::vector<cv::Mat>& proj_mat);

  /**
   * @name  WarpView
   * @fn    void WarpView(const std::vector<cv::Mat>& image)
   * @brief Warp each view into texture map space, including visibility test
   * @param[in] image    List of input view to warp into texture map coordinate
   * @param[in] triangle Triangles to use will warping
   * @return -1 if error, 0 otherwise
   */
  int WarpView(const std::vector<cv::Mat>& image,
               const std::vector<Triangle>& triangle);

  /**
   *  @name GenerateTextureMap
   *  @fn int GenerateTextureMap(const LTS5::Mesh<float>* mesh,
                         const std::vector<cv::Mat>& images,
                         const std::vector<cv::Mat>& proj_mat,
                         const std::vector<cv::Mat>& landmarks)
   *  @brief  Generate texture map
   *  @param[in]  mesh      Mesh from where to compute texture map
   *  @param[in]  images    Input images
   *  @param[in]  proj_mat  Projection matrix
   *  @param[in]  landmarks List of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int GenerateTextureMap(const LTS5::Mesh<float>* mesh,
                         const std::vector<cv::Mat>& images,
                         const std::vector<cv::Mat>& proj_mat,
                         const std::vector<cv::Mat>& landmarks);
  };
}
#endif /* __LTS5_TEXTURE_MAPPER__ */
