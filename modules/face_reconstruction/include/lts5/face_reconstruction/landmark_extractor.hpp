/**
 *  @file   landmark_extractor.hpp
 *  @brief  Extract landmark from a given stream of images
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 04/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_LANDMARK_EXTRACTOR__
#define __LTS5_LANDMARK_EXTRACTOR__

#include <iostream>
#include <vector>

#include "opencv2/opencv.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/face_reconstruction/base_extractor.hpp"
#include "lts5/face_reconstruction/base_stream.hpp"
#include "lts5/face_tracker/mstream_face_tracker.hpp"

#ifdef HAS_CUDA_
#include "lts5/cuda/utils/matrix.hpp"
#endif


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  LandmarkExtractor
 *  @brief  Apply preprocess algorithm on frames (cv::Mat).
 *          Run landmarks detection/tracking
 *  @author Christophe Ecabert
 *  @date   04/03/14
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS LandmarkExtractor : public BaseExtractor {

public:

#pragma mark -
#pragma mark Type defintion

  /**
   *  @name   Parameter
   *  @brief  Configuration for face tracker extractor
   */
  struct Parameter : public BaseExtractor::BaseParameter {
    /** Path to tracker configuration */
    std::string tracker_configuration_path;
    /** Path to face detector configuration */
    std::string tracker_face_detector_path;
    /** Value to use for overriding tracker reset threshold */
    std::string tracker_threshold;
    /** Tracker type */
    std::string type;
    /** Starting pass */
    int start_pass;
  };


#pragma mark -
#pragma mark Initialization
  /**
   *  @name   LandmarkExtractor
   *  @fn LandmarkExtractor(const LandmarkExtractor::Parameter& extractor_config)
   *  @brief  Constructor
   *  @param[in]  extractor_config Module configuration
   */
  explicit LandmarkExtractor(const LandmarkExtractor::Parameter& extractor_config);

  /**
   *  @name   ~LandmarkExtractor
   *  @fn ~LandmarkExtractor()
   *  @brief  Destructor
   */
  ~LandmarkExtractor();

  /**
   *  @name   Process
   *  @fn void Process(const BaseStream::Buffer& buffer)
   *  @brief  Run face tracker or load annotation from file and put result
   *          into data buffer. Estimate head  pose from landmarks if available
   *  @param[in]  buffer    Buffer holding captured image
   */
  void Process(const BaseStream::Buffer& buffer);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_frame_index
   *  @fn void set_frame_index(int frame_index)
   *  @brief  Set the index of the frame to process
   *  @param[in]  frame_index       Index of the frame
   */
  void set_frame_index(int frame_index) {
    LTS5_LOG_WARNING("Not available for the class");
  }

#pragma mark -
#pragma mark Private
 private :

/*#ifdef HAS_CUDA_
  / Multi-stream Face tracker /
  LTS5::MStreamFaceTracker<cv::cuda::GpuMat, CUDA::Matrix<double>>* face_tracker_;
  / Landmarks /
  std::vector<CUDA::Matrix<double>> dbl_landmarks_;
#else*/
  /** Multi-stream Face tracker */
  LTS5::MStreamFaceTracker<cv::Mat, cv::Mat>* face_tracker_;
  /** Landmarks */
  std::vector<cv::Mat> dbl_landmarks_;
//#endif

  /** Face bounding box */
  float face_bounding_box_[4];
  /** Extractor parameters */
  LandmarkExtractor::Parameter params_;
};
}
#endif /* __LTS5_LANDMARK_EXTRACTOR__ */
