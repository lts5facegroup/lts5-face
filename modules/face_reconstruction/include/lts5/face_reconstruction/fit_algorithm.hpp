/**
 *  @file   fit_algorithm.hpp
 *  @brief  Algorithm to fit 3D Morphable Model
 *  @ingroup    face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   13/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_FIT_ALGORITHM__
#define __LTS5_FIT_ALGORITHM__

#include <lts5/model.hpp>
#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/model/morphable_model.hpp"
#include "lts5/model/camera.hpp"
#include "lts5/model/perspective_projection.hpp"
#include "lts5/ogl/offscreen_renderer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   MMFitAlgorithm
 * @brief   Foundation to fit 3DMM
 * @author  Christophe Ecabert
 * @date    13/09/2017
 * @ingroup face_reconstruction
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS MMFitAlgorithm {
 public:

#pragma mark -
#pragma mark Type Definitions
  using Mat4 = Matrix4x4<T>;
  /** Mesh Type */
  using Mesh = typename PCAModel<T>::Mesh;
  /** Camera */
  using Camera_t = Camera<T, PerspectiveProjection>;
  /** Vertex */
  using Vertex = typename Mesh::Vertex;
  /** ImagePoint */
  using ImagePoint = typename PCAModel<T>::ImagePoint;


  /**
   * @struct    Regularization
   * @brief     Regularization factor
   */
  struct Regularization {
    /** Data Weight */
    T data;
    /** Shape Regularizer */
    T shape;
    /** Texture Regularizer */
    T tex;
    /** Shape Regularizer */
    T landmark;

    /**
     * @name    Regularization
     * @fn  Regularization()
     * @brief Constructor with default value
     */
    Regularization() {
      data = T(1.0);
      shape = T(1.0);
      tex = T(1.0);
      landmark = T(1.0);
    }
  };

  /**
   * @struct    Stat
   * @brief Fit statistics
   */
  struct Stat {
    /** Overall cost */
    std::vector<T> cost;
    /** Data Term */
    std::vector<T> data;
    /** Shape prior Term */
    std::vector<T> shape_prior;
    /** Texture prior Term */
    std::vector<T> tex_prior;
    /** Landmarks Term */
    std::vector<T> landmarks;

    /**
     * @name    Clear
     * @fn  void Clear()
     * @brief   Reset structure
     */
    void Clear();


    /**
     * @name    ToMat
     * @fn  void ToMat(cv::Mat* m) const
     * @brief Export data into Mat container
     * @param[out] m    Matrix where to place data
     */
    void ToMat(cv::Mat* m) const;

    /**
     * @name    Print
     * @fn  void Print(std::ostream& stream)
     * @brief   Dump structure into a given stream
     * @param[in] stream    Stream where to dump data
     */
    void Print(std::ostream& stream) const;
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name  MMFitAlgorithm
   * @fn    explicit MMFitAlgorithm(const MorphableModel<T>* model)
   * @brief Constructor
   * @param[in] model       Model to fit
   */
  explicit MMFitAlgorithm(const MorphableModel<T>* model);

  /**
   * @name  ~MMFitAlgorithm
   * @fn    virtual ~MMFitAlgorithm()
   * @brief Destructor
   */
  virtual ~MMFitAlgorithm();

#pragma mark -
#pragma mark Usage

  /**
   * @name  Process
   * @fn    virtual int Process(const cv::Mat& image,
                                const cv::Mat& landmarks,
                                const cv::Mat& shape_p,
                                const cv::Mat& tex_p,
                                const cv::Mat& illu_p,
                                const Regularization& reg,
                                Mesh* mesh,
                                Camera* camera,
                                Stat* stat) = 0
   * @brief Estimate the model parameters
   * @param[in] image       Image to fit
   * @param[in] landmarks   Facial landmarks (optional)
   * @param[in] shape_p     Initial shape parameters, can be left empty
   * @param[in] tex_p       Initial texture parameters, can be left empty
   * @param[in] illu_p      Illumination parameters, can be left empty
   * @param[in] reg         Regularization factor
   * @param[in, out] mesh   Initial mesh (can be meanshape / texture)
   * @param[in, out] camera Initial camera estimation
   * @param[out] stat       Algorithm statistics (can be nullptr)
   * @return    -1 if not converged, 0 otherwise
   */
  virtual int Process(const cv::Mat& image,
                      const cv::Mat& landmarks,
                      const cv::Mat& shape_p,
                      const cv::Mat& tex_p,
                      const cv::Mat& illu_p,
                      const Regularization& reg,
                      Mesh* mesh,
                      Camera_t* camera,
                      Stat* stat) = 0;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_shape_parameter
   * @fn    const cv::Mat& get_shape_parameter() const
   * @brief Provide estimated shape parameters
   * @return    Shape parameters
   */
  const cv::Mat& get_shape_parameter() const {
    return shape_p_;
  }

  /**
   * @name  get_texture_parameter
   * @fn    const cv::Mat& get_texture_parameter() const
   * @brief Provide estimated texture parameters
   * @return    Texture parameters
   */
  const cv::Mat& get_texture_parameter() const {
    return tex_p_;
  }

  /**
   * @name  get_illumination_parameter
   * @fn    const cv::Mat& get_illumination_parameter() const
   * @brief Provide estimated illumination parameters
   * @return    Illumination parameters
   */
  const cv::Mat& get_illumination_parameter() const {
    return illu_p_;
  }

  /**
   * @name  set_number_sample
   * @fn    void set_number_sample(const size_t n_sample)
   * @brief Set the number of sample to draw from the texture (stochastic fit)
   * @param n_sample    Number of sample to draw/pick
   */
  void set_number_sample(const size_t n_sample) {
    n_sample_ = n_sample;
  }

  /**
   * @name  set_convergence_eps
   * @fn    void set_convergence_eps(const T eps)
   * @brief Set convergence criterion
   * @param eps Converge criterion
   */
  void set_convergence_eps(const T eps) {
    tol_ = eps;
  }

  /**
   * @name  get_active_shape_component
   * @fn    int get_active_shape_component() const
   * @brief Provide the number of principal component in the shape model
   * @return    Active component
   */
  int get_active_shape_component() const {
    return model_->get_active_shape_component();
  }

  /**
   * @name  get_active_texture_component
   * @fn    int get_active_texture_component() const
   * @brief Provide the number of principal component in the texture model
   * @return    Active component
   */
  int get_active_texture_component() const {
    return model_->get_active_texture_component();
  }

  /**
   * @name  get_n_vertex
   * @fn    size_t get_n_vertex() const
   * @brief Provide number of vertices in the shape model
   * @return number of vertices
   */
  size_t get_n_vertex() const {
    return model_->get_n_vertex();
  }

  /**
   * @name  get_n_texel
   * @fn    size_t get_n_texel() const
   * @brief Provide number of texel in the texture model
   * @return number of texel
   */
  size_t get_n_texel() const {
    return model_->get_n_texel();
  }

  /**
   * @name  get_n_channels
   * @fn    int get_n_channels() const
   * @brief Indicate how many channels are used in texture model
   * @return    Amount of channels
   */
  int get_n_channels() const {
    return model_->get_n_channels();
  }

  /**
   * @name  set_maximum_iteration
   * @fn    void set_maximum_iteration(const int max_iter)
   * @brief Set the number maximum of iteration
   * @param[in] max_iter    Maximum number of iteration
   */
  void set_maximum_iteration(const int max_iter) {
    max_iter_ = max_iter;
  }

#pragma mark -
#pragma mark Protected
 protected:

  /**
   * @name  SampleVisiblePoint
   * @fn    int SampleVisiblePoint(const Mesh& instance,
                         const Camera_t& camera,
                         std::vector<ImagePoint>* pts)
   * @brief Sample visible points and compute coordinate in image plane and
   *        corresponding barycentric coordinates
   * @param[in] instance    Current instance of the model
   * @param[in] camera      Current estimation of camera
   * @param[out] pts    Selected visible points
   * @return -1 if error, 0 otherwise
   */
  int SampleVisiblePoint(const Mesh& instance,
                         const Camera_t& camera,
                         std::vector<ImagePoint>* pts);

  /**
   * @name  RenderInstance
   * @fn    int RenderInstance(const Mesh& instance,
                               const Camera_t& camera, cv::Mat* image)
   * @brief Render an image of a given model instance
   * @param[in] instance    Model instance
   * @param[in] camera      Camera estimation
   * @param[out] image      Rendered image
   * @return    -1 if error, 0 otherwise
   */
  int RenderInstance(const Mesh& instance,
                     const Camera_t& camera,
                     cv::Mat* image);

  /**
   * @name  ImageGradient
   * @fn    void ImageGradient(const cv::Mat& image,
                                cv::Mat* grad_x,
                                cv::Mat* grad_y) const
   * @brief Compute image gradient in X/Y direction
   * @param[in] image   Image
   * @param[out] grad_x Image's gradient in X direction
   * @param[out] grad_y Image's gradient in Y direction
   */
  void ImageGradient(const cv::Mat& image,
                     cv::Mat* grad_x,
                     cv::Mat* grad_y) const;

  /**
   * @name  SampleImage
   * @fn    void SampleImage(const cv::Mat& image,
                   const std::vector<ImagePoint>& pts,
                   const bool is_grad,
                   cv::Mat* sample) const
   * @brief Sample a given image at locations \p pts.
   * @param[in] image   Image to sample
   * @param[in] pts     List of sampling point
   * @param[in] is_grad Indicate if image is gradient or not
   * @param[out] sample Sampled elements
   */
  void SampleImage(const cv::Mat& image,
                   const std::vector<ImagePoint>& pts,
                   const bool& is_grad,
                   cv::Mat* sample) const;

  /**
   * @name  Cost
   * @fn    T Cost(const cv::Mat& shape_w,
                  const cv::Mat& tex_w,
                  const cv::Mat& data_e,
                  const cv::Mat& lms_e,
                  const T shape_prior_reg,
                  const T tex_prior_reg,
                  const T lms_prior_reg)
   * @brief Compute current cost
   * @param[in] shape_w         Shape parameters
   * @param[in] tex_w           Texture parameters
   * @param[in] data_e          Data error term
   * @param[in] lms_e           Landmarks error term
   * @param[in] shape_prior_reg Shape prior regularizer
   * @param[in] tex_prior_reg   Texture prior regularizer
   * @param[in] lms_prior_reg   Landmarks prior regularizer
   * @return Current cost
   */
  T Cost(const cv::Mat& shape_w,
            const cv::Mat& tex_w,
            const cv::Mat& data_e,
            const cv::Mat& lms_e,
            const T& shape_prior_reg,
            const T& tex_prior_reg,
            const T& lms_prior_reg);

  /**
   * @name  JacobianLandmark
   * @fn    void JacobianLandmark(const Camera_t& camera,
                                  const cv::Mat& shape,
                                  const cv::Mat& var,
                                  cv::Mat* jacob_lms)
   * @brief Compute the Jacobian for the landmarks transformation
   * @param[in] camera      Camera estimation
   * @param[in] shape       3D shape
   * @param[in] var         Shape variation
   * @param[out] jacob_lms  Landmarks jacobian [2L x (ns + nc + nt)]
   */
  void JacobianLandmark(const Camera_t& camera,
                        const cv::Mat& shape,
                        const cv::Mat& var,
                        cv::Mat* jacob_lms);

  /**
   * @name  DcamDshapeParam
   * @fn    void DcamDshapeParam(const Camera_t& camera,
                       const cv::Mat& shape,
                       const cv::Mat& var,
                       cv::Mat* dcam_dsp)
   * @brief Compute the derivative of the perspective projection with respect
   *        to the shape parameters
   * @param[in] camera  Camera estimation
   * @param[in] shape   3D shape
   * @param[in] var     Shape model variance
   * @param[out] dcam_dsp   Derivative (2N x ns)
   */
  void DcamDshapeParam(const Camera_t& camera,
                       const cv::Mat& shape,
                       const cv::Mat& var,
                       cv::Mat* dcam_dsp);

  /**
   * @name  DcamDcamParam
   * @fn    void DcamDcamParam(const Camera_t& camera,
                     const cv::Mat& shape,
                     cv::Mat* dcam_dcamp)
   * @brief Compute the derivative of the perspective projection with respect
   *        to the camera parameters
   * @param[in] camera  Camera estimation
   * @param[in] shape   3D shape
   * @param[out] dcam_dcamp   Derivative (2N x nc)
   */
  void DcamDcamParam(const Camera_t& camera,
                     const cv::Mat& shape,
                     cv::Mat* dcam_dcamp);


  /** Model */
  const MorphableModel<T>* model_;
  /** Shape parameters */
  cv::Mat shape_p_;
  /** Texture parameters */
  cv::Mat tex_p_;
  /** Illumination parameters [3 x 9]*/
  cv::Mat illu_p_;
  /** Mesh formatted for interpolation */
  Mesh* mesh_;
  /** Offscreen renderer - interpolator */
  OGLOffscreenRenderer<T, Vector3<T>>* interpolator_;
  /** Offscreen renderer - image */
  OGLOffscreenRenderer<T, Vector3<T>>* renderer_;
  /** Shader - interpolator */
  OGLProgram* shader_interp_;
  /** Shader */
  OGLProgram* shader_;
  /** Number of sample */
  size_t n_sample_;
  /** Convergence tol */
  T tol_;
  /** Jacobian shape prior */
  cv::Mat j_shape_prior_;
  /** Shape Principal Component for landmarks */
  cv::Mat shape_pc_lms_;
  /** Shape Scaled Principal Component for landmarks */
  cv::Mat shape_scaled_pc_lms_;
  /** Landmarks indexes */
  std::vector<int> lms_index_;
  /** Jacobian texture prior */
  cv::Mat j_tex_prior_;
  /** Maximum iteration */
  int max_iter_;

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  Init
   * @fn    void Init()
   * @brief Initialize some internal states
   */
  void Init();

  /**
   * @name  ConvertMesh
   * @fn    void ConvertMesh(const Mesh& instance)
   * @brief Convert an instance into a mesh with one vertex for each triangle
   *        corner (i.e. no shared vertex)
   * @param[in] instance
   */
  void ConvertMesh(const Mesh& instance);

  /**
   * @name  InitInterpolator
   * @fn    int InitInterpolator(const Mesh& instance,
                       const Camera_t& camera)
   * @brief Initialize OpenGL interpolator (Lazy init)
   * @param[in] instance    Current instance of the model
   * @param[in] camera      Current estimation of camera
   * @return    @return -1 if error, 0 otherwise
   */
  int InitInterpolator(const Mesh& instance,
                       const Camera_t& camera);

  /**
   * @name  InitRenderer
   * @fn    int InitRenderer(const Mesh& instance,
                             const Camera_t& camera)
   * @brief Initialize OpenGL renderer (Lazy init)
   * @param[in] instance    Current instance of the model
   * @param[in] camera      Current estimation of camera
   * @return    @return -1 if error, 0 otherwise
   */
  int InitRenderer(const Mesh& instance, const Camera_t& camera);

  /**
   * @name  ComputeOGLCameraTransform
   * @fn    Mat4 ComputeOGLCameraTransform(const Camera_t& camera,
                                                const T near,
                                                const T far)
   * @brief Define OpenGL transformation for the current camera estimation
   * @see http://ksimek.github.io/2013/06/03/calibrated_cameras_in_opengl/
   * @param[in] camera  Current camera estimation
   * @param[in] near    Near plane
   * @param[in] far     Far plane
   * @return    OpenGL transform
   */
  Mat4 ComputeOGLCameraTransform(const Camera_t& camera,
                                 const T& near,
                                 const T& far);

};


/**
 * @class   MMFitAlgorithmProxy
 * @brief   Registration mechanism for morphable model fit algorithm
 * @author  Christophe Ecabert
 * @date    21/09/2017
 * @tparam T Data type
 */
template<typename T>
class LTS5_EXPORTS MMFitAlgorithmProxy {
 public:

  /**
   * @name  MMFitAlgorithmProxy
   * @fn    MMFitAlgorithmProxy()
   * @brief Constructor
   */
  MMFitAlgorithmProxy();

  /**
   * @name  ~MMFitAlgorithmProxy
   * @fn    virtual ~MMFitAlgorithmProxy() = default
   * @brief Destructor
   */
  virtual ~MMFitAlgorithmProxy() = default;

  /**
   * @name  Create
   * @fn    virtual MMFitAlgorithm<T>* Create(const MorphableModel<T>* model) const = 0
   * @brief Instantiate a specific instance for a given proxy
   * @param[in] model   Morphable model to use for fitting
   * @return    Fitting algorithm
   */
  virtual MMFitAlgorithm<T>* Create(const MorphableModel<T>* model) const = 0;

  /**
   * @name  Name
   * @fn    virtual const char* Name() const = 0
   * @brief Indicate the name of that proxy
   * @return    Fit algorithm's name
   */
  virtual const char* Name() const = 0;
};

}  // namepsace LTS5
#endif //__LTS5_FIT_ALGORITHM__
