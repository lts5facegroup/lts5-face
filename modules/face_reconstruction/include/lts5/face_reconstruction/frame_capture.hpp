/**
 *  @file   frame_capture.hpp
 *  @brief  Frame acquisition module
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 04/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_FRAME_CAPTURE__
#define __LTS5_FRAME_CAPTURE__

#include <iostream>
#include <string>
#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  FrameCapture
 *  @brief  Image acquisition module. Supported type : [Camera, images, video, NoData]
 *  @author Christophe Ecabert
 *  @date   04/03/14
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS FrameCapture {
 public :
  /**
   *  @enum   FrameCaptureInputTypeEnum
   *  @brief  List of possible input types
   */
  enum FrameCaptureInputTypeEnum {
    /** Image source are PointGrey camera */
    kPointGrey = 0,
    /** Image source are images */
    kImage = 1,
    /** Image source are videos */
    kVideo = 2
  };

  /**
   *  @struct FrameCaptureParameter
   *  @brief  List of parameters used to construct FrameCapture object
   */
  struct FrameCaptureParameter {
    /** Input types : Camera,images,videos */
    FrameCaptureInputTypeEnum   input_type;
    /** Array holding indexes where cameras can be found */
    int* device_index = nullptr;
    /** Where images or videos are stored */
    std::string input_folder;
    /** Folder where images are stored */
    std::vector<std::string>  image_folder;
    /** Images/Videos extension types */
    std::string extension_type;
    /** Defines how many views are used in input */
    int number_of_view;
    /** Frame width */
    int width;
    /** Frame height */
    int height;

    /**
     *  @name   ~FrameCaptureParameter
     *  @fn ~FrameCaptureParameter(void)
     *  @brief  Destructor
     */
    ~FrameCaptureParameter(void) {
      if(device_index) {
        delete [] device_index;
      }
    }
  };

 private :

  /** Input types */
  FrameCaptureInputTypeEnum input_type_;
  /** Flag to indicates module's init state  */
  bool initialized_;
  /** Number of view per frame */
  int number_of_view_;
  /** Number of frame to treat */
  int number_max_frame_;
  /** How many frames have been loaded */
  int number_of_frame_loaded_;
  /** Frame width */
  int frame_width_;
  /** Frame height */
  int frame_height_;
  /** View buffer where data are stored */
  std::vector<cv::Mat> image_buffer_;
  /** Array storing all the VideoCapture element used to capture from video/camera */
  std::vector<cv::VideoCapture> capture_device_list_;
  /** List of file scanned from system */
  std::vector<std::string> file_list_;
  /** Data root folder */
  std::string data_root_folder_;

  /**
   *  @name   FrameCaptureCamera
   *  @fn void FrameCaptureCamera(const FrameCaptureParameter& configuration)
   *  @brief  Constructor helper for camera input
   *  @param[in]  configuration Capture parameters
   */
  void FrameCaptureCamera(const FrameCaptureParameter& configuration);

  /**
   *  @name   FrameCaptureImage
   *  @fn void FrameCaptureImage(const FrameCaptureParameter& configuration)
   *  @brief  Constructor helper for images input
   *  @param[in]  configuration Capture parameters
   */
  void FrameCaptureImage(const FrameCaptureParameter& configuration);

  /**
   *  @name   FrameCaptureVideo
   *  @fn void FrameCaptureVideo(const FrameCaptureParameter& configuration)
   *  @brief  Constructor helper for videos input
   *  @param[in]  configuration Capture parameters
   */
  void FrameCaptureVideo(const FrameCaptureParameter& configuration);

 public :
  /**
   *  @name   FrameCapture
   *  @fn FrameCapture(const FrameCaptureParameter& configuration)
   *  @brief  Base constructor
   *  @param[in]  configuration     Structure that's hold all configuration information.
   */
  explicit FrameCapture(const FrameCaptureParameter& configuration);

  /**
   *  @name   ~FrameCapture
   *  @fn ~FrameCapture()
   *  @brief  Destructor
   */
  ~FrameCapture();

  /**
   *  @name   CaptureFrame
   *  @fn void CaptureFrame(void)
   *  @brief  Capture nViews for current frame and push results into views buffers
   */
  void CaptureFrame(void);

  /**
   *  @name   ClearFrameCounter
   *  @fn void ClearFrameCounter(void)
   *  @brief  Reset loaded frame counter
   */
  void ClearFrameCounter(void);

#pragma mark -
#pragma mark Getters / Setters
  /**
   *  @name   set_frame_to_process
   *  @fn void set_frame_to_process(int frame_index)
   *  @brief  Set from which frame the pipeline process
   *  @param[in]  frame_index   Frame's index
   */
  void set_frame_to_process(int frame_index) {
    number_of_frame_loaded_ = frame_index < number_max_frame_ ? frame_index : number_max_frame_;
  }

  /**
   *  @name get_current_frame_index
   *  @fn int get_current_frame_index(void) const
   *  @brief  Provide how many frames have been already loaded
   *  @return Number of frame loaded
   */
  int get_current_frame_index(void) const {
    return number_of_frame_loaded_ - 1;
  }

  /**
   *  @name   get_frame
   *  @fn const std::vector<cv::Mat>& get_frame(void) const
   *  @brief  Give the reference to the location where frames are stored
   *          and the number of views
   *  @return Reference to image buffer
   */
  const std::vector<cv::Mat>& get_frame(void) const {
    return image_buffer_;
  }

  /**
   *  @name get_frame_name
   *  @fn void get_frame_name(std::string* frame_name, std::string* frame_ext) const
   *  @brief  Provide the name of the current frame according to the media
   *  @param[out] frame_name  Frame name
   *  @param[out] frame_ext   Frame extension
   */
  void get_frame_name(std::string* frame_name, std::string* frame_ext) const;

  /**
   *  @name   get_image_name
   *  @fn const std::string* get_image_name(void) const
   *  @brief  Provide the name of the last image loaded
   *  @return Image name
   */
  const std::string* get_image_name(void) const {
    return (input_type_ == kImage ?
            &file_list_[number_of_frame_loaded_-1] :
            nullptr);
  }

  /**
   *  @name   get_number_of_view
   *  @fn int get_number_of_view(void) const
   *  @brief  Indicate how many cameras are used for acquisition
   *  @return Number of cameras
   */
  int get_number_of_view(void) const {
    return number_of_view_;
  }

  /**
   *  @name   get_number_max_frame
   *  @fn int get_number_max_frame(void) const
   *  @brief  Give the number of frame to process
   *  @return Number of frame to process by the pipeline
   */
  int get_number_max_frame(void) const {
    return number_max_frame_;
  }
};
}
#endif /* __LTS5_FRAME_CAPTURE__ */
