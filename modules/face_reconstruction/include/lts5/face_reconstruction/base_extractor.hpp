/**
 *  @file   base_extractor.hpp
 *  @brief  Interface for landmark extraction from different sources
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 04/03/14
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_BASE_EXTRACTOR__
#define __LTS5_BASE_EXTRACTOR__

#include <iostream>
#include <vector>

#include "opencv2/core/core.hpp"

#include "lts5/face_reconstruction/base_stream.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BaseExtractor
 *  @brief  Preprocessing interface for face reconstruction
 *  @author Christophe Ecabert
 *  @date   24/08/15
 *  @ingroup face_reconstruction
 */
class BaseExtractor {

 public :

  /**
   *  @enum   DetectionType
   *  @brief  Possible type of detection
   */
  enum DetectionType {
    /** Run landmark detector */
    kDetector = 0,
    /** Use 2D annotation file */
    kAnnotation2D = 1,
  };

  /**
   *  @name   BaseParameter
   *  @brief  Configuration for landmarks extractor
   */
  struct BaseParameter {
    /** Type of detection */
    DetectionType detection_type;
    /** Number of view in the configuration */
    int number_of_view;
    /** Image width */
    int image_width;
    /** Image height */
    int image_height;
    /** Inversion factor for axis */
    float axis_inversion[3];
    /** Need to detect contour */
    bool use_contour = false;
    /** Canny edge detector thresholds */
    int edge_threshold[2] = {10,100};
    /** Face bounding box extra margin (percentage)*/
    float face_bbox_margin = 0.f;
  };

  /**
   *  @name   PoseEstimation
   *  @brief  Pose estimated by PDM
   */
  struct PoseEstimation {
    /** Gamma angle estimated*/
    float gamma;
    /** Theta angle estimated */
    float theta;
    /** Phi angle estimated */
    float phi;
  };

  /**
   *  @name   ExtractorData
   *  @brief  Hold all extracted data for a given extractor
   */
  struct ExtractorData {
    /** Face landmarks vector */
    std::vector<cv::Mat> landmarks;
    /** Pose estimation */
    std::vector<PoseEstimation> pose;
    /** Contour */
    std::vector<cv::Mat> contour;
    /** Face bounding box origin */
    std::vector<cv::Point2f> face_region;

    /**
     *  @name ExtractorData
     *  @fn ExtractorData(void)
     *  @brief  Constructor
     */
    ExtractorData(void) {}

    /**
     *  @name ExtractorData
     *  @fn explicit ExtractorData(const int n_view)
     *  @brief  Constructor
     *  @param[in]  n_view  Number of view included in the structure
     */
    explicit ExtractorData(const int n_view) : landmarks(n_view),
                                               pose(n_view),
                                               contour(n_view),
                                               face_region(n_view) {
    }
  };

 protected :

  /** Extractor data */
  ExtractorData data_;

 public :

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   ~BaseExtractor
   *  @fn virtual ~BaseExtractor(void)
   *  @brief  Destructor
   */
  explicit BaseExtractor(const int n_view) : data_(n_view) {
  }

  /**
   *  @name   ~BaseExtractor
   *  @fn virtual ~BaseExtractor(void)
   *  @brief  Destructor
   */
  virtual ~BaseExtractor(void) {
  }

#pragma mark -
#pragma mark Process

  /**
   *  @name   Process
   *  @fn virtual void Process(const BaseStream::Buffer& image_buffer) = 0
   *  @brief  Process image to extract information
   *  @param[in]  image_buffer  List of image to process
   */
  virtual void Process(const BaseStream::Buffer& image_buffer) = 0;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_frame_index
   *  @fn virtual void set_frame_index(int frame_index) = 0
   *  @brief  Set the index of the frame to process
   *  @param[in]  frame_index       Index of the frame
   */
  virtual void set_frame_index(int frame_index) = 0;

  /**
   *  @name   get_number_max_frame
   *  @fn virtual int get_number_max_frame(void)
   *  @brief  Give the number of frame to process
   *  @return Number of frames
   */
  virtual int get_number_max_frame(void) { return -1;};

  /**
   *  @name   get_current_frame_index
   *  @fn virtual int get_current_frame_index(void) const
   *  @brief  Return the index of which frame have been loaded
   *  @return Frame index starting at 0;
   */
  virtual int get_current_frame_index(void) const {return -1;};

  /**
   *  @name   get_face_landmarks
   *  @fn virtual const ExtractorData& get_data(void) const
   *  @brief  Give the position of the detected landmarks for each views
   *  @return List of annotations
   */
  virtual const ExtractorData& get_data(void) const {
    return data_;
  }
};
}
#endif /* __LTS5_BASE_EXTRACTOR__ */
