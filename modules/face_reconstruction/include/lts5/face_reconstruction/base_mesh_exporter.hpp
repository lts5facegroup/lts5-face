/**
 *  @file   base_mesh_exporter.hpp
 *  @brief  Interface for reconstruction exportation
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date 04/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_BASE_MESH_EXPORTER__
#define __LTS5_BASE_MESH_EXPORTER__

#include "lts5/utils/logger.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/ogl/texture.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BaseMeshExporter
 *  @brief  Basic interface for mesh exportation
 *  @author Christophe Ecabert
 *  @date   25/08/15
 *  @ingroup face_reconstruction
 */
class BaseMeshExporter {

 public :

#pragma mark -
#pragma mark Initialization

  /**
   *  @name ~BaseMeshExporter
   *  @fn virtual ~BaseMeshExporter(void)
   *  @brief  Destructor
   */
  BaseMeshExporter(void) : is_initialized_(false) {}

  /**
   *  @name ~BaseMeshExporter
   *  @fn virtual ~BaseMeshExporter(void)
   *  @brief  Destructor
   */
  virtual ~BaseMeshExporter(void) {}

  /**
   *  @name Initialize
   *  @fn void Initialize(const LTS5::Mesh<float>& mesh,
                          const cv::Mat& texture,
                          const int width,
                          const int height)
   *  @brief  Initialize converter
   *  @param[in]  mesh  Mesh to convert to image
   *  @param[in]  texture Mesh's texture provided as an image
   *  @param[in]  width   Image width
   *  @param[in]  height  Image height
   */
  virtual void Initialize(const LTS5::Mesh<float>& mesh,
                          const cv::Mat& texture,
                          const int width,
                          const int height) {
    LTS5_LOG_INFO("Fail safe, not implemented");
  }

#pragma mark -
#pragma mark Process

  /**
   * @name  ActiveContext
   * @fn    virtual void ActiveContext(void) const
   * @brief Make this context the current one
   */
  virtual void ActiveContext(void) const {}

  /**
   * @name  DisableContext
   * @fn    virtual void DisableContext(void) const
   * @brief Detach from current context
   */
  virtual void DisableContext(void) const {}

  /**
   *  @name Process
   *  @fn virtual void Process(void) = 0
   *  @brief  Export mesh according to exporter class
   */
  virtual void Process(void) = 0;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  is_initialiazed
   * @fn    virtual bool is_initialiazed(void) const
   * @brief Indicate if exported is initialized
   * @return    true if init, false otherwise
   */
  virtual bool is_initialiazed(void) const {
    return is_initialized_;
  }

  /**
   * @name  set_output_name
   * @fn    virtual void set_output_name(const std::string& output)
   * @brief Set the name to use as output
   * @param output    Output name
   */
  virtual void set_output_name(const std::string& output) {
    LTS5_LOG_INFO("Fail safe, not implemented");
  }

  /**
   * @name  set_mesh
   * @fn    virtual void set_mesh(const LTS5::Mesh<float>& mesh)
   * @brief Set what mesh need to be exported
   * @param mesh    Mesh to export
   */
  virtual void set_mesh(const LTS5::Mesh<float>& mesh) {
    LTS5_LOG_INFO("Fail safe, not implemented");
  }

  /**
   *  @name set_texture
   *  @fn virtual void set_texture(const cv::Mat& texture)
   *  @brief  Set texture of the object
   *  @param[in]  texture Texture object
   */
  virtual void set_texture(const cv::Mat& texture) {
    LTS5_LOG_INFO("Fail safe, not implemented");
  }

  /**
   *  @name get_image
   *  @fn virtual cv::Mat get_image(void) const
   *  @brief  Provide access to the converted mesh to image
   *  @return Image
   */
  virtual cv::Mat get_image(void) const {
    LTS5_LOG_INFO("Fail safe, not implemented");
    return cv::Mat();
  }

  /**
   * @name  get_texture
   * @fn    virtual const OGLTexture* get_texture(void) const
   * @brief Provide access to opengl texture object
   * @return    OGLTexture object
   */
  virtual const OGLTexture* get_texture(void) const {
    LTS5_LOG_INFO("Fail safe, not implemented");
    return nullptr;
  }

#pragma mark -
#pragma mark Protected
 protected:
  /** Indicate if module is initialized or not */
  bool is_initialized_;
};

}  // namespace LTS5
#endif  /* __LTS5_BASE_MESH_EXPORTER__ */
