/**
 *  @file   annotation_2d_extractor.hpp
 *  @brief  Extract 2D Annotation from file
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   04/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_ANNOTATION_2D_EXTRACTOR__
#define __LTS5_ANNOTATION_2D_EXTRACTOR__

#include <iostream>
#include <vector>

#include "opencv2/opencv.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_reconstruction/base_extractor.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/face_reconstruction/base_stream.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  Annotation2DExtractor
 *  @brief  Apply preprocess algorithm on frames (cv::Mat).
 *          Run landmarks detection/tracking
 *  @author Christophe Ecabert
 *  @date   04/03/14
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS Annotation2DExtractor : public BaseExtractor {
 public:

#pragma mark -
#pragma mark Type definition

  /**
   *  @name   Parameter
   *  @brief  Configuration for face tracker extractor
   */
  struct Parameter : public BaseExtractor::BaseParameter {
    /** Root folder for annotation */
    std::string annotation_root_folder;
    /** Type of extension for annotation */
    std::string annotation_extension_type;
    /** Pose estimation files */
    std::vector<std::string> pose_estimation_files;
    /** Annotation folders */
    std::vector<std::string> annotation_folders;
  };


#pragma mark -
#pragma mark Initialization
  /**
   *  @name   Annotation2DExtractor
   *  @fn Annotation2DExtractor(const Parameter& extractor_config)
   *  @brief  Constructor
   *  @param[in]  extractor_config Module configuration
   */
  explicit Annotation2DExtractor(const Parameter& extractor_config);

  /**
   *  @name   ~Annotation2DExtractor
   *  @fn ~Annotation2DExtractor()
   *  @brief  Destructor
   */
  ~Annotation2DExtractor();

  /**
   *  @name   Process
   *  @fn void Process(const BaseStream::Buffer& buffer)
   *  @brief  Run face tracker or load annotation from file and put result
   *          into landmark buffer
   *  @param[in]  buffer
   */
  void Process(const BaseStream::Buffer& buffer);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_frame_index
   *  @fn void set_frame_index(int frame_index)
   *  @brief  Set the index of the frame to process
   *  @param[in]  frame_index       Index of the frame
   */
  void set_frame_index(int frame_index);

  /**
   *  @name   get_number_max_frame
   *  @fn int get_number_max_frame(void) const
   *  @brief  Give the number of frame to process
   *  @return Number of frames
   */
  int get_number_max_frame(void) const {
    return maximum_annotation_;
  }

  /**
   *  @name   get_current_frame_index
   *  @fn int get_current_frame_index(void) const
   *  @brief  Return the index of which frame have been loaded
   *  @return Frame index starting at 0;
   */
  int get_current_frame_index(void) const {
    return annotation_loaded_;
  }

#pragma mark -
#pragma mark Private

 private :
  /** Annotation files list */
  std::vector<std::vector<std::string>> list_annotation_file_;
  /** Number of annotation file loaded */
  int annotation_loaded_;
  /** Number of annotation for the sequence */
  int maximum_annotation_;
  /** Landmarks bounding box : [Xmin Ymin Xmax Ymax] */
  float face_bbox_[4];
  /** Extractor parameters */
  Annotation2DExtractor::Parameter params_;
};
}
#endif /* __LTS5_ANNOTATION_2D_EXTRACTOR__ */
