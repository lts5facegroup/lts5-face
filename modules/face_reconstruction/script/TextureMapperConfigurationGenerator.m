function [ err ] = TextureMapperConfigurationGenerator( model_filename, blendshape_filename, ctrl_pts, output_filename )
%TEXTUREMAPPERCONFIGURATIONGENERATOR Summary of this function goes here
%   Detailed explanation goes here
err = -1;
% Load model
[MShape, NExp, AvgId] = ExtractFrom3DModel(model_filename);

figure(1)
plot3(MShape(1:3:end), MShape(2:3:end), MShape(3:3:end), 'x')
hold all
plot3(MShape(ctrl_pts+1), MShape(ctrl_pts+2), MShape(ctrl_pts+3), 'rx')
hold off

figure(2)
plot(MShape(ctrl_pts+1), MShape(ctrl_pts+2), 'x')

% Ask confirmation
str = input('Are control points correctly placed ? Y/N [Y]', 's')
if isempty(str)
    str = 'Y'
else 
    str =  upper(str(1))
end

if strcmp(str, 'Y') == 1
    % Continue, load open mouth expression
    ExpShape = ExtractExpressionFromBlendshape(blendshape_filename, 22);
    if ~isempty(ExpShape)
        % Project onto XY plane
        Pts = [ExpShape(ctrl_pts + 1) ExpShape(ctrl_pts + 2)];
        % Triangulate
        dt = delaunayTriangulation(Pts)
        
        figure(10)
        plot(Pts(:,1),Pts(:,2),'x')
        hold all
        triplot(dt)
        
        % Remove too small triangle
        tri_sel = [];
        tri_rmv = [];
        hist_a = [];
        for k = 1:size(dt,1)
            % Compute tri area
            tri = dt(k,:);
            e1 = Pts(tri(2),:) - Pts(tri(1),:);
            e2 = Pts(tri(3),:) - Pts(tri(1),:);
            e1 = [e1 0];
            e2 = [e2 0];
            tri_n = cross(e1,e2);
            if tri_n(2) < 0
               a = 0; 
            end
            area = norm(cross(e1,e2));
            hist_a = [hist_a area];
            if area < 0.005
               % Ask for removal
               h = triplot(tri, Pts(:,1), Pts(:,2), 'Color', 'red');
               is_correct = 0;
               while ~is_correct
                   str = input('Keep triangle ? Y/N [Y]', 's');
                   if isempty(str)
                       str = 'Y';
                       is_correct = 1;
                   else
                       str = upper(str(1));
                       if strcmp(str,'N') == 1
                           is_correct = 1;
                       end
                   end
               end
               % Add to list
               if strcmp(str, 'Y') == 1
                   tri_sel = [tri_sel ; tri];
               end 
               % clear plot
               delete(h);
            else
            	tri_sel = [tri_sel ; tri];
            end
        end
        hold off
        a = 0;
    end
    
    % Done, write config file
    [pathstr,name,ext] = fileparts(output_filename) 
    if isempty(ext) || strcmp(ext, '.cfg') == 0
        ext = '.cfg';       
    end
    filename = '';
    if isempty(pathstr)
        filename = [name ext];
    else 
        filename = [pathstr '/' name ext];
    end
    
    fid = fopen(filename, 'w');
    if fid > 0
        fprintf(fid, '# Control points\n');
        fprintf(fid, '%d\n', ctrl_pts./3);
        fprintf(fid, '# Triangulation\n');
        tri_sel = transpose(tri_sel - 1);
        fprintf(fid, '%d %d %d\n', tri_sel(:));
        fclose(fid);
        fprintf('Save configuration into file : %s\n', output_filename);
        err = 0;
    else
        fprintf('Unable to open file : %s\n', output_filename);
    end
else
    fprintf('Edit CtrlPts to suit your need.\n');
end
end

