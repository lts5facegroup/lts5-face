function [ Shape ] = ExtractExpressionFromBlendshape( filename, shape_number)
%EXTRACTEXPRESSIONFROMBLENDSHAPE Summary of this function goes here
%   Detailed explanation goes here
Shape = [];
fid = fopen(filename, 'r');
if fid > 0
    % Read dimension
    n_shape = fread(fid, 1, 'int32');
    n_vertex = fread(fid, 1, 'int32');
    n_face = fread(fid, 1, 'int32');
    % Compute offset
    n = 0;
    if shape_number >= n_shape 
        n = n_shape
    else
        n = shape_number;
    end
    offset = n_vertex * 3 * n * 4;
    fseek(fid, offset, 'cof');
    % Load shape
    Shape = fread(fid, n_vertex * 3, 'float32');
    % Done
    fclose(fid);
else
   fprintf('Unable to open file : %s', filename); 
end
end

