function [ MShape, NExp, AvgId ] = ExtractFrom3DModel( filename )
%EXTRACTFROM3DMODEL Summary of this function goes here
%   Detailed explanation goes here
MShape = [];
NExp = [];
AvgId = [];

% Open model file
fid = fopen(filename, 'r');
if fid ~= -1
    % Read model dim
    n_vertex = fread(fid, 1, 'int32');
    n_id = fread(fid, 1, 'int32');
    n_exp = fread(fid, 1, 'int32');
    offset = n_vertex * n_id * n_exp * 4;
    fseek(fid, offset, 'cof');
    % Read meanshape dim
    mshape_size = fread(fid,1,'int32');
    MShape = fread(fid, [mshape_size 1], 'float32');
    % Read neutral expression dim
    nexp_size = fread(fid,1,'int32');
    NExp = fread(fid, [nexp_size 1], 'float32');
    % Read average id dim
    avgid_size = fread(fid,1,'int32');
    AvgId = fread(fid, [avgid_size 1], 'float32');
    fclose(fid);
else
   fprintf('Unable to open file : %s\n',filename); 
end
    

end

