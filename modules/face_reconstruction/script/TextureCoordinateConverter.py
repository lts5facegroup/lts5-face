""" Texture coordinate converter """
import argparse
__author__ = 'Christophe Ecabert'


class Mesh:
  """ Simple mesh container. Can load only .obj file """

  def __init__(self, filename=None):
    """
    Constructor
    :param filename:  Path to the mesh
    """
    self.vertex = []
    self.tcoord = []
    self.triangle = []
    self.tcoord_index = []
    self.reorder_tcood = []
    if filename is not None:
      self.load(filename=filename)

  def load(self, filename):
    """
    Load an obj file
    :param filename:  Path to the mesh
    """
    self.vertex = []
    self.tcoord = []
    self.triangle = []
    self.tcoord_index = []
    with open(filename, 'r') as file:
      for line in file:
        # check first char to know the type
        elem_type = line[:2]
        if elem_type == 'v ':
          part = map(float, line[2:].strip('\n').split(' '))
          self.vertex.append(part)
        elif elem_type == 'vt':
          part = map(float, line[3:].strip('\n').split(' '))
          self.tcoord.append(part)
        elif elem_type == 'f ':
          grp = line[2:].strip('\n').split(' ')
          part_tri = []
          part_tex = []
          for g in grp:
            p = g.split('/')
            part_tri.append(int(p[0]))
            part_tex.append(int(p[1]))

          self.triangle.append(part_tri)
          self.tcoord_index.append(part_tex)

  # Change texture coordinate order to match vertex one
  def reorder_texture_coordinate(self):
    """ Reorder texture coordinate """
    self.reorder_tcood = [None] * len(self.tcoord)
    for i in range(0, len(self.triangle)):
      tri = self.triangle[i]
      tc_tri = self.tcoord_index[i]
      for t, tc in zip(tri, tc_tri):
        v_idx = t - 1
        tc_idx = tc - 1
        # Check if already assigned
        if self.reorder_tcood[v_idx] is None:
          # No assign
          self.reorder_tcood[v_idx] = self.tcoord[tc_idx]
        else:
          # check if value are same
          set_reorder = set(self.reorder_tcood[v_idx])
          set_init = set(self.tcoord[tc_idx])
          if set_reorder != set_init:
            print('Error')

  # Export reordered tex coordinate
  def export(self, filename):
    if self.reorder_tcood:
      with open(filename, 'w') as file:
        for tc in self.reorder_tcood:
          s = str(tc[0]) + ' ' + str(tc[1]) + '\n'
          file.write(s)
      print('Exported into file : ' + filename)
    else:
      print('Nothing to export')


if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Texture coordinate converter')
  parser.add_argument('-m',
                      nargs=1,
                      dest='mesh',
                      help='Path to .obj file',
                      required=True)
  parser.add_argument('-o',
                      nargs=1,
                      dest='outfile',
                      help='Name of the exported file',
                      required=True)

  args = parser.parse_args()

  # Texture coordinate reordering
  mesh = Mesh(args.mesh[0])
  mesh.reorder_texture_coordinate()
  mesh.export(args.outfile[0])
