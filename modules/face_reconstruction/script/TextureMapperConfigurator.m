clear all;
clc
close all

% Zero based control points indexes
% First 68 indexes correspond to face tracker landmarks
CtrlPts = [3800
3615
3655
3927
4008
1838
1865
1524
1768
5570
5905
5929
5881
7918
7710
7658
7739
2457
2589
2633
2631
2614
6662
6667
6672
6596
6513
2516
1016
1035
996
1251
1255
1264
5320
5225
2732
2708
2702
2446
2741
2752
6777
6741
6744
6040
6769
6718
496
626
569
4662
4695
4740
4649
4790
4731
4663
605
551
492
578
4623
4817
4880
4612
4603
613
2948
327
289
82
112
8254
196
2835
2829
2822
4399];

% DO NOT EDIT FROM HERE
CtrlPts = CtrlPts .* 3;

% Call generator
model_path = '../../../Data/BilinearFaceModel/v1.3/BilinearFaceModel_ocv.raw';
blendshape_path = '../../../Data/FaceWarehouse/Tester_001/Blendshape/shape_subset_tri.bs';
config_name = '../../../Data/texture_mapper_config'
e = TextureMapperConfigurationGenerator(model_path, blendshape_path, CtrlPts, config_name);




