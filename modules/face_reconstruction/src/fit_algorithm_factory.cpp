/**
 *  @file   fit_algorithm_factory.cpp
 *  @brief
 *  @ingroup
 *
 *  @author Christophe Ecabert
 *  @date   21/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

 #include "lts5/face_reconstruction/fit_algorithm_factory.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  Get
 * @fn    static MMFitAlgorithmFactory<T>& Get(void)
 * @brief Provide single factory instance
 * @return    Factory instance
 */
template<typename T>
MMFitAlgorithmFactory<T>& MMFitAlgorithmFactory<T>::Get(void) {
  static MMFitAlgorithmFactory<T> factory;
  return factory;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name CreateByName
 *  @fn MMFitAlgorithm<T>* CreateByName(const std::string& name,
                                const MorphableModel<T>* model)
 *  @brief  Create a spcific fitting algorithm instance
 *  @param[in]  name  Algorithm name
 *  @return Pointer to a fitting instance, or nullptr if name is not known by
 *            the factory
 */
template<typename T>
MMFitAlgorithm<T>*
MMFitAlgorithmFactory<T>::CreateByName(const std::string& name,
                                       const MorphableModel<T>* model) {
  MMFitAlgorithm<T>* algo = nullptr;
  for (const auto* p : proxies_) {
    // Look for corresponding proxy
    if (strcmp(name.c_str(), p->Name()) == 0) {
      algo = p->Create(model);
      break;
    }
  }
  return algo;
}

/*
 *  @name Register
 *  @fn void Register(const MMFitAlgorithmProxy<T>* algo)
 *  @brief  Register a type of image with a given proxy.
 *  @param[in]  algo  Algorithm to register
 */
template<typename T>
void MMFitAlgorithmFactory<T>::Register(const MMFitAlgorithmProxy<T>* algo) {
  proxies_.push_back(algo);
}

/*
 * @name  Registered
 * @fn    void Registered(std::vector<std::string>* names) const
 * @brief Provide a list of all registered proxy within this factory
 * @param[out] name   List of registered algorithm
 */
template<typename T>
void MMFitAlgorithmFactory<T>::Registered(std::vector<std::string>* names) const {
  names->resize(proxies_.size());
  for (size_t i = 0; i < proxies_.size(); ++i) {
    names->at(i) = std::string(proxies_[i]->Name());
  }
}

#pragma mark -
#pragma mark Explicit Instantiation

/** Factory - Float */
template class MMFitAlgorithmFactory<float>;
/** Factory - Double */
template class MMFitAlgorithmFactory<double>;

}  // namepsace LTS5
