/**
 *  @file   data_generator.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 09/05/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <fstream>

#include "tinyxml2.h"

#include "lts5/utils/logger.hpp"
#include "lts5/face_reconstruction/data_generator.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/face_reconstruction/utilities.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/utils/file_io.hpp"

#pragma mark -
#pragma mark Initialization

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name DataGenerator
 *  @fn explicit DataGenerator(const std::string& config_file);
 *  @brief  Constructor
 *  @param[in]  config_file Path to the config file
 */
DataGenerator::DataGenerator(const std::string& config_file) {
  // Flag
  initialized_ = false;
  number_of_view_ = 0;
  // Create mesh
  mesh_ = new LTS5::Mesh<float>();
  // Parse config file
  tinyxml2::XMLDocument reader;
  if (reader.LoadFile(config_file.c_str()) == tinyxml2::XMLError::XML_NO_ERROR) {
    const tinyxml2::XMLElement* root = reader.RootElement();
    const tinyxml2::XMLElement* node;
    const tinyxml2::XMLElement* prop;
    // Get working dir
    std::string work_dir = "";
    size_t p = config_file.rfind("/");
    if (p != std::string::npos) {
      work_dir = config_file.substr(0, p + 1);
    }

    // Get camera info
    node = root->FirstChildElement("FRAME_CAPTURE");
    prop = node->FirstChildElement("NView");
    prop->QueryIntText(&number_of_view_);
    prop = prop->NextSiblingElement();
    for (int i = 0; i < number_of_view_; ++i) {
      // Load camera
      std::string calib = prop->Attribute("CalibFile");
      if (calib != "") {
        camera_setup_.push_back(new VirtualCamera(work_dir + calib));
      }
      prop = prop->NextSiblingElement();
    }
    // Get generator prop
    node = root->FirstChildElement("DATA_GENERATOR");
    prop = node->FirstChildElement();
    // Mesh
    std::string tri = prop->GetText();
    if (mesh_->Load(work_dir + tri)) {
      throw ProcessError(ProcessError::ProcessErrorEnum::kErrOpeningFile,
                         "Unable to load triangulation file : " + tri,
                         FUNC_NAME);
    }
    has_connectivity_ = false;
    // blendshape path
    prop = prop->NextSiblingElement();
    std::string shape_root = prop->Attribute("Root");
    std::string shape_ext = prop->Attribute("Ext");
    LTS5::GetFileListing(blendshape_list_,
                         work_dir + shape_root,
                         "",
                         shape_ext);
    if (!blendshape_list_.size()) {
      throw ProcessError(ProcessError::ProcessErrorEnum::kErr,
                         "No blendshape founded at : " + shape_root,
                         FUNC_NAME);
    } else {
      max_identity_ = static_cast<int>(blendshape_list_.size());
    }
    // Landmark selection
    prop = prop->NextSiblingElement();
    std::string landmark_file = prop->GetText();
    if (this->LoadLandmarkVerticesIndex(work_dir + landmark_file)) {
      throw ProcessError(ProcessError::ProcessErrorEnum::kErr,
                         "Unable to open file : " + landmark_file,
                         FUNC_NAME);
    }
    // Done
    initialized_ = true;
  } else {
    throw ProcessError(ProcessError::ProcessErrorEnum::kErrOpeningFile,
                       "Unable to open file : " + config_file,
                       FUNC_NAME);
  }
}

/*
 *  @name   ~DataGenerator
 *  @brief  Destructor
 */
DataGenerator::~DataGenerator(void) {
  if (mesh_) {
    delete mesh_;
    mesh_ = nullptr;
  }
  for(auto& cam : camera_setup_) {
    delete cam;
    cam = nullptr;
  }
}

/*
 *  @name   LoadLandmarkVerticesIndex
 *  @brief  Load the vertex indexes corresponding to the selected landmarks from config files
 *  @param  landmark_selection_file     Configuration file
 *  @return 0 if everything went fine otherwise -1.
 */
int DataGenerator::LoadLandmarkVerticesIndex(const std::string& landmark_selection_file) {
  std::ifstream input_stream(landmark_selection_file.c_str());
  std::string line;
  int index = 0;

  if (input_stream.is_open()) {
    // File open
    std::getline(input_stream, line);

    while (!input_stream.eof()) {
      // Scan line
      sscanf(line.c_str(), "%d",&index);
      index_features_.push_back(index);
      // Read next line
      std::getline(input_stream, line);
    }
  } else {
    // Error
    return LTS5::ProcessError::ProcessErrorEnum::kErrOpeningFile;
  }

  // Everthing ok
  number_2D_features_ = (int)index_features_.size();
  LTS5_LOG_DEBUG("Landmarks indexes loaded...");
  return LTS5::ProcessError::ProcessErrorEnum::kSuccess;
}


#pragma mark -
#pragma mark Generate Synthetic Data

/*
 *  @name   generateSyntheticData
 *  @brief  Function used to generate some synthetic data from FaceWarehouse
 *          database. It basically select one blendshape and apply a known
 *          transformation on it.
 *  @param  data_parameters      Set of parameters used for data synthesis
 */
int DataGenerator::GenerateData(const SyntheticDataParameter& data_parameters) {
  // Dimension have been set ?
  if (initialized_) {
    // Can generate data
    // Init landmark vector
    if (synthetic_landmarks_.size() != camera_setup_.size()) {
      // Reset
      number_of_view_ = static_cast<int>(camera_setup_.size());
      synthetic_landmarks_.clear();

      // Init
      for (int i = 0; i < number_of_view_; i++) {
        synthetic_landmarks_.push_back(cv::Mat(number_2D_features_* 2,
                                               1,
                                               CV_32FC1));
      }
    }

    // Keep only the one we're interested in
    std::string filename = blendshape_list_[data_parameters.id];

    // Open files
    // -------------------------------------------------------------------------
    FILE* shape_id = fopen(filename.c_str(), "rb");
    if (shape_id) {
      // Ok
      int number_polygon = 0;
      fread(&max_expression_, sizeof(int), 1, shape_id);
      fread(&number_of_vertex_, sizeof(int), 1, shape_id);
      fread(&number_polygon, sizeof(int), 1, shape_id);
      auto& vertex = mesh_->get_vertex();
      vertex.resize(number_of_vertex_);
      if (data_parameters.expr <= max_expression_) {
        // Expression index is in boundary
        // Change file selection positiion
        long int aOffset = data_parameters.expr * number_of_vertex_ * 3 *
        sizeof(float) + 3 * sizeof(int);
        if(!fseek(shape_id, aOffset, SEEK_SET)) {
          // Read data
          fread((void*)vertex.data(),
                sizeof(float),
                number_of_vertex_ * 3,
                shape_id);
          // Close file - reading is done
          fclose(shape_id);
        } else {
          // Error changing index
          fclose(shape_id);
          return LTS5::ProcessError::ProcessErrorEnum::kErr;
        }

        if (!has_connectivity_) {
          mesh_->BuildConnectivity();
          has_connectivity_ = true;
        }
        mesh_->ComputeVertexNormal();

        //  Generate synthetic data
        // ---------------------------------------------------------------------
        this->ApplyRigidTransformation(data_parameters);

        // Project points
        this->ProjectLandmark(data_parameters);
      }
    } else {
      // Error while opening file
      return LTS5::ProcessError::ProcessErrorEnum::kErrOpeningFile;
    }

    // Everthing went ok
    return LTS5::ProcessError::ProcessErrorEnum::kSuccess;
  } else {
    std::cerr << "Mesh dimension have not been set !!!" << std::endl;
    // Error while opening file
    return LTS5::ProcessError::ProcessErrorEnum::kErr;
  }
}

/*
 *  @name   ApplyRigidTransformation
 *  @brief  Apply rigid transform on shape (Rotation - translation)
 *  @param  transformation_parameters    Transformation parameters
 */
void DataGenerator::ApplyRigidTransformation(const SyntheticDataParameter& transformation_parameters)
{
  // Define angles
  pose_angle_[0] = transformation_parameters.gamma * (CV_PI/180.f);
  pose_angle_[1] = transformation_parameters.theta * (CV_PI/180.f);
  pose_angle_[2] = transformation_parameters.phi * (CV_PI/180.f);

  // Define transformation
  float sin_gamma = std::sin(pose_angle_[0]);
  float sin_theta = std::sin(pose_angle_[1]);
  float sin_phi = std::sin(pose_angle_[2]);

  float cos_gamma = std::cos(pose_angle_[0]);
  float cos_theta = std::cos(pose_angle_[1]);
  float cos_phi = std::cos(pose_angle_[2]);

  // Apply transformation
  float dx = transformation_parameters.delta_x;
  float dy = transformation_parameters.delta_y;
  float dz = transformation_parameters.delta_z;

  // Rotation - translation
  std::vector<Vector3<float>>& vert = mesh_->get_vertex();
  float x=0,y=0,z=0;
  float scale = transformation_parameters.scale;
  for (int i = 0; i < number_of_vertex_; i++) {
    auto& v = vert[i];
    // Scale
    x = scale * v.x_;
    y = scale * v.y_;
    z = scale * v.z_;
    // Rotate + translate
    v.x_ = (cos_phi * cos_gamma * x -
            sin_gamma * cos_phi * y +
            sin_phi * z +
            dx);
    v.y_ = ((sin_gamma * cos_theta +
             sin_theta * sin_phi * cos_gamma) * x +
            (cos_theta * cos_gamma -
             sin_theta * sin_phi * sin_gamma) * y -
            sin_theta * cos_phi * z +
            dy);
    v.z_ = ((sin_theta * sin_gamma -
             sin_phi*cos_theta * cos_gamma) *x +
            (sin_phi * sin_gamma * cos_theta +
             sin_theta * cos_gamma) * y +
            cos_theta*cos_phi*z +
            dz);
  }

  // Compute eye distance
  Vector3<float> left_eye = vert[index_features_[index_eye_vertex_[0]]];
  left_eye += vert[index_features_[index_eye_vertex_[1]]];
  left_eye *= 0.5f;
  Vector3<float> right_eye = vert[index_features_[index_eye_vertex_[2]]];
  right_eye += vert[index_features_[index_eye_vertex_[3]]];
  right_eye *= 0.5f;
  auto d = right_eye - left_eye;
  eye_distance_ = std::sqrt((d.x_ * d.x_) + (d.y_ * d.y_) + (d.z_ * d.z_));
}

/*
 *  @name   ProjectLandmark
 *  @brief  Project 3D points onto image plane
 *  @param  transformation_parameters    Transformation parameters
 */
void DataGenerator::ProjectLandmark(const SyntheticDataParameter& transformation_parameters) {
  float* synthetic_landmark_ptr = nullptr;
  std::vector<cv::Mat>::iterator synthetic_landmark_it = synthetic_landmarks_.begin();
  float normalized_noise_std = transformation_parameters.noise_std *
                                0.01f *
                                eye_distance_;
  std::normal_distribution<float> normal_distribution(0.f,normalized_noise_std);
  for (int i = 0; i < number_of_view_; ++i,++synthetic_landmark_it) {
    synthetic_landmark_ptr = (float*)(synthetic_landmark_it->data);
    // Get camera information
    LTS5::VirtualCamera* camera = camera_setup_[i];
    // Project onto camera image plane
    cv::Mat projection_matrix = camera->get_projection_matrix();
    cv::Mat points_3d = cv::Mat::ones(4, 1, CV_32FC1);
    cv::Mat projected_pts;
    float z = 0.f;
    const auto& vertex = mesh_->get_vertex();
    for (int k = 0; k < number_2D_features_; k++) {
      auto& v = vertex[index_features_[k]];
      // Project facial landmarks
      points_3d.at<float>(0) = v.x_;
      points_3d.at<float>(1) = v.y_;
      points_3d.at<float>(2) = v.z_;
      projected_pts = projection_matrix * points_3d;
      z = projected_pts.at<float>(2);
      projected_pts /= z;
      // Add noise if needed
      if (transformation_parameters.add_noise) {
        synthetic_landmark_ptr[k*2] = projected_pts.at<float>(0) + normal_distribution(random_generator_);
        synthetic_landmark_ptr[k*2+1] = projected_pts.at<float>(1) + normal_distribution(random_generator_);
      } else {
        synthetic_landmark_ptr[k*2] = projected_pts.at<float>(0);
        synthetic_landmark_ptr[k*2+1] = projected_pts.at<float>(1);
      }
    }
  }
}


#pragma mark -
#pragma mark Accessors

}  // namespace LTS5
