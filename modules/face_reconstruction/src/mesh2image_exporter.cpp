/**
 *  @file   mesh2image_exporter.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 25/08/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <cstring>

#include "opencv2/highgui.hpp"

#include "lts5/utils/process_error.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/constant.hpp"
#include "lts5/utils/sys/file_system.hpp"
#include "lts5/face_reconstruction/mesh2image_exporter.hpp"
#include "lts5/ogl/command.hpp"

#pragma mark -
#pragma mark Initialization

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name Mesh2ImageExporter
 *  @fn Mesh2ImageExporter()
 *  @brief  Constructor
 */
Mesh2ImageExporter::Mesh2ImageExporter() : mesh_(nullptr),
                                               texture_(nullptr),
                                               program_(nullptr),
                                               cam_(nullptr),
                                               renderer_(nullptr),
                                               output_name_(""){
  renderer_ = new LTS5::OGLOffscreenRenderer<float>();
}

/*
 *  @name ~Mesh2ImageExporter
 *  @fn ~Mesh2ImageExporter()
 *  @brief  Destructor
 */
Mesh2ImageExporter::~Mesh2ImageExporter() {
  if (texture_) {
    delete texture_;
    texture_ = nullptr;
  }
  if (program_) {
    delete program_;
    program_ = nullptr;
  }
  if (cam_) {
    delete cam_;
    cam_ = nullptr;
  }
  if (renderer_) {
    delete renderer_;
    renderer_ = nullptr;
  }
}

/*
 *  @name Mesh2ImageExporter
 *  @fn Mesh2ImageExporter(const LTS5::Mesh<float>& mesh,
                           const cv::Mat& texture,
                           const int width,
                           const int height)
 *  @brief  Constructor
 *  @param[in]  mesh    Mesh to convert to image
 *  @param[in]  width   Image width
 *  @param[in]  height  Image height
 */
Mesh2ImageExporter::Mesh2ImageExporter(const LTS5::Mesh<float>& mesh,
                                       const cv::Mat& texture,
                                       const int width,
                                       const int height) : mesh_(nullptr),
                                                           texture_(nullptr),
                                                           program_(nullptr),
                                                           cam_(nullptr),
                                                           renderer_(nullptr),
                                                           output_name_(""){
  // Create renderer
  renderer_ = new LTS5::OGLOffscreenRenderer<float>();
  // Initialize pipeline
  this->Initialize(mesh, texture, width, height);
}

/**
 *  @name Mesh2ImageExporter
 *  @fn Mesh2ImageExporter(const int width,
                           const int height)
 *  @brief  Constructor
 *  @param[in]  width   Image width
 *  @param[in]  height  Image height
 */
Mesh2ImageExporter::Mesh2ImageExporter(const int width,
                                       const int height) :  mesh_(nullptr),
                                                            texture_(nullptr),
                                                            program_(nullptr),
                                                            cam_(nullptr),
                                                            renderer_(nullptr),
                                                            output_name_("") {
  // Create renderer
  renderer_ = new LTS5::OGLOffscreenRenderer<float>(/*width, height*/);
}

/*
 *  @name Initialize
 *  @fn void Initialize(const vtkPolyData* mesh)
 *  @brief  Initialize converter
 *  @param[in]  mesh  Mesh to convert to image
 *  @param[in]  width   Image width
 *  @param[in]  height  Image height
 */
void Mesh2ImageExporter::Initialize(const LTS5::Mesh<float>& mesh,
                                    const cv::Mat& texture,
                                    const int width,
                                    const int height) {
  if (!is_initialized_) {
    // Setup
    mesh_ = const_cast<LTS5::Mesh<float>*>(&mesh);
    // Init renderer
    renderer_->Init(width,
                    height,
                    0,
                    false,
                    LTS5::OGLOffscreenRenderer<float>::ImageType::kGrayscale);
    renderer_->ActiveContext();
    // Texture
    if (!texture_) {

      texture_ = new LTS5::OGLTexture(texture,
                                      OGLCommand::Constant::kGLLinear,
                                      OGLCommand::Constant::kGLClampToEdge);
    } else {
      texture_->Upload(texture);
    }

    if (!program_) {
      // Create shader + program
      std::string vertex_shader_str = "#version 330\n"
              "layout (location = 0) in vec3 position;\n"
              "layout (location = 1) in vec3 normal;\n"
              "layout (location = 2) in vec2 tex_coord;\n"
              "uniform mat4 camera;\n"
              "out vec2 tex_coord0;\n"
              "out vec3 normal0;\n"
              "void main() {\n"
              "  gl_Position = camera * vec4(position, 1);\n"
              "  normal0 = normal;\n"
              "  tex_coord0 = tex_coord;\n"
              "}";
      std::string frag_shader_str = "#version 330\n"
              "in vec2 tex_coord0;\n"
              "in vec3 normal0;\n"
              "uniform sampler2D tex_sampler;\n"
              "out vec4 frag_color;\n"
              "void main() {\n"
              "  float t = texture(tex_sampler, tex_coord0).r;\n"
              "  frag_color = vec4(t, t, t, 1.f);\n"
              "}";
      using ShaderType = typename LTS5::OGLShader::ShaderType;
      std::vector<OGLShader> shaders;
      shaders.emplace_back(OGLShader(vertex_shader_str, ShaderType::kVertex));
      shaders.emplace_back(OGLShader(frag_shader_str, ShaderType::kFragment));
      std::vector<OGLProgram::Attributes> attrib;
      attrib.emplace_back(OGLProgram::Attributes(OGLProgram::AttributeType::kPoint,
                                                 0,
                                                 "position"));
      attrib.emplace_back(OGLProgram::Attributes(OGLProgram::AttributeType::kNormal,
                                                 1,
                                                 "normal"));
      attrib.emplace_back(OGLProgram::Attributes(OGLProgram::AttributeType::kTexCoord,
                                                 2,
                                                 "tex_coord"));
      program_ = new LTS5::OGLProgram(shaders, attrib);
    }
    // Camera
    if (!cam_) {
      cam_ = new LTS5::OGLCamera<float>(width, height);
    }
    renderer_->AddMesh(*mesh_);
    renderer_->AddProgram(*program_);
    renderer_->AddTexture(*texture_);
    // Bind to opengl
    int err = renderer_->BindToOpenGL();
    renderer_->DisableContext();
    // Done
    this->is_initialized_ = (err == 0);
  }
}

#pragma mark -
#pragma mark Process

/*
 * @name  ActiveContext
 * @fn    void ActiveContext() const
 * @brief Make this context the current one
 */
void Mesh2ImageExporter::ActiveContext() const {
  renderer_->ActiveContext();
}

/*
 * @name  DisableContext
 * @fn    void DisableContext() const
 * @brief Detach from current context
 */
void Mesh2ImageExporter::DisableContext() const {
  renderer_->DisableContext();
}

/*
 *  @name Process
 *  @fn void Process()
 *  @brief  Convert rendered mesh into image
 */
void Mesh2ImageExporter::Process() {
  using Vec3f = Vector3<float>;
  if (is_initialized_) {
    this->renderer_->ActiveContext();
    // Define camera position + look at
    mesh_->ComputeBoundingBox();
    const AABB<float>& bbox = mesh_->bbox();
    auto dt = bbox.max_ - bbox.center_;
    float r = std::sqrt((dt.x_ * dt.x_) + (dt.y_ * dt.y_) + (dt.z_ * dt.z_));
    // Define displacement in Z direction
    float dz = r / std::sin(cam_->get_field_of_view() * Constants<float>::PI * 0.002777778f);
    cam_->set_position(Vec3f(bbox.center_.x_,
                             bbox.center_.y_,
                             bbox.center_.z_ + dz));
    // Update projection transformation
    auto proj = cam_->projection();
    auto view = cam_->view(true);
    program_->Use();
    program_->SetUniform("camera", proj * view);
    program_->SetUniform("tex_sampler", 0);
    program_->StopUsing();
    // Render
    renderer_->Render();
    // Get image
    renderer_->GetImage(&image_);
    // write if output not empty
    if (!output_name_.empty()) {
      size_t pos = output_name_.rfind('/');
      if (pos != std::string::npos) {
        std::string dir = output_name_.substr(0, pos);
        auto* fs = GetDefaultFileSystem();
        bool dir_exist = fs->FileExist(dir).Good();
        if (!dir_exist) {
          fs->CreateDir(dir);
        }
      }
      cv::imwrite(output_name_, image_);
    }
    renderer_->DisableContext();
  }
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name set_mesh
 *  @fn void set_mesh(const LTS5::Mesh<float>& mesh)
 *  @brief  Link to the mesh to export
 *  @param[in]  mesh  Mesh to export
 */
void Mesh2ImageExporter::set_mesh(const LTS5::Mesh<float>& mesh) {
  if (is_initialized_) {
    static bool first_pass = true;
    mesh_ = const_cast<LTS5::Mesh<float>*>(&mesh);
    renderer_->ActiveContext();
    renderer_->AddMesh(*mesh_);
    if(renderer_->UpdateVertex()) {
      LTS5_LOG_WARNING("Can not update mesh");
    }
    if (first_pass) {
      renderer_->UpdateTCoord();
      first_pass = false;
    }
  }
}

/**
 *  @name set_texture
 *  @fn void set_texture(const vtkTexture* texture)
 *  @brief  Set texture of the object
 *  @param[in]  texture Texture object
 */
void Mesh2ImageExporter::set_texture(const cv::Mat& texture) {
  if (is_initialized_) {
    this->renderer_->ActiveContext();
    if(texture_->Upload(texture)) {
      LTS5_LOG_WARNING("Can not upload texture");
    }
  }
}
}  // namespace LTS5











