/**
 *  @file   fit_algorithm.cpp
 *  @brief  Algorithm to fit 3D Morphable Model
 *  @ingroup    face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   13/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <vector>
#include <algorithm>
#include <random>
#include <chrono>
#include <numeric>
#include <utility>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include "lts5/face_reconstruction/fit_algorithm.hpp"
#include "lts5/face_reconstruction/fit_algorithm_factory.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/constant.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/sys/parallel.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Type Definition

/*
 * @name    Clear
 * @fn  void Clear()
 * @brief   Reset structure
 */
template<typename T>
void MMFitAlgorithm<T>::Stat::Clear() {
  cost.clear();
  data.clear();
  shape_prior.clear();
  tex_prior.clear();
  landmarks.clear();
}

/*
 * @name    ToMat
 * @fn  void ToMat(cv::Mat* m) const
 * @brief Export data into Mat container
 * @param[out] m    Matrix where to place data
 */
template<typename T>
void MMFitAlgorithm<T>::Stat::ToMat(cv::Mat* m) const {
  int n_var = 0;
  size_t n_data = 0;
  // Cost
  n_var += cost.empty() ? 0 : 1;
  n_data = n_data > cost.size() ? n_data : cost.size();
  // Data term
  n_var += data.empty() ? 0 : 1;
  n_data = n_data > data.size() ? n_data : data.size();
  // Shape prior
  n_var += shape_prior.empty() ? 0 : 1;
  n_data = n_data > shape_prior.size() ? n_data : shape_prior.size();
  // Tex prior
  n_var += tex_prior.empty() ? 0 : 1;
  n_data = n_data > tex_prior.size() ? n_data : tex_prior.size();
  // Landmarks
  n_var += landmarks.empty() ? 0 : 1;
  n_data = n_data > landmarks.size() ? n_data : landmarks.size();

  // Init container
  m->create(static_cast<int>(n_data), n_var, cv::DataType<T>::type);
  m->setTo(T(0.0));
  std::vector<const std::vector<T>*> array{&cost,
                                           &data,
                                           &shape_prior,
                                           &tex_prior,
                                           &landmarks};
  for (size_t k = 0; k < array.size(); ++k) {
    const auto* arr = array[k];
    if (!arr->empty()) {
      for (size_t i = 0; i < arr->size(); ++i) {
        m->at<T>(i, k) = arr->at(i);
      }
    }
  }
}

/*
 * @name    Print
 * @fn  void Print(std::ostream& stream)
 * @brief   Dump structure into a given stream
 * @param[in] stream    Stream where to dump data
 */
template<typename T>
void MMFitAlgorithm<T>::Stat::Print(std::ostream& stream) const {
  for (size_t k = 0; k < cost.size(); ++k) {
    stream << "*** Step - " << k << " ***" << std::endl;
    stream << "Edata " << data[k] << std::endl;
    stream << "Es_prior " << shape_prior[k] << std::endl;
    stream << "Et_prior " << tex_prior[k] << std::endl;
    stream << "Elms " << landmarks[k] << std::endl;
    stream << "E " << cost[k] << std::endl;
  }
}

#pragma mark -
#pragma mark Initialization

// Vertex shader
static constexpr const char* vs_str = "#version 330\n"
        "layout (location = 0) in vec3 position;\n"
        "layout (location = 1) in vec2 tcoord;\n"
        "uniform mat4 camera;\n"
        "out VS_OUT {\n"
        "  flat vec2 tcoord0;\n"
        "} vs_out;"
        "void main() {\n"
        "  gl_Position = camera * vec4(position, 1);\n"
        "  vs_out.tcoord0 = tcoord;\n"
        "}";

static constexpr const char* vs_ren_str = "#version 330\n"
        "layout (location = 0) in vec3 position;\n"
        "layout (location = 1) in vec3 vcolor;\n"
        "uniform mat4 camera;\n"
        "out vec3 color0;\n"
        "void main() {\n"
        "  gl_Position = camera * vec4(position, 1);\n"
        "  color0 = vcolor;\n"
        "}";

// Geometry shader - https://stackoverflow.com/a/25508724/4546884
static constexpr const char* gs_str = "#version 330\n"
        "layout (triangles) in;\n"
        "layout (triangle_strip, max_vertices = 3) out;\n"
        "in VS_OUT {\n"
        "  flat vec2 tcoord0;\n"
        "} gs_in[];\n"
        "out vec3 bary0;\n"
        "flat out vec2 tcoord1;\n"
        "void main() {\n"
        "  for (int i = 0; i < 3; ++i) {\n"
        "  \tgl_Position = gl_in[i].gl_Position;\n"
        "  \ttcoord1 = gs_in[i].tcoord0;\n"
        "  \tbary0 = vec3(0.f);\n"
        "  \tbary0[i] = 1.f;\n"
        "  \tEmitVertex();\n"
        "  }\n"
        "  EndPrimitive();\n"
        "}";

// Fragment shader
static constexpr const char* fs_str = "#version 330\n"
                  "in vec3 bary0;\n"
                  "flat in vec2 tcoord1;\n"
                  "out vec4 fragColor;\n"
                  "void main() {\n"
                  "  fragColor = vec4(tcoord1.x, bary0.x, bary0.y, bary0.z);\n"
                  "}";

static constexpr const char* fs_ren_str = "#version 330\n"
                    "in vec3 color0;\n"
                    "out vec3 fragColor;\n"
                    "void main() {\n"
                    "  fragColor = vec3(color0.b, color0.g, color0.r);\n"
                    "}";

/*
 * @name  MMFitAlgorithm
 * @fn    MMFitAlgorithm(const MorphableModel<T>* model)
 * @brief Constructor
 * @param[in] model       Model to fit
 */
template<typename T>
MMFitAlgorithm<T>::MMFitAlgorithm(const MorphableModel<T>* model) :
        model_(model),
        mesh_(nullptr),
        interpolator_(nullptr),
        renderer_(nullptr),
        shader_interp_(nullptr),
        shader_(nullptr),
        n_sample_(2000),
        tol_(1e-4),
        max_iter_(75) {
  // Init internal state
  this->Init();
}

/*
 * @name  ~MMFitAlgorithm
 * @fn    virtual ~MMFitAlgorithm()
 * @brief Destructor
 */
template<typename T>
MMFitAlgorithm<T>::~MMFitAlgorithm() {
  if (mesh_ != nullptr) {
    delete mesh_;
    mesh_ = nullptr;
  }
  if (interpolator_ != nullptr) {
    delete interpolator_;
    interpolator_ = nullptr;
  }
  if (renderer_ != nullptr) {
    delete renderer_;
    renderer_ = nullptr;
  }
  if (shader_interp_ != nullptr) {
    delete shader_interp_;
    shader_interp_ = nullptr;
  }
  if (shader_ != nullptr) {
    delete shader_;
    shader_ = nullptr;
  }
}

#pragma mark -
#pragma mark Protected

/*
 * @name  SampleVisiblePoint
 * @fn    int SampleVisiblePoint(const Mesh& instance,
                       const Camera_t& camera,
                       std::vector<ImagePoint>* pts)
 * @brief Sample visible points and compute coordinate in image plane and
 *        corresponding barycentric coordinates
 * @param[in] instance    Current instance of the model
 * @param[in] camera      Current estimation of camera
 * @param[out] pts    Selected visible points
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int MMFitAlgorithm<T>::SampleVisiblePoint(const Mesh& instance,
                                          const Camera_t& camera,
                                          std::vector<ImagePoint>* pts) {
  static bool interpolator_init = false;
  int err = -1;
  // Init renderer
  if (!interpolator_init) {
    err = this->InitInterpolator(instance, camera);
    interpolator_init = true;
  } else {
    this->ConvertMesh(instance);
    interpolator_->ActiveContext();
    interpolator_->AddMesh(*mesh_);
    err = interpolator_->UpdateVertex();
  }
  if (err == 0) {
    err = -1;
    interpolator_->ActiveContext();
    // Compute proper camera parameters
    constexpr T near = 1e-3;
    constexpr T far = 1e6;
    auto transform = this->ComputeOGLCameraTransform(camera, near, far);
    shader_interp_->Use();
    shader_interp_->SetUniform("camera", transform);
    shader_interp_->StopUsing();
    // Render
    interpolator_->Render();
    cv::Mat interp, depth;
    interpolator_->GetImage(&interp);
    interpolator_->GetDepth(&depth);
    interpolator_->DisableContext();

    // Retrive barycentric coordinate
    const int n = interp.rows * interp.cols;
    // TODO(Christophe):  check
    const auto& tri = instance.get_triangle();
    std::vector<ImagePoint> candidate;
    for (int i = 0; i < n; ++i) {
      // Compute depth
      const T d = near + (far - near) * T(depth.at<float>(i));
      if (d < far) {
        // Pixel belong to object
        ImagePoint p;
        p.x = i % interp.cols;
        p.y = interp.rows - (i / interp.cols) - 1;
        // tri index
        const auto v = interp.at<cv::Vec4f>(i);
        p.bcoord.t_idx = static_cast<size_t>(std::round(v[0]));
        p.bcoord.va_idx = tri[p.bcoord.t_idx].x_;
        p.bcoord.vb_idx = tri[p.bcoord.t_idx].y_;
        p.bcoord.vc_idx = tri[p.bcoord.t_idx].z_;
        // Barycentric
        p.bcoord.alpha = static_cast<T>(v[1]);
        p.bcoord.beta = static_cast<T>(v[2]);
        p.bcoord.gamma = static_cast<T>(v[3]);
        // Add to list
        candidate.emplace_back(p);
      }
    }
    // Sample them
    if (!candidate.empty()) {
      if (candidate.size() > n_sample_) {
        // Random permutation
        // obtain a time-based seed:
        auto seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::shuffle(candidate.begin(),
                     candidate.end(),
                     std::mt19937_64(seed));
        pts->resize(n_sample_);
        std::move(candidate.begin(),
                  candidate.begin() + n_sample_,
                  pts->begin());
      } else {
        pts->resize(candidate.size());
        std::move(candidate.begin(), candidate.end(), pts->begin());
      }
      err = 0;
    } else {
      LTS5_LOG_WARNING("No sampling candidate was found");
    }
  }
  return err;
}

/*
   * @name  RenderInstance
   * @fn    int RenderInstance(const Mesh& instance,
                               const Camera& camera, cv::Mat* image)
   * @brief Render an image of a given model instance
   * @param[in] instance    Model instance
   * @param[in] camera      Camera estimation
   * @param[out] image      Rendered image
   * @return    -1 if error, 0 otherwise
   */
template<typename T>
int MMFitAlgorithm<T>::RenderInstance(const Mesh& instance,
                                      const Camera_t& camera,
                                      cv::Mat* image) {
  static bool renderer_init = false;
  int err = -1;
  // Init renderer
  if (!renderer_init) {
    err = this->InitRenderer(instance, camera);
    renderer_init = true;
  } else {
    renderer_->AddMesh(instance);
    err = renderer_->UpdateVertex();
    err |= renderer_->UpdateColor();
  }
  if (err == 0) {
    err = -1;
    renderer_->ActiveContext();
    // Compute proper camera parameters
    constexpr T near = 1e-3;
    constexpr T far = 1e6;
    auto transform = this->ComputeOGLCameraTransform(camera, near, far);
    shader_->Use();
    shader_->SetUniform("camera", transform);
    shader_->StopUsing();
    // Render
    renderer_->Render();
    // Get result
    static cv::Mat buffer;
    err = renderer_->GetImage(&buffer);
    // Flip image since opengl texture coordinate starts in bottom left and ocv
    // starts top left.
    cv::flip(buffer, *image, 0);
    renderer_->DisableContext();
  }
  return err;
}

/*
 * @name  ImageGradient
 * @fn    void ImageGradient(const cv::Mat& image,
                              cv::Mat* grad_x,
                              cv::Mat* grad_y) const
 * @brief Compute image gradient in X/Y direction
 * @param[in] image   Image
 * @param[out] grad_x Image's gradient in X direction
 * @param[out] grad_y Image's gradient in Y direction
 */
template<typename T>
void MMFitAlgorithm<T>::ImageGradient(const cv::Mat& image,
                                      cv::Mat* grad_x,
                                      cv::Mat* grad_y) const {
  // Define central difference kernel
  T k_data[3] = {-0.5, 0.0, 0.5};
  cv::Mat kx(1, 3, cv::DataType<T>::type, k_data);
  cv::Mat ky(3, 1, cv::DataType<T>::type, k_data);
  // Filter
  cv::filter2D(image, *grad_x, cv::DataType<T>::type, kx);
  cv::filter2D(image, *grad_y, cv::DataType<T>::type, ky);
}

/*
 * @name  SampleImage
 * @fn    void SampleImage(const cv::Mat& image,
                 const std::vector<ImagePoint>& pts,
                 const bool is_grad,
                 cv::Mat* sample) const
 * @brief Sample a given image at locations \p pts.
 * @param[in] image   Image to sample
 * @param[in] pts     List of sampling point
 * @param[in] is_grad Indicate if image is gradient or not
 * @param[out] sample Sampled elements
 */
template<typename T>
void MMFitAlgorithm<T>::SampleImage(const cv::Mat& image,
                                    const std::vector<ImagePoint>& pts,
                                    const bool& is_grad,
                                    cv::Mat* sample) const {
  assert(image.channels() == 3 && image.type() == CV_8UC3);
  const int n = static_cast<int>(pts.size());
  const int n_col = is_grad ? 2 : 1;
  const int c = this->get_n_channels();
  sample->create(n * c, n_col, cv::DataType<T>::type);

  if (is_grad) {
    Parallel::For(n,
                  [&](const size_t& i) {
                    const auto& p = pts[i];
                    auto d_ptr = sample->ptr<T>(c * i);
                    // Compute grad X / Y
                    const auto& in_p1x = image.at<cv::Vec3b>(p.y, p.x + 1);
                    const auto& in_m1x = image.at<cv::Vec3b>(p.y, p.x - 1);
                    const auto& in_p1y = image.at<cv::Vec3b>(p.y + 1, p.x);
                    const auto& in_m1y = image.at<cv::Vec3b>(p.y - 1, p.x);
                    // Normalize at the same time as it computes grad
                    // ((a - b) / 2) / 255
                    *d_ptr++ = (T(in_p1x[2]) - T(in_m1x[2])) / T(510.0);  // dR
                    *d_ptr++ = (T(in_p1y[2]) - T(in_m1y[2])) / T(510.0);  // dR
                    *d_ptr++ = (T(in_p1x[1]) - T(in_m1x[1])) / T(510.0);  // dG
                    *d_ptr++ = (T(in_p1y[1]) - T(in_m1y[1])) / T(510.0);  // dG
                    *d_ptr++ = (T(in_p1x[0]) - T(in_m1x[0])) / T(510.0);  // dB
                    *d_ptr = (T(in_p1y[0]) - T(in_m1y[0])) / T(510.0);  // dB
                  });
  } else {
    Parallel::For(n,
                  [&](const size_t& i) {
                    const auto& p = pts[i];
                    auto d_ptr = sample->ptr<T>(3 * i);
                    // Sample image
                    const auto& in = image.at<cv::Vec3b>(p.y, p.x);
                    // Output format RGBA, image format BGR
                    *d_ptr++ = T(in[2]) / T(255.0);
                    *d_ptr++ = T(in[1]) / T(255.0);
                    *d_ptr = T(in[0]) / T(255.0);
                  });

  }
}

/*
 * @name  Cost
 * @fn    T Cost(const cv::Mat& shape_w,
                const cv::Mat& tex_w,
                const cv::Mat& data_e,
                const cv::Mat& lms_e,
                const T shape_prior_reg,
                const T tex_prior_reg,
                const T lms_prior_reg)
 * @brief Compute current cost
 * @param[in] shape_w         Shape parameters
 * @param[in] tex_w           Texture parameters
 * @param[in] data_e          Data error term
 * @param[in] lms_e           Landmarks error term
 * @param[in] shape_prior_reg Shape prior regularizer
 * @param[in] tex_prior_reg   Texture prior regularizer
 * @param[in] lms_prior_reg   Landmarks prior regularizer
 * @return Current cost
 */
template<typename T>
T MMFitAlgorithm<T>::Cost(const cv::Mat& shape_w,
                          const cv::Mat& tex_w,
                          const cv::Mat& data_e,
                          const cv::Mat& lms_e,
                          const T& shape_prior_reg,
                          const T& tex_prior_reg,
                          const T& lms_prior_reg) {
  using LA = LinearAlgebra<T>;
  // Cost on data term
  T cost = LA::Dot(data_e, data_e);
  // Cost shape prior
  cv::Mat p_square;
  if (shape_prior_reg != T(0.0)) {
    LA::Sbmv(shape_w, T(1.0), shape_w, T(0.0), &p_square);
    cost += shape_prior_reg * LA::Dot(p_square, j_shape_prior_);
  }
  // Cost texture prior
  if (tex_prior_reg != T(0.0)) {
    LA::Sbmv(tex_w, T(1.0), tex_w, T(0.0), &p_square);
    cost += tex_prior_reg * LA::Dot(p_square, j_tex_prior_);
  }
  // Cost landmarks prior
  if (lms_prior_reg != T(0.0)) {
    cost += lms_prior_reg * LA::Dot(lms_e, lms_e);
  }
  return cost;
}

/*
 * @name  JacobianLandmark
 * @fn    void JacobianLandmark(const Camera_t& camera,
                                const std::vector<Vertex>& shape,
                                const cv::Mat& var,
                                cv::Mat* jacob_lms)
 * @brief Compute the Jacobian for the landmarks transformation
 * @param[in] camera      Camera estimation
 * @param[in] shape       3D shape
 * @param[in] var         Shape variation
 * @param[out] jacob_lms  Landmarks jacobian [2L x (ns + nc + nt)]
 */
template<typename T>
void MMFitAlgorithm<T>::JacobianLandmark(const Camera_t& camera,
                                         const cv::Mat& shape,
                                         const cv::Mat& var,
                                         cv::Mat* jacob_lms) {
  // TODO(Christophe): Check if n == 68
  auto n = std::max(shape.rows, shape.cols) / 3;
  assert(n == (var.rows/3));
  const int ns = this->model_->get_active_shape_component();
  const int nt = this->model_->get_active_texture_component();
  const int ni = this->model_->get_active_illumination_component();
  const int nc = camera.get_n_parameter();
  jacob_lms->create(2 * n, ns + nc + nt + ni, cv::DataType<T>::type);
  // Compute derivative of camera wrt shape parameters
  if (ns != 0) {
    cv::Mat Jwp = (*jacob_lms)(cv::Rect(0, 0, var.cols, 2 * n));
    this->DcamDshapeParam(camera, shape, var, &Jwp);
  }
  // Compute derivative of camera wrt camera parameters
  if (nc != 0) {
    cv::Mat Jwc = (*jacob_lms)(cv::Rect(var.cols, 0, nc, 2 * n));
    this->DcamDcamParam(camera, shape, &Jwc);
  }
  // Add texture derivative + Illumination
  cv::Mat Jwt = (*jacob_lms)(cv::Rect(ns + nc, 0, nt + ni, 2 * n));
  Jwt = T(0.0);
}

/*
 * @name  DcamDshapeParam
 * @fn    void DcamDshapeParam(const Camera_t& camera,
                               const std::vector<Vertex>& shape,
                               const cv::Mat& var,
                               cv::Mat* dcam_dsp)
 * @brief Compute the derivative of the perspective projection with respect
 *        to the shape parameters
 * @param[in] camera  Camera estimation
 * @param[in] shape   3D shape
 * @param[in] var     Shape model variance
 * @param[out] dcam_dsp   Derivative (2N x ns)
 */
template<typename T>
void MMFitAlgorithm<T>::DcamDshapeParam(const Camera_t& camera,
                                        const cv::Mat& shape,
                                        const cv::Mat& var,
                                        cv::Mat* dcam_dsp) {
  // Init output
  auto n = std::max(shape.rows, shape.cols) / 3;
  assert(n >= this->n_sample_ || n == this->lms_index_.size());
  dcam_dsp->create(2 * n, var.cols, cv::DataType<T>::type);
  // Get rotation matrix + focal length
  const T f = camera.get_focal_length();
  auto rot = camera.get_rotation_matrix();
  const T* ax = camera.get_axis_inversion();
  for (int i = 0; i < 3; ++i) {
    rot(i, 0) *= ax[0];
    rot(i, 1) *= ax[1];
    rot(i, 2) *= ax[2];
  }
  // Loop over matrix
  Parallel::For(n,
                [&](const size_t& i) {
                  const auto& t = camera.get_translation();
                  auto* shape_ptr = reinterpret_cast<const Vertex*>(shape.data);
                  // Rotation variance
                  cv::Mat vark = rot * var(cv::Rect(0, 3*i, var.cols, 3));
                  // Access vertex
                  const auto& v = shape_ptr[i];
                  const auto vx = (rot * v) + t;
                  const T fivzz = f / (vx.z_ * vx.z_);
                  // Compute derivative
                  dcam_dsp->row(2*i) = ((vark.row(0) * vx.z_) -
                                        (vx.x_ * vark.row(2))) * fivzz;
                  dcam_dsp->row(2*i + 1) = ((vark.row(1) * vx.z_) -
                                            (vx.y_ * vark.row(2))) * fivzz;
                });
}

/*
 * @name  DcamDcamParam
 * @fn    void DcamDcamParam(const Camera_t& camera,
                             const std::vector<Vertex>& shape,
                             cv::Mat* dcam_dcamp)
 * @brief Compute the derivative of the perspective projection with respect
 *        to the camera parameters
 * @param[in] camera  Camera estimation
 * @param[in] shape   3D shape
 * @param[out] dcam_dcamp   Derivative (2N x nc)
 */
template<typename T>
void MMFitAlgorithm<T>::DcamDcamParam(const Camera_t& camera,
                                      const cv::Mat& shape,
                                      cv::Mat* dcam_dcamp) {
  // Init
  const int nc = camera.get_n_parameter();
  auto n = std::max(shape.rows, shape.cols) / 3;
  assert(n == this->n_sample_ || n == this->lms_index_.size());
  dcam_dcamp->create(2 * n, nc, cv::DataType<T>::type);
  // Access elem
  const T f = camera.get_focal_length();
  const auto& q = camera.get_rotation();
  const auto& rot = camera.get_rotation_matrix();
  const auto& t = camera.get_translation();
  const auto* shape_ptr = reinterpret_cast<const Vertex*>(shape.data);
  const T* ax = camera.get_axis_inversion();
  // Fill
  Parallel::For(n,
                [&](const size_t& i) {
    int ix = 2 * i;
    int iy = ix + 1;
    // Untransformed vertex
    auto v = shape_ptr[i];
    v.x_ *= ax[0];
    v.y_ *= ax[1];
    v.z_ *= ax[2];
    // Transformed vertex
    auto vx = (rot * v) + t;
    const T ivzz = T(1.0) / (vx.z_ * vx.z_);
    // f derivative
    dcam_dcamp->at<T>(ix, 0) = vx.x_ / vx.z_;
    dcam_dcamp->at<T>(iy, 0) = vx.y_ / vx.z_;
    // Q1 derivative
    T dvx_dq = T(2.0) * (q.y_ * v.y_ + q.z_ * v.z_);
    T dvy_dq = T(2.0) * (q.y_ * v.x_ - 2.0 * q.x_ * v.y_ - q.w_ * v.z_);
    T dvz_dq = T(2.0) * (q.z_ * v.x_ + q.w_ * v.y_  - 2.0 * q.x_ * v.z_);
    dcam_dcamp->at<T>(ix, 1) = f * (dvx_dq * vx.z_ - vx.x_ * dvz_dq) * ivzz;
    dcam_dcamp->at<T>(iy, 1) = f * (dvy_dq * vx.z_ - vx.y_ * dvz_dq) * ivzz;
    // Q2 derivative
    dvx_dq = T(2.0) * (-2.0 * q.y_ * v.x_ + q.x_ * v.y_ + q.w_ * v.z_);
    dvy_dq = T(2.0) * (q.x_ * v.x_ + q.z_ * v.z_);
    dvz_dq = T(2.0) * (-q.w_ * v.x_ + q.z_ * v.y_ - 2.0 * q.y_ * v.z_);
    dcam_dcamp->at<T>(ix, 2) = f * (dvx_dq * vx.z_ - vx.x_ * dvz_dq) * ivzz;
    dcam_dcamp->at<T>(iy, 2) = f * (dvy_dq * vx.z_ - vx.y_ * dvz_dq) * ivzz;
    // Q3 derivative
    dvx_dq = T(2.0) * (-2.0 * q.z_ * v.x_ - q.w_ * v.y_ + q.x_ * v.z_);
    dvy_dq = T(2.0) * (q.w_ * v.x_ - 2.0 * q.z_ * v.y_ + q.y_ * v.z_);
    dvz_dq = T(2.0) * (q.x_ * v.x_ + q.y_ * v.y_);
    dcam_dcamp->at<T>(ix, 3) = f * (dvx_dq * vx.z_ - vx.x_ * dvz_dq) * ivzz;
    dcam_dcamp->at<T>(iy, 3) = f * (dvy_dq * vx.z_ - vx.y_ * dvz_dq) * ivzz;
    // Tx derivative
    dcam_dcamp->at<T>(ix, 4) = f / vx.z_;
    dcam_dcamp->at<T>(iy, 4) = T(0.0);
    // Ty derivative
    dcam_dcamp->at<T>(ix, 5) = T(0.0);
    dcam_dcamp->at<T>(iy, 5) = f / vx.z_;
    // Tz derivative
    dcam_dcamp->at<T>(ix, 6) = -f * (vx.x_ * ivzz);
    dcam_dcamp->at<T>(iy, 6) = -f * (vx.y_ * ivzz);
  });
}

#pragma mark -
#pragma mark Private

/*
 * @name  Init
 * @fn    void Init()
 * @brief Initialize some internal states
 */
template<typename T>
void MMFitAlgorithm<T>::Init() {
  // Init mesh
  if (mesh_ == nullptr) {
    mesh_ = new Mesh();
  }
  // Shape prior
  const cv::Mat& sp = model_->get_shape_prior();
  const int ns = std::max(sp.rows, sp.cols);
  j_shape_prior_.create(sp.rows, sp.cols, sp.type());
  for (int i = 0; i < ns; ++i) {
    j_shape_prior_.at<T>(i) = T(1.0) / sp.at<T>(i);
  }
  // Tex prior
  const cv::Mat& tp = model_->get_texture_prior();
  const int nt = std::max(tp.rows, tp.cols);
  j_tex_prior_.create(tp.rows, tp.cols, tp.type());
  for (int i = 0; i < nt; ++i) {
    j_tex_prior_.at<T>(i) = T(1.0) / tp.at<T>(i);
  }
  // Landmark principal component
  lms_index_ = model_->get_landmark_index();
  const int n_lms = static_cast<int>(lms_index_.size());
  const cv::Mat& var = model_->get_shape_principal_component();
  shape_pc_lms_.create(3 * n_lms, ns, sp.type());
  for (int i = 0; i < n_lms; ++i) {
    int i_out = i * 3;
    int i_in = lms_index_[i] * 3;
    var.row(i_in).copyTo(shape_pc_lms_.row(i_out));
    var.row(i_in + 1).copyTo(shape_pc_lms_.row(i_out + 1));
    var.row(i_in + 2).copyTo(shape_pc_lms_.row(i_out + 2));
  }
  // Landmarks PC scaled by eigval
  this->shape_scaled_pc_lms_ = (this->shape_pc_lms_ *
                                cv::Mat::diag(this->model_->get_shape_prior()));
}

/*
 * @name  ConvertMesh
 * @fn
 * @brief Convert an instance into a mesh with one vertex for each triangle
 *        corner (i.e. no shared vertex)
 * @param[in] instance
 */
template<typename T>
void MMFitAlgorithm<T>::ConvertMesh(const Mesh& instance) {
  // Init new triangulation
  const auto& tri = instance.get_triangle();
  auto& ntri = mesh_->get_triangle();
  if (ntri.size() != tri.size()) {
    // Define new triangulation
    ntri.resize(tri.size());
    auto* tri_ptr = reinterpret_cast<int*>(ntri.data());
    std::iota(tri_ptr, tri_ptr + (ntri.size() * 3), 0);
    // Setup triangle index
    auto& ntcoord = mesh_->get_tex_coord();
    ntcoord.resize(ntri.size() * 3);  // Use TCoord as triangle index storage
    size_t cnt = 0;
    for (size_t t = 0; t < tri.size(); ++t) {
      const auto tidx = Vector2<T>(t, t);
      ntcoord[cnt++] = tidx;
      ntcoord[cnt++] = tidx;
      ntcoord[cnt++] = tidx;
    }
    assert(cnt == (ntri.size() * 3));
  }
  // Convert pts
  const auto& vertex = instance.get_vertex();
  auto& nvertex = mesh_->get_vertex();
  nvertex.resize(ntri.size() * 3);
  size_t cnt = 0;
  for (size_t t = 0; t < tri.size(); ++t) {
    const auto& triangle = tri[t];
    nvertex[cnt++] = vertex[triangle.x_];
    nvertex[cnt++] = vertex[triangle.y_];
    nvertex[cnt++] = vertex[triangle.z_];
  }
  assert(cnt == (ntri.size() * 3));
}

/*
 * @name  InitInterpolator
 * @fn    int InitInterpolator(const Mesh& instance,
                     const Camera_t& camera)
 * @brief Initialize OpenGL interpolator (Lazy init)
 * @param[in] instance    Current instance of the model
 * @param[in] camera      Current estimation of camera
 * @return    @return -1 if error, 0 otherwise
 */
template<typename T>
int MMFitAlgorithm<T>::InitInterpolator(const Mesh& instance,
                                        const Camera_t& camera) {
  int err = 0;
  if (interpolator_ == nullptr && shader_interp_ == nullptr) {
    using Att = OGLProgram::Attributes;
    using AttTyp = OGLProgram::AttributeType;
    using ShaderType = typename LTS5::OGLShader::ShaderType;
    using ImageType = typename OGLOffscreenRenderer<T, Vector3<T>>::ImageType;
    // Create OpenGL context
    interpolator_ = new OGLOffscreenRenderer<T, Vector3<T>>();
    const auto w = static_cast<int>(T(2.0) * camera.get_principal_point_x());
    const auto h = static_cast<int>(T(2.0) * camera.get_principal_point_y());
    err = interpolator_->Init(w,
                              h,
                              0,
                              false,
                              ImageType::kRGBAf);
    interpolator_->ActiveContext();
    // Init shader
    std::vector<OGLShader> shaders;
    shaders.emplace_back(LTS5::OGLShader(vs_str, ShaderType::kVertex));
    shaders.emplace_back(LTS5::OGLShader(gs_str, ShaderType::kGeometry));
    shaders.emplace_back(LTS5::OGLShader(fs_str, ShaderType::kFragment));
    std::vector<Att> attrib;
    attrib.emplace_back(Att(AttTyp::kPoint, 0, "position"));
    attrib.emplace_back(Att(AttTyp::kTexCoord, 1, "tcoord"));
    shader_interp_ = new LTS5::OGLProgram(shaders, attrib);
    // Convert instance for interpolation
    this->ConvertMesh(instance);
    // Add to renderer
    interpolator_->AddMesh(*mesh_);
    interpolator_->AddProgram(*shader_interp_);
    err |= interpolator_->BindToOpenGL();
    interpolator_->DisableContext();
  }
  return err;
}

/*
 * @name  InitRenderer
 * @fn    int InitRenderer(const Mesh& instance,
                           const Camera_t& camera)
 * @brief Initialize OpenGL renderer (Lazy init)
 * @param[in] instance    Current instance of the model
 * @param[in] camera      Current estimation of camera
 * @return    @return -1 if error, 0 otherwise
 */
template<typename T>
int MMFitAlgorithm<T>::InitRenderer(const Mesh& instance,
                                    const Camera_t& camera) {
  int err = 0;
  if (renderer_ == nullptr && shader_ == nullptr) {
    using Att = OGLProgram::Attributes;
    using AttTyp = OGLProgram::AttributeType;
    using ShaderType = typename OGLShader::ShaderType;
    using ImageType = typename OGLOffscreenRenderer<T, Vector3<T>>::ImageType;
    // Create OpenGL context
    renderer_ = new OGLOffscreenRenderer<T, Vector3<T>>();
    const auto w = static_cast<int>(T(2.0) * camera.get_principal_point_x());
    const auto h = static_cast<int>(T(2.0) * camera.get_principal_point_y());
    err = renderer_->Init(w,
                          h,
                          4,
                          false,
                          ImageType::kRGB);
    renderer_->ActiveContext();
    // Init shader
    std::vector<OGLShader> shaders;
    shaders.emplace_back(vs_ren_str, ShaderType::kVertex);
    shaders.emplace_back(fs_ren_str, ShaderType::kFragment);
    std::vector<Att> attrib;
    attrib.emplace_back(AttTyp::kPoint, 0, "position");
    attrib.emplace_back(AttTyp::kColor, 1, "vcolor");
    shader_ = new LTS5::OGLProgram(shaders, attrib);
    // Add to renderer
    renderer_->AddMesh(instance);
    renderer_->AddProgram(*shader_);
    err |= renderer_->BindToOpenGL();
    renderer_->DisableContext();
  }
  return err;
}

/*
 * @name  ComputeOGLCameraTransform
 * @fn    glm::mat4 ComputeOGLCameraTransform(const Camera_t& camera,
                                              const T near,
                                              const T far)
 * @brief Define OpenGL transformation for the current camera estimation
 * @see http://ksimek.github.io/2013/06/03/calibrated_cameras_in_opengl/
 * @param[in] camera  Current camera estimation
 * @param[in] near    Near plane
 * @param[in] far     Far plane
 * @return    OpenGL transform
 */
template<typename T>
typename MMFitAlgorithm<T>::Mat4
MMFitAlgorithm<T>::ComputeOGLCameraTransform(const Camera_t& camera,
                                             const T& near,
                                             const T& far) {
  LTS5::Matrix3x3<T> rot;
  auto qcam = camera.get_rotation();
  qcam.y_ *= T(-1.0);
  qcam.z_ *= T(-1.0);
  qcam.ToRotationMatrix(&rot);
  const auto& tvec = camera.get_translation();
  const T f = camera.get_focal_length();
  const T cx = camera.get_principal_point_x();
  const T cy = camera.get_principal_point_y();
  // Define model
  Mat4 model;
  // Rotation
  model(0, 0) = rot[0]; model(0, 1) = rot[1]; model(0, 2) = rot[2];
  model(1, 0) = rot[3]; model(1, 1) = rot[4]; model(1, 2) = rot[5];
  model(2, 0) = rot[6]; model(2, 1) = rot[7]; model(2, 2) = rot[8];
  model(3, 0) = 0.f;    model(3, 1) = 0.f;    model(3, 2) = 0.f;
  // Translation
  model(0, 3) = tvec.x_;
  model(1, 3) = -tvec.y_;
  model(2, 3) = -tvec.z_;
  model(3, 3) = 1.0f;
  // Define projection
  // see: http://kgeorge.github.io/2014/03/08/calculating-opengl-perspective-matrix-from-opencv-intrinsic-matrix
  // ------------------------------------
  Mat4 p;
  p(0, 0) = f / cx;
  p(1, 1) = f / cy;
  p(2, 2) = - (far + near) / (far - near);
  p(2, 3) = - (T(2.0) * far * near) / (far - near);
  p(3, 2) = T(-1.0);
  return p *  model;
}

#pragma mark -
#pragma mark Proxy

/*
 * @name  MMFitAlgorithmProxy
 * @fn    MMFitAlgorithmProxy()
 * @brief Constructor
 */
template<typename T>
MMFitAlgorithmProxy<T>::MMFitAlgorithmProxy() {
  MMFitAlgorithmFactory<T>::Get().Register(this);
}


#pragma mark -
#pragma mark Explicit Instantiation

/** Float - Fit algo */
template class MMFitAlgorithm<float>;
/** Double - Fit algo*/
template class MMFitAlgorithm<double>;

/** Float - Fit Proxy */
template class MMFitAlgorithmProxy<float>;
/** Double - Fit Proxy*/
template class MMFitAlgorithmProxy<double>;

}  // namespace LTS5
