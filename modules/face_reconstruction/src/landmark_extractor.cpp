/**
 *  @file   landmark_extractor.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 04/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <fstream>
#include <cctype>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/face_reconstruction/landmark_extractor.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/face_tracker/mstream_sdm_tracker.hpp"
#include "lts5/face_tracker/mstream_lbf_tracker.hpp"

#ifdef HAS_CUDA_
#include "lts5/cuda/face_tracker/mstream_sdm_tracker.hpp"
#endif

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/*
 *  @name   LandmarkExtractor
 *  @brief  Constructor
 */
LandmarkExtractor::LandmarkExtractor(const LandmarkExtractor::Parameter& extractor_config) :
        BaseExtractor::BaseExtractor(extractor_config.number_of_view),
        dbl_landmarks_(extractor_config.number_of_view),
        params_(extractor_config) {
  using ProcessErrorEnum = LTS5::ProcessError::ProcessErrorEnum;
  // Init new face tracker based on tracker type information
  std::string type(params_.type.length(), ' ');
  std::transform(params_.type.begin(),
                 params_.type.end(),
                 type.begin(),
                 ::toupper);
  int err = -1;
  if (type == "SDM") {
    // Sdm type
/*#ifdef HAS_CUDA_
    face_tracker_ = new CUDA::MStreamSdmTracker(params_.number_of_view);
    auto* tracker = static_cast<CUDA::MStreamSdmTracker*>(face_tracker_);
#else*/
    face_tracker_ = new LTS5::MStreamSdmTracker(params_.number_of_view);
    auto* tracker = static_cast<MStreamSdmTracker*>(face_tracker_);
//#endif
    err = face_tracker_->Load(extractor_config.tracker_configuration_path,
                              extractor_config.tracker_face_detector_path);
    auto param = tracker->get_tracker_parameters();
    param.starting_pass = params_.start_pass;
    tracker->set_tracker_parameters(param);
    // Set thresh ?
    if (extractor_config.tracker_threshold.compare("n/a")) {
      // Need to change
      double thresh = std::stod(extractor_config.tracker_threshold);
      tracker->set_score_threshold(thresh);
    }
  } else if (type == "LBF") {
/*#ifdef HAS_CUDA_
    throw LTS5::ProcessError(ProcessErrorEnum::kErr,
                             "Face tracker has no GPU implementation",
                             FUNC_NAME);
#else*/
    // lbf type
    face_tracker_ = new LTS5::MStreamLBFTracker(params_.number_of_view);
    err = face_tracker_->Load(extractor_config.tracker_configuration_path,
                              extractor_config.tracker_face_detector_path);
    // Set thresh ?
    if (extractor_config.tracker_threshold.compare("n/a")) {
      // Need to change
      auto* tracker = static_cast<MStreamLBFTracker*>(face_tracker_);
      double thresh = std::stod(extractor_config.tracker_threshold);
      tracker->set_score_threshold(thresh);
    }
//#endif
  } else {
    throw LTS5::ProcessError(ProcessErrorEnum::kErr,
                             "Unknown face tracker type",
                             FUNC_NAME);
  }
  // Something went wrong when loading face tracker
  if (err) {
    throw LTS5::ProcessError(ProcessErrorEnum::kErrOpeningFile,
                             "Unable to load face tracker",
                             FUNC_NAME);

  }
}

/*
 *  @name   ~LandmarkExtractor
 *  @brief  Destructor
 */
LandmarkExtractor::~LandmarkExtractor() {
  if (face_tracker_) {
    delete face_tracker_;
    face_tracker_ = nullptr;
  }
}

/*
 *  @name   Process
 *  @fn void Process(const BaseStream::Buffer& buffer)
 *  @brief  Run face tracker or load annotation from file and put result
 *          into data buffer. Estimate head  pose from landmarks if available
 *  @param[in]  image_buffer    Buffer holding captured image
 */
void LandmarkExtractor::Process(const BaseStream::Buffer& buffer) {
  // Sanity check
  assert(data_.contour.size() == params_.number_of_view);
  // Goes for each views
  cv::Mat landmarks;
  // Track landmarks
/*#ifdef HAS_CUDA_
  const_cast<BaseStream::Buffer*>(&buffer)->WaitCompletion();
  face_tracker_->Track(buffer.device, &dbl_landmarks_);
#else*/
  face_tracker_->Track(buffer.host, &dbl_landmarks_);
//#endif
  for (int k = 0; k < params_.number_of_view; ++k) {
    // Got something ?
    if (!dbl_landmarks_[k].empty()) {
      // Ok, convert
/*#ifdef HAS_CUDA_
      cv::Mat l;
      dbl_landmarks_[k].Download(&l);
#else*/
      cv::Mat& l = dbl_landmarks_[k];
//#endif
      landmarks = l.reshape(l.channels(), 2).t();
      landmarks = landmarks.reshape(landmarks.channels(), 1).t();
      landmarks.convertTo(data_.landmarks[k], CV_32FC1);
    } else {
      data_.landmarks[k] = cv::Mat();
    }
  }
  // Process each view
  auto image_it = buffer.host.cbegin();
  auto landmarks_it = data_.landmarks.cbegin();
  auto face_origin_it = data_.face_region.begin();
  auto contour_it = data_.contour.begin();
  for (; landmarks_it != data_.landmarks.cend();
       ++landmarks_it,
       ++image_it,
       ++contour_it,
       ++face_origin_it) {
    // Shape valid ?
    if (!landmarks_it->empty()) {
      // Compute bounding box
      const float* landmarks_ptr = reinterpret_cast<const float*>(landmarks_it->data);
      float x = 0.f, y = 0.f;
      int n_point = landmarks_it->rows / 2;
      x = landmarks_ptr[0];
      y = landmarks_ptr[1];
      face_bounding_box_[0] = face_bounding_box_[1] = x;
      face_bounding_box_[2] = face_bounding_box_[3] = y;
      for (int i = 1; i < n_point; ++i) {
        x = landmarks_ptr[2 * i];
        y = landmarks_ptr[2 * i + 1];
        // Xmax
        face_bounding_box_[1] = face_bounding_box_[1] < x ? x : face_bounding_box_[1];
        // Xmin
        face_bounding_box_[0] = face_bounding_box_[0] > x ? x : face_bounding_box_[0];
        // Ymax
        face_bounding_box_[3] = face_bounding_box_[3] < y ? y : face_bounding_box_[3];
        // Ymin
        face_bounding_box_[2] = face_bounding_box_[2] > y ? y : face_bounding_box_[2];
      }
      // Extract contour
      if (params_.use_contour) {
        // Extend bbox
        float face_w = ((face_bounding_box_[1] - face_bounding_box_[0]) *
                        params_.face_bbox_margin);
        float face_h = ((face_bounding_box_[3] - face_bounding_box_[2]) *
                        params_.face_bbox_margin);
        face_bounding_box_[0] -= face_w;
        face_bounding_box_[1] += face_w;
        face_bounding_box_[2] -= face_h;
        face_bounding_box_[3] += face_h;
        // Check range, out of image boundaries ?
        int cols = image_it->cols;
        int rows = image_it->rows;
        face_bounding_box_[0] = (face_bounding_box_[0] < 0.f ?
                                 0.f :
                                 face_bounding_box_[0] > cols ?
                                 cols :
                                 face_bounding_box_[0]);
        face_bounding_box_[1] = (face_bounding_box_[1] < 0.f ?
                                 0.f :
                                 face_bounding_box_[1] > cols ?
                                 cols :
                                 face_bounding_box_[1]);
        face_bounding_box_[2] = (face_bounding_box_[2] < 0.f ?
                                 0.f :
                                 face_bounding_box_[2] > rows ?
                                 rows :
                                 face_bounding_box_[2]);
        face_bounding_box_[3] = (face_bounding_box_[3] < 0.f ?
                                 0.f :
                                 face_bounding_box_[3] > rows ?
                                 rows :
                                 face_bounding_box_[3]);
        // Save origin (x,y)
        face_origin_it->x = face_bounding_box_[0];
        face_origin_it->y = face_bounding_box_[2];
        // Copy
        cv::Rect roi = cv::Rect((int)face_bounding_box_[0],
                                (int)face_bounding_box_[2],
                                (int)(face_bounding_box_[1] -
                                      face_bounding_box_[0]),
                                (int)(face_bounding_box_[3] -
                                      face_bounding_box_[2]));
        contour_it->create(roi.height, roi.width, image_it->type());
        cv::Mat buffer;
        ((*image_it)(roi)).copyTo(buffer);
        // Edge detector
        cv::blur(buffer,
                 *contour_it,
                 cv::Size(3,3));
        cv::Canny(*contour_it,
                  *contour_it,
                  params_.edge_threshold[0],
                  params_.edge_threshold[1]);
      } else {
        contour_it->release();
        face_origin_it->x = 0.f;
        face_origin_it->y = 0.f;
      }
    }
  }
}
}  // namespace LTS5
