/**
 *  @file   texture_mapper.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 29/08/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <cstring>
#ifdef __APPLE__
#include <dispatch/dispatch.h>
#endif

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"


#include "lts5/utils/file_io.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/utils/image_transforms.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/face_reconstruction/texture_mapper.hpp"
#include "lts5/geometry/octree.hpp"
#include "lts5/geometry/discrete_remeshing.hpp"
#include "lts5/geometry/intersection_utils.hpp"

#pragma mark -
#pragma mark Constructor/Destructor

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name   TextureMapper
 *  @brief  Constructor
 */
TextureMapper::TextureMapper() : is_initialized_(false) {
}

/*
 *  @name   TextureMapper
 *  @fn explicit TextureMapper(const TextureMapperConfig& config)
 *  @brief  Constructor
 *  @param[in]  config  Mapper configuration
 */
TextureMapper::TextureMapper(const TextureMapperConfig& config) : is_initialized_(false) {
  // Setup config
  mapper_config_ = config;
  // Load points if needed
  if (!mapper_config_.control_pts_config_file.empty() &&
      config.mapping_type != MappingType::kProjection) {
    // Load pts
    int err = this->Load(mapper_config_.control_pts_config_file);
    if (err) {
      throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErrReadingData,
                               "Unable to load texture mapper control points",
                               FUNC_NAME);
    }
    texture_map_ = cv::Mat(mapper_config_.texture_map_size,
                           mapper_config_.texture_map_size,
                           CV_8UC1,
                           cv::Scalar(0));
  }
}

/*
 *  @name Load
 *  @fn int Load(const std::string& file)
 *  @brief  Load mapper configuration
 *  @param[in] file File holding configuration
 */
int TextureMapper::Load(const std::string& file) {
  int error = -1;
  std::ifstream in(file.c_str());
  if (in.is_open()) {
    control_pts_index_.clear();
    tex_coordinate_.clear();
    target_pts_.clear();
    // Start to read
    std::string line;
    std::stringstream ss;
    int idx;
    int section_counter = 0;
    float tx_min = std::numeric_limits<float>::max();
    float ty_min = std::numeric_limits<float>::max();
    float tx_max = std::numeric_limits<float>::lowest();
    float ty_max = std::numeric_limits<float>::lowest();
    float map_size = static_cast<float>(mapper_config_.texture_map_size);
    while (in.good()) {
      std::getline(in,line);
      if (line.length() > 0) {
        if (line[0] == '#') {
          section_counter++;
        } else {
          // Handle data
          ss.str(line);
          if (section_counter == 1) {
            // Control points
            ss >> idx;
            control_pts_index_.push_back(idx);
          } else if (section_counter == 2) {
            // Triangles
            Triangle t;
            ss >> t;
            triangle_list_.push_back(t);
          }
          else if (section_counter == 3) {
            // Triangles
            Triangle t;
            ss >> t;
            t -= 1;
            extra_triangle_list_.push_back(t);
          }
          else if (section_counter == 4) {
            // Texture coordinate
            TCoord tcoord;
            ss >> tcoord;
            tex_coordinate_.push_back(tcoord);
            target_pts_.push_back(tcoord * map_size);
            const auto& tc = target_pts_.back();
            tx_min = tc.x_ < tx_min ? tc.x_ : tx_min;
            ty_min = tc.y_ < ty_min ? tc.y_ : ty_min;
            tx_max = tc.x_ > ty_max ? tc.x_ : ty_max;
            ty_max = tc.y_ > ty_max ? tc.y_ : ty_max;
          }
          ss.clear();
        }
      }
    }
    in.close();
    tex_bbox_.min_.x_ = tx_min;
    tex_bbox_.min_.y_ = ty_min;
    tex_bbox_.min_.z_ = 0.f;
    tex_bbox_.max_.x_ = tx_max;
    tex_bbox_.max_.y_ = ty_max;
    tex_bbox_.max_.z_ = 0.f;
    error = 0;
  }
  return error;
}

/*
 *  @name   ~TextureMapper
 *  @brief  Destructor
 */
TextureMapper::~TextureMapper() {
}

/*
 *  @name Initialize
 *  @fn void Initialize(LTS5::Mesh<float>* face_mesh)
 *  @brief  Initialize texture mapper module
 *  @param[in]  face_mesh Mesh used to map texture on
 */
void TextureMapper::Initialize(LTS5::Mesh<float>* face_mesh) {
  // Set texture coordinate
  // --------------------------------------------------------------
  face_mesh->set_tex_coord(tex_coordinate_);
  // Add extra triangle
  // --------------------------------------------------------------
  auto& tri = face_mesh->get_triangle();
  for (const auto& t : extra_triangle_list_) {
    tri.push_back(t);
  }
  // Downsample texture map + define close neighbors
  this->DownsampleTextureMap(face_mesh);
  // Triangle map
  this->ComputeTriangleMap();
  // Build connectivity
  // --------------------------------------------------------------
  face_mesh->BuildConnectivity();
  // Done
  is_initialized_ = true;
}

#pragma mark -
#pragma mark Process

/*
 *  @name Process
 *  @fn void Process(const BaseStream::Buffer& images,
                     const std::vector<cv::Mat>& projection_mat,
                     const std::vector<cv::Mat>& landmarks,
                     LTS5::Mesh<float>* face_mesh)
 *  @brief  Map texture for a given set of images
 *  @param[in]  images          Set of images used to map texture
 *  @param[in]  projection_mat  Projection matrix for each views
 *  @param[in]  landmarks       List of landmarks detected by the face tracker
 *  @param[in,out]  face_mesh   Reconstructed mesh
 *  @return -1 if error, 0 otherwise
 */
int TextureMapper::Process(const BaseStream::Buffer& images,
                           const std::vector<cv::Mat>& projection_mat,
                           const std::vector<cv::Mat>& landmarks,
                           LTS5::Mesh<float>* face_mesh) {
  // Check first if module is init, otherwise init it
  int err = -1;
  if (!is_initialized_) {
    this->Initialize(face_mesh);
  }
  // If not image skip texture generation
  if (!images.host.empty()) {
    // Generate texture
    err = this->GenerateTextureMap(face_mesh,
                                   images.host,
                                   projection_mat,
                                   landmarks);
  }
  return err;
}

#pragma mark -
#pragma mark Private

/*
 *  @name ProjectionMat2RotationMat
 *  @fn void ProjectionMat2RotationMat(const cv::Mat& proj_mat, cv::Mat* rot_mat)
 *  @brief  Extract rotation from projection matrix
 *  @param[in]  proj_mat  Projection matrix
 *  @param[out] rot_mat   Rotation matrix
 */
void TextureMapper::ProjectionMat2RotationMat(const cv::Mat& proj_mat,
                                              cv::Mat* rot_mat) {
  // Define output
  rot_mat->create(3, 3, CV_32FC1);
  // Get first row
  rot_mat->at<float>(0, 0) = proj_mat.at<float>(0, 0);
  rot_mat->at<float>(0, 1) = proj_mat.at<float>(0, 1);
  rot_mat->at<float>(0, 2) = proj_mat.at<float>(0, 2);
  // Second row
  rot_mat->at<float>(1, 0) = proj_mat.at<float>(1, 0);
  rot_mat->at<float>(1, 1) = proj_mat.at<float>(1, 1);
  rot_mat->at<float>(1, 2) = proj_mat.at<float>(1, 2);
  // Remove scale effect
  cv::normalize(rot_mat->row(0), rot_mat->row(0));
  cv::normalize(rot_mat->row(1), rot_mat->row(1));
  // Get 3rd dimension
  rot_mat->at<float>(2, 0) = ((rot_mat->at<float>(0, 1) *
                               rot_mat->at<float>(1, 2)) -
                              ((rot_mat->at<float>(0, 2)) *
                               (rot_mat->at<float>(1, 1))));
  rot_mat->at<float>(2, 1) = ((rot_mat->at<float>(0, 2) *
                               rot_mat->at<float>(1, 0)) -
                              ((rot_mat->at<float>(0, 0)) *
                               (rot_mat->at<float>(1, 2))));
  rot_mat->at<float>(2, 2) = ((rot_mat->at<float>(0, 0) *
                               rot_mat->at<float>(1, 1)) -
                              ((rot_mat->at<float>(0, 1)) *
                               (rot_mat->at<float>(1, 0))));
  rot_mat->row(1) *= -1.f;
  rot_mat->row(2) *= -1.f;
}

/*
 *  @name GenerateTextureMap
 *  @fn void GenerateTextureMap(const LTS5::Mesh<float>* mesh,
                                const std::vector<cv::Mat>& images,
                                const std::vector<cv::Mat>& proj_mat,
                                const std::vector<cv::Mat>& landmarks,
                                const bool unwrap)
 *  @brief  Generate texture map
 *  @param[in]  mesh      Mesh from where to compute texture map
 *  @param[in]  images    Input images
 *  @param[in]  proj_mat  Projection matrix
 *  @param[in]  landmarks List of landmarks
 *  @param[in]  unwrap    Indicate if 3D model is unwrap or not
 *  @return -1 if error, 0 otherwise
 */
int TextureMapper::GenerateTextureMap(const LTS5::Mesh<float>* mesh,
                                      const std::vector<cv::Mat>& images,
                                      const std::vector<cv::Mat>& proj_mat,
                                      const std::vector<cv::Mat>& landmarks) {
  // Setup control points
  image_pts_.resize(images.size());
  this->SetupControlPoints(mesh, proj_mat);
  // Compute triangle visibility
  this->ComputeVertexVisibilityAndWeight(mesh, proj_mat);
  // Warp each view into texture map
  return this->WarpView(images, low_res_triangulation_);
}

/*
 *  @name SetupControlPoints
 *  @fn void SetupControlPoints_v1(const LTS5::Mesh<float>* mesh,
                                   const cv::Mat& proj_mat)
 *  @brief  Setup control points onto image plane, either from face tracker
 *          detection or by projecting 3d pts
 *  @param[in]  mesh      Where control points are stored
 *  @param[in]  proj_mat  List of projection matrix
 */
void TextureMapper::SetupControlPoints(const LTS5::Mesh<float>* mesh,
                                       const std::vector<cv::Mat>& proj_mat) {
  // Loop over all set of landmarks
  const auto& triangle = mesh->get_triangle();
  const auto& vertex = mesh->get_vertex();
  const size_t n_pts = static_cast<int>(interpolation_map_.size());
  image_pts_.resize(proj_mat.size());
  bool v_sampled = !sampled_vertex_.empty();
  for (int i = 0; i < proj_mat.size(); ++i) {
    if (!proj_mat[i].empty()) {
      // Setup container
      image_pts_[i].resize(n_pts);
      auto& pts = image_pts_[i];
      // Access projection matrix by ptr
      const float* p_mat = reinterpret_cast<const float*>(proj_mat[i].data);
      // Loop over control pts
      for (int p = 0; p < n_pts; ++p) {
        // Interpolate
        if (!v_sampled) {
          const auto map = interpolation_map_[p];
          const auto& tri = triangle[map.tri_index];
          const auto& va = vertex[tri.x_];
          const auto& vb = vertex[tri.y_];
          const auto& vc = vertex[tri.z_];
          const auto v = (va * map.u + vb * map.v + vc * map.w);
          sampled_vertex_.push_back(v);
        }
        // Image pts
        LTS5::Vector2<float>& p_2d = pts[p];
        // Project pts into image
        const LTS5::Vector3<float> &p_3d = sampled_vertex_[p];
        // Project
        p_2d.x_ = (p_mat[0] * p_3d.x_ +
                   p_mat[1] * p_3d.y_ +
                   p_mat[2] * p_3d.z_ +
                   p_mat[3]);
        p_2d.y_ = (p_mat[4] * p_3d.x_ +
                   p_mat[5] * p_3d.y_ +
                   p_mat[6] * p_3d.z_ +
                   p_mat[7]);
      }
      v_sampled = true;
    } else {
      image_pts_[i].clear();
    }
  }
}

/*
 * @name  ComputeTriangleMap
 * @fn    void ComputeTriangleMap()
 * @brief Compute a map where each pixel has the triangle number that belong
 *        to it in the texture map
 */
void TextureMapper::ComputeTriangleMap() {
  // Setup map
  triangle_map_.create(mapper_config_.texture_map_size,
                       mapper_config_.texture_map_size,
                       CV_32FC4);
  triangle_map_.setTo(-1);
  // Loop overall triangle in texture map
  const auto& triangle = low_res_triangulation_; //mesh->get_triangle();
  const auto& tcoord = low_res_target_pts_; // mesh->get_tex_coord();
  for (int t = 0; t < triangle.size(); ++t) {
    // Get tri + target points
    const auto& tri = triangle[t];
    const Vec2& p0 = tcoord[tri.x_];
    const Vec2& p1 = tcoord[tri.y_];
    const Vec2& p2 = tcoord[tri.z_];
    // Define bbox
    float xmin = -1.f, xmax = -1.f;
    float ymin = -1.f, ymax = -1.f;
    if (p0.x_ <= p1.x_ && p0.x_ <= p2.x_) {
      // p0.x == xmin
      xmin = p0.x_;
      // xmax ?
      xmax = p1.x_ > p2.x_ ? p1.x_ : p2.x_;
    } else if (p1.x_ <= p0.x_ && p1.x_ <= p2.x_) {
      // p1.x == xmin
      xmin = p1.x_;
      // xmax ?
      xmax = p0.x_ > p2.x_ ? p0.x_ : p2.x_;
    } else {
      // p2.x == xmin
      xmin = p2.x_;
      xmax = p1.x_ > p0.x_ ? p1.x_ : p0.x_;
    }
    if (p0.y_ <= p1.y_ && p0.y_ <= p2.y_) {
      // p0.y == ymin
      ymin = p0.y_;
      // xmax ?
      ymax = p1.y_ > p2.y_ ? p1.y_ : p2.y_;
    } else if (p1.y_ <= p0.y_ && p1.y_ <= p2.y_) {
      // p1.y == ymin
      ymin = p1.y_;
      // ymax ?
      ymax = p0.y_ > p2.y_ ? p0.y_ : p2.y_;
    } else {
      // p2.y == xmin
      ymin = p2.y_;
      ymax = p1.y_ > p0.y_ ? p1.y_ : p0.y_;
    }
    // Loop over patch define by bbox
    xmin = std::round(xmin);
    xmax = std::round(xmax);
    ymin = std::round(ymin);
    ymax = std::round(ymax);
    assert(xmin <= xmax && ymin <= ymax);

    // Get barycentric coordinate of the point
    for (float x = xmin; x <= xmax; ++x) {
      for (float y = ymin; y <= ymax; ++y) {
        const Vec2 v0 = p1 - p0;
        const Vec2 v1 = p2 - p0;
        const Vec2 v2 = Vec2(x, y) - p0;
        const float d00 = v0 * v0;
        const float d01 = v0 * v1;
        const float d11 = v1 * v1;
        const float d20 = v2 * v0;
        const float d21 = v2 * v1;
        const float denom = d00 * d11 - d01 * d01;
        const float v = (d11 * d20 - d01 * d21) / denom;
        const float w = (d00 * d21 - d01 * d20) / denom;
        const float u = (1.0f - v - w);

        if ((v >= 0.f) && (w >= 0.f) && (v + w <= 1.f)) {
          // Inside triangle
          const float tf = static_cast<float>(t);
          triangle_map_.at<cv::Vec4f>(y, x) = cv::Vec4f(tf,
                                                        u,
                                                        v,
                                                        w);
        }
      }
    }
  }
  //LTS5::SaveMatToBin("3DRec/dbg/tri_map.bin",triangle_map_);
}

/*
 * @name  DownsampleTextureMap
 * @fn void DownsampleTextureMap(const LTS5::Mesh<float>* mesh);
 * @brief Uniformly downsample the texture map and compute triangle close
 *        neighbors
 * @param[in] mesh    Mesh holding texture coordinate.
 */
void TextureMapper::DownsampleTextureMap(const LTS5::Mesh<float>* mesh) {
  using Vertex = Mesh<float>::Vertex;

  // Uniformly downsample the texture map
  // -----------------------------------------------------------------
  Mesh<float> texture_mesh;
  Mesh<float> downsampled_mesh;
  auto& low_tex_vertex = texture_mesh.get_vertex();
  const auto& tcoord = mesh->get_tex_coord();
  float sign = 1.f;
  for(const auto& v : tcoord) {
    low_tex_vertex.push_back({v.x_,
                              v.y_,
                              std::numeric_limits<float>::epsilon() * sign});
    sign *= -1.f;
  }
  texture_mesh.set_triangle(mesh->get_triangle());
  // Downsample
  IsotropicDiscreteRemesher<float> remesher;
  remesher.Initialize(&texture_mesh,
                      static_cast<int>(low_tex_vertex.size() /
                                       mapper_config_.d_factor));
  remesher.Process(&downsampled_mesh);
  // Copy triangle
  low_res_triangulation_ = downsampled_mesh.get_triangle();
  low_res_target_pts_.clear();
  downsampled_mesh.ComputeHalfedges();

  // Build connectivity
  low_res_connectivity_.resize(low_tex_vertex.size(), std::vector<int>(0));
  const int n_tri = static_cast<int>(low_res_triangulation_.size());
  for (int i = 0; i < n_tri; ++i) {
    const int* t_ptr = &(low_res_triangulation_[i].x_);
    for (int e = 0; e < 3; ++e) {
      int idx_in = t_ptr[e];
      int idx_out_1 = t_ptr[(e + 1) % 3];
      int idx_out_2 = t_ptr[(e + 2) % 3];
      // Add to connectivity list
      low_res_connectivity_[idx_in].push_back(idx_out_1);
      low_res_connectivity_[idx_in].push_back(idx_out_2);
    }
  }

  // Compute low to high resolution correspondance for each vertices
  // -----------------------------------------------------------------
  auto& low_res_vertex = downsampled_mesh.get_vertex();
  const auto& vertex = texture_mesh.get_vertex();
  const auto& triangle = texture_mesh.get_triangle();
  using Tree = LTS5::OCTree<float>;
  interpolation_map_.resize(low_res_vertex.size());
  Tree input_tree;
  using PrimitiveType = LTS5::Mesh<float>::PrimitiveType;
  input_tree.Insert(texture_mesh, PrimitiveType::kTriangle);
  input_tree.Build();
  std::vector<size_t> neighbors;
  std::vector<int> tri_index;
  for (int i = 0; i < low_res_vertex.size(); ++i) {
    // Pick vertex to find correspondance
    const Vertex& v = low_res_vertex[i];
    // Get one ring neighbors to find search radius
    downsampled_mesh.OneRingNeighbors(i, &neighbors);
    // Find maximum distance + define sphere
    auto it = std::max_element(neighbors.begin(),
                               neighbors.end(),
                               [&](const size_t& lhs, const size_t& rhs)->bool{
                                 const auto& l_p = low_res_vertex[lhs];
                                 const auto& r_p = low_res_vertex[rhs];
                                 const auto& l_dist = (v - l_p).Norm();
                                 const auto& r_dist = (v - r_p).Norm();
                                 return l_dist < r_dist;
                               });
    LTS5::Sphere<float> sph;
    sph.radius_ = 1.5f * (v - low_res_vertex[*it]).Norm();
    sph.center_ = v;
    // Query
    input_tree.RadiusNearestNeighbor(sph, &tri_index);
    // Find closest
    int min_dist_idx = -1;
    float min_dist = std::numeric_limits<float>::max();
    for (const auto idx : tri_index) {
      // Find closest triangle next to sphere center
      const auto& tri = triangle[idx];
      Vec3 A = vertex[tri.x_];
      Vec3 B = vertex[tri.y_];
      Vec3 C = vertex[tri.z_];
      const auto c = (A + B + C) / 3.f;

      float d = (c - sph.center_).Norm();
      if (d < min_dist) {
        min_dist = d;
        min_dist_idx = idx;
      }
    }
    assert(min_dist_idx != -1);
    // Compute correspondance, i.e. barycentric coordinate
    const auto& tri = triangle[min_dist_idx];
    Vec3 A = vertex[tri.x_];
    Vec3 B = vertex[tri.y_];
    Vec3 C = vertex[tri.z_];
    // Coordinates
    Vec3 v0 = B - A;
    Vec3 v1 = C - A;
    Vec3 v2 = sph.center_ - A;
    float d00 = v0 * v0;
    float d01 = v0 * v1;
    float d11 = v1 * v1;
    float d20 = v2 * v0;
    float d21 = v2 * v1;
    float denom = d00 * d11 - d01 * d01;
    interpolation_map_[i].v = (d11 * d20 - d01 * d21) / denom;
    interpolation_map_[i].w = (d00 * d21 - d01 * d20) / denom;
    interpolation_map_[i].u = (1.0f -
                               interpolation_map_[i].v -
                               interpolation_map_[i].w);
    interpolation_map_[i].tri_index = min_dist_idx;

    const TCoord& ta = tcoord[tri.x_];
    const TCoord& tb = tcoord[tri.y_];
    const TCoord& tc = tcoord[tri.z_];
    TCoord sampled_tc = (ta * interpolation_map_[i].u +
                         tb * interpolation_map_[i].v +
                         tc * interpolation_map_[i].w);
    sampled_tc *= mapper_config_.texture_map_size;
    low_res_target_pts_.push_back(sampled_tc);
  }
  downsampled_mesh.set_tex_coord(low_res_target_pts_);


  /*// Dump stuff
  cv::Mat low_res_tex(low_res_target_pts_.size(), 2, CV_32FC1, (void*)low_res_target_pts_.data());
  LTS5::SaveMatToBin("3DRec/dbg/low_tex.bin", low_res_tex);
  cv::Mat low_res_tri(low_res_triangulation_.size(), 3, CV_32SC1, (void*)low_res_triangulation_.data());
  LTS5::SaveMatToBin("3DRec/dbg/low_tri.bin", low_res_tri);
  cv::Mat tex(mesh->get_tex_coord().size(), 2, CV_32FC1, (void*)mesh->get_tex_coord().data());
  LTS5::SaveMatToBin("3DRec/dbg/tex.bin", tex);
  cv::Mat tri(mesh->get_triangle().size(), 3, CV_32SC1, (void*)mesh->get_triangle().data());
  LTS5::SaveMatToBin("3DRec/dbg/tri.bin", tri);
*/

  // Find closest neighbors
  // -----------------------------------------------------------------
  Vertex v;
  for (int i = 0; i < low_res_vertex.size(); ++i) {
    v.x_ = low_res_target_pts_[i].x_;
    v.y_ = low_res_target_pts_[i].y_;
    v.z_ = 0.f;
    low_res_vertex[i] = v;
  }
  LTS5::OCTree<float> tree;
  using PrimitiveType = LTS5::Mesh<float>::PrimitiveType;
  tree.Insert(downsampled_mesh, PrimitiveType::kPoint);
  tree.Build();
  // Define radius, cover 99% of the surface by using 3*sigma
  float radius = 10.f *  std::sqrt(1.f / (2.f * mapper_config_.w_u));
  // Query
  triangle_vertex_weight_selection_.resize(low_res_triangulation_.size());
  LTS5::Sphere<float> sphere;
  std::vector<int> idx;
  const auto& low_tcoord = low_res_target_pts_;
  for (int t = 0; t < low_res_triangulation_.size(); ++t)  {
    // Get triangle center
    const auto& tri = low_res_triangulation_[t];
    auto cog = low_tcoord[tri.x_] + low_tcoord[tri.y_] + low_tcoord[tri.z_];
    cog /= 3.f;
    // Define query
    sphere.center_.x_ = cog.x_;
    sphere.center_.y_ = cog.y_;
    sphere.center_.z_ = 0.f;
    sphere.radius_ = radius;
    tree.RadiusNearestNeighbor(sphere, &idx);
    if (idx.empty()) {
      idx.push_back(tri.x_);
      idx.push_back(tri.y_);
      idx.push_back(tri.z_);
    }

    // Sort texture coordinate by increasing distance from barycenter
    Vec2 center(sphere.center_.x_, sphere.center_.y_);
    auto fcn = [&](const int& lhs, const int& rhs)->bool{
      // Get tex coordinate
      const auto& vec_lhs = low_res_target_pts_[lhs];
      const auto& vec_rhs = low_res_target_pts_[rhs];
      // Compute distance
      const float dist_lhs = (center - vec_lhs).Norm();
      const float dist_rhs = (center - vec_rhs).Norm();
      return dist_lhs < dist_rhs;
    };
    std::sort(idx.begin(), idx.end(), fcn);
    // Add to the per-triangle list
    for (int sel = 0; sel < idx.size(); ++sel) {
      if (sel < mapper_config_.per_vertex_thresh) {
        triangle_vertex_weight_selection_[t].push_back(idx[sel]);
      } else {
        break;
      }
    }
  }
}

/*
 * @name  ComputeVertexVisibilityAndWeight
 * @fn    void ComputeTriangleVisibility(const LTS5::Mesh<float>* mesh,
                               const std::vector<cv::Mat>& proj_mat)
 * @brief In one pass compute the following coefficients :
 *        vertex visibility by view
 *        vertex visibility in the frontal view
 *        vertex weight for a given view
 * @param[in] mesh                    Reconstructed mesh
 * @param[in] proj_mat                Projection matrix for each views
 */
void TextureMapper::ComputeVertexVisibilityAndWeight(const LTS5::Mesh<float>* mesh,
                                                     const std::vector<cv::Mat>& proj_mat) {
  // Alloate container
  vertex_view_visibility_.create(static_cast<int>(interpolation_map_.size()),
                                 static_cast<int>(proj_mat.size()),
                                 CV_32SC1);
  vertex_visibility_.create(static_cast<int>(interpolation_map_.size()), 1, CV_32SC1);
  vertex_view_weight_.create(static_cast<int>(interpolation_map_.size()),
                             static_cast<int>(proj_mat.size()),
                             CV_32FC1);

  // Define rotation matrix
  std::vector<Vector3<float>> cam_dir;
  for (int i = 0; i < proj_mat.size(); ++i) {
    cv::Mat R;
    if (!proj_mat[i].empty()) {
      this->ProjectionMat2RotationMat(proj_mat[i], &R);
      const Vector3<float>* ptr = reinterpret_cast<Vector3<float>*>(R.data);
      cam_dir.push_back(ptr[2]);
      //cam_dir.back().y_ = 0.f;
      cam_dir.back().Normalize();
    } else {
      cam_dir.push_back(Vector3<float>(-5.f, -5.f, -5.f));
    }
  }

  // Loop over each triangles
  Parallel::For(interpolation_map_.size(),
                [&] (const size_t& i) {
                  // Compute normals
                  const auto& con = low_res_connectivity_[i];
                  const Vertex& A = sampled_vertex_[i];
                  const auto n_con = con.size();
                  Normal weighted_n;
                  for (size_t j = 0; j < n_con; j += 2) {
                    const Vertex& B = sampled_vertex_[con[j]];
                    const Vertex& C = sampled_vertex_[con[j + 1]];
                    // Define edges AB, AC
                    Edge AB = B - A;
                    Edge AC = C - A;
                    // Compute surface's normal (triangle ABC)
                    Normal n = AB ^ AC;
                    n.Normalize();
                    // Stack each face contribution and weight with angle
                    AB.Normalize();
                    AC.Normalize();
                    float dot = AB * AC;
                    dot = dot > 1.f ? 1.f : dot < -1.f ? -1.f : dot;
                    const float angle = std::acos(dot);
                    weighted_n += (n * angle);
                  }
                  // normalize and set
                  weighted_n.Normalize();
                  // Loop over each rotation matrix
                  for (int v = 0; v < cam_dir.size(); ++v) {
                    // Get angle
                    const auto& dir = cam_dir[v];
                    if (dir.x_ != -5.f) {
                      // View visibility
                      float d = dir * weighted_n;
                      vertex_view_visibility_.at<int>(i, v) = d > angle_thr ?
                                                              1 : 0;

                      // Weight
                      float e = mapper_config_.w_n * ((1.f - d) * (1.f - d));
                      vertex_view_weight_.at<float>(i, v) = std::exp(-e);

                    } else {
                      vertex_view_visibility_.at<int>(i, v) = 0;
                      vertex_view_weight_.at<float>(i, v) = 0.f;
                    }
                  }
                  // Frontal
                  float d = weighted_n * Vec3(0.f, 0.f, 1.f);
                  vertex_visibility_.at<int>(i) = d > front_angle_thr ? 1 : 0;
                });
  // Export stuff
  /*cv::Mat cams(cam_dir.size(), 3, CV_32FC1, (void*)cam_dir.data());
  cv::Mat tri(mesh->get_triangle().size(), 3, CV_32SC1, (void*)mesh->get_triangle().data());
  cv::Mat ver(mesh->get_vertex().size(), 3, CV_32FC1, (void*)mesh->get_vertex().data());
  cv::Mat nrm(normals.size(), 3, CV_32FC1, (void*)normals.data());
  cv::Mat sampled_tri(low_res_triangulation_.size(), 3, CV_32SC1, (void*)low_res_triangulation_.data());


  LTS5::SaveMatToBin("3DRec/dbg/vertex_view_visi.bin", vertex_view_visibility_);
  LTS5::SaveMatToBin("3DRec/dbg/vertex_visi.bin", vertex_visibility_);
  LTS5::SaveMatToBin("3DRec/dbg/vertex_weight.bin", vertex_view_weight_);
  LTS5::SaveMatToBin("3DRec/dbg/tri.bin", tri);
  LTS5::SaveMatToBin("3DRec/dbg/vertex.bin", ver);
  LTS5::SaveMatToBin("3DRec/dbg/normal.bin", nrm);
  LTS5::SaveMatToBin("3DRec/dbg/sampled_tri.bin", sampled_tri);
  LTS5::SaveMatToBin("3DRec/dbg/sampled_n.bin", sample_n);
  LTS5::SaveMatToBin("3DRec/dbg/cams.bin", cams);*/
}

/*
 * @name  WarpView
 * @fn    void WarpView(const std::vector<cv::Mat>& image)
 * @brief Warp each view into texture map space, including visibility test
 * @param[in] image    List of input view to warp into texture map coordinate
 * @param[in] triangle Triangles to use will warping
 * @return -1 if error, 0 otherwise
 */
int TextureMapper::WarpView(const std::vector<cv::Mat>& image,
                             const std::vector<Triangle>& triangle) {
  // Loop over texture coordinate space bbox
  int x_min = static_cast<int>(std::floor(tex_bbox_.min_.x_));
  int x_max = static_cast<int>(std::ceil(tex_bbox_.max_.x_));
  int y_min = static_cast<int>(std::floor(tex_bbox_.min_.y_));
  int y_max = static_cast<int>(std::ceil(tex_bbox_.max_.y_));
  auto xrange = static_cast<size_t>(x_max - x_min);

  has_some_pixel_ = false;

  /*std::vector<cv::Mat> warped_view;
  for (int i = 0; i < image.size(); ++i) {
    warped_view.push_back(cv::Mat(texture_map_.rows,
                                  texture_map_.cols,
                                  texture_map_.type(),
                                  cv::Scalar_<uchar>(0)));
  }*/

  Parallel::For(xrange,
                [&](const size_t& xx) {
    int x = x_min + static_cast<int>(xx);
    std::vector<float> pixel_w(image.size());
    std::vector<float> pixel_value(image.size());
    uchar color = 0;
    for (int y = y_min; y < y_max; ++y) {
      // Get pixel info
      const cv::Vec4f p_info = triangle_map_.at<cv::Vec4f>(y, x);
      int t_idx = static_cast<int>(p_info[0]);
      if (p_info[0] != -1.f) {
        // In a triangle
        const Triangle& tri = triangle[t_idx];
        const auto& vertex_weight_sel = triangle_vertex_weight_selection_[t_idx];
        const size_t vertex_weight_sz = vertex_weight_sel.size();
        // Sample each view if visible
        const float uu = p_info[1];
        const float vv = p_info[2];
        const float ww = p_info[3];
        float sum_wip = 0.f;
        assert(std::abs(uu+vv+ww-1.f) <= std::numeric_limits<float>::epsilon());
        for (int v = 0; v < image.size(); ++v) {
          // Visible
          int visi = vertex_view_visibility_.at<int>(tri.x_, v) +
                     vertex_view_visibility_.at<int>(tri.y_, v) +
                     vertex_view_visibility_.at<int>(tri.z_, v);
          if (visi > 0) {
            // Visible define sampling location with barycentric coordinate
            const auto& pts = image_pts_[v];
            const TCoord& p0 = pts[tri.x_];
            const TCoord& p1 = pts[tri.y_];
            const TCoord& p2 = pts[tri.z_];
            Vec2 p = (uu * p0) + (vv * p1) + (ww * p2);

            if ((p.x_ < 0.f ||
                 p.x_ > image[v].cols ||
                 p.y_ < 0.f ||
                 p.y_ > image[v].rows)) {
              color = 0;
              pixel_w[v] = -1.f;
            } else {
              // Interpolate
              has_some_pixel_ = true;
              int X = static_cast<int>(p.x_);
              int Y = static_cast<int>(p.y_);
              // Get delta
              float dx = p.x_ - std::floor(p.x_);
              float dy = p.y_ - std::floor(p.y_);
              float dx1 = 1.f - dx;
              float dy1 = 1.f - dy;
              // Interpolate
              const uchar* p_c00 = image[v].data + (X + Y * image[v].cols);
              const uchar* p_c01 = p_c00 + 1;
              const uchar* p_c10 = p_c00 + image[v].cols;
              const uchar* p_c11 = p_c10 + 1;
              // Compute weight
              int weight[4];
              weight[0] = static_cast<int>(dx1 * dy1 * 256.f);
              weight[1] = static_cast<int>(dx * dy1 * 256.f);
              weight[2] = static_cast<int>(dx1 * dy * 256.f);
              weight[3] = static_cast<int>(dx * dy * 256.f);
              // Return value
              color = static_cast<uchar >((*p_c00 * weight[0] +
                                           *p_c01 * weight[1] +
                                           *p_c10 * weight[2] +
                                           *p_c11 * weight[3]) >> 8);
              //warped_view[v].at<uchar>(y, x) = color;
              // Set image
              pixel_value[v] = color;
              // Per-pixel weight
              float w_ip = 0.f;
              for (int k = 0; k < vertex_weight_sz; ++k) {
                int vertex_idx = vertex_weight_sel[k];
                if (vertex_visibility_.at<int>(vertex_idx) > 0) {
                  float dx = x - low_res_target_pts_[vertex_idx].x_;
                  float dy = y - low_res_target_pts_[vertex_idx].y_;
                  float e = mapper_config_.w_u * (dx*dx + dy*dy);
                  float coef = std::exp(-e) * vertex_view_weight_.at<float>(vertex_idx, v);
                  w_ip = w_ip + coef;
                }
              }
              pixel_w[v] = w_ip;
              sum_wip += w_ip;
            }
          } else {
            pixel_w[v] = -1.f;
          }
        }
        // Final blend
        float final_color = 0.f;
        if (sum_wip > 0.f) {
          for (int v = 0; v < pixel_w.size(); ++v) {
            if (pixel_w[v] != -1.f) {
              float part_color = (pixel_w[v]/sum_wip) * pixel_value[v];
              final_color += part_color ;
            }
          }
        }
        texture_map_.at<uchar>(y, x) = cv::saturate_cast<uchar>(final_color);
      }
    }
  });
  /*cv::imwrite("3DRec/tex_map0.jpg",warped_view[0]);
  cv::imwrite("3DRec/tex_map1.jpg",warped_view[1]);
  cv::imwrite("3DRec/tex.jpg",texture_map_);*/
  return (has_some_pixel_ == true ? 0 : -1);
}
}  // namespace LTS5

