/**
 *  @file   utilities.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 24/02/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <fstream>
#include <string>
#include <sstream>
#include <dirent.h>
#include <algorithm>
#include <assert.h>

#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/face_reconstruction/utilities.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @namespace  Utilities
 *  @brief      System tools like folder searching,...
 */
namespace Utilities
{
/*
 *  Convert number into strings
 *  @param Number      Number to convert
 *  @return            Input converted into string
 */
template<typename T>
std::string NumberToString ( T number ) {
  std::ostringstream ss;
  ss << number;
  return ss.str();
}

template std::string NumberToString<char>(char number);
template std::string NumberToString<short>(short number);
template std::string NumberToString<int>(int number);
template std::string NumberToString<float>(float number);
template std::string NumberToString<double>(double number);

/*
 *  Get a list with the name of the whole file into a specific directory
 *  @param[out] files   Returned value as a vector string
 *  @param[in]  dir     Full name the directory to check
 *  @param[in]  subdir  Subfolder name, initialise with ""
 *  @param[in]  ext     Extension file type, DAFAULT = ".jpg"
 */
/*void GetFileListing(std::vector<std::string> &files,
                    const std::string &dir,
                    const std::string &subdir,
                    const std::string &ext) {
  DIR *d;
  std::string aFolderName = std::string(dir);

  if (subdir.empty() == false) {
    // Add sub folder to the name
    if (aFolderName[aFolderName.length() - 1 ] != '/') {
      aFolderName += "/";
    }
    if (subdir[subdir.length() - 1] != '/') {
      aFolderName += subdir + "/";
    } else {
      aFolderName += subdir;
    }
  }

  if ((d = opendir(aFolderName.c_str())) == NULL) return;
  //if (dir.at(dir.length() - 1) != '/') dir += "/";// maybe without the "-1"

  std::string CapitalExt(ext.size(),'0');
  std::transform(ext.begin(),ext.end(),CapitalExt.begin(),toupper);

  struct dirent *dent;

  while ((dent = readdir(d)) != NULL) {
    if (std::string(dent->d_name) != "." &&
        std::string(dent->d_name) != "..") {
      // Check if it is a subfolder of a file
      if (dent->d_type == DT_DIR) {
        // Subfolder
        // std::cout << "subfolder" << std::endl;
        // Get files into the subfolder
        // Change folder
        GetFileListing(files, aFolderName,std::string(dent->d_name),ext);
      } else {
        // Get the name
        std::string filename = aFolderName + std::string(dent->d_name);

        // Check if there is an extension in the detected string
        unsigned long foundLower = filename.find(ext);
        unsigned long foundUpper = filename.find(CapitalExt);

        if (foundLower != std::string::npos ||
            foundUpper != std::string::npos) {
          // Extension found -> Add string to the list file
          files.push_back(filename);
        }
      }
    }
  }
  closedir(d);
}*/

/*
 *  @name   WriteMatToCsv
 *  @brief  Write OpenCV matrix (cv::Mat()) to .csv file
 *  @param  filename       Name of the created .csv file
 *  @param  matrix            Matrix to write
 */
int WriteMatToCsv(const std::string &filename,
                  const cv::Mat &matrix) {
  // Create file
  int ret = -1;
#if defined(DEBUG)
  std::ofstream file;
  file.open(filename.c_str());
  cv::Mat tmp = cv::Mat(matrix.rows,matrix.cols,CV_32FC1);

  if (file.is_open()) {
    ret = 0;
  }
  // Convert in float mat
  if (matrix.depth() != CV_32FC1) {
    matrix.convertTo(tmp, CV_32FC1);
  } else {
    tmp = matrix;
  }

  for (int row = 0; row < tmp.rows; row++) {
    for (int col = 0 ; col < tmp.cols; col++) {
      // Write the data at pos : [row,col]
      if (col != tmp.cols - 1) {
        file << tmp.at<float>(row,col) << ',';
      } else {
        file << tmp.at<float>(row,col);
      }
    }
    // Add \n at the end
    file << "\n";
  }
  // Close file
  file.close();
#endif
  return ret;
}

/*
 *  @name   WriteMatToBin
 *  @brief  Write OpenCV matrix (cv::Mat()) to .bin file
 *  @param  filename       Name of the created .bin file
 *  @param  matrix            Matrix to write
 */
int WriteMatToBin(const std::string &filename,const cv::Mat &matrix) {
  int ret = -1;
#if defined(DEBUG)
  // Support only one channel data
  assert(matrix.depth()==CV_32FC1 || matrix.depth()==CV_64FC1 ||
         matrix.depth()==CV_8UC1 || matrix.depth()==CV_8SC1 ||
         matrix.depth()==CV_16SC1 || matrix.depth()==CV_16UC1||
         matrix.depth()==CV_32SC1);

  // Open file
  FILE* file_id = std::fopen(filename.c_str(), "wb");
  if(file_id) {
    ret = 0;
    // Type and dimension
    int element_size = (int)matrix.elemSize();
    std::fwrite((const void*)&element_size, sizeof(int), 1, file_id);
    std::fwrite((const void*)&matrix.rows, sizeof(int), 1, file_id);
    std::fwrite((const void*)&matrix.cols, sizeof(int), 1, file_id);
    // Data
    std::fwrite((const void*)matrix.data,
                element_size,
                matrix.rows * matrix.cols,
                file_id);

    std::fflush(file_id);
    std::fclose(file_id);
  }
#endif
  return ret;
}


/*
 *  @name   ReadCsvToMat
 *  @brief  Fill a matrix with values from an .csv file
 *  @param  filename   Csv filename
 *  @param  matrix        Matrix to fill
 */
int ReadCsvToMat(const std::string &filename, cv::Mat* matrix) {

  std::ifstream input_stream(filename.c_str(),std::ifstream::in);
  std::string line,aSVal;
  int aNbRow = 0;
  int aNbCol = 0;
  int ret = -1;

  if (input_stream.is_open())
  {
    ret = 0;

    // File open
    std::getline(input_stream, line);

    // Count the number of columns
    aNbCol = (int)std::count(line.begin(), line.end(), ',') + 1;

    while (!input_stream.eof())
    {
      // Increase row conter
      aNbRow++;

      // Read next line
      std::getline(input_stream, line);
    }

    // Go back to the beginning of the files
    input_stream.clear();                    // => Clear error
    input_stream.seekg(0, input_stream.beg);

    // Allocate matrix
    matrix->create(aNbRow, aNbCol, CV_32FC1);

    // Read data
    float* data = (float*)matrix->data;
    float value;

    std::getline(input_stream, line);
    while (!input_stream.eof())
    {
      // Scan line
      std::stringstream aSStream(line);
      while (std::getline(aSStream,aSVal,','))
      {
        value = std::atof(aSVal.c_str());
        *data = value;
        data++;
      }

      std::getline(input_stream, line);
    }

    // Done
    input_stream.close();
  }

  return ret;
}

/*
 *  @name   ReadBinToMat
 *  @brief  Fill a matrix with values from an .csv file
 *  @param  filename   Csv filename
 *  @param  matrix        Matrix to fill
 *  @return -1 if error
 */
int ReadBinToMat(const std::string &filename, cv::Mat* matrix)
{
  return 0;
}

/*
 *  @name   SortByNumber
 *  @brief  Sort string by number increasing order
 *  @param  A   First string
 *  @param  B   Second string
 */
bool SortByNumber(const std::string& A,const std::string& B)
{
  // Extract image number
  // Find extension location
  int aAExtPos = (int)A.find_last_of(".");
  int aASlashPos = (int)A.find_last_of("/");
  int aBExtPos = (int)B.find_last_of(".");
  int aBSlashPos = (int)B.find_last_of("/");

  std::string aANum = A.substr(aASlashPos+1,aAExtPos-aASlashPos-1);
  std::string aBNum = B.substr(aBSlashPos+1,aBExtPos-aBSlashPos-1);

  return (std::atoi(aANum.c_str()) < std::atoi(aBNum.c_str()));
}

/*
 *  @name   GetRotationMatrix
 *  @brief  Generate rotation matrix for specific angles in degrees : Gamma, Theta, Phi
 *  @param  gamma          Rotation around Z axis
 *  @param  theta          Rotation around X axis
 *  @param  phi            Rotation around Y axis
 *  @param  rotation            Rotation matrix
 *  @param  rotation_type        Rotation type
 *  @param  aIsRad          Indicates if angle are in radians
 */
void GetRotationMatrix(const float gamma,
                       const float theta,
                       const float phi,
                       const RotationDimension& rotation_type,
                       const bool is_rad,
                       cv::Mat* rotation) {
  float sin_gamma=0.f,sin_theta=0.f,sin_phi=0.f;
  float cos_gamma=0.f,cos_theta=0.f,cos_phi=0.f;

  if (is_rad) {
    sin_gamma = std::sin(gamma);
    sin_theta = std::sin(theta);
    sin_phi = std::sin(phi);
    cos_gamma = std::cos(gamma);
    cos_theta = std::cos(theta);
    cos_phi = std::cos(phi);
  } else {
    sin_gamma = std::sin(gamma*(CV_PI/180.f));
    sin_theta = std::sin(theta*(CV_PI/180.f));
    sin_phi = std::sin(phi*(CV_PI/180.f));
    cos_gamma = cosf(gamma*(CV_PI/180.f));
    cos_theta = cosf(theta*(CV_PI/180.f));
    cos_phi = cosf(phi*(CV_PI/180.f));
  }

  rotation->at<float>(0,0) = cos_gamma*cos_phi;
  rotation->at<float>(0,1) = -sin_gamma*cos_phi;
  rotation->at<float>(0,2) = sin_phi;

  rotation->at<float>(1,0) = sin_gamma*cos_theta + sin_theta*sin_phi*cos_gamma;
  rotation->at<float>(1,1) = -sin_gamma*sin_theta*sin_phi + cos_theta*cos_gamma;
  rotation->at<float>(1,2) = -sin_theta*cos_phi;

  if (rotation_type == k3d3d) {
    rotation->at<float>(2,0) = sin_theta*sin_gamma - sin_phi*cos_theta*cos_gamma;
    rotation->at<float>(2,1) = -sin_gamma*sin_phi*cos_theta + sin_theta*cos_gamma;
    rotation->at<float>(2,2) = cos_theta*cos_phi;
  }
}

/*
 *  @name   GetRotationAngle
 *  @brief  Given a rotation matrix, computes the corresponding rotation angle.
 *  @param  rotation_matrix        Rotation matrix
 *  @param  gamma              Gamma angle
 *  @param  theta              Theta angle
 *  @param  phi                Phi angle
 */
void GetRotationAngle(const cv::Mat& rotation_matrix,
                      float* gamma,
                      float* theta,
                      float* phi) {
  const float aEps = 1E-20;

  assert(rotation_matrix.depth() == CV_32FC1 || rotation_matrix.depth() == CV_64FC1);

  cv::Mat rot;
  if (rotation_matrix.depth() == CV_64F)  {
    rotation_matrix.convertTo(rot, CV_32FC1);
  } else {
    rot = rotation_matrix;
  }

  *gamma = std::atan(-rotation_matrix.at<float>(0,1)/(rotation_matrix.at<float>(0,0)+aEps));
  *theta = std::atan(-rotation_matrix.at<float>(1,2)/(rotation_matrix.at<float>(2,2)+aEps));
  *phi = std::asin(rotation_matrix.at<float>(0,2));
}

/*
 *  @name   SwapMatrixRowsFloat
 *  @brief  Swap the ith row of A with the jth row of A (A Must be of type FLOAT)
 *  @param  matrix  Matrix
 *  @param  i   Src row to swap
 *  @param  j   Dst row to swap
 */
void SwapMatrixRowsFloat(cv::Mat* matrix,int i, int j) {
  float* data = (float*)(matrix->data);
  float* src = &data[i*(int)matrix->step1()];
  float* dst = &data[j*(int)matrix->step1()];
  int width = matrix->cols;

  float dummy;
  for (int k = 0; k < width; k++) {
    // Swap
    dummy = *src;
    *src = *dst;
    *dst = dummy;

    // Increase ptr
    src++;
    dst++;
  }
}

/*
 *  @name   SwapMatrixRowsDouble
 *  @brief  Swap the ith row of A with the jth row of A (A Must be of type FLOAT)
 *  @param  matrix  Matrix
 *  @param  i   Src row to swap
 *  @param  j   Dst row to swap
 */
void SwapMatrixRowsDouble(cv::Mat* matrix,int i, int j) {
  double* data = (double*)(matrix->data);
  double* src = &data[i*(int)matrix->step1()];
  double* dst = &data[j*(int)matrix->step1()];
  int width = matrix->cols;

  double dummy;
  for (int k = 0; k < width; k++) {
    // Swap
    dummy = *src;
    *src = *dst;
    *dst = dummy;

    // Increase ptr
    src++;
    dst++;
  }
}

/*
 *  @name   SubstractRowsByMultipleFloat
 *  @brief  Substract multiple mutiplied by row i from row j
 *  @param  matrix      Input matrix
 *  @param  i       Row to multiply
 *  @param  j       Row to substract to
 *  @param  multiple    Value to multiply
 */
void SubstractRowsByMultipleFloat(cv::Mat* matrix,int i, int j,float multiple) {
  float* data = (float*)(matrix->data);
  float* src = &data[i*(int)matrix->step1()];
  float* dst = &data[j*(int)matrix->step1()];
  int width = matrix->cols;

  float dummy;
  for (int k = 0; k < width; k++) {
    // Multiply and substract
    dummy = *dst;
    *dst = dummy - (*src * multiple);

    // Increase ptr
    src++;
    dst++;
  }
}

/*
 *  @name   SubstractRowsByMultipleDouble
 *  @brief  Substract multiple mutiplied by row i from row j
 *  @param  matrix      Input matrix
 *  @param  i       Row to multiply
 *  @param  j       Row to substract to
 *  @param  multiple    Value to multiply
 */
void SubstractRowsByMultipleDouble(cv::Mat* matrix,int i, int j,double multiple) {
  double* data = (double*)(matrix->data);
  double* src = &data[i*(int)matrix->step1()];
  double* dst = &data[j*(int)matrix->step1()];
  int width = matrix->cols;

  double dummy;
  for (int k = 0; k < width; k++) {
    // Multiply and substract
    dummy = *dst;
    *dst = dummy - (*src * multiple);

    // Increase ptr
    src++;
    dst++;
  }
}

/*
 *  @name   DivideRowFloat
 *  @brief  Divide a specific row by value value.
 *  @param  matrix      Input matrix
 *  @param  i       Row to divide
 *  @param  value    Value to divide by.
 */
void DivideRowFloat(cv::Mat* matrix, int i, float value) {
  float* data = (float*)(matrix->data);
  float* src = &data[i*(int)matrix->step1()];
  float aDiv = 1.f/value;
  int width = matrix->cols;

  for (int k = 0; k < width; k++) {
    // Divide
    *src = *src * aDiv;
    // Increase ptr
    src++;
  }
}

/*
 *  @name   DivideRowFloat
 *  @brief  Divide a specific row by value value.
 *  @param  matrix      Input matrix
 *  @param  i       Row to divide
 *  @param  value    Value to divide by.
 */
void DivideRowDouble(cv::Mat* matrix, int i, double value) {
  double* data = (double*)(matrix->data);
  double* src = &data[i*(int)matrix->step1()];
  double aDiv = 1.0/value;
  int width = matrix->cols;

  for (int k = 0; k < width; k++) {
    // Divide
    *src = *src * aDiv;
    // Increase ptr
    src++;
  }
}

/*
 *  @name   MatrixRowReductionEchelonForm
 *  @brief  Perform row reduction in form of echelon matrix
 *  @param  matrix  Matrix to reduce
 *  @see    http:// rosettacode.org/wiki/Reduced_row_echelon_form#C.2B.2B
 */
void MatrixRowReductionEchelonForm(cv::Mat* matrix) {
  int lead = 0;
  int num_row = matrix->rows;
  int num_col = matrix->cols;

  if (matrix->depth() == CV_32FC1) {
    // Loop over p  rows
    int i = 0;
    for (int r = 0; r < num_row; r++) {
      // Lead bigger than maximum col ?
      if (num_col <= lead) {
        break;
      }

      i = r;

      // Is it a zero column ?
      while (matrix->at<float>(i,lead) == 0.f) {
        i = i + 1;
        // Overflow ?
        if (i == num_row) {
          i = r;
          lead++;
          if (lead == num_col) {
            break;
          }
        }
      }

      // Swap rows i and r
      Utilities::SwapMatrixRowsFloat(matrix, i, r);

      if (matrix->at<float>(r,lead) != 0.f) {
        // Divide row r by matrix(r,lead);
        Utilities::DivideRowFloat(matrix,
                                  r,
                                  matrix->at<float>(r,lead));
      }

      // Apply reduction
      for (int i = 0; i < num_row; i++) {
        if (i != r) {
          // Subtract matrix[i, lead] multiplied by row r from row i
          Utilities::SubstractRowsByMultipleFloat(matrix,
                                                  r,
                                                  i,
                                                  matrix->at<float>(i,lead));
        }
      }

      lead++;
    }
  } else {
    // Loop over p  rows
    int i = 0;
    for (int r = 0; r < num_row; r++) {
      // Lead bigger than maximum col ?
      if (num_col <= lead) {
        break;
      }

      i = r;

      // Is it a zero column ?
      while (matrix->at<double>(i,lead) == 0.0) {
        i = i + 1;
        // Overflow ?
        if (i == num_row) {
          i = r;
          lead++;
          if (lead == num_col) {
            break;
          }
        }
      }

      // Swap rows i and r
      Utilities::SwapMatrixRowsDouble(matrix, i, r);

      if (matrix->at<double>(r,lead) != 0.0) {
        // Divide row r by matrix(r,lead);
        Utilities::DivideRowDouble(matrix, r, matrix->at<double>(r,lead));
      }

      // Apply reduction
      for (int i = 0; i < num_row; i++) {
        if (i != r) {
          // Subtract matrix[i, lead] multiplied by row r from row i
          Utilities::SubstractRowsByMultipleDouble(matrix,
                                                   r,
                                                   i,
                                                   matrix->at<double>(i,
                                                                      lead));
        }
      }
      lead++;
    }
  }
}

/*
 *  @name   LoadIndexListFromFile
 *  @brief  Load a list of index from file
 *  @param  index        Vector including the list of index
 *  @param  filename   File where indexes are stored
 */
void LoadIndexListFromFile(const std::string& filename,
                           std::vector<int>* index)
{
  std::ifstream input_stream(filename.c_str());
  std::string line;
  int value = 0;

  if (input_stream.is_open()) {
    // File open
    std::getline(input_stream, line);
    while (!input_stream.eof()) {
      // Scan line
      sscanf(line.c_str(), "%d",&value);
      index->push_back(value);
      // Read next line
      std::getline(input_stream, line);
    }
    // Close file
    input_stream.close();
  }
}

/*
 *  @name   GetCmdOption
 *  @brief  Give the content of a given option
 *  @param  begin       Begining of the command line args
 *  @param  end         End of the command line args
 *  @param  option      Which option to look for i.e : -e
 *  @return Return the command
 */
char* GetCmdOption(char ** begin,char ** end, const std::string & option) {
  char ** itr = std::find(begin, end, option);
  if (itr != end && ++itr != end) {
    return *itr;
  }
  return 0;
}

/*
 *  @name   CmdOptionExists
 *  @brief  Check if the command exist inside the argument from cmd line
 *  @param  begin       Begining of the command line args
 *  @param  end         End of the command line args
 *  @param  option      Which option to look for i.e : -e
 *  @return Indicate if a command is present or not
 */
bool CmdOptionExists(const char** begin,
                     const char** end,
                     const std::string& option) {
  return std::find(begin, end, option) != end;
}

/*
 *  @name   JumpToLineInFile
 *  @brief  Jump to specific line into a file
 *  @param  file           File input stream
 *  @param  line_number     Which line to jump
 */
void JumpToLineInFile(std::ifstream &file, int line_number)
{
  // Rewind to the beginning of the file
  file.seekg(0, std::ios::beg);

  // Jump over all line
  std::string line;
  for (int k = 0; k < line_number; k++){  getline(file, line);}
}

/*
 *  @name   ImageMedian
 *  @brief  Compute median value of the image (Single channel)
 *  @param  image      Image input
 */
float ImageMedian(const cv::Mat& image) {
  // Single channel ?
  assert(image.channels() == 1);

  // Access image data
  int image_size = image.cols*image.rows;
  const unsigned char* image_data_ptr = (const unsigned char*) image.data;
  std::vector<unsigned char> pixel_buffer = std::vector<unsigned char>(image_size);

  // Copy data and sort each element at the same time
  std::partial_sort_copy(&image_data_ptr[0],
                         &image_data_ptr[image_size-1],
                         pixel_buffer.begin(),
                         pixel_buffer.end());

  int index = -1;
  float median_value = -1.f;
  if((image_size%2)==0) {
    // Even
    index = image_size/2;
    median_value = ((float)pixel_buffer[index] + (float)pixel_buffer[index-1])/2;
  } else {
    // Odd
    index = (image_size - 1)/2;
    median_value = pixel_buffer[index];
  }
  return median_value;
}

/*
 *  @name   AutomaticCannyThresholdComputation
 *  @brief  Estimate canny threshold based on image properties
 *  @param  image          Input image
 *  @param  lower_bound_thresh     Lower threshold
 *  @param  aUpperbound     Upper threshold
 *  @param  sigma          Percentage (+/-)
 */
void AutomaticCannyThresholdComputation(const cv::Mat& image,
                                        int* lower_bound_thresh,
                                        int* upper_bound_thresh,
                                        const float sigma /* = 0.33f*/) {
  // Single channel
  assert(image.channels()==1);

  // Compute median
  float median_value = Utilities::ImageMedian(image);

  *lower_bound_thresh = (int)std::max(0.f, (1.f - sigma)*median_value);
  *upper_bound_thresh = (int)std::min(255.f,(1.f + sigma)*median_value);
}

/*
 *  @name   AutomaticCannyEdge
 *  @brief  Extract edge from single channel images
 *  @param  image      Source image
 *  @param  edges      Extracted edges
 *  @param  sigma      Threshold percentages (+/-)
 *  @see    http:// www.pyimagesearch.com/2015/04/06/zero-parameter-automatic-canny-edge-detection-with-python-and-opencv/
 */
void AutomaticCannyEdge(const cv::Mat& image,
                        cv::Mat* edges,
                        const float sigma /* = 0.33f*/) {
  // Single channel
  assert(image.channels() == 1);

  // Compute image median
  float median_value = Utilities::ImageMedian(image);

  // Compute lower and Upper threshold based on median value
  int lower = (int)std::max(0.f, (1.f - sigma)*median_value);
  int upper = (int)std::min(255.f,(1.f + sigma)*median_value);

  // Call OpenCV canny
  cv::blur(image, image, cv::Size(3,3));
  cv::Canny(image, *edges, lower, upper);
}
}
}




