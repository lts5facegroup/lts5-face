/**
 *  @file   face_model_fitter.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 01/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifdef __APPLE__
#include <dispatch/dispatch.h>
#endif

#include <string>
#include <algorithm>

#include "opencv2/core/core.hpp"
#include "opencv2/core/core_c.h"

#include "levmar.h"
#include "lbfgs.h"

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/face_reconstruction/face_model_fitter.hpp"
#include "lts5/face_reconstruction/utilities.hpp"
#include "lts5/face_reconstruction/data_generator.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"


#include "opencv2/highgui.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @name   ProjectionFunction
 *  @brief  Project points using full perspective model
 *  @param[in,out]  rigid_parameter Current estimation of the projection
 *                  parameters (i.e rotation angle and translation)
 *  @param[out]     new_landmark    New points on image plane with current
 *                                  parameters estimation.
 *  @param[in]      n               #Measurements
 *  @param[in]      m               #Variables
 */
void ProjectionFunction(float* rigid_parameter,
                        float* new_landmark,
                        int m,
                        int n,
                        void* data);

/**
 *  @name   JacobianFunction
 *  @brief  Compute jacobian matrix of the full perspective function.
 *  @param  rigid_parameter   Current estimation of the projection
 *                            parameters (i.e rotation angle and translation)
 *  @param  jacobian          Estimation of the Jacobian matrix for current
 *                            p parameters
 *  @param  n                 #Measurements
 *  @param  m                 #Variables
 */
void JacobianFunction(float* rigid_parameter,
                      float* jacobian,
                      int m,
                      int n,
                      void* data);

#pragma mark -
#pragma mark Constructor
/*
 *  @name   FaceModelFitter
 *  @brief  Constructor
 *  @param  configuration  Structure holding fitter's parameters
 */
FaceModelFitter::FaceModelFitter(const FitterParameter& configuration) {
  bool ok = true;
  int error = LTS5::ProcessError::ProcessErrorEnum::kSuccess;
  // Load model - if file is correct (check only extensio)
  size_t position_model = configuration.model_filename.find("_ocv.raw");
  size_t position_landmark = configuration.feature_index_filename.find(".cfg");
  if (position_model != std::string::npos &&
      position_landmark != std::string::npos) {
    face_model_ = new BilinearFaceModel(configuration.model_filename);
#ifdef HAS_CUDA_
    d_face_model_ = nullptr;
    //d_face_model_ = new CUDA::BilinearFaceModel(configuration.model_filename);
#endif
    // Load lankmarks - vertices indexes
    // Load mean shape
    // Load neutral expression weight vector
    error = LoadLandmarkVerticesIndex(configuration.feature_index_filename);
    error |= LoadInitParameters(configuration.model_filename,
                                configuration.close_triangulation_filename);

    if (error == LTS5::ProcessError::ProcessErrorEnum::kSuccess) {
      number_of_view_ = configuration.number_of_view;
      projection_mode_ = configuration.projection_model;
      // Init weight vectors vectors
      number_identity_ = face_model_->get_identity_length();
      number_expression_ = face_model_->get_expression_length();
      identity_weight_ = cv::Mat(number_identity_, 1, CV_32FC1, 0.f);
      identity_weight_tm1_ = cv::Mat(number_identity_, 1, CV_32FC1, 0.f);
      identity_weight_tm2_ = cv::Mat(number_identity_, 1, CV_32FC1, 0.f);
      // Identity aggregation
      Mt_ = cv::Mat(number_identity_, number_identity_, CV_32FC1, 0.f);
      Mtm1_ = cv::Mat(number_identity_, number_identity_, CV_32FC1, 0.f);
      Yt_ = cv::Mat(number_identity_, 1, CV_32FC1, 0.f);
      Ytm1_ = cv::Mat(number_identity_, 1, CV_32FC1, 0.f);
      st_ = 0.f;
      stm1_ = 0.f;
      // Expression + time penalization
      expression_weight_ = cv::Mat(number_expression_, 1, CV_32FC1, 0.f);
      expression_weight_tm1_ = cv::Mat(number_expression_, 1, CV_32FC1, 0.f);
      wexp_tm1_ = cv::Mat(number_expression_ - 1, 1, CV_32FC1, 0.f);
      expression_weight_tm2_ = cv::Mat(number_expression_, 1, CV_32FC1, 0.f);
      wexp_tm2_ = cv::Mat(number_expression_ - 1, 1, CV_32FC1, 0.f);
      is_tracking_ = false;

      // Init matrices
      number_of_vertex_ = face_model_->get_number_vertex();
      data_vertex_expr_ = cv::Mat(number_of_vertex_ * 3,
                                  expression_weight_.rows,
                                  CV_32FC1,
                                  0.f);
      // Features selection
      subset_vertex_identity_ = cv::Mat(number_2D_features_ * 3,
                                        identity_weight_.rows,
                                        CV_32FC1,
                                        0.f);
      subset_vertex_expression_ = cv::Mat(number_2D_features_ * 3,
                                          expression_weight_.rows,
                                          CV_32FC1,
                                          0.f);
      // 3D vertices
      face_vertex_ = cv::Mat(number_of_vertex_ * 3, 1, CV_32FC1, 0.f);
      landmark_vertex_ = cv::Mat(number_2D_features_ * 3, 1, CV_32FC1, 0.f);
      // Basis for rigid transformation estimation w/ weak perspective
      rigid_basis_ = cv::Mat(number_2D_features_ * 2,
                             6,
                             CV_32FC1,
                             0.f);
      // Camera information
      camera_configuration_ = *configuration.camera_setup;
      // Contour extraction, only if needed !!!
      if (configuration.extractor_configuration != "") {
        try {
          contour_extractor_ = new ContourExtractor(configuration.extractor_configuration);
          contour_candidate_ = contour_extractor_->get_contour_candidates();
          contour_candidate_surface_ = cv::Mat(3 *
                                               static_cast<int>(contour_candidate_.size()),
                                               1,
                                               CV_32FC1,
                                               0.f);
          contour_extractor_->set_mesh(contour_candidate_surface_);
        } catch(LTS5::ProcessError& e) {
          // Re throw
          throw;
        }

      } else {
        contour_extractor_ = nullptr;
        use_contour_ = false;
      }
      // Multi-view parameters init
      this->InitMultiViewVariables(number_of_view_);

      w_exp_solver_ = lbfgs_malloc(this->number_expression_ - 1);
      for (int i = 0; i < this->number_expression_ - 1; ++i) {
        w_exp_solver_[i] = 0.f;
      }

      // Done
      LTS5_LOG_DEBUG("Fitter loaded");
    } else {
      LTS5_LOG_ERROR("Error while load Fitter configuration files");
      initialized_ = false;
      ok = false;
    }
  } else {
    // Error with files -> module won't work !
    ok = false;
  }

  /*
   *  Handle error hear
   */
  if (ok) {
    initialized_ = true;
  } else {
    initialized_ = false;
    throw LTS5::ProcessError((LTS5::ProcessError::ProcessErrorEnum)error,
                             "Error in constructor",
                             FUNC_NAME);
  }
}

/*
 *  @name   FaceModelFitter
 *  @brief  Constructor
 */
FaceModelFitter::FaceModelFitter() {
}

#pragma mark -
#pragma mark Destructor

FaceModelFitter::~FaceModelFitter() {
  // Clear object
  if (face_model_) {
    delete face_model_;
    face_model_ = nullptr;
  }
  if (mean_shape_mesh_) {
    delete mean_shape_mesh_;
    mean_shape_mesh_ = nullptr;
  }
  if (collision_tree_) {
    delete collision_tree_;
    collision_tree_ = nullptr;
  }
  if (contour_extractor_) {
    delete contour_extractor_;
    contour_extractor_ = nullptr;
  }
  if (w_exp_solver_) {
    lbfgs_free(w_exp_solver_);
    w_exp_solver_ = nullptr;
  }
#ifdef HAS_CUDA_
  if (d_face_model_) {
    delete d_face_model_;
    d_face_model_ = nullptr;
  }
#endif
}

#pragma mark -
#pragma mark Initialization
/*
 *  @name   LoadLandmarkVerticesIndex
 *  @brief  Load the vertex indexes corresponding to the selected landmarks
 *          from config files
 *  @param  landmark_selection_file     Configuration file
 *  @return 0 if everything went fine otherwise -1.
 */
int FaceModelFitter::LoadLandmarkVerticesIndex(const std::string& file) {
  std::ifstream input_stream(file.c_str());
  std::string line;
  int index = 0;
  if (input_stream.is_open()) {
    // File open
    std::getline(input_stream, line);
    while (!input_stream.eof()) {
      // Scan line
      sscanf(line.c_str(), "%d", &index);
      feature_vertex_index_.push_back(index);
      // Read next line
      std::getline(input_stream, line);
    }
  } else {
    // Error
    return LTS5::ProcessError::ProcessErrorEnum::kErrOpeningFile;
  }
  // Everthing ok
  number_2D_features_ = static_cast<int>(feature_vertex_index_.size());
  return LTS5::ProcessError::kSuccess;
}

/*
 *  @name   LoadInitParameters
 *  @fn int LoadInitParameters(const std::string& init_param_file,
 const std::string& tri_file)
 *  @brief  Load the initial parameters used for fit model (Mean shape -
 *          neutral expression weights)
 *  @param[in]  init_param_file   Init parameters file
 *  @param[in]  tri_file          Triangulation filename
 *  @return     0 if everything went fine otherwise -1.
 */
int FaceModelFitter::LoadInitParameters(const std::string& init_param_file,
                                        const std::string& tri_file) {
  FILE* file_id = fopen(init_param_file.c_str(), "rb");
  if (file_id) {
    // Open
    int core_vertex = 0, identity_length = 0, expression_length = 0;
    int n_landmark = 0;
    fread(reinterpret_cast<void*>(&core_vertex), sizeof(int), 1, file_id);
    fread(reinterpret_cast<void*>(&identity_length), sizeof(int), 1, file_id);
    fread(reinterpret_cast<void*>(&expression_length),
          sizeof(int), 1, file_id);

    int offset = static_cast<int>((core_vertex * identity_length *
                                   expression_length * sizeof(float) +
                                   3 * sizeof(int)));
    fseek(file_id, offset, SEEK_SET);

    // Mean shape
    int mean_shape_size = 0;
    n_landmark = static_cast<int>(feature_vertex_index_.size());
    fread(static_cast<void*>(&mean_shape_size), sizeof(int), 1, file_id);
    mean_shape_landmark_ = cv::Mat(number_2D_features_ * 3, 1, CV_32FC1);
    mean_shape_ = cv::Mat(core_vertex, 1, CV_32FC1);
    fread(static_cast<void*>(mean_shape_.data),
          sizeof(float),
          mean_shape_size,
          file_id);
    for (int i = 0; i < n_landmark ; i++) {
      int index_buffer = feature_vertex_index_[i] * 3;
      mean_shape_landmark_.at<float>(i*3) = mean_shape_.at<float>(index_buffer);
      mean_shape_landmark_.at<float>(i*3 + 1) = mean_shape_.at<float>(index_buffer + 1);
      mean_shape_landmark_.at<float>(i*3 + 2) = mean_shape_.at<float>(index_buffer + 2);
    }

    // Compute Meanshape CoG and Scale
    cv::Mat cog;
    cv::reduce(mean_shape_landmark_.reshape(3),
               cog,
               0,
               CV_REDUCE_AVG);
    cog = cog.reshape(1, 3);
    float nrm = 0.f;
    for (int k = 0; k <static_cast<int>(feature_vertex_index_.size()); k++) {
      float dx = mean_shape_landmark_.at<float>(3*k) -  cog.at<float>(0);
      float dy = mean_shape_landmark_.at<float>(3*k+1) -  cog.at<float>(1);
      float dz = mean_shape_landmark_.at<float>(3*k+2) -  cog.at<float>(2);
      nrm += (dx*dx + dy*dy + dz*dz);
    }
    norm_mean_shape_ = std::sqrt(nrm);
    // Neutral expression vector - Dummy read, not used in this case
    int size = -1;
    fread(reinterpret_cast<void*>(&size), sizeof(int), 1, file_id);
    neutral_expression_weight_ = cv::Mat(size, 1, CV_32FC1, 0.f);
    fread(reinterpret_cast<void*>(neutral_expression_weight_.data),
          sizeof(float),
          size,
          file_id);

    // Avg id weights
    size = -1;
    fread(reinterpret_cast<void*>(&size), sizeof(int), 1, file_id);
    average_identity_weight_ = cv::Mat(size, 1, CV_32FC1, 0.f);
    fread(reinterpret_cast<void*>(average_identity_weight_.data),
          sizeof(float), size, file_id);
    // Done
    fclose(file_id);
    // Create mesh
    mean_shape_mesh_ = new Mesh<float>();
    mean_shape_mesh_->Load(tri_file);
    auto* mean_shape_ptr = reinterpret_cast<Vector3<float>*>(mean_shape_.data);
    std::vector<Vector3<float>> vertex(mean_shape_ptr,
                                       mean_shape_ptr + (core_vertex/3));
    mean_shape_mesh_->set_vertex(vertex);
    mean_shape_mesh_->BuildConnectivity();
    mean_shape_mesh_->ComputeVertexNormal();
    collision_tree_ = new OCTree<float>(8, 25);
    collision_tree_->Insert(*mean_shape_mesh_,
                            LTS5::Mesh<float>::PrimitiveType::kTriangle);
    collision_tree_->Build();
  } else {
    return LTS5::ProcessError::ProcessErrorEnum::kErrOpeningFile;
  }

  // Ok
  return LTS5::ProcessError::ProcessErrorEnum::kSuccess;
}

/*
 *  @name   initVariablesForSynthData
 *  @brief  Initialise/Reinitialise variable depending on the number of
 *          views used for fitting
 *  @param  number_view    Number of views
 */
void FaceModelFitter::InitMultiViewVariables(int number_view) {
  // Reinitialise variables
  number_of_view_ = number_view;
  // Rigid transform struct
  rigid_trsfrm_.InitStruct(number_view);
  // Vector
  local_landmark_.clear();
  subset_transformation_.clear();
  shifted_landmark_.clear();
  image_center_shift_.clear();
  rigid_transform_weight_.clear();
  valid_shape_.clear();
  previous_valid_shape_.clear();
  bshape_vars_.clear();

  // Contour extractor
  if (contour_extractor_ != nullptr) {
    contour_extractor_->InitMultiView(number_view);
  }
  // Vector
  for (int k = 0; k < number_of_view_; k++) {
    // Vector
    local_landmark_.push_back(cv::Mat(number_2D_features_ * 2,
                                      1,
                                      CV_32FC1));
    subset_transformation_.push_back(cv::Mat(number_2D_features_ * 2,
                                             number_2D_features_ * 3,
                                             CV_32FC1));
    shifted_landmark_.push_back(cv::Mat(number_2D_features_ * 2,
                                       1,
                                       CV_32FC1));;
    rigid_transform_weight_.push_back(cv::Mat(6, 1, CV_32FC1, 0.f));
    valid_shape_.push_back(false);
    previous_valid_shape_.push_back(false);
    bshape_vars_.push_back(BlendshapeParameter());

    // Define image center - Only if working with calibrated cameras
    // (i.e. size_feature_ == 2 && projection_mode_ != kWeakPerspective)
    if (projection_mode_ == ProjectionMode::kFullPerspective) {
      const float* image_center = camera_configuration_[k]->get_center_point();
      image_center_shift_.push_back(cv::Mat(2,
                                            1,
                                            CV_32FC1,
                                            const_cast<float*>(image_center)));
    } else if (projection_mode_ == ProjectionMode::kWeakPerspective) {
      image_center_shift_.push_back(cv::Mat(2,
                                            1,
                                            CV_32FC1,
                                            0.f));
    }
  }
}

/*
 *  @name   IsModuleWorking
 *  @brief  Return the initialization flag of the fitter. This indicates if
 *          fitter will be working on not.
 *  @return True if module is initialized correctly.
 */
bool FaceModelFitter::IsModuleWorking() const {
  return  initialized_;
}

/*
 *  @name   InitFirstIteration
 *  @brief  Initialize fitter for reconstruction
 *  @param  landmark    Set of landmarks to use
 *  @param  fitter_configuration       Fitting parameters
 */
void FaceModelFitter::InitFirstIteration(const std::vector<cv::Mat>& landmark,
                                         const FitParameter& fitter_configuration) {

  // Init identity vector with average identity weights
  if (!is_tracking_) {
    average_identity_weight_.copyTo(identity_weight_);
    identity_weight_tm1_.setTo(0.f);
    identity_weight_tm2_.setTo(0.f);
    expression_weight_tm1_.setTo(0.f);
    expression_weight_tm2_.setTo(0.f);
    // Aggregation parameters
    Mtm1_.setTo(0.f);
    Ytm1_.setTo(0.f);
    stm1_ = 0.f;
  }
  // Projection mode ?
  if (projection_mode_ == kFullPerspective) {
    // Full P
    if (!is_tracking_) {
      // Init
      rigid_trsfrm_.euler_angle[0] = 0.f;
      rigid_trsfrm_.euler_angle[1] = 0.f;
      rigid_trsfrm_.euler_angle[2] = 0.f;
      rigid_trsfrm_.translation[0] = 0.f;
      rigid_trsfrm_.scale[0] = 1.f;
      // Init rotation matrix
      Utilities::GetRotationMatrix(rigid_trsfrm_.euler_angle[0],
                                   rigid_trsfrm_.euler_angle[1],
                                   rigid_trsfrm_.euler_angle[2],
                                   Utilities::RotationDimension::k3d3d,
                                   true,
                                   &rigid_trsfrm_.rotation[0]);
    }
  } else {
    // Weak P
    //if (!is_tracking_) {
      // Estimate translation and scale for each views if needed
    cv::Mat input_point;
    cv::Mat center_gravity;
    cv::Mat centered_point;
    for (int k = 0; k < number_of_view_; k++) {
      if ((!is_tracking_ && valid_shape_[k]) ||
          (is_tracking_ && (valid_shape_[k] != previous_valid_shape_[k]))) {
        // Work with 2D or 3D pts ?
        input_point = landmark[k].reshape(2);
        // Get centroid
        cv::Mat input_center_gravity;
        cv::reduce(input_point, input_center_gravity, 0, CV_REDUCE_AVG);
        // Center pts
        cv::repeat(input_center_gravity, number_2D_features_, 1, center_gravity);
        centered_point = (input_point - center_gravity);
        float size = static_cast<float>(cv::norm(centered_point));
        rigid_trsfrm_.scale[k] = size / norm_mean_shape_;
        // Rotation
        //TODO: TEST
        float gamma = 0.f;//fitter_configuration.pose_estimation->gamma[k];
        float theta = 0.f ;//fitter_configuration.pose_estimation->theta[k];
        float phi = 0.f ;//fitter_configuration.pose_estimation->phi[k];
        cv::Mat rotation = rigid_trsfrm_.rotation[k];
        if ((gamma < -10.f || gamma > 10.f) &&
            (theta < -10.f || theta > 10.f) &&
            (phi < -10.f || phi > 10.f)) {
          rotation = cv::Mat::eye(3, 3, CV_32FC1);
        } else {
          Utilities::GetRotationMatrix(gamma,
                                       theta,
                                       phi,
                                       Utilities::RotationDimension::k3d3d,
                                       true,
                                       &rotation);
        }

        // Translation
        cv::Mat translation = rigid_trsfrm_.translation[k];
        translation = 0.f;
        // Init landmarks selection
        landmark[k].copyTo(local_landmark_[k]);
      }
    }
    //}
  }
}

#pragma mark -
#pragma mark Fitting
/*
 *  @name   ApplyTransformation
 *  @brief  Apply translation on landmarks
 *          Fill projection matrix with information from rotation matrix.
 *  @param  iteration  Current iteration number
 */
void FaceModelFitter::ApplyTransformation(int iteration, bool skip_contour) {
  // Index
  int index_2d = 0, index_3d = 0;
  // Matrix pointer
  float* landmark_ptr = nullptr;
  float* shift_landmark_ptr = nullptr;
  // Iterator
  std::vector<cv::Mat>::iterator shift_landmark_it = shifted_landmark_.begin();
  std::vector<cv::Mat>::iterator transform_it = subset_transformation_.begin();
  std::vector<cv::Mat>::const_iterator landmark_it = local_landmark_.cbegin();
  auto contour_image_position_it = contour_image_position_.cbegin();
  static auto dummy_vector = std::vector<std::vector<int>>(0);
  auto contour_index_it = ((use_contour_ && !skip_contour) ?
                           contour_extractor_->get_matched_contour().cbegin() :
                           dummy_vector.cbegin());

  if (projection_mode_ == ProjectionMode::kFullPerspective) {
    // Use 2D landmarks
    // -------------------------------------------------
    // Access shape
    const float* shape_3d_ptr = (iteration == 0 ?
                                 reinterpret_cast<const float*>(mean_shape_landmark_.data) :
                                 reinterpret_cast<const float*>(landmark_vertex_.data));
    // Update transformation matrix for each views
    std::vector<LTS5::VirtualCamera*>::const_iterator camera_it;
    int k = 0;
    for (camera_it = camera_configuration_.cbegin();
         camera_it != camera_configuration_.cend();
         ++camera_it,
         ++k) {
      // Set matrix dimension, i.e TransformMatrix | ShiftedFeatures
      // according to the number of selected vertex + contour vertex
      int n_contour_vertex = (contour_index_it == dummy_vector.begin() ?
                              0 :
                              static_cast<int>(contour_index_it->size()));
      int n_total_vertex = (iteration == 0 ?
                            number_2D_features_ :
                            (use_contour_ && iteration > 0) ?
                            number_2D_features_ + n_contour_vertex :
                            number_2D_features_);
      shift_landmark_it->create(2 * n_total_vertex, 1, CV_32FC1);
      transform_it->create(2 * n_total_vertex, 3*n_total_vertex, CV_32FC1);
      transform_it->setTo(0.f);

      // Get reference to shifted features
      shift_landmark_ptr = reinterpret_cast<float*>(shift_landmark_it->data);
      // Select reference to landmark
      landmark_ptr = reinterpret_cast<float*>(landmark_it->data);
      // Extrinsic cam
      cv::Mat matrix_ext = ((*camera_it)->get_extrinsic_matrix());
      cv::Mat rotation_camera = matrix_ext(cv::Rect(0, 0, 3, 3));
      cv::Mat translation_camera = matrix_ext(cv::Rect(3, 0, 1, 3));
      const float* focal_length = (*camera_it)->get_focal();

      cv::Mat projection_matrix;
      cv::Mat offset;
      cv::Mat const_shift = ((rotation_camera *
                              rigid_trsfrm_.translation[0]) +
                             translation_camera);
      const float* const_shift_ptr = reinterpret_cast<const float*>(const_shift.data);
      cv::Mat rot_point = (rigid_trsfrm_.scale[0] *
                           (rotation_camera *
                            rigid_trsfrm_.rotation[0]));
      const float* rot_point_ptr = reinterpret_cast<const float*>(rot_point.data);
      float point_src[3] = {0.f, 0.f, 0.f};
      float point[3] = {0.f, 0.f, 0.f};
      float focal_scale[2]={};
      cv::Mat K = cv::Mat::eye(2, 3, CV_32FC1);

      // Fill subset_transformation_ with the new rotation mat -
      // apply translation on features
      for (int i = 0; i < number_2D_features_; i++) {
        index_2d = i * 2;
        index_3d = i * 3;
        // Apply translation on 2D landmarks
        if (landmark_ptr[i*2] != 0.f) {
          // Compute Fscale
          // -------------------------------------------------------------
          // Set pts
          point_src[0] = shape_3d_ptr[index_3d];
          point_src[1] = shape_3d_ptr[index_3d+1];
          point_src[2] = shape_3d_ptr[index_3d+2];

          // Apply rotation + scale
          point[0] = ((rot_point_ptr[0] * point_src[0]) +
                      (rot_point_ptr[1] * point_src[1]) +
                      (rot_point_ptr[2] * point_src[2]));
          point[1] = ((rot_point_ptr[3] * point_src[0]) +
                      (rot_point_ptr[4] * point_src[1]) +
                      (rot_point_ptr[5] * point_src[2]));
          point[2] = ((rot_point_ptr[6] * point_src[0]) +
                      (rot_point_ptr[7] * point_src[1]) +
                      (rot_point_ptr[8] * point_src[2]));
          // Add constant shift
          point[0] += const_shift_ptr[0];
          point[1] += const_shift_ptr[1];
          point[2] += const_shift_ptr[2];
          // Define focal scaling factor
          focal_scale[0] = focal_length[0] / point[2];
          focal_scale[1] = focal_length[1] / point[2];
          // Define intrinsic matrix
          K.at<float>(0, 0) = focal_scale[0];
          K.at<float>(1, 1) = focal_scale[1];
          projection_matrix = K * rot_point;
          offset = K * const_shift;
          // Projection opertor
          // --------------------------------------------------------------
          // Shifted features
          shift_landmark_ptr[index_2d] = (landmark_ptr[index_2d] -
                                         offset.at<float>(0));
          shift_landmark_ptr[index_2d+1] = (landmark_ptr[index_2d+1] -
                                           offset.at<float>(1));
          // Fill projection matrix.
          projection_matrix.copyTo((*transform_it)(cv::Range(index_2d,
                                                             index_2d+2),
                                                   cv::Range(index_3d,
                                                             index_3d+3)));
        } else {
          shift_landmark_ptr[index_2d] = landmark_ptr[index_2d];
          shift_landmark_ptr[index_2d+1] = landmark_ptr[index_2d+1];

          // Fill projection matrix.
          (*transform_it)(cv::Range(index_2d,
                                    index_2d+2),
                          cv::Range(index_3d,
                                    index_3d+3)) = 0.f;
        }
      }

      if (iteration > 0) {
        auto contour_view_index_it = (use_contour_ ?
                                      contour_index_it->cbegin() :
                                      std::vector<int>::const_iterator());
        auto contour_img_position_it = (use_contour_ ?
                                        contour_image_position_it->cbegin() :
                                        std::vector<float>::const_iterator());
        // Access shape
        const float* contour_shape_ptr =
          reinterpret_cast<const float*>(contour_candidate_surface_.data);
        // Goes through contour
        // Compute projection matrix & shift for vertex corresponding to
        // contour
        int contour_index;
        for (int i = number_2D_features_;
             i < n_total_vertex;
             i++, ++contour_view_index_it) {
          index_2d = i * 2;
          index_3d = i * 3;
          // Compute Fscale
          // -------------------------------------------------------------------
          // Set pts
          contour_index = *contour_view_index_it * 3;
          point_src[0] = contour_shape_ptr[contour_index];
          point_src[1] = contour_shape_ptr[contour_index+1];
          point_src[2] = contour_shape_ptr[contour_index+2];
          // Apply rotation + scale
          point[0] = ((rot_point_ptr[0] * point_src[0]) +
                      (rot_point_ptr[1] * point_src[1]) +
                      (rot_point_ptr[2] * point_src[2]));
          point[1] = ((rot_point_ptr[3] * point_src[0]) +
                      (rot_point_ptr[4] * point_src[1]) +
                      (rot_point_ptr[5] * point_src[2]));
          point[2] = ((rot_point_ptr[6] * point_src[0]) +
                      (rot_point_ptr[7] * point_src[1]) +
                      (rot_point_ptr[8] * point_src[2]));
          // Add constant shift
          point[0] += const_shift_ptr[0];
          point[1] += const_shift_ptr[1];
          point[2] += const_shift_ptr[2];
          // Define focal scaling factor
          focal_scale[0] = focal_length[0] / point[2];
          focal_scale[1] = focal_length[1] / point[2];
          // Define intrinsic matrix
          K.at<float>(0, 0) = focal_scale[0];
          K.at<float>(1, 1) = focal_scale[1];
          float aW = 2.f;
          projection_matrix = K * rot_point * aW;
          offset = K * const_shift;
          // Fill matrix/vector for optimization
          projection_matrix.copyTo((*transform_it)(cv::Range(index_2d,
                                                             index_2d+2),
                                                   cv::Range(index_3d,
                                                             index_3d+3)));
          shift_landmark_it->at<float>(i*2) = (aW *
                                               (*contour_img_position_it -
                                                offset.at<float>(0)));
          ++contour_img_position_it;
          shift_landmark_it->at<float>(i*2+1) = (aW *
                                                 (*contour_img_position_it -
                                                  offset.at<float>(1)));
          ++contour_img_position_it;
        }
      }

      // Goes to the next iterator
      ++shift_landmark_it;
      ++transform_it;
      ++landmark_it;
      if (use_contour_) {
        ++contour_index_it;
        ++contour_image_position_it;
      }
    }
  } else {
    landmark_ptr = nullptr;
    shift_landmark_ptr = nullptr;
    float inv = -1.0f;
    // Projection matrix
    cv::Mat K = cv::Mat::eye(2, 3, CV_32FC1);
    for (int k = 0; k < number_of_view_; k++) {
      if (valid_shape_[k]) {
        // Set matrix dimension, i.e TransformMatrix | ShiftedFeatures
        // according to the number of selected vertex + contour vertex
        int n_contour_vertex = (contour_index_it == dummy_vector.begin() ?
                                0 :
                                static_cast<int>(contour_index_it->size()));
        int n_total_vertex = (iteration == 0 ?
                              number_2D_features_ :
                              (use_contour_ && iteration > 0) ?
                              number_2D_features_ + n_contour_vertex :
                              number_2D_features_);
        shift_landmark_it->create(2 * n_total_vertex, 1, CV_32FC1);
        transform_it->create(2 * n_total_vertex, 3 * n_total_vertex, CV_32FC1);
        transform_it->setTo(0.f);
        // Init pointers
        landmark_ptr = reinterpret_cast<float*>(landmark_it->data);
        shift_landmark_ptr = reinterpret_cast<float*>(shift_landmark_it->data);
        cv::Mat& translation = rigid_trsfrm_.translation[k];
        cv::Mat rotation = ((rigid_trsfrm_.scale[k] * K) *
                            rigid_trsfrm_.rotation[k]);
        // Fill subset_transformation_ with the new rotation mat -
        // apply translation on features
        int index = 0;
        int aIdx3 = 0;
        for (int i = 0; i < number_2D_features_; i++) {
          // Fill matrix with transformation
          index = i*2;
          aIdx3 = i*3;
          if (landmark_ptr[index] != 0.0f) {
            // Landmark used
            shift_landmark_ptr[index] = (landmark_ptr[index] -
                                         translation.at<float>(0));
            shift_landmark_ptr[index+1] = inv * landmark_ptr[index+1] -
                                          translation.at<float>(1);
            // Fill transformation matrix.
            rotation.copyTo((*transform_it)(cv::Range(index,
                                                      index + 2),
                                            cv::Range(aIdx3,
                                                      aIdx3 + 3)));
          } else {
            shift_landmark_ptr[index] = landmark_ptr[index];
            shift_landmark_ptr[index + 1] = (inv * landmark_ptr[index + 1]);
            (*transform_it)(cv::Range(index,
                                      index + 2),
                            cv::Range(aIdx3,
                                      aIdx3 + 3)) = 0.f;
          }
        }
        if (iteration > 0) {
          // Add contour
          auto contour_img_position_it = (use_contour_ && !skip_contour ?
                                          contour_image_position_it->cbegin() :
                                          std::vector<float>::const_iterator());
          for (int i = number_2D_features_; i < n_total_vertex; ++i) {
            index_2d = i*2;
            index_3d = i*3;
            shift_landmark_ptr[index_2d] = (*contour_img_position_it -
                                            translation.at<float>(0));
            ++contour_img_position_it;
            shift_landmark_ptr[index_2d+1] = ((inv *
                                               *contour_img_position_it) -
                                              translation.at<float>(1));
            ++contour_img_position_it;
            // Fill transformation matrix.
            rotation.copyTo((*transform_it)(cv::Range(index_2d,
                                                      index_2d + 2),
                                            cv::Range(index_3d,
                                                      index_3d + 3)));
          }
        }
      }
      // Increase iterator
      ++landmark_it;
      ++shift_landmark_it;
      ++transform_it;
      if (use_contour_ && !skip_contour) {
        ++contour_index_it;
        ++contour_image_position_it;
      }
    }
  }
}

/*
 *  @name   UpdateTransformation
 *  @fn UpdateTransformation(void)
 *  @brief  Update transformation parameters (i.e Rotation,
 *          translation) with the estimation (rigid_transform_weight_)
 */
void FaceModelFitter::UpdateTransformation(void) {
  float scale_update = 0.f;
  float gamma = 0.f, theta = 0.f, phi = 0.f, angular_step;
  for (int k = 0; k < number_of_view_; k++) {
    cv::Mat weight = rigid_transform_weight_[k];
    cv::Mat rotation = rigid_trsfrm_.rotation[k];
    cv::Mat translation = rigid_trsfrm_.translation[k];
    if(valid_shape_[k] && !weight.empty()) {
      // Update scale
      scale_update = weight.at<float>(0);
      if (scale_update == 0.f) {
        LTS5_LOG_WARNING("Pose : \n" << weight);
        scale_update = 1.f;
      }
      rigid_trsfrm_.scale[k] *= scale_update;
      // Update translation
      translation.at<float>(0) += weight.at<float>(4);
      translation.at<float>(1) += weight.at<float>(5);
      // Update rotation
      LTS5::Utilities::GetRotationAngle(rotation, &gamma, &theta, &phi);
      // Clip update angle step if >1.0f to avoid problem with asin function
      // --------------------------------------------------------------------
      angular_step = weight.at<float>(1)/ rigid_trsfrm_.scale[k];
      angular_step = (angular_step > 1.f ?
                      1.f :
                      angular_step < -1.f ?
                      -1.f : angular_step);
      if (angular_step >= 30.f * CV_PI / 180.f) {
        angular_step = static_cast<float>(30.f * CV_PI / 180.f);
      }
      gamma += std::asin(angular_step);
      // --------------------------------------------------------------------
      angular_step = weight.at<float>(2) / rigid_trsfrm_.scale[k];
      angular_step = (angular_step > 1.f ?
                      1.f :
                      angular_step < -1.f ?
                      -1.f : angular_step);
      if (angular_step >= 30.f*CV_PI/180.f) {
        angular_step = static_cast<float>(30.f*CV_PI/180.f);
      }
      theta += std::asin(angular_step);
      // --------------------------------------------------------------------
      angular_step = weight.at<float>(3) / rigid_trsfrm_.scale[k];
      angular_step = (angular_step > 1.f ?
                      1.f :
                      angular_step < -1.f ?
                      -1.f : angular_step);
      if (angular_step >= 30.f*CV_PI/180.f) {
        angular_step = static_cast<float>(30.f*CV_PI/180.f);
      }
      phi += std::asin(angular_step);
      // Update rotation matrix
      LTS5::Utilities::GetRotationMatrix(gamma,
                                         theta,
                                         phi,
                                         Utilities::RotationDimension::k3d3d,
                                         true,
                                         &rotation);
      rigid_trsfrm_.euler_angle[0] = gamma;
      rigid_trsfrm_.euler_angle[1] = theta;
      rigid_trsfrm_.euler_angle[2] = phi;
    } else {
      valid_shape_[k] = false;
      rotation.setTo(-1.f);
      translation.setTo(-1.f);
    }
  }
}

/*
 *  @name   UpdateRigidBasis
 *  @brief  Update the bassis used to recover rigid transformation
 *  @param  current_view    Current view point index
 *  @param  aItern          Current iteration
 */
void FaceModelFitter::UpdateRigidBasis(int current_view, int iteration) {
  const float* meanshape_ptr = nullptr;
  if (iteration == 0) {
    meanshape_ptr = reinterpret_cast<const float*>(mean_shape_landmark_.data);
  } else {
    meanshape_ptr = reinterpret_cast<const float*>(landmark_vertex_.data);
  }
  float* basis_ptr = reinterpret_cast<float*>(rigid_basis_.data);
  const int strd = rigid_basis_.cols;
  float x, y, z;
  const float* rot = reinterpret_cast<const float*>(rigid_trsfrm_.rotation[current_view].data);
  const float scale = rigid_trsfrm_.scale[current_view];
  const float dx = rigid_trsfrm_.translation[current_view].at<float>(0);
  const float dy = rigid_trsfrm_.translation[current_view].at<float>(1);
  int index;
  const float* landmark_ptr = reinterpret_cast<const float*>(local_landmark_[current_view].data);
  float* shifted_basis_ptr = reinterpret_cast<float*>(shifted_landmark_[current_view].data);
  for (int i = 0; i < number_2D_features_; i++) {
    index = i * 2;
    if (landmark_ptr[index] != 0.f) {
      x = meanshape_ptr[i*3];
      y = meanshape_ptr[i*3+1];
      z = meanshape_ptr[i*3+2];
      // Sscale
      basis_ptr[index*strd] = scale *((rot[0] * x) +
                                        (rot[1] * y) +
                                        (rot[2] * z));
      basis_ptr[(index+1)*strd] = scale * ((rot[3] * x) +
                                             (rot[4] * y) +
                                             (rot[5] * z));
      // Sgamma
      basis_ptr[index*strd + 1] = -y;
      basis_ptr[(index+1)*strd + 1] = x;
      // Stheta
      basis_ptr[index*strd + 2] = 0.0f;
      basis_ptr[(index+1)*strd + 2] = -z;
      // Sphi
      basis_ptr[index*strd + 3] = z;
      basis_ptr[(index+1)*strd + 3] = 0.0f;
      // Stx
      basis_ptr[index*strd + 4] = 1.f;
      basis_ptr[(index+1)*strd + 4] = 0.0f;
      // Sty
      basis_ptr[index*strd + 5] = 0.f;
      basis_ptr[(index+1)*strd + 5] = 1.0f;
      // Stz
      //basis_ptr[index*strd + 6] = 0.f;
      //basis_ptr[(index+1)*strd + 6] = 0.0f;
      // Landmark
      shifted_basis_ptr[index] = landmark_ptr[index] - dx;
      shifted_basis_ptr[index + 1] = -landmark_ptr[index + 1] - dy;
    } else {
      basis_ptr[index*strd] = 0.f;
      basis_ptr[(index+1)*strd] = 0.f;
      // Sgamma
      basis_ptr[index*strd + 1] = 0.f;
      basis_ptr[(index+1)*strd + 1] = 0.f;
      // Stheta
      basis_ptr[index*strd + 2] = 0.0f;
      basis_ptr[(index+1)*strd + 2] = 0.f;
      // Sphi
      basis_ptr[index*strd + 3] = 0.f;
      basis_ptr[(index+1)*strd + 3] = 0.f;
      // Stx
      basis_ptr[index*strd + 4] = 0.f;
      basis_ptr[(index+1)*strd + 4] = 0.f;
      // Sty
      basis_ptr[index*strd + 5] = 0.f;
      basis_ptr[(index+1)*strd + 5] = 0.f;
      // Stz
      //basis_ptr[index*strd + 6] = 0.f;
      //basis_ptr[(index+1)*strd + 6] = 0.f;
      // Landmark
      shifted_basis_ptr[index] = 0.f;
      shifted_basis_ptr[index + 1] = 0.f;
    }
  }
}

/*
 *  @name   ComputeWeight
 *  @fn void ComputeWeight(const DataType& direction,
                           const float eta,
                           const float eta_time,
                           const int iteration)
 *  @brief  Compute weights for given direction specified as parameters
 *  @param[in] direction      Direction of interest #DataType
 *  @param[in] eta            Regularization coef
 *  @param[in] eta_time       Regularization coef for time smoothing
 *  @param[in] iteration      Current iteration
 */
void FaceModelFitter::ComputeWeight(const DataType& direction,
                                    const float eta,
                                    const float eta_time,
                                    const int iteration ) {
  // Computes weights
  auto transform_it = subset_transformation_.cbegin();
  auto shift_landmark_it = shifted_landmark_.cbegin();
  auto rigid_weight_it = rigid_transform_weight_.begin();
  switch (direction) {
    case kIdentity : {
      // Recover identity weight (With aggregation)
      this->SolveIdentity(iteration, eta, eta_time);
    }
      break;

    case kExpression : {
      // Recover expression weight
      this->SolveExpression(iteration, eta, eta_time);
      //this->SolveBlendshapeExpression(iteration, eta, eta_time);
    }
      break;

    case kTransformation : {
      for (int k = 0; k < number_of_view_; k++) {
        if (valid_shape_[k]) {
          // Apply rigid transformation on scale basis
          this->UpdateRigidBasis(k, iteration);
          // Compute weights
          // Gives relatives update for rigid transoform
          auto mask = cv::Rect(0, 0, 1, rigid_basis_.rows);
          this->SolveLinearLeastSquare(rigid_basis_,
                                       (*shift_landmark_it)(mask),
                                       &(*rigid_weight_it));
        }
        // Increase
        ++transform_it;
        ++shift_landmark_it;
        ++rigid_weight_it;
      }
    }
      break;

    case kLevMar : {
      // Recover pose/location from mulitple view
      // --------------------------------------------------------------------
      cv::Mat container = cv::Mat(1,
                                  number_of_view_ * number_2D_features_ * 2,
                                  CV_32FC1,
                                  0.f);
      TransformationData local_data;
      local_data.number_landmark = number_2D_features_;
      local_data.number_of_view = number_of_view_;
      local_data.shape_valid = &valid_shape_;
      local_data.camera_setup = &camera_configuration_;
      local_data.landmark = reinterpret_cast<float*>(container.data);
      for (int k = 0; k < number_of_view_; k++) {
        if (valid_shape_[k]) {
          memcpy(reinterpret_cast<void*>(&(local_data.landmark[k * number_2D_features_ * 2])),
                 reinterpret_cast<const void*>(local_landmark_[k].data),
                 2 * number_2D_features_ * sizeof(float));
        }
      }
      local_data.point_3d = (iteration == 0 ?
                             reinterpret_cast<const float*>(mean_shape_landmark_.data) :
                             reinterpret_cast<const float*>(landmark_vertex_.data));
      local_data.estimated_parameter[0] = rigid_trsfrm_.euler_angle[0];
      local_data.estimated_parameter[1] = rigid_trsfrm_.euler_angle[1];
      local_data.estimated_parameter[2] = rigid_trsfrm_.euler_angle[2];
      float* translation_ptr = reinterpret_cast<float*>(rigid_trsfrm_.translation[0].data);
      local_data.estimated_parameter[3] = translation_ptr[0];
      local_data.estimated_parameter[4] = translation_ptr[1];
      local_data.estimated_parameter[5] = translation_ptr[2];
      local_data.estimated_parameter[6] = rigid_trsfrm_.scale[0];

      // Triangulate pts to estimate scale
      if (iteration == 0) {
        if (number_of_view_ > 1) {
          // Get rough estimation of the head position in the world reference
          // in order to initialise as close as possible the pose estimator.
          this->EstimateFacePosition(&local_data);
        } else {
          // Estimate roughly the scale
          this->EstimateFaceSize(&local_data);
        }
      }
      // Recover pose
      this->EstimateRigidTransformSolver(&local_data);
    }
      break;

    default: LTS5_LOG_ERROR("Not supported");
      break;
  }
}

/*
 *  @name SolveExpression
 *  @fn void SolveExpression(const int iteration, const float eta,
 const float eta_time)
 *  @brief  Compute expression weight
 *  @param[in]  iteration Current iteration number
 *  @param[in]  eta       Expression regularization factor
 *  @param[in]  eta_time  Expression regularization factor over time
 */
void FaceModelFitter::SolveExpression(const int iteration,
                                      const float eta,
                                      const float eta_time) {
  using LA = LTS5::LinearAlgebra<float>;
  using TransposeType = LTS5::LinearAlgebra<float>::TransposeType;
  using ModelDimensionEnum = LTS5::BilinearFaceModel::ModelDimensionEnum;
  // Compute Q matrix with the correct information
  // Computes weights
  auto transform_it = subset_transformation_.cbegin();
  auto shift_landmark_it = shifted_landmark_.cbegin();
  // Compute
  static cv::Mat q_matrix;
  static cv::Mat a_matrix = cv::Mat(expression_weight_.rows,
                                    expression_weight_.rows,
                                    CV_32FC1);
  static cv::Mat b_matrix = cv::Mat(expression_weight_.rows,
                                    1,
                                    CV_32FC1);
  if (number_of_view_ == 1) {
    //   Transform bilinear model into linear one
    if (!use_contour_ || (use_contour_ && iteration == 0)) {
      subset_vertex_expression_.create(number_2D_features_ * 3,
                                       expression_weight_.rows,
                                       CV_32FC1);
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                          identity_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_expression_);
    } else {
      auto contour_match_it = contour_extractor_->get_matched_global_contour().cbegin();
      int n_contour_vertex = static_cast<int>(contour_match_it->size());
      subset_vertex_expression_.create((number_2D_features_ +
                                        n_contour_vertex) * 3,
                                       expression_weight_.rows,
                                       CV_32FC1);

      // Compute landmarks selection
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                          identity_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_expression_);
      // Compute contour selection
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                          identity_weight_,
                                          &(*contour_match_it),
                                          &subset_vertex_expression_,
                                          number_2D_features_);
    }

    // Compute Q + QtQ + Qty
    LA::Gemm(*transform_it,
             TransposeType::kNoTranspose,
             1.f,
             subset_vertex_expression_,
             TransposeType::kNoTranspose,
             0.f,
             &q_matrix);
    LA::Gemm(q_matrix,
             TransposeType::kTranspose,
             1.f,
             q_matrix,
             TransposeType::kNoTranspose,
             0.f,
             &a_matrix);
    LA::Gemv(q_matrix,
             TransposeType::kTranspose,
             1.f,
             *shift_landmark_it,
             0.f,
             &b_matrix);

    // Add temporal smoothing
    const float scale = rigid_trsfrm_.scale[0] * rigid_trsfrm_.scale[0];
    const float factor = -(eta_time * scale);
    LA::Axpy(expression_weight_tm2_, factor, &b_matrix);
    LA::Axpy(expression_weight_tm1_, -2.f * factor, &b_matrix);
    // Compute weights
    this->SolveLinearLeastSquareOpt_v2(a_matrix,
                                       b_matrix,
                                       scale,
                                       eta + eta_time,
                                       &expression_weight_);
  } else {
    a_matrix.setTo(0.f);
    b_matrix.setTo(0.f);
    float n_view = 0.f;
    //   Transform bilinear model into linear one
    float cumulate_scale = 0.f;
    if (!use_contour_ || (use_contour_ && iteration == 0)) {
      subset_vertex_expression_.create(number_2D_features_ * 3,
                                       expression_weight_.rows,
                                       CV_32FC1);
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                          identity_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_expression_);

      for (int k = 0;
           k < number_of_view_;
           ++k, ++transform_it, ++shift_landmark_it) {
        if (valid_shape_[k]) {
          n_view += 1.f;
          // Scale for current view
          cumulate_scale += (projection_mode_ == kWeakPerspective ?
                             rigid_trsfrm_.scale[k] *
                             rigid_trsfrm_.scale[k] :
                             rigid_trsfrm_.scale[0] *
                             rigid_trsfrm_.scale[0]);
          // Compute Qmat = *transform_it * subset_vertex_expression_;
          LA::Gemm(*transform_it,
                   TransposeType::kNoTranspose,
                   1.f,
                   subset_vertex_expression_,
                   TransposeType::kNoTranspose,
                   0.f,
                   &q_matrix);
          // Stack operation : a_matrix += (q_matrix.t() * q_matrix)
          //                   b_matrix += (q_matrix.t() * *shift_feature_it);
          LA::Gemm(q_matrix,
                   TransposeType::kTranspose,
                   1.f,
                   q_matrix,
                   TransposeType::kNoTranspose,
                   1.f,
                   &a_matrix);
          LA::Gemv(q_matrix,
                   TransposeType::kTranspose,
                   1.f,
                   *shift_landmark_it,
                   1.f,
                   &b_matrix);
        }
      }
    } else {
      // Get access to contour
      auto contour_match_it = contour_extractor_->get_matched_global_contour().cbegin();
      // Compute landmarks selection
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                          identity_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_expression_);
      // Goes through each views
      for (int k = 0;
           k < number_of_view_;
           ++k,
           ++transform_it,
           ++shift_landmark_it,
           ++contour_match_it) {
        if (valid_shape_[k]) {
          n_view += 1.f;
          // Get the number of extra rows to add. If matrix reallocated
          // the first min(Mat::rows, sz) rows are preserved.
          subset_vertex_expression_.resize((number_2D_features_ +
                                            contour_match_it->size()) * 3,
                                           0.f);
          // Scale for current view
          cumulate_scale += (projection_mode_ == kWeakPerspective ?
                             rigid_trsfrm_.scale[k] *
                             rigid_trsfrm_.scale[k] :
                             rigid_trsfrm_.scale[0] *
                             rigid_trsfrm_.scale[0]);
          // Compute contour selection
          face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                              identity_weight_,
                                              &(*contour_match_it),
                                              &subset_vertex_expression_,
                                              number_2D_features_);
          // Compute Qmat = *transform_it * subset_vertex_expression_;
          LA::Gemm(*transform_it,
                   TransposeType::kNoTranspose,
                   1.f,
                   subset_vertex_expression_,
                   TransposeType::kNoTranspose,
                   0.f,
                   &q_matrix);
          // Stack operation : a_matrix += (q_matrix.t() * q_matrix)
          //                   b_matrix += (q_matrix.t() * *shift_feature_it);
          LA::Gemm(q_matrix,
                   TransposeType::kTranspose,
                   1.f,
                   q_matrix,
                   TransposeType::kNoTranspose,
                   1.f,
                   &a_matrix);
          LA::Gemv(q_matrix,
                   TransposeType::kTranspose,
                   1.f,
                   *shift_landmark_it,
                   1.f,
                   &b_matrix);
        }
      }
    }
    // Add temporal smoothing
    const float factor = -(eta_time * cumulate_scale * n_view);
    LA::Axpy(expression_weight_tm2_, factor, &b_matrix);
    LA::Axpy(expression_weight_tm1_, -2.f * factor, &b_matrix);
    // Compute weights
    this->SolveLinearLeastSquareOpt_v2(a_matrix,
                                       b_matrix,
                                       cumulate_scale * n_view,
                                       eta + eta_time,
                                       &expression_weight_);
  }
}

/*
 *  @name SolveBlendshapeExpression
 *  @fn void SolveBlendshapeExpression(const int iteration, const float eta,
 const float eta_time)
 *  @brief  Compute expression weight
 *  @param[in]  iteration Current iteration number
 *  @param[in]  eta       Expression regularization factor
 *  @param[in]  eta_time  Expression regularization factor over time
 */
void FaceModelFitter::SolveBlendshapeExpression(const int iteration,
                                                const float eta,
                                                const float eta_time) {
  using TransposeType = LTS5::LinearAlgebra<float>::TransposeType;
  using ModelDimensionEnum = LTS5::BilinearFaceModel::ModelDimensionEnum;
  // Compute Q matrix with the correct information
  // Computes weights
  auto transform_it = subset_transformation_.cbegin();
  auto shift_landmark_it = shifted_landmark_.cbegin();
  // Compute Qmat - Amat - Bmat depending on number of views
  cv::Mat q_matrix;
  if (number_of_view_ == 1) {
    // Transform bilinear model into linear one
    if (!use_contour_ || (use_contour_ && iteration == 0)) {
      subset_vertex_expression_.create(number_2D_features_ * 3,
                                       expression_weight_.rows,
                                       CV_32FC1);
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                          identity_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_expression_);
    } else {
      auto contour_match_it = contour_extractor_->get_matched_global_contour().cbegin();
      int n_contour_vertex = static_cast<int>(contour_match_it->size());
      subset_vertex_expression_.create((number_2D_features_ +
                                        n_contour_vertex) * 3,
                                       expression_weight_.rows,
                                       CV_32FC1);

      // Compute landmarks selection
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                          identity_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_expression_);
      // Compute contour selection
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                          identity_weight_,
                                          &(*contour_match_it),
                                          &subset_vertex_expression_,
                                          number_2D_features_);
    }


    LTS5_LOG_ERROR("NEED TO BE IMPLEMENTED");



  } else {

    float scale = 0.f;
    if (!use_contour_ || (use_contour_ && iteration == 0)) {
      // No contour
      subset_vertex_expression_.create(number_2D_features_ * 3,
                                       expression_weight_.rows,
                                       CV_32FC1);
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                          identity_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_expression_);
      // Get blendshape model
      cv::Mat Bo = subset_vertex_expression_.col(0).clone();
      cv::Mat B = subset_vertex_expression_(cv::Rect(1,
                                                     0,
                                                     number_expression_ - 1,
                                                     subset_vertex_expression_.rows)).clone();
      // Generate parameters for each views
      for (int k = 0;
           k < number_of_view_;
           ++k, ++transform_it, ++shift_landmark_it) {
        auto& params = bshape_vars_[k];
        params.eta = eta;
        params.eta_time = eta_time;
        if (valid_shape_[k]) {
          // Set scale
          params.scale = rigid_trsfrm_.scale[k];
          scale += params.scale * params.scale;
          // Compute Bi
          LTS5::LinearAlgebra<float>::Gemm(*transform_it,
                                           TransposeType::kNoTranspose,
                                           1.f,
                                           B,
                                           TransposeType::kNoTranspose,
                                           0.f,
                                           &params.Bi);
          // Compute BiTBi
          LTS5::LinearAlgebra<float>::Gemm(params.Bi,
                                           TransposeType::kTranspose,
                                           1.f,
                                           params.Bi,
                                           TransposeType::kNoTranspose,
                                           0.f,
                                           &params.BiTBi);
          // Compute ri
          shift_landmark_it->copyTo(params.ri);
          LTS5::LinearAlgebra<float>::Gemv(*transform_it,
                                           TransposeType::kNoTranspose,
                                           -1.f,
                                           Bo,
                                           1.f, &params.ri);
          // Compute BiTri
          LTS5::LinearAlgebra<float>::Gemv(params.Bi,
                                           TransposeType::kTranspose,
                                           -1.f,
                                           params.ri,
                                           0.f,
                                           &params.BiTri);
        }
      }
    } else {
      // Contour
      // Get access to contour
      auto contour_match_it = contour_extractor_->get_matched_global_contour().cbegin();
      // Compute landmarks selection
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                          identity_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_expression_);
      for (int k = 0;
           k < number_of_view_;
           ++k,
           ++transform_it,
           ++shift_landmark_it,
           ++contour_match_it) {
        // Treat if view is valid
        auto& params = bshape_vars_[k];
        params.eta = eta;
        params.eta_time = eta_time;
        if (valid_shape_[k]) {
          // the first min(Mat::rows, sz) rows are preserved.
          subset_vertex_expression_.resize((number_2D_features_ +
                                            contour_match_it->size()) * 3,
                                           0.f);
          // Scale for current view
          params.scale = rigid_trsfrm_.scale[k];
          scale += params.scale * params.scale;
          // Compute contour selection
          face_model_->ModelTimesWeightVector(ModelDimensionEnum::kId,
                                              identity_weight_,
                                              &(*contour_match_it),
                                              &subset_vertex_expression_,
                                              number_2D_features_);
          // Get blendshape model
          cv::Mat Bo = subset_vertex_expression_.col(0).clone();
          cv::Mat B = subset_vertex_expression_(cv::Rect(1,
                                                         0,
                                                         number_expression_ - 1,
                                                         subset_vertex_expression_.rows)).clone();


          // Compute Bi
          LTS5::LinearAlgebra<float>::Gemm(*transform_it,
                                           TransposeType::kNoTranspose,
                                           1.f,
                                           B,
                                           TransposeType::kNoTranspose,
                                           0.f,
                                           &params.Bi);
          // Compute BiTBi
          LTS5::LinearAlgebra<float>::Gemm(params.Bi,
                                           TransposeType::kTranspose,
                                           1.f,
                                           params.Bi,
                                           TransposeType::kNoTranspose,
                                           0.f,
                                           &params.BiTBi);
          // Compute ri
          shift_landmark_it->copyTo(params.ri);
          LTS5::LinearAlgebra<float>::Gemv(*transform_it,
                                           TransposeType::kNoTranspose,
                                           -1.f,
                                           Bo,
                                           1.f, &params.ri);
          // Compute BiTri
          LTS5::LinearAlgebra<float>::Gemv(params.Bi,
                                           TransposeType::kTranspose,
                                           -1.f,
                                           params.ri,
                                           0.f,
                                           &params.BiTri);
        }
      }
    }


    // Solve
    lbfgs_parameter_t param;
    param.m = 6;
    param.epsilon = 1e-5;
    param.past = 0;
    param.delta = 1e-5;
    param.max_iterations = 10;
    param.linesearch = LBFGS_LINESEARCH_MORETHUENTE;
    //param.linesearch = LBFGS_LINESEARCH_BACKTRACKING;
    param.max_linesearch = 40;
    param.min_step = 1e-20; // 1e-20
    param.max_step = 1e20;
    param.ftol = 1e-4;
    param.wolfe = 0.9f;
    param.gtol = 0.9f;
    param.xtol = 1.0e-16;
    param.orthantwise_c = scale * eta;
    param.orthantwise_start = 0;
    param.orthantwise_end = this->number_expression_ - 1;

    float fx = 0.f;
    int ret = lbfgs(this->number_expression_ - 1,
                    w_exp_solver_,
                    &fx,
                    FaceModelFitter::LBFGSEvaluateCallback,
                    nullptr,
                    this,
                    &param);

    if (ret != LBFGS_SUCCESS) {
      if (ret == LBFGSERR_UNKNOWNERROR) {
        std::cout << "Unknown error";
      } else if (ret == LBFGSERR_LOGICERROR) {
        std::cout << "Logic error";
      } else if (ret == LBFGSERR_OUTOFMEMORY) {
        std::cout << "Insufficient memory";
      } else if (ret == LBFGSERR_CANCELED) {
        std::cout << "The minimization process has been canceled";
      } else if (ret == LBFGSERR_INVALID_N) {
        std::cout << "Invalid number of variables specified";
      } else if (ret == LBFGSERR_INVALID_N_SSE) {
        std::cout << "Invalid number of variables (for SSE) specified";
      } else if (ret == LBFGSERR_INVALID_X_SSE) {
        std::cout << "The array x must be aligned to 16 (for SSE)";
      } else if (ret == LBFGSERR_INVALID_EPSILON) {
        std::cout << "Invalid parameter ::epsilon specified";
      } else if (ret == LBFGSERR_INVALID_TESTPERIOD) {
        std::cout << "Invalid parameter ::past specified";
      } else if (ret == LBFGSERR_INVALID_DELTA) {
        std::cout << "Invalid parameter ::delta specified";
      } else if (ret == LBFGSERR_INVALID_LINESEARCH) {
        std::cout << "Invalid parameter ::linesearch specified";
      } else if (ret == LBFGSERR_INVALID_MINSTEP) {
        std::cout << "Invalid parameter ::min_step specified";
      } else if (ret == LBFGSERR_INVALID_MAXSTEP) {
        std::cout << "Invalid parameter ::max_step specified";
      } else if (ret == LBFGSERR_INVALID_FTOL) {
        std::cout << "Invalid parameter ::ftol specified";
      } else if (ret == LBFGSERR_INVALID_WOLFE) {
        std::cout << "Invalid parameter ::wolfe specified";
      } else if (ret == LBFGSERR_INVALID_GTOL) {
        std::cout << "Invalid parameter ::gtol specified";
      } else if (ret == LBFGSERR_INVALID_XTOL) {
        std::cout << "Invalid parameter ::xtol specified";
      } else if (ret == LBFGSERR_INVALID_MAXLINESEARCH) {
        std::cout << "Invalid parameter ::max_linesearch specified";
      } else if (ret == LBFGSERR_INVALID_ORTHANTWISE) {
        std::cout << "Invalid parameter ::orthantwise_c specified";
      } else if (ret == LBFGSERR_INVALID_ORTHANTWISE_START) {
        std::cout << "Invalid parameter ::orthantwise_start specified";
      } else if (ret == LBFGSERR_INVALID_ORTHANTWISE_END) {
        std::cout << "Invalid parameter ::orthantwise_end specified";
      } else if (ret == LBFGSERR_OUTOFINTERVAL) {
        std::cout << "The line-search step went out of the interval ";
        std::cout << "of uncertainty";
      } else if (ret == LBFGSERR_INCORRECT_TMINMAX) {
        std::cout << "A logic error occurred; alternatively, the interval";
        std::cout << " of uncertainty became too small";
      } else if (ret == LBFGSERR_ROUNDING_ERROR) {
        std::cout << "A rounding error occurred; alternatively, no line-search";
        std::cout << " step satisfies the sufficient decrease and curvature";
        std::cout << " conditions";
      } else if (ret == LBFGSERR_MINIMUMSTEP) {
        std::cout << "The line-search step became smaller than ::min_step";
      } else if (ret == LBFGSERR_MAXIMUMSTEP) {
        std::cout << "The line-search step became larger than ::max_step";
      } else if (ret == LBFGSERR_MAXIMUMLINESEARCH) {
        std::cout << "The line-search routine reaches the max number of eval";
      } else if (ret == LBFGSERR_MAXIMUMITERATION) {
        std::cout << "The algorithm routine reaches the max iterations";
      } else if (ret == LBFGSERR_WIDTHTOOSMALL) {
        std::cout << "Relative width of the interval of uncertainty is at";
        std::cout << " most lbfgs_parameter_t::xtol";
      } else if (ret == LBFGSERR_INVALIDPARAMETERS) {
        std::cout << "A logic error (negative line-search step) occurred";
      } else if (ret == LBFGSERR_INCREASEGRADIENT) {
        std::cout << "The current search direction increases the obj fun";
      }
      std::cout << std::endl;
    }


    expression_weight_.at<float>(0) = 1.f;
    for (int e = 0; e < this->number_expression_ - 1; ++e) {
      float& v = w_exp_solver_[e];
      if (v > 1.f) {
        v = 1.f;
      } else if (v < 0.f) {
        v = 0.f;
      }
      expression_weight_.at<float>(e + 1) = v;
    }
  }
}

/*
 *  @name LBFGSEvaluateCallback
 *  @fn
 *  @brief  Callback invoked by LBFGS solver to estimate cost function + grad
 *  @param[in] instance   Object linked to the solver
 *  @param[in] x          Current estimation of the parameters
 *  @param[in] gx         Current estimation of gradient
 *  @param[in] n          Number of variables in \p x.
 *  @param[in] step       Step size
 */

float FaceModelFitter::LBFGSEvaluateCallback(void* instance,
                                             const float* x,
                                             float* gx,
                                             const int n,
                                             const float step) {
  using TransposeType = LinearAlgebra<float>::TransposeType;
  using LA = LinearAlgebra<float>;
  // Recover object
  FaceModelFitter* thiz = static_cast<FaceModelFitter*>(instance);
  // Define current parameters
  cv::Mat we(n, 1, CV_32FC1, (void*)x);
  cv::Mat grad(n, 1, CV_32FC1, (void*)gx);
  grad.setTo(0.f);
  // Compute cost
  auto fx = 0.f;
  float n_view = 0.f;
  float scale  = 0.f;
  float eta = 0.f, eta_time = 0.f;
  cv::Mat v0, v1, m0;
  for (int i = 0; i < thiz->number_of_view_; ++i) {
    const auto& p = thiz->bshape_vars_[i];
    eta = p.eta;
    eta_time = p.eta_time;
    if (thiz->valid_shape_[i]) {
      n_view += 1.f;
      scale += p.scale * p.scale;
      // Compute cost
      LA::Gemv(p.Bi,
               TransposeType::kNoTranspose,
               1.f,
               we,
               0.f,
               &v0);
      LA::Axpy(p.ri,
               -1.f,
               &v0);
      float nrm = LA::L2Norm(v0);
      fx += nrm * nrm;

      // Compute grad
      LA::Gemv(p.BiTBi,
               TransposeType::kNoTranspose,
               1.f,
               we,
               1.f,
               &grad);
      LA::Axpy(p.BiTri, 1.f, &grad);
    }
  }

  eta *= scale;
  eta_time *= scale;

  // Compute cost
  fx /= n_view;
  // Add eta * |we|
  /*float nrm = LA::L2Norm(we);
  nrm *= nrm;
  fx += eta * nrm;*/

  // Add time reg
  thiz->wexp_tm2_.copyTo(v0);
  LA::Axpy(thiz->wexp_tm1_, -2.f, &v0);
  LA::Axpy(we, 1.f, &v0);
  float nrm = LA::L2Norm(v0);
  nrm *= nrm;
  fx += eta_time * nrm;

  // Compute grad
  LA::Axpy(v0, n_view * eta_time, &grad);

  return fx;
}

/*
 *  @name SolveIdentity
 *  @fn void SolveIdentity(const int iteration, const float eta,
 const float gamma)
 *  @brief  Compute identity weight
 *  @param[in]  iteration Current iteration number
 *  @param[in]  eta       Identity regularization factor
 *  @param[in]  gamma     Identity aggregation factor over time
 */
void FaceModelFitter::SolveIdentity(const int iteration,
                                    const float eta,
                                    const float gamma) {
  using LA = LinearAlgebra<float>;
  using TransposeType = LTS5::LinearAlgebra<float>::TransposeType;
  using ModelDimensionEnum = LTS5::BilinearFaceModel::ModelDimensionEnum;
  // Compute Q matrix with the correct information
  // Computes weights
  auto transform_it = subset_transformation_.cbegin();
  auto shift_landmark_it = shifted_landmark_.cbegin();
  // Compute Qmat - Amat - Bmat depending on number of views
  cv::Mat q_matrix;
  if (number_of_view_ == 1) {
    //   Transform bilinear model into linear one
    if (!use_contour_ || (use_contour_ && (iteration < 1))) {
      subset_vertex_identity_.create(number_2D_features_ * 3,
                                     identity_weight_.rows,
                                     CV_32FC1);
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kExpr,
                                          expression_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_identity_);
    } else {
      auto contour_match_it = contour_extractor_->get_matched_global_contour().cbegin();
      int n_contour_vertex = static_cast<int>(contour_match_it->size());
      subset_vertex_identity_.create((number_2D_features_ + n_contour_vertex) * 3,
                                     identity_weight_.rows,
                                     CV_32FC1);
      // Compute landmarks selection
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kExpr,
                                          expression_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_identity_);
      // Compute contour selection
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kExpr,
                                          expression_weight_,
                                          &(*contour_match_it),
                                          &subset_vertex_identity_,
                                          number_2D_features_);
    }
    // Compute QtQ + Qty
    LA::Gemm(*transform_it,
             TransposeType::kNoTranspose,
             1.f,
             subset_vertex_identity_,
             TransposeType::kNoTranspose,
             0.f,
             &q_matrix);
    LA::Gemm(q_matrix,
             TransposeType::kTranspose,
             1.f,
             q_matrix,
             TransposeType::kNoTranspose,
             0.f,
             &Mt_);
    LA::Gemv(q_matrix,
             TransposeType::kTranspose,
             1.f,
             *shift_landmark_it,
             0.f,
             &Yt_);

    // Add aggregation
    // ----------------------------
    // compute s(t)
    st_ = gamma * stm1_ + 1.f;
    const float y_factor = 1.f / st_;
    const float m_factor = (gamma * stm1_) * y_factor;
    // compute M(t)
    LA::Axpby(Mtm1_, m_factor, y_factor, &Mt_);
    LA::Axpby(Ytm1_, m_factor, y_factor, &Yt_);
    // Compute weights
    float s = rigid_trsfrm_.scale[0] * rigid_trsfrm_.scale[0];
    this->SolveLinearLeastSquareOpt_v2(Mt_, Yt_, s, eta, &identity_weight_);
  } else {
    Mt_.setTo(0.f);
    Yt_.setTo(0.f);
    float n_view = 0.f;
    float cumulate_scale = 0.f;
    if (!use_contour_ || (use_contour_ && iteration < 1)) {
      // Allocate correct matrix size -> only for landmarks
      subset_vertex_identity_.create(number_2D_features_ * 3,
                                     identity_weight_.rows,
                                     CV_32FC1);
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kExpr,
                                          expression_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_identity_);
      for (int k = 0;
           k < number_of_view_;
           ++k, ++transform_it, ++shift_landmark_it) {
        if (valid_shape_[k]) {
          n_view += 1.f;
          // Scale for current view
          cumulate_scale += (projection_mode_ == kWeakPerspective ?
                             rigid_trsfrm_.scale[k] *
                             rigid_trsfrm_.scale[k] :
                             rigid_trsfrm_.scale[0] *
                             rigid_trsfrm_.scale[0]);
          // Compute Qmat = *transform_it * subset_vertex_identity_;
          LA::Gemm(*transform_it,
                   TransposeType::kNoTranspose,
                   1.f,
                   subset_vertex_identity_,
                   TransposeType::kNoTranspose,
                   0.f,
                   &q_matrix);
          // Stack operation   :   a_matrix += (q_matrix.t() * q_matrix)
          //                       b_matrix += (q_matrix.t() * *shift_feature_it);
          LA::Gemm(q_matrix,
                   TransposeType::kTranspose,
                   1.f,
                   q_matrix,
                   TransposeType::kNoTranspose,
                   1.f,
                   &Mt_);
          LA::Gemv(q_matrix,
                   TransposeType::kTranspose,
                   1.f,
                   *shift_landmark_it,
                   1.f,
                   &Yt_);
        }
      }
    } else {
      // Get access to contour
      auto contour_match_it = contour_extractor_->get_matched_global_contour().cbegin();
      // Compute landmarks selection
      face_model_->ModelTimesWeightVector(ModelDimensionEnum::kExpr,
                                          expression_weight_,
                                          &feature_vertex_index_,
                                          &subset_vertex_identity_);
      // Goes through each views
      for (int k = 0;
           k < number_of_view_;
           ++k,
                   ++transform_it,
                   ++shift_landmark_it,
                   ++contour_match_it) {
        if (valid_shape_[k]) {
          n_view += 1.f;
          // Get the number of extra rows to add. If matrix reallocated
          // the first min(Mat::rows, sz) rows are preserved.
          subset_vertex_identity_.resize((number_2D_features_ +
                                          contour_match_it->size()) * 3,
                                         0.f);
          // Scale for current view
          cumulate_scale += (projection_mode_ == kWeakPerspective ?
                             rigid_trsfrm_.scale[k] *
                             rigid_trsfrm_.scale[k] :
                             rigid_trsfrm_.scale[0] *
                             rigid_trsfrm_.scale[0]);
          // Compute contour selection
          face_model_->ModelTimesWeightVector(ModelDimensionEnum::kExpr,
                                              expression_weight_,
                                              &(*contour_match_it),
                                              &subset_vertex_identity_,
                                              number_2D_features_);
          // Compute Qmat = *transform_it * subset_vertex_expression_;
          LA::Gemm(*transform_it,
                   TransposeType::kNoTranspose,
                   1.f,
                   subset_vertex_identity_,
                   TransposeType::kNoTranspose,
                   0.f,
                   &q_matrix);
          // Stack operation : a_matrix += (q_matrix.t() * q_matrix)
          //                   b_matrix += (q_matrix.t() * *shift_feature_it);
          LA::Gemm(q_matrix,
                   TransposeType::kTranspose,
                   1.f,
                   q_matrix,
                   TransposeType::kNoTranspose,
                   1.f,
                   &Mt_);
          LA::Gemv(q_matrix,
                   TransposeType::kTranspose,
                   1.f,
                   *shift_landmark_it,
                   1.f,
                   &Yt_);
        }
      }
    }
    // Add aggregation
    // ----------------------------
    // compute s(t)
    st_ = gamma * stm1_ + 1.f;
    const float y_factor = 1.f / st_;
    const float m_factor = (gamma * stm1_) * y_factor;
    // compute M(t)
    LA::Axpby(Mtm1_, m_factor, y_factor, &Mt_);
    LA::Axpby(Ytm1_, m_factor, y_factor, &Yt_);
    // Compute weights
    this->SolveLinearLeastSquareOpt_v2(Mt_,
                                       Yt_,
                                       cumulate_scale * n_view,
                                       eta,
                                       &identity_weight_);
  }
}

/*
 *  @name   SolveLinearLeastSquare
 *  @brief  Solve problem of form min(w) || Qw - y ||^2 + rho*|| w ||^2
 *  @param  q_matrix   System matrix
 *  @param  y_vector  Output vector
 *  @param  eta    Regularizazion coef
 *  @param  weight  Result
 */
void FaceModelFitter::SolveLinearLeastSquare(const cv::Mat& q_matrix,
                                             const cv::Mat& y_vector,
                                             const float eta,
                                             cv::Mat* weight) {
  cv::Mat U;
  cv::Mat W;
  cv::Mat Vt;
  // Apply svd on Qmat
  cv::SVD::compute(q_matrix, W, U, Vt, cv::SVD::FULL_UV);
  // Compute diagonal matrix : diag( Wk / Wk^2 + rho)
  cv::Mat diag = cv::Mat(q_matrix.cols, q_matrix.rows, CV_32FC1, 0.f);
  int min_dimension = std::min(q_matrix.cols, q_matrix.rows);
  float* diag_ptr = reinterpret_cast<float*>(diag.data);
  float* W_ptr = reinterpret_cast<float*>(W.data);
  int n_diag_col = diag.cols;
  float scale = rigid_trsfrm_.scale[0];
  for (int i = 0; i < min_dimension ; i++) {
    float weight_i = W_ptr[i];
    diag_ptr[i * n_diag_col + i] = (weight_i /
                                    (weight_i * weight_i + (eta *
                                                            scale * scale)));
  }
  // Compute weight
  *weight = (Vt.t()*(diag * (U.t() * y_vector)));
}

/*
 *  @name   SolveLinearLeastSquare
 *  @brief  Solve problem of form min(w) || Qw - y ||^2 + rho*|| w ||^2
 *  @param  q_matrix   System matrix
 *  @param  y_vector  Output vector
 *  @param  eta    Regularizazion coef
 *  @param  weight  Result
 */
void FaceModelFitter::SolveLinearLeastSquareOpt(const cv::Mat& q_matrix,
                                                const cv::Mat& y_vector,
                                                const float eta,
                                                cv::Mat* weight) {
  // TEST - OPTM
  using SVDType = LinearAlgebra<float>::Lapack::SVDType;
  LTS5::LinearAlgebra<float>::SvdDecomposition svd_decomp;
  svd_decomp.Init(q_matrix, SVDType::kFullUV);
  svd_decomp.Compute();
  cv::Mat diag = cv::Mat(q_matrix.cols, q_matrix.rows, CV_32FC1, 0.f);
  int min_dimension = std::min(q_matrix.cols, q_matrix.rows);
  float* diag_ptr = reinterpret_cast<float*>(diag.data);
  float* W_ptr = svd_decomp.p_.kS;
  int n_diag_col = diag.cols;
  float weight_i = 0.f;
  float scale = rigid_trsfrm_.scale[0];
  for (int i = 0; i < min_dimension ; i++) {
    weight_i = W_ptr[i];
    diag_ptr[i*n_diag_col + i] = (weight_i / (weight_i * weight_i +
                                              (eta * scale * scale)));
  }
  cv::Mat Vt = cv::Mat(q_matrix.cols,
                       q_matrix.cols ,
                       CV_32FC1,
                       svd_decomp.p_.kU);
  cv::Mat U = cv::Mat(q_matrix.rows,
                      q_matrix.rows,
                      CV_32FC1,
                      svd_decomp.p_.kVt);
  // Compute weight
  *weight = (Vt.t()*(diag * (U.t() * y_vector)));
}

/*
 *  @name   SolveLinearLeastSquare
 *  @brief  Solve problem of form min(w) || Qw - y ||^2 + rho*|| w ||^2
 *  @param  q_matrix   System matrix
 *  @param  y_vector  Output vector
 *  @param  eta    Regularizazion coef
 *  @param  weight  Result

 */
/*void FaceModelFitter::SolveLinearLeastSquareOpt_v2(const cv::Mat& q_matrix,
                                                   const cv::Mat& y_vector,
                                                   const float eta,
                                                   cv::Mat* weight) {
  using TransposeType = LTS5::LinearAlgebra<float>::TransposeType;
  // Define QtQ + eta*I
  cv::Mat Q = cv::Mat::eye(q_matrix.cols, q_matrix.cols, q_matrix.type());
  float s = eta * rigid_trsfrm_.scale[0] * rigid_trsfrm_.scale[0];
  LTS5::LinearAlgebra<float>::Gemm(q_matrix,
                                   TransposeType::kTranspose,
                                   1.f,
                                   q_matrix,
                                   TransposeType::kNoTranspose,
                                   s,
                                   &Q);
  // Define Qty
  cv::Mat y(q_matrix.cols, 1, CV_32FC1);
  LTS5::LinearAlgebra<float>::Gemv(q_matrix,
                                   TransposeType::kTranspose,
                                   1.f,
                                   y_vector,
                                   0.f,
                                   &y);
  // Solve
  LTS5::LinearAlgebra<float>::LinearSolver solver;
  solver.Solve(Q, y, weight);
  if (weight->empty()) {
    *weight = cv::Mat(Q.cols, 1, CV_32FC1, cv::Scalar(1.f));
    LTS5_LOG_ERROR("Error solving least square problem");
  }
}*/

/*
 *  @name   SolveLinearLeastSquare
 *  @brief  Solve problem of form min(w) Sum {|| Qw - y ||^2 } + rho*|| w ||^2
 *  @param  a_matrix       Sum{ Qt*Q }
 *  @param  b_matrix       Sum{ Qt*y }
 *  @param  sum_length  How many elements are in the sum
 *  @param  eta        Regularizazion coef
 *  @param  weight      Result - w
 */
void FaceModelFitter::SolveLinearLeastSquare(const cv::Mat& a_matrix,
                                             const cv::Mat& b_matrix,
                                             const int sum_length,
                                             const float eta,
                                             cv::Mat* weight) {
  // Add n * I * eta
  a_matrix += (cv::Mat::eye(a_matrix.rows,
                            a_matrix.cols,
                            a_matrix.type()) *
               static_cast<float>(sum_length) *
               eta);
  // Compute pseudo-inverse
  cv::SVD svd(a_matrix);
  cv::Mat pinv_matrix_a = (svd.vt.t() *
                           (cv::Mat::diag(1.f / svd.w)) * svd.u.t());
  // Compute weight
  *weight = pinv_matrix_a * b_matrix;
}

/*
 *  @name   SolveLinearLeastSquare
 *  @brief  Solve problem of form min(w) Sum {|| Qw - y ||^2 } + rho*|| w ||^2
 *  @param  a_matrix       Sum{ Qt*Q }
 *  @param  b_matrix       Sum{ Qt*y }
 *  @param  cumulate_scale Cumulated scale for each views
 *  @param  eta        Regularizazion coef
 *  @param  weight      Result - w
 */
void FaceModelFitter::SolveLinearLeastSquareOpt(const cv::Mat& a_matrix,
                                                const cv::Mat& b_matrix,
                                                const float cumulate_scale,
                                                const float eta,
                                                cv::Mat* weight) {
  using TransposeType = LTS5::LinearAlgebra<float>::TransposeType;
  // Add n * I * eta
  cv::Mat Q = (cv::Mat::eye(a_matrix.rows,
                            a_matrix.cols,
                            a_matrix.type()) *
               cumulate_scale *
               eta);
  Q += a_matrix;
  // Compute pseudo-inverse
  LTS5::LinearAlgebra<float>::SvdDecomposition svd_decomp;
  svd_decomp.Init(Q,
                  LinearAlgebra<float>::Lapack::SVDType::kStd);
  svd_decomp.Compute();
  int min_dimension = std::min(Q.cols, Q.rows);
  cv::Mat Umat, scale, Vtmat;
  svd_decomp.Get(&Umat, &scale, &Vtmat);
  // Compute weight
  cv::Mat tmp;
  cv::Mat pinv_t;
  LinearAlgebra<float>::Gemm(scale,
                             TransposeType::kNoTranspose,
                             1.f,
                             Vtmat,
                             TransposeType::kNoTranspose,
                             0.f,
                             &tmp);
  LinearAlgebra<float>::Gemm(Umat,
                             TransposeType::kNoTranspose,
                             1.f,
                             tmp,
                             TransposeType::kNoTranspose,
                             0.f,
                             &pinv_t);
  LinearAlgebra<float>::Gemv(pinv_t,
                             TransposeType::kTranspose,
                             1.f,
                             b_matrix,
                             0.f,
                             weight);
}

/*
 *  @name   SolveLinearLeastSquare
 *  @fn void SolveLinearLeastSquareOpt_v2(const cv::Mat& a_matrix,
                                           const cv::Mat& b_matrix,
                                           const float cumulate_scale,
                                           const float eta,
                                           cv::Mat* weight)
 *  @brief  Solve problem of form min(w) Sum {| Qw - y |^2 } + rho*|w|^2
 *  @param[in]  a_matrix        Sum{ Qt*Q }
 *  @param[in]  b_matrix        Sum{ Qt*y }
 *  @param[in]  cumulate_scale  Cumulated scale for each views
 *  @param[in]  eta             Regularizazion coef
 *  @param[out] weight          Result - w
 */
void FaceModelFitter::SolveLinearLeastSquareOpt_v2(const cv::Mat& a_matrix,
                                                   const cv::Mat& b_matrix,
                                                   const float cumulate_scale,
                                                   const float eta,
                                                   cv::Mat* weight) {
  // Add regularization
  cv::Mat Q = (cv::Mat::eye(a_matrix.rows, a_matrix.cols, a_matrix.type()) *
               cumulate_scale *
               eta);
  Q += a_matrix;
  // Solve
  LTS5::LinearAlgebra<float>::LinearSolver solver;
  solver.Solve(Q, b_matrix, weight);
  if (weight->empty()) {
    *weight = cv::Mat(Q.cols, 1, CV_32FC1, cv::Scalar(1.f));
    LTS5_LOG_ERROR("Error solving least square problem");
  }
}

/*
 *  @name   SolveLinearLeastSquare
 *  @fn void SolveLinearLeastSquareOpt_v2(const cv::Mat& a_matrix,
 const cv::Mat& b_matrix,
 const float cumulate_scale,
 const float eta,
 cv::Mat* weight)
 *  @brief  Solve problem of form min(w) Sum {| Qw - y |^2 } + rho*|w|^2
 *  @param[in]  a_matrix        Sum{ Qt*Q }
 *  @param[in]  b_matrix        Sum{ Qt*y }
 *  @param[in]  cumulate_scale  Cumulated scale for each views
 *  @param[in]  eta             Regularizazion coef
 *  @param[out] weight          Result - w
 */
void FaceModelFitter::SolveLinearLeastSquareOpt_v3(const cv::Mat& a_matrix,
                                                   const cv::Mat& b_matrix,
                                                   const float cumulate_scale,
                                                   const float eta,
                                                   cv::Mat* weight) {
  // Add regularization
  cv::Mat Q = (cv::Mat::eye(a_matrix.rows, a_matrix.cols, a_matrix.type()) *
               cumulate_scale *
               eta);
  Q += a_matrix;
  // Solve

  cv::Mat w;
  cv::Mat Qtemp = Q(cv::Rect(1 ,0, Q.cols - 1, Q.rows)).clone();



  LTS5::LinearAlgebra<float>::LinearSolver solver;
  solver.Solve(Qtemp, b_matrix, &w);
  if (w.empty()) {
    *weight = cv::Mat(Q.cols, 1, CV_32FC1, cv::Scalar(1.f));
    LTS5_LOG_ERROR("Error solving least square problem");
  } else {
    float* w_ptr = reinterpret_cast<float*>(w.data);
    float* weight_ptr = reinterpret_cast<float*>(weight->data);
    float w_sum = 0.f;

    for (int e = 0; e < w.rows; ++e) {
      float ei = w_ptr[e];
      ei = ei > 1.f ? 1.f : ei < 0.f ? 0.f : ei;
      weight_ptr[e + 1] = ei;
      w_sum += ei;
    }
    weight_ptr[0] = 1.f - w_sum;
  }
}

/*
 *  @name   SolveLinearLeastSquare
 *  @brief  Solve problem of form min(x) || Qw - y ||^2
 *  @param  q_matrix   System matrix
 *  @param  y_vector  Output vector
 *  @param  weight  Result
 */
void FaceModelFitter::SolveLinearLeastSquare(const cv::Mat& q_matrix,
                                             const cv::Mat& y_vector,
                                             cv::Mat* weight) {
  // pseudo inv : ((Qt * Q)^-1)*Qt
  // if Q = UWVt then pinvQ = V W^-1 Ut
  // Compute SVD + pseudo-inverse
  //cv::SVD aQsvd(q_matrix);
  //cv::Mat aPinvQmat = aQsvd.vt.t()*(cv::Mat::diag(1.f/aQsvd.w))*aQsvd.u.t();
  // Compute weight
  //*weight = aPinvQmat * y_vector;

  using SolverType = LinearAlgebra<float>::LinearSolver;
  SolverType solver;
  solver.Solve(q_matrix, y_vector, weight);
}

/*
 *  @name   FitModel
 *  @brief  Methods call to fit bilinear face model on a new entry
 *          (new set of landmarks) [Single view]
 *  @param  landmark    Set of points used to compute model coefficients
 *  @param  fitter_config   Parameters set used by fitter
 *  @param  fit_result      Fit results
 *  @return -1 if error, 0 otherwise
 */
int FaceModelFitter::FitModel(const std::vector<cv::Mat>& landmark,
                               const FitParameter& fit_config,
                               FitResult* fit_result) {
  using LA = LinearAlgebra<float>;
  int err = -1;
  if (initialized_ && landmark.size() > 0) {
    // Save reference to input landmarks
    real_landmark_ = &landmark;
    // Indicates if contour will be used
    use_contour_ = fit_config.use_contour;
    // Valid indicator
    bool at_least_one = false;
    for (int i = 0; i < landmark.size(); ++i) {
      valid_shape_[i] = !landmark[i].empty();
      at_least_one |= valid_shape_[i];
    }
    // Fit model
    if (at_least_one) {
      int t0 = static_cast<int>(cv::getTickCount());
      for (int i = 0; i < fit_config.max_iteration; i++) {
        // Init
        if (i == 0) {
          // Init rigid transformation estimation
          this->InitFirstIteration(landmark, fit_config);
        } else {
          // Generate model from previous parameters estimation
          this->ComputeMeshVertices(VertexSelection::kLandmarks, false);
        }
        // 2D : Landmark selection + rigid transform estimation
        // 3D : rigid transformation estimation => Same as weak perspective
        // --------------------------------------------------------------------
        this->EstimatetRigidTransform(landmark, fit_config, i);
        // Recover expression
        // -------------------------------------------------------------------
        //   Compute expression weights
        this->ComputeWeight(DataType::kExpression,
                            fit_config.eta_expression,
                            fit_config.eta_time_expression,
                            i);
        // Recover identity
        // -------------------------------------------------------------------
        // Compute identity weights
        this->ComputeWeight(DataType::kIdentity,
                            fit_config.eta_identity,
                            fit_config.gamma_identity,
                            i);
      }
      // Generate 3D mesh
      LTS5::Utilities::GetRotationAngle(rigid_trsfrm_.rotation[0],
                                        &rigid_trsfrm_.euler_angle[0],
                                        &rigid_trsfrm_.euler_angle[1],
                                        &rigid_trsfrm_.euler_angle[2]);

      if (projection_mode_ == ProjectionMode::kWeakPerspective) {
        // Weak perspective recontruct without rigid transformation
        auto t0 = cv::getTickCount();
        this->ComputeMeshVertices(VertexSelection::kFull, false);
        auto t1 = cv::getTickCount();
        auto dt = (t1-t0)*1000.0 / cv::getTickFrequency();
        LTS5_LOG_DEBUG("Surface : " << dt << " ms");
      } else {
        // Full perspective recontruct with rigid transformation
        //TODO: Non-rigid comparison purpose !!!
        this->ComputeMeshVertices(VertexSelection::kFull, false);
      }

      fit_result->delta_x = rigid_trsfrm_.translation[0].at<float>(0, 0);
      fit_result->delta_y = rigid_trsfrm_.translation[0].at<float>(1, 0);
      fit_result->delta_z = rigid_trsfrm_.translation[0].at<float>(2, 0);
      fit_result->gamma = rigid_trsfrm_.euler_angle[0] * (180.f/CV_PI);
      fit_result->theta = rigid_trsfrm_.euler_angle[1] * (180.f/CV_PI);
      fit_result->phi = rigid_trsfrm_.euler_angle[2] * (180.f/CV_PI);
      fit_result->scale = rigid_trsfrm_.scale[0];

      // Save identity weight
      LA::Copy(identity_weight_tm1_, &identity_weight_tm2_);
      LA::Copy(identity_weight_, &identity_weight_tm1_);
      // Identity aggregation, M(t-1), y(t-1) and s(t-1)
      stm1_ = st_;
      LA::Copy(Mt_, &Mtm1_);
      LA::Copy(Yt_, &Ytm1_);
      // Expression
      cv::Rect mask(0, 1, 1, number_expression_ - 1);
      LA::Copy(expression_weight_tm1_, &expression_weight_tm2_);
      expression_weight_tm1_(mask).copyTo(wexp_tm2_);
      LA::Copy(expression_weight_, &expression_weight_tm1_);
      expression_weight_(mask).copyTo(wexp_tm1_);
      is_tracking_ = true;
      // Sanity check
      bool valid = rigid_trsfrm_.CheckTransform();
      err = valid ? 0 : -1;
      is_tracking_ = valid;
      // Compute fit time
      t0 = static_cast<int>(cv::getTickCount()) - t0;
      fit_result->fit_time = ((static_cast<float>(t0) *
                               1000.f) /
                              static_cast<float>(cv::getTickFrequency()));
    } else {
      is_tracking_ = false;
    }
    previous_valid_shape_ = valid_shape_;
  }
  return err;
}

/*
 *  @name ResetTrackingState
 *  @fn void ResetTrackingState(void)
 *  @brief  Reinitialize tracking state
 */
void FaceModelFitter::ResetTrackingState(void) {
  is_tracking_ = false;
}

#pragma mark -
#pragma mark Contour extraction + matching

/*
 *  @name   ProcessContour
 *  @brief  Extract 3D contour and match it with 2D image
 *  @param  fitter_configuration    Fit parameters
 */
void FaceModelFitter::ProcessContour(const FitParameter& config) {
  // Extract contour
  // Use z camera axis as viewing direction and first view as a starting points
  auto rotation_it = rigid_trsfrm_.rotation.begin();
  std::vector<cv::Mat> all_view_direction;
  cv::Mat accumultate_rot_cam;
  if (projection_mode_ == ProjectionMode::kFullPerspective) {
    for (auto camera_it = camera_configuration_.cbegin();
         camera_it != camera_configuration_.cend();
         ++camera_it) {
      cv::Mat view_direction = (rotation_it->t() *
                                (*camera_it)->get_camera_axis_z());
      all_view_direction.push_back(view_direction);
      accumultate_rot_cam.push_back((*camera_it)->get_extrinsic_matrix());
    }

    // Process contour
    contour_extractor_->ExtractContour3D(all_view_direction);
    contour_extractor_->MatchContourWithImage(camera_configuration_,
                                              rigid_trsfrm_,
                                              config.image_contour,
                                              config.face_region_origin);
  } else {
    // Weak P
    for (int v = 0; v < number_of_view_; ++v) {
      cv::Mat view_direction = rotation_it->t();
      all_view_direction.push_back(view_direction(cv::Rect(2, 0, 1, 3)));
      accumultate_rot_cam.push_back(*rotation_it);
      accumultate_rot_cam.push_back(cv::Mat());
      ++rotation_it;
    }
    // Process contour
    contour_extractor_->ExtractContour3D(all_view_direction);
    contour_extractor_->MatchContourWithImage(rigid_trsfrm_,
                                              config.image_contour,
                                              config.face_region_origin,
                                              valid_shape_);
  }
}

/*
 *  @name   estimatetRigidTransformWeakPerspective
 *  @brief  Estimate rigid transformation using Weak perspective camera model
 *  @param  landmark    Facial landmarks for each views
 *  @param  config      Parameters for the current fit
 *  @param  iteration   Iteration number
 */
void FaceModelFitter::EstimatetRigidTransform(const std::vector<cv::Mat> &landmark,
                                              const FitParameter& config,
                                              const int iteration) {
  if (projection_mode_ == kFullPerspective) {
    // Landmarks selection for 2D points -> Occlusions
    this->PickLandmark(iteration, landmark);
    // Recover pose
    this->ComputeWeight(kLevMar, -1.f, -1.f, iteration);
    // Update contour if used
    if (use_contour_ && iteration > 0) {
      //   Compute surface corresponding to vertices laying possibly on contour
      this->ComputeMeshVertices(VertexSelection::kContour);
      //   Extract contour from 3D surface and match it to image input
      this->ProcessContour(config);
      //   Get contour position
      contour_image_position_ = contour_extractor_->get_matched_image_position();
    }
    // Compute shifted_feature_/face_rigid_transformation_.translation
    this->ApplyTransformation(iteration);
  } else {
    // 2D points, need to apply selection => First iteration pose/scale
    // is not available take all points, then for other iteration select
    // landmarks with previous pose/scale

    // Selection
    if (iteration > 0) {
      // Selection
      this->PickLandmark(iteration, landmark);
    } else {
      for (int k = 0; k < number_of_view_; ++k) {
        landmark[k].copyTo(local_landmark_[k]);
      }
    }
    // Recover rigid transformation
    this->ComputeWeight(kTransformation,
                        -1.f,
                        -1.f,
                        iteration);
    // Update rigid transformation estimation
    this->UpdateTransformation();
    // Update contour if used
    if (use_contour_ && iteration > 0) {
      // Compute surface corresponding to vertices laying possibly on contour
      this->ComputeMeshVertices(VertexSelection::kContour);
      // Extract contour from 3D surface and match it to image input
      this->ProcessContour(config);
      // Get contour position
      contour_image_position_ = contour_extractor_->get_matched_image_position();
    }
    // Compute shifted_feature_/face_rigid_transformation_.translation
    this->ApplyTransformation(iteration);
  }
}

/*
 *  @name   ProjectionFunction
 *  @brief  Project points during Levenberg-Marquardt algo.
 *  @param  n           Measurements
 *  @param  m           #Variables
 */
void  ProjectionFunction(float* rigid_parameter,
                         float* new_landmark,
                         int m,
                         int n,
                         void* data) {
  using TransposeType = LTS5::LinearAlgebra<float>::TransposeType;
  auto* local_data = (FaceModelFitter::TransformationData*) data;
  int n_point = local_data->number_landmark;
  int number_of_view = local_data->number_of_view;
  float gamma = rigid_parameter[0];
  float theta = rigid_parameter[1];
  float phi = rigid_parameter[2];
  float sin_gamma = std::sin(gamma);
  float sin_theta = std::sin(theta);
  float sin_phi = std::sin(phi);
  float cos_gamma = std::cos(gamma);
  float cos_theta = std::cos(theta);
  float cos_phi = std::cos(phi);
  float rotation[9];
  rotation[0] = cos_gamma * cos_phi;
  rotation[1] = -sin_gamma * cos_phi;
  rotation[2] = sin_phi;
  rotation[3] = sin_gamma * cos_theta + sin_theta * sin_phi * cos_gamma;
  rotation[4] = -sin_gamma * sin_theta*sin_phi + cos_theta * cos_gamma;
  rotation[5] = -sin_theta * cos_phi;
  rotation[6] = sin_theta*sin_gamma - sin_phi*cos_theta*cos_gamma;
  rotation[7] = -sin_gamma*sin_phi*cos_theta + sin_theta*cos_gamma;
  rotation[8] = cos_theta*cos_phi;
  float projected_pts[3];
  cv::Mat projected_pts_mat = cv::Mat(3, 1, CV_32FC1, (void*)&projected_pts[0]);
  // Goes through all views
  LTS5::VirtualCamera* camera;
  int index = 0;
  float point[4];
  cv::Mat point_mat = cv::Mat(4, 1, CV_32FC1, (void*)&point[0]);
  const float* obj_point = local_data->point_3d;
  float focal_x, focal_y;
  float xp, yp, xpp, ypp, r2, r4, r6, Kdist;
  for (int k = 0; k < number_of_view; k++) {
    if (local_data->shape_valid->at(k)) {
      camera = (*(local_data->camera_setup))[k];
      cv::Mat aExtrinsicMat = camera->get_extrinsic_matrix();
      const float* aF = camera->get_focal();
      focal_x = aF[0];
      focal_y = aF[1];
      const float* Kc = camera->get_distortion_coefficient();
      for (int i = 0; i < n_point; i++) {
        index = (k * n_point + i) * 2;
        // Is this landmark used for estimating the rigid transfo ?
        if (local_data->landmark[index] != 0.f) {
          // Yes
          // Project pts using full perspective model
          point[0] = (rigid_parameter[6] * (rotation[0] * obj_point[i*3] +
                                            rotation[1] * obj_point[i*3+1] +
                                            rotation[2] * obj_point[i*3+2]) +
                      rigid_parameter[3]);
          point[1] = (rigid_parameter[6] * (rotation[3] * obj_point[i*3] +
                                            rotation[4] * obj_point[i*3+1] +
                                            rotation[5] * obj_point[i*3+2]) +
                      rigid_parameter[4]);
          point[2] = (rigid_parameter[6] * (rotation[6] * obj_point[i*3] +
                                            rotation[7] * obj_point[i*3+1] +
                                            rotation[8] * obj_point[i*3+2]) +
                      rigid_parameter[5]);
          point[3] = 1.f;
          LTS5::LinearAlgebra<float>::Gemv(aExtrinsicMat,
                                           TransposeType::kNoTranspose,
                                           1.f,
                                           point_mat,
                                           0.f,
                                           &projected_pts_mat);
          // Add distortion
          xp = projected_pts[0] / projected_pts[2];
          yp = projected_pts[1] / projected_pts[2];
          r2 = xp * xp + yp * yp;
          r4 = r2 * r2;
          r6 = r4 * r2;
          Kdist = (1.f + Kc[0]*r2 + Kc[1]*r4 + Kc[4]*r6);
          xpp = xp * Kdist + (2.f * Kc[2] * xp * yp) + (Kc[3]*(r2 + 2.f*xp*xp));
          ypp = yp*Kdist + Kc[2]*(r2 + 2.f*yp*yp) + (2.f * Kc[3]*xp*yp);
          new_landmark[index] = focal_x*xpp;              // X
          new_landmark[index+1] = focal_y*ypp;            // Y
        } else {
          // No, points set to 0.f
          new_landmark[index] = 0.f;
          new_landmark[index+1] = 0.f;
        }
      }
    } else {
      memset(reinterpret_cast<void*>(&new_landmark[k * 2 * n_point]),
             0,
             n_point * 2 * sizeof(new_landmark[0]));
    }
  }
}

/**
 *  @name   JacobianFunction
 *  @brief  Compute jacobian matrix
 */
void JacobianFunction(float* rigid_parameter,
                      float* jacobian, int m, int n, void* data) {
  auto* local_data = (FaceModelFitter::TransformationData*) data;
  /* fill Jacobian row by row */
  int index_3d = 0;
  float x = 0.f, y = 0.f, z = 0.f, aU = 0.f, aV = 0.f, aW = 0.f;
  float aFuncF, aFuncG;
  float gamma = rigid_parameter[0];
  float theta = rigid_parameter[1];
  float phi = rigid_parameter[2];
  float sin_gamma = std::sin(gamma);
  float sin_theta = std::sin(theta);
  float sin_phi = std::sin(phi);
  float cos_gamma = std::cos(gamma);
  float cos_theta = std::cos(theta);
  float cos_phi = std::cos(phi);
  float rotation[9] = {};
  rotation[0] = cos_gamma*cos_phi;
  rotation[1] = -sin_gamma*cos_phi;
  rotation[2] = sin_phi;
  rotation[3] = sin_gamma*cos_theta + sin_theta*sin_phi*cos_gamma;
  rotation[4] = -sin_gamma*sin_theta*sin_phi + cos_theta*cos_gamma;
  rotation[5] = -sin_theta*cos_phi;
  rotation[6] = sin_theta*sin_gamma - sin_phi*cos_theta*cos_gamma;
  rotation[7] = -sin_gamma*sin_phi*cos_theta + sin_theta*cos_gamma;
  rotation[8] = cos_theta*cos_phi;
  float aTx = rigid_parameter[3];
  float aTy = rigid_parameter[4];
  float aTz = rigid_parameter[5];
  float aTcamX = 0.f, aTcamY = 0.f, aTcamZ = 0.f;
  float aA11 = 0.f, aA12 = 0.f, aA13 = 0.f, aA21 = 0.f, aA22 = 0.f;
  float aA23 = 0.f, aA31 = 0.f, aA32 = 0.f, aA33 = 0.f;
  float focal_x, focal_y;
  int n_point = local_data->number_landmark;
  int number_of_view = local_data->number_of_view;
  float scale = local_data->estimated_parameter[6];
  LTS5::VirtualCamera* camera;
  const float * ext_cam_ptr;
  for (int k=0, j=0; k < number_of_view; k++) {
    camera = (*local_data->camera_setup)[k];
    cv::Mat aExtCam = camera->get_extrinsic_matrix();
    ext_cam_ptr = reinterpret_cast<const float*>(aExtCam.data);
    aTcamX = ext_cam_ptr[3];
    aTcamY = ext_cam_ptr[7];
    aTcamZ = ext_cam_ptr[11];
    aA11 = ext_cam_ptr[0];
    aA12 = ext_cam_ptr[1];
    aA13 = ext_cam_ptr[2];
    aA21 = ext_cam_ptr[4];
    aA22 = ext_cam_ptr[5];
    aA23 = ext_cam_ptr[6];
    aA31 = ext_cam_ptr[8];
    aA32 = ext_cam_ptr[9];
    aA33 = ext_cam_ptr[10];
    const float* aF = camera->get_focal();
    focal_x = aF[0];
    focal_y = aF[1];
    for (int i=0 ; i < n_point; i++) {
      index_3d = i*3;
      x = local_data->point_3d[index_3d];
      y = local_data->point_3d[index_3d+1];
      z = local_data->point_3d[index_3d+2];
      aU = (focal_x *
            (scale * aA11 *
             (rotation[0] * x + rotation[1] * y + rotation[2] * z) +
             aA11 * aTx +
             scale * aA12 *
             (rotation[3] * x + rotation[4] * y + rotation[5] * z) +
             aA12 * aTy + scale * aA13 * (rotation[6] * x + rotation[7] * y +
                                          rotation[8] * z) + aA13 * aTz + aTcamX));
      aV = (focal_y * (scale * aA21 * (rotation[0] * x + rotation[1] * y +
                                       rotation[2] * z) + aA21 * aTx +
                       scale * aA22 * (rotation[3] * x + rotation[4] * y +
                                       rotation[5] * z) + aA22 * aTy +
                       scale * aA23 * (rotation[6] * x + rotation[7] * y +
                                       rotation[8] * z) + aA23 * aTz + aTcamY));
      aW = (scale * aA31 * (rotation[0] * x + rotation[1] * y +
                            rotation[2] * z) + aA31 * aTx +
            scale * aA32 * (rotation[3] * x + rotation[4] * y +
                            rotation[5] * z) + aA32 * aTy +
            scale * aA33 * (rotation[6] * x + rotation[7] * y +
                            rotation[8] * z) + aA33 * aTz + aTcamZ);

      // --------------------------------------------------------------------
      //  dU/dP
      // --------------------------------------------------------------------
      // du/dGamma
      aFuncF = (scale * focal_x * (aA11 * (-cos_phi * sin_gamma * x -
                                           cos_gamma * cos_phi * y) +
                                   aA12 * ((cos_gamma * cos_theta +
                                            sin_theta * sin_phi *
                                            cos_gamma) * x -
                                           (cos_gamma * sin_theta *
                                            sin_phi + cos_theta *
                                            sin_gamma) * y) +
                                   aA13 * ((sin_theta * cos_phi + sin_phi *
                                            sin_gamma * cos_theta) * x +
                                           (sin_phi * cos_gamma *
                                            cos_theta - sin_theta *
                                            sin_gamma) * y)));
      aFuncG = (scale * aA31 * (-cos_phi * sin_gamma * x - cos_gamma *
                                cos_phi * y) +
                aA32 * ((cos_gamma * cos_theta + sin_theta * sin_phi *
                         cos_gamma) * x - (cos_gamma * sin_theta * sin_phi +
                                           cos_theta * sin_gamma) * y) +
                aA33 * ((sin_theta * cos_phi + sin_phi * sin_gamma *
                         cos_theta) * x + (sin_phi * cos_gamma * cos_theta -
                                           sin_theta * sin_gamma) * y));
      jacobian[j++] = (aFuncF * aW - aU * aFuncG)/(aW * aW);
      // du/dTheta
      aFuncF = (scale * focal_x * (aA12 * ((-sin_gamma * sin_theta +
                                            cos_theta * sin_phi * cos_gamma)
                                           * x - (sin_gamma * sin_theta *
                                                  sin_phi + sin_theta *
                                                  cos_gamma) * y -
                                           cos_theta * cos_phi * z) +
                                   aA13 * ((cos_theta * sin_gamma + sin_phi *
                                            sin_theta * cos_gamma) * x +
                                           (-sin_phi * sin_gamma * sin_theta +
                                            cos_theta * cos_gamma) * y -
                                           sin_theta * cos_phi * z)));
      aFuncG = (scale * aA32 * ((-sin_gamma * sin_theta + cos_theta *
                                 sin_phi * cos_gamma) * x -
                                (sin_gamma * sin_theta * sin_phi +
                                 sin_theta * cos_gamma) * y -
                                cos_theta * cos_phi * z) +
                aA33 * ((cos_theta * sin_gamma + sin_phi * sin_theta *
                         cos_gamma) * x + (-sin_phi * sin_gamma * sin_theta +
                                           cos_theta * cos_gamma) * y -
                        sin_theta * cos_phi * z));
      jacobian[j++] = (aFuncF * aW - aU * aFuncG)/(aW * aW);
      // du/dPhi
      aFuncF = (scale * focal_x * (aA11 * (-sin_phi * cos_gamma * x +
                                           sin_gamma * sin_phi * y + cos_phi *
                                           z) +
                                   aA12 * (sin_theta * cos_phi * x -
                                           sin_gamma * sin_theta * cos_phi *
                                           y + sin_theta * sin_phi * z) +
                                   aA13 * (-cos_phi * cos_gamma * cos_theta *
                                           x + cos_phi * sin_gamma *
                                           cos_theta * y - cos_theta *
                                           sin_phi * z)));
      aFuncG = (scale *
                aA31 * (-sin_phi * cos_gamma * x + sin_gamma * sin_phi * y +
                        cos_phi * z) +
                aA32 * (sin_theta * cos_phi * x - sin_gamma * sin_theta *
                        cos_phi * y + sin_theta * sin_phi * z) +
                aA33 * (-cos_phi * cos_gamma * cos_theta * x +
                        cos_phi * sin_gamma * cos_theta * y - cos_theta *
                        sin_phi * z));
      jacobian[j++] = (aFuncF * aW - aU * aFuncG)/(aW * aW);
      // du/dTx
      aFuncF = focal_x * aA11;
      aFuncG = aA31;
      jacobian[j++] = (aFuncF * aW - aU * aFuncG)/(aW * aW);
      // du/dTy
      aFuncF = focal_x * aA12;
      aFuncG = aA32;
      jacobian[j++] = (aFuncF * aW - aU * aFuncG)/(aW * aW);
      // du/dTz
      aFuncF = focal_x * aA13;
      aFuncG = aA33;
      jacobian[j++] = (aFuncF * aW - aU * aFuncG)/(aW * aW);
      // du/ds
      aFuncF = (focal_x *
                (aA11 * (rotation[0] * x + rotation[1] * y +
                         rotation[2] * z) +
                 aA12 * (rotation[3] * x + rotation[4] * y +
                         rotation[5] * z) +
                 aA13 * (rotation[6] * x + rotation[7] * y +
                         rotation[8] * z)));
      aFuncG = (aA31 * (rotation[0] * x + rotation[1] * y + rotation[2] * z) +
                aA32 * (rotation[3] * x + rotation[4] * y + rotation[5] * z) +
                aA33 * (rotation[6] * x + rotation[7] * y + rotation[8] * z));
      jacobian[j++] = (aFuncF * aW - aU * aFuncG)/(aW * aW);
      // --------------------------------------------------------------------
      //  dV/dP
      // --------------------------------------------------------------------
      // dv/dGamma
      aFuncF = (scale * focal_y * (aA21 * (-cos_phi * sin_gamma * x -
                                           cos_gamma * cos_phi * y) +
                                   aA22 * ((cos_gamma * cos_theta +
                                            sin_theta * sin_phi * cos_gamma) *
                                           x - (cos_gamma * sin_theta *
                                                sin_phi + cos_theta *
                                                sin_gamma) * y) +
                                   aA13 * ((sin_theta * cos_phi + sin_phi *
                                            sin_gamma * cos_theta) * x +
                                           (sin_phi * cos_gamma * cos_theta -
                                            sin_theta * sin_gamma) * y)));
      aFuncG = (scale * aA31 * (-cos_phi * sin_gamma * x - cos_gamma *
                                cos_phi * y) +
                aA32 * ((cos_gamma * cos_theta + sin_theta * sin_phi *
                         cos_gamma) * x - (cos_gamma * sin_theta * sin_phi +
                                           cos_theta * sin_gamma) * y) +
                aA33 * ((sin_theta * cos_phi + sin_phi * sin_gamma *
                         cos_theta) * x + (sin_phi * cos_gamma * cos_theta -
                                           sin_theta * sin_gamma) * y));
      jacobian[j++] = (aFuncF * aW - aV * aFuncG)/(aW * aW);
      // dv/dTheta
      aFuncF = (scale * focal_y * (aA22 * ((-sin_gamma * sin_theta +
                                            cos_theta * sin_phi * cos_gamma) *
                                           x - (sin_gamma * sin_theta *
                                                sin_phi + sin_theta *
                                                cos_gamma) * y - cos_theta *
                                           cos_phi * z) +
                                   aA23 * ((cos_theta * sin_gamma + sin_phi *
                                            sin_theta * cos_gamma) * x +
                                           (-sin_phi * sin_gamma * sin_theta +
                                            cos_theta * cos_gamma) * y -
                                           sin_theta * cos_phi * z)));
      aFuncG = (scale * aA32 * ((-sin_gamma * sin_theta + cos_theta *
                                 sin_phi * cos_gamma) * x -
                                (sin_gamma * sin_theta * sin_phi + sin_theta *
                                 cos_gamma) * y - cos_theta * cos_phi * z) +
                aA33 * ((cos_theta * sin_gamma + sin_phi * sin_theta *
                         cos_gamma) * x + (-sin_phi * sin_gamma * sin_theta +
                                           cos_theta * cos_gamma) * y -
                        sin_theta * cos_phi * z));
      jacobian[j++] = (aFuncF * aW - aV * aFuncG)/(aW * aW);
      // dv/dPhi
      aFuncF = (scale * focal_y *
                (aA21 * (-sin_phi * cos_gamma * x +
                         sin_gamma * sin_phi * y + cos_phi * z) +
                 aA22 * (sin_theta * cos_phi * x - sin_gamma * sin_theta *
                         cos_phi * y + sin_theta * sin_phi * z) +
                 aA23 * (-cos_phi * cos_gamma * cos_theta * x + cos_phi *
                         sin_gamma * cos_theta * y - cos_theta * sin_phi *
                         z)));
      aFuncG = (scale *
                aA31 * (-sin_phi * cos_gamma * x + sin_gamma * sin_phi * y +
                        cos_phi * z) +
                aA32 * (sin_theta * cos_phi * x - sin_gamma * sin_theta *
                        cos_phi * y + sin_theta * sin_phi * z) +
                aA33 * (-cos_phi * cos_gamma * cos_theta * x + cos_phi *
                        sin_gamma * cos_theta * y - cos_theta * sin_phi * z));
      jacobian[j++] = (aFuncF * aW - aV * aFuncG)/(aW * aW);
      // dv/dTx
      aFuncF = focal_y * aA21;
      aFuncG = aA31;
      jacobian[j++] = (aFuncF * aW - aV * aFuncG)/(aW * aW);
      // dv/dTy
      aFuncF = focal_y * aA22;
      aFuncG = aA32;
      jacobian[j++] = (aFuncF * aW - aV * aFuncG)/(aW * aW);
      // dv/dTz
      aFuncF = focal_y * aA23;
      aFuncG = aA33;
      jacobian[j++] = (aFuncF * aW - aV * aFuncG)/(aW * aW);
      // dv/ds
      aFuncF = (focal_y * (aA21 * (rotation[0] * x + rotation[1] * y +
                                   rotation[2] * z) +
                           aA22 * (rotation[3] * x + rotation[4] * y +
                                   rotation[5] * z) +
                           aA23 * (rotation[6] * x + rotation[7] * y +
                                   rotation[8] * z)));
      aFuncG = (aA31 * (rotation[0] * x + rotation[1] * y +
                        rotation[2] * z) +
                aA32 * (rotation[3] * x + rotation[4] * y +
                        rotation[5] * z) +
                aA33 * (rotation[6] * x + rotation[7] * y +
                        rotation[8] * z));
      jacobian[j++] = (aFuncF * aW - aV * aFuncG)/(aW * aW);
    }
  }
}

/*
 *  @name   CreateLandmarkSelectionMask
 *  @brief  Compute the landmark selection based on ray tracing methods.
 *          If one ray from one landmark to the camera origin is \n
 *          intercepted by the shape (face) it is discarded.
 *  @param  iteration       Current iteration
 */
void FaceModelFitter::PickLandmark(const int iteration,
                                   const std::vector<cv::Mat>& landmark) {
  float* landmark_ptr = reinterpret_cast<float*>(mean_shape_landmark_.data);
  float camera_org[3];
  for (int k = 0; k < number_of_view_; k++) {
    if (valid_shape_[k]) {
      // Get optical axis
      if (projection_mode_ == kFullPerspective) {
        // Define camera origin
        LTS5::VirtualCamera* camera = camera_configuration_.at(k);
        cv::Mat cam_org_mat = cv::Mat(1,
                                      3,
                                      CV_32FC1,
                                      const_cast<float*>(camera->get_position()));
        // Apply rotate camera instead of shape
        cv::Mat global_cam_org = cam_org_mat * rigid_trsfrm_.rotation[0];
        camera_org[0] = global_cam_org.at<float>(0);
        camera_org[1] = global_cam_org.at<float>(1);
        camera_org[2] = global_cam_org.at<float>(2);
      } else {
        cv::Mat& rotation = rigid_trsfrm_.rotation[k];
        // Get camera position - assumed to be far.
        camera_org[0] = rotation.at<float>(2, 0) * camera_distance_;
        camera_org[1] = rotation.at<float>(2, 1) * camera_distance_;
        camera_org[2] = rotation.at<float>(2, 2) * camera_distance_;
      }
      // Check for collision
      const Vector3<float> cam_pos(camera_org[0],
                                   camera_org[1],
                                   camera_org[2]);
      const float* input_ptr = reinterpret_cast<const float*>(landmark[k].data);
      const float* img_cent_ptr = reinterpret_cast<float*>(image_center_shift_[k].data);
      float* local_ptr = reinterpret_cast<float*>(local_landmark_[k].data);

      Parallel::For(number_2D_features_,
                    [&](const size_t& i) {
        const Vector3<float> Q(landmark_ptr[i*3],
                               landmark_ptr[i*3 + 1],
                               landmark_ptr[i*3 + 2]);
        float t = -1.f;
        bool inter = collision_tree_->IntersectWithSegment(*mean_shape_mesh_,
                                                           cam_pos,
                                                           Q,
                                                           (1.f - 1e-6f),
                                                           &t);
        if (inter) {
          local_ptr[2*i] = 0.f;
          local_ptr[2*i + 1] = 0.f;
        } else {
          local_ptr[2*i] = input_ptr[2*i] - img_cent_ptr[0];
          local_ptr[2*i + 1] = input_ptr[2*i +1] - img_cent_ptr[1];
        }
      });
    }
  }
}

/*
 *  @name   EstimateFacePosition
 *  @brief  Estimate the position of the head in the space by reprojecting
 *          landmarks
 *          - First determine which cameras to use
 *          - Compute centroid and use it as position estimation
 *  @param  transformation     3D shape data structure
 */
void FaceModelFitter::EstimateFacePosition(TransformationData* transformation) {
  // How many control points ?
  const int n_control_point = (sizeof(control_landmark_) /
                               sizeof(control_landmark_[0]));
  bool* visible_landmark = new bool[n_control_point];
  // Estimate between left - right cameras
  int n_visible_point = 0;
  // Back projection variables
  cv::Mat sum_3d_pts = cv::Mat(3, 1, CV_32FC1, 0.f);
  cv::Mat shape_3d = cv::Mat(number_2D_features_ * 3,
                             1,
                             CV_32FC1,
                             (void*)transformation->point_3d);
  // Accumulated pts from Model shapes - for translation compensation
  cv::Mat sum_3d_model_pts = cv::Mat(3, 1, CV_32FC1, 0.f);
  cv::Mat* points_3d = new cv::Mat[n_control_point];
  float model_dist = 0.f, object_dist = 0.f;
  int last_visible_index = -1;
  for (int k = 0; k < n_control_point; k++) {
    if (number_of_view_ == 2) {
      if (IsPointVisibleFromCamera(camera_pair_index_[0],
                                   camera_pair_index_[1],
                                   control_landmark_[k])) {
        visible_landmark[k] = true;
        n_visible_point++;

        // Since we're looping overall control points take the time to
        // back project point and estimate scale at the same time
        // Backproject landmarks and accumalate 3D position and pts from model
        points_3d[k] = cv::Mat(3, 1, CV_32FC1);
        this->BackProjectLandmark(camera_pair_index_[0],
                                  camera_pair_index_[1],
                                  control_landmark_[k],
                                  &points_3d[k]);
        sum_3d_pts += points_3d[k];
        sum_3d_model_pts += shape_3d(cv::Rect(0,
                                              control_landmark_[k] * 3,
                                              1,
                                              3));

        // Estimate scale
        if (k > 0 && last_visible_index >= 0) {
          model_dist += cv::norm(shape_3d(cv::Rect(0,
                                                   control_landmark_[k] * 3,
                                                   1,
                                                   3)) -
                                 shape_3d(cv::Rect(0,
                                                   control_landmark_[last_visible_index] * 3,
                                                   1,
                                                   3)));
          object_dist += (cv::norm(points_3d[k] -
                                   points_3d[last_visible_index]));
        }

        // Save index for future distance computations
        last_visible_index = k;
      } else {
        visible_landmark[k] = false;
      }
    } else {
      // Need to check pairs
      // Three possible choice :   Left-Mid / Mid-Right / Left-Right

      // How many pairs ?
      int n_pair = (sizeof(camera_pair_index_) / sizeof(camera_pair_index_[0]))/2;
      for (int m = 0; m < n_pair; m++) {
        if (IsPointVisibleFromCamera(camera_pair_index_[2*m],
                                     camera_pair_index_[2*m+1],
                                     control_landmark_[k])) {
          // Visible from current pair
          visible_landmark[k] = true;
          n_visible_point++;

          // Since we're looping overall control points take the time to back
          // project point and estimate scale at the same time
          points_3d[k] = cv::Mat(3, 1, CV_32FC1);
          // Backproject landmarks and accumalate 3D position and pts from model
          this->BackProjectLandmark(camera_pair_index_[2*m],
                                    camera_pair_index_[2*m+1],
                                    control_landmark_[k],
                                    &points_3d[k]);
          sum_3d_pts += points_3d[k];
          sum_3d_model_pts += shape_3d(cv::Rect(0,
                                                control_landmark_[k] * 3,
                                                1,
                                                3));

          // Estimate scale
          if (k > 0 && last_visible_index >= 0) {
            model_dist += cv::norm(shape_3d(cv::Rect(0,
                                                     control_landmark_[k] * 3,
                                                     1,
                                                     3)) -
                                   shape_3d(cv::Rect(0,
                                                     control_landmark_[last_visible_index] * 3,
                                                     1,
                                                     3)));
            object_dist += (cv::norm(points_3d[k] -
                                     points_3d[last_visible_index]));
          }

          // Save index for future distance computations
          last_visible_index = k;

          // Don't need to check other pairs
          break;
        } else {
          visible_landmark[k] = false;
        }
      }
    }
  }
  delete [] visible_landmark;
  delete [] points_3d;
  // Back project pts
  if (n_visible_point > 1) {
    // Init scale
    float init_scale = object_dist / model_dist;
    // Average
    sum_3d_pts /= n_visible_point;
    sum_3d_model_pts /= n_visible_point;
    cv::Mat avg_rot_model_3d_pts = ((rigid_trsfrm_.rotation[0] *
                                     sum_3d_model_pts) *
                                    init_scale);
    transformation->estimated_parameter[3] = (sum_3d_pts.at<float>(0) -
                                              avg_rot_model_3d_pts.at<float>(0));
    transformation->estimated_parameter[4] = (sum_3d_pts.at<float>(1) -
                                              avg_rot_model_3d_pts.at<float>(1));
    transformation->estimated_parameter[5] = (sum_3d_pts.at<float>(2) -
                                              avg_rot_model_3d_pts.at<float>(2));
    transformation->estimated_parameter[6] = init_scale;

    if (std::isnan(init_scale)) {
      LTS5_LOG_ERROR("Solver return NaN for initial scale");
    }
  } else {

    LTS5_LOG_WARNING("No visible landmarks to initialize head position");
    LTS5_LOG_WARNING("Init : (x,y,z) = (0, 0, -500), s = 100");
    transformation->estimated_parameter[3] = 0.f;
    transformation->estimated_parameter[4] = 0.f;
    transformation->estimated_parameter[5] = -500.f;
    transformation->estimated_parameter[6] = 100.f;
  }
}

/*
 *  @name   EstimateFaceSize
 *  @brief  Estimate the size of the head for single view
 *  @param  transformation     3D shape data structure
 */
void FaceModelFitter::EstimateFaceSize(TransformationData* transformation) {
  // How many pts used for estimating head size ?
  const int n_control_point = (sizeof(control_landmark_) /
                               sizeof(control_landmark_[0]));

  float image_dist = 0.f;
  float model_dist = 0.f;
  int n_visible_point = 0;
  int last_visible_index = -1;
  cv::Mat shape_3d = cv::Mat(number_2D_features_ * 3,
                             1,
                             CV_32FC1,
                             (void*)transformation->point_3d);
  cv::Mat image_shape = local_landmark_[0];
  if (valid_shape_[0]) {
    for (int k = 0; k < n_control_point; k++) {
      // If vertex visible
      if (image_shape.at<float>(control_landmark_[k]*2) != 0.f) {
        // Visible
        if (last_visible_index >= 0) {
          // Compute distance
          model_dist += cv::norm(shape_3d(cv::Rect(0,
                                                   control_landmark_[k] * 3,
                                                   1,
                                                   3)) -
                                 shape_3d(cv::Rect(0,
                                                   control_landmark_[last_visible_index] * 3,
                                                   1,
                                                   3)));
          image_dist += cv::norm(image_shape(cv::Rect(0,
                                                      control_landmark_[k] * 2,
                                                      1,
                                                      2)) -
                                 image_shape(cv::Rect(0,
                                                      control_landmark_[last_visible_index] * 2,
                                                      1,
                                                      2)));
        }
        last_visible_index = k;
        n_visible_point++;
      }
    }
    // Scale ?
    if (image_dist > 0.f) {
      rigid_trsfrm_.scale[0] = image_dist / model_dist;
      last_visible_index = -1;
      cv::Mat image_shape_new = cv::Mat(2 * n_control_point, 1, CV_32FC1);
      cv::Mat point = cv::Mat::ones(4, 1, CV_32FC1);
      cv::Mat last_visible = cv::Mat(2, 1, CV_32FC1);
      cv::Mat rotation = rigid_trsfrm_.rotation[0];
      cv::Mat extension = camera_configuration_[0]->get_extrinsic_matrix();
      cv::Mat K = cv::Mat::eye(3, 3, CV_32FC1);
      K.at<float>(0, 0) = camera_configuration_[0]->get_focal()[0];
      K.at<float>(1, 1) = camera_configuration_[0]->get_focal()[1];
      float new_image_dist = 0.f;
      // Reproject using current parameters estimation
      for (int k = 0; k < n_control_point; k++) {
        if (image_shape.at<float>(control_landmark_[k]*2) != 0.f) {
          cv::Mat rot_point = ((rotation * shape_3d(cv::Rect(0,
                                                             control_landmark_[k] * 3,
                                                             1,
                                                             3))) *
                               rigid_trsfrm_.scale[0]);
          rot_point.copyTo(point(cv::Rect(0, 0, 1, 3)));
          rot_point = K * extension * point;

          image_shape_new.at<float>(2*k) = (rot_point.at<float>(0) /
                                            rot_point.at<float>(2));
          image_shape_new.at<float>(2*k+1) = (rot_point.at<float>(1) /
                                              rot_point.at<float>(2));
          // Visible
          if (last_visible_index >= 0) {
            new_image_dist += cv::norm(image_shape_new(cv::Rect(0,
                                                                2*k,
                                                                1,
                                                                2)) -
                                       last_visible);
          }

          last_visible.at<float>(0) = image_shape_new.at<float>(2*k);
          last_visible.at<float>(1) = image_shape_new.at<float>(2*k+1);
          last_visible_index = k;
        }
      }
      // Correction
      float ratio = new_image_dist / image_dist;
      rigid_trsfrm_.scale[0] = image_dist / (ratio * model_dist);
      transformation->estimated_parameter[3] = 0.f;
      transformation->estimated_parameter[4] = 0.f;
      transformation->estimated_parameter[5] = -500.f;
      transformation->estimated_parameter[6] = 100.f;
    } else {
      rigid_trsfrm_.scale[0] = 100.f;
      transformation->estimated_parameter[3] = 0.f;
      transformation->estimated_parameter[4] = 0.f;
      transformation->estimated_parameter[5] = -500.f;
      transformation->estimated_parameter[6] = 100.f;
    }
  } else {
    rigid_trsfrm_.scale[0] = 100.f;
    transformation->estimated_parameter[3] = 0.f;
    transformation->estimated_parameter[4] = 0.f;
    transformation->estimated_parameter[5] = -500.f;
    transformation->estimated_parameter[6] = 100.f;
  }
}

/*
 *  @name   IsPointVisibleFromCamera
 *  @brief  Indicates if a landmarks is visible from a pair of cameras
 *  @param  camera_0_index    First Camera index
 *  @param  camera_0_index    Second Camera index
 *  @param  point_index     Landmark index
 *  @return True if visible
 */
bool FaceModelFitter::IsPointVisibleFromCamera(const int camera_0_index,
                                               const int camera_1_index,
                                               const int point_index) {
  if (valid_shape_[camera_0_index] && valid_shape_[camera_1_index]) {
    return ((local_landmark_[camera_0_index].at<float>(point_index*2) != 0.f) &&
            (local_landmark_[camera_1_index].at<float>(point_index*2) != 0.f));
  } else {
    return false;
  }
}

/*
 *  @name   BackProjectLandmark
 *  @brief  Back project point by triangulation
 *  @param  camera_0_index
 *  @param  camera_1_index
 *  @param  landmark_index
 *  @param  point
 */
void FaceModelFitter::BackProjectLandmark(const int camera_0_index,
                                          const int camera_1_index,
                                          const int landmark_index,
                                          cv::Mat* point) {
  //  P = C0 + lamda_0 * R^ K^ p0 = C1 + lambda_1 + R1^ K1^ p1
  // ----------------------------------------------------------
  // Camera parameters
  cv::Mat extension = (camera_configuration_[camera_0_index]
                       ->get_extrinsic_matrix())(cv::Rect(0, 0, 3, 3));
  cv::Mat intrinsic = camera_configuration_[camera_0_index]->get_intrinsic_matrix();
  //intrinsic.at<float>(0,2) = 0.f;
  //intrinsic.at<float>(1,2) = 0.f;
  // Landmarks
  cv::Mat p0 = cv::Mat::ones(3, 1, CV_32FC1);
  p0.at<float>(0) = (*real_landmark_)[camera_0_index].at<float>(landmark_index*2);
  p0.at<float>(1) = (*real_landmark_)[camera_0_index].at<float>(landmark_index*2+1);
  cv::Mat p1 = cv::Mat::ones(3, 1, CV_32FC1);
  p1.at<float>(0) = (*real_landmark_)[camera_1_index].at<float>(landmark_index*2);
  p1.at<float>(1) = (*real_landmark_)[camera_1_index].at<float>(landmark_index*2+1);
  // D mat
  cv::Mat d0 = extension.t() * (intrinsic.inv() * p0);
  extension = (camera_configuration_[camera_1_index]
               ->get_extrinsic_matrix())(cv::Rect(0, 0, 3, 3));
  intrinsic = camera_configuration_[camera_1_index]->get_intrinsic_matrix();
  //intrinsic.at<float>(0,2) = 0.f;
  //intrinsic.at<float>(1,2) = 0.f;
  cv::Mat d1 = extension.t() * (intrinsic.inv() * p1);
  cv::Mat aD = cv::Mat(3, 2, CV_32FC1);
  cv::hconcat(d0, -d1, aD);
  // Center
  cv::Mat c0 = cv::Mat(3,
                       1,
                       CV_32FC1,
                       const_cast<float*>(camera_configuration_[camera_0_index]
                                          ->get_position()));
  cv::Mat c1 = cv::Mat(3,
                       1,
                       CV_32FC1,
                       const_cast<float*>(camera_configuration_[camera_1_index]
                                          ->get_position()));
  cv::Mat c = c1 - c0;
  // Solve for lamba
  cv::Mat lambda = aD.inv(cv::DECOMP_SVD) * c;
  // Compute point location
  *point = lambda.at<float>(0) * d0 + c0;
}

/*
 *  @name   estimateRigidTransform
 *  @brief  Estimate rigid transformation (i.e pose/location) using non-linear
 *          model (Full perspective). Minimization is done through \n
 *          Levenberg-Marquardt iterative algorithm.
 *  @param  TransformationData    Pointer to structure holding parameters
 *          such as landmark/object points/...
 */
void FaceModelFitter::EstimateRigidTransformSolver(TransformationData* transformation) {
  // Init parameters
  int n_params = (sizeof(transformation->estimated_parameter) /
                  sizeof(transformation->estimated_parameter[0]));
  float opts[LM_OPTS_SZ], info[LM_INFO_SZ];

  /* optimization control parameters; passing to levmar NULL instead of opts
   reverts to defaults */
  opts[0]= 1E4;/*LM_INIT_MU*/;
  opts[1] = 1E-15;
  opts[2] = 1E-15;
  opts[3] = 1E-4;
  // relevant only if the finite difference Jacobian version is used
  opts[4] = LM_DIFF_DELTA;
  /*  I: minim. options [\mu, \epsilon1, \epsilon2, \epsilon3].
   *  Respectively the scale factor for initial \mu, stopping thresholds
   *  for ||J^T e||_inf, ||Dp||_2 and ||e||_2. Set to NULL for defaults
   *  to be used
   */
  /* invoke the optimization function */
  //   #Variables
  int m = n_params;
  //   #Measurements
  int n = transformation->number_landmark * transformation->number_of_view * 2;
  int ret = slevmar_der(ProjectionFunction,
                        JacobianFunction,
                        static_cast<float*>(transformation->estimated_parameter),
                        transformation->landmark,
                        m,
                        n,
                        300,
                        opts,
                        info,
                        NULL,
                        NULL,
                        reinterpret_cast<void*>(transformation));
  /* O: information regarding the minimization. Set to NULL if don't care
   * info[0]= ||e||_2 at initial p.
   * info[1-4]=[ ||e||_2, ||J^T e||_inf,  ||Dp||_2, mu/max[J^T J]_ii ],
   * all computed at estimated p.
   * info[5]= # iterations,
   * info[6]=reason for terminating: 1 - stopped by small gradient J^T e
   *                                 2 - stopped by small Dp
   *                                 3 - stopped by itmax
   *                                 4 - singular matrix. Restart from
   *                                     current p with increased mu
   *                                 5 - no further error reduction is
   *                                     possible. Restart with increased mu
   *                                 6 - stopped by small ||e||_2
   *                                 7 - stopped by invalid (i.e. NaN or
   *                                     Inf)"func"values.This is a user error
   * info[7]= # function evaluations
   * info[8]= # Jacobian evaluations
   * info[9]= # linear systems solved, i.e. # attempts for reducing error
   */
  // Copy result
  rigid_trsfrm_.euler_angle[0] = transformation->estimated_parameter[0];
  rigid_trsfrm_.euler_angle[1] = transformation->estimated_parameter[1];
  rigid_trsfrm_.euler_angle[2] = transformation->estimated_parameter[2];
  rigid_trsfrm_.translation[0].at<float>(0) = transformation->estimated_parameter[3];
  rigid_trsfrm_.translation[0].at<float>(1) = transformation->estimated_parameter[4];
  rigid_trsfrm_.translation[0].at<float>(2) = transformation->estimated_parameter[5];
  rigid_trsfrm_.scale[0] = transformation->estimated_parameter[6];
  LTS5::Utilities::GetRotationMatrix(rigid_trsfrm_.euler_angle[0],
                                     rigid_trsfrm_.euler_angle[1],
                                     rigid_trsfrm_.euler_angle[2],
                                     LTS5::Utilities::RotationDimension::k3d3d,
                                     true,
                                     &rigid_trsfrm_.rotation[0]);
  static const char* converter[] = {"",
                                    "stopped by small gradient J^T e",
                                    "stopped by small Dp",
                                    "stopped by itmax",
                                    "singular matrix. Restart from current p with increased mu",
                                    "no further error reduction is possible. Restart with increased mu",
                                    "stopped by small ||e||_2",
                                    "stopped by invalid (i.e. NaN or Inf) 'func' values. This is a user error"};
  int aReason = static_cast<int>(info[6]);
  LTS5_LOG_DEBUG(converter[aReason]);

  LTS5_LOG_DEBUG1("#Iter : " << ret);
  LTS5_LOG_DEBUG1("Scale : " << transformation->estimated_parameter[6]);
  LTS5_LOG_DEBUG1("Gamma : " << transformation->estimated_parameter[0] * (180.f/CV_PI));
  LTS5_LOG_DEBUG1("Theta : " << transformation->estimated_parameter[1] * (180.f/CV_PI));
  LTS5_LOG_DEBUG1("Phi : " << transformation->estimated_parameter[2] * (180.f/CV_PI));
  LTS5_LOG_DEBUG1("Tx : " << transformation->estimated_parameter[3]);
  LTS5_LOG_DEBUG1("Ty : " << transformation->estimated_parameter[4]);
  LTS5_LOG_DEBUG1("Tz : " << transformation->estimated_parameter[5]);
}

#pragma mark -
#pragma mark Generate Mesh

/*
 *  @name   ComputeMeshVertices
 *  @brief  Compute mesh vertices from model parameters
 *  @param  selection   Indicate what type of vertices has to be
 *                      reconstruct (All of them, only landmarks,...)
 *  @param  with_rigid_transformation     Indicates if reconstruction need
 *                to take into account the rigid transfromation or only the
 *                non-rigid part (i.e generate using only face model)
 */
void FaceModelFitter::ComputeMeshVertices(const VertexSelection& selection,
                                          bool with_rigid_transformation) {
  using TransposeType = LTS5::LinearAlgebra<float>::TransposeType;
  switch (selection) {
    case kFull : {
/*#ifdef HAS_CUDA_
      d_face_model_->Generate(identity_weight_,
                              expression_weight_,
                              &face_vertex_);/
#else*/
      // Mulitply model time identity weights
      face_model_->ModelTimesWeightVector(BilinearFaceModel::ModelDimensionEnum::kId,
                                          identity_weight_,
                                          nullptr,
                                          &data_vertex_expr_);
      // Multiply resulting matrix time expression wieghts
      LTS5::LinearAlgebra<float>::Gemv(data_vertex_expr_,
                                       TransposeType::kNoTranspose,
                                       1.f,
                                       expression_weight_,
                                       0.f,
                                       &face_vertex_);
      // Apply transformation if needed
      if (with_rigid_transformation) {
        face_vertex_ *= rigid_trsfrm_.scale[0];
        float gamma = rigid_trsfrm_.euler_angle[0];
        float theta = rigid_trsfrm_.euler_angle[1];
        float phi = rigid_trsfrm_.euler_angle[2];
        float sin_gamma = std::sin(gamma);
        float sin_theta = std::sin(theta);
        float sin_phi = std::sin(phi);
        float cos_gamma = std::cos(gamma);
        float cos_theta = std::cos(theta);
        float cos_phi = std::cos(phi);
        cv::Mat rotation = cv::Mat(3, 4, CV_32FC1);
        rotation.at<float>(0, 0) = cos_phi*cos_gamma;
        rotation.at<float>(0, 1) = - sin_gamma*cos_phi;
        rotation.at<float>(0, 2) = sin_phi;
        rotation.at<float>(1, 0) = (sin_gamma * cos_theta +
                                   sin_theta * sin_phi * cos_gamma);
        rotation.at<float>(1, 1) = (cos_theta * cos_gamma -
                                   sin_theta * sin_phi * sin_gamma);
        rotation.at<float>(1, 2) = - sin_theta*cos_phi;

        rotation.at<float>(2, 0) = (sin_theta*sin_gamma -
                                   sin_phi * cos_theta * cos_gamma);
        rotation.at<float>(2, 1) = (sin_phi * sin_gamma * cos_theta +
                                   sin_theta * cos_gamma);
        rotation.at<float>(2, 2) = cos_theta*cos_phi;
        rotation.at<float>(0, 3) = rigid_trsfrm_.translation[0].at<float>(0);
        rotation.at<float>(1, 3) = rigid_trsfrm_.translation[0].at<float>(1);
        rotation.at<float>(2, 3) = rigid_trsfrm_.translation[0].at<float>(2);
        cv::Mat multi_chanel_face = face_vertex_.reshape(3);
        cv::transform(multi_chanel_face, multi_chanel_face, rotation);
      }
//#endif
    }
      break;

    case kLandmarks : {
      // Mulitply model time identity weights
      cv::Mat dummy = cv::Mat(number_2D_features_ * 3,
                              number_identity_,
                              CV_32FC1);
      face_model_->ModelTimesWeightVector(BilinearFaceModel::ModelDimensionEnum::kExpr,
                                          expression_weight_,
                                          &feature_vertex_index_,
                                          &dummy);
      // Multiply resulting matrix time expression wieghts

      LTS5::LinearAlgebra<float>::Gemv(dummy,
                                       TransposeType::kNoTranspose,
                                       1.f,
                                       identity_weight_,
                                       0.f,
                                       &landmark_vertex_);
      if (with_rigid_transformation) {
        if (projection_mode_ == ProjectionMode::kFullPerspective) {
          // Reconstruct landmarks with rigid transfo applied on it
          LTS5_LOG_ERROR("ERROR NOT IMPLEMENTED YET");
        } else {
          // Reconstruct landmraks with rotation and translation
          cv::Mat rotation = cv::Mat(3, 4, CV_32FC1);
          cv::hconcat(rigid_trsfrm_.rotation[0],
                      rigid_trsfrm_.translation[0],
                      rotation);

          cv::Mat aTmpLandmarkVertex = landmark_vertex_.reshape(3);      // O(0)
          cv::Mat aRotatedLandmarkVertex;
          cv::transform(aTmpLandmarkVertex, aTmpLandmarkVertex, rotation);
        }
      }
    }
      break;

    case kContour : {
      // Multiply model by weights
      cv::Mat dummy = cv::Mat(contour_candidate_surface_.rows,
                              number_identity_,
                              CV_32FC1);
      face_model_->ModelTimesWeightVector(BilinearFaceModel::ModelDimensionEnum::kExpr,
                                          expression_weight_,
                                          &contour_candidate_,
                                          &dummy);
      LTS5::LinearAlgebra<float>::Gemv(dummy,
                                       TransposeType::kNoTranspose,
                                       1.f,
                                       identity_weight_,
                                       0.f,
                                       &contour_candidate_surface_);
    }
      break;

    default:
      break;
  }
}

#pragma mark -
#pragma mark Accessors

/**
 * @name  set_external_mesh_container
 * @fn    void set_external_mesh_container(LTS5::Mesh<float>* mesh)
 * @bief  Setup the container to directly output synthetic reconstruction in
 *        a given \p mesh.
 * @param[in,out] mesh    Where to put reconstructed shape
 */
void FaceModelFitter::set_external_mesh_container(LTS5::Mesh<float>* mesh) {
  face_vertex_ = cv::Mat(face_vertex_.rows, face_vertex_.cols,
                         face_vertex_.type(),
                         reinterpret_cast<void*>(mesh->get_vertex().data()));
  // Copy mean shape inside
  mesh->set_vertex(mean_shape_mesh_->get_vertex());
}


/*
 *  @name   get_projection_matrix
 *  @brief  Return the projection matrix for a given view
 *  @param  View selection
 *  @return Projection matrix P
 */
const cv::Mat FaceModelFitter::get_projection_matrix(const int view) const {
  cv::Mat proj;
  if (view < number_of_view_ && view >= 0) {
    if (valid_shape_[view]) {
      // Ok in range
      if (projection_mode_ == ProjectionMode::kWeakPerspective) {
        // Weak perspective : Rot + T + s
        cv::Mat K = (cv::Mat::eye(2, 3, CV_32FC1) *
                     rigid_trsfrm_.scale[view]);
        cv::Mat aRevY = cv::Mat::eye(2, 2, CV_32FC1);
        aRevY.at<float>(1, 1) = -1.0f;
        cv::Mat aSR = aRevY*(K * rigid_trsfrm_.rotation[view]);

        // Compute projection matrix
        proj.create(2, 4, CV_32FC1);
        aSR.copyTo(proj(cv::Rect(0, 0, 3, 2)));
        proj.at<float>(0, 3) = rigid_trsfrm_.translation[view].at<float>(0);
        proj.at<float>(1, 3) = (-rigid_trsfrm_.translation[view].at<float>(1));
      } else {
        // Full Projection
        cv::Mat K;
        (camera_configuration_[view]->get_intrinsic_matrix()).copyTo(K);
        cv::Mat extension = camera_configuration_[view]->get_extrinsic_matrix();
        cv::Mat rotation = cv::Mat(4, 4, CV_32FC1, 0.f);

        // Define R|T mat
        rigid_trsfrm_.rotation[0].copyTo(rotation(cv::Rect(0, 0, 3, 3)));
        rigid_trsfrm_.translation[0].copyTo(rotation(cv::Rect(3, 0, 1, 3)));
        rotation.at<float>(3, 3) = 1.f;
        // Compute the projection matrix
        proj = K * extension;
      }
    }
  }
  return proj;
}
}  // namespace LTS
