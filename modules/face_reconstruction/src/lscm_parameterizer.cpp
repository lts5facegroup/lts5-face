/**
 *  @file   lscm_parameterizer.cpp
 *  @brief  The class `LSCMParameterizer` implements the "Least Squares
 *          Conformal Maps (LSCM)" parameterization by :
 *            Bruno Lévy, Sylvain Petitjean, Nicolas Ray, Jérome Maillot
 *          This is a conformal parameterization, i.e. it attempts to preserve
 *          angles.
 *          This is a free border parameterization. No need to map the border
 *          of the surface onto a convex polygon (only two pinned vertices are
 *          needed to ensure a unique solution), but one-to-one mapping
 *          is *not* guaranteed.
 *          Based on OpenNL/CGAL libraries implementation
 *  @author Christophe Ecabert
 *  @date   13/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <limits>

#include "lts5/face_reconstruction/lscm_parameterizer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name LSCMParameterizer
 *  @fn LSCMParameterizer(void)
 *  @brief  Constructor
 */
template<class T>
LSCMParameterizer<T>::LSCMParameterizer(void) : is_initialized_(false),
                                                solver_(nullptr) {
}

/*
 *  @name LSCMParameterizer
 *  @fn explicit LSCMParameterizer(const vtkPolyData& mesh)
 *  @brief  Constructor
 *  @param[in]  mesh  Mesh to parameterize
 */
template<class T>
LSCMParameterizer<T>::LSCMParameterizer(const LTS5::Mesh<T>& mesh) :
  is_initialized_(false),
  solver_(nullptr){
    this->Initialize(mesh);
}

/*
 *  @name Initialize
 *  @fn void Initialize(const vtkPolyData& mesh)
 *  @brief  Initialize LSCMParameterizer with the given mesh
 *  @param[in]  mesh  Mesh to parameterize
 */
template<class T>
void LSCMParameterizer<T>::Initialize(const LTS5::Mesh<T>& mesh) {
  // Set mesh
  if (!is_initialized_) {
    mesh_ = const_cast<Mesh<T>*>(&mesh);
    // Access vertex + add texture coordinate if necessary
    vertex_ptr_ = &(mesh_->get_vertex()[0]);
    // Get number of vertex
    n_vertex_ = static_cast<int>(mesh_->get_vertex().size());
    // Init solver
    if (solver_ != nullptr) {
      delete solver_;
    }
    solver_ = new LTS5::SparseLinearSolver<T>(2 * n_vertex_);
    solver_->set_least_square(true);
    solver_->set_unwrap(true);
    // Init done
    is_initialized_ = true;
  }
}

/*
 *  @name ~LSCMParameterizer
 *  @fn ~LSCMParameterizer(void)
 *  @brief  Destructor
 */
template<class T>
LSCMParameterizer<T>::~LSCMParameterizer(void) {
  if (solver_ != nullptr) {
    delete solver_;
  }
}

#pragma mark -
#pragma mark Process

/*
 *  @name Process
 *  @fn int Process(const vtkPolyData& mesh)
 *  @brief  Compute a one-to-one mapping from a triangular 3D surface mesh
 *          to a piece of the 2D space.
 *          The mapping is linear by pieces (linear in each triangle). The
 *          result is the (u,v) pair image of each vertex of the 3D surface.
 *          Based on OpenNL/CGAL libraries
 *  @param[in] mesh Mesh to process
 *  @return 0 if success, -1 otherwise
 *
 *  @note   Outline of the algorithm:
 *          1) Find an initial solution by projecting on a plane.
 *          2) Lock two vertices of the mesh.
 *          3) Copy the initial u,v coordinates to OpenNL.
 *          4) Solve the equation with OpenNL.
 *          5) Copy OpenNL solution to the u,v coordinates.
 */
template<class T>
int LSCMParameterizer<T>::Process(const LTS5::Mesh<T>* mesh) {
  int error = -1;
  // TODO: (Christophe) Check if correct !!!
  if (mesh_ == nullptr || mesh != mesh_){
    this->Initialize(*mesh);
  }
  // Define texture coordinate
  auto& tcoord = mesh_->get_tex_coord();
  tcoord.resize(n_vertex_);
  tex_ptr_ = &(tcoord[0]);
  // Reset locked vertex indicator
  locked_vertex_index_[0] = -1;
  locked_vertex_index_[1] = -1;

  // Compute (u,v) for (at least two) border vertices
  // and mark them as "parameterized". Save (u,v) into #uv_locked_vertex_ array
  this->ParameterizeMeshBorder();

  // Skip unwrapping (use only projection)
  if(solver_->get_unwrap()) {
    // Initialize the "A*X = B" linear system after
    // (at least two) border vertices parameterization
    this->InitializeSystemFromMeshBorder();

    // Fill the matrix for the other vertices, i.e. Free parameters (u,v).
    solver_->BeginSystem();

    // Iterate over every triangle in the mesh
    this->SetupTriangleRelation();

    // System is constructed
    solver_->EndSystem();

    // Solve the "A*X = B" linear system in the least squares sense
    error = solver_->Solve();

    // Copy X coordinates into the (u,v) pair of each vertex.
    // i.e. Fill vtkPolyData structure with (u,v) coordinates
    this->SetMeshUVFromSystem();
  }

  return error;
}

#pragma mark -
#pragma mark Private

/*
 *  @name ParameterizeMeshBorder
 *  @fn void ParameterizeMeshBorder(void)
 *  @brief  Map two extreme vertices of the 3D mesh and mark them as "locked"
 */
template<class T>
void LSCMParameterizer<T>::ParameterizeMeshBorder(void) {

  //Get mesh bounding box
  T xmin = std::numeric_limits<T>::max();
  T ymin = std::numeric_limits<T>::max();
  T zmin = std::numeric_limits<T>::max();
  T xmax = std::numeric_limits<T>::lowest();
  T ymax = std::numeric_limits<T>::lowest();
  T zmax = std::numeric_limits<T>::lowest();
  for (int v = 0; v < n_vertex_; ++v) {
    const LTS5::Vector3<T>& vert = vertex_ptr_[v];
    xmin = std::min(vert.x_, xmin);
    ymin = std::min(vert.y_, ymin);
    zmin = std::min(vert.z_, zmin);
    xmax = std::max(vert.x_, xmax);
    ymax = std::max(vert.y_, ymax);
    zmax = std::max(vert.z_, zmax);
  }


  // Find longest bound box axes
  const T dx = xmax - xmin;
  const T dy = ymax - ymin;
  const T dz = zmax - zmin;

  LTS5::Vector3<T> V1,      // bounding box' longest axis
  V2 ;                      // bounding box' 2nd longest axis
  T V1_min = 0, V1_max = 0; // bounding box' dimensions along V1
  T V2_min = 0, V2_max = 0; // bounding box' dimensions along V2
  if (dx < dy && dx < dz) {
    // dx is the smallest
    if (dy > dz) {
      // Longest = Y
      V1.x_ = 0.0; V1.y_ = 1.0; V1.z_ = 0.0;
      V1_min = ymin;
      V1_max = ymax;
      // Long = Z
      V2.x_ = 0.0; V2.y_ = 0.0; V2.z_ = 1.0;
      V2_min = zmin;
      V2_max = zmax;
    } else {
      // Longest = Z
      V1.x_ = 0.0; V1.y_ = 0.0; V1.z_ = 1.0;
      V1_min = zmin;
      V1_max = zmax;
      // Long = Y
      V2.x_ = 0.0; V2.y_ = 1.0; V2.z_ = 0.0;
      V2_min = ymin;
      V2_max = ymax;
    }
  } else if (dy < dx && dy < dz) {
    // dy is the smallest
    if (dx > dz) {
      // Longest = X
      V1.x_ = 1.0; V1.y_ = 0.0; V1.z_ = 0.0;
      V1_min = xmin;
      V1_max = xmax;
      // Long = Z
      V2.x_ = 0.0; V2.y_ = 0.0; V2.z_ = 1.0;
      V2_min = zmin;
      V2_max = zmax;
    } else {
      // Longest = Z
      V1.x_ = 0.0; V1.y_ = 0.0; V1.z_ = 1.0;
      V1_min = zmin;
      V1_max = zmax;
      // Long = X
      V2.x_ = 1.0; V2.y_ = 0.0; V2.z_ = 0.0;
      V2_min = xmin;
      V2_max = xmax;
    }
  } else {
    // dz is the smallest
    if (dx > dy) {
      // Longest = X
      V1.x_ = 1.0; V1.y_ = 0.0; V1.z_ = 0.0;
      V1_min = xmin;
      V1_max = xmax;
      // Long = Y
      V2.x_ = 0.0; V2.y_ = 1.0; V2.z_ = 0.0;
      V2_min = ymin;
      V2_max = ymax;
    } else {
      // Longest = Y
      V1.x_ = 0.0; V1.y_ = 1.0; V1.z_ = 0.0;
      V1_min = ymin;
      V1_max = ymax;
      // Long = X
      V2.x_ = 1.0; V2.y_ = 0.0; V2.z_ = 0.0;
      V2_min = xmin;
      V2_max = xmax;
    }
  }
  // Project onto longest bounding box axes,
  // Set extrema vertices' (u,v) in unit square and mark them as "parameterized"
  T umin = std::numeric_limits<T>::max();
  T vmin = std::numeric_limits<T>::max();
  T umax = std::numeric_limits<T>::lowest();
  T vmax = std::numeric_limits<T>::lowest();
  int vxmin = -1;
  int vxmax = -1;
  for (int vert = 0; vert < n_vertex_ ; ++vert) {
    // coordinate along the bounding box' main axes
    T u = vertex_ptr_[vert] * V1;
    T v = vertex_ptr_[vert] * V2;

    // convert to unit square coordinates
    assert(V1_max > V1_min && V2_max > V2_min);
    u = (u - V1_min) / (V1_max - V1_min);
    v = (v - V2_min) / (V2_max - V2_min);

    assert(u >= 0.0 && v >= 0.0);

    // Set (u,v) into mesh
    tex_ptr_[vert].x_ = u;
    tex_ptr_[vert].y_ = v;

    // Save min/max
    if (u < umin || (u == umin && v < vmin)) {
      vxmin = vert;
      umin = u;
      vmin = v;
    }
    if (u > umax || (u == umax && v > vmax)) {
      vxmax = vert;
      umax = u;
      vmax = v;
    }
  }
  // Lock vertex
  locked_vertex_index_[0] = vxmin;
  locked_vertex_index_[1] = vxmax;
}

/*
 *  @name InitializeSystemFromMeshBorder
 *  @fn void InitializeSystemFromMeshBorder(void)
 *  @brief  Initialize Ax = B system after two border vertices are locked
 */
template<class T>
void LSCMParameterizer<T>::InitializeSystemFromMeshBorder(void) {
  int index;
  assert(locked_vertex_index_[0] != -1 && locked_vertex_index_[1] != -1);
  for (int i = 0; i < n_vertex_; ++i) {
    // Get index -> i
    index = 2 * i;
    // Get (u,v) (meaningless if vertex is not parameterized)
    LTS5::Vector2<T>& uv = tex_ptr_[i];

    // Write (u,v) in X (meaningless if vertex is not parameterized)
    // Note  : 2*index     --> u
    //         2*index + 1 --> v
    solver_->variable(index).set_value(uv.x_);
    solver_->variable(index + 1).set_value(uv.y_);

    if (i == locked_vertex_index_[0] || i == locked_vertex_index_[1]) {
      // Lock variable if pinned
      solver_->variable(index).lock();
      solver_->variable(index + 1).lock();
    }
  }
}

/*
 *  @name
 *  @fn void ProjectTriangle(const LTS5::Vector3<T>& p0,
                             const LTS5::Vector3<T>& p1,
                             const LTS5::Vector3<T>& p2,
                             LTS5::Vector2<T>* z0,
                             LTS5::Vector2<T>* z1,
                             LTS5::Vector2<T>* z2)
 *  @brief  Computes the coordinates of the vertices of a triangle in a
 *          local 2D orthonormal basis of the triangle's plane.
 *  @param[in]  p0  First triangle's vertex (3D)
 *  @param[in]  p1  Second triangle's vertex (3D)
 *  @param[in]  p2  Third triangle's vertex (3D)
 *  @param[out] z0  Corresponding 2D location of p0
 *  @param[out] z1  Corresponding 2D location of p1
 *  @param[out] z2  Corresponding 2D location of p2
 */
template<class T>
inline
void LSCMParameterizer<T>::ProjectTriangle(const LTS5::Vector3<T>& p0,
                                           const LTS5::Vector3<T>& p1,
                                           const LTS5::Vector3<T>& p2,
                                           LTS5::Vector2<T>* z0,
                                           LTS5::Vector2<T>* z1,
                                           LTS5::Vector2<T>* z2) {
  // Define orthonormal basis
  LTS5::Vector3<T> X = p1 - p0;
  X.Normalize();
  LTS5::Vector3<T> Z = X ^ (p2 - p0);
  Z.Normalize();
  LTS5::Vector3<T> Y = Z ^ X;

  // Define local coordinate
  const T x0 = 0.0;
  const T y0 = 0.0;   // Origin
  const T x1 = (p1 - p0).Norm();    // Lay on X axis, therefore y1 = 0
  const T y1 = 0.0;
  const T x2 = (p2 - p0) * X;
  const T y2 = (p2 - p0) * Y;

  // Fill outptu
  *z0 = LTS5::Vector2<T>(x0, y0);
  *z1 = LTS5::Vector2<T>(x1, y1);
  *z2 = LTS5::Vector2<T>(x2, y2);
}

/*
 *  @name SetupTriangleRelation
 *  @fn void SetupTriangleRelation(void)
 *  @brief  Create two lines in the linear sstem per triangle (one fur u,
 *          one for v).
 */
template<class T>
void LSCMParameterizer<T>::SetupTriangleRelation(void) {
  // Access triangulation
  const auto& triangle = mesh_->get_triangle();
  int n_tri = static_cast<int>(triangle.size());

  // Vertex index
  int idx0 = -1, idx1 = -1, idx2 = -1;
  LTS5::Vector2<T> z0, z1, z2;  // 2D coordinate in local coordinate system

  // Iterate over all triangle
  const auto& vertex = mesh_->get_vertex();
  for (int t = 0 ; t < n_tri; ++t) {
    // Get triangle's vertex index
    auto& tri = triangle[t];
    idx0 = tri.x_;
    idx1 = tri.y_;
    idx2 = tri.z_;

    // Get vertex position
    const LTS5::Vector3<T>& p0 = vertex[idx0];
    const LTS5::Vector3<T>& p1 = vertex[idx1];
    const LTS5::Vector3<T>& p2 = vertex[idx2];
    // Computes the coordinates of the vertices of a triangle
    // in a local 2D orthonormal basis of the triangle's plane.
    this->ProjectTriangle(p0, p1, p2, &z0, &z1, &z2);
    T a = z1.x_;
    T c = z2.x_;
    T d = z2.y_;
    assert(z1.y_ == 0.0);

    // Create two lines in the linear system per triangle (one for u, one for v)
    // LSCM equation is:
    //       (Z1 - Z0)(U2 - U0) = (Z2 - Z0)(U1 - U0)
    // where Uk = uk + i.v_k is the complex number corresponding to (u,v) coords
    //       Zk = xk + i.yk is the complex number corresponding to local (x,y) coords
    //
    // Note  : 2*index     --> u
    //         2*index + 1 --> v
    int u0_idx = 2 * idx0;
    int v0_idx = u0_idx + 1;
    int u1_idx = 2 * idx1;
    int v1_idx = u1_idx + 1;
    int u2_idx = 2 * idx2;
    int v2_idx = u2_idx + 1;

    // Real part
    // Note: b = 0
    solver_->BeginRow();
    solver_->AddCoefficient(u0_idx, -a + c);
    solver_->AddCoefficient(v0_idx, -d);
    solver_->AddCoefficient(u1_idx, -c);
    solver_->AddCoefficient(v1_idx, d);
    solver_->AddCoefficient(u2_idx, a);
    solver_->EndRow();
    // Imaginary part
    // Note: b = 0
    solver_->BeginRow();
    solver_->AddCoefficient(u0_idx, d);
    solver_->AddCoefficient(v0_idx, -a + c);
    solver_->AddCoefficient(u1_idx, -d);
    solver_->AddCoefficient(v1_idx, -c);
    solver_->AddCoefficient(v2_idx, a);
    solver_->EndRow();
  }
}

/*
 *  @name SetMeshUVFromSystem
 *  @fn void SetMeshUVFromSystem(void)
 *  @brief  Copy X coordinates int the (u,v) pair of each vertex. i.e. Fill
 *          vtkPolyData structure with proper (u,v) pairs.
 */
template<class T>
void LSCMParameterizer<T>::SetMeshUVFromSystem(void) {
  // Loop over all vertices
  T u_pos = -1.0, v_pos = -1.0;
  T u_min = std::numeric_limits<T>::max();
  T u_max = std::numeric_limits<T>::lowest();
  T v_min = std::numeric_limits<T>::max();
  T v_max = std::numeric_limits<T>::lowest();
  for (int v = 0; v < n_vertex_; ++v) {
    // Get system solution
    u_pos = solver_->variable(2 * v).value();
    v_pos = solver_->variable(2 * v + 1).value();
    // Set back into mesh
    LTS5::Vector2<T>& uv = tex_ptr_[v];
    uv.x_ = u_pos;
    uv.y_ = v_pos;
    // Find min/max for recentering
    u_min = std::min(u_pos, u_min);
    u_max = std::max(u_pos, u_max);
    v_min = std::min(v_pos, v_min);
    v_max = std::max(v_pos, v_max);
  }
  // Center and scale in order to be in the correct texture space [0,1]
  for (int v = 0; v < n_vertex_; ++v) {
    LTS5::Vector2<T>& uv = tex_ptr_[v];
    uv.x_ = (uv.x_ - u_min) / (u_max - u_min);
    uv.y_ = (uv.y_ - v_min) / (v_max - v_min);
  }

  // Reset selection
  locked_vertex_index_[0] = -1;
  locked_vertex_index_[1] = -1;
}

#pragma mark -
#pragma mark Explicit declaration

template class LTS5_EXPORTS LSCMParameterizer<float>;
template class LTS5_EXPORTS LSCMParameterizer<double>;

}  // namespace LTS5
