/**
 *  @file   external_video_stream.cpp
 *  @brief  External Stream class to handle data importation via
 *          callbacks interface
 *
 *  @author Christophe Ecabert
 *  @date   22/07/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#include <thread>

#ifdef HAS_CUDA_
#include "opencv2/cudaimgproc.hpp"
#else
#include "opencv2/imgproc.hpp"
#endif

#include "lts5/face_reconstruction/external_video_stream.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name   ExternalVideoStream
 *  @fn ExternalVideoStream(const Parameter& param)
 *  @brief  Constructor
 *  @param[in]  param Stream parameters
 */
ExternalVideoStream::ExternalVideoStream(const Parameter& param) : BaseStream::BaseStream(param),
                                                                   n_frame_loaded_(0) {
}

/*
 * @name  ~ExternalVideoStream
 * @fn  ~ExternalVideoStream(void)
 * @brief Destructor
 */
ExternalVideoStream::~ExternalVideoStream(void) {
}

/*
 * @name  InitCallback
 * @fn  void InitCallback(ICallbacks* callback)
 * @brief Define which callbacks use to load data
 * @param[in] callback  Callback to use by the stream
 */
void ExternalVideoStream::InitCallback(ICallbacks* callback) {
  callback_ = callback;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Read
 *  @fn int Read(void)
 *  @brief  Read image from the stream
 *  @return -1 if error, 0 otherwise
 */
int ExternalVideoStream::Read(void) {
  int err = callback_->Read(&(this->buffer_.host));
  if (!err) {
    if (this->buffer_.host[0].channels() != 1) {
      for (size_t i = 0; i < this->buffer_.host.size(); ++i) {
      #ifdef HAS_CUDA_
        cv::cuda::GpuMat buffer;
        buffer.upload(this->buffer_.host[i]);
        cv::cuda::cvtColor(buffer, this->buffer_.device[i], cv::COLOR_BGR2GRAY);
        buffer_.device[i].download(buffer_.host[i]);
      #else
        cv::cvtColor(this->buffer_.host[i],
                     this->buffer_.host[i],
                     cv::COLOR_BGR2GRAY);
      #endif
      }
  }
    // Push to device if needed
#ifdef HAS_CUDA_
    buffer_.Upload();
#endif
    n_frame_loaded_++;
  }
  return err;
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name get_frame_name
 *  @fn void get_frame_name(std::string* frame_name, std::string* frame_ext) const
 *  @brief  Provide the name of the current frame according to the media
 *  @param[out] frame_name  Frame name
 *  @param[out] frame_ext   Frame extension
 */
void ExternalVideoStream::get_frame_name(std::string* frame_name,
                                 std::string* frame_ext) const {
  std::string num = std::to_string(n_frame_loaded_ - 1);
  std::stringstream sstream;
  sstream << std::this_thread::get_id();
  *frame_name = sstream.str() + "_frame_" + std::string(6 - num.length(), '0') + num;
  *frame_ext = ".jpg";
}
}  // namepsace LTS5


