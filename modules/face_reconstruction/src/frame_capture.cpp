/**
 *  @file   frame_capture.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 04/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/utils/process_error.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/face_reconstruction/frame_capture.hpp"
#include "lts5/face_reconstruction/utilities.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5
{
/*
 *  @name   FrameCapture
 *  @brief  Base constructor
 *  @param  configuration     Structure that's hold all configuration information.
 */
FrameCapture::FrameCapture(const FrameCaptureParameter& configuration) {
  // Define common information such as #Views per frame, frame size, input types ...
  input_type_ = configuration.input_type;
  number_of_view_ = configuration.number_of_view;
  frame_width_ = configuration.width;
  frame_height_ = configuration.height;
  number_of_frame_loaded_ = 0;

  // Call the helper function
  switch (input_type_) {
    case kPointGrey:   FrameCaptureCamera(configuration);
      break;

    case kImage :   FrameCaptureImage(configuration);
      break;

    case kVideo :   FrameCaptureVideo(configuration);
      break;
  }

  /*
   *  Inialize the view buffer and images
   *  Call after helper function since dimension are not known previously
   */
  for ( int i = 0; i < number_of_view_; i++) {
    image_buffer_.push_back(cv::Mat(frame_height_,frame_width_,CV_8UC1));
  }
}

/*
 *  @name   ~FrameCapture
 *  @brief  Destructor
 */
FrameCapture::~FrameCapture() {
  // Release memory
  if (!capture_device_list_.empty())
  {
    for (int i = 0; i < capture_device_list_.size(); i++) {
      capture_device_list_[i].release();
    }
  }
}

/*
 *  @name   FrameCaptureCamera
 *  @brief  Constructor helper for camera input
 */
void FrameCapture::FrameCaptureCamera(const FrameCaptureParameter& configuration) {
  // Goes through all VideoCapture and start it
  capture_device_list_.clear();

  for (int i = 0; i < number_of_view_; i++) {
    // Open video capture
    capture_device_list_.push_back(cv::VideoCapture(configuration.device_index[i]));
    if (capture_device_list_[i].isOpened()) {
      // Set properties
      capture_device_list_[i].set(cv::CAP_PROP_FRAME_WIDTH , frame_width_);
      capture_device_list_[i].set(cv::CAP_PROP_FRAME_HEIGHT, frame_height_);

      if (i == (number_of_view_ -1)) {
        initialized_ = true;
        number_max_frame_ = -1;
      }
    } else {
      LTS5_LOG_ERROR("Unable to initialize camera : " << i);
      initialized_ = false;
      break;
    }
  }
}

/*
 *  @name   FrameCaptureImage
 *  @brief  Constructor helper for images input
 */
void FrameCapture::FrameCaptureImage(const FrameCaptureParameter& configuration) {
  // Scan folder where video are stored
  int image_per_folder = -1;

  for (int k = 0; k < configuration.number_of_view; k++) {
    // Scan first folder
    data_root_folder_ = configuration.input_folder;
    LTS5::GetFileListing(file_list_,
                         configuration.input_folder,
                         configuration.image_folder[k],
                         configuration.extension_type);

    // Get amount of images in folder
    image_per_folder = (int)file_list_.size() / (k+1);

    // Sort by number only the part relative to the scanned folder
    std::sort(file_list_.begin().base() + k * image_per_folder,
              file_list_.end().base() ,
              Utilities::SortByNumber);
  }

  // Check if there enough images
  if (((int)file_list_.size() % number_of_view_) == 0) {
    // Number of images is a multiple of _numberViews
    number_max_frame_ = (int)file_list_.size()/number_of_view_;
    initialized_ = true;
  } else {
    initialized_ = false;
    throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErr,
                             "Number of images dosen't match for each views !",
                             FUNC_NAME);
  }
}

/*
 *  @name   FrameCaptureVideo
 *  @brief  Constructor helper for videos input
 */
void FrameCapture::FrameCaptureVideo(const FrameCaptureParameter& configuration) {
  // Scan folder where video are stored
  for (int i = 0; i < configuration.number_of_view; ++i) {
    LTS5::GetFileListing(file_list_,
                              configuration.input_folder,
                              configuration.image_folder[i],
                              configuration.extension_type);
  }
  // Something detected ?
  if (file_list_.empty()) {
    // Something went wrong
    throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErr,
                             "Unable to locate file to process",
                             FUNC_NAME);
  }

  // Sort
  std::sort(file_list_.begin(), file_list_.end());
  // Remove duplicate
  file_list_.erase(std::unique(file_list_.begin(),
                               file_list_.end()),
                   file_list_.end());

  // Check if there enough video
  if (((int)file_list_.size() % number_of_view_) == 0) {
    // Number of video is a multiple of _numberViews
    capture_device_list_.clear();
    std::vector<int> video_length = std::vector<int>(number_of_view_,0);

    int width = 0, height = 0;
    for (int i = 0; i < number_of_view_ ; i++) {
      // Open video
      capture_device_list_.push_back(cv::VideoCapture(file_list_[i]));
      if (capture_device_list_[i].isOpened()) {
        // Get properties
        video_length[i] = capture_device_list_[i].get(cv::CAP_PROP_FRAME_COUNT);
        width = capture_device_list_[i].get(cv::CAP_PROP_FRAME_WIDTH);
        height = capture_device_list_[i].get(cv::CAP_PROP_FRAME_HEIGHT);
        if (i != 0) {
          // Check if size are the same
          if ((height != frame_height_) && (width != frame_width_)) {
            // Video don't have the same dimension
            LTS5_LOG_WARNING("Error at opening, videos don't have the same resolution");
            initialized_ = false;
            break;
          }
        } else {
          frame_width_ = width;
          frame_height_ = height;
        }

        if (i == (number_of_view_-1)) {
          // Look for the smallest length
          number_max_frame_ = *(std::min_element(video_length.begin(), video_length.end()));
          initialized_ = true;
        }
      }
    }
  } else {
    initialized_ = false;
    throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErr,
                             "Number of videos doesn't match for each views !",
                             FUNC_NAME);
  }
}

/*
 *  @name   CaptureFrame
 *  @brief  Capture nViews for current frame and push results into views buffers
 */
void FrameCapture::CaptureFrame(void) {
  // Capture one frame from multiple view
  if (initialized_) {
    if (number_of_frame_loaded_ < number_max_frame_ || number_max_frame_ == -1) {
      // Can capture frame
      // Select the correct method of acquisition
      if (input_type_ == kImage) {
        for (int i = 0; i < number_of_view_; i++) {
          // From left to right
          std::string name = file_list_[number_max_frame_ * i + number_of_frame_loaded_];
          LTS5_LOG_DEBUG("Load image : " << name);
          cv::Mat input_image = cv::imread(name,cv::IMREAD_UNCHANGED);
          if (input_image.channels() != 1) {
            cv::cvtColor(input_image, image_buffer_[i], cv::COLOR_BGR2GRAY);
          } else {
            image_buffer_[i] = input_image.clone();
          }
        }
      } else {
        // Use VideoCapture in two separate loop for keeping as
        // possible frame synched !
        // See : http:// docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html#videocapture-grab
        for (int i = 0; i < number_of_view_; i++) {
          // First grab frame
          capture_device_list_[i].grab();
        }
        // Call slower methods
        cv::Mat input_image;
        for (int i = 0; i < number_of_view_; i++) {
          // Retrieve images
          if (input_type_ == kPointGrey) {
            capture_device_list_[i].retrieve(input_image);
          } else {
            capture_device_list_[i].read(input_image);
          }
          if (input_image.channels() != 1) {
            cv::cvtColor(input_image, image_buffer_[i], cv::COLOR_BGR2GRAY);
          } else {
            image_buffer_[i] = input_image.clone();
          }
        }
      }
      // Increase frame counter
      number_of_frame_loaded_++;
    }
  }
}

/*
 *  @name   ClearFrameCounter
 *  @brief  Reset loaded frame counter
 */
void FrameCapture::ClearFrameCounter(void) {
  number_of_frame_loaded_ = 0;
}

/*
 *  @name get_frame_name
 *  @fn void get_frame_name(std::string* frame_name, std::string* frame_ext) const
 *  @brief  Provide the name of the current frame according to the media
 *  @param[out] frame_name  Frame name
 *  @param[out] frame_ext   Frame extension
 */
void FrameCapture::get_frame_name(std::string* frame_name,
                                  std::string* frame_ext) const {
  // Base on input type
  if (input_type_ == kImage) {
    // Image
    std::string img_name = file_list_[number_of_frame_loaded_ - 1];
    size_t pos = img_name.rfind(".");
    if (pos != std::string::npos) {
      *frame_ext = img_name.substr(pos, img_name.length() - pos);
      *frame_name = img_name.substr(data_root_folder_.length(), pos - data_root_folder_.length());
    } else {
      *frame_name = img_name;
      *frame_ext = ".jpg";
    }
  } else {
    // Video/Webcam
    std::string fname = "Frame_";
    std::string num = std::to_string(number_of_frame_loaded_ - 1);
    *frame_name = std::string(6 - num.length(), '0') + num;
    *frame_ext = ".jpg";
  }
}

}

