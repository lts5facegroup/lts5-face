/**
 *  @file   mm_fitter.cpp
 *  @brief  3DMM fitter object
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   21/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/utils/process_error.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/face_reconstruction/mm_fitter.hpp"
#include "lts5/face_reconstruction/fit_algorithm_factory.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  MorphableModelFitter
 * @fn    MorphableModelFitter(const std::string& fit_algo,
                               const MorphableModel<T>* model)
 * @brief Constructor
 * @param[in] fit_algo    Fit algorithm's name
 * @param[in] model       Morphable model to use
 */
template<typename T>
MorphableModelFitter<T>::MorphableModelFitter(const std::string& fit_algo,
                                              const MorphableModel<T>* model) :
        algo_(nullptr),
        model_(model) {
  algo_ = MMFitAlgorithmFactory<T>::Get().CreateByName(fit_algo, model);
  if (algo_ == nullptr) {
    throw ProcessError(ProcessError::kErr,
                       "Unable to initialize fit algorithm from factory",
                       FUNC_NAME);
  }
}

/*
 * @name  ~MorphableModelFitter
 * @fn    ~MorphableModelFitter(void)
 * @brief Destructor
 */
template<typename T>
MorphableModelFitter<T>::~MorphableModelFitter(void) {
  if (algo_ != nullptr) {
    delete algo_;
    algo_ = nullptr;
  }
}

#pragma mark -
#pragma mark Usage

/*
 * @name  FitWithBoundingBox
 * @fn    int FitWithBoundingBox(const cv::Mat& image,
                                 const cv::Rect& bbox,
                                 Camera_t* camera,
                                 Mesh* mesh,
                                 Stat* stats)
 * @brief Fit 3D Model from a given image and bounding box.
 * @param[in] image
 * @param[in] bbox
 * @param[out] camera
 * @param[out] mesh
 * @param[out] stats
 * @return    -1 if not converged, 0 otherwise
 */
template<typename T>
int MorphableModelFitter<T>::FitWithBoundingBox(const cv::Mat& image,
                                                const cv::Rect& bbox,
                                                Camera_t* camera,
                                                Mesh* mesh,
                                                Stat* stats) {
  // Place meanshape within bbox
  cv::Mat lms, patch, plms;
  this->PlaceMeanLandmarks(camera, bbox, &lms);
  // Convert input
  this->ConvertInput(image, lms, T(400.0), false, &patch, &plms);
  // Initialize camera from landmarks
  camera->From3Dto2D(this->model_->get_mean_landmarks(), plms, T(5.0));
  // Fit, landmarks only used to init camera, therefore shut down optimization
  // on landmarks
  Regularization reg(reg_);
  reg.landmark = T(0.0);
  return this->Fit(patch, cv::Mat(), reg, camera, mesh, stats);
}

/*
 * @name  FitWithLandmarks
 * @fn    int FitWithLandmarks(const cv::Mat& image,
                               const cv::Mat& landmarks,
                               Camera_t* camera,
                               Mesh* mesh,
                               Stat* stats)
 * @brief Fit 3D Model from a given image and set of landmarks.
 * @param[in] image       Input image
 * @param[in] landmarks   Set of landmarks
 * @param[out] camera     Estimated camera transform
 * @param[out] mesh       Reconstructed mesh
 * @param[out] stats      Optimization statistics, can be nullptr if not used
 * @return    -1 if not converged, 0 otherwise
 */
template<typename T>
int MorphableModelFitter<T>::FitWithLandmarks(const cv::Mat& image,
                                              const cv::Mat& landmarks,
                                              Camera_t* camera,
                                              Mesh* mesh,
                                              Stat* stats) {

  // Convert inputs
  cv::Mat patch, lms;
  this->ConvertInput(image, landmarks, T(400.0), true, &patch, &lms);
  // Initialize camera from landmarks
  camera->set_principal_point_x(patch.cols / T(2.0));
  camera->set_principal_point_y(patch.rows / T(2.0));
  camera->set_focal_length(static_cast<T>(std::max(patch.cols, patch.rows)));
  camera->From3Dto2D(this->model_->get_mean_landmarks(), lms, T(10.0));




  const cv::Mat& pts3 = this->model_->get_mean_landmarks();
  cv::Mat pts2;
  //(*camera)(pts3, &pts2);
  camera->operator()(pts3, &pts2);
  int n_pts = std::max(lms.rows, lms.cols) / 2;
  cv::Mat canvas = patch.clone();
  for (int i = 0; i < n_pts; ++i) {
    cv::circle(canvas,
               cv::Point(pts2.at<T>(i * 2),
                         pts2.at<T>(i * 2 + 1)),
               2,
               CV_RGB(0, 255, 0), -1);
    cv::circle(canvas,
               cv::Point(lms.at<T>(i * 2),
                         lms.at<T>(i * 2 + 1)),
               2,
               CV_RGB(0, 0, 255), -1);
  }
  cv::imshow("proj", canvas);
  cv::waitKey(0);



  // Start new fit
  return this->Fit(patch, lms, reg_, camera, mesh, stats);
}

#pragma mark -
#pragma mark Private

/*
 * @name  ConvertInput
 * @fn    void ConvertInput(const cv::Mat& image, const cv::Mat& landmarks,
                            const T& diag, const bool& lms_stacked,
                            cv::Mat* patch, cv::Mat* lms)
 * @brief Convert inputs (image + landmarks) into proper format for fitting
 * @param[in] image       Image to normalize
 * @param[in] landmarks   Landmarks from tracker
 * @param[in] diag        Desired main diagonal dimension
 * @param[in] lms_stacked True if lms are in [x,x,x,.. ,y,y,y,...] format,
 *                        False otherwise
 * @param[out] patch      Converted image
 * @param[out] lms        Converted landmarks
 */
template<typename T>
void MorphableModelFitter<T>::ConvertInput(const cv::Mat& image,
                                           const cv::Mat& landmarks,
                                           const T& diag,
                                           const bool& lms_stacked,
                                           cv::Mat* patch,
                                           cv::Mat* lms) {
  // Reformat landmarks input is: [x0, x1, ... y0, y1, ...] but need to be
  // in [x0, y0, x1, y1, ...]
  auto n_lms = std::max(landmarks.cols, landmarks.rows) / 2;
  auto n_sel = static_cast<int>(this->model_->get_landmark_index().size());

  lms->create(2 * n_sel, 1, cv::DataType<T>::type);
  T min_x = std::numeric_limits<T>::max();
  T min_y = std::numeric_limits<T>::max();
  T max_x = T(0.0);
  T max_y = T(0.0);
  for (int i = 0; i < n_lms; ++i) {
    T x = lms_stacked ?
          static_cast<T>(landmarks.at<double>(i)) :
          static_cast<T>(landmarks.at<double>(2*i));
    T y = lms_stacked ?
          static_cast<T>(landmarks.at<double>(i + n_lms)) :
          static_cast<T>(landmarks.at<double>(2 * i + 1));
    lms->at<T>(2 * i) = x;
    lms->at<T>((2 * i) + 1) = y;
    // Find bounding box
    min_x = x < min_x ? x : min_x;
    min_y = y < min_y ? y : min_y;
    max_x = x > max_x ? x : max_x;
    max_y = y > max_y ? y : max_y;
  }

  // Normalize image, map smallest side to a fixed value while preserving
  // aspect ratio.
  // Define bounding box
  cv::Rect bbox;
  auto w = static_cast<int>(max_x - min_x);
  auto h = static_cast<int>(max_y - min_y);
  bbox.width = static_cast<int>(w * T(1.4));
  bbox.height = static_cast<int>(h * T(1.8));
  bbox.x = min_x - ((bbox.width - w) / 2);
  bbox.y = min_y - ((bbox.height - h) / 1.5);
  // Scaling factor
  T lms_diag = static_cast<T>(std::sqrt((bbox.width * bbox.width) +
                                        (bbox.height * bbox.height)));
  T scale = diag / lms_diag;

  // Convert landmarks
  cv::Rect_<T> bbox_t = bbox;
  for (int i = 0; i < n_lms; ++i) {
    const T x = lms->at<T>(2 * i);
    const T y = lms->at<T>(2 * i + 1);
    lms->at<T>(2 * i) = (x - bbox_t.x) * scale;
    lms->at<T>(2 * i + 1) = (y - bbox_t.y) * scale;
  }
  // Convert patch
  cv::resize(image(bbox), *patch, cv::Size(), scale, scale);
}

/*
 * @name  Fit
 * @fn    int Fit(const cv::Mat& image,
                  const cv::Mat& landmarks,
                  const Regularization& reg,
                  Camera_t* camera,
                  Mesh* mesh,
                  Stat* stats)
 * @brief Fit a 3DMM tp a given image and set of landmarks
 * @param[in] image       Image to fit model on
 * @param[in] landmarks   Set of landmarks (can be left empty)
 * @param[in] reg         Regularization parameters
 * @param[in] camera      Estimated camera transform
 * @param[in] mesh        Reconstructed mesh. Can hold initial guess but not
 *                        mandatory
 * @param[in] stats       Fitting statistics, can be nullptr if not used
 * @return    -1 if not converged, 0 otherwise
 */
template<typename T>
int MorphableModelFitter<T>::Fit(const cv::Mat& image,
                                 const cv::Mat& landmarks,
                                 const Regularization& reg,
                                 Camera_t* camera,
                                 Mesh* mesh,
                                 Stat* stats) {
  // Initial mesh ?
  if (mesh->get_vertex().empty()) {
    const int ns = this->model_->get_active_shape_component();
    const int nt = this->model_->get_active_texture_component();
    cv::Mat sp(ns, 1, cv::DataType<T>::type, cv::Scalar_<T>(0.0));
    cv::Mat tp(nt, 1, cv::DataType<T>::type, cv::Scalar_<T>(0.0));
    this->model_->Generate(sp, tp, false, mesh);
  }
  // Fit
  return algo_->Process(image,
                        landmarks,
                        cv::Mat(),
                        cv::Mat(),
                        cv::Mat(),
                        reg,
                        mesh,
                        camera,
                        stats);
}

/*
 * @name  PlaceMeanLandmarks
 * @fn    void PlaceMeanLandmarks(const Camera_t* camera,
                                  const cv::Rect& bbox,
                                  cv::Mat* landmarks) const
 * @brief Place meanshape within a given bbox
 * @param[in] camera  Camera object
 * @param[in] bbox    Bounding box where to place mean shape
 * @param[out] landmarks   Mean landmarks place within bbox
 */
template<typename T>
void MorphableModelFitter<T>::PlaceMeanLandmarks(const Camera_t* camera,
                                                 const cv::Rect& bbox,
                                                 cv::Mat* landmarks) const {
  // Access elements
  const cv::Mat& mean_lms = this->model_->get_normalized_mean_landmarks();
  const int n = std::max(mean_lms.rows, mean_lms.cols) / 3;
  // Compute mean landmark bbox
  landmarks->create(2 * n, 1, mean_lms.type());
  const T w = static_cast<T>(bbox.width);
  const T h = static_cast<T>(bbox.height);
  const T x = static_cast<T>(bbox.x);
  const T y = static_cast<T>(bbox.y);
  const T* ax = camera->get_axis_inversion();
  // Apply transform
  for (int i = 0; i < n; ++i) {
    const int in = i * 3;
    const int out = i * 2;
    T lx = mean_lms.at<T>(in);
    T ly = mean_lms.at<T>(in + 1);
    lx = ax[0] < T(0.0) ? -lx + T(1.0) : lx;
    ly = ax[1] < T(0.0) ? -ly + T(1.0) : ly;
    landmarks->at<T>(out) = lx * w + x;
    landmarks->at<T>(out + 1) = ly * h + y;
  }
}


#pragma mark -
#pragma mark Explicit Instanciation

/** Float - MorphableModelFitter */
template class MorphableModelFitter<float>;
/** Double - MorphableModelFitter */
template class MorphableModelFitter<double>;

}  // namepsace LTS5
