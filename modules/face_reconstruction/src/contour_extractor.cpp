/**
 *  @file   contour_extractor.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 03/02/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <fstream>
#include <assert.h>
#include <algorithm>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/logger.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/face_reconstruction/contour_extractor.hpp"
#include "lts5/face_reconstruction/utilities.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5
{
  #pragma mark -
  #pragma mark Constructor/Destructor/Init

  /*
   *  @name   ContourExtractor
   *  @brief  Constructor
   *  @param[in]  config_filename     Binary file where configuration is stored
   */
  ContourExtractor::ContourExtractor(const std::string& config_filename) {
    // Open stream to binary file
    std::ifstream bin_stream(config_filename.c_str(),
                             std::ios::in|std::ios::binary);
    if (bin_stream.is_open()) {
      // Ok, get number of lines on contour
      bin_stream.read((char*)&nb_contour_line_, sizeof(int));
      // Instantiate array + read data
      start_line_idx_ = std::vector<int>(nb_contour_line_,0);
      bin_stream.read((char*)start_line_idx_.data(),
                      nb_contour_line_*sizeof(int));

      // Get indexes for vertices on lines
      bin_stream.read((char*)&nb_total_contour_vertex_, sizeof(int));
      line_vertex_idx_ = std::vector<int>(nb_total_contour_vertex_,0);
      bin_stream.read((char*)line_vertex_idx_.data(),
                      nb_total_contour_vertex_*sizeof(int));

      // Get connection starting index
      bin_stream.read((char*)&nb_total_contour_vertex_, sizeof(int));
      start_connection_idx_ = std::vector<int>(nb_total_contour_vertex_,0);
      bin_stream.read((char*)start_connection_idx_.data(),
                      nb_total_contour_vertex_*sizeof(int));

      // Read connection index
      bin_stream.read((char*)&nb_onnected_vertex_, sizeof(int));
      connection_vertex_idx_ = std::vector<int>(nb_onnected_vertex_);
      bin_stream.read((char*)connection_vertex_idx_.data(),
                      nb_onnected_vertex_*sizeof(int));

      // Read candidate index
      bin_stream.read((char*)&nb_candidate_vertex_, sizeof(int));
      candidate_vertex_idx_ = std::vector<int>(nb_candidate_vertex_);
      bin_stream.read((char*)candidate_vertex_idx_.data(),
                      nb_candidate_vertex_*sizeof(int));

      // Read index mapping
      int map_length = 0;
      bin_stream.read((char*)&map_length, sizeof(int));
      if (map_length != nb_candidate_vertex_) {
        // Close stream
        bin_stream.close();
        // Throw exception
        throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErrReadingData,
                                 "Map index is not consistant with the number of contour candidates",
                                 FUNC_NAME);
      }

      // Load key/value pair
      int key,value;
      index_map_.reserve(map_length);
      for (int p = 0; p < map_length; ++p) {
        // Load key
        bin_stream.read((char*)&key, sizeof(int));
        bin_stream.read((char*)&value, sizeof(int));
        index_map_.insert(std::make_pair(key, value));
      }

      // Load constraints
      float lower_bound = 1.f;
      float upper_bound = 1.f;
      int selection_length = -1;
      int* selection_ptr = nullptr;
      while (!bin_stream.eof()) {
        // Read constraint type
        char constraint_type;
        bin_stream.read((char*)&constraint_type, sizeof(char));
        // Read bounds
        bin_stream.read((char*)&lower_bound, sizeof(float));
        bin_stream.read((char*)&upper_bound, sizeof(float));
        // Read size
        bin_stream.read((char*)&selection_length, sizeof(int));
        // Read selection
        switch (constraint_type) {
          case 'T':   theta_features_.push_back(std::vector<int>(selection_length));
            selection_ptr = theta_features_[theta_features_.size() - 1].data();
            theta_bounds_.push_back(std::make_pair(lower_bound, upper_bound));
            break;

          case 'P':   phi_features_.push_back(std::vector<int>(selection_length));
            selection_ptr = phi_features_[phi_features_.size() - 1].data();
            phi_bounds_.push_back(std::make_pair(lower_bound, upper_bound));
            break;

          default:
            break;
        }

        bin_stream.read((char*)selection_ptr, selection_length*sizeof(int));
      }

      // Done close file
      bin_stream.close();

      // Normal
      // Normal - DBG
      mesh_normal_DBG_ = cv::Mat::zeros(6, nb_total_contour_vertex_, CV_32FC1);

      // Init multiview variables for single view
      number_views_ = -1;
      this->InitMultiView(1);
    } else {
      // Error file not open
      throw ProcessError(ProcessError::ProcessErrorEnum::kErrOpeningFile,"Unable to open contour configuration file", "ContourExtractor::ContourExtractor()");
    }
  }

  /*
   *  @name   ~ContourExtractor
   *  @brief  Destructor
   */
  ContourExtractor::~ContourExtractor() {
    // Do some clean up
    if (!start_line_idx_.empty()) {
      start_line_idx_.clear();
    }
    if (!line_vertex_idx_.empty()) {
      line_vertex_idx_.clear();
    }
    if (!start_connection_idx_.empty()) {
      start_connection_idx_.clear();
    }
    if (!connection_vertex_idx_.empty()) {
      connection_vertex_idx_.clear();
    }
    if (!contour_3d_.empty()) {
      ContourIndexIt contour_it;
      ContourIndexIt matched_contour_it = matched_contour_3d_.begin();
      ContourIndexIt matched_global_contour_it = matched_global_idx_contour_3d_.begin();
      ContourValueIt aMatechedContourImgPosIt = matched_contour_img_position_.begin();

      for (contour_it = contour_3d_.begin(); contour_it != contour_3d_.end(); ++contour_it,++matched_contour_it,++aMatechedContourImgPosIt,++matched_global_contour_it) {
        contour_it->clear();
        matched_contour_it->clear();
        aMatechedContourImgPosIt->clear();
        matched_global_contour_it->clear();
      }
      // Clear outter vector
      contour_3d_.clear();
      matched_contour_3d_.clear();
      matched_contour_img_position_.clear();
      matched_global_idx_contour_3d_.clear();
    }
  }

  /*
   *  @name   InitMultiView
   *  @brief  Initialize the extractor for a given number of view
   *  @param[in]  number_view         Number of views
   */
  void ContourExtractor::InitMultiView(const int number_view) {
    // If already the correct size do nothing
    if (number_view != number_views_) {
      number_views_ = number_view;
      if (!contour_normal_.empty()) {
        contour_normal_.clear();
      }

      // Clear inner vector
      ContourIndexIt contour_it;
      ContourIndexIt matched_contour_it = matched_contour_3d_.begin();
      ContourIndexIt matched_global_contour_it = matched_global_idx_contour_3d_.begin();
      ContourValueIt aMatechedContourImgPosIt = matched_contour_img_position_.begin();
      for (contour_it = contour_3d_.begin();
           contour_it != contour_3d_.end();
           ++contour_it,
           ++matched_contour_it,
           ++aMatechedContourImgPosIt,
           ++matched_global_contour_it) {
        contour_it->clear();
        matched_contour_it->clear();
        aMatechedContourImgPosIt->clear();
        matched_global_contour_it->clear();
      }
      // Clear outter vector
      contour_3d_.clear();
      matched_contour_3d_.clear();
      matched_contour_img_position_.clear();
      matched_global_idx_contour_3d_.clear();

      // Goes through each views
      for (int i = 0; i < number_views_; i++) {
        // Vertex's index for contour points
        contour_3d_.push_back(std::vector<int>(nb_contour_line_,0));
        // Normal on contour(s)
        contour_normal_.push_back(cv::Mat::zeros(3 * nb_contour_line_,
                                                 1,
                                                 CV_32FC1));
        // Match vertex indec
        matched_contour_3d_.push_back(std::vector<int>(nb_contour_line_, 0));
        // Match positiion
        matched_contour_img_position_.push_back(std::vector<float>(2 * nb_contour_line_,
                                                                   0.f));
        // Global index
        matched_global_idx_contour_3d_.push_back(std::vector<int>(nb_contour_line_,
                                                                  0));
      }
    }
  }

#pragma mark -
#pragma mark Process

  /*
   *  @name   ExtractContour3D
   *  @brief  Compute the index of the vertices on the chin contour
   *  @param  aCameraFacingDirection      Optical axis (Camera Z axis) for each views
   *  @param[in]  camera_facing_direction      Optical axis (Camera Z axis) for each views
   */
  void ContourExtractor::ExtractContour3D(const std::vector<cv::Mat>& camera_facing_direction) {
    // Goes through each line
    int count = 0;
    for (int l = 0; l < nb_contour_line_; l++) {

      ContourMatrixConstIt cam_direction_it;
      // Compute line index
      int index_line_start = start_line_idx_[l];
      int index_line_end = l+1 == nb_contour_line_ ? nb_total_contour_vertex_ :
      start_line_idx_[l+1];
      int line_size = index_line_end - index_line_start;

      // Goes through each vertex on the line
      float weighted_surface_normal[3] = {0.f,0.f,0.f};
      float edge_ab[3];
      float edge_ac[3];
      float cross_prod_ab_ac[3];

      std::vector<float> min_dot_prod = std::vector<float>(camera_facing_direction.size(),
                                                           FLT_MAX);
      std::vector<float>::iterator min_dot_prod_it;
      ContourIndexIt contour_it;
      std::vector<cv::Mat>::iterator contour_normal_it;
      for(int v = 0 ; v < line_size ; ++v) {
        // Take current pts
        int vertex_index = index_line_start + v;
        float* point_a_ptr = &(((float*)mesh_.data)[line_vertex_idx_[vertex_index]*3]);

        // Connected points
        int connection_start_index = start_connection_idx_[vertex_index];
        int connection_end_index = (((v+1 == line_size) &&
                                     (l+1==nb_contour_line_)) ?
                                    nb_onnected_vertex_ :
                                    start_connection_idx_[vertex_index+1]);
        int number_of_connection = connection_end_index - connection_start_index;
        for (int n = 0; n < number_of_connection; n+=2) {
          int index_connection = start_connection_idx_[vertex_index] + n;

          // Get 3D pts (part of triangle) B+C.
          int index_point_b = connection_vertex_idx_[index_connection]*3;
          int index_point_c = connection_vertex_idx_[index_connection+1]*3;
          float* point_b_ptr = &(((float*)mesh_.data)[index_point_b]);
          float* point_c_ptr = &(((float*)mesh_.data)[index_point_c]);
          // Define segment
          edge_ab[0] = point_b_ptr[0] - point_a_ptr[0];
          edge_ab[1] = point_b_ptr[1] - point_a_ptr[1];
          edge_ab[2] = point_b_ptr[2] - point_a_ptr[2];
          edge_ac[0] = point_c_ptr[0] - point_a_ptr[0];
          edge_ac[1] = point_c_ptr[1] - point_a_ptr[1];
          edge_ac[2] = point_c_ptr[2] - point_a_ptr[2];
          // Compute face's normal + normalize
          cross_prod_ab_ac[0] = edge_ab[1] * edge_ac[2] - edge_ab[2] * edge_ac[1];
          cross_prod_ab_ac[1] = edge_ab[2] * edge_ac[0] - edge_ab[0] * edge_ac[2];
          cross_prod_ab_ac[2] = edge_ab[0] * edge_ac[1] - edge_ab[1] * edge_ac[0];
          float cross_prod_ab_ac_norm = cross_prod_ab_ac[0] *
                                        cross_prod_ab_ac[0] +
                                        cross_prod_ab_ac[1] *
                                        cross_prod_ab_ac[1] +
                                        cross_prod_ab_ac[2] *
                                        cross_prod_ab_ac[2];
          cross_prod_ab_ac_norm = std::sqrt(cross_prod_ab_ac_norm);
          cross_prod_ab_ac[0] = cross_prod_ab_ac[0] / cross_prod_ab_ac_norm;
          cross_prod_ab_ac[1] = cross_prod_ab_ac[1] / cross_prod_ab_ac_norm;
          cross_prod_ab_ac[2] = cross_prod_ab_ac[2] / cross_prod_ab_ac_norm;
          // Stack each face contribution and weight them by their angles :
          // i.e. Angle Weighted Pseudo Normal
          float edge_ab_norm = edge_ab[0] * edge_ab[0] +
                               edge_ab[1] * edge_ab[1] +
                               edge_ab[2] * edge_ab[2];
          edge_ab_norm = std::sqrt(edge_ab_norm);
          float edge_ac_norm = edge_ac[0] * edge_ac[0] +
                               edge_ac[1] * edge_ac[1] +
                               edge_ac[2] * edge_ac[2];
          edge_ac_norm = std::sqrt(edge_ac_norm);

          float weight = edge_ab[0] * edge_ac[0] +
                         edge_ab[1] * edge_ac[1] +
                         edge_ab[2] * edge_ac[2];
          weight = weight / (edge_ab_norm*edge_ac_norm);
          weight = std::acos(weight);

          weighted_surface_normal[0] += weight*cross_prod_ab_ac[0];
          weighted_surface_normal[1] += weight*cross_prod_ab_ac[1];
          weighted_surface_normal[2] += weight*cross_prod_ab_ac[2];
        }

        // Average sum of cross-product to get normal's surface
        float surface_normal = weighted_surface_normal[0] *
                               weighted_surface_normal[0] +
                               weighted_surface_normal[1] *
                               weighted_surface_normal[1] +
                               weighted_surface_normal[2] *
                               weighted_surface_normal[2];
        surface_normal = std::sqrt(surface_normal);
        weighted_surface_normal[0] /= surface_normal;
        weighted_surface_normal[1] /= surface_normal;
        weighted_surface_normal[2] /= surface_normal;

        // Compute dot product between surface normal (at current location)
        // with each cam direction (Z axis)
        for (cam_direction_it = camera_facing_direction.begin(),
             min_dot_prod_it = min_dot_prod.begin(),
             contour_it = contour_3d_.begin(),
             contour_normal_it = contour_normal_.begin();
             cam_direction_it != camera_facing_direction.end();
             ++cam_direction_it,
             ++min_dot_prod_it,
             ++contour_it,
             ++contour_normal_it) {
          // Compute dot product
          float* camera_direction_ptr = (float*)cam_direction_it->data;
          float dot_prod = camera_direction_ptr[0] * weighted_surface_normal[0] +
                           camera_direction_ptr[1] * weighted_surface_normal[1] +
                           camera_direction_ptr[2] * weighted_surface_normal[2];
          dot_prod = std::abs(dot_prod);
          if (dot_prod < *min_dot_prod_it) {
            // Save min dot product for each view
            *min_dot_prod_it = dot_prod;
            // Save vertex index
            contour_it->at(l) = line_vertex_idx_[vertex_index];
            // Copy pts
            float* contour_normal_data_ptr = &(((float*)contour_normal_it->data)[3*l]);
            contour_normal_data_ptr[0] = weighted_surface_normal[0];
            contour_normal_data_ptr[1] = weighted_surface_normal[1];
            contour_normal_data_ptr[2] = weighted_surface_normal[2];
            // aSurfWeightedNormal.copyTo((*contour_normal_it)(cv::Rect(0,3*l,1,3)));
          }

          // Save
          float* mesh_normal_data_ptr = (float*)mesh_normal_DBG_.data;
          mesh_normal_data_ptr[count] = point_a_ptr[0];
          mesh_normal_data_ptr[mesh_normal_DBG_.cols + count] = point_a_ptr[1];
          mesh_normal_data_ptr[2*mesh_normal_DBG_.cols + count] = point_a_ptr[2];

          mesh_normal_data_ptr[3*mesh_normal_DBG_.cols + count] = weighted_surface_normal[0];
          mesh_normal_data_ptr[4*mesh_normal_DBG_.cols + count] = weighted_surface_normal[1];
          mesh_normal_data_ptr[5*mesh_normal_DBG_.cols + count] = weighted_surface_normal[2];
        }
        count++;
      }
    }
  }

  /*
   *  @name   MatchContourWithImage
   *  @brief  Compute the correspondance between 3D contour and the image(s) for cases with calibrated environment
   *  @param[in]  cam_structure       Camera configuration setup
   *  @param[in]  model_pose          Model rigid transfrom
   *  @param[in]  image_contour       Binary image with contour, i.e. canny output
   *  @param[in]  face_region_origin  Location of the ROI on the image, used to compensate the projection
   */
  void ContourExtractor::MatchContourWithImage(const std::vector<VirtualCamera*>& cam_structure,
                                               const PPLData::ModelPoseParametersStruct&  model_pose,
                                               const std::vector<cv::Mat>& image_contour,
                                               const std::vector<cv::Point2f>& face_region_origin) {
    // Check for correct inputs
    assert((cam_structure.size() == image_contour.size()) && (cam_structure.size() == number_views_));

    //   Reset matched vector size
    this->InitMatchedContainer();

    // Get the list of valid features (i.e. valid lines)
    float gamma,theta,phi;

    // Goes through each views
    std::vector<VirtualCamera*>::const_iterator camera_it = cam_structure.begin();
    ContourIndexConstIt contour_view_it = contour_3d_.begin();
    ContourIndexIt contour_view_matched_it = matched_contour_3d_.begin();
    ContourIndexIt contour_view_matched_global_it = matched_global_idx_contour_3d_.begin();
    ContourMatrixConstIt surf_normal_it = contour_normal_.begin();
    ContourMatrixConstIt image_it = image_contour.begin();
    ContourPointConstIt image_roi_it = face_region_origin.begin();
    ContourValueIt matched_contour_position_it = matched_contour_img_position_.begin();
    ContourMatrixConstIt model_rotation_it = model_pose.rotation.begin();
    const float* mesh_ptr = (const float*)mesh_.data;
    cv::Rect vertex_roi = cv::Rect(0,0,1,3);
    int v = 0;
    for (; camera_it != cam_structure.end();
         ++camera_it,
         ++contour_view_it,
         ++surf_normal_it,
         ++image_it,
         ++image_roi_it,
         ++contour_view_matched_it,
         ++matched_contour_position_it,
         ++contour_view_matched_global_it) {
      cv::Mat color_image;
      if (show_contour_search_space_) {
        cv::cvtColor(*image_it, color_image, cv::COLOR_GRAY2RGB);
      }

      // Get pose relative to camera
      const float* cam_rot_ptr = (const float*)((*camera_it)->get_extrinsic_matrix().data);
      const float* rot_obj_ptr = (const float*)(model_rotation_it->data);
      cv::Mat rot_obj = cv::Mat(3,3,CV_32FC1);
      float* rot_cam_obj_ptr = (float*)rot_obj.data;

      rot_cam_obj_ptr[0] = cam_rot_ptr[0] * rot_obj_ptr[0] +
                           cam_rot_ptr[1] * rot_obj_ptr[3] +
                           cam_rot_ptr[2] * rot_obj_ptr[6];
      rot_cam_obj_ptr[1] = cam_rot_ptr[0] * rot_obj_ptr[1] +
                           cam_rot_ptr[1] * rot_obj_ptr[4] +
                           cam_rot_ptr[2] * rot_obj_ptr[7];
      rot_cam_obj_ptr[2] = cam_rot_ptr[0] * rot_obj_ptr[2] +
                           cam_rot_ptr[1] * rot_obj_ptr[5] +
                           cam_rot_ptr[2] * rot_obj_ptr[8];
      rot_cam_obj_ptr[3] = cam_rot_ptr[4] * rot_obj_ptr[0] +
                           cam_rot_ptr[5] * rot_obj_ptr[3] +
                           cam_rot_ptr[6] * rot_obj_ptr[6];
      rot_cam_obj_ptr[4] = cam_rot_ptr[4] * rot_obj_ptr[1] +
                           cam_rot_ptr[5] * rot_obj_ptr[4] +
                           cam_rot_ptr[6] * rot_obj_ptr[7];
      rot_cam_obj_ptr[5] = cam_rot_ptr[4] * rot_obj_ptr[2] +
                           cam_rot_ptr[5] * rot_obj_ptr[5] +
                           cam_rot_ptr[6] * rot_obj_ptr[8];
      rot_cam_obj_ptr[6] = cam_rot_ptr[8] * rot_obj_ptr[0] +
                           cam_rot_ptr[9] * rot_obj_ptr[3] +
                           cam_rot_ptr[10]* rot_obj_ptr[6];
      rot_cam_obj_ptr[7] = cam_rot_ptr[8] * rot_obj_ptr[1] +
                           cam_rot_ptr[9] * rot_obj_ptr[4] +
                           cam_rot_ptr[10]* rot_obj_ptr[7];
      rot_cam_obj_ptr[8] = cam_rot_ptr[8] * rot_obj_ptr[2] +
                           cam_rot_ptr[9] * rot_obj_ptr[5] +
                           cam_rot_ptr[10]* rot_obj_ptr[8];

      Utilities::GetRotationAngle(rot_obj, &gamma, &theta, &phi);
      gamma *= Utilities::RadToDeg;
      theta *= Utilities::RadToDeg;
      phi *= Utilities::RadToDeg;

      // Define valid features set
      this->SetValidFeatures(theta, phi);

      // Define projection matrix
      cv::Mat rot = cv::Mat::zeros(4, 4, CV_32FC1);
      ((float*)rot.data)[15] = 1.f;
      cv::Mat pose = (model_pose.scale[0] * model_pose.rotation[0]);
      pose.copyTo(rot(cv::Rect(0,0,3,3)));
      model_pose.translation[0].copyTo(rot(cv::Rect(3,0,1,3)));
      cv::Mat projection_mat = (*camera_it)->get_projection_matrix() * rot;
      const float* proj_mat_ptr = (const float*)projection_mat.data;
      const float* image_center = (*camera_it)->get_center_point();

      // Define points for projection
      cv::Mat vertex = cv::Mat::ones(4, 1, CV_32FC1);
      float* vertex_ptr = (float*)vertex.data;
      cv::Mat normal = cv::Mat::zeros(4, 1, CV_32FC1);
      float* normal_ptr = (float*)normal.data;
      float projected_vertex[3];
      float projected_normal[3];

      // Goes through the whole contour
      std::vector<int>::const_iterator contour_it = contour_view_it->begin();
      std::vector<int>::iterator match_contour_it = contour_view_matched_it->begin();
      std::vector<int>::iterator match_global_contour_it = contour_view_matched_global_it->begin();
      std::vector<float>::iterator match_image_position_it = matched_contour_position_it->begin();
      std::vector<int>::const_iterator valid_feature_it = valid_features_selection_.begin();
      for (int l = 0;
           l < nb_contour_line_;
           ++l,
           ++contour_it,
           match_image_position_it += 2,
           ++match_contour_it,
           ++match_global_contour_it) {
        // Is it a valid features/line ?
        if (valid_feature_it != valid_features_selection_.end() &&
            l == *valid_feature_it) {
          ++valid_feature_it;

          // Define two 3D points to project onto image plane
          // ----------------------------------------------------
          int vertex_index = *contour_it * 3;

          if (vertex_index < 0) {
            LTS5_LOG_ERROR("Unvalid vertex index : " << vertex_index);
          }

          // Save indexes
          *match_contour_it = *contour_it;
          // Convert index into global indexing
          auto found_it = index_map_.find(*contour_it);
          *match_global_contour_it = found_it->second;

          int normal_index = l*3;
          vertex_ptr[0] = mesh_ptr[vertex_index];
          vertex_ptr[1] = mesh_ptr[vertex_index+1];
          vertex_ptr[2] = mesh_ptr[vertex_index+2];
          ((*surf_normal_it)(cv::Rect(0,normal_index,1,3))).copyTo(normal(vertex_roi));
          normal /= (0.05f * model_pose.scale[0]);
          cv::Mat end_normal = cv::Mat(4,1,CV_32FC1);
          float* end_normal_ptr = (float*)end_normal.data;
          end_normal_ptr[0] = vertex_ptr[0] + normal_ptr[0];
          end_normal_ptr[1] = vertex_ptr[1] + normal_ptr[1];
          end_normal_ptr[2] = vertex_ptr[2] + normal_ptr[2];
          end_normal_ptr[3] = vertex_ptr[3] + normal_ptr[3];

          // Project pts onto image plane - 2 pts.
          // cv::Mat projected_vertex = projection_mat * vertex;
          projected_vertex[0] = proj_mat_ptr[0] * vertex_ptr[0] +
                                proj_mat_ptr[1] * vertex_ptr[1] +
                                proj_mat_ptr[2] * vertex_ptr[2] +
                                proj_mat_ptr[3] * vertex_ptr[3];
          projected_vertex[1] = proj_mat_ptr[4] * vertex_ptr[0] +
                                proj_mat_ptr[5] * vertex_ptr[1] +
                                proj_mat_ptr[6] * vertex_ptr[2] +
                                proj_mat_ptr[7] * vertex_ptr[3];
          projected_vertex[2] = proj_mat_ptr[8] * vertex_ptr[0] +
                                proj_mat_ptr[9] * vertex_ptr[1] +
                                proj_mat_ptr[10]* vertex_ptr[2] +
                                proj_mat_ptr[11]* vertex_ptr[3];
          projected_vertex[0] /= projected_vertex[2];
          projected_vertex[1] /= projected_vertex[2];

          // cv::Mat projected_normal = projection_mat * end_normal;
          // projected_normal /= projected_normal.at<float>(2);
          projected_normal[0] = proj_mat_ptr[0] * end_normal_ptr[0] +
                                proj_mat_ptr[1] * end_normal_ptr[1] +
                                proj_mat_ptr[2] * end_normal_ptr[2] +
                                proj_mat_ptr[3] * end_normal_ptr[3];
          projected_normal[1] = proj_mat_ptr[4] * end_normal_ptr[0] +
                                proj_mat_ptr[5] * end_normal_ptr[1] +
                                proj_mat_ptr[6] * end_normal_ptr[2] +
                                proj_mat_ptr[7] * end_normal_ptr[3];
          projected_normal[2] = proj_mat_ptr[8] * end_normal_ptr[0] +
                                proj_mat_ptr[9] * end_normal_ptr[1] +
                                proj_mat_ptr[10]* end_normal_ptr[2] +
                                proj_mat_ptr[11]* end_normal_ptr[3];
          projected_normal[0] /= projected_normal[2];
          projected_normal[1] /= projected_normal[2];


          // Define search line
          // ----------------------------------------------------
          projected_vertex[0] -= image_roi_it->x;
          projected_vertex[1] -= image_roi_it->y;
          projected_normal[0] -= image_roi_it->x;
          projected_normal[1] -= image_roi_it->y;

          cv::Point2f proj_vertex = cv::Point2f(projected_vertex[0],projected_vertex[1]);
          cv::Point2f point_start = cv::Point2f(projected_vertex[0],projected_vertex[1]);
          cv::Point2f point_end = cv::Point2f(projected_normal[0],projected_normal[1]);
          point_start -= (point_end - point_start);

          // Check position consistancy, start should correspond to top left of bounding box
          if (point_start.x > point_end.x) {
            // Start point on the right -> swap
            float dummy = point_start.x;
            point_start.x = point_end.x;
            point_end.x = dummy;
            dummy = point_start.y;
            point_start.y = point_end.y;
            point_end.y = dummy;
          }

          if (show_contour_search_space_) {
            cv::line(color_image, point_start, point_end, cv::Scalar(255,0,0));
            cv::line(color_image, cv::Point2f(projected_vertex[0]-2.f,projected_vertex[1]), cv::Point2f(projected_vertex[0]+2.f,projected_vertex[1]), cv::Scalar(0,255,0));
            cv::line(color_image, cv::Point2f(projected_vertex[0],projected_vertex[1]-2.f), cv::Point2f(projected_vertex[0],projected_vertex[1]+2.f), cv::Scalar(0,255,0));
          }

          // Parametrize search line function
          float dx = point_end.x - point_start.x;
          float dy = point_end.y - point_start.y;
          float slope = dy/dx;
          float offset = point_start.y - slope * point_start.x;

          // Find intersection with contour if there is one
          // ----------------------------------------------------
          int x = (int)std::floor(point_start.x);
          x = x < 0 ? 0 : x > image_it->cols ? image_it->cols - 1 : x;
          int last_x = (int)std::ceil(point_end.x);
          last_x = last_x < 0 ? 0 : last_x > image_it->cols ? image_it->cols - 1 : last_x;
          float distance;
          int y;
          int value;
          float min_dist = FLT_MAX;
          bool matched = false;
          while (x <= last_x) {
            // Get the y
            y = (int)(slope*(float)x + offset);
            if (y < 0 || y >= image_it->rows) {
              break;
            }

            // value = image_it->at<unsigned char>((int)y,(int)x);
            value = ((unsigned char*)(image_it->data))[y*image_it->cols + x];
            if (value == 0xFF) {
              matched = true;
              // Contour, check if closest.
              dx = x - proj_vertex.x;
              dy = y - proj_vertex.y;
              distance = dx*dx + dy*dy;
              if (distance < min_dist)
              {
                min_dist = distance;
                // Save position
                *match_image_position_it = x + image_roi_it->x - image_center[0];
                match_image_position_it[1] = y + image_roi_it->y - image_center[1];
              }
            }
            x++;
          }
          // Check if nothing have been found
          if (!matched) {
            *match_image_position_it = -1.f;
            match_image_position_it[1] = -1.f;
            *match_contour_it = -1;
            *match_global_contour_it = -1;
          } else {
            if (show_contour_search_space_) {
              cv::circle(color_image,
                         cv::Point2f(match_image_position_it[0] -
                                     image_roi_it->x +
                                     image_center[0],
                                     match_image_position_it[1] -
                                     image_roi_it->y +
                                     image_center[1]),
                         2,
                         cv::Scalar(0,0,255));
            }
          }
        } else {
          *match_image_position_it = -1.f;
          match_image_position_it[1] = -1.f;
          *match_contour_it = -1;
          *match_global_contour_it = -1;
        }
      }

      // View has been processed, can safely remove vertex with no match
      contour_view_matched_it->resize(std::distance(contour_view_matched_it->begin(),
                                                    std::remove_if(contour_view_matched_it->begin(),
                                                                   contour_view_matched_it->end(),
                                                                   ContourExtractor::HasFoundMatch<int>)));
      matched_contour_position_it->resize(std::distance(matched_contour_position_it->begin(),
                                                        std::remove_if(matched_contour_position_it->begin(),
                                                                       matched_contour_position_it->end(),
                                                                       ContourExtractor::HasFoundMatch<float>)));
      contour_view_matched_global_it->resize(std::distance(contour_view_matched_global_it->begin(),
                                                           std::remove_if(contour_view_matched_global_it->begin(),
                                                                          contour_view_matched_global_it->end(),
                                                                          ContourExtractor::HasFoundMatch<int>)));

      // Show
      if (show_contour_search_space_) {
        cv::imshow("Contour" + std::to_string(v), color_image);
        cv::waitKey(0);
      }
      v++;
    }
  }

  /*
   *  @name   MatchContourWithImage
   *  @brief  Compute the correspondance between 3D contour and the image(s) for cases with uncalibrated environment
   *  @param[in]  model_pose          Model rigid transfrom
   *  @param[in]  image_contour       Binary image with contour, i.e. canny output
   *  @param[in]  face_region_origin  Location of the ROI on the image, used to compensate the projection
   */
  void ContourExtractor::MatchContourWithImage(const PPLData::ModelPoseParametersStruct& model_pose,
                                               const std::vector<cv::Mat>& image_contour,
                                               const std::vector<cv::Point2f>& face_region_origin,
                                               const std::vector<bool>& view_valid) {
    // Check for correct inputs
    assert((number_views_ == image_contour.size()) && (model_pose.rotation.size() == number_views_));

    //   Reset matched vector size
    this->InitMatchedContainer();

    // Get the list of valid features (i.e. valid lines)
    float gamma,theta,phi;

    // Goes through each views
    ContourIndexConstIt contour_view_it = contour_3d_.begin();
    ContourIndexIt contour_view_matched_it = matched_contour_3d_.begin();
    ContourIndexIt contour_view_matched_global_it = matched_global_idx_contour_3d_.begin();
    ContourMatrixConstIt surf_normal_it = contour_normal_.begin();
    ContourMatrixConstIt image_it = image_contour.begin();
    ContourPointConstIt image_roi_it = face_region_origin.begin();
    ContourValueIt matched_contour_position_it = matched_contour_img_position_.begin();
    cv::Rect vertex_roi = cv::Rect(0,0,1,3);
    const float* mesh_ptr = (const float*)mesh_.data;
    // Goes through each views
    for (int v = 0;
         v < number_views_ ;
         ++v,
         ++contour_view_it,
         ++surf_normal_it,
         ++image_it,
         ++image_roi_it,
         ++contour_view_matched_it,
         ++matched_contour_position_it,
         ++contour_view_matched_global_it) {
      if (view_valid[v]) {
        cv::Mat color_image;
        if (show_contour_search_space_) {
          cv::cvtColor(*image_it, color_image, cv::COLOR_GRAY2RGB);
        }

        // Get pose
        Utilities::GetRotationAngle(model_pose.rotation[v], &gamma, &theta, &phi);
        gamma *= Utilities::RadToDeg;
        theta *= Utilities::RadToDeg;
        phi *= Utilities::RadToDeg;

        // Define valid features set
        this->SetValidFeatures(theta, phi);

        // Define projection matrix
        cv::Mat K =  cv::Mat::eye(2, 3, CV_32FC1) * model_pose.scale[v];
        cv::Mat flip_y = cv::Mat::eye(2, 2, CV_32FC1);
        flip_y.at<float>(1,1) = -1.f;
        cv::Mat rot_mat = flip_y * (K * model_pose.rotation[v]);
        cv::Mat pos_obj = model_pose.translation[v](cv::Rect(0,0,1,2));
        cv::Mat projection_mat = cv::Mat::zeros(2, 4, CV_32FC1);
        rot_mat.copyTo(projection_mat(cv::Rect(0,0,3,2)));
        projection_mat.at<float>(0,3) = pos_obj.at<float>(0);
        projection_mat.at<float>(1,3) = -pos_obj.at<float>(1);
        const float* proj_mat_ptr = (const float*)projection_mat.data;

        // Define points for projection
        cv::Mat vertex = cv::Mat::ones(4, 1, CV_32FC1);
        float* vertex_ptr = (float*)vertex.data;
        cv::Mat normal = cv::Mat::zeros(4, 1, CV_32FC1);
        float* normal_ptr = (float*)normal.data;

        // Normal length
        float aNormalScaling = 0.05f * model_pose.scale[v];

        // Goes through the whole contour, i.e. All lines candidate
        std::vector<int>::const_iterator contour_it = contour_view_it->begin();
        std::vector<int>::iterator match_contour_it = contour_view_matched_it->begin();
        std::vector<int>::iterator match_global_contour_it = contour_view_matched_global_it->begin();
        std::vector<float>::iterator match_image_position_it = matched_contour_position_it->begin();
        std::vector<int>::const_iterator valid_feature_it = valid_features_selection_.begin();
        for (int l = 0;
             l < nb_contour_line_;
             ++l,
             ++contour_it,
             match_image_position_it += 2,
             ++match_contour_it,
             ++match_global_contour_it) {
          // Is it a valid features/line ?
          if (valid_feature_it != valid_features_selection_.end() &&
              l == *valid_feature_it) {
            ++valid_feature_it;

            // Define two 3D points to project onto image plane
            // ----------------------------------------------------
            int vertex_index = *contour_it * 3;
            if (vertex_index < 0) {
              LTS5_LOG_ERROR("Unvalid vertex index : " << vertex_index);
            }

            // Save indexes
            *match_contour_it = *contour_it;
            // Convert index into global indexing
            auto found_it = index_map_.find(*contour_it);
            *match_global_contour_it = found_it->second;

            int normal_index = l*3;
            vertex_ptr[0] = mesh_ptr[vertex_index];
            vertex_ptr[1] = mesh_ptr[vertex_index+1];
            vertex_ptr[2] = mesh_ptr[vertex_index+2];
            ((*surf_normal_it)(cv::Rect(0,normal_index,1,3))).copyTo(normal(vertex_roi));
            normal_ptr[0] /= aNormalScaling;
            normal_ptr[1] /= aNormalScaling;
            normal_ptr[2] /= aNormalScaling;
            cv::Mat end_normal = cv::Mat(4,1,CV_32FC1);
            float* end_normal_ptr = (float*)end_normal.data;
            end_normal_ptr[0] = vertex_ptr[0] + normal_ptr[0];
            end_normal_ptr[1] = vertex_ptr[1] + normal_ptr[1];
            end_normal_ptr[2] = vertex_ptr[2] + normal_ptr[2];
            end_normal_ptr[3] = vertex_ptr[3] + normal_ptr[3];

            // Project pts onto image plane - 2 pts.
            float projected_vertex[2];
            float projected_normal[2];
            // projected_vertex = projection_mat * vertex;
            projected_vertex[0] = proj_mat_ptr[0] * vertex_ptr[0] +
            proj_mat_ptr[1] * vertex_ptr[1] +
            proj_mat_ptr[2] * vertex_ptr[2] +
            proj_mat_ptr[3] * vertex_ptr[3];
            projected_vertex[1] = proj_mat_ptr[4] * vertex_ptr[0] +
            proj_mat_ptr[5] * vertex_ptr[1] +
            proj_mat_ptr[6] * vertex_ptr[2] +
            proj_mat_ptr[7] * vertex_ptr[3];
            // projected_normal = projection_mat * end_normal;
            projected_normal[0] = proj_mat_ptr[0] * end_normal_ptr[0] +
            proj_mat_ptr[1] * end_normal_ptr[1] +
            proj_mat_ptr[2] * end_normal_ptr[2] +
            proj_mat_ptr[3] * end_normal_ptr[3];
            projected_normal[1] = proj_mat_ptr[4] * end_normal_ptr[0] +
            proj_mat_ptr[5] * end_normal_ptr[1] +
            proj_mat_ptr[6] * end_normal_ptr[2] +
            proj_mat_ptr[7] * end_normal_ptr[3];

            // Define search line
            // ----------------------------------------------------
            projected_vertex[0] -= image_roi_it->x;
            projected_vertex[1] -= image_roi_it->y;
            projected_normal[0] -= image_roi_it->x;
            projected_normal[1] -= image_roi_it->y;

            cv::Point2f proj_vertex = cv::Point2f(projected_vertex[0],projected_vertex[1]);
            cv::Point2f point_start = cv::Point2f(projected_vertex[0],projected_vertex[1]);
            cv::Point2f point_end = cv::Point2f(projected_normal[0],projected_normal[1]);
            point_start -= (point_end - point_start);

            // Check position consistancy, start should correspond to top left of bounding box
            if (point_start.x > point_end.x) {
              // Start point on the right -> swap
              float dummy = point_start.x;
              point_start.x = point_end.x;
              point_end.x = dummy;
              dummy = point_start.y;
              point_start.y = point_end.y;
              point_end.y = dummy;
            }

            if (show_contour_search_space_) {
              cv::line(color_image, point_start, point_end, cv::Scalar(255,0,0));
              // FIXME: DEBUG
              cv::line(color_image,
                       cv::Point2f(projected_vertex[0] - 2.f,
                                   projected_vertex[1]),
                       cv::Point2f(projected_vertex[0] + 2.f,
                                   projected_vertex[1]),
                       cv::Scalar(0,255,0));
              cv::line(color_image,
                       cv::Point2f(projected_vertex[0],
                                   projected_vertex[1] - 2.f),
                       cv::Point2f(projected_vertex[0],
                                   projected_vertex[1] + 2.f),
                       cv::Scalar(0,255,0));
            }

            // Parametrize search line function
            float dx = point_end.x - point_start.x;
            float dy = point_end.y - point_start.y;
            float slope = dy/dx;
            float offset = point_start.y - slope * point_start.x;

            // Find intersection with contour if there is one
            // ----------------------------------------------------
            int x = (int)std::floor(point_start.x);
            x = x < 0 ? 0 : x > image_it->cols ? image_it->cols - 1 : x;
            int last_x = (int)std::ceil(point_end.x);
            last_x = last_x < 0 ? 0 : last_x > image_it->cols ? image_it->cols - 1 : last_x;
            float distance;
            int y;
            int value;
            float min_dist = FLT_MAX;
            bool matched = false;
            while (x <= last_x) {
              // Get the y
              y = (int)(slope*(float)x + offset);
              if (y < 0 || y >= image_it->rows) {
                // Out of boundaries
                break;
              }

              // value = image_it->at<unsigned char>(y,x);
              value = ((unsigned char*)(image_it->data))[y*image_it->cols + x];
              if (value == 0xFF) {
                matched = true;
                // Contour, check if closest.
                dx = x - proj_vertex.x;
                dy = y - proj_vertex.y;
                distance = dx*dx + dy*dy;
                if (distance < min_dist) {
                  min_dist = distance;
                  // Save position
                  *match_image_position_it = x + image_roi_it->x;
                  match_image_position_it[1] = y + image_roi_it->y;
                }
              }
              x++;
            }
            // Check if nothing have been found
            if (!matched) {
              *match_image_position_it = -1.f;
              match_image_position_it[1] = -1.f;
              *match_contour_it = -1;
              *match_global_contour_it = -1;
            } else {
              if (show_contour_search_space_) {
                cv::circle(color_image,
                           cv::Point2f(match_image_position_it[0] -
                                       image_roi_it->x,
                                       match_image_position_it[1] -
                                       image_roi_it->y),
                           2,
                           cv::Scalar(0,0,255));
              }
            }
          } else {
            *match_image_position_it = -1.f;
            match_image_position_it[1] = -1.f;
            *match_contour_it = -1;
            *match_global_contour_it = -1;
          }
        }

        // View has been processed, can safely remove vertex with no match
        contour_view_matched_it->resize(std::distance(contour_view_matched_it->begin(),
                                                      std::remove_if(contour_view_matched_it->begin(),
                                                                     contour_view_matched_it->end(),
                                                                     ContourExtractor::HasFoundMatch<int>)));
        matched_contour_position_it->resize(std::distance(matched_contour_position_it->begin(),
                                                          std::remove_if(matched_contour_position_it->begin(),
                                                                         matched_contour_position_it->end(),
                                                                         ContourExtractor::HasFoundMatch<float>)));
        contour_view_matched_global_it->resize(std::distance(contour_view_matched_global_it->begin(),
                                                             std::remove_if(contour_view_matched_global_it->begin(),
                                                                            contour_view_matched_global_it->end(),
                                                                            ContourExtractor::HasFoundMatch<int>)));

        // Show
        if(show_contour_search_space_) {
          cv::imshow("Contour" + std::to_string(v), color_image);
          cv::waitKey(5);
        }
      } else {
        contour_view_matched_it->clear();
        matched_contour_position_it->clear();
        contour_view_matched_global_it->clear();
      }
    }
  }

#pragma mark -
#pragma mark Debug

  /*
   *  @name   Dump3DContourIntoFile
   *  @brief  Dump into a binary file the 3D contour extracted
   *  @param[in]  filename    Where to write output
   *  @return -1 If error
   */
  int ContourExtractor::Dump3DContourIntoFile(const std::string& filename) const {
    int ret = 0;

    // Open stream
    std::ofstream out_stream(filename,std::ios::out|std::ios::binary);
    if(out_stream.is_open())
    {
      // Ok
      // Write size
      out_stream.write((const char*)&number_views_, sizeof(int));
      out_stream.write((const char*)&nb_contour_line_, sizeof(int));

      // Write data
      for (ContourIndexConstIt contour_it = contour_3d_.begin();
           contour_it != contour_3d_.end();
           ++contour_it) {
        // Write
        out_stream.write((const char*)contour_it->data(),
                         nb_contour_line_*sizeof(int));
      }

      // Close file
      out_stream.flush();
      out_stream.close();
    } else {
      ret = -1;
    }

    // End
    return ret;
  }

  /*
   *  @name   WriteInternalMatToFile
   *  @brief  Dump internal matrices into file(s)
   *  @param  exported filename
   */
  void ContourExtractor::WriteInternalMatToFile(const std::string& filename) const {
    // Check if extension is present
    std::string name = filename;
    const int position_extension = (int)filename.find(".", filename.size()-5);
    if(position_extension != std::string::npos) {
      // remove extension
      name = filename.substr(0,position_extension);
    }
    // Finally add extension
    name.append(".csv");
    Utilities::WriteMatToCsv(name, mesh_normal_DBG_);
  }

  #pragma mark -
  #pragma mark Private Functions

  /*
   *  @name   InitMatchedContainer
   *  @brief  Initiliation of internal container for 3D-2D matching step
   */
  void ContourExtractor::InitMatchedContainer(void) {
    ContourIndexIt matched_contour_it = matched_contour_3d_.begin();
    if (matched_contour_it->size() != nb_contour_line_) {
      ContourValueIt matched_contour_image_position_it = matched_contour_img_position_.begin();
      ContourIndexIt matched_global_contour_it = matched_global_idx_contour_3d_.begin();
      for (; matched_contour_it != matched_contour_3d_.end();
           ++matched_contour_it,
           ++matched_contour_image_position_it,
           ++matched_global_contour_it) {
        matched_contour_it->resize(nb_contour_line_,-1);
        matched_contour_image_position_it->resize(2*nb_contour_line_,-1.f);
        matched_global_contour_it->resize(nb_contour_line_,-1);
      }
    }
  }

  /*
   *  @name   HasFoundMatch
   *  @brief  Indicate if the element in the vector has a match in the contour. No match is indicate by -1
   *  @param[in]  value      Vector value
   *  @return     True if value != -1
   */
  template<class T>
  bool ContourExtractor::HasFoundMatch(const T value) {
    return value == (T)-1;
  }

  /*
   *  @name   isInRange
   *  @brief  Look into constraint vector (i.e. aBoounds) and if value is included in the range, provide the corresponding index
   *  @param  aBounds     Range for a given angle
   *  @param  value      Angle to look for inside the range list
   *  @return Index where the value is inside the range, -1 otherwise
   */
  int ContourExtractor::IsInRange(const std::vector<std::pair<float, float>>& bounds,
                                  const float value) {
    int index = -1;
    std::vector<std::pair<float, float>>::const_iterator found_it = bounds.begin();
    for (int k = 0; found_it != bounds.end(); ++k, ++found_it) {
      if (found_it->first < value && value <= found_it->second) {
        index = k;
        break;
      }
    }
    return index;
  }

  /*
   *  @name   SetValidFeatures
   *  @brief  Provide a List of valid vertex index used to match with images
   *  @param[in]  theta  Head pose, theta (left.right)
   *  @param[in]  phi    Head pose, phi  (up,down)
   */
  void ContourExtractor::SetValidFeatures(const float& theta,const float& phi) {
    // Look up for correct selection
    int index = ContourExtractor::IsInRange(phi_bounds_, phi);
    if (index != -1) {
      valid_features_selection_ = phi_features_[index];
    } else {
      valid_features_selection_.clear();
    }
  }
}
