/**
 *  @file   annotation_2d_extractor.cpp
 *  @brief  Extract 2D Annotation from file
 *
 *  @author Christophe Ecabert
 *  @date   04/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <fstream>
#include <limits>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/logger.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/face_reconstruction/annotation_2d_extractor.hpp"
#include "lts5/face_reconstruction/utilities.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/*
 *  @name   Annotation2DExtractor
 *  @brief  Constructor
 */
Annotation2DExtractor::Annotation2DExtractor(const Parameter& extractor_config) :
        BaseExtractor::BaseExtractor(extractor_config.number_of_view),
        params_(extractor_config) {

  // Scan folder
  annotation_loaded_ = 0;
  face_bbox_[0] = 0.f;
  face_bbox_[1] = 0.f;
  face_bbox_[2] = 0.f;
  face_bbox_[3] = 0.f;
  // Init list
  list_annotation_file_.resize(params_.number_of_view);
  size_t ann_per_folder = std::numeric_limits<size_t>::max();
  for (int k = 0; k < params_.number_of_view; k++) {
    // Scan for annotation file
    LTS5::GetFileListing(list_annotation_file_[k],
                         params_.annotation_root_folder,
                         extractor_config.annotation_folders[k],
                         extractor_config.annotation_extension_type);
    // Sort by number only the part relative to the scanned folder
    std::sort(list_annotation_file_[k].begin(),
              list_annotation_file_[k].end());
    ann_per_folder = std::min(ann_per_folder, list_annotation_file_[k].size());
  }

  if ((int)list_annotation_file_.size() % params_.number_of_view != 0) {
    // Error
    using ProcessErrorEnum = LTS5::ProcessError::ProcessErrorEnum;
    throw LTS5::ProcessError(ProcessErrorEnum::kErr,
                             "Number of annotation/pose file does not match!",
                             FUNC_NAME);
  }

  // Ok
  maximum_annotation_ = static_cast<int>(ann_per_folder);
}

/*
 *  @name   ~Annotation2DExtractor
 *  @brief  Destructor
 */
Annotation2DExtractor::~Annotation2DExtractor() {
}

/*
 *  @name   Process
 *  @fn void Process(const BaseStream::Buffer& buffer)
 *  @brief  Run face tracker or load annotation from file and put result
 *          into data buffer. Estimate head  pose from landmarks if available
 *  @param[in]  buffer    Buffer holding captured image
 */
void Annotation2DExtractor::Process(const BaseStream::Buffer& buffer) {
  // Annotation - from 2D landmarks
  if (annotation_loaded_ < maximum_annotation_) {
    // Goes for each views
    auto image_it = buffer.host.cbegin();
    auto face_origin_it = data_.face_region.begin();
    auto contour_it = data_.contour.begin();

    for(int k = 0 ; k < params_.number_of_view ; k++) {
      // Open annotation file
      std::string f_name = list_annotation_file_[k][annotation_loaded_];
      std::ifstream annotation_stream(f_name.c_str(),
                                      std::ifstream::in);
      LTS5_LOG_DEBUG("Load annotation from : " << f_name);
      // Correctly open ?
      if (annotation_stream.is_open()) {
        // ---------------------------------------------------------------
        // Ok - read first line
        std::string line;
        int max_landmark;
        int landmark_counter = 0;
        float x,y;
        bool value = true;
        do {
          std::getline(annotation_stream, line);
        }
        while (line.find("n_points") == std::string::npos);
        std::sscanf(line.c_str(), "n_points:%d",&max_landmark);
        // Load landmarks
        cv::Mat landmark = cv::Mat(2 * max_landmark, 1, CV_32FC1);
        while (!annotation_stream.eof()) {
          // Read line
          std::getline(annotation_stream, line);
          // Read value
          if (std::sscanf(line.c_str(), "%f %f", &x, &y) == 2) {
            // Get landmarks position
            landmark.at<float>(landmark_counter * 2) = x;
            landmark.at<float>(landmark_counter * 2 + 1) = y;
            // Look for bounding box
            if (value) {
              face_bbox_[0] = x;   face_bbox_[1] = x;
              face_bbox_[2] = y;   face_bbox_[3] = y;
              value = false;
              ++landmark_counter;
              continue;
            }

            // Xmax
            face_bbox_[1] = face_bbox_[1] < x ? x : face_bbox_[1];
            // Xmin
            face_bbox_[0] = face_bbox_[0] > x ? x : face_bbox_[0];
            // Ymax
            face_bbox_[3] = face_bbox_[3] < y ? y : face_bbox_[3];
            // Ymin
            face_bbox_[2] = face_bbox_[2] > y ? y : face_bbox_[2];
            // Cnt
            ++landmark_counter;
          }
        }
        data_.landmarks[k] = landmark.clone();
        // Finish close file
        annotation_stream.close();
        // Extract countour from image
        // ---------------------------------------------------------------
        if (params_.use_contour) {
          // Extend bounding box
          float face_width = (face_bbox_[1] -
                              face_bbox_[0]) *
                              params_.face_bbox_margin;
          float face_height = (face_bbox_[3] -
                               face_bbox_[2]) *
                                params_.face_bbox_margin;

          face_bbox_[0] -= face_width;
          face_bbox_[1] += face_width;
          face_bbox_[2] -= face_height;
          face_bbox_[3] += face_height;

          // Out of boundary ?
          face_bbox_[0] = face_bbox_[0] < 0.f ? 0.f :
                                  face_bbox_[0];
          face_bbox_[1] = face_bbox_[1] > params_.image_width ?
                                  params_.image_width :
                                  face_bbox_[1];
          face_bbox_[2] = face_bbox_[2] < 0.f ? 0.f :
                                  face_bbox_[2];
          face_bbox_[3] = face_bbox_[3] > params_.image_height ?
                                  params_.image_height :
                                  face_bbox_[3];
          // Save origin (x,y)
          (*face_origin_it).x = face_bbox_[0];
          (*face_origin_it).y = face_bbox_[2];
          ++face_origin_it;
          // Copy
          cv::Rect roi = cv::Rect((int)face_bbox_[0],
                                  (int)face_bbox_[2],
                                  (int)(face_bbox_[1] -
                                        face_bbox_[0]),
                                  (int)(face_bbox_[3] -
                                        face_bbox_[2]));
          contour_it->create(roi.height, roi.width, image_it->type());
          ((*image_it)(roi)).copyTo(*contour_it);
          // Edge detector
          cv::blur(*contour_it, *contour_it, cv::Size(3,3));
          cv::Canny(*contour_it,
                    *contour_it,
                    params_.edge_threshold[0],
                    params_.edge_threshold[1]);
          // Move iterator
          ++contour_it;
        }
      } else {
        using ProcessErrorEnum = LTS5::ProcessError::ProcessErrorEnum;
        throw LTS5::ProcessError(ProcessErrorEnum::kErrOpeningFile,
                                 "Unable to open annotation/pose file",
                                 FUNC_NAME);
      }

      // Next images
      ++image_it;
    }

    // Inc annotation loaded
    annotation_loaded_++;
  }
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name   set_frame_index
 *  @fn void set_frame_index(int frame_index)
 *  @brief  Set the index of the frame to process
 *  @param[in]  frame_index       Index of the frame
 */
void Annotation2DExtractor::set_frame_index(int frame_index) {
  annotation_loaded_ = frame_index < maximum_annotation_ ? frame_index :
  maximum_annotation_ - 1 ;
}
}  // namepsace LTS5
