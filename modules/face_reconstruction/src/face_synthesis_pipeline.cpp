/**
 *  @file   face_synthesis_pipeline.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 04/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <random>
#include <vector>
#include <string>
#include <limits>
#include <chrono>

#include "tinyxml2.h"
#include "opencv2/opencv.hpp"

#include "lts5/utils/itokenstream.hpp"
#include "lts5/utils/profiling.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/face_reconstruction/face_synthesis_pipeline.hpp"
#include "lts5/face_reconstruction/utilities.hpp"
#include "lts5/face_reconstruction/image_stream.hpp"
#include "lts5/face_reconstruction/video_stream.hpp"
#include "lts5/face_reconstruction/pointgrey_stream.hpp"
#include "lts5/face_reconstruction/external_video_stream.hpp"
#include "lts5/face_reconstruction/annotation_2d_extractor.hpp"
#include "lts5/face_reconstruction/landmark_extractor.hpp"
#include "lts5/face_reconstruction/mesh2image_exporter.hpp"
#include "lts5/face_reconstruction/mesh2screen_exporter.hpp"

#include "lts5/utils/file_io.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   FaceSynthesisPipeline
 *  @brief  Constructor
 *  @param  pipeline_configuration      Pipeline parameters
 */
FaceSynthesisPipeline::FaceSynthesisPipeline(const FaceSynthesisPipelineParameters& configuration) {
  // Open xml config file
  tinyxml2::XMLDocument document;
  auto err = document.LoadFile(configuration.pipeline_xml_config_file.c_str());

  if(err != tinyxml2::XMLError::XML_NO_ERROR) {
    throw LTS5::ProcessError(ProcessError::ProcessErrorEnum::kErrOpeningFile,
                             "Unable to open XML config file.",
                             FUNC_NAME);
  }
  // Define internal working directory
  working_directory_ = configuration.working_directory;
  if (working_directory_.length() > 0) {
    if (working_directory_.at(working_directory_.length() - 1) != '/') {
      working_directory_ += "/";
    }
  }
  // Define output if any provided, default empty
  output_folder_ = configuration.output_folder;
  if (!output_folder_.empty()) {
    output_folder_ = output_folder_.back() == '/' ?
                     working_directory_ + output_folder_ :
                     working_directory_ + output_folder_ + '/';
  }
  // Config open
  this->ParseXML(document);

  try {
    // Input module
    // ---------------------------------
    switch (stream_params_->stream_type) {
      case BaseStream::StreamType::kPointGrey: {
        auto& param = *reinterpret_cast<PointGreyStream::Parameter*>(stream_params_);
        frame_capture_ = new PointGreyStream(param);
      }
        break;
      case BaseStream::StreamType::kImage: {
        auto& param = *reinterpret_cast<ImageStream::Parameter*>(stream_params_);
        frame_capture_ = new ImageStream(param);
      }
        break;
      case BaseStream::StreamType::kVideo: {
        auto& param = *reinterpret_cast<VideoStream::Parameter*>(stream_params_);
        frame_capture_ = new VideoStream(param);
      }
        break;
      case BaseStream::StreamType::kExtVideo: {
        auto& param = *reinterpret_cast<ExternalVideoStream::Parameter*>(stream_params_);
        frame_capture_ = new ExternalVideoStream(param);
      }
        break;

      default: throw LTS5::ProcessError(LTS5::ProcessError::kErr,
                                        "Unknown stream type",
                                        FUNC_NAME);
        break;
    }

    // LandmarkExtractor
    // ---------------------------------
    extractor_config_->number_of_view = stream_params_->n_view;
    extractor_config_->image_width = frame_capture_->get_image_width();
    extractor_config_->image_height = frame_capture_->get_image_height();
    switch (extractor_config_->detection_type) {
      case BaseExtractor::kDetector: {
        auto& param = *reinterpret_cast<LandmarkExtractor::Parameter*>(extractor_config_);
        face_extractor_ = new LTS5::LandmarkExtractor(param);
      }
        break;

      case BaseExtractor::kAnnotation2D: {
        auto& param = *reinterpret_cast<Annotation2DExtractor::Parameter*>(extractor_config_);
        face_extractor_ = new LTS5::Annotation2DExtractor(param);
      }
        break;

      default:  throw LTS5::ProcessError(LTS5::ProcessError::kErr,
                                         "Unknown extractor type",
                                         FUNC_NAME);
        break;
    }

    // Fitter module
    // ---------------------------------
    // fitter_configuration_.camera_setup = &_syntheticCameraSetup;
    fitter_configuration_.camera_setup = &camera_setup_;
    model_fitter_ = new FaceModelFitter(fitter_configuration_);
    cv::Scalar model_dim = model_fitter_->get_model_size();

    // Mesh
    // ---------------------------------
    face_mesh_ = new LTS5::Mesh<float>();
    face_mesh_->Load(triangulation_file_);
    face_mesh_->get_vertex().resize(static_cast<size_t>(model_dim[2]));
    face_mesh_->get_normal().resize(static_cast<size_t>(model_dim[2]));

    // Set mesh container, reconstruction will be placed directly in the mesh
    model_fitter_->set_external_mesh_container(face_mesh_);

    // Texture mapper
    // ---------------------------------
    texture_mapper_ = new TexMapper(texture_mapper_config_);
    texture_mapper_->Initialize(face_mesh_);

    for (int v = 0; v < stream_params_->n_view; ++v) {
      projection_mat_.push_back(cv::Mat(2, 4, CV_32FC1));
    }


    // Exporter
    // ---------------------------------
    if (output_folder_.empty() && configuration.output_on_screen) {
      mesh_exporter_ = new LTS5::Mesh2ScreenExporter();
    } else {
      mesh_exporter_ = new LTS5::Mesh2ImageExporter();
    }


  } catch(const LTS5::ProcessError& e) {
    // Release some memory
    this->ReleaseMemory();
    // Indicate there is an error during construction, rethrow
    throw;
  }
}

/*
 *  @name   ~ProcessPipeline
 *  @brief  Destructor
 */
FaceSynthesisPipeline::~FaceSynthesisPipeline() {
  // Release memory
  this->ReleaseMemory();
}

/*
 *  @name   deallocateMe
 *  @brief  Release dynamically instantiated objects
 */
void FaceSynthesisPipeline::ReleaseMemory(void) {
  // Frame Capture
  // ---------------------------------------------------
  if (frame_capture_) {
    delete frame_capture_;
    frame_capture_ = nullptr;
  }
  if (stream_params_) {
    delete stream_params_;
    stream_params_ = nullptr;
  }
  // Landmark Extractor
  // ---------------------------------------------------
  if (face_extractor_ != nullptr) {
    delete face_extractor_;
    face_extractor_ = nullptr;
  }
  if (extractor_config_) {
    delete extractor_config_;
    extractor_config_ = nullptr;
  }
  // 3D Reconstruction
  // ---------------------------------------------------
  if (model_fitter_ != nullptr) {
    delete model_fitter_;
    model_fitter_ = nullptr;
  }
  // Mesh
  // ---------------------------------------------------
  if (face_mesh_) {
    delete face_mesh_;
    face_mesh_ = nullptr;
  }
  // Texture mapper
  // ---------------------------------------------------
  if (texture_mapper_) {
    mesh_exporter_->ActiveContext();
    delete texture_mapper_;
    texture_mapper_ = nullptr;
    mesh_exporter_->DisableContext();
  }
  // Export
  // ---------------------------------------------------
  if(mesh_exporter_) {
    delete mesh_exporter_;
    mesh_exporter_ = nullptr;
  }
  // Release camera
  // ---------------------------------------------------
  if(!camera_setup_.empty()) {
    for (LTS5::VirtualCamera* camera : camera_setup_) {
      delete camera;
    }
    camera_setup_.clear();
  }
}

/*
 *  @name   ParseXML
 *  @fn void ParseXML(const tinyxml2::XMLDocument& document)
 *  @brief  Read xml file and fill proper structure to
 *          initialize process pipeline
 *  @param[in]    document    XML document
 */
void FaceSynthesisPipeline::ParseXML(const tinyxml2::XMLDocument& document) {
  // Get root node
  const tinyxml2::XMLElement* root = document.RootElement();
  // Define nodes (One for each module)
  const tinyxml2::XMLElement* xml_node;
  const tinyxml2::XMLElement* xml_property;

  // Frame capture
  // ---------------------------------
  xml_node = root->FirstChildElement("FRAME_CAPTURE");
  xml_property = xml_node->FirstChildElement();

  // Input type
  int mode = -1;
  xml_property->QueryIntText(&mode);
  xml_property = xml_property->NextSiblingElement();
  switch ((BaseStream::StreamType)mode) {
    case BaseStream::StreamType::kPointGrey : {
      stream_params_ = new PointGreyStream::Parameter();
      stream_params_->stream_type = BaseStream::StreamType::kPointGrey;
      auto* param = reinterpret_cast<PointGreyStream::Parameter*>(stream_params_);
      param->external_trigger = true;
      xml_property = xml_property->NextSiblingElement();
    }
      break;

    case BaseStream::StreamType::kImage : {
      stream_params_ = new ImageStream::Parameter();
      stream_params_->stream_type = BaseStream::StreamType::kImage;
      auto* param = reinterpret_cast<ImageStream::Parameter*>(stream_params_);
      param->root_folder = working_directory_ + xml_property->Attribute("root");
      param->image_ext = xml_property->Attribute("ext");
      xml_property = xml_property->NextSiblingElement();
    }
      break;
    case BaseStream::StreamType::kVideo : {
      stream_params_ = new VideoStream::Parameter();
      stream_params_->stream_type = BaseStream::StreamType::kVideo;
      auto* param = reinterpret_cast<VideoStream::Parameter*>(stream_params_);
      param->root_folder = working_directory_ + xml_property->Attribute("root");
      param->video_ext = xml_property->Attribute("ext");
      xml_property = xml_property->NextSiblingElement();
    }
      break;

    case BaseStream::StreamType::kExtVideo : {
      stream_params_ = new ExternalVideoStream::Parameter();
      stream_params_->stream_type = BaseStream::StreamType::kExtVideo;
      xml_property = xml_property->NextSiblingElement();
    }
      break;

    default: LTS5_LOG_ERROR("Input type not supported (config file)");
      break;
  }
  // Number of camera
  xml_property->QueryIntText(&stream_params_->n_view);
  xml_property = xml_property->NextSiblingElement();
  // Camera position - TMP
  std::string calibration_file;
  std::string image_folder;
  for (int i = 0; i < stream_params_->n_view; i++) {
    // Query attributes
    calibration_file = working_directory_ + xml_property->Attribute("CalibFile");
    image_folder = xml_property->Attribute("ImgFolder");
    // Goes to the next one
    xml_property = xml_property->NextSiblingElement();
    // Create camera
    if (calibration_file != "") {
      camera_setup_.push_back(new LTS5::VirtualCamera(calibration_file));
    }
    if (stream_params_->stream_type == BaseStream::StreamType::kImage) {
      // Push folder
      auto* param = reinterpret_cast<ImageStream::Parameter*>(stream_params_);
      param->image_folder.push_back(image_folder);
    }

  }

  // LandmarkExtractor
  // ---------------------------------
  xml_node = root->FirstChildElement("FACE_EXTRACTOR");
  xml_property = xml_node->FirstChildElement();
  mode = -1;
  xml_property->QueryIntText(&mode);
  xml_property = xml_property->NextSiblingElement();
  // Type of extraction
  switch (static_cast<BaseExtractor::DetectionType>(mode)) {
    // 2D Annotations
    case BaseExtractor::DetectionType::kAnnotation2D:
      extractor_config_ = new LTS5::Annotation2DExtractor::Parameter();
      break;
    case BaseExtractor::DetectionType::kDetector:
      extractor_config_ = new LTS5::LandmarkExtractor::Parameter();
      break;

    default:
      break;
  }
  extractor_config_->detection_type = static_cast<BaseExtractor::DetectionType>(mode);
  // Axis inversion
  extractor_config_->axis_inversion[0] = xml_property->FloatAttribute("gamma");
  extractor_config_->axis_inversion[1] = xml_property->FloatAttribute("theta");
  extractor_config_->axis_inversion[2] = xml_property->FloatAttribute("phi");
  xml_property = xml_property->NextSiblingElement();

  // Contour
  extractor_config_->use_contour = (xml_property->IntAttribute("mode") > 0);
  std::string threshold = xml_property->Attribute("px_thresh");
  std::sscanf(threshold.c_str(),
              "%d %d",
              &extractor_config_->edge_threshold[0],
              &extractor_config_->edge_threshold[1]);
  extractor_config_->face_bbox_margin = xml_property->FloatAttribute("bbox");
  xml_property = xml_property->NextSiblingElement();
  // Load parameters
  switch (extractor_config_->detection_type) {
    case BaseExtractor::DetectionType::kDetector : {
      // Read configuration path + thresh if used
      auto* param = reinterpret_cast<LandmarkExtractor::Parameter*>(extractor_config_);
      param->tracker_configuration_path = (working_directory_ +
                                           xml_property->Attribute("configuration"));
      param->tracker_face_detector_path = (working_directory_ +
                                           xml_property->Attribute("f_detector"));
      param->tracker_threshold = xml_property->Attribute("thresh");
      param->type = xml_property->Attribute("type");
      param->start_pass = xml_property->IntAttribute("start_pass");
    }
      break;

    case BaseExtractor::DetectionType::kAnnotation2D : {
      // Skip tracker entry
      xml_property = xml_property->NextSiblingElement();
      // Get annotation folder
      auto* param = reinterpret_cast<Annotation2DExtractor::Parameter*>(extractor_config_);
      param->annotation_root_folder = working_directory_ + xml_property->Attribute("root");
      param->annotation_extension_type = xml_property->Attribute("ext");
      xml_property = xml_property->NextSiblingElement();
      // Scan annotation folder
      for (int k = 0; k < stream_params_->n_view; k++) {
        // Annotation + pose
        param->annotation_folders.push_back(xml_property->Attribute("folder"));
        param->pose_estimation_files.push_back(xml_property->Attribute("pose"));
        xml_property = xml_property->NextSiblingElement();
      }
    }
      break;

    default:
      break;
  }

  // Fitter
  // ---------------------------------
  xml_node = root->FirstChildElement("MODEL_FITTER");
  xml_property = xml_node->FirstChildElement();
  // Number of views
  fitter_configuration_.number_of_view = stream_params_->n_view;
  // Bilinear model
  fitter_configuration_.model_filename = working_directory_ + xml_property->GetText();
  xml_property = xml_property->NextSiblingElement();
  // Landmark
  fitter_configuration_.feature_index_filename = working_directory_ + xml_property->GetText();
  xml_property = xml_property->NextSiblingElement();
  // Triangulation configuration
  triangulation_file_ = working_directory_ + xml_property->GetText();
  xml_property = xml_property->NextSiblingElement();
  // Close triangulation
  fitter_configuration_.close_triangulation_filename = working_directory_ + xml_property->GetText();
  xml_property = xml_property->NextSiblingElement();
  // Is contour part of the optimisation ?
  if(std::atoi(xml_property->Attribute("Usage")) > 0) {
    fitter_configuration_.extractor_configuration = working_directory_ + std::string(xml_property->Attribute("File"));
  }
  xml_property = xml_property->NextSiblingElement();
  // Regularization factor (Eta)
  xml_property->QueryFloatAttribute("id", &fit_parameter_.eta_identity);
  xml_property->QueryFloatAttribute("expr", &fit_parameter_.eta_expression);
  xml_property->QueryFloatAttribute("gamma_id",&fit_parameter_.gamma_identity);
  xml_property->QueryFloatAttribute("time_expr",&fit_parameter_.eta_time_expression);
  xml_property = xml_property->NextSiblingElement();
  // PRojection model
  xml_property->QueryIntText(&mode);
  projection_model_ = (FaceModelFitter::ProjectionMode)mode;
  fitter_configuration_.projection_model = projection_model_;
  xml_property = xml_property->NextSiblingElement();
  // Number of iteration
  xml_property->QueryIntText(&fit_parameter_.max_iteration);

  // Texture mapper
  // ---------------------------------
  xml_property = xml_property->NextSiblingElement();
  xml_property->QueryAttribute("mode", &mode);
  texture_mapper_config_.mapping_type = static_cast<TextureMapper::MappingType>(mode);
  texture_mapper_config_.control_pts_config_file = (working_directory_ +
                                                    std::string(xml_property->
                                                            Attribute("ctrl_pts")));
  xml_property->QueryIntAttribute("size", &texture_mapper_config_.texture_map_size);
  xml_property->QueryIntAttribute("d_factor", &texture_mapper_config_.d_factor);
  xml_property->QueryFloatAttribute("Wn", &texture_mapper_config_.w_n);
  xml_property->QueryFloatAttribute("Wu", &texture_mapper_config_.w_u);
  xml_property->QueryIntAttribute("WnThresh", &texture_mapper_config_.per_vertex_thresh);
  xml_property->QueryFloatAttribute("GammaExp", &texture_mapper_config_.gamma_exp);
}

#pragma mark -
#pragma mark Fit

/*
 *  @name   Process
 *  @brief  Start the synthesis for the current frame
 *  @param  process_configuration  Configuration for current fit
 */
int FaceSynthesisPipeline::Process(const PipelineProcessConfiguration& process_configuration) {
  int err = -1;
  if (!process_configuration.has_synthetic_data) {
    // Query frame
    // -------------------------------------------------------------------------
    double t1 = cv::getTickCount();
    err = frame_capture_->Read();
    double t2 = cv::getTickCount() - t1;
    double dt = (t2/cv::getTickFrequency())*1000.0;
    LTS5_LOG_DEBUG("\tcapture time : " << dt << " ms");
    if (!err) {
      // Run Extractor
      // -----------------------------------------------------------------------
      t1 = cv::getTickCount();
      face_extractor_->Process(frame_capture_->get_buffer());
      t2 = cv::getTickCount() - t1;
      dt = (t2/cv::getTickFrequency())*1000.0;
      LTS5_LOG_DEBUG("\ttracker time : " << dt  << " ms");
      extractor_data_ = face_extractor_->get_data();
      fit_parameter_.use_contour = extractor_config_->use_contour;
      fit_parameter_.face_region_origin = extractor_data_.face_region;
      fit_parameter_.image_contour = extractor_data_.contour;



      /*const auto& images = frame_capture_->get_buffer().host;
      const auto& lms = extractor_data_.landmarks;
      for (size_t k = 0; k < images.size(); ++k) {
        cv::Mat canvas;
        cv::cvtColor(images[k], canvas, cv::COLOR_GRAY2BGR);
        if (!lms[k].empty()) {
          const auto& shape = lms[k];
          int n_pts = std::max(shape.cols, shape.rows) / 2;
          for (int p = 0; p < n_pts; ++p) {
            cv::circle(canvas,
                       cv::Point(shape.at<float>(p*2),
                                 shape.at<float>(p*2 + 1)),
                       2,
                       CV_RGB(0, 255, 0),
                       -1);
          }
        }
        cv::imshow("input_" + std::to_string(k), canvas);
      }
      cv::waitKey(30);*/






      // 3D Reconstruction
      // -------------------------------------------------------------------------
      t1 = cv::getTickCount();
      err = model_fitter_->FitModel(extractor_data_.landmarks, fit_parameter_, &fit_result_);
      t2 = cv::getTickCount() - t1;
      dt = (t2/cv::getTickFrequency())*1000.0;
      LTS5_LOG_DEBUG("\tfit time : " << dt << " ms");
      if (!err) {
        // Update normal filter
        face_mesh_->ComputeVertexNormal();
        // OpenGL Initialization, lazy init
        if (!mesh_exporter_->is_initialiazed()) {
          cv::Mat dummy(texture_mapper_config_.texture_map_size,
                        texture_mapper_config_.texture_map_size,
                        CV_8UC1);
          mesh_exporter_->Initialize(*face_mesh_,
                                     dummy,
                                     512,
                                     512);

#ifdef HAS_CUDA_
          mesh_exporter_->ActiveContext();
          // Init CUDA-OpenGL mapping
          err = texture_mapper_->RegisterOpenGL(mesh_exporter_->get_texture());
          if (err) {
            LTS5_LOG_ERROR("Can not link Cuda&OpenGL together");
          }
          mesh_exporter_->DisableContext();
#endif
        }
        // Set proper ogl context
        mesh_exporter_->ActiveContext();
        // Texture mapping
        // -------------------------------------------------------------------------
        t1 = cv::getTickCount();
        for (int v = 0; v < stream_params_->n_view; ++v) {
          projection_mat_[v] = model_fitter_->get_projection_matrix(v);
        }
        err = texture_mapper_->Process(frame_capture_->get_buffer(),
                                       projection_mat_,
                                       extractor_data_.landmarks,
                                       face_mesh_);
        t2 = cv::getTickCount() - t1;
        dt = (t2/cv::getTickFrequency())*1000.0;
        LTS5_LOG_DEBUG("\ttex time : " << dt << " ms");
        // Save image
        // -------------------------------------------------------------------------
        std::string output_name;
        if (!output_folder_.empty()) {
          std::string image_name;
          std::string image_ext;
          frame_capture_->get_frame_name(&image_name, &image_ext);
          output_name = output_folder_ + image_name + "_synth" + image_ext;
        }
        if (!err) {
          mesh_exporter_->set_mesh(*face_mesh_);
#ifndef HAS_CUDA_
          mesh_exporter_->set_texture(texture_mapper_->get_texture_map());
#endif
          t1 = cv::getTickCount();
          mesh_exporter_->set_output_name(output_name);
          mesh_exporter_->Process();
          t2 = cv::getTickCount() - t1;
          dt = (t2/cv::getTickFrequency()) * 1000.0;
          LTS5_LOG_DEBUG("\texport time : " << dt << " ms");
        }
        // Detach
        mesh_exporter_->DisableContext();
      }
    }
  } else {
    // 3D Reconstruction
    // -------------------------------------------------------------------------
    auto t1 = cv::getTickCount();
    int err = model_fitter_->FitModel(process_configuration.landmark,
                                      fit_parameter_,
                                      &fit_result_);
    auto t2 = cv::getTickCount() - t1;
    auto dt = (t2/cv::getTickFrequency())*1000.0;
    LTS5_LOG_DEBUG("\tfit time : " << dt << " ms");
    if (!err) {
      // Update normal
      face_mesh_->ComputeVertexNormal();
      // Texture mapping
      // -----------------------------------------------------------------------
      t1 = cv::getTickCount();
      for (int v = 0; v < stream_params_->n_view; ++v) {
        projection_mat_[v] = model_fitter_->get_projection_matrix(v);
      }
      err = texture_mapper_->Process(BaseStream::Buffer(0),
                                     projection_mat_,
                                     process_configuration.landmark,
                                     face_mesh_);
      t2 = cv::getTickCount() - t1;
      dt = (t2/cv::getTickFrequency())*1000.0;
      LTS5_LOG_DEBUG("\ttex time : " << dt << " ms");
    }
  }
  return err;
}

/*
 *  @name ResetTrackingState
 *  @fn void ResetTrackingState(void)
 *  @brief  Reinitialize tracking state
 */
void FaceSynthesisPipeline::ResetTrackingState(void) {
  model_fitter_->ResetTrackingState();
}

/*
 * @name  TrackFrontalImage
 * @fn    int TrackFrontalImage(cv::Mat* shape)
 * @brief Detect landmark on synthetic frontal image
 * @param[out] shape  Detected landmarks
 * @return    -1 if error, 0 otherwise
 */
int FaceSynthesisPipeline::TrackFrontalImage(cv::Mat* shape) {
  int err = -1;
  /*// Get 3d shape
  cv::Mat recon = model_fitter_->get_raw_3d_surface();
  // Get landmark list
  const std::vector<int>& vertex_list = model_fitter_->get_vertex_index();
  // Sanity check
  if (!recon.empty()) {
    // Get transformation
    const cv::Mat& proj = mesh_exporter_->get_projection_transform();
    const cv::Mat& mode_view = mesh_exporter_->get_model_view_transform();
    cv::Mat trsfrm = proj * mode_view;
    // Get ptr
    const auto* surf_ptr = reinterpret_cast<const LTS5::Vector3<float>*>(recon.data);
    const float* trsfrm_ptr = reinterpret_cast<const float*>(trsfrm.data);
    float width = mesh_exporter_->get_window_width();
    float half_w = width / 2.f;
    float half_h = mesh_exporter_->get_window_height() /2.f;
    // Loop over vertex list
    shape->create(static_cast<int>(2 * vertex_list.size()), 1, CV_64FC1);
    double* dest = reinterpret_cast<double*>(shape->data);
    int sz = static_cast<int>(vertex_list.size());
    LTS5::Vector4<float> p;
    for (int i = 0; i < sz; ++i) {
      // Get 3D point
      const LTS5::Vector3<float>& vert = surf_ptr[vertex_list[i]];
      // Apply transform
      p.x_ = (trsfrm_ptr[0] * vert.x_ +
              trsfrm_ptr[1] * vert.y_ +
              trsfrm_ptr[2] * vert.z_ +
              trsfrm_ptr[3]);
      p.y_ = (trsfrm_ptr[4] * vert.x_ +
              trsfrm_ptr[5] * vert.y_ +
              trsfrm_ptr[6] * vert.z_ +
              trsfrm_ptr[7]);
      p.z_ = (trsfrm_ptr[8] * vert.x_ +
              trsfrm_ptr[9] * vert.y_ +
              trsfrm_ptr[10] * vert.z_ +
              trsfrm_ptr[11]);
      p.w_ = (trsfrm_ptr[12] * vert.x_ +
              trsfrm_ptr[13] * vert.y_ +
              trsfrm_ptr[14] * vert.z_ +
              trsfrm_ptr[15]);
      // Clip
      p /= p.w_;
      // Transform to view port,
      // Should be in the form of {x0, x1, ..., y0, y1,...} to match SDM shape
      // definition
      p.x_ = width - (p.x_ * half_w + half_w);
      p.y_ = p.y_ * half_h + half_h;
      dest[i] = static_cast<double>(p.x_);
      dest[i + sz] = static_cast<double>(p.y_);
    }
  }*/
  return err;
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name   set_export_folder
 *  @fn void set_export_folder(const std::string& folder)
 *  @brief  Define where file are exported
 *  @param[in]  folder     Output location
 */
/*void FaceSynthesisPipeline::set_export_folder(const std::string& folder) {
  output_folder_ = folder;
  if (output_folder_.at(output_folder_.length() - 1) != '/') {
    output_folder_ += "/";
  }
  output_folder_ = working_directory_ + output_folder_;
}*/

/*
 *  @name   get_total_number_of_frame
 *  @brief  Gives the number of image to process
 *  @return Number of frame
 */
int FaceSynthesisPipeline::get_total_number_of_frame(void) const {
  int frame = 0;
  // Check first with input module
  // If -1 indicate that no images are provided and only 3D pts
  frame = frame_capture_->get_n_max_frame();
  if (frame == -1) {
    // Check with face tracker -> annotation
    frame = face_extractor_->get_number_max_frame();
  }
  return frame;
}
} // namespace LTS5
