/**
 *  @file   wiberg_forward_fit.cpp
 *  @brief  Wiberg Forward fit (Projecting out texture parameters)
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   21/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include "lts5/face_reconstruction/wiberg_forward_fit.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  WibergFit
 * @fn    WibergFit(const MorphableModel<T>* model)
 * @brief Constructor
 * @param[in] model       Model to fit
 */
template<typename T>
WibergFit<T>::WibergFit(const MorphableModel<T>* model) :
        MMFitAlgorithm<T>::MMFitAlgorithm(model) {
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Process
 * @fn    int Process(const cv::Mat& image,
                      const cv::Mat& landmarks,
                      const cv::Mat& shape_p,
                      const cv::Mat& tex_p,
                      const cv::Mat& illu_p,
                      const Regularization& reg,
                      Mesh* mesh,
                      Camera_t* camera,
                      Stat* stat)
 * @brief Estimate the model parameters
 * @param[in] image       Image to fit
 * @param[in] landmarks   Facial landmarks (optional)
 * @param[in] shape_p     Initial shape parameters, can be left empty
 * @param[in] tex_p       Initial texture parameters, can be left empty
 * @param[in] illu_p      Initial illumination parameters, can be left empty
 * @param[in] reg         Regularization factor
 * @param[in, out] mesh   Initial mesh (can be meanshape / texture)
 * @param[in, out] camera Initial camera estimation
 * @param[out] stat       Algorithm statistics (can be nullptr)
 * @return    -1 if not converged, 0 otherwise
 */
template<typename T>
int WibergFit<T>::Process(const cv::Mat& image,
                          const cv::Mat& landmarks,
                          const cv::Mat& shape_p,
                          const cv::Mat& tex_p,
                          const cv::Mat& illu_p,
                          const Regularization& reg,
                          Mesh* mesh,
                          Camera_t* camera,
                          Stat* stat) {
  using LA = LinearAlgebra<T>;
  using TType = typename LinearAlgebra<T>::TransposeType;
  // Get current estimation of the camera
  camera->ToVector(cam_p_);
  // Estimate shape + texture parameters
  if (shape_p.empty()) {
    this->model_->Project(*mesh,
                          cv::Mat(),
                          InstanceType::kShape,
                          false,
                          &shape_p_);
  } else {
    shape_p_ = shape_p;
  }
  if (tex_p.empty()) {
    this->model_->Project(*mesh,
                          cv::Mat(),
                          InstanceType::kTex,
                          false,
                          &tex_p_);
  } else {
    tex_p_ = tex_p;
  }
  if (illu_p.empty()) {
    // Initialise illumination coefficients
    int b = this->model_->get_active_illumination_component() /
            this->get_n_channels();
    illu_p_.create(this->get_n_channels(), b, cv::DataType<T>::type);
    illu_p_ = T(0.0);
    illu_p_.at<T>(0, 0) = T(1.0);
    illu_p_.at<T>(1, 0) = T(1.0);
    illu_p_.at<T>(2, 0) = T(1.0);
  } else {
    illu_p_ = illu_p;
  }
  // Init structure if needed
  if (stat != nullptr) {
    stat->Clear();
  }
  // Init loop
  int iter = 0;
  int status = -1;
  T eps = std::numeric_limits<T>::max();
  T cost_data = T(0.0), cost_shape_prior = T(0.0);
  T cost_landmark = T(0.0);
  cv::Mat s_shape;    // Sampled shape from model
  cv::Mat s_normal;   // Sampled normal
  cv::Mat s_tex;      // Sampled texture
  cv::Mat s_mean_tex; // Sampled mean texture
  cv::Mat s_img;      // Sampled image
  cv::Mat s_grad;     // Sampled grad X/Y [CN x 2]
  cv::Mat s_shape_pc; // Sampled PC from shape model
  cv::Mat s_tex_pc;   // Sampled PC from tex model
  cv::Mat data_err;   // Data error term
  cv::Mat lms_err;    // Landmark error term
  cv::Mat j_data;     // Jacobian for data term
  cv::Mat j_lms;      // Jacobian for landmark term
  cv::Mat hessian;    // Hessian
  cv::Mat sd_error;   // Jacobian * error
  cv::Mat sd_shape;   // Jacobian * error
  cv::Mat sd_tex;     // Jacobian * error
  cv::Mat sd_lms;     // Jacobian * error
  cv::Mat delta_s;    // Shape update
  cv::Mat delta_t;    // Texture update
  cv::Mat delta_c;    // Camera update
  cv::Mat delta_i;    // Illumination update
  cv::Mat landmark3d; // Current estimation of landmarks (3D)
  while (iter < this->max_iter_ && eps > this->tol_) {
    // Generate instance with parameters estimated earlier
    const auto& m_view = camera->get_view_transform();
    auto m_normal = m_view.Inverse().Transpose();
    this->model_->Generate(shape_p_, tex_p_, false, mesh);
    this->model_->AddIllumination(m_normal, illu_p_ ,mesh);
    // TODO: (Christophe) do we need to clamp ?
    auto& color = mesh->get_vertex_color();
    for (size_t k = 0; k < color.size(); ++k) {
      auto& c = color[k];
      c.r_ = c.r_ < T(0.0) ? T(0.0) : c.r_ > T(1.0) ? T(1.0) : c.r_;
      c.g_ = c.g_ < T(0.0) ? T(0.0) : c.g_ > T(1.0) ? T(1.0) : c.g_;
      c.b_ = c.b_ < T(0.0) ? T(0.0) : c.b_ > T(1.0) ? T(1.0) : c.b_;
    }
    // Sample visible points
    T curr_cost = T(0.0);
    status = this->SampleVisiblePoint(*mesh, *camera, &visible_pts_);
    if (status == 0) {
      // Sample shape model (Instance + variation + normal)
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kShape,
                           &s_shape);
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kShapeVariation,
                           &s_shape_pc);
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kShapeNormal,
                           &s_normal);
      // Sample texture model (Instance + variation + mean)
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kTex, &s_tex);
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kTexVariation, &s_tex_pc);
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kMeanTex,
                           &s_mean_tex);
      // Sample Image terms (Color + grad X/Y)
      this->SampleImage(image, visible_pts_, false, &s_img);
      this->SampleImage(image, visible_pts_, true, &s_grad);
      // Compute error, use mean texture according to Booth et al.
      data_err = s_img.clone();
      LA::Axpy(s_mean_tex, T(-1.0), &data_err);
      // Compute Jacobian, SD and Hessian of data term
      if (reg.data > T(0.0)) {
        this->JacobianData(*camera,
                           s_shape,
                           s_shape_pc,
                           s_mean_tex,
                           s_tex_pc,
                           s_grad,
                           s_normal,
                           &j_data);
        // Update hessian + sd_error
        LA::Gemm(j_data, TType::kTranspose, reg.data,
                 j_data, TType::kNoTranspose, T(0.0),
                 &hessian);
        LA::Gemv(j_data, TType::kTranspose, reg.data, data_err, T(0.0),
                 &sd_error);
        // Add cost contribution
        cost_data = LA::L2Norm(data_err);
        cost_data *= cost_data;
        cost_data /= T(this->n_sample_);
        curr_cost += reg.data * cost_data;
      } else {
        // Init matrix
        const int nc = camera->get_n_parameter();
        const int ns = this->model_->get_active_shape_component();
        const int ni = this->model_->get_active_illumination_component();
        hessian.create(ns + nc + ni,
                       ns + nc + ni,
                       cv::DataType<T>::type);
        sd_error.create(ns + nc + ni, 1, cv::DataType<T>::type);
        hessian.setTo(T(0.0));
        sd_error.setTo(T(0.0));
      }
      // Compute Jacobian, update SD and Hessian wrt shape prior
      if (reg.shape > T(0.0)) {
        const cv::Mat& inv_p = this->model_->get_shape_inverse_prior();
        sd_shape = reg.shape * inv_p;
        int n = this->model_->get_active_shape_component();
        cost_shape_prior  = T(0.0);
        for (int i = 0; i < n; ++i) {
          hessian.at<T>(i, i) += sd_shape.at<T>(i);
          sd_error.at<T>(i) += sd_shape.at<T>(i) * shape_p_.at<T>(i);
          const T de = inv_p.at<T>(i) * shape_p_.at<T>(i);
          cost_shape_prior += de * de;
        }
        // Add cost contribution
        curr_cost += reg.shape * cost_shape_prior;
      }
      // Compute Jacobian, update SD and Hessian wrt landmarks prior
      if (reg.landmark > T(0.0)) {
        // Project landmarks
        int n = std::max(landmarks.rows, landmarks.cols) / 2;
        landmark3d.create(3 * n, 1, cv::DataType<T>::type);
        lms_err.create(2 * n, 1, cv::DataType<T>::type);
        const auto &vertex = mesh->get_vertex();
        auto *landmark3d_ptr = reinterpret_cast<Vertex *>(landmark3d.data);
        Vector2<T> vp;
        cv::Mat canvas_lms = image.clone();
        for (int i = 0; i < n; ++i) {
          // Retrieve vertex index + value
          const int idx = this->lms_index_[i];
          const auto& v = vertex[idx];
          // Update current landmarks with current reconstruction
          landmark3d_ptr[i] = v;
          // Project + compute error
          camera->operator()(v, &vp);
          int k = 2 * i;
          lms_err.at<T>(k) = vp.x_ - landmarks.at<T>(k);
          lms_err.at<T>(k + 1) = vp.y_ - landmarks.at<T>(k + 1);


          cv::circle(canvas_lms,
                     cv::Point(vp.x_, vp.y_),
                     1,
                     CV_RGB(255,0,0),
                     -1);
          cv::circle(canvas_lms,
                     cv::Point(landmarks.at<T>(k),
                               landmarks.at<T>(k+1)),
                     1,
                     CV_RGB(0,255,0),
                     -1);
        }

        cv::imshow("Jlms", canvas_lms);
        cv::waitKey(30);


        // Compute Jacobian + Hessian + err
        this->JacobianLandmark(*camera,
                               landmark3d,
                               this->shape_pc_lms_,
                               &j_lms);
        LA::Gemm(j_lms, TType::kTranspose, reg.landmark,
                 j_lms, TType::kNoTranspose, T(1.0),
                 &hessian);
        LA::Gemv(j_lms, TType::kTranspose, reg.landmark,
                 lms_err, T(1.0), &sd_error);
        // Add cost contribution
        cost_landmark = LA::L2Norm(lms_err);
        cost_landmark *= cost_landmark;
        cost_landmark /= T(n);
        curr_cost += reg.landmark * cost_landmark;
      }
      // Solve system in order to find increment
      status = this->ComputeUpdate(hessian,
                                   sd_error,
                                   reg,
                                   &delta_s,
                                   &delta_c,
                                   &delta_i);
      if (status == 0) {
        // Update parameters (shape, tex, cam)
        if (delta_s.rows != 0) {
          shape_p_ += delta_s;
        }
        if (delta_c.rows != 0) {
          this->UpdateCamera(delta_c, camera);
        }
        if (delta_i.rows != 0) {
          illu_p_ += delta_i;
        }
        // Recover texture parameters using closed form solution
        // lambda = Ut^T ( F + Jp,c * Dp,c





        // Update cost
        eps = curr_cost != T(0.0) ? curr_cost : eps;
        // Fill stat if needed
        if (stat != nullptr) {
          // Add stuff here
          stat->cost.push_back(eps);
          stat->data.push_back(cost_data);
          stat->shape_prior.push_back(cost_shape_prior);
          stat->landmarks.push_back(cost_landmark);
        }
      } else {
        break;
      }
    } else {
      LTS5_LOG_ERROR("Can not update renderer !!!");
    }
    // Increase counter
    iter += 1;
  }
  // Generate final result with parameters previously estimated
  this->model_->Generate(shape_p_, tex_p_, false, mesh);
  auto& color = mesh->get_vertex_color();
  for (size_t k = 0; k < color.size(); ++k) {
    auto& c = color[k];
    c.r_ = c.r_ < T(0.0) ? T(0.0) : c.r_ > T(1.0) ? T(1.0) : c.r_;
    c.g_ = c.g_ < T(0.0) ? T(0.0) : c.g_ > T(1.0) ? T(1.0) : c.g_;
    c.b_ = c.b_ < T(0.0) ? T(0.0) : c.b_ > T(1.0) ? T(1.0) : c.b_;
  }
  int err = status == -1 ?
            -2 :
            iter < this->max_iter_ && eps < this->tol_ ?
            0 :
            -1;
  return err;
}

#pragma mark -
#pragma mark Private

/*
 * @name  JacobianData
 * @fn    void JacobianData(const Camera_t& camera,
                  const cv::Mat& shape,
                  const cv::Mat& shape_pc,
                  const cv::Mat& mean_tex,
                  const cv::Mat& tex_pc,
                  const cv::Mat& grad,
                  const cv::Mat& normal,
                  const T reg,
                  cv::Mat* j_data)
 * @brief Compute Jacobian for data term
 * @param[in] camera      Current camera estimation
 * @param[in] shape       Sampled shape   [3N x 1]
 * @param[in] shape_pc    Sampled shape principal component [3N x ns]
 * @param[in] mean_tex    Sampled mean texutre [3N x 1]
 * @param[in] tex_pc      Sampled texture principal component [3N x nt]
 * @param[in] grad        Sampled X/Y gradient  [CN x 2]
 * @param[in] normal      Sampled surface's normals [N x 3]
 * @param[out] j_data     Jacobian for data term  [CN x (ns + nc + nt)]
 */
template<typename T>
void WibergFit<T>::JacobianData(const Camera_t& camera,
                                const cv::Mat& shape,
                                const cv::Mat& shape_pc,
                                const cv::Mat& mean_tex,
                                const cv::Mat& tex_pc,
                                const cv::Mat& grad,
                                const cv::Mat& normal,
                                cv::Mat* j_data) {

}

/*
 * @name  ProjectOut
 * @fn    void ProjectOut(const cv::Mat& hessian, const cv::Mat& tex_pc,
                          cv::Mat* phessian)
 * @brief Project-out texture subspace
 * @param[in] hessian
 * @param[in] tex_pc
 * @param[out] phessian
 */
template<typename T>
void WibergFit<T>::ProjectOut(const cv::Mat& hessian,
                              const cv::Mat& tex_pc,
                              cv::Mat* phessian) {

}

/*
 * @name  ComputeUpdate
 * @fn    int ComputeUpdate(const cv::Mat& hessian,
                           const cv::Mat& error,
                           const Regularization& reg,
                           cv::Mat* delta_s,
                           cv::Mat* delta_c,
                           cv::Mat* delta_i)
 * @brief Compute coefficients update given the \p hessian and \p error.
 * @param[in] hessian     Current Hessian estimation
 * @param[in] error       Current SD error estimation
 * @param[in] reg         Regularization factors
 * @param[out] delta_s    Shape update
 * @param[out] delta_c    Camera update
 * @param[out] delta_i    Illumination update
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int WibergFit<T>::ComputeUpdate(const cv::Mat& hessian,
                                const cv::Mat& error,
                                const Regularization& reg,
                                cv::Mat* delta_s,
                                cv::Mat* delta_c,
                                cv::Mat* delta_i) {
  return -1;
}

/*
 * @name  UpdateCamera
 * @fn    void UpdateCamera(const cv::Mat& delta_c, Camera_t* camera)
 * @brief Update current camera estimation
 * @param delta_c Camera update
 * @param camera  Current camera estimation to update
 */
template<typename T>
void WibergFit<T>::UpdateCamera(const cv::Mat& delta_c, Camera_t* camera) {

}


#pragma mark -
#pragma mark Proxy

/*
 * @name  Create
 * @fn    MMFitAlgorithm<T>* Create(const MorphableModel<T>* model) const
 * @brief Instantiate a specific instance for a given proxy
 * @param[in] model   Morphable model to use for fitting
 * @return    Fitting algorithm
 */
template<typename T>
MMFitAlgorithm<T>*
WibergFitProxy<T>::Create(const MorphableModel<T>* model) const {
  return new WibergFit<T>(model);
}

/*
 * @name  Name
 * @fn    const char* Name(void) const
 * @brief Indicate the name of that proxy
 * @return    Fit algorithm's name
 */
template<typename T>
const char* WibergFitProxy<T>::Name(void) const {
  return "WibergForward";
}

/** Explicit registration */
WibergFitProxy<float> wiberg_fit_proxy_f;
WibergFitProxy<double> wiberg_fit_proxy_d;

#pragma mark -
#pragma mark Explicit Instantiation

/** Float - Wiberg Fit */
template class WibergFit<float>;
/** Double - Wiberg Fit */
template class WibergFit<double>;

}  // namepsace LTS5
