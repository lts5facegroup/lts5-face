/**
 *  @file   image_stream.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   01/06/16
 *  Copyright © 2016 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <iostream>
#include <thread>

#include "opencv2/core/core.hpp"
#include "opencv2/imgcodecs.hpp"
#ifdef HAS_CUDA_
#include "opencv2/cudaimgproc.hpp"
#else
#include "opencv2/imgproc.hpp"
#endif

#include "lts5/face_reconstruction/image_stream.hpp"
#include "lts5/face_reconstruction/utilities.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/sys/parallel.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name       ImageStream
 *  @fn         ImageStream(const ImageStream::Parameter& params)
 *  @brief      Constructor
 *  @param[in]  params  Stream parameters
 */
ImageStream::ImageStream(const ImageStream::Parameter& params) :
        BaseStream::BaseStream(params),
        params_(params),
        n_frame_loaded_(0),
        n_max_frame_(-1),
        valid_index_(0) {
  // Scan folder
  size_t img_per_folder = std::numeric_limits<size_t>::max();
  list_image_.resize(params_.n_view, std::vector<std::string>());
  for (int i = 0; i < params_.n_view; ++i) {
    // Scan folder
    LTS5::GetFileListing(list_image_[i],
                         params_.root_folder,
                         params_.image_folder[i],
                         params_.image_ext);
    // Get amount of images in folder
    img_per_folder = std::min(img_per_folder, list_image_[i].size());
    // Sort by number only the part relative to the scanned folder
    std::sort(list_image_[i].begin(), list_image_[i].end());
  }
  // Check if there enough images
  if (img_per_folder != 0) {
    // Number of images is a multiple of _numberViews
    n_max_frame_ = static_cast<int>(img_per_folder);
    // Get image dimension
    for (size_t i = 0; i < list_image_.size(); ++i) {
      if (!list_image_[i].empty()) {
        valid_index_ = i;
        cv::Mat frame = cv::imread(list_image_[i][0]);
        image_width_ = frame.cols;
        image_height_ = frame.rows;
        break;
      }
    }
  } else {
    std::string msg = "No images founded in folder : " + params_.root_folder;
    throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErr,
                             msg,
                             FUNC_NAME);
  }
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Read
 *  @fn int Read()
 *  @brief  Read image from the stream
 *  @return -1 if error, 0 otherwise
 */
int ImageStream::Read() {
  int err = -1;
  if ((n_frame_loaded_ < n_max_frame_) || (n_max_frame_ != -1)) {
    static std::vector<cv::cuda::GpuMat> buffer(static_cast<size_t>(params_.n_view));
    // Load each view
    Parallel::For(params_.n_view,
                  [&](const size_t& i){
                    const std::string& name = list_image_[i][n_frame_loaded_];
                    cv::Mat frame = cv::imread(name, cv::IMREAD_UNCHANGED);
                    if (frame.channels() != 1) {
#ifdef HAS_CUDA_
                      buffer[i].upload(frame);
                      cv::cuda::cvtColor(buffer[i],
                                         buffer_.device[i],
                                         cv::COLOR_BGR2GRAY);
                      buffer_.device[i].download(buffer_.host[i]);
#else
                      cv::cvtColor(frame, buffer_.host[i], cv::COLOR_BGR2GRAY);
#endif
                    } else {
                      buffer_.host[i] = frame.clone();
                    }
                    // Push to device if needed
#ifdef HAS_CUDA_
                    buffer_.Upload(i);
#endif
                  });
    // Inc
    if (!buffer_.host[0].empty()) {
      err = 0;
      n_frame_loaded_++;
    }
  }
  return err;
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name get_frame_name
 *  @fn void get_frame_name(std::string* frame_name, std::string* frame_ext) const
 *  @brief  Provide the name of the current frame according to the media
 *  @param[out] frame_name  Frame name
 *  @param[out] frame_ext   Frame extension
 */
void ImageStream::get_frame_name(std::string* frame_name,
                                 std::string* frame_ext) const {
  const std::string& img_name = list_image_[valid_index_][n_frame_loaded_ - 1];
  size_t p = img_name.rfind('.');
  if (p != std::string::npos) {
    *frame_ext = img_name.substr(p, img_name.length() - p);
    size_t ps = img_name.rfind('/');
    if (ps == std::string::npos) {
      ps = params_.root_folder.length();
    }
    *frame_name = img_name.substr(ps,
                                  p - ps);
  } else {
    std::stringstream sstream;
    sstream << std::this_thread::get_id();
    *frame_name = sstream.str() + "_" + img_name;
    *frame_ext = ".jpg";
  }
}

}  // namespace LTS5
