/**
 *  @file   video_stream.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   01/06/16
 *  Copyright © 2016 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <algorithm>
#include <thread>

#ifdef HAS_CUDA_
#include "opencv2/cudaimgproc.hpp"
#else
#include "opencv2/imgproc.hpp"
#endif

#include "lts5/face_reconstruction/video_stream.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/sys/parallel.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name       VideoStream
 *  @fn         VideoStream(const VideoStream::Parameter& params)
 *  @brief      Constructor
 *  @param[in]  params  Stream parameters
 */
VideoStream::VideoStream(const VideoStream::Parameter& params) :
BaseStream::BaseStream(params),
params_(params),
n_frame_loaded_(0),
n_max_frame_(-1) {
  // Scan folder for video
  LTS5::GetFileListing(list_video_,
                       params_.root_folder,
                       "",
                       params_.video_ext);
  // Something detected ?
  if (list_video_.empty()) {
    // Something went wrong
    throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErr,
                             "Unable to locate file to process",
                             FUNC_NAME);
  }
  // Check if there enough video
  if ((static_cast<int>(list_video_.size()) % params_.n_view) == 0) {
    std::vector<int> v_length = std::vector<int>(params_.n_view, 0);
    caps_.resize(params_.n_view);
    for (int i = 0; i < params_.n_view; ++i) {
      caps_[i].open(list_video_[i]);
      if (caps_[i].isOpened()) {
        // Get properties
        v_length[i] = static_cast<int>(caps_[i].get(cv::CAP_PROP_FRAME_COUNT));
        image_height_ = static_cast<int>(caps_[i].get(cv::CAP_PROP_FRAME_HEIGHT));
        image_width_ = static_cast<int>(caps_[i].get(cv::CAP_PROP_FRAME_WIDTH));
      }
    }
    // Get max length
    n_max_frame_ = *std::min_element(v_length.begin(), v_length.end());
    if (n_max_frame_ == 0) {
      n_max_frame_ = std::numeric_limits<int>::max();
    }
  } else {
    throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErr,
                             "Number of videos doesn't match for each views !",
                             FUNC_NAME);
  }
}

/*
 *  @name       ~VideoStream
 *  @fn         ~VideoStream()
 *  @brief      Destructor
 */
VideoStream::~VideoStream() {
  // Release capture device
  for (auto& cap : caps_) {
    if (cap.isOpened()) {
      cap.release();
    }
  }
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Read
 *  @fn int Read()
 *  @brief  Read image from the stream
 *  @return -1 if error, 0 otherwise
 */
int VideoStream::Read() {
  int err = -1;
  if (n_frame_loaded_ < n_max_frame_) {
    static std::vector<cv::cuda::GpuMat> buffer(caps_.size());
    // Loop over cap device
    Parallel::For(params_.n_view,
                  [&](const size_t& i) {
                    // Grab
                    if (caps_[i].grab()) {
                      // Retrieve
                      cv::Mat frame;
                      caps_[i].retrieve(frame);
                      // Convert into grey scale
                      if (frame.channels() != 1) {
#ifdef HAS_CUDA_
                        buffer[i].upload(frame);
                        cv::cuda::cvtColor(buffer[i],
                                           buffer_.device[i],
                                           cv::COLOR_BGR2GRAY);
                        buffer_.device[i].download(buffer_.host[i]);
#else
                        cv::cvtColor(frame, buffer_.host[i],
                                     cv::COLOR_BGR2GRAY);
#endif
                      } else {
                        buffer_.host[i] = frame.clone();
                      }
                    } else {
                      buffer_.host[i].release();
                    }
    });
    // Inc
    if (!buffer_.host[0].empty()) {
      err = 0;
      n_frame_loaded_++;
    }
  }
  return err;
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name get_frame_name
 *  @fn void get_frame_name(std::string* frame_name, std::string* frame_ext) const
 *  @brief  Provide the name of the current frame according to the media
 *  @param[out] frame_name  Frame name
 *  @param[out] frame_ext   Frame extension
 */
void VideoStream::get_frame_name(std::string* frame_name,
                                 std::string* frame_ext) const {
  std::string num = std::to_string(n_frame_loaded_ - 1);
  std::stringstream sstream;
  sstream << std::this_thread::get_id();
  *frame_name = sstream.str() + "_frame_" + std::string(6 - num.length(), '0') + num;
  *frame_ext = ".jpg";
}

}  // namespace LTS5
