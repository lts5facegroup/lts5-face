/**
 *  @file   mesh2obj_exporter.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 30/07/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include "lts5/face_reconstruction/mesh2obj_exporter.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   Mesh2OBJExporter
 *  @brief  Constructor
 */
Mesh2OBJExporter::Mesh2OBJExporter(void) : mesh_(nullptr), output_name_("") {
}

/*
 *  @name   ~Mesh2OBJExporter
 *  @brief  Destructor
 */
Mesh2OBJExporter::~Mesh2OBJExporter(void) {
}

/*
 *  @name   set_mesh
 *  @fn void set_mesh(const vtkPolyData* mesh)
 *  @brief  Link the exporter to a given mesh.
 *  @param[in]  mesh   Face mesh to export to .obj file
 */
void Mesh2OBJExporter::set_mesh(const LTS5::Mesh<float>& mesh) {
  mesh_ = const_cast<LTS5::Mesh<float>*>(&mesh);
  this->is_initialized_ = mesh_ != nullptr;
}

/*
 *  @name   Process
 *  @fn void Process(const std::string& output_name)
 *  @brief  Export mesh into obj file
 *  @param[in]  output_name   Output filename
 */
void Mesh2OBJExporter::Process(void) {
  // Define output name
  if (mesh_) {
    mesh_->Save(output_name_);
  }
}
}
