/**
 *  @file   pointgrey_stream.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   01/06/16
 *  Copyright © 2016 Ecabert Christophe. All rights reserved.
 */

#include <thread>

#include "lts5/face_reconstruction/pointgrey_stream.hpp"
#include "lts5/utils/process_error.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name       PointGreyStream
 *  @fn         PointGreyStream(const PointGreyStream::Parameter& params)
 *  @brief      Constructor
 *  @param[in]  params  Stream parameters
 */
PointGreyStream::PointGreyStream(const PointGreyStream::Parameter& params) :
BaseStream::BaseStream(params),
params_(params),
n_frame_loaded_(0),
n_max_frame_(-2) {
#ifdef HAS_POINTGREY_
  // Create rig
  caps_ = new LTS5::PGCameraRig();
  int err = caps_->Init(params_.n_view, params_.external_trigger);
  if (err) {
    throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErr,
                             "Can not initialize PointGrey camera system !",
                             FUNC_NAME);
  }
  if(!caps_->Start()) {
    while(caps_->Read(false, &buffer_.host));
    image_height_ = buffer_.host[0].rows;
    image_width_ = buffer_.host[0].cols;
  }
#else
  throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErr,
                             "Unsupported stream !",
                             FUNC_NAME);
#endif
}

/*
 *  @name       ~PointGreyStream
 *  @fn         ~PointGreyStream(void)
 *  @brief      Destructor
 */
PointGreyStream::~PointGreyStream(void) {
#ifdef HAS_POINTGREY_
  if (caps_) {
    caps_->Stop();
    delete caps_;
    caps_ = nullptr;
  }
#endif
}

#pragma mark -
#pragma mark Usage

/**
   *  @name   Read
   *  @fn virtual int Read(void) = 0
   *  @brief  Read image from the stream
   *  @return -1 if error, 0 otherwise
   */
int PointGreyStream::Read(void) {
  int err = -1;
#ifdef HAS_POINTGREY_
  caps_->Read(true, &buffer_.host);
  if (!buffer_.host[0].empty()) {
    // Push if needed
    #ifdef HAS_CUDA_
    buffer_.Upload();
    #endif
    n_frame_loaded_++;
    err = 0;
  }
#endif
  return err;
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name get_frame_name
 *  @fn void get_frame_name(std::string* frame_name, std::string* frame_ext) const
 *  @brief  Provide the name of the current frame according to the media
 *  @param[out] frame_name  Frame name
 *  @param[out] frame_ext   Frame extension
 */
void PointGreyStream::get_frame_name(std::string* frame_name,
                                     std::string* frame_ext) const {
  std::string num = std::to_string(n_frame_loaded_ - 1);
  std::stringstream sstream;
  sstream << std::this_thread::get_id();
  *frame_name = sstream.str() + "_frame_" + std::string(6 - num.length(), '0') + num;
  *frame_ext = ".jpg";
}


}  // namespace LTS5
