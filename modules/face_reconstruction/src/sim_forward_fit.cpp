/**
 *  @file   sim_forward_fit.cpp
 *  @brief  Simultaneous Forward fit (Solve for shape and texture parameters at
 *          the same time)
 *  @ingroup    face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   19/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <limits>
#include <algorithm>

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/face_reconstruction/sim_forward_fit.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/constant.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/geometry/spherical_harmonic.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  SimultaneousFit
 * @fn    SimultaneousFit(const MorphableModel<T>* model,
                          const size_t n_sample,
                          const T tol)
 * @brief Constructor
 * @param[in] model       Model to fit
 */
template<typename T>
SimultaneousFit<T>::SimultaneousFit(const MorphableModel<T>* model) :
        MMFitAlgorithm<T>::MMFitAlgorithm(model) {
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Process
 * @fn    int Process(const cv::Mat& image,
                      const cv::Mat& landmarks,
                      const cv::Mat& shape_p,
                      const cv::Mat& tex_p,
                      const cv::Mat& illu_p,
                      const Regularization& reg,
                      Mesh* mesh,
                      Camera_t* camera,
                      Stat* stat)
 * @brief Estimate the model parameters
 * @param[in] image       Image to fit
 * @param[in] landmarks   Facial landmarks (optional)
 * @param[in] shape_p     Initial shape parameters, can be left empty
 * @param[in] tex_p       Initial texture parameters, can be left empty
 * @param[in] illu_p      Initial texture parameters, can be left empty
 * @param[in] reg         Regularization factor
 * @param[in, out] mesh   Initial mesh (can be meanshape / texture)
 * @param[in, out] camera Initial camera estimation
 * @param[out] stat       Algorithm statistics (can be nullptr)
 * @return    -2 if numerical error appears, -1 if not converged, 0 otherwise
 */
template<typename T>
int SimultaneousFit<T>::Process(const cv::Mat& image,
                                const cv::Mat& landmarks,
                                const cv::Mat& shape_p,
                                const cv::Mat& tex_p,
                                const cv::Mat& illu_p,
                                const Regularization& reg,
                                Mesh* mesh,
                                Camera_t* camera,
                                Stat* stat) {
  using LA = LinearAlgebra<T>;
  using TType = typename LinearAlgebra<T>::TransposeType;
  // Get current estimation of the camera
  camera->ToVector(cam_p_);
  // Estimate shape + texture parameters
  if (shape_p.empty()) {
    this->model_->Project(*mesh,
                          cv::Mat(),
                          InstanceType::kShape,
                          false,
                          &this->shape_p_);
  } else {
    this->shape_p_ = shape_p;
  }
  if (tex_p.empty()) {
    this->model_->Project(*mesh,
                          cv::Mat(),
                          InstanceType::kTex,
                          false,
                          &this->tex_p_);
  } else {
    this->tex_p_ = tex_p;
  }
  if (illu_p.empty()) {
    // Initialise illumination coefficients
    int b = this->model_->get_active_illumination_component() /
            this->get_n_channels();
    this->illu_p_.create(this->get_n_channels(), b, cv::DataType<T>::type);
    this->illu_p_ = T(0.0);
    this->illu_p_.template at<T>(0, 0) = T(1.0);
    this->illu_p_.template at<T>(1, 0) = T(1.0);
    this->illu_p_.template at<T>(2, 0) = T(1.0);
  } else {
    this->illu_p_ = illu_p;
  }




  // Init structure if needed
  if (stat != nullptr) {
    stat->Clear();
  }
  // Init loop
  int iter = 0;
  int status = -1;
  T res = std::numeric_limits<T>::max();
  T old_res = std::numeric_limits<T>::min();
  T cost_data = T(0.0), cost_shape_prior = T(0.0);
  T cost_tex_prior = T(0.0), cost_landmark = T(0.0);
  cv::Mat s_shape;     // Sampled shape from model
  cv::Mat s_normal;    // Sampled normal
  cv::Mat s_tex;       // Sampled texture
  cv::Mat s_mean_tex;  // Sampled mean texture
  cv::Mat s_img;       // Sampled image
  cv::Mat s_grad;      // Sampled grad X/Y [CN x 2]
  cv::Mat s_shape_pc;  // Sampled PC from shape model
  cv::Mat s_tex_pc;    // Sampled PC from tex model
  cv::Mat data_err;    // Data error term
  cv::Mat lms_err;     // Landmark error term
  cv::Mat j_data;      // Jacobian for data term
  cv::Mat j_lms;       // Jacobian for landmark term
  cv::Mat hessian;     // Hessian
  cv::Mat sd_error;    // Jacobian * error
  cv::Mat sd_shape;    // Jacobian * error
  cv::Mat sd_tex;      // Jacobian * error
  cv::Mat sd_lms;      // Jacobian * error
  cv::Mat delta_s;     // Shape update
  cv::Mat delta_t;     // Texture update
  cv::Mat delta_c;     // Camera update
  cv::Mat delta_i;     // Illumination update
  cv::Mat landmark3d;  // Current estimation of landmarks (3D)
  while (iter < this->max_iter_ && std::abs(res - old_res) > this->tol_) {
    // Generate instance with parameters estimated earlier
    const auto& m_view = camera->get_view_transform();
    auto m_normal = m_view.Inverse().Transpose();
    this->model_->Generate(this->shape_p_, this->tex_p_, false, mesh);
    this->model_->AddIllumination(m_normal, this->illu_p_ , mesh);
    // TODO(Christophe): do we need to clamp ?
    auto& color = mesh->get_vertex_color();
    for (size_t k = 0; k < color.size(); ++k) {
      auto& c = color[k];
      c.r_ = c.r_ < T(0.0) ? T(0.0) : c.r_ > T(1.0) ? T(1.0) : c.r_;
      c.g_ = c.g_ < T(0.0) ? T(0.0) : c.g_ > T(1.0) ? T(1.0) : c.g_;
      c.b_ = c.b_ < T(0.0) ? T(0.0) : c.b_ > T(1.0) ? T(1.0) : c.b_;
    }
    // Sample visible points
    T curr_cost = T(0.0);
    status = this->SampleVisiblePoint(*mesh, *camera, &visible_pts_);
    if (status == 0) {
      cv::Mat canvas = image.clone();
      for (auto &v : visible_pts_) {
        cv::circle(canvas, cv::Point(v.x, v.y), 1, CV_RGB(0, 255, 0), -1);
      }
      cv::imshow("sampling", canvas);
      cv::waitKey(30);




      // Sample shape model (Instance + variation + normal)
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kShape,
                           &s_shape);
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kShapeVariation,
                           &s_shape_pc);
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kShapeNormal,
                           &s_normal);
      // Sample texture model (Instance + variation + mean)
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kTex, &s_tex);
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kTexVariation, &s_tex_pc);
      this->model_->Sample(*mesh,
                           cv::Mat(),
                           visible_pts_,
                           InstanceType::kMeanTex,
                           &s_mean_tex);
      // Sample Image terms (Color + grad X/Y)
      this->SampleImage(image, visible_pts_, false, &s_img);
      this->SampleImage(image, visible_pts_, true, &s_grad);
      // Compute error
      data_err = s_img.clone();
      LA::Axpy(s_tex, T(-1.0), &data_err);
      // Compute Jacobian, SD and Hessian of data term
      if (reg.data > T(0.0)) {
        this->JacobianData(*camera,
                           s_shape,
                           s_shape_pc,
                           s_mean_tex,
                           s_tex_pc,
                           s_grad,
                           s_normal,
                           &j_data);
        // Update hessian + sd_error
        LA::Gemm(j_data, TType::kTranspose, reg.data,
                 j_data, TType::kNoTranspose, T(0.0),
                 &hessian);
        LA::Gemv(j_data, TType::kTranspose, reg.data, data_err, T(0.0),
                 &sd_error);
        // Add cost contribution
        cost_data = LA::L2Norm(data_err);
        cost_data *= cost_data;
        cost_data /= T(this->n_sample_);
        curr_cost += reg.data * cost_data;
      } else {
        // Init matrix
        const int nc = camera->get_n_parameter();
        const int ns = this->model_->get_active_shape_component();
        const int nt = this->model_->get_active_texture_component();
        const int ni = this->model_->get_active_illumination_component();
        hessian.create(ns + nt + nc + ni,
                       ns + nt + nc + ni,
                       cv::DataType<T>::type);
        sd_error.create(ns + nt + nc + ni, 1, cv::DataType<T>::type);
        hessian.setTo(T(0.0));
        sd_error.setTo(T(0.0));
      }
      // Compute Jacobian, update SD and Hessian wrt shape prior
      if (reg.shape > T(0.0)) {
        const cv::Mat& inv_p = this->model_->get_shape_inverse_prior();
        sd_shape = reg.shape * inv_p;
        int n = this->model_->get_active_shape_component();
        cost_shape_prior  = T(0.0);
        for (int i = 0; i < n; ++i) {
          hessian.at<T>(i, i) += sd_shape.at<T>(i);
          sd_error.at<T>(i) += (sd_shape.at<T>(i) *
                                this->shape_p_.template at<T>(i));
          const T de = inv_p.at<T>(i) * this->shape_p_.template at<T>(i);
          cost_shape_prior += de * de;
        }
        // Add cost contribution
        curr_cost += reg.shape * cost_shape_prior;
      }
      // Compute Jacobian, update SD and Hessian wrt texture prior
      if (reg.tex > T(0.0)) {
        const cv::Mat& inv_p = this->model_->get_texture_inverse_prior();
        sd_tex = reg.tex * inv_p;
        const int nt = this->model_->get_active_texture_component();
        const int ns = this->model_->get_active_shape_component();
        const int nc = camera->get_n_parameter();
        int idx = ns + nc;
        cost_tex_prior = T(0.0);
        for (int i = 0; i < nt; ++i) {
          hessian.at<T>(idx + i, idx + i) += sd_tex.at<T>(i);
          sd_error.at<T>(idx + i) += (sd_tex.at<T>(i) *
                                      this->tex_p_.template at<T>(i));
          const T de = inv_p.at<T>(i) * this->tex_p_.template at<T>(i);
          cost_tex_prior += de * de;
        }
        // Add cost contribution
        curr_cost += reg.tex * cost_tex_prior;
      }
      // Compute Jacobian, update SD and Hessian wrt landmarks prior
      if (reg.landmark > T(0.0)) {
        // Project landmarks
        int n = std::max(landmarks.rows, landmarks.cols) / 2;
        landmark3d.create(3 * n, 1, cv::DataType<T>::type);
        lms_err.create(2 * n, 1, cv::DataType<T>::type);
        const auto &vertex = mesh->get_vertex();
        auto *landmark3d_ptr = reinterpret_cast<Vertex *>(landmark3d.data);
        Vector2<T> vp;
        cv::Mat canvas_lms = image.clone();
        for (int i = 0; i < n; ++i) {
          // Retrieve vertex index + value
          const int idx = this->lms_index_[i];
          const auto& v = vertex[idx];
          // Update current landmarks with current reconstruction
          landmark3d_ptr[i] = v;
          // Project + compute error
          camera->operator()(v, &vp);
          int k = 2 * i;
          lms_err.at<T>(k) = vp.x_ - landmarks.at<T>(k);
          lms_err.at<T>(k + 1) = vp.y_ - landmarks.at<T>(k + 1);


          cv::circle(canvas_lms,
                     cv::Point(vp.x_, vp.y_),
                     1,
                     CV_RGB(255, 0, 0),
                     -1);
          cv::circle(canvas_lms,
                     cv::Point(landmarks.at<T>(k),
                               landmarks.at<T>(k+1)),
                     1,
                     CV_RGB(0, 255, 0),
                     -1);
        }

        cv::imshow("Jlms", canvas_lms);
        cv::waitKey(30);


        // Compute Jacobian + Hessian + err
        this->JacobianLandmark(*camera,
                               landmark3d,
                               this->shape_pc_lms_,
                               &j_lms);
        LA::Gemm(j_lms, TType::kTranspose, reg.landmark,
                 j_lms, TType::kNoTranspose, T(1.0),
                 &hessian);
        LA::Gemv(j_lms, TType::kTranspose, reg.landmark,
                 lms_err, T(1.0), &sd_error);
        // Add cost contribution
        cost_landmark = LA::L2Norm(lms_err);
        cost_landmark *= cost_landmark;
        cost_landmark /= T(n);
        curr_cost += reg.landmark * cost_landmark;
      }
      // Solve system in order to find increment
      status = this->ComputeUpdate(hessian,
                                   sd_error,
                                   reg,
                                   &delta_s,
                                   &delta_t,
                                   &delta_c,
                                   &delta_i);
      if (status == 0) {
        // Update parameters (shape, tex, cam)
        if (delta_s.rows != 0) {
          this->shape_p_ += delta_s;
        }
        if (delta_t.rows != 0) {
          this->tex_p_ += delta_t;
        }
        if (delta_c.rows != 0) {
          this->UpdateCamera(delta_c, camera);
        }
        if (delta_i.rows != 0) {
          this->illu_p_ += delta_i;
        }
        // Update cost
        old_res = res;
        res = curr_cost != T(0.0) ? curr_cost : res;
        // Fill stat if needed
        if (stat != nullptr) {
          // Add stuff here
          stat->cost.push_back(res);
          stat->data.push_back(cost_data);
          stat->shape_prior.push_back(cost_shape_prior);
          stat->tex_prior.push_back(cost_tex_prior);
          stat->landmarks.push_back(cost_landmark);
        }
      } else {
        break;
      }
    } else {
      LTS5_LOG_ERROR("Can not update renderer !!!");
    }
    // Increase counter
    iter += 1;
  }


  // Generate final result
  this->model_->Generate(this->shape_p_, this->tex_p_, false, mesh);
  auto& color = mesh->get_vertex_color();
  for (size_t k = 0; k < color.size(); ++k) {
    auto& c = color[k];
    c.r_ = c.r_ < T(0.0) ? T(0.0) : c.r_ > T(1.0) ? T(1.0) : c.r_;
    c.g_ = c.g_ < T(0.0) ? T(0.0) : c.g_ > T(1.0) ? T(1.0) : c.g_;
    c.b_ = c.b_ < T(0.0) ? T(0.0) : c.b_ > T(1.0) ? T(1.0) : c.b_;
  }

  // Copy reflectance mesh for illumination illustration
  Mesh illum(*mesh);
  Mesh full(*mesh);
  Mesh shape(*mesh);



  const auto& view_t = camera->get_view_transform();
  const auto normal_t = view_t.Inverse().Transpose();

  auto& vertex = mesh->get_vertex();
  auto& normal = mesh->get_normal();
  shape.get_vertex_color().assign(vertex.size(), Vector3<T>(0.5, 0.5, 0.5));
  // Init illumination to gray
  illum.get_vertex_color().assign(vertex.size(), Vector3<T>(0.5, 0.5, 0.5));
  this->model_->AddIllumination(normal_t, this->illu_p_, &illum);
  this->model_->AddIllumination(normal_t, this->illu_p_, &full);
  auto& ivertex = illum.get_vertex();
  auto& inormal = illum.get_normal();
  auto& fvertex = full.get_vertex();
  auto& fnormal = full.get_normal();


  auto& fcolor = full.get_vertex_color();
  auto& icolor = illum.get_vertex_color();
  for (size_t k = 0; k < color.size(); ++k) {
    auto& c = fcolor[k];
    c.r_ = c.r_ < T(0.0) ? T(0.0) : c.r_ > T(1.0) ? T(1.0) : c.r_;
    c.g_ = c.g_ < T(0.0) ? T(0.0) : c.g_ > T(1.0) ? T(1.0) : c.g_;
    c.b_ = c.b_ < T(0.0) ? T(0.0) : c.b_ > T(1.0) ? T(1.0) : c.b_;
    auto& d = icolor[k];
    d.r_ = d.r_ < T(0.0) ? T(0.0) : d.r_ > T(1.0) ? T(1.0) : d.r_;
    d.g_ = d.g_ < T(0.0) ? T(0.0) : d.g_ > T(1.0) ? T(1.0) : d.g_;
    d.b_ = d.b_ < T(0.0) ? T(0.0) : d.b_ > T(1.0) ? T(1.0) : d.b_;
  }
  cv::Mat img;
  int e = this->RenderInstance(full, *camera, &img);
  if (e == 0) {
    // Blend the two
    for (int r = 0; r < image.rows; ++r) {
      for (int c = 0; c < image.cols; ++c) {
        cv::Vec3b& p = img.at<cv::Vec3b>(r, c);
        if (p[0] == 0 && p[1] == 0 && p[1] == 0) {
          const cv::Vec3b& src = image.at<cv::Vec3b>(r, c);
          p[0] = src[0];
          p[1] = src[1];
          p[2] = src[2];
        }
      }
    }
    cv::imwrite("debug/instance.png", img);
  }


  // Apply View transform for reflectance mesh
  /*for (size_t i = 0; i < vertex.size(); ++i) {
    auto& v = vertex[i];
    auto& n = normal[i];
    v = view_t * v;
    n = normal_t * n;

    ivertex[i] = view_t * ivertex[i];
    inormal[i] = normal_t * inormal[i];
    fvertex[i] = view_t * fvertex[i];
    fnormal[i] = normal_t * fnormal[i];
  }*/


  LTS5::Matrix3x3<T> rot;
  auto qt = Quaternion<T>({0.0, 1.0, 0.0}, 0.0 * Constants<T>::PI / 180.0);
  qt.ToRotationMatrix(&rot);
  cv::Mat trsfm = cv::Mat(4, 4, cv::DataType<T>::type);
  trsfm.at<T>(0, 0) = rot(0, 0);
  trsfm.at<T>(0, 1) = rot(0, 1);
  trsfm.at<T>(0, 2) = rot(0, 2);

  trsfm.at<T>(1, 0) = rot(1, 0);
  trsfm.at<T>(1, 1) = rot(1, 1);
  trsfm.at<T>(1, 2) = rot(1, 2);

  trsfm.at<T>(2, 0) = rot(2, 0);
  trsfm.at<T>(2, 1) = rot(2, 1);
  trsfm.at<T>(2, 2) = rot(2, 2);

  trsfm.at<T>(3, 0) = 0.0;
  trsfm.at<T>(3, 1) = 0.0;
  trsfm.at<T>(3, 2) = 0.0;

  trsfm.at<T>(0, 3) = -190.0;
  trsfm.at<T>(0, 3) = 0.0;
  trsfm.at<T>(1, 3) = 0.0;
  trsfm.at<T>(2, 3) = 0.0;
  trsfm.at<T>(3, 3) = 1.0;

  // Center
  mesh->Transform(trsfm);
  mesh->Save("debug/mesh_reflectance.ply");
  // Right
  // illum.Transform(trsfm);
  illum.Save("debug/mesh_illu.ply");

  // Left
  qt = Quaternion<T>({0.0, 1.0, 0.0},
                     0.0 * Constants<T>::PI / 180.0);
  qt.ToRotationMatrix(&rot);
  trsfm.at<T>(0, 0) = rot(0, 0);
  trsfm.at<T>(0, 1) = rot(0, 1);
  trsfm.at<T>(0, 2) = rot(0, 2);

  trsfm.at<T>(1, 0) = rot(1, 0);
  trsfm.at<T>(1, 1) = rot(1, 1);
  trsfm.at<T>(1, 2) = rot(1, 2);

  trsfm.at<T>(2, 0) = rot(2, 0);
  trsfm.at<T>(2, 1) = rot(2, 1);
  trsfm.at<T>(2, 2) = rot(2, 2);

  trsfm.at<T>(3, 0) = 0.0;
  trsfm.at<T>(3, 1) = 0.0;
  trsfm.at<T>(3, 2) = 0.0;

  trsfm.at<T>(0, 3) = 190.0;
  trsfm.at<T>(0, 3) = 0.0;
  trsfm.at<T>(1, 3) = 0.0;
  trsfm.at<T>(2, 3) = 0.0;
  trsfm.at<T>(3, 3) = 1.0;

  shape.Transform(trsfm);
  shape.Save("debug/mesh_shape.ply");
  // Left
  full.Transform(trsfm);
  full.Save("debug/mesh_all.ply");

  std::cout << "*** this->shape_p_ ***" << std::endl;
  std::cout << this->shape_p_.t() << std::endl;
  std::cout << "*** this->tex_p_ ***" << std::endl;
  std::cout << this->tex_p_.t() << std::endl;


  int err = status == -1 ?
            -2 :
            iter < this->max_iter_ && (res - old_res) < this->tol_ ?
            0 :
            -1;
  return err;
}

#pragma mark -
#pragma mark Private

/*
 * @name  JacobianData
 * @fn    void JacobianData(const Camera_t& camera,
                  const cv::Mat& shape,
                  const cv::Mat& shape_pc,
                  const cv::Mat& mean_tex,
                  const cv::Mat& tex_pc,
                  const cv::Mat& grad,
                  const cv::Mat& normal,
                  const T reg,
                  cv::Mat* j_data)
 * @brief Compute Jacobian for data term
 * @param[in] camera      Current camera estimation
 * @param[in] shape       Sampled shape   [3N x 1]
 * @param[in] shape_pc    Sampled shape principal component [3N x ns]
 * @param[in] mean_tex    Sampled mean texutre [3N x 1]
 * @param[in] tex_pc      Sampled texture principal component [3N x nt]
 * @param[in] grad        Sampled X/Y gradient  [CN x 2]
 * @param[in] normal      Sampled surface's normals [N x 3]
 * @param[out] j_data     Jacobian for data term  [CN x (ns + nc + nt)]
 */
template<typename T>
void SimultaneousFit<T>::JacobianData(const Camera_t& camera,
                                      const cv::Mat& shape,
                                      const cv::Mat& shape_pc,
                                      const cv::Mat& mean_tex,
                                      const cv::Mat& tex_pc,
                                      const cv::Mat& grad,
                                      const cv::Mat& normal,
                                      cv::Mat* j_data) {
  using LA = LinearAlgebra<T>;
  using TType = typename LinearAlgebra<T>::TransposeType;
  using Vec3 = Vector3<T>;
  // init jacobian container
  int n = shape.rows / 3;
  int c = this->get_n_channels();
  assert(n >= this->n_sample_);
  int ns = this->model_->get_active_shape_component();
  int nt = this->model_->get_active_texture_component();
  int ni = this->model_->get_active_illumination_component();
  int nc = camera.get_n_parameter();
  j_data->create(c * n, ns + nc + nt + ni, cv::DataType<T>::type);
  // Compute normal matrix + current texture estimation
  const auto normal_m = camera.get_view_transform().Inverse().Transpose();
  cv::Mat curr_tex = mean_tex.clone();
  LA::Gemv(tex_pc,
          TType::kNoTranspose,
          T(1.0),
          this->tex_p_,
          T(1.0),
          &curr_tex);
  // Camera derivative wrt shape/camera params
  cv::Mat dcam(2 * n, ns + nc, cv::DataType<T>::type);
  if (ns != 0) {
    cv::Mat dcam_dshape = dcam(cv::Rect(0, 0, ns, 2 * n));
    this->DcamDshapeParam(camera, shape, shape_pc, &dcam_dshape);
  }
  if (nc != 0) {
    cv::Mat dcam_dcam = dcam(cv::Rect(ns, 0, nc, 2 * n));
    this->DcamDcamParam(camera, shape, &dcam_dcam);
  }
  // Image derivative wrt to shape projection
  Parallel::For(n,
                [&](const size_t& i) {
                  // Define input/output
                  const T* ax_ptr = dcam.ptr<T>(2 * i);
                  const T* ay_ptr = dcam.ptr<T>(2 * i + 1);
                  // Compute Spherical harmonics
                  const auto* n_ptr = reinterpret_cast<Vec3*>(normal.data);
                  cv::Mat sh_coef;
                  SphericalHarmonic<T, 3> sh(normal_m * n_ptr[i]);
                  sh(this->illu_p_, &sh_coef);
                  const T* sh_ptr = sh.data();
                  assert(sh_coef.rows == c);
                  // Multiply with spatial grad
                  for (int k = 0; k < c; ++k) {
                    const int idx_r = (i * c) + k;
                    // Get scaling coefficients
                    const T& alpha = grad.at<T>(idx_r, 0);
                    const T& beta = grad.at<T>(idx_r, 1);
                    // Init B
                    T* b_ptr = j_data->ptr<T>(idx_r);
                    std::memcpy(b_ptr, ay_ptr, dcam.cols * sizeof(T));
                    // Compute B = alpha Ax + beta B
                    LA::Axpby(dcam.cols, ax_ptr, alpha, beta, b_ptr);

                    // Copy scaled texture as well (due to illumination
                    // contribution)
                    if (nt != 0) {
                      T* tex_ptr = j_data->ptr<T>(idx_r, dcam.cols);
                      const T* tex_pc_ptr = tex_pc.ptr<T>(idx_r);
                      const T alpha = -sh_coef.at<T>(k);
                      LA::Axpby(tex_pc.cols,
                              tex_pc_ptr,
                              alpha,
                              T(0.0),
                              tex_ptr);
                    }
                    // Add illumination
                    if (ni != 0) {
                      (*j_data)(cv::Rect(dcam.cols + nt,
                                         idx_r, ni, 1)) = T(0.0);
                      T* j_illu = j_data->ptr<T>(idx_r,
                                                 dcam.cols + nt +
                                                 k * this->illu_p_.cols);
                      LA::Axpby(this->illu_p_.cols,
                                sh_ptr,
                                -curr_tex.at<T>(idx_r),
                                T(0.0),
                                j_illu);
                    }
                  }
                });
}

/*
 * @name  ComputeUpdate
 * @fn    int ComputeUpdate(const cv::Mat& hessian,
                           const cv::Mat& error,
                           const Regularization& reg,
                           cv::Mat* delta_s,
                           cv::Mat* delta_t,
                           cv::Mat* delta_c,
                           cv::Mat* delta_i)
 * @brief Compute coefficients update given the \p hessian and \p error.
 * @param[in] hessian     Current Hessian estimation
 * @param[in] error       Current SD error estimation
 * @param[in] reg         Regularization factors
 * @param[out] delta_s    Shape update
 * @param[out] delta_t    Texture update
 * @param[out] delta_c    Camera update
 * @param[out] delta_i    Illumination update
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int SimultaneousFit<T>::ComputeUpdate(const cv::Mat& hessian,
                                      const cv::Mat& error,
                                      const Regularization& reg,
                                      cv::Mat* delta_s,
                                      cv::Mat* delta_t,
                                      cv::Mat* delta_c,
                                      cv::Mat* delta_i) {
  using SqLinSolver = typename LinearAlgebra<T>::SquareLinearSolver;
  // Solve linear system
  int err = -1;
  static SqLinSolver solver;
  cv::Mat p;
  solver.Solve(hessian, error, &p);
  if (!p.empty()) {
    // Updates
    int ns = this->model_->get_active_shape_component();
    int nt = this->model_->get_active_texture_component();
    int ni = this->model_->get_active_illumination_component();
    int nc = p.rows - ns - nt - ni;
    // Shape
    delta_s->create(ns, 1, cv::DataType<T>::type);
    *delta_s = -p(cv::Rect(0, 0, 1, ns));
    // Camera
    delta_c->create(nc, 1, cv::DataType<T>::type);
    *delta_c = -p(cv::Rect(0, ns, 1, nc));
    // Texture
    delta_t->create(nt, 1, cv::DataType<T>::type);
    *delta_t = -p(cv::Rect(0, ns + nc, 1, nt));
    // Illumination
    int c = this->get_n_channels();
    int b = ni / c;
    delta_i->create(c, b, cv::DataType<T>::type);
    *delta_i = -p(cv::Rect(0, ns + nc + nt, 1, ni)).reshape(1, c);


    err = 0;
  }
  return err;
}

/*
 * @name  UpdateCamera
 * @fn    void UpdateCamera(const cv::Mat& delta_c, Camera_t* camera)
 * @brief Update current camera estimation
 * @param delta_c Camera update
 * @param camera  Current camera estimation to update
 */
template<typename T>
void SimultaneousFit<T>::UpdateCamera(const cv::Mat& delta_c, Camera_t* camera) {
  // Update quaternion parameters, first make sure |dq| == 1
  auto dq = *delta_c.ptr<Quaternion<T>>(1);
  dq.w_ = T(1.0);
  dq.Normalize();
  // Get current rotation
  const auto& q = camera->get_rotation();
  // Define update destination
  auto& qu = *(reinterpret_cast<LTS5::Quaternion<T>*>(&cam_p_[3]));
  // Update rotation
  qu = dq * q;
  // Update focal
  cam_p_[0] += delta_c.at<T>(0);
  // Update tx, ty, tz
  cam_p_[7] += delta_c.at<T>(4);
  cam_p_[8] += delta_c.at<T>(5);
  cam_p_[9] += delta_c.at<T>(6);
  // Update camera object
  camera->FromVector(cam_p_);
}

#pragma mark -
#pragma mark Proxy

/*
 * @name  Create
 * @fn    MMFitAlgorithm<T>* Create(const MorphableModel<T>* model) const
 * @brief Instantiate a specific instance for a given proxy
 * @param[in] model   Morphable model to use for fitting
 * @return    Fitting algorithm
 */
template<typename T>
MMFitAlgorithm<T>*
SimultaneousFitProxy<T>::Create(const MorphableModel<T>* model) const {
  return new SimultaneousFit<T>(model);
}

/*
 * @name  Name
 * @fn    const char* Name(void) const
 * @brief Indicate the name of that proxy
 * @return    Fit algorithm's name
 */
template<typename T>
const char* SimultaneousFitProxy<T>::Name(void) const {
  return "SimultaneousForward";
}

/** Explicit registration */
SimultaneousFitProxy<float> sim_proxy_float;
SimultaneousFitProxy<double> sim_proxy_double;

#pragma mark -
#pragma mark Explicit Instantiation

/** Simultaneous Forward - Float */
template class SimultaneousFit<float>;
/** Simultaneous Forward - Double */
template class SimultaneousFit<double>;

}  // namespace LTS5
