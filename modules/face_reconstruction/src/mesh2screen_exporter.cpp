/**
 *  @file   mesh2screen_exporter.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   04/04/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <string>
#include <vector>

#ifdef __APPLE__
#include <OpenGL/gl3.h>  // <GL/glew.h>
// Might be not necessary to test for ARCH, need further test.
#define GLFW_INCLUDE_GLCOREARB
#else
#include <GL/glew.h>
#endif

#ifdef HAS_GLFW_
#define GLFW_INCLUDE_GLCOREARB
#include "GLFW/glfw3.h"
#endif

#include "lts5/face_reconstruction/mesh2screen_exporter.hpp"
#include "lts5/utils/math/constant.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name Mesh2ScreenExporter
 *  @fn Mesh2ScreenExporter()
 *  @brief  Constructor
 */
Mesh2ScreenExporter::Mesh2ScreenExporter() : ctx_(nullptr),
                                                 mesh_(nullptr),
                                                 texture_(nullptr),
                                                 program_(nullptr),
                                                 cam_(nullptr),
                                                 renderer_(nullptr),
                                                 is_initialized_(false) {
  // Init OpenGL context
  ctx_ = new OGLContext();
}

/*
 *  @name ~Mesh2ScreenExporter
 *  @fn ~Mesh2ScreenExporter()
 *  @brief  Destructor
 */
Mesh2ScreenExporter::~Mesh2ScreenExporter() {
  if (texture_) {
    delete texture_;
    texture_ = nullptr;
  }
  if (program_) {
    delete program_;
    program_ = nullptr;
  }
  if (cam_) {
    delete cam_;
    cam_ = nullptr;
  }
  if (renderer_) {
    delete renderer_;
    renderer_ = nullptr;
  }
  // Release OpenGL context
  if (ctx_) {
    delete ctx_;
    ctx_ = nullptr;
  }
}

/*
 *  @name Initialize
 *  @fn void Initialize(const LTS5::Mesh<float>& mesh,
                        const cv::Mat& texture,
                        const int width,
                        const int height)
 *  @brief  Initialize converter
 *  @param[in]  mesh  Mesh to convert to image
 *  @param[in]  texture Mesh's texture, can be null if not used
 *  @param[in]  width   Image width
 *  @param[in]  height  Image height
 */
void Mesh2ScreenExporter::Initialize(const LTS5::Mesh<float>& mesh,
                                     const cv::Mat& texture,
                                     const int width,
                                     const int height) {
  if (!is_initialized_) {
    // Setup
    mesh_ = const_cast<LTS5::Mesh<float>*>(&mesh);
    // Init OpenGL windows
    int err = ctx_->Initialize(width, height, true);
    if (!err) {
      ctx_->ActiveContext();
      // Texture
      if (!texture_) {
        texture_ = new LTS5::OGLTexture(texture,
                                        GL_LINEAR,
                                        GL_CLAMP_TO_EDGE);
      } else {
        texture_->Upload(texture);
      }
      // Program
      if (!program_) {
        // Create shader + program
        std::string vertex_shader_str = "#version 330\n"
                "layout (location = 0) in vec3 position;\n"
                "layout (location = 1) in vec3 normal;\n"
                "layout (location = 2) in vec2 tex_coord;\n"
                "uniform mat4 camera;\n"
                "out vec2 tex_coord0;\n"
                "out vec3 normal0;\n"
                "void main() {\n"
                "  gl_Position = camera * vec4(position, 1);\n"
                "  normal0 = normal;\n"
                "  tex_coord0 = tex_coord;\n"
                "}";
        std::string frag_shader_str = "#version 330\n"
                "in vec2 tex_coord0;\n"
                "in vec3 normal0;\n"
                "uniform sampler2D tex_sampler;\n"
                "out vec4 frag_color;\n"
                "void main() {\n"
                "  float t = texture(tex_sampler, tex_coord0).r;\n"
                "  frag_color = vec4(t, t, t, 1.f);\n"
                "}";
        using ShaderType = typename LTS5::OGLShader::ShaderType;
        std::vector<OGLShader> shaders;
        shaders.emplace_back(vertex_shader_str, ShaderType::kVertex);
        shaders.emplace_back(frag_shader_str, ShaderType::kFragment);
        using AttributeType = OGLProgram::AttributeType;
        std::vector<OGLProgram::Attributes> attrib;
        attrib.emplace_back(AttributeType::kPoint, 0, "position");
        attrib.emplace_back(AttributeType::kNormal, 1, "normal");
        attrib.emplace_back(AttributeType::kTexCoord, 2, "tex_coord");
        program_ = new LTS5::OGLProgram(shaders, attrib);
        // Camera
        if (!cam_) {
          cam_ = new LTS5::OGLCamera<float>(width, height);
        }
        if (!renderer_) {
          renderer_ = new OGLRenderer<float>();
        }
        renderer_->AddMesh(mesh_);
        renderer_->AddProgram(program_);
        renderer_->AddTexture(texture_);
        // Bind to opengl
        err = renderer_->BindToOpenGL();
        // Done
        this->is_initialized_ = (err == 0);
      }
      ctx_->DisableContext();
    } else {
      LTS5_LOG_ERROR("Error, can not create OpenGL Window");
    }
  }
}

#pragma mark -
#pragma mark Usage

/**
 * @name  ActiveContext
 * @fn    void ActiveContext() const
 * @brief Make this context the current one
 */
void Mesh2ScreenExporter::ActiveContext() const {
  ctx_->ActiveContext();
}

/**
 * @name  DisableContext
 * @fn    void DisableContext() const
 * @brief Detach from current context
 */
void Mesh2ScreenExporter::DisableContext() const {
  ctx_->DisableContext();
}

/*
 *  @name Process
 *  @fn void Process(const std::string& output_filename)
 *  @brief  Export mesh according to exporter class
 *  @param[in]  output_filename Name of the exported file
 */
void Mesh2ScreenExporter::Process() {
  using Vec3f = Vector3<float>;
  if (is_initialized_) {
    this->ctx_->ActiveContext();
    // Define camera position + look at
    mesh_->ComputeBoundingBox();
    const AABB<float>& bbox = mesh_->bbox();
    auto dt = bbox.max_ - bbox.center_;
    float r = std::sqrt((dt.x_ * dt.x_) + (dt.y_ * dt.y_) + (dt.z_ * dt.z_));
    // Define displacement in Z direction
    float dz = r / std::sin(cam_->get_field_of_view() *
                            Constants<float>::PI * 0.002777778f);
    cam_->set_position(Vec3f(bbox.center_.x_,
                             bbox.center_.y_,
                             bbox.center_.z_ + dz));
    // Update projection transformation
    auto proj = cam_->projection();
    auto view = cam_->view();
    program_->Use();
    program_->SetUniform("camera", proj * view);
    program_->SetUniform("tex_sampler", 0);
    program_->StopUsing();
    // Render
    renderer_->ClearBuffer();
    renderer_->Render();
    // Swap
    ctx_->SwapBuffer();
    // Detach
    ctx_->DisableContext();
  }
}

#pragma mark -
#pragma mark Accessors

/*
 * @name  set_mesh
 * @fn    void set_mesh(const LTS5::Mesh<float>& mesh)
 * @brief Set what mesh need to be exported
 * @param mesh    Mesh to export
 */
void Mesh2ScreenExporter::set_mesh(const LTS5::Mesh<float>& mesh) {
  if (is_initialized_) {
    static bool first_pass = true;
    mesh_ = const_cast<LTS5::Mesh<float>*>(&mesh);
    this->ctx_->ActiveContext();
    renderer_->AddMesh(mesh_);
    if (renderer_->UpdateVertex()) {
      LTS5_LOG_WARNING("Can not update mesh");
    }
    if (first_pass) {
      renderer_->UpdateTCoord();
      first_pass = false;
    }
  }
}

/*
 *  @name set_texture
 *  @fn void set_texture(const vtkTexture* texture)
 *  @brief  Set texture of the object
 *  @param[in]  texture Texture object
 */
void Mesh2ScreenExporter::set_texture(const cv::Mat& texture) {
  if (is_initialized_) {
    this->ctx_->ActiveContext();
    if (texture_->Upload(texture)) {
      LTS5_LOG_WARNING("Can not upload texture");
    }
  }
}

}  // namespace LTS5
