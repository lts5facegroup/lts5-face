/**
 *  @file   virtual_camera.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 24/02/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <fstream>

#include "lts5/face_reconstruction/virtual_camera.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/face_reconstruction/utilities.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/*
 *  @name   VirtualCamera
 *  @brief  Basic constructor
 */
VirtualCamera::VirtualCamera(void) : real_camera_(false),
                                     position_{-1.f,-1.f,-1.f},
                                     focal_{100.f,100.f},
                                     center_point_{0.f,0.f},
                                     distortion_coefficient_{-1.f,
                                                             -1.f,
                                                             -1.f,
                                                             -1.f,
                                                             -1.f} {
}

/*
 *  @name   VirtualCamera
 *  @brief  Constructor
 *  @param  x      X location
 *  @param  y      Y location
 *  @param  z      Z location
 */
VirtualCamera::VirtualCamera(const float x,
                             const float y,
                             const float z) :
real_camera_(false),
focal_{100.f,100.f},
center_point_{0.f,0.f},
distortion_coefficient_{-1.f,-1.f,-1.f,-1.f,-1.f} {
  // Set position
  this->set_position(x, y, z);
  // Init intrinsic
  this->InitIntrinsicParameters();
  // Init extrinsicam cam
  this->InitExtrinsicParameters();
  // Init internal matrix
  this->InitMatrix();
}

/*
 *  @name   VirtualCamera
 *  @brief  Constructor
 *  @param  x                  X location
 *  @param  y                  Y location
 *  @param  z                  Z location
 *  @param  focal_x       Camera's lens focal length X
 *  @param  aFocalLengthY       Camera's lens focal length Y
 */
VirtualCamera::VirtualCamera(const float x,
                             const float y,
                             const float z,
                             const float focal_x,
                             const float focal_y) :
real_camera_(false),
center_point_{0.f,0.f},
distortion_coefficient_{-1.f,-1.f,-1.f,-1.f,-1.f} {
  // Set position
  this->set_position(x, y, z);
  // Set focal length
  this->set_focal(focal_x,focal_y);
  // Init intrinsic
  this->InitIntrinsicParameters();
  // Init extrinsic
  this->InitExtrinsicParameters();
  // Init internal matrix
  this->InitMatrix();
}

/*
 *  @name   VirtualCamera
 *  @brief  Load calibration information (Intrinsic+Extrinsic) from binary File (*.cal)
 *  @param  camera_calibration   Camera calibration parameters
 */
VirtualCamera::VirtualCamera(const std::string& camera_calibration) :
real_camera_(true) {
  // Open binary calibration files
  std::ifstream fid(camera_calibration.c_str(), std::ios_base::binary);
  if (fid.is_open() &&
      camera_calibration.at(camera_calibration.length() - 1) != '/') {
    // Set focal length
    fid.read(reinterpret_cast<char*>(&focal_[0]), 2 * sizeof(focal_[0]));
    // Set principle points
    fid.read(reinterpret_cast<char*>(&center_point_[0]),
             2 * sizeof(center_point_[0]));
    // Init intrinsic
    this->InitIntrinsicParameters();
    // Extrinsic parameters
    extrinsic_matrix_.create(3, 4, CV_32FC1);
    // Ext mat
    fid.read(reinterpret_cast<char*>(extrinsic_matrix_.data),
             extrinsic_matrix_.rows * extrinsic_matrix_.cols * sizeof(float));
    // Cam position
    fid.read(reinterpret_cast<char*>(&position_[0]), 3 * sizeof(position_[0]));
    // Read disto coef
    fid.read(reinterpret_cast<char*>(&distortion_coefficient_[0]),
             5 * sizeof(distortion_coefficient_[0]));
    // Close file
    fid.close();
    // Init internal matrix
    this->InitMatrix();
  } else {
    throw LTS5::ProcessError(ProcessError::ProcessErrorEnum::kErrOpeningFile,
                             "Unable to load calibration file (*.cal)",
                             FUNC_NAME);
  }

}

/*
 *  @name   VirtualCamera
 *  @brief  Constructor
 *  @param  x              X location
 *  @param  y              Y location
 *  @param  z              Z location
 *  @param  focal_x   Camera's lens focal length X
 *  @param  focal_x   Camera's lens focal length Y
 *  @param  image_center_x             Camera center points in x
 *  @param  image_center_y             Camera center points in y
 *  @param  real_camera        Indicate if it simulate camera of not
 */
VirtualCamera::VirtualCamera(const float x,
                             const float y,
                             const float z,
                             const float focal_x,
                             const float focal_y,
                             const float image_center_x,
                             const float image_center_y,
                             const bool real_camera) {
  real_camera_ = real_camera;
  // Set position
  this->set_position(x, y, z);
  // Set focal length
  this->set_focal(focal_x,focal_y);
  // Set image center
  this->set_center_point(image_center_x, image_center_y);
  // Init intrinsic
  this->InitIntrinsicParameters();
  // Init extrinsic
  this->InitExtrinsicParameters();
  // Init internal matrix
  this->InitMatrix();
}

/*
 *  @name   ~VirtualCamera
 *  @brief  Destructor
 */
VirtualCamera::~VirtualCamera(void) {
}

/*
 *  @name   InitIntrinsicParameters
 *  @brief  Init intrinsic matrix
 */
void VirtualCamera::InitIntrinsicParameters(void) {
  // Instanciate matrix
  intrinsic_matrix_.create(3, 3, CV_32FC1);
  intrinsic_matrix_.setTo(0.f);

  // Fill with information
  intrinsic_matrix_.at<float>(0,0) = focal_[0];
  intrinsic_matrix_.at<float>(0,2) = center_point_[0];
  intrinsic_matrix_.at<float>(1,1) = focal_[1];
  intrinsic_matrix_.at<float>(1,2) = center_point_[1];
  intrinsic_matrix_.at<float>(2,2) = 1.f;
}

/*
 *  @name   InitExtrinsicParameters
 *  @brief  Init extrinsic matrix with given position, assuming camera is pointing at origin
 */
void VirtualCamera::InitExtrinsicParameters(void) {
  // Compute rotation - translation
  float gamma = 0.f,theta = 0.f,phi = 0.f;
  // Define angles
  phi = -std::atan(position_[0]/position_[2]);
  theta = std::atan(position_[1]/position_[2]) + CV_PI;

  // Define cam origin
  cv::Mat camera_origin = cv::Mat(3,1,CV_32FC1,position_);
  // Define translation - rotation

  // Instanciate extrinsic param
  extrinsic_matrix_.create(3, 4, CV_32FC1);
  extrinsic_matrix_.setTo(0.f);

  cv::Mat extrinsic_rotation = extrinsic_matrix_(cv::Rect(0,0,3,3));
  cv::Mat extrinsic_translation = extrinsic_matrix_(cv::Rect(3,0,1,3));

  LTS5::Utilities::GetRotationMatrix(gamma,
                                     theta,
                                     phi,
                                     LTS5::Utilities::k3d3d,
                                     true,
                                     &extrinsic_rotation);
  extrinsic_translation = -1.f * (extrinsic_rotation * camera_origin);
}

/*
 *  @name   InitMatrix
 *  @brief  Initialize internal matrices such as projection_matrix_,back_projection_matrix_,camera_orientation_,...
 */
void VirtualCamera::InitMatrix(void) {
  // Compute camera orientation
  camera_orientation_ = extrinsic_matrix_(cv::Rect(0,0,3,3)).t();
  // Projection matrix
  projection_matrix_ = intrinsic_matrix_ * extrinsic_matrix_;
  // Back projection matrix
  back_projection_matrix_ = extrinsic_matrix_(cv::Rect(0,0,3,3)).t() * intrinsic_matrix_.inv();
}
}





