/**
 *  @file   ambient_occlusion_wrapper.cpp
 *  @brief  
 *  @ingroup 
 *
 *  @author Christophe Ecabert
 *  @date   11/13/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include "ambient_occlusion_wrapper.hpp"
#include "lts5/python/ocv_converter.hpp"

#include "lts5/geometry/ambient_occlusion.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

namespace py = pybind11;

/*
 * @name    ExposeAmbientOcclusion
 * @param[in] m     Python module where to add the bindings
 * @param[in] name  Export name
 * @tparam T Data type
 */
template<typename T>
void ExposeAmbientOcclusion(py::module& m, const std::string& name) {
  using AOType = AmbientOcclusionGenerator<T>;
  using MeshType = typename AOType::Mesh_t;

  py::class_<AOType> AoGen(m,
                           name.c_str(),
                           R"doc(Ambient Occlusion Generator)doc");
  // Constructor
  AoGen.def(py::init<MeshType&>(),
            py::arg("mesh"),
            R"doc(Create an Ambient Occlusion Generator for a given
surface.
> mesh: Surface for which to compute AOs.)doc")
  // Methods
       .def("process",
            &AOType::Generate,
            py::arg("n_sample"),
            R"doc(Generate Ambient Occlusion using Monte Carlo sampling.
> n_sample: Number of sample to use to estimate AO.)doc")
       .def("save",
            &AOType::Save,
            py::arg("path"),
            R"doc(Save ambient occlusion into a file.
> path: Location where to dump the data (i.e. AO))doc")

  // Property
       .def_property_readonly("ao",
                              &AOType::get_ao,
                              R"doc(Estimated Ambient Occlusions)doc");
}

/**
 * @name    AddAmbientOcclusion
 * @fn      void AddAmbientOcclusion(pybind11::module& m)
 * brief    Expose Ambient Occlusion to python
 * @param m Python module where to add them
 */
void AddAmbientOcclusion(py::module& m) {
  ExposeAmbientOcclusion<float>(m, "AOGeneratorFloat");
  ExposeAmbientOcclusion<double>(m, "AOGeneratorDouble");
}

}  // namespace Python
}  // namespace LTS5