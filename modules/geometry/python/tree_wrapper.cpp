/**
 *  @file   tree_wrapper.cpp
 *  @brief  Binding for OCTree
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   10/16/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"
#include "pybind11/stl.h"

#include "tree_wrapper.hpp"

#include "lts5/geometry/octree.hpp"
#include "lts5/geometry/bounding_sphere.hpp"

#include "math_converter.hpp"

namespace py = pybind11;
using namespace pybind11::literals;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    ExposeOCTree
 * @fn  void ExposeOCTree(py::module& m, const std::string& name)
 * @brief   Expose OCTree to python
 * @param[in] m     Python module where to add OCTree
 * @param[in] name  Binding name
 * @tparam T    Data type
 */
template<typename T>
void ExposeOCTree(py::module& m, const std::string& name) {
  using OCTree_t = OCTree<T>;
  using Mesh = Mesh<T, Vector3<T>>;
  using Vec3 = typename Mesh::Vertex;

  // Define "python class"
  py::class_<OCTree_t> octree(m,
                            name.c_str(),
                            R"doc(Spatial partitionning based on OCTree)doc");
  // Constructor
  octree.def(py::init<>(),
             R"doc(Create an empty OCTree)doc")
        .def(py::init<int, int>(),
             py::arg("bucket_size"),
             py::arg("max_depth"),
             R"doc(Constructor
> bucket_size: Maximum number of object per leaf node
> max_depth: Maximum tree depth)doc")
  // Methods
        .def("insert",
             &OCTree_t::template Insert<Vector3<T>>,
             py::arg("mesh"),
             py::arg("type"),
             R"doc(Insert a range of element into the tree
> mesh: 3D mesh used to build tree from
> type: Type of primitive to consider (points, triangle))doc")
        .def("build",
             &OCTree_t::Build,
             R"doc(Iteratively build the tree)doc")
        .def("clear",
             &OCTree_t::Clear,
             R"doc(Clear the underlying tree)doc")
        .def("intersect",
             [](OCTree_t& tree, const Vec3& p, const Vec3& dir) {
                py::list index;
                std::vector<int> idx;
                if (tree.DoIntersect(p, dir, &idx)) {
                  for (const auto& i : idx) {
                    index.append(i);
                  }
                }
               return index;
             },
             py::arg("p"),
             py::arg("dir"),
             R"doc(Check if a given ray ,p + t*dir, intersect with one of
the triangle's bounding box in the tree and provide its corresponding index
> p: Starting point of the ray
> dir: Direction of the the ray)doc")
      .def("intersect_with_segment",
           [](OCTree_t& tree, const Mesh& mesh, const Vec3& p,
                   const Vec3& q, const T& t_max) {
              T t = T(-1.0);
              tree.IntersectWithSegment(mesh, p, q, t_max, &t);
              return t;
           },
           py::arg("mesh"),
           py::arg("p"),
           py::arg("q"),
           py::arg("t_max") = T(1.0),
           R"doc(Test if the mesh intersects with a given line PQ
> mesh: Mesh to test against
> p: Starting point of the segment to test against
> q: Stopping point of the segment to test against
> t_max: Maximum t allowed, by default should be 1.f
Return -1.0 if no intersection otherwise `t` in [0, 1.0])doc");
}

template<typename T>
struct TypeToString;

template<>
struct TypeToString<float> {
  static constexpr const char* const value = "Float";
};
template<>
struct TypeToString<double> {
  static constexpr const char* const value = "Double";
};

template<typename T>
void ExposeBoundingSphereTree(py::module& m) {
  using BSTree_t = BoundingSphereTree<T>;
  using CPts = ClosestPointSurface<T>;
  using CPtsType = typename CPts::Type;
  using Mesh = Mesh<T, Vector3<T>>;

  // ClosestPointSurface
  auto class_name = "ClosestPoint" + std::string(TypeToString<T>::value);
  py::class_<CPts> c_pts(m,
                         class_name.c_str(),
                         R"doc(Information about closest point found on surface)doc");

  c_pts.def_property_readonly("distance",
                              [](const CPts& obj) {return obj.dist;},
                              R"doc(Distance from the query to the surface)doc")
          .def_property_readonly("point",
                                 [](const CPts& obj) {return obj.pts;},
                                 R"doc(Closest point on the surface)doc")
          .def_property_readonly("type",
                                 [](const CPts& obj) {return obj.type;},
                                 R"doc(Type of closest position (i.e. in triangle, on edge, vertex))doc")
          .def_property_readonly("indexes",
                                 [](const CPts& obj) {return obj.idx;},
                                 R"doc(List of vertex index of the closest triangle defining the closest point)doc")
          .def_property_readonly("triangle_index",
                                 [](const CPts& obj) {return obj.tri_idx;},
                                 R"doc(Index of the triangle closest to the query point)doc")
          ;
  py::enum_<CPtsType>(c_pts, "Type")
          .value("kOnPoint",
                 CPtsType::kOnPoint,
                 R"doc(Closest point is a vertex)doc")
          .value("kOnLine",
                 CPtsType::kOnLine,
                 R"doc(Closest point lies on a line)doc")
          .value("kInTriangle",
                 CPtsType::kInTriangle,
                 R"doc(CLosest point is in a triangle)doc")
          .export_values();

  auto tree_name = "BoundingSphereTree" + std::string(TypeToString<T>::value);
  py::class_<BSTree_t> tree(m,
                            tree_name.c_str(),
                            R"doc(Spatial partitioning based on Sphere for 3D points)doc");
  tree.def(py::init<>(), R"doc(Create an empty tree)doc")
          .def("build",
               [](BSTree_t& tree, const Mesh& mesh) {
                 tree.Build(mesh.get_vertex(), mesh.get_triangle());
               },
               "mesh"_a,
               R"doc(Build internal structure for triangle mesh
> mesh: Mesh object to build tree for)doc")
          .def("closest_point_on_surface",
               &BSTree_t::ClosestPointOnSurface,
               "point"_a,
               R"doc(Find closest point on the underlying surface.
> point: Query point)doc")
          ;
}

/**
 * @name    AddTree
 * @fn      void AddTree(pybind11::module& m)
 * brief    Expose Tree (OCTree/AABB) to python
 * @param m Python module where to add them
 */
void AddTree(pybind11::module& m) {
  ExposeOCTree<float>(m, "OCTreeFloat");
  ExposeOCTree<double>(m, "OCTreeDouble");
  ExposeBoundingSphereTree<float>(m );
  ExposeBoundingSphereTree<double>(m);
}

}  // namespace Python
}  // namespace LTS5
