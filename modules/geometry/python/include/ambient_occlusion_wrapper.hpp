/**
 *  @file   ambient_occlusion_wrapper.hpp
 *  @brief  Add Ambient Occlusion python bindings
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   11/13/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __AMBIENT_OCCLUSION_WRAPPER__
#define __AMBIENT_OCCLUSION_WRAPPER__

#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"
#include "pybind11/pybind11.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    AddAmbientOcclusion
 * @fn      void AddAmbientOcclusion(pybind11::module& m)
 * brief    Expose Ambient Occlusion to python
 * @param m Python module where to add them
 */
void AddAmbientOcclusion(pybind11::module& m);

}  // namespace Python
}  // namespace LTS5

#endif  // __AMBIENT_OCCLUSION_WRAPPER__
