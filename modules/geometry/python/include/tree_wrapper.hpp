/**
 *  @file   tree_wrapper.hpp
 *  @brief  Binding for OCTree
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   10/16/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TREE_PYTHON_WRAPPER__
#define __LTS5_TREE_PYTHON_WRAPPER__

#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"
#include "pybind11/pybind11.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    AddTree
 * @fn      void AddTree(pybind11::module& m)
 * brief    Expose Tree (OCTree/AABB) to python
 * @param m Python module where to add them
 */
void AddTree(pybind11::module& m);

}  // namespace Python
}  // namespace LTS5
#endif  // __LTS5_TREE_PYTHON_WRAPPER__
