/**
 *  @file   mesh_wrapper_impl.hpp
 *  @brief  Python wrapper for Mesh class
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   8/6/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <vector>
#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"

#include "pybind11/stl.h"

#include "mesh_wrapper.hpp"
#include "lts5/python/ocv_converter.hpp"
#include "lts5/geometry/mesh.hpp"

#include "math_converter.hpp"

namespace py = pybind11;
using namespace pybind11::literals; // to bring in the `_a` literal

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/*
 * @name    ExposeMaterial
 * @fn  void ExposeMaterial(py::module& m, const std::string& name)
 * @brief   Expose the Material class to a python module
 * @tparam T    Data type
 * @param[in,out] m Python module to which the definition will be added
 * @param[in] name  Name of the class to expose
 */
template<typename T>
void ExposeMaterial(py::module& m, const std::string& name) {
  using Mat = Material<T>;
  using IlluType = typename Mat::IlluminationType;

  // Define "python class"
  py::class_<Mat> material(m,
                           name.c_str(),
                           R"doc(Mesh material representation)doc");

  // Constructor
  material.def(py::init<>(),
               R"doc(Create an empty material)doc")
          .def_static("from_file",
                      &Mat::CreateFromFile,
                      py::arg("path"),
                      R"doc(Create a list of materials from a given file.
> path: Path to material file (i.e. *.mtl))doc")

  // Properties
          .def_property("name",
                        &Mat::get_name,
                        &Mat::set_name,
                        py::return_value_policy::reference,
                        R"doc(Material's name)doc")
          .def_property("ka",
                        &Mat::get_ambient,
                        &Mat::set_ambient,
                        py::return_value_policy::reference,
                        R"doc(Ambient normalized color)doc")
          .def_property("kd",
                        &Mat::get_diffuse,
                        &Mat::set_diffuse,
                        py::return_value_policy::reference,
                        R"doc(Diffuse normalized color)doc")
          .def_property("ks",
                        &Mat::get_specular,
                        &Mat::set_specular,
                        py::return_value_policy::reference,
                        R"doc(Specular normalized color)doc")
          .def_property("ns",
                        &Mat::get_specular_exponent,
                        &Mat::set_specular_exponent,
                        py::return_value_policy::reference,
                        R"doc(Specular exponent)doc")
          .def_property("transmittance",
                        &Mat::get_transmittance,
                        &Mat::set_transmittance,
                        py::return_value_policy::reference,
                        R"doc(Transmittance)doc")
          .def_property("emission",
                        &Mat::get_emission,
                        &Mat::set_emission,
                        py::return_value_policy::reference,
                        R"doc(Emission)doc")
          .def_property("index_of_refraction",
                        &Mat::get_index_of_refraction,
                        &Mat::set_index_of_refraction,
                        py::return_value_policy::reference,
                        R"doc(Index of refraction)doc")
          .def_property("dissolve",
                        &Mat::get_dissolve,
                        &Mat::set_dissolve,
                        py::return_value_policy::reference,
                        R"doc(Dissolve)doc")
          .def_property("illumination",
                        &Mat::get_illumination,
                        &Mat::set_illumination,
                        py::return_value_policy::reference,
                        R"doc(Illumination model)doc")
          .def_property("ka_texture",
                        &Mat::get_ambient_texture,
                        &Mat::set_ambient_texture,
                        py::return_value_policy::reference,
                        R"doc(Ambient color texture)doc")
          .def_property("kd_texture",
                        &Mat::get_diffuse_texture,
                        &Mat::set_diffuse_texture,
                        py::return_value_policy::reference,
                        R"doc(Diffuse color texture)doc")
          .def_property("ks_texture",
                        &Mat::get_specular_texture,
                        &Mat::set_specular_texture,
                        py::return_value_policy::reference,
                        R"doc(Specular color texture)doc")
          .def_property("ns_texture",
                        &Mat::get_specular_exponent_texture,
                        &Mat::set_specular_exponent_texture,
                        py::return_value_policy::reference,
                        R"doc(Specular exponent texture)doc")
          .def_property("bump_texture",
                        &Mat::get_bump_texture,
                        &Mat::set_bump_texture,
                        py::return_value_policy::reference,
                        R"doc(Bump texture)doc")
          .def_property("displacement_texture",
                        &Mat::get_displacement_texture,
                        &Mat::set_displacement_texture,
                        py::return_value_policy::reference,
                        R"doc(Displacement texture)doc")
          .def_property("alpha_texture",
                        &Mat::get_alpha_texture,
                        &Mat::set_alpha_texture,
                        py::return_value_policy::reference,
                        R"doc(Alpha texture)doc")
          .def_property("reflection_texture",
                        &Mat::get_reflection_texture,
                        &Mat::set_reflection_texture,
                        py::return_value_policy::reference,
                        R"doc(Reflection texture)doc");

  // Enum
  // See: https://pybind11.readthedocs.io/en/stable/classes.html#enumerations-and-internal-types
  py::enum_<IlluType>(material, "IlluminationType")
          .value("kColorOnAndAmbientOff",
                 IlluType::kColorOnAndAmbientOff)
          .value("kColorOnAndAmbientOn",
                 IlluType::kColorOnAndAmbientOn)
          .value("kHighlightOn",
                  IlluType::kHighlightOn)
          .value("kReflectionOnAndRayTraceOn",
                  IlluType::kReflectionOnAndRayTraceOn)
          .value("kTransparency_GlassOn_Reflection_RayTraceOn",
                  IlluType::kTransparency_GlassOn_Reflection_RayTraceOn)
          .value("kReflection_FresnelOnAndRayTraceOn",
                  IlluType::kReflection_FresnelOnAndRayTraceOn)
          .value("kTransparency_RefractionOn_Reflection_FresnelOffAndRayTraceOn",
                  IlluType::kTransparency_RefractionOn_Reflection_FresnelOffAndRayTraceOn)
          .value("kTransparency_RefractionOn_Reflection_FresnelOnAndRayTraceOn",
                  IlluType::kTransparency_RefractionOn_Reflection_FresnelOnAndRayTraceOn)
          .value("kReflectionOnAndRayTraceOff",
                  IlluType::kReflectionOnAndRayTraceOff)
          .value("kTransparency_GlassOn_Reflection_RayTraceOff",
                  IlluType::kTransparency_GlassOn_Reflection_RayTraceOff)
          .value("kCastsShadowsOntoInvisibleSurfaces",
                  IlluType::kCastsShadowsOntoInvisibleSurfaces)
          .export_values();
}

/*
 * @name    ExposeEdgeFunction
 * @fn  void ExposeEdgeFunction(py::module& m)
 * @brief   Expose the edge class + function to a python module
 * @param[in,out] m Python module to which the definition will be added
 */
void ExposeEdgeFunction(py::module& m) {
  // Expose edge class
  py::class_<Edge> edge(m,
                        "Edge",
                        R"doc(Edge representation between two vertices. Holds
 information about adjacent faces as well)doc");

  edge.def(py::init<const int64_t&, const int64_t&,
                    const int64_t&, const int64_t&>(),
           "v0"_a,
           "v1"_a,
           "f0"_a,
           "f1"_a,
           R"doc(Constructor
> v0: First vertex index
> v1: Second vertex index
> f0: First face index
> f1: Second face index)doc")
      .def_property_readonly("v0",
                             &Edge::get_first_vertex,
                             R"doc(Provide index of first vertex)doc")
      .def_property_readonly("v1",
                             &Edge::get_second_vertex,
                             R"doc(Provide index of second vertex)doc")
      .def_property("f0",
                    &Edge::get_first_face,
                    &Edge::set_first_face,
                    R"doc(Set index of first face
> f0: First face index)doc")
      .def_property("f1",
                    &Edge::get_second_face,
                    &Edge::set_second_face,
                    R"doc(Set index of second face
> f1: Second face index)doc");
  // Expose function
  m.def("extract_edge_information",
        [](const cv::Mat& triangle, const int& n_vertex) {
            using Tri = Vector3<int>;
            if (triangle.cols != 3) {
              throw std::invalid_argument("Triangule must have shape [T, 3]");
            }
            // Convert to vector
            const auto* tri_ptr = triangle.ptr<Tri>(0);
            std::vector<Tri> tris(tri_ptr, tri_ptr + triangle.rows);
            // Call function
            std::vector<Edge> edges;
            std::vector<std::vector<size_t>> neighbours;
            ExtractEdgeInformation(tris, n_vertex, &edges, &neighbours);
            return std::make_tuple(edges, neighbours);
        },
        "triangle"_a,
        "n_vertex"_a,
        R"doc(Extract list of edges and one ring neighbour from a given
 triangulation.
> triangle: Array of triangles, [T, 3],
> n_vertex: Total number of vertex in the triangulation)doc");
}

/*
 * @name    ExposeMesh
 * @fn  void ExposeMesh(py::module& m, const std::string& name)
 * @brief   Expose the mesh class to a python module
 * @tparam T    Data type
 * @param[in,out] m Python module to which the definition will be added
 * @param[in] name  Name of the class to expose
 */
template<typename T>
void ExposeMesh(py::module& m, const std::string& name) {
  using Mesh = Mesh<T, Vector3<T>>;
  using vpe_t = typename Mesh::Vpe;
  using fpe_t = typename Mesh::Fpe;
  using Tri = typename Mesh::Triangle;
  using PrimType = typename Mesh::PrimitiveType;

#define MESH_GETTER(attr, dim, type)                        \
  [](const Mesh& m) {                                       \
    const auto& x = m.get_##attr();                         \
    return cv::Mat(x.size(), dim, type, (void*)x.data());   \
  }
#define MESH_SETTER(attr, dim, type)                              \
  [](Mesh& m, const cv::Mat& value) {                             \
    using Type = typename Mesh::type;                             \
    int n = value.rows * value.cols;                              \
    assert((n % dim) == 0);                                       \
    n /= dim;                                                     \
    const auto* ptr = value.ptr<Type>(0);                         \
    m.get_##attr().assign(ptr, ptr + n);                          \
  }

  using getter_cmt = const std::vector<std::string>&(Mesh::*)() const;

  // Define "python class"
  py::class_<Mesh> mesh(m,
                        name.c_str(),
                        R"doc(Define 3D Mesh representation)doc");
  // Constructor
  mesh.def(py::init<>(), R"doc(Create an empty mesh)doc")
      .def(py::init<std::string>(),
           py::arg("filename"),
           R"doc(Create a mesh from a given file
> filename: Path of the mesh to open)doc")
      .def("load",
           &Mesh::Load,
           py::arg("filename"),
           R"doc(Load a mesh from a given file
> filename: Path of the mesh to open)doc")
      .def("save",
           &Mesh::Save,
           py::arg("filename"),
           R"doc(Save the mesh into a file
> filename: Path to location where to save the object)doc")
              // Methods
      .def("check_vertex_duplicate",
           &Mesh::CheckVertexDuplicates,
           py::arg("tol"),
           R"doc(Check if some vertices of the mesh have duplicates)doc")
      .def("fix_triangle_soup",
           &Mesh::FixTriangleSoup,
           py::arg("tol"),
           R"doc(Merge identical vertex and redifine the triangulation accordingly)doc")
      .def("build_connectivity",
           &Mesh::BuildConnectivity,
           R"doc(Compute the connection list for each vertex)doc")
      .def("compute_halfedge",
           &Mesh::ComputeHalfedges,
           R"doc(Compute half-edge data structure)doc")
      .def("compute_vertex_normal",
           &Mesh::ComputeVertexNormal,
           R"doc(Compute normal for each vertex)doc")
      .def("compute_principal_curvatures",
           &Mesh::ComputePrincipalCurvatures,
           R"doc(Compute principal curvature of the surface.
See: "Estimating the tensor of curvature of a surface from a polyhedral
approximation", G. Taubin)doc")
      .def("compute_edge_properties",
              [](Mesh& m) {
                std::vector<vpe_t> vpe;
                std::vector<fpe_t> fpe;
                m.ComputeEdgeProperties(&vpe, &fpe);
                return std::make_tuple(vpe, fpe);
              },
      R"doc(Compute the list of edges for the mesh.
return: tuple: VertexPerEdge, FacePerEdge)doc"
              )
      .def("compute_bbox",
           &Mesh::ComputeBoundingBox,
           R"doc(Compute mesh bounding box)doc")
      .def("transform",
           &Mesh::Transform,
           py::arg("matrix"),
           R"doc(Apply a given transformation (4x4) on each vertex
> matrix: Rigid transform to apply)doc")
      .def("scale",
           &Mesh::Scale,
           py::arg("s"),
           R"doc(Rescale mesh by a given factor
> scale: Scaling factor to apply)doc")
      .def("translate",
           &Mesh::Translate,
           py::arg("t"),
           R"doc("Translate the whole mesh by `t`
> t: Vector of displacement (x, y, z))doc")
      .def("normalize_main_diag",
           &Mesh::NormalizeMainDiagonal,
           R"doc(Scale the whole mesh such that main diagonal of the cube enclosing the bounding box is 1)doc")
      .def("normalize",
           &Mesh::NormalizeMesh,
           R"doc(Scale the whole mesh such that main diagonal is 1 and it's centered around the origin)doc")
      .def("remesh_for_triangle",
           [](Mesh& m, const cv::Mat& subset) {
             const auto* sel = reinterpret_cast<const Tri*>(subset.data);
             std::vector<Tri> tri_subset(sel, sel + subset.rows);
             m.RemeshForTriangles(tri_subset);
           },
           py::arg("subset"),
           R"doc(Given a subset of triangles, update the mesh vertex/"
"normal and triangulation.
> subset: List of selected triangles, must be part of the original
triangulation)doc")
      .def("crop_around_vertex",
           &Mesh::CropAroundVertex,
           "index"_a,
           "distance"_a,
           R"doc(Crop mesh around a given vertex. Triangle that are
further away than a given distance are remove. No vertex are deleted, therefore
the mesh will be degenerated after this step.
> index: Vertex index to crop around
> distance: Radius of the cropping sphere)doc")
              // Properties
      .def_property("vertex",
                    MESH_GETTER(vertex, 3, cv::DataType<T>::type),
                    MESH_SETTER(vertex, 3, Vertex),
                    py::return_value_policy::reference,
                    R"doc(Access or define new set of vertices)doc")
      .def_property("vertex_color",
                    MESH_GETTER(vertex_color, 3,cv::DataType<T>::type),
                    MESH_SETTER(vertex_color, 3, Color),
                    py::return_value_policy::reference,
                    R"doc(Access or define new set of color for each vertex)doc")
      .def_property("normal",
                    MESH_GETTER(normal, 3, cv::DataType<T>::type),
                    MESH_SETTER(normal, 3, Normal),
                    py::return_value_policy::reference,
                    R"doc(Access or define new set of vertex's normal)doc")
      .def_property("tex_coord",
                    MESH_GETTER(tex_coord,2, cv::DataType<T>::type),
                    MESH_SETTER(tex_coord, 2, TCoord),
                    py::return_value_policy::reference,
                    R"doc(Access or define new set of vertex's texture coordinate)doc")
      .def_property("tri",
                    MESH_GETTER(triangle, 3, CV_32SC1),
                    MESH_SETTER(triangle, 3, Triangle),
                    py::return_value_policy::reference,
                    R"doc(Access or define new triangulation)doc")
      .def_property("materials",
                    &Mesh::get_materials,
                    &Mesh::set_materials,
                    py::return_value_policy::reference,
                    R"doc(List of materials)doc")
      .def_property("comments",
                    (getter_cmt)&Mesh::get_comments,
                    &Mesh::set_comments,
                    py::return_value_policy::reference,
                    R"doc(List of comments)doc")
      .def_property("export_binary_ply",
                    &Mesh::get_export_binary_ply,
                    &Mesh::set_export_binary_ply,
                    R"doc(Indicate if `.ply` file will be exported as
 plain text or binary)")
      .def_property_readonly("principal_curvatures",
                             [](const Mesh& m){
                               auto& curv = m.get_principal_curvatures();
                               return cv::Mat(curv.size(),
                                              2,
                                              cv::DataType<T>::type,
                                              (void*)curv.data());
                             },
                             py::return_value_policy::reference,
                             R"doc(Principal curvatures of the surface:
 k1, k2)doc")
      .def_property_readonly("bbox",
                             [](Mesh& m) {
                               if (!m.bbox_is_computed()) {
                                 m.ComputeBoundingBox();
                               }
                               const auto& bbox = m.bbox();
                               return py::make_tuple(bbox.min_.x_,
                                                     bbox.min_.y_,
                                                     bbox.min_.z_,
                                                     bbox.max_.x_,
                                                     bbox.max_.y_,
                                                     bbox.max_.z_);
                             },
                             R"doc(Access the bounding box)doc")

      .def("__repr__",
           [](const Mesh& mesh) {
             int n_vertex = mesh.get_vertex().size();
             int n_normal = mesh.get_normal().size();
             int n_tcoord = mesh.get_tex_coord().size();
             int n_vcolor = mesh.get_vertex_color().size();
             int n_tri = mesh.get_triangle().size();
             std::ostringstream s;
             s << "Mesh properties: ";
             s << "  #Vertex: " << n_vertex;
             s << "  #Normal: " << n_normal;
             s << "  #VColor: " << n_vcolor;
             s << "  #TCoord: " << n_tcoord;
             s << "  #Tri   : " << n_tri;
             return s.str();
           },
           R"doc(Print mesh summary)doc");
  // Enum
  // See: https://pybind11.readthedocs.io/en/stable/classes.html#enumerations-and-internal-types
  py::enum_<PrimType>(mesh, "PrimitiveType")
          .value("kPoint", PrimType::kPoint)
          .value("kTriangle", PrimType::kTriangle)
          .export_values();

#undef MESH_GETTER
#undef MESH_SETTER
}

/*
 * @name    AddMesh
 * @fn  void AddMesh(pybind11::module& m)
 * @brief   Expose mesh class to python
 * @param[in,out] m Module where to expose the class
 */
void AddMesh(pybind11::module& m) {
  ExposeEdgeFunction(m);
  ExposeMaterial<float>(m, "MaterialFloat");
  ExposeMaterial<double>(m, "MaterialDouble");
  ExposeMesh<float>(m, "MeshFloat");
  ExposeMesh<double>(m, "MeshDouble");
}

}  // namepsace Python
}  // namepsace LTS5
