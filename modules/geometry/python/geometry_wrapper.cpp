/**
 *  @file   geometry_wrapper.cpp
 *  @brief  Python wrapper for lts5 geometry module
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   08/11/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <memory>
#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"

#include "pybind11/pybind11.h"

#include "lts5/python/ocv_converter.hpp"
#include "mesh_wrapper.hpp"
#include "tree_wrapper.hpp"
#include "ambient_occlusion_wrapper.hpp"


namespace py = pybind11;

PYBIND11_MODULE(pygeometry, m) {

  // Init numpy
  LTS5::Python::InitNumpyArray();

  // Doc
  m.doc() = R"doc(LTS5 Geometry modules)doc";

  // Expose mesh
  LTS5::Python::AddMesh(m);
  // Expose OCTree
  LTS5::Python::AddTree(m);
  // Expose Ambient Occlusion
  LTS5::Python::AddAmbientOcclusion(m);
}
