/**
 *  @file   discrete_remeshing.cpp
 *  @brief  Uniform discrete mesh remeshing
 *          Based on : https://github.com/valette/ACVD
 *
 *          Approximated Centroidal Voronoi Diagrams for Uniform Polygonal Mesh
 *          S. Vallette et al.
 *
 *  @author Christophe Ecabert
 *  @date   11/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <numeric>

#include "lts5/geometry/discrete_remeshing.hpp"
#include "lts5/utils/file_io.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  IsotropicDiscreteRemesher
 * @fn    IsotropicDiscreteRemesher(void)
 * @brief Constructor
 */
template<typename T>
IsotropicDiscreteRemesher<T>::IsotropicDiscreteRemesher(void) : mesh_(nullptr),
                                                                n_cluster_(0),
                                                                metric_(nullptr),
                                                                n_loop_(0),
                                                                relative_n_loop_(0),
                                                                n_max_loop_(5000000),
                                                                n_max_convergence_(1000000000),
                                                                log_(false) {
  metric_ = new IsotropicClusteringMetric();
}

/*
 * @name  ~IsotropicDiscreteRemesher
 * @fn    ~IsotropicDiscreteRemesher(void)
 * @brief Destructor
 */
template<typename T>
IsotropicDiscreteRemesher<T>::~IsotropicDiscreteRemesher(void) {
  if (metric_) {
    delete metric_;
    metric_ = nullptr;
  }
}

/*
 * @name  Initialize
 * @fn    void Initialize(const Mesh<T>* mesh, const int number_cluster)
 * @brief Initialize remesher
 * @param[in] mesh            Mesh to resample
 * @param[in] number_cluster  Number of cluster wanted (i.e. number of vertex)
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int IsotropicDiscreteRemesher<T>::Initialize(const Mesh<T>* mesh,
                                             const int number_cluster) {
  // Mesh input
  mesh_ = mesh;
  // Define number of cluser
  n_cluster_ = number_cluster;
  // Metric
  int err = metric_->Build(mesh, n_cluster_);
  // Build edge list
  this->InitEdgeList(mesh);
  // Set container
  cluster_size_.resize(static_cast<size_t>(n_cluster_));
  cluster_is_freezed_.SetSize(static_cast<size_t>(n_cluster_));
  clustering_.resize(mesh->get_vertex().size());
  cluster_last_modification_.resize(static_cast<size_t>(n_cluster_));
  edge_last_loop_.resize(edge_.size());
  //Done
  return err;
}

#pragma mark -
#pragma mark Process

/*
 * @name  Process
 * @fn    void Process(LTS5::Mesh<T>* mesh)
 * @brief Remesh
 * @param[out] mesh    Resampled mesh
 * @return -1 if error, 0 otherwise
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::Process(LTS5::Mesh<T>* mesh) {
  // Init container
  this->InitProcess();
  // Init clusters
  this->InitCluster();
  // Optimize cluster's energy
  this->n_convergence_ = 0;
  this->MinimizeEnergy();
  // Triangulate
  this->Triangulate(mesh);
}

#pragma mark -
#pragma mark Private

/*
 * @name  InitProcess
 * @fn    void InitProcess(void)
 * @brief Reinitialiaze module in ordre to process a new meshs
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::InitProcess(void) {
  // Reset cluster size + freezed
  for (size_t i = 0; i < n_cluster_; ++i) {
    cluster_size_[i] = 0;
    cluster_is_freezed_.Reset(i);
  }
  // Clear edge queue
  while(!edge_queue_.empty()) {
    edge_queue_.pop();
  }
  // Reset edge last loop
  for (int i = 0; i < edge_.size(); ++i) {
    edge_last_loop_[i] = 0;
  }
  // Clear loop counter
  this->relative_n_loop_ = 1;
  this->n_loop_ = 0;
}

/*
 * @name  InitEdgeList
 * @fn    int InitEdgeList(const Mesh<T>* mesh)
 * @brief Setup a list of edge, and list of face adjacent to an edge
 * @param[in] mesh Mesh from where to build edge list
 * @return -1 if error, 0 otherwise
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::InitEdgeList(const Mesh<T>* mesh) {
  const auto& hedge = mesh->get_halfedges();
  const auto& vertex = mesh->get_vertex();
  vertex_edge_list_.resize(vertex.size(), std::vector<int>(0));
  // Note : hedge[i].origin = index of the vertex where this halfedge come from
  edge_.clear();
  edge_face_.clear();
  std::vector<uchar> visited_hedge(hedge.size(), 0);
  // Loop over all half edge
  for (int i = 0; i < hedge.size(); ++i) {
    const auto& he = hedge[i];
    const int sz = static_cast<int>(edge_.size());
    if(he.IsBoundary()) {
      // Boundary edge, add it automatically
      const int idx = ((i + 1) % 3) == 0 ? i - 2 : i + 1 ;
      edge_.push_back({he.origin, hedge[idx].origin});
      edge_face_.push_back({i/3,-1});
      visited_hedge[i] = 1;
      // update vertex-edge list
      vertex_edge_list_[he.origin].push_back(sz);
      vertex_edge_list_[hedge[idx].origin].push_back(sz);
    } else {
      // Has edge already been selected ?
      if (visited_hedge[i] == 0 && visited_hedge[he.opposite] == 0) {
        // New edge
        edge_.push_back({he.origin, hedge[he.opposite].origin});
        edge_face_.push_back({i/3, he.opposite/3});
        visited_hedge[i] = 1;
        visited_hedge[he.opposite] = 1;
        // update vertex-edge list
        vertex_edge_list_[he.origin].push_back(sz);
        vertex_edge_list_[hedge[he.opposite].origin].push_back(sz);
      }
    }
  }
}

/*
 * @name  InitCluster
 * @fn    void InitCluster(void)
 * @brief Initialize clustering with random selection
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::InitCluster(void) {
  // Define value to cluser
  const int c_size = static_cast<int>(clustering_.size());
  for (auto& c : clustering_) {
    c = this->n_cluster_;
  }
  // Compute Random initial sampling
  std::vector<int> items(clustering_.size());
  int n_remaining_item = c_size;
  int n_remaining_region = this->n_cluster_;
  bool found;
  std::queue<int> item_queue;

  // Shuffle item ordering
  std::iota(items.begin(), items.end(), 0);
  std::random_shuffle(items.begin(), items.end());

  // Compute total weight
  T sweight = T(0.0);
  for (int i = 0; i < n_remaining_item; ++i) {
    sweight += metric_->get_cluster_data_weight(i);
  }
  // Compute desired average weight
  T weight = sweight / static_cast<T>(n_remaining_region);

  // Start
  int first_item = 0;
  while ((n_remaining_item > 0) && (n_remaining_region > 0)) {
    found = false;
    int item = 0;
    while(!found && first_item < c_size) {
      // Search for unselected item (defined as clutering_[k] == n_cluster)
      item = items[first_item];
      if (clustering_[item] == this->n_cluster_) {
        found = true;
      } else {
        first_item++;
      }
    }
    // Found a valid item ?
    if (found) {
      // Clear item queue
      while (!item_queue.empty()) {
        item_queue.pop();
      }
      // Add new item
      item_queue.push(item);
      sweight = T(0.0);
      n_remaining_region--;
      // Loop over item queue
      while (!item_queue.empty()) {
        // Get item at the begining and remove from the queue
        item = item_queue.front();
        item_queue.pop();
        // Already picked ?
        if (clustering_[item] == this->n_cluster_) {
          // Select it
          clustering_[item] = n_remaining_region;
          // Add contribution to cluster
          sweight += metric_->get_cluster_data_weight(item);
          n_remaining_item--;
          // Look for neighboring vertex
          std::vector<size_t> neighbors;
          mesh_->OneRingNeighbors(item, &neighbors);
          // Add them to the queue
          for (int i = 0; i < neighbors.size(); ++i) {
            item_queue.push(static_cast<int>(neighbors[i]));
          }
          // Check if already enough item in the region
          if (sweight > weight) {
            break;
          }
        }
      }
    }
  }
  // Remain some region
  if (n_remaining_region) {
    for (int i = 0; i < this->n_cluster_; ++i) {
      cluster_size_[i] = 0;
    }
    // shuffle
    for (int i = 0; i < clustering_.size(); ++i) {
      int cluster = clustering_[i];
      if (cluster != this->n_cluster_) {
        cluster_size_[cluster] += 1;
      }
      items[i] = i;
    }
    std::random_shuffle(items.begin(), items.end());
    // Loop again
    first_item = 0;
    while (n_remaining_region > 0) {
      found = false;
      int item = -1;
      while (!found) {
        item = items[first_item];
        first_item++;
        int cluster = clustering_[item];
        if (cluster == this->n_cluster_) {
          found = true;
        } else {
          if (cluster_size_[cluster] > 1) {
            found = true;
            clustering_[item] = n_remaining_region;
            cluster_size_[cluster] -= 1;
          }
        }
      }
      n_remaining_region--;
      clustering_[item] = n_remaining_region;
    }
  }
}

/**
 * @name  MinimizeEnergy
 * @fn    void MinimizeEnergy(void)
 * @brief Do the actual clustering
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::MinimizeEnergy(void) {
  // Counter
  int n_modifications = 0;
  int n_disconnected_cluster = 0;
  // Init
  this->FillHolesInClustering();
  this->FillQueuesFromClustering();
  this->RecomputeStatistics();
  this->SetAllClustersToModified();

  // Cluster
  while (true) {
    // Do one iteration
    n_modifications = this->ProcessOneLoop();

    if (log_) {
      std::cout <<"Loop "<<this-> n_loop_ << ", "<< n_modifications;
      std::cout << " Modifications"<<std::endl;
    }

    // Cnt
    this->n_loop_++;
    if (this->relative_n_loop_ == 0xFF) {
      for (int i = 0; i < edge_last_loop_.size(); ++i) {
        edge_last_loop_[i] = 0;
      }
      this->relative_n_loop_ = 1;
    } else {
      this->relative_n_loop_++;
    }

    // Check if need to leave
    if ((n_modifications == 0) ||
        (this->n_loop_ > this->n_max_loop_) ||
        ((n_modifications < (clustering_.size()/1000)) &&
         (this->n_convergence_ == 0))) {
      if (log_ && n_modifications) {
        std::cout << "Triggering early convergence for speed inc." << std::endl;
      }
      this->n_convergence_++;
      n_disconnected_cluster = this->CleanClustering();
      this->FillHolesInClustering();
      this->RecomputeClusterSize();

      // Stop
      if ((n_disconnected_cluster == 0) && (n_modifications == 0)) {
        break;
      }

      // Maximum iteration reached ?
      if ((this->n_loop_ >= this->n_max_loop_) ||
          (this->n_convergence_ >= this->n_max_convergence_)) {
        break;
      }
      this->RecomputeStatistics();
      this->FillQueuesFromClustering();
      this->SetAllClustersToModified();
    }
  }
}

/*
 * @name  FillHolesInClustering
 * @fn    void FillHolesInClustering(void)
 * @brief Expand clusters
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::FillHolesInClustering(void) {
  std::queue<int> item_queue;
  int cluster1, cluster2;
  size_t temp_value;
  int init_n_problem = 0, n_problem = 0;

  // Count how many item are not clustered
  for (int i = 0; i < clustering_.size(); ++i) {
    cluster1 = clustering_[i];
    if (cluster1 < 0 || cluster1 >= this->n_cluster_) {
      init_n_problem++;
    }
  }
  // Loop over all edges
  for (int i = 0; i < edge_.size(); ++i) {
    const auto& e = edge_[i];
    if (e.first != -1 && e.second != -1) {
      // Get cluster for each end of this edge
      cluster1 = clustering_[e.first];
      cluster2 = clustering_[e.second];
      if ((cluster1 < 0) || (cluster1 >= this->n_cluster_)) {
        if ((cluster2 >= 0) || (cluster2 < this->n_cluster_)) {
          // item not clustered, add to the queue
          item_queue.push(i);
        }
      } else {
        if ((cluster2 < 0) || (cluster2 >= this->n_cluster_)) {
          // item not clustered, add to the queue
          item_queue.push(i);
        }
      }
    }
  }
  while (!item_queue.empty()) {
    int e_idx = item_queue.front();
    item_queue.pop();
    auto& e = edge_[e_idx];
    if (e.first != -1 && e.second != -1) {
      cluster1 = clustering_[e.first];
      cluster2 = clustering_[e.second];

      if (cluster1 == this->n_cluster_) {
        temp_value = e.first;
        e.first = e.second;
        e.second = temp_value;
        cluster1 = cluster2;
        cluster2 = this->n_cluster_;
      }

      if ((cluster1 != this->n_cluster_) && (cluster2 == this->n_cluster_)) {
        clustering_[e.second] = cluster1;
        const auto& neighbors = vertex_edge_list_[e.second];
        for (int i = 0; i < neighbors.size(); ++i) {
          item_queue.push(neighbors[i]);
        }
      }
    }
  }

  // Count issues
  n_problem = 0;
  for (int i = 0; i < clustering_.size(); ++i) {
    cluster1 = clustering_[i];
    if (cluster1 < 0 || cluster1 >= this->n_cluster_) {
      n_problem++;
    }
  }

  if (log_) {
    std::cout << "The number of uncorrectly assigned vertex was reduced from ";
    std::cout <<init_n_problem<< " to " <<n_problem << " problems" << std::endl;
  }
}

/*
 * @name  FillQueuesFromClustering
 * @fn    void FillQueuesFromClustering(void)
 * @brief Fill up edge queue with edges belgong to two different clusters
 *        (generally null cluster, i.e. unassigned cluster)
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::FillQueuesFromClustering(void) {
  // Clear queue
  while (!edge_queue_.empty()) {
    edge_queue_.pop();
  }
  // Fill with new unassigned edge
  int cluster1, cluster2;
  for (int i = 0; i < edge_.size(); ++i) {
    const auto& e = edge_[i];
    if (e.second != -1) {
      cluster1 = clustering_[e.first];
      cluster2 = clustering_[e.second];
      if (cluster1 != cluster2) {
        edge_queue_.push(static_cast<size_t>(i));
      }
    }
  }
  // Flag the end
  edge_queue_.push(static_cast<size_t>(-1));
}

/*
 * @name    RecomputeStatistics
 * @fn      void RecomputeStatistics(void)
 * @brief   Add vertex to clusters and compute cluster's centroid + energy
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::RecomputeStatistics(void) {
  // Update cluster size
  this->RecomputeClusterSize();
  // Reset cluster's data
  for (int i = 0; i < this->n_cluster_; ++i) {
    metric_->Reset(i);
  }
  // Loop over every item and update cluster centroid/energy
  for (int i = 0; i < clustering_.size(); ++i) {
    int cluster = clustering_[i];
    if (cluster >= 0 && cluster < this->n_cluster_) {
      metric_->AddDataToCluster(i, cluster);
    } else {
      if (log_ && cluster != this->n_cluster_) {
        std::cout << "Error, vertex " << i << " belongs to cluster ";
        std::cout << cluster << std::endl;
      }
    }
  }
  for (int i = 0; i < this->n_cluster_; ++i) {
    metric_->ComputeClusterCentroid(i);
    metric_->ComputeClusterEnergy(i);
    if (log_ && cluster_size_[i] == 0) {
      std::cout << "Cluster " << i << " is empty" << std::endl;
    }
  }
}

/*
 * @name  SetAllClustersToModified
 * @fn    void SetAllClustersToModified(void)
 * @brief Update when cluster have beed changed for the last time
 *        (i.e. with current iteration loop number).
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::SetAllClustersToModified(void) {
  for (int i = 0; i < this->n_cluster_; ++i) {
    this->cluster_last_modification_[i] = this->n_loop_;
  }
}

/*
 * @name  ProcessOneLoop
 * @fn    int ProcessOneLoop(void)
 * @brief Minimize clustering enery for one iteration
 * @return Number of item changed
 */
template<typename T>
int IsotropicDiscreteRemesher<T>::ProcessOneLoop(void) {

  int n_modification = 0;
  int val1, val2;
  size_t edge;
  using Cluster = typename IsotropicClusteringMetric::Cluster;
  Cluster cluster21, cluster22, cluster31, cluster32;
  double try1, try2, try3;
  double try11, try12, try21, try22, try31, try32;
  while (true) {
    edge = edge_queue_.front();
    edge_queue_.pop();
    // reach end ?
    if (edge == static_cast<size_t>(-1)) {
      break;
    }
    // Nope still some work to do
    auto& e = edge_[edge];
    // Check if	this edge was not already visited.
    if ((this->edge_last_loop_[edge] != this->relative_n_loop_) &&
        (e.second >= 0)) {
      // Mark it as visited
      this->edge_last_loop_[edge] = this->relative_n_loop_;
      val1 = clustering_[e.first];
      val2 = clustering_[e.second];
      if (val1 != val2) {
        if (val1 == this->n_cluster_) {
          // I1 is not associated. Give it to the same cluster as I2
          metric_->AddDataToCluster(static_cast<int>(e.first), val2);
          metric_->ComputeClusterCentroid(val2);
          metric_->ComputeClusterEnergy(val2);
          // Increase cluster size
          cluster_size_[val2] += 1;
          this->AddItemRingToProcess(static_cast<int>(e.first));
          n_modification++;
          clustering_[e.first] = val2;
          this->cluster_last_modification_[val2] = this->n_loop_;
        } else if (val2 == this->n_cluster_) {
          // I2 is not associated. Give it to the same cluster as I1
          metric_->AddDataToCluster(static_cast<int>(e.second), val1);
          metric_->ComputeClusterCentroid(val1);
          metric_->ComputeClusterEnergy(val1);
          // Increase cluster size
          cluster_size_[val1] += 1;
          this->AddItemRingToProcess(static_cast<int>(e.second));
          n_modification++;
          clustering_[e.second] = val1;
          this->cluster_last_modification_[val1] = this->n_loop_;
        }
        // determine whether one of	the	two	adjacent clusters was modified,
        // or whether any of the clusters is freezed
        //	If not,	the	test is	useless, and the speed improved	:)
        else if (((cluster_last_modification_[val1] >= this->n_loop_ - 1) ||
                  (cluster_last_modification_[val2] >= this->n_loop_ - 1)) &&
                 ((!cluster_is_freezed_.Get(static_cast<size_t>(val1))) &&
                 (!cluster_is_freezed_.Get(static_cast<size_t>(val2))))) {
          int result = 1;
          int cluster1 = val1;
          int cluster2 = val2;

          int sz1 = cluster_size_[val1];
          int sz2 = cluster_size_[val2];
          // Compute the initial energy
          try11 = metric_->get_cluster_energy(cluster1);
          try12 = metric_->get_cluster_energy(cluster2);
          try1 = try11 + try12;

          // Compute the energy when setting I1 to the same cluster as I2;
          if (sz1 == 1) {
            try2 = 100000000.0;
          } else {
            metric_->Sub(cluster1, static_cast<int>(e.first), &cluster21);
            metric_->Add(cluster2, static_cast<int>(e.first), &cluster22);
            cluster21.ComputeEnergy();
            cluster22.ComputeEnergy();
            try21 = cluster21.energy;
            try22 = cluster22.energy;
            try2 = try21 + try22;
          }

          // Compute the energy when setting I2 to the same cluster as I1;
          if (sz2 == 1) {
            try3 = 1000000000.0;
          } else {
            metric_->Sub(cluster2, static_cast<int>(e.second), &cluster32);
            metric_->Add(cluster1, static_cast<int>(e.second), &cluster31);
            cluster31.ComputeEnergy();
            cluster32.ComputeEnergy();
            try31 = cluster31.energy;
            try32 = cluster32.energy;
            try3 = try31 + try32;
          }
          // Search for min energy
          if ((try1 < try2) && (try1 < try3)) {
            result = 1;
          } else if ((try2 < try1) && (try2 < try3)) {
            result = 2;
          } else {
            result = 3;
          }

          switch (result) {
            case 1: {
              //Don't	do anything!
              edge_queue_.push(edge);
            }
              break;

            case 2: {
              // Set I1 in the same cluster as I2
              clustering_[e.first] = val2;
              cluster_size_[val2] += 1;
              cluster_size_[val1] -= 1;
              metric_->Copy(cluster21, cluster1);
              metric_->Copy(cluster22, cluster2);
              this->AddItemRingToProcess(static_cast<int>(e.first));
              n_modification++;
              this->cluster_last_modification_[val1] = this->n_loop_;
              this->cluster_last_modification_[val2] = this->n_loop_;
            }
              break;

            case 3: {
              // Set I2 in the same cluster as I1
              clustering_[e.second] = val1;
              cluster_size_[val1] += 1;
              cluster_size_[val2] -= 1;
              metric_->Copy(cluster31, cluster1);
              metric_->Copy(cluster32, cluster2);
              this->AddItemRingToProcess(static_cast<int>(e.second));
              n_modification++;
              this->cluster_last_modification_[val1] = this->n_loop_;
              this->cluster_last_modification_[val2] = this->n_loop_;
            }
              break;
            default: std::cout << "Should not arrived here !" << std::endl;
              break;
          }
        } else {
          // Do nothing
          edge_queue_.push(edge);
        }
      }
    }
  }
  edge_queue_.push(static_cast<size_t>(-1));
  return n_modification;
}

template<typename T>
void IsotropicDiscreteRemesher<T>::AddItemRingToProcess(const int idx) {
  const auto& neighbors = vertex_edge_list_[idx];
  for (int i = 0; i < neighbors.size(); ++i) {
    this->edge_queue_.push(static_cast<size_t>(neighbors[i]));
  }
}

template<typename T>
int IsotropicDiscreteRemesher<T>::CleanClustering(void) {
  std::vector<std::vector<int>> clusters;
  std::vector<int> sizes;
  std::vector<char> visited;
  std::vector<int> visited_clusters;
  int number = 0, size, type, i1, i2;
  std::vector<size_t> neighbors;
  // Init container
  std::queue<int> queue;
  size_t cluster_sz = static_cast<size_t>(this->n_cluster_);
  clusters.resize(cluster_sz,
                  std::vector<int>(0));
  sizes.resize(cluster_sz, 0);
  visited.resize(clustering_.size(), 0);
  visited_clusters.resize(cluster_sz, 0);

  // Detect clusters that	have several connected components
  for (int i = 0; i < clustering_.size(); ++i) {
    // clear queue
    while (!queue.empty()) {
      queue.pop();
    }

    if ((visited[i] == 0) &&
            (clustering_[i] != this->n_cluster_)) {
      size = 0;
      queue.push(i);
      type = clustering_[i];
      while (!queue.empty()) {
        i1 = queue.front();
        queue.pop();
        if (visited[i1] == 0) {
          ++size;
          visited[i1] = 1;
          // Get neighbors - OneRing vertex ?
          mesh_->OneRingNeighbors(static_cast<size_t>(i1), &neighbors);
          for (int j = 0; j < neighbors.size(); ++j) {
            i2 = static_cast<int>(neighbors[j]);
            if (visited[i2] == 0 && clustering_[i2] == type) {
              queue.push(i2);
            }
          }
        }
      }
      if (type != this->n_cluster_) {
        if (visited_clusters[type] == 0) {
          // First connected component
          visited_clusters[type] = i;
          sizes[type] = size;
        } else {
          // The cluster has an other connected component
          if (clusters[type].empty()) {
            clusters[type].push_back(visited_clusters[type]);
            clusters[type].push_back(sizes[type]);
          }
          clusters[type].push_back(i);
          clusters[type].push_back(size);
        }
      }
    }
  }

  for (int i = 0; i < clustering_.size(); ++i) {
    visited[i] = 0;
  }
  // Reset the smallest unconnected components to	NULLCLUSTER
  int size_max, i_max = 0;
  for (int i = 0; i< this->n_cluster_; ++i) {
    if (!clusters[i].empty()) {
      number++;
      size_max = 0;
      // Detect for each cluster, the biggest connected component;
      for (int j = 0; j < clusters[i].size()/2; ++j) {
        if (size_max < clusters[i][(2 * j) + 1]) {
          size_max = clusters[i][(2 * j) + 1];
          i_max = j;
        }
      }
      // Reset the smallest components to -1
      for (int j = 0; j < clusters[i].size()/2; ++j) {
        if (j != i_max) {
          // Empty queue
          while (!queue.empty()) {
            queue.pop();
          }
          queue.push(clusters[i][2 * j]);
          type = clustering_[clusters[i][2 * j]];
          while (!queue.empty()) {
            i1 = queue.front();
            queue.pop();
            if (visited[i1] == 0) {
              visited[i1] = 1;
              clustering_[i1] = this->n_cluster_;
              mesh_->OneRingNeighbors(static_cast<size_t>(i1), &neighbors);
              for (int k = 0; k < neighbors.size(); ++k) {
                i2 = static_cast<int>(neighbors[k]);
                if (clustering_[i2] == type) {
                  queue.push(i2);
                }
              }
            }
          }
        }
      }
    }
  }
  return number;
}

/*
 * @name  RecomputeClusterSize
 * @fn    void RecomputeClusterSize(void)
 * @brief Update cluster size counter
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::RecomputeClusterSize(void) {
  // Reset
  for (auto& sz : cluster_size_) {
    sz = 0;
  }
  // Update
  for (int i = 0; i < clustering_.size(); ++i) {
    int cluster = clustering_[i];
    if (cluster >= 0 && cluster < this->n_cluster_) {
      cluster_size_[cluster] += 1;
    } else {
      if (log_ && cluster != this->n_cluster_) {
        std::cout << "Error, vertex " << i << " belongs to cluster ";
        std::cout << cluster << std::endl;
      }
    }
  }
}

/**
 * @name  Triangulate
 * @fn    void Triangulate(const LTS5::Mesh<T>* mesh)
 * @brief Define mesh from clustering
 * @param[out] mesh   Resampled mesh
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::Triangulate(LTS5::Mesh<T>* mesh) {
  using Vertex = typename LTS5::Mesh<T>::Vertex;
  using Triangle = typename LTS5::Mesh<T>::Triangle;
  // get vertex + normal
  auto& vertex = mesh->get_vertex();
  vertex.clear();
  auto& triangle = mesh->get_triangle();
  triangle.clear();
  // Find the first non-empty cluster and put its index in Valid
  int valid = 0;
  for (int i = 0; i < this->n_cluster_; ++i) {
    if (cluster_size_[i] > 0) {
      valid = i;
      break;
    }
  }
  // Compute the vertices as inertia centers of each Cluster
  Vector3<double> v;
  for (int i = 0; i < this->n_cluster_; ++i) {
    if (cluster_size_[i] == 0) {
      metric_->GetClusterCentroid(valid, &v);
    } else {
      metric_->GetClusterCentroid(i, &v);
    }
    Vertex v_typed = Vertex(v.x_, v.y_, v.z_);
    vertex.push_back(v_typed);
  }
  // Loop overall initial triangle
  const auto& init_triangle = mesh_->get_triangle();
  size_t n_dual_item = init_triangle.size();
  for (int i = 0; i < n_dual_item; ++i) {
    const Triangle tri = init_triangle[i];
    const int* tri_ptr = &tri.x_;
    std::vector<int> vertex_list;
    for (int j = 0; j < 3; ++j) {
      int cluster = clustering_[tri_ptr[j]];
      if (cluster != this->n_cluster_) {
        if (vertex_list.empty()) {
          vertex_list.push_back(cluster);
        } else {
          const auto it = std::find(vertex_list.begin(),
                                    vertex_list.end(),
                                    cluster);
          if (it == vertex_list.end()) {
            // Not in the set, can add
            vertex_list.push_back(cluster);
          }
        }
      }
    }
    if (vertex_list.size() == 3) {
      triangle.push_back(Triangle(vertex_list[0],
                                  vertex_list[1],
                                  vertex_list[2]));
    }
  }
}

/*
 * @name  FixBoundaries
 * @fn    void FixBoundaries(LTS5::Mesh<T>* mesh)
 * @brief Fix mesh boundaries
 * @param[in,out] mesh    Resampled mesh
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::FixBoundaries(LTS5::Mesh<T>* mesh) {
  using Vertex = typename LTS5::Mesh<T>::Vertex;
  using Triangle = typename LTS5::Mesh<T>::Triangle;
  const auto& init_vertex = mesh_->get_vertex();
  auto& vertex = mesh->get_vertex();
  auto& triangle = mesh->get_triangle();

  std::vector<int> edge_new_point(edge_.size(), -2);
  // Loop over all edges
  for (int i = 0; i < edge_.size(); ++i) {
    const auto& e = edge_[i];
    const auto& f = edge_face_[i];
    if (f.second == -1) {
      // Edge is on boundary
      int c1 = clustering_[e.first];
      int c2 = clustering_[e.second];
      if (c1 != c2) {
        Vertex v1 = init_vertex[e.first];
        Vertex v2 = init_vertex[e.second];
        Vertex v = (v1 + v2) * T(0.5);
        // create a new vertex in the resampled mesh
        edge_new_point[i] = static_cast<int>(vertex.size());
        vertex.push_back(v);
        // create a new triangle (NewVertex,Cluster1,Cluster2);
        triangle.push_back(Triangle(edge_new_point[i], c1, c2));
      }
    }
  }
  for (int i = 0; i < edge_.size(); ++i) {
    if (edge_new_point[i] < 0) {
      // the edge was not visited previously
      const auto& f = edge_face_[i];
      if (f.second != -1) {
        // the edge is a boundary edge
        // with two vertices belonging to the same cluster
        std::queue<int> equeue;
        std::vector<int> elist;

        const auto& e = edge_[i];
        int cluster = clustering_[e.first];
        equeue.push(i);
        while (!equeue.empty()) {
          int edge = equeue.front();
          equeue.pop();
          switch (edge_new_point[edge]) {
            case -2 : {
              // this edge was never visited, so continue conquest
              edge_new_point[edge] = -1;
              for (const auto& vedge : vertex_edge_list_[e.first]) {
                // Check if edge is on boundary
                const auto& ff = edge_face_[vedge];
                if (ff.second == -1) {
                  equeue.push(vedge);
                }
              }
              for (const auto& vedge : vertex_edge_list_[e.second]) {
                // Check if edge is on boundary
                const auto& ff = edge_face_[vedge];
                if (ff.second == -1) {
                  equeue.push(vedge);
                }
              }
            }
              break;

            case -1:
              break;

            default: {
              // Insert unique
              if (elist.empty()) {
                elist.push_back(edge);
              } else {
                const auto it = std::find(elist.begin(), elist.end(), edge);
                if (it == elist.end()) {
                  elist.push_back(edge);
                }
              }
            }
              break;
          }
        }
        if (elist.size() >= 2) {
          triangle.push_back(Triangle(cluster,
                                      edge_new_point[elist[0]],
                                      edge_new_point[elist[1]]));
        }
      }
    }
  }
  // fix when two neighbour edges actually have several clusters
  for (int i = 0; i < init_vertex.size(); ++i) {
    int n_boundary;
    this->GetNumberOfBoundary(i, &n_boundary);
    if (n_boundary > 0) {
      std::vector<int> elist;
      const auto& v_edge = vertex_edge_list_[i];
      for (const auto& e : v_edge) {
        if (edge_new_point[e] >= 0) {
          if (elist.empty()) {
            elist.push_back(edge_new_point[e]);
          } else {
            const auto it = std::find(elist.begin(),
                                      elist.end(),
                                      edge_new_point[e]);
            if (it == elist.end()) {
              elist.push_back(edge_new_point[e]);
            }
          }
        }
      }
      if (elist.size() > 1) {
        triangle.push_back(Triangle(clustering_[i],
                                    elist[0],
                                    elist[1]));
      }
    }
  }
}

/*
 * @name  GetNumberOfBoundary
 * @fn    void GetNumberOfBoundary(const int vertex_idx, int* boundary)
 * @brief Compute how many edges from a specific vertex are on the mesh
 *        boundary
 * @param[in] vertex_idx
 * @param[out] boundary
 */
template<typename T>
void IsotropicDiscreteRemesher<T>::GetNumberOfBoundary(const int vertex_idx,
                                                       int* boundary) {
  *boundary = 0;
  for (const auto& e : vertex_edge_list_[vertex_idx]) {
    const auto& f = edge_face_[e];
    if (f.second == -1) {
      *boundary += 1;
    }
  }
}

#pragma mark -
#pragma mark Declaration
/** Float */
template class IsotropicDiscreteRemesher<float>;
/** Double */
template class IsotropicDiscreteRemesher<double>;

}  // namepsace LTS5
