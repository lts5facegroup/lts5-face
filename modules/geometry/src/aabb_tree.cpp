/**
 *  @file   aabb_tree.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   24/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <limits>
#include <cstring>
#include <stack>

#include "lts5/geometry/aabb_tree.hpp"
#include "lts5/geometry/intersection_utils.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  AABBTree
 * @fn  AABBTree(void)
 * @brief Constructor
 */
template<typename T>
AABBTree<T>::AABBTree(void) : data_bbox_(),
                              root_node_(nullptr),
                              need_build_(false) {
}

/*
 * @name  AABBTree
 * @fn  AABBTree(const Mesh<T>& mesh)
 * @brief Constructor
 * @param[in] mesh  3D mesh used to build tree
 */
template<typename T>
AABBTree<T>::AABBTree(const Mesh<T>& mesh) : data_bbox_(),
                                                root_node_(nullptr),
                                                need_build_(false) {
  // Add the given range
  this->Insert(mesh);
}

/*
 * @name  ~AABBTree
 * @fn  ~AABBTree(void)
 * @brief Destructor
 */
template<typename T>
AABBTree<T>::~AABBTree(void) {
  // Release ressources
  this->Clear();
}

/*
 * @name  Insert
 * @fn  void Insert(const Mesh<T>& mesh)
 * @brief Insert a range of element into the tree
 * @param[in] mesh  3D mesh used to build tree
 */
template<typename T>
void AABBTree<T>::Insert(const Mesh<T>& mesh) {
  // Get vertex + triangles
  const std::vector<Vertex>& vertex = mesh.get_vertex();
  const std::vector<Triangle>& tri = mesh.get_triangle();

  // Loop over all triangles
  int idx = 0;
  auto first_triangle = tri.begin();
  auto last_triangle = tri.end();
  while(first_triangle != last_triangle) {
    // Compute triangle bounding box
    const int* tri_ptr =  &first_triangle->x_;
    T xmin = std::numeric_limits<T>::max();
    T xmax = std::numeric_limits<T>::lowest();
    T ymin = std::numeric_limits<T>::max();
    T ymax = std::numeric_limits<T>::lowest();
    T zmin = std::numeric_limits<T>::max();
    T zmax = std::numeric_limits<T>::lowest();
    for (int i = 0; i < 3; ++i) {
      const Vertex& v = vertex[tri_ptr[i]];
      xmin = v.x_ < xmin ? v.x_ : xmin;
      xmax = v.x_ > xmax ? v.x_ : xmax;
      ymin = v.y_ < ymin ? v.y_ : ymin;
      ymax = v.y_ > ymax ? v.y_ : ymax;
      zmin = v.z_ < zmin ? v.z_ : zmin;
      zmax = v.z_ > zmax ? v.z_ : zmax;
    }
    data_bbox_.push_back(AABB<T>(xmin, xmax, ymin, ymax, zmin, zmax, idx));
    ++first_triangle;
    ++idx;
  }
  need_build_ = true;
}

/*
 * @name  Clear
 * @fn  void Clear(void)
 * @brief Clear the whole tree
 */
template<typename T>
void AABBTree<T>::Clear(void) {
  // Clear AABB
  this->ClearNodes();
  data_bbox_.clear();
}

/*
 * @name  Build
 * @fn   void Build(void)
 * @brief Build recursively the whole tree
 */
template<typename T>
void AABBTree<T>::Build(void) {
  // Clear node
  this->ClearNodes();
  // Build tree if data are present
  if (data_bbox_.size() > 1) {
    // Allocate nodes
    root_node_ = new AABBNode<T>[data_bbox_.size() - 1];
    if (root_node_) {
      // Start to build tree
      root_node_->first_ = 0;
      root_node_->last_ = data_bbox_.size();
      std::stack<AABBNode<T>*> stack;
      stack.push(root_node_);
      while(!stack.empty()) {
        // Get current node
        AABBNode<T>* node = stack.top();
        stack.pop();
        // Expand
        // ------------------------------------------------------------------
        // Compute node bbox
        node->bbox_ = AABB<T>::ObjectBoundingBox(&data_bbox_[node->first_],
                                                 &data_bbox_[node->last_]);
        // Sort bounding box along the longest aabb axis
        std::size_t mid = node->first_ + ((node->last_ - node->first_) / 2);
        switch(AABB<T>::LongestAxis(node->bbox_)) {
          // Along X axis
          case AABB<T>::AxisType::kX : {
            std::nth_element(&data_bbox_[node->first_],
                             &data_bbox_[mid],
                             &data_bbox_[node->last_],
                             AABB<T>::LessX);
          }
            break;
          // Along X axis
          case AABB<T>::AxisType::kY : {
            std::nth_element(&data_bbox_[node->first_],
                             &data_bbox_[mid],
                             &data_bbox_[node->last_],
                             AABB<T>::LessY);
          }
            break;
          // Along Z axis
          case AABB<T>::AxisType::kZ : {
            std::nth_element(&data_bbox_[node->first_],
                             &data_bbox_[mid],
                             &data_bbox_[node->last_],
                             AABB<T>::LessZ);
          }
            break;
          default:
            std::cout << "Error, unknown direction" << std::endl;
        }
        // Define children
        std::size_t range = node->last_ - node->first_;
        switch(range) {
          // Reach end point, only to bbox in range
          case 2 : {
            // Point on actual data
            node->left_child_ = reinterpret_cast<void*>(&data_bbox_[node->first_]);
            node->right_child_ = reinterpret_cast<void*>(&data_bbox_[node->first_ + 1]);
            node->is_leaf_ = true;
          }
            break;
            // Almost reach end (last step before it)
          case 3 : {
            // Point left child onto real data
            node->left_child_ = reinterpret_cast<void*>(&data_bbox_[node->first_]);
            node->right_child_ = reinterpret_cast<void*>(node + 1);
            // Note, "this" is part of a continuous array, therefore when node is not
            // a leaf, child point to the next element in the array
            // Populate right child
            AABBNode<T>* next = node + 1;
            next->first_ = node->first_ + 1;
            next->last_ = node->last_;
            next->level_ = node->level_ + 1;
            // Push onto stack for further processing
            stack.push(next);
          }
            break;
          // Still lots of box to process
          default : {
            long new_range = range / 2;
            // Populate children
            node->left_child_ = reinterpret_cast<void*>(node + 1);
            node->right_child_ = reinterpret_cast<void*>(node + new_range);
            // Push left onto stack
            AABBNode<T>* next = node->get_left_child();
            next->first_ = node->first_;
            next->last_ = node->first_ + new_range;
            next->level_ = node->level_ + 1;
            stack.push(next);
            // Push right onto stack
            next = node->get_right_child();
            next->first_ = node->first_ + new_range;
            next->last_ = node->last_ ;
            next->level_ = node->level_ + 1;
            stack.push(next);
          }
            break;
        }
      }
    } else {
      std::cout << "Unable to allocate memory for AABBTree node !" << std::endl;
      this->Clear();
    }
  }
  need_build_ = false;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  DoIntersect
 * @fn  bool DoIntersect(const Vector3<T>& p, const Vector3<T>& dir, int* idx)
 * @brief Check if a given ray ,p + t*dir, intersect with one of the
 *        triangle's bounding box in the tree and provide its corresponding
 *        index
 *  @param[in]  p   Starting point of the ray
 *  @param[in]  dir Direction of the the ray
 *  @param[out] idx Index of the corresponding intersection
 *  @return True if intersect, false otherwise
 */
template<typename T>
bool AABBTree<T>::DoIntersect(const Vector3<T>& p,
                              const Vector3<T>& dir,
                              std::vector<int>* idx) {
  // Go down the tree starting from top
  idx->clear();
  std::stack<AABBNode<T>*> stack;
  stack.push(root_node_);
  while(!stack.empty()) {
    // Get node
    AABBNode<T>* node = stack.top();
    stack.pop();

    // Check if node intersect
    T t;
    AABB<T> bbox = node->get_bbox();
    if(AABB<T>::IntersectObject(p, dir, bbox, &t)) {
      // Intersect, check for children
      switch(node->size()) {
        // Size 2, reach leaf
        case 2 : {
          bbox = *node->get_left_data();
          if(AABB<T>::IntersectObject(p, dir, bbox, &t)) {
            idx->push_back(bbox.index_);
          }
          bbox = *node->get_right_data();
          if(AABB<T>::IntersectObject(p, dir, bbox, &t)) {
            idx->push_back(bbox.index_);
          }
        }
          break;
          // Size 3, check left data and push right child
        case 3 : {
          // Check left data
          bbox = *node->get_left_data();
          if(AABB<T>::IntersectObject(p, dir, bbox, &t)) {
            idx->push_back(bbox.index_);
          }
          // Push right child on stack
          stack.push(node->get_right_child());
        }
          break;
          // Else push both child
        default : {
          // Push left/right child onto stack
          stack.push(node->get_left_child());
          stack.push(node->get_right_child());
        }
          break;
      }
    }
  }
  return (!idx->empty());
}

/**
 * @name  IntersectWithSegment
 * @fn  bool IntersectWithSegment(const Mesh<T>& mesh,
                                  const Vertex& p,
                                  const Vertex& q,
                                  const T t_max,
                                  T* t)
 * @brief Test if the mesh intersects with a given line PQ
 * @param[in]   mesh  Mesh to test against
 * @param[in]   p     Starting point of the segement to test against
 * @param[in]   q     Stoping point of the segement to test against
 * @param[in]   t_max Maximum t allowed, by default should be 1.f
 * @param[out]  t     Position along the line PQ, t in [0, 1]
 * @return  True if segment intersect with the mesh, False otherwise
 */
template<typename T>
bool AABBTree<T>::IntersectWithSegment(const Mesh<T>& mesh,
                                       const Vertex& p,
                                       const Vertex& q,
                                       const T t_max,
                                       T* t) {
  bool intersect = false;
  // Use tree to prune search space
  std::vector<int> idx;
  idx.reserve(500);
  if(this->DoIntersect(p, q - p, &idx)) {
    const std::vector<Vertex>& vertex = mesh.get_vertex();
    const std::vector<Triangle>& tri_list = mesh.get_triangle();
    for (int i = 0; i < idx.size(); ++i) {
      const Triangle& tri = tri_list[idx[i]];
      const Vertex& A = vertex[tri.x_];
      const Vertex& B = vertex[tri.y_];
      const Vertex& C = vertex[tri.z_];
      intersect = LTS5::IntersectTriWithSegment(p, q, A, B, C, t);
      if (intersect && (*t < t_max)) {
        break;
      } else {
        intersect = false;
      }
    }
  }
  return intersect;
}

#pragma mark -
#pragma mark Private

/**
 * @name  ClearNodes
 * @fn  void ClearNodes(void)
 * @brief Clear all tree's nodes
 */
template<typename T>
void AABBTree<T>::ClearNodes(void) {
  if (data_bbox_.size() > 1) {
    delete[] root_node_;
    root_node_ = nullptr;
  }
}

#pragma mark -
#pragma mark Declaration

/** Float AABBTree */
template class AABBTree<float>;
/** Double AABBTree */
template class AABBTree<double>;

}  // namepsace LTS5
