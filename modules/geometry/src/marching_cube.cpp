/**
 *  @file   marching_cube.cpp
 *  @brief  MarchingCube implementation
 *
 *  @author Gabriel Cuendet
 *  @date   31/05/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <stack>
#include <vector>
#include <limits>

#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/geometry/aabc.hpp"
#include "lts5/geometry/marching_cube_tables.h"
#include "lts5/geometry/marching_cube.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
#pragma mark -
#pragma mark Initialization

/*
 * @name  MarchingCube
 * @fn  MarchingCube()
 * @brief Default constructor of the class
 */
template<typename T>
MarchingCube<T>::MarchingCube() : implicit_(nullptr), max_level_(8) {}

/*
 * @name  MarchingCube
 * @fn  MarchingCube(int max_level)
 * @brief Constructor of the class
 * @param[in]  max_level
 */
template<typename T>
MarchingCube<T>::MarchingCube(int max_level) :implicit_(nullptr),
                                              max_level_(max_level) {}

/*
 * @name  ~MarchingCube
 * @fn  ~MarchingCube()
 * @brief Destructor of the class
 */
template<typename T>
MarchingCube<T>::~MarchingCube() {}

/*
 * @name  Init
 * @fn  void Init(const MPU<T>& implicit)
 * @brief Initialize the marching cube with an implicit to render
 * @param[in]  implicit, the implicit surface to render
 */
template<typename T>
void MarchingCube<T>::Init(const MPU<T>& implicit) {
  implicit_ = &implicit;
  domain_ = implicit_->RootNode().bbox();
}

#pragma mark -
#pragma mark Usage

/*
 * @name  ComputeMesh
 * @fn  int ComputeMesh(Mesh<T>* mesh)
 * @brief Compute the mesh from the implicit
 * @param[out] mesh, the computed mesh
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int MarchingCube<T>::ComputeMesh(Mesh<T>* mesh) {
  int error = -1;
  if (implicit_ == nullptr) {
    std::cout << "[MarchingCube::ComputeMesh] ERROR: Not initialized!" <<
    std::endl;
    return -1;
  }

  std::cout << "[MarchingCube<T>::ComputeMesh] Marching the cubes..." <<
  std::endl;

  Cube* root_cube = new Cube(0, LTS5::AABC<T>(implicit_->RootNode().bbox()));
  std::stack<Cube*> cubes_stack;
  // Directly divide the root cube
  DivideAndStack(*root_cube, &cubes_stack);
  std::vector<Cube*> cubes_leaves;

  while (!cubes_stack.empty()) {
    Cube* cube = cubes_stack.top();
    cubes_stack.pop();

    if (cube->level != max_level_) {
      DivideAndStack(*cube, &cubes_stack);
      delete cube;
      cube = nullptr;
    } else {
      cubes_leaves.push_back(cube);
    }
  }

  const size_t n_cubes(cubes_leaves.size());
  Parallel::For(n_cubes,
                [&](const size_t& i) {
    // Compare the values of the 8 corners: if they are not all same sign, divide!
    // unless you have reached the max level already
    bool contains_surface = false;

    T res = implicit_->Evaluate(cubes_leaves[i]->bbox.min_);
    int sign = (res > 0) - (res < 0);

    for (int corner = 0; corner < 8; ++corner) {
      res = implicit_->Evaluate(cubes_leaves[i]->corners[corner]);
      cubes_leaves[i]->values[corner] = res;
      // If one corner is on the surface, or the sign changes divide!
      if (sign * res <= 0) {
        contains_surface = true;
      }
    }

    if (contains_surface) {
      Polygonize(*(cubes_leaves[i]), mesh);
    }
  });

  std::cout << "[MarchingCube<T>::ComputeMesh] Done!" <<
  std::endl;

  error = 0;
  return error;
}

/*
 * @name  DivideAndStack
 * @fn  void DivideAndStack(const Cube& cube, std::stack<Cube*>* stack)
 * @brief Divide the cube into eight and push on the stack
 * @param[in]  cube
 * @param[out] stack
 */
template<typename T>
void MarchingCube<T>::DivideAndStack(const Cube& cube, std::stack<Cube*>* stack) {
  // Divide and add to the stack
  LTS5::Vector3<T> half_edge = (cube.bbox.max_ - cube.bbox.min_) * T(0.5);

  AABB<T> bbox0(cube.bbox.min_.x_,
                cube.bbox.min_.x_ + half_edge.x_,
                cube.bbox.min_.y_,
                cube.bbox.min_.y_ + half_edge.y_,
                cube.bbox.min_.z_,
                cube.bbox.min_.z_ + half_edge.z_, -1);
  int new_level = cube.level + 1;

  Cube* node0 = new MarchingCube<T>::Cube(new_level, bbox0);
  stack->push(node0);

  AABB<T> bbox1(cube.bbox.min_.x_,
                cube.bbox.min_.x_ + half_edge.x_,
                cube.bbox.min_.y_,
                cube.bbox.min_.y_ + half_edge.y_,
                cube.bbox.min_.z_ + half_edge.z_,
                cube.bbox.max_.z_, -1);
  new_level = cube.level + 1;

  Cube* node1 = new MarchingCube<T>::Cube(new_level, bbox1);
  stack->push(node1);

  AABB<T> bbox2(cube.bbox.min_.x_,
                cube.bbox.min_.x_ + half_edge.x_,
                cube.bbox.min_.y_ + half_edge.y_,
                cube.bbox.max_.y_,
                cube.bbox.min_.z_,
                cube.bbox.min_.z_ + half_edge.z_, -1);
  new_level = cube.level + 1;

  Cube* node2 = new MarchingCube<T>::Cube(new_level, bbox2);
  stack->push(node2);

  AABB<T> bbox3(cube.bbox.min_.x_,
                cube.bbox.min_.x_ + half_edge.x_,
                cube.bbox.min_.y_ + half_edge.y_,
                cube.bbox.max_.y_,
                cube.bbox.min_.z_ + half_edge.z_,
                cube.bbox.max_.z_, -1);
  new_level = cube.level + 1;

  Cube* node3 = new MarchingCube<T>::Cube(new_level, bbox3);
  stack->push(node3);

  AABB<T> bbox4(cube.bbox.min_.x_ + half_edge.x_,
                cube.bbox.max_.x_,
                cube.bbox.min_.y_,
                cube.bbox.min_.y_ + half_edge.y_,
                cube.bbox.min_.z_,
                cube.bbox.min_.z_ + half_edge.z_, -1);
  new_level = cube.level + 1;

  Cube* node4 = new MarchingCube<T>::Cube(new_level, bbox4);
  stack->push(node4);

  AABB<T> bbox5(cube.bbox.min_.x_ + half_edge.x_,
                cube.bbox.max_.x_,
                cube.bbox.min_.y_,
                cube.bbox.min_.y_ + half_edge.y_,
                cube.bbox.min_.z_ + half_edge.z_,
                cube.bbox.max_.z_, -1);
  new_level = cube.level + 1;

  Cube* node5 = new MarchingCube<T>::Cube(new_level, bbox5);
  stack->push(node5);

  AABB<T> bbox6(cube.bbox.min_.x_ + half_edge.x_,
                cube.bbox.max_.x_,
                cube.bbox.min_.y_ + half_edge.y_,
                cube.bbox.max_.y_,
                cube.bbox.min_.z_,
                cube.bbox.min_.z_ + half_edge.z_, -1);
  new_level = cube.level + 1;

  Cube* node6 = new MarchingCube<T>::Cube(new_level, bbox6);
  stack->push(node6);

  AABB<T> bbox7(cube.bbox.min_.x_ + half_edge.x_,
                cube.bbox.max_.x_,
                cube.bbox.min_.y_ + half_edge.y_,
                cube.bbox.max_.y_,
                cube.bbox.min_.z_ + half_edge.z_,
                cube.bbox.max_.z_, -1);
  new_level = cube.level + 1;

  Cube* node7 = new MarchingCube<T>::Cube(new_level, bbox7);
  stack->push(node7);
}

/*
 * @name Polygonize
 * @fn int MarchingCube<T>::Polygonize(const Cube& cube, Mesh<T>* mesh)
 * @brief Polygonize a single cube and update the mesh
 * @param[in]  cube  current cube to march
 * @param[out] mesh  mesh to update
 * @returns the number of triangles
 */
template<typename T>
int MarchingCube<T>::Polygonize(const Cube& cube, Mesh<T>* mesh) {
  T isolevel = T(0.0);

  int cubeindex(0);
  int vert_id[12] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
  std::vector<Vertex> vert_list;
  int last_index = -1;

  //   Determine the index into the edge table which
  //   tells us which vertices are inside of the surface
  cubeindex = 0;
  if (cube.values[0] < isolevel) cubeindex |= 1;
  if (cube.values[1] < isolevel) cubeindex |= 2;
  if (cube.values[2] < isolevel) cubeindex |= 4;
  if (cube.values[3] < isolevel) cubeindex |= 8;
  if (cube.values[4] < isolevel) cubeindex |= 16;
  if (cube.values[5] < isolevel) cubeindex |= 32;
  if (cube.values[6] < isolevel) cubeindex |= 64;
  if (cube.values[7] < isolevel) cubeindex |= 128;

  // Cube is entirely in/out of the surface
  if (edgeTable[cubeindex] == 0)
    return(0);

  // Find the vertices where the surface intersects the cube
  if (edgeTable[cubeindex] & 1) {
//    vertlist[0] = VertexInterp(isolevel, cube.corners[0], cube.corners[1],
//                               cube.values[0], cube.values[1]);
    vert_list.push_back(InterpolateVertex(cube.corners[0], cube.corners[1],
                                          cube.values[0], cube.values[1],
                                          isolevel));
    vert_id[0] = ++last_index;
  }
  if (edgeTable[cubeindex] & 2) {
//    vertlist[1] = VertexInterp(isolevel, cube.corners[1], cube.corners[2],
//                               cube.values[1], cube.values[2]);
    vert_list.push_back(InterpolateVertex(cube.corners[1], cube.corners[2],
                                          cube.values[1], cube.values[2],
                                          isolevel));
    vert_id[1] = ++last_index;
  }
  if (edgeTable[cubeindex] & 4) {
//    vertlist[2] = VertexInterp(isolevel, cube.corners[2], cube.corners[3],
//                               cube.values[2], cube.values[3]);
    vert_list.push_back(InterpolateVertex(cube.corners[2], cube.corners[3],
                                          cube.values[2], cube.values[3],
                                          isolevel));
    vert_id[2] = ++last_index;
  }
  if (edgeTable[cubeindex] & 8) {
//    vertlist[3] = VertexInterp(isolevel, cube.corners[3], cube.corners[0],
//                               cube.values[3], cube.values[0]);
    vert_list.push_back(InterpolateVertex(cube.corners[3], cube.corners[0],
                                          cube.values[3], cube.values[0],
                                          isolevel));
    vert_id[3] = ++last_index;
  }
  if (edgeTable[cubeindex] & 16) {
//    vertlist[4] = VertexInterp(isolevel, cube.corners[4], cube.corners[5],
//                               cube.values[4], cube.values[5]);
    vert_list.push_back(InterpolateVertex(cube.corners[4], cube.corners[5],
                                          cube.values[4], cube.values[5],
                                          isolevel));
    vert_id[4] = ++last_index;
  }
  if (edgeTable[cubeindex] & 32) {
//    vertlist[5] = VertexInterp(isolevel, cube.corners[5],cube.corners[6],
//                               cube.values[5],cube.values[6]);
    vert_list.push_back(InterpolateVertex(cube.corners[5], cube.corners[6],
                                          cube.values[5], cube.values[6],
                                          isolevel));
    vert_id[5] = ++last_index;
  }
  if (edgeTable[cubeindex] & 64) {
//    vertlist[6] = VertexInterp(isolevel, cube.corners[6], cube.corners[7],
//                               cube.values[6], cube.values[7]);
    vert_list.push_back(InterpolateVertex(cube.corners[6], cube.corners[7],
                                          cube.values[6], cube.values[7],
                                          isolevel));
    vert_id[6] = ++last_index;
  }
  if (edgeTable[cubeindex] & 128) {
//    vertlist[7] = VertexInterp(isolevel, cube.corners[7], cube.corners[4],
//                               cube.values[7], cube.values[4]);
    vert_list.push_back(InterpolateVertex(cube.corners[7], cube.corners[4],
                                          cube.values[7], cube.values[4],
                                          isolevel));
    vert_id[7] = ++last_index;
  }
  if (edgeTable[cubeindex] & 256) {
//    vertlist[8] = VertexInterp(isolevel, cube.corners[0], cube.corners[4],
//                               cube.values[0], cube.values[4]);
    vert_list.push_back(InterpolateVertex(cube.corners[0], cube.corners[4],
                                          cube.values[0], cube.values[4],
                                          isolevel));
    vert_id[8] = ++last_index;
  }
  if (edgeTable[cubeindex] & 512) {
//    vertlist[9] = VertexInterp(isolevel, cube.corners[1], cube.corners[5],
//                               cube.values[1], cube.values[5]);
    vert_list.push_back(InterpolateVertex(cube.corners[1], cube.corners[5],
                                          cube.values[1], cube.values[5],
                                          isolevel));
    vert_id[9] = ++last_index;
  }
  if (edgeTable[cubeindex] & 1024) {
//    vertlist[10] = VertexInterp(isolevel, cube.corners[2], cube.corners[6],
//                                cube.values[2], cube.values[6]);
    vert_list.push_back(InterpolateVertex(cube.corners[2], cube.corners[6],
                                          cube.values[2], cube.values[6],
                                          isolevel));
    vert_id[10] = ++last_index;
  }
  if (edgeTable[cubeindex] & 2048) {
//    vertlist[11] = VertexInterp(isolevel, cube.corners[3], cube.corners[7],
//                                cube.values[3], cube.values[7]);
    vert_list.push_back(InterpolateVertex(cube.corners[3], cube.corners[7],
                                          cube.values[3], cube.values[7],
                                          isolevel));
    vert_id[11] = ++last_index;
  }

  ScopedLock guard(this->polygonize_mutex_);

  int nvert_mesh = static_cast<int>(mesh->get_vertex().size());
  // Create the triangle
  int ntriang = 0;
  std::vector<Triangle> triangles;
  for (int i = 0; triTable[cubeindex][i] != -1; i += 3) {
    Triangle new_tri(vert_id[triTable[cubeindex][i]] + nvert_mesh,
                     vert_id[triTable[cubeindex][i+1]] + nvert_mesh,
                     vert_id[triTable[cubeindex][i+2]] + nvert_mesh);
    triangles.push_back(new_tri);
    ntriang++;
  }

  mesh->get_vertex().insert(mesh->get_vertex().end(),
                            std::make_move_iterator(vert_list.begin()),
                            std::make_move_iterator(vert_list.end()));
  mesh->get_triangle().insert(mesh->get_triangle().end(),
                              std::make_move_iterator(triangles.begin()),
                              std::make_move_iterator(triangles.end()));

  return(ntriang);
}

/**
 * @name InterpolateVertex
 * @fn void InterpolateVertex(const LTS5::Vector3<T>& point1,
 *                            const LTS5::Vector3<T>& point2, const T& value1,
 *                            const T& value2, const T& isolevel,
 *                            LTS5::Vector3<T> interp)
 * @brief Linearly interpolate between two vertices with given values to find
 *        the point with value isolevel
 * @param[in]  point1
 * @param[in]  point2
 * @param[in]  value1
 * @param[in]  value2
 * @param[in]  isolevel
 * @returns interp
 */
template<typename T>
LTS5::Vector3<T> MarchingCube<T>::InterpolateVertex(const LTS5::Vector3<T>& point1,
                                                    const LTS5::Vector3<T>& point2,
                                                    const T& value1,
                                                    const T& value2,
                                                    const T& isolevel) {
  // It has been suggested that the interpolation should be handled with the
  // swap, that this solves an issue of small cracks in the isosurface.
  // source: http://paulbourke.net/geometry/polygonise/
  LTS5::Vector3<T> interp;

  /*  LTS5::Vector3<T> p1 = point1;
  LTS5::Vector3<T> p2 = point2;
  LTS5::Vector3<T> interp;
  bool swap = false;
  if (p2.x_ < p1.x_) swap = true;
  if (p2.x_ == p1.x_ && p2.y_ < p1.y_) swap = true;
  if (p2.x_ == p1.x_ && p2.y_ == p1.y_ && p2.z_ < p1.z_) swap = true;

  if (swap) {
    LTS5::Vector3<T> temp;
    temp = p1;
    p1 = p2;
    p2 = temp;
  }
*/
  if(fabs(value1 - value2) > std::numeric_limits<T>::epsilon())
    interp = point1 + (point2 - point1)/(value2 - value1) * (isolevel - value1);
  else
    interp = point1;

  return interp;
}

#pragma mark -
#pragma mark Declaration

  /** Float MarchingCube */
  template class MarchingCube<float>;
  /** Double MarchingCube */
  template class MarchingCube<double>;

}  // namespace LTS5
