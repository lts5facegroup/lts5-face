/**
 *  @file   manifold_harmonic_transform.cpp
 *  @brief  MHT implementation
 *
 *  @author Gabriel Cuendet
 *  @date   12/09/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <limits>
#include <fstream>

#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/geometry/manifold_harmonic_transform.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  MHT
 * @fn  MHT(void);
 * @brief Default class constructor
 */
template<typename T>
MHT<T>::MHT(void) : ndim_(0), nvec_(0), eigenpairs_per_band_(50),
  eig_decomposition_() {}

/*
 * @name  MHT
 * @fn  MHT(const Eigen::SparseMatrix<T>& Q,
 *          const Eigen::SparseMatrix<T>& D);
 * @brief Class constructor
 * @param[in]  Q, sparse stifness matrix
 * @param[in]  D, diagonal lumped mass matrix
 * @param[in]  n_basis, number of basis vector to compute
 */
template<typename T>
MHT<T>::MHT(const Eigen::SparseMatrix<T>& Q,
            const Eigen::SparseMatrix<T>& D,
            const int& n_basis) : ndim_(D.rows()), nvec_(n_basis), D_(D), eigenpairs_per_band_(50), eig_decomposition_() {
  Init(Q, D, n_basis);
}

/*
 * @name  Init
 * @fn  void Init(const Eigen::SparseMatrix<T>& Q, const Eigen::SparseMatrix<T>& D, const int& n_basis)
 * @brief Initialize the transform by setting important params and computing the basis
 * @param[in]  Q, sparse stifness matrix
 * @param[in]  D, diagonal lumped mass matrix
 * @param[in]  n_basis, number of basis vector to compute
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int MHT<T>::Init(const Eigen::SparseMatrix<T>& Q,
                  const Eigen::SparseMatrix<T>& D,
                  const int& n_basis) {
  int err = -1;
  ndim_ = D.rows();
  nvec_ = n_basis;
  D_ = D;

  //ComputeBasis(Q);

  if (n_basis < 2 * eigenpairs_per_band_) {
    err = ComputeBasis(Q);
  } else {
    err = ComputeBandByBandBasis(Q, eigenpairs_per_band_);
  }

  return err;
}

/*
 * @name  Read
 * @fn  int Read(const std::string& filename)
 * @brief Read MHT from file
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int MHT<T>::Read(const std::string& filename) {
  std::ifstream in(filename, std::ios::in | std::ios::binary);

  if (!in.is_open()) {
    return -1;
  }

  // Read eigenvalues_
  typename Eigen::Matrix<T, Eigen::Dynamic, 1>::Index rows=0, cols=0;
  in.read((char*) (&rows), sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, 1>::Index));
  in.read((char*) (&cols), sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, 1>::Index));
  this->eigenvalues_.resize(rows, cols);
  in.read((char *) eigenvalues_.data(),
          rows*cols*sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, 1>::Scalar));

  // Read eigenvectors_ (basis_)
  rows=0;
  cols=0;
  in.read((char*) (&rows), sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Index));
  in.read((char*) (&cols), sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Index));
  ndim_ = static_cast<int>(cols);
  nvec_ = static_cast<int>(rows);
  this->basis_.resize(rows, cols);
  in.read((char *) basis_.data(),
          rows * cols * sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Scalar));

  // Read D_
  typename Eigen::SparseMatrix<T>::Index d_rows=0, d_cols=0, innerSz=0,
    outerSz=0, matSz=0;
  in.read((char*) (&d_rows), sizeof(typename Eigen::SparseMatrix<T>::Index));
  in.read((char*) (&d_cols), sizeof(typename Eigen::SparseMatrix<T>::Index));
  in.read((char*) (&matSz), sizeof(typename Eigen::SparseMatrix<T>::Index));
  in.read((char*) (&outerSz), sizeof(typename Eigen::SparseMatrix<T>::Index));
  in.read((char*) (&innerSz), sizeof(typename Eigen::SparseMatrix<T>::Index));

  D_.resize(d_rows, d_cols);
  D_.makeCompressed();
  D_.resizeNonZeros(matSz);

  in.read((char*) (D_.valuePtr()),
          matSz * sizeof(typename Eigen::SparseMatrix<T>::Scalar));
  in.read((char*) (D_.outerIndexPtr()),
          outerSz * sizeof(typename Eigen::SparseMatrix<T>::Index));
  in.read((char*) (D_.innerIndexPtr()),
          matSz * sizeof(typename Eigen::SparseMatrix<T>::Index));

  D_.finalize();

  in.close();

  basis_t_ = basis_.transpose();

  return 0;
}

/*
 * @name  Write
 * @fn  void Write(const std::string& filename) const
 * @brief Write the MHT to file
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int MHT<T>::Write(const std::string& filename) {
  std::ofstream out(filename,
                    std::ios::out | std::ios::binary | std::ios::trunc);
  if (!out.is_open()) {
    return -1;
  }

  // Write eigenvalues_
  typename Eigen::Matrix<T, Eigen::Dynamic, 1>::Index rows=eigenvalues_.rows(),
    cols=eigenvalues_.cols();
  out.write((char*) (&rows),
            sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, 1>::Index));
  out.write((char*) (&cols),
            sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, 1>::Index));
  out.write((char*) eigenvalues_.data(),
            rows*cols*sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, 1>::Scalar));

  // Write eigenvectors_ (basis_)
  rows=basis_.rows(),
  cols=basis_.cols();
  out.write((char*) (&rows),
            sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Index));
  out.write((char*) (&cols),
            sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Index));
  out.write((char*) basis_.data(),
            rows*cols*sizeof(typename Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Scalar));

  // Write D_
  D_.makeCompressed();

  typename Eigen::SparseMatrix<T>::Index d_rows = D_.rows();
  typename Eigen::SparseMatrix<T>::Index d_cols = D_.cols();

  typename Eigen::SparseMatrix<T>::Index matSz = D_.nonZeros();
  typename Eigen::SparseMatrix<T>::Index outerSz = D_.outerSize();
  typename Eigen::SparseMatrix<T>::Index innerSz = D_.innerSize();

  out.write((char*) (&d_rows), sizeof(typename Eigen::SparseMatrix<T>::Index));
  out.write((char*) (&d_cols), sizeof(typename Eigen::SparseMatrix<T>::Index));
  out.write((char*) (&matSz), sizeof(typename Eigen::SparseMatrix<T>::Index));
  out.write((char*) (&outerSz), sizeof(typename Eigen::SparseMatrix<T>::Index));
  out.write((char*) (&innerSz), sizeof(typename Eigen::SparseMatrix<T>::Index));

  // Data:nonZeros() -> valuePtr()
  out.write((char*) D_.valuePtr(),
            matSz*sizeof(typename Eigen::SparseMatrix<T>::Scalar));
  // Outer size: outerSize() -> outerIndexPtr()
  out.write((char*) D_.outerIndexPtr(),
            outerSz*sizeof(typename Eigen::SparseMatrix<T>::Index));
  // Inner size: innerSize() -> innerIndexPtr()
  out.write((char*) D_.innerIndexPtr(),
            innerSz*sizeof(typename Eigen::SparseMatrix<T>::Index));

  out.close();
  return 0;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Transform
 * @fn  void Transform(const std::vector<T>& spat_vec,
 *                     std::vector<T>* freq_vec) const
 * @brief Transform the content of the vector using the MHT, from spatial
 *        domain to frequency domain
 * @param[in] spat_vec
 * @param[out] freq_vec
 */
template<typename T>
void MHT<T>::Transform(const std::vector<T>& spat_vec,
                       std::vector<T>* freq_vec) const {
  freq_vec->clear();
  freq_vec->resize(nvec_);
  Parallel::For(nvec_,
                [&](const size_t& i) {
    T& val = (*freq_vec)[i];
    auto basis_row = basis_.row(i);
    for (int j = 0; j < spat_vec.size(); ++j) {
      val += basis_row[j] * D_.coeff(i,j) * spat_vec[j];
    }
  });
}

/*
 * @name  Transform
 * @fn  void Transform(const std::vector<LTS5::Vector2<T> >& spat_vec,
 *                     std::vector<LTS5::Vector2<T> >* freq_vec) const
 * @brief Transform the content of the vector using the MHT, from spatial
 *        domain to frequency domain
 * @param[in] spat_vec
 * @param[out] freq_vec
 */
template<typename T>
void MHT<T>::Transform(const std::vector<LTS5::Vector2<T> >& spat_vec,
                       std::vector<LTS5::Vector2<T> >* freq_vec) const {
  freq_vec->clear();
  freq_vec->resize(nvec_);
  Parallel::For(nvec_,
                [&](const size_t& i) {
    T& x = (*freq_vec)[i].x_;
    T& y = (*freq_vec)[i].y_;

    auto basis_row = basis_.row(i);
    for (int j = 0; j < spat_vec.size(); ++j) {
      x += basis_row[j] * D_.coeff(j,j) * spat_vec[j].x_;
      y += basis_row[j] * D_.coeff(j,j) * spat_vec[j].y_;
    }
  });
}

/*
 * @name  Transform
 * @fn  void Transform(const std::vector<LTS5::Vector3<T> >& spat_vec,
 *                     std::vector<LTS5::Vector3<T> >* freq_vec) const
 * @brief Transform the content of the vector using the MHT, from spatial
 *        domain to frequency domain
 * @param[in] spat_vec
 * @param[out] freq_vec
 */
template<typename T>
void MHT<T>::Transform(const std::vector<LTS5::Vector3<T> >& spat_vec,
                       std::vector<LTS5::Vector3<T> >* freq_vec) const {
  freq_vec->clear();
  freq_vec->resize(nvec_);

  Parallel::For(nvec_,
                [&](const size_t& i) {
    T& x = (*freq_vec)[i].x_;
    T& y = (*freq_vec)[i].y_;
    T& z = (*freq_vec)[i].z_;

    auto basis_row = basis_.row(i);
    for (int j = 0; j < spat_vec.size(); ++j) {
      x += basis_row[j] * D_.coeff(j,j) * spat_vec[j].x_;
      y += basis_row[j] * D_.coeff(j,j) * spat_vec[j].y_;
      z += basis_row[j] * D_.coeff(j,j) * spat_vec[j].z_;
    }
  });
}

/*
 * @name  Transform
 * @fn  void Transform(const std::vector<LTS5::Vector4<T> >& spat_vec,
 *                     std::vector<LTS5::Vector4<T> >* freq_vec) const
 * @brief Transform the content of the vector using the MHT, from spatial
 *        domain to frequency domain
 * @param[in] spat_vec
 * @param[out] freq_vec
 */
template<typename T>
void MHT<T>::Transform(const std::vector<LTS5::Vector4<T> >& spat_vec,
                       std::vector<LTS5::Vector4<T> >* freq_vec) const {
  freq_vec->clear();
  freq_vec->resize(nvec_);
  Parallel::For(nvec_,
                [&](const size_t& i) {
    T& x = (*freq_vec)[i].x_;
    T& y = (*freq_vec)[i].y_;
    T& z = (*freq_vec)[i].z_;
    T& w = (*freq_vec)[i].w_;

    auto basis_row = basis_.row(i);
    for (int j = 0; j < spat_vec.size(); ++j) {
      x += basis_row[j] * D_.coeff(j,j) * spat_vec[j].x_;
      y += basis_row[j] * D_.coeff(j,j) * spat_vec[j].y_;
      z += basis_row[j] * D_.coeff(j,j) * spat_vec[j].z_;
      w += basis_row[j] * D_.coeff(j,j) * spat_vec[j].w_;
    }
  });
}

/*
 * @name  Transform
 * @fn  void Transform(const std::vector<LTS5::Vector4<unsigned char> >& spat_vec,
 *                     std::vector<LTS5::Vector4<unsigned char> >* freq_vec) const
 * @brief Transform the content of the vector using the MHT, from spatial
 *        domain to frequency domain
 * @param[in] spat_vec
 * @param[out] freq_vec
 */
template<typename T>
void MHT<T>::Transform(const std::vector<LTS5::Vector4<unsigned char> >& spat_vec,
                       std::vector<LTS5::Vector4<T> >* freq_vec) const {
  freq_vec->clear();
  freq_vec->resize(nvec_);
  Parallel::For(nvec_,
                [&](const size_t& i) {
    T& x = (*freq_vec)[i].x_;
    T& y = (*freq_vec)[i].y_;
    T& z = (*freq_vec)[i].z_;
    T& w = (*freq_vec)[i].w_;

    auto basis_row = basis_.row(i);
    for (int j = 0; j < spat_vec.size(); ++j) {
      x += basis_row[j] * D_.coeff(j,j) * spat_vec[j].x_;
      y += basis_row[j] * D_.coeff(j,j) * spat_vec[j].y_;
      z += basis_row[j] * D_.coeff(j,j) * spat_vec[j].z_;
      w += basis_row[j] * D_.coeff(j,j) * spat_vec[j].w_;
    }
  });
}


/*
 * @name  Transform
 * @fn  void Transform(const std::vector<LTS5::Vector3<T> >& spat_vec,
 *                     Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* freq_vec) const
 * @brief Transform the content of the vector using the MHT, from spatial
 *        domain to frequency domain
 * @param[in]  spat_vec  Spatial coordinates vector
 * @param[out] freq_vec  Spectral (frequencies) coordinates vector
 */
template<typename T>
void MHT<T>::Transform(const std::vector<LTS5::Vector3<T> >& spat_vec,
                       Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* freq_vec) const {
  int dims = 3;

  // @TODO: DEBUG PURPOSE
  assert(spat_vec.size() == nvec_);

  freq_vec->resize(dims, nvec_);

  std::vector<LTS5::Vector3<T> >& spat = const_cast<std::vector<LTS5::Vector3<T> >&>(spat_vec);
  Eigen::Map<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> >
    vertices(reinterpret_cast<T*>(spat.data()), dims, spat.size());

  Parallel::For(nvec_,
                [&](const size_t& i) {
    auto col = freq_vec->col(i);
    auto basis_row = basis_t_.col(i);
    col = (vertices * D_) * basis_row;
  });
}

/*
 * @name  Transform
 * @fn  void Transform(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>&  spat_vec,
 *                     Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* freq_vec) const
 * @brief Transform the content of the vector using the MHT, from spatial
 *        domain to frequency domain
 * @param[in]  spat_vec  Spatial coordinates vector (vertices are in columns!)
 * @param[out] freq_vec  Spectral (frequencies) coordinates vector
 */
template<typename T>
void MHT<T>::Transform(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& spat_vec,
                       Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* freq_vec) const {
  freq_vec->resize(spat_vec.innerSize(), nvec_);
  Parallel::For(nvec_,
                [&](const size_t& i) {
    auto col = freq_vec->col(i);
    auto basis_row = basis_t_.col(i);
    col = (spat_vec * D_) * basis_row;  // TODO: Check if valid ....
  });
}

/*
 * @name  InvTransform
 * @fn  void InvTransform(const std::vector<T>& freq_vec,
 *                        std::vector<T>* spat_vec) const
 * @brief Inverse MHT transform, from frequency domain to spatial domain
 * @param[in]  freq_vec
 * @param[out] spat_vec
 */
template<typename T>
void MHT<T>::InvTransform(const std::vector<T>& freq_vec,
                          std::vector<T>* spat_vec) const {
  spat_vec->clear();
  spat_vec->resize(ndim_);
  Parallel::For(ndim_,
                [&](const size_t& j) {
    T& val = (*spat_vec)[j];

    auto basis_col = basis_.col(j);
    for (int i = 0; i < freq_vec.size(); ++i) {
      val += basis_col[i] * freq_vec[i];
    }
  });
}

/*
 * @name  InvTransform
 * @fn  void InvTransform(const std::vector<LTS5::Vector2<T> >& freq_vec,
 *                        std::vector<LTS5::Vector2<T> >* spat_vec) const
 * @brief Inverse MHT transform, from frequency domain to spatial domain
 * @param[in]  freq_vec
 * @param[out] spat_vec
 */
template<typename T>
void MHT<T>::InvTransform(const std::vector<LTS5::Vector2<T> >& freq_vec,
                          std::vector<LTS5::Vector2<T> >* spat_vec) const {
  spat_vec->clear();
  spat_vec->resize(ndim_);

  Parallel::For(ndim_,
                [&](const size_t& j) {
    T& x = (*spat_vec)[j].x_;
    T& y = (*spat_vec)[j].y_;

    auto basis_col = basis_.col(j);
    for (int i = 0; i < freq_vec.size(); ++i) {
      x += basis_col[i] * freq_vec[i].x_;
      y += basis_col[i] * freq_vec[i].y_;
    }
  });
}

/*
 * @name  InvTransform
 * @fn  void InvTransform(const std::vector<LTS5::Vector3<T> >& freq_vec,
 *                        std::vector<LTS5::Vector3<T> >* spat_vec) const
 * @brief Inverse MHT transform, from frequency domain to spatial domain
 * @param[in]  freq_vec
 * @param[out] spat_vec
 */
template<typename T>
void MHT<T>::InvTransform(const std::vector<LTS5::Vector3<T> >& freq_vec,
                          std::vector<LTS5::Vector3<T> >* spat_vec) const {
  spat_vec->clear();
  spat_vec->resize(ndim_);

  Parallel::For(nvec_,
                [&](const size_t& j) {
    T& x = (*spat_vec)[j].x_;
    T& y = (*spat_vec)[j].y_;
    T& z = (*spat_vec)[j].z_;

    auto basis_col = basis_.col(j);
    for (int i = 0; i < freq_vec.size(); ++i) {
      x += basis_col[i] * freq_vec[i].x_;
      y += basis_col[i] * freq_vec[i].y_;
      z += basis_col[i] * freq_vec[i].z_;
    }
  });
}

/*
 * @name  InvTransform
 * @fn  void InvTransform(const std::vector<LTS5::Vector4<T> >& freq_vec,
 *                        std::vector<LTS5::Vector4<T> >* spat_vec) const
 * @brief Inverse MHT transform, from frequency domain to spatial domain
 * @param[in]  freq_vec
 * @param[out] spat_vec
 */
template<typename T>
void MHT<T>::InvTransform(const std::vector<LTS5::Vector4<T> >& freq_vec,
                          std::vector<LTS5::Vector4<T> >* spat_vec) const {
  spat_vec->clear();
  spat_vec->resize(ndim_);
  Parallel::For(ndim_,
                [&](const size_t& j) {
    T& x = (*spat_vec)[j].x_;
    T& y = (*spat_vec)[j].y_;
    T& z = (*spat_vec)[j].z_;
    T& w = (*spat_vec)[j].w_;

    auto basis_col = basis_.col(j);
    for (int i = 0; i < freq_vec.size(); ++i) {
      x += basis_col[i] * freq_vec[i].x_;
      y += basis_col[i] * freq_vec[i].y_;
      z += basis_col[i] * freq_vec[i].z_;
      w += basis_col[i] * freq_vec[i].w_;
    }
  });
}

/*
 * @name  InvTransform
 * @fn  void InvTransform(const std::vector<LTS5::Vector4<T> >& freq_vec,
 *                        std::vector<LTS5::Vector4<unsigned char> >* spat_vec) const
 * @brief Inverse MHT transform, from frequency domain to spatial domain
 * @param[in]  freq_vec
 * @param[out] spat_vec
 */
template<typename T>
void MHT<T>::InvTransform(const std::vector<LTS5::Vector4<T> >& freq_vec,
                          std::vector<LTS5::Vector4<unsigned char> >* spat_vec) const {
  spat_vec->clear();
  spat_vec->resize(ndim_);

  Parallel::For(ndim_,
                [&](const size_t& j) {
    T x = 0.0;
    T y = 0.0;
    T z = 0.0;
    T w = 0.0;

    auto basis_col = basis_.col(j);
    for (int i = 0; i < freq_vec.size(); ++i) {
      x += basis_col[i] * freq_vec[i].x_;
      y += basis_col[i] * freq_vec[i].y_;
      z += basis_col[i] * freq_vec[i].z_;
      w += basis_col[i] * freq_vec[i].w_;
    }

    (*spat_vec)[j].x_ = std::round(x);
    (*spat_vec)[j].y_ = std::round(y);
    (*spat_vec)[j].z_ = std::round(z);
    (*spat_vec)[j].w_ = std::round(w);
  });
}

/*
 * @name  InvTransform
 * @fn  void InvTransform(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& freq_vec,
 *                        Eigen::Matrix<T,Eigen::Dynamic, Eigen::Dynamic>* spat_vec) const
 * @brief Inverse MHT transform, from frequency domain to spatial domain
 * @param[in]  freq_vec  Spectral (frequencies) coordinates vector
 * @param[out] spat_vec  Spatial coordinates vector
 */
template<typename T>
void MHT<T>::InvTransform(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& freq_vec,
                          Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* spat_vec) const {
  spat_vec->resize(freq_vec.innerSize(), ndim_); // colomn majow -> rows

  Parallel::For(ndim_,
                [&](const size_t& j) {
    auto col = spat_vec->col(j);
    auto basis_col = basis_.col(j);

    col = (freq_vec * basis_col);
  });
}

#pragma mark -
#pragma mark Private attributes and methods

/*
 * @name  ComputeBandByBandBasis
 * @fn  int ComputeBandByBandBasis(const Eigen::SparseMatrix<T>& Q)
 * @brief Compute basis with a large number of vector, using Band-by-Band algo
 * @param[in]  Q, sparse stifness matrix
 * @param[in]  n_values_per_band, number of values to compute in each band
 */
template<typename T>
int MHT<T>::ComputeBandByBandBasis(const Eigen::SparseMatrix<T>& Q, const int& n_values_per_band) {
  assert(nvec_ <= Q.rows() - 1);

  std::cout << "Computing basis using Band-by-band algo..." << std::endl;

  SparseLinearAlgebra::Arpack::non_sym_eigen_params<T>& params = eig_decomposition_.get_params();

  // Set parameters
  params.nevp = n_values_per_band;
  params.ncvp = std::min(2 * params.nevp, static_cast<int>(Q.rows()) - 1);
  if (params.enable_shift_inverse && std::abs(params.fs) <= std::numeric_limits<T>::epsilon()) {
    params.fs = 1e-6;  // CAUTION: seems unstable if fs is too small
  }

  T f_last = 0.0;
  T f_old_last = 0.0;
  int n_basis = 0;
  T tol = 1e-6;
  Eigen::DiagonalMatrix<T, Eigen::Dynamic> DInv(D_.diagonal());
  DInv = DInv.inverse();
  Eigen::SparseMatrix<T> A = T(-1.0) * DInv * Q;

  int err = eig_decomposition_.Init(A);
  if (!err) {
    basis_.resize(nvec_, ndim_);
    eigenvalues_.resize(nvec_, 1);

    while (n_basis < nvec_) {
      f_old_last = n_basis == 0 ? 0.0 : eigenvalues_[n_basis-1];
      std::cout << n_basis << " / " << nvec_ << "  last eigenvalue: " << f_last <<
      std::endl;

      eig_decomposition_.Compute();

      Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> partial_eigenvecs;
      Eigen::Matrix<T, Eigen::Dynamic, 1> partial_eigenvals;
      eig_decomposition_.GetSortedEigenvectors(&partial_eigenvecs);
      eig_decomposition_.GetSortedEigenvalues(&partial_eigenvals);

      for (int k = 0; k < partial_eigenvals.size(); ++k) {
        if ((partial_eigenvals[k] - f_last > tol) || (f_last==0.0)) {
          int remaining_eig = static_cast<int>(partial_eigenvals.rows()) - k;
          int n_to_copy = std::min(remaining_eig, nvec_ - n_basis);

          Eigen::Block<Eigen::Matrix<T, Eigen::Dynamic, 1> >
            eigval_block(partial_eigenvals, k, 0, n_to_copy, 1);
          Eigen::Block<Eigen::Matrix<T, Eigen::Dynamic, 1> >
            values_block(eigenvalues_, n_basis, 0, n_to_copy, 1);
          values_block = eigval_block;

          Eigen::Block<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> >
            eigvec_block(partial_eigenvecs, k, 0, n_to_copy, ndim_);
          Eigen::Block<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> >
            basis_block(basis_, n_basis, 0, n_to_copy, ndim_);
          basis_block = eigvec_block;

          n_basis += n_to_copy;
          break;
        }
      }

      // @TODO: (Gabriel) BUG: If band becomes small... you have infinite loop!
      f_last = partial_eigenvals[partial_eigenvals.size()-1];
      T half_band(0.0);
      if (f_last - f_old_last > std::numeric_limits<T>::epsilon()) {
        half_band = 0.4 * (f_last - f_old_last);
        half_band = half_band / (partial_eigenvals.size() / n_values_per_band);
      } else {
        break;
      }
      T new_fs = f_last + half_band;
      eig_decomposition_.ChangeFs(new_fs);
    }

    basis_.resize(n_basis, ndim_);
    eigenvalues_.resize(n_basis, 1);
    nvec_ = n_basis;

    DNormalizeBasis();
    basis_t_ = basis_.transpose();

  }
  return err;
}

/*
 * @name  ComputeBasis
 * @fn  int ComputeBasis(const Eigen::SparseMatrix<T>& Q);
 * @brief From Q and D, perform Eigendecomposition to compute the MHB
 * @param[in]  Q, sparse stifness matrix
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int MHT<T>::ComputeBasis(const Eigen::SparseMatrix<T>& Q) {
  std::cout << "Computing basis..." << std::endl;

  SparseLinearAlgebra::Arpack::non_sym_eigen_params<T>& params = eig_decomposition_.get_params();

  // Set parameters
  params.nevp = nvec_;
  //@TODO: (Gabriel) Find out how to set this parameter! Seems critical!
  params.ncvp = std::min(2 * nvec_, static_cast<int>(Q.rows()) - 1);

  if (params.enable_shift_inverse && std::abs(params.fs) <= std::numeric_limits<T>::epsilon()) {
    params.fs = 1e-6;  // CAUTION: seems unstable if fs is too small
  }

  Eigen::DiagonalMatrix<T, Eigen::Dynamic> DInv(D_.diagonal());
  DInv = DInv.inverse();
  Eigen::SparseMatrix<T> A = T(-1.0) * DInv * Q;

  int err = eig_decomposition_.Init(A);
  if (!err) {
    eig_decomposition_.Compute();
    eig_decomposition_.GetSortedEigenvectors(&basis_);
    eig_decomposition_.GetSortedEigenvalues(&eigenvalues_);

    // DEBUG PURPOSE
    std::cout << "Basis computed: " << basis_.rows() << " rows x " <<
    basis_.cols() << " cols" << std::endl;
    //std::cout << "Eigenvalues: " << eigenvalues_ << std::endl;
    DNormalizeBasis();
    basis_t_ = basis_.transpose();
  }

  return err;
}

/*
 * @name  DNormalizeBasis
 * @fn  void DNormalizeBasis(void)
 * @brief Normalize the basis with respect to the inner product defined with D
 */
template<typename T>
void MHT<T>::DNormalizeBasis(void) {
  Parallel::For(basis_.rows(),
                [&](const size_t& i) {
  // Normalize basis vectors with respect to the custom inner product
    auto row = basis_.row(i);

    T row_norm(0.0);
    for (int k = 0; k < row.size(); ++k) {
      row_norm += row[k] * D_.coeff(k, k) * row[k];
    }
    row = (1/std::sqrt(row_norm)) * row;
  });
}

#pragma mark -
#pragma mark Declaration

/** Float MHT */
template class MHT<float>;
/** Double MHT */
template class MHT<double>;

}  // namespace LTS5
