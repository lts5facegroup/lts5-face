/**
 *  @file   octree.cpp
 *  @brief  OCTree implementation, contructed from a given mesh, adapted from
 *          CGAL
 *
 *  @author Christophe Ecabert
 *  @date   03/03/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <cassert>
#include <iostream>
#include <fstream>
#include <limits>
#include<utility>
#include <algorithm>
#include <stack>
#include <vector>

#include "lts5/geometry/octree.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  OCTree
 * @fn  OCTree()
 * @brief Constructor
 */
template< typename T>
OCTree<T>::OCTree() : data_(0),
                      root_node_(nullptr),
                      index_map_(0),
                      bucket_size_(25),
                      max_depth_(8),
                      max_tree_level_(0) {}

/*
 * @name  OCTree
 * @fn  OCTree()
 * @brief Constructor
 * @param[in] bucket_size Minimum number of object per node
 * @param[in] max_depth   Maximum tree depth
 */
template< typename T>
OCTree<T>::OCTree(const int& bucket_size, const int& max_depth)  :
        data_(0),
        root_node_(nullptr),
        index_map_(0),
        bucket_size_(bucket_size),
        max_depth_(max_depth),
        max_tree_level_(0) {}

/*
 * @name  ~OCTree
 * @fn  ~OCTree()
 * @brief Destructor
 */
template< typename T>
OCTree<T>::~OCTree() {
  // Delete tree
  this->Clear();
}

/*
 * @name  Build
 * @fn   void Build()
 * @brief Build iteratively the whole tree
 */
template< typename T>
void OCTree<T>::Build() {
  using Vertex = typename Mesh<T>::Vertex;
  // Clear tree
  this->Clear();
  // Build new one
  if (!data_.empty()) {
    // Compute bounding volume
    T xmin = std::numeric_limits<T>::max();
    T xmax = std::numeric_limits<T>::lowest();
    T ymin = std::numeric_limits<T>::max();
    T ymax = std::numeric_limits<T>::lowest();
    T zmin = std::numeric_limits<T>::max();
    T zmax = std::numeric_limits<T>::lowest();
    for (int j = 0; j < data_.size(); ++j) {
      const Vertex &v = data_[j];
      xmin = v.x_ < xmin ? v.x_ : xmin;
      xmax = v.x_ > xmax ? v.x_ : xmax;
      ymin = v.y_ < ymin ? v.y_ : ymin;
      ymax = v.y_ > ymax ? v.y_ : ymax;
      zmin = v.z_ < zmin ? v.z_ : zmin;
      zmax = v.z_ > zmax ? v.z_ : zmax;
    }

    AABB<T> bbox_volume(xmin, xmax, ymin, ymax, zmin, zmax, -1);

    // Initialize root node
    std::size_t count = 0;
    max_tree_level_ = 0;
    root_node_ = new OCTreeNode<T>(0, data_.size() - 1, bbox_volume, 0);
    std::stack< OCTreeNode<T>* > stack;
    stack.push(root_node_);
    // Iterate overall node
    while (!stack.empty()) {
      OCTreeNode<T>* node = stack.top();
      stack.pop();
      // Define maximum level
      max_tree_level_ = std::max(max_tree_level_, node->level_);
      // If reach maximum level, nothing to do
      if (node->level_ == max_depth_) { continue;}
      // Sort data such that objects in one cell are in one range and ordered
      // child-wise. In a sense of : Xmin -> Xmax, Ymin -> Ymax, Zmin -> Zmax.
      std::size_t xlow_y_split = 0;
      std::size_t xlow_ylow_z_split = 0;
      std::size_t xlow_yhigh_z_split = 0;
      std::size_t xhigh_y_split = 0;
      std::size_t xhigh_ylow_z_split = 0;
      std::size_t xhigh_yhigh_z_split = 0;
      // Start with x direction
      std::size_t x_split = this->SortAndSplit(node->first_,
                                               node->last_,
                                               SortSplitType::kX,
                                               node->center_.x_);
      if (x_split != max_size_t) {
        // -----------------------------------------------------------------
        // xlow not empty, split xlow region in Y direction
        xlow_y_split = this->SortAndSplit(node->first_,
                                          x_split,
                                          SortSplitType::kY,
                                          node->center_.y_);
        if (xlow_y_split != max_size_t) {
          // xlow_ylow not empty, split region in Z direction
          xlow_ylow_z_split = this->SortAndSplit(node->first_,
                                                 xlow_y_split,
                                                 SortSplitType::kZ,
                                                 node->center_.z_);
          xlow_yhigh_z_split = this->SortAndSplit(xlow_y_split + 1,
                                                  x_split,
                                                  SortSplitType::kZ,
                                                  node->center_.z_);
        } else {
          xlow_ylow_z_split = max_size_t;  // xlow_ylow = empty
          // Split  xlow_yhigh in z direction
          xlow_yhigh_z_split = this->SortAndSplit(node->first_,
                                                  x_split,
                                                  SortSplitType::kZ,
                                                  node->center_.z_);
        }

        // -----------------------------------------------------------------
        // split xhigh region Y direction
        xhigh_y_split = this->SortAndSplit(x_split + 1,
                                           node->last_,
                                           SortSplitType::kY,
                                           node->center_.y_);
        if (xhigh_y_split != max_size_t) {
          // xhigh_ylow not empty, split in Z direction
          xhigh_ylow_z_split = this->SortAndSplit(x_split + 1,
                                                  xhigh_y_split,
                                                  SortSplitType::kZ,
                                                  node->center_.z_);
          xhigh_yhigh_z_split = this->SortAndSplit(xhigh_y_split + 1,
                                                   node->last_,
                                                   SortSplitType::kZ,
                                                   node->center_.z_);
        } else {
          // xhigh_ylow does not exist
          xhigh_ylow_z_split = max_size_t;
          xhigh_yhigh_z_split = this->SortAndSplit(x_split + 1,
                                                   node->last_,
                                                   SortSplitType::kZ,
                                                   node->center_.z_);
        }
      } else {
        // xlow empty, fill xhigh
        xlow_y_split = max_size_t;
        xlow_ylow_z_split = max_size_t;
        xlow_yhigh_z_split = max_size_t;
        xhigh_y_split = this->SortAndSplit(node->first_,
                                           node->last_,
                                           SortSplitType::kY,
                                           node->center_.y_);
        if (xhigh_y_split != max_size_t) {
          xhigh_ylow_z_split = this->SortAndSplit(node->first_,
                                                  xhigh_y_split,
                                                  SortSplitType::kZ,
                                                  node->center_.z_);
          xhigh_yhigh_z_split = this->SortAndSplit(xhigh_y_split + 1,
                                                  node->last_,
                                                  SortSplitType::kZ,
                                                  node->center_.z_);
        } else {
          // xhigh_ylow empty
          xhigh_ylow_z_split = max_size_t;
          xhigh_yhigh_z_split = this->SortAndSplit(node->first_,
                                                   node->last_,
                                                   SortSplitType::kZ,
                                                   node->center_.z_);
        }
      }

      // Populate children
      Vector3<T> width = node->half_edge_;
      if (x_split != max_size_t) {
        // ---------------------------------------------------------------
        // Child 0, 1
        if (xlow_y_split != max_size_t) {
          if (xlow_ylow_z_split != max_size_t) {
            // Add children if needed
            if (node->first_ <= xlow_ylow_z_split) {
              // child[0]
              AABB<T> bbox(node->bbox_.min_.x_,
                           node->bbox_.min_.x_ + width.x_,
                           node->bbox_.min_.y_,
                           node->bbox_.min_.y_ + width.y_,
                           node->bbox_.min_.z_,
                           node->bbox_.min_.z_ + width.z_, -1);
              node->child_[0] = new OCTreeNode<T>(node->first_,
                                                  xlow_ylow_z_split,
                                                  bbox ,
                                                  node->level_ + 1);
              if (node->child_[0]->Size() > bucket_size_) {
                stack.push(node->child_[0]);
              }
            }
          } else {
            xlow_ylow_z_split = node->first_ - 1;
          }
          if (xlow_ylow_z_split < xlow_y_split ||
              xlow_ylow_z_split == max_size_t) {
            // child[1]
            AABB<T> bbox(node->bbox_.min_.x_,
                         node->bbox_.min_.x_ + width.x_,
                         node->bbox_.min_.y_,
                         node->bbox_.min_.y_ + width.y_,
                         node->bbox_.min_.z_  + width.z_,
                         node->bbox_.max_.z_, -1);
            node->child_[1] = new OCTreeNode<T>(xlow_ylow_z_split + 1,
                                                xlow_y_split,
                                                bbox,
                                                node->level_ + 1);
            if (node->child_[1]->Size() > bucket_size_) {
              stack.push(node->child_[1]);
            }
          }
        } else {
          // xlow_ylow does not exist
          xlow_y_split = node->first_ - 1;
        }

        // ---------------------------------------------------------------
        // Child 2, 3

        // Treat xlow_yhigh region
        if (xlow_yhigh_z_split != max_size_t) {
          if (xlow_y_split < xlow_yhigh_z_split || xlow_y_split == max_size_t) {
            // Child[2] ?
            AABB<T> bbox(node->bbox_.min_.x_,
                         node->bbox_.min_.x_ + width.x_,
                         node->bbox_.min_.y_ + width.y_,
                         node->bbox_.max_.y_,
                         node->bbox_.min_.z_,
                         node->bbox_.min_.z_ + width.z_, -1);
            node->child_[2] = new OCTreeNode<T>(xlow_y_split + 1,
                                                xlow_yhigh_z_split,
                                                bbox,
                                                node->level_ + 1);
            if (node->child_[2]->Size() > bucket_size_) {
              stack.push(node->child_[2]);
            }
          }
        } else {
          xlow_yhigh_z_split = xlow_y_split;
        }

        if (xlow_yhigh_z_split < x_split || xlow_yhigh_z_split == max_size_t) {
          // Child[3]
          AABB<T> bbox(node->bbox_.min_.x_,
                       node->bbox_.min_.x_ + width.x_,
                       node->bbox_.min_.y_ + width.y_,
                       node->bbox_.max_.y_,
                       node->bbox_.min_.z_ + width.z_,
                       node->bbox_.max_.z_, -1);
          node->child_[3] = new OCTreeNode<T>(xlow_yhigh_z_split + 1,
                                              x_split,
                                              bbox,
                                              node->level_ + 1);
          if (node->child_[3]->Size() > bucket_size_) {
            stack.push(node->child_[3]);
          }
        }
      } else {
        // xlow does not exist
        x_split = node->first_ - 1;
      }

      // ---------------------------------------------------------------
      // Child 4, 5
      if (xhigh_y_split != max_size_t) {
        if (xhigh_ylow_z_split != max_size_t) {
          if (x_split < xhigh_ylow_z_split || x_split == max_size_t) {
            // Child[4]
            AABB<T> bbox(node->bbox_.min_.x_ + width.x_,
                         node->bbox_.max_.x_ ,
                         node->bbox_.min_.y_ ,
                         node->bbox_.min_.y_ + width.y_,
                         node->bbox_.min_.z_,
                         node->bbox_.min_.z_ + width.z_, -1);
            node->child_[4] = new OCTreeNode<T>(x_split + 1,
                                                xhigh_ylow_z_split,
                                                bbox, node->level_ + 1);
            if (node->child_[4]->Size() > bucket_size_) {
              stack.push(node->child_[4]);
            }
          }
        } else {
          xhigh_ylow_z_split = x_split;
        }

        if (xhigh_ylow_z_split < xhigh_y_split ||
            xhigh_ylow_z_split == max_size_t) {
          // Child[5]
          AABB<T> bbox(node->bbox_.min_.x_ + width.x_,
                       node->bbox_.max_.x_ ,
                       node->bbox_.min_.y_ ,
                       node->bbox_.min_.y_ + width.y_,
                       node->bbox_.min_.z_  + width.z_,
                       node->bbox_.max_.z_, -1);
          node->child_[5] = new OCTreeNode<T>(xhigh_ylow_z_split + 1,
                                              xhigh_y_split,
                                              bbox,
                                              node->level_ + 1);
          if (node->child_[5]->Size() > bucket_size_) {
            stack.push(node->child_[5]);
          }
        }
      } else {
        xhigh_y_split = x_split;
      }

      // ---------------------------------------------------------------
      // Child 6, 7
      if (xhigh_yhigh_z_split != max_size_t) {
        if (xhigh_y_split < xhigh_yhigh_z_split ||
            xhigh_y_split == max_size_t) {
          // child[6]
          AABB<T> bbox(node->bbox_.min_.x_ + width.x_,
                       node->bbox_.max_.x_ ,
                       node->bbox_.min_.y_  + width.y_,
                       node->bbox_.max_.y_,
                       node->bbox_.min_.z_,
                       node->bbox_.min_.z_  + width.z_, -1);
          node->child_[6] = new OCTreeNode<T>(xhigh_y_split + 1,
                                              xhigh_yhigh_z_split,
                                              bbox,
                                              node->level_ + 1);
          if (node->child_[6]->Size() > bucket_size_) {
            stack.push(node->child_[6]);
          }
        }
      } else {
        xhigh_yhigh_z_split = xhigh_y_split;
      }

      if (xhigh_yhigh_z_split <= node->last_ ||
          xhigh_yhigh_z_split == max_size_t) {
        if (xhigh_yhigh_z_split < node->last_ ||
            xhigh_yhigh_z_split == max_size_t) {
          AABB<T> bbox(node->bbox_.min_.x_ + width.x_,
                       node->bbox_.max_.x_,
                       node->bbox_.min_.y_ + width.y_,
                       node->bbox_.max_.y_,
                       node->bbox_.min_.z_ + width.z_,
                       node->bbox_.max_.z_, -1);
          node->child_[7] = new OCTreeNode<T>(xhigh_yhigh_z_split + 1,
                                              node->last_,
                                              bbox,
                                              node->level_ + 1);
          if (node->child_[7]->Size() > bucket_size_) {
            stack.push(node->child_[7]);
          }
        }
      }

      std::size_t sum = 0;
      for (int i = 0; i < 8; ++i) {
          sum += node->child_[i] ? node->child_[i]->Size() : 0;
      }
      assert(sum == node->Size());
      count++;
    }
//    // ################## DEBUG #####################3333
//    std::stack<OCTreeNode<T>*> queue;
//    queue.push(root_node_);
//
//    std::vector<OCTreeNode<T>*> leafs;
//    while (!queue.empty()) {
//      auto* node = queue.top();
//      queue.pop();
//
//      if (node->IsLeaf()) {
//        leafs.push_back(node);
//      } else {
//        for (int i = 0; i < 8; ++i) {
//          if (node->child_[i]) {
//            queue.push(node->child_[i]);
//          }
//        }
//      }
//    }
//
//    for (const auto* leaf : leafs) {
//      T d = (leaf->center_ - Vector3<T>(-0.7982, 0.6827, 131.31)).Norm();
//      if (d < 3.f) {
//        std::cout << "*** Leaf ***" << std::endl;
//        std::cout << "Dist: " << d << std::endl;
//        std::cout << leaf->bbox_ <<  std::endl;
//        std::cout << "First: " << leaf->first_ << std::endl;
//        std::cout << "Last: " << leaf->last_ << std::endl;
//        std::cout << "  *** Pts ***" << std::endl;
//        for (auto i = leaf->first_; i <= leaf->last_; ++i) {
//          std::cout << "  " << data_[index_map_[i]] << std::endl;
//        }
//      }
//    }
//    // Dump each cells
//    std::ofstream stream("tree.txt");
//    stream << leafs.size() << std::endl;
//    for (auto k = 0; k < leafs.size(); ++k) {
//      const auto* leaf = leafs[k];
//      auto sz = leaf->Size();
//      stream << "child" << k << std::endl;
//      stream << sz << std::endl;
//      stream << leaf->bbox_.min_ << std::endl;
//      stream << leaf->bbox_.max_ << std::endl;
//      for (int p = leaf->first_; p <= leaf->last_; ++p) {
//        stream << data_[index_map_[p]] << std::endl;
//      }
//    }
  }
}

/*
 * @name  Clear
 * @fn  void Clear()
 * @brief Clear the whole tree
 */
template< typename T>
void OCTree<T>::Clear() {
  if (root_node_) {
    std::stack<OCTreeNode<T>*> stack;
    stack.push(root_node_);
    while (!stack.empty()) {
      OCTreeNode<T>* node = stack.top();
      stack.pop();
      for (int i = 0; i < 8; ++i) {
        if (node->child_[i] != nullptr) {
          stack.push(node->child_[i]);
        }
      }
      delete node;
    }
  }
}

#pragma mark -
#pragma mark Usage

/*
 * @name  DoIntersect
 * @fn  bool DoIntersect(const Vector3<T>& p, const Vector3<T>& dir,
 *                        std::vector<int>* idx)
 * @brief Check if a given ray ,p + t*dir, intersect with one of the
 *        triangle's bounding box in the tree and provide its corresponding
 *        index
 *  @param[in]  p   Starting point of the ray
 *  @param[in]  dir Direction of the the ray (UNIT vector)
 *  @param[out] idx Index of the corresponding intersection
 *  @return True if intersect, false otherwise
 */
template< typename T>
bool OCTree<T>::DoIntersect(const Vector3<T>& p,
                            const Vector3<T>& dir,
                            std::vector<int>* idx) {
  // Tree init
  bool intersect = false;
  if (root_node_) {
    // Put origin at bbox min
    Vector3<T> org = p;
    Vector3<T> ray_dir = dir;
    ray_dir.Normalize();
    const Vector3<T> width = root_node_->bbox_.max_ - root_node_->bbox_.min_;
    // Define symetric problem if dir has negative component
    RayTraversal helper;
//    Vector3<T> o;
    if (ray_dir.x_ < T(0.0)) {
      // mirror ray around cude center - x axis
      org.x_ = (T(2.0) * root_node_->center_.x_) - org.x_;
      ray_dir.x_ = -ray_dir.x_;
      helper.ray_flag_ |= 0x4;
    }
    if (ray_dir.y_ < T(0.0)) {
      // mirror ray around cude center - y axis
      org.y_ = (T(2.0) * root_node_->center_.y_) - org.y_;
      ray_dir.y_ = -ray_dir.y_;
      helper.ray_flag_ |= 0x2;
    }
    if (ray_dir.z_ < T(0.0)) {
      // mirror ray around cude center - z axis
      org.z_ = (T(2.0) * root_node_->center_.z_) - org.z_;
      ray_dir.z_ = -ray_dir.z_;
      helper.ray_flag_ |= 0x1;
    }
    // Compute t value, handle parallel ray to main axis
    const Vector3<T> inv_dir(T(1.0) / ray_dir.x_,
                             T(1.0) / ray_dir.y_,
                             T(1.0) / ray_dir.z_);
    if (ray_dir.x_ == T(0.0)) {
      helper.entry_.x_ = ((root_node_->bbox_.min_.x_ -org.x_) < T(0.0) ?
                         -std::numeric_limits<T>::infinity() :
                         std::numeric_limits<T>::infinity());
      helper.exit_.x_ = ((root_node_->bbox_.max_.x_ -org.x_) < T(0.0) ?
                         -std::numeric_limits<T>::infinity() :
                         std::numeric_limits<T>::infinity());
    } else {
      helper.entry_.x_ = (root_node_->bbox_.min_.x_ -org.x_) * inv_dir.x_;
      helper.exit_.x_ =  (root_node_->bbox_.max_.x_ -org.x_) * inv_dir.x_;
    }

    if (ray_dir.y_ == T(0.0)) {
      helper.entry_.y_ = ((root_node_->bbox_.min_.y_ -org.y_) < T(0.0) ?
                          -std::numeric_limits<T>::infinity() :
                          std::numeric_limits<T>::infinity());
      helper.exit_.y_ = ((root_node_->bbox_.max_.y_ -org.y_) < T(0.0) ?
                         -std::numeric_limits<T>::infinity() :
                         std::numeric_limits<T>::infinity());
    } else {
      helper.entry_.y_ = (root_node_->bbox_.min_.y_ -org.y_) * inv_dir.y_;
      helper.exit_.y_ = (root_node_->bbox_.max_.y_ -org.y_) * inv_dir.y_;
    }

    if (ray_dir.z_ == T(0.0)) {
      helper.entry_.z_ = ((root_node_->bbox_.min_.z_ -org.z_) < T(0.0) ?
                          -std::numeric_limits<T>::infinity() :
                          std::numeric_limits<T>::infinity());
      helper.exit_.z_ = ((root_node_->bbox_.max_.z_ -org.z_) < T(0.0) ?
                         -std::numeric_limits<T>::infinity() :
                         std::numeric_limits<T>::infinity());
    } else {
      helper.entry_.z_ = (root_node_->bbox_.min_.z_ -org.z_) * inv_dir.z_;
      helper.exit_.z_ = (root_node_->bbox_.max_.z_ -org.z_) * inv_dir.z_;
    }
    // Check if ray intersect with root node
    T max_entry = std::max(helper.entry_.x_,
                           std::max(helper.entry_.y_,
                                    helper.entry_.z_));
    T min_exit = std::max(helper.exit_.x_,
                          std::max(helper.exit_.y_,
                                   helper.exit_.z_));
    if (max_entry < min_exit) {
      // Process tree
      helper.node_ = root_node_;
      helper.level_ = 0;
      helper.id_ = 0;
      std::stack<RayTraversal> stack;
      stack.push(helper);
      // Iterate over tree
      while (!stack.empty()) {
        // Get top of stack
        RayTraversal node = stack.top();
        stack.pop();
        // Children present ?
        if (node.node_ == nullptr) {
          continue;
        }
        // Valid exit point ?
        if (node.exit_.x_ < T(0.0) ||
            node.exit_.y_ < T(0.0) ||
            node.exit_.z_ < T(0.0)) {
          continue;
        }
        // Reach leaf ?
        if (node.node_->IsLeaf()) {
          for (auto i = node.node_->first_; i <= node.node_->last_; ++i) {
            idx->push_back(static_cast<int>(index_map_[i]));
          }
          continue;
        }

        // Compute middle parametric value, handle parallel ray
        Vector3<T> center = node.node_->center_;
        Vector3<T> mid;
        if (ray_dir.x_ == T(0.0)) {
          mid.x_ = (org.x_ < center.x_) ?
                   std::numeric_limits<T>::infinity() :
                   -std::numeric_limits<T>::infinity();
        } else {
          mid.x_ = (node.entry_.x_ + node.exit_.x_) * T(0.5);
        }
        if (ray_dir.y_ == T(0.0)) {
          mid.y_ = (org.y_ < center.y_) ?
                   std::numeric_limits<T>::infinity() :
                   -std::numeric_limits<T>::infinity();
        } else {
          mid.y_ = (node.entry_.y_ + node.exit_.y_) * T(0.5);
        }
        if (ray_dir.z_ == T(0.0)) {
          mid.z_ = (org.z_ < center.z_) ?
                   std::numeric_limits<T>::infinity() :
                   -std::numeric_limits<T>::infinity();
        } else {
          mid.z_ = (node.entry_.z_ + node.exit_.z_) * T(0.5);
        }
        // Get first node hit
        unsigned char node_idx = RayTraversal::GetFirstNode(node.entry_,
                                                            mid);
        // Loop over children
        RayTraversal next;
        do {
          switch (node_idx) {
            // child[0]
            case 0: {
              next.entry_ = node.entry_;
              next.exit_ = mid;
              next.node_ = node.node_->child_[node.ray_flag_];
              next.level_ = node.level_ + 1;
              next.ray_flag_ = node.ray_flag_;
              next.id_ = 0;
              stack.push(next);
              node_idx = RayTraversal::GetNextNode(node_idx,
                                                   next.exit_.x_,
                                                   next.exit_.y_,
                                                   next.exit_.z_);
            }
              break;
            // child[1]
            case 1: {
              next.entry_.x_ = node.entry_.x_;
              next.entry_.y_ = node.entry_.y_;
              next.entry_.z_ = mid.z_;
              next.exit_.x_ = mid.x_;
              next.exit_.y_ = mid.y_;
              next.exit_.z_ = node.exit_.z_;
              next.node_ = node.node_->child_[node.ray_flag_^0x01];
              next.level_ = node.level_ + 1;
              next.ray_flag_ = node.ray_flag_;
              next.id_ = (node.ray_flag_^static_cast<unsigned char>(0x01));
              stack.push(next);
              node_idx = RayTraversal::GetNextNode(node_idx,
                                                   next.exit_.x_,
                                                   next.exit_.y_,
                                                   next.exit_.z_);
            }
              break;
            // child[2]
            case 2: {
              next.entry_.x_ = node.entry_.x_;
              next.entry_.y_ = mid.y_;
              next.entry_.z_ = node.entry_.z_;
              next.exit_.x_ = mid.x_;
              next.exit_.y_ = node.exit_.y_;
              next.exit_.z_ = mid.z_;
              next.node_ = node.node_->child_[node.ray_flag_^2];
              next.level_ = node.level_ + 1;
              next.ray_flag_ = node.ray_flag_;
              next.id_ = (node.ray_flag_^static_cast<unsigned char>(0x02));
              stack.push(next);
              node_idx = RayTraversal::GetNextNode(node_idx,
                                                   next.exit_.x_,
                                                   next.exit_.y_,
                                                   next.exit_.z_);
            }
              break;
              // child[3]
            case 3: {
              next.entry_.x_ = node.entry_.x_;
              next.entry_.y_ = mid.y_;
              next.entry_.z_ = mid.z_;
              next.exit_.x_ = mid.x_;
              next.exit_.y_ = node.exit_.y_;
              next.exit_.z_ = node.exit_.z_;
              next.node_ = node.node_->child_[node.ray_flag_^0x03];
              next.level_ = node.level_ + 1;
              next.ray_flag_ = node.ray_flag_;
              next.id_ = (node.ray_flag_^static_cast<unsigned char>(0x03));
              stack.push(next);
              node_idx = RayTraversal::GetNextNode(node_idx,
                                                   next.exit_.x_,
                                                   next.exit_.y_,
                                                   next.exit_.z_);
            }
              break;
              // child[4]
            case 4: {
              next.entry_.x_ = mid.x_;
              next.entry_.y_ = node.entry_.y_;
              next.entry_.z_ = node.entry_.z_;
              next.exit_.x_ = node.exit_.x_;
              next.exit_.y_ = mid.y_;
              next.exit_.z_ = mid.z_;
              next.node_ = node.node_->child_[node.ray_flag_^0x04];
              next.level_ = node.level_ + 1;
              next.ray_flag_ = node.ray_flag_;
              next.id_ = (node.ray_flag_^static_cast<unsigned char>(0x04));
              stack.push(next);
              node_idx = RayTraversal::GetNextNode(node_idx,
                                                   next.exit_.x_,
                                                   next.exit_.y_,
                                                   next.exit_.z_);
            }
              break;
              // child[5]
            case 5: {
              next.entry_.x_ = mid.x_;
              next.entry_.y_ = node.entry_.y_;
              next.entry_.z_ = mid.z_;
              next.exit_.x_ = node.exit_.x_;
              next.exit_.y_ = mid.y_;
              next.exit_.z_ = node.exit_.z_;
              next.node_ = node.node_->child_[node.ray_flag_^0x05];
              next.level_ = node.level_ + 1;
              next.ray_flag_ = node.ray_flag_;
              next.id_ = (node.ray_flag_^static_cast<unsigned char>(0x05));
              stack.push(next);
              node_idx = RayTraversal::GetNextNode(node_idx,
                                                   next.exit_.x_,
                                                   next.exit_.y_,
                                                   next.exit_.z_);
            }
              break;
              // child[6]
            case 6: {
              next.entry_.x_ = mid.x_;
              next.entry_.y_ = mid.y_;
              next.entry_.z_ = node.entry_.z_;
              next.exit_.x_ = node.exit_.x_;
              next.exit_.y_ = node.exit_.y_;
              next.exit_.z_ = mid.z_;
              next.node_ = node.node_->child_[node.ray_flag_^0x06];
              next.level_ = node.level_ + 1;
              next.ray_flag_ = node.ray_flag_;
              next.id_ = (node.ray_flag_^static_cast<unsigned char>(0x06));
              stack.push(next);
              node_idx = RayTraversal::GetNextNode(node_idx,
                                                   next.exit_.x_,
                                                   next.exit_.y_,
                                                   next.exit_.z_);
            }
              break;
              // child[7]
            case 7: {
              next.entry_.x_ = mid.x_;
              next.entry_.y_ = mid.y_;
              next.entry_.z_ = mid.z_;
              next.exit_.x_ = node.exit_.x_;
              next.exit_.y_ = node.exit_.y_;
              next.exit_.z_ = node.exit_.z_;
              next.node_ = node.node_->child_[node.ray_flag_^0x07];
              next.level_ = node.level_ + 1;
              next.ray_flag_ = node.ray_flag_;
              next.id_ = (node.ray_flag_^static_cast<unsigned char>(0x07));
              stack.push(next);
              node_idx = 8;
            }
              break;

            default:
              std::cout << "Error, should not reach this point !" << std::endl;
          }
        } while (node_idx < 8);
      }
      intersect = !idx->empty();
    }
  }
  return intersect;
}

/*
 * @name  RadiusNearestNeighbor
 * @fn  void RadiusNearestNeighbor(const Sphere<T>& sphere,
                                   std::vector<int>* idx)
 * @brief Find all point laying inside a given sphere
 * @param[in]   sphere    Sphere defining search region
 * @param[out]  idx       List of primitive satisfying the condition
 */
template<typename T>
void OCTree<T>::RadiusNearestNeighbor(const Sphere<T>& sphere,
                                      std::vector<int>* idx) {
  // Setup output
  idx->clear();
  std::vector<std::size_t> candidate;
  // Start search
  std::stack<OCTreeNode<T>*> stack;
  stack.push(root_node_);
  while (!stack.empty()) {
    OCTreeNode<T>* node = stack.top();
    stack.pop();
    // Check if node intersection with sphere
    if (AABB<T>::IntersectSphere(node->bbox_, sphere)) {
      // Has reach a leaf ?
      if (node->IsLeaf()) {
        // Push candidate
        for (std::size_t i = node->first_; i <= node->last_; ++i) {
          candidate.push_back(index_map_[i]);
        }
      } else {
        // Push children
        for (int i = 0; i < 8; ++i) {
          if (node->child_[i] != nullptr) {
            stack.push(node->child_[i]);
          }
        }
      }
    }
  }
  // Check candidates
  const T* sph_ptr = &sphere.center_.x_;
  const T sq_radius = sphere.radius_ * sphere.radius_;
  for (const auto& c : candidate) {
    // Compute distance
    T sq_dist = T(0.0);
    const T* vert_ptr = &data_[c].x_;
    for (int j = 0; j < 3; ++j) {
      sq_dist += (vert_ptr[j] - sph_ptr[j]) * (vert_ptr[j] - sph_ptr[j]);
    }
    if (sq_dist <= sq_radius) {
      // Inside
      idx->push_back(static_cast<int>(c));
    }
  }
}

#pragma mark -
#pragma mark Private

/*
 * @name  SortAndSplit
 * @fn  std::size_t SortAndSplit(std::size_t first,
 *                               std::size_t last,
 *                               SortSplitType direction,
 *                               const T threshold)
 * @brief Sort index in the map from first to last. Return index of the
 *        element right below a given threshold
 * @param[in] first       First index
 * @param[in] last        Last index
 * @param[in] direction   Direction in which to perform sort
 * @param[in] threshold   Threshold value for the split
 * @return  Element right below threshold
 */
template< typename T>
std::size_t OCTree<T>::SortAndSplit(std::size_t first,
                                    std::size_t last,
                                    const SortSplitType& direction,
                                    const T& threshold) {
  // Check undefined range
  if (first == max_size_t ||
      last == max_size_t) {
    return max_size_t;
  }
  if (first > last) {
    return first - 1;
  }
  // Save entry point
  std::size_t org_first = first;
  // Start to loop
  while (first < last) {
    // Find first above threshold
    while (get_element(first, direction) < threshold && first < last) {
      first++;
    }
    // Check if last has been reached
    if (first == last) {
      return (get_element(first, direction) < threshold ?
              first :
              (first == org_first) ?
              max_size_t :
              first - 1);
    }
    // Find last below threshold
    while (get_element(last, direction) >= threshold && last > first) {
      last--;
    }
    // Check if first has been reached
    if (last == first) {
      return (get_element(first, direction) < threshold ?
              first :
              (first == org_first) ?
              max_size_t :
              first - 1);
    }

    // Swap
    std::swap(index_map_[first], index_map_[last]);
    first++;
    last--;
  }
  // Return
  return (get_element(first, direction) < threshold ?
          first :
          (first == org_first) ?
          max_size_t :
          first - 1);
}


/*
 * @name  get_element
 * @fn  T get_element(const std::size_t index, const SortSplitType direction)
 * @brief Get element value from original data structure for a given
 *        direction and index
 * @param[in] index       Index to query
 * @param[in] direction   Direction selection
 * @return    Element value
 */
template< typename T>
T OCTree<T>::get_element(const std::size_t& index,
                         const SortSplitType& direction) {
  assert(data_.size() > 0 && data_.size() == index_map_.size());
  const T* ptr = &(data_[index_map_[index]].x_);
  return ptr[direction];
}

/**
 * @struct  RayTraversal
 * @brief   Goes through the tree to detect collision with a given ray
 * @author  Christophe Ecabert
 * @date    08/03/16
 * @see http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.29.987&rep=rep1&type=pdf
 */
template< typename T>
struct OCTree<T>::RayTraversal {
  /** Unsigned char shortcut */
  using uchar = unsigned char;
  /** Node level */
  std::size_t level_;
  /** Node ID [0,7]*/
  uchar id_;
  /** Ray entry point, one for each axis */
  Vector3<T> entry_;
  /** Ray exit point, one for each axis */
  Vector3<T> exit_;
  /** Tree node */
  OCTreeNode<T>* node_;
  /** Ray flag, in case of negative direction : bits X,Y,Z */
  uchar ray_flag_;
  /** Next node lookup table, (node x plane) */
  static constexpr uchar node_lut_[8][3] {
          // Exit plane : YZ, XZ, XY
          {4, 2, 1},  // Node 0
          {5, 3, 8},  // Node 1
          {6, 8, 3},  // Node 2
          {7, 8, 8},  // Node 3
          {8, 6, 5},  // Node 4
          {8, 7, 8},  // Node 5
          {8, 8, 7},  // Node 6
          {8, 8, 8},  // Node 7
  };

  /**
   * @name  RayTraversal
   * @fn  RayTraversal()
   * @brief Constructor
   */
  RayTraversal() :
          level_(0),
          id_(0),
          entry_(T(0.0), T(0.0), T(0.0)),
          exit_(T(0.0), T(0.0), T(0.0)),
          node_(nullptr),
          ray_flag_(0) {}

  /**
   * @name  RayTraversal
   * @fn  RayTraversal(const RayTraversal& other)
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  RayTraversal(const RayTraversal& other) {
    if (this != &other) {
      level_ = other.level_;
      id_ = other.id_;
      entry_ = other.entry_;
      exit_ = other.exit_;
      node_ = other.node_;
      ray_flag_ = other.ray_flag_;
    }
  }

  /**
   * @name  operator=
   * @fn  RayTraversal& operator=(const RayTraversal& rhs)
   * @brief Assignment operator
   * @param[in] rhs   Object to assign from
   */
  RayTraversal& operator=(const RayTraversal& rhs) {
    if (this != &rhs) {
      level_ = rhs.level_;
      id_ = rhs.id_;
      entry_ = rhs.entry_;
      exit_ = rhs.exit_;
      node_ = rhs.node_;
      ray_flag_ = rhs.ray_flag_;
    }
    return *this;
  }

  /**
   * @name  GetFirstNode
   * @fn  static uchar GetFirstNode(const Vector3<T>& entry,
   *                                const Vector3<T>& mid)
   * @brief Determine which node is hit by the given ray.
   * @param[in]   entry   Entry point parametric value (t value)
   * @param[in]   mid     Middle point parametric value (t value)
   */
  static uchar GetFirstNode(const Vector3<T>& entry, const Vector3<T>& mid) {
    // Find entry plane by looking at max(entry_x, entry_y, entry_z).
    uchar node = 0;
    if (entry.x_ > entry.y_) {
      if (entry.x_ > entry.z_) {
        // Max X -> plane YZ
        node |= mid.y_ < entry.x_ ? uchar(0x2) : uchar(0x0);
        node |= mid.z_ < entry.x_ ? uchar(0x1) : uchar(0x0);
        return node;
      }
    } else if (entry.y_ > entry.z_) {
      // Max Y -> plane XZ
      node |= mid.x_ < entry.y_ ? uchar(0x4) : uchar(0x0);
      node |= mid.z_ < entry.y_ ? uchar(0x1) : uchar(0x0);
      return node;
    }
    // Max Z -> plane XY
    node |= mid.x_ < entry.z_ ? uchar(0x4) : uchar(0x0);
    node |= mid.y_ < entry.z_ ? uchar(0x2) : uchar(0x0);

    return node;
  }

  /**
   * @name  GetNextNode
   * @fn  static uchar GetNextNode(const uchar node_idx,const Vector3<T>& exit)
   * @brief Determine the next node hit from a given node and t value.
   * @param[in]   node_idx  Current node index [0-7]
   * @param[in]   exit_x    Exit point X parametric value (t value)
   * @param[in]   exit_y    Exit point Y parametric value (t value)
   * @param[in]   exit_z    Exit point Z parametric value (t value)
   */
  static uchar GetNextNode(const uchar node_idx,
                           const T exit_x,
                           const T exit_y,
                           const T exit_z) {
    // Exit plane
    // YZ = 0
    // XZ = 1
    // XY = 2
    // Define exit plane, min tx val
    T t_min;
    uchar exit_plane;
    if (exit_x < exit_y) {
      // Min = tx
      t_min = exit_x;
      exit_plane = 0;
    } else {
      // Min = ty
      t_min = exit_y;
      exit_plane = 1;
    }
    // check for z
    if (exit_z < t_min) {
      exit_plane = 2;
    }
    // Query LUT
    return node_lut_[node_idx][exit_plane];
  }
};
template<typename T>
constexpr unsigned char OCTree<T>::RayTraversal::node_lut_[8][3];

#pragma mark -
#pragma mark Declaration

/** Float type OCTree */
template class OCTree<float>;
/** Float type OCTree */
template class OCTree<double>;

}  // namespace LTS5
