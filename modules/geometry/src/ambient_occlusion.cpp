/**
 *  @file   ambient_occlusion.cpp
 *  @brief  Compute Ambient Occlusion for a given mesh
 *  @ingroup    geometry
 *
 *  @author Christophe Ecabert
 *  @date   11/13/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <chrono>

#include "lts5/geometry/ambient_occlusion.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/utils/math/rodrigues.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Sampler

/*
 * @name    HalfSphereSampler
 * @fn      HalfSphereSampler()
 * @brief   Constructor
 */
template<typename T>
AmbientOcclusionGenerator<T>::
HalfSphereSampler::HalfSphereSampler() :
        gen_(std::chrono::system_clock::now().time_since_epoch().count()),
        dist_(T(0.0), T(1.0)) {
}

/*
 * @name    operator()
 * @fn  Vec3 operator()()
 * @brief Sample 3D pts randomly on a half sphere.
 * @return  Sampled points
 */
template<typename T>
Vector3<T> AmbientOcclusionGenerator<T>::HalfSphereSampler::operator()() {
  Vec3 pt;
  pt.x_ = dist_(gen_);
  pt.y_ = dist_(gen_);
  pt.z_ = dist_(gen_);
  // normalize
  pt.Normalize();
  pt.z_ = std::abs(pt.z_);
  return pt;
}

#pragma mark -
#pragma mark Initialization

/*
 * @name  AmbientOcclusionGenerator
 * @fn    AmbientOcclusionGenerator(Mesh* mesh)
 * @brief Constructor
 * @param[in] mesh    Mesh
 */
template<typename T>
AmbientOcclusionGenerator<T>::
AmbientOcclusionGenerator(Mesh_t& mesh) : mesh_(&mesh),
                                          tree_(),
                                          sampler_() {
  // Ensure normals are computed
  mesh_->ComputeVertexNormal();
  // Compute sampling radius
  const auto& bbox = mesh.bbox();
  auto diff = bbox.max_ - bbox.min_;
  this->radius_ = std::max(diff.x_, std::max(diff.y_, diff.z_));
  // Build octree
  this->tree_.Insert(mesh, Mesh_t::PrimitiveType::kTriangle);
  this->tree_.Build();
  // Init AO buffer
  this->ao_.create(mesh.get_vertex().size(), 1,cv::DataType<T>::type);
}

#pragma mark -
#pragma mark Usage

/**
 * @name  Generate
 * @fn    void Generate(const int& n_sample)
 * @brief Generate Ambient Occlusion using Monte Carlo sampling
 * @param[in] n_sample Number of sample to use to estimate AO.
 */
template<typename T>
void AmbientOcclusionGenerator<T>::Generate(const int& n_sample) {
  // Access data
  Vector3<T> z_axis = Vector3<T>(0.0, 0.0, 1.0);
  const auto& vertex = this->mesh_->get_vertex();
  const auto& normal = this->mesh_->get_normal();
  const auto n = vertex.size();
  // Run processing of AO in parallel
  auto fcn = [&](const size_t& k) {
    const auto& v = vertex[k];
    const auto& n = normal[k];
    // Define rotation matrix
    // -> Axis
    auto axis = z_axis ^ n;
    axis.Normalize();
    // -> Angle
    T dot = z_axis * n;
    dot = dot > T(1.0) ? T(1.0) : dot;
    dot = dot < T(-1.0) ? T(-1.0) : dot;
    T ang = std::acos(dot);
    // Rmat
    Matrix3x3<T> rmat;
    auto r = Rodrigues<T>(axis, ang);
    r.ToRotationMatrix(&rmat);
    // Sample
    size_t cnt = 0;
    for (int i = 0; i < n_sample; ++i) {
      // Sample points on sphere
      auto pts = this->sampler_();
      pts = (rmat * pts) * this->radius_;
      // Define segment + look for intersection
      auto p = v + pts;
      auto q = v;
      T t = T(-1.0);
      bool inter = this->tree_.IntersectWithSegment(*this->mesh_,
                                                    p,
                                                    q,
                                                    false,
                                                    T(0.99),
                                                    &t);
      if (!inter) {
        // No intersection, count it
        cnt += 1;
      }
    }
    this->ao_.template at<T>(k) = (static_cast<T>(cnt) /
                                   static_cast<T>(n_sample));
  };
  Parallel::For(n, fcn);
}

/*
 * @name  Save
 * @fn    int Save(const std::string& path)
 * @brief Save ambient occlusion into a file
 * @param[in] path Location where to dump the data (i.e. AO)
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int AmbientOcclusionGenerator<T>::Save(const std::string& path) {
  return SaveMatToBin(path, this->ao_);
}


#pragma mark -
#pragma mark Explicit instantiation

/** Float */
template class AmbientOcclusionGenerator<float>;
/** Double */
template class AmbientOcclusionGenerator<double>;

}  // namespace LTS5