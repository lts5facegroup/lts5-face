/**
 *  @file   spherical_harmonic.cpp
 *  @brief  Compute spherical harmonics basis
 *  @ingroup    geometry
 *
 *  @author Christophe Ecabert
 *  @date   02/11/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#include "lts5/geometry/spherical_harmonic.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name  SphericalHarmonic
 * @fn    explicit SphericalHarmonic(const Normal& normal)
 * @brief Constructor
 * @param[in] normal Surface normal
 */
template<typename T, int B>
SphericalHarmonic<T, B>::SphericalHarmonic(const Normal& normal) {
  LTS5_LOG_WARNING("Fail safe, not implemented in primary template");
}

/*
 * @name  Generate
 * @fn    void Generate(const Normal& normal)
 * @brief Create spherical harmonics
 * @param[in] normal  Surface normal
 */
template<typename T, int B>
void SphericalHarmonic<T, B>::Generate(const Normal &normal) {
  LTS5_LOG_WARNING("Fail safe, not implemented in primary template");
}

/*
 * @name  operator()
 * @fn    void operator()(const cv::Mat& p, cv::Mat* projection) const
 * @brief Project a signal \p into the spherical harmonic basis.
 * @param[in] p  Signal to project. [N X B²] or [B² x 9]
 * @param[out] projection Projected signal
 */
template<typename T, int B>
void SphericalHarmonic<T, B>::operator()(const cv::Mat &p,
                                         cv::Mat *projection) const {
  LTS5_LOG_WARNING("Fail safe, not implemented in primary template");
}


/** Specialization - First Band */
template<typename T>
class LTS5_EXPORTS SphericalHarmonic<T, 1> {
 public:
  using Normal = typename LTS5::Mesh<T>::Normal;

  explicit SphericalHarmonic(const Normal& normal) {
    this->Generate(normal);
  }

  void Generate(const Normal& normal) {
    basis_.create(1, 1, cv::DataType<T>::type);
    basis_.at<T>(0, 0) = T(1.0);
  }

  void operator()(const cv::Mat& p, cv::Mat* projection) const {
    using TType = typename LinearAlgebra<T>::TransposeType;
    using LA = LinearAlgebra<T>;
    assert(p.cols == basis_.rows || p.rows == basis_.rows);
    TType tp = p.cols == basis_.rows ? TType::kNoTranspose : TType::kTranspose;
    LA::Gemv(p, tp, T(1.0), basis_, T(0.0), projection);
  }

  const T* data() const {
    return reinterpret_cast<T*>(basis_.data);
  }
 private:
  cv::Mat basis_;
};
/** Specialization - Second Band */
template<typename T>
class LTS5_EXPORTS SphericalHarmonic<T, 2> {
 public:
  using Normal = typename LTS5::Mesh<T>::Normal;

  explicit SphericalHarmonic(const Normal& normal) {
    this->Generate(normal);
  }

  void Generate(const Normal& normal) {
    basis_.create(4, 1, cv::DataType<T>::type);
    basis_.at<T>(0) = T(0.0);
    basis_.at<T>(1) = normal.x_;
    basis_.at<T>(2) = normal.y_;
    basis_.at<T>(3) = normal.z_;
  }

  void operator()(const cv::Mat& p, cv::Mat* projection) const {
    using TType = typename LinearAlgebra<T>::TransposeType;
    using LA = LinearAlgebra<T>;
    assert(p.cols == basis_.rows || p.rows == basis_.rows);
    TType tp = p.cols == basis_.rows ? TType::kNoTranspose : TType::kTranspose;
    LA::Gemv(p, tp, T(1.0), basis_, T(0.0), projection);
  }
  const T* data() const {
    return reinterpret_cast<T*>(basis_.data);
  }
 private:
  cv::Mat basis_;
};
/** Specialization - Thrid Band */
template<typename T>
class LTS5_EXPORTS SphericalHarmonic<T, 3> {
 public:
  using Normal = typename LTS5::Mesh<T>::Normal;

  explicit SphericalHarmonic(const Normal& normal) {
    this->Generate(normal);
  }

  void Generate(const Normal& normal) {
    basis_.create(9, 1, cv::DataType<T>::type);
    basis_.at<T>(0) = T(1.0);
    basis_.at<T>(1) = normal.x_;
    basis_.at<T>(2) = normal.y_;
    basis_.at<T>(3) = normal.z_;
    basis_.at<T>(4) = normal.x_ * normal.y_;
    basis_.at<T>(5) = normal.x_ * normal.z_;
    basis_.at<T>(6) = normal.y_ * normal.z_;
    basis_.at<T>(7) = (normal.x_ * normal.x_) - (normal.y_ * normal.y_);
    basis_.at<T>(8) = (T(3.0) * normal.z_ * normal.z_) - T(1.0);
  }

  void operator()(const cv::Mat& p, cv::Mat* projection) const {
    using TType = typename LinearAlgebra<T>::TransposeType;
    using LA = LinearAlgebra<T>;
    assert(p.cols == basis_.rows || p.rows == basis_.rows);
    TType tp = p.cols == basis_.rows ? TType::kNoTranspose : TType::kTranspose;
    LA::Gemv(p, tp, T(1.0), basis_, T(0.0), projection);
  }
  const T* data() const {
    return reinterpret_cast<T*>(basis_.data);
  }
 private:
  cv::Mat basis_;
};

#ifdef __APPLE__
template class SphericalHarmonic<float, 1>;
template class SphericalHarmonic<double, 1>;
template class SphericalHarmonic<float, 2>;
template class SphericalHarmonic<double, 2>;
template class SphericalHarmonic<float, 3>;
template class SphericalHarmonic<double, 3>;
#endif





}  // namepsace LTS5