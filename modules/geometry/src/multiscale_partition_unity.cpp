/**
 *  @file   multiscale_partition_unity.cpp
 *  @brief
 *
 *  @author Gabriel Cuendet
 *  @date   11/04/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <numeric>
#include <utility>
#include <stack>

#include "lts5/geometry/octree.hpp"
#include "lts5/geometry/aabc.hpp"
#include "lts5/geometry/multiscale_partition_unity.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  MPU
 * @fn  MPU(void)
 * @brief Constructor
 */
template< typename T>
MPU<T>::MPU(void) : nodes_(0), aabb_tree_(), alpha_(0.75),
n_min_(30), max_error_(1e-6),  max_depth_(8), max_tree_level_(0) {}

/*
 * @name  MPU
 * @fn  MPU(const double max_error, const int max_depth, const double alpha)
 * @brief Constructor
 * @param[in] max_error   Approximation error threshold
 * @param[in] max_depth   Maximum tree depth
 * @param[in] alpha       Constant by which to multiply main diagonal to get R
 */
template< typename T>
MPU<T>::MPU(const double max_error, const int max_depth, const double alpha)
: nodes_(0), aabb_tree_(), alpha_(alpha), n_min_(15), max_error_(max_error),
  max_depth_(max_depth),max_tree_level_(0) {}

/*
 * @name  ~MPU
 * @fn  ~MPU(void)
 * @brief Destructor
 */
template< typename T>
MPU<T>::~MPU(void) {
  // Delete tree
  this->Clear();
}

/*
 * @name  Build
 * @fn   void Build(void)
 * @brief Build iteratively the whole tree
 * @param[in] mesh      3D mesh used to build tree
 * @param[in] domain    Bounding cube of additional space domain to include
 */
template<typename T>
void MPU<T>::Build(const Mesh<T>& mesh, const AABC<T>& domain) {
  std::cout << "[MPU<T>::Build] Building the tree..." << std::endl;

  // Construct the octree for neighbors search problems
  OCTree<T> octree;
  octree.Insert(mesh, Mesh<T>::PrimitiveType::kPoint);
  octree.Build();

  // Initialize hash_map_ with the first node: the root node
  max_tree_level_ = 0;

  // Definition of the domain (Initial AABC)
  AABC<T> full_domain = AABC<T>(mesh.bbox()) + domain;

  MPUNode<T>* root_node = new MPUNode<T>(1, full_domain);
  root_node_ = new MPUNode<T>(1, domain);
  
  std::stack< MPUNode<T>* > stack;
  stack.push(root_node);

  while (!stack.empty()) {
    MPUNode<T>* node = stack.top();
    stack.pop();

    // Define maximum level
    max_tree_level_ = std::max(max_tree_level_, node->Level());

    // compute approximation!
    T Ri = alpha_ * node->diagonal();

    LTS5::Vector3<T> c_i = node->center();
    LTS5::Sphere<T> sph(c_i, Ri);
    std::vector<int> idx;
    octree.RadiusNearestNeighbor(sph, &idx);
    bool empty_ball = idx.size() == 0;

    while (idx.size() < n_min_) {
      Ri *= 1.1;
      sph.radius_ = Ri;
      octree.RadiusNearestNeighbor(sph, &idx);
    }

    // Pass the data points within the support ball to the node
    std::vector<Vertex> p_i(idx.size());
    std::vector<Normal> n_i(idx.size());
    for (int i = 0; i < idx.size(); ++i) {
      p_i[i] = mesh.get_vertex()[idx[i]];
      n_i[i] = mesh.get_normal()[idx[i]];
    }
    node->Approximate(p_i, n_i, Ri);

    // If reach maximum level, nothing to do
    if (node->Level() == max_depth_) {
      // Add the node to the list of leafs
      nodes_.push_back(node);
      continue;
    }

    if (!empty_ball && (node->epsilon() > max_error_)) {
      // Divide the node and add to the stack
      AABB<T> node_bbox = node->bbox();
      LTS5::Vector3<T> half_edge = (node_bbox.max_ - node_bbox.min_) * T(0.5);

      AABB<T> bbox0(node_bbox.min_.x_,
                    node_bbox.min_.x_ + half_edge.x_,
                    node_bbox.min_.y_,
                    node_bbox.min_.y_ + half_edge.y_,
                    node_bbox.min_.z_,
                    node_bbox.min_.z_ + half_edge.z_, -1);
      uint64_t new_location = (node->location() << 3) + 0;

      MPUNode<T>* node0 = new MPUNode<T>(new_location, bbox0);
      stack.push(node0);

      AABB<T> bbox1(node_bbox.min_.x_,
                    node_bbox.min_.x_ + half_edge.x_,
                    node_bbox.min_.y_,
                    node_bbox.min_.y_ + half_edge.y_,
                    node_bbox.min_.z_ + half_edge.z_,
                    node_bbox.max_.z_, -1);
      new_location = (node->location() << 3) + 1;

      MPUNode<T>* node1 = new MPUNode<T>(new_location, bbox1);
      stack.push(node1);

      AABB<T> bbox2(node_bbox.min_.x_,
                    node_bbox.min_.x_ + half_edge.x_,
                    node_bbox.min_.y_ + half_edge.y_,
                    node_bbox.max_.y_,
                    node_bbox.min_.z_,
                    node_bbox.min_.z_ + half_edge.z_, -1);
      new_location = (node->location() << 3) + 2;

      MPUNode<T>* node2 = new MPUNode<T>(new_location, bbox2);
      stack.push(node2);

      AABB<T> bbox3(node_bbox.min_.x_,
                    node_bbox.min_.x_ + half_edge.x_,
                    node_bbox.min_.y_ + half_edge.y_,
                    node_bbox.max_.y_,
                    node_bbox.min_.z_ + half_edge.z_,
                    node_bbox.max_.z_, -1);
      new_location = (node->location() << 3) + 3;

      MPUNode<T>* node3 = new MPUNode<T>(new_location, bbox3);
      stack.push(node3);

      AABB<T> bbox4(node_bbox.min_.x_ + half_edge.x_,
                    node_bbox.max_.x_,
                    node_bbox.min_.y_,
                    node_bbox.min_.y_ + half_edge.y_,
                    node_bbox.min_.z_,
                    node_bbox.min_.z_ + half_edge.z_, -1);
      new_location = (node->location() << 3) + 4;

      MPUNode<T>* node4 = new MPUNode<T>(new_location, bbox4);
      stack.push(node4);

      AABB<T> bbox5(node_bbox.min_.x_ + half_edge.x_,
                    node_bbox.max_.x_,
                    node_bbox.min_.y_,
                    node_bbox.min_.y_ + half_edge.y_,
                    node_bbox.min_.z_ + half_edge.z_,
                    node_bbox.max_.z_, -1);
      new_location = (node->location() << 3) + 5;

      MPUNode<T>* node5 = new MPUNode<T>(new_location, bbox5);
      stack.push(node5);

      AABB<T> bbox6(node_bbox.min_.x_ + half_edge.x_,
                    node_bbox.max_.x_,
                    node_bbox.min_.y_ + half_edge.y_,
                    node_bbox.max_.y_,
                    node_bbox.min_.z_,
                    node_bbox.min_.z_ + half_edge.z_, -1);
      new_location = (node->location() << 3) + 6;

      MPUNode<T>* node6 = new MPUNode<T>(new_location, bbox6);
      stack.push(node6);

      AABB<T> bbox7(node_bbox.min_.x_ + half_edge.x_,
                    node_bbox.max_.x_,
                    node_bbox.min_.y_ + half_edge.y_,
                    node_bbox.max_.y_,
                    node_bbox.min_.z_ + half_edge.z_,
                    node_bbox.max_.z_, -1);
      new_location = (node->location() << 3) + 7;

      MPUNode<T>* node7 = new MPUNode<T>(new_location, bbox7);
      stack.push(node7);

      delete node;
      node = nullptr;
    }
    else {
      // Add the node to the list of leafs
      nodes_.push_back(node);
    }
  }

  // Build the AABB Mpu Tree...
  std::cout << "                   Build AABB Tree..." << std::endl;
  aabb_tree_.Insert(nodes_);
  aabb_tree_.Build();
  std::cout << "                   Done !" << std::endl;
  std::cout << "[MPU<T>::Build] Done!" << std::endl;
}

/*
 * @name  Clear
 * @fn  void Clear(void)
 * @brief Clear the whole tree
 */
template<typename T>
void MPU<T>::Clear(void) {
  // You need to delete manually the pointers
  for (auto v_it = nodes_.begin(); v_it != nodes_.end(); ++v_it) {
    delete (*v_it);
    (*v_it) = nullptr;
  }

  delete root_node_;
  root_node_ = nullptr;
}

#pragma mark -
#pragma mark Usage

/**
 * @name  Evaluate
 * @fn  T Evaluate(const LTS5::Vector3<T>& x) const
 * @brief Evaluate the value of the implicit function at a given position x
 * @param[in] x, a Vector3<T> where to evaluate the function
 * @return the value, of type T
 */
template<typename T>
T MPU<T>::Evaluate(const LTS5::Vector3<T>& x) const {
  T Sw(0.0);
  T SwQ(0.0);

  std::vector<int> idx;
  aabb_tree_.DoIntersect(x, &idx);
  for (int k = 0; k < idx.size(); ++k) {
    nodes_[idx[k]]->Evaluate(x, &SwQ, &Sw);
  }

  if (Sw < std::numeric_limits<T>::epsilon()) {
    std::cerr << "[MPU::Evaluate] ERROR: probe is outside the domain!" << std::endl;
    return T(0.0);
  }
  
  T res = SwQ / Sw;
  return res;
}

/*
 * @name  EvaluateSquare
 * @fn  T EvaluateSquare(const LTS5::Vector3<T>& x) const
 * @brief Evaluate the value of the squared implicit function at a given position x
 * @param[in]  x  a Vector3<T> where to evaluate the function
 * @return the value, of type T
 */
template<typename T>
T MPU<T>::EvaluateSquare(const LTS5::Vector3<T>& x) const {
  T Sw(0.0);
  T SwQ2(0.0);

  std::vector<int> idx;
  aabb_tree_.DoIntersect(x, &idx);
  for (int k = 0; k < idx.size(); ++k) {
    nodes_[idx[k]]->EvaluateSquare(x, &SwQ2, &Sw);
  }

  if (Sw < std::numeric_limits<T>::epsilon()) {
    std::cerr << "[MPU::Evaluate] ERROR: probe is outside the domain!" << std::endl;
    return T(0.0);
  }

  T res = SwQ2 / Sw;
  return res;
}

/**
 * @name  EvaluateGradient
 * @fn  void EvaluateGradient(const LTS5::Vector3<T>& x,
 *                            LTS5::Vector3<T>* grad) const
 * @brief Evaluate the gradient of the implicit function at a given position x
 * @param[in]  x     a Vector3<T> where to evaluate the gradient
 * @param[out] grad  the resulting gradient in x
 */
template<typename T>
void MPU<T>::EvaluateGradient(const LTS5::Vector3<T>& x,
                              LTS5::Vector3<T>* grad) const {
  T Sw(0.0);
  LTS5::Vector3<T> SwGradQ(0.0, 0.0, 0.0);

  std::vector<int> idx;
  aabb_tree_.DoIntersect(x, &idx);
  for (int k = 0; k < idx.size(); ++k) {
    nodes_[idx[k]]->EvaluateGrad(x, &SwGradQ, &Sw);
  }

  if (Sw < std::numeric_limits<T>::epsilon()) {
    std::cerr << "[MPU::Evaluate] ERROR: probe is outside the domain!" << std::endl;
    *grad = LTS5::Vector3<T>(0.0, 0.0, 0.0);
    return;
  }

  *grad = SwGradQ / Sw;
}

/*
 * @name  EvaluateGradientSquare
 * @fn  void EvaluateGradientSquare(const LTS5::Vector3<T>& x,
 *                                  LTS5::Vector3<T>* grad) const
 * @brief Evaluate the gradient of the squared implicit function at a given
 *        position x
 * @param[in]  x     a Vector3<T> where to evaluate the gradient
 * @param[out] grad  the resulting gradient in x
 */
template<typename T>
void MPU<T>::EvaluateGradientSquare(const LTS5::Vector3<T>& x,
                                    LTS5::Vector3<T>* grad) const {
  T Sw(0.0);
  LTS5::Vector3<T> SwGradQ2(0.0, 0.0, 0.0);

  std::vector<int> idx;
  aabb_tree_.DoIntersect(x, &idx);
  for (int k = 0; k < idx.size(); ++k) {
    nodes_[idx[k]]->EvaluateGradSquare(x, &SwGradQ2, &Sw);
  }

  if (Sw < std::numeric_limits<T>::epsilon()) {
    std::cerr << "[MPU::Evaluate] ERROR: probe is outside the domain!" << std::endl;
    *grad = LTS5::Vector3<T>(0.0, 0.0, 0.0);
    return;
  }

  *grad = SwGradQ2 / Sw;
}

#pragma mark -
#pragma mark Declaration

/** Float type MPU */
template class MPU<float>;
/** Float type MPU */
template class MPU<double>;

} // namespace LTS5
