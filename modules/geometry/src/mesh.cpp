/**
 *  @file   mesh.cpp
 *  @brief  3D Mesh helper interface
 *
 *  @author Christophe Ecabert
 *  @date   15/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <cstring>
#include <string>
#include <utility>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <numeric>
#include <limits>

#include "ply.h"
#include "opencv2/core.hpp"

#include "lts5/utils/logger.hpp"
#include "lts5/utils/hash.hpp"
#include "lts5/utils/math/matrix.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/octree.hpp"

// After every `lts5` header due to forward declaration of Eigen type in
// mesh.hpp
#include "Eigen/SparseCore"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @struct PLYVertex
 *  @brief  Data object to read ply file
 */
template<typename T, typename ColorContainer>
struct PLYVertex {
  /** Vertex typedef */
  using Vertex = typename Mesh<T, ColorContainer>::Vertex;
  /** Normal typedef */
  using Normal = typename Mesh<T, ColorContainer>::Normal;
  /** Color typedef */
  using Color = typename Mesh<T, ColorContainer>::Color;
  /** Tex coordinate */
  using TCoord = typename Mesh<T, ColorContainer>::TCoord;

  /** Position */
  Vertex vertex;
  /** Normal */
  Normal normal;
  /** Color */
  Color color;
  /** Tex Coord */
  TCoord tcoord;

  /**
   *  @name PLYVertex
   *  @fn PLYVertex()
   *  @brief  Constructor
   */
  PLYVertex() : vertex(-1.0, -1.0, -1.0),
                normal(-1.0, -1.0, -1.0),
                color(-1.0, -1.0, -1.0, -1.0),
                tcoord(-1.0, -1.0) {}
};

template<>
PLYVertex<float, Vector3<float>>::
PLYVertex() : vertex(-1.f, -1.f, -1.f),
              normal(-1.f, -1.f, -1.f),
              color(-1.f, -1.f, -1.f) {}
template<>
PLYVertex<double, Vector3<double>>::
PLYVertex() : vertex(-1.0, -1.0, -1.0),
              normal(-1.0, -1.0, -1.0),
              color(-1.0, -1.0, -1.0) {}
template<>
PLYVertex<float, Vector4<float>>::
PLYVertex() : vertex(-1.f, -1.f, -1.f),
              normal(-1.f, -1.f, -1.f),
              color(-1.f, -1.f, -1.f, -1.f) {}
template<>
PLYVertex<double, Vector4<double>>::
PLYVertex() : vertex(-1.0, -1.0, -1.0),
              normal(-1.0, -1.0, -1.0),
              color(-1.0, -1.0, -1.0, -1.0) {}

/**
 *  @struct PLYFace
 *  @brief  Data object to read ply file
 */
template<typename T, typename ColorContainer>
struct PLYFace {
  /** Triangle typedef */
  using Triangle = typename Mesh<T, ColorContainer>::Triangle;
  /** Texture coordinate typedef */
  using TCoord = typename Mesh<T, ColorContainer>::TCoord;

  /** number of vertex in the face */
  int n_vertex;
  /** List of vertex indices */
  Triangle* list_idx;
  /** number of tcoord */
  int n_tcoord;
  /** List of texture coordinate */
  TCoord* list_tcoord;

  /**
   *  @name PLYFace()
   *  @fn PLYFace()
   *  @brief  Constructor
   */
  PLYFace() : n_vertex(0),
                  list_idx(nullptr),
                  n_tcoord(0),
                  list_tcoord(nullptr) {
  }

  /**
   *  @name ~PLYFace()
   *  @fn ~PLYFace()
   *  @brief  Destructor
   */
  ~PLYFace() = default;
};

#pragma mark -
#pragma mark Material

/*
 * @name Material
 * @fn   Material()
 * @brief Constructor
 */
template<typename T>
Material<T>::Material() : name_("default"),
                          ka_(1.0, 1.0, 1.0),  // white
                          kd_(1.0, 1.0, 1.0),
                          ks_(0.0, 0.0, 0.0),  // black, disable
                          ns_(0.0),
                          transmittance_(0.0, 0.0, 0.0),
                          emission_(0.0, 0.0, 0.0),
                          index_of_refraction_(1.0),
                          dissolve_(1.0),
                          illumination_(kColorOnAndAmbientOff),
                          ambient_tex_(""),   // No texture
                          diffuse_tex_(""),
                          specular_tex_(""),
                          specular_exponent_tex_(""),
                          bump_tex_(""),
                          disp_tex_(""),
                          alpha_tex_(""),
                          reflection_tex_("") {
}

/*
 * @name  Load
 * @fn    int Load(std::istream& stream)
 * @brief Parse material from a given stream
 * @param[in] stream Stream to parse
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int Material<T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    err = this->LoadInternal(stream);
  }
  return err;
}

/*
 * @name  SetAbsolutePath
 * @fn    void SetAbsolutePath(const std::string& folder)
 * @brief Prepend a given folder path to each texture filename to convert them
 *        into absolute path.
 * @param[in] folder Folder where material file is stored
 */
template<typename T>
void Material<T>::SetAbsolutePath(const std::string& folder) {
  if (!ambient_tex_.empty()) {
    ambient_tex_ = Path::Join(folder, ambient_tex_);
  }
  if (!diffuse_tex_.empty()) {
    diffuse_tex_ = Path::Join(folder, diffuse_tex_);
  }
  if (!specular_tex_.empty()) {
    specular_tex_ = Path::Join(folder, specular_tex_);
  }
  if (!specular_exponent_tex_.empty()) {
    specular_exponent_tex_ = Path::Join(folder, specular_exponent_tex_);
  }
  if (!bump_tex_.empty()) {
    bump_tex_ = Path::Join(folder, bump_tex_);
  }
  if (!disp_tex_.empty()) {
    disp_tex_ = Path::Join(folder, disp_tex_);
  }
  if (!alpha_tex_.empty()) {
    alpha_tex_ = Path::Join(folder, alpha_tex_);
  }
  if (!reflection_tex_.empty()) {
    reflection_tex_ = Path::Join(folder, reflection_tex_);
  }
}

/*
 * @name
 * @fn
 * @brief Parse a single material from a given stream
 * @param[in] stream Stream containing a valid material definition
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int Material<T>::LoadInternal(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Scan each lines
    bool first_material = true;
    std::string line, token;
    std::stringstream str_stream;
    while (!stream.eof()) {
      // Parse line, skip if empty
      auto line_start_pos = stream.tellg();
      std::getline(stream, line);
      if (line.back() == '\r') {
        line.pop_back();
      }
      if (!line.empty()) {
        str_stream.clear();
        str_stream.str(line);
        str_stream >> token;
        // normalize token to lower letter to avoid surprise
        std::transform(token.begin(),
                       token.end(),
                       token.begin(),
                       [](unsigned char c) {return std::tolower(c);});
        if (token == "newmtl") {
          if (!first_material) {
            // next material, rewind stream + exit
            stream.seekg(line_start_pos, std::ios_base::beg);
            break;
          } else {
            // Get material's name
            first_material = false;
            str_stream >> this->name_;
            continue;   // Token is consumed, skip to next line
          }
        }
        // Parse common token and init material if at the beginning of material
        if (!first_material) {
          if (token == "ka") {                          // Ambient color
            str_stream >> this->ka_;
          } else if (token == "kd") {                   // Diffuse color
            str_stream >> this->kd_;
          } else if (token == "ks") {                   // Specular color
            str_stream >> this->ks_;
          } else if (token == "ns") {                   // Specular exponent
            str_stream >> this->ns_;
          } else if (token == "kt" || token == "tf") {  // Transmittance
            str_stream >> this->transmittance_;
          } else if (token == "ke") {                   // Emission
            str_stream >> this->emission_;
          } else if (token == "ni") {                   // Index of refraction
            str_stream >> this->index_of_refraction_;
          } else if (token == "d") {                    // Dissolve
            str_stream >> this->dissolve_;
          } else if (token == "tr") {
            T value;
            str_stream >> value;
            this->dissolve_ = T(1.0) - value;
          } else if (token == "illum") {                // Illumination
            int value;
            str_stream >> value;
            this->illumination_ = static_cast<IlluminationType>(value);
          } else if (token == "map_ka") {               // Ambient tex
            str_stream >> this->ambient_tex_;
          } else if (token == "map_kd") {               // Diffuse tex
            str_stream >> this->diffuse_tex_;
          } else if (token == "map_ks") {               // Specular tex
            str_stream >> this->specular_tex_;
          } else if (token == "map_ns") {               // Specular exponent tex
            str_stream >> this->specular_exponent_tex_;
          } else if (token == "bump" || token == "bump_map") {
            str_stream >> this->bump_tex_;
          } else if (token == "disp") {                 // Displacement map
            str_stream >> this->disp_tex_;
          } else if (token == "map_d") {                // Alpha tex
            str_stream >> this->alpha_tex_;
          } else if (token == "refl") {                 // Reflection tex
            str_stream >> this->reflection_tex_;
          }
        }
      }
    }
    err = stream.good() ? 0 : stream.eof() && !first_material ? 0 : -1;
  }
  return err;
}

/*
 * @name  CreateFromFile
 * @fn    static std::vector<Material> CreateFromFile(const std::string& path)
 * @brief Parse all material from a given file
 * @param[in] path Path to the material file (i.e. *.mtl file)
 * @return    List of material or empty if something went wrong
 */
template<typename T>
std::vector<Material<T>> Material<T>::CreateFromFile(const std::string& path) {
  // Open files
  std::vector<Material> materials;
  std::string folder = Path::Dirname(path);
  std::ifstream stream(path);
  // Parse all materials in the file
  do {
    Material m;
    int err = m.Load(stream);
    if (!err) {
      m.SetAbsolutePath(folder);
      materials.push_back(m);
    } else {
      materials.clear();
    }
  } while (stream.good());
  // Done
  return materials;
}

/*
 * @name  Edge
 * @fn    Edge(const int64_t& v0, const int64_t& v1, const int64_t& f0,
 *             const int64_t& f1)
 * @brief Constructor
 * @param[in] v0  First vertex
 * @param[in] v1  Second vertex
 * @param[in] f0  First face
 * @param[in] f1  Second face
 */
Edge::Edge(const int64_t& v0,
           const int64_t& v1,
           const int64_t& f0,
           const int64_t& f1) : v0_(std::min(v0, v1)),
                                v1_(std::max(v0, v1)),
                                f0_(f0),
                                f1_(f1) {
}

/*
 * @name  operator==
 * @fn    bool operator==(const Edge& other) const
 * @brief Equal operator
 * @param[in] other   Object to test against
 * @return    true if equal, false otherwise
 */
bool Edge::operator==(const Edge& other) const {
  return v0_ == other.v0_ && v1_ == other.v1_;
}

/*
 * @name    ExtractEdgeInformation
 * @fn  void ExtractEdgeInformation(const std::vector<Vector3<int>>& triangles,
                            const size_t& n_vertex,
                            std::vector<Edge>* edges,
                            std::vector<std::vector<size_t>>* neighbours)
 * @brief   Extract list of edges and one ring neighbour from a given
 *  triangulation
 * @param[in] triangles List of triangles
 * @param[in] n_vertex  Total number of vertex in the triangulation
 * @param[out] edges    List of all edges in the triangulation
 * @param[out] neighbours   List of all edges connected to each vertices
 */
void ExtractEdgeInformation(const std::vector<Vector3<int>>& triangles,
                            const size_t& n_vertex,
                            std::vector<Edge>* edges,
                            std::vector<std::vector<size_t>>* neighbours) {
  // Initialize outputs
  edges->clear();
  neighbours->resize(n_vertex, std::vector<size_t>());
  // Iterate over all triangles
  for (size_t k = 0; k < triangles.size(); ++k) {
    const int* tri_ptr = &(triangles[k].x_);
    for (size_t i = 0; i < 3; ++i) {
      int v0 = tri_ptr[i];
      int v1 = tri_ptr[(i + 1) % 3];
      Edge edge(v0, v1, k, -1);
      // Does edge already exists ?
      auto it = std::find(edges->begin(), edges->end(), edge);
      if (it == edges->end()) {
        // No, new entry
        auto sz = edges->size();
        edges->push_back(edge);
        neighbours->at(v0).push_back(sz);
        neighbours->at(v1).push_back(sz);
      } else {
        // Update edge with second face index
        it->set_second_face(k);
      }
    }
  }
}




#pragma mark -
#pragma mark Initialization

/*
 *  @name
 *  @fn
 *  @brief  Constructor
 */
template<typename T, typename ColorContainer>
Mesh<T, ColorContainer>::Mesh() :
        comments_(1, "Generated from LTS5 C++ library lts5-face"),
        bbox_is_computed_(false),
        halfedges_is_computed_(false),
        binary_ply_(false) {
}

/*
 *  @name Mesh
 *  @fn Mesh(const std::string& filename)
 *  @brief  Load mesh from supported file :
 *            .obj, .ply, .tri
 */
template<typename T, typename ColorContainer>
Mesh<T, ColorContainer>::Mesh(const std::string& filename) : Mesh::Mesh() {
  if (this->Load(filename)) {
    LTS5_LOG_ERROR("Error while loading mesh from file : " + filename);
  }
}

/*
 *  @name Mesh
 *  @fn Mesh(const Mesh<T, ColorContainer>& other) = delete
 *  @brief  Copy constructor
 *  @param[in]  other Mesh to copy from
 */
template<typename T, typename ColorContainer>
Mesh<T, ColorContainer>::Mesh(const Mesh<T, ColorContainer>& other) :
        vertex_(other.vertex_),
        normal_(other.normal_),
        tex_coord_(other.tex_coord_),
        vertex_color_(other.vertex_color_),
        tri_(other.tri_),
        vertex_con_(other.vertex_con_),
        comments_(other.comments_),
        bbox_(other.bbox_),
        bbox_is_computed_(other.bbox_is_computed_),
        vert_halfedges_(other.vert_halfedges_),
        halfedges_(other.halfedges_),
        halfedges_is_computed_(other.halfedges_is_computed_),
        principal_curvature_(other.principal_curvature_),
        materials_(other.materials_),
        binary_ply_(other.binary_ply_) {
}

/*
 *  @name Mesh  @fn Meshid)
 *  @brief  Destructor
 */
template<typename T, typename ColorContainer>
Mesh<T, ColorContainer>::~Mesh() {}

/*
 *  @name Load
 *  @fn int Load(const std::string& filename)
 *  @brief  Load mesh from supported file :
 *            .obj, .ply, .tri
 *  @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int Mesh<T, ColorContainer>::Load(const std::string& filename) {
  // Load data
  int err = -1;
  std::size_t pos = filename.rfind('.');
  if (pos != std::string::npos) {
    std::string ext = filename.substr(pos + 1, filename.length());
    FileExt file_ext = this->HashExt(ext);
    switch (file_ext) {
      // OBJ
      case kObj: {
        // Ensure empty containter
        vertex_.clear();
        normal_.clear();
        tex_coord_.clear();
        tri_.clear();
        vertex_con_.clear();
        vertex_color_.clear();
        err = this->LoadOBJ(filename);
      }
        break;
      // PLY
      case kPly: {
        // Ensure empty containter
        vertex_.clear();
        normal_.clear();
        tex_coord_.clear();
        tri_.clear();
        vertex_con_.clear();
        vertex_color_.clear();
        err = this->LoadPLY(filename);
      }
        break;
      // Triangulation only
      case kTri: {
        // Ensure empty containter
        tri_.clear();
        vertex_con_.clear();
        err = this->LoadTri(filename);
      }
        break;
      // Undef
      case kUndef:
        break;
    }
    if (!err && (file_ext != FileExt::kTri)) {
      this->BuildConnectivity();
    }
  }
  if (!bbox_is_computed_) {
    ComputeBoundingBox();
  }
  return err;
}

/*
 *  @name Save
 *  @fn int Save(const std::string& filename)
 *  @brief  Save mesh to supported file format:
 *            .ply
 *  @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int Mesh<T, ColorContainer>::Save(const std::string& filename) const {
  // Load data
  int err = -1;
  std::size_t pos = filename.rfind('.');
  if (pos != std::string::npos) {
    std::string ext = filename.substr(pos + 1, filename.length());
    FileExt file_ext = this->HashExt(ext);
    switch (file_ext) {
        // PLY
      case kPly: {
        err = this->SavePLY(filename);
      }
        break;
        // OBJ
      case kObj: {
        err = this->SaveOBJ(filename);
      }
        break;
        // Undef
      default:
        LTS5_LOG_ERROR("Unsuported file format!");
        break;
    }
  }
  return err;
}

/*
 *  @name CheckVertexDuplicates
 *  @fn bool CheckVertexDuplicates(double epsilon)
 *  @brief Check if some vertices of the mesh have duplicates
 *  @param[in]  epsilon  tolerance for duplicates
 *  @return true if duplicates are found, false otherwise
 */
template<typename T, typename ColorContainer>
bool Mesh<T, ColorContainer>::CheckVertexDuplicates(double epsilon) {
  bool ret = false;
  // Create tree
  LTS5::OCTree<T> tree;
  tree.Insert(*this, LTS5::Mesh<T, ColorContainer>::PrimitiveType::kPoint);
  tree.Build();

  std::vector<int> idx;
  for (int i = 0; i < vertex_.size(); ++i) {
    LTS5::Sphere<T> sph(vertex_[i], epsilon);
    tree.RadiusNearestNeighbor(sph, &idx);

    if (idx.size() > 1) {
      return true;
    }
    idx.clear();
  }
  return ret;
}

/*
 *  @name FixTriangleSoup
 *  @fn void FixTriangleSoup(double epsilon)
 *  @brief Merge identical vertices and redefine the rectangles
 *  @param[in]  epsilon  tolerance for duplicates
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::FixTriangleSoup(double epsilon) {
  // Create tree
  LTS5::OCTree<T> tree;
  tree.Insert(*this, LTS5::Mesh<T, ColorContainer>::PrimitiveType::kPoint);
  tree.Build();
  std::vector<int> idx;
  // array of duplicate indicator
  std::vector<int> dest(vertex_.size(), -1);
  // What I need to fuse:
  std::vector<Vertex> vertex_new;
  std::vector<Normal> normal_new;
  std::vector<TCoord> tcoord_new;
  int new_id = 0;  // new index
  for (int old_id = 0; old_id < vertex_.size(); ++old_id) {
    if (dest[old_id] >= 0) {
      continue;
    }
    vertex_new.push_back(vertex_[old_id]);
    if (!normal_.empty()) {
      normal_new.push_back(normal_[old_id]);
    }
    if (!tex_coord_.empty()) {
      tcoord_new.push_back(tex_coord_[old_id]);
    }
    LTS5::Sphere<T> sph(vertex_[old_id], epsilon);
    tree.RadiusNearestNeighbor(sph, &idx);
    // For each Vertex, Normal and TCoord, store the corresponding index in the
    // destination array, then replace
    //for (auto v_it = idx.begin(); v_it != idx.end(); ++v_it) {
    for (const auto& v : idx) {
      dest[v] = new_id;
    }
    idx.clear();
    ++new_id;
  }
  this->vertex_.swap(vertex_new);
  this->normal_.swap(normal_new);
  this->tex_coord_.swap(tcoord_new);
  // Replace the vertex indices in the triangles
  for (auto tri_it = this->tri_.begin(); tri_it != this->tri_.end();) {
    tri_it->x_ = dest[tri_it->x_];
    tri_it->y_ = dest[tri_it->y_];
    tri_it->z_ = dest[tri_it->z_];
    if ((tri_it->x_ == tri_it->y_) || (tri_it->x_ == tri_it->z_) ||
        (tri_it->y_ == tri_it->z_)) {

      std::cerr << FUNC_NAME << std::endl;
      std::cerr << "WARNING: Triangle collapse! The triangle is removed";
      std::cerr << " from the mesh." << std::endl;
      tri_it = tri_.erase(tri_it);
    } else {
      ++tri_it;
    }
  }
  // Need to rebuild connectivity
  BuildConnectivity();
}

/*
 *  @name BuildConnectivity
 *  @fn void BuildConnectivity()
 *  @brief  Construct vertex connectivity (vertex connection) used later
 *          in normal computation
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::BuildConnectivity() {
  // Init outter containter
  assert(vertex_.size() != 0 && tri_.size() != 0);
  vertex_con_.clear();
  vertex_con_ = std::vector<std::vector<int>>(vertex_.size(),
                                              std::vector<int>(0));
  // Loop over all triangle
  const int n_tri = static_cast<int>(tri_.size());
  for (int i = 0; i < n_tri; ++i) {
    int* tri_idx_ptr = &(tri_[i].x_);
    for (int e = 0; e < 3; ++e) {
      int idx_in = tri_idx_ptr[e];
      int idx_out_1 = tri_idx_ptr[(e + 1) % 3];
      int idx_out_2 = tri_idx_ptr[(e + 2) % 3];
      // Add to connectivity list
      vertex_con_[idx_in].push_back(idx_out_1);
      vertex_con_[idx_in].push_back(idx_out_2);
    }
  }
}

#pragma mark -
#pragma mark Process

/*
 * @name  ComputeHalfedges
 * @fn  int ComputeHalfedges()
 * @brief Compute the halfedges representation from vertices and triangles
 * @return 0 if success, -1 otherwise
 */
template<typename T, typename ColorContainer>
int Mesh<T, ColorContainer>::ComputeHalfedges() {
  int error = 0;
  //Initialize
  this->vert_halfedges_.resize(this->vertex_.size(),
                               std::numeric_limits<std::size_t>::max());
  // Edgemap
  std::unordered_map<std::pair<int, int>, int> edgemap;
  for (auto tri_it = this->tri_.begin(); tri_it != this->tri_.end(); ++tri_it) {
    const int* tri_idx_ptr = &(tri_it->x_);
    bool revert = false; // Whether to change indices order or not
    // in order to ensure normal consistency on manifold.

    // First check the consistency: check that each edge is present only once
    for (int i = 0; i < 3; ++i) {
      int origin_id = tri_idx_ptr[i];
      int end_id = revert ? tri_idx_ptr[(i+2)%3] : tri_idx_ptr[(i+1)%3];

      std::pair<int, int> p(origin_id, end_id);
      // Check if the edge already exists (cannot exists twice)
      auto ret = edgemap.find(p);
      if (ret == edgemap.end()) {
        continue;
      } else if (!revert) {
        revert = true;
        i = -1;
      } else {
        // Might be due to the order in which the faces are processed:
        // If they are not processed in neighbooring order,you can have problems
        // Ex: face1 and face2 share an edge, face2 and face3 share an edge but
        //     face1 and face3 don't.
        //     face1 is correctly defined, face2 and face3 are NOT.
        //     face1 is processed first -> no problem
        //     face3 is processed second-> no problem, it does not share edge
        //     with face1, so there is no way of knowing that normals are not
        //     consistent
        //     Now face2 is processed last->problem!
        //     Inconsistent with BOTH face1 and face3!
        // @TODO: (Gabriel) Think about a solution
        std::cerr << FUNC_NAME << std::endl;
        std::cerr << "ERROR : Non manifold topology (non unique halfedge)!";
        std::cerr << std::endl;
        return -1;
      }
    }

    // If you get here,the face is consistent,either in normal or reverse order!
    for (int i = 0; i< 3; ++i) {
      int origin_id = tri_idx_ptr[i];
      int end_id = revert ? tri_idx_ptr[(i+2)%3] : tri_idx_ptr[(i+1)%3];
      std::pair<int, int> p(origin_id, end_id);
      auto ret = edgemap.emplace(p, halfedges_.size());
      if (!ret.second) {
        std::cout << FUNC_NAME << std::endl;
        std::cerr << "ERROR : Halfedge already exists!" << std::endl;
        return -1;
      }
      halfedges_.emplace_back(origin_id);
      if (vert_halfedges_[origin_id] ==
          std::numeric_limits<std::size_t>::max()) {
        vert_halfedges_[origin_id] = halfedges_.size()-1;
      }
    }
  }
  // Add info about opposite halfedge
  for (auto edge_it = edgemap.begin(); edge_it != edgemap.end(); ++edge_it) {
    std::pair<int, int> reverse_key = std::make_pair(edge_it->first.second,
                                                     edge_it->first.first);
    auto opposite_it = edgemap.find(reverse_key);
    if (opposite_it != edgemap.end()) {
      halfedges_[edge_it->second].opposite = opposite_it->second;
    }
  }

  halfedges_is_computed_ = true;
  return error;
}

/*
 * @name OneRingNeighbors
 * @fn void OneRingNeighbors(const std::size_t& vertex_id,
 *                           std::vector<std::size_t>* neighbors_id) const
 * @brief Query the one ring neighborhood of a vertex
 * @param[in] vertex_id id of the center vertex
 * @param[out] neighbors_id indices of one ring neighbors
 * @return Wether or not the one ring is complete
 */
template<typename T, typename ColorContainer>
bool Mesh<T, ColorContainer>::OneRingNeighbors(const std::size_t& vertex_id,
                               std::vector<std::size_t>* neighbors_id) const {
  neighbors_id->clear();
  bool complete = false;
  if (!halfedges_is_computed_) {
    std::cerr << FUNC_NAME << std::endl;
    std::cerr << "ERROR : Halfedges are not computed!" << std::endl;
    // Cannot compute them here because the function is marked const...
    return complete;
  }
  if (vertex_id >= vert_halfedges_.size()) {
    std::cerr << FUNC_NAME << std::endl;
    std::cerr << "ERROR : vertex index out of bounds!" << std::endl;
    return complete;
  }
  std::size_t halfedge_id = vert_halfedges_[vertex_id];
  std::size_t initial_halfedge_id = vert_halfedges_[vertex_id];
  if (halfedge_id != std::numeric_limits<size_t>::max()) {
    do {
      complete = true;
      std::size_t origin_id = halfedges_[Halfedge::Next(halfedge_id)].origin;
      neighbors_id->push_back(origin_id);

      // Check that the previous halfedge is not a boundary halfedge
      const Halfedge& prev = halfedges_[Halfedge::Previous(halfedge_id)];
      if (prev.IsBoundary()) {
        // If you encounter a boundary, then the one ring is not a complete ring
        complete = false;
        origin_id = prev.origin;
        neighbors_id->push_back(origin_id);
        // Start in the oppossite direction!
        break;
      }
      // take the opposite of the previous halfedge
      halfedge_id = prev.opposite;
    } while (halfedge_id != initial_halfedge_id);

    if (!complete && !halfedges_[initial_halfedge_id].IsBoundary()) {
      // Go for a round in the opposite direction!
      std::reverse(neighbors_id->begin(), neighbors_id->end());
      halfedge_id = initial_halfedge_id;
      do {
        halfedge_id = Halfedge::Next(halfedges_[halfedge_id].opposite);
        std::size_t origin_id = halfedges_[Halfedge::Next(halfedge_id)].origin;
        neighbors_id->push_back(origin_id);
      } while (!halfedges_[halfedge_id].IsBoundary());

      std::reverse(neighbors_id->begin(), neighbors_id->end());
    }
  }
  return complete;
}

/*
 *  @name ComputeVertexNormal
 *  @fn void ComputeVertexNormal()
 *  @brief  Compute normal for each vertex in the object
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::ComputeVertexNormal() {
  // Loop over all vertex
  if (vertex_con_.empty()) {
    this->BuildConnectivity();
  }
  assert(vertex_con_.size() > 0);
  normal_.resize(vertex_.size(), Mesh::Normal());
  Parallel::For(vertex_.size(),
                [&](const size_t& v) {
                  //TODO: Change type
                  // Loop over all connect vertex
                  const auto& conn = vertex_con_[v];
                  const Vertex& A = vertex_[v];
                  const int n_conn = static_cast<int>(conn.size());
                  Normal weighted_n;
                  for (int j = 0; j < n_conn; j += 2) {
                    const Vertex& B = vertex_[conn[j]];
                    const Vertex& C = vertex_[conn[j+1]];
                    // Define edges AB, AC
                    Edge AB = B - A;
                    Edge AC = C - A;
                    // Compute surface's normal (triangle ABC)
                    Normal n = AB ^ AC;
                    n.Normalize();
                    // Stack each face contribution and weight with angle
                    AB.Normalize();
                    AC.Normalize();
                    T ddot = AB * AC;
                    ddot = ddot > T(1.0) ? T(1.0) : ddot;
                    ddot = ddot < T(-1.0) ? T(-1.0) : ddot;
                    const T angle = std::acos(ddot);
                    weighted_n += (n * angle);
                  }
                  // normalize and set
                  weighted_n.Normalize();
                  normal_[v] = weighted_n;
                });
}

/*
 *  @name ComputeBoundingBox
 *  @fn void ComputeBoundingBox()
 *  @brief  Compute the bounding box of the mesh
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::ComputeBoundingBox() {
  // Init bbox
  bbox_.min_.x_ = std::numeric_limits<T>::max();
  bbox_.max_.x_ = std::numeric_limits<T>::lowest();
  bbox_.min_.y_ = std::numeric_limits<T>::max();
  bbox_.max_.y_ = std::numeric_limits<T>::lowest();
  bbox_.min_.z_ = std::numeric_limits<T>::max();
  bbox_.max_.z_ = std::numeric_limits<T>::lowest();

  for (auto v_it = this->vertex_.begin(); v_it != this->vertex_.end(); ++v_it) {
    // Compute boundary box
    bbox_.min_.x_ = bbox_.min_.x_ < v_it->x_ ? bbox_.min_.x_ : v_it->x_;
    bbox_.max_.x_ = bbox_.max_.x_ > v_it->x_ ? bbox_.max_.x_ : v_it->x_;
    bbox_.min_.y_ = bbox_.min_.y_ < v_it->y_ ? bbox_.min_.y_ : v_it->y_;
    bbox_.max_.y_ = bbox_.max_.y_ > v_it->y_ ? bbox_.max_.y_ : v_it->y_;
    bbox_.min_.z_ = bbox_.min_.z_ < v_it->z_ ? bbox_.min_.z_ : v_it->z_;
    bbox_.max_.z_ = bbox_.max_.z_ > v_it->z_ ? bbox_.max_.z_ : v_it->z_;
  }
  bbox_.center_ = (bbox_.min_ + bbox_.max_) * T(0.5);
  bbox_is_computed_ = true;
}

/*
 * @name ComputeUniformLaplacian
 * @fn void ComputeUniformLaplacian(Eigen::SparseMatrix<T>* L) const
 * @brief Compute uniform graph Laplacian of a mesh
 * @param[out] D  a diagonal eigen sparse matrix containing the w_i
 * @param[out] M  an eigen sparse matrix containing the non diagonal w_ij
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::ComputeUniformLaplacian(Eigen::SparseMatrix<T, 0, int>* D,
                                      Eigen::SparseMatrix<T, 0, int>* M) const {
  std::vector<Eigen::Triplet<T> > D_vec(this->vertex_.size());
  std::vector<Eigen::Triplet<T> > M_vec;

  // @TODO: (Gabriel) You can parallelize this!
  for (int row = 0; row < this->vertex_.size(); ++row) {
    std::vector<size_t> one_ring;
    OneRingNeighbors(row, &one_ring);

    for (int j = 0; j < one_ring.size(); ++j) {
      M_vec.push_back(Eigen::Triplet<T>(static_cast<T>(row),
                                        static_cast<T>(one_ring[j]),
                                        T(1.0)));
    }
    // Fill in matrix D
    T w_i(0.0);
    int one_ring_size = static_cast<int>(one_ring.size());
    if (one_ring_size != 0) {
      w_i = 1.0 / one_ring.size();
      D_vec[row] = Eigen::Triplet<T>(row, row, w_i);

      // Diag of M (Only valid for Graph Laplacian)
      M_vec.push_back(Eigen::Triplet<T>(row, row, -one_ring_size));
    }
  }
  // Init the matrices from the vectors
  D->setFromTriplets(D_vec.begin(), D_vec.end());
  M->setFromTriplets(M_vec.begin(), M_vec.end());
}

/*
 * @name  ComputeCotanLaplacian
 * @fn  void ComputeCotanLaplacian(Eigen::SparseMatrix<T>* D,
 *                                 Eigen::SparseMatrix<T>* M) const
 * @brief Compute cotan weights Laplacian of the mesh
 * @param[out] D  a diagonal eigen sparse matrix containing the w_i
 * @param[out] M  an eigen sparse matrix containing the non diagonal w_ij
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::ComputeCotanLaplacian(Eigen::SparseMatrix<T>* D,
                           Eigen::SparseMatrix<T>* M) const {
  std::vector<Eigen::Triplet<T> > D_vec(this->vertex_.size());
  std::vector<Eigen::Triplet<T> > M_vec;

  for (int row = 0; row < this->vertex_.size(); ++row) {
    std::vector<size_t> neighbors;
    bool complete = OneRingNeighbors(row, &neighbors);
    size_t one_ring_size = neighbors.size();
    T sum_w_ij = 0.0;
    T sum_area(0.0);

    // First vertex of the  current edge
    const Vertex& I = this->vertex_[row];
    for (size_t n = 0; n < one_ring_size; ++n) {
      // Second vertex of the current edge
      const Vertex& J = this->vertex_[neighbors[n]];
      T w_ij(0.0);
      int n_w = 0;
      // if IJ is a boundary edge (only possible when complete == false)
      // in that case, either IA does not exist (when n == 0)
      //     OR IB does not exist (when n == one_ring_size-1)
      if (complete || n != 0) {
        const Vertex& A = (n == 0) ? this->vertex_[neighbors[one_ring_size-1]] :
                          this->vertex_[neighbors[n-1]];
        T weight(0.0);
        T area(0.0);
        ComputeCotanWeight(I, J, A, &weight, &area);
        w_ij += weight;
        ++n_w;
      }
      if (complete || n != one_ring_size-1) {
        const Vertex& B = (n == one_ring_size-1) ? this->vertex_[neighbors[0]] :
                          this->vertex_[neighbors[(n+1)]];
        T weight(0.0);
        T area(0.0);
        ComputeCotanWeight(I, J, B, &weight, &area);
        sum_area += area; // Compute area only ONCE!
        w_ij += weight;
        ++n_w;
      }
      w_ij = (n_w == 0) ? 0.0 : w_ij/n_w;
      M_vec.push_back(Eigen::Triplet<T>(static_cast<T>(row),
                                        static_cast<T>(neighbors[n]),
                                        w_ij));
      sum_w_ij += w_ij;
    }

    M_vec.push_back(Eigen::Triplet<T>(row, row, -sum_w_ij));
    D_vec[row] = Eigen::Triplet<T>(row, row, sum_area);
  }
  // Init the matrices from the vectors
  D->setFromTriplets(D_vec.begin(), D_vec.end());
  M->setFromTriplets(M_vec.begin(), M_vec.end());
}

/*
 *  @name Transform
 *  @fn void Transform(const cv::Mat& transform)
 *  @brief Apply transform to the mesh
 *  @param[in] transform  4x4 mat (in homogeneous coordinates)
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::Transform(const cv::Mat& transform) {
  // Apply transform
  assert(transform.rows == 4);
  assert(transform.cols == 4);
  assert(transform.type() == cv::DataType<T>::type);
  for (int v = 0; v < vertex_.size(); ++v) {
    Vector3<T>& vert = vertex_[v];
    Vector3<T> res;
    res.x_ = (transform.at<T>(0, 0) * vert.x_ +
              transform.at<T>(0, 1) * vert.y_ +
              transform.at<T>(0, 2) * vert.z_ +
              transform.at<T>(0, 3));
    res.y_ = (transform.at<T>(1, 0) * vert.x_ +
              transform.at<T>(1, 1) * vert.y_ +
              transform.at<T>(1, 2) * vert.z_ +
              transform.at<T>(1, 3));
    res.z_ = (transform.at<T>(2, 0) * vert.x_ +
              transform.at<T>(2, 1) * vert.y_ +
              transform.at<T>(2, 2) * vert.z_ +
              transform.at<T>(2, 3));
    T w = (transform.at<T>(3, 0) * vert.x_ +
           transform.at<T>(3, 1) * vert.y_ +
           transform.at<T>(3, 2) * vert.z_ +
           transform.at<T>(3, 3));
    res.x_ /= w;
    res.y_ /= w;
    res.z_ /= w;
    vert = res;
  }
  ComputeBoundingBox(); // AABB does not rotate with the mesh! So center
  // might shift!
}

/*
 *  @name Scale
 *  @fn void Scale(const T& factor);
 *  @brief  Scale the whole mesh (with respect to its COG)
 *  @param[in] factor, by which to scale the mesh
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::Scale(const T& factor) {
  // In case the bounding box is not already computed, compute it!
  if (!bbox_is_computed_) {
    ComputeBoundingBox();
  }

  Vertex mesh_center = Cog();

  for (auto v_it = this->vertex_.begin(); v_it != this->vertex_.end(); ++v_it) {
    (*v_it) = (((*v_it) - mesh_center) * factor) + mesh_center;
    //    v_it->x_ = ((v_it->x_ - mesh_center.x_) * factor) + mesh_center.x_;
    //    v_it->y_ = ((v_it->y_ - mesh_center.y_) * factor) + mesh_center.y_;
    //    v_it->z_ = ((v_it->z_ - mesh_center.z_) * factor) + mesh_center.z_;

    // Recompute boundary box at the same time!
    bbox_.min_.x_ = bbox_.min_.x_ < v_it->x_ ? bbox_.min_.x_ : v_it->x_;
    bbox_.max_.x_ = bbox_.max_.x_ > v_it->x_ ? bbox_.max_.x_ : v_it->x_;
    bbox_.min_.y_ = bbox_.min_.y_ < v_it->y_ ? bbox_.min_.y_ : v_it->y_;
    bbox_.max_.y_ = bbox_.max_.y_ > v_it->y_ ? bbox_.max_.y_ : v_it->y_;
    bbox_.min_.z_ = bbox_.min_.z_ < v_it->z_ ? bbox_.min_.z_ : v_it->z_;
    bbox_.max_.z_ = bbox_.max_.z_ > v_it->z_ ? bbox_.max_.z_ : v_it->z_;
  }
}

/*
 *  @name Translate
 *  @fn void Translate(const Vector3<T>& vector);
 *  @brief  Translate the whole mesh
 *  @param[in] vector, by which to translate the mesh
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::Translate(const LTS5::Vector3<T>& vector) {
  for (auto v_it = vertex_.begin(); v_it != vertex_.end(); ++v_it) {
    (*v_it) += vector;
  }
  // Translate the bounding box as well
  this->bbox_.min_ += vector;
  this->bbox_.max_ += vector;
  this->bbox_.center_ += vector;
}

/*
 *  @name NormalizeMainDiagonal
 *  @fn void NormalizeMainDiagonal();
 *  @brief  Scale the whole mesh such that main diagonal of the cube enclosing
 *          the bounding box is 1
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::NormalizeMainDiagonal() {
  if (!bbox_is_computed_) {
    ComputeBoundingBox();
  }

  T a = bbox_.max_.x_ - bbox_.min_.x_;
  T b = bbox_.max_.y_ - bbox_.min_.y_;
  T c = bbox_.max_.z_ - bbox_.min_.z_;
  Scale(T(1.0 / (sqrt(a*a + b*b + c*c))));
  // Update bbox
  ComputeBoundingBox();
}

/*
 *  @name NormalizeMesh
 *  @fn void NormalizeMesh();
 *  @brief  Scale the whole mesh such that main diagonal is 1 and its COG is at
 *          the origin
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::NormalizeMesh() {
  NormalizeMainDiagonal();
  LTS5::Vector3<T> cog = Cog();
  LTS5::Vector3<T> minus_cog(-cog.x_, -cog.y_, -cog.z_);
  Translate(minus_cog);
}

/*
 * @name  CropAroundVertex
 * @fn    void CropAroundVertex(const size_t& idx, const T& dist)
 * @brief Crop mesh around a given vertex. Triangle that are further away than
 *        a given distance are remove. No vertex are deleted, therefore the
 *        mesh will be degenerated after this step.
 * @param[in] idx     Vertex index to crop around
 * @param[in] dist    Radius of the cropping sphere
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::CropAroundVertex(const size_t& idx,
        const T& dist) {
  // Compute distance
  std::vector<T> distance;
  distance.reserve(vertex_.size());
  const auto& anchor = vertex_[idx];
  for (const auto& v : vertex_) {
    T d = (v - anchor).Norm();
    distance.push_back(d);
  }
  // Iterate over every triangle, if every vertex of the tri are in the sphere,
  // the tri is kept, otherwise it is dropped.
  std::vector<Triangle> ntris;
  for (const auto& t : tri_) {
    const auto* t_ptr = &t.x_;
    bool selected = true;
    for (int k = 0; k < 3; ++k) {
      selected &= distance[t_ptr[k]] < dist;
    }
    if (selected) {
      ntris.push_back(t);
    }
  }
  // Set updated triangles
  this->set_triangle(ntris);
}

/*
 * @name  Area
 * @fn    T Area() const
 * @brief Compute surface area (i.e. sum of each triangle area)
 * @return    Mesh area
 */
template<typename T, typename ColorContainer>
T Mesh<T, ColorContainer>::Area() const {
  T area = T(0.0);
  for (const auto& tri : tri_) {
    const auto& v0 = vertex_[tri.x_];
    const auto& v1 = vertex_[tri.y_];
    const auto& v2 = vertex_[tri.z_];
    T tri_a = ((v1 - v0) ^ (v2 - v0)).Norm() * T(0.5);
    area += tri_a;
  }
  return area;
}

/**
 * @name  Cog
 * @fn  Vertex Cog() const
 * @brief Compute the CENTER OF GRAVITY of the mesh
 * @return COG
 */
template<typename T, typename ColorContainer>
Vector3<T> Mesh<T, ColorContainer>::Cog() const {
  Vertex sum(0.0, 0.0, 0.0);
  for (int i = 0; i < size(); ++i) {
    sum += vertex_[i];
  }
  sum /= size();
  return sum;
}

/*
 *  @name HashExt
 *  @fn FileExt HashExt(const std::string& ext)
 *  @brief  Provide the type of extension
 *  @param[in]  ext Name of the extension
 *  @return Hash of the given extension if know by the object
 */
template<typename T, typename ColorContainer>
typename Mesh<T, ColorContainer>::FileExt Mesh<T, ColorContainer>::HashExt(const std::string& ext) const {
  FileExt fext = kUndef;
  if (ext == "obj") {
    fext = kObj;
  } else if (ext == "ply") {
    fext = kPly;
  } else if (ext == "tri") {
    fext = kTri;
  }
  return fext;
}

/*
 *  @name LoadOBJ
 *  @fn int LoadOBJ(const std::string& path)
 *  @brief  Load mesh from .obj file
 *  @param[in]  path  Path to obj file
 *  @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int Mesh<T, ColorContainer>::LoadOBJ(const std::string& path) {
  int error = -1;
  std::ifstream stream(path, std::ios_base::in);
  if (stream.is_open()) {
    // Init bbox
    bbox_.min_.x_ = std::numeric_limits<T>::max();
    bbox_.max_.x_ = std::numeric_limits<T>::lowest();
    bbox_.min_.y_ = std::numeric_limits<T>::max();
    bbox_.max_.y_ = std::numeric_limits<T>::lowest();
    bbox_.min_.z_ = std::numeric_limits<T>::max();
    bbox_.max_.z_ = std::numeric_limits<T>::lowest();
    // Start to read
    std::string line, key;
    std::stringstream str_stream;
    Vertex vertex;
    Normal normal;
    TCoord tcoord;
    Triangle tri;
    while (!stream.eof()) {
      // Scan line
      std::getline(stream, line);
      if (!line.empty()) {
        str_stream.str(line);
        str_stream >> key;
        if (key == "v") {
          // Vertex
          str_stream >> vertex;
          vertex_.push_back(vertex);
          // Compute boundary box
          bbox_.min_.x_ = bbox_.min_.x_ < vertex.x_ ? bbox_.min_.x_ : vertex.x_;
          bbox_.max_.x_ = bbox_.max_.x_ > vertex.x_ ? bbox_.max_.x_ : vertex.x_;
          bbox_.min_.y_ = bbox_.min_.y_ < vertex.y_ ? bbox_.min_.y_ : vertex.y_;
          bbox_.max_.y_ = bbox_.max_.y_ > vertex.y_ ? bbox_.max_.y_ : vertex.y_;
          bbox_.min_.z_ = bbox_.min_.z_ < vertex.z_ ? bbox_.min_.z_ : vertex.z_;
          bbox_.max_.z_ = bbox_.max_.z_ > vertex.z_ ? bbox_.max_.z_ : vertex.z_;
        } else if (key == "vn") {
          // Normal
          str_stream >> normal;
          normal_.push_back(normal);
        } else if (key == "vt") {
          // Texture coordinate
          str_stream >> tcoord;
          tex_coord_.push_back(tcoord);
        } else if (key == "f") {
          // Faces
          int* t_ptr = &tri.x_;
          std::string tri_idx;
          int t0, t1, t2;
          for (int i = 0; i < 3; ++i) {
            str_stream >> tri_idx;
            if (sscanf(tri_idx.c_str(), "%d/%d/%d", &t0, &t1, &t2) == 3) {
              t_ptr[i] = t0;
            } else if (sscanf(tri_idx.c_str(), "%d//%d", &t0, &t1) == 2) {
              t_ptr[i] = t0;
            } else if (sscanf(tri_idx.c_str(), "%d/%d", &t0, &t1) == 2) {
              t_ptr[i] = t0;
            } else if (sscanf(tri_idx.c_str(), "%d", &t0) == 1) {
              t_ptr[i] = t0;
            } else {
              LTS5_LOG_ERROR("Unsupported triangle format");
            }
          }
          // Convert to 0-index
          tri -= 1;
          tri_.push_back(tri);
        } else if (key == "mtllib") {   // Material
          std::string filename;
          std::string folder = Path::Dirname(path);
          str_stream >> filename;
          this->materials_ = Material<T>::CreateFromFile(Path::Join(folder,
                                                                    filename));
        }
        str_stream.clear();
      }
    }
    bbox_.center_ = (bbox_.min_ + bbox_.max_) * T(0.5);
    bbox_is_computed_ = true;
    error = 0;
  }
  return error;
}

template<class ColorContainer>
void LoadColor(const ColorContainer& color, std::vector<ColorContainer>* colors);

template<>
void LoadColor(const Vector3<float>& color,
               std::vector<Vector3<float>>* colors) {
  if ((color.r_ >= 0.f) || (color.g_ >= 0.f) || (color.b_ >= 0.f)) {
    // Convert to [0, 1]
    colors->push_back(color / 255.f);
  }
}
template<>
void LoadColor(const Vector3<double>& color,
               std::vector<Vector3<double>>* colors) {
  if ((color.r_ >= 0.0) || (color.g_ >= 0.0) || (color.b_ >= 0.0)) {
    // Convert to [0, 1]
    colors->push_back(color / 255.0);
  }
}
template<>
void LoadColor(const Vector4<float>& color,
               std::vector<Vector4<float>>* colors) {
  if ((color.r_ >= 0.f) || (color.g_ >= 0.f) ||
  (color.b_ >= 0.f) || (color.a_ >= 0.f)) {
    // Convert to [0, 1]
    colors->push_back(color / 255.f);
  }
}
template<>
void LoadColor(const Vector4<double>& color,
               std::vector<Vector4<double>>* colors) {
  if ((color.r_ >= 0.0) || (color.g_ >= 0.0) ||
  (color.b_ >= 0.0) || (color.a_ >= 0.0)) {
    // Convert to [0, 1]
    colors->push_back(color / 255.0);
  }
}

template<typename T, typename ColorContainer>
void AddChannelA(std::vector<PlyProperty>* props) {
  // Stub
}

template<>
void AddChannelA<float, Vector4<float>>(std::vector<PlyProperty>* props) {
  typedef PLYVertex<float, Vector4<float>> PLYVertexType ;
  props->push_back({"alpha",
                    PLY_UCHAR,
                    PLY_FLOAT,
                    int(offsetof(PLYVertexType, color.a_)), 0, 0, 0, 0});
}

template<>
void AddChannelA<double, Vector4<double>>(std::vector<PlyProperty>* props) {
  typedef PLYVertex<double, Vector4<double>> PLYVertexType ;
  props->push_back({"alpha",
                    PLY_UCHAR,
                    PLY_DOUBLE,
                    int(offsetof(PLYVertexType, color.a_)), 0, 0, 0, 0});
}

template<typename T, typename ColorContainer>
void GetChannelAProperty(PlyFile* file, char* name, PlyProperty* prop) {
  // Stub
}

template<>
void GetChannelAProperty<float, Vector4<float>>(PlyFile* file,
                                                char* name,
                                                PlyProperty* prop) {
  ply_get_property(file, name, prop);
}
template<>
void GetChannelAProperty<double, Vector4<double>>(PlyFile* file,
                                                  char* name,
                                                  PlyProperty* prop) {
  ply_get_property(file, name, prop);
}






/*
 *  @name LoadPLY
 *  @fn int LoadPLY(const std::string& path)
 *  @brief  Load mesh from .obj file
 *  @param[in]  path  Path to ply file
 *  @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int Mesh<T, ColorContainer>::LoadPLY(const std::string& path) {
  int error = -1;
  // Read ply file
  PlyFile* ply_file;
  int n_elem;
  char ** elem_list;
  int file_type;
  float version;
  // open a PLY file for reading
  ply_file = ply_open_for_reading(const_cast<char*>(path.c_str()),
                                  &n_elem,
                                  &elem_list,
                                  &file_type, &version);
  if (ply_file) {

    int type = sizeof(T) == 4 ? PLY_FLOAT : PLY_DOUBLE;
    typedef PLYVertex<T, ColorContainer> PLYVertexType;
    typedef PLYFace<T, ColorContainer> PLYFaceType;
    std::vector<PlyProperty> v_prop; // list of property for a vertex
    v_prop.push_back({"x", PLY_FLOAT, type, int(offsetof(PLYVertexType, vertex.x_)), 0, 0, 0, 0});
    v_prop.push_back({"y", PLY_FLOAT, type, int(offsetof(PLYVertexType, vertex.y_)), 0, 0, 0, 0});
    v_prop.push_back({"z", PLY_FLOAT, type, int(offsetof(PLYVertexType,vertex.z_)), 0, 0, 0, 0});
    v_prop.push_back({"nx", PLY_FLOAT, type, int(offsetof(PLYVertexType,normal.x_)), 0, 0, 0, 0});
    v_prop.push_back({"ny", PLY_FLOAT, type, int(offsetof(PLYVertexType,normal.y_)), 0, 0, 0, 0});
    v_prop.push_back({"nz", PLY_FLOAT, type, int(offsetof(PLYVertexType,normal.z_)), 0, 0, 0, 0});
    v_prop.push_back({"red", PLY_UCHAR, type, int(offsetof(PLYVertexType,color.r_)), 0, 0, 0, 0});
    v_prop.push_back({"green", PLY_UCHAR, type, int(offsetof(PLYVertexType,color.g_)), 0, 0, 0, 0});
    v_prop.push_back({"blue", PLY_UCHAR, type, int(offsetof(PLYVertexType,color.b_)), 0, 0, 0, 0});
    // Optional channel
    AddChannelA<T, ColorContainer>(&v_prop);
    std::vector<PlyProperty> f_prop;
    f_prop.push_back({"vertex_indices", PLY_INT, PLY_INT,
                      int(offsetof(PLYFaceType, list_idx)), 1, PLY_INT, PLY_INT,
                      int(offsetof(PLYFaceType, n_vertex))});
    f_prop.push_back({"texcoord", PLY_FLOAT, type,
                      int(offsetof(PLYFaceType, list_tcoord)),  1, PLY_INT,
                      PLY_INT, int(offsetof(PLYFaceType, n_tcoord))});

    // go through each kind of element that we learned is in the file
    // and read them
    int cnt = 0;
    for (int i = 0; i < n_elem; ++i) {
      /* get the description of the first element */
      int num_elems;
      int n_props;
      char* elem_name = elem_list[i];
      ply_get_element_description(ply_file,
                                  elem_name,
                                  &num_elems,
                                  &n_props);
      // if we're on vertex elements, read them in
      if (equal_strings ("vertex", elem_name)) {
        // Get position
        ply_get_property(ply_file, elem_name, &v_prop[0]);
        ply_get_property(ply_file, elem_name, &v_prop[1]);
        ply_get_property(ply_file, elem_name, &v_prop[2]);
        // Get normal
        ply_get_property(ply_file, elem_name, &v_prop[3]);
        ply_get_property(ply_file, elem_name, &v_prop[4]);
        ply_get_property(ply_file, elem_name, &v_prop[5]);
        // Get color
        ply_get_property(ply_file, elem_name, &v_prop[6]);
        ply_get_property(ply_file, elem_name, &v_prop[7]);
        ply_get_property(ply_file, elem_name, &v_prop[8]);
        GetChannelAProperty<T, ColorContainer>(ply_file, elem_name, &v_prop[9]);

        // Init bbox
        bbox_.min_.x_ = std::numeric_limits<T>::max();
        bbox_.max_.x_ = std::numeric_limits<T>::lowest();
        bbox_.min_.y_ = std::numeric_limits<T>::max();
        bbox_.max_.y_ = std::numeric_limits<T>::lowest();
        bbox_.min_.z_ = std::numeric_limits<T>::max();
        bbox_.max_.z_ = std::numeric_limits<T>::lowest();

        // grab all the vertex elements
        for (int j = 0; j < num_elems; ++j) {
          PLYVertex<T, ColorContainer> vert;
          ply_get_element(ply_file, reinterpret_cast<void*>(&vert));
          // Push data into structure
          vertex_.push_back(vert.vertex);
          // Compute boundary box
          bbox_.min_.x_ = bbox_.min_.x_ < vert.vertex.x_ ? bbox_.min_.x_ : vert.vertex.x_;
          bbox_.max_.x_ = bbox_.max_.x_ > vert.vertex.x_ ? bbox_.max_.x_ : vert.vertex.x_;
          bbox_.min_.y_ = bbox_.min_.y_ < vert.vertex.y_ ? bbox_.min_.y_ : vert.vertex.y_;
          bbox_.max_.y_ = bbox_.max_.y_ > vert.vertex.y_ ? bbox_.max_.y_ : vert.vertex.y_;
          bbox_.min_.z_ = bbox_.min_.z_ < vert.vertex.z_ ? bbox_.min_.z_ : vert.vertex.z_;
          bbox_.max_.z_ = bbox_.max_.z_ > vert.vertex.z_ ? bbox_.max_.z_ : vert.vertex.z_;
          // Normal if present
          if ((vert.normal.x_ != T(-1.0)) &&
              (vert.normal.y_ != T(-1.0)) &&
              (vert.normal.z_ != T(-1.0))) {
            normal_.push_back(vert.normal);
          }
          // Color if present
          LoadColor(vert.color, &vertex_color_);
        }
        bbox_.center_ = (bbox_.min_ + bbox_.max_) * T(0.5);
        bbox_is_computed_ = true;
      }
      // if we're on face elements, read them in
      if (equal_strings("face", elem_name)) {
        // set up for getting face elements
        ply_get_property (ply_file, elem_name, &f_prop[0]);
        ply_get_property (ply_file, elem_name, &f_prop[1]);
        // grab all the face elements
        for (int j = 0; j < num_elems; j++) {
          PLYFace<T, ColorContainer> face;
          ply_get_element(ply_file, reinterpret_cast<void*>(&face));
          if (face.n_vertex != 3) {
            if (cnt < 5) {
              LTS5_LOG_WARNING("Only triangle supported, load only 3 elements");
            }
            cnt++;
          }
          if (face.list_idx != nullptr) {
            tri_.push_back(face.list_idx[0]);
          }
          if (face.list_tcoord != nullptr) {
            assert(face.n_tcoord == 6);
            tex_coord_.push_back(face.list_tcoord[0]);
            tex_coord_.push_back(face.list_tcoord[1]);
            tex_coord_.push_back(face.list_tcoord[2]);
          }
        }
      }
      // print out the properties we got, for debugging
      //for (int  j = 0; j < n_props; j++)
      //  std::cout << "property " << plist[j]->name << std::endl;
    }
    //Read the comments
    this->comments_.clear();
    for (int i = 0; i < ply_file->num_comments; ++i) {
      this->comments_.push_back(std::string(ply_file->comments[i]));
    }
    // close the PLY file
    ply_close (ply_file);
    // Shall we free elem_list ?
    free(elem_list);
    // Done
    error = 0;
  }
  return error;
}

/*
 *  @name SaveOBJ
 *  @fn int SaveOBJ(const std::string path) const
 *  @brief SAve mesh to a .obj file
 *  @param[in]  path  Path to .obj file
 *  @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int Mesh<T, ColorContainer>::SaveOBJ(const std::string& path) const {
  int err = -1;
  std::ofstream out_stream(path, std::ios_base::out);
  if (out_stream.is_open()) {
    // Header
    out_stream << "# wavefront obj file written by LTS5 c++ library" << std::endl;

    // Vertex
    if (!vertex_.empty()) {
      size_t n_vert = vertex_.size();
      for (size_t i = 0; i < n_vert; ++i) {
        const Vertex& v = vertex_[i];
        out_stream << "v " << v.x_ << " " << v.y_ << " " << v.z_ << std::endl;
      }
    }
    // Normal
    if (!normal_.empty()) {
      size_t n_norm = normal_.size();
      for (size_t i = 0; i < n_norm; ++i) {
        const Normal& n = normal_[i];
        out_stream << "vn " << n.x_ << " " << n.y_ << " " << n.z_ << std::endl;
      }
    }
    // Texture coordinate
    if (!tex_coord_.empty()) {
      size_t n_tcoord = tex_coord_.size();
      for (size_t i = 0; i < n_tcoord; ++i) {
        const TCoord& tc = tex_coord_[i];
        out_stream << "vt " << tc.x_ << " " << tc.y_ << std::endl;
      }
    }
    // Tri
    if (!tri_.empty()) {
      size_t n_tri = tri_.size();
      for (size_t i = 0; i < n_tri; ++i) {
        const Triangle & tri = tri_[i];
        out_stream << "f " << tri.x_ + 1 << " " << tri.y_ + 1;
        out_stream << " " << tri.z_ + 1 << std::endl;
      }
    }
    // Done
    err = out_stream.good() ? 0 : -1;
    out_stream.close();
  }
  return err;
}


template<typename T, typename ColorContainer>
void AddChannelAProperty(PlyFile* file,
                         const char* name,
                         std::vector<PlyProperty>* prop) {
  // Stub
}
template<>
void AddChannelAProperty<float, Vector4<float>>(PlyFile* file,
                                         const char* name,
                                         std::vector<PlyProperty>* prop) {
  typedef PLYVertex<float, Vector4<float>> PLYVertexType;
  const size_t sz = prop->size() - 1;
  prop->push_back({"alpha",
                   PLY_UCHAR,
                   PLY_FLOAT,
                   int(offsetof(PLYVertexType, color.a_)),
                   0,
                   0,
                   0,
                   0});
  ply_describe_property(file, name, &prop->at(sz + 1));
}
template<>
void AddChannelAProperty<double, Vector4<double>>(PlyFile* file,
                                                  const char* name,
                                                  std::vector<PlyProperty>* prop) {
  typedef PLYVertex<double, Vector4<double>> PLYVertexType;
  const size_t sz = prop->size() - 1;
  prop->push_back({"alpha",
                   PLY_UCHAR,
                   PLY_DOUBLE,
                   int(offsetof(PLYVertexType, color.a_)),
                   0,
                   0,
                   0,
                   0});
  ply_describe_property(file, name, &prop->at(sz + 1));
}

/**
 *  @name SavePLY
 *  @fn int SavePLY(const std::string& path) const
 *  @brief SAve mesh to a .ply file
 *  @param[in]  path  Path to .ply file
 *  @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int Mesh<T, ColorContainer>::SavePLY(const std::string& path) const {
  int error = -1;
  //int i,j;
  PlyFile *ply_file;
  int nelems(2);
  char *elem_names[] = { // kinds of elements in the user's object
    (char*)"vertex",
    (char*)"face"
  };
  // set to PLY_BINARY_BE for binary
  int file_type(this->binary_ply_ ? PLY_BINARY_LE : PLY_ASCII);
  float version;
  int nverts = static_cast<int>(this->vertex_.size());
  int nfaces = static_cast<int>(this->tri_.size());

  // create the vertex index lists for the faces
  //for (i = 0; i < nfaces; i++)
  //  faces[i].verts = vert_ptrs[i];

  // open either a binary or ascii PLY file for writing
  // (the file will be called "test.ply" because the routines
  //  enforce the .ply filename extension)

  ply_file = ply_open_for_writing(const_cast<char*>(path.c_str()),
                                  nelems,
                                  elem_names,
                                  file_type,
                                  &version);
  if (ply_file) {
    // Define data type
    int type = sizeof(T) == 4 ? PLY_FLOAT : PLY_DOUBLE;
    typedef PLYVertex<T, ColorContainer> PLYVertexType;
    typedef PLYFace<T, ColorContainer> PLYFaceType;
    // List of properties for the mesh
    std::vector<PlyProperty> v_prop;
    if (!vertex_.empty()) {
      v_prop.push_back({"x",
                        PLY_FLOAT,
                        type,
                        int(offsetof(PLYVertexType,vertex.x_)),
                        0, 0, 0, 0});
      v_prop.push_back({"y",
                        PLY_FLOAT,
                        type,
                        int(offsetof(PLYVertexType,vertex.y_)),
                        0, 0, 0, 0});
      v_prop.push_back({"z",
                        PLY_FLOAT,
                        type,
                        int(offsetof(PLYVertexType,vertex.z_)),
                        0, 0, 0, 0});
      ply_describe_property(ply_file, "vertex", &v_prop[0]);
      ply_describe_property(ply_file, "vertex", &v_prop[1]);
      ply_describe_property(ply_file, "vertex", &v_prop[2]);
    }
    if (!normal_.empty()) {
      v_prop.push_back({"nx",
                        PLY_FLOAT,
                        type,
                        int(offsetof(PLYVertexType, normal.x_)),
                        0, 0, 0, 0});
      v_prop.push_back({"ny",
                        PLY_FLOAT,
                        type,
                        int(offsetof(PLYVertexType, normal.y_)),
                        0, 0, 0, 0});
      v_prop.push_back({"nz",
                        PLY_FLOAT,
                        type,
                        int(offsetof(PLYVertexType, normal.z_)),
                        0, 0, 0, 0});
      int sz = static_cast<int>(v_prop.size()) - 1;
      ply_describe_property(ply_file, "vertex", &v_prop[sz - 2]);
      ply_describe_property(ply_file, "vertex", &v_prop[sz - 1]);
      ply_describe_property(ply_file, "vertex", &v_prop[sz]);
    }
    if (!vertex_color_.empty()) {
      v_prop.push_back({"red",
                        PLY_UCHAR,
                        type,
                        int(offsetof(PLYVertexType, color.r_)),
                        0, 0, 0, 0});
      v_prop.push_back({"green",
                        PLY_UCHAR,
                        type,
                        int(offsetof(PLYVertexType,color.g_)),
                        0, 0, 0, 0});
      v_prop.push_back({"blue",
                        PLY_UCHAR,
                        type,
                        int(offsetof(PLYVertexType, color.b_)),
                        0, 0, 0, 0});
      int sz = static_cast<int>(v_prop.size()) - 1;
      ply_describe_property(ply_file, "vertex", &v_prop[sz - 2]);
      ply_describe_property(ply_file, "vertex", &v_prop[sz - 1]);
      ply_describe_property(ply_file, "vertex", &v_prop[sz]);
      AddChannelAProperty<T, ColorContainer>(ply_file, "vertex", &v_prop);
    }
    // Face properties
    std::vector<PlyProperty> f_prop;
    if (!tri_.empty()) {
      f_prop.push_back({"vertex_indices",
                        PLY_INT,
                        PLY_INT,
                        int(offsetof(PLYFaceType, list_idx)),
                        1,
                        PLY_INT,
                        PLY_INT,
                        int(offsetof(PLYFaceType, n_vertex))});
      ply_describe_property(ply_file, "face", &f_prop[0]);
    }
    if (!tex_coord_.empty()) {
      f_prop.push_back({"texcoord",
                        PLY_FLOAT,
                        type,
                        int(offsetof(PLYFaceType, list_tcoord)),
                        1,
                        PLY_INT,
                        PLY_INT,
                        int(offsetof(PLYFaceType, n_tcoord))});
      int sz = static_cast<int>(f_prop.size()) - 1;
      ply_describe_property(ply_file, "face", &f_prop[sz]);
    }

    // describe what properties go into the vertex and face elements
    ply_element_count(ply_file, "vertex", nverts);
    // Face properties
    ply_element_count(ply_file, "face", nfaces);

    // write a comment and an object information field
    for (int ith_comment = 0; ith_comment < comments_.size(); ++ith_comment) {
      ply_put_comment(ply_file,
                      const_cast<char*>(comments_[ith_comment].c_str()));
    }

    // we have described exactly what we will put in the file, so
    // we are now done with the header info
    ply_header_complete(ply_file);

    // set up and write the vertex elements
    ply_put_element_setup(ply_file, "vertex");
    for (int i = 0; i < nverts; i++) {
      PLYVertex<T, ColorContainer> vert;
      if (!this->vertex_.empty()) {
        vert.vertex = this->vertex_[i];
      }
      if (!this->normal_.empty()) {
        vert.normal = this->normal_[i];
      }
      if (!this->vertex_color_.empty()) {
        auto& v = this->vertex_color_[i];
        if (v.r_ <= T(1.0) && v.g_ <= T(1.0) && v.b_ <= T(1.0)) {
          // Color is normalized, scale it in range [0, 255] to be uchar
          // compliant
          vert.color = this->vertex_color_[i] * T(255.0);
        } else {
          vert.color = this->vertex_color_[i];
        }
      }
      ply_put_element(ply_file, (void *) &(vert));
    }
    // set up and write the face elements
    ply_put_element_setup(ply_file, "face");
    for (int i = 0; i < nfaces; i++) {
      PLYFace<T, ColorContainer> face;
      Triangle tri(this->tri_[i]);
      TCoord tcoord;
      face.n_vertex = 3;
      face.list_idx = &tri;
      if (!this->tex_coord_.empty()) {
        tcoord = this->tex_coord_[i];
        face.n_tcoord = 2;
        face.list_tcoord = &tcoord;
      }
      ply_put_element(ply_file, (void *) &(face));
    }
    // close the PLY file
    ply_close(ply_file);
    error = 0;
  }
  return error;
}

/*
 *  @name LoadTri
 *  @fn int LoadTri(const std::string& path)
 *  @brief  Load mesh triangulation from .tri file
 *  @param[in]  path  Path to .tri file
 *  @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int Mesh<T, ColorContainer>::LoadTri(const std::string& path) {
  int error = -1;
  FILE* file_id = fopen(path.c_str(), "r");
  if (file_id) {
    const int LINE_SIZE = 1024;
    char buffer[LINE_SIZE];
    int n_line = 0;
    bool ok = true;
    // Start to loop over
    while (ok && fgets(buffer, LINE_SIZE, file_id) != NULL) {
      n_line++;
      char* line_ptr = buffer;
      char* end_ptr = buffer + strlen(buffer);
      // Find first non-white space character
      while (isspace(*line_ptr) && line_ptr < end_ptr) {line_ptr++;}
      // Check command, (expected to be 'f')
      const char* cmd = line_ptr;
      // Skip over non-whitespace
      while ( !isspace(*line_ptr) && line_ptr < end_ptr) {line_ptr++;}
      //  terminate command
      if (line_ptr < end_ptr) {
        *line_ptr = '\0';
        line_ptr++;
      }
      // in the OBJ format the first characters determine how to interpret
      // the line:
      if (strcmp(cmd, "f") == 0) {
        Triangle tri;
        int* tri_ptr = &tri.x_;
        int vertex_cnt = 0;

        while (ok && line_ptr < end_ptr) {
          //  find the first non-whitespace character
          while (isspace(*line_ptr) && line_ptr < end_ptr) { line_ptr++; }
          //  there is still data left on this line
          if (line_ptr < end_ptr) {
            int vertex_idx, t_coord_idx, normal_idx;
            if (sscanf(line_ptr,
                       "%d",
                       &vertex_idx) == 1) {
              // Shape polygon
              tri_ptr[vertex_cnt] = vertex_idx - 1;
              vertex_cnt++;
            } else if (sscanf(line_ptr,
                       "%d/%d",
                       &vertex_idx,
                       &t_coord_idx) == 2) {
              // Shape polygon
              tri_ptr[vertex_cnt] = vertex_idx - 1;
              vertex_cnt++;
            } else if (sscanf(line_ptr,
                              "%d/%d/%d",
                              &vertex_idx,
                              &t_coord_idx,
                              &normal_idx) == 3) {
              // Shape polygon
              tri_ptr[vertex_cnt] = vertex_idx - 1;
              vertex_cnt++;
            } else {
              std::cout << "Error reading 'f' at line : " <<
              n_line << std::endl;
              ok = false;
            }
            //  skip over what we just read
            //  (find the first whitespace character)
            while (!isspace(*line_ptr) && line_ptr < end_ptr) { line_ptr++; }
          }
        }
        assert(vertex_cnt == 3);
        tri_.push_back(tri);
      }
    }
    fclose(file_id);
    error = ok ? 0 : -1;
  } else {
    std::cerr << "Can't load triangulation file !!!" << std::endl;
  }
  return error;
}

/*
 * @name  ComputeCotanWeight
 * @fn T ComputeCotanWeight(const Vertex& I,
                            const Vertex& J,
                            const Vertex& A,
                            T* weight,
                            T* barycentric_area) const
 * @brief Compute individual cotan weight for the given triangle
 * @param[in] I        1st vertex of the triangle
 * @param[in] J        2nd vertex of the triangle
 * @param[in] A        3rd vertex of the triangle
 * @param[out] weight  cotan weight
 * @param[out] area    area of the triangle
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::ComputeCotanWeight(const Vertex& I,
                                 const Vertex& J,
                                 const Vertex& A,
                                 T* weight,
                                 T* barycentric_area) const {
  // Formulas come from:
  // https://igl.ethz.ch/projects/bbw/a-cotangent-laplacian-for-images-as-surfaces-2012-jacobson-sorkine.pdf
  T lij = (J - I).Norm();
  T lja = (A - J).Norm();
  T lai = (I - A).Norm();
  // Semi-perimeter
  T r = 0.5 * (lij + lja + lai);
  T triangle_area = std::sqrt(r * (r - lij) * (r - lja) * (r - lai));
  // Decompose barycentric cell into two triangles
  Vertex barycenter = I * T(1/3.0) + J * T(1/3.0) + A * T(1/3.0);
  Vertex k = I * T(1/2.0) + A * T(1/2.0);
  Vertex l = I * T(1/2.0) + J * T(1/2.0);
//  T lik = (k - I).Norm(); // = lai/2
//  T lil = (l - I).Norm(); // = lij/2
  T lkl = (k - l).Norm();
  T r1 = 0.5 * (lij/2.0 + lai/2.0 + lkl);

  T lkc = (barycenter - k).Norm();
  T lcl = (l - barycenter).Norm();
  T r2 = 0.5 * (lkl + lkc + lcl);

  T area1 = std::sqrt(r1 * (r1 - (lij/2.0)) * (r1 - (lai/2.0)) * (r1 - lkl));
  T area2 = std::sqrt(r2 * (r2 - lkl) * (r2 - lkc) * (r2 - lcl));
  *barycentric_area = area1 + area2;

  // TODO(gabriel): DEBUG PURPOSE
  if (*barycentric_area < 0) {
    LTS5_LOG_ERROR("barycentric area < 0!");
  }
  T cot_alpha = (- (lij*lij) + (lja*lja) + (lai*lai)) / (4 * (triangle_area));
  *weight =  cot_alpha;

  // TODO(gabriel): DEBUG PURPOSE
  if (*weight < 0) {
    LTS5_LOG_ERROR("cotan(alpha) < 0!");
  }
}

/*
 * @name  ComputeEdgeProperties
 * @fn    void ComputeEdgeProperties(std::vector<Vpe>* vpe,
 *                                   std::vector<Fpe>* fpe)
 * @brief Compute the list of edges for the mesh.
 * @param[out] vpe List of vertices connected to the edges
 * @param[out] fpe List of faces connected to the edges
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::ComputeEdgeProperties(std::vector<Vpe>* vpe,
                                                    std::vector<Fpe>* fpe) {
  // Clear output
  vpe->clear();
  fpe->clear();
  // Build halfedges
  if (halfedges_.empty()) {
    this->ComputeHalfedges();
  }
  // Compute edfes
  std::vector<std::uint8_t> he_visited(halfedges_.size(), 0);
  for (int i = 0; i < halfedges_.size(); ++i) {
    const auto& he = halfedges_[i];
    if (he.IsBoundary()) {
      // Boundary edge, add it automatically
      const int idx = ((i + 1) % 3) == 0 ? i - 2 : i + 1;
      vpe->emplace_back(he.origin, halfedges_[idx].origin);
      fpe->emplace_back(i/3, -1);
    } else {
      // Already selected ?
      if (he_visited[i] == 0 && he_visited[he.opposite] == 0) {
        // New edge
        vpe->emplace_back(he.origin, halfedges_[he.opposite].origin);
        fpe->emplace_back(i/3, he.opposite/3);
        he_visited[i] = 1;
        he_visited[he.opposite] = 1;
      }
    }
  }
}

/*
 * @name  RemeshForTriangles
 * @fn    void RemeshForTriangles(const std::vector<Triangle>& subset)
 * @brief Given a subset of triangles, update the mesh vertex/normal and
 *        triangulation.
 * @param[in] subset  List of selected triangles, must be part of the original
 *                    triangulation
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::
RemeshForTriangles(const std::vector<Triangle>& subset) {
  std::vector<Vertex> vertex;
  std::vector<Triangle> tris;
  std::unordered_map<int, int> vertex_map;
  int v_idx = 0;
  for (auto tri : subset) {  // Copy so can edit and reuse it
    auto* t_ptr = &tri.x_;
    for (auto i = 0; i < 3; ++i) {
      // Vertex already picked ?
      auto& t_idx = t_ptr[i];
      auto it = vertex_map.find(t_idx);
      if (it == vertex_map.end()) {
        // New vertex
        const auto& vi = this->vertex_[t_idx];
        vertex.push_back(vi);
        vertex_map.emplace(t_idx, v_idx);
        t_idx = v_idx;    // overwrite previous index
        v_idx += 1;       // Indicates new vertex
      } else {
        // Vertex already used, only update triangle
        t_idx = it->second;
      }
    }
    tris.push_back(tri);
  }
  // Update vertices
  this->set_vertex(vertex);
  this->set_triangle(tris);
  this->ComputeVertexNormal();
}

/**
 * @name    VectorProduct
 * @fn Matrix3x3<T> VectorProduct(const Vector3<T>& a, const Vector3<T>& b)
 * @brief Compute product between two vectors: a * b' resulting in a matrix
 * @param[in] a Vector A
 * @param[in] b Vector B
 * @tparam T    Data type
 * @return  Matrix holding a * b'
 */
template<typename T>
Matrix3x3<T> VectorProduct(const Vector3<T>& a, const Vector3<T>& b) {
  Matrix3x3<T> mat;
  mat[0] = a.x_ * b.x_;
  mat[1] = a.x_ * b.y_;
  mat[2] = a.x_ * b.z_;
  mat[3] = a.y_ * b.x_;
  mat[4] = a.y_ * b.y_;
  mat[5] = a.y_ * b.z_;
  mat[6] = a.z_ * b.x_;
  mat[7] = a.z_ * b.y_;
  mat[8] = a.z_ * b.z_;
  return mat;
}

/*
 * @name  ComputePrincipalCurvatures
 * @fn    void ComputePrincipalCurvatures()
 * @brief Compute principal curvature of the surface based on "ESTIMATING THE
 *        TENSOR OF CURVATURE OF A SURFACE FROM A POLYHEDRAL APPROXIMATION",
 *        G. Taubin
 */
template<typename T, typename ColorContainer>
void Mesh<T, ColorContainer>::ComputePrincipalCurvatures() {
  // Type
  using Mat3 = Matrix3x3<T>;
  using Vec3 = Vector3<T>;

  bool use_surface = false;

  // Init container
  principal_curvature_.clear();
  principal_curvature_.resize(vertex_.size());
  // Compute vertex normal + halfedge (i.e. one-ring neighbour)
  if (normal_.empty()) {
    this->ComputeVertexNormal();
  }
  if (halfedges_.empty()) {
    this->ComputeHalfedges();
  }

  std::vector<size_t> vi_neighbors;
  std::vector<size_t> vj_neighbors;
  for (size_t i = 0; i < vertex_.size(); ++i) {
    // Estimate matrix Mvi
    // Mvi = sum( Wij * Kij * Tij * Tij' )
    // Tij = unit length normalized vector of vj - vi projected onto the tangent
    // plane
    // Get one-ring
    this->OneRingNeighbors(i, &vi_neighbors);
    std::sort(vi_neighbors.begin(), vi_neighbors.end());  // needed later
    // Projection onto tangent plane
    const auto& ni = this->normal_[i];
    const auto& vi = this->vertex_[i];
    Mat3 p_tan;
    p_tan = p_tan.Identity() - VectorProduct(ni, ni);
    // Loop over neighbour
    std::vector<Vec3> Tij;
    std::vector<T> Kij;
    std::vector<T> Wij;
    for (const auto& j : vi_neighbors) {
      const auto& vj = this->vertex_[j];
      auto v_ij = vj - vi;
      // Project Vij onto tangent plane
      auto T_ij = p_tan * v_ij;
      T_ij.Normalize();
      Tij.push_back(T_ij);
      // Approximate directional curvatures, Kij
      auto K_ij = (T(2.0) * (ni * v_ij)) / v_ij.SquaredNorm();
      Kij.push_back(K_ij);

      // Estimate weight: Wij, find incident triangles that are common to both
      // vertex vi + vj. Two possible value, 2 values or 1 value (i.e. border)
      // Find intersection between neighbour of vi and neighbour of vj
      this->OneRingNeighbors(j, &vj_neighbors);
      std::sort(vj_neighbors.begin(), vj_neighbors.end());
      size_t n_neighbours = std::min(vj_neighbors.size(),
                                     vi_neighbors.size());
      std::vector<size_t> shared_idx(n_neighbours);
      auto it = std::set_intersection(vi_neighbors.begin(),
                                      vi_neighbors.end(),
                                      vj_neighbors.begin(),
                                      vj_neighbors.end(),
                                      shared_idx.begin());
      shared_idx.resize(it - shared_idx.begin());

      assert(shared_idx.size() == 2 || shared_idx.size() == 1);

      T w_ij = T(0.0);


      for (const auto& k : shared_idx) {
        if (use_surface) {
          // Compute triangle area between triangle: vi, vj, vk
          const auto& vk = this->vertex_[k];
          const auto v_ik = vk - vi;
          T area = (v_ij ^ v_ik).Norm() * T(0.5);
          w_ij += area;
        } else {
          // Estimating normal vectors and curvatures by centroid weight,
          // Chen and Yu
          // Need to find the last vertex of triangle

          auto v_ik = vertex_[k] - vi;
          T v_ik_dir = (v_ij ^ v_ik) * ni;
          if (v_ik_dir > T(0.0)) {
            // Aligned, find correct last vertex of triangle
            auto gk = (vi + vj + vertex_[k]) / T(3.0);
            w_ij = T(1.0) / (gk - vi).SquaredNorm();
            break;
          }
        }
      }
      assert(w_ij != T(0.0));
      Wij.push_back(w_ij);
    }
    // Build M_vi = sum( Wij * Kij * Tij * Tij' )
    Mat3 Mvi;
    T Wij_sum = std::accumulate(Wij.begin(), Wij.end(), T(0.0));
    for (size_t k = 0; k < Wij.size(); ++k) {
      Mvi += VectorProduct(Tij[k], Tij[k]) * Kij[k] * (Wij[k] / Wij_sum);
    }
    // Build Householder matrix
    // Wvi = (E1 +/- Nvi) / | (E1 +/- Nvi) |
    Vec3 E1(T(1.0), T(0.0), T(0.0));
    Vec3 Wvi_plus = E1 + ni;
    Vec3 Wvi_minus = E1 - ni;
    Vec3 Wvi = (Wvi_minus.SquaredNorm() > Wvi_plus.SquaredNorm() ?
                Wvi_minus :
                Wvi_plus);
    Wvi.Normalize();
    // Qvi = I - 2 * Wvi * Wvi'
    Mat3 Qvi;
    Qvi = Qvi.Identity() - (VectorProduct(Wvi, Wvi) * T(2.0));
    // Compute eigenvalues + Princiape curvatures
    Mat3 QtMQ = Qvi.Transpose() * Mvi * Qvi;
    Curv kp;
    kp.x_ = T(3.0) * QtMQ(1, 1) - QtMQ(2, 2);
    kp.y_ = T(3.0) * QtMQ(2, 2) - QtMQ(1, 1);
    this->principal_curvature_[i] = kp;
  }
}

#pragma mark -
#pragma mark Declaration

/** Float Mesh - Color = Vector4 */
template class Mesh<float, Vector4<float>>;
/** Float Mesh - Color = Vector3 */
template class Mesh<float, Vector3<float>>;
/** Double Mesh - Color = Vector4 */
template class Mesh<double, Vector4<double>>;
/** Double Mesh - Color = Vector3 */
template class Mesh<double, Vector3<double>>;
/** Float Material */
template class Material<float>;
/** Double Material */
template class Material<double>;


}  // namespace LTS5
