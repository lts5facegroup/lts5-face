/**
 *  @file   general_quadric.cpp
 *  @brief  General Quadric local shape estimator function implementation
 *          Check out section 4 (a) Local fit of a general quadric, from
 *          Y. Ohtake "Multi-level Partition of Unity Implicits"
 *  @author Gabriel Cuendet
 *  @date   12/04/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <limits>
#include <queue>
#include <iterator>
#include <algorithm>

#include "lts5/geometry/general_quadric.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/*
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/*
 * @name  Clone
 * @fn  Quadric* Clone(void);
 * @brief Deep copy method
 */
template<typename T>
Quadric<T>* Quadric<T>::Clone(void) {
  Quadric<T>* deep_cpy = new Quadric();
  deep_cpy->A_ = this->A_.clone();
  deep_cpy->b_ = this->b_;
  deep_cpy->c_ = this->c_;
  deep_cpy->center_ = this->center_;
  deep_cpy->radius_ = this->radius_;
  deep_cpy->w_ = this->w_;
  deep_cpy->Sw_ = this->Sw_;
  deep_cpy->NN_aux_ = this->NN_aux_;

  return deep_cpy;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Fit
 * @fn  void Fit(const std::vector<LTS5::Vector3<T> >& points,
 *               const std::vector<LTS5::Vector3<T> >& normals,
 *               const AABB& bbox)
 * @brief Fit the general quadric to the input points using least squares
 * @param[in] points   Vertices within the support radius of the cell
 * @param[in] normals  Normals of the vertices
 * @param[in] bbox     Bounding box of the current cell
 * @return    error    fitting error
 */
template<typename T>
T Quadric<T>::Fit(const std::vector<LTS5::Vector3<T> >& points,
                  const std::vector<LTS5::Vector3<T> >& normals,
                  const AABB<T>& bbox) {
  // By default, the support radius R is equal to 0.75 times the main diagonal
  // of the bounding box: R = 0.75*d (cf. Eq.(5))
  T d = (bbox.max_ - bbox.min_).Norm();
  this->radius_ = 0.75 * d;
  return this->Fit(points, normals, bbox, radius_);
}

/*
 * @name  Fit
 * @fn  void Fit(const std::vector<LTS5::Vector3<T> >& points,
 *               const std::vector<LTS5::Vector3<T> >& normals,
 *               const AABB& bbox, const T& R)
 * @brief Fit the general quadric to the input points using least squares
 * @param[in] points   Vertices within the support radius of the cell
 * @param[in] normals  Normals of the vertices
 * @param[in] bbox     Bounding box of the current cell
 * @param[in] R        Radius of the support sphere (used for the weights)
 * @return    error    fitting error
 */
template<typename T>
T Quadric<T>::Fit (const std::vector<LTS5::Vector3<T> > &points,
                   const std::vector<LTS5::Vector3<T> > &normals,
                   const AABB<T> &bbox, const T& R) {
  T error = std::numeric_limits<T>::max();
  this->radius_ = R;
  this->center_ = bbox.center_;

  // 1) Get auxiliary points
  std::vector<LTS5::Vector3<T> > aux_pts;
  aux_pts.push_back(bbox.center_);
  for (int i = 0; i < 8; ++i) {
    aux_pts.push_back(bbox.min_ +
      LTS5::Vector3<T>(i%2 * (bbox.max_.x_ - bbox.min_.x_),
                       (i/2)%2 * (bbox.max_.y_ - bbox.min_.y_),
                       (i/4)%2 * (bbox.max_.z_ - bbox.min_.z_)));
  }

  // 2) For each of them, get the 6 nearest neighbors and test the orientation
  //    of the normals to decide wether or not to keep the aux. pt.
  // 3) For each remaining one, compute the average distance according to (10)
  std::vector<T> aux_di;
  this->CheckAuxPts(points, normals, &aux_pts, &aux_di);
  if (aux_pts.empty()) {
    std::cout << "[Quadric::Fit] Warning: No consistent auxiliary points!" <<
    std::endl;

    return error;
  }

  // 4) Solve eq. (11)
  int n_pts = static_cast<int>(points.size());
  int n_aux = static_cast<int>(aux_pts.size());
  cv::Mat P;
  cv::Mat V;
  cv::Mat W = cv::Mat::eye(n_pts + n_aux,
                           1, cv::DataType<T>::type);
  cv::Mat q;

  this->ComputeWeights(points, static_cast<int>(aux_pts.size()), &W);
  this->BuildLinearSystem(points, aux_pts, aux_di, &P, &V);

  cv::Mat PtW(10, n_pts + n_aux, cv::DataType<T>::type);
  for (int i = 0; i < W.rows; ++i) {
    for (int j = 0; j < 10; ++j) {
      PtW.at<T>(j, i) =  P.at<T>(i, j) * W.at<T>(i);
    }
  }

  cv::Mat PtWP = PtW * P;
  cv::Mat PtWV = PtW * V;
  typename LTS5::LinearAlgebra<T>::LinearSolver linear_least_squares;
  linear_least_squares.Solve(PtWP, PtWV, &q);

  // DEBUG PURPOSE
  //std::cout << "q = " << q << std::endl;

  // Update A_, b_ and c_
  A_.create(3, 3, cv::DataType<T>::type);
  A_.at<T>(0, 0) = q.at<T>(0);
  A_.at<T>(1, 0) = q.at<T>(1);
  A_.at<T>(2, 0) = q.at<T>(2);
  A_.at<T>(0, 1) = q.at<T>(1);  // Enforce that A is diagonal!
  A_.at<T>(1, 1) = q.at<T>(3);
  A_.at<T>(2, 1) = q.at<T>(4);
  A_.at<T>(0, 2) = q.at<T>(2);  // Enforce that A is diagonal!
  A_.at<T>(1, 2) = q.at<T>(4);  // Enforce that A is diagonal!
  A_.at<T>(2, 2) = q.at<T>(5);
  b_ = LTS5::Vector3<T>(q.at<T>(6), q.at<T>(7), q.at<T>(8));
  c_ = q.at<T>(9);

  // DEBUG PURPOSE
  //std::cout << "A = " << A_ << std::endl;
  //std::cout << "b = " << b_ << std::endl;
  //std::cout << "c = " << c_ << std::endl;

  return this->Epsilon(points);
}

/*
 * @name  Evaluate
 * @fn  void Evaluate(const LTS5::Vector3<T>& x)
 * @brief Evalute the value of x, given the quadric approximation
 * @param[in]  x       Point to evaluate
 * @param[out] SwQ     Sum Qi(x)wi(x)
 * @param[out] Sw      Sum wi(x)
 */
template<typename T>
void Quadric<T>::Evaluate(const LTS5::Vector3<T>& x, T* SwQ, T* Sw) {
  // Compute the weight w_i(x), compute the sum of weight Sw (they are equal
  // when there is only one approximation region), and eval Q_i(x)
  // multiply Q_i(x) by w_i(x), that is SwQ
  // At the end, divide by Sw!
  LTS5::Vector3<T> vec_xc = x - center_;
  T r_i = vec_xc.Norm() / radius_;
  T w_i = w_(r_i);

  *Sw += w_i;

  T Q_i = EvaluateQ(x);
  *SwQ += w_i * Q_i;
}

/*
 * @name  EvaluateSquare
 * @fn  virtual void EvaluateSquare(const LTS5::Vector3<T>& x, T* SwQ2,
 *                                  T* Sw) = 0
 * @brief Evalute the squared value of x, given the approximation
 * @param[in]  x     Point to evaluate
 * @param[out] SwQ2  Sum Qi(x)^2 wi(x)
 * @param[out] Sw    Sum wi(x)
 */
template<typename T>
void Quadric<T>::EvaluateSquare(const LTS5::Vector3<T>& x, T* SwQ2,
                                T* Sw) {
  // Compute the weight w_i(x), compute the sum of weight Sw (they are equal
  // when there is only one approximation region), and eval Q_i(x)
  // multiply Q_i(x) by w_i(x), that is SwQ
  // At the end, divide by Sw!
  LTS5::Vector3<T> vec_xc = x - center_;
  T r_i = vec_xc.Norm() / radius_;
  T w_i = w_(r_i);

  if (w_i == 0.0)
    return;

  *Sw += w_i;

  T Q_i = EvaluateQ(x);
  *SwQ2 += w_i * Q_i * Q_i;
}

/*
 * @name  EvaluateGrad
 * @fn  void EvaluateGrad(const LTS5::Vector3<T>& x,
 *                        LTS5::Vector3<T>* SwGradQ, T* Sw)
 * @brief Evaluate the gradient of Q at x
 * @param[in]  x       Point at which to evaluate the grad
 * @param[out] SwGradQ Sum Grad(Qi)(x)wi(x)
 * @param[out] Sw      Sum wi(x)
 */
template<typename T>
void Quadric<T>::EvaluateGrad(const LTS5::Vector3<T>& x,
                              LTS5::Vector3<T>* SwGradQ, T* Sw) {
  // Compute the weight w_i(x), compute the sum of weight Sw (they are equal
  // when there is only one approximation region), and eval Q_i(x)
  // multiply Q_i(x) by w_i(x), that is SwQ
  // At the end, divide by Sw!
  LTS5::Vector3<T> vec_xc = x - center_;
  T r_i = vec_xc.Norm() / radius_;
  T w_i = w_(r_i);

  *Sw += w_i;

  LTS5::Vector3<T> GradQ_i = EvaluateGradQ(x);
  *SwGradQ += w_i * GradQ_i;
}

/*
 * @name  EvaluateGradSquare
 * @fn  virtual void EvaluateGradSquare(const LTS5::Vector3<T>& x,
 *                                      LTS5::Vector3<T>* SwGradQ2, T* Sw) = 0
 * @brief Evaluate the gradient of Q^2 at x
 * @param[in]  x         Point to evaluate
 * @param[out] SwGradQ2  Sum Grad(Q_i^2)(x) wi(x)
 * @param[out] Sw        Sum wi(x)
 */
template<typename T>
void Quadric<T>::EvaluateGradSquare(const LTS5::Vector3<T>& x,
                                    LTS5::Vector3<T>* SwGradQ2, T* Sw) {
  // Compute the weight w_i(x), compute the sum of weight Sw (they are equal
  // when there is only one approximation region), and eval Grad(Q_i)(x)
  // multiply Grad(Q_i)(x) by w_i(x), that is SwGradQ
  // At the end, divide by Sw!
  LTS5::Vector3<T> vec_xc = x - center_;
  T r_i = vec_xc.Norm() / radius_;
  T w_i = w_(r_i);

  if (w_i == 0.0)
    return;

  *Sw += w_i;

  LTS5::Vector3<T> GradQ_i = EvaluateGradQ(x);
  T Q_i = EvaluateQ(x);
  *SwGradQ2 += w_i * T(2.0) * Q_i * GradQ_i;
}

/*
 * @name  EvaluateGradQ
 * @fn  T EvaluateGradQ(const LTS5::Vector3<T>& x, LTS5::Vector3<T>* grad)
 * @brief Evalute the gradient of x, given the quadric approximation
 * @param[in]  x       Point to evaluate
 * @param[out] grad    Gradient of the quadric evaluated at x
 */
template<typename T>
void Quadric<T>::EvaluateGradQ(const LTS5::Vector3<T>& x,
                               LTS5::Vector3<T>* grad) {
  grad->x_ = 2 * A_.at<T>(0, 0) * x.x_ +
             2 * A_.at<T>(0, 1) * x.y_ +
             2 * A_.at<T>(0, 2) * x.z_ + b_.x_;

  grad->y_ = 2 * A_.at<T>(1, 0) * x.x_ +
             2 * A_.at<T>(1, 1) * x.y_ +
             2 * A_.at<T>(1, 2) * x.z_ + b_.y_;

  grad->z_ =  2 * A_.at<T>(2, 0) * x.x_ +
              2 * A_.at<T>(2, 1) * x.y_ +
              2 * A_.at<T>(2, 2) * x.z_ + b_.z_;
}

/*
 * @name  EvaluateGradQ
 * @fn  LTS5::Vector3<T> EvaluateGradQ(const LTS5::Vector3<T>& x)
 * @brief Evalute the gradient of x, given the quadric approximation
 * @param[in]  x       Point to evaluate
 * @return     grad    Gradient of the quadric evaluated at x
 */
template<typename T>
LTS5::Vector3<T> Quadric<T>::EvaluateGradQ(const LTS5::Vector3<T>& x) {
  LTS5::Vector3<T> grad;
  EvaluateGradQ(x, &grad);
  return grad;
}

/*
 * @name  EvaluateQ
 * @fn  T EvaluateQ(const LTS5::Vector3<T>& x)
 * @brief Evalute the value of x, given the quadric approximation
 * @param[in]  x       Point to evaluate
 * @return     val     Value of the quadric evaluated at x
 */
template<typename T>
T Quadric<T>::EvaluateQ(const LTS5::Vector3<T>& x) {
  LTS5::Vector3<T>
  temp(A_.at<T>(0,0) * x.x_ + A_.at<T>(0,1) * x.y_ + A_.at<T>(0,2) * x.z_,
       A_.at<T>(1,0) * x.x_ + A_.at<T>(1,1) * x.y_ + A_.at<T>(1,2) * x.z_,
       A_.at<T>(2,0) * x.x_ + A_.at<T>(2,1) * x.y_ + A_.at<T>(2,2) * x.z_);

  T res = x.x_ * temp.x_ + x.y_ * temp.y_ + x.z_ * temp.z_;
  res += b_.x_ * x.x_ + b_.y_ * x.y_ + b_.z_ * x.z_;
  res += c_;

  return res;
}

#pragma mark -
#pragma mark Private methods

/*
 * @name CheckAuxPoints
 * @fn void CheckAuxPts(const std::vector<LTS5::Vector3<T> >& points,
 *                      const std::vector<LTS5::Vector3<T> >& normals,
 *                      std::vector<LTS5::Vector3<T> >* aux_points,
 *                      std::vector<T>* di)
 * @brief Check the consistency of normals of the aux_points and compute the
 *        arithmetic mean normal of each of the aux points
 * @param[in]     points       Points of the mesh within the support ball
 * @param[in]     normals      Normals of these points
 * @param[in,out] aux_points   Auxiliary points, cheked for consistency
 * @param[out]    di           Computed mean distance of the aux points to
 *                             the surface
 */
template<typename T>
void Quadric<T>::CheckAuxPts(const std::vector<LTS5::Vector3<T> >& points,
                             const std::vector<LTS5::Vector3<T> >& normals,
                             std::vector<LTS5::Vector3<T> >* aux_points,
                             std::vector<T>* di) {
  typename std::vector<LTS5::Vector3<T>>::reverse_iterator r_it = aux_points->rbegin();
  std::priority_queue<std::pair<double, int>,
                      std::vector<std::pair<double, int> >,
                      LTS5::Quadric<T>::PairComparison> NN_idx;

  for (; r_it != aux_points->rend(); ) {
    bool valid = true;

    // 1) Six NN are found using brute force NN
    for (int p = 0; p < points.size(); ++p) {
      LTS5::Vector3<T> diff((*r_it) - points[p]);
      double dist = diff.Norm();

      if (NN_idx.size() < this->NN_aux_) {
        NN_idx.push(std::make_pair(dist, p));
        continue;
      }

      if (dist < NN_idx.top().first) {
        NN_idx.push(std::make_pair(dist, p));
      }

      if (NN_idx.size() > this->NN_aux_) {
        NN_idx.pop();
      }
    }

    // 2) n^(i)*(q-p^(i)) are computed for each of them and if they don't have
    //    the same sign, q is removed from aux_points
    std::pair<double, int> neighbor = NN_idx.top();
    LTS5::Vector3<T> vec_ab((*r_it) - points[neighbor.second]);
    NN_idx.pop();
    T projection = normals[neighbor.second] * vec_ab;
    T proj_sum = projection;
    while (!NN_idx.empty()) {
      neighbor = NN_idx.top();
      vec_ab = (*r_it) - points[neighbor.second];
      NN_idx.pop();
      projection = normals[neighbor.second] * vec_ab;
      if (projection * proj_sum < 0) {
        valid = false;
        // Remove the invalid aux_pt from aux_points
        // @TODO: (Gab) Check that it removes the right element!
        aux_points->erase(std::next(r_it).base());
        break;
      } else {
        proj_sum += projection;
      }
    }

    //3) Compute d_i, 1/6*sum_{i=1}^{6}{n^(i)*(q-p^(i))}
    if (valid) {
      di->push_back(proj_sum / this->NN_aux_);
    }

    ++r_it;
  }

  // push_back is done in a loop using a REVERSE iterator!
  std::reverse(di->begin(), di->end());

  /* DEBUG PURPOSE
  std::cout << "Auxiliary points : " << std::endl;
  for (int i = 0; i < di->size(); ++i) {
    std::cout << "d" << (*aux_points)[i] << " = " << (*di)[i] << std::endl;
  }
   */
}

/*
 * @name BuildLinearSystem
 * @fn void BuildLinearSystem(const std::vector<LTS5::Vector3<T> >& points,
                              const std::vector<LTS5::Vector3<T> >& aux_points,
                              const std::vector<T> di,
                              cv::Mat* A, cv::Mat* B)
 * @brief From the points on the surface and the auxiliary points with value
 *        di, build the linear system Ax=B matrices A and B
 * @param[in]     points       Points of the mesh within the support ball
 * @param[in]     aux_points   Auxiliary points with consistent normals
 * @param[in]     di           Values of the aux. points (distances)
 * @param[out]    A            Coordinates second degree matrix
 * @param[out]    B            Values vector (0, ..., 0, d1, ..., dn)
 */
template<typename T>
void Quadric<T>::BuildLinearSystem(const std::vector<LTS5::Vector3<T> >& points,
                                   const std::vector<LTS5::Vector3<T> >& aux_points,
                                   const std::vector<T>& di,
                                   cv::Mat* A, cv::Mat* B) {
  // Allocate memory for A and B
  int n_points = static_cast<int>(points.size());
  int n_aux = static_cast<int>(aux_points.size());

  A->create(n_points + n_aux, 10, cv::DataType<T>::type);
  B->create(n_points + n_aux, 1, cv::DataType<T>::type);

  int i = 0;
  for (auto p_it = points.begin(), p_end = points.end();
       p_it != p_end; ++p_it, ++i) {
    A->at<T>(i, 0) = p_it->x_*p_it->x_;
    A->at<T>(i, 1) = p_it->x_*p_it->y_,
    A->at<T>(i, 2) = p_it->x_*p_it->z_,
    A->at<T>(i, 3) = p_it->y_*p_it->y_,
    A->at<T>(i, 4) = p_it->y_*p_it->z_,
    A->at<T>(i, 5) = p_it->z_*p_it->z_,
    A->at<T>(i, 6) = p_it->x_,
    A->at<T>(i, 7) = p_it->y_,
    A->at<T>(i, 8) = p_it->z_,
    A->at<T>(i, 9) = T(1.0);

    B->at<T>(i) = T(0.0);
  }

  for (auto p_it = aux_points.begin(), p_end = aux_points.end();
       p_it != p_end; ++p_it, ++i) {
    A->at<T>(i, 0) = p_it->x_*p_it->x_;
    A->at<T>(i, 1) = p_it->x_*p_it->y_,
    A->at<T>(i, 2) = p_it->x_*p_it->z_,
    A->at<T>(i, 3) = p_it->y_*p_it->y_,
    A->at<T>(i, 4) = p_it->y_*p_it->z_,
    A->at<T>(i, 5) = p_it->z_*p_it->z_,
    A->at<T>(i, 6) = p_it->x_,
    A->at<T>(i, 7) = p_it->y_,
    A->at<T>(i, 8) = p_it->z_,
    A->at<T>(i, 9) = T(1.0);

    B->at<T>(i) = di[i - n_points];
  }
}

/*
 * @name ComputeWeights
 * @fn void ComputeWeights(const std::vector<LTS5::Vector3<T> >& points,
 *                         const int& aux_pts_size, cv::Mat* weights)
 * @brief Compute the weights diagonal matrix
 * @param[in]     points       Points of the mesh within the support ball
 * @param[in]     aux_pts_size Number of aux points (w = 1/n for these)
 * @param[out]    weights      Diagonal opencv Mat with the weights
 */
template<typename T>
void Quadric<T>::ComputeWeights(const std::vector<LTS5::Vector3<T> >& points,
                                const int& aux_pts_size, cv::Mat* weights) {
  int n_points = static_cast<int>(points.size());
  weights->create(n_points + aux_pts_size, 1, cv::DataType<T>::type);
  (*weights) = cv::Mat::eye(n_points + aux_pts_size, 1, cv::DataType<T>::type);

  Sw_ = 0.0;
  int i = 0;
  for (auto p_it = points.begin(); p_it != points.end(); ++p_it, ++i) {
    LTS5::Vector3<T> vec_xc = (*p_it) - center_;
    T r_i = vec_xc.Norm() / radius_;
    T w_i = w_(r_i);

    weights->at<T>(i) = w_i;
    Sw_ += w_i;
  }

  (*weights) /= Sw_;

  for (int j = 0; j < aux_pts_size; ++j) {
    weights->at<T>(n_points + j) = T(1.0 / aux_pts_size);
  }
}

#pragma mark -
#pragma mark Declaration

/** Float Quadric */
template class Quadric<float>;
/** Double Quadric */
template class Quadric<double>;

} // namespace LTS5
