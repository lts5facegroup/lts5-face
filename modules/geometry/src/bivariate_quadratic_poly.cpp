/**
 *  @file   bivariate_quadratic_poly.cpp
 *  @brief  Bivariate Quadratic Polynomial approximation implementation
 *
 *  @author Gabriel Cuendet
 *  @date   20/04/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include "lts5/geometry/bivariate_quadratic_poly.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  Clone
 * @fn  BivariatePoly* Clone(void);
 * @brief Deep copy method
 */
template<typename T>
BivariatePoly<T>* BivariatePoly<T>::Clone(void) {
  BivariatePoly* deep_cpy = new BivariatePoly();
  deep_cpy->coefficients_ = this->coefficients_;
  deep_cpy->w_ = this->w_;
  deep_cpy->Sw_ = this->Sw_;
  deep_cpy->center_ = this->center_;
  deep_cpy->mean_normal_ = this->mean_normal_;
  deep_cpy->basis_change_ = this->basis_change_;
  deep_cpy->radius_ = this->radius_;

  return deep_cpy;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Fit
 * @fn  void Fit(const std::vector<LTS5::Vector3<T> >& points,
 *               const std::vector<LTS5::Vector3<T> >& normals,
 *               const AABB& bbox)
 * @brief Fit the quadratic poly to the input points using least squares
 * @param[in] points   Vertices within the support radius of the cell
 * @param[in] normals  Normals of the vertices
 * @param[in] bbox     Bounding box of the current cell
 * @return    error    fitting error
 */
template<typename T>
T BivariatePoly<T>::Fit(const std::vector<LTS5::Vector3<T> >& points,
                        const std::vector<LTS5::Vector3<T> >& normals,
                        const AABB<T>& bbox) {
  // By default, the support radius R is equal to 0.75 times the main diagonal
  // of the bounding box: R = 0.75*d (cf. Eq.(5))
  T d = (bbox.max_ - bbox.min_).Norm();
  this->radius_ = 0.75 * d;
  return this->Fit(points, normals, bbox, radius_);
}

/*
 * @name  Fit
 * @fn  void Fit(const std::vector<LTS5::Vector3<T> >& points,
 *               const std::vector<LTS5::Vector3<T> >& normals,
 *               const AABB& bbox)
 * @brief Fit the quadratic poly to the input points using least squares
 * @param[in] points   Vertices within the support radius of the cell
 * @param[in] normals  Normals of the vertices
 * @param[in] bbox     Bounding box of the current cell
 * @param[in] R        Radius of the support sphere (used for the weights)
 * @return    error    fitting error
 */
template<typename T>
T BivariatePoly<T>::Fit(const std::vector<LTS5::Vector3<T> >& points,
                        const std::vector<LTS5::Vector3<T> >& normals,
                        const AABB<T>& bbox, const T& R) {
  radius_ = R;
  center_ = bbox.center_;

  ComputeMeanNormal(points, normals);
  ComputeBasisChange();

  cv::Mat A;
  cv::Mat W = cv::Mat::ones(static_cast<int>(points.size()), 1, cv::DataType<T>::type);
  cv::Mat V;
  cv::Mat q;

  this->ComputeWeights(points, &W);
  BuildLinearSystem(points, &A, &V);

  cv::Mat AtW(6, static_cast<int>(points.size()), cv::DataType<T>::type);
  for (int i = 0; i < W.rows; ++i) {
    for (int j = 0; j < 6; ++j) {
      AtW.at<T>(j, i) =  A.at<T>(i, j) * W.at<T>(i);
    }
  }

  // Solve
  cv::Mat PtWP = AtW * A;
  cv::Mat PtWV = AtW * V;
  typename LTS5::LinearAlgebra<T>::LinearSolver linear_least_squares;
  linear_least_squares.Solve(PtWP, PtWV, &q);

  // Update coefficients_
  coefficients_ = std::vector<T>(6);
  coefficients_[0] = q.at<T>(0);
  coefficients_[1] = q.at<T>(1);
  coefficients_[2] = q.at<T>(2);
  coefficients_[3] = q.at<T>(3);
  coefficients_[4] = q.at<T>(4);
  coefficients_[5] = q.at<T>(5);

  /* DEBUG PURPOSE
  std::cout << "Coefficients : ";
  for (int i=0; i<coefficients_.size(); ++i) {
    std::cout << coefficients_[i] << "   ";
  }
  std::cout << std::endl;
   */

  return this->Epsilon(points);
}

/*
 * @name  Evaluate
 * @fn  void Evaluate(const LTS5::Vector3<T>& x)
 * @brief Evalute the value of x, given the quadratic approximation
 * @param[in]  x       Point to evaluate
 * @param[out] SwQ     Sum Qi(x)wi(x)
 * @param[out] Sw      Sum wi(x)
 */
template<typename T>
void BivariatePoly<T>::Evaluate(const LTS5::Vector3<T>& x, T* SwQ, T* Sw) {
  // Compute the weight w_i(x), compute the sum of weight Sw (they are equal
  // when there is only one approximation region), and eval Q_i(x)
  // multiply Q_i(x) by w_i(x), that is SwQ
  // At the end, divide by Sw!
  LTS5::Vector3<T> vec_xc = x - center_;
  T r_i = vec_xc.Norm() / radius_;
  T w_i = w_(r_i);

  if (w_i == 0.0)
    return;

  *Sw += w_i;

  T Q_i = EvaluateQ(x);
  *SwQ += w_i * Q_i;
}

/*
 * @name  EvaluateSquare
 * @fn  virtual void EvaluateSquare(const LTS5::Vector3<T>& x, T* SwQ2,
 *                                  T* Sw) = 0
 * @brief Evalute the squared value of x, given the approximation
 * @param[in]  x     Point to evaluate
 * @param[out] SwQ2  Sum Qi(x)^2 wi(x)
 * @param[out] Sw    Sum wi(x)
 */
template<typename T>
void BivariatePoly<T>::EvaluateSquare(const LTS5::Vector3<T>& x, T* SwQ2,
                                      T* Sw) {
  // Compute the weight w_i(x), compute the sum of weight Sw (they are equal
  // when there is only one approximation region), and eval Q_i(x)
  // multiply Q_i(x) by w_i(x), that is SwQ
  // At the end, divide by Sw!
  LTS5::Vector3<T> vec_xc = x - center_;
  T r_i = vec_xc.Norm() / radius_;
  T w_i = w_(r_i);

  if (w_i == 0.0)
    return;

  *Sw += w_i;

  T Q_i = EvaluateQ(x);
  *SwQ2 += w_i * Q_i * Q_i;
}

/*
 * @name  EvaluateGrad
 * @fn  void EvaluateGrad(const LTS5::Vector3<T>& x, LTS5::Vector3<T>* SwGradQ,
 *                        T* Sw)
 * @brief Evaluate the value of the gradient of Q at x
 * @param[in]  x        Point to evaluate
 * @param[out] SwGradQ  Sum Grad(Qi)(x)wi(x)
 * @param[out] Sw       Sum wi(x)
 */
template<typename T>
void BivariatePoly<T>::EvaluateGrad(const LTS5::Vector3<T>& x,
                                    LTS5::Vector3<T>* SwGradQ, T* Sw) {
  // Compute the weight w_i(x), compute the sum of weight Sw (they are equal
  // when there is only one approximation region), and eval Grad(Q_i)(x)
  // multiply Grad(Q_i)(x) by w_i(x), that is SwGradQ
  // At the end, divide by Sw!
  LTS5::Vector3<T> vec_xc = x - center_;
  T r_i = vec_xc.Norm() / radius_;
  T w_i = w_(r_i);

  if (w_i == 0.0)
    return;

  *Sw += w_i;

  LTS5::Vector3<T> GradQ_i = EvaluateGradQ(x);
  *SwGradQ += w_i * GradQ_i;
}

/*
 * @name  EvaluateGradSquare
 * @fn  virtual void EvaluateGradSquare(const LTS5::Vector3<T>& x,
 *                                      LTS5::Vector3<T>* SwGradQ2, T* Sw) = 0
 * @brief Evaluate the gradient of Q^2 at x
 * @param[in]  x         Point to evaluate
 * @param[out] SwGradQ2  Sum Grad(Q_i^2)(x) wi(x)
 * @param[out] Sw        Sum wi(x)
 */
template<typename T>
void BivariatePoly<T>::EvaluateGradSquare(const LTS5::Vector3<T>& x,
                                          LTS5::Vector3<T>* SwGradQ2, T* Sw) {
  // Compute the weight w_i(x), compute the sum of weight Sw (they are equal
  // when there is only one approximation region), and eval Grad(Q_i)(x)
  // multiply Grad(Q_i)(x) by w_i(x), that is SwGradQ
  // At the end, divide by Sw!
  LTS5::Vector3<T> vec_xc = x - center_;
  T r_i = vec_xc.Norm() / radius_;
  T w_i = w_(r_i);

  if (w_i == 0.0)
    return;

  *Sw += w_i;

  LTS5::Vector3<T> GradQ_i = EvaluateGradQ(x);
  T Q_i = EvaluateQ(x);
  *SwGradQ2 += w_i * T(2.0) * Q_i * GradQ_i;
}

/*
 * @name  EvaluateGradQ
 * @fn  void EvaluateGradQ(const LTS5::Vector3<T>& x,
 *                        LTS5::Vector3<T>* grad)
 * @brief Evalute the gradient of x, given the quadratic approximation
 * @param[in]  x       Point to evaluate
 * @param[out] grad    Gradient of the quadric evaluated at x
 */
template<typename T>
void BivariatePoly<T>::EvaluateGradQ(const LTS5::Vector3<T>& x,
                                    LTS5::Vector3<T>* grad) {
  // Q(x) = B(Q(u)) with B = Bx
  // Q(u) = w - (Au^2 + 2Buv + Cv^2 + Du + Ev + F)
  // dQ/dx = dQ/du * du/dx = B * dQ/du
  // dQ/du = -2Au - 2Bv - D
  // dQ/dv = -2Cv - 2Bu - E
  // dQ/dw = 1
  assert(!coefficients_.empty());

  // @TODO: (Gabriel) Compute this only once and for all
  LTS5::Vector3<T> uvw = ComputeLocalCoords(x);
  LTS5::Vector3<T> grad_uvw(
  -2 * coefficients_[0] * uvw.x_ - 2 * coefficients_[1] * uvw.y_ - coefficients_[3],
  -2 * coefficients_[2] * uvw.y_ - 2 * coefficients_[1] * uvw.x_ - coefficients_[4],
  1);

  grad->x_ = basis_change_.at<T>(0, 0) * grad_uvw.x_ +
             basis_change_.at<T>(0, 1) * grad_uvw.y_ +
             basis_change_.at<T>(0, 2) * grad_uvw.z_;
  grad->y_ = basis_change_.at<T>(1, 0) * grad_uvw.x_ +
             basis_change_.at<T>(1, 1) * grad_uvw.y_ +
             basis_change_.at<T>(1, 2) * grad_uvw.z_;
  grad->z_ = basis_change_.at<T>(2, 0) * grad_uvw.x_ +
             basis_change_.at<T>(2, 1) * grad_uvw.y_ +
             basis_change_.at<T>(2, 2) * grad_uvw.z_;
}

/*
 * @name  EvaluateGradQ
 * @fn  LTS5::Vector3<T> EvaluateGradQ(const LTS5::Vector3<T>& x)
 * @brief Evalute the gradient of x, given the quadratic approximation
 * @param[in]  x       Point to evaluate
 * @return     grad    Gradient of the quadric evaluated at x
 */
template<typename T>
LTS5::Vector3<T> BivariatePoly<T>::EvaluateGradQ(const LTS5::Vector3<T>& x) {
  LTS5::Vector3<T> grad;
  EvaluateGradQ(x, &grad);
  return grad;
}

/*
 * @name  EvaluateQ
 * @fn  T EvaluateQ(const LTS5::Vector3<T>& x)
 * @brief Evalute the value of x, given the quadric approximation
 * @param[in]  x       Point to evaluate
 * @return     val     Value of the quadric evaluated at x
 */
template<typename T>
T BivariatePoly<T>::EvaluateQ(const LTS5::Vector3<T>& x) {
  assert(!coefficients_.empty());

  // @TODO: (Gabriel) Compute this only once and for all
  LTS5::Vector3<T> uvw = ComputeLocalCoords(x);
  T res = uvw.z_ - (coefficients_[0] * uvw.x_ * uvw.x_
                    + 2 * coefficients_[1] * uvw.x_ * uvw.y_
                    + coefficients_[2] * uvw.y_ * uvw.y_
                    + coefficients_[3] * uvw.x_
                    + coefficients_[4] * uvw.y_
                    + coefficients_[5]);
  return res;
}

#pragma mark -
#pragma mark Private methods

/*
 * @name ComputeWeights
 * @fn void ComputeWeights(const std::vector<LTS5::Vector3<T> >& points,
 *                         cv::Mat* weights)
 * @brief Compute the weights vector
 * @param[in]     points       Points of the mesh within the support ball
 * @param[out]    weights      Nx1 opencv Mat with the weights
 */
template<typename T>
void BivariatePoly<T>::ComputeWeights(const std::vector<LTS5::Vector3<T> >& points,
                                      cv::Mat* weights) {
  int n_points = static_cast<int>(points.size());
  weights->create(n_points, 1, cv::DataType<T>::type);
  (*weights) = cv::Mat::ones(n_points, 1, cv::DataType<T>::type);

  T Sw = 0.0;
  int i = 0;
  for (auto p_it = points.begin(); p_it != points.end(); ++p_it, ++i) {
    LTS5::Vector3<T> vec_xc = (*p_it) - center_;
    T r_i = vec_xc.Norm() / radius_;
    T w_i = w_(r_i);

    weights->at<T>(i) = w_i;
    Sw += w_i;
  }

  (*weights) /= Sw;
}

/*
 * @name ComputeLocalCoords
 * @fn std::vector<T> ComputeLocalCoords(const LTS5::Vector3<T>& x)
 * @brief Compute the local coordinates u, v, w
 * @param[in]     x       Point for which to compute u,v,w coordinates
 * @return        coord   vector of local coordinates
 */
template<typename T>
LTS5::Vector3<T> BivariatePoly<T>::ComputeLocalCoords(const LTS5::Vector3<T>& x) {
  if (basis_change_.empty()) {
    std::cout << "[BivariatePoly<T>::ComputeLocalCoords] ERROR:" <<
    "Change of basis matrix has not been defined yet!" << std::endl;
    return x;
  }

  LTS5::Vector3<T> x_p = x-center_;

  return LTS5::Vector3<T>(
    basis_change_.at<T>(0,0)*x_p.x_ + basis_change_.at<T>(0,1)*x_p.y_ + basis_change_.at<T>(0,2)*x_p.z_,
    basis_change_.at<T>(1,0)*x_p.x_ + basis_change_.at<T>(1,1)*x_p.y_ + basis_change_.at<T>(1,2)*x_p.z_,
    basis_change_.at<T>(2,0)*x_p.x_ + basis_change_.at<T>(2,1)*x_p.y_ + basis_change_.at<T>(2,2)*x_p.z_);
}

/*
 * @name ComputeMeanNormal
 * @fn void ComputeMeanNormal(const std::vector<LTS5::Vector3<T>& points,
 *                            const std::vector<LTS5::Vector3<T>& normals)
 * @brief Compute the mean normal from the points and normals of the cell
 * @param[in]  points   vector of points within the support ball
 * @param[in]  normals  vector of their normals
 */
template<typename T>
void BivariatePoly<T>::ComputeMeanNormal(const std::vector<LTS5::Vector3<T> >& points,
                                         const std::vector<LTS5::Vector3<T> >& normals) {
  std::vector<T> weights;
  T Sw = 0.0;
  for (auto p_it = points.begin(); p_it != points.end(); ++p_it) {
    LTS5::Vector3<T> vec_xc = (*p_it) - center_;
    T r = vec_xc.Norm() / radius_;
    T w_i = w_(r);
    Sw += w_i;
    weights.push_back(w_i);
  }

  mean_normal_ = LTS5::Vector3<T>();  // Reset previous value...
  for (int i = 0; i < normals.size(); ++i) {
    mean_normal_ += weights[i] * normals[i];
  }

  mean_normal_ /= Sw;

  // DEBUG PURPOSE
  //std::cout << "|| n || = " << mean_normal_.Norm() << std::endl;
}

/*
 * @name  ComputeBasisChange
 * @fn  void ComputeBasisChange(void)
 * @brief Compute the basis change from global coordinates to local coordinates
 */
template<typename T>
void BivariatePoly<T>::ComputeBasisChange(void) {
  basis_change_.create(3, 3, cv::DataType<T>::type);
  // Basis vector are stored along the ROWS! (or Rotation matrix is TRANSPOSED)
  // Here, I don t transpose, I store directly along the rows

  // e_w is the mean normal
  if (std::abs(mean_normal_.Norm() - 1.0) > 10 * std::numeric_limits<T>::epsilon()) {
    mean_normal_.Normalize();
  }
  basis_change_.at<T>(2, 0) = mean_normal_.x_;
  basis_change_.at<T>(2, 1) = mean_normal_.y_;
  basis_change_.at<T>(2, 2) = mean_normal_.z_;

  // Compute e_x, e_y and e_z projections onto the plane
  LTS5::Vector3<T> proj_ex(1.0, 0.0, 0.0);
  LTS5::Vector3<T> proj_ey(0.0, 1.0, 0.0);
  LTS5::Vector3<T> proj_ez(0.0, 0.0, 1.0);

  proj_ex -= mean_normal_ * ((proj_ex * mean_normal_) / (mean_normal_.Norm()*mean_normal_.Norm()));
  proj_ey -= mean_normal_ * ((proj_ey * mean_normal_) / (mean_normal_.Norm()*mean_normal_.Norm()));
  proj_ez -= mean_normal_ * ((proj_ez * mean_normal_) / (mean_normal_.Norm()*mean_normal_.Norm()));

  LTS5::Vector3<T> eu;
  // Keep the largest as e_u
  if (proj_ex.Norm() >= proj_ey.Norm() && proj_ex.Norm() >= proj_ez.Norm()) {
    proj_ex.Normalize();
    eu = proj_ex;
    basis_change_.at<T>(0, 0) = proj_ex.x_;
    basis_change_.at<T>(0, 1) = proj_ex.y_;
    basis_change_.at<T>(0, 2) = proj_ex.z_;
  } else if (proj_ey.Norm() >= proj_ex.Norm() && proj_ey.Norm() >= proj_ez.Norm()) {
    proj_ey.Normalize();
    eu = proj_ey;
    basis_change_.at<T>(0, 0) = proj_ey.x_;
    basis_change_.at<T>(0, 1) = proj_ey.y_;
    basis_change_.at<T>(0, 2) = proj_ey.z_;
  } else {
    proj_ez.Normalize();
    eu = proj_ez;
    basis_change_.at<T>(0, 0) = proj_ez.x_;
    basis_change_.at<T>(0, 1) = proj_ez.y_;
    basis_change_.at<T>(0, 2) = proj_ez.z_;
  }

  // Compute cross product for the third one
  LTS5::Vector3<T> ev = mean_normal_ ^ eu;
  ev.Normalize();
  basis_change_.at<T>(1, 0) = ev.x_;
  basis_change_.at<T>(1, 1) = ev.y_;
  basis_change_.at<T>(1, 2) = ev.z_;
}

/*
 * @name BuildLinearSystem
 * @fn void BuildLinearSystem(const std::vector<LTS5::Vector3<T> >& points,
 *                            cv::Mat* A, cv::Mat V*)
 * @brief From the points on the surface build the linear system Ax=V
 * @param[in]     points       Points of the mesh within the support ball
 * @param[out]    A            Local coordinates matrix
 * @param[out]    V            Local w coordinates of the points
 */
template<typename T>
void BivariatePoly<T>::BuildLinearSystem(const std::vector<LTS5::Vector3<T> >& points,
                                         cv::Mat* A, cv::Mat* V) {
  A->create(static_cast<int>(points.size()), 6, cv::DataType<T>::type);
  V->create(static_cast<int>(points.size()), 1, cv::DataType<T>::type);

  int i=0;
  for (auto p_it = points.begin(); p_it != points.end(); ++p_it, ++i) {
    LTS5::Vector3<T> uvw = ComputeLocalCoords(*p_it);
    A->at<T>(i, 0) = uvw.x_ * uvw.x_;  // u^2
    A->at<T>(i, 1) = uvw.x_ * uvw.y_;  // uv
    A->at<T>(i, 2) = uvw.y_ * uvw.y_;  // v^2
    A->at<T>(i, 3) = uvw.x_;  // u
    A->at<T>(i, 4) = uvw.y_;  // v
    A->at<T>(i, 5) = T(1.0);  // 1

    V->at<T>(i) = uvw.z_;
  }
}

#pragma mark -
#pragma mark Declaration

  /** Float BivariatePoly */
  template class BivariatePoly<float>;
  /** Double BivariatePoly */
  template class BivariatePoly<double>;

} // namespace LTS5
