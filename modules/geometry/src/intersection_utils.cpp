/**
 *  @file   intersection_utils.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   02/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <cassert>

#include "lts5/geometry/intersection_utils.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name IntersectMeshWithSegment
 *  @fn bool IntersectMeshWithSegment(const Mesh<T>& mesh,
 *                                    const Vertex& p,
 *                                    const Vertex& q)
 *  @brief  Test if the mesh object intersect with a given segment PQ
 *  @param[in]  mesh  Mesh to compute intersection with
 *  @param[in]  p     Starting point of the segment to test against
 *  @param[in]  q     Stoping point of the segment to test against
 *  @return True if segment PQ intersect with mesh, False otherwise
 */
template<typename T>
bool IntersectMeshWithSegment(const Mesh<T>& mesh,
                              const typename Mesh<T>::Vertex& p,
                              const typename Mesh<T>::Vertex& q) {
  using Triangle = typename Mesh<T>::Triangle;
  using Vertex = typename Mesh<T>::Vertex;
  using Edge = typename Mesh<T>::Edge;

  bool intersect = false;
  const std::vector<Triangle>& tri_list = mesh.get_triangle();
  const std::vector<Vertex>& vertex = mesh.get_vertex();
  const unsigned int n_tri = static_cast<int>(tri_list.size());
  const Edge PQ = q - p;
  for (int i = 0; i < n_tri; ++i) {
    // Access triangle information
    const Triangle& tri = tri_list[i];
    const Vertex& A = vertex[tri.x_];
    const Vertex& B = vertex[tri.y_];
    const Vertex& C = vertex[tri.z_];
    // Define edges
    const Edge PA = A - p;
    const Edge PB = B - p;
    const Edge PC = C - p;
    // Test if pq is inside the edges bc, ca and ab. Done by testing
    // that the signed tetrahedral volumes, computed using scalar triple
    // products, have all the same sign
    Vector3<T> m = PQ ^ PC;
    const T u = PB * m;
    const T v = -(PA * m);
    // u, v, w MUST have the same sign, early check to avoid unnecessary
    // computations
    if((u >= T(0.0)) != (v >= T(0.0)) ) {
      continue;
    }
    // Continue computation since both u,v have same sign
    const T w = Edge::ScalarTriple(PQ, PB, PA);
    if((u >= T(0.0)) == (w >= T(0.0)) ) {
      intersect = true;
      break;
    }
  }
  return intersect;
}

/*
 *  @name IntersectTriWithSegment
 *  @fn bool LTS5_EXPORTS IntersectTriWithSegment(const Vertex<T>& p,
                                                  const Vertex<T>& q,
                                                  const Vertex<T>& A,
                                                  const Vertex<T>& B,
                                                  const Vertex<T>& C,
                                                  const bool& cull_face,
                                                  const T& tol,
                                                  T* t)
 *  @brief  Test if the triangle ABC intersects with a given segment PQ
 *  @ingroup geometry
 *  @param[in]  p           Starting point of the segment to test against
 *  @param[in]  q           Stoping point of the segment to test against
 *  @param[in]  A           First vertex of the triangle
 *  @param[in]  B           Second vertex of the triangle
 *  @param[in]  C           Third vertex of the triangle
 *  @param[in]  cull_face   If `True`, do not consider back facing triangles
 *  @param[in]  tol         Tolerance to use when computing barycentric
 *                          coordinates
 *  @param[out] t           position along the segment PQ, t in [0, 1]
 *  @return True if segment intersect with the triangle, False otherwise
 */
template<typename T>
bool IntersectTriWithSegment(const Vertex<T>& p,
                             const Vertex<T>& q,
                             const Vertex<T>& A,
                             const Vertex<T>& B,
                             const Vertex<T>& C,
                             const bool& cull_face,
                             const T& tol,
                             T* t) {
  using Vertex = typename Mesh<T>::Vertex;
  using Normal = typename Mesh<T>::Normal;
  using Edge = typename Mesh<T>::Edge;
  // Local vertex
  Vertex pl = p;
  Vertex ql = q;
  // Define edges
  const Edge AB = B - A;
  const Edge AC = C - A;
  Edge QP = pl - ql;
  // Compute triangle normal. Can be precalculated or cached if
  // intersecting multiple segments against the same triangle
  const Normal n = AB ^ AC;
  // Compute denominator d. If d <= 0, segment is parallel to or points
  // away from triangle, so exit early
  T d = QP * n;
  bool revert = false;
  if (d < T(0.0) && !cull_face) {
    // Revert segment to make it face triangle
    std::swap(pl, ql);
    QP = pl - ql;
    d *= T(-1.0);
    revert = true;
  }
  if (d <= T(0.0)) { return false; }
  // Compute intersection t value of pq with plane of triangle. A ray
  // intersects iff 0 <= t. Segment intersects iff 0 <= t <= 1. Delay
  // dividing by d until intersection has been found to pierce triangle
  const Edge AP = pl - A;
  T tt = AP * n;
  if (tt < T(0.0)) { return false; }
  if (tt > d) { return false;}  // For segment; exclude this code line for a ray
  // Compute barycentric coordinate components and test if within bounds
  const Vector3<T> E = QP ^ AP;
  T v = AC * E;
  if (v < -tol || (v - tol) > d) { return false; }
  T w = -(AB * E);
  if (w < -tol || (v + w - tol) > d) { return false; }
  *t = revert ? T(1.0) - tt/d : tt/d;
  // Reach intersection
  return true;
}

/*
 *  @name IntersectTriWithSegment
 *  @fn bool IntersectTriWithSegment(const Mesh<T>::Vertex& p,
                                     const Mesh<T>::Vertex& q,
                                     const Mesh<T>::Vertex& A,
                                     const Mesh<T>::Vertex& B,
                                     const Mesh<T>::Vertex& C,
                                     T* t)
 *  @brief  Test if the triangle ABC intersects with a given segment PQ
 *  @param[in]  p       Starting point of the segment to test against
 *  @param[in]  q       Stoping point of the segment to test against
 *  @param[in]  A       First vertex of the triangle
 *  @param[in]  B       Second vertex of the triangle
 *  @param[in]  C       Third vertex of the triangle
 *  @param[out] t       position along the segment PQ, t in [0, 1]
 *  @return True if segment intersect with the triangle, False otherwise
 */
template<typename T>
bool IntersectTriWithSegment(const typename Mesh<T>::Vertex& p,
                             const typename Mesh<T>::Vertex& q,
                             const typename Mesh<T>::Vertex& A,
                             const typename Mesh<T>::Vertex& B,
                             const typename Mesh<T>::Vertex& C,
                             T* t) {
  return IntersectTriWithSegment(p,
                                 q,
                                 A,
                                 B,
                                 C,
                                 true,
                                 T(2.0) * std::numeric_limits<T>::epsilon(),
                                 t);
}

/*
 * @name  IntersectTriWithLine
 * @fn  bool IntersectTriWithLine(const Vector3<T>& p,
                                  const Vector3<T>& d,
                                  const typename Mesh<T>::Vertex& A,
                                  const typename Mesh<T>::Vertex& B,
                                  const typename Mesh<T>::Vertex& C,
                                  T* lambda)
 * @brief Test if the triangle ABC intersects with a given line p + lamda * d
 * @param[in]  p       A point on the line to test against
 * @param[in]  d       Unit direction vector of the line to test against
 * @param[in]  A       First vertex of the triangle
 * @param[in]  B       Second vertex of the triangle
 * @param[in]  C       Third vertex of the triangle
 * @param[out] t       position along the line, p + lambda * d
 * @return True if line intersects with the triangle, false otherwise
 */
template<typename T>
bool IntersectTriWithLine(const Vector3<T>& p,
                          const Vector3<T>& d,
                          const typename Mesh<T>::Vertex& A,
                          const typename Mesh<T>::Vertex& B,
                          const typename Mesh<T>::Vertex& C,
                          T* lambda) {
  assert((std::abs(d.Norm() - 1.0)) < std::numeric_limits<float>::epsilon());
  using Normal = typename Mesh<T>::Normal;
  using Edge = typename Mesh<T>::Edge;
  // Define edges
  const Edge AB = B - A;
  const Edge AC = C - A;
  // Compute triangle normal. Can be precalculated or cached if
  // intersecting multiple segments against the same triangle
  Normal n = AB ^ AC;
  n.Normalize();
  // Compute denominator d. If d == 0, segment is parallel to triangle, so exit early
  T denominator = d * n;
  if (denominator == T(0.0)) { return false; }
  // Compute intersection t value of pq with plane of triangle. A ray
  // intersects iff 0 <= t. Segment intersects iff 0 <= t <= 1. Delay
  // dividing by d until intersection has been found to pierce triangle
  const Edge PA = A - p;
  T tt = PA * n;
  //if (tt < T(0.0)) { return false; }
  //if (tt > d) { return false;}  // For segment; exclude this code line for a ray
  // We know it intersects the plane, let's see if the intersection is inside
  // the triangle
  *lambda = tt/denominator;
  // Compute barycentric coordinate components and test if within bounds
  const Vector3<T> PI = p + (*lambda)*d;

  const Edge w = PI - A;
  T denom = (AB*AC) * (AB*AC) - (AB*AB) * (AC*AC);
  T s = ((AB*AC) * (w*AC) - (AC*AC) * (w*AB)) / denom;
  if (s < T(0.0)) {return false;}
  T t = ((AB*AC) * (w*AB) - (AB*AB) * (w*AC)) / denom;
  if (t < T(0.0)) {return false;}
  if (s + t > T(1.0)) {return false;}
  // Reach intersection
  return true;
}

/*
 * @name
 * @fn void ClosestPointToRay(const Vector3<T>& p,
                              const Vector3<T>& d,
                              const std::vector<typename Mesh<T>::Vertex>& pts,
                              size_t* idx)
 * @brief   Find the closest point to a given ray : r = p + lambda * d
 * @tparam T    Data type
 * @param[in] p     Origin of the ray
 * @param[in] d     Ray's unit direction
 * @param[in] pts   List of points to test against
 * @param[in] idx   Index of the closest point in the list of \p pts.
 */
template<typename T>
void ClosestPointToRay(const Vector3<T>& p,
                       const Vector3<T>& d,
                       const std::vector<typename Mesh<T>::Vertex>& pts,
                       size_t* idx) {
  if (!pts.empty()) {
    size_t min_idx = 0;
    T min_dist = std::numeric_limits<T>::max();
    for (size_t k = 0; k < pts.size(); ++k) {
      auto r = pts[k] - p;
      auto dist = r - (d * (d * r));
      const T dn = dist.Norm();

      dist.Normalize();
      assert(std::abs(dist * d) - T(1.0) < 0.01);

      if (dn < min_dist) {
        min_dist = dn;
        min_idx = k;
      }
    }
    *idx = min_idx;
  }
}

/*
 * @name    PointInTriangle
 * @fn  bool PointInTriangle(const Vector3<T>& p,
                             const typename Mesh<T>::Vertex& A,
                             const typename Mesh<T>::Vertex& B,
                             const typename Mesh<T>::Vertex& C)
 * @brief   Compute if point \p p is inside triangle ABC
 * @param[in] p     Query point
 * @param[in] A     Triangle point A
 * @param[in] B     Triangle point B
 * @param[in] C     Triangle point C
 * @return True if inside, false otherwise
 */
template<typename T>
bool PointInTriangle(const Vector3<T>& p,
                     const typename Mesh<T>::Vertex& A,
                     const typename Mesh<T>::Vertex& B,
                     const typename Mesh<T>::Vertex& C) {
  // Bring into local reference
  const auto v0 = C - A;
  const auto v1 = B - A;
  const auto v2 = p - A;
  // Compute dot products
  const T dot00 = v0 * v0;
  const T dot01 = v0 * v1;
  const T dot02 = v0 * v2;
  const T dot11 = v1 * v1;
  const T dot12 = v1 * v2;
  // Compute barycentric coordinate
  const T invDenom = T(1.0) / (dot00 * dot11 - dot01 * dot01);
  const T u = (dot11 * dot02 - dot01 * dot12) * invDenom;
  const T v = (dot00 * dot12 - dot01 * dot02) * invDenom;
  // Check if point is in triangle
  return (u >= 0) && (v >= 0) && (u + v <= T(1.0));
}

#pragma mark -
#pragma mark Declaration

/** IntersectMeshWithSegment - Float */
template bool IntersectMeshWithSegment(const Mesh<float>& mesh,
                                       const Mesh<float>::Vertex& p,
                                       const Mesh<float>::Vertex& q);
/** IntersectMeshWithSegment - Double */
template bool IntersectMeshWithSegment(const Mesh<double>& mesh,
                                       const Mesh<double>::Vertex& p,
                                       const Mesh<double>::Vertex& q);
/** IntersectTriWithSegment - Float */
template bool IntersectTriWithSegment(const Mesh<float>::Vertex& p,
                                      const Mesh<float>::Vertex& q,
                                      const Mesh<float>::Vertex& A,
                                      const Mesh<float>::Vertex& B,
                                      const Mesh<float>::Vertex& C,
                                      const bool&,
                                      const float&,
                                      float* t);
/** IntersectTriWithSegment - Double */
template bool IntersectTriWithSegment(const Mesh<double>::Vertex& p,
                                      const Mesh<double>::Vertex& q,
                                      const Mesh<double>::Vertex& A,
                                      const Mesh<double>::Vertex& B,
                                      const Mesh<double>::Vertex& C,
                                      const bool&,
                                      const double&,
                                      double* t);
/** IntersectTriWithSegment - Float */
template bool IntersectTriWithSegment(const Mesh<float>::Vertex& p,
                                      const Mesh<float>::Vertex& q,
                                      const Mesh<float>::Vertex& A,
                                      const Mesh<float>::Vertex& B,
                                      const Mesh<float>::Vertex& C,
                                      float* t);
/** IntersectTriWithSegment - Double */
template bool IntersectTriWithSegment(const Mesh<double>::Vertex& p,
                                      const Mesh<double>::Vertex& q,
                                      const Mesh<double>::Vertex& A,
                                      const Mesh<double>::Vertex& B,
                                      const Mesh<double>::Vertex& C,
                                      double* t);
/** IntersectTriWithLine - Float */
template bool IntersectTriWithLine(const Vector3<float>& p,
                                   const Vector3<float>& d,
                                   const Mesh<float>::Vertex& A,
                                   const Mesh<float>::Vertex& B,
                                   const Mesh<float>::Vertex& C,
                                   float* lambda);
/** IntersectTriWithLine - Double */
template bool IntersectTriWithLine(const Vector3<double>& p,
                                   const Vector3<double>& d,
                                   const Mesh<double>::Vertex& A,
                                   const Mesh<double>::Vertex& B,
                                   const Mesh<double>::Vertex& C,
                                   double* lambda);
/** ClosestPointToRay - Float */
template
void ClosestPointToRay(const Vector3<float>& p,
                       const Vector3<float>& d,
                       const std::vector<Mesh<float>::Vertex>& pts,
                       size_t* idx);
/** ClosestPointToRay - Double */
template
void ClosestPointToRay(const Vector3<double>& p,
                       const Vector3<double>& d,
                       const std::vector<Mesh<double>::Vertex>& pts,
                       size_t* idx);
/** Point inside triangle - Float */
template bool PointInTriangle(const Vector3<float>& p,
                              const Mesh<float>::Vertex& A,
                              const Mesh<float>::Vertex& B,
                              const Mesh<float>::Vertex& C);
/** Point inside triangle - Double */
template bool PointInTriangle(const Vector3<double>& p,
                              const Mesh<double>::Vertex& A,
                              const Mesh<double>::Vertex& B,
                              const Mesh<double>::Vertex& C);
}  // namepsace LTS5
