/**
 *  @file   isotropic_clustering_metric.cpp
 *  @brief  Metric for uniform mesh clustering
 *          Used in uniform remeshing functionality
 *          Based on : https://github.com/valette/ACVD
 *
 *          Approximated Centroidal Voronoi Diagrams for Uniform Polygonal Mesh
 *          S. Vallette et al.
 *
 *  @author Christophe Ecabert
 *  @date   10/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/geometry/isotropic_clustering_metric.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  IsotropicClusteringMetric
 * @fn    IsotropicClusteringMetric(void)
 * @brief Constructor
 */
IsotropicClusteringMetric::IsotropicClusteringMetric(void)  {
  this->gradation_ = 0;
  custom_weights_ = nullptr;
}

/*
 * @name  ~IsotropicClusteringMetric
 * @fn    ~IsotropicClusteringMetric(void)
 * @brief Destructor
 */
IsotropicClusteringMetric::~IsotropicClusteringMetric(void) {
  if (custom_weights_) {
    delete custom_weights_;
    custom_weights_ = nullptr;
  }
}

#pragma mark -
#pragma mark Process

/*
 * @name  IsCurvatureIndicatorNeeded
 * @fn    int IsCurvatureIndicatorNeeded(void) const
 * @brief Indicate if curvature is needed
 * @return    0 if not needed, 1 otherwise
 */
int IsotropicClusteringMetric::IsCurvatureIndicatorNeeded(void) const {
  return this->gradation_ > 0.0 ? 1 : 0;
}

/*
 * @name  IsPrincipalDirectionsNeeded
 * @fn    int IsPrincipalDirectionsNeeded(void) const
 * @brief Indicate if principal direction is needed
 * @return    0, not used for this metric
 */
int IsotropicClusteringMetric::IsPrincipalDirectionsNeeded(void) const {
  return 0;
}

/*
 * @name  ComputeDistanceDataCluster
 * @fn    T ComputeDistanceDataCluster(const int data_idx,
                                       const int cluster_idx)
 * @brief Compute distance between a given cluster and a given data point
 * @param[in] data_idx        Data point index
 * @param[in] cluster_idx     Cluster index
 * @return    0
 */
double IsotropicClusteringMetric::ComputeDistanceDataCluster(const int data_idx,
                                                             const int cluster_idx) {
  return 0.0;
}

/*
 * @name  ComputeDistanceDataPoint
 * @fn    T ComputeDistanceDataPoint(const int data_idx,
                                     const Vector3<T>& vertex)
 * @brief Compute distance between cluster data point and a vertex
 * @param data_idx    Index of the cluster data
 * @param vertex      Vertex
 * @return    Distance between cluster data and vertex
 */
double IsotropicClusteringMetric::ComputeDistanceDataPoint(const int data_idx,
                                                           const Vector3<double>& vertex) {
  const ClusterData& data = cluster_data_[data_idx];
  Vector3<double> d = data.value / data.weight;
  return (d - vertex).Norm();
}

/*
 * @name  Reset
 * @fn    void Reset(const int idx)
 * @brief Clear a given cluster
 * @param[in] idx    Cluster index to reset
 */
void IsotropicClusteringMetric::Reset(const int idx) {
  clusters_[idx].Reset();
}

/*
 * @name  Copy
 * @fn    void Copy(const Cluster& cluster, const int dst_index)
 * @brief Copy one cluster to a specific location
 * @param[in] cluster     Source cluster to copy
 * @param[in] dst_index   Index where to copy
 */
void IsotropicClusteringMetric::Copy(const Cluster& cluster,
                                     const int dst_index) {
  clusters_[dst_index] = cluster;
}

/*
 * @name  AddDataToCluster
 * @fn    void AddDataToCluster(const int data_idx, const int cluster_idx)
 * @brief Add a given data point into a specific cluster
 * @param[in] data_idx
 * @param[in] cluster_idx
 */
void IsotropicClusteringMetric::AddDataToCluster(const int data_idx,
                                                 const int cluster_idx) {
  clusters_[cluster_idx].AddData(cluster_data_[data_idx]);
}

/*
 * @name    Add
 * @fn  void Add(const int src,
                 const int data_idx,
                 Cluster* dst)
 * @brief Copy a cluster to a destination and add data to it.
 * @param[in] src        Index of the cluster to copy
 * @param[in] data_idx   Index of the data to add to the destination
 * @param[in,out] dst    Cluster where to put result
 */
void IsotropicClusteringMetric::Add(const int src,
                                    const int data_idx,
                                    Cluster* dst) {
  // Deep copy
  *dst = clusters_[src];
  // Add data
  dst->AddData(cluster_data_[data_idx]);
}

/*
 * @name    Sub
 * @fn  void Sub(const int src,
                 const int data_idx,
                 Cluster* dst)
 * @brief Copy a cluster to a destination and substract data to it.
 * @param[in] src        Index of the cluster to copy
 * @param[in] data_idx   Index of the data to substract to the destination
 * @param[in,out] dst        Cluster where to put result
 */
void IsotropicClusteringMetric::Sub(const int src,
                                    const int data_idx,
                                    Cluster* dst) {
  // Deep copy
  *dst = clusters_[src];
  // Add data
  dst->SubData(cluster_data_[data_idx]);
}

/*
 * @name  Build
 * @fn    int Build(const LTS5::Mesh<T>* mesh, const int number_cluster)
 * @brief Build metric
 * @param[in] mesh            Mesh to build the metric from
 * @param[in] number_cluster  Number of cluster wanted
 * @return    -1 if error, 0 otherwise
 */
template<>
int IsotropicClusteringMetric::Build(const LTS5::Mesh<float>* mesh,
                                     const int number_cluster) {
  using Edge = typename Mesh<float>::Edge;
  int err = 0;
  // Init cluster
  clusters_.resize(static_cast<size_t>(number_cluster));
  for (auto& cluster : clusters_) {
    cluster.Reset();
  }
  // Check if halfedge are computed, if not compute them
  if (!mesh->halfedge_is_computed()) {
    err = const_cast<Mesh<float>*>(mesh)->ComputeHalfedges();
  }
  if (!err) {
    // Init metric
    size_t n_vertex = mesh->size();
    cluster_data_.resize(n_vertex);
    // Init cluster data point
    const auto& vertex = mesh->get_vertex();
    for (size_t i = 0; i < n_vertex; ++i) {
      const auto& vert = vertex[i];
      // Get each vertex area which is defined by the one ring of the vertex.
      std::vector<size_t> neighbors;
      bool full = mesh->OneRingNeighbors(i,&neighbors);
      // Compute area
      double area = 0.0;
      size_t n_neighbor = neighbors.size();
      if (neighbors.size() > 0) {
        size_t end = full ? n_neighbor : n_neighbor - 1;
        for (size_t v = 0; v < end; ++v) {
          Edge e0 = vertex[neighbors[v]] - vert;
          Edge e1 = vertex[neighbors[(v+1) % n_neighbor]] - vert;
          area += (e0 ^ e1).Norm() / 6.0; // Divide area by the #vertex
          // on the face (i.e. 3)
        }
      }
      // Set to data_cluser
      cluster_data_[i].weight = area;
      cluster_data_[i].value = Vector3<double>(vert.x_, vert.y_, vert.z_);
    }
    // Clamp
    this->ClampWeight(100000.0);
    // Update value with new weight
    for (auto& data : cluster_data_) {
      data.value *= data.weight;
    }
  }
  return err;
}

/*
 * @name  Build
 * @fn    int Build(const LTS5::Mesh<T>* mesh, const int number_cluster)
 * @brief Build metric
 * @param[in] mesh            Mesh to build the metric from
 * @param[in] number_cluster  Number of cluster wanted
 * @return    -1 if error, 0 otherwise
 */
template<>
int IsotropicClusteringMetric::Build(const LTS5::Mesh<double>* mesh,
                                     const int number_cluster) {
  using Edge = typename Mesh<double>::Edge;
  int err = 0;
  // Init cluster
  clusters_.resize(static_cast<size_t>(number_cluster));
  for (auto& cluster : clusters_) {
    cluster.Reset();
  }
  // Check if halfedge are computed, if not compute them
  if (!mesh->halfedge_is_computed()) {
    err = const_cast<Mesh<double>*>(mesh)->ComputeHalfedges();
  }
  if (!err) {
    // Init metric
    size_t n_vertex = mesh->size();
    cluster_data_.resize(n_vertex);
    // Init cluster data point
    const auto& vertex = mesh->get_vertex();
    for (size_t i = 0; i < n_vertex; ++i) {
      const auto& vert = vertex[i];
      // Get each vertex area which is defined by the one ring of the vertex.
      std::vector<size_t> neighbors;
      bool full = mesh->OneRingNeighbors(i,&neighbors);
      // Compute area
      double area = 0.0;
      size_t n_neighbor = neighbors.size();
      if (neighbors.size() > 0) {
        size_t end = full ? n_neighbor : n_neighbor - 1;
        for (size_t v = 0; v < end; ++v) {
          Edge e0 = vertex[neighbors[v]] - vert;
          Edge e1 = vertex[neighbors[(v+1) % n_neighbor]] - vert;
          area += (e0 ^ e1).Norm() / 6.0; // Divide area by the #vertex
          // on the face (i.e. 3)
        }
      }
      // Set to data_cluser
      cluster_data_[i].weight = area;
      cluster_data_[i].value = vert;
    }
    // Clamp
    this->ClampWeight(100000.0);
    // Update value with new weight
    for (auto& data : cluster_data_) {
      data.value *= data.weight;
    }
  }
  return err;
}

/*
 * @name  ComputeClusterCentroid
 * @fn    void ComputeClusterCentroid(const int cluster_idx)
 * @brief Compute centroid for a given cluster
 * @param[in] cluster_idx Index where to compute centroid
 */
void IsotropicClusteringMetric::ComputeClusterCentroid(const int cluster_idx) {
  // Not needed here
}

/**
 * @name  GetClusterCentroid
 * @fn    void GetClusterCentroid(const int cluster_idx, Vector3<T>* centroid)
 * @brief Get cluster's centroid
 * @param[in] cluster_idx Cluster index
 * @param[out] centroid   Centroid
 */
void IsotropicClusteringMetric::GetClusterCentroid(const int cluster_idx,
                                                   Vector3<double>* centroid) {
  clusters_[cluster_idx].GetCentroid(centroid);
}

/*
 * @name  ComputeClusterEnergy
 * @fn    void ComputeClusterEnergy(const int cluster_idx)
 * @brief Compute energy for a given cluster
 * @param[in] cluster_idx Index where to compute energy
 */
void IsotropicClusteringMetric::ComputeClusterEnergy(const int cluster_idx) {
  clusters_[cluster_idx].ComputeEnergy();
}

#pragma mark -
#pragma mark Private

/*
 * @name  ClampWeight
 * @fn    void ClampWeight(const T ratio)
 * @brief Clamps the weights between AverageValue/Ratio and AverageValue*Ratio
 * @param[in] ratio   Ratio to clamp
 */
void IsotropicClusteringMetric::ClampWeight(const double ratio) {
  // Compute average
  double average = 0.0;
  for (const auto& data : cluster_data_) {
    average += data.weight;
  }
  average /= static_cast<double>(cluster_data_.size());
  // Define margin
  double min = average / ratio;
  double max = average * ratio;
  // Apply clamp
  for(auto& data : cluster_data_) {
    data.weight = data.weight > max ? max : data.weight;
    data.weight = data.weight < min ? min : data.weight;
  }
}
}  // namepsace LTS5







