/**
 *  @file   aabb_mpu_tree.cpp
 *  @brief  AABB tree for MPU speed-up class implementation
 *
 *  @author Gabriel Cuendet
 *  @date   07/06/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <stack>

#include "lts5/geometry/aabb_mpu_tree.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
#pragma mark -
#pragma mark Initialization

/*
 * @name  AABBMPUTree
 * @fn  AABBMPUTree(void)
 * @brief Constructor
 */
template<typename T>
AABBMPUTree<T>::AABBMPUTree(void) : AABBTree<T>() {}

/*
 * @name  AABBMPUTree
 * @fn  AABBMPUTree(const std::vector<MPUNode*>& nodes)
 * @brief Constructor taking a vector of nodes and calling Insert directly
 * @param[in] nodes  Collection of MPUNodes used to build the tree
 */
template<typename T>
AABBMPUTree<T>::AABBMPUTree(const std::vector<MPUNode<T>*>& nodes)
: AABBTree<T>(){
  // Add the given range
  this->Insert(nodes);
}

/*
 * @name  Insert
 * @fn  void Insert(const Mesh<T>& mesh)
 * @brief Insert a range of element into the tree
 * @param[in] nodes  Collection of MPUNode to use to build the tree
 */
template<typename T>
void AABBMPUTree<T>::Insert(const std::vector<MPUNode<T>*>& nodes) {
  // Loop over all the nodes
  int idx = 0;  // Correspondence between each bbox and each nodes
  auto first_node = nodes.begin();
  auto last_node = nodes.end();
  while (first_node != last_node) {
    // Get the node support box (with respect to support radius and NOT bbox)
    const Vector3<T>& c_i((*first_node)->center());
    T R_i((*first_node)->Ri());
    this->data_bbox_.push_back(AABB<T>(c_i.x_ - R_i, c_i.x_ + R_i,
                                       c_i.y_ - R_i, c_i.y_ + R_i,
                                       c_i.z_ - R_i, c_i.z_ + R_i, idx));
    ++first_node;
    ++idx;
  }

  this->need_build_ = true;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  DoIntersect
 * @fn  bool DoIntersect(const Vector3<T>& p, const Vector3<T>& dir,
 *                       std::vector<int>* idx)
 * @brief Check if a given point intersects with one of the bounding box in
 *        the tree and provide its corresponding index
 *  @param[in]  p   Coordinates of the query point
 *  @param[out] idx Indices of the corresponding intersections
 *  @return True if intersect, false otherwise
 */
template<typename T>
bool AABBMPUTree<T>::DoIntersect(const Vector3<T>& p, std::vector<int>* idx) const {
  // Go down the tree starting from top
  idx->clear();
  std::stack<AABBNode<T>*> stack;
  stack.push(this->root_node_);
  while(!stack.empty()) {
    // Get node
    AABBNode<T>* node = stack.top();
    stack.pop();

    // Check if node intersect
    AABB<T> bbox = node->get_bbox();
    if(AABB<T>::IntersectPoint(bbox, p)) {
      // Intersect, check for children
      switch(node->size()) {
          // Size 2, reach leaf
        case 2 : {
          bbox = *node->get_left_data();
          if(AABB<T>::IntersectPoint(bbox, p)) {
            idx->push_back(bbox.index_);
          }
          bbox = *node->get_right_data();
          if(AABB<T>::IntersectPoint(bbox, p)) {
            idx->push_back(bbox.index_);
          }
        }
          break;
          // Size 3, check left data and push right child
        case 3 : {
          // Check left data
          bbox = *node->get_left_data();
          if(AABB<T>::IntersectPoint(bbox, p)) {
            idx->push_back(bbox.index_);
          }
          // Push right child on stack
          stack.push(node->get_right_child());
        }
          break;
          // Else push both child
        default : {
          // Push left/right child onto stack
          stack.push(node->get_left_child());
          stack.push(node->get_right_child());
        }
          break;
      }
    }
  }
  return (!idx->empty());
}

#pragma mark -
#pragma mark Declaration

  /** Float AABBMPUTree */
  template class AABBMPUTree<float>;
  /** Double AABBMPUTree */
  template class AABBMPUTree<double>;

}  // namespace LTS5
