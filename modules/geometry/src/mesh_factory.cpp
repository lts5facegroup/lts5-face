/**
 *  @file   mesh_factory.cpp
 *  @brief  Mesh factory, helper class to generate basic meshes such as
 *          Sphere, Cube, Plane and read FWH data
 *
 *  @author Christophe Ecabert, Marina Zimmermann
 *  @date   18/07/2016, 27/06/2016
 *  Copyright (c) 2016 Christophe Ecabert, Marina Zimmermann. All rights reserved.
 */

#include <iostream>
#include <string>
#include <vector>
#include <limits>

#include "lts5/utils/shape_transforms.hpp"
#include "lts5/geometry/mesh_factory.hpp"


/*
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Sphere

/*
 * @name  GenerateSphere
 * @fn  static void GenerateSphere(SphereParameters* param, Mesh<T>* mesh)
 * @brief Helper function to generate user defined Sphere
 * @param[in]   param   Parameters to generate the sphere
 * @param[out]  mesh    Mesh where to add the generate sphere
 */
template<typename T>
void MeshFactory<T>::GenerateSphere(SphereParameters* param,
                                    Mesh<T> *mesh) {
  // Make parameters valid
  param->MakeParameterValid();
  // Access Vertex, Triangle, Normal
  std::vector<Vertex>& vertex = mesh->get_vertex();
  std::vector<Normal>& normal = mesh->get_normal();
  std::vector<Triangle>& tri = mesh->get_triangle();

  // Complete sphere ?
  bool f_phi = (std::abs((param->phi_max - param->phi_min) -
                         2.0 * Constants<T>::PI) <=
                T(5) * std::numeric_limits<T>::epsilon());
  // Define angular increment
  T d_phi = (param->phi_max - param->phi_min);
  T d_theta = (param->theta_max - param->theta_min);
  d_phi /= f_phi ?
           static_cast<T>(param->phi_step) :
           static_cast<T>(param->phi_step - 1);
  d_theta /= static_cast<T>(param->theta_step - 1);


  // Loop over theta, phi
  const T r = param->radius;
  for (int t = 0 ; t < param->theta_step; ++t) {
    // Current angle
    const T theta = param->theta_min + static_cast<T>(t) * d_theta;
    const T sin_t = std::sin(theta);
    const T cos_t = std::cos(theta);
    // Put only a single vertex at poles
    int phi_max = std::abs(sin_t) < std::numeric_limits<T>::epsilon() ?
                  1 :
                  param->phi_step;
    for (int p = 0; p < phi_max; ++p) {
      // Current phi
      const T phi = param->phi_min + static_cast<T>(p) * d_phi;
      const T sin_p = std::sin(phi);
      const T cos_p = std::cos(phi);
      // Define vertex position
      T x = r * sin_t * cos_p;
      T y = r * sin_t * sin_p;
      T z = r * cos_t;
      vertex.push_back(Vertex(x, y, z));
      Normal n(x, y, z);
      n.Normalize();
      normal.push_back(n);

      // Setup triangle
      if (p != 0 && t != 0) {
        const int sz = static_cast<int>(vertex.size()) - 1;
        const int idx_n1 = sz - param->phi_step;
        const int idx_n0 = sz - param->phi_step - 1;
        const int idx_1 = sz;
        const int idx_0 = sz - 1;

        if (idx_n0 < 0) {
          // Top pole
          tri.push_back(Triangle(0, idx_0, idx_1));
        } else if (idx_n0 >= 0) {
          tri.push_back(Triangle(idx_n1, idx_n0, idx_0));
          tri.push_back(Triangle(idx_n1, idx_0, idx_1));
        }
        // Full circle
        if (f_phi && (p == param->phi_step - 1)) {
          if (idx_n0 < 0) {
            // Top pole
            const int idx_11 = idx_1 - p;
            tri.push_back(Triangle(0, idx_1, idx_11));
          } else {
            const int idx_11 = idx_1 - p;
            const int idx_n11 = idx_n1 - p;
            tri.push_back(Triangle(idx_n11, idx_n1, idx_1));
            tri.push_back(Triangle(idx_n11, idx_1, idx_11));
          }
        }
      } else {
        // p==0 or t == 0
        if (phi_max == 1 && t > 0) {
          // at pole bottom
          const int idx0 = static_cast<int>(vertex.size()) - 1;
          for (int k = 0; k < param->phi_step; ++k) {
            const int idx1 = idx0 - param->phi_step + k;
            const int idx2 = idx0 - param->phi_step + k + 1;
            tri.push_back(Triangle(idx0, idx2, idx1));
            if (f_phi && (k == param->phi_step - 1)) {
              // add last element
              const int idx_n = idx0 - param->phi_step;
              tri.push_back(Triangle(idx1, idx2, idx_n));
            }
          }
        }
      }
    }
  }
}

/*
 * @name  GenerateCuboid
 * @fn  static void GenerateCuboid(SphereParameters* param, Mesh<T>* mesh)
 * @brief Helper function to generate user defined Cuboid
 * @param[in,out] param   Parameters to generate the Cuboid. If \p param
 *                        are not in the proper range, get automatically
 *                        updated to correct value.
 * @param[out]    mesh    Mesh where to add the generated Cuboid
 */
template<typename T>
void MeshFactory<T>::GenerateCuboid(CuboidParameters* param,
                                    Mesh<T>* mesh) {
  // Make parameters valid
  param->MakeParameterValid();
  // Access Vertex, Triangle, Normal
  std::vector<Vertex>& vertex = mesh->get_vertex();
  std::vector<Triangle>& tri = mesh->get_triangle();

  // Define border
  const T half_w = param->width / T(2.0);
  const T half_h = param->height / T(2.0);
  const T half_d = param->depth / T(2.0);
  // Define increment
  const T w_step = param->width / (param->width_step - 1);
  const T h_step = param->height / (param->height_step - 1);
  const T d_step = param->depth / (param->depth_step - 1);

  int idx_11 = -1;
  int idx_10 = -1;
  int idx_01 = -1;
  int idx_00 = -1;

  // Frontal Face + Back face
  // -----------------------------------------------------------------------
  for (int r = 0; r < param->height_step; ++r) {
    // Current Height
    const T h = -half_h + r * h_step;
    for (int c = 0; c < param->width_step; ++c) {
      // Current Width
      const T w = -half_w + c * w_step;
      // Add vertex (Interleave front + back)
      vertex.push_back(Vertex(w, h, half_d));   // Front
      vertex.push_back(Vertex(w, h, -half_d));  // Back
      // Add triangle if needed
      if (r != 0 && c != 0) {
        // Back
        int sz = static_cast<int>(vertex.size()) - 1;
        idx_11 = sz;
        idx_10 = sz - 2;
        idx_01 = sz - (2 * param->width_step);
        idx_00 = idx_01 - 2;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
        // Front
        tri.push_back(Triangle(idx_10-1, idx_00-1, idx_01-1));
        tri.push_back(Triangle(idx_10-1, idx_01-1, idx_11-1));
      }
    }
  }
  // Left Face + Right face
  // -----------------------------------------------------------------------
  for (int r = 0; r < param->height_step; ++r) {
    // Current Height
    const T h = -half_h + r * h_step;
    for (int w = 0; w < param->depth_step; ++w) {
      // Current depth
      const T d = -half_d + w * d_step;
      if (w != 0 && w != (param->depth_step - 1)) {
        vertex.push_back(Vertex(-half_w, h, d));  // Left
        vertex.push_back(Vertex(half_w, h, d));   // Right
      }
      // Add triangle if needed
      int sz = static_cast<int>(vertex.size()) - 1;
      if (r > 0 && w == 1) {
        // Left
        idx_11 = sz - 1;
        idx_10 = r * (2 * param->width_step) + 1;
        idx_01 = sz - (2 * param->depth_step) + 3;
        idx_00 = (r-1) * (2 * param->width_step) + 1;
        tri.push_back(Triangle(idx_10, idx_00, idx_01));
        tri.push_back(Triangle(idx_10, idx_01, idx_11));
        // Right
        idx_11 = sz;
        idx_10 = (r+1) * (2 * param->width_step) - 1;
        idx_01 = sz - (2 * param->depth_step) + 4;
        idx_00 = r * (2 * param->width_step) - 1;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
      } else if (r > 0 && w > 1 && w < (param->depth_step - 1)) {
        // Left
        idx_11 = sz;
        idx_10 = sz - 2;
        idx_01 = sz - (2 * param->depth_step) + 4;
        idx_00 = idx_01 - 2;
        // Left
        tri.push_back(Triangle(idx_10-1, idx_00-1, idx_01-1));
        tri.push_back(Triangle(idx_10-1, idx_01-1, idx_11-1));
        // Right
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));

      } else if (r > 0 && w == (param->depth_step-1)) {
        // Left
        idx_10 = sz - 1;
        idx_00 = sz - (2 * param->depth_step) + 3;
        idx_01 = (r-1) * (2 * param->width_step);
        idx_11 = r * (2 * param->width_step);
        tri.push_back(Triangle(idx_10, idx_00, idx_01));
        tri.push_back(Triangle(idx_10, idx_01, idx_11));
        // Right
        idx_10 = sz;
        idx_00 = sz - (2 * param->depth_step) + 4;
        idx_01 = r * (2 * param->width_step) - 2;
        idx_11 = (r+1) * (2 * param->width_step) - 2;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
      }
    }
  }
  // Top Face + Bottom face
  // -----------------------------------------------------------------------
  for (int k = 0; k < param->depth_step; ++k) {
    // Current depth
    const T d = -half_d + k * d_step;
    for (int c = 0; c < param->width_step; ++c) {
      // Current Width
      const T w = -half_w + c * w_step;
      if (c != 0 && k != 0 &&
          c != (param->width_step - 1) &&
          k != (param->depth_step - 1)) {
        vertex.push_back(Vertex(w, half_h, d));     // Top
        vertex.push_back(Vertex(w, -half_h, d));    // Bottom
      }

      // Add triangle
      int sz = static_cast<int>(vertex.size()) - 1;
      if (c == 1 && k == 1) {
        // Bottom
        idx_11 = sz;
        idx_10 = (2 * param->width_step * param->height_step);
        idx_00 = 1;
        idx_01 = 3;
        tri.push_back(Triangle(idx_10, idx_00, idx_01));
        tri.push_back(Triangle(idx_10, idx_01, idx_11));
        // Top
        idx_11 = sz -1;
        idx_01 = (2 * param->width_step * (param->height_step-1)) + 3;
        idx_00 = idx_01 - 2;
        idx_10 += (2 * (param->depth_step-2) * (param->height_step-1));
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
      } else if (c == 1 && k > 1 && k < (param->depth_step - 1)) {
        // Top
        idx_11 = sz - 1;
        idx_01 = sz - (2 * param->width_step) + 3;
        idx_00 = 2 * param->width_step * param->height_step;
        idx_00 += (2 * ((param->height_step - 1) * (param->depth_step - 2)) +
                   2 * (k - 2));
        idx_10 = idx_00 + 2;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
        // Bottom
        idx_11 = sz;
        idx_01 = sz - (2 * param->width_step) + 4;
        idx_00 = 2 * (param->width_step * param->height_step) + 2 * (k - 2);
        idx_10 = idx_00 + 2;
        tri.push_back(Triangle(idx_10, idx_00, idx_01));
        tri.push_back(Triangle(idx_10, idx_01, idx_11));
      } else if (c== 1 && k == (param->depth_step - 1)) {
        // Top
        idx_01 = sz - (2 * (param->width_step - 2)) + 1;
        idx_10 = 2 * param->width_step * (param->height_step - 1);
        idx_11 = idx_10 + 2;
        idx_00 = 2 * ((param->width_step * param->height_step) +
                      ((param->depth_step - 2) * param->height_step)) - 2;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
        // Bottom
        idx_10 = 0;
        idx_11 = 2;
        idx_01 = sz - (2 * (param->width_step - 2)) + 2;
        idx_00 = 2 * ((param->width_step * param->height_step) +
                      (param->depth_step - 2)) - 2;
        tri.push_back(Triangle(idx_10, idx_00, idx_01));
        tri.push_back(Triangle(idx_10, idx_01, idx_11));
      } else if (c > 1 && k == 1 && c < (param->width_step - 1)) {
        // Top
        idx_11 = sz - 1;
        idx_10 = sz - 3;
        idx_01 = 2 * (param->height_step - 1) * param->width_step + 2 * c + 1;
        idx_00 = idx_01 - 2;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
        // bottom
        idx_11 = sz;
        idx_10 = sz - 2;
        idx_01 = 2 * c + 1;
        idx_00 = idx_01 - 2;
        tri.push_back(Triangle(idx_10, idx_00, idx_01));
        tri.push_back(Triangle(idx_10, idx_01, idx_11));
      } else if (c > 1 && k > 1 &&
          c < (param->width_step - 1) &&
          k < (param->depth_step - 1)) {
        // Top
        idx_11 = sz - 1;
        idx_10 = sz - 3;
        idx_01 = sz - (2 * param->width_step) + 3;
        idx_00 = idx_01 - 2;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
        // Bottom
        tri.push_back(Triangle(idx_10+1, idx_00+1, idx_01+1));
        tri.push_back(Triangle(idx_10+1, idx_01+1, idx_11+1));
      } else if (c > 1 &&
                 k == (param->depth_step - 1) &&
                 c < (param->width_step - 1)) {
        // Top
        idx_00 = sz - 2 * (param->width_step - 2) + 2 * (c-2) + 1;
        idx_01 = idx_00 + 2;
        idx_11 = 2 * param->width_step * (param->height_step - 1) + 2*c;
        idx_10 = idx_11 - 2;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
        // Bottom
        idx_00 = sz - 2 * (param->width_step - 2) + 2 * (c-2) + 2;
        idx_01 = idx_00 + 2;
        idx_11 = 2*c;
        idx_10 = idx_11 - 2;
        tri.push_back(Triangle(idx_10, idx_00, idx_01));
        tri.push_back(Triangle(idx_10, idx_01, idx_11));
      } else if (c == (param->width_step - 1) && k == 1) {
        // Top
        idx_10 = sz - 1;
        idx_01 = 2 * param->height_step * param->width_step - 1;
        idx_00 = idx_01 - 2;
        idx_11 = 2 * param->height_step * param->width_step;
        idx_11 += (2 * (param->height_step-1) * (param->depth_step - 2)) + 1;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
        // Bottom
        idx_10 = sz;
        idx_01 = 2 * param->width_step - 1;
        idx_00 = idx_01 - 2;
        idx_11 = 2 * param->height_step * param->width_step + 1;
        tri.push_back(Triangle(idx_10, idx_00, idx_01));
        tri.push_back(Triangle(idx_10, idx_01, idx_11));
      } else if (c == (param->width_step - 1) &&
                 k > 1 &&
                 k < (param->depth_step - 1)) {
        // Top
        idx_10 = sz - 1;
        idx_00 = sz - 2 * (param->width_step - 2) - 1;
        idx_01 = (2 * param->width_step * param->height_step +
                  2 * (param->height_step - 1) * (param->depth_step - 2) +
                  2*k - 3);
        idx_11 = idx_01 + 2;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
        // Bottom
        idx_10 = sz;
        idx_00 = sz - 2 * (param->width_step - 2);
        idx_01 = 2 * param->width_step * param->height_step  + 2*k - 3;
        idx_11 = idx_01 + 2;
        tri.push_back(Triangle(idx_10, idx_00, idx_01));
        tri.push_back(Triangle(idx_10, idx_01, idx_11));
      } else if (c == (param->width_step - 1) && k == (param->depth_step - 1)) {
        // Top
        idx_00 =  sz - 1;
        idx_01 = (2 * param->width_step * param->height_step +
                  2 * param->height_step * (param->depth_step - 2) - 1);
        idx_11 = 2 * param->width_step * param->height_step - 2;
        idx_10 = idx_11 - 2;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
        // Bottom
        idx_00 =  sz;
        idx_01 = (2 * param->width_step * param->height_step +
                  2 * (param->depth_step - 2) - 1);
        idx_11 = 2 * param->width_step - 2;
        idx_10 = idx_11 - 2;
        tri.push_back(Triangle(idx_10, idx_00, idx_01));
        tri.push_back(Triangle(idx_10, idx_01, idx_11));
      }
    }
  }
}

#pragma mark -
#pragma mark Plane

/*
 * @name  GeneratePlane
 * @fn  static void GeneratePlane(PlaneParameters* param, Mesh<T>* mesh)
 * @brief Helper function to generate user defined Plane
 * @param[in,out] param   Parameters to generate the plane. If \p param
 *                        are not in the proper range, get automatically
 *                        updated to correct value.
 * @param[out]    mesh    Mesh where to add the generated plane
 */
template<typename T>
void MeshFactory<T>::GeneratePlane(PlaneParameters* param, Mesh<T>* mesh) {
  // Make parameters valid
  param->MakeParameterValid();
  // Access Vertex, Triangle, Normal
  std::vector<Vertex>& vertex = mesh->get_vertex();
  std::vector<Triangle>& tri = mesh->get_triangle();
  // Define border
  const T half_w = param->width / T(2.0);
  const T half_h = param->height / T(2.0);
  // Define increment
  const T w_step = param->width / (param->u_step - 1);
  const T h_step = param->height / (param->v_step - 1);
  // Define uv-coordinate system
  LTS5::Vector3<T> u_dir;
  if (param->normal.x_ != T(0) && param->normal.y_ != T(0)) {
    u_dir.x_ = param->normal.y_;
    u_dir.y_ = -param->normal.x_;
    u_dir.z_ = T(0);
  } else {
    u_dir.x_ = T(0);
    u_dir.y_ = param->normal.z_;
    u_dir.z_ = -param->normal.y_;
  }
  u_dir.Normalize();
  LTS5::Vector3<T> v_dir = param->normal ^ u_dir;

  for (int u = 0; u < param->u_step; ++u) {
    // Current u
    const LTS5::Vector3<T> u_c = (-half_w + u * w_step) * u_dir;
    for (int v = 0; v < param->v_step; ++v) {
      // Current v
      const LTS5::Vector3<T> v_c = (-half_h + v * h_step) * v_dir;
      // Push
      vertex.push_back(Vertex(u_c.x_ + v_c.x_,
                              u_c.y_ + v_c.y_,
                              u_c.z_ + v_c.z_));
      if (u > 0 && v > 0) {
        int sz = static_cast<int>(vertex.size() - 1);
        int idx_11 = sz;
        int idx_10 = sz - 1;
        int idx_01 = sz - param->v_step;
        int idx_00 = idx_01 - 1;
        tri.push_back(Triangle(idx_00, idx_10, idx_01));
        tri.push_back(Triangle(idx_01, idx_10, idx_11));
      }
    }
  }
}

#pragma mark -
#pragma mark LoadingFWH

/*
 *  @name LoadFWHObj
 *  @fn static int LoadFWHObj(const std::string& filepath, LTS5::Mesh<T>* mesh)
 *  @brief  Load FaceWarehouse mesh from OBJ file WITHOUT any connectivity!
 *  @param[in]  filename  Path to the FWH obj mesh file
 *  @param[out] mesh      Pointer to mesh
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int MeshFactory<T>::LoadFWHObj(const std::string& filepath,
                               LTS5::Mesh<T>* mesh) {
  int error = -1;
  std::ifstream stream(filepath, std::ios_base::in);
  if (stream.is_open()) {
    // Start to read
    std::string line, key;
    std::stringstream str_stream;

    std::vector<Vertex>& vertex_list = mesh->get_vertex();
    std::vector<Normal>& normal_list = mesh->get_normal();
    std::vector<TCoord>& tex_coord_list = mesh->get_tex_coord();
    Vertex vertex;
    Normal normal;
    TCoord tcoord;

    std::string temp_str;
    std::stringstream temp_strstream;

    while (!stream.eof()) {
      // Scan line
      std::getline(stream, line);
      if (!line.empty()) {
        str_stream.str(line);
        str_stream >> key;
        if (key == "v") {
          // Vertex
          str_stream >> vertex;
          vertex_list.push_back(vertex);
        } else if (key == "vn") {
          // Normal
          str_stream >> normal;
          normal_list.push_back(normal);
        } else if (key == "vt") {
          // Texture coordinate
          str_stream >> tcoord;
          tex_coord_list.push_back(tcoord);
        }
        str_stream.clear();
      }
    }
    error = 0;
  }

  mesh->ComputeBoundingBox();
  return error;
}

/*
 *  @name LoadSingleExprFromBs
 *  @fn int LoadSingleExprFromBs(const std::string& path,
 *                               int expression_id,
 *                               LTS5::Mesh<T>* mesh)
 *  @brief  Load FaceWarehouse mesh from blendshape file (.bs) for a single expression
 *  @param[in]  filename       Path to the FWH blendshape file (.bs)
 *  @param[in]  expression_id  Index of wanted expression
 *  @param[out] mesh           Pointer to mesh
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int MeshFactory<T>::LoadSingleExprFromBs(const std::string& path,
                                         int expression_id,
                                         LTS5::Mesh<T>* mesh) {
  int error = -1;
  std::ifstream stream(path, std::ios_base::in | std::ios_base::binary);
  if (stream.is_open()) {
    int nShapes = 0;
    int nVerts = 0;
    int nFaces = 0;
    stream.read((char*)&nShapes, sizeof(nShapes));    // nShape = 46
    stream.read((char*)&nVerts, sizeof(nVerts));      // nVerts = 11510
    stream.read((char*)&nFaces, sizeof(nFaces));      // nFaces = 11540

    std::vector<Vertex>& vertex_list = mesh->get_vertex();
    vertex_list.resize(nVerts);

    // Load specified expression
    int offset = 3 * sizeof(int) + expression_id * nVerts * sizeof(Vertex);
    stream.seekg(offset);

    stream.read(reinterpret_cast<char*>(vertex_list.data()),
                nVerts * sizeof(Vertex));
    mesh->ComputeBoundingBox();
    error = 0;
  }
  return error;
}

/*
 *  @name LoadAllExprFromBs
 *  @fn static int LoadAllExprFromBs(const std::string& filename,
 *                                   std::vector<LTS5::Mesh<T> >* mesh_vector);
 *  @brief  Load FaceWarehouse meshes for all expressions from blendshape file (.bs)
 *  @param[in]  filename     Path to the FWH blendshape file (.bs)
 *  @param[in]  mesh_vector  Vector of loaded meshes
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int MeshFactory<T>::
LoadAllExprFromBs(const std::string& filename,
                  std::vector<LTS5::Mesh<T>* >* mesh_vector) {
  int error = -1;
  if (!mesh_vector->empty()) {
    for (auto it = mesh_vector->begin(); it != mesh_vector->end(); ++it) {
      delete *it;
    }
    mesh_vector->clear();
  }

  std::ifstream stream(filename, std::ios_base::in | std::ios_base::binary);
  if (stream.is_open()) {
    int nShapes = 0;
    int nVerts = 0;
    int nFaces = 0;
    stream.read((char*)&nShapes, sizeof(nShapes));    // nShape = 46
    stream.read((char*)&nVerts, sizeof(nVerts));      // nVerts = 11510
    stream.read((char*)&nFaces, sizeof(nFaces));      // nFaces = 11540

    // Load neutral expression B_0
    mesh_vector->push_back(new LTS5::Mesh<T>());

    std::vector<Vertex>& vertex_list = mesh_vector->at(0)->get_vertex();
    vertex_list.resize(nVerts);
    stream.read(reinterpret_cast<char*>(vertex_list.data()),
                nVerts * sizeof(Vertex));
    mesh_vector->at(0)->ComputeBoundingBox();

    // Load other expressions B_i ( 1 <= i <= 46 )
    for (int expr_id=0; expr_id < nShapes; expr_id++) {
      mesh_vector->push_back(new LTS5::Mesh<T>());
      auto& vertex_list = mesh_vector->at(expr_id + 1)->get_vertex();
      vertex_list.resize(nVerts);
      stream.read(reinterpret_cast<char*>(vertex_list.data()),
                  nVerts * sizeof(Vertex));
      mesh_vector->at(expr_id + 1)->ComputeBoundingBox();
    }
    error = 0;
  }

  return error;
}

/*
 * @name  LoadQuads
 * @fn  static int LoadQuads(const std::string& filepath,
 *                           std::vector<LTS5::Vector4<int> >* quads)
 * @brief Load quads into vector of Quad
 * @param[in]  filepath  File to read the quad from
 * @param[out] quads
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int MeshFactory<T>::LoadQuads(const std::string& filepath,
                              std::vector<LTS5::Vector4<int> >* quads) {
  int error = -1;
  std::ifstream stream(filepath, std::ios_base::in);
  if (stream.is_open()) {
    quads->clear();

    // Start to read
    std::string line, key;
    std::stringstream str_stream;
    LTS5::Vector4<int> quad;

    while (!stream.eof()) {
      // Scan line
      std::getline(stream, line);
      if (!line.empty()) {
        str_stream.str(line);
        str_stream >> key;
        if (key == "f") {
          // Faces
          // Read the 4 indices one by one from the line
          // id1/xx/0 id2/xx/0 id3/xx/0 id4/xx/0
          int* quad_ptr = &quad.x_;
          for (int id = 0; id < 4; ++id) {
            std::string tmp_string;
            str_stream >> tmp_string;
            if (!tmp_string.empty()) {
              size_t pos = tmp_string.find('/');
              quad_ptr[id] = std::stoi(tmp_string.substr(0, pos)) - 1;
            }
          }
          quads->push_back(quad);
        }
        str_stream.clear();
      }
    }
    error = 0;
  }
  stream.close();
  return error;
}

/*
 * @name  DivideQuadIntoTri
 * @fn  static void DivideQuadIntoTri(const std::vector<LTS5::Vector4<int> >& quads,
 *                                    LTS5::Mesh<T>* mesh)
 * @brief Divide the quads into triangles along the shortest diagonal and
 *        apply these triangles to the mesh
 * @param[in]     quads  List of quads to divide
 * @param[in,out] mesh   Vertices to take into account when dividing, and tri to set
 */
template<typename T>
void MeshFactory<T>::
DivideQuadIntoTri(const std::vector<LTS5::Vector4<int> >& quads,
                  LTS5::Mesh<T>* mesh) {
  const auto& vertices = mesh->get_vertex();
  auto& triangles = mesh->get_triangle();
  triangles.clear();

  for (const auto& q : quads) {
    const Vertex& I = vertices[q.x_];
    const Vertex& J = vertices[q.z_];
    const Vertex& A = vertices[q.y_];
    const Vertex& B = vertices[q.w_];

    T diag_IJ = (J-I).Norm();
    T diag_AB = (B-A).Norm();

    Triangle tri1, tri2;
    if (diag_IJ < diag_AB) {
      tri1 = Triangle(q.x_, q.y_, q.z_);
      tri2 = Triangle(q.x_, q.z_, q.w_);
    } else {
      tri1 = Triangle(q.x_, q.y_, q.w_);
      tri2 = Triangle(q.y_, q.z_, q.w_);
    }
    triangles.push_back(tri1);
    triangles.push_back(tri2);
  }
}

/*
 *  @name ComputeMeanShape
 *  @fn void ComputeMeanShape(const std::vector<LTS5::Mesh<T>>& mesh_vector,
                              LTS5::Mesh<T>* mean_mesh)
 *  @brief  Compute the mean shape of all subjects for an expression
 *  @param[in]  mesh_vector Pointer to mesh vector
 *  @param[out] mean_mesh   Pointer to resulting mean mesh
 */
template<typename T>
void MeshFactory<T>::
ComputeMeanShape(const std::vector<LTS5::Mesh<T>*>& mesh_vector,
                 LTS5::Mesh<T>* mean_mesh) {
  size_t vector_len = mesh_vector.size();
  size_t mesh_size = mesh_vector[0]->get_vertex().size();

  std::vector<Vertex>& vertex_list = mean_mesh->get_vertex();
  vertex_list.resize(mesh_size);

  for (int i = 0; i < vector_len; ++i) {
    mesh_vector[i]->NormalizeMesh();
  }

  for (int m = 0; m < static_cast<int>(mesh_size); ++m) {
    Vertex sum(T(0.0), T(0.0), T(0.0));
    for (int i = 0; i < vector_len; ++i) {
      sum += mesh_vector[i]->get_vertex()[m];
    }
    sum /= static_cast<T>(vector_len);  // CHECK
    vertex_list[m] = sum;
  }
}

#pragma mark -
#pragma mark Declaration

/** MeshFactory - Float */
template class MeshFactory<float>;
/** MeshFactory - Double */
template class MeshFactory<double>;

}  // namespace LTS5
