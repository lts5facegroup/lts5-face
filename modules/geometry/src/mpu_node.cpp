/**
 *  @file   mpu_node.cpp
 *  @brief
 *
 *  @author Gabriel Cuendet
 *  @date   11/04/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include "lts5/geometry/bivariate_quadratic_poly.hpp"
#include "lts5/geometry/general_quadric.hpp"
#include "lts5/geometry/mpu_node.hpp"
#include "lts5/utils/math/constant.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name  ComputeMeanNormalDeviation
 * @fn  static double ComputeMeanNormalDeviation(const std::vector<LTS5::Vector3<T> >& normals)
 * @brief Compute the mean normal, and return the angle between each normal and their mean
 * @param[in]  normals, a vector of normals
 * @return mean deviation angle
 */
template<typename T>
double MPUNode<T>::ComputeMeanNormalDeviation(const std::vector<LTS5::Vector3<T> >& normals) {
  LTS5::Vector3<T> mean_normal(0.0, 0.0, 0.0);
  for (auto n_it = normals.begin(); n_it != normals.end(); ++n_it) {
    mean_normal += (*n_it);
  }
  mean_normal *= T(1/normals.size());

  double norm_m = mean_normal.Norm();

  // Project each normal on the mean normal and compute the angle
  double max_angle = 0.0;
  for (auto n_it = normals.begin(); n_it != normals.end(); ++n_it) {
    double norm_x = n_it->Norm();

    double cos_a = (mean_normal.x_ * n_it->x_ +
                    mean_normal.y_ * n_it->y_ +
                    mean_normal.z_ * n_it->z_) / (norm_m * norm_x);

    double alpha = acos(cos_a);
    // @TODO: (Gabriel) Check the intervall for cosine
    if (alpha > max_angle) {
      max_angle = alpha;
    }
  }

  return max_angle;
}

#pragma mark -
#pragma mark Initialization

/*
 * @name  MPUNode
 * @fn  MPUNode(void)
 * @brief Constructor
 */
template<typename T>
MPUNode<T>::MPUNode(void) : location_hash_(0), empty_(false), has_childs_(false),
diagonal_(0), Ri_(0), epsiloni_(0), estimator_(nullptr) {}

/*
 * @name  MPUNode
 * @fn  MPUNode(void)
 * @brief Constructor
 * @param[in] location  Hash code of the location of the node
 * @param[in] bbox      Bounding box of the current node
 */
template<typename T>
MPUNode<T>::MPUNode(const uint64_t& location, const AABB<T>& bbox)
: empty_(false), has_childs_(false), Ri_(0), epsiloni_(0), estimator_(nullptr) {
  this->location_hash_ = location;
  this->bbox_ = bbox;
  this->ci_ = (bbox.min_ + bbox.max_) * T(0.5);
  this->diagonal_ = (bbox.max_ - bbox.min_).Norm();
}

/*
 * @name  MPUNode
 * @fn  MPUNode(const MPUNode& other) = delete
 * @brief Copy constructor
 * @param[in] other Object to copy from
 */
template<typename T>
MPUNode<T>::MPUNode(const MPUNode& other) {
  this->location_hash_ = other.location_hash_;
  this->empty_ = other.empty_;
  this->has_childs_ = other.has_childs_;
  this->ci_ = other.ci_;
  this->bbox_ = other.bbox_;
  this->Ri_ = other.Ri_;
  this->epsiloni_ = other.epsiloni_;
  // Deep copy of the members that are pointers!
  this->estimator_ = other.estimator_->Clone();
}

/*
 * @name  ~MPUNode
 * @fn  ~MPUNode(void)
 * @brief Destructor
 */
template<typename T>
MPUNode<T>::~MPUNode(void) {
  if (estimator_) {
    delete estimator_;
  }
}

#pragma mark -
#pragma mark Getters and Setters

/*
 * @name  Level
 * @fn  std::size_t Level(void)
 * @brief Compute the level of the current node from its location hash
 * @return level    The computed level
 */
template<typename T>
std::size_t MPUNode<T>::Level(void) const {
  if (location_hash_ <= 1) {
    return 0;
  } else {
    size_t level = 0;
    uint64_t shift = location_hash_;
    while (shift > 1) {
      level += 1;
      shift = shift >> 3;
    }
    return level;
  }
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Approximate
 * @fn  void Approximate(const std::vector<LTS5::Vector3<T> >& vertices,
 *                       const std::vector<LTS5::Vector3<T> >& normals,
 *                       const T& Ri);
 * @brief Compute the implicite approximation of the mesh defined by vertices and normals
 * @param[in]  vertices
 * @param[in]  normals
 * @param[in]  Ri, support radius for the approximation
 */
template<typename T>
void MPUNode<T>::Approximate(const std::vector<LTS5::Vector3<T> >& vertices,
                             const std::vector<LTS5::Vector3<T> >& normals,
                             const T& Ri) {
  this->Ri_ = Ri;

  double max_deviation = ComputeMeanNormalDeviation(normals);
  if (max_deviation > Constants<T>::PI_2) {
    estimator_ = new Quadric<T>();
  } else {
    estimator_ = new BivariatePoly<T>();
  }

  this->epsiloni_ = estimator_->Fit(vertices, normals, bbox_, Ri);
}

/**
 * @name  Evaluate
 * @fn  void Evaluate(const Vector3<T>& x,  T* SwQ, T* Sw) const
 * @brief Evaluate SwQ and Sw in that node, given x
 * @param[in] x     Querry point x
 * @param[out] SwQ  The sum of weighted values Q
 * @param[out] Sw   The sum of weights
 */
template<typename T>
void MPUNode<T>::Evaluate(const Vector3<T>& x,  T* SwQ, T* Sw) const {
  estimator_->Evaluate(x, SwQ, Sw);
}

/*
 * @name  EvaluateSquare
 * @fn  void EvaluateSquare(const Vector3<T>& x, T* SwQ2, T* Sw) const
 * @brief Evaluate SwQ2 (square of Q) and Sw in that node, given x
 * @param[in] x      Querry point x
 * @param[out] SwQ2  The sum of weighted values Q
 * @param[out] Sw    The sum of weights
 */
template<typename T>
void MPUNode<T>::EvaluateSquare(const Vector3<T>& x, T* SwQ2, T* Sw) const {
  estimator_->EvaluateSquare(x, SwQ2, Sw);
}

/*
 * @name  EvaluateGrad
 * @fn  void EvaluateGrad(const Vector3<T>& x, Vector3<T>* SwGradQ, T* Sw) const
 * @brief Evaluate gradient of that node, given x
 * @param[in]  x        position where to evaluate gradient
 * @param[out] SwGradQ  Sum of weighted gradients
 * @param[out] Sw       Sum of weights
 */
template<typename T>
void MPUNode<T>::EvaluateGrad(const Vector3<T>& x, Vector3<T>* SwGradQ, T* Sw) const {
  estimator_->EvaluateGrad(x, SwGradQ, Sw);
}

/*
 * @name  EvaluateGradSquare
 * @fn  void EvaluateGradSquare(const Vector3<T>& x, Vector3<T>* SwGradQ2,
 *                              T* Sw) const
 * @brief Evaluate gradient of that node, given x
 * @param[in]  x         position where to evaluate gradient
 * @param[out] SwGradQ2  Sum of weighted gradients
 * @param[out] Sw        Sum of weights
 */
template<typename T>
void MPUNode<T>::EvaluateGradSquare(const Vector3<T>& x, Vector3<T>* SwGradQ2,
                                    T* Sw) const {
  estimator_->EvaluateGradSquare(x, SwGradQ2, Sw);
}

#pragma mark -
#pragma mark Declaration

/** Float type MPUNode */
template class MPUNode<float>;
/** Float type MPUNode */
template class MPUNode<double>;

} // namespace LTS5
