/**
 *  @file   bivariate_quadratic_poly.hpp
 *  @brief  Bivariate Quadratic Polynomial approximation declaration
 *  @ingroup geometry
 *
 *  @author Gabriel Cuendet
 *  @date   20/04/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef __LTS5_BIVARIATE_QUADRATIC_POLY__
#define __LTS5_BIVARIATE_QUADRATIC_POLY__

#include <stdio.h>
#include <vector>
#include <utility>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/geometry/weight.hpp"
#include "lts5/geometry/shape_estimator_base.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BivariatePoly
 *  @brief  Bivariate Quadratic Polynomial approximation class
 *  @author Gabriel Cuendet
 *  @date   20/04/2016
 *  @ingroup geometry
 */
template<typename T>
class LTS5_EXPORTS BivariatePoly : public ShapeEstimatorBase<T> {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  BivariatePoly
   * @fn  BivariatePoly(void)
   * @brief Default constructor of the class
   */
  BivariatePoly(void) {}

  /**
   * @name  BivariatePoly
   * @fn  BivariatePoly(const BivariatePoly& other) = delete
   * @brief Copy constructor, disable
   */
  BivariatePoly(const BivariatePoly& other) = delete;

  /**
   * @name  operator=
   * @fn  BivariatePoly& operator=(const BivariatePoly& rhs) = delete
   * @brief Assignment operator, disable
   */
  BivariatePoly& operator=(const BivariatePoly& rhs) = delete;

  /**
   * @name  Clone
   * @fn  BivariatePoly* Clone(void);
   * @brief Deep copy method
   */
  BivariatePoly* Clone(void);

#pragma mark -
#pragma mark Getters and Setters

  /**
   * @name  coefficients
   * @fn  const std::vector<T>& coefficients(void) const
   * @brief Return const reference to the vector of coefficients
   */
  const std::vector<T>& coefficients(void) const {
    return coefficients_;
  }

#pragma mark -
#pragma mark Usage

  /**
   * @name  Fit
   * @fn  T Fit(const std::vector<LTS5::Vector3<T> >& points,
   *            const std::vector<LTS5::Vector3<T> >& normals,
   *            const AABB& bbox)
   * @brief Fit the quadratic poly to the input points using least squares
   * @param[in] points   Vertices within the support radius of the cell
   * @param[in] normals  Normals of the vertices
   * @param[in] bbox     Bounding box of the current cell
   * @return    error    fitting error
   */
  T Fit(const std::vector<LTS5::Vector3<T> >& points,
        const std::vector<LTS5::Vector3<T> >& normals,
        const AABB<T>& bbox);

  /**
   * @name  Fit
   * @fn  T Fit(const std::vector<LTS5::Vector3<T> >& points,
   *            const std::vector<LTS5::Vector3<T> >& normals,
   *            const AABB& bbox, const T& R)
   * @brief Fit the quadratic poly to the input points using least squares
   * @param[in] points   Vertices within the support radius of the cell
   * @param[in] normals  Normals of the vertices
   * @param[in] bbox     Bounding box of the current cell
   * @param[in] R        Radius of the support sphere (used for the weights)
   * @return    error    fitting error
   */
  T Fit(const std::vector<LTS5::Vector3<T> >& points,
        const std::vector<LTS5::Vector3<T> >& normals,
        const AABB<T>& bbox, const T& R);

  /**
   * @name  Evaluate
   * @fn  void Evaluate(const LTS5::Vector3<T>& x)
   * @brief Evalute the value of x, given the quadratic approximation
   * @param[in]  x       Point to evaluate
   * @param[out] SwQ     Sum Qi(x)wi(x)
   * @param[out] Sw      Sum wi(x)
   */
  void Evaluate(const LTS5::Vector3<T>& x, T* SwQ, T* Sw);

  /**
   * @name  EvaluateSquare
   * @fn  virtual void EvaluateSquare(const LTS5::Vector3<T>& x, T* SwQ2, T* Sw) = 0
   * @brief Evalute the squared value of x, given the approximation
   * @param[in]  x     Point to evaluate
   * @param[out] SwQ2  Sum Qi(x)^2 wi(x)
   * @param[out] Sw    Sum wi(x)
   */
  void EvaluateSquare(const LTS5::Vector3<T>& x, T* SwQ2, T* Sw);

  /**
   * @name  EvaluateGrad
   * @fn  void EvaluateGrad(const LTS5::Vector3<T>& x, LTS5::Vector3<T>* SwGradQ,
   *                        T* Sw)
   * @brief Evaluate the value of the gradient of Q at x
   * @param[in]  x        Point to evaluate
   * @param[out] SwGradQ  Sum Grad(Qi)(x)wi(x)
   * @param[out] Sw       Sum wi(x)
   */
  void EvaluateGrad(const LTS5::Vector3<T>& x, LTS5::Vector3<T>* SwGradQ,
                    T* Sw);

  /**
   * @name  EvaluateGradSquare
   * @fn  virtual void EvaluateGradSquare(const LTS5::Vector3<T>& x,
   *                                      LTS5::Vector3<T>* SwGradQ2, T* Sw) = 0
   * @brief Evaluate the gradient of Q^2 at x
   * @param[in]  x         Point to evaluate
   * @param[out] SwGradQ2  Sum Grad(Q_i^2)(x) wi(x)
   * @param[out] Sw        Sum wi(x)
   */
  void EvaluateGradSquare(const LTS5::Vector3<T>& x,
                          LTS5::Vector3<T>* SwGradQ2, T* Sw);

  /**
   * @name  EvaluateGradQ
   * @fn  void EvaluateGradQ(const LTS5::Vector3<T>& x,
   *                         LTS5::Vector3<T>* grad)
   * @brief Evalute the gradient of x, given the quadratic approximation
   * @param[in]  x       Point to evaluate
   * @param[out] grad    Gradient of the quadric evaluated at x
   */
  void EvaluateGradQ(const LTS5::Vector3<T>& x, LTS5::Vector3<T>* grad);

  /**
   * @name  EvaluateGradQ
   * @fn  LTS5::Vector3<T> EvaluateGradQ(const LTS5::Vector3<T>& x)
   * @brief Evalute the gradient of x, given the quadratic approximation
   * @param[in]  x       Point to evaluate
   * @return     grad    Gradient of the quadric evaluated at x
   */
  LTS5::Vector3<T> EvaluateGradQ(const LTS5::Vector3<T>& x);

  /**
   * @name  EvaluateQ
   * @fn  T EvaluateQ(const LTS5::Vector3<T>& x)
   * @brief Evalute the value of x, given the quadric approximation
   * @param[in]  x       Point to evaluate
   * @return     val     Value of the quadric evaluated at x
   */
  T EvaluateQ(const LTS5::Vector3<T>& x);

 private:

#pragma mark -
#pragma mark Private methods

  /**
   * @name ComputeWeights
   * @fn void ComputeWeights(const std::vector<LTS5::Vector3<T> >& points,
   *                         cv::Mat* weights)
   * @brief Compute the weights vector
   * @param[in]     points       Points of the mesh within the support ball
   * @param[out]    weights      Nx1 opencv Mat with the weights
   */
  void ComputeWeights(const std::vector<LTS5::Vector3<T> >& points,
                      cv::Mat* weights);

  /**
   * @name ComputeLocalCoords
   * @fn std::vector<T> ComputeLocalCoords(const LTS5::Vector3<T>& x)
   * @brief Compute the local coordinates u,v
   * @param[in]     x       Point for which to compute u,v coordinates
   * @return        coord   vector of local coordinates
   */
  LTS5::Vector3<T> ComputeLocalCoords(const LTS5::Vector3<T>& x);

  /**
   * @name ComputeMeanNormal
   * @fn void ComputeMeanNormal(const std::vector<LTS5::Vector3<T>& points,
   *                            const std::vector<LTS5::Vector3<T>& normals)
   * @brief Compute the mean normal from the points and normals of the cell
   * @param[in]  points   vector of points within the support ball
   * @param[in]  normals  vector of their normals
   */
  void ComputeMeanNormal(const std::vector<LTS5::Vector3<T> >& points,
                         const std::vector<LTS5::Vector3<T> >& normals);

  /**
   * @name  ComputeBasisChange
   * @fn  void ComputeBasisChange(void)
   * @brief Compute the basis change from global coordinates to local coordinates
   */
  void ComputeBasisChange(void);

  /**
   * @name BuildLinearSystem
   * @fn void BuildLinearSystem(const std::vector<LTS5::Vector3<T> >& points,
   *                            cv::Mat* A, cv::Mat V*)
   * @brief From the points on the surface build the linear system Ax=V
   * @param[in]     points       Points of the mesh within the support ball
   * @param[out]    A            Local coordinates matrix
   * @param[out]    V            Local w coordinates of the points
   */
  void BuildLinearSystem(const std::vector<LTS5::Vector3<T> >& points,
                         cv::Mat* A, cv::Mat* V);

  /** Coefficients of the quadratic polynomial, A, B, C, D, E, F */
  std::vector<T> coefficients_;
  /** Weight function */
  LTS5::Weight::BSpline<T> w_;
  /** Normalization factor: sum of the weights of all the points */
  T Sw_;
  /** Cell center*/
  LTS5::Vector3<T> center_;
  /** Mean weighted normal */
  LTS5::Vector3<T> mean_normal_;
  /** 3x3 matrix defining the change of basis from (x,y,z) to (u,v,w) */
  cv::Mat basis_change_;
  /** Support radius of the cell */
  T radius_;
};
} // namespace LTS5
#endif /* __LTS5_BIVARIATE_QUADRATIC_POLY__ */
