/**
 *  @file   octree_node.hpp
 *  @brief  Node for OCTree building, adapted from CGAL
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   03/03/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_OCTREE_NODE__
#define __LTS5_OCTREE_NODE__

#include <vector>
#include <limits>

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/aabb.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @struct  OCTreeNode
 * @brief   Node for OCTree
 * @author  Christophe Ecabert
 * @date    03/03/16
 * @ingroup geometry
 */
template< typename T>
struct LTS5_EXPORTS OCTreeNode {
  /** Max index value */
  static const std::size_t max_size_t = std::numeric_limits<std::size_t>::max();
  /** Node level */
  int level_;
  /** Center of the bounding volume */
  Vector3<T> center_;
  /** Half-edge of the bounding volume */
  Vector3<T> half_edge_;
  /** Bounding volume */
  AABB<T> bbox_;
  /** First index in the map */
  std::size_t first_;
  /** Last index in the map */
  std::size_t last_;
  /** Child nodes */
  OCTreeNode* child_[8];

#pragma mark -
#pragma mark Initialization

  /**
   * @name  OCTreeNode
   * @fn    OCTreeNode()
   * @brief Constructor
   */
  OCTreeNode() : level_(-1), child_{nullptr} {}

  /**
   * @name  OCTreeNode
   * @fn    OCTreeNode(const std::size_t first,
                       const std::size_t last,
                       const AABB<T>& bbox,
                       const int level)
   * @brief Constructor
   * @param[in] first     Entry index in the map
   * @param[in] last      Exit index in the map
   * @param[in] bbox      Bounding volume of the node
   * @param[in] level     Node tree level (i.e. depth)
   */
  OCTreeNode(const std::size_t first,
             const std::size_t last,
             const AABB<T>& bbox,
             const int level) : level_(level),
                                bbox_(bbox),
                                first_(first),
                                last_(last),
                                child_{nullptr} {
    center_ = (bbox.min_ + bbox.max_) * T(0.5);
    half_edge_ = (bbox.max_ - bbox.min_) * T(0.5);
  }

  /**
   * @name  OCTreeNode
   * @fn    OCTreeNode(const OCTreeNode& other) = delete
   * @brief Copy constructor, disable
   */
  OCTreeNode(const OCTreeNode& other) = delete;

  /**
   * @name  operator=
   * @fn  OCTreeNode& operator=(const OCTreeNode& rhs) = delete
   * @brief Assignment operator, disable
   */
  OCTreeNode& operator=(const OCTreeNode& rhs) = delete;

  /**
   * @name  ~OCTreeNode
   * @fn  ~OCTreeNode()
   * @brief Destructor
   */
  ~OCTreeNode() = default;

#pragma mark -
#pragma mark Accessor

  /**
   * @name  IsLeaf
   * @fn      bool IsLeaf() const
   * @brief   Check if this node is a leaf
   * @return  Return true if it is a leaf, false otherwise
   */
  bool IsLeaf() const {
    for (int i = 0; i < 8; ++i) {
      if (child_[i] != nullptr) {
        return false;
      }
    }
    return true;
  }

  /**
   * @name  Size
   * @fn    std::size_t Size() const
   * @brief Compute the size of this node, meaning the number of objects inside
   * @return  Number of objectes inside the node
   */
  std::size_t Size() const {
    if (first_ == max_size_t || last_ == max_size_t) {
      return 0;
    } else {
      return (last_ - first_ + 1);
    }
  }
};

}  // namespace LTS5

#endif  // __LTS5_OCTREE_NODE__
