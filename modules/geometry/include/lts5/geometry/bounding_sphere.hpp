/**
 *  @file   bounding_sphere.hpp
 *  @brief  Tree storing holding primitive bounding sphere
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   04/01/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_GEOMETRY_BOUNDING_SPHERE__
#define __LTS5_GEOMETRY_BOUNDING_SPHERE__

#include <limits>
#include <algorithm>
#include <utility>
#include <stack>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/math/compare_type.hpp"
#include "lts5/geometry/point_set.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<int NDim, typename T>
using PointType = typename Point<NDim, T>::Type;

/**
 * @struct  BarycentricCoordinate
 * @brief   Represent barycentric coordinate in a triangle
 * @author  Christophe Ecabert
 * @date    04/01/2020
 * @tparam  T Data type
 * @ingroup geometry
 */
template<typename T>
struct LTS5_EXPORTS BarycentricCoordinate {

  /**
   * @name  BarycentricCoordinate
   * @fn    BarycentricCoordinate
   * @brief Constructor
   */
  BarycentricCoordinate() : BarycentricCoordinate(T(-1.0), T(-1.0), T(-1.0)) {}

  /**
   * @name  BarycentricCoordinate
   * @fn    BarycentricCoordinate(T s, T t, T st)
   * @brief Constructor
   * @param[in] s   First element
   * @param[in] t   Second element
   * @param[in] st  Sum of both or 0 if degenerated triangle
   */
  BarycentricCoordinate(T s, T t, T st) : s(s), t(t), st(st) {}

  /** First element */
  T s;
  /** Second element */
  T t;
  /** Sum of both element */
  T st;
};

/**
 * @struct  ClosestPointSurface
 * @brief   Information about closest point found on surface
 * @author  Christophe Ecabert
 * @date    04/01/2020
 * @ingroup geometry
 * @tparam T    Data type
 */
template<typename T>
struct LTS5_EXPORTS ClosestPointSurface {
  /** Tuple of index */
  using Indexes = std::pair<int, int>;

  /**
   * @enum  Type
   * @brief List of possible type of closest point
   */
  enum Type {
    /** On Point */
    kOnPoint,
    /** On line */
    kOnLine,
    /** In triangle */
    kInTriangle
  };

  /**
   * @name  ClosestPointSurface
   * @fn    ClosestPointSurface()
   * @brief Default constructor
   */
  ClosestPointSurface() : ClosestPointSurface(std::numeric_limits<T>::max(),
                                              {T(-1.0), T(-1.0), T(-1.0)},
                                              kOnPoint,
                                              {T(0.0), T(0.0), T(0.0)},
                                              {-1, -1}) {
  }

  /**
   * @name  ClosestPointSurface
   * @brief Constructor
   * @param[in] distance    Distane to surface
   * @param[in] point       Points
   * @param[in] type        Type of points
   * @param[in] bcoords     Barycentric coordinates
   * @param[in] idx         Tuple of index
   */
  ClosestPointSurface(const T& distance,
                      const PointType<3, T>& point,
                      const Type& type,
                      const BarycentricCoordinate<T>& bcoords,
                      const Indexes& idx) : dist(distance),
                                            dist2(distance * distance),
                                            pts(point),
                                            type(type),
                                            bcoord(bcoords),
                                            idx(idx),
                                            tri_idx(-1) {
  }

  /** Distance to surface */
  T dist;
  /** Squared distance to surface */
  T dist2;
  /** Point */
  PointType<3, T> pts;
  /** Type */
  Type type;
  /** Barycentric coordinates */
  BarycentricCoordinate<T> bcoord;
  /** Indexes */
  Indexes idx;
  /** Triangle index */
  size_t tri_idx;
};


template<typename T>
void ClosestPointOnLine(const PointType<3, T>& p,
                        const PointType<3, T>& pt1,
                        const PointType<3, T>& pt2,
                        T* dist,
                        PointType<3, T>* nearest,
                        T* bcoord) {
  auto dir = pt2 - pt1;   // Line direction
  T len2 = dir.SquaredNorm();
  if (len2 < std::numeric_limits<T>::epsilon()) {
    *nearest = (pt1 + pt2) * T(0.5);
    *dist = (p - *nearest).Norm();
    *bcoord = T(0.5);
  } else {
    T s = dir * (p - pt1);
    if (s < T(0.0)) {
      *dist = (p - pt1).Norm();
      *nearest = pt1;
      *bcoord = T(0.0);
    } else if (s > len2) {
      *dist = (p - pt2).Norm();
      *nearest = pt2;
      *bcoord = T(1.0);
    } else {
      *bcoord = s / len2;
      *nearest = pt1 + dir * (*bcoord);
      *dist = (p - *nearest).Norm();
    }
  }
}




/**
 * @class   Triangle
 * @brief   Represent a triangle
 * @author  Christophe Ecabert
 * @date    04/01/2020
 * @tparam  NDim    Number of dimensions
 * @tparam  T Data type
 * @ingroup geometry
 */
template<int NDim, typename T>
class LTS5_EXPORTS Triangle {
 public:
  /** Point type */
  using PtType = PointType<NDim, T>;
  /** Edge type */
  using EdgeType = PointType<NDim, T>;
  /** Barycentric coordinate type */
  using BCoords = BarycentricCoordinate<T>;

  /**
   * @name  Triangle
   * @fn    Triangle(const PtType& a, const PtType& b, const PtType& c)
   * @brief Constructor
   * @param[in] a   First vertex
   * @param[in] b   Second vertex
   * @param[in] c   Third vertex
   */
  Triangle(const PtType& a, const PtType& b, const PtType& c) :
          a_(a), b_(b), c_(c),
          ab_(b - a), ac_(c - a), bc_(c - b),
          n_(ab_ ^ ac_) {
    // Degenerated ?
    degenerated_ = 0;
    if (n_.Norm() == T(0.0)) {
      degenerated_ = (a_ == b_ && b_ == c_) ? 2 : 1;
    }
    // Longest side
    longest_side_ = 0;
    if (ab_.SquaredNorm() > ac_.SquaredNorm()) {
      longest_side_ = ab_.SquaredNorm() > bc_.SquaredNorm() ? 0 : 2;
    } else {
      longest_side_ = ac_.SquaredNorm() > bc_.SquaredNorm() ? 1 : 2;
    }
  }

  BCoords BarycentricCoordinates(const PtType& p) const {
    if (degenerated_ == 2) {
      return BCoords(T(1.0), T(0.0), T(0.0));
    } else if (degenerated_ == 1) {
      if (longest_side_ == 0) {
        T s = ab_.Normalized() * (p - a_);
        T coordinate = s / ab_.Norm();
        return BCoords(T(1.0) - coordinate, coordinate, T(0.0));
      } else if (longest_side_ == 1) {
        T s = ac_.Normalized() * (p - a_);
        T cooridnate = s / ac_.Norm();
        return BCoords(T(1.0) - cooridnate, T(0.0), cooridnate);
      } else {
        T s = bc_.Normalized() * (p - b_);
        T cooridnate = s / bc_.Norm();
        return BCoords(T(0.0), T(1.0) - cooridnate, cooridnate);
      }
    } else {
      PtType positionRelativeToA = a_ - p;
      T ab2 = ab_ * ab_;
      T abac = ab_ * ac_;
      T ac2 = ac_ * ac_;
      T xab = positionRelativeToA * ab_;
      T xac = positionRelativeToA * ac_;
      T div = ab2 * ac2 - abac * abac;
      T s = (abac * xac - ac2 * xab) / div;
      T t = (abac * xab - ab2 * xac) / div;
      T st = s + t;
      return BCoords(s, t, st);
    }
  }

  /**
   * @name  ClosestPoint
   * @fn    ClosestPointSurface<T> ClosestPoint(const PtType& pt) const
   * @brief Finds closest point to triangle.
   * @see http://www.geometrictools.com/Documentation/DistancePoint3Triangle3.pdf
   * @param[in] pt  Query point
   * @return    Closest point information
   */
  ClosestPointSurface<T> ClosestPoint(const PtType& pt) const {
    using CPSType = typename ClosestPointSurface<T>::Type;

    if ((std::abs(ab_[0]) +
         std::abs(ab_[1]) +
         std::abs(ab_[2])) < std::numeric_limits<T>::epsilon()) {
      // Degenerated case where a and b are the same points
      T dist, bc;
      PtType nearest;
      ClosestPointOnLine(pt, c_, a_, &dist, &nearest, &bc);
      return ClosestPointSurface<T>(dist,
                                    nearest,
                                    CPSType::kOnLine,
                                    {bc, T(0.0), T(0.0)},
                                    {2, -1});
    } else if ((std::abs(ac_[0]) +
                std::abs(ac_[1]) +
                std::abs(ac_[2])) < std::numeric_limits<T>::epsilon()) {
      // degenerated case where a and c are the same points
      T dist, bc;
      PtType nearest;
      ClosestPointOnLine(pt, a_, b_, &dist, &nearest, &bc);
      return ClosestPointSurface<T>(dist,
                                    nearest,
                                    CPSType::kOnLine,
                                    {bc, T(0.0), T(0.0)},
                                    {0, -1});
    } else {
      // regular case bcoords == (s, t, st)
      auto bcoords = this->BarycentricCoordinates(pt);

      /* Determine Region that the projected point is in by looking at a and b.
      //     t
      //
      //     ^
      // \ 2 |
      //  \  |
      //   \ |
      //    \|
      //     C
      //     |\
      //     | \
      //     |  \
      //     |   \
      //   3 | 0  \   1
      //     |     \
      //     |      \
      // ----A-------B----------> s
      //     |        \
      //   4 |    5    \   6
      //     |          \
      // Then calculate the distance to the nearest point or line segment.
       */
      if (bcoords.st < T(1.0)) {
        if (bcoords.s > T(0.0)) {
          if (bcoords.t > T(0.0)) {
            // region 0
            auto nearest = a_ + ab_ * bcoords.s + ac_ * bcoords.t;
            T dist = (nearest - pt).Norm();
            return ClosestPointSurface<T>(dist,
                                          nearest,
                                          CPSType::kInTriangle,
                                          bcoords,
                                          {-1, -1});
          } else {
            // region 5
            T dist, bc;
            PtType nearest;
            ClosestPointOnLine(pt, a_, b_, &dist, &nearest, &bc);
            return ClosestPointSurface<T>(dist,
                                          nearest,
                                          CPSType::kOnLine,
                                          {bc, T(0.0), T(0.0)},
                                          {0, -1});
          }
        } else {  // s < 0.0
          if (bcoords.t > T(0.0)) {
            // region 3
            T dist, bc;
            PtType nearest;
            ClosestPointOnLine(pt, c_, a_, &dist, &nearest, &bc);
            return ClosestPointSurface<T>(dist,
                                          nearest,
                                          CPSType::kOnLine,
                                          {bc, T(0.0), T(0.0)},
                                          {2, -1});
          } else {
            // region 4
            T dist = (a_ - pt).Norm();
            return ClosestPointSurface<T>(dist,
                                          a_,
                                          CPSType::kOnPoint,
                                          {T(0.0), T(0.0), T(0.0)},
                                          {0, -1});
          }
        }
      } else {  // st > 1.0
        if (bcoords.s > T(0.0)) {
          if (bcoords.t > T(0.0)) {
            // region 1
            T dist, bc;
            PtType nearest;
            ClosestPointOnLine(pt, b_, c_, &dist, &nearest, &bc);
            return ClosestPointSurface<T>(dist,
                                          nearest,
                                          CPSType::kOnLine,
                                          {bc, T(0.0), T(0.0)},
                                          {1, -1});
          } else {
            // region 6
            T dist = (b_ - pt).Norm();
            return ClosestPointSurface<T>(dist,
                                          b_,
                                          CPSType::kOnPoint,
                                          {T(1.0), T(0.0), T(0.0)},
                                          {1, -1});
          }
        } else {
          // region 2
          T dist = (c_ - pt).Norm();
          return ClosestPointSurface<T>(dist,
                                        c_,
                                        CPSType::kOnPoint,
                                        {T(0.0), T(1.0), T(0.0)},
                                        {2, -1});
        }
      }
    }
  }

  /** First vertex */
  PtType a_;
  /** Second vertex */
  PtType b_;
  /** Third vertex */
  PtType c_;
  /** AB edge */
  EdgeType ab_;
  /** AC edge */
  EdgeType ac_;
  /** BC edge */
  EdgeType bc_;
  /** Normal */
  EdgeType n_;
  /** Number of degenerated vertex */
  int degenerated_;
  /** Longest side indicator => 0: ab, 1: ac, 2: bc */
  int longest_side_;
};

/**
 * @name    SmallestSphere
 * @fn  static void SmallestSphere(const PointType<NDim, T>& pt1,
                           const PointType<NDim, T>& pt2,
                           PointType<NDim, T>* center,
                           T* radius)
 * @brief Compute parameters for sphere englobing a line
 * @param[in] pt1   First point
 * @param[in] pt2   Second point
 * @param[out] center   Center
 * @param[out] radius   Radius
 * @tparam NDim     Number of dimensions
 * @tparam T        Data type
 */
template<int NDim, typename T>
static void SmallestSphere(const PointType<NDim, T>& pt1,
                           const PointType<NDim, T>& pt2,
                           PointType<NDim, T>* center,
                           T* radius) {
  *center = (pt1 + pt2) * T(0.5);
  *radius = std::max((pt1 - center).Norm(),
                     (pt2 - center).Norm());
}

/**
 * @name    SmallestSphere
 * @fn  static void SmallestSphere(const Vector3<T>& a,
                           const Vector3<T>& b,
                           const Vector3<T>& c,
                           Vector3<T>* center,
                           T* radius)
 * @brief Compute parameters for sphere englobing 3 points (i.e. triangle)
 * @param[in] a   First point
 * @param[in] b   Second point
 * @param[in] c   Third point
 * @param[out] center   Center
 * @param[out] radius   Radius
 * @tparam T        Data type
 */
template<typename T>
static void SmallestSphere(const Vector3<T>& a,
                           const Vector3<T>& b,
                           const Vector3<T>& c,
                           Vector3<T>* center,
                           T* radius) {
  // Init output
  *center = a;
  *radius = T(1.0);

  // Compute eddes
  auto ab = b - a;
  auto ac = c - a;
  auto bc = c - b;
  // Mid-edges
  auto aMb = (a + b) * T(0.5);
  auto aMc = (a + c) * T(0.5);
  // handle degenerated cases
  if (ab.SquaredNorm() < std::numeric_limits<T>::epsilon()) {
    *center = aMc;
    *radius = std::max((aMc - a).Norm(),
                       (aMc - c).Norm());
  } else if (ac.SquaredNorm() < std::numeric_limits<T>::epsilon() ||
             bc.SquaredNorm() < std::numeric_limits<T>::epsilon()) {
    *center = aMb;
    *radius = std::max((aMb - a).Norm(),
                       (aMb - b).Norm());
  } else if (CompareType<T>(std::abs(ab.Normalized() * ac.Normalized())) ==
             CompareType<T>(T(1.0))) {
    // all points on same line
    using SegInfo = std::pair<std::string, T>;
    std::vector<SegInfo> seg{{"ab", ab.Norm()},
                             {"ac", ac.Norm()},
                             {"bc", bc.Norm()}};
    auto it = std::max_element(seg.begin(),
                               seg.end(),
                               [](const SegInfo& a, const SegInfo& b) {
                                 return a.second < b.second;
                               });
    if (it->first == "ab") {
      *center = aMb;
      *radius = std::max((aMb - a).Norm(),
                         (aMb - b).Norm());
    } else if (it->first == "ac") {
      *center = aMc;
      *radius = std::max((aMc - a).Norm(),
                         (aMc - c).Norm());
    } else {  // BC
      auto mid = (b + c) * T(0.5);
      *center = mid;
      *radius = std::max((mid - b).Norm(),
                         (mid - c).Norm());
    }
  } else {
    // non degenerated case
    auto triangleNormal = ab ^ ac;
    auto normalToAB = triangleNormal ^ ab;
    auto normalToAC = triangleNormal ^ ac;

    auto m = aMb - aMc;

    {
      T d0 = normalToAC[0] * normalToAB[1] - normalToAC[1] * normalToAB[0];
      T d1 = normalToAC[0] * normalToAB[2] - normalToAC[2] * normalToAB[0];
      T d2 = normalToAC[1] * normalToAB[2] - normalToAC[2] * normalToAB[1];
      T beta1 = T(0.0);
      if ((std::abs(d0) >= std::abs(d1)) &&
          (std::abs(d0) >= std::abs(d2))) {
        beta1 = (m[0] * normalToAB[1] - m[1] * normalToAB[0]) / d0;
      } else if ((std::abs(d1) >= std::abs(d0)) &&
                 (std::abs(d1) >= std::abs(d2))) {
        beta1 = (m[0] * normalToAB[2] - m[2] * normalToAB[0]) / d1;
      } else {
        beta1 = (m[1] * normalToAB[2] - m[2] * normalToAB[1]) / d2;
      }
      *center = aMc + normalToAC * beta1;
    }

    {
      // check weather calculated center is in the triangle...
      T d0 = ab[0] * ac[1] - ab[1] * ac[0];
      T d1 = ab[0] * ac[2] - ab[2] * ac[0];
      T d2 = ab[1] * ac[2] - ab[2] * ac[1];
      auto p = *center - a;

      T beta2 = T(0.0);
      if ((std::abs(d0) >= std::abs(d1)) &&
          (std::abs(d0) >= std::abs(d2))) {
        beta2 = (p[1] * ab[0] - p[0] * ab[1]) / d0;
      } else if ((std::abs(d1) >= std::abs(d0)) &&
                 (std::abs(d1) >= std::abs(d2))) {
        beta2 = (p[2] * ab[0] - p[0] * ab[2]) / d1;
      } else {
        beta2 = (p[2] * ab[1] - p[1] * ab[2]) / d2;
      }

      T alpha2 = T(0.0);
      if ((std::abs(ab[0]) >= std::abs(ab[1])) &&
          (std::abs(ab[0]) >= std::abs(ab[2]))) {
        alpha2 = (p[0] - beta2 * ac[0]) / ab[0];
      } else if ((std::abs(ab[1]) >= std::abs(ab[0])) &&
                 (std::abs(ab[1]) >= std::abs(ab[1]))) {
        alpha2 = (p[1] - beta2 * ac[1]) / ab[1];
      } else {
        alpha2 = (p[2] - beta2 * ac[2]) / ab[2];
      }

      if (alpha2 < 0 || beta2 < 0 || alpha2 + beta2 > 1) {
        // center is outside triangle
        T r1 = ab.Norm();
        T r2 = ac.Norm();
        T r3 = bc.Norm();
        if (r1 > r2) {
          *center = r1 > r3 ? aMb : (b + c) * T(0.5);
        } else {  // r2 >= r1
          *center = r2 > r3 ? aMc : (b + c) * T(0.5);
        }
        *radius = std::max((*center - a).Norm(),
                           std::max((*center - b).Norm(),
                                    (*center - c).Norm()));
      } else {
        // center is in the triangle
        *radius = std::max((*center - a).Norm(),
                           std::max((*center - b).Norm(),
                                    (*center - c).Norm()));
      }
    }
  }
}


/**
 * @class   Sphere
 * @brief   3D Sphere
 * @author  Christophe Ecabert
 * @date    04/01/2020
 * @tparam  T Data type
 * @ingroup geometry
 */
template<typename T>
class LTS5_EXPORTS Sphere {
 public:
  /** Point type */
  using PtType = PointType<3, T>;

  /**
   * @name  FromPoint
   * @fn    static Sphere FromPoint(const PtType& pt, T radius = T(1e-6))
   * @brief Create a tiny sphere surrounding the given point `pt`.
   * @param[in] pt      Point to encapsulate
   * @param[in] radius  Tiny radius
   * @return    Sphere
   */
  static Sphere FromPoint(const PtType& pt, T radius = T(1e-6)) {
    return Sphere(pt, radius);
  }

  /**
   * @name  FromLine
   * @fn    static Sphere FromLine(const PtType& pt1, const PtType& pt2)
   * @brief Create a sphere encapsulating a line between `pt1` and `pt2`.
   * @param[in] pt1 First point
   * @param[in] pt2 Second point
   * @return Sphere
   */
  static Sphere FromLine(const PtType& pt1, const PtType& pt2) {
    PtType center;
    T radius;
    SmallestSphere<T>(pt1, pt2, &center, &radius);
    return Sphere(center, radius);
  }

  /**
   * @name  FromTriangle
   * @fn    static Sphere FromTriangle(const Triangle<NDim, T>& tri)
   * @brief Create a sphere that encapsulate the given triangle.
   * @param[in] tri Triangle
   * @return    Shere
   */
  static Sphere FromTriangle(const Triangle<3, T>& tri) {
    PtType center;
    T radius;
    SmallestSphere<T>(tri.a_, tri.b_, tri.c_, &center, &radius);
    return Sphere(center, radius);
  }

  /**
   * @name  Sphere
   * @fn    Sphere()
   * @brief Constructor
   */
  Sphere() = default;

  /**
   * @name  Sphere
   * @fn    Sphere(const PtType& center, T radius)
   * @brief Constructor
   * @param[in] center      Shere center
   * @param[in] radius      Radius
   */
  Sphere(const PtType& center, T radius) : center_(center),
                                            radius_(radius) {
  }

  /**
   * @name  operator<<
   * @fn  friend std::ostream& operator<<(std::ostream& os,
   *                                      const Sphere& sphere)
   * @brief Output operator
   * @param[in] os      Output stream
   * @param[in] sphere  Sphere to print
   * @return  Output stream
   */
  friend std::ostream& operator<<(std::ostream& os, const Sphere& sphere) {
    os << "c : " << sphere.center_ << std::endl;
    os << "r : " << sphere.radius_ << std::endl;
    return os;
  }

  /** Center */
  PtType center_;
  /** Radius */
  T radius_;
};


/**
 * @class   BoundingSphereTree
 * @brief   Spatial partitioning based on Sphere for 3D points.
 * @author  Christophe Ecabert
 * @date    04/01/2020
 * @ingroup geometry
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS BoundingSphereTree {
 public:
  /** Point type */
  using PtType = typename Point<3, T>::Type;
  /** Triangle type */
  using TriType = Vector3<int>;

  /**
   * @name  BoundingSphereTree
   * @fn    BoundingSphereTree()
   * @brief Constructor
   */
  BoundingSphereTree();

  /**
   * @name  ~BoundingSphereTree
   * @fn    ~BoundingSphereTree()
   * @brief Destructor
   */
  ~BoundingSphereTree();


  /**
   * @name  Build
   * @fn    void Build(const std::vector<PtType>& points,
                        const std::vector<TriType>& triangles)
   * @brief Build internal structure for triangle mesh
   * @param[in] points      List of points
   * @param[in] triangles   List of triangles
   */
  void Build(const std::vector<PtType>& points,
             const std::vector<TriType>& triangles);

  /**
   * @name  ClosestPointOnSurface
   * @fn    ClosestPointSurface<T> ClosestPointOnSurface(const PtType& point)
   * @brief Find closest point on the underlying surface
   * @param[in] point   Query points
   * @return    Closest point on surface
   */
  ClosestPointSurface<T> ClosestPointOnSurface(const PtType& point) const;


  /**
   * @name  Clear
   * @fn    void Clear()
   * @brief Clear structure.
   */
  void Clear();

  /**
   * @name Empty
   * @fn    bool Empty() const
   * @brief Check if tree is empty
   * @return    `true` if empty, `false` otherwise
   */
  bool Empty() const;

  /**
   * @name  Size
   * @fn    size_t Size() const
   * @brief Get tree size
   * @return    Tree size
   */
  size_t Size() const;

 private:

  /**
   * @class   Node
   * @brief   Internal tree node
   * @author  Christophe Ecabert
   * @date    04/01/2020
   * @ingroup geometry
   */
  class Node {
   public:
    /** Node list */
    using NodeList = std::vector<const Node*>;
    /** Index list */
    using IndexList = std::vector<size_t>;

    /**
     * @name    Merge
     * @fn  Node* Merge(const Node* a, const Node* b)
     * @brief Merge two nodes into a single one.
     * @param[in] a First node
     * @param[in] b Second node
     * @return  Merged node
     */
    static Node* Merge(const Node* a, const Node* b);

    /**
     * @name    Node
     * @fn  Node(const PtType& center, const T& radius, const int& index,
                  const Node* left, const Node* right)
     * @brief Constructor
     * @param[in] center    Sphere's center
     * @param[in] radius    Sphere's radius
     * @param[in] index     Sphere index
     * @param[in] left      Left child
     * @param[in] right     Right child
     */
    Node(const PtType& center,
         const T& radius,
         const int& index,
         const Node* left,
         const Node* right) : center_(center),
                              radius_(radius),
                              index_(index),
                              left_(left),
                              right_(right) {
    }

    /**
     * @name    MergeSpherePairs
     * @fn  static void MergeSpherePairs(const NodeList& partitions,
                                 const IndexList& nearest_idx,
                                 NodeList* merged_nodes)
     * @brief Merge a list of node `partitions` based on distance.
     * @param[in] partitions    List of node to merge
     * @param[in] nearest_idx   List of index to merge
     * @param[out] merged_nodes List of merge nodes
     */
    static void MergeSpherePairs(const NodeList& partitions,
                                 const IndexList& nearest_idx,
                                 NodeList* merged_nodes);

    /**
     * @name    Center
     * @fn  const PtType& Center() const
     * @brief Node's center
     * @return Center position
     */
    const PtType& Center() const {
      return center_;
    }

    /**
     * @name    Radius
     * @fn  T Radius() const
     * @brief Node's radius
     * @return Radius
     */
    T Radius() const {
      return radius_;
    }

    /**
     * @name    Index
     * @fn  int Index() const
     * @brief Node's index
     * @return Index
     */
    int Index() const {
      return index_;
    }

    /**
     * @name    HasLeftChild
     * @fn  bool HasLeftChild() const
     * @brief Indicates if node has a `left` child.
     * @return  True if it has a left child, false otherwise.
     */
    bool HasLeftChild() const {
      return left_ != nullptr;
    }

    /**
     * @name    HasRightChild
     * @fn  bool HasRightChild() const
     * @brief Indicates if node has a `right` child.
     * @return  True if it has a right child, false otherwise.
     */
    bool HasRightChild() const {
      return right_ != nullptr;
    }

    /**
     * @name    Left
     * @fn  const Node* Left() const
     * @brief Give reference to left child
     * @return  Pointer, can be nullptr.
     */
    const Node* Left() const {
      return left_;
    }

    /**
     * @name    Right
     * @fn  const Node* Right() const
     * @brief Give reference to right child
     * @return  Pointer, can be nullptr.
     */
    const Node* Right() const {
      return right_;
    }

   private:
    /** Center */
    PtType center_;
    /** Radius */
    T radius_;
    /** index */
    int index_;
    /** Left child */
    const Node* left_;
    /** Right child */
    const Node* right_;
  };

  /**
   * @name  BuildTree
   * @fn    const Node* BuildTree(const std::vector<const Node*>& partitions)
   * @brief Build internal tree structure
   * @param[in] partitions List of node to use
   * @return    Root node
   */
  const Node* BuildTree(const std::vector<const Node*>& partitions);

  /**
   * @name  DistanceToPartition
   * @brief Compute distance to bounding sphere
   * @param[in] point   Query point
   * @param[out] result Closest point
   */
  void DistanceToPartition(const PtType& point,
                           ClosestPointSurface<T>* result) const;

  /** Root node */
  const Node* root_;
  /** Leaves */
  std::vector<const Node*> leaves_;
  /** Triangles */
  std::vector<Triangle<3, T>> triangles_;
};

#pragma mark -
#pragma mark Implementation - Node

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<typename T>
typename BoundingSphereTree<T>::Node*
BoundingSphereTree<T>::Node::Merge(const Node* a, const Node* b) {
  // Line between two spheres + distance
  auto ab = b->Center() - a->Center();
  T dist = ab.Norm();
  if (dist < std::numeric_limits<T>::epsilon()) {
    // Sphere overlaps, same centers
    auto center = a->Center();
    T radius = std::max(a->Radius(),
                        b->Radius()) + std::numeric_limits<T>::epsilon();
    return new Node(center, radius, -1, a, b);
  } else {
    // both have different center
    // calculate opposite points of spheres
    auto na = a->Center() - ab * (a->Radius() / dist);
    auto nb = b->Center() + ab * (b->Radius() / dist);
    auto newCenter = (na + nb) * T(0.5);
    T newRadius = std::max((newCenter - a->Center()).Norm() + a->Radius(),
                           (newCenter - b->Center()).Norm() + b->Radius());
    return new Node(newCenter, newRadius, -1, a, b);
  }
}

template<typename T>
void BoundingSphereTree<T>::Node::MergeSpherePairs(const NodeList& partitions,
                                                   const IndexList& nearest_idx,
                                                   NodeList* merged_nodes){
  std::vector<bool> used(nearest_idx.size(), false);
  for (size_t k = 0; k < nearest_idx.size(); ++k) {
    if (k != nearest_idx[k]) {
      // Avoid merging with itself
      if (!used[k] && !used[nearest_idx[k]]) {
        // Don't merge already used sphere
        used[k] = true;
        used[nearest_idx[k]] = true;
        merged_nodes->emplace_back(Node::Merge(partitions[k],
                                               partitions[nearest_idx[k]]));
      }
    }
  }
  // Add back unused nodes for future merge.
  for (size_t k = 0; k < used.size(); ++k) {
    if (!used[k]) {
      merged_nodes->emplace_back(partitions[k]);
    }
  }
}

#pragma mark -
#pragma mark Implementation - BoundingSphereTree

template<typename T>
BoundingSphereTree<T>::BoundingSphereTree() : root_(nullptr),
                                              leaves_() {
}

template<typename T>
BoundingSphereTree<T>::~BoundingSphereTree() {
  this->Clear();
}

template<typename T>
void BoundingSphereTree<T>::Build(const std::vector<PtType>& points,
                                  const std::vector<TriType>& triangles) {
  // Clear structure if needed
  this->Clear();
  // Build new structure
  // 1) Leaves
  for (int k = 0; k < triangles.size(); ++k) {
    const auto& tri = triangles[k];
    const auto& v1 = points[tri[0]];
    const auto& v2 = points[tri[1]];
    const auto& v3 = points[tri[2]];
    Triangle<3, T> tri_with_pt(v1, v2, v3);
    triangles_.emplace_back(tri_with_pt);
    auto sph = Sphere<T>::FromTriangle(tri_with_pt);
    leaves_.emplace_back(new Node(sph.center_,
                                  sph.radius_,
                                  k,
                                  nullptr,
                                  nullptr));
  }
  // 2) Tree
  root_ = BuildTree(leaves_);
}

template<typename T>
ClosestPointSurface<T>
BoundingSphereTree<T>::ClosestPointOnSurface(const PtType& point) const {
  ClosestPointSurface<T> result;
  this->DistanceToPartition(point, &result);
  return result;
}

template<typename T>
void BoundingSphereTree<T>::Clear() {
  // Clear internal nodes
  if (root_ != nullptr) {
    // Query all internal node
    std::stack<const Node*> queue;
    queue.push(root_);
    while (!queue.empty()) {
      const auto* node = queue.top();
      queue.pop();

      // Add child to queue
      if (node->HasLeftChild()) {
        queue.push(node->Left());
      }
      if (node->HasRightChild()) {
        queue.push(node->Right());
      }
      // Delete node
      delete node;
    }
  }
  leaves_.clear();
  triangles_.clear();
}

template<typename T>
bool BoundingSphereTree<T>::Empty() const {
  return leaves_.empty() && root_ == nullptr;
}

template<typename T>
size_t BoundingSphereTree<T>::Size() const {
  return leaves_.size();
}

template<typename T>
const typename BoundingSphereTree<T>::Node*
BoundingSphereTree<T>::BuildTree(const std::vector<const Node*>& partitions) {
  if (partitions.size() != 1) {
    std::vector<PtType> points;
    for (const auto& node : partitions) {
      points.emplace_back(node->Center());
    }
    std::vector<size_t> nearest_point_index;
    std::vector<const Node*> subtree;
    CalculateNearestPointPairs(points, &nearest_point_index);
    Node::MergeSpherePairs(partitions, nearest_point_index, &subtree);
    return this->BuildTree(subtree);
  } else {
    return partitions.front();
  }
}

template<typename T>
void BoundingSphereTree<T>::DistanceToPartition(const PtType& point,
                                                ClosestPointSurface<T>* result)
                                                const {
  // Start recursion
  std::stack<const Node*> queue;
  queue.push(root_);
  while (!queue.empty()) {
    const auto* node = queue.top();
    queue.pop();

    if (node->Index() >= 0) {
      // Leaf node
      const auto& tri = triangles_[node->Index()];
      auto res = tri.ClosestPoint(point);
      if (res.dist < result->dist) {
        // Found better solution.
        *result = res;
        result->tri_idx = node->Index();
      }
    } else {
      // Standard node
      if (node->HasLeftChild() && node->HasRightChild()) {
        // Has both child
        T distToLeftCenter = (point - node->Left()->Center()).SquaredNorm();
        T distToRightCenter = (point - node->Right()->Center()).SquaredNorm();
        T leftRadius = node->Left()->Radius();
        leftRadius = leftRadius * leftRadius;
        T rightRadius = node->Right()->Radius();
        rightRadius = rightRadius * rightRadius;

        if (distToLeftCenter < distToRightCenter) {
          // nearer sphere first
          if ((distToLeftCenter <= leftRadius) || // point in sphere?
              (result->dist2 >= distToLeftCenter + leftRadius) || // are we close?
              (T(4.0) * distToLeftCenter * leftRadius >=
               std::pow(distToLeftCenter + leftRadius - result->dist2,
                        T(2.0)))) {
            // even better estimation
            queue.push(node->Left());
          }
          if ((distToRightCenter <= rightRadius) ||
              (result->dist2 >= distToRightCenter + rightRadius) ||
              (T(4.0) * distToRightCenter * rightRadius >=
               std::pow(distToRightCenter + rightRadius - result->dist2,
                        T(2.0)))) {
            queue.push(node->Right());
          }
        } else {
          if ((distToRightCenter <= rightRadius) ||
              (result->dist2 >= distToRightCenter + rightRadius) ||
              (T(4.0) * distToRightCenter * rightRadius >=
               std::pow(distToRightCenter + rightRadius - result->dist2,
                        T(2.0)))) {
            queue.push(node->Right());
          }
          if ((distToLeftCenter <= leftRadius) ||
              (result->dist2 >= distToLeftCenter + leftRadius) ||
              (T(4.0) * distToLeftCenter * leftRadius >=
               std::pow(distToLeftCenter + leftRadius - result->dist2,
                        T(2.0)))) {
            queue.push(node->Left());
          }
        }
      } else {
        // Has only one child
        if (node->HasLeftChild()) {
          T lc2 = (point - node->Left()->Center()).SquaredNorm();
          T lr2 = node->Left()->Radius();
          lr2 = lr2 * lr2;
          if ((lc2 <= lr2) ||
              (result->dist2 >= lc2 + lr2) ||
              (T(4.0) * lc2 * lr2 >= std::pow(lc2 + lr2 - result->dist2,
                                              T(2.0)))) {
            queue.push(node->Left());
          }
        } else if (node->HasRightChild()) {
          T rc2 = (point - node->Right()->Center()).SquaredNorm();
          T rr2 = node->Right()->Radius();
          rr2 = rr2 * rr2;
          if ((rc2 <= rr2) ||
              (result->dist2 >= rc2 + rr2) ||
              (T(4.0) * rc2 * rr2 >= std::pow(rc2 + rr2 - result->dist2,
                                              T(2.0)))) {
            queue.push(node->Right());
          }
        }
      }
    }
  }
}

#endif  // DOXYGEN_SHOULD_SKIP_THIS

}  // namespace LTS5
#endif //__LTS5_GEOMETRY_BOUNDING_SPHERE__
