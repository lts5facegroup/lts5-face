/**
 *  @file   general_quadric.hpp
 *  @brief  General Quadric local shape estimator function declaration
 *  @ingroup geometry
 *
 *  @author Gabriel Cuendet
 *  @date   12/04/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef __LTS5_GENERAL_QUADRIC__
#define __LTS5_GENERAL_QUADRIC__

#include <vector>
#include <utility>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/geometry/shape_estimator_base.hpp"
#include "lts5/geometry/weight.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  Quadric
 *  @brief  General Quadric local shape estimation class
 *  @author Gabriel Cuendet
 *  @date   12/04/2016
 *  @ingroup geometry
 */
template< typename T>
class LTS5_EXPORTS Quadric : public ShapeEstimatorBase<T> {
 public:

  /**
   * @struct  PairComparison
   * @brief   Pair comparison operator for the first member of the pair
   * @author  Gabriel Cuendet
   * @date    19/04/2016
   */
  struct PairComparison {
      bool operator()(const std::pair<double, int>& a,
                      const std::pair<double, int>& b) {
        return a.first < b.first;
      }
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name  Quadric
   * @fn  Quadric(void)
   * @brief Default constructor of the class
   */
  Quadric(void) : NN_aux_(6) {}

  /**
   * @name  Quadric
   * @fn  Quadric(int NN_aux)
   * @param[in] NN_aux  Number of nearest neighbors to consider when checking
   *                    the aux. pts for normal consistency
   * @brief Constructor of the class
   */
  Quadric(int NN_aux) : NN_aux_(NN_aux) {}

  /**
   * @name  Quadric
   * @fn  Quadric(const Quadric& other) = delete
   * @brief Copy constructor, disable
   */
  Quadric(const Quadric& other) = delete;

  /**
   * @name  operator=
   * @fn  Quadric& operator=(const Quadric& rhs) = delete
   * @brief Assignment operator, disable
   */
  Quadric& operator=(const Quadric& rhs) = delete;

  /**
   * @name  Clone
   * @fn  Quadric* Clone(void);
   * @brief Deep copy method
   */
  Quadric* Clone(void);

#pragma mark -
#pragma mark Getters

  /**
   * @name  A
   * @fn  const cv::Mat& A(void) const
   * @brief Getter for the matrix A of the quadric
   * @return const reference to the matrix
   */
  const cv::Mat& A(void) const {
    return A_;
  }

  /**
   * @name  b
   * @fn  const LTS5::Vector3<T>& b(void) const
   * @brief Getter for the vector b of the quadric
   * @return const reference to b
   */
  const LTS5::Vector3<T>& b(void) const {
    return b_;
  }

  /**
   * @name  c
   * @fn  T c(void) const
   * @brief Getter for c
   * @return value fo c
   */
  T c(void) const {
    return c_;
  }

#pragma mark -
#pragma mark Usage

  /**
   * @name  Fit
   * @fn  T Fit(const std::vector<LTS5::Vector3<T> >& points,
   *            const std::vector<LTS5::Vector3<T> >& normals,
   *            const AABB& bbox)
   * @brief Fit the general quadric to the input points using least squares
   * @param[in] points   Vertices within the support radius of the cell
   * @param[in] normals  Normals of the vertices
   * @param[in] bbox     Bounding box of the current cell
   * @return    error    fitting error
   */
  T Fit(const std::vector<LTS5::Vector3<T> >& points,
        const std::vector<LTS5::Vector3<T> >& normals,
        const AABB<T>& bbox);

  /**
   * @name  Fit
   * @fn  T Fit(const std::vector<LTS5::Vector3<T> >& points,
   *            const std::vector<LTS5::Vector3<T> >& normals,
   *            const AABB& bbox, const T& R)
   * @brief Fit the general quadric to the input points using least squares
   * @param[in] points   Vertices within the support radius of the cell
   * @param[in] normals  Normals of the vertices
   * @param[in] bbox     Bounding box of the current cell
   * @param[in] R        Radius of the support sphere (used for the weights)
   * @return    error    fitting error
   */
  T Fit(const std::vector<LTS5::Vector3<T> >& points,
        const std::vector<LTS5::Vector3<T> >& normals,
        const AABB<T>& bbox, const T& R);

  /**
   * @name  Evaluate
   * @fn  void Evaluate(const LTS5::Vector3<T>& x)
   * @brief Evalute the value of x, given the quadric approximation
   * @param[in]  x       Point to evaluate
   * @param[out] SwQ     Sum Qi(x)wi(x)
   * @param[out] Sw      Sum wi(x)
   */
  void Evaluate(const LTS5::Vector3<T>& x, T* SwQ, T* Sw);

  /**
   * @name  EvaluateSquare
   * @fn  virtual void EvaluateSquare(const LTS5::Vector3<T>& x, T* SwQ2, T* Sw) = 0
   * @brief Evalute the squared value of x, given the approximation
   * @param[in]  x     Point to evaluate
   * @param[out] SwQ2  Sum Qi(x)^2 wi(x)
   * @param[out] Sw    Sum wi(x)
   */
  void EvaluateSquare(const LTS5::Vector3<T>& x, T* SwQ2, T* Sw);

  /**
   * @name  EvaluateGrad
   * @fn  void EvaluateGrad(const LTS5::Vector3<T>& x,
   *                        LTS5::Vector3<T>* SwGradQ, T* Sw)
   * @brief Evaluate the gradient of Q at x
   * @param[in]  x       Point at which to evaluate the grad
   * @param[out] SwGradQ Sum Grad(Qi)(x)wi(x)
   * @param[out] Sw      Sum wi(x)
   */
  void EvaluateGrad(const LTS5::Vector3<T>& x, LTS5::Vector3<T>* SwGradQ,
                    T* Sw);

  /**
   * @name  EvaluateGradSquare
   * @fn  virtual void EvaluateGradSquare(const LTS5::Vector3<T>& x,
   *                                      LTS5::Vector3<T>* SwGradQ2, T* Sw) = 0
   * @brief Evaluate the gradient of Q^2 at x
   * @param[in]  x         Point to evaluate
   * @param[out] SwGradQ2  Sum Grad(Q_i^2)(x) wi(x)
   * @param[out] Sw        Sum wi(x)
   */
  void EvaluateGradSquare(const LTS5::Vector3<T>& x,
                          LTS5::Vector3<T>* SwGradQ2, T* Sw);

  /**
   * @name  EvaluateGradQ
   * @fn  void EvaluateGradQ(const LTS5::Vector3<T>& x, LTS5::Vector3<T>* grad)
   * @brief Evalute the gradient of x, given the quadric approximation
   * @param[in]  x       Point to evaluate
   * @param[out] grad    Gradient of the quadric evaluated at x
   */
  void EvaluateGradQ(const LTS5::Vector3<T>& x, LTS5::Vector3<T>* grad);

  /**
   * @name  EvaluateGradQ
   * @fn  LTS5::Vector3<T> EvaluateGradQ(const LTS5::Vector3<T>& x)
   * @brief Evalute the gradient of x, given the quadric approximation
   * @param[in]  x       Point to evaluate
   * @return     grad    Gradient of the quadric evaluated at x
   */
  LTS5::Vector3<T> EvaluateGradQ(const LTS5::Vector3<T>& x);

  /**
   * @name  EvaluateQ
   * @fn  T EvaluateQ(const LTS5::Vector3<T>& x)
   * @brief Evalute the value of x, given the quadric approximation
   * @param[in]  x       Point to evaluate
   * @return     val     Value of the quadric evaluated at x
   */
  T EvaluateQ(const LTS5::Vector3<T>& x);

 private:

#pragma mark -
#pragma mark Private methods

  /**
   * @name CheckAuxPoints
   * @fn void CheckAuxPts(const std::vector<LTS5::Vector3<T> >& points,
   *                      const std::vector<LTS5::Vector3<T> >& normals,
   *                      std::vector<LTS5::Vector3<T> >* aux_points,
   *                      std::vector<T>* di)
   * @brief Check the consistency of normals of the aux_points and compute the
   *        arithmetic mean normal of each of the aux points
   * @param[in]     points       Points of the mesh within the support ball
   * @param[in]     normals      Normals of these points
   * @param[in,out] aux_points   Auxiliary points, cheked for consistency
   * @param[out]    di           Computed mean distance of the aux points to
   *                             the surface
   */
  void CheckAuxPts(const std::vector<LTS5::Vector3<T> >& points,
                   const std::vector<LTS5::Vector3<T> >& normals,
                   std::vector<LTS5::Vector3<T> >* aux_points,
                   std::vector<T>* di);

  /**
   * @name BuildLinearSystem
   * @fn void BuildLinearSystem(const std::vector<LTS5::Vector3<T> >& points,
   *                            const std::vector<LTS5::Vector3<T> >& aux_points,
   *                            const std::vector<T> di,
   *                            cv::Mat* A, cv::Mat* B)
   * @brief From the points on the surface and the auxiliary points with value
   *        di, build the linear system Ax=B matrices A and B
   * @param[in]     points       Points of the mesh within the support ball
   * @param[in]     aux_points   Auxiliary points with consistent normals
   * @param[in]     di           Values of the aux. points (distances)
   * @param[out]    A            Coordinates second degree matrix
   * @param[out]    B            Values vector (0, ..., 0, d1, ..., dn)
   */
  void BuildLinearSystem(const std::vector<LTS5::Vector3<T> >& points,
                         const std::vector<LTS5::Vector3<T> >& aux_points,
                         const std::vector<T>& di,
                         cv::Mat* A, cv::Mat* B);

  /**
   * @name ComputeWeights
   * @fn void ComputeWeights(const std::vector<LTS5::Vector3<T> >& points,
   *                         const int& aux_pts_size, cv::Mat* weights)
   * @brief Compute the weights diagonal matrix
   * @param[in]     points       Points of the mesh within the support ball
   * @param[in]     aux_pts_size Number of aux points (w = 1/n for these)
   * @param[out]    weights      Diagonal opencv Mat with the weights
   */
  void ComputeWeights(const std::vector<LTS5::Vector3<T> >& points,
                      const int& aux_pts_size, cv::Mat* weights);

  /** Matrix A (3x3) */
  cv::Mat A_;
  /** Vector b */
  LTS5::Vector3<T> b_;
  /** Scalar c */
  T c_;
  /** Cell center */
  LTS5::Vector3<T> center_;
  /** Support radius of the cell */
  T radius_;
  /** Weight function */
  LTS5::Weight::BSpline<T> w_;
  /** Normalization factor: sum of the weights of all the points */
  T Sw_;
  /** Number of nearest neighbors to consider when checking the aux points */
  int NN_aux_;
};
} // namespace LTS5
#endif // __LTS5_GENERAL_QUADRIC__
