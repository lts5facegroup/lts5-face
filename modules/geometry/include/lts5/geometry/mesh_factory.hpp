/**
 *  @file   mesh_factory.hpp
 *  @brief  Mesh factory, helper class to generate basic meshes such as
 *          Sphere, Cube, Plane and read FWH data
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert, Marina Zimmermann
 *  @date   18/07/2016, 27/06/2016
 *  Copyright (c) 2016 Christophe Ecabert, Marina Zimmermann. All rights reserved.
 */
#ifndef __LTS5_MESH_FACTORY__
#define __LTS5_MESH_FACTORY__

#include <string>
#include <vector>
#include <map>
#include <fstream>

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/aabb.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/math/constant.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   MeshFactory
 * @brief   Helper class to generate basic meshes such as
 *          Sphere, Cube, Plane and read FWH data
 * @author  Christophe Ecabert, Marina Zimmermann
 * @date    18/07/2016, 26/07/2016
 * @ingroup geometry
 */
template<typename T>
class LTS5_EXPORTS MeshFactory {

 public:

#pragma mark -
#pragma mark Type definition

  /** Vertex definition */
  using Vertex = typename Mesh<T>::Vertex;
  /** Normal definition */
  using Normal = typename Mesh<T>::Normal;
  /** Texture Coordinate type */
  using TCoord = typename Mesh<T>::TCoord;
  /** Triangle definition */
  using Triangle = typename Mesh<T>::Triangle;

#pragma mark -
#pragma mark Parameters interface

  /**
   * @name  Parameters
   * @brief Parameters control structure
   */
  struct Parameters {
    /**
     * @name  ~Parameters
     * @fn  virtual ~Parameters()
     * @brief Destructor
     */
    virtual ~Parameters() = default;
    /**
     * @name  MakeParameterValid
     * @fn  virtual void MakeParameterValid() = 0
     * @brief Make sure the parameters are in the proper range
     */
    virtual void MakeParameterValid() = 0;
  };

#pragma mark -
#pragma mark Sphere

  /**
   * @struct  SphereParameters
   * @brief   Parameters for sphere generation
   */
  struct SphereParameters : public Parameters {
    /** Phi minimum angle, [-pi, pi] */
    T phi_min;
    /** Phi maximum angle [-pi, pi]*/
    T phi_max;
    /** Step over phi angle */
    int phi_step;
    /** Theta minimum angle, [0, pi] */
    T theta_min;
    /** Theta maximum angle, [0, pi] */
    T theta_max;
    /** Step over theta angle */
    int theta_step;
    /** Sphere radius */
    T radius;

    /**
     * @name  SphereParameters
     * @fn  SphereParameters()
     * @brief Constructor
     */
    SphereParameters() : phi_min(-Constants<T>::PI),
                         phi_max(Constants<T>::PI),
                         phi_step(10),
                         theta_min(0),
                         theta_max(Constants<T>::PI),
                         theta_step(10),
                         radius(1.0) {
    }

    /**
     * @name  MakeParameterValid
     * @fn  void MakeParameterValid()
     * @brief Make sure the parameters are in the proper range
     */
    void MakeParameterValid() {
      // Theta
      theta_min = theta_min < T(0.0) ? T(0.0) : theta_min;
      theta_max = theta_max > Constants<T>::PI ? Constants<T>::PI : theta_max;
      theta_step = theta_step < 3 ? 3 : theta_step;
      // Phi
      phi_min = phi_min < -Constants<T>::PI ? -Constants<T>::PI : phi_min;
      phi_max = phi_max > Constants<T>::PI ? Constants<T>::PI : phi_max;
      phi_step = phi_step < 3 ? 3 : phi_step;
      // Radius
      radius = radius < T(0.0) ? T(0.0) : radius;
    }


  };

  /**
   * @name  GenerateSphere
   * @fn  static void GenerateSphere(SphereParameters* param, Mesh<T>* mesh)
   * @brief Helper function to generate user defined Sphere
   * @param[in,out] param   Parameters to generate the sphere. If \p param are
   *                        not in the proper range, get automatically updated
   *                        to correct value,
   * @param[out]    mesh    Mesh where to add the generated sphere
   */
  static void GenerateSphere(SphereParameters* param, Mesh<T>* mesh);

#pragma mark -
#pragma mark Cube

  /**
   * @struct  CuboidParameters
   * @brief   Parameters for cuboid generation
   */
  struct CuboidParameters : public Parameters {
    /** Width */
    T width;
    /** Height */
    T height;
    /** Depth */
    T depth;
    /** Number of step in width */
    int width_step;
    /** Number of sStep in height */
    int height_step;
    /** Number of step in depth */
    int depth_step;

    /**
     * @name  CuboidParameters
     * @fn  CuboidParameters()
     * @brief Constructor
     */
    CuboidParameters() : width(1.0),
                                height(1.0),
                                depth(1.0),
                                width_step(10),
                                height_step(10),
                                depth_step(10) {
    }



    /**
     * @name  MakeParameterValid
     * @fn  void MakeParameterValid()
     * @brief Make sure the parameters are in the proper range
     */
    void MakeParameterValid() {
      // Check dimension are positive
      width = width < T(0.0) ? -width : width;
      height = height < T(0.0) ? -height : height;
      depth = depth < T(0.0) ? -depth : depth;
      // Increment
      width_step = width_step < 2 ? 2 : width_step;
      height_step = height_step < 2 ? 2 : height_step;
      depth_step = depth_step < 2 ? 2 : depth_step;
    }
  };

  /**
   * @name  GenerateCuboid
   * @fn  static void GenerateCuboid(CuboidParameters* param, Mesh<T>* mesh)
   * @brief Helper function to generate user defined Cuboid
   * @param[in,out] param   Parameters to generate the Cuboid. If \p param
   *                        are not in the proper range, get automatically
   *                        updated to correct value.
   * @param[out]    mesh    Mesh where to add the generated Cuboid
   */
  static void GenerateCuboid(CuboidParameters* param, Mesh<T>* mesh);

#pragma mark -
#pragma mark Plane

  /**
   * @struct  PlaneParameters
   * @brief   Parameters for generation of a plane centered at origin
   */
  struct PlaneParameters : public Parameters {
    /** Normal */
    LTS5::Vector3<T> normal;
    /** Width - u */
    T width;
    /** Height - v */
    T height;
    /** Number of step in u */
    int u_step;
    /** Number of step in v */
    int v_step;


    /**
     * @name  PlaneParameters
     * @fn  PlaneParameters()
     * @brief Constructor
     */
    PlaneParameters() : normal(0,0,1),
                            width(1),
                            height(1),
                            u_step(2),
                            v_step(2) {
    }

    /**
     * @name  MakeParameterValid
     * @fn  void MakeParameterValid()
     * @brief Make sure the parameters are in the proper range
     */
    void MakeParameterValid() {
      // Normalize normal if not already done
      normal.Normalize();
      // Check dimension
      width = width < T(0) ? -width : width;
      height = height < T(0) ? - height : height;
      // Increment
      u_step = u_step < 2 ? 2 : u_step;
      v_step = v_step < 2 ? 2 : v_step;
    }
  };

  /**
   * @name  GeneratePlane
   * @fn  static void GeneratePlane(PlaneParameters* param, Mesh<T>* mesh)
   * @brief Helper function to generate user defined Plane
   * @param[in,out] param   Parameters to generate the plane. If \p param
   *                        are not in the proper range, get automatically
   *                        updated to correct value.
   * @param[out]    mesh    Mesh where to add the generated plane
   */
  static void GeneratePlane(PlaneParameters* param, Mesh<T>* mesh);


#pragma mark -
#pragma mark ReadFWH

  /**
   *  @name LoadFWHObj
   *  @fn static int LoadFWHObj(const std::string& filepath, LTS5::Mesh<T>* mesh)
   *  @brief  Load FaceWarehouse mesh from OBJ file WITHOUT any connectivity!
   *  @param[in]  filepath  Path to the FWH obj mesh file
   *  @param[out] mesh      Pointer to mesh
   *  @return -1 if error, 0 otherwise
   */
  static int LoadFWHObj(const std::string& filepath, LTS5::Mesh<T>* mesh);

  /**
   *  @name LoadSingleExprFromBs
   *  @fn static int LoadSingleExprFromBs(const std::string& filename,
                                  int expression_id,
                                  LTS5::Mesh<T>* mesh)
   *  @brief  Load FaceWarehouse mesh from blendshape file (.bs) for a single expression
   *  @param[in]  filename       Path to the FWH blendshape file (.bs)
   *  @param[in]  expression_id  Index of wanted expression
   *  @param[out] mesh           Pointer to mesh
   *  @return -1 if error, 0 otherwise
   */
  static int LoadSingleExprFromBs(const std::string& filename,
                                  int expression_id,
                                  LTS5::Mesh<T>* mesh);

  /**
   *  @name LoadAllExprFromBs
   *  @fn static int LoadAllExprFromBs(const std::string& filename,
   *                                   std::vector<LTS5::Mesh<T> >* mesh_vector);
   *  @brief  Load FaceWarehouse meshes for all expressions from blendshape file (.bs)
   *  @param[in]  filename     Path to the FWH blendshape file (.bs)
   *  @param[in]  mesh_vector  Vector of loaded meshes
   *  @return -1 if error, 0 otherwise
   */
  static int LoadAllExprFromBs(const std::string& filename,
                               std::vector<LTS5::Mesh<T>* >* mesh_vector);

  /**
   * @name  LoadQuads
   * @fn  static int LoadQuads(const std::string& filepath,
   *                           std::vector<LTS5::Vector4<int> >* quads)
   * @brief Load quads into vector of Quad
   * @param[in]  filepath  File to read the quad from
   * @param[out] quads  vector of quads
   * @return 0 if success, -1 otherwise
   */
  static int LoadQuads(const std::string& filepath,
                       std::vector<LTS5::Vector4<int> >* quads);

  /**
   * @name  DivideQuadIntoTri
   * @fn  static void DivideQuadIntoTri(const std::vector<LTS5::Vector4<int> >& quads,
   *                                    LTS5::Mesh<T>* mesh)
   * @brief Divide the quads into triangles along the shortest diagonal and
   *        apply these triangles to the mesh
   * @param[in]     quads  List of quads to divide
   * @param[in,out] mesh   Vertices to take into account when dividing, and tri to set
   */
  static void DivideQuadIntoTri(const std::vector<LTS5::Vector4<int> >& quads,
                                LTS5::Mesh<T>* mesh);

  /**
   *  @name ComputeMeanShape
   *  @fn void ComputeMeanShape(const std::vector<LTS5::Mesh<T>>& mesh_vector,
                                LTS5::Mesh<T>* mean_mesh)
   *  @brief  Compute the mean shape of all subjects for an expression
   *  @param[in]  mesh_vector Pointer to mesh vector
   *  @param[out]  mean_mesh  Pointer to mesh
   */
  static void ComputeMeanShape(const std::vector<LTS5::Mesh<T>*>& mesh_vector,
                               LTS5::Mesh<T>* mean_mesh);

};

}  // namepsace LTS5

#endif //__LTS5_MESH_FACTORY__
