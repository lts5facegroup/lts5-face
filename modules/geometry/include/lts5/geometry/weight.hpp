/**
 *  @file   weight.hpp
 *  @brief  Weighting scheme
 *  @ingroup geometry
 *
 *  @author Gabriel Cuendet
 *  @date   15/04/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef LTS5_WEIGHT_HPP
#define LTS5_WEIGHT_HPP

#include "lts5/utils/math/vector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Weight
 *  @brief      Namespace for weight functors
 */
namespace Weight {
template<typename T>
/**
 * @name BSpline
 * @brief 1D, centered and scaled (between [-1,1]) quadratic B-spline function
 * @ingroup geometry
 */
struct LTS5_EXPORTS BSpline {
  T operator()(const T& r) {
    if (r < -1.0 || r > 1.0)
      return T(0.0);

    T x = 1.5*r + 1.5;  // Shifts r to the interval [0, 3]
    if (x <= 1.0)
      return 0.5 * x * x;
    else if (x <= 2.0)
      return 0.5 * (-2.0 * x * x + 6 * x - 3);
    else
      return 0.5 * (3-x) * (3-x);
  }
};


#pragma mark -
#pragma mark Declaration

/** Float type BSpline */
template struct BSpline<float>;
/** Float type BSpline */
template struct BSpline<double>;

} // namespace Weight
} // namespace LTS5
#endif //LTS5_WEIGHT_HPP
