/**
 *  @file   spherical_harmonic.hpp
 *  @brief  Compute spherical harmonics basis
 *  @ingroup    geometry
 *
 *  @author Christophe Ecabert
 *  @date   02/11/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_SPHERICAL_HARMONIC__
#define __LTS5_SPHERICAL_HARMONIC__

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mesh.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   SphericalHarmonic
 * @brief   Utility functor to compue spherical harmonics basis
 * @author  Christophe Ecabert
 * @date    02/11/17
 * @tparam T Data type
 * @tparam B Number of basis to pick, usually between 1-3
 */
template<typename T, int B>
class LTS5_EXPORTS SphericalHarmonic {
 public:

#pragma mark -
#pragma mark Type definition

  /** Normal Type */
  using Normal = typename LTS5::Mesh<T>::Normal;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  SphericalHarmonic
   * @fn    SphericalHarmonic(void)
   * @brief Constructor
   */
  SphericalHarmonic(void) = default;

  /**
   * @name  SphericalHarmonic
   * @fn    explicit SphericalHarmonic(const Normal& normal)
   * @brief Constructor
   * @param[in] normal Surface normal
   */
  explicit SphericalHarmonic(const Normal& normal);

  /**
   * @name  Generate
   * @fn    void Generate(const Normal& normal)
   * @brief Create spherical harmonics
   * @param[in] normal  Surface normal
   */
  void Generate(const Normal& normal);

#pragma mark -
#pragma mark Usage

  /**
   * @name  operator()
   * @fn    void operator()(const cv::Mat& p, cv::Mat* projection) const
   * @brief Project a signal \p into the spherical harmonic basis.
   * @param[in] p  Signal to project. [N X B²] or [B² x 9]
   * @param[out] projection Projected signal
   */
  void operator()(const cv::Mat& p, cv::Mat* projection) const;

  /**
   * @name  data
   * @fn    const T* data() const
   * @brief Provide direct access to computed basis
   * @return    Pointer to basis data
   */
  const T* data() const {
    return reinterpret_cast<T*>(basis_.data);
  }

#pragma mark -
#pragma mark Private
 private:
  /** Basis */
  cv::Mat basis_;

};

#ifndef __APPLE__
/** SH - Frist band - Float */
template class LTS5_EXPORTS SphericalHarmonic<float, 1>;
template class LTS5_EXPORTS SphericalHarmonic<double, 1>;
/** SH - Second band - Float */
template class LTS5_EXPORTS SphericalHarmonic<float, 2>;
template class LTS5_EXPORTS SphericalHarmonic<double, 2>;
/** SH - Third band - Float */
template class LTS5_EXPORTS SphericalHarmonic<float, 3>;
template class LTS5_EXPORTS SphericalHarmonic<double, 3>;
#endif
}  // namepsace LTS5
#endif //__LTS5_SPHERICAL_HARMONIC__
