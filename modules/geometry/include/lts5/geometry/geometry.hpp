/**
 *  @file   lts5/geometry/geometry.hpp
 *  @brief  Geometry modules
 *
 *  @author Christophe Ecabert
 *  @date   27/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_GEOMETRY__
#define __LTS5_GEOMETRY__

/** Axis Aligned Bounding Box */
#include "lts5/geometry/aabb.hpp"
/** Axis Aligned Bounding Cube */
#include "lts5/geometry/aabc.hpp"
/** Axis Aligned Bounding Box traversal intersection */
#include "lts5/geometry/aabb_intersect.hpp"
/** AABB Node */
#include "lts5/geometry/aabb_node.hpp"
/** AABB Tree */
#include "lts5/geometry/aabb_tree.hpp"
/** Collision tree interface */
#include "lts5/geometry/base_collision_tree.hpp"
/** Mesh */
#include "lts5/geometry/mesh.hpp"
/** MeshFactory */
#include "lts5/geometry/mesh_factory.hpp"
/** OCTree */
#include "lts5/geometry/octree.hpp"
/** OCTree node */
#include "lts5/geometry/octree_node.hpp"
/** Mesh Factory */
#include "lts5/geometry/mesh_factory.hpp"

#endif //__LTS5_GEOMETRY__
