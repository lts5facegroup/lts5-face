/**
 *  @file   multiscale_partition_unity.hpp
 *  @brief  Multilevel Partition of Unity class
 *  @ingroup geometry
 *
 *  @author Gabriel Cuendet
 *  @date   11/04/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef LTS5_MULTISCALE_PARTITION_UNITY_HPP
#define LTS5_MULTISCALE_PARTITION_UNITY_HPP

#include <unordered_map>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/mpu_node.hpp"
#include "lts5/geometry/aabb_mpu_tree.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  MPU
 *  @brief  Multilevel Partition of Unity class
 *  @author Gabriel Cuendet
 *  @date   11/04/2016
 *  @ingroup geometry
 */
template< typename T>
class LTS5_EXPORTS MPU {
 public:

#pragma mark -
#pragma mark Initialization

  /** Vertex type */
  using Vertex = typename LTS5::Mesh<T>::Vertex;
  /** Normal and normal vector */
  using Normal = typename LTS5::Mesh<T>::Normal;
  /** Mesh primitive */
  using PrimitiveType = typename LTS5::Mesh<T>::PrimitiveType;

  /**
   * @name  MPU
   * @fn  MPU(void)
   * @brief Constructor
   */
  MPU(void);

  /**
   * @name  MPU
   * @fn  MPU(const double max_error, const int max_depth,
   *          const double alpha);
   * @brief Constructor
   * @param[in] max_error   Approximation error threshold
   * @param[in] max_depth   Maximum tree depth
   * @param[in] alpha       Factor of support radius
   */
  MPU(const double max_error, const int max_depth,
      const double alpha);

  /**
   * @name  MPU
   * @fn  MPU(const MPU& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  MPU(const MPU& other) = delete;

  /**
   * @name  operator=
   * @fn  MPU& operator=(const MPU& rhs) = delete
   * @brief Assign operator
   * @param[in] rhs Object to assign from
   */
  MPU& operator=(const MPU& rhs) = delete;

  /**
   * @name  ~MPU
   * @fn  ~MPU(void)
   * @brief Destructor
   */
  ~MPU(void);

  /**
   * @name  Build
   * @fn   void Build(void)
   * @brief Build iteratively the whole tree and the approximation
   * @param[in] mesh      3D mesh used to build tree
   * @param[in] domain    Bounding cube of additional space domain to include
   */
  void Build(const Mesh<T>& mesh, const AABC<T>& domain);

  /**
   * @name  Clear
   * @fn  void Clear(void)
   * @brief Clear the whole tree
   */
  void Clear(void);

#pragma mark -
#pragma mark Getters and Setters

  /**
   * @name RootNode
   * @fn const LTS5::MPUNode<T>& RootNode(void) const
   * @brief Getter for the root node
   * @returns a const ref to the root node
   */
  const LTS5::MPUNode<T>& RootNode(void) const {
    return (*root_node_);
  }

  /**
   * @name  alpha
   * @fn  double alpha(void) const
   * @brief Getter for alpha
   * @return alpha
   */
  double alpha(void) const {
    return alpha_;
  }

  /**
   * @name  set_alpha
   * @fn  double set_alpha(void)
   * @brief Setter for alpha
   * @param[in] alpha  value to set alpha to
   */
  void set_alpha(double alpha) {
    alpha_ = alpha;
  }

  /**
   * @name  max_error
   * @fn  double max_error(void) const
   * @brief Getter for max_error
   * @return max_error
   */
  double max_error(void) const {
    return max_error_;
  }

  /**
   * @name  set_max_error
   * @fn  double set_max_error(void)
   * @brief Setter for max_error
   * @param[in] error  value to set max_error to
   */
  void set_max_error(double error) {
    max_error_ = error;
  }

  /**
   * @name  max_depth
   * @fn  double max_depth(void) const
   * @brief Getter for max_depth
   * @return max_depth
   */
  std::size_t max_depth(void) {
    return max_depth_;
  }

  /**
   * @name  set_max_depth
   * @fn  double set_max_depth(void)
   * @brief Setter for max_depth
   * @param[in] depth  value to set max_depth to
   */
  void set_max_depth(size_t depth) {
    max_depth_ = depth;
  }

#pragma mark -
#pragma mark Usage

  /**
   * @name  Evaluate
   * @fn  T Evaluate(const LTS5::Vector3<T>& x) const
   * @brief Evaluate the value of the implicit function at a given position x
   * @param[in]  x  a Vector3<T> where to evaluate the function
   * @return the value, of type T
   */
  T Evaluate(const LTS5::Vector3<T>& x) const;

  /**
   * @name  EvaluateSquare
   * @fn  T EvaluateSquare(const LTS5::Vector3<T>& x) const
   * @brief Evaluate the value of the squared implicit function at a given position x
   * @param[in]  x  a Vector3<T> where to evaluate the function
   * @return the value, of type T
   */
  T EvaluateSquare(const LTS5::Vector3<T>& x) const;

  /**
   * @name  EvaluateGradient
   * @fn  void EvaluateGradient(const LTS5::Vector3<T>& x,
   *                            LTS5::Vector3<T>* grad) const
   * @brief Evaluate the gradienet of the implicit function at a given position x
   * @param[in]  x     a Vector3<T> where to evaluate the gradient
   * @param[out] grad  the resulting gradient in x
   */
  void EvaluateGradient(const LTS5::Vector3<T>& x,
                        LTS5::Vector3<T>* grad) const;

  /**
   * @name  EvaluateGradientSquare
   * @fn  void EvaluateGradientSquare(const LTS5::Vector3<T>& x,
   *                                  LTS5::Vector3<T>* grad) const
   * @brief Evaluate the gradient of the squared implicit function at a given
   *        position x
   * @param[in]  x     a Vector3<T> where to evaluate the gradient
   * @param[out] grad  the resulting gradient in x
   */
  void EvaluateGradientSquare(const LTS5::Vector3<T>& x,
                              LTS5::Vector3<T>* grad) const;

 protected:
  /**< Vector of MPUNode containing the leaves of the tree */
  std::vector<LTS5::MPUNode<T>*> nodes_;
  /**< AABBTree of the nodes to speed-up evaluation **/
  LTS5::AABBMPUTree<T> aabb_tree_;
  /**< Root node of the MPU */
  LTS5::MPUNode<T>* root_node_;
  /** Factor by which to multiply main diagonal to get support radius */
  double alpha_;
  /** Minimum number of points in the support ball to approximate */
  int n_min_;
  /** Maximum approximation error threshold */
  double max_error_;
  /** Maximum tree depth */
  std::size_t max_depth_;
  /** Maximum level in the tree */
  std::size_t max_tree_level_;
};
} // namespace LTS5
#endif //LTS5_MULTISCALE_PARTITION_UNITY_HPP
