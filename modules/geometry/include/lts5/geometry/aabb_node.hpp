/**
 *  @file   aabb_node.hpp
 *  @brief  AABB Node
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   24/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_AABB_NODE__
#define __LTS5_AABB_NODE__

#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/aabb.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @struct  AABBNode
 * @brief   AABBNode representation,
 *          Based on CGAL
 * @author  Christophe Ecabert
 * @date    24/02/16
 * @ingroup geometry
 */
template<typename T>
struct LTS5_EXPORTS AABBNode {

  /** node level */
  int level_;
  /** Node bounding box */
  AABB<T> bbox_;
  /** Left child */
  void* left_child_;
  /** Right child */
  void* right_child_;
  /** Start index */
  std::size_t first_;
  /** Last index */
  std::size_t last_;
  /** Leaf flag */
  bool is_leaf_;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  AABBNode
   * @fn  AABBNode(void)
   * @brief Constructor
   */
  AABBNode(void) : level_(0),
                   left_child_(nullptr),
                   right_child_(nullptr),
                   first_(0),
                   last_(0),
                   is_leaf_(false) {
  }

  /**
   * @name  AABBNode
   * @fn  AABBNode(const AABBNode& other) = delete
   * @brief Copy constructor, disable
   */
  AABBNode(const AABBNode& other) = delete;

  /**
   * @name  operator=
   * @fn  AABBNode& operator=(const AABBNode& rhs) = delete
   * @brief Assignment operator, disable
   */
  AABBNode& operator=(const AABBNode& rhs) = delete;

  /**
   * @name  ~AABBNode
   * @fn  ~AABBNode(void)
   * @brief Destructor
   */
  ~AABBNode(void) {
  }

#pragma mark -
#pragma mark Accessor

  /**
   * @name  get_bbox
   * @fn  const AABB& get_bbox(void) const
   * @brief Access node bounding box
   * @return  Reference to node bounding box
   */
  const AABB<T>& get_bbox(void) const {
    return bbox_;
  }

  /**
   * @name  get_left_child
   * @fn  const AABBNode* get_left_child(void) const
   * @brief Access left child
   * @return  Pointer to left child
   */
  const AABBNode* get_left_child(void) const {
    if (((last_ - first_) > 3) && left_child_) {
      return reinterpret_cast<const AABBNode*>(left_child_);
    } else {
      return nullptr;
    }
  }

  /**
   * @name  get_left_child
   * @fn  AABBNode* get_left_child(void)
   * @brief Access left child
   * @return  Reference to left child
   */
  AABBNode* get_left_child(void) {
    if (((last_ - first_) > 3) && left_child_) {
      return reinterpret_cast<AABBNode*>(left_child_);
    } else {
      return nullptr;
    }
  }

  /**
   * @name  get_left_data
   * @fn  const AABB* get_left_data(void) const
   * @brief Access left child
   * @return  Pointer to left data
   */
  const AABB<T>* get_left_data(void) const {
    if (((last_ - first_) <= 3) && left_child_) {
      return reinterpret_cast<const AABB<T>*>(left_child_);
    } else {
      return nullptr;
    }
  }

/**
 * @name  get_left_data
 * @fn  AABB* get_left_data(void)
 * @brief Access left child
 * @return  Reference to left data
 */
  AABB<T>* get_left_data(void) {
    if (((last_ - first_) <= 3) && left_child_) {
      return reinterpret_cast<AABB<T>*>(left_child_);
    } else {
      return nullptr;
    }
  }

  /**
   * @name  get_right_child
   * @fn  const AABBNode* get_right_child(void) const
   * @brief Access left child
   * @return  Reference to left child
   */
  const AABBNode* get_right_child(void) const {
    if (((last_ - first_) > 2) && right_child_) {
      return reinterpret_cast<const AABBNode*>(right_child_);
    } else {
      return nullptr;
    }
  }

  /**
   * @name  get_right_child
   * @fn  AABBNode* get_right_child(void)
   * @brief Access left child
   * @return  Pointer to left child
   */
  AABBNode* get_right_child(void) {
    if (((last_ - first_) > 2) && right_child_) {
      return reinterpret_cast<AABBNode*>(right_child_);
    } else {
      return nullptr;
    }
  }

  /**
   * @name  get_right_data
   * @fn  const AABB* get_right_data(void) const
   * @brief Access left child
   * @return  Reference to left data
   */
  const AABB<T>* get_right_data(void) const {
    if (((last_ - first_) == 2) && right_child_) {
      return reinterpret_cast<const AABB<T>*>(right_child_);
    } else {
      return nullptr;
    }
  }

/**
 * @name  get_right_data
 * @fn  AABB* get_right_data(void)
 * @brief Access left child
 * @return  Reference to left data
 */
  AABB<T>* get_right_data(void) {
    if (((last_ - first_) == 2) && right_child_) {
      return reinterpret_cast<AABB<T>*>(right_child_);
    } else {
      return nullptr;
    }
  }

  /**
   * @name  is_leaf
   * @fn  bool is_leaf(void) const
   * @brief Indicate if node is a leaf
   * @return  True if node is a leaf, false otherwise
   */
  bool is_leaf(void) const {
    return is_leaf_;
  }

  /**
   * @name  Size
   * @fn  std::size_t size(void) const
   * @brief Provide how many bounding box is covered by this node
   * @return  Size
   */
  std::size_t size(void) const {
    return (last_ - first_);
  }
};
}  // namepsace LTS5

#endif //__LTS5_AABB_NODE__
