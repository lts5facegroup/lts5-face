/**
 *  @file   isotropic_clustering_metric.hpp
 *  @brief  Metric for uniform mesh clustering
 *          Used in uniform remeshing functionality
 *          Based on : https://github.com/valette/ACVD
 *
 *          Approximated Centroidal Voronoi Diagrams for Uniform Polygonal Mesh
 *          S. Vallette et al.
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   10/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __ISOTROPICCLUSTERINGMETRIC__
#define __ISOTROPICCLUSTERINGMETRIC__

#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/geometry/mesh.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   IsotropicClusteringMetric
 * @brief   etric for uniform mesh clustering
 *          Used in uniform remeshing functionality
 *          Based on : https://github.com/valette/ACVD
 *
 *          Approximated Centroidal Voronoi Diagrams for Uniform Polygonal Mesh
 *          S. Vallette et al.
 * @author  Christophe Ecabert
 * @date    10/11/2016
 * @ingroup geometry
 */
class LTS5_EXPORTS IsotropicClusteringMetric {
 public:

#pragma mark -
#pragma mark Type definitions

  /**
   * @struct ClusterData
   * @brief  Data link to a given cluster
   */
  struct ClusterData {

    /**
     * @name    ClusterData
     * @fn  ClusterData(void)
     * @brief   Constructor
     */
    ClusterData(void) : value(0.0, 0.0, 0.0),
                        weight(0.0) {
    }

    /**
     * @name    ClusterData
     * @fn  ClusterData(const ClusterData& other)
     * @brief   Copy constructor
     * @param[in] other Object to copy from
     */
    explicit ClusterData(const ClusterData& other) {
      value = other.value;
      weight = other.weight;
    }

    /**
     * @name operator=
     * @fn  ClusterData& operator=(const ClusterData& rhs)
     * @brief   Assignment operator
     * @param rhs   Object to assign from
     * @return  Newly assigned object
     */
    ClusterData& operator=(const ClusterData& rhs) {
      if (this != &rhs) {
        value = rhs.value;
        weight = rhs.weight;
      }
      return *this;
    }

    /**
     * @name    ~ClusterData
     * @fn  ~ClusterData(void)
     * @brief   Destructor
     */
    ~ClusterData(void) {
    }

    /** Value : Vertex value */
    Vector3<double> value;
    /** Data weight */
    double weight;
  };

  /**
   * @struct Cluster
   * @brief Cluster object used to clusterize mesh
   */
  struct Cluster {

#pragma mark -
#pragma mark Initialization

    /**
     * @name    Cluster
     * @fn      Cluster(void)
     * @brief   Constructor
     */
    Cluster(void) : sgamma(0.0, 0.0, 0.0),
                    sweight(0.0),
                    energy(0.0) {
    }

    /**
     * @name    Cluster
     * @fn  explicit Cluster(const Cluster& other)
     * @brief   Copy constructor
     * @param other Object to copy from
     */
    explicit Cluster(const Cluster &other) {
      sgamma = other.sgamma;
      sweight = other.sweight;
      energy = other.energy;
    }

    /**
     * @name    operator=
     * @fn  Cluster& operator=(const Cluster& rhs)
     * @brief   Assignment operator
     * @param[in] rhs   Object to assign from
     * @return  Newly assigned object
     */
    Cluster &operator=(const Cluster &rhs) {
      if (this != &rhs) {
        sgamma = rhs.sgamma;
        sweight = rhs.sweight;
        energy = rhs.energy;
      }
      return *this;
    }

    /**
     * @name    ~Cluster
     * @fn      ~Cluster(void)
     * @brief   Destructor
     */
    ~Cluster(void) {
    }

#pragma mark -
#pragma mark Process

    /**
     * @name  ComputeEnergy
     * @fn    void ComputeEnergy(void)
     * @brief Compute energy of the cluster
     */
    void ComputeEnergy(void) {
      energy = ((-(sgamma.x_ * sgamma.x_) -
                (sgamma.y_ * sgamma.y_) -
                (sgamma.z_ * sgamma.z_)) / sweight);
    }

    /**
     * @name GetCentroid
     * @fn  void GetCentroid(Vector3<double>* centroid)
     * @brief Compute cluster's centroid
     * @param[out] centroid Cluster's centroid
     */
    void GetCentroid(Vector3<double>* centroid) {
      *centroid = sgamma / sweight;
    }

    /**
     * @name SetCentroid
     * @fn  void SetCentroid(const Vector3<double>& centroid)
     * @brief Set cluster's centroid
     * @param[in] centroid Cluster's centroid
     */
    void SetCentroid(const Vector3<double>& centroid) {
      sgamma = centroid * sweight;
    }
    /**
     * @name    AddData
     * @fn      void AddData(const ClusterData& data)
     * @brief   Add data to this cluster
     * @param[in] data Cluster's data to add
     */
    void AddData(const ClusterData& data) {
      this->sgamma += data.value;
      this->sweight += data.weight;
    }

    /**
     * @name    SubData
     * @fn      void SubData(const ClusterData& data)
     * @brief   Remove data from this cluster
     * @param[in] data Cluster's data to remove
     */
    void SubData(const ClusterData& data) {
      this->sgamma -= data.value;
      this->sweight -= data.weight;
    }

    /**
     * @name    Reset
     * @fn      void Reset(void)
     * @brief   Reset cluster, set to zero
     */
    void Reset(void) {
      sgamma.x_ = 0.0;
      sgamma.y_ = 0.0;
      sgamma.z_ = 0.0;
      sweight = 0.0;
      energy = 0.0;
    }

#pragma mark -
#pragma mark Accessors

    /**
     * @name    get_rank_deficiency
     * @fn      int get_rank_deficiency(void)
     * @brief   Provide cluster rank deficiency
     * @return  0
     */
    int get_rank_deficiency(void) {
      return 0;
    }

    /** Gamma coefficient, cf article */
    Vector3<double> sgamma;
    /** Cluster weight */
    double sweight;
    /** Cluster energy */
    double energy;
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name  IsotropicClusteringMetric
   * @fn    IsotropicClusteringMetric(void)
   * @brief Constructor
   */
  IsotropicClusteringMetric(void);

  /**
   * @name  IsotropicClusteringMetric
   * @fn IsotropicClusteringMetric(const IsotropicClusteringMetric& other) = delete
   * @brief Copy constructor
   * @param[in] other   Object ot copy from
   */
  IsotropicClusteringMetric(const IsotropicClusteringMetric& other) = delete;

  /**
   * @name  operator=
   * @fn IsotropicClusteringMetric& operator=(const IsotropicClusteringMetric& rhs) = delete
   * @brief Assignment operator
   * @param rhs Object to assign from
   * @return Newly assigned object
   */
  IsotropicClusteringMetric& operator=(const IsotropicClusteringMetric& rhs) = delete;

  /**
   * @name  ~IsotropicClusteringMetric
   * @fn    ~IsotropicClusteringMetric(void)
   * @brief Destructor
   */
  ~IsotropicClusteringMetric(void);

#pragma mark -
#pragma mark Process

  /**
   * @name  IsCurvatureIndicatorNeeded
   * @fn    int IsCurvatureIndicatorNeeded(void) const
   * @brief Indicate if curvature is needed
   * @return    0 if not needed, 1 otherwise
   */
  int IsCurvatureIndicatorNeeded(void) const;

  /**
   * @name  IsPrincipalDirectionsNeeded
   * @fn    int IsPrincipalDirectionsNeeded(void) const
   * @brief Indicate if principal direction is needed
   * @return    0, not used for this metric
   */
  int IsPrincipalDirectionsNeeded(void) const;

  /**
   * @name  ComputeDistanceDataCluster
   * @fn    T ComputeDistanceDataCluster(const int data_idx,
                                                   const int cluster_idx)
   * @brief Compute distance between a given cluster and a given data point
   * @param[in] data_idx        Data point index
   * @param[in] cluster_idx     Cluster index
   * @return    0
   */
  double ComputeDistanceDataCluster(const int data_idx,
                                    const int cluster_idx);

  /**
   * @name  ComputeDistanceDataPoint
   * @fn    T ComputeDistanceDataPoint(const int data_idx,
                                       const Vector3<double>& vertex)
   * @brief Compute distance between cluster data point and a vertex
   * @param data_idx    Index of the cluster data
   * @param vertex      Vertex
   * @return    Distance between cluster data and vertex
   */
  double ComputeDistanceDataPoint(const int data_idx,
                                  const Vector3<double>& vertex);

  /**
   * @name  Reset
   * @fn    void Reset(const int idx)
   * @brief Clear a given cluster
   * @param[in] idx    Cluster index to reset
   */
  void Reset(const int idx);

  /**
   * @name  Copy
   * @fn    void Copy(const Cluster& cluster, const int dst_index)
   * @brief Copy one cluster to a specific location
   * @param[in] cluster     Source cluster to copy
   * @param[in] dst_index   Index where to copy
   */
  void Copy(const Cluster& cluster, const int dst_index);

  /**
   * @name  AddDataToCluster
   * @fn    void AddDataToCluster(const int data_idx, const int cluster_idx)
   * @brief Add a given data point into a specific cluster
   * @param[in] data_idx
   * @param[in] cluster_idx
   */
  void AddDataToCluster(const int data_idx, const int cluster_idx);

  /**
   * @name    Add
   * @fn  void Add(const int src,
                   const int data_idx,
                   Cluster* dst)
   * @brief Copy a cluster to a destination and add data to it.
   * @param[in] src        Index of the cluster to copy
   * @param[in] data_idx   Index of the data to add to the destination
   * @param[in,out] dst    Cluster where to put result
   */
  void Add(const int src,
           const int data_idx,
           Cluster* dst);

  /**
   * @name    Sub
   * @fn  void Sub(const int src,
                   const int data_idx,
                   Cluster* dst)
   * @brief Copy a cluster to a destination and substract data to it.
   * @param[in] src        Index of the cluster to copy
   * @param[in] data_idx   Index of the data to substract to the destination
   * @param[in,out] dst        Cluster where to put result
   */
  void Sub(const int src,
           const int data_idx,
           Cluster* dst);

  /**
   * @name  Build
   * @fn    int Build(const LTS5::Mesh<T>* mesh, const int number_cluster)
   * @brief Build metric
   * @param[in] mesh            Mesh to build the metric from
   * @param[in] number_cluster  Number of cluster wanted
   * @return    -1 if error, 0 otherwise
   */
  template<typename T>
  int Build(const LTS5::Mesh<T>* mesh, const int number_cluster);

  /**
   * @name  ComputeClusterCentroid
   * @fn    void ComputeClusterCentroid(const int cluster_idx)
   * @brief Compute centroid for a given cluster
   * @param[in] cluster_idx Index where to compute centroid
   */
  void ComputeClusterCentroid(const int cluster_idx);

  /**
   * @name  GetClusterCentroid
   * @fn    void GetClusterCentroid(const int cluster_idx, Vector3<double>* centroid)
   * @brief Get cluster's centroid
   * @param[in] cluster_idx Cluster index
   * @param[out] centroid   Centroid
   */
  void GetClusterCentroid(const int cluster_idx, Vector3<double>* centroid);

  /**
   * @name  ComputeClusterEnergy
   * @fn    void ComputeClusterEnergy(const int cluster_idx)
   * @brief Compute energy for a given cluster
   * @param[in] cluster_idx Index where to compute energy
   */
  void ComputeClusterEnergy(const int cluster_idx);

#pragma mark -
#pragma mark Accessors

  /**
   * @name    set_constrained_clustering
   * @fn  void set_constrained_clustering(const int c)
   * @brief   Empty, no constraint for this metric
   * @param c Constraint
   */
  void set_constrained_clustering(const int c) {
  }

  /**
   * @name  get_cluster_data_weight
   * @fn    T get_cluster_data_weight(const int index) const
   * @brief Give weight for a given cluster
   * @param[in] index   Index of the cluster
   * @return    Weight of the cluster
   */
  double get_cluster_data_weight(const int index) const {
    return cluster_data_[index].weight;
  }

  /**
   * @name  get_cluster_energy
   * @fn    T get_cluster_energy(const int index) const
   * @brief Give the energy for a given cluster
   * @param[in] index   Index of the cluster
   * @return    Enery of the cluster
   */
  double get_cluster_energy(const int index) const {
    return clusters_[index].energy;
  }

#pragma mark -
#pragma mark Private

  /**
   * @name  ClampWeight
   * @fn    void ClampWeight(const T ratio)
   * @brief Clamps the weights between AverageValue/Ratio and AverageValue*Ratio
   * @param[in] ratio   Ratio to clamp
   */
  void ClampWeight(const double ratio);

  /** List of data linked to cluster */
  std::vector<ClusterData> cluster_data_;
  /** List of cluster */
  std::vector<Cluster> clusters_;
  /** Gradation, 0 = uniform */
  double gradation_;
  /** Weight, depending on mesh curvature */
  double* custom_weights_;
};

}  // namepsace LTS5



#endif //__ISOTROPICCLUSTERINGMETRIC__
