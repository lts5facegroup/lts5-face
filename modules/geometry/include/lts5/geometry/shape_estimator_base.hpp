/**
 *  @file   shape_estimator_base.hpp
 *  @brief  Interface for Local shape estimator functions classes
 *  @ingroup geometry
 *
 *  @author Gabriel Cuendet
 *  @date   12/04/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef LTS5_SHAPE_ESTIMATOR_BASE_HPP
#define LTS5_SHAPE_ESTIMATOR_BASE_HPP

#include <vector>
#include <cmath>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/geometry/aabb.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

  /**
   *  @class  ShapeEstimatorBase
   *  @brief  Base class defining the interface of shape estimator functions
   *  @author Gabriel Cuendet
   *  @date   12/04/2016
   *  @ingroup geometry
   */
template< typename T>
class LTS5_EXPORTS ShapeEstimatorBase {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  Clone
   * @fn  ShapeEstimatorBase* Clone(void);
   * @brief Deep copy method
   */
  virtual ShapeEstimatorBase* Clone(void) = 0;

  /**
   * @name  ~ShapeEstimatorBase
   * @fn  ~ShapeEstimatorBase(void)
   * @brief Destructor
   */
  virtual ~ShapeEstimatorBase(void) {}

#pragma mark -
#pragma mark Usage

  /**
   * @name  Fit
   * @fn  virtual T Fit(const std::vector<LTS5::Vector3<T> >& points,
   *                    const std::vector<LTS5::Vector3<T> >& normals,
   *                    const AABB& bbox) = 0
   * @brief Fit the shape approximant to the input points
   * @param[in] points   Vertices within the support radius of the cell
   * @param[in] normals  Normals of the vertices
   * @param[in] bbox     Bounding box of the current cell
   * @return    error    fitting error
   */
  virtual T Fit(const std::vector<LTS5::Vector3<T> >& points,
                const std::vector<LTS5::Vector3<T> >& normals,
                const AABB<T>& bbox) = 0;

  /**
   * @name  Fit
   * @fn virtual T Fit(const std::vector<LTS5::Vector3<T> >& points,
   *                   const std::vector<LTS5::Vector3<T> >& normals,
   *                   const AABB& bbox, const T& R) = 0
   * @brief Fit the shape approximant to the input points
   * @param[in] points   Vertices within the support radius of the cell
   * @param[in] normals  Normals of the vertices
   * @param[in] bbox     Bounding box of the current cell
   * @param[in] R        Radius of the support sphere (used for the weights)
   * @return    error    fitting error
   */
  virtual T Fit(const std::vector<LTS5::Vector3<T> >& points,
                const std::vector<LTS5::Vector3<T> >& normals,
                const AABB<T>& bbox, const T& Ri) = 0;

  /**
   * @name  Evaluate
   * @fn  virtual void Evaluate(const LTS5::Vector3<T>& x) = 0
   * @brief Evalute the value of x, given the approximation
   * @param[in]  x       Point to evaluate
   * @param[out] SwQ     Sum Qi(x)wi(x)
   * @param[out] Sw      Sum wi(x)
   */
  virtual void Evaluate(const LTS5::Vector3<T>& x, T* SwQ, T* Sw) = 0;

  /**
   * @name  EvaluateSquare
   * @fn  virtual void EvaluateSquare(const LTS5::Vector3<T>& x, T* SwQ2, T* Sw) = 0
   * @brief Evalute the squared value of x, given the approximation
   * @param[in]  x     Point to evaluate
   * @param[out] SwQ2  Sum Qi(x)^2 wi(x)
   * @param[out] Sw    Sum wi(x)
   */
  virtual void EvaluateSquare(const LTS5::Vector3<T>& x, T* SwQ2, T* Sw) = 0;

  /**
   * @name  EvaluateGrad
   * @fn  virtual void EvaluateGrad(const LTS5::Vector3<T>& x,
   *                                LTS5::Vector3<T>* SwGradQ, T* Sw)
   * @brief Evaluate gradient of Q at x
   * @param[in]  x        Point
   * @param[out] SwGradQ  Sum Grad(Q_i)(x)wi(x)
   * @param[out] Sw       Sum wi(x)
   */
  virtual void EvaluateGrad(const LTS5::Vector3<T>& x,
                            LTS5::Vector3<T>* SwGradQ, T* Sw) = 0;

  /**
   * @name  EvaluateGradSquare
   * @fn  virtual void EvaluateGradSquare(const LTS5::Vector3<T>& x,
   *                                      LTS5::Vector3<T>* SwGradQ2, T* Sw) = 0
   * @brief Evaluate the gradient of Q^2 at x
   * @param[in]  x         Point to evaluate
   * @param[out] SwGradQ2  Sum Grad(Q_i^2)(x) wi(x)
   * @param[out] Sw        Sum wi(x)
   */
  virtual void EvaluateGradSquare(const LTS5::Vector3<T>& x,
                                  LTS5::Vector3<T>* SwGradQ2, T* Sw) = 0;

  /**
   * @name  EvaluateGradQ
   * @fn virtual void EvaluateGradQ(const LTS5::Vector3<T>& x,
   *                                LTS5::Vector3<T>* grad) = 0
   * @brief Evalute the gradient of x, given the approximation
   * @param[in]  x       Point to evaluate
   * @param[out] grad    Gradient of the quadric evaluated at x
   */
  virtual void EvaluateGradQ(const LTS5::Vector3<T>& x,
                             LTS5::Vector3<T>* grad) = 0;

  /**
   * @name  EvaluateQ
   * @fn virtual T EvaluateQ(const LTS5::Vector3<T>& x) = 0
   * @brief Evalute the value of x, given the approximation
   * @param[in]  x       Point to evaluate
   * @return     val     Value of the quadric evaluated at x
   */
  virtual T EvaluateQ(const LTS5::Vector3<T>& x) = 0;

  /**
   * @name Epsilon
   * @fn T Epsilon(const std::vector<LTS5::Vector3<T> >& points)
   * @brief Compute the unweighted approximation error on points
   * @param[in]     points       Points of the mesh within the support ball
   * @return        error        epsilon, the approx error
   */
  virtual T Epsilon(const std::vector<LTS5::Vector3<T> >& points) {
    T max_error = 0.0;

    for (auto p_it = points.begin(); p_it != points.end(); ++p_it) {
      LTS5::Vector3<T> grad;
      this->EvaluateGradQ((*p_it), &grad);

      T val = this->EvaluateQ(*p_it);
      T error = std::abs(val) / grad.Norm();
      if (std::isnan(val)) {
        max_error = std::numeric_limits<T>::max();
      } else if (error > max_error) {
        max_error = error;
      }

      // DEBUG PURPOSE
      //std::cout << "x = " << (*p_it) << std::endl;
      //std::cout << "Grad(Q)(x) = " << grad <<  "   |Grad| = " << grad.Norm() << std::endl;
      //std::cout << "Q(x) = " << val << std::endl;
    }

    return max_error;
  }
};

#pragma mark -
#pragma mark Declaration

/** Float ShapeEstimatorBase */
template class ShapeEstimatorBase<float>;
/** Double ShapeEstimatorBase */
template class ShapeEstimatorBase<double>;

} // namespace LTS5
#endif //LTS5_SHAPE_ESTIMATOR_BASE_HPP
