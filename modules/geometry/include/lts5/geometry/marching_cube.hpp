/**
 *  @file   marching_cube.hpp
 *  @brief  MarchingCube declaration
 *  @ingroup geometry
 *
 *  @author Gabriel Cuendet
 *  @date   31/05/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef __LTS5_MARCHING_CUBE__
#define __LTS5_MARCHING_CUBE__

#include <vector>
#include <stack>

#include "lts5/utils/sys/parallel.hpp"
#include "lts5/geometry/aabb.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/multiscale_partition_unity.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  MarchingCube
 *  @brief  Class implementing marching cube
 *  @author Gabriel Cuendet
 *  @date   31/05/2016
 *  @ingroup geometry
 */
template<typename T>
class LTS5_EXPORTS MarchingCube {

 public :

  /** Triangle type */
  using Triangle = typename Mesh<T>::Triangle;
  /** Vertex type */
  using Vertex = typename Mesh<T>::Vertex;

  /**
   *  @struct  Cube
   *  @brief  Struct to describe a cube, with values attached to vertices
   *  @author Gabriel Cuendet
   *  @date   31/05/2016
   *
   *                 v4__________e4__________v5
   *                  /|                    /|
   *                 / |                   / |
   *              e7/  |                e5/  |
   *               /___|______e6_________/   |
   *            v7|    |                 |v6 |e9
   *              |    |e8               |   |
   *              |    |                 |e10|
   *           e11|    |                 |   |
   *              |    |_________________|___|
   *              |   / v0      e0       |   /v1
   *              |  /                   |  /
   *              | /e3                  | /e1
   *              |/_____________________|/
   *              v3         e2          v2
   *
   */
    struct Cube {
    /**< Level of the cube */
    int level;
    /**< Bounding box*/
    AABB<T> bbox;
    /**< Corners following the convention above */
    std::vector<LTS5::Vector3<T> > corners;
    /**< Values of the implicit at the corners */
    std::vector<T> values;

    /**
     * @name Cube
     * @brief Constructor
     * @param[in] level  Level of that cube
     * @param[in] bbox   Bbox defining the cube and from which you compute corners
     */
    Cube(const int& level, const AABB<T>& bbox) : level(level), bbox(bbox),
      corners(8), values(8) {

      corners[0] = bbox.min_;
      corners[1] = LTS5::Vector3<T>(bbox.max_.x_, bbox.min_.y_, bbox.min_.z_);
      corners[2] = LTS5::Vector3<T>(bbox.max_.x_, bbox.min_.y_, bbox.max_.z_);
      corners[3] = LTS5::Vector3<T>(bbox.min_.x_, bbox.min_.y_, bbox.max_.z_);
      corners[4] = LTS5::Vector3<T>(bbox.min_.x_, bbox.max_.y_, bbox.min_.z_);
      corners[5] = LTS5::Vector3<T>(bbox.max_.x_, bbox.max_.y_, bbox.min_.z_);
      corners[6] = bbox.max_;
      corners[7] = LTS5::Vector3<T>(bbox.min_.x_, bbox.max_.y_, bbox.max_.z_);
    }

    /**
     * @name get_corners
     * @brief Getter for the corners of the bounding box
     */
      const std::vector<Vector3<T> >& get_corners(void) const {
      return this->corners;
    }

    /**
     * @name get_corners
     * @brief Getter for the corners of the bounding box
     */
      const std::vector<T>& get_values(void) const {
      return this->values;
    }
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name  MarchingCube
   * @fn  MarchingCube(void)
   * @brief Default constructor of the class
   */
  MarchingCube(void);

  /**
   * @name  MarchingCube
   * @fn  MarchingCube(int max_level)
   * @brief Constructor of the class
   * @param[in]  max_level
   */
  MarchingCube(int max_level);

  /**
   * @name  MarchingCube
   * @fn  MarchingCube(const MarchingCube& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  MarchingCube(const MarchingCube& other) = delete;

  /**
   * @name  operator=
   * @fn  MarchingCube& operator=(const MarchingCube& rhs) = delete
   * @brief Assign operator
   * @param[in] rhs Object to assign from
   */
  MarchingCube& operator=(const MarchingCube& rhs) = delete;

  /**
   * @name  ~MarchingCube
   * @fn  ~MarchingCube(void)
   * @brief Destructor of the class
   */
  ~MarchingCube(void);

  /**
   * @name  Init
   * @fn  void Init(const MPU<T>& implicit)
   * @brief Initialize the marching cube with an implicit to render
   * @param[in]  implicit the implicit surface to render
   */
  void Init(const MPU<T>& implicit);

#pragma mark -
#pragma mark Usage

  /**
   * @name  ComputeMesh
   * @fn  int ComputeMesh(Mesh<T>* mesh)
   * @brief Compute the mesh from the implicit
   * @param[out] mesh the computed mesh
   * @return -1 if error, 0 otherwise
   */
  int ComputeMesh(Mesh<T>* mesh);

private:

  /**
   * @name  DivideAndStack
   * @fn  void DivideAndStack(const Cube& cube, std::stack<Cube*>* stack)
   * @brief Divide the cube into eight and push on the stack
   * @param[in]  cube
   * @param[out] stack
   */
  void DivideAndStack(const Cube& cube, std::stack<Cube*>* stack);

  /**
   * @name Polygonize
   * @fn int Polygonize(const Cube& cube, Mesh<T>* mesh)
   * @brief Polygonize a single cube and update the mesh
   * @param[in]  cube  current cube to march
   * @param[out] mesh  mesh to update
   * @returns the number of triangles
   */
  int Polygonize(const Cube& cube, Mesh<T>* mesh);

  /**
   * @name InterpolateVertex
   * @fn void InterpolateVertex(const LTS5::Vector3<T>& point1,
   *                            const LTS5::Vector3<T>& point2, const T& value1,
   *                            const T& value2, const T& isolevel,
   *                            LTS5::Vector3<T> interp)
   * @brief Linearly interpolate between two vertices with given values to find
   *        the point with value isolevel
   * @param[in]  point1
   * @param[in]  point2
   * @param[in]  value1
   * @param[in]  value2
   * @param[in]  isolevel
   * @returns interp
   */
  LTS5::Vector3<T> InterpolateVertex(const LTS5::Vector3<T>& point1,
                                     const LTS5::Vector3<T>& point2,
                                     const T& value1, const T& value2,
                                     const T& isolevel);

  /**< Pointer to the implicit surface to render */
  const MPU<T>* implicit_;
  /**< Domain bounding box */
  AABB<T> domain_;
  /**< Maximum level of division of space */
  int max_level_;
  /**< Mutex **/
  Mutex polygonize_mutex_;
};
}  // namespace LTS5
#endif /* __LTS5_MARCHING_CUBE__ */
