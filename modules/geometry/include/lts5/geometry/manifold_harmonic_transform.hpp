/**
 *  @file   manifold_harmonic_transform.hpp
 *  @brief  MHT declaration
 *  @ingroup geometry
 *
 *  @author Gabriel Cuendet
 *  @date   12/09/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef __LTS5_MANIFOLD_HARMONIC_TRANSFORM__
#define __LTS5_MANIFOLD_HARMONIC_TRANSFORM__

#include <vector>

#include "Eigen/Sparse"
#include "Eigen/Dense"
#include "Eigen/SparseCore"

#include "lts5/utils/math/sparse_linear_algebra_wrapper.hpp"
#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  MHT
 *  @brief  Manifold harmonic transform implementation
 *  @author Gabriel Cuendet
 *  @date   12/09/2016
 *  @ingroup geometry
 */
template<typename T>
class LTS5_EXPORTS MHT {

 public:

  enum VizNormalizationType {
    kGlobal = 0,
    kPerMode = 1
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name  MHT
   * @fn  MHT(void);
   * @brief Default class constructor
   */
  MHT(void);

  /**
   * @name  MHT
   * @fn  MHT(const Eigen::SparseMatrix<T>& Q,
              const Eigen::SparseMatrix<T>& D,
              const int& n_basis)
   * @brief Class constructor
   * @param[in] Q       sparse stifness matrix
   * @param[in] D       diagonal lumped mass matrix
   * @param[in] n_basis number of basis vector to compute
   */
  MHT(const Eigen::SparseMatrix<T>& Q,
      const Eigen::SparseMatrix<T>& D,
      const int& n_basis);

  /**
   * @name  Init
   * @fn  void Init(const Eigen::SparseMatrix<T>& Q, const Eigen::SparseMatrix<T>& D, const int& n_basis)
   * @brief Initialize the transform by setting important params and computing the basis
   * @param[in] Q       sparse stifness matrix
   * @param[in] D       diagonal lumped mass matrix
   * @param[in] n_basis number of basis vector to compute
   * @return 0 if success, -1 otherwise
   */
  int Init(const Eigen::SparseMatrix<T>& Q,
            const Eigen::SparseMatrix<T>& D,
            const int& n_basis);

  /**
   * @name  Read
   * @fn  int Read(const std::string& filename)
   * @brief Read MHT from file
   * @return 0 if success, -1 otherwise
   */
  int Read(const std::string& filename);

  /**
   * @name  Write
   * @fn  void Write(const std::string& filename)
   * @brief Write the MHT to file
   * @return 0 if success, -1 otherwise
   */
  int Write(const std::string& filename);

#pragma mark -
#pragma mark Getters and Setters

  /**
   * @name  get_eigenvalues
   * @fn  const Eigen::Matrix<T, Eigen::Dynamic, 1>& get_eigenvalues(void) const
   * @brief Getter for the computed eigenvalues
   * @return const ref to the eigenvalues
   */
  const Eigen::Matrix<T, Eigen::Dynamic, 1>& get_eigenvalues(void) const {
    return eigenvalues_;
  }

  /**
   * @name  get_eigenvalues
   * @fn  Eigen::Matrix<T, Eigen::Dynamic, 1>& get_eigenvalues(void)
   * @brief Getter for the computed eigenvalues
   * @return ref to the eigenvalues
   */
  Eigen::Matrix<T, Eigen::Dynamic, 1> get_eigenvalues(void) {
    return eigenvalues_;
  }

  /**
   * @name  get_basis
   * @fn  const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& get_basis(void) const
   * @brief Getter for the computed basis
   * @return const ref to an Eigen::Matrix
   */
  const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& get_basis(void) const {
    return basis_;
  }

  /**
   * @name  get_basis
   * @fn  Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& get_basis(void)
   * @brief Getter for the computed basis
   * @return ref to an Eigen::Matrix
   */
  Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& get_basis(void) {
    return basis_;
  }

  /**
   * @name  get_basis_t
   * @fn  const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& get_basis_t(void) const
   * @brief Getter for the computed basis (transposed version)
   * @return const ref to an Eigen::Matrix
   */
  const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& get_basis_t(void) const {
    return basis_t_;
  }

  /**
   * @name  get_basis_t
   * @fn  Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& get_basis_t(void)
   * @brief Getter for the computed basis (transposed version)
   * @return ref to an Eigen::Matrix
   */
  Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& get_basis_t(void) {
    return basis_t_;
  }

  /**
   * @name  get_D
   * @fn  const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& get_D(void) const
   * @brief Getter for the matrix D
   * @return const ref to an Eigen::Matrix
   */
  const Eigen::SparseMatrix<T>& get_D(void) const {
    return D_;
  }

  /**
   * @name  get_D
   * @fn  Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& get_D(void)
   * @brief Getter for the matrix D
   * @return ref to an Eigen::Matrix
   */
  Eigen::SparseMatrix<T>& get_D(void) {
    return D_;
  }

  /**
   * @name  get_eigenpairs_per_band
   * @fn  int get_eigenpairs_per_band(void) const
   * @brief Getter for eigenpairs_per_band parameter
   */
  int get_eigenpairs_per_band(void) const {
    return eigenpairs_per_band_;
  }

  /**
   * @name  set_eigenpairs_per_band
   * @fn  void set_eigenpairs_per_band(const int& n)
   * @brief Setter for eigenpairs_per_band parameter
   */
  void set_eigenpairs_per_band(const int& n) {
    eigenpairs_per_band_ = n;
  }

  /**
   * @name  get_decomposition_params
   * @fn  SparseLinearAlgebra::Arpack::non_sym_eigen_params<T>& get_decomposition_params(void)
   * @brief Get the parameters from the SparseLinearAlgebra decomposition
   * @return parameters
   */
  SparseLinearAlgebra::Arpack::non_sym_eigen_params<T>& get_decomposition_params(void) {
    return eig_decomposition_.get_params();
  }

  /**
   * @name  get_decomposition_params
   * @fn  const SparseLinearAlgebra::Arpack::non_sym_eigen_params<T>& get_decomposition_params(void) const
   * @brief Get the parameters from the SparseLinearAlgebra decomposition
   * @return parameters
   */
  const SparseLinearAlgebra::Arpack::non_sym_eigen_params<T>& get_decomposition_params(void) const {
    return eig_decomposition_.get_params();
  }

#pragma mark -
#pragma mark Usage

  /**
   * @name  Transform
   * @fn  void Transform(const std::vector<T>& spat_vec,
   *                     std::vector<T>* freq_vec) const
   * @brief Transform the content of the vector using the MHT, from spatial
   *        domain to frequency domain
   * @param[in]  spat_vec  Spatial coordinates vector
   * @param[out] freq_vec  Spectral (frequencies) coordinates vector
   */
  void Transform(const std::vector<T>& spat_vec,
                 std::vector<T>* freq_vec) const;

  /**
   * @name  Transform
   * @fn  void Transform(const std::vector<LTS5::Vector2<T> >& spat_vec,
   *                     std::vector<LTS5::Vector2<T> >* freq_vec) const
   * @brief Transform the content of the vector using the MHT, from spatial
   *        domain to frequency domain
   * @param[in]  spat_vec  Spatial coordinates vector
   * @param[out] freq_vec  Spectral (frequencies) coordinates vector
   */
  void Transform(const std::vector<LTS5::Vector2<T> >& spat_vec,
                 std::vector<LTS5::Vector2<T> >* freq_vec) const;

  /**
   * @name  Transform
   * @fn  void Transform(const std::vector<LTS5::Vector3<T> >& spat_vec,
   *                     std::vector<LTS5::Vector3<T> >* freq_vec) const
   * @brief Transform the content of the vector using the MHT, from spatial
   *        domain to frequency domain
   * @param[in]  spat_vec  Spatial coordinates vector
   * @param[out] freq_vec  Spectral (frequencies) coordinates vector
   */
  void Transform(const std::vector<LTS5::Vector3<T> >& spat_vec,
                 std::vector<LTS5::Vector3<T> >* freq_vec) const;

  /**
   * @name  Transform
   * @fn  void Transform(const std::vector<LTS5::Vector4<T> >& spat_vec,
   *                     std::vector<LTS5::Vector4<T> >* freq_vec) const
   * @brief Transform the content of the vector using the MHT, from spatial
   *        domain to frequency domain
   * @param[in]  spat_vec  Spatial coordinates vector
   * @param[out] freq_vec  Spectral (frequencies) coordinates vector
   */
  void Transform(const std::vector<LTS5::Vector4<T> >& spat_vec,
                 std::vector<LTS5::Vector4<T> >* freq_vec) const;

  /**
   * @name  Transform
   * @fn  void Transform(const std::vector<LTS5::Vector4<unsigned char> >& spat_vec,
                 std::vector<LTS5::Vector4<T> >* freq_vec) const
   * @brief Transform the content of the vector using the MHT, from spatial
   *        domain to frequency domain
   * @param[in]  spat_vec  Spatial coordinates vector
   * @param[out] freq_vec  Spectral (frequencies) coordinates vector
   */
  void Transform(const std::vector<LTS5::Vector4<unsigned char> >& spat_vec,
                 std::vector<LTS5::Vector4<T> >* freq_vec) const;

  /**
   * @name  Transform
   * @fn  void Transform(const std::vector<LTS5::Vector3<T> >& spat_vec,
   *                     Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* freq_vec) const
   * @brief Transform the content of the vector using the MHT, from spatial
   *        domain to frequency domain
   * @param[in]  spat_vec  Spatial coordinates vector
   * @param[out] freq_vec  Spectral (frequencies) coordinates vector
   */
  void Transform(const std::vector<LTS5::Vector3<T> >& spat_vec,
                 Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* freq_vec) const;

  /**
   * @name  Transform
   * @fn  void Transform(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& spat_vec,
   *                     Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* freq_vec) const
   * @brief Transform the content of the vector using the MHT, from spatial
   *        domain to frequency domain
   * @param[in]  spat_vec  Spatial coordinates vector
   * @param[out] freq_vec  Spectral (frequencies) coordinates vector
   */
  void Transform(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& spat_vec,
                 Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* freq_vec) const;

  /**
   * @name  InvTransform
   * @fn  void InvTransform(const std::vector<T>& freq_vec,
   *                        std::vector<T>* spat_vec) const
   * @brief Inverse MHT transform, from frequency domain to spatial domain
   * @param[in]  freq_vec  Spectral (frequencies) coordinates vector
   * @param[out] spat_vec  Spatial coordinates vector
   */
  void InvTransform(const std::vector<T>& freq_vec,
                    std::vector<T>* spat_vec) const;

  /**
   * @name  InvTransform
   * @fn  void InvTransform(const std::vector<LTS5::Vector2<T> >& freq_vec,
   *                        std::vector<LTS5::Vector2<T> >* spat_vec) const
   * @brief Inverse MHT transform, from frequency domain to spatial domain
   * @param[in]  freq_vec  Spectral (frequencies) coordinates vector
   * @param[out] spat_vec  Spatial coordinates vector
   */
  void InvTransform(const std::vector<LTS5::Vector2<T> >& freq_vec,
                    std::vector<LTS5::Vector2<T> >* spat_vec) const;

  /**
   * @name  InvTransform
   * @fn  void InvTransform(const std::vector<LTS5::Vector3<T> >& freq_vec,
   *                        std::vector<LTS5::Vector3<T> >* spat_vec) const
   * @brief Inverse MHT transform, from frequency domain to spatial domain
   * @param[in]  freq_vec  Spectral (frequencies) coordinates vector
   * @param[out] spat_vec  Spatial coordinates vector
   */
  void InvTransform(const std::vector<LTS5::Vector3<T> >& freq_vec,
                    std::vector<LTS5::Vector3<T> >* spat_vec) const;

  /**
   * @name  InvTransform
   * @fn  void InvTransform(const std::vector<LTS5::Vector4<T> >& freq_vec,
   *                        std::vector<LTS5::Vector4<T> >* spat_vec) const
   * @brief Inverse MHT transform, from frequency domain to spatial domain
   * @param[in]  freq_vec  Spectral (frequencies) coordinates vector
   * @param[out] spat_vec  Spatial coordinates vector
   */
  void InvTransform(const std::vector<LTS5::Vector4<T> >& freq_vec,
                    std::vector<LTS5::Vector4<T> >* spat_vec) const;

  /**
   * @name  InvTransform
   * @fn  void InvTransform(const std::vector<LTS5::Vector4<T> >& freq_vec,
   *                        std::vector<LTS5::Vector4<unsigned char> >* spat_vec) const
   * @brief Inverse MHT transform, from frequency domain to spatial domain
   * @param[in]  freq_vec  Spectral (frequencies) coordinates vector
   * @param[out] spat_vec  Spatial coordinates vector
   */
  void InvTransform(const std::vector<LTS5::Vector4<T> >& freq_vec,
                    std::vector<LTS5::Vector4<unsigned char> >* spat_vec) const;

  /**
   * @name  InvTransform
   * @fn  void InvTransform(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& freq_vec,
   *                        Eigen::Matrix<T,Eigen::Dynamic, Eigen::Dynamic>* spat_vec) const
   * @brief Inverse MHT transform, from frequency domain to spatial domain
   * @param[in]  freq_vec  Spectral (frequencies) coordinates vector
   * @param[out] spat_vec  Spatial coordinates vector
   */
  void InvTransform(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& freq_vec,
                    Eigen::Matrix<T,Eigen::Dynamic, Eigen::Dynamic>* spat_vec) const;

#pragma mark -
#pragma mark Private attributes and methods

 private:

  /**
   * @name  ComputeBandByBandBasis
   * @fn  int ComputeBandByBandBasis(const Eigen::SparseMatrix<T>& Q)
   * @brief Compute basis with a large number of vector, using Band-by-Band algo
   * @param[in]  Q                  sparse stifness matrix
   * @param[in]  n_values_per_band  number of values to compute in each band
   * @return 0 if success, -1 otherwise
   */
  int ComputeBandByBandBasis(const Eigen::SparseMatrix<T>& Q, const int& n_values_per_band);

  /**
   * @name  ComputeBasis
   * @fn  int ComputeBasis(const Eigen::SparseMatrix<T>& Q);
   * @brief From Q and D, perform Eigendecomposition to compute the MHB
   * @param[in]  Q  sparse stifness matrix
   * @return 0 if success, -1 otherwise
   */
  int ComputeBasis(const Eigen::SparseMatrix<T>& Q);

  /**
   * @name  DNormalizeBasis
   * @fn  void DNormalizeBasis(void)
   * @brief Normalize the basis with respect to the inner product defined with D
   */
  void DNormalizeBasis(void);

  /**< Dimension of the basis */
  int ndim_;
  /**< Number of basis vectors */
  int nvec_;
  /**< D, lumped mass matrix */
  Eigen::SparseMatrix<T> D_;
  /**< Q, stifness matrix */
  //Eigen::SparseMatrix<T, Eigen::Dynamic, Eigen::Dynamic> Q_;
  /**< Manifold harmonic basis */
  Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> basis_;
  /**< Manifold harmonic basis (transposed version) */
  Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> basis_t_;
  /**< Eigenvalues */
  Eigen::Matrix<T, Eigen::Dynamic, 1> eigenvalues_;
  /**< Number of eigenpairs to compute in each band, when using the band-by-band algo */
  int eigenpairs_per_band_;
  /**< the sparse eigenpairs decomposition */
  SparseLinearAlgebra::LuNonSymEigShift<T> eig_decomposition_;

};

}  // namespace LTS5
#endif /* __LTS5_MANIFOLD_HARMONIC_TRANSFORM__ */
