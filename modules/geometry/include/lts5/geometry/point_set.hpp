/**
 *  @file   point_set.hpp
 *  @brief  Function acting on point sets.
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   05/01/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_GEOMETRY_POINT_SET__
#define __LTS5_GEOMETRY_POINT_SET__

#include <utility>
#include <vector>
#include <limits>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<typename T>
using PointWithId = std::pair<Vector3<T>, size_t>;

template<typename T>
using PointWithIdWithIndex = std::pair<PointWithId<T>, size_t>;

/**
 * @struct  ClosestPointPair
 * @brief Close point pair information
 * @tparam T    Data type
 */
template<typename T>
struct ClosestPointPair {

  /**
   * @name  ClosestPointPair
   * @fn    ClosestPointPair() = default
   * @brief Constructor
   */
  ClosestPointPair() = default;

  /**
   * @name  ClosestPointPair
   * @fn    ClosestPointPair(const T& dist, const size_t& idx,
   *                            const PointWithIdWithIndex<T>& p)
   * @brief Constructor
   * @param[in] dist    Distance between pair
   * @param[in] idx     Index of second element
   * @param[in] p       Point with index
   */
  ClosestPointPair(const T& dist,
                   const size_t& idx,
                   const PointWithIdWithIndex<T>& p) : dist(dist),
                                                       index(idx),
                                                       pts(p) {}

  /** Distance */
  T dist;
  /** Index */
  size_t index;
  /** Point */
  PointWithIdWithIndex<T> pts;
};

/**
 * @name    FindClosestPointPairs
 * @fn  void FindClosestPointPairs(const std::vector<PointWithId<T>>& sorted_points,
                           std::vector<ClosestPointPair<T>>* pairs)
 * @brief Find for each point the closest neighbour
 * @param[in] sorted_points List of point to match
 * @param[out] pairs  List of pairs of points
 * @tparam T    Data type
 */
template<typename T>
void FindClosestPointPairs(const std::vector<PointWithId<T>>& sorted_points,
                           std::vector<ClosestPointPair<T>>* pairs) {
  pairs->clear();
  for (size_t k = 0; k < sorted_points.size(); ++k) {
    const auto& sp = sorted_points[k];
    size_t sp_idx = k;
    const auto& base_pts = sp.first;

    size_t best_idx = (sp_idx + 1) % sorted_points.size();
    T dist = (base_pts - sorted_points[best_idx].first).Norm();

    // Iterate over upper part
    for (size_t m = k + 2; m < sorted_points.size(); ++m) {
      const auto& pts = sorted_points[m].first;
      T q = std::abs(base_pts[1] - pts[1]);
      if (q < dist) {
        // early stopping according to y difference
        T t = (base_pts - pts).Norm();
        if (t < dist) {
          dist = t;
          best_idx = m;
        }
      } else {
        // Next point (i.e. k+2) is further away than 'k+1', since point are
        // sorted can skip remaining checks.
        break;
      }
    }
    for (size_t m = k - 1; m > -1; --m) {
      const auto& pts = sorted_points[m].first;
      T q = std::abs(base_pts[1] - pts[1]);
      if (q < dist) {
        // early stopping according to y difference
        T t = (base_pts - pts).Norm();
        if (t < dist) {
          dist = t;
          best_idx = m;
        }
      } else {
        // Next point (i.e. k+2) is further away than 'k+1', since point are
        // sorted can skip remaining checks.
        break;
      }
    }
    // Done
    pairs->emplace_back(dist, best_idx, std::make_pair(sp, k));
  }
}

template<typename T>
void ChoosePairsAndUpdateIndex(
        const std::vector<ClosestPointPair<T>>& closest_point_pairs,
        const std::vector<PointWithId<T>>& sorted_points,
        std::vector<size_t>* matched_points,
        std::vector<bool>* chosen) {
  // Init output
  chosen->clear();
  chosen->resize(closest_point_pairs.size(), false);
  // Sort pairs
  std::vector<ClosestPointPair<T>> best_pairs = closest_point_pairs;
  std::sort(best_pairs.begin(),
            best_pairs.end(),
            [](const ClosestPointPair<T>& a, const ClosestPointPair<T>& b) {
              return a.dist < b.dist;
            });
  // Update match index
  for (size_t k = 0; k < best_pairs.size(); ++k) {
    const auto& cp = best_pairs[k];
    size_t best_sorted_pts_idx = cp.index;
    size_t sorted_point_idx = cp.pts.second;
    size_t point_idx = sorted_points[sorted_point_idx].second;
    size_t best_point_idx = sorted_points[best_sorted_pts_idx].second;

    if (!chosen->at(sorted_point_idx) &&
        !chosen->at(best_sorted_pts_idx)) {
      matched_points->at(point_idx) = best_point_idx;
      matched_points->at(best_point_idx) = point_idx;
      chosen->at(sorted_point_idx) = true;
      chosen->at(best_sorted_pts_idx) = true;
    }
  }
}

/**
 * @name    MatchPoints
 * @fn  void MatchPoints(const std::vector<PointWithId<T>>& points,
                         std::vector<size_t>* matched_index)
 * @brief   Match points recursively to get n/2 pairs and an optional single
 *  point.
 * @param[in]   points  List of points to match
 * @param[in,out] matched_index Matched index
 * @tparam T    Data type
 */
template<typename T>
void MatchPoints(const std::vector<PointWithId<T>>& points,
                 std::vector<size_t>* matched_index) {
  switch (points.size()) {
    case 0:
      // Empty list of points, nothing to do
      break;

    case 1: {
      // Single points -> self assigned
      const auto &p = points.front();
      matched_index->at(p.second) = p.second;
      break;
    }

    // More than 1
    default: {
      // Sort points by y-direction
      std::vector<PointWithId<T>> sorted_points = points;
      std::sort(sorted_points.begin(),
                sorted_points.end(),
                [](const PointWithId<T>& a, const PointWithId<T>& b) {
                  return a.first[1] < b.first[1];
                });
      // Find closest pairs
      std::vector<ClosestPointPair<T>> closest_point_pairs;
      FindClosestPointPairs(sorted_points, &closest_point_pairs);
      // Update point selection
      std::vector<bool> chosen;
      ChoosePairsAndUpdateIndex(closest_point_pairs,
                                sorted_points,
                                matched_index,
                                &chosen);
      // Pick non-paired points and continue recursion.
      std::vector<PointWithId<T>> still_alive;
      for (size_t k = 0; k < chosen.size(); ++k) {
        if (!chosen[k]) {
          still_alive.emplace_back(sorted_points[k]);
        }
      }
      MatchPoints(still_alive, matched_index);
      break;
    }
  }
}

/**
 * @name    CalculateNearestPointPairs
 * @fn  void LTS5_EXPORTS CalculateNearestPointPairs(
 *                              const std::vector<Vector3<T>>& points,
                                std::vector<size_t>* matched_index)
 * @brief   Calculate index of nearest points pairs for a given list.
 * @see https://github.com/unibas-gravis/scalismo
 * @param[in] points    List of points to group by pair
 * @param[in,out] matched_index Matched index
 * @tparam T    Data type
 */
template<typename T>
void LTS5_EXPORTS
CalculateNearestPointPairs(const std::vector<Vector3<T>>& points,
                           std::vector<size_t>* matched_index) {

  // Init output
  matched_index->clear();
  matched_index->resize(points.size(), std::numeric_limits<size_t>::max());
  // Add index to points to keep track of position
  std::vector<PointWithId<T>> point_with_id;
  for (size_t k = 0; k < points.size(); ++k) {
    point_with_id.emplace_back(points[k], k);
  }
  // Match pairs
  MatchPoints(point_with_id, matched_index);
}
}  // namespace LTS5
#endif //__LTS5_GEOMETRY_POINT_SET__
