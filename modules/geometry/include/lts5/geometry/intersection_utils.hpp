/**
 *  @file   intersection_utils.hpp
 *  @brief  Intersection utility functions
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   02/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __INTERSECTION_UTILS__
#define __INTERSECTION_UTILS__

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mesh.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<typename T>
using Vertex = typename Mesh<T>::Vertex;


/**
 *  @name IntersectMeshWithSegment
 *  @fn bool LTS5_EXPORTS IntersectMeshWithSegment(const Mesh<T>& mesh,
                                           const Vertex<T>& p,
                                           const Vertex<T>& q)
 *  @brief  Test if the mesh object intersect with a given segment PQ
 *  @ingroup geometry
 *  @param[in]  mesh  Mesh to compute intersection with
 *  @param[in]  p     Starting point of the segment to test against
 *  @param[in]  q     Stoping point of the segment to test against
 *  @return True if segment PQ intersect with mesh, False otherwise
 */
template<typename T>
bool LTS5_EXPORTS IntersectMeshWithSegment(const Mesh<T>& mesh,
                                           const Vertex<T>& p,
                                           const Vertex<T>& q);

/**
 *  @name IntersectTriWithSegment
 *  @fn bool LTS5_EXPORTS IntersectTriWithSegment(const Vertex<T>& p,
                                                  const Vertex<T>& q,
                                                  const Vertex<T>& A,
                                                  const Vertex<T>& B,
                                                  const Vertex<T>& C,
                                                  const bool& cull_face,
                                                  const T& tol,
                                                  T* t)
 *  @brief  Test if the triangle ABC intersects with a given segment PQ
 *  @ingroup geometry
 *  @param[in]  p           Starting point of the segment to test against
 *  @param[in]  q           Stoping point of the segment to test against
 *  @param[in]  A           First vertex of the triangle
 *  @param[in]  B           Second vertex of the triangle
 *  @param[in]  C           Third vertex of the triangle
 *  @param[in]  cull_face   If `True`, do not consider back facing triangles
 *  @param[in]  tol         Tolerance to use when computing barycentric
 *                          coordinates
 *  @param[out] t           position along the segment PQ, t in [0, 1]
 *  @return True if segment intersect with the triangle, False otherwise
 */
template<typename T>
bool LTS5_EXPORTS IntersectTriWithSegment(const Vertex<T>& p,
                                          const Vertex<T>& q,
                                          const Vertex<T>& A,
                                          const Vertex<T>& B,
                                          const Vertex<T>& C,
                                          const bool& cull_face,
                                          const T& tol,
                                          T* t);

/**
 *  @name IntersectTriWithSegment
 *  @fn bool LTS5_EXPORTS IntersectTriWithSegment(const Vertex<T>& p,
                                          const Vertex<T>& q,
                                          const Vertex<T>& A,
                                          const Vertex<T>& B,
                                          const Vertex<T>& C,
                                          T* t)
 *  @brief  Test if the triangle ABC intersects with a given segment PQ
 *  @ingroup geometry
 *  @param[in]  p       Starting point of the segment to test against
 *  @param[in]  q       Stoping point of the segment to test against
 *  @param[in]  A       First vertex of the triangle
 *  @param[in]  B       Second vertex of the triangle
 *  @param[in]  C       Third vertex of the triangle
 *  @param[out] t       position along the segment PQ, t in [0, 1]
 *  @note   Internally using tol = 2*eps for barycentric computation.
 *  @return True if segment intersect with the triangle, False otherwise
 */
template<typename T>
bool LTS5_EXPORTS IntersectTriWithSegment(const Vertex<T>& p,
                                          const Vertex<T>& q,
                                          const Vertex<T>& A,
                                          const Vertex<T>& B,
                                          const Vertex<T>& C,
                                          T* t);

/**
 * @name  IntersectTriWithLine
 * @fn  bool LTS5_EXPORTS IntersectTriWithLine(const Vector3<T>& p,
                                       const Vector3<T>& d,
                                       const Vertex<T>& A,
                                       const Vertex<T>& B,
                                       const Vertex<T>& C,
                                       T* lambda)
 * @brief Test if the triangle ABC intersects with a given line p + lamda * d
 * @ingroup geometry
 * @param[in]  p       A point on the line to test against
 * @param[in]  d       Unit direction vector of the line to test against
 * @param[in]  A       First vertex of the triangle
 * @param[in]  B       Second vertex of the triangle
 * @param[in]  C       Third vertex of the triangle
 * @param[out] lambda  position along the line, p + lambda * d
 * @return True if line intersects with the triangle, false otherwise
 */
template<typename T>
bool LTS5_EXPORTS IntersectTriWithLine(const Vector3<T>& p,
                                       const Vector3<T>& d,
                                       const Vertex<T>& A,
                                       const Vertex<T>& B,
                                       const Vertex<T>& C,
                                       T* lambda);

/**
 * @name    ClosestPointToRay
 * @fn void ClosestPointToRay(const Vector3<T>& p,
                              const Vector3<T>& d,
                              const std::vector<Vertex<T>>& pts,
                              size_t* idx)
 * @brief   Find the closest point to a given ray : r = p + lambda * d
 * @tparam T    Data type
 * @param[in] p     Origin of the ray
 * @param[in] d     Ray's unit direction
 * @param[in] pts   List of points to test against
 * @param[in] idx   Index of the closest point in the list of \p pts.
 */
template<typename T>
void LTS5_EXPORTS ClosestPointToRay(const Vector3<T>& p,
                                    const Vector3<T>& d,
                                    const std::vector<Vertex<T>>& pts,
                                    size_t* idx);

/**
 * @name    PointInTriangle
 * @fn  bool LTS5_EXPORTS PointInTriangle(const Vector3<T>& p,
                                  const Vertex<T>& A,
                                  const Vertex<T>& B,
                                  const Vertex<T>& C)
 * @brief   Compute if point \p p is inside triangle ABC
 * @ingroup geometry
 * @param[in] p     Query point
 * @param[in] A     Triangle point A
 * @param[in] B     Triangle point B
 * @param[in] C     Triangle point C
 * @return True if inside, false otherwise
 */
template<typename T>
bool LTS5_EXPORTS PointInTriangle(const Vector3<T>& p,
                                  const Vertex<T>& A,
                                  const Vertex<T>& B,
                                  const Vertex<T>& C);

}  // namepsace LTS5

#endif //__INTERSECTION_UTILS__
