/**
 *  @file   kdtree.hpp
 *  @brief  Spatial partioning based on KDTree
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   14/12/2020
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KDTREE__
#define __LTS5_KDTREE__

#include <vector>
#include <stack>
#include <algorithm>
#include <cassert>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/vector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Utility

/** N-Dimensional points */
template<int NDim, typename T>
using PointType = typename Point<NDim, T>::Type;

/**
 * @struct  Metric
 * @brief   Utility class to measure distance between points
 * @author  Christophe Ecabert
 * @date    14/12/2020
 * @ingroup geometry
 * @tparam NDim Number of dimension
 * @tparam T    Data type
 */
template<int NDim, typename T>
struct Metric {
 public:
  /**
   * @name  Distance
   * @brief Measure distance between two points `x`, `y`
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Squared distance
   */
  static T Distance(const PointType<NDim, T>& x, const PointType<NDim, T>& y) {
    return (x - y).SquaredNorm();
  }

  /**
   * @name  PlanarDistance
   * @brief Measure planar distance between two points `x`, `y` (i.e. along a
   *    specific dimension)
   * @param[in] x   First point
   * @param[in] y   Second point
   * @param[in] dim Index along which dimension the distance has to be computed.
   * @return    Squared planar distance
   */
  static T PlanarDistance(const PointType<NDim, T>& x,
                          const PointType<NDim, T>& y,
                          int dim) {
    assert(dim >= 0 && dim < NDim &&
           "Dimension index must be smaller than space size");
    T d = x[dim] - y[dim];
    return d * d;
  }
};

/**
 * @struct  PointWithScore
 * @brief   Comtainer for point and its score value
 * @author  Christophe Ecabert
 * @date    14/12/2020
 * @ingroup geometry
 * @tparam NDim Number of dimension
 * @tparam T    Data type
 */
template<int NDim, typename T>
struct PointWithScore {
 public:
  /**
   * @name  PointWithScore
   * @fn    PointWithScore()
   * @brief Constructor
   */
  PointWithScore() : pts(), score(-1.0) {}
  /**
   * @name  PointWithScore
   * @brief Constructor
   * @param[in] x   Point
   * @param[in] score   Score for this point (i.e. distance)
   */
  PointWithScore(const PointType<NDim,T>& x,
                 const T& score) : pts(x),
                                   score(score) {
  }

  /** Point */
  PointType<NDim, T> pts;
  /** Score */
  T score;
};

template<int NDim, typename T>
struct Ordering {

  /**
   * @name  Ordering
   * @fn    explicit Ordering(int dim)
   * @brief Constructor
   * @param[in] dim Dimension along which comparison needs to be done
   */
  explicit Ordering(int dim) : dim(dim) {}

  /**
   * @name  lt
   * @brief Returns `true` if `x` is smaller than `y` for a given direction
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    True if x is smaller than y in a given direction.
   */
  bool lt(const PointType<NDim, T>& x, const PointType<NDim, T>& y) const {
    return x[dim] < y[dim];
  }

  /**
   * @name  gt
   * @brief Returns `true` if `x` is larger than `y` for a given direction
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    True if x is smaller than y in a given direction.
   */
  bool gt(const PointType<NDim, T>& x, const PointType<NDim, T>& y) const {
    return x[dim] > y[dim];
  }

  /** Ordering dimensions */
  int dim;
};




#pragma mark -
#pragma mark Definition


/**
 * @class   KDTree
 * @brief   Spatial partioning based on KDTree. Support {1,2,3,4}D points.
 * @author  Christophe Ecabert
 * @date    14/12/2020
 * @ingroup geometry
 * @tparam  NDim Number of dimension
 * @tparam T    Data type
 */
template<int NDim, typename T>
class KDTree {
 public:
  /** List of points */
  using PointList = std::vector<PointType<NDim, T>>;

  /**
   * @name  KDTree
   * @fn    KDTree()
   * @brief Constructor
   */
  KDTree();

  /**
   * @name  KDTree
   * @fn    KDTree(const KDTree<NDim, T>& other) = delete
   * @brief Copy Constructor
   * @param[in] other   Object to copy from
   */
  KDTree(const KDTree<NDim, T>& other) = delete;

  /**
   * @name  operator=
   * @fn    KDTree<NDim, T>& operator=(const KDTree<NDim, T>& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  KDTree<NDim, T>& operator=(const KDTree<NDim, T>& rhs) = delete;

  /**
   * @name  KDTree
   * @fn    explicit KDTree(const std::vector<PointType<NDim, T>>& points)
   * @brief Constructor
   * @param[in] points List of point to partion
   */
  explicit KDTree(const std::vector<PointType<NDim, T>>& points);

  /**
   * @name  ~KDTree
   * @fn    ~KDTree()
   * @brief Destructor
   */
  ~KDTree();

  /**
   * @name  Build
   * @fn    int Build(const std::vector<PointType<NDim, T>>& points)
   * @brief Build internal tree
   * @param[in] points Points to partition
   * @return -1 if error, 0 otherwise (number of node does not match number of
   *    points)
   */
  int Build(const std::vector<PointType<NDim, T>>& points);

  /**
   * @name  Clear
   * @fn    void Clear()
   * @brief Clear underlying tree
   */
  void Clear();

  /**
   * @name Empty
   * @fn    bool Empty() const
   * @brief Check if tree is empty
   * @return    `true` if empty, `false` otherwise
   */
  bool Empty() const;

  /**
   * @name  Size
   * @fn    size_t Size() const
   * @brief Get tree size
   * @return    Tree size
   */
  size_t Size() const;

  /**
   * @name    FindNearest
   * @fn  PointList FindNearest(const PointType<NDim, T>& x, int n) const;
   * @brief Find N nearest points
   * @param[in] x Query point
   * @param[in] n Number of neighbour to look for.
   * @return  List of neighbouring points
   */
  PointList FindNearest(const PointType<NDim, T>& x, const int& n) const;

 private:
  /**
   * @class   NodeBase
   * @brief   Internal tree node interface
   * @author  Christophe Ecabert
   * @date    14/12/2020
   * @ingroup geometry
   */
  class NodeBase {
   public:
    /** List of points */
    using PointList = std::vector<PointType<NDim, T>>;
    /** List of points with score */
    using PointWithScoreList = std::vector<PointWithScore<NDim, T>>;

    /**
     * @name    NodeBase
     * @fn  NodeBase(int ordering, const PointType<NDim, T>& key,
                const NodeBase* above, const NodeBase* below)
     * @brief Constructor
     * @param[in] ordering Node's ordering (i.e. Along which direction the
     *  points will be ordererd
     * @param[in] key Node's key
     * @param[in] above Node's above
     * @param[in] below Node's below
     */
    NodeBase(int ordering,
             const PointType<NDim, T>& key,
             const NodeBase* above,
             const NodeBase* below) : dim_(ordering),
                                      ordering_(ordering),
                                      key_(key),
                                      above_(above),
                                      below_(below) {
    }

    /**
     * @name    ~NodeBase
     * @fn  virtual ~NodeBase() = default
     * @brief Destructor
     */
    virtual ~NodeBase() = default;

    /**
     * @name    BuildRecursive
     * @fn  static NodeBase* BuildRecursive(const int& depth,
     *                                      PointList* points)
     * @brief Build KDTree recursively
     * @param[in] depth     Current node depth
     * @param[in] points    List of points under this node
     * @return  Tree node instance
     */
    static NodeBase* BuildRecursive(const int& depth, PointList* points);

    /**
     * @name    FindMinimalParent
     * @fn  static const NodeBase* FindMinimalParent(const NodeBase* node,
     *        const PointType<NDim, T>& x, const size_t& n)
     * @brief   Find parent that englob a given number of neighbours
     * @param[in] node  Starting node
     * @param[in] x     Query point
     * @param[in] n     Number of neighbour to select
     * @return Node
     */
    static const NodeBase* FindMinimalParent(const NodeBase* node,
                                             const PointType<NDim, T>& x,
                                             const size_t& n);

    /**
     * @name Size
     * @fn  virtual size_t Size() const = 0
     * @brief Compute the size of the node
     * @return  Node's size
     */
    virtual size_t Size() const = 0;

    /**
     * @name    Empty
     * @fn  virtual bool Empty() const = 0
     * @brief Check if node is empty
     * @return `true` if empty, `false` otherwise.
     */
    virtual bool Empty() const = 0;

    /**
     * @name    IsBelow
     * @fn  virtual bool IsBelow(const PointType<NDim, T>& x) const = 0
     * @brief Check if a given point `x` is below this node for a given
     *  ordering (i.e. dimensions / direction)
     * @param[in] x Point to check
     * @return  `true` if point is below, `false` otherwise
     */
    virtual bool IsBelow(const PointType<NDim, T>& x) const = 0;

    /**
     * @name    IsAbove
     * @fn  virtual bool IsAbove(const PointType<NDim, T>& x) const = 0
     * @brief Check if a given point `x` is above this node for a given
     *  ordering (i.e. dimensions / direction)
     * @param[in] x Point to check
     * @return  `true` if point is above, `false` otherwise
     */
    virtual bool IsAbove(const PointType<NDim, T>& x) const = 0;

    /**
     * @name    ToVector
     * @fn  PointList ToVector() const
     * @brief Convert every node's key under this one into a vector (i.e. list
     *  of points)
     * @return  List of points (i.e. node's key)
     */
    PointList ToVector() const;

    /**
     * @name    ToVectorWithScore
     * @fn  PointList ToVectorWithScore(const PointType<NDim, T>& x) const
     * @brief Convert every node's key under this one into a vector (i.e. list
     *  of points) with scoring metric (i.e. distance
     * @param[in] x Point to score from
     * @return  List of points (i.e. node's key)
     */
    PointWithScoreList ToVectorWithScore(const PointType<NDim, T>& x) const;

    /**
     * @name    FindNearest
     * @fn  virtual PointList FindNearest(const PointType<NDim, T>& x,
     *                                    size_t n) const = 0;
     * @brief Find N nearest points
     * @param[in] x Query point
     * @param[in] n Number of neighbour to look for.
     * @return  List of neighbouring points
     */
    virtual PointList FindNearest(const PointType<NDim, T>& x, size_t n) const = 0;

    /**
     * @name    FindNearest0
     * @param[in] x Point to look around
     * @param[in] n Number of neighbour to look for
     * @param[in] skip_parent   Node to skip
     * @param[in] values    Values to look into
     * @return  List of point with scores
     */
    virtual PointWithScoreList FindNearest0(const PointType<NDim, T>& x,
                                            size_t n,
                                            const NodeBase* skip_parent,
                                            const PointWithScoreList& values) const = 0;

    /**
     * @name    get_above
     * @brief Provide reference to node's above
     * @return  Pointer, can be nullptr
     */
    const NodeBase* get_above() const {
      return above_;
    }

    /**
     * @name    get_below
     * @brief Provide reference to node's below
     * @return  Pointer, can be nullptr
     */
    const NodeBase* get_below() const {
      return below_;
    }

    /**
     * @name    get_key
     * @fn  const PointType<NDim, T>& get_key() const
     * @brief Provide node's key
     * @return  Point
     */
    const PointType<NDim, T>& get_key() const {
      return key_;
    }

   protected:
    /** Ordering index */
    int dim_;
    /** Ordering instance */
    Ordering<NDim, T> ordering_;
    /** Node's key */
    PointType<NDim, T> key_;
    /** Node above */
    const NodeBase* above_;
    /** Node below */
    const NodeBase* below_;
  };

  /**
   * @class   Node
   * @brief   Generic tree Node implementation
   * @author  Christophe Ecabert
   * @date    14/12/2020
   * @ingroup geometry
   */
  class Node : public NodeBase {
   public:
    /** List of points */
    using PointList = std::vector<PointType<NDim, T>>;
    /** List of points with score */
    using PointWithScoreList = std::vector<PointWithScore<NDim, T>>;

    /**
     * @name    Node
     * @fn  Node(const PointType<NDim, T>& key, int dim, const NodeBase* above,
                 const NodeBase* below)
     * @brief Constructor
     * @param[in] key   Node's key (i.e. point)
     * @param[in] dim   Index of the ordering dimensions
     * @param[in] above Node's above
     * @param[in] below Node's below
     */
    Node(const PointType<NDim, T>& key,
         int dim,
         const NodeBase* above,
         const NodeBase* below);

    /**
     * @name
     * @brief Copy constructor
     * @param other
     */
    Node(const Node& other) = delete;

    /**
     * @name Size
     * @fn  virtual size_t Size() const = 0
     * @brief Compute the size of the node
     * @return  Node's size
     */
    size_t Size() const override;

    /**
     * @name    Empty
     * @fn  bool Empty() const override
     * @brief Check if node is empty
     * @return `true` if empty, `false` otherwise.
     */
    bool Empty() const override;

    /**
     * @name    IsBelow
     * @fn  bool IsBelow(const PointType<NDim, T>& x) const override
     * @brief Check if a given point `x` is below this node for a given
     *  ordering (i.e. dimensions / direction)
     * @param[in] x Point to check
     * @return  `true` if point is below, `false` otherwise
     */
    bool IsBelow(const PointType<NDim, T>& x) const override;

    /**
     * @name    IsAbove
     * @fn  bool IsAbove(const PointType<NDim, T>& x) const override
     * @brief Check if a given point `x` is above this node for a given
     *  ordering (i.e. dimensions / direction)
     * @param[in] x Point to check
     * @return  `true` if point is above, `false` otherwise
     */
    bool IsAbove(const PointType<NDim, T>& x) const override;

    /**
     * @name    FindNearest
     * @fn  virtual PointList FindNearest(const PointType<NDim, T>& x,
     *                                    int n) const = 0;
     * @brief Find N nearest points
     * @param[in] x Query point
     * @param[in] n Number of neighbour to look for.
     * @return  List of neighbouring points
     */
    PointList FindNearest(const PointType<NDim, T>& x, size_t n) const override;

    /**
     * @name    FindNearest0
     * @param[in] x Point to look around
     * @param[in] n Number of neighbour to look for
     * @param[in] skip_parent   Node to skip
     * @param[in] values    Values to look into
     * @return  List of point with scores
     */
    PointWithScoreList FindNearest0(const PointType<NDim, T>& x,
                                    size_t n,
                                    const NodeBase* skip_parent,
                                    const PointWithScoreList& values) const override;
  };

  /**
   * @class   EmptyNode
   * @brief   Terminal tree node implementation
   * @author  Christophe Ecabert
   * @date    14/12/2020
   * @ingroup geometry
   */
   class EmptyNode : public NodeBase {
    public:
     /** List of points */
     using PointList = std::vector<PointType<NDim, T>>;
     /** List of points with score */
     using PointWithScoreList = std::vector<PointWithScore<NDim, T>>;

     /**
      * @name    EmptyNode
      * @fn  explicit EmptyNode(int dim)
      * @brief Constructor
      * @param[in] dim   Index of the ordering dimensions
      */
     explicit EmptyNode(int dim);

     /**
      * @name   EmptyNode
      * @brief Copy constructor
      * @param other
      */
     EmptyNode(const EmptyNode& other) = delete;

     /**
      * @name Size
      * @fn  size_t Size() const override
      * @brief Compute the size of the node
      * @return  Node's size
      */
     size_t Size() const override;

     /**
      * @name    Empty
      * @fn  bool Empty() const override
      * @brief Check if node is empty
      * @return `true` if empty, `false` otherwise.
      */
     bool Empty() const override;

     /**
      * @name    IsBelow
      * @fn  bool IsBelow(const PointType<NDim, T>& x) const override
      * @brief Check if a given point `x` is below this node for a given
      *  ordering (i.e. dimensions / direction)
      * @param[in] x Point to check
      * @return  `true` if point is below, `false` otherwise
      */
     bool IsBelow(const PointType<NDim, T>& x) const override;

     /**
      * @name    IsAbove
      * @fn  bool IsAbove(const PointType<NDim, T>& x) const override
      * @brief Check if a given point `x` is above this node for a given
      *  ordering (i.e. dimensions / direction)
      * @param[in] x Point to check
      * @return  `true` if point is above, `false` otherwise
      */
     bool IsAbove(const PointType<NDim, T>& x) const override;

     /**
      * @name    FindNearest
      * @fn  virtual PointList FindNearest(const PointType<NDim, T>& x,
      *                                    int n) const = 0;
      * @brief Find N nearest points
      * @param[in] x Query point
      * @param[in] n Number of neighbour to look for.
      * @return  List of neighbouring points
      */
     PointList FindNearest(const PointType<NDim, T>& x, size_t n) const override;

     /**
      * @name    FindNearest0
      * @param[in] x Point to look around
      * @param[in] n Number of neighbour to look for
      * @param[in] skip_parent   Node to skip
      * @param[in] values    Values to look into
      * @return  List of point with scores
      */
     PointWithScoreList FindNearest0(const PointType<NDim, T>& x,
                                     size_t n,
                                     const NodeBase* skip_parent,
                                     const PointWithScoreList& values) const override;
   };

   /** Root node */
   NodeBase* root_;
};

#pragma mark -
#pragma mark Implementation - KDTree

template<int NDim, typename T>
KDTree<NDim, T>::KDTree() : root_(nullptr) {}

template<int NDim, typename T>
KDTree<NDim, T>::
KDTree(const std::vector<PointType<NDim, T>> &points) : KDTree() {
  this->Build(points);
}

template<int NDim, typename T>
KDTree<NDim, T>::~KDTree() {
  this->Clear();
}

template<int NDim, typename T>
int KDTree<NDim, T>::
Build(const std::vector<PointType<NDim, T>> &points) {
  // Already build ?
  if (root_ != nullptr) {
    this->Clear();
  }
  // Copy point to keep order correct
  auto pts = std::vector<PointType<NDim, T>>(points);
  this->root_ = NodeBase::BuildRecursive(0, &pts);
  // Sanity check
  return this->root_->Size() == points.size() ? 0 : -1;
}

template<int NDim, typename T>
void KDTree<NDim, T>::Clear() {
  if (this->root_ != nullptr) {
    std::stack<const NodeBase*> queue;
    queue.push(this->root_);
    // Delete every nodes
    while (!queue.empty()) {
      auto* node = queue.top();
      queue.pop();

      // Child present ? If yes add them to the deleting queue.
      auto* above = node->get_above();
      if (above != nullptr) {
        queue.push(above);
      }
      auto* below = node->get_below();
      if (below != nullptr) {
        queue.push(below);
      }
      // Can safely delete current node
      delete node;
    }
    // All nodes have been cleared
    this->root_ = nullptr;
  }
}

template<int NDim, typename T>
bool KDTree<NDim, T>::Empty() const {
  if (this->root_) {
    return this->root_->Empty();
  } else {
    // No root node -> empty
    return true;
  }
}

template<int NDim, typename T>
size_t KDTree<NDim, T>::Size() const {
  if (this->root_) {
    return this->root_->Size();
  } else {
    // No root node -> empty
    return 0;
  }
}

template<int NDim, typename T>
typename KDTree<NDim, T>::PointList
KDTree<NDim, T>::FindNearest(const PointType<NDim, T>& x, const int& n) const {
  if (this->root_) {
    return this->root_->FindNearest(x, n);
  } else {
    // Not build, empty list
    return PointList();
  }
}

#pragma mark -
#pragma mark Implementation - NodeBase

template<int NDim, typename T>
typename KDTree<NDim, T>::NodeBase*
KDTree<NDim, T>::NodeBase::BuildRecursive(const int& depth,
                                          PointList* points) {
  // Find spliting direction
  int i = depth % NDim; // Change direction at each depth
  // Create node
  if (points->empty()) {
    // No more points to add in tree
    return new EmptyNode(i);
  } else {
    // Continue recursion
    // Find split
    size_t mid = points->size() / 2;
    Ordering<NDim, T> ordering(i);
    std::nth_element(points->begin(),
                     points->begin() + mid,
                     points->end(),
                     [&](const PointType<NDim, T>& x,
                         const PointType<NDim, T>& y) {
                       return ordering.lt(x, y);
                     });
    // Points above
    auto above = PointList(points->begin() + mid + 1, points->end());
    auto below = PointList(points->begin(), points->begin() + mid);
    return new Node(points->at(mid),
                    i,
                    KDTree<NDim, T>::NodeBase::BuildRecursive(depth + 1,
                                                              &above),
                    KDTree<NDim, T>::NodeBase::BuildRecursive(depth + 1,
                                                              &below));
  }
}

template<int NDim, typename T>
const typename KDTree<NDim, T>::NodeBase*
KDTree<NDim, T>::NodeBase::FindMinimalParent(const NodeBase* node,
                                             const PointType<NDim, T>& x,
                                             const size_t &n) {
  // WITHOUT RECURSION, SHOULD WORK AS WELL!
  std::stack<const NodeBase*> queue;
  queue.push(node);
  while (!queue.empty()) {
    const auto* node = queue.top();
    queue.pop();
    if (node->get_key() == x) {
      return node;
    } else {
      const NodeBase* next = nullptr;
      if (node->IsBelow(x)) {
        next = node->get_below();
      } else {
        next = node->get_above();
      }
      if (next->Size() < n) {
        return node;
      } else {
        // Go to next node
        queue.push(next);
      }
    }
  }
  // Should not reach here, return `nullptr` to make compiler happy, otherwise
  // get: "warning: control may reach end of non-void function".
  return nullptr;
}

template<int NDim, typename T>
typename KDTree<NDim, T>::NodeBase::PointList
KDTree<NDim, T>::NodeBase::ToVector() const {
  std::vector<PointType<NDim, T>> values;

  // Iterate over nodes
  std::stack<const NodeBase*> queue;
  queue.push(this);
  while (!queue.empty()) {
    const auto* node = queue.top();
    queue.pop();

    // Add node's key to the output values
    values.push_back(node->get_key());

    // Push node's child if any
    const auto* next = node->get_above();
    if (next != nullptr && !next->Empty()) {
      queue.push(next);
    }
    next = node->get_below();
    if (next != nullptr && !next->Empty()) {
      queue.push(next);
    }
  }
  return values;
}

template<int NDim, typename T>
typename KDTree<NDim, T>::NodeBase::PointWithScoreList
KDTree<NDim, T>::NodeBase::
ToVectorWithScore(const PointType<NDim, T>& x) const {
  std::vector<PointWithScore<NDim, T>> values;

  // Iterate over nodes
  std::stack<const NodeBase*> queue;
  queue.push(this);
  while (!queue.empty()) {
    const auto* node = queue.top();
    queue.pop();
    // Add node's key to the output values
    const auto& key = node->get_key();
    values.emplace_back(key, Metric<NDim, T>::Distance(x, key));
    // Push node's child if any, don't push terminal node, i.e. `EmptyNode`
    const auto* next = node->get_above();
    if (next != nullptr && !next->Empty()) {
      queue.push(next);
    }
    next = node->get_below();
    if (next != nullptr && !next->Empty()) {
      queue.push(next);
    }
  }
  return values;
}

#pragma mark -
#pragma mark Implementation - Node

template<int NDim, typename T>
KDTree<NDim, T>::Node::Node(const PointType<NDim, T> &key,
                            int dim,
                            const NodeBase* above,
                            const NodeBase* below) : NodeBase(dim,
                                                              key,
                                                              above,
                                                              below) {
}

template<int NDim, typename T>
size_t KDTree<NDim, T>::Node::Size() const {
  // above/below are EmtpyNode when tree reached leafs!
  return this->above_->Size() + this->below_->Size() + 1;
}

template<int NDim, typename T>
bool KDTree<NDim, T>::Node::Empty() const {
  // Node are never empty, only `EmptyNode` return true.
  return false;
}

template<int NDim, typename T>
bool KDTree<NDim, T>::Node::IsBelow(const PointType<NDim, T> &x) const {
  // Check if x is lower than node's key
  return this->ordering_.lt(x, this->key_);
}

template<int NDim, typename T>
bool KDTree<NDim, T>::Node::IsAbove(const PointType<NDim, T> &x) const {
  // Check if x is greater than node's key
  return this->ordering_.gt(x, this->key_);
}

template<int NDim, typename T>
typename KDTree<NDim, T>::Node::PointList
KDTree<NDim, T>::Node::
FindNearest(const PointType<NDim, T>& x, size_t n) const {
  // Build initial set of candidates from the smallest subtree containing x
  // with at least n points.
  const auto min_parent = NodeBase::FindMinimalParent(this, x, n);
  // Convert nodes to list of points with distance to the query points
  auto points = min_parent->ToVectorWithScore(x);
  // sort by increasing score (i.e. distance)
  std::sort(points.begin(),
            points.end(),
            [](const PointWithScore<NDim, T>& x,
               const PointWithScore<NDim, T>& y) {
              return x.score < y.score;
            });
  // Select `n` first element (copy)
  points = std::vector<PointWithScore<NDim, T>>(points.begin(),
          points.begin() + n);

  points = this->FindNearest0(x, n, min_parent, points);

  // Remove scoring
  PointList selected;
  for (const auto& pts : points) {
    selected.push_back(pts.pts);
  }
  return selected;
}

template<int NDim, typename T>
typename KDTree<NDim, T>::Node::PointWithScoreList
KDTree<NDim, T>::Node::FindNearest0(const PointType<NDim, T>& x,
                                    size_t n,
                                    const NodeBase* skip_parent,
                                    const PointWithScoreList& values) const {
  if (skip_parent == this) {
    return values;
  } else {
    T my_dist = Metric<NDim, T>::Distance(this->key_, x);
    const T current_best_score = values.back().score;

    PointWithScoreList new_values = values;
    if (my_dist < current_best_score) {
      // Append (x, my_dist) to 'values'
      new_values.emplace_back(this->key_, my_dist);
      // Sort...
      std::sort(new_values.begin(),
                new_values.end(),
                [](const PointWithScore<NDim, T>& x,
                   const PointWithScore<NDim, T>& y) {
                  return x.score < y.score;
                });
      // and take 'n' entries
      assert(new_values.size() == n + 1);
      new_values.resize(n);  // Works since new_values is always n+1
    }
    // Pick new best score
    const T new_current_best_score = new_values.back().score;
    // Get distance
    T dp = Metric<NDim, T>::PlanarDistance(x, this->key_, this->dim_);
    if (dp <= new_current_best_score) {
      auto values2 = this->above_->FindNearest0(x, n, skip_parent, new_values);
      return this->below_->FindNearest0(x, n, skip_parent, values2);
    } else if (this->IsAbove(x)) {
      return this->above_->FindNearest0(x, n, skip_parent, new_values);
    } else if (this->IsBelow(x)) {
      return this->below_->FindNearest0(x, n, skip_parent, new_values);
    } else {
      LTS5_LOG_ERROR("Unexpected value!");
    }
  }
  return PointWithScoreList();
}

#pragma mark -
#pragma mark Implementation - EmptyNode

template<int NDim, typename T>
KDTree<NDim, T>::EmptyNode::EmptyNode(int dim) : NodeBase(dim,
                                                          PointType<NDim, T>(),
                                                          nullptr,
                                                          nullptr) {
}

template<int NDim, typename T>
size_t KDTree<NDim, T>::EmptyNode::Size() const {
  // Terminal node, size == 0
  return 0;
}

template<int NDim, typename T>
bool KDTree<NDim, T>::EmptyNode::Empty() const {
  // Terminal node, empty
  return true;
}

template<int NDim, typename T>
bool KDTree<NDim, T>::EmptyNode::IsBelow(const PointType<NDim, T> &x) const {
  return false;
}

template<int NDim, typename T>
bool KDTree<NDim, T>::EmptyNode::IsAbove(const PointType<NDim, T> &x) const {
  return false;
}

template<int NDim, typename T>
typename KDTree<NDim, T>::EmptyNode::PointList
KDTree<NDim, T>::EmptyNode::
FindNearest(const PointType<NDim, T>& x, size_t n) const {
  // Terminal node, return empty list
  return PointList();
}

template<int NDim, typename T>
typename KDTree<NDim, T>::EmptyNode::PointWithScoreList
KDTree<NDim, T>::EmptyNode::
FindNearest0(const PointType<NDim, T>& x,
             size_t n,
             const NodeBase* skip_parent,
             const PointWithScoreList& values) const {
  // Terminal node -> return input
  return values;
}






}  // namespace LTS5

#endif  // __LTS5_KDTREE__
