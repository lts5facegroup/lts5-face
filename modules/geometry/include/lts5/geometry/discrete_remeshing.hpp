/**
 *  @file   discrete_remeshing.hpp
 *  @brief  Uniform discrete mesh remeshing
 *          Based on : https://github.com/valette/ACVD
 *
 *          Approximated Centroidal Voronoi Diagrams for Uniform Polygonal Mesh
 *          S. Vallette et al.
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   11/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __DISCRETE_REMESHING__
#define __DISCRETE_REMESHING__

#include <queue>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/bit_array.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/isotropic_clustering_metric.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   IsotropicDiscreteRemesher
 * @brief   Uniform mesh resampling
 *          Based on : https://github.com/valette/ACVD
 *
 *          Approximated Centroidal Voronoi Diagrams for Uniform Polygonal Mesh
 *          S. Vallette et al.
 * @author  Christophe Ecabert
 * @date    11/11/2016
 * @ingroup geometry
 */
template<typename T>
class LTS5_EXPORTS IsotropicDiscreteRemesher {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  IsotropicDiscreteRemesher
   * @fn    IsotropicDiscreteRemesher(void)
   * @brief Constructor
   */
  IsotropicDiscreteRemesher(void);

  /**
   * @name  IsotropicDiscreteRemesher
   * @fn IsotropicDiscreteRemesher(const IsotropicDiscreteRemesher& other) = delete
   * @brief Copy constructor
   * @param[in] other
   */
  IsotropicDiscreteRemesher(const IsotropicDiscreteRemesher& other) = delete;

  /**
   * @name  operator=
   * @fn IsotropicDiscreteRemesher& operator=(const IsotropicDiscreteRemesher& rhs) = delete
   * @brief Assignment operator
   * @param rhs Object to assign from
   * @return Newly assigned operator
   */
  IsotropicDiscreteRemesher& operator=(const IsotropicDiscreteRemesher& rhs) = delete;

  /**
   * @name  ~IsotropicDiscreteRemesher
   * @fn    ~IsotropicDiscreteRemesher(void)
   * @brief Destructor
   */
  ~IsotropicDiscreteRemesher(void);

  /**
   * @name  Initialize
   * @fn    void Initialize(const Mesh<T>* mesh,
                            const int number_cluster)
   * @brief Initialize remesher
   * @param[in] mesh            Mesh to resample
   * @param[in] number_cluster  Number of cluster wanted (i.e. number of vertex)
   * @return -1 if error, 0 otherwise
   */
  int Initialize(const Mesh<T>* mesh,
                 const int number_cluster);

#pragma mark -
#pragma mark Process

  /**
   * @name  Process
   * @fn    void Process(Mesh<T>* mesh)
   * @brief Remesh
   * @param[out] mesh    Resampled mesh
   */
  void Process(Mesh<T>* mesh);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  set_log
   * @fn    void set_log(const bool log)
   * @brief Enable/disable logging
   * @param[in] log Logging state
   */
  void set_log(const bool log) {
    log_ = log;
  }

#pragma mark -
#pragma mark Private

 private:

  /**
   * @name  InitProcess
   * @fn    void InitProcess(void)
   * @brief Reinitialiaze module in ordre to process a new meshs
   */
  void InitProcess(void);

  /**
   * @name  InitEdgeList
   * @fn    int InitEdgeList(const Mesh<T>* mesh)
   * @brief Setup a list of edge, and list of face adjacent to an edge
   * @param[in] mesh Mesh from where to build edge list
   */
  void InitEdgeList(const Mesh<T>* mesh);

  /**
   * @name  InitCluster
   * @fn    void InitCluster(void)
   * @brief Initialize clustering with random selection
   */
  void InitCluster(void);

  /**
   * @name  MinimizeEnergy
   * @fn    void MinimizeEnergy(void)
   * @brief Do the actual clustering
   */
  void MinimizeEnergy(void);

  /**
   * @name  FillHolesInClustering
   * @fn    void FillHolesInClustering(void)
   * @brief Expand clusters
   */
  void FillHolesInClustering(void);

  /**
   * @name  FillQueuesFromClustering
   * @fn    void FillQueuesFromClustering(void)
   * @brief Fill up edge queue with edges belgong to two different clusters
   *        (generally null cluster, i.e. unassigned cluster)
   */
  void FillQueuesFromClustering(void);

  /**
   * @name  RecomputeStatistics
   * @fn    void RecomputeStatistics(void)
   * @brief Add vertex to clusters and compute cluster's centroid + energy
   */
  void RecomputeStatistics(void);

  /**
   * @name  SetAllClustersToModified
   * @fn    void SetAllClustersToModified(void)
   * @brief Update when cluster have beed changed for the last time
   *        (i.e. with current iteration loop number).
   */
  void SetAllClustersToModified(void);

  /**
   * @name  ProcessOneLoop
   * @fn    int ProcessOneLoop(void)
   * @brief Minimize clustering enery for one iteration
   * @return Number of item changed
   */
  int ProcessOneLoop(void);

  /**
   * @name  AddItemRingToProcess
   * @fn    void AddItemRingToProcess(const int idx)
   * @brief
   * @param[in] idx
   */
  void AddItemRingToProcess(const int idx);

  /**
   * @name  CleanClustering
   * @fn    int CleanClustering(void)
   * @brief Detect clusters that have several connected components
   */
  int CleanClustering(void);

  /**
   * @name  RecomputeClusterSize
   * @fn    void RecomputeClusterSize(void)
   * @brief Update cluster size counter
   */
  void RecomputeClusterSize(void);

  /**
   * @name  Triangulate
   * @fn    void Triangulate(LTS5::Mesh<T>* mesh)
   * @brief Define mesh from clustering
   * @param[out] mesh   Resampled mesh
   */
  void Triangulate(LTS5::Mesh<T>* mesh);

  /**
   * @name  FixBoundaries
   * @fn    void FixBoundaries(LTS5::Mesh<T>* mesh)
   * @brief Fix mesh boundaries
   * @param[in,out] mesh    Resampled mesh
   */
  void FixBoundaries(LTS5::Mesh<T>* mesh);

  /**
   * @name  GetNumberOfBoundary
   * @fn    void GetNumberOfBoundary(const int vertex_idx, int* boundary)
   * @brief Compute how many edges from a specific vertex are on the mesh
   *        boundary
   * @param[in] vertex_idx
   * @param[out] boundary
   */
  void GetNumberOfBoundary(const int vertex_idx, int* boundary);


  /** Mesh to resample */
  const Mesh<T>* mesh_;
  /** Number of desired cluster */
  int n_cluster_;
  /** Isotropic clustering metric */
  IsotropicClusteringMetric* metric_;
  /** Number of each in each cluster */
  std::vector<int> cluster_size_;
  /** Clustering, define in which cluster belongs a given vertex*/
  std::vector<int> clustering_;
  /** Indicate the last time a cluster was modified */
  std::vector<int> cluster_last_modification_;
  /** Indicate the last time an edge was visited */
  std::vector<uchar> edge_last_loop_;
  /** Queue containing the edges between two clusters */
  std::queue<size_t> edge_queue_;
  /** Edges, list of adjacent vertex */
  std::vector<std::pair<size_t, size_t>> edge_;
  /** List of adjacent face for a given edge */
  std::vector<std::pair<size_t, size_t>> edge_face_;
  /** List of eges connected to a given vertex */
  std::vector<std::vector<int>> vertex_edge_list_;
  /** Indicate if a cluster if freezed */
  BitArray cluster_is_freezed_;
  /** the number of times the edges queue has been processed */
  int n_loop_;
  /** value to keep a relative notion of NumberOfloops */
  unsigned char relative_n_loop_;
  /** Number maximum of loop */
  int n_max_loop_;
  /** Indicate number of convergence */
  int n_convergence_;
  /** Maximum number of convergence */
  int n_max_convergence_;
  /** Concole output */
  bool log_;
};

}  // namepsace LTS5

#endif //__DISCRETE_REMESHING__
