/**
 *  @file   aabb_mpu_tree.hpp
 *  @brief  AABB tree for MPU speed-up class declaration
 *  @ingroup geometry
 *
 *  @author Gabriel Cuendet
 *  @date   07/06/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef __LTS5_AABB_MPU_TREE__
#define __LTS5_AABB_MPU_TREE__

#include <stdio.h>
#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mpu_node.hpp"
#include "lts5/geometry/aabb_tree.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  AABBMPUTree
 *  @brief  ABB class for MPU speed-up
 *  @author Gabriel Cuendet
 *  @date   07/06/2016
 *  @ingroup geometry
 */
template<typename T>
class LTS5_EXPORTS AABBMPUTree : public AABBTree<T> {
 public :

#pragma mark -
#pragma mark Initialization

  /**
   * @name  AABBMPUTree
   * @fn  AABBMPUTree(void)
   * @brief Constructor
   */
  AABBMPUTree(void);

  /**
   * @name  AABBMPUTree
   * @fn  AABBMPUTree(const std::vector<MPUNode*>& nodes)
   * @brief Constructor taking a vector of nodes and calling Insert directly
   * @param[in] nodes  Collection of MPUNodes used to build the tree
   */
  AABBMPUTree(const std::vector<MPUNode<T>*>& nodes);

  /**
   * @name  ~AABBMPUTree
   * @fn  ~AABBMPUTree(void)
   * @brief Destructor
   */
  ~AABBMPUTree(void) {}

  /**
   * @name  Insert
   * @fn  void Insert(const Mesh<T>& mesh)
   * @brief Insert a range of element into the tree
   * @param[in] nodes  Collection of MPUNode to use to build the tree
   */
  void Insert(const std::vector<MPUNode<T>*>& nodes);

#pragma mark -
#pragma mark Usage

  /**
   * @name  DoIntersect
   * @fn  bool DoIntersect(const Vector3<T>& p, const Vector3<T>& dir,
   *                       std::vector<int>* idx)
   * @brief Check if a given point intersects with one of the bounding box in
   *        the tree and provide its corresponding index
   *  @param[in]  p   Coordinates of the query point
   *  @param[out] idx Indices of the corresponding intersections
   *  @return True if intersect, false otherwise
   */
  bool DoIntersect(const Vector3<T>& p,
                   std::vector<int>* idx) const;

};

}  // namespace LTS5
#endif /* __LTS5_AABB_MPU_TREE__ */
