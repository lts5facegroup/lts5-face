/**
 *  @file   aabc.hpp
 *  @brief  Axis aligned bounding cube declaration and implementation
 *  @ingroup geometry
 *
 *  @author Gabriel Cuendet
 *  @date   26/05/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef __LTS5_AABC__
#define __LTS5_AABC__

#include "lts5/geometry/aabb.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @struct   AABC
 * @brief   Axis Aligned Bounding Cube
 * @author  Gabriel Cuendet
 * @date    26/05/2016
 * @ingroup geometry
 */
template<typename T>
struct LTS5_EXPORTS AABC : AABB<T> {

#pragma mark -
#pragma mark Initialization

  /**
   * @name  AABC
   * @fn  AABC(const AABC<T>& other)
   * @brief Constructor
   */
  AABC(const AABC<T>& other) : AABB<T>::AABB(other) {
    this->half_edge_ = other.half_edge_;
  }

  /**
   * @name  operator=
   * @fn  AABC& operator=(const AABC<T>& rhs)
   * @brief Assignment operator
   * @param[in] rhs AABB to assign from
   * @return  New assigned object
   */
  AABC& operator=(const AABC<T>& rhs) {
    if (this != &rhs) {
      this->min_ = rhs.min_;
      this->max_ = rhs.max_;
      this->index_ = rhs.index_;
      this->center_ = rhs.center_;
      this->half_edge_ = rhs.half_edge_;
    }
    return *this;
  }

  /**
   * @name  AABC
   * @fn  AABC(const T xmin,const T xmax,const T ymin,const T ymax,
   const T zmin,const T zmax)
   * @brief Constructor
   * @param[in] xmin  Minimum X value
   * @param[in] xmax  Maximum X value
   * @param[in] ymin  Minimum Y value
   * @param[in] ymax  Maximum Y value
   * @param[in] zmin  Minimum Z value
   * @param[in] zmax  Maximum Z value
   */
  AABC(const T xmin, const T xmax,
       const T ymin, const T ymax,
       const T zmin, const T zmax) : AABB<T>() {
    half_edge_ = std::max({xmax-xmin, ymax-ymin, zmax-zmin}) / T(2.0);
    LTS5::Vector3<T> half_diag(half_edge_, half_edge_, half_edge_);

    this->center_.x_ = 0.5 * (xmax + xmin);
    this->center_.y_ = 0.5 * (ymax + ymin);
    this->center_.z_ = 0.5 * (zmax + zmin);

    this->min_ = this->center_ - half_diag;
    this->max_ = this->center_ + half_diag;
    this->index_ = -1;
  }

  /**
   * @name  AABC
   * @fn  AABC(const T xmin,const T xmax,const T ymin,const T ymax,
   const T zmin,const T zmax, const int index)
   * @brief Constructor
   * @param[in] xmin  Minimum X value
   * @param[in] xmax  Maximum X value
   * @param[in] ymin  Minimum Y value
   * @param[in] ymax  Maximum Y value
   * @param[in] zmin  Minimum Z value
   * @param[in] zmax  Maximum Z value
   * @param[in] index Index of the bounding box
   */
  AABC(const T xmin, const T xmax,
       const T ymin, const T ymax,
       const T zmin, const T zmax,
       const int index) : AABB<T>() {
    half_edge_ = std::max({xmax-xmin, ymax-ymin, zmax-zmin}) / T(2.0);
    LTS5::Vector3<T> half_diag(half_edge_, half_edge_, half_edge_);

    this->center_.x_ = 0.5 * (xmax + xmin);
    this->center_.y_ = 0.5 * (ymax + ymin);
    this->center_.z_ = 0.5 * (zmax + zmin);

    this->min_ = this->center_ - half_diag;
    this->max_ = this->center_ + half_diag;
    this->index_ = index;
  }

  /**
   * @name  AABC
   * @fn  AABC(const AABB<T>& other)
   * @brief Constructor of AABC from AABB (convert AABB into a cube)
   * @param[in]  other the AABB to convert
   */
  AABC(const AABB<T>& other) : AABB<T>() {
    this->half_edge_ = std::max({other.max_.x_ - other.min_.x_,
                           other.max_.y_ - other.min_.y_,
                           other.max_.z_ - other.min_.z_}) * T(0.5);
    LTS5::Vector3<T> half_diag(this->half_edge_, this->half_edge_, this->half_edge_);

    // Don't move the center
    this->center_ = other.center_;

    this->min_ = this->center_ - half_diag;
    this->max_ = this->center_ + half_diag;
    this->index_ = other.index_;
  }

  /**
   * @name  ~AABC
   * @fn  ~AABC(void)
   * @brief Destructor
   */
  ~AABC(void) {}

#pragma mark -
#pragma mark Operator

  /**
   * @name  operator+
   * @fn  AABC<T> operator+(const AABC<T>& rhs)
   * @brief Union operator
   * @param[in] rhs Right hand sign
   * @return  Union of this with rhs
   */
  AABC<T> operator+(const AABC<T>& rhs) {
    return AABC(std::min(this->min_.x_, rhs.min_.x_),
                std::max(this->max_.x_, rhs.max_.x_),
                std::min(this->min_.y_, rhs.min_.y_),
                std::max(this->max_.y_, rhs.max_.y_),
                std::min(this->min_.z_, rhs.min_.z_),
                std::max(this->max_.z_, rhs.max_.z_), -1);
  }

  /**
   * @name  operator+=
   * @fn  AABC<T>& operator+=(const AABC<T>& rhs)
   * @brief Union operator
   * @param[in] rhs Right hand sign
   * @return  Union of this
   */
  AABC<T>& operator+=(const AABC<T>& rhs) {
    // Update bbox
    this->min_.x_ = std::min(this->min_.x_, rhs.min_.x_);
    this->max_.x_ = std::max(this->max_.x_, rhs.max_.x_);
    this->min_.y_ = std::min(this->min_.y_, rhs.min_.y_);
    this->max_.y_ = std::max(this->max_.y_, rhs.max_.y_);
    this->min_.z_ = std::min(this->min_.z_, rhs.min_.z_);
    this->max_.z_ = std::max(this->max_.z_, rhs.max_.z_);
    // Update center
    this->center_ = (this->min_ + this->max_) * T(0.5);
    return *this;
  }

#pragma mark -
#pragma mark Members

  /**< Half-edge of the cube */
  T half_edge_;
};
}  // namespace LTS5
#endif /* __LTS_AABC__ */
