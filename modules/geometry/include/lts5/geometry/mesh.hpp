/**
 *  @file   mesh.hpp
 *  @brief  3D Mesh helper interface
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   15/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_MESH__
#define __LTS5_MESH__

#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <limits>

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/aabb.hpp"
#include "lts5/geometry/aabc.hpp"
#include "lts5/utils/math/vector.hpp"

namespace Eigen {
template<typename Scalar, int Flags, typename StorageIndex>
class SparseMatrix;
}

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   Material
 * @brief   Object's material definition based on "Material Template Library,
 *          MTL"
 * @author  Christophe Ecabert
 * @date    17/12/2019
 * @tparam  T Data type
 * @ingroup geometry
 */
template<typename T>
class LTS5_EXPORTS Material {
 public:
#pragma mark -
#pragma mark Type Definition

  /** Vector 3 */
  using Vec3 = Vector3<T>;
  /**
   * @enum IlluminationType
   * @brief Possible type of illumination
   */
  enum IlluminationType {
    kColorOnAndAmbientOff = 0,
    kColorOnAndAmbientOn = 1,
    kHighlightOn = 2,
    kReflectionOnAndRayTraceOn = 3,
    kTransparency_GlassOn_Reflection_RayTraceOn = 4,
    kReflection_FresnelOnAndRayTraceOn = 5,
    kTransparency_RefractionOn_Reflection_FresnelOffAndRayTraceOn = 6,
    kTransparency_RefractionOn_Reflection_FresnelOnAndRayTraceOn = 7,
    kReflectionOnAndRayTraceOff = 8,
    kTransparency_GlassOn_Reflection_RayTraceOff = 9,
    kCastsShadowsOntoInvisibleSurfaces = 10
  };

  /**
   * @name Material
   * @fn   Material()
   * @brief Constructor
   */
  Material();

  /**
   * @name Material
   * @fn   Material(const Material& other) = default
   * @brief Copy constructor
   * @param[in] other  Object to copy from
   */
  Material(const Material& other) = default;

  /**
   * @name operator=
   * @fn   Material& operator=(const Material& rhs) = default
   * @brief Assignment operator
   * @param[in] rhs    Object to assign from
   * @return   Newly assigned object
   */
  Material& operator=(const Material& rhs) = default;

  /**
   * @name ~Material
   * @fn   ~Material() = default
   * @brief Destructor
   */
  ~Material() = default;

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream)
   * @brief Parse material from a given stream
   * @param[in] stream Stream to parse
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream);

  /**
   * @name  CreateFromFile
   * @fn    static std::vector<Material> CreateFromFile(const std::string& path)
   * @brief Parse all material from a given file
   * @param[in] path Path to the material file (i.e. *.mtl file)
   * @return    List of material or empty if something went wrong
   */
  static std::vector<Material> CreateFromFile(const std::string& path);

  /**
   * @name  SetAbsolutePath
   * @fn    void SetAbsolutePath(const std::string& folder)
   * @brief Prepend a given folder path to each texture filename to convert them
   *        into absolute path.
   * @param[in] folder Folder where material file is stored
   */
  void SetAbsolutePath(const std::string& folder);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  set_name
   * @brief Set material's name
   * @param[in] name Name
   */
  void set_name(const std::string& name) {
    name_ = name;
  }

  /**
   * @name  set_ambient
   * @brief Define new ambient color
   * @param[in] ambient New normalized ambient color
   */
  void set_ambient(const Vec3& ambient) {
    this->ka_ = ambient;
  }

  /**
   * @name  set_diffuse
   * @brief Define new diffuse color
   * @param[in] diffuse New normalized diffuse color
   */
  void set_diffuse(const Vec3& diffuse) {
    this->kd_ = diffuse;
  }

  /**
   * @name  set_specular
   * @brief Define new specular color
   * @param[in] specular New normalized specular color
   */
  void set_specular(const Vec3& specular) {
    this->ks_ = specular;
  }

  /**
   * @name  set_specular_exponent
   * @brief Define new specular exponent
   * @param[in] exponent New specular exponent
   */
  void set_specular_exponent(const T& exponent) {
    this->ns_ = exponent;
  }

  /**
   * @name  set_transmittance
   * @brief Define new transmittance
   * @param[in] transmittance Transmittance
   */
  void set_transmittance(const Vec3& transmittance) {
    this->transmittance_ = transmittance;
  }

  /**
   * @name  set_emission
   * @brief Define new emission
   * @param[in] emission Emission
   */
  void set_emission(const Vec3& emission) {
    this->emission_ = emission;
  }

  /**
   * @name  set_index_of_refraction
   * @brief Define new index of refraction
   * @param[in] index_of_refraction   Index of refraction
   */
  void set_index_of_refraction(const T& index_of_refraction) {
    this->index_of_refraction_ = index_of_refraction;
  }

  /**
   * @name  set_dissolve
   * @brief Define new dissolve value
   * @param[in] dissolve   Dissolve
   */
  void set_dissolve(const T& dissolve) {
    this->dissolve_ = dissolve;
  }

  /**
   * @name  set_illumination
   * @brief Define new illumination type
   * @param[in] illumination Illumination
   */
  void set_illumination(const IlluminationType& illumination) {
    this->illumination_ = illumination;
  }

  /**
   * @name  set_ambient_texture
   * @brief Define new ambient texture path
   * @param[in] path Path
   */
  void set_ambient_texture(const std::string& path) {
    this->ambient_tex_ = path;
  }

  /**
   * @name  set_diffuse_texture
   * @brief Define new diffuse texture path
   * @param[in] path Path
   */
  void set_diffuse_texture(const std::string& path) {
    this->diffuse_tex_ = path;
  }

  /**
   * @name  set_specular_texture
   * @brief Define new specular texture path
   * @param[in] path Path
   */
  void set_specular_texture(const std::string& path) {
    this->specular_tex_ = path;
  }

  /**
   * @name  set_specular_exponent_texture
   * @brief Define new specular exponent texture path
   * @param[in] path Path
   */
  void set_specular_exponent_texture(const std::string& path) {
    this->specular_exponent_tex_ = path;
  }

  /**
   * @name  set_bump_texture
   * @brief Define new bump texture path
   * @param[in] path Path
   */
  void set_bump_texture(const std::string& path) {
    this->bump_tex_ = path;
  }

  /**
   * @name  set_displacement_texture
   * @brief Define new displacement texture path
   * @param[in] path Path
   */
  void set_displacement_texture(const std::string& path) {
    this->disp_tex_ = path;
  }

  /**
   * @name  set_alpha_texture
   * @brief Define new alpha texture path
   * @param[in] path Path
   */
  void set_alpha_texture(const std::string& path) {
    this->alpha_tex_ = path;
  }

  /**
   * @name  set_reflection_texture
   * @brief Define new reflection texture path
   * @param[in] path Path
   */
  void set_reflection_texture(const std::string& path) {
    this->reflection_tex_ = path;
  }

  /**
   * @name  get_name
   * @brief Access material's name
   * @return    Name
   */
  const std::string& get_name() const {
    return this->name_;
  }

  /**
   * @name  get_ambient
   * @brief Access ambient color
   * @return Ambient color
   */
  const Vec3& get_ambient() const {
    return this->ka_;
  }

  /**
   * @name  get_diffuse
   * @brief Access diffuse color
   * @return Diffuse color
   */
  const Vec3& get_diffuse() const {
    return this->kd_;
  }

  /**
   * @name  get_specular
   * @brief Access specular color
   * @return Specular color
   */
  const Vec3& get_specular() const {
    return this->ks_;
  }

  /**
   * @name  get_specular_exponent
   * @brief Access specular exponent
   * @return Specular exponent
   */
  const T& get_specular_exponent() const {
    return this->ns_;
  }

  /**
   * @name  get_transmittance
   * @brief Access transmittance
   * @return Transmittance
   */
  const Vec3& get_transmittance() const {
    return this->transmittance_;
  }

  /**
   * @name  get_emission
   * @brief Access emission
   * @return Emission
   */
  const Vec3& get_emission() const {
    return this->emission_;
  }

  /**
   * @name  get_index_of_refraction
   * @brief Access index of refraction
   * @return Index of refraction
   */
  const T& get_index_of_refraction() const {
    return this->index_of_refraction_;
  }

  /**
   * @name  get_dissolve
   * @brief Access dissolve
   * @return Dissolve
   */
  const T& get_dissolve() const {
    return this->dissolve_;
  }

  /**
   * @name  get_illumination
   * @brief Access illumination
   * @return Illumination
   */
  const IlluminationType& get_illumination() const {
    return this->illumination_;
  }

  /**
   * @name  get_ambient_texture
   * @brief Access ambient texture path
   * @return Path
   */
  const std::string& get_ambient_texture() const {
    return this->ambient_tex_;
  }

  /**
   * @name  get_diffuse_texture
   * @brief Access diffuse texture path
   * @return Path
   */
  const std::string& get_diffuse_texture() const {
    return this->diffuse_tex_;
  }

  /**
   * @name  get_specular_texture
   * @brief Access specular texture path
   * @return Path
   */
  const std::string& get_specular_texture() const {
    return this->specular_tex_;
  }

  /**
   * @name  get_specular_exponent_texture
   * @brief Access specular exponent texture path
   * @return Path
   */
  const std::string& get_specular_exponent_texture() const {
    return this->specular_exponent_tex_;
  }

  /**
   * @name  get_bump_texture
   * @brief Access bump texture path
   * @return Path
   */
  const std::string& get_bump_texture() const {
    return this->bump_tex_;
  }

  /**
   * @name  get_displacement_texture
   * @brief Access displacement texture path
   * @return Path
   */
  const std::string& get_displacement_texture() const {
    return this->disp_tex_;
  }

  /**
   * @name  get_alpha_texture
   * @brief Access alpha texture path
   * @return Path
   */
  const std::string& get_alpha_texture() const {
    return this->alpha_tex_;
  }

  /**
   * @name  get_reflection_texture
   * @brief Access reflection texture path
   * @return Path
   */
  const std::string& get_reflection_texture() const {
    return this->reflection_tex_;
  }

 private:
  /**
   * @name  LoadInternal
   * @fn    int LoadInternal(std::istream& stream)
   * @brief Parse a single material from a given stream
   * @param[in] stream Stream containing a valid material definition
   * @return    -1 if error, 0 otherwise
   */
  int LoadInternal(std::istream& stream);

  /** Name */
  std::string name_;
  /** Ambient color, RGB where each channel's value is between 0 and 1 */
  Vec3 ka_;
  /** Diffuse color, similar to ambient */
  Vec3 kd_;
  /** Specular color */
  Vec3 ks_;
  /** Specular exponent, range between 0, 1000 */
  T ns_;
  /** Transmittance */
  Vec3 transmittance_;
  /** Emission */
  Vec3 emission_;
  /** Index of refraction */
  T index_of_refraction_;
  /** Dissolve, 1 == opaque; 0 == fully transparent */
  T dissolve_;
  /** Illumination model */
  IlluminationType illumination_;
  /** Ambient texture, map_Ka */
  std::string ambient_tex_;
  /** Diffuse texture, map_Kd */
  std::string diffuse_tex_;
  /** Specular texture, map_Ks */
  std::string specular_tex_;
  /** Specular exponent texture, map_Ns */
  std::string specular_exponent_tex_;
  /** Bump texture, map_bump, map_Bump, bump */
  std::string bump_tex_;
  /** Displacement texture */
  std::string disp_tex_;
  /** Alpha texture, map_d */
  std::string alpha_tex_;
  /** Reflection texture, refl */
  std::string reflection_tex_;
};

/**
 * @class   Edge
 * @brief   Edge representation between two vertices. Holds information about
 *  adjacent faces as well
 * @author  Christophe Ecabert
 * @date    18/11/2020
 * @ingroup geometry
 */
class LTS5_EXPORTS Edge {
 public:
#pragma mark -
#pragma mark Initialization
  /**
   * @name  Edge
   * @fn    Edge(const int64_t& v0, const int64_t& v1, const int64_t& f0,
   *             const int64_t& f1)
   * @brief Constructor
   * @param[in] v0  First vertex
   * @param[in] v1  Second vertex
   * @param[in] f0  First face
   * @param[in] f1  Second face
   */
  Edge(const int64_t& v0, const int64_t& v1,
       const int64_t& f0, const int64_t& f1);

  /**
   * @name  operator==
   * @fn    bool operator==(const Edge& other) const
   * @brief Equal operator
   * @param[in] other   Object to test against
   * @return    true if equal, false otherwise
   */
  bool operator==(const Edge& other) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_first_vertex
   * @fn    int64_t get_first_vertex() const
   * @brief Provide index of first vertex
   * @return    First vertex index
   */
  int64_t get_first_vertex() const {
    return v0_;
  }

  /**
   * @name  get_second_vertex
   * @fn    int64_t get_second_vertex() const
   * @brief Provide index of second vertex
   * @return    Second vertex index
   */
  int64_t get_second_vertex() const {
    return v1_;
  }

  /**
   * @name  set_first_face
   * @fn    void set_first_face(const int64_t& f0)
   * @brief Set index of first face
   * @param[in] f0 Index of the first face
   */
  void set_first_face(const int64_t& f0) {
    f0_ = f0;
  }

  /**
   * @name  get_first_face
   * @fn    int64_t get_first_face() const
   * @brief Provide index of first face
   * @return    First face index
   */
  int64_t get_first_face() const {
    return f0_;
  }

  /**
   * @name  set_second_face
   * @fn    void set_second_face(const int64_t& f1)
   * @brief Set index of second face
   * @param[in] f1 Index of the first face
   */
  void set_second_face(const int64_t& f1) {
    f1_ = f1;
  }

  /**
   * @name  get_second_face
   * @fn    int64_t get_second_face() const
   * @brief Provide index of second face
   * @return    Second face index
   */
  int64_t get_second_face() const {
    return f1_;
  }

 private:
  /** First vertex */
  int64_t v0_;
  /** Second vertex */
  int64_t v1_;
  /** First face */
  int64_t f0_;
  /** Second face - can be null */
  int64_t f1_;
};

/**
 * @name    ExtractEdgeInformation
 * @fn  void ExtractEdgeInformation(const std::vector<Vector3<int>>& triangles,
                            const size_t& n_vertex,
                            std::vector<Edge>* edges,
                            std::vector<std::vector<size_t>>* neighbours)
 * @brief   Extract list of edges and one ring neighbour from a given
 *  triangulation
 * @param[in] triangles List of triangles
 * @param[in] n_vertex  Total number of vertex in the triangulation
 * @param[out] edges    List of all edges in the triangulation
 * @param[out] neighbours   List of all edges connected to each vertices
 */
void LTS5_EXPORTS ExtractEdgeInformation(
        const std::vector<Vector3<int>>& triangles,
        const size_t& n_vertex,
        std::vector<Edge>* edges,
        std::vector<std::vector<size_t>>* neighbours);

/**
 *  @class  Mesh
 *  @brief  3D Mesh interface
 *  @author Christophe Ecabert
 *  @date   15/01/16
 *  @ingroup geometry
 *  @tparam T Data type
 *  @tparam ColorContainer  Container type for vertex color (RGB/RGBA)
 */
template<typename T, typename ColorContainer = Vector4<T>>
class LTS5_EXPORTS Mesh {
 public :
  /** Data type */
  using DType = T;
  /** Vertex type */
  using Vertex = Vector3<T>;
  /** Edge type */
  using Edge = Vector3<T>;
  /** Triangle type */
  using Triangle = Vector3<int>;
  /** Normal type */
  using Normal = Vector3<T>;
  /** Texture coordinate */
  using TCoord = Vector2<T>;
  /** Vertex color */
  using Color = ColorContainer;
  /** Index of surrounding neighbourd */
  using NeighbourIdx = std::vector<int>;
  /** Vpe - Index of vertices connected to an edge */
  using Vpe = Vector2<int>;
  /** Fpe - Index of faces connected to an edge */
  using Fpe = Vector2<int>;
  /** Curvature */
  using Curv = Vector2<T>;
  /** Materials */
  using Mat = Material<T>;

#pragma mark -
#pragma mark Type definition

  /** Halfedge type */
  struct Halfedge {
    /** Vertex reference: origin of the halfedge */
    std::size_t origin;
    /** Opposite halfedge of the current one */
    std::size_t opposite;

    /**
     * @name  Halfedge
     * @fn  Halfedge(const std::size_t& origin_id)
     * @brief Constructor
     * @param[in]  origin_id  index of the vertex at the origin of the halfedge
     */
    explicit Halfedge(const std::size_t& origin_id) : origin(origin_id),
                            opposite(std::numeric_limits<std::size_t>::max()) {
    }

    /**
     * @name  Halfedge
     * @fn  Halfedge(const std::size_t& origin_id, const std::size_t& opposite_id)
     * @brief Constructor
     * @param[in]  origin_id   index of the vertex at the origin of the halfedge
     * @param[in]  opposite_id index of the opposite halfedge
     */
    Halfedge(const std::size_t& origin_id,
             const std::size_t& opposite_id) : origin(origin_id),
                                               opposite(opposite_id) {
    }

    /**
     * @name  IsBoundary
     * @fn  bool IsBoundary() const
     * @brief Returns wether the half edge is at the boundary of the mesh or not
     * @return true if at the boundary, false otherwise
     */
    bool IsBoundary() const {
      return opposite == std::numeric_limits<std::size_t>::max();
    }

    /**
     * @name  Next
     * @fn  std::size_t Next(std::size_t index) const
     * @brief Helper function to navigate between halfedges
     * @param[in]  index  index of the current halfedge
     * @return index of the next halfedge in the face
     */
    static std::size_t Next(std::size_t index) {
      if ((index + 1) % 3 != 0) {
        return index + 1;
      } else {
        return index - 2;
      }
    }

    /**
     * @name  Previous
     * @fn  static std::size_t Previous(std::size_t index)
     * @brief Helper function to navigate between halfedges
     * @param[in]  index  index of the current halfedge
     * @return index of the previous halfedge in the face
     */
    static std::size_t Previous(std::size_t index) {
      if (index % 3 != 0) {
        return index - 1;
      } else {
        return index + 2;
      }
    }
  };

  /**
   * @enum  PrimitiveType
   * @brief   Type of mesh primitive
   */
  enum PrimitiveType {
    /** Points */
    kPoint = 1,
    /** Triangle */
    kTriangle = 3
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name Mesh
   *  @fn Mesh()
   *  @brief  Constructor
   */
  Mesh();

  /**
   *  @name Mesh
   *  @fn explicit Mesh(const std::string& filename)
   *  @brief  Load mesh from supported file :
   *            .obj, .ply, .tri
   *  @param[in]  filename  Path to the mesh file
   */
  explicit Mesh(const std::string& filename);

  /**
   *  @name Mesh
   *  @fn Mesh(const Mesh<T, ColorContainer>& other) = delete
   *  @brief  Copy constructor
   *  @param[in]  other Mesh to copy from
   */
  Mesh(const Mesh<T, ColorContainer>& other);

  /**
   *  @name operator=
   *  @fn Mesh& operator=(const Mesh& rhs) = delete
   *  @brief  Assignment operator
   *  @param[in]  rhs   Mesh to assign from
   */
  Mesh& operator=(const Mesh<T, ColorContainer>& rhs) = delete;

  /**
   *  @name ~Mesh
   *  @fn ~Mesh()
   *  @brief  Destructor
   */
  ~Mesh();

  /**
   *  @name Load
   *  @fn int Load(const std::string& filename,
                   const LTS5::OGLProgram& program)
   *  @brief  Load mesh from supported file :
   *            .obj, .ply, .tri
   *  @param[in]  filename  Path to the mesh file
   *  @return -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   *  @name Save
   *  @fn int Save(const std::string& filename)
   *  @brief  Save mesh to supported file format:
   *            .ply
   *  @return -1 if error, 0 otherwise
   */
  int Save(const std::string& filename) const;

  /**
   *  @name CheckVertexDuplicates
   *  @fn bool CheckVertexDuplicates(double epsilon)
   *  @brief Check if some vertices of the mesh have duplicates
   *  @param[in]  epsilon  tolerance for duplicates
   *  @return true if duplicates are found, false otherwise
   */
  bool CheckVertexDuplicates(double epsilon);

  /**
   *  @name FixTriangleSoup
   *  @fn void FixTriangleSoup(double epsilon)
   *  @brief Merge identical vertices and redefine the rectangles
   *  @param[in]  epsilon  tolerance for duplicates
   */
  void FixTriangleSoup(double epsilon);

  /**
   *  @name BuildConnectivity
   *  @fn void BuildConnectivity()
   *  @brief  Construct vertex connectivity (vertex connection) used later
   *          in normal computation
   */
  void BuildConnectivity();

#pragma mark -
#pragma mark Process

  /**
   * @name  ComputeHalfedges
   * @fn  int ComputeHalfedges()
   * @brief Compute the halfedges representation from vertices and triangles
   * @return 0 if success, -1 otherwise
   */
  int ComputeHalfedges();

  /**
   * @name OneRingNeighbors
   * @fn void OneRingNeighbors(const std::size_t& vertex_id,
   *                           std::vector<std::size_t>* neighbors_id) const
   * @brief Query the one ring neighborhood of a vertex
   * @param[in] vertex_id id of the center vertex
   * @param[out] neighbors_id indices of one ring neighbors
   * @return Wether or not the one ring is complete
   */
  bool OneRingNeighbors(const std::size_t& vertex_id,
                        std::vector<std::size_t>* neighbors_id) const;

  /**
   *  @name ComputeVertexNormal
   *  @fn void ComputeVertexNormal()
   *  @brief  Compute normal for each vertex in the object
   */
  void ComputeVertexNormal();

  /**
   *  @name ComputeBoundingBox
   *  @fn void ComputeBoundingBox()
   *  @brief  Compute the bounding box of the mesh
   */
  void ComputeBoundingBox();

  /**
   * @name ComputeUniformLaplacian
   * @fn void ComputeUniformLaplacian(Eigen::SparseMatrix<T>* L) const
   * @brief Compute uniform graph Laplacian of a mesh
   * @param[out] D  a diagonal eigen sparse matrix containing the w_i
   * @param[out] M  an eigen sparse matrix containing the non diagonal w_ij
   */
  void ComputeUniformLaplacian(Eigen::SparseMatrix<T, 0, int>* D,
                               Eigen::SparseMatrix<T, 0, int>* M) const;

  /**
   * @name  ComputeCotanLaplacian
   * @fn  void ComputeCotanLaplacian(Eigen::SparseMatrix<T>* D,
   *                                 Eigen::SparseMatrix<T>* M) const
   * @brief Compute cotan weights Laplacian of the mesh
   * @param[out] D  a diagonal eigen sparse matrix containing the w_i
   * @param[out] M  an eigen sparse matrix containing the non diagonal w_ij
   */
  void ComputeCotanLaplacian(Eigen::SparseMatrix<T, 0, int>* D,
                             Eigen::SparseMatrix<T, 0, int>* M) const;

  /**
   * @name  ComputeEdgeProperties
   * @fn    void ComputeEdgeProperties(std::vector<Vpe>* vpe,
   *                                   std::vector<Fpe>* fpe)
   * @brief Compute the list of edges for the mesh.
   * @param[out] vpe List of vertices connected to the edges
   * @param[out] fpe List of faces connected to the edges
   */
  void ComputeEdgeProperties(std::vector<Vpe>* vpe, std::vector<Fpe>* fpe);

  /**
   * @name  RemeshForTriangles
   * @fn    void RemeshForTriangles(const std::vector<Triangle>& subset)
   * @brief Given a subset of triangles, update the mesh vertex/normal and
   *        triangulation.
   * @param[in] subset  List of selected triangles, must be part of the original
   *                    triangulation
   */
  void RemeshForTriangles(const std::vector<Triangle>& subset);

  /**
   * @name  ComputePrincipalCurvatures
   * @fn    void ComputePrincipalCurvatures()
   * @brief Compute principal curvature of the surface based on "ESTIMATING THE
   *        TENSOR OF CURVATURE OF A SURFACE FROM A POLYHEDRAL APPROXIMATION",
   *        G. Taubin
   */
  void ComputePrincipalCurvatures();

  /**
   *  @name Transform
   *  @fn void Transform(const cv::Mat& transform)
   *  @brief Apply transform to the mesh
   *  @param[in] transform  4x4 mat
   */
  void Transform(const cv::Mat& transform);

  /**
   *  @name Scale
   *  @fn void Scale(const T& factor);
   *  @brief  Scale the whole mesh (with respect to its COG
   *  @param[in] factor Factor by which to scale the mesh
   */
  void Scale(const T& factor);

  /**
   *  @name Translate
   *  @fn void Translate(const Vector3<T>& vector);
   *  @brief  Translate the whole mesh
   *  @param[in] vector   Vector by which to translate the mesh
   */
  void Translate(const LTS5::Vector3<T>& vector);

  /**
   *  @name NormalizeMainDiagonal
   *  @fn void NormalizeMainDiagonal();
   *  @brief  Scale the whole mesh such that main diagonal of the cube enclosing
   *          the bounding box is 1
   */
  void NormalizeMainDiagonal();

  /**
   *  @name NormalizeMesh
   *  @fn void NormalizeMesh();
   *  @brief  Scale the whole mesh such that main diagonal is 1 and it's
   *          centered around the origin
   */
  void NormalizeMesh();

  /**
   * @name  CropAroundVertex
   * @fn    void CropAroundVertex(const size_t& idx, const T& dist)
   * @brief Crop mesh around a given vertex. Triangle that are further away than
   *        a given distance are remove. No vertex are deleted, therefore the
   *        mesh will be degenerated after this step.
   * @param[in] idx     Vertex index to crop around
   * @param[in] dist    Radius of the cropping sphere
   */
  void CropAroundVertex(const size_t& idx, const T& dist);

  /**
   * @name  Area
   * @fn    T Area() const
   * @brief Compute surface area (i.e. sum of each triangle area)
   * @return    Mesh area
   */
  T Area() const;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name set_vertex
   *  @fn void set_vertex(const std::vector<Vertex>& data)
   *  @brief Define vertex position from given data. NOTE : Data will be copied
   *         into internal storage
   *  @param[in]  data  Custom vertex position to initialize mesh
   */
  void set_vertex(const std::vector<Vertex>& data) {
    vertex_ = data;
  }

  /**
   *  @name get_vertex
   *  @fn const std::vector<Vertex>& get_vertex() const
   *  @brief Give reference to internal vertex storage, can not be modified.
   *  @return Vertex array
   */
  const std::vector<Vertex>& get_vertex() const {
    return vertex_;
  }

  /**
   *  @name get_vertex
   *  @fn std::vector<Vertex>& get_vertex()
   *  @brief Give reference to internal vertex storage.
   *  @return Vertex array
   */
  std::vector<Vertex>& get_vertex() {
    return vertex_;
  }

  /**
   *  @name set_normal
   *  @fn void set_normal(const std::vector<Normal>& normal)
   *  @brief Define new normal. NOTE : Data will be copied
   *         into internal storage
   *  @param[in]  normal  Normal to initialize mesh
   */
  void set_normal(const std::vector<Normal>& normal) {
    normal_ = normal;
  }

  /**
   *  @name get_normal
   *  @fn const std::vector<Normal>& get_normal() const
   *  @brief Give reference to internal normal storage, can not be modified.
   *  @return Normal array
   */
  const std::vector<Normal>& get_normal() const {
    return normal_;
  }

  /**
   *  @name get_normal
   *  @fn std::vector<Normal>& get_normal()
   *  @brief Give reference to internal normal storage.
   *  @return Normal array
   */
  std::vector<Normal>& get_normal() {
    return normal_;
  }

  /**
   *  @name set_tex_coord
   *  @fn void set_tex_coord(const std::vector<TCoord>& tex_coord)
   *  @brief Define new texture coordinate. NOTE : Data will be copied
   *         into internal storage
   *  @param[in]  tex_coord  Texture coordinate to initialize mesh
   */
  void set_tex_coord(const std::vector<TCoord>& tex_coord) {
    tex_coord_ = tex_coord;
  }

  /**
   *  @name get_tex_coord
   *  @fn const std::vector<TCoord>& get_tex_coord() const
   *  @brief  Give reference to internal texture coordinate storage, can not be
   *          modified.
   *  @return Texture coordinate array
   */
  const std::vector<TCoord>& get_tex_coord() const {
    return tex_coord_;
  }

  /**
   *  @name get_tex_coord
   *  @fn std::vector<TCoord>& get_tex_coord()
   *  @brief  Give reference to internal texture coordinate storage.
   *  @return Texture coordinate array
   */
  std::vector<TCoord>& get_tex_coord() {
    return tex_coord_;
  }

  /**
   *  @name set_vertex_color
   *  @fn void set_vertex_color(const std::vector<Color>& vertex_color)
   *  @brief Define new vertex color. NOTE : Data will be copied
   *         into internal storage
   *  @param[in]  vertex_color  Vertex color to initialize mesh
   */
  void set_vertex_color(const std::vector<Color>& vertex_color) {
    vertex_color_ = vertex_color;
  }

  /**
   *  @name get_vertex_color
   *  @fn const std::vector<Color>& get_vertex_color() const
   *  @brief  Give reference to internal vertex color storage, can not be
   *          modified.
   *  @return Vertex color array
   */
  const std::vector<Color>& get_vertex_color() const {
    return vertex_color_;
  }

  /**
   *  @name get_vertex_color
   *  @fn std::vector<Color>& get_vertex_color()
   *  @brief  Give reference to internal vertex color storage.
   *  @return Vertex color array
   */
  std::vector<Color>& get_vertex_color() {
    return vertex_color_;
  }

  /**
   *  @name set_triangle
   *  @fn void set_triangle(const std::vector<Triangle >& tri)
   *  @brief Define new triangulation. NOTE : Data will be copied
   *         into internal storage
   *  @param[in]  tri  Triangle to initialize mesh
   */
  void set_triangle(const std::vector<Triangle >& tri) {
    tri_ = tri;
  }

  /**
   *  @name get_triangle
   *  @fn const std::vector<Triangle>& get_triangle() const
   *  @brief  Give reference to internal triangulation storage, can not be
   *          modified.
   *  @return Triangle array
   */
  const std::vector<Triangle>& get_triangle() const {
    return tri_;
  }

  /**
   *  @name get_triangle
   *  @fn std::vector<Triangle>& get_triangle()
   *  @brief  Give reference to internal triangulation storage, can be modified.
   *  @return Triangle array
   */
  std::vector<Triangle>& get_triangle() {
    return tri_;
  }

  /**
   *  @name get_comments
   *  @fn const std::vector<std::string>& get_comments()
   *  @brief  Give reference to internal comments storage, cannot be modified
   *  @return comments vector of string
   */
  const std::vector<std::string>& get_comments() const {
    return comments_;
  }

  /**
   * @name  set_comments
   * @fn    void set_comments(const std::vector<std::string>& comments)
   * @brief Set comment for this mesh
   * @param[in] comments List of comment
   */
  void set_comments(const std::vector<std::string>& comments) {
    comments_ = comments;
  }

  /**
   *  @name get_comments
   *  @fn const std::vector<std::string>& get_comments()
   *  @brief  Give reference to internal comments storage, can be modified
   *  @return comments vector of string
   */
  std::vector<std::string>& get_comments() {
    return comments_;
  }

  /**
   * @name  get_vertex_halfedges
   * @fn  const std::vector<std::size_t>& get_vertex_halfedges() const
   * @brief Get vector of references to a halfedge for each vertex
   * @return const ref to the vector
   */
  const std::vector<std::size_t>& get_vertex_halfedges() const {
    return vert_halfedges_;
  }

  /**
   * @name  get_halfedges
   * @fn  const std::vector<Halfedge>& get_halfedges() const
   * @brief Getter for the vector of halfedges
   * @return const ref to the vector
   */
  const std::vector<Halfedge>& get_halfedges() const {
    return halfedges_;
  }

  /**
   * @name  get_principal_curvatures
   * @fn    const std::vector<Curv>& get_principal_curvatures() const
   * @brief Provide principal curvatures of the surface
   * @return Vector of curvatures (k1, k2)
   */
  const std::vector<Curv>& get_principal_curvatures() const {
    return this->principal_curvature_;
  }

  /**
   *  @name bbox
   *  @fn const AABB<T>& bbox() const
   *  @brief  Getter for the bounding box
   */
  const AABB<T>& bbox() const {
    return bbox_;
  }

  /**
   *  @name bbox
   *  @fn AABB<T>& bbox()
   *  @brief  Getter for the bounding box
   */
  AABB<T>& bbox() {
    return bbox_;
  }

  /**
   *  @name bbox_is_computed
   *  @fn bool& bbox_is_computed()
   *  @brief  Getter for the bounding box flag
   */
  bool& bbox_is_computed() {
    return bbox_is_computed_;
  }

  /**
   *  @name halfedge_is_computed
   *  @fn bool halfedge_is_computed()
   *  @brief  Getter for the halfedge flag
   */
  bool halfedge_is_computed() const {
    return halfedges_is_computed_;
  }

  /**
   *  @name bcube
   *  @fn AABC<T> bcube() const
   *  @brief  Getter for the bounding cube
   */
  AABC<T> bcube() const {
    return AABC<T>(bbox_);
  }

  /**
   *  @name size
   *  @fn size_t size() const
   *  @brief  Getter for the size (aka the number of vertices) of the mesh
   */
  size_t size() const {
    return vertex_.size();
  }

  /**
   * @name  Cog
   * @fn  Vertex Cog() const
   * @brief Compute the CENTER OF GRAVITY of the mesh
   * @return COG
   */
  Vertex Cog() const;

  /**
   * @name  get_connectivity
   * @fn const std::vector<NeighbourIdx>& get_connectivity() const
   * @brief Provide vertex connectivity (i.e. sort-of one ring neighbour)
   * @return    Vertex connectivity
   */
  const std::vector<NeighbourIdx>& get_connectivity() const {
    return vertex_con_;
  }

  /**
   * @name  set_materials
   * @fn    void set_materials(const std::vector<Mat>& materials)
   * @brief Set new list of material for the mesh
   * @param[in] materials List of materials
   */
  void set_materials(const std::vector<Mat>& materials) {
    this->materials_ = materials;
  }

  /**
   * @name  get_materials
   * @fn    const std::vector<Mat>& get_materials() const
   * @brief Access list of materials used by this mesh
   * @return    List of materials
   */
  const std::vector<Mat>& get_materials() const {
    return this->materials_;
  }

  /**
   * @name  export_binary_ply
   * @fn    void set_export_binary_ply(const bool& flag)
   * @brief Indicates that `.ply` file will be exported as binary instead of
   *    plain text.
   * @param[in] flag Boolean flag, if `true` export ply file in binary format,
   *    otherwise plain text
   */
  void set_export_binary_ply(const bool& flag) {
    binary_ply_ = flag;
  }

  /**
   * @name  get_export_binary_ply
   * @fn    const bool& get_export_binary_ply() const
   * @brief Indicate if `.ply` file will be exported as plain text or binary
   * @return    Bool
   */
  const bool& get_export_binary_ply() const {
    return binary_ply_;
  }

#pragma mark -
#pragma mark Private

 private:
  /**
   *  @enum FileExt
   *  @brief  Type of possible mesh extension file
   */
  enum FileExt {
    /** Undefined */
    kUndef,
    /** .obj */
    kObj,
    /** .ply */
    kPly,
    /** .tri */
    kTri
  };

  /** Vertex position */
  std::vector<Vertex> vertex_;
  /** Vertex's normal */
  std::vector<Normal> normal_;
  /** Texture coordinate */
  std::vector<TCoord> tex_coord_;
  /** Vertex color */
  std::vector<Color> vertex_color_;
  /** Triangulation */
  std::vector<Triangle> tri_;
  /** Connectivity - vertex interconnection */
  std::vector<std::vector<int>> vertex_con_;
  /** Comments */
  std::vector<std::string> comments_;
  /** Boundary box, {x_min, x_max, y_min, y_max, z_min, z_max, } */
  AABB<T> bbox_;
  /** Wether or not the bounding box has been computed already or not */
  bool bbox_is_computed_;
  /** Vertices pointer to corresponding halfedge */
  std::vector<std::size_t> vert_halfedges_;
  /** Halfedges, encoding the connectivity of the mesh */
  std::vector<Halfedge> halfedges_;
  /** Wether or not the halfedges have been computed */
  bool halfedges_is_computed_;
  /** Principal curvature */
  std::vector<Curv> principal_curvature_;
  /** List of materials */
  std::vector<Material<T>> materials_;
  /** PLY File type */
  bool binary_ply_;

  /**
   *  @name HashExt
   *  @fn FileExt HashExt(const std::string& ext)
   *  @brief  Provide the type of extension
   *  @param[in]  ext Name of the extension
   *  @return Hash of the given extension if know by the object
   */
  FileExt HashExt(const std::string& ext) const;

  /**
   *  @name LoadOBJ
   *  @fn int LoadOBJ(const std::string& path)
   *  @brief  Load mesh from .obj file
   *  @param[in]  path  Path to .obj file
   *  @return -1 if error, 0 otherwise
   */
  int LoadOBJ(const std::string& path);

  /**
   *  @name SaveOBJ
   *  @fn int SaveOBJ(const std::string path) const
   *  @brief SAve mesh to a .obj file
   *  @param[in]  path  Path to .obj file
   *  @return -1 if error, 0 otherwise
   */
  int SaveOBJ(const std::string& path) const;

  /**
   *  @name LoadPLY
   *  @fn int LoadPLY(const std::string path)
   *  @brief  Load mesh from .ply file
   *  @param[in]  path  Path to .ply file
   *  @return -1 if error, 0 otherwise
   */
  int LoadPLY(const std::string& path);

  /**
   *  @name SavePLY
   *  @fn int SavePLY(const std::string path) const
   *  @brief SAve mesh to a .ply file
   *  @param[in]  path  Path to .ply file
   *  @return -1 if error, 0 otherwise
   */
  int SavePLY(const std::string& path) const;

  /**
   *  @name LoadTri
   *  @fn int LoadTri(const std::string& path)
   *  @brief  Load mesh triangulation from .tri file
   *  @param[in]  path  Path to .tri file
   *  @return -1 if error, 0 otherwise
   */
  int LoadTri(const std::string& path);

  /**
   * @name  ComputeCotanWeight
   * @fn T ComputeCotanWeight(const Vertex& I,
                              const Vertex& J,
                              const Vertex& A,
                              T* weight,
                              T* area) const
   * @brief Compute individual cotan weight for the given triangle
   * @param[in] I        1st vertex of the triangle
   * @param[in] J        2nd vertex of the triangle
   * @param[in] A        3rd vertex of the triangle
   * @param[out] weight  cotan weight
   * @param[out] area    area of the triangle
   */
  void ComputeCotanWeight(const Vertex& I,
                          const Vertex& J,
                          const Vertex& A,
                          T* weight,
                          T* area) const;
};
}  // namespace LTS5
#endif /* __LTS5_MESH__ */
