/**
 *  @file   base_collision_tree.hpp
 *  @brief  Interface definition for collision Tree (AABBTree, OCTree)
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   10/03/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __COLLISION_TREE__
#define __COLLISION_TREE__

#include <vector>

#include "lts5/utils/math/vector.hpp"
#include "lts5/geometry/mesh.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class BaseCollisionTree
 * @brief Interface definition for Tree
 * @author  Christophe Ecabert
 * @date    10/03/19
 * @ingroup geometry
 */
template< typename T>
class BaseCollisionTree {

 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  BaseCollisionTree
   * @fn  virtual ~BaseCollisionTree(void)
   * @brief Destructor
   */
  virtual ~BaseCollisionTree(void) {}

  /**
   * @name  Insert
   * @fn  virtual void Insert(const Mesh<T>& mesh) = 0
   * @brief Insert a mesh into the tree
   * @param[in] mesh  3D mesh used to build tree
   */
  virtual void Insert(const Mesh<T>& mesh) = 0;

  /**
   * @name  Build
   * @fn   virtual void Build(void) = 0
   * @brief Build iteratively the whole tree
   */
  virtual void Build(void) = 0;

  /**
   * @name  Clear
   * @fn  virtual void Clear(void) = 0
   * @brief Clear the whole tree
   */
  virtual void Clear(void) = 0;

#pragma mark -
#pragma mark Usage

  /**
   * @name  DoIntersect
   * @fn  virtual bool DoIntersect(const Vector3<T>& p,
   *                                const Vector3<T>& dir,
   *                                std::vector<int>* idx) = 0
   * @brief Check if a given ray ,p + t*dir, intersect with one of the
   *        triangle's bounding box in the tree and provide its corresponding
   *        index
   *  @param[in]  p   Starting point of the ray
   *  @param[in]  dir Direction of the the ray
   *  @param[out] idx Index of the corresponding intersection (cells)
   *  @return True if intersect, false otherwise
   */
  virtual bool DoIntersect(const Vector3<T>& p,
                           const Vector3<T>& dir,
                           std::vector<int>* idx) = 0;
};
}  // namepsace LTS5
#endif //__COLLISION_TREE__
