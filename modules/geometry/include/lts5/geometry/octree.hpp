/**
 *  @file   lts5/geometry/octree.hpp
 *  @brief  OCTree implementation, contructed from a given mesh, adapted from
 *          CGAL
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   03/03/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_OCTREE__
#define __LTS5_OCTREE__

#include <vector>
#include <stack>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/geometry/base_collision_tree.hpp"
#include "lts5/geometry/aabb.hpp"
#include "lts5/geometry/octree_node.hpp"
#include "lts5/geometry/bounding_sphere.hpp"
#include "lts5/geometry/intersection_utils.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OCTree
 * @brief   OCTree implementation, constructed from a given mesh, adapted from
 *          CGAL
 * @author  Christophe Ecabert
 * @date    03/03/2016
 * @ingroup geometry
 */
template< typename T>
class LTS5_EXPORTS OCTree {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  OCTree
   * @fn  OCTree()
   * @brief Constructor
   */
  OCTree();

  /**
   * @name  OCTree
   * @fn  OCTree(const int& bucket_size, const int& max_depth)
   * @brief Constructor
   * @param[in] bucket_size Maximum number of object per leaf node
   * @param[in] max_depth   Maximum tree depth
   */
  OCTree(const int& bucket_size, const int& max_depth);

  /**
   * @name  OCTree
   * @fn  OCTree(const OCTree& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  OCTree(const OCTree& other) = delete;

  /**
   * @name  operator=
   * @fn  OCTree& operator=(const OCTree& rhs) = delete
   * @brief Assign operator
   * @param[in] rhs Object to assign from
   */
  OCTree& operator=(const OCTree& rhs) = delete;

  /**
   * @name  ~OCTree
   * @fn  ~OCTree()
   * @brief Destructor
   */
  ~OCTree();

  /**
   * @name  Insert
   * @fn  int Insert(const Mesh<T, ColorContainer>& mesh,
                const typename Mesh<T, ColorContainer>::PrimitiveType primitive)
   * @brief Insert a range of element into the tree
   * @param[in] mesh      3D mesh used to build tree
   * @param[in] primitive Type of primitive to consider (points, triangle)
   * @return -1 if error, 0 otherwise
   */
  template<typename ColorContainer>
  int Insert(const Mesh<T, ColorContainer>& mesh,
             const typename Mesh<T, ColorContainer>::PrimitiveType& primitive);

  /**
   * @name  Build
   * @fn   void Build()
   * @brief Build iteratively the whole tree
   */
  void Build();

  /**
   * @name  Clear
   * @fn  void Clear()
   * @brief Clear the whole tree
   */
  void Clear();

#pragma mark -
#pragma mark Usage

  /**
   * @name  DoIntersect
   * @fn  bool DoIntersect(const Vector3<T>& p, const Vector3<T>& dir,
   *                        std::vector<int>* idx)
   * @brief Check if a given ray ,p + t*dir, intersect with one of the
   *        triangle's bounding box in the tree and provide its corresponding
   *        index
   *  @param[in]  p   Starting point of the ray
   *  @param[in]  dir Direction of the the ray
   *  @param[out] idx Index of the corresponding intersection
   *  @return True if intersect, false otherwise
   */
  bool DoIntersect(const Vector3<T>& p,
                   const Vector3<T>& dir,
                   std::vector<int>* idx);

  /**
   * @name  IntersectWithSegment
   * @fn  bool IntersectWithSegment(const Mesh<T, ColorContainer>& mesh,
                            const typename Mesh<T, ColorContainer>::Vertex& p,
                            const typename Mesh<T, ColorContainer>::Vertex& q,
                            const T& t_max,
                            T* t)
   * @brief Test if the mesh intersects with a given line PQ
   * @param[in]   mesh  Mesh to test against
   * @param[in]   p     Starting point of the segement to test against
   * @param[in]   q     Stopping point of the segment to test against
   * @param[in]   t_max Maximum t allowed, by default should be 1.f
   * @param[out]  t     Position along the line PQ, t in [0, 1]
   * @return  True if segment intersect with the mesh, False otherwise
   */
  template<typename ColorContainer>
  bool IntersectWithSegment(const Mesh<T, ColorContainer>& mesh,
                            const typename Mesh<T, ColorContainer>::Vertex& p,
                            const typename Mesh<T, ColorContainer>::Vertex& q,
                            const T& t_max,
                            T* t);

  /**
   * @name  IntersectWithSegment
   * @fn  bool IntersectWithSegment(const Mesh<T, ColorContainer>& mesh,
                            const typename Mesh<T, ColorContainer>::Vertex& p,
                            const typename Mesh<T, ColorContainer>::Vertex& q,
                            const bool& cull_face,
                            const T& t_max,
                            T* t)
   * @brief Test if the mesh intersects with a given line PQ
   * @param[in]   mesh  Mesh to test against
   * @param[in]   p     Starting point of the segement to test against
   * @param[in]   q     Stopping point of the segment to test against
   * @param[in]   cull_face   If `True`, do not consider back facing triangles
   * @param[in]   t_max Maximum t allowed, by default should be 1.f
   * @param[out]  t     Position along the line PQ, t in [0, 1]
   * @return  True if segment intersect with the mesh, False otherwise
   */
  template<typename ColorContainer>
  bool IntersectWithSegment(const Mesh<T, ColorContainer>& mesh,
                            const typename Mesh<T, ColorContainer>::Vertex& p,
                            const typename Mesh<T, ColorContainer>::Vertex& q,
                            const bool& cull_face,
                            const T& t_max,
                            T* t);

  /**
   * @name  RadiusNearestNeighbor
   * @fn  void RadiusNearestNeighbor(const Sphere<T>& sphere,
                                     std::vector<int>* idx)
   * @brief Find all point laying inside a given sphere
   * @param[in]   sphere    Sphere defining search region
   * @param[out]  idx       List of primitive satisfying the condition
   */
  void RadiusNearestNeighbor(const Sphere<T>& sphere,
                             std::vector<int>* idx);

#pragma mark -
#pragma mark accessors

  /**
   * @name  get_root_node
   * @fn  const OCTreeNode<T>* get_root_node() const
   * @brief Provide const access to root node
   * @return    Root node
   */
  const OCTreeNode<T>* get_root_node() const {
    return root_node_;
  }

  /**
   * @name  get_index_map
   * @fn    const std::vector<size_t>& get_index_map() const
   * @brief Provide the correspondance between nodes and triangle bounding box
   * @return    index map
   */
  const std::vector<size_t>& get_index_map() const {
    return index_map_;
  }

#pragma mark -
#pragma mark Private

 protected:

  /** Forward declaration - Tree traversal */
  struct RayTraversal;

  /**
   * @enum  SortSplitType
   * @brief Possible direction for sort/split
   */
  enum SortSplitType {
    /** Sort/Split in X axis */
    kX = 0,
    /** Sort/Split in Y axis */
    kY = 1,
    /** Sort/Split in Z axis */
    kZ = 2
  };

  /** Max index value */
  static const std::size_t max_size_t = OCTreeNode<T>::max_size_t;

  /**
   * @name  SortAndSplit
   * @fn  std::size_t SortAndSplit(std::size_t first,
   *                               std::size_t last,
   *                               const SortSplitType direction,
   *                               const T threshold)
   * @brief Sort index in the map from first to last. Return index of the
   *        element right below a given threshold
   * @param[in] first       First index
   * @param[in] last        Last index
   * @param[in] direction   Direction in which to perform sort
   * @param[in] threshold   Threshold value for the split
   * @return  Element right below threshold
   */
  std::size_t SortAndSplit(std::size_t first,
                           std::size_t last,
                           const SortSplitType& direction,
                           const T& threshold);

  /**
   * @name  get_element
   * @fn  T get_element(const std::size_t index, const SortSplitType direction)
   * @brief Get element value from original data structure for a given
   *        direction and index
   * @param[in] index       Index to query
   * @param[in] direction   Direction selection
   * @return    Element value
   */
  T get_element(const std::size_t& index, const SortSplitType& direction);

  /** Data (basically a vector of Vertices) from a Mesh */
  std::vector<Vector3<T>> data_;
  /** Root node */
  OCTreeNode<T>* root_node_;
  /** Index map to keep object properly ordered */
  std::vector<std::size_t> index_map_;
  /** Bucket size, i.e. number Maximum of object per leaf node */
  int bucket_size_;
  /** Maximum tree depth */
  int max_depth_;
  /** Maximum level in the tree */
  int max_tree_level_;
};

#pragma mark -
#pragma mark Implementation Of Templated Methods

/*
 * @name  Insert
 * @fn  int Insert(const Mesh<T, ColorContainer>& mesh,
              const typename Mesh<T, ColorContainer>::PrimitiveType primitive)
 * @brief Insert a range of element into the tree
 * @param[in] mesh      3D mesh used to build tree
 * @param[in] primitive Type of primitive to consider (points, triangle)
 * @return -1 if error, 0 otherwise
 */
template<typename T>
template<typename ColorContainer>
int OCTree<T>::Insert(const Mesh<T, ColorContainer>& mesh,
                      const typename Mesh<T, ColorContainer>::PrimitiveType& primitive) {
  // Insert
  using PrimitiveType = typename Mesh<T, ColorContainer>::PrimitiveType;
  using Vertex = typename Mesh<T, ColorContainer>::Vertex;
  int err = -1;
  switch(primitive) {
    // Point
    case PrimitiveType::kPoint : {
      // Get mesh element
      const auto& vertex = mesh.get_vertex();
      std::size_t idx_map = index_map_.size();
      auto first = vertex.begin();
      auto last = vertex.end();
      while(first != last) {
        data_.push_back(*first);
        index_map_.push_back(idx_map);
        ++first;
        ++idx_map;
      }
      err = 0;
    }
      break;
      // Triangle
    case PrimitiveType::kTriangle : {
      // Get mesh element
      const auto& vertex = mesh.get_vertex();
      const auto& tri = mesh.get_triangle();
      auto first_triangle = tri.begin();
      auto last_triangle = tri.end();
      // Loop over all triangles
      std::size_t idx_map = index_map_.size();
      while(first_triangle != last_triangle) {
        // Compute triangle center of gravity
        const int* tri_ptr =  &first_triangle->x_;
        Vertex cog(0.0, 0.0, 0.0);
        for (int i = 0; i < 3; ++i) {
          const auto& v = vertex[tri_ptr[i]];
          cog += v;
        }
        cog /= T(3.0);
        data_.push_back(cog);
        index_map_.push_back(idx_map);
        ++first_triangle;
        ++idx_map;
      }
      err = 0;
    }
      break;
    default: std::cout << "Error, unsupported mesh primitive" << std::endl;
      break;
  }
  return err;
}

/*
 * @name  IntersectWithSegment
 * @fn  bool IntersectWithSegment(const Mesh<T, ColorContainer>& mesh,
                          const typename Mesh<T, ColorContainer>::Vertex& p,
                          const typename Mesh<T, ColorContainer>::Vertex& q,
                          const T t_max,
                          T* t)
 * @brief Test if the mesh intersects with a given line PQ
 * @param[in]   mesh  Mesh to test against
 * @param[in]   p     Starting point of the segement to test against
 * @param[in]   q     Stoping point of the segement to test against
 * @param[in]   t_max Maximum t allowed, by default should be 1.f
 * @param[out]  t     Position along the line PQ, t in [0, 1]
 * @return  True if segment intersect with the mesh, False otherwise
 */
template<typename T>
template<typename ColorContainer>
bool OCTree<T>::IntersectWithSegment(const Mesh<T, ColorContainer>& mesh,
                          const typename Mesh<T, ColorContainer>::Vertex& p,
                          const typename Mesh<T, ColorContainer>::Vertex& q,
                          const T& t_max,
                          T* t) {
  return this->IntersectWithSegment(mesh, p, q, true, t_max, t);
}

/*
 * @name  IntersectWithSegment
 * @fn  bool IntersectWithSegment(const Mesh<T, ColorContainer>& mesh,
                          const typename Mesh<T, ColorContainer>::Vertex& p,
                          const typename Mesh<T, ColorContainer>::Vertex& q,
                          const bool& cull_face,
                          const T t_max,
                          T* t)
 * @brief Test if the mesh intersects with a given line PQ
 * @param[in]   mesh  Mesh to test against
 * @param[in]   p     Starting point of the segement to test against
 * @param[in]   q     Stopping point of the segment to test against
 * @param[in]   cull_face   If `True`, do not consider back facing triangles
 * @param[in]   t_max Maximum t allowed, by default should be 1.f
 * @param[out]  t     Position along the line PQ, t in [0, 1]
 * @return  True if segment intersect with the mesh, False otherwise
 */
template<typename T>
template<typename ColorContainer>
bool OCTree<T>::
IntersectWithSegment(const Mesh<T, ColorContainer>& mesh,
                     const typename Mesh<T, ColorContainer>::Vertex& p,
                     const typename Mesh<T, ColorContainer>::Vertex& q,
                     const bool& cull_face,
                     const T& t_max,
                     T* t) {
  bool intersect = false;
  const T t_init = *t;
  // Use tree to prune detection search space
  std::vector<int> indexes;
  indexes.reserve(500);
  if(this->DoIntersect(p, q - p, &indexes)) {
    const auto& tri_list = mesh.get_triangle();
    const auto& vertex = mesh.get_vertex();
    for (const auto& idx : indexes) {
      const auto& tri = tri_list[idx];
      const auto& A = vertex[tri.x_];
      const auto& B = vertex[tri.y_];
      const auto& C = vertex[tri.z_];
      const auto tol = T(2.0) * std::numeric_limits<T>::epsilon();
      intersect = LTS5::IntersectTriWithSegment(p, q,       // Segment
                                                A, B, C,    // Triangle
                                                cull_face,  // Cull face ?
                                                tol,
                                                t);
      if (intersect && (*t < t_max)) {
        break;
      } else {
        intersect = false;
        *t = t_init;
      }
    }
  }
  return intersect;
}

}  // namepsace LTS5
#endif //__LTS5_OCTREE__
