/**
 *  @file   aabb_tree.hpp
 *  @brief  AABB tree implementation, based on CGAL
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   24/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_AABB_TREE__
#define __LTS5_AABB_TREE__

#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/aabb_node.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   AABBTree
 * @brief   AABB tree implementation, based on CGAL
 * @author  Christophe Ecabert
 * @date    24/02/16
 * @ingroup geometry
 */
template<typename T>
class LTS5_EXPORTS AABBTree {

#pragma mark -
#pragma mark Initialization

 public :

  /** Vertex type definition */
  using Vertex = typename Mesh<T>::Vertex;
  /** Triangle type definition */
  using Triangle = typename LTS5::Mesh<T>::Triangle;

  /**
   * @name  AABBTree
   * @fn  AABBTree(void)
   * @brief Constructor
   */
  AABBTree(void);

  /**
   * @name  AABBTree
   * @fn  AABBTree(const AABBTree& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  AABBTree(const AABBTree& other) = delete;

  /**
   * @name  operator=
   * @fn  AABBTree& operator=(const AABBTree& rhs) = delete
   * @brief Assign operator
   * @param[in] rhs Object to assign from
   */
  AABBTree& operator=(const AABBTree& rhs) = delete;

  /**
   * @name  AABBTree
   * @fn  AABBTree(const Mesh<T>& mesh)
   * @brief Constructor
   * @param[in] mesh  3D mesh used to build tree
   */
  AABBTree(const Mesh<T>& mesh);

  /**
   * @name  ~AABBTree
   * @fn  ~AABBTree(void)
   * @brief Destructor
   */
  virtual ~AABBTree(void);

  /**
   * @name  Insert
   * @fn  void Insert(const Mesh<T>& mesh)
   * @brief Insert a range of element into the tree
   * @param[in] mesh  3D mesh used to build tree
   */
  void Insert(const Mesh<T>& mesh);

  /**
   * @name  Clear
   * @fn  void Clear(void)
   * @brief Clear the whole tree
   */
  void Clear(void);

  /**
   * @name  Build
   * @fn   void Build(void)
   * @brief Build recursively the whole tree
   */
  void Build(void);

#pragma mark -
#pragma mark Usage

  /**
   * @name  DoIntersect
   * @fn  bool DoIntersect(const Vector3<T>& p, const Vector3<T>& dir,
   *                        std::vector<int>* idx)
   * @brief Check if a given ray ,p + t*dir, intersect with one of the
   *        triangle's bounding box in the tree and provide its corresponding
   *        index
   *  @param[in]  p   Starting point of the ray
   *  @param[in]  dir Direction of the the ray
   *  @param[out] idx Index of the corresponding intersection
   *  @return True if intersect, false otherwise
   */
  bool DoIntersect(const Vector3<T>& p,
                   const Vector3<T>& dir,
                   std::vector<int>* idx);

  /**
   * @name  IntersectWithSegment
   * @fn  bool IntersectWithSegment(const Mesh<T>& mesh,
                                    const Vertex& p,
                                    const Vertex& q,
                                    const T t_max,
                                    T* t)
   * @brief Test if the mesh intersects with a given line PQ
   * @param[in]   mesh  Mesh to test against
   * @param[in]   p     Starting point of the segement to test against
   * @param[in]   q     Stoping point of the segement to test against
   * @param[in]   t_max Maximum t allowed, by default should be 1.f
   * @param[out]  t     Position along the line PQ, t in [0, 1]
   * @return  True if segment intersect with the mesh, False otherwise
   */
  bool IntersectWithSegment(const Mesh<T>& mesh,
                            const Vertex& p,
                            const Vertex& q,
                            const T t_max,
                            T* t);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_bbox
   * @fn  const AABB<T>& get_bbox(void) const
   * @brief Give the overall object bounding box
   * @return  Object bounding box
   */
  const AABB<T>& get_bbox(void) const {
      return root_node_->get_bbox();
  }

  /**
   * @name  Size
   * @fn  size_t Size(void) const
   * @brief Provide the size of the tree
   * @return Tree size
   */
  size_t Size(void) const {
    return data_bbox_.size();
  }

  /**
   * @name  Empty
   * @fn  bool Empty(void) const
   * @brief Indicate if tree is empty
   * @return  True if empty, false otherwise
   */
  bool Empty(void) const {
    return data_bbox_.empty();
  }

  /**
   * @name  get_root_node
   * @fn  const AABBNode<T>* get_root_node(void) const
   * @brief Give access to root node
   * @return  Root node pointer
   */
  const AABBNode<T>* get_root_node(void) const {
    return root_node_;
  }

#pragma mark -
#pragma mark Protected

 protected:

  /**
   * @name  ClearNodes
   * @fn  void ClearNodes(void)
   * @brief Clear all tree's nodes
   */
  void ClearNodes(void);

  /** Primitive bounding boxes */
  std::vector<AABB<T>> data_bbox_;
  /** Root node */
  AABBNode<T>* root_node_;
  /** Indicate if tree need to be build */
  bool need_build_;
};
}  // namepsace LTS5

#endif //__LTS5_AABB_TREE__
