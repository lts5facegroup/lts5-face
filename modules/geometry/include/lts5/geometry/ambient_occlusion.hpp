/**
 *  @file   lts5/geometry/ambient_occlusion.hpp
 *  @brief  Compute Ambient Occlusion for a given surface
 *  @ingroup geoemtry
 *
 *  @author Christophe Ecabert
 *  @date   11/13/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_AMBIENT_OCCLUSION__
#define __LTS5_AMBIENT_OCCLUSION__

#include <random>

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/octree.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<typename T>
class LTS5_EXPORTS AmbientOcclusionGenerator {
 public:
  /** Mesh Type */
  using Mesh_t = Mesh<T, Vector3<T>>;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  AmbientOcclusionGenerator
   * @fn    AmbientOcclusionGenerator(Mesh_t& mesh)
   * @brief Constructor
   * @param[in] mesh    Mesh
   */
  explicit AmbientOcclusionGenerator(Mesh_t& mesh);

  /**
   * @name  AmbientOcclusionGenerator
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  AmbientOcclusionGenerator(const AmbientOcclusionGenerator& other) = delete;

  /**
   * @name  operator=
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  AmbientOcclusionGenerator&
  operator=(const AmbientOcclusionGenerator& rhs) = delete;

  /**
   * @name  ~AmbientOcclusionGenerator
   * @fn    ~AmbientOcclusionGenerator() = default
   * @brief Destructor
   */
  ~AmbientOcclusionGenerator() = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Generate
   * @fn    void Generate(const int& n_sample)
   * @brief Generate Ambient Occlusion using Monte Carlo sampling
   * @param[in] n_sample Number of sample to use to estimate AO.
   */
  void Generate(const int& n_sample);

  /**
   * @name  Save
   * @fn    int Save(const std::string& path)
   * @brief Save ambient occlusion into a file
   * @param[in] path Location where to dump the data (i.e. AO)
   * @return -1 if error, 0 otherwise
   */
  int Save(const std::string& path);

#pragma mark -
#pragma mark Accessor

  /**
   * @name  get_ao
   * @fn    const cv::Mat& get_ao() const
   * @brief Return estimated Ambient Occlusions
   * @return    AOs
   */
  const cv::Mat& get_ao() const {
    return ao_;
  }

#pragma mark -
#pragma mark Private

 private:
  /**
   * @class SphericalSampler
   * @brief Uniform sampler over half-sphere
   * @date  11/13/19
   * @ingroup   geometry
   */
  class HalfSphereSampler {
   public:
    /** Vec3 */
    using Vec3 = Vector3<T>;

    /**
     * @name    HalfSphereSampler
     * @fn      HalfSphereSampler()
     * @brief   Constructor
     */
    HalfSphereSampler();

    /**
     * @name    operator()
     * @fn  Vec3 operator()()
     * @brief Sample 3D pts randomly on a half sphere.
     * @return  Sampled points
     */
    Vec3 operator()();

   private:
    /** Generator */
    std::mt19937 gen_;
    /** Distribution */
    std::normal_distribution<T> dist_;
  };

  /** Mesh reference */
  Mesh_t* mesh_;
  /** OCTree for fast intersection dection */
  OCTree<T> tree_;
  /** Sampler */
  HalfSphereSampler sampler_;
  /** Ambient occlusion */
  cv::Mat ao_;
  /** Half-sphere radius */
  T radius_;
};
}  // namespace LTS5
#endif  // __LTS5_AMBIENT_OCCLUSION__
