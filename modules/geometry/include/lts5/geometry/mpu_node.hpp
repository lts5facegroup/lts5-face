/**
 *  @file   mpu_node.hpp
 *  @brief  Placeholder to store data while building MPU.
 *  @ingroup geometry
 *
 *  @author Gabriel Cuendet
 *  @date   11/04/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */
#ifndef __LTS5_MPU_NODE__
#define __LTS5_MPU_NODE__

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/aabb.hpp"
#include "lts5/geometry/shape_estimator_base.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   MPUNode
 * @brief   Placeholder to store data while building MPU.
 * @author  Gabriel Cuendet
 * @date    11/04/2016
 * @ingroup geometry
 */
template< typename T>
class LTS5_EXPORTS MPUNode {
 public:

  /**
   * @name  ComputeMeanNormalDeviation
   * @fn  static double ComputeMeanNormalDeviation(const std::vector<LTS5::Vector3<T> >& normals)
   * @brief Compute the mean normal, and return the angle between each normal and their mean
   * @param[in]  normals Vector of normals
   * @return mean deviation angle
   */
  static double ComputeMeanNormalDeviation(const std::vector<LTS5::Vector3<T> >& normals);

#pragma mark -
#pragma mark Initialization

  /**
   * @name  MPUNode
   * @fn  MPUNode(void)
   * @brief Constructor
   */
  MPUNode(void);

  /**
   * @name  MPUNode
   * @fn  MPUNode(const uint64_t& location, const AABB<T>& bbox)
   * @brief Constructor
   * @param[in] location  Hash code of the location of the node
   * @param[in] bbox      Bounding box of the current node
   */
  MPUNode(const uint64_t& location, const AABB<T>& bbox);

  /**
   * @name  MPUNode
   * @fn  MPUNode(const MPUNode& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  MPUNode(const MPUNode& other);

  /**
   * @name  operator=
   * @fn  MPUNode& operator=(const MPUNode& rhs) = delete
   * @brief Assign operator
   * @param[in] rhs Object to assign from
   */
  MPUNode& operator=(const MPUNode& rhs) = delete;

  /**
   * @name  ~MPUNode
   * @fn  ~MPUNode(void)
   * @brief Destructor
   */
  ~MPUNode(void);

#pragma mark -
#pragma mark Getters and Setters

  uint64_t location(void) const {return location_hash_;}

  /**
   * @name  Level
   * @fn  std::size_t Level(void)
   * @brief Compute the level of the current node from its location hash
   * @return level    The computed level
   */
  std::size_t Level(void) const;

  /**
   * @name  diagonal
   * @fn  T diagonal(void)
   * @brief Getter for the main diagonal of the bounding box
   */
  T diagonal(void) const {return diagonal_;}

  /**
   * @name  epsilon
   * @fn  T epsilon(void)
   * @brief Getter for the approximation error epsilon
   */
  T epsilon(void) const {return epsiloni_;}

  /**
   * @name  bbox
   * @fn  T bbox(void)
   * @brief Getter for the bounding box
   */
  const AABB<T>& bbox(void) const {return bbox_;}

  /**
   * @name  center
   * @fn  LTS5::Vector3<T> center(void)
   * @brief Getter for the center of the bounding box
   * @return center   A Vector3<T> containing the center
   */
  const LTS5::Vector3<T>& center(void) const {return ci_;}

  /**
   * @name  set_empty
   * @fn  void set_empty(const bool& value)
   * @brief Setter for the empty flag indicating wether or not the initial
   *        ball was empty
   */
  void set_empty(const bool& value) {this->empty_ = value;}

  /**
   * @name  Ri
   * @fn  T Ri(void) const
   * @brief Getter for the support radius Ri
   * @return support radius of type T
   */
  T Ri(void) const {return Ri_;}

#pragma mark -
#pragma mark Usage

  /**
   * @name  Approximate
   * @fn  void Approximate(const std::vector<LTS5::Vector3<T> >& vertices,
                   const std::vector<LTS5::Vector3<T> >& normals,
                   const T& Ri);
   * @brief Compute the implicite approximation of the mesh defined by vertices and normals
   * @param[in]  vertices   List of vertices
   * @param[in]  normals    List of normals
   * @param[in]  Ri Support radius for the approximation
   */
  void Approximate(const std::vector<LTS5::Vector3<T> >& vertices,
                   const std::vector<LTS5::Vector3<T> >& normals,
                   const T& Ri);

  /**
   * @name  Evaluate
   * @fn  void Evaluate(const Vector3<T>& x,  T* SwQ, T* Sw) const
   * @brief Evaluate SwQ and Sw in that node, given x
   * @param[in] x     Querry point x
   * @param[out] SwQ  The sum of weighted values Q
   * @param[out] Sw   The sum of weights
   */
  void Evaluate(const Vector3<T>& x,  T* SwQ, T* Sw) const;

  /**
   * @name  EvaluateSquare
   * @fn  void EvaluateSquare(const Vector3<T>& x, T* SwQ2, T* Sw) const
   * @brief Evaluate SwQ2 (square of Q) and Sw in that node, given x
   * @param[in] x      Querry point x
   * @param[out] SwQ2  The sum of weighted values Q
   * @param[out] Sw    The sum of weights
   */
  void EvaluateSquare(const Vector3<T>& x, T* SwQ2, T* Sw) const;

  /**
   * @name  EvaluateGrad
   * @fn  void EvaluateGrad(const Vector3<T>& x, Vector3<T>* SwGradQ,
   *                        T* Sw) const
   * @brief Evaluate gradient of that node, given x
   * @param[in]  x        position where to evaluate gradient
   * @param[out] SwGradQ  Sum of weighted gradients
   * @param[out] Sw       Sum of weights
   */
  void EvaluateGrad(const Vector3<T>& x, Vector3<T>* SwGradQ, T* Sw) const;

  /**
   * @name  EvaluateGradSquare
   * @fn  void EvaluateGradSquare(const Vector3<T>& x, Vector3<T>* SwGradQ2,
   *                              T* Sw) const
   * @brief Evaluate gradient of that node, given x
   * @param[in]  x         position where to evaluate gradient
   * @param[out] SwGradQ2  Sum of weighted gradients
   * @param[out] Sw        Sum of weights
   */
  void EvaluateGradSquare(const Vector3<T>& x, Vector3<T>* SwGradQ2,
                          T* Sw) const;

 private:
  /** Max index value */
  static const std::size_t max_size_t = std::numeric_limits<std::size_t>::max();
  /** Hash code of the cell, given by its location */
  uint64_t location_hash_;
  /** Is the initial ball empty or not? */
  bool empty_;
  /** Indicator variable for childs */
  bool has_childs_;
  /** Center of the cell c_i */
  LTS5::Vector3<T> ci_;
  /** Bounding volume (to define the split) */
  AABB<T> bbox_;
  /** Main diagonal of the bounding box */
  T diagonal_;
  /** Support radius R_i */
  T Ri_;
  /** Local max-norm approximation error epsilon_i */
  T epsiloni_;
  /** Local shape estimator */
  ShapeEstimatorBase<T>* estimator_;
};
} // namespace LTS5
#endif //__LTS5_MPU_NODE__
