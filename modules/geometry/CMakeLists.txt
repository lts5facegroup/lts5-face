set(SUBSYS_NAME geometry)
set(SUBSYS_DESC "LTS5 3D Geometry library")
#Set internal library dependencies, here there isn't other dependencies
set(SUBSYS_DEPS utils)
# Python internal dependencies
set(PYTHON_DEPS python)

IF(OPT_PYTHON_WRAPPER)
  LIST(APPEND SUBSYS_DEPS ${PYTHON_DEPS})
ENDIF(OPT_PYTHON_WRAPPER)


set(build TRUE)
LTS5_SUBSYS_OPTION(build "${SUBSYS_NAME}" "${SUBSYS_DESC}" ON)
#Add dependencies as well as external dependencies
SET(inc_deps)
LTS5_SUBSYS_DEPEND(build "${SUBSYS_NAME}" inc_deps DEPS ${SUBSYS_DEPS} EXT_DEPS opencv)
if(build)
  # Work around to add external dependency build on the fly since no mechanism handle it in the libary
  ADD_SUBDIRECTORY(${LTS5_SOURCE_DIR}/3rdparty/ply ${LTS5_OUTPUT_3RDPARTY_LIB_DIR}/ply EXCLUDE_FROM_ALL)

  set(srcs
      src/aabb_mpu_tree.cpp
      src/aabb_tree.cpp
      src/ambient_occlusion.cpp
      src/bivariate_quadratic_poly.cpp
      src/discrete_remeshing.cpp
      src/general_quadric.cpp
      src/intersection_utils.cpp
      src/isotropic_clustering_metric.cpp
      src/manifold_harmonic_transform.cpp
      src/marching_cube.cpp
      src/mesh.cpp
      src/mesh_factory.cpp
      src/mpu_node.cpp
      src/multiscale_partition_unity.cpp
      src/octree.cpp
      src/spherical_harmonic.cpp)
  set(incs_base
      include/lts5/geometry.hpp)
  set(srcs_python
      python/ambient_occlusion_wrapper.cpp
      python/geometry_wrapper.cpp
      python/mesh_wrapper.cpp
      python/tree_wrapper.cpp)
  set(incs
      include/lts5/${SUBSYS_NAME}/aabb_mpu_tree.hpp
      include/lts5/${SUBSYS_NAME}/aabb.hpp
      include/lts5/${SUBSYS_NAME}/aabb_node.hpp
      include/lts5/${SUBSYS_NAME}/aabb_tree.hpp
      include/lts5/${SUBSYS_NAME}/aabc.hpp
      include/lts5/${SUBSYS_NAME}/ambient_occlusion.hpp
      include/lts5/${SUBSYS_NAME}/base_collision_tree.hpp
      include/lts5/${SUBSYS_NAME}/bivariate_quadratic_poly.hpp
      include/lts5/${SUBSYS_NAME}/bounding_sphere.hpp
      include/lts5/${SUBSYS_NAME}/discrete_remeshing.hpp
      include/lts5/${SUBSYS_NAME}/general_quadric.hpp
      include/lts5/${SUBSYS_NAME}/geometry.hpp
      include/lts5/${SUBSYS_NAME}/intersection_utils.hpp
      include/lts5/${SUBSYS_NAME}/isotropic_clustering_metric.hpp
      include/lts5/${SUBSYS_NAME}/kdtree.hpp
      include/lts5/${SUBSYS_NAME}/manifold_harmonic_transform.hpp
      include/lts5/${SUBSYS_NAME}/marching_cube.hpp
      include/lts5/${SUBSYS_NAME}/mesh.hpp
      include/lts5/${SUBSYS_NAME}/mesh_factory.hpp
      include/lts5/${SUBSYS_NAME}/mpu_node.hpp
      include/lts5/${SUBSYS_NAME}/multiscale_partition_unity.hpp
      include/lts5/${SUBSYS_NAME}/octree.hpp
      include/lts5/${SUBSYS_NAME}/octree_node.hpp
      include/lts5/${SUBSYS_NAME}/point_set.hpp
      include/lts5/${SUBSYS_NAME}/shape_estimator_base.hpp
      include/lts5/${SUBSYS_NAME}/spherical_harmonic.hpp
      include/lts5/${SUBSYS_NAME}/weight.hpp)
  set(python_files
      script/filter.py
      script/laplacian.py
      script/matcher.py
      script/metric.py
      script/registration.py
      script/selector.py)

  set(LIB_NAME "lts5_${SUBSYS_NAME}")
  # Add library
  LTS5_ADD_LIBRARY("${LIB_NAME}" "${SUBSYS_NAME}" ${srcs} ${ext_srcs} ${incs})
  TARGET_INCLUDE_DIRECTORIES(${LIB_NAME}
          PUBLIC
            $<INSTALL_INTERFACE:include>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
          PRIVATE
            ${CMAKE_CURRENT_SOURCE_DIR}/include
            ${inc_deps}
            ${LTS5_SOURCE_DIR}/3rdparty/ply
            $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
  TARGET_LINK_LIBRARIES(${LIB_NAME}
          PUBLIC
            lts5_utils ${OpenCV_LIBS}
          PRIVATE
            ply_helper)
  set(EXT_DEPS)
  # Work around to add external dependency build on the fly since no mechanism handle it in the libary
  ADD_DEPENDENCIES(${LIB_NAME} ply_helper)

  #EXAMPLES
  IF(OPT_BUILD_EXAMPLE)
    LTS5_ADD_EXAMPLE(lts5_test_quadric FILES test/test_estimator_quadric.cpp LINK_WITH lts5_utils lts5_geometry INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
    LTS5_ADD_EXAMPLE(lts5_test_ply FILES test/test_ply.cpp LINK_WITH lts5_geometry INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
    LTS5_ADD_EXAMPLE(lts5_factory FILES test/test_mesh_factory.cpp LINK_WITH lts5_geometry INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
    LTS5_ADD_EXAMPLE(lts5_get_mean_fwh FILES test/get_mean_fwh.cpp LINK_WITH lts5_geometry lts5_utils INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
    LTS5_ADD_EXAMPLE(lts5_discrete_remeshing FILES test/discrete_uniform_remeshing.cpp LINK_WITH lts5_geometry lts5_utils INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
    LTS5_ADD_EXAMPLE(lts5_ao_generator FILES test/ao_generator.cpp LINK_WITH lts5_geometry lts5_utils INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
  ENDIF(OPT_BUILD_EXAMPLE)

  #TESTS
  LTS5_ADD_TEST(octree_rnn lts5_test_octree_rnn FILES test/test_octree_rnn.cpp LINK_WITH lts5_geometry INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
  LTS5_ADD_TEST(shape_estimator lts5_test_shape_estimator FILES test/test_shape_estimator.cpp ARGUMENTS "-t" "${CMAKE_CURRENT_SOURCE_DIR}/test/" LINK_WITH lts5_geometry INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
  LTS5_ADD_TEST(mesh_method lts5_test_mesh_method FILES test/test_mesh_methods.cpp LINK_WITH lts5_geometry INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
  LTS5_ADD_TEST(mesh_factory_fwh lts5_test_mesh_factory_fwh FILES test/test_mesh_factory_fwh.cpp ARGUMENTS "-t" "${CMAKE_CURRENT_SOURCE_DIR}/test/" LINK_WITH lts5_geometry INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
  LTS5_ADD_TEST(halfedge_mesh lts5_test_halfedge_mesh FILES test/test_halfedge_mesh.cpp LINK_WITH lts5_geometry INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
  LTS5_ADD_TEST(kdtree lts5_test_kdtree FILES test/test_kdtree.cpp ARGUMENTS "--mesh" "${CMAKE_CURRENT_SOURCE_DIR}/test/bunny.ply" LINK_WITH lts5_geometry INC_FOLDER $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)

  # Python wrapper
  IF(OPT_PYTHON_WRAPPER)
    LTS5_ADD_PYTHON_WRAPPER("${SUBSYS_NAME}" "${SUBSYS_NAME}"
            FILES ${srcs_python}
            PYFILES ${python_files}
            INCS ${inc_deps} $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES> $<TARGET_PROPERTY:pyutils,INCLUDE_DIRECTORIES>
            LINK_WITH lts5_geometry lts5_python ${PYTHON_LIBRARY}
            PYFILES ${python_files})
  ENDIF(OPT_PYTHON_WRAPPER)

  # Install include files
  LTS5_ADD_INCLUDES("${SUBSYS_NAME}" "" ${incs_base})
  LTS5_ADD_INCLUDES("${SUBSYS_NAME}" "${SUBSYS_NAME}" ${incs})
endif(build)
