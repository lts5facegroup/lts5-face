/**
 *  @file   test_marching_cube.cpp
 *  @brief  Testing target for the MarchingCube class
 *
 *  @author Gabriel Cuendet
 *  @date   01.06.16
 *  Copyright © 2016 Gabriel Cuendet. All rights reserved.
 */

#include <limits>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/marching_cube.hpp"


std::string root_folder;

/**
 * @class   MarchingCubeTest
 * @brief   Test unit for MarchingCube class methods
 * @author  Gabriel Cuendet
 * @date    01/06/16
 */
class MarchingCubeTest : public ::testing::Test {

public:

  using Vertex = LTS5::Mesh<float>::Vertex;
  using Triangle = LTS5::Mesh<float>::Triangle;

  MarchingCubeTest(void) {

  }

protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

  LTS5::Mesh<float> mesh_;
};

/** Define unit test, MarchingCube */
TEST_F(MarchingCubeTest, GeneralQuadricApprox_Plane) {
  double eps = std::numeric_limits<float>::epsilon();

  EXPECT_FALSE(false);
}

int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Test folder path (folder containing the test meshes");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    parser.HasArgument("-t", &root_folder);
    // Init test
    ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
    // Run test
    return RUN_ALL_TESTS();
  } else {
    std::cout << "Unable to parse cmd line" << std::endl;
  }
  return err;
}
