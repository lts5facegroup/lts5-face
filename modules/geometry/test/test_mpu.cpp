/**
 *  @file   test_mpu.cpp
 *  @brief  Testing target for MPU class methods
 *  @author Gabriel Cuendet
 *  @date   25/05/16
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <limits>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/geometry/shape_estimator_base.hpp"
#include "lts5/geometry/general_quadric.hpp"
#include "lts5/geometry/bivariate_quadratic_poly.hpp"
#include "lts5/geometry/multiscale_partition_unity.hpp"


/**
 * @class   MPUTest
 * @brief   Test unit for MPU class methods
 * @author  Gabriel Cuendet
 * @date    25/05/16
 */
class MPUTest : public ::testing::Test {

public:

  using Vertex = LTS5::Mesh<float>::Vertex;
  using Triangle = LTS5::Mesh<float>::Triangle;

  MPUTest(void) {

  }

protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

  /**< Shape estimator used for the approximation */
  LTS5::ShapeEstimatorBase<float>* approx_;

};

/** Define unit test, GeneralQuadricApprox_Plane */
TEST_F(MPUTest, GeneralQuadricApprox_Plane) {
  double eps = std::numeric_limits<float>::epsilon();
  EXPECT_FALSE(false);
}

int main(int argc, const char * argv[]) {
  // Init test
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  // Run test
  return RUN_ALL_TESTS();
}
