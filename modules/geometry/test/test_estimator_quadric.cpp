/**
 *  @file   test_estimator_quadric.cpp
 *  @brief
 *
 *  @author Gabriel Cuendet
 *  @date   13/04/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <string>
#include <limits>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/aabc.hpp"
#include "lts5/geometry/general_quadric.hpp"
#include "lts5/geometry/marching_cube.hpp"
#include "lts5/geometry/bivariate_quadratic_poly.hpp"
#include "lts5/geometry/multiscale_partition_unity.hpp"

int main(int argc, const char * argv[]) {
  using CmdState = LTS5::CmdLineParser::ArgState;
  LTS5::CmdLineParser parser;
  std::string mesh_path;
  parser.AddArgument("-m", CmdState::kNeeded, "mesh filename");
  if (parser.ParseCmdLine(argc, argv)) {
    return -1;
  }

  parser.HasArgument("-m", &mesh_path);

  // Create mesh
  LTS5::Mesh<double>* mesh_ = new LTS5::Mesh<double>();
  mesh_->Load(mesh_path);
  mesh_->NormalizeMesh();

  double eps = std::numeric_limits<double>::epsilon();
  if (mesh_->CheckVertexDuplicates(eps)) {
    mesh_->FixTriangleSoup(eps);
  }

  mesh_->ComputeVertexNormal();
  //mesh_->Save("test_save.ply");

  // Linear octree with MPU nodes
  double max_error = 1e-4;
  int max_depth = 7;
  double alpha = 0.75;
  LTS5::AABC<double> domain(0.,0.,0.,0.,0.,0.);
  LTS5::MPU<double> mpu_tree(max_error, max_depth, alpha);
  mpu_tree.Build(*mesh_, domain);

  // Marching cube
  int max_level = 6;
  LTS5::MarchingCube<double> march(max_level);
  march.Init(mpu_tree);

  LTS5::Mesh<double> reconstruction;
  march.ComputeMesh(&reconstruction);
  reconstruction.Save("reconstruction.ply");

  return 0;
}
