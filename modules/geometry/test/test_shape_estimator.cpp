/**
 *  @file   test_shape_estimator.cpp
 *  @brief  Testing target for shape_estimator_base class and derived classes
 *  @author Gabriel Cuendet
 *  @date   25/05/16
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <limits>
#include <string>
#include <chrono>
#include <random>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/shape_estimator_base.hpp"
#include "lts5/geometry/general_quadric.hpp"
#include "lts5/geometry/bivariate_quadratic_poly.hpp"
#include "lts5/utils/math/constant.hpp"

std::string root_folder;
const double factor_eps(100); // Two orders of magnitude
const double factor_approx(15); // One order of magnitude

/**
 * @class   ShapeEstimatorTest
 * @brief   Test unit for Shape estimator classes methods
 * @author  Gabriel Cuendet
 * @date    25/05/16
 */
class ShapeEstimatorTest : public ::testing::Test {

public:

  using Vertex = LTS5::Mesh<float>::Vertex;
  using Triangle = LTS5::Mesh<float>::Triangle;

  ShapeEstimatorTest(void) : approx_(nullptr) {

  }

protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

  /**< Shape estimator used for the approximation */
  LTS5::ShapeEstimatorBase<float>* approx_;
  /**< Mesh to approximate */
  LTS5::Mesh<float> mesh_;
};

/** Define unit test, GeneralQuadricApprox_Plane */
TEST_F(ShapeEstimatorTest, GeneralQuadricApprox_Plane) {
  double tol = factor_eps * std::numeric_limits<float>::epsilon();

  int err = mesh_.Load(root_folder + "xy_plane.ply");
  ASSERT_EQ(err, 0) << "Error when loading the mesh (check the path)";
  mesh_.ComputeVertexNormal();

  LTS5::Quadric<float> approx;

  float epsilon = approx.Fit(mesh_.get_vertex(), mesh_.get_normal(),
                             mesh_.bcube());
  std::cout << "[   INFO   ] Fit approximant with accuracy epsilon = " << epsilon << std::endl;
  EXPECT_LE(epsilon, tol) <<
  "[  WARNING ] Approximation error epsilon is greater than two orders of magnitude above the precision";

  // Check results of the approximation
  // A_
  for (int i=0; i<9; ++i) {
    EXPECT_NEAR(approx.A().at<float>(i), 0.0, factor_approx * epsilon) <<
    "[  ERROR   ] An element of the matrix A is different than 0.0 (above tol)";
  }
  // b_
  EXPECT_NEAR(approx.b().x_, 0.0, factor_approx * epsilon) <<
  "[  ERROR   ] First element of the vector b is different than 0.0 (above tol)";
  EXPECT_NEAR(approx.b().y_, 0.0, factor_approx * epsilon) <<
  "[  ERROR   ] Second element of the vector b is different than 0.0 (above tol)";
  EXPECT_NEAR(approx.b().z_, 1.0, factor_approx * epsilon) <<
  "[  ERROR   ] Third element of the vector b is different than 0.0 (above tol)";

  // c_
  EXPECT_NEAR(approx.c(), -1.0, factor_approx * epsilon) <<
  "[  ERROR   ] c is different than 0.0 (above tol)";
}

/** Define unit test, BivariatePolyApprox_Plane */
TEST_F(ShapeEstimatorTest, BivariatePolyApprox_Plane) {
  double tol = factor_eps * std::numeric_limits<float>::epsilon();

  int err = mesh_.Load(root_folder + "xy_plane.ply");
  ASSERT_EQ(err, 0) << "Error when loading the mesh (check the path)";
  mesh_.ComputeVertexNormal();

  LTS5::BivariatePoly<float> approx;

  float epsilon = approx.Fit(mesh_.get_vertex(), mesh_.get_normal(),
                             mesh_.bcube());
  std::cout << "[   INFO   ] Fit approximant with accuracy epsilon = " << epsilon << std::endl;
  EXPECT_LE(epsilon, tol) <<
  "[  WARNING ] Approximation error epsilon is greater than two orders of magnitude above the precision";

  // Check results of the approximation
  // Q(x) = w - (Au^2 + 2Buv + Cv^2 + Du + Ev + F)
  // coefficients_
  EXPECT_NEAR(approx.coefficients()[0], 0.0, factor_approx * epsilon);  // A
  EXPECT_NEAR(approx.coefficients()[1], 0.0, factor_approx * epsilon); // B
  EXPECT_NEAR(approx.coefficients()[2], 0.0, factor_approx * epsilon); // C
  EXPECT_NEAR(approx.coefficients()[3], 0.0, factor_approx * epsilon); // D
  EXPECT_NEAR(approx.coefficients()[4], 0.0, factor_approx * epsilon); // E
  EXPECT_NEAR(approx.coefficients()[5], 0.0, factor_approx * epsilon); // F
}

/** Define unit test, GeneralQuadricApprox_TiltedPlane */
TEST_F(ShapeEstimatorTest, GeneralQuadricApprox_TiltedPlane) {
  double tol = factor_eps * std::numeric_limits<float>::epsilon();

  int err = mesh_.Load(root_folder + "tilted_xy_plane.ply");
  ASSERT_EQ(err, 0) << "Error when loading the mesh (check the path)";
  mesh_.ComputeVertexNormal();

  LTS5::Quadric<float> approx;

  float epsilon = approx.Fit(mesh_.get_vertex(), mesh_.get_normal(),
                             mesh_.bcube());
  std::cout << "[   INFO   ] Fit approximant with accuracy epsilon = " << epsilon << std::endl;
  EXPECT_LE(epsilon, tol) <<
  "[  WARNING ] Approximation error epsilon is greater than two orders of magnitude above the precision";

  // Check results of the approximation
  // A_
  for (int i=0; i<9; ++i) {
    EXPECT_NEAR(approx.A().at<float>(i), 0.0, factor_approx * epsilon) <<
    "[  ERROR   ] An element of the matrix A is different than 0.0 (above tol)";
  }
  // b_
  EXPECT_NEAR(approx.b().x_, 0.0, factor_approx * epsilon) <<
  "[  ERROR   ] First element of the vector b is different than 0.0 (above tol)";
  EXPECT_NEAR(approx.b().y_, -cos(LTS5::Constants<float>::PI_3), factor_approx * epsilon) <<
  "[  ERROR   ] Second element of the vector b is different than -cos(PI/3) (above tol)";
  EXPECT_NEAR(approx.b().z_, sin(LTS5::Constants<float>::PI_3), factor_approx * epsilon) <<
  "[  ERROR   ] Third element of the vector b is different than sin(PI/3) (above tol)";

  // c_
  EXPECT_NEAR(approx.c(), -0.616024, factor_approx * epsilon) <<
  "[  ERROR   ] c is different than -0.616024 (above tol)";
}

/** Define unit test, BivariatePolyApprox_TiltedPlane */
TEST_F(ShapeEstimatorTest, BivariatePolyApprox_TiltedPlane) {
  double tol = factor_eps * std::numeric_limits<float>::epsilon();

  int err = mesh_.Load(root_folder + "tilted_xy_plane.ply");
  ASSERT_EQ(err, 0) << "Error when loading the mesh (check the path)";
  mesh_.ComputeVertexNormal();

  // std::cout << mesh_.bcube() << std::endl;

  LTS5::BivariatePoly<float> approx;

  float epsilon = approx.Fit(mesh_.get_vertex(), mesh_.get_normal(),
                             mesh_.bcube());
  std::cout << "[   INFO   ] Fit approximant with accuracy epsilon = " << epsilon << std::endl;
  EXPECT_LE(epsilon, tol) <<
  "[  WARNING ] Approximation error epsilon is greater than two orders of magnitude above the precision";

  // Check results of the approximation
  // Q(x) = w - (Au^2 + 2Buv + Cv^2 + Du + Ev + F)
  // coefficients_
  EXPECT_NEAR(approx.coefficients()[0], 0.0, factor_approx * epsilon); // A
  EXPECT_NEAR(approx.coefficients()[1], 0.0, factor_approx * epsilon); // B
  EXPECT_NEAR(approx.coefficients()[2], 0.0, factor_approx * epsilon); // C
  EXPECT_NEAR(approx.coefficients()[3], 0.0, factor_approx * epsilon); // D
  EXPECT_NEAR(approx.coefficients()[4], 0.0, factor_approx * epsilon); // E
  EXPECT_NEAR(approx.coefficients()[5], 0.0, factor_approx * epsilon); // F
}

/** Define unit test, BivariatePolyApprox_CurvedPlane */
TEST_F(ShapeEstimatorTest, BivariatePolyApprox_CurvedPlane) {
  double tol = factor_eps * std::numeric_limits<float>::epsilon();

  int err = mesh_.Load(root_folder + "curved_xy_plane.ply");
  ASSERT_EQ(err, 0) << "Error when loading the mesh (check the path)";
  mesh_.ComputeVertexNormal();

  // std::cout << mesh_.bcube() << std::endl;

  LTS5::BivariatePoly<float> approx;

  float epsilon = approx.Fit(mesh_.get_vertex(), mesh_.get_normal(),
                             mesh_.bcube());
  std::cout << "[   INFO   ] Fit approximant with accuracy epsilon = " << epsilon << std::endl;
  EXPECT_LE(epsilon, tol) <<
                          "[  WARNING ] Approximation error epsilon is greater than two orders of magnitude above the precision";

  // Check results of the approximation
  // Q(x) = w - (Au^2 + 2Buv + Cv^2 + Du + Ev + F)
  // coefficients_
  EXPECT_NEAR(approx.coefficients()[0], 0.0, factor_approx * epsilon); // A
  EXPECT_NEAR(approx.coefficients()[1], 0.0, factor_approx * epsilon); // B
  EXPECT_NEAR(approx.coefficients()[2], -2.0, factor_approx * epsilon); // C
  EXPECT_NEAR(approx.coefficients()[3], 0.0, factor_approx * epsilon); // D
  EXPECT_NEAR(approx.coefficients()[4], 0.0, factor_approx * epsilon); // E
  EXPECT_NEAR(approx.coefficients()[5], 0.25, factor_approx * epsilon); // F
}

/** Define unit test, BivariatePolyEvaluation */
TEST_F(ShapeEstimatorTest, BivariatePolyEvaluation) {
  double tol = 10 * std::numeric_limits<float>::epsilon();

  int err = mesh_.Load(root_folder + "xy_plane.ply");
  ASSERT_EQ(err, 0) << "Error when loading the mesh (check the path)";
  mesh_.ComputeVertexNormal();

  LTS5::BivariatePoly<float> approx;

  float epsilon = approx.Fit(mesh_.get_vertex(), mesh_.get_normal(),
                             mesh_.bcube());
  std::cout << "[   INFO   ] Fit approximant with accuracy epsilon = " << epsilon << std::endl;

  float z_values[10] = {0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4};
  for (int i = 0; i < 10; ++i) {
    // In the center of the cell (0.5, 0.5, z)
    LTS5::Vector3<float> probe(0.5, 0.5, z_values[i]);  // SwQ/Sw should be equal to z-1
    float SwQ(0.0);
    float Sw(0.0);
    std::cout << "[   INFO   ] Evaluating (" << probe.x_ << ", " <<
    probe.y_ << ", " <<
    probe.z_ << ")" << std::endl;
    approx.Evaluate(probe, &SwQ, &Sw);
    EXPECT_NEAR(z_values[i] - 1, SwQ/Sw, tol);

    // Somewhere else
    // Create random generator
    long seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937_64 gen(seed);
    std::uniform_real_distribution<float> dist(0.f, 1.0f);
    float x = dist(gen);
    float y = dist(gen);
    LTS5::Vector3<float> random_probe(x, y, z_values[i]);  // SwQ/Sw should be equal to z-1
    SwQ = 0.0;
    Sw = 0.0;
    std::cout << "[   INFO   ] Evaluating (" << random_probe.x_ << ", " <<
    random_probe.y_ << ", " <<
    random_probe.z_ << ")" << std::endl;
    approx.Evaluate(probe, &SwQ, &Sw);
    EXPECT_NEAR(z_values[i] - 1, SwQ/Sw, tol);
  }
}

/** Define unit test, BivariatePolyEvaluation2 */
TEST_F(ShapeEstimatorTest, BivariatePolyEvaluation2) {
  double tol = 10 * std::numeric_limits<float>::epsilon();

  int err = mesh_.Load(root_folder + "tilted_xy_plane.ply");
  ASSERT_EQ(err, 0) << "Error when loading the mesh (check the path)";
  mesh_.ComputeVertexNormal();

  LTS5::BivariatePoly<float> approx;

  float epsilon = approx.Fit(mesh_.get_vertex(), mesh_.get_normal(),
                             mesh_.bcube());
  std::cout << "[   INFO   ] Fit approximant with accuracy epsilon = " << epsilon << std::endl;

  float z_values[10] = {0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4};

  // Origin of the plane
  LTS5::Vector3<float> orig(0.5, 0.5, 1.0);
  LTS5::Vector3<float> norm(0.0,
                            sin(LTS5::Constants<float>::PI_6),
                            cos(LTS5::Constants<float>::PI_6));

  for (int i = 0; i < 10; ++i) {
    // In the center of the cell (0.5, 0.5, z)
    LTS5::Vector3<float> probe(0.5, 0.5, z_values[i]);  // SwQ/Sw should be equal to z-1
    float SwQ(0.0);
    float Sw(0.0);
    std::cout << "[   INFO   ] Evaluating (" << probe.x_ << ", " <<
                                                probe.y_ << ", " <<
                                                probe.z_ << ")" << std::endl;
    approx.Evaluate(probe, &SwQ, &Sw);
    LTS5::Vector3<float> diff =  probe-orig;
    EXPECT_NEAR(norm.x_*diff.x_ + norm.y_*diff.y_ + norm.z_*diff.z_, SwQ/Sw, tol);

    // Somewhere else
    // Create random generator
    long seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937_64 gen(seed);
    std::uniform_real_distribution<float> dist(0.f, 1.0f);
    float x = dist(gen);
    float y = dist(gen);
    LTS5::Vector3<float> random_probe(x, y, z_values[i]);  // SwQ/Sw should be equal to z-1
    SwQ = 0.0;
    Sw = 0.0;
    std::cout << "[   INFO   ] Evaluating (" << random_probe.x_ << ", " <<
                                                random_probe.y_ << ", " <<
                                                random_probe.z_ << ")" << std::endl;
    approx.Evaluate(probe, &SwQ, &Sw);
    EXPECT_NEAR(norm.x_*diff.x_ + norm.y_*diff.y_ + norm.z_*diff.z_, SwQ/Sw, tol);
  }
}

int main(int argc, const char * argv[]) {

  for (int i = 0; i < argc; ++i) {
    std::cout << argv[i] << std::endl;
  }

  LTS5::CmdLineParser parser;
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Test folder path (folder containing the test meshes");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    parser.HasArgument("-t", &root_folder);

    // Init test
    ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
    // Run test
    return RUN_ALL_TESTS();
  } else {
    std::cout << "Unable to parse cmd line" << std::endl;
  }
  return err;
}
