/**
 *  @file   test_mesh_factory_fwh.cpp
 *  @brief  Testing target for MeshFactory class methods
 *  @author Marina Zimmermann
 *  @date   25/07/16
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */

#include <string>
#include <limits>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/mesh_factory.hpp"


std::string root_folder;
double eps = std::numeric_limits<float>::epsilon();

/**
 * @class   MeshFactoryTest
 * @brief   Test unit for MeshFactory class methods
 * @author  Marina Zimmermann
 * @date    25/07/16
 */
class MeshFactoryTest : public ::testing::Test {

public:

/*  using Vertex = LTS5::Mesh<float>::Vertex;
  using Triangle = LTS5::Mesh<float>::Triangle;


  MeshFactoryTest(void) : mesh_(nullptr) {
    // initialization code here
    std::vector<Vertex> vertex;

    vertex.push_back(Vertex(0.0, 0.0, 0.0)); // 0
    vertex.push_back(Vertex(2.0, 0.0, 0.0)); // 1
    vertex.push_back(Vertex(1.0, 2.0, 0.0)); // 2
    vertex.push_back(Vertex(2.0, 4.0, 0.0)); // 3
    vertex.push_back(Vertex(0.0, 4.0, 0.0)); // 4
    vertex.push_back(Vertex(3.0, 2.0, 0.0)); // 5
    vertex.push_back(Vertex(4.0, 0.0, 0.0)); // 6
    vertex.push_back(Vertex(5.0, 2.0, 0.0)); // 7
    vertex.push_back(Vertex(4.0, 4.0, 0.0)); // 8

    std::vector<Triangle> triangles;
    triangles.push_back(Triangle(0, 1, 5));
    triangles.push_back(Triangle(0, 5, 2));
    triangles.push_back(Triangle(1, 6, 7));
    triangles.push_back(Triangle(1, 7, 5));
    triangles.push_back(Triangle(2, 5, 3));
    triangles.push_back(Triangle(2, 3, 4));
    triangles.push_back(Triangle(5, 7, 8));
    triangles.push_back(Triangle(5, 8, 3));

    // Create mesh
    mesh_ = new LTS5::Mesh<float>();
    mesh_->set_vertex(vertex);
    mesh_->set_triangle(triangles);
  }*/

protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

  // put in any custom data members that you need
  LTS5::Mesh<float>* mesh_;

};

/** Define unit test, MeshFactoryReadFromObj */
TEST(MeshFactoryTest, MeshFactoryReadFromObj) {
  using Factory = LTS5::MeshFactory<float>;
  using Vertex = LTS5::Mesh<float>::Vertex;
  using Triangle = LTS5::Mesh<float>::Triangle;

  // initialization code here
  std::vector<Vertex> vertex;
  vertex.push_back(Vertex(0.f, 0.f, 0.f)); // 0
  vertex.push_back(Vertex(2.f, 0.f, 0.f)); // 1
  vertex.push_back(Vertex(1.f, 2.f, 0.f)); // 2
  vertex.push_back(Vertex(2.f, 4.f, 0.f)); // 3
  vertex.push_back(Vertex(0.f, 4.f, 0.f)); // 4
  vertex.push_back(Vertex(3.f, 2.f, 0.f)); // 5
  vertex.push_back(Vertex(4.f, 0.f, 0.f)); // 6
  vertex.push_back(Vertex(5.f, 2.f, 0.f)); // 7
  vertex.push_back(Vertex(4.f, 4.f, 0.f)); // 8

  std::vector<Triangle> triangles;
  triangles.push_back(Triangle(0, 1, 2));
  triangles.push_back(Triangle(1, 5, 2));
  triangles.push_back(Triangle(1, 6, 5));
  triangles.push_back(Triangle(6, 7, 5));
  triangles.push_back(Triangle(2, 5, 3));
  triangles.push_back(Triangle(2, 3, 4));
  triangles.push_back(Triangle(5, 7, 8));
  triangles.push_back(Triangle(5, 8, 3));

  // Create mesh
  LTS5::Mesh<float> mesh;
  LTS5::Mesh<float> ref_mesh;
  ref_mesh.set_vertex(vertex);
  ref_mesh.set_triangle(triangles);

  int err = Factory::LoadFWHObj(root_folder + "/test_quad_mesh.obj",
                                &mesh);
  EXPECT_FALSE(err);
  EXPECT_THAT(mesh.get_vertex(),
              ::testing::ContainerEq(ref_mesh.get_vertex()));
}

/** Define unit test, MeshFactoryReadShape0FromBs */
TEST(MeshFactoryTest, MeshFactoryReadShape0FromBs) {
  using Factory = LTS5::MeshFactory<float>;

  LTS5::Mesh<float> mesh_bs;
  LTS5::Mesh<float> mesh_obj;

  int err_bs = Factory::LoadSingleExprFromBs(root_folder + "/test_blendshape.bs",
                                             0,
                                             &mesh_bs);
  EXPECT_FALSE(err_bs);
  int err_obj = Factory::LoadFWHObj(root_folder + "/test_shape_0.obj",
                                    &mesh_obj);
  EXPECT_FALSE(err_obj);

  const auto& vertices_bs = mesh_bs.get_vertex();
  const auto& vertices_obj = mesh_obj.get_vertex();
  EXPECT_EQ(mesh_bs.size(), mesh_obj.size());

  const float eps = 5.f * std::numeric_limits<float>::epsilon();
  for (int i = 0; i < mesh_bs.size(); ++i) {
    EXPECT_NEAR(vertices_bs[i].x_, vertices_obj[i].x_, eps);
    EXPECT_NEAR(vertices_bs[i].y_, vertices_obj[i].y_, eps);
    EXPECT_NEAR(vertices_bs[i].z_, vertices_obj[i].z_, eps);
  }
}

/** Define unit test, MeshFactoryReadShape40FromBs */
TEST(MeshFactoryTest, MeshFactoryReadShape40FromBs) {
  using Factory = LTS5::MeshFactory<float>;

  LTS5::Mesh<float> mesh_bs;
  LTS5::Mesh<float> mesh_obj;

  int err_bs = Factory::LoadSingleExprFromBs(root_folder + "/test_blendshape.bs",
                                             40,
                                             &mesh_bs);
  EXPECT_FALSE(err_bs);
  int err_obj = Factory::LoadFWHObj(root_folder + "/test_shape_40.obj",
                                    &mesh_obj);
  EXPECT_FALSE(err_obj);

  const auto& vertices_bs = mesh_bs.get_vertex();
  const auto& vertices_obj = mesh_obj.get_vertex();
  EXPECT_EQ(mesh_bs.size(),mesh_obj.size());
  const float eps = 5.f * std::numeric_limits<float>::epsilon();
  for (int i = 0; i < mesh_bs.size(); ++i) {
    EXPECT_NEAR(vertices_bs[i].y_, vertices_obj[i].y_, eps);
    EXPECT_NEAR(vertices_bs[i].x_, vertices_obj[i].x_, eps);
    EXPECT_NEAR(vertices_bs[i].z_, vertices_obj[i].z_, eps);
  }
}

/** Define unit test, MeshFactoryComputeMean */
TEST(MeshFactoryTest, MeshFactoryComputeMean) {
  using Factory = LTS5::MeshFactory<float>;
  using Vertex = LTS5::Mesh<float>::Vertex;

  LTS5::Mesh<float> mesh0;
  auto& v0 = mesh0.get_vertex();
  v0.push_back(Vertex(0.f, 0.f, 0.f));
  v0.push_back(Vertex(0.f, 0.f, 1.f));
  v0.push_back(Vertex(1.f, 1.f, 1.f));
  v0.push_back(Vertex(1.f, 1.f, 0.f));
  LTS5::Mesh<float> mesh1;
  auto& v1 = mesh1.get_vertex();
  v1.push_back(Vertex(0.f, 1.f, 0.f));
  v1.push_back(Vertex(0.f, 1.f, 1.f));
  v1.push_back(Vertex(1.f, 0.f, 1.f));
  v1.push_back(Vertex(1.f, 0.f, 0.f));
  // Compute mean
  LTS5::Mesh<float> mean;
  std::vector<LTS5::Mesh<float>*> mesh = {&mesh0, &mesh1};
  Factory::ComputeMeanShape(mesh,&mean);
  // Compare
  const float r = 1.f / (2.f * std::sqrt(3.f));
  std::vector<Vertex> ref = {Vertex(-r, 0.f, -r),
                             Vertex(-r, 0.f, r),
                             Vertex(r, 0.f, r),
                             Vertex(r, 0.f, -r)};
  const auto& v = mean.get_vertex();
  EXPECT_THAT(v, ::testing::ElementsAreArray(ref));
}

int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Test folder path (folder containing the test meshes");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    parser.HasArgument("-t", &root_folder);

    // Init test
    ::testing::InitGoogleTest(&argc, const_cast<char **>(argv));
    // Run test
    return RUN_ALL_TESTS();
  }
  else {
    std::cout << "Unable to parse cmd line" << std::endl;
  }
  return err;
}
