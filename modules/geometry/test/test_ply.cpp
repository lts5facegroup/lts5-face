/**
 *  @file   test_ply.cpp
 *  @brief  Testing target for PLY
 *  @author Gabriel Cuendet
 *  @date   25/05/16
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <limits>

#include "lts5/geometry/mesh.hpp"

int main(int argc, const char * argv[]) {
  using Mesh = LTS5::Mesh<float>;
  Mesh mesh;
  auto& vert = mesh.get_vertex();
  auto& tri = mesh.get_triangle();
  auto& col = mesh.get_vertex_color();
  auto& nor = mesh.get_normal();

  vert.push_back(LTS5::Vector3<float>(0.f, 0.f, 0.f));
  vert.push_back(LTS5::Vector3<float>(0.f, 1.f, 0.f));
  vert.push_back(LTS5::Vector3<float>(1.f, 0.f, 0.f));
  nor.push_back(LTS5::Vector3<float>(1.f, 0.f, 0.f));
  nor.push_back(LTS5::Vector3<float>(1.f, 0.f, 0.f));
  nor.push_back(LTS5::Vector3<float>(1.f, 0.f, 0.f));

  col.push_back(LTS5::Vector4<float>(0.5f,0.5f,0.5f,1.f));
  col.push_back(LTS5::Vector4<float>(0.5f,0.5f,0.5f,1.f));
  col.push_back(LTS5::Vector4<float>(0.5f,0.5f,0.5f,1.f));

  tri.push_back(LTS5::Vector3<int>(0,1,2));

  std::vector<std::string>& comments = mesh.get_comments();
  comments.push_back("Saved by Gabriel");
  comments.push_back("Metadata no 1");
  comments.push_back("Metadata no 2");
  mesh.Save("toto.ply");

  LTS5::Mesh<double> d_mesh;
  d_mesh.Load("toto.ply");

  return 0;
}
