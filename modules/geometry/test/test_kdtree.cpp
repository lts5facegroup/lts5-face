/**
 *  @file   test_kdtree.cpp
 *  @brief  Test kdtree implementation
 *  @ingroup    geometry
 *
 *  @author Christophe Ecabert
 *  @date   14/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <limits>
#include <vector>
#include <algorithm>

#include "gtest/gtest.h"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/geometry/kdtree.hpp"
#include "lts5/geometry/mesh.hpp"

// Path to mesh file to load
static std::string mesh_filename;

using namespace LTS5;

template<int NDim, typename T>
struct TestParams {
  static constexpr int kDims() {return NDim;};
  using Type = T;
};

template<typename T>
class TestKDTree : public ::testing::Test {
 public:
  TestKDTree() = default;
  using DType = typename T::Type;
  using PtType = PointType<T::kDims(), DType>;
  using PointList = std::vector<PtType>;

  static void FindClosestPoint(const PointList& points,
                               const PtType& x,
                               PtType* closest_pt,
                               size_t* index) {
    auto min_dist = std::numeric_limits<DType>::max();
    for (size_t i = 0; i < points.size(); ++i) {
      const auto& pts = points[i];
      auto d = (pts - x).SquaredNorm();
      if (d < min_dist) {
        min_dist = d;
        *closest_pt = pts;
        *index = i;
      }
    }
  }

  static void FindNClosestPoint(const PointList& points,
                                const PtType& query,
                                size_t n,
                                PointList* closest_pt) {
    // Copy + Sort based on distance to query points
    PointList pts = points;
    std::sort(pts.begin(),
              pts.end(),
              [&](const PtType& x, const PtType& y) {
      return (query - x).SquaredNorm() < (query - y).SquaredNorm();
    });
    // Pick n first points
    n = std::min(n, pts.size());
    closest_pt->clear();
    for (size_t i = 0; i < n; ++i) {
      closest_pt->push_back(pts[i]);
    }
  }

};

// List of type to test against
using MyTestTypes = ::testing::Types<TestParams<3, float>,
                                     TestParams<3, double>> ;

// Instantiate test suite
TYPED_TEST_CASE(TestKDTree, MyTestTypes);

TYPED_TEST(TestKDTree, BuildTree) {
  using DType = typename TestFixture::DType;
  using Mesh = Mesh<DType>;
  using Tree = KDTree<TypeParam::kDims(), DType>;

  Mesh m;
  EXPECT_EQ(m.Load(mesh_filename), 0);
  // Build tree
  Tree tree;

  // Check properties on un-initialized tree
  EXPECT_TRUE(tree.Empty());
  EXPECT_EQ(tree.Size(), 0);
  // Build
  EXPECT_EQ(tree.Build(m.get_vertex()), 0);
  // Check properties on initialized tree
  EXPECT_FALSE(tree.Empty());
  EXPECT_EQ(tree.Size(), m.get_vertex().size());
}

TYPED_TEST(TestKDTree, SingleSearchTree) {
  using DType = typename TestFixture::DType;
  using PtType = typename TestFixture::PtType;
  using Mesh = Mesh<DType>;
  using Tree = KDTree<TypeParam::kDims(), DType>;

  Mesh m;
  EXPECT_EQ(m.Load(mesh_filename), 0);
  // Build tree
  Tree tree;
  EXPECT_EQ(tree.Build(m.get_vertex()), 0);

  { // Point not on mesh but close
    PtType query(0.0, 0.0, 0.0);
    PtType true_closest;
    size_t true_closest_idx;
    TestFixture::FindClosestPoint(m.get_vertex(), query,
                                  &true_closest, &true_closest_idx);

    auto pts = tree.FindNearest(query, 1);
    EXPECT_EQ(pts.size(), 1);
    EXPECT_EQ(pts[0], true_closest);
  }
  { // Point not on mesh but further away
    PtType query(10.0, 5.0, -5.0);
    PtType true_closest;
    size_t true_closest_idx;
    TestFixture::FindClosestPoint(m.get_vertex(), query,
                                  &true_closest, &true_closest_idx);

    auto pts = tree.FindNearest(query, 1);
    EXPECT_EQ(pts.size(), 1);
    EXPECT_EQ(pts[0], true_closest);
  }
  { // Point on mesh
    auto query = m.get_vertex()[0];
    auto pts = tree.FindNearest(query, 1);
    EXPECT_EQ(pts.size(), 1);
    EXPECT_EQ(pts[0], query);
  }
}

TYPED_TEST(TestKDTree, MultiSearchTree) {
  using DType = typename TestFixture::DType;
  using PtType = typename TestFixture::PtType;
  using Mesh = Mesh<DType>;
  using Tree = KDTree<TypeParam::kDims(), DType>;

  // Build tree
  Mesh m;
  EXPECT_EQ(m.Load(mesh_filename), 0);
  Tree tree;
  EXPECT_EQ(tree.Build(m.get_vertex()), 0);

  { // Point not on mesh but close
    PtType query(0.0, 0.0, 0.0);
    std::vector<PtType> true_closest;
    TestFixture::FindNClosestPoint(m.get_vertex(),
                                  query,
                                  2,
                                  &true_closest);
    auto pts = tree.FindNearest(query, 2);
    EXPECT_EQ(pts.size(), 2);
    EXPECT_EQ(pts[0], true_closest[0]);
    EXPECT_EQ(pts[1], true_closest[1]);
  }

  { // Point not on mesh but further away
    PtType query(10.0, 5.0, -5.0);
    std::vector<PtType> true_closest;
    TestFixture::FindNClosestPoint(m.get_vertex(),
                                   query,
                                   2,
                                   &true_closest);
    auto pts = tree.FindNearest(query, 2);
    EXPECT_EQ(pts.size(), 2);
    EXPECT_EQ(pts[0], true_closest[0]);
    EXPECT_EQ(pts[1], true_closest[1]);
  }

  { // Point on mesh
    auto query = m.get_vertex()[0];
    std::vector<PtType> true_closest;
    TestFixture::FindNClosestPoint(m.get_vertex(),
                                   query,
                                   2,
                                   &true_closest);
    auto pts = tree.FindNearest(query, 2);
    EXPECT_EQ(pts.size(), 2);
    EXPECT_EQ(pts[0], true_closest[0]);
    EXPECT_EQ(pts[1], true_closest[1]);
  }
}


int main(int argc, const char * argv[]) {

  CmdLineParser parser;
  parser.AddArgument("--mesh",
                     CmdLineParser::ArgState::kNeeded,
                     "Path to mesh file");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Get command line arguments
    parser.HasArgument("--mesh", &mesh_filename);

    // Init test
    ::testing::InitGoogleTest(&argc, const_cast<char **>(argv));
    // Run test
    return RUN_ALL_TESTS();
  }
}