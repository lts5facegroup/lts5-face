/**
 *  @file   test_ogl.cpp
 *  @brief  Testing target for ogl module
 *  @author Christophe Ecabert
 *  @date   13/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>
#include <iostream>
#include <vector>
#include <chrono>
#include <random>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/utils/time_lapse.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/octree.hpp"

typedef LTS5::Vector3<float> Vec3;


/**
 * @class   OCTreeRNNTest
 * @brief   Test unit for OCTree RaduisNearestNeighbor
 * @author  Christophe Ecabert
 * @date    05/05/16
 * @see http://www.ibm.com/developerworks/aix/library/au-googletestingframework.html
 */
class OCTreeRNNTest : public ::testing::TestWithParam<int> {

 public:

  using Vertex = LTS5::Mesh<float>::Vertex;

  /**
   * @name
   * @fn
   * @brief
   */
  OCTreeRNNTest(void) : n_pt(GetParam()), mesh_(nullptr), tree_(nullptr) {
    std::cout << "[          ] # vertices = " << n_pt * 8 << std::endl;

    // initialization code here
    // Create list of vertex randomly located
    std::vector<Vertex> vertex;

    // Create random generator
    long seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937_64 gen(seed);
    std::normal_distribution<float> dist(4.f, 0.1f); // u = 4.0, std = 0.1

    // Front/top/right
    for (int i = 0; i < n_pt; ++i) {
      Vertex vert(dist(gen), dist(gen), dist(gen));
      vertex.push_back(vert);
    }
    // Front/top/left
    for (int i = 0; i < n_pt; ++i) {
      Vertex vert(-dist(gen), dist(gen), dist(gen));
      vertex.push_back(vert);
    }
    // Front/bottom/left
    for (int i = 0; i < n_pt; ++i) {
      Vertex vert(-dist(gen), -dist(gen), dist(gen));
      vertex.push_back(vert);
    }
    // Front/bottom/right
    for (int i = 0; i < n_pt; ++i) {
      Vertex vert(dist(gen), -dist(gen), dist(gen));
      vertex.push_back(vert);
    }
    // Back/top/right
    for (int i = 0; i < n_pt; ++i) {
      Vertex vert(dist(gen), dist(gen), -dist(gen));
      vertex.push_back(vert);
    }
    // Back/top/left
    for (int i = 0; i < n_pt; ++i) {
      Vertex vert(-dist(gen), dist(gen), -dist(gen));
      vertex.push_back(vert);
    }
    // Back/bottom/left
    for (int i = 0; i < n_pt; ++i) {
      Vertex vert(-dist(gen), -dist(gen), -dist(gen));
      vertex.push_back(vert);
    }
    // Back/bottom/right
    for (int i = 0; i < n_pt; ++i) {
      Vertex vert(dist(gen), -dist(gen), -dist(gen));
      vertex.push_back(vert);
    }

    // Create mesh
    mesh_ = new LTS5::Mesh<float>();
    mesh_->set_vertex(vertex);

    // Create tree
    tree_ = new LTS5::OCTree<float>();
    tree_->Insert(*mesh_, LTS5::Mesh<float>::PrimitiveType::kPoint);
    auto start = std::chrono::system_clock::now();
    tree_->Build();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start);
    std::cout << "[          ] OCTree->Build()                 : " <<
    duration.count() << " µs"<< std::endl;
  }

  /**
   * @name
   * @fn
   * @brief
   */
  ~OCTreeRNNTest(void) {
    // cleanup any pending stuff, but no exceptions allowed
    if (mesh_) {
      delete mesh_;
      mesh_ = nullptr;
    }
    if (tree_) {
      delete tree_;
      tree_ = nullptr;
    }
  }

 protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

  // put in any custom data members that you need
  int n_pt;
  LTS5::Mesh<float>* mesh_;
  LTS5::OCTree<float>* tree_;
};

/** Define unit test, FrontTopRightQuery */
TEST_P(OCTreeRNNTest, FrontTopRightQuery) {
  // Test front/top/right corner
  // ---------------------------------------------------
  LTS5::Sphere<float> sph(Vec3(4.f, 4.f, 4.f), 2.f);
  std::vector<int> idx;
  auto start = std::chrono::system_clock::now();
  tree_->RadiusNearestNeighbor(sph, &idx);
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start);
  std::cout << "[          ] OCTree->RadiusNearestNeighbor() : " <<
  duration.count() << " µs" <<std::endl;
  // Idx should have 0-9
  // http://stackoverflow.com/questions/1460703/comparison-of-arrays-in-google-test
  EXPECT_THAT(idx, ::testing::Each(::testing::AllOf(::testing::Gt(-1),
                                                    ::testing::Lt(n_pt))));
}

/** Define unit test, FrontTopLeftQuery */
TEST_P(OCTreeRNNTest, FrontTopLeftQuery) {
  // Test front/top/left corner
  // ---------------------------------------------------
  LTS5::Sphere<float> sph(Vec3(-4.f, 4.f, 4.f), 2.f);
  std::vector<int> idx;
  auto start = std::chrono::system_clock::now();
  tree_->RadiusNearestNeighbor(sph, &idx);
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start);
  std::cout << "[          ] OCTree->RadiusNearestNeighbor() : " <<
  duration.count() << " µs" <<std::endl;
  // Idx should have 10-19
  EXPECT_THAT(idx, ::testing::Each(::testing::AllOf(::testing::Gt(n_pt-1),
                                                    ::testing::Lt(2*n_pt))));
}

/** Define unit test, FrontBottomLeftQuery */
TEST_P(OCTreeRNNTest, FrontBottomLeftQuery) {

  // Test front/bottom/left corner
  // ---------------------------------------------------
  LTS5::Sphere<float> sph(Vec3(-4.f, -4.f, 4.f), 2.f);
  std::vector<int> idx;
  auto start = std::chrono::system_clock::now();
  tree_->RadiusNearestNeighbor(sph, &idx);
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start);
  std::cout << "[          ] OCTree->RadiusNearestNeighbor() : " <<
  duration.count() << " µs" <<std::endl;
  // Idx should have 20-29
  EXPECT_THAT(idx, ::testing::Each(::testing::AllOf(::testing::Gt(2*n_pt-1),
                                                    ::testing::Lt(3*n_pt))));
}

/** Define unit test, FrontBottomRightQuery */
TEST_P(OCTreeRNNTest, FrontBottomRightQuery) {

  // Test front/bottom/right corner
  // ---------------------------------------------------
  LTS5::Sphere<float> sph(Vec3(4.f, -4.f, 4.f), 2.f);
  std::vector<int> idx;
  auto start = std::chrono::system_clock::now();
  tree_->RadiusNearestNeighbor(sph, &idx);
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start);
  std::cout << "[          ] OCTree->RadiusNearestNeighbor() : " <<
  duration.count() << " µs" <<std::endl;
  // Idx should have 30-39
  EXPECT_THAT(idx, ::testing::Each(::testing::AllOf(::testing::Gt(3*n_pt-1),
                                                    ::testing::Lt(4*n_pt))));
}

/** Define unit test, BackTopRightQuery */
TEST_P(OCTreeRNNTest, BackTopRightQuery) {
  // Test back/top/right corner
  // ---------------------------------------------------
  LTS5::Sphere<float> sph(Vec3(4.f, 4.f, -4.f), 2.f);
  std::vector<int> idx;
  auto start = std::chrono::system_clock::now();
  tree_->RadiusNearestNeighbor(sph, &idx);
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start);
  std::cout << "[          ] OCTree->RadiusNearestNeighbor() : " <<
  duration.count() << " µs" <<std::endl;
  // Idx should have 40-49
  EXPECT_THAT(idx, ::testing::Each(::testing::AllOf(::testing::Gt(4*n_pt-1),
                                                    ::testing::Lt(5*n_pt))));
}

/** Define unit test, BackTopLeftQuery */
TEST_P(OCTreeRNNTest, BackTopLeftQuery) {
  // Test back/top/left corner
  // ---------------------------------------------------
  LTS5::Sphere<float> sph(Vec3(-4.f, 4.f, -4.f), 2.f);
  std::vector<int> idx;
  auto start = std::chrono::system_clock::now();
  tree_->RadiusNearestNeighbor(sph, &idx);
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start);
  std::cout << "[          ] OCTree->RadiusNearestNeighbor() : " <<
  duration.count() << " µs" <<std::endl;
  // Idx should have 50-59
  EXPECT_THAT(idx, ::testing::Each(::testing::AllOf(::testing::Gt(5*n_pt-1),
                                                    ::testing::Lt(6*n_pt))));
}

/** Define unit test, BackBottomLeftQuery */
TEST_P(OCTreeRNNTest, BackBottomLeftQuery) {
  // Test back/bottom/left corner
  // ---------------------------------------------------
  LTS5::Sphere<float> sph(Vec3(-4.f, -4.f, -4.f), 2.f);
  std::vector<int> idx;
  auto start = std::chrono::system_clock::now();
  tree_->RadiusNearestNeighbor(sph, &idx);
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start);
  std::cout << "[          ] OCTree->RadiusNearestNeighbor() : " <<
  duration.count() << " µs" <<std::endl;
  // Idx should have 60-69
  EXPECT_THAT(idx, ::testing::Each(::testing::AllOf(::testing::Gt(6*n_pt-1),
                                                    ::testing::Lt(7*n_pt))));
}

/** Define unit test, BackBottomRightQuery */
TEST_P(OCTreeRNNTest, BackBottomRightQuery) {
  // Test back/bottom/right corner
  // ---------------------------------------------------
  LTS5::Sphere<float> sph(Vec3(4.f, -4.f, -4.f), 2.f);
  std::vector<int> idx;
  auto start = std::chrono::system_clock::now();
  tree_->RadiusNearestNeighbor(sph, &idx);
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start);
  std::cout << "[          ] OCTree->RadiusNearestNeighbor() : " <<
  duration.count() << " µs" <<std::endl;
  // Idx should have 70-79
  EXPECT_THAT(idx, ::testing::Each(::testing::AllOf(::testing::Gt(7*n_pt-1),
                                                    ::testing::Lt(8*n_pt))));
}

/** Define unit test, MiddleQuery */
TEST_P(OCTreeRNNTest, MiddleQuery) {
  // Test ouside
  // ---------------------------------------------------
  LTS5::Sphere<float> sph(Vec3(0.f, 0.f, 0.f), 2.f);
  std::vector<int> idx;
  auto start = std::chrono::system_clock::now();
  tree_->RadiusNearestNeighbor(sph, &idx);
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start);
  std::cout << "[          ] OCTree->RadiusNearestNeighbor() : " <<
  duration.count() << " µs" <<std::endl;
  // Idx should have 70-79
  EXPECT_EQ(idx.size(), 0);
}

INSTANTIATE_TEST_CASE_P(InstantiationName,
                        OCTreeRNNTest,
                        ::testing::Values(10,100,1000,10000));

int main(int argc, const char * argv[]) {
  // Init test
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  // Run test
  return RUN_ALL_TESTS();
}
