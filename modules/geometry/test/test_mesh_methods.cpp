/**
 *  @file   test_mesh_methods.cpp
 *  @brief  Testing target for mesh class methods
 *  @author Gabriel Cuendet
 *  @date   23/05/16
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <limits>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/geometry/mesh.hpp"

/**
 * @class   OCTreeRNNTest
 * @brief   Test unit for OCTree RaduisNearestNeighbor
 * @author  Christophe Ecabert
 * @date    05/05/16
 * @see http://www.ibm.com/developerworks/aix/library/au-googletestingframework.html
 */
class MeshTest : public ::testing::Test {

public:

  using Vertex = LTS5::Mesh<float>::Vertex;
  using Triangle = LTS5::Mesh<float>::Triangle;

  /**
   * @name
   * @fn
   * @brief
   */
  MeshTest(void) : mesh_(nullptr), mesh_duplicate_(nullptr) {
    // initialization code here
    std::vector<Vertex> vertex;

    vertex.push_back(Vertex(0.0, 0.0, 0.0)); // 0
    vertex.push_back(Vertex(2.0, 0.0, 0.0)); // 1
    vertex.push_back(Vertex(1.0, 2.0, 0.0)); // 2
    vertex.push_back(Vertex(2.0, 4.0, 0.0)); // 3
    vertex.push_back(Vertex(0.0, 4.0, 0.0)); // 4
    vertex.push_back(Vertex(3.0, 2.0, 0.0)); // 5
    vertex.push_back(Vertex(4.0, 0.0, 0.0)); // 6
    vertex.push_back(Vertex(5.0, 2.0, 0.0)); // 7
    vertex.push_back(Vertex(4.0, 4.0, 0.0)); // 8

    std::vector<Triangle> triangles;
    triangles.push_back(Triangle(0,1,2));
    triangles.push_back(Triangle(2,3,4));
    triangles.push_back(Triangle(1,5,2));
    triangles.push_back(Triangle(2,5,3));
    triangles.push_back(Triangle(1,6,5));
    triangles.push_back(Triangle(6,7,5));
    triangles.push_back(Triangle(5,8,3));
    triangles.push_back(Triangle(5,7,8));

    // Create mesh
    mesh_ = new LTS5::Mesh<float>();
    mesh_->set_vertex(vertex);
    mesh_->set_triangle(triangles);

    std::vector<Vertex> vertex_dupl;

    vertex_dupl.push_back(vertex[0]); // 0
    vertex_dupl.push_back(vertex[1]); // 1
    vertex_dupl.push_back(vertex[2]); // 2

    vertex_dupl.push_back(vertex[2]); // 2
    vertex_dupl.push_back(vertex[3]); // 3
    vertex_dupl.push_back(vertex[4]); // 4

    vertex_dupl.push_back(vertex[1]); // 1
    vertex_dupl.push_back(vertex[5]); // 5
    vertex_dupl.push_back(vertex[2]); // 2

    vertex_dupl.push_back(vertex[2]); // 2
    vertex_dupl.push_back(vertex[5]); // 5
    vertex_dupl.push_back(vertex[3]); // 3

    // Degenerate triangle
    vertex_dupl.push_back(vertex[1]);
    vertex_dupl.push_back(vertex[1]);
    vertex_dupl.push_back(vertex[5]);

    vertex_dupl.push_back(vertex[1]); // 1
    vertex_dupl.push_back(vertex[6]); // 6
    vertex_dupl.push_back(vertex[5]); // 5

    vertex_dupl.push_back(vertex[6]); // 6
    vertex_dupl.push_back(vertex[7]); // 7
    vertex_dupl.push_back(vertex[5]); // 5

    vertex_dupl.push_back(vertex[5]); // 5
    vertex_dupl.push_back(vertex[8]); // 8
    vertex_dupl.push_back(vertex[3]); // 3

    vertex_dupl.push_back(vertex[5]); // 5
    vertex_dupl.push_back(vertex[7]); // 7
    vertex_dupl.push_back(vertex[8]); // 8

    std::vector<Triangle> triangles_dupl;
    triangles_dupl.push_back(Triangle(0,1,2));
    triangles_dupl.push_back(Triangle(3,4,5));
    triangles_dupl.push_back(Triangle(6,7,8));
    triangles_dupl.push_back(Triangle(9,10,11));
    triangles_dupl.push_back(Triangle(12,13,14));
    triangles_dupl.push_back(Triangle(15,16,17));
    triangles_dupl.push_back(Triangle(18,19,20));
    triangles_dupl.push_back(Triangle(21,22,23));
    triangles_dupl.push_back(Triangle(24,25,26));

    // Create mesh
    mesh_duplicate_ = new LTS5::Mesh<float>();
    mesh_duplicate_->set_vertex(vertex_dupl);
    mesh_duplicate_->set_triangle(triangles_dupl);
  }

  /**
   * @name
   * @fn
   * @brief
   */
  ~MeshTest(void) {
    // cleanup any pending stuff, but no exceptions allowed
    if (mesh_) {
      delete mesh_;
      mesh_ = nullptr;
    }
    if (mesh_duplicate_) {
      delete mesh_duplicate_;
      mesh_duplicate_ = nullptr;
    }
  }

protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

  // put in any custom data members that you need
  LTS5::Mesh<float>* mesh_;
  LTS5::Mesh<float>* mesh_duplicate_;
  
};

/** Define unit test, NoDuplicatesInMesh */
TEST_F(MeshTest, NoDuplicatesInMesh) {
  double eps = std::numeric_limits<float>::epsilon();
  EXPECT_FALSE(mesh_->CheckVertexDuplicates(eps));
}

/** Define unit test, DuplicatesInMesh */
TEST_F(MeshTest, DuplicatesInMesh) {
  double eps = std::numeric_limits<float>::epsilon();
  EXPECT_TRUE(mesh_duplicate_->CheckVertexDuplicates(eps));
}

/** Define unit test, FixNoDuplicatesInMesh */
TEST_F(MeshTest, FixNoDuplicatesInMesh) {
  double eps = std::numeric_limits<float>::epsilon();
  LTS5::Mesh<float> *test_mesh = new LTS5::Mesh<float>();
  test_mesh->set_vertex(mesh_->get_vertex());
  test_mesh->set_triangle(mesh_->get_triangle());

  test_mesh->FixTriangleSoup(eps);
  EXPECT_THAT(test_mesh->get_vertex(),
              ::testing::ContainerEq(mesh_->get_vertex()));

  delete test_mesh;
}

/** Define unit test, FixDuplicatesInMesh */
TEST_F(MeshTest, FixDuplicatesInMesh) {
  double eps = std::numeric_limits<float>::epsilon();
  mesh_duplicate_->FixTriangleSoup(eps);
  EXPECT_THAT(mesh_->get_vertex(),
              ::testing::ContainerEq(mesh_duplicate_->get_vertex()));
}

/** Define unit test, ScaleMesh */
TEST_F(MeshTest, ScaleMesh) {
  mesh_->Scale(2);

  std::vector<Vertex> result;
  result.push_back(Vertex(-7/3.0, -2.0, 0.0)); // 0
  result.push_back(Vertex(5/3.0, -2.0, 0.0)); // 1
  result.push_back(Vertex(-1/3.0, 2.0, 0.0)); // 2
  result.push_back(Vertex(5/3.0, 6.0, 0.0)); // 3
  result.push_back(Vertex(-7/3.0, 6.0, 0.0)); // 4
  result.push_back(Vertex(11/3.0, 2.0, 0.0)); // 5
  result.push_back(Vertex(17/3.0, -2.0, 0.0)); // 6
  result.push_back(Vertex(23/3.0, 2.0, 0.0)); // 7
  result.push_back(Vertex(17/3.0, 6.0, 0.0)); // 8

  EXPECT_THAT(mesh_->get_vertex(),
              ::testing::ContainerEq(result));
}

/** Define unit test, NormalizeMesh */
TEST_F(MeshTest, DISABLED_NormalizeMesh) {
  mesh_->NormalizeMesh();
  std::vector<Vertex> vertices = mesh_->get_vertex();

  std::vector<Vertex> result;
  result.push_back(Vertex(0.0, 0.0, 0.0)); // 0
  result.push_back(Vertex(2.0, 0.0, 0.0)); // 1
  result.push_back(Vertex(1.0, 2.0, 0.0)); // 2
  result.push_back(Vertex(2.0, 4.0, 0.0)); // 3
  result.push_back(Vertex(0.0, 4.0, 0.0)); // 4
  result.push_back(Vertex(3.0, 2.0, 0.0)); // 5
  result.push_back(Vertex(4.0, 0.0, 0.0)); // 6
  result.push_back(Vertex(5.0, 2.0, 0.0)); // 7
  result.push_back(Vertex(4.0, 4.0, 0.0)); // 8

  LTS5::Vector3<float> cog(2.5, 2.0, 0.0);
  for (int i = 0; i < result.size(); ++i) {
    result[i] = result[i] - cog;
    result[i] *= float(1.0/(sqrt(3) * 5.0));

    EXPECT_NEAR(vertices[i].x_, result[i].x_, std::numeric_limits<float>::epsilon());
    EXPECT_NEAR(vertices[i].y_, result[i].y_, std::numeric_limits<float>::epsilon());
    EXPECT_NEAR(vertices[i].z_, result[i].z_, std::numeric_limits<float>::epsilon());
  }
}


int main(int argc, const char * argv[]) {
  // Init test
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  // Run test
  return RUN_ALL_TESTS();
}
