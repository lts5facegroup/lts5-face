/**
 *  @file   test_halfedge_mesh.cpp
 *  @brief
 *
 *  @author Gabriel Cuendet
 *  @date   17.08.16
 *  Copyright © 2016 Gabriel Cuendet. All rights reserved.
 */

#include <limits>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Eigen/IterativeLinearSolvers"

#include "lts5/geometry/mesh.hpp"

/**
 * @class   HalfedgeMeshTest
 * @brief   Test unit for HalfedgeMesh
 * @author  Gabriel Cuendet
 * @date    17/08/16
 * @see http://www.ibm.com/developerworks/aix/library/au-googletestingframework.html
 */
class HalfedgeMeshTest : public ::testing::Test {

public:

  using Vertex = LTS5::Mesh<float>::Vertex;
  using Triangle = LTS5::Mesh<float>::Triangle;

  using Halfedge = LTS5::Mesh<float>::Halfedge;

  /**
   * @name
   * @fn
   * @brief
   */
  HalfedgeMeshTest(void) {
    // initialization code here
    std::vector<Vertex> vertex;
    vertex.push_back(Vertex(0.0, 0.0, 0.0)); // 0
    vertex.push_back(Vertex(2.0, 0.0, 0.0)); // 1
    vertex.push_back(Vertex(1.0, 2.0, 0.0)); // 2
    vertex.push_back(Vertex(2.0, 4.0, 0.0)); // 3
    vertex.push_back(Vertex(0.0, 4.0, 0.0)); // 4
    vertex.push_back(Vertex(3.0, 2.0, 0.0)); // 5
    vertex.push_back(Vertex(4.0, 0.0, 0.0)); // 6
    vertex.push_back(Vertex(5.0, 2.0, 0.0)); // 7
    vertex.push_back(Vertex(4.0, 4.0, 0.0)); // 8
    std::vector<Triangle> triangles;
    triangles.push_back(Triangle(0,1,2));
    triangles.push_back(Triangle(1,5,2));
    triangles.push_back(Triangle(1,6,5));
    triangles.push_back(Triangle(6,7,5));
    triangles.push_back(Triangle(5,7,8));
    triangles.push_back(Triangle(5,8,3));
    triangles.push_back(Triangle(2,5,3));
    triangles.push_back(Triangle(2,3,4));

    // Create mesh
    he_mesh_ = new LTS5::Mesh<float>();
    he_mesh_->set_vertex(vertex);
    he_mesh_->set_triangle(triangles);
    he_mesh_->ComputeHalfedges();
  }

  /**
   * @name
   * @fn
   * @brief
   */
  ~HalfedgeMeshTest(void) {
    // cleanup any pending stuff, but no exceptions allowed
    if (he_mesh_) {
      delete he_mesh_;
      he_mesh_ = nullptr;
    }
  }

  // put in any custom data members that you need
  LTS5::Mesh<float>* he_mesh_;

protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }
};

/** Define unit test, IsBoundary */
TEST_F(HalfedgeMeshTest, IsBoundary) {
  const std::vector<Halfedge>& halfedges = he_mesh_->get_halfedges();

  EXPECT_TRUE(halfedges[0].IsBoundary());
  EXPECT_FALSE(halfedges[1].IsBoundary());
  EXPECT_TRUE(halfedges[2].IsBoundary());
}

/** Define unit test, Vertex_halfedges */
TEST_F(HalfedgeMeshTest, Vertex_halfedges) {
  const std::vector<size_t>& v_he_ref = he_mesh_->get_vertex_halfedges();

  EXPECT_TRUE(v_he_ref[0] == 0);
}

/** Define unit test, Vertex_OneRingN */
TEST_F(HalfedgeMeshTest, Vertex_OneRingN) {
  std::vector<size_t> one_ring;
  bool complete_ring = he_mesh_->OneRingNeighbors(5, &one_ring);

//  const std::vector<Vertex>& vert = he_mesh.get_vertex();
//  for (int i = 0; i < one_ring.size(); ++i) {
//    std::cout << "(" << vert[one_ring[i]].x_ << ", " << vert[one_ring[i]].y_ << ", " << vert[one_ring[i]].z_ << ")" << std::endl;
//  }

  EXPECT_TRUE(one_ring.size() == 6);
  EXPECT_TRUE(complete_ring);
}

/** Define unit test, Vertex_OneRingNIncomplete */
TEST_F(HalfedgeMeshTest, Vertex_OneRingNIncomplete) {
  std::vector<size_t> one_ring;
  bool complete_ring = he_mesh_->OneRingNeighbors(3, &one_ring);

//  const std::vector<Vertex>& vert = he_mesh.get_vertex();
//  for (int i = 0; i < one_ring.size(); ++i) {
//    std::cout << "(" << vert[one_ring[i]].x_ << ", " << vert[one_ring[i]].y_ << ", " << vert[one_ring[i]].z_ << ")" << std::endl;
//  }

  EXPECT_TRUE(one_ring.size() == 4);
  EXPECT_FALSE(complete_ring);
}

/** Define unit test, Vertex_UniformLaplacian */
TEST_F(HalfedgeMeshTest, Vertex_UniformLaplacian) {
  int n = he_mesh_->get_vertex().size();

  Eigen::SparseMatrix<float> M(n, n);
  Eigen::SparseMatrix<float> D(n, n);
  he_mesh_->ComputeUniformLaplacian(&D, &M);

  std::cout << M << std::endl;
  std::cout << D << std::endl;

  // Ground truth
  Eigen::SparseMatrix<float> M_true(n, n);
  std::vector<Eigen::Triplet<float> > M_true_vec{
    Eigen::Triplet<float>(0,0,-2),
    Eigen::Triplet<float>(0,1,1),
    Eigen::Triplet<float>(0,2,1),
    Eigen::Triplet<float>(1,0,1),
    Eigen::Triplet<float>(1,1,-4),
    Eigen::Triplet<float>(1,2,1),
    Eigen::Triplet<float>(1,5,1),
    Eigen::Triplet<float>(1,6,1),
    Eigen::Triplet<float>(2,0,1),
    Eigen::Triplet<float>(2,1,1),
    Eigen::Triplet<float>(2,2,-5),
    Eigen::Triplet<float>(2,3,1),
    Eigen::Triplet<float>(2,4,1),
    Eigen::Triplet<float>(2,5,1),
    Eigen::Triplet<float>(3,2,1),
    Eigen::Triplet<float>(3,3,-4),
    Eigen::Triplet<float>(3,4,1),
    Eigen::Triplet<float>(3,5,1),
    Eigen::Triplet<float>(3,8,1),
    Eigen::Triplet<float>(4,2,1),
    Eigen::Triplet<float>(4,3,1),
    Eigen::Triplet<float>(4,4,-2),
    Eigen::Triplet<float>(5,1,1),
    Eigen::Triplet<float>(5,2,1),
    Eigen::Triplet<float>(5,3,1),
    Eigen::Triplet<float>(5,5,-6),
    Eigen::Triplet<float>(5,6,1),
    Eigen::Triplet<float>(5,7,1),
    Eigen::Triplet<float>(5,8,1),
    Eigen::Triplet<float>(6,1,1),
    Eigen::Triplet<float>(6,5,1),
    Eigen::Triplet<float>(6,6,-3),
    Eigen::Triplet<float>(6,7,1),
    Eigen::Triplet<float>(7,5,1),
    Eigen::Triplet<float>(7,6,1),
    Eigen::Triplet<float>(7,7,-3),
    Eigen::Triplet<float>(7,8,1),
    Eigen::Triplet<float>(8,3,1),
    Eigen::Triplet<float>(8,5,1),
    Eigen::Triplet<float>(8,7,1),
    Eigen::Triplet<float>(8,8,-3)};
  Eigen::SparseMatrix<float> D_true(n, n);
  std::vector<Eigen::Triplet<float> > D_true_vec{
    Eigen::Triplet<float>(0,0,0.5),
    Eigen::Triplet<float>(1,1,0.25),
    Eigen::Triplet<float>(2,2,0.2),
    Eigen::Triplet<float>(3,3,0.25),
    Eigen::Triplet<float>(4,4,0.5),
    Eigen::Triplet<float>(5,5,0.166667),
    Eigen::Triplet<float>(6,6,0.333333),
    Eigen::Triplet<float>(7,7,0.333333),
    Eigen::Triplet<float>(8,8,0.333333),
  };

  M_true.setFromTriplets(M_true_vec.begin(), M_true_vec.end());
  D_true.setFromTriplets(D_true_vec.begin(), D_true_vec.end());
  EXPECT_TRUE(M.isApprox(M_true));
  EXPECT_TRUE(D.isApprox(D_true));
}

/** Define unit test, Vertex_UniformLaplacian */
TEST_F(HalfedgeMeshTest, Vertex_CotanLaplacian) {
  int n = he_mesh_->get_vertex().size();

  Eigen::SparseMatrix<float> M(n, n);
  Eigen::SparseMatrix<float> D(n, n);
  he_mesh_->ComputeCotanLaplacian(&D, &M);

  std::cout << M << std::endl;
  std::cout << D << std::endl;

  // Ground truth
  Eigen::SparseMatrix<float> M_true(n, n);
  std::vector<Eigen::Triplet<float> > M_true_vec{
    Eigen::Triplet<float>(0,0,-1.25),
    Eigen::Triplet<float>(0,1,0.75),
    Eigen::Triplet<float>(0,2,0.5),
    Eigen::Triplet<float>(1,0,0.75),
    Eigen::Triplet<float>(1,1,-2.5),
    Eigen::Triplet<float>(1,2,0.5),
    Eigen::Triplet<float>(1,5,0.5),
    Eigen::Triplet<float>(1,6,0.75),
    Eigen::Triplet<float>(2,0,0.5),
    Eigen::Triplet<float>(2,1,0.5),
    Eigen::Triplet<float>(2,2,-2.75),
    Eigen::Triplet<float>(2,3,0.5),
    Eigen::Triplet<float>(2,4,0.5),
    Eigen::Triplet<float>(2,5,0.75),
    Eigen::Triplet<float>(3,2,0.5),
    Eigen::Triplet<float>(3,3,-2.5),
    Eigen::Triplet<float>(3,4,0.75),
    Eigen::Triplet<float>(3,5,0.5),
    Eigen::Triplet<float>(3,8,0.75),
    Eigen::Triplet<float>(4,2,0.5),
    Eigen::Triplet<float>(4,3,0.75),
    Eigen::Triplet<float>(4,4,-1.25),
    Eigen::Triplet<float>(5,1,0.5),
    Eigen::Triplet<float>(5,2,0.75),
    Eigen::Triplet<float>(5,3,0.5),
    Eigen::Triplet<float>(5,5,-3.5),
    Eigen::Triplet<float>(5,6,0.5),
    Eigen::Triplet<float>(5,7,0.75),
    Eigen::Triplet<float>(5,8,0.5),
    Eigen::Triplet<float>(6,1,0.75),
    Eigen::Triplet<float>(6,5,0.5),
    Eigen::Triplet<float>(6,6,-1.75),
    Eigen::Triplet<float>(6,7,0.5),
    Eigen::Triplet<float>(7,5,0.75),
    Eigen::Triplet<float>(7,6,0.5),
    Eigen::Triplet<float>(7,7,-1.75),
    Eigen::Triplet<float>(7,8,0.5),
    Eigen::Triplet<float>(8,3,0.75),
    Eigen::Triplet<float>(8,5,0.5),
    Eigen::Triplet<float>(8,7,0.5),
    Eigen::Triplet<float>(8,8,-1.75)};
  Eigen::SparseMatrix<float> D_true(n, n);
  std::vector<Eigen::Triplet<float> > D_true_vec{
    Eigen::Triplet<float>(0,0,2/3.f),
    Eigen::Triplet<float>(1,1,2.f),
    Eigen::Triplet<float>(2,2,8/3.f),
    Eigen::Triplet<float>(3,3,2),
    Eigen::Triplet<float>(4,4,2/3.f),
    Eigen::Triplet<float>(5,5,4),
    Eigen::Triplet<float>(6,6,4/3.f),
    Eigen::Triplet<float>(7,7,4/3.f),
    Eigen::Triplet<float>(8,8,4/3.f),
  };

  M_true.setFromTriplets(M_true_vec.begin(), M_true_vec.end());
  D_true.setFromTriplets(D_true_vec.begin(), D_true_vec.end());

  std::cout << D_true << std::endl;
  EXPECT_TRUE(M.isApprox(M_true));
  EXPECT_TRUE(D.isApprox(D_true));
}

int main(int argc, const char * argv[]) {
  // Init test
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  // Run test
  return RUN_ALL_TESTS();
}
