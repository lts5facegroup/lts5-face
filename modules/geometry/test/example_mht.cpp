/**
 *  @file   example_mht.cpp
 *  @brief  Example of Manifold Harmonic Transform Computation
 *
 *  @author Gabriel Cuendet
 *  @date   2016/09/26
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <string>
#include <fstream>
#include <iostream>

#include "Eigen/Dense"
#include "Eigen/Sparse"

#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/halfedge_mesh.hpp"
#include "lts5/geometry/manifold_harmonic_transform.hpp"
#include "lts5/utils/cmd_parser.hpp"

int main(int argc, const char * argv[]) {
  std::string mesh_filepath;
  std::string basis_filepath;

  LTS5::CmdLineParser parser;
  parser.AddArgument("m", LTS5::CmdLineParser::ArgState::kNeeded, "Mesh");
  parser.AddArgument("b", LTS5::CmdLineParser::ArgState::kNeeded, "Basis");

  int error = parser.ParseCmdLine(argc, argv);
  if (!error) {
    parser.HasArgument("-m", &mesh_filepath);
    parser.HasArgument("-b", &basis_filepath);
  }

  LTS5::Mesh<double> mesh(mesh_filepath);
  mesh.NormalizeMesh();
  LTS5::HalfedgeMesh<double> he_mesh(mesh);

  int n = static_cast<int>(he_mesh.size());

  Eigen::SparseMatrix<double> Q(n,n);
  Eigen::SparseMatrix<double> D(n,n);
  he_mesh.ComputeCotanLaplacian(&D, &Q);

  // Compute basis
  LTS5::MHT<double> transform;
  //transform.Init(Q, D, n-1);
  //transform.Write(basis_filepath);

  transform.Read(basis_filepath);
  std::vector<LTS5::Vector3<double> >& v_spat = mesh.get_vertex();
  std::vector<LTS5::Vector3<double> > v_freq;
  transform.Transform(v_spat, &v_freq);

  // Write to csv file: lambda,norm()
  std::ofstream csv_file;
  csv_file.open ("spectral_coeffs.csv");
  const Eigen::Matrix<double, Eigen::Dynamic, 1>& eigenvalues = transform.get_eigenvalues();

  for (int i = 0; i < v_freq.size(); ++i) {
    csv_file << eigenvalues[i] << ", " << v_freq[i].Norm() << "\n";
  }
  csv_file.close();

}
