/**
 *  @file   test_ao_generator.cpp
 *  @brief  Test target for ambient occlusion generator
 *  @ingroup    geometry
 *
 *  @author Christophe Ecabert
 *  @date   2/10/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include "lts5/utils/logger.hpp"
#include "lts5/utils/cmd_parser.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/ambient_occlusion.hpp"
#include "lts5/utils/colormap.hpp"
#include "lts5/utils/string_util.hpp"

int main(int argc, const char * argv[]) {
  using namespace LTS5;
  using T = float;
  using Mesh_t = Mesh<T, Vector3<T>>;
  using Color = typename Mesh_t::Color;
  using AOGenerator = AmbientOcclusionGenerator<T>;
  using CMap = ColorMap<T>;

  CmdLineParser parser;
  parser.AddArgument("-m",
                     CmdLineParser::kNeeded,
                     "Mesh file to compute ambient occlusion for");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    std::string mesh_file;
    parser.HasArgument("-m", &mesh_file);

    // Load mesh
    Mesh_t mesh(mesh_file);
    AOGenerator generator(mesh);
    generator.Generate(5000);

    const auto& ao = generator.get_ao();
    CMap cm(CMap::kGrayscale, T(0.0), T(1.0));
    std::vector<Color> colors;
    for (int i = 0; i < ao.rows; ++i) {
      Color c;
      cm.PickColor(ao.at<T>(i), &c);
      colors.push_back(c);
    }
    mesh.set_vertex_color(colors);
    // Dump mesh + ao
    std::string folder, file, ext;
    ExtractDirectory(mesh_file, &folder, &file, &ext);
    file += "_ao_cpu";
    ext = ".ply";
    mesh.Save(Path::Join(folder, file + ext));
  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
}
