/**
 *  @file   test_mesh_factory.cpp
 *  @brief  Teest target for mesh factory
 *
 *  @author Christophe Ecabert
 *  @date   18/07/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#include "lts5/geometry/mesh_factory.hpp"
#include "lts5/utils/cmd_parser.hpp"


int main(int argc, const char * argv[]) {
  /** Mesh Factory */
  using MeshFactory = LTS5::MeshFactory<double>;
  /** Sphere parameters */
  using SphereParameters = LTS5::MeshFactory<double>::SphereParameters;
  /** Rectangle parameters */
  using CuboidParameters = LTS5::MeshFactory<double>::CuboidParameters;
  /** Plane parameters */
  using PlaneParameters = LTS5::MeshFactory<double>::PlaneParameters;

  /** Mesh */
  LTS5::Mesh<double>* mesh = new LTS5::Mesh<double>();

  LTS5::CmdLineParser parser;
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "type of mesh : (s)phere, (r)ectangle, (p)lane");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    std::string type;
    parser.HasArgument("-t", &type);
    // Select proper type
    switch (type[0]) {
      // Sphere
      case 's' : {
        // Sphere parameters
        SphereParameters sph_param;
        sph_param.theta_min = LTS5::Constants<double>::PI_4;
        sph_param.theta_max = 3.0 * LTS5::Constants<double>::PI_4;
        sph_param.theta_step = 20;
        sph_param.phi_min = -LTS5::Constants<double>::PI_2;
        sph_param.phi_max = LTS5::Constants<double>::PI_2;
        sph_param.phi_step = 10;
        // Generate Sphere
        MeshFactory::GenerateSphere(&sph_param, mesh);
        // Save
        mesh->Save("sphere.obj");
      }
        break;
      // Rectangle
      case 'r' : {
        // Rectangle parameters
        CuboidParameters rect_param;
        rect_param.height_step = 15;
        rect_param.width_step = 25;
        rect_param.depth_step = 35;
        // Generate Rectangle
        MeshFactory::GenerateCuboid(&rect_param, mesh);
        // Save
        mesh->Save("rectangle.ply");
      }
        break;

      // Plane
      case 'p' : {
        // Rectangle parameters
        PlaneParameters plane_param;
        plane_param.u_step = 10;
        plane_param.v_step = 5;
        // Generate Plane
        MeshFactory::GeneratePlane(&plane_param, mesh);
        // Save
        mesh->Save("plane.ply");
      }
        break;

      default: std::cout << "Error, unknown type" << std::endl;
        break;
    }
  } else {
    std::cout << "Unable to parse command line" << std::endl;
  }
  return err;
}