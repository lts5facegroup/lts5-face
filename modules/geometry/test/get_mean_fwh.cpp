/**
 *  @file   get_mean_fwh.cpp
 *  @brief  Compute mean expression from FWH data
 *
 *  @author Marina Zimmermann
 *  @date   14/09/2016
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */

#include <iostream>
#include <sstream>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/mesh_factory.hpp"

int main(int argc, const char * argv[]) {
  int error = -1;

  LTS5::CmdLineParser parser;

  parser.AddArgument("-i", LTS5::CmdLineParser::ArgState::kNeeded,
                     "Path to the input folder (i.e. folder containing FW)");
  parser.AddArgument("-o", LTS5::CmdLineParser::ArgState::kNeeded,
                     "Path to the output folder (i.e. where to save the meanshapes");
  if (parser.ParseCmdLine(argc, argv)) {
    return -1;
  }

  std::string root_dir;  // "/media/marina/backupdata/data/face/FaceWarehouse/"
  parser.HasArgument("-i", &root_dir);
  std::string output_dir; // "/media/marina/backupdata/data/face/FaceWarehouse/"
  parser.HasArgument("-o", &output_dir);

  // Load the quads once
  std::vector<LTS5::Vector4<int> > quads;
  LTS5::MeshFactory<float>::LoadQuads(root_dir + "Tester_1/Blendshape/shape_0.obj",
                                      &quads);

  // Then load every expression for every subject from the binary blendshape file
  int no_subjects = 150;

  for (int expression = 0; expression < 47; expression++) {

    std::vector<LTS5::Mesh<float>* > mesh_vector;
    LTS5::Mesh<float> *mean_mesh = new LTS5::Mesh<float>();

    mesh_vector.resize(no_subjects);

    // Load a given expression from BS binary file for all subject
    for (int i = 0; i < no_subjects; ++i) {
      mesh_vector[i] = new LTS5::Mesh<float>();
      std::string folder = "Tester_" + std::to_string(i + 1);
      std::string filename = root_dir + folder +
                             "/Blendshape/shape.bs";

      error = LTS5::MeshFactory<float>::LoadSingleExprFromBs(filename,
                                                             expression,
                                                             mesh_vector[i]);
    }

    if (!error) {
      LTS5::MeshFactory<float>::ComputeMeanShape(mesh_vector, mean_mesh);
      LTS5::MeshFactory<float>::DivideQuadIntoTri(quads, mean_mesh);

      std::string filepath =  output_dir + "mean_shape_" + std::to_string(expression) + ".ply";
      error = mean_mesh->Save(filepath);
    }

    for (auto ptr : mesh_vector) {
      delete ptr;
      ptr = nullptr;
    }
    delete mean_mesh;
  }

  return error;
}
