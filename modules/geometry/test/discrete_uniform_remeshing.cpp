/**
 *  @file   uniform_remeshing.cpp
 *  @brief  Example how to use uniform remeshing
 *
 *  @author Christophe Ecabert
 *  @date   15/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/discrete_remeshing.hpp"

using ArgState = LTS5::CmdLineParser::ArgState;
using Mesh = LTS5::Mesh<double>;
using DiscreteRemesher = LTS5::IsotropicDiscreteRemesher<double>;

int main(int argc, const char * argv[]) {
  // User friendly command line input
  LTS5::CmdLineParser parser;
  parser.AddArgument("-m",
                     ArgState::kNeeded,
                     "Mesh to resample");
  parser.AddArgument("-c",
                     ArgState::kNeeded,
                     "Number of vertex wanted");
  parser.AddArgument("-o",
                     ArgState::kNeeded,
                     "Location where to save the output");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    std::string input_path;
    std::string output_path;
    std::string n_vertex_str;
    parser.HasArgument("-m", &input_path);
    parser.HasArgument("-c", &n_vertex_str);
    parser.HasArgument("-o", &output_path);

    // Load mesh
    Mesh* input = new Mesh();
    err = input->Load(input_path);
    if (!err) {
      int n_cluster = std::atoi(n_vertex_str.c_str());
      // Output
      Mesh* output = new Mesh();
      // Remesher
      DiscreteRemesher* remesher = new DiscreteRemesher();
      remesher->set_log(true);
      err = remesher->Initialize(input, n_cluster);
      if (!err) {
        // Process
        remesher->Process(output);
        // Save
        err = output->Save(output_path);
        if (!err) {
          std::cout << "Done, save into file : " << output_path << std::endl;
        } else {
          std::cout << "Error, can not save in file : " << output_path;
          std::cout << std::endl;
        }
      } else {
        std::cout << "Unable to initialize remesher" << std::endl;
      }
      delete output;
      delete remesher;
    } else {
      std::cout << "Unable to load mesh : " << input_path << std::endl;
    }
    // clean up
    delete input;
  } else {
    std::cout << "Unable to parse command line !" << std::endl;
    parser.PrintHelp();
  }
  return err;
}
