/**
 *  @file   test_mht.cpp
 *  @brief  Testing target for manifold harmonic tranform class
 *
 *  @author Gabriel Cuendet
 *  @date   13.09.16
 *  Copyright © 2016 Gabriel Cuendet. All rights reserved.
 */

#include <string>
#include <limits>
#include <vector>
#include <chrono>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Eigen/Dense"
#include "Eigen/Sparse"

#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/halfedge_mesh.hpp"
#include "lts5/geometry/manifold_harmonic_transform.hpp"
#include "lts5/utils/cmd_parser.hpp"

std::string filepath;

/**
 * @class   MTHTest
 * @brief   Test unit for manifold harmonic tranform class
 * @author  Gabriel Cuendet
 * @date    13/09/16
 */
class MHTTest : public ::testing::Test {

 public:
  MHTTest(void) : mesh_(filepath) {
    mesh_.NormalizeMesh();
    he_mesh_.ConvertFromMesh(mesh_);
  }

 protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

  LTS5::Mesh<double> mesh_;
  LTS5::HalfedgeMesh<double> he_mesh_;

  LTS5::MHT<double> mhtransform_;
};

/** Define unit test, low_freq_visualization */
TEST_F(MHTTest, DISABLED_low_freq_visualization) {
  int n = static_cast<int>(he_mesh_.size());
  int n_basis_vectors = 10;

  Eigen::SparseMatrix<double> Q(n,n);
  Eigen::SparseMatrix<double> D(n,n);
  he_mesh_.ComputeCotanLaplacian(&D, &Q);

  // Compute basis
  LTS5::SparseLinearAlgebra::Arpack::non_sym_eigen_params<double>& params = mhtransform_.get_decomposition_params();
  params.fs = 1e-6;
  params.whichp = "LM";
  mhtransform_.Init(Q, D, n_basis_vectors);

  // DEBUG PURPOSE
  const Eigen::Matrix<double, Eigen::Dynamic, 1>& eig_values = mhtransform_.get_eigenvalues();
  //std::cout << eig_values << std::endl;

  EXPECT_EQ(eig_values.size(), n_basis_vectors);

  // Display!
  /*
  for (int i = 0; i < eig_values.size(); i++) {
    // DEBUG PURPOSE
    std::cout << "lambda = " << eig_values[i] << std::endl;

    mhtransform_.VizualizeEigOnMesh(i, LTS5::MHT<double>::VizNormalizationType::kPerMode, &mesh_);
    std::string filename = "output/mode_" + std::to_string(i) + ".ply";
    mesh_.Save(filename);
  }
   */
  EXPECT_TRUE(true);
}

/** Define unit test, high_freq_visualization */
TEST_F(MHTTest, DISABLED_high_freq_visualization) {
  int n = static_cast<int>(he_mesh_.size());
  int n_basis_vectors = 10;

  Eigen::SparseMatrix<double> Q(n,n);
  Eigen::SparseMatrix<double> D(n,n);
  he_mesh_.ComputeCotanLaplacian(&D, &Q);

  // Compute basis
  LTS5::SparseLinearAlgebra::Arpack::non_sym_eigen_params<double>& params = mhtransform_.get_decomposition_params();
  params.whichp = "LM";
  params.enable_shift_inverse = false;
  mhtransform_.Init(Q, D, n_basis_vectors);

  // DEBUG PURPOSE
  const Eigen::Matrix<double, Eigen::Dynamic, 1>& eig_values = mhtransform_.get_eigenvalues();
  //std::cout << eig_values << std::endl;

  EXPECT_EQ(eig_values.size(), n_basis_vectors);

  // Display!
  /*
  for (int i = 0; i < eig_values.size(); i++) {
    // DEBUG PURPOSE
    std::cout << "lambda = " << eig_values[i] << std::endl;

    mhtransform_.VizualizeEigOnMesh(i, LTS5::MHT<double>::VizNormalizationType::kPerMode, &mesh_);
    std::string filename = "output/mode_hi_" + std::to_string(i) + ".ply";
    mesh_.Save(filename);
  }
   */
  std::ofstream file("test.txt");
  if (file.is_open()) {
    file << mhtransform_.get_basis() << '\n';
  }

  EXPECT_TRUE(true);
}

/** Define unit test, ReadWriteMHT */
TEST_F(MHTTest, ReadWriteMHT) {
  int n = static_cast<int>(he_mesh_.size());
  int n_basis_vectors = 20;

  Eigen::SparseMatrix<double> Q(n,n);
  Eigen::SparseMatrix<double> D(n,n);
  he_mesh_.ComputeCotanLaplacian(&D, &Q);

  // Compute basis
  LTS5::SparseLinearAlgebra::Arpack::non_sym_eigen_params<double>& params = mhtransform_.get_decomposition_params();
  params.whichp = "LM";
  params.enable_shift_inverse = true;
  mhtransform_.Init(Q, D, n_basis_vectors);

  mhtransform_.Write("dump.bin");

  LTS5::MHT<double> mht_reloaded;
  mht_reloaded.Read("dump.bin");

  const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>& basis1 = mhtransform_.get_basis();
  const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>& basis2 = mht_reloaded.get_basis();
  for (int i = 0; i < basis1.rows(); ++i) {
    for (int j = 0; j < basis1.cols(); ++j) {
      EXPECT_EQ(basis1(i,j), basis2(i,j));
    }
  }
}

/** Define unit test, UniformBasisOrthoCheck */
TEST_F(MHTTest, UniformBasisOrthoCheck) {
  int n = static_cast<int>(he_mesh_.size());
  double tol = 5e-7;

  Eigen::SparseMatrix<double> Q(n,n);
  Eigen::SparseMatrix<double> D(n,n);
  he_mesh_.ComputeUniformLaplacian(&D, &Q);

  // Compute basis
  mhtransform_.Init(Q, D, 15);

  const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>& basis = mhtransform_.get_basis();
  //const Eigen::SparseMatrix<double>& D = mhtransform_.get_D();
  for (int i = 0; i < basis.rows()-1; ++i) {
    Eigen::RowVectorXd row_i = basis.row(i);
    for (int j = i; j < basis.rows(); ++j) {
      Eigen::RowVectorXd row_j = basis.row(j);
      double val = 0.0;
      for (int k=0; k < row_i.size(); ++k) {
        val += row_i[k] * D.coeff(k,k) * row_j[k];
      }
      if (i == j) {
        EXPECT_NEAR(val, 1.0, tol);
      } else {
        EXPECT_NEAR(val, 0.0, tol);
      }
    }
  }
}

/** Define unit test, CotanBasisOrthoCheck */
TEST_F(MHTTest, CotanBasisOrthoCheck) {
  int n = static_cast<int>(he_mesh_.size());
  double tol = 5e-7;

  Eigen::SparseMatrix<double> Q(n,n);
  Eigen::SparseMatrix<double> D(n,n);
  he_mesh_.ComputeCotanLaplacian(&D, &Q);

  // Compute basis
  mhtransform_.Init(Q, D, 15);

  const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>& basis = mhtransform_.get_basis();
  //const Eigen::SparseMatrix<double>& D = mhtransform_.get_D();
  for (int i = 0; i < basis.rows()-1; ++i) {
    Eigen::RowVectorXd row_i = basis.row(i);
    for (int j = i; j < basis.rows(); ++j) {
      Eigen::RowVectorXd row_j = basis.row(j);
      double val = 0.0;
      for (int k=0; k < row_i.size(); ++k) {
        val += row_i[k] * D.coeff(k,k) * row_j[k];
      }
      if (i == j) {
        EXPECT_NEAR(val, 1.0, tol);
      } else {
        EXPECT_NEAR(val, 0.0, tol);
      }
    }
  }
}

/** Define unit test, reconstruction */
TEST_F(MHTTest, DISABLED_reconstruction) {
  int n = static_cast<int>(he_mesh_.size());

  Eigen::SparseMatrix<double> Q(n,n);
  Eigen::SparseMatrix<double> D(n,n);
  he_mesh_.ComputeCotanLaplacian(&D, &Q);

  // Compute basis
  std::cout << "Computing the basis (can take some time) ..." << std::flush;
  mhtransform_.Init(Q, D, std::floor(0.05 * n));
  std::cout << "DONE!" << std::endl;

  // Transform
  std::vector<LTS5::Vector3<double> >& v_spat = mesh_.get_vertex();
  std::vector<LTS5::Vector3<double> > v_freq;
  mhtransform_.Transform(v_spat, &v_freq);

  // Inv transform
  mhtransform_.InvTransform(v_freq, &v_spat);

  mesh_.Save("face_reconstruction_cotan_20percent.ply");

  // Reference mesh
  const std::vector<LTS5::Vector3<double> >& ref = he_mesh_.get_vertex();
  LTS5::Vector3<double> cumulative_error(0.0, 0.0, 0.0);
  for (int i = 0; i < v_spat.size(); ++i) {
    cumulative_error.x_ += std::abs(v_spat[i].x_ - ref[i].x_);
    cumulative_error.y_ += std::abs(v_spat[i].y_ - ref[i].y_);
    cumulative_error.z_ += std::abs(v_spat[i].z_ - ref[i].z_);
  }

  EXPECT_LE(cumulative_error.Norm() / n, 1e-2);
}

/** Define unit test, BandbyBandAlgo */
TEST_F(MHTTest, BandbyBandAlgo) {
  int n_basis = 200;

  int n = static_cast<int>(he_mesh_.size());
  Eigen::SparseMatrix<double> Q(n,n);
  Eigen::SparseMatrix<double> D(n,n);
  he_mesh_.ComputeCotanLaplacian(&D, &Q);

  std::cout << "Computing 50 basis..." << std::flush;
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

  mhtransform_.Init(Q, D, 50);

  std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end-begin);
  const int elapsedSec = duration.count();
  std::cout << "DONE!" << std::endl;
  std::cout << "It took " << std::to_string(elapsedSec) << " ms" << std::endl;

  LTS5::MHT<double> mht_4x50;
  std::cout << "Computing 200 basis using BandByBand algo (4x50)..." << std::flush;
  begin = std::chrono::steady_clock::now();

  mht_4x50.Init(Q, D, 200);

  end= std::chrono::steady_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(end-begin);
  const int elapsedSec2 = duration.count();
  std::cout << "DONE!" << std::endl;
  std::cout << "It took " << std::to_string(elapsedSec2) << " ms" << std::endl;

  LTS5::MHT<double> mht_200;
  mht_200.set_eigenpairs_per_band(200);
  std::cout << "Computing 200 basis..." << std::flush;
  begin = std::chrono::steady_clock::now();

  mht_200.Init(Q, D, 200);

  end= std::chrono::steady_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(end-begin);
  const int elapsedSec3 = duration.count();
  std::cout << "DONE!" << std::endl;
  std::cout << "It took " << std::to_string(elapsedSec3) << " ms" << std::endl;

  const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>& basis_4x50 = mht_4x50.get_basis();
  const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>& basis_200 = mht_200.get_basis();
  const Eigen::Matrix<double, Eigen::Dynamic, 1>& eigenval_4x50 = mht_4x50.get_eigenvalues();
  const Eigen::Matrix<double, Eigen::Dynamic, 1>& eigenval_200 = mht_200.get_eigenvalues();

  EXPECT_EQ(basis_200.rows(), basis_4x50.rows());

  if (basis_200.rows() == basis_4x50.rows()) {
    for (int i = 0; i < basis_200.rows(); ++i) {
      EXPECT_NEAR(eigenval_4x50[i], eigenval_200[i], 1e-5);
      EXPECT_LE((basis_200.row(i).cwiseAbs() - basis_4x50.row(i).cwiseAbs()).maxCoeff(), 1e-5);
    }
  }
}

/** Define unit test, partial_reconstruction */
TEST_F(MHTTest, partial_reconstruction) {
  int n = static_cast<int>(he_mesh_.size());

  Eigen::SparseMatrix<double> Q(n,n);
  Eigen::SparseMatrix<double> D(n,n);
  he_mesh_.ComputeCotanLaplacian(&D, &Q);

  // Compute basis
  int n_basis[] = {50, 100, 200, 500};

  std::cout << "Computing the basis (can take some time) ..." << std::flush;
  mhtransform_.Init(Q, D, *std::max_element(std::begin(n_basis), std::end(n_basis)));
  std::cout << "DONE!" << std::endl;

  for (int i = 0; i < 4; ++i) {

    // Transform
    std::vector<LTS5::Vector3<double> >& v_spat = mesh_.get_vertex();
    std::vector<LTS5::Vector3<double> > v_freq;
    mhtransform_.Transform(v_spat, &v_freq);

    std::vector<LTS5::Vector4<unsigned char> >& vc_spat = mesh_.get_vertex_color();
    std::vector<LTS5::Vector4<double> > vc_freq;
    if (!vc_spat.empty()) {
      mhtransform_.Transform(vc_spat, &vc_freq);
    }

    std::vector<LTS5::Vector3<double> > troncated_v_freq(v_freq.begin(), v_freq.begin() + n_basis[i]);
    std::vector<LTS5::Vector4<double> > troncated_vc_freq;
    if (!vc_spat.empty()) {
      troncated_vc_freq = std::vector<LTS5::Vector4<double> >(vc_freq.begin(), vc_freq.begin() + n_basis[i]);
    }
    // Inv transform
    mhtransform_.InvTransform(troncated_v_freq, &v_spat);
    if (!vc_spat.empty()) {
      mhtransform_.InvTransform(troncated_vc_freq, &vc_spat);
    }

    mesh_.Save("output/reconstruction_" + std::to_string(n_basis[i]) + ".ply");
  }

  EXPECT_TRUE(true);
}

int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("m", LTS5::CmdLineParser::ArgState::kNeeded, "Mesh");
  int error = parser.ParseCmdLine(argc, argv);
  if (!error) {
    parser.HasArgument("-m", &filepath);
  }

  // Init test
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  // Run test
  return RUN_ALL_TESTS();
}
