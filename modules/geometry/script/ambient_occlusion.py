# coding=utf-8
import numpy as np
from lts5.utils import RodriguesFloat as Rodrigues
from lts5.geometry import MeshFloat as Mesh
from lts5.geometry import OCTreeFloat as OCTree


__author__ = 'Toto'


class SphericalSampler:

  def __call__(self):
    """
    Generate random spherical coordinates
    :return: np.ndarray
    """
    u = np.random.normal(0, 1)
    v = np.random.normal(0, 1)
    w = abs(np.random.normal(0, 1))
    norm = (u * u + v * v + w * w) ** (0.5)
    x, y, z = u * 1 / norm, v * 1 / norm, w * 1 / norm
    return np.asarray([x, y, z], dtype=np.float32).reshape(3, -1)


class AmbientOcclusionGenerator:

  def __init__(self, mesh):
    self.mesh = mesh
    self.mesh.compute_vertex_normal()
    xmin, ymin, zmin, xmax, ymax, zmax = mesh.bbox
    self.radius = max([xmax - xmin, ymax - ymin, zmax - zmin])
    self.tree = OCTree()
    self.tree.insert(mesh, Mesh.PrimitiveType.kTriangle)
    self.tree.build()
    self.ao = np.zeros(mesh.vertex.shape[0], mesh.vertex.dtype)
    self.sampler = SphericalSampler()

  def process(self, n_sample):
    z_axis = np.array([0.0, 0.0, 1.0], dtype=np.float32)
    for k, (vertex, normal) in enumerate(zip(self.mesh.vertex, self.mesh.normal)):
      axis = np.cross(z_axis, normal)
      axis /= (np.linalg.norm(axis) + 1e-6)
      angle = np.arccos(np.dot(z_axis, normal))
      r = Rodrigues(axis, angle)
      rotation_matrix = r.to_rotation_matrix()[:3, :3]
      cnt = 0
      for _ in range(n_sample):
        pts = self.sampler()
        pts = (rotation_matrix @ pts) * self.radius
        vx, vy, vz = vertex
        pts_x, pts_y, pts_z = pts[:, 0]
        p = (vx + pts_x, vy + pts_y, vz + pts_z)
        q = (vx, vy, vz)
        inter = self.tree.intersect_with_segment(mesh=self.mesh, p=p, q=q, t_max=0.99)
        if inter == -1.0:
          cnt += 1
      ao = float(cnt) / float(n_sample)
      self.ao[k] = ao

  def save(self, path):
    with open(path, 'wb') as f:
      f.write(self.ao.tobytes())

