# coding=utf-8
"""
Implementation of error function used for ICP:
  - Point-to-Point
  - Point-to-Plane

Based on:
 - https://gfx.cs.princeton.edu/proj/iccv05_course/iccv05_icp_gr.pdf
 - http://www.hao-li.com/cs599-ss2015/slides/Lecture05.1.pdf
 - https://link.springer.com/article/10.1007/s10514-013-9327-2
"""
import math
import numpy as np
from scipy.spatial.distance import directed_hausdorff

__author__ = 'Christophe Ecabert'


class ICPMetric:
  """ Interface for ICP metric """

  def minimize(self, x, y, nx, ny):
    """
    Minimize the underlying for a given set of points
    :param x:   Source surface vertex, Nx3
    :param y:   Target surface vertex, Nx3
    :param nx:  Source surface normals, Nx3
    :param ny:  Target surface normals, Nx3
    """
    raise NotImplementedError('Must be implemented by subclass')

  def distance(self, x, y, nx, ny, aggregated=True):
    """
    Distance between to sets of points
    :param x:   Source surface vertex, Nx3
    :param y:   Target surface vertex, Nx3
    :param nx:   Source surface normals, Nx3
    :param ny:  Target surface normals, Nx3
    :param aggregated: If `True` return aggregated error, otherwise per points
                       error
    :return:  float, error function value
    """
    raise NotImplementedError('Must be implemented by subclass')

  @property
  def rotation(self):
    """
    Estimated rotation matrix
    :return:  Rotation matrix
    """
    return np.eye(3)

  @property
  def translation(self):
    """
    Estimated translation vector
    :return: Translation vector
    """
    return np.zeros((1, 3))

  @property
  def scale(self):
    """
    Estimated isotropic scale
    :return:  Scale, default to 1.0
    """
    return 1.0


class ICPPointToPointMetric(ICPMetric):
  """ Point-to-point metric implementation """

  def __init__(self, isotropic_scale=False):
    """
    Constructor
    :param isotropic_scale: If `True` add isotropic scale to the transform
    """
    # Estimated parameters: [rx, ry, rz, tx, ty, tz]
    self.n_params = 7 if isotropic_scale else 6
    self.p = np.zeros(self.n_params)
    if isotropic_scale:
      self.p[-1] = 1.0  # Init scale to 1.0
    # Least square factory function
    self.build_ls_fn = (self._build_least_square_with_scale if isotropic_scale
                        else self._build_least_square_without_scale)

  def minimize(self, x, y, nx, ny):
    """
    Minimize the linearized Point-to-plane error between to given set of points
    :param x:   Source surface vertex, Nx3
    :param y:   Target surface vertex, Nx3
    :param nx:  Source surface normals, Nx3 (Not used)
    :param ny:  Target surface normals, Nx3
    """
    A, b = self.build_ls_fn(x, y)
    # Solve
    AtA = A.T @ A
    Atb = A.T @ b
    self.p = np.linalg.solve(AtA, Atb).ravel()

  def distance(self, x, y, nx=None, ny=None, aggregated=True):
    """
    Point-to-point error value
    :param x:   Source surface vertex, Nx3
    :param y:   Target surface vertex, Nx3
    :param nx:  Source surface normals, Nx3 (Not used)
    :param ny:  Target surface normals, Nx3 (Not used)
    :param aggregated: If `True` return aggregated error, otherwise per points
                       error
    :return: point-to-point distance
    """
    # Compute error
    d = np.sum((x - y) ** 2.0, axis=1) ** 0.5   # L2 norm
    if aggregated:
      d = d.mean(axis=0)                        # Average error
    return d

  def _build_least_square_without_scale(self, x, y):
    """
    Build linear least square without isotropic scale
    :param x: Source pts, Nx3
    :param y: Target pts, Nx3
    :return:  Design matrix `A`, bias `b`
    """
    N = x.shape[0]
    px = x[:, 0]
    py = x[:, 1]
    pz = x[:, 2]
    # Build linear least square
    A = np.zeros((3 * N, self.n_params))
    b = (y - x).reshape(-1, 1)
    # rx
    A[1::3, 0] = -pz
    A[2::3, 0] = py
    # ry
    A[0::3, 1] = pz
    A[0::3, 1] = pz
    A[2::3, 1] = -px
    # rz
    A[0::3, 2] = -py
    A[1::3, 2] = px
    # tx
    A[0::3, 3] = 1.0
    # ty
    A[1::3, 4] = 1.0
    # tz
    A[2::3, 5] = 1.0
    return A, b

  def _build_least_square_with_scale(self, x, y):
    """
    Build linear least square with isotropic scale
    :param x: Source pts, Nx3
    :param y: Target pts, Nx3
    :return:  Design matrix `A`, bias `b`
    """
    N = x.shape[0]
    px = x[:, 0]
    py = x[:, 1]
    pz = x[:, 2]
    # Build linear least square
    A = np.zeros((3 * N, self.n_params))
    b = y.reshape(-1, 1)
    # rx
    A[1::3, 0] = -pz
    A[2::3, 0] = py
    # ry
    A[0::3, 1] = pz
    A[0::3, 1] = pz
    A[2::3, 1] = -px
    # rz
    A[0::3, 2] = -py
    A[1::3, 2] = px
    # tx
    A[0::3, 3] = 1.0
    # ty
    A[1::3, 4] = 1.0
    # tz
    A[2::3, 5] = 1.0
    # scale
    A[:, 6] = x.ravel()
    return A, b

  @property
  def rotation(self):
    """
    Estimated rotation matrix
    :return:  Rotation matrix
    """
    if self.n_params == 6:
      rx, ry, rz = np.clip(self.p[:3], -1.0, 1.0)
    else:
      rx, ry, rz = np.clip(self.p[:3] / self.p[6], -1.0, 1.0)
    gamma = math.asin(rz)  # Rotation around `z`
    theta = math.asin(ry)  # Rotation around `y`
    phi = math.asin(rx)    # Rotation around `x`
    # Build rotation matrix
    cg = math.cos(gamma)
    sg = math.sin(gamma)
    ct = math.cos(theta)
    st = math.sin(theta)
    cp = math.cos(phi)
    sp = math.sin(phi)
    return np.asarray([[ct * cg, (sp * st * cg) - (cp * sg), (sp * sg) + (cp * st * cg)],
                       [ct * sg, (sp * st * sg) + (cp * cg), (cp * st * sg) - (sp * cg)],
                       [-st, sp * ct, cp * ct]],
                      dtype=np.float64)

  @property
  def translation(self):
    """
    Estimated translation vector
    :return: Translation vector
    """
    tx, ty, tz = self.p[3:6]
    return np.asarray([tx, ty, tz], dtype=np.float64)

  @property
  def scale(self):
    """
    Estimated scale
    :return:  Isotropic scale
    """
    if self.n_params == 6:
      return 1.0
    else:
      return self.p[6]


class ICPPointToPlaneMetric(ICPMetric):
  """ Point-to-plane metric implementation """

  def __init__(self, isotropic_scale=False):
    """
    Constructor
    :param isotropic_scale: If `True` add isotropic scale to the transform
    """
    # Estimated parameters: [rx, ry, rz, tx, ty, tz]
    self.n_params = 7 if isotropic_scale else 6
    self.p = np.zeros(self.n_params)
    if isotropic_scale:
      self.p[-1] = 1.0  # Init scale to 1.0
    # Least square factory function
    self.build_ls_fn = (self._build_least_square_with_scale if isotropic_scale
                        else self._build_least_square_without_scale)

  def minimize(self, x, y, nx, ny):
    """
    Minimize the linearized Point-to-plane error between to given set of points
    :param x:   Source surface vertex, Nx3
    :param y:   Target surface vertex, Nx3
    :param nx:  Source surface normals, Nx3 (Not used)
    :param ny:  Target surface normals, Nx3
    """
    A, b = self.build_ls_fn(x, y, ny)
    # Solve
    AtA = A.T @ A
    Atb = A.T @ b
    self.p = np.linalg.solve(AtA, Atb).ravel()

  def _build_least_square_without_scale(self, x, y, n):
    """
    Build least square without isotropic scale
    :param x: Source pts, Nx3
    :param y: Target pts, Nx3
    :param n: Target normals, Nx3
    :return:  Design matrix `A`, bias `b`
    """
    # Build least square problem Ax = bm where
    #     | x1 x ny1, ny1 |                                     |-(x1 - y1)ny1 |
    # A = |       ...     |, x = [rx, ry, rz, tx, ty, tz]', b = |      ...     |
    #     | xN x nyN, nyN |                                     |-(xN - yN)nyN |

    cross = np.cross(x, n, axis=1)
    A = np.concatenate([cross, n], axis=1)
    b = - np.sum((x - y) * n, axis=1).reshape(-1, 1)
    return A, b

  def _build_least_square_with_scale(self, x, y, n):
    """
    Build least square with isotropic scale
    :param x: Source pts, Nx3
    :param y: Target pts, Nx3
    :param n: Target normals, Nx3
    :return:  Design matrix `A`, bias `b`
    """
    cross = np.cross(x, n, axis=1)
    dot = (x * n).sum(axis=1).reshape(-1, 1)
    A = np.concatenate([cross, n, dot], axis=1)
    b = (y * n).sum(axis=1).reshape(-1, 1)
    return A, b

  def distance(self, x, y, nx, ny, aggregated=True):
    """
    Symmetric point-to-plane distance as defined in "Unsupervised Training for
     3D Morphable Model Regression", Genova et al.
    :param x:   Source surface vertex, Nx3
    :param y:   Target surface vertex, Nx3
    :param nx:  Source surface normals, Nx3 (Not used)
    :param ny:  Target surface normals, Nx3
    :param aggregated: If `True` return aggregated error, otherwise per points
                       error
    :return: point-to-plane error
    """
    # Compute error, since pts-tp-plane distance is a signed distance, take
    # absolute value to get the symmetric distance.
    d = np.abs(np.sum((x - y) * ny, axis=1))
    if aggregated:
      d = d.mean(axis=0)
    return d

  @property
  def rotation(self):
    """
    Estimated rotation matrix
    :return:  Rotation matrix
    """
    if self.n_params == 6:
      rx, ry, rz = np.clip(self.p[:3], -1.0, 1.0)
    else:
      rx, ry, rz = np.clip(self.p[:3] / self.p[6], -1.0, 1.0)
    gamma = math.asin(rz)    # Rotation around `z`
    theta = math.asin(ry)    # Rotation around `y`
    phi = math.asin(rx)      # Rotation around `x`
    # Build rotation matrix
    cg = math.cos(gamma)
    sg = math.sin(gamma)
    ct = math.cos(theta)
    st = math.sin(theta)
    cp = math.cos(phi)
    sp = math.sin(phi)
    return np.asarray([[ct * cg, (sp * st * cg) - (cp * sg), (sp * sg) + (cp * st * cg)],
                       [ct * sg, (sp * st * sg) + (cp * cg), (cp * st * sg) - (sp * cg)],
                       [-st, sp * ct, cp * ct]],
                      dtype=np.float64)

  @property
  def translation(self):
    """
    Estimated translation vector
    :return: Translation vector
    """
    tx, ty, tz = self.p[3:6]
    return np.asarray([tx, ty, tz], dtype=np.float64)

  @property
  def scale(self):
    """
    Estimated scale
    :return:  Isotropic scale
    """
    if self.n_params == 6:
      return 1.0
    else:
      return self.p[6]


class ICPHausdorffDistance(ICPMetric):
  """
  Class measuring Hausdorff distance between two surfaces
  See:
   - https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.directed_hausdorff.html
  """

  def __init__(self, symmetric=True):
    """
    Constructor
    :param symmetric: If `True` compute the symmetric Hausdorff distance between
                      the source and target surface.
    """
    self.sym = symmetric

  def minimize(self, x, y, nx, ny):
    """
    Minimize the linearized Point-to-plane error between to given set of points
    :param x:   Source surface vertex, Nx3
    :param y:   Target surface vertex, Nx3
    :param nx:  Source surface normals, Nx3 (Not used)
    :param ny:  Target surface normals, Nx3
    """
    raise RuntimeError('Hausdorff distance can not be used in optimization!')

  def distance(self, x, y, nx, ny, aggregated=True):
    """
    Compute Hausdorff distance between `x` and `y`
    :param x:     Source points, Nx3
    :param y:     Target points, Nx3
    :param nx:    Source normals, Nx3
    :param ny:    Target normals, Nx3
    :param aggregated:  Not used
    :return: Distance
    """
    d_xy, i1, i2 = directed_hausdorff(x, y)
    if self.sym:
      d_yx, i1, i2 = directed_hausdorff(y, x)
      return np.asarray(max(d_xy, d_yx))
    return np.asarray(d_xy)
