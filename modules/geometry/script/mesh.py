#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 16 16:45:59 2017

@author: gabrielcuendet
"""
import os.path
import numpy as np
from plyfile import PlyData


class Mesh:
  """ Mesh """

  def __init__(self):
    """ Constructor """
    self.vertices = np.ndarray((3, 0), dtype=np.double)
    self.normals = np.ndarray((3, 0), dtype=np.double)
    self.vertex_colors = np.ndarray((4, 0), dtype=np.int8)
    self.faces = np.ndarray((3, 0), dtype=np.int)

  def load(self, filepath):
    """ Load a mesh from file

    :param filepath: Path to the file to load
    """
    if filepath.endswith('.ply'):
      self.load_from_ply(filepath)
    else:
      print('ERROR: unknown format (only ply is supported for now)')

  def load_from_ply(self, filepath):
    """ Load a mesh from a ply file

    :param filepath: Path to the file to load
    """
    plydata = PlyData.read(filepath)
    self.vertices = np.array([plydata['vertex']['x'],
                              plydata['vertex']['y'],
                              plydata['vertex']['z']],
                             dtype=np.double)
    self.vertex_colors = np.array([plydata['vertex']['red'],
                                   plydata['vertex']['green'],
                                   plydata['vertex']['blue'],
                                   plydata['vertex']['alpha']],
                                  dtype=np.ubyte)
    tri_data = plydata['face']['vertex_indices']
    self.faces = np.fromiter(tri_data,
                             [('data', tri_data[0].dtype, (3,))],
                             count=len(tri_data))['data']

  def plot(self, ax):
    """ Plot the mesh in the axes ax """
    ax.plot(self.vertices[0, :],
            self.vertices[1, :],
            self.vertices[2, :],
            '.k')


def test01():
  """ Dummy test function """
  mesh = Mesh()
  dir_path = os.path.dirname(os.path.realpath(__file__))
  mesh.load('{}/../test/sine_xy_plane.ply'.format(dir_path))


if __name__ == '__main__':
  test01()
