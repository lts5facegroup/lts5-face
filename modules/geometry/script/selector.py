# coding=utf-8
"""
Implement point selection for ICP algorithm:
  - Random point selection
  - Normal-space sampling

Based on:
 - https://gfx.cs.princeton.edu/proj/iccv05_course/iccv05_icp_gr.pdf
 - http://www.hao-li.com/cs599-ss2015/slides/Lecture05.1.pdf
 - https://link.springer.com/article/10.1007/s10514-013-9327-2
"""
import numpy as np
from lts5.utils import HistogramDouble as Histogram
from lts5.utils import uniform_histogram_sampling

__author__ = 'Christophe Ecabert'


class ICPSelector:
  """ Selector Interface """

  def select(self, x, nx):
    """
    Select a subset of points from `x`
    :param x:   Source points
    :param nx:  Source normals
    :return:  Selected index
    """
    raise NotImplementedError('Must be implemented by subclass')


class ICPGlobalSelector(ICPSelector):
  """ Select every available vertex """

  def select(self, x, nx):
    """
    Select every vertex
    :param x:   Source points, Nx3
    :param nx:  Source normals, Nx3
    :return:  Selected index, N
    """
    return np.arange(x.shape[0])


class ICPRandomPointSelector(ICPSelector):
  """ Select randomly subset of a given surface """

  def __init__(self, ratio):
    """
    Constructor
    :param ratio:  Percentage of point to select
    """
    self.ratio = ratio

  def select(self, x, nx):
    """
    Select a subset of points from `x`
    :param x:   Source points, Nx3
    :param nx:  Source normals, Nx3
    :return:  Selected index, K
    """
    n_pts = x.shape[0]
    candidates = np.arange(n_pts)
    np.random.shuffle(candidates)
    p = int(self.ratio * n_pts)
    return candidates[:p]


class ICPNormalSpaceSampling(ICPSelector):
  """
  Implement normal-space sampling algorithm of Rusinkiewicz et al.
  See:
    https://github.com/PointCloudLibrary/pcl/blob/master/filters/include/pcl/filters/normal_space.h
  """

  def __init__(self,
               ratio,
               normals,
               bin_per_channel):
    """
    Constructor
    :param ratio: Percentage of normals to select
    :param normals: Normals to sample from
    :param bin_per_channel: Number of bin per channel used to build the
                            histogram of normal
    """
    self.ratio = ratio
    self.hist = Histogram()
    e = self.hist.build(data=normals,
                        ranges=[(-1.0, 1.0), (-1.0, 1.0), (-1.0, 1.0)],
                        bin_per_channel=bin_per_channel)
    if e != 0:
      raise RuntimeError('Error while build histogram of normal, check your'
                         ' data')

  def select(self, x, nx):
    """
    Select a subset of points from `x`
    :param x:   Source points, Nx3
    :param nx:  Source normals, Nx3
    :return:  Selected index, K
    """
    p = int(self.ratio * x.shape[0])
    # Sample histogram
    return np.asarray(uniform_histogram_sampling(self.hist, p))


class ICPCovarianceSampling(ICPSelector):
  """
  Implement covariance sampling algorithm of Gelfand et al, "Geometrically
   Stable Sampling for the ICP Algorithm"
  See:
   - https://graphics.stanford.edu/papers/stabicp/stabicp.pdf
   - https://github.com/PointCloudLibrary/pcl/blob/master/filters/include/pcl/filters/covariance_sampling.h
  """

  def __init__(self, ratio):
    """
    Constructor
    :param ratio: Percentage of points to select
    """
    self.ratio = ratio
    self.x_norm = None  # Normalized source points, Nx3
    self.Ft = None       # F matrix

  def select(self, x, nx):
    """
    Select a subset of points from `x`
    :param x:   Source points, Nx3
    :param nx:  Source normals, Nx3
    :return:  Selected index, K
    """

    cond_n = self._compute_condition_number(x, nx)

    # --- Part A, do not seek overlapping points, only perform A3
    # Compute covariance matrix + eigenvectors
    cov_m = self._compute_covariance_matrix(x, nx)
    eigval, eigvec = np.linalg.eigh(cov_m)
    # --- Part  B
    # from the paper figure out how to fill the candidate_indices - see
    # subsequent paper paragraphs
    # B1, Compute the v 6-vectors
    v = self.Ft

    # Setup list to be sorted in decreasing order
    L = np.abs(v @ eigvec)                  # Value
    Lsort = np.argsort(L, axis=0)[::-1, :]  # Sorted index
    Liter = np.zeros(6, dtype=np.int32)     # List iteartor

    # Now select the actual points
    t = np.zeros(6 , dtype=np.float64)
    x_select = np.zeros(x.shape[0], dtype=np.bool)
    n_sample = int(self.ratio * x.shape[0])
    indices = np.zeros(n_sample, dtype=np.int32)
    for k in range(n_sample):
      # Find the most unconstrained dimension, i.e., the minimum t
      min_ti = np.argmin(t)
      # Add the point from the top of the list corresponding to the dimension
      # to the set of samples
      Lmin_value = L[:, min_ti]
      Lmin_idx = Lsort[:, min_ti]

      # Already selected ?
      while x_select[Lmin_idx[Liter[min_ti]]]:
        Liter[min_ti] += 1

      # No, select it
      pts_idx = Lmin_idx[Liter[min_ti]]
      indices[k] = pts_idx                  # Save selected index
      x_select[pts_idx] = True              # Mark pts as selected
      Liter[min_ti] += 1                    # Move to the next element

      # Update the running totals
      val = (v[pts_idx, :].reshape(1, -1) @ eigvec).ravel()
      t += (val * val)

    cond_n2 = self._compute_condition_number(x[indices, :], nx[indices, :])

    return indices

  def _normalize_points(self, x):
    """
    Prepare the point cloud by centering at the origin and then scaling the
     points such that the average distance from the origin is 1.0 => rotations
     and translations will have the same magnitude
    :param x: Source points, Nx3
    :return:  Normalized data points
    """
    # Remove cog
    data = x - x.mean(axis=0).reshape(1, -1)
    # Remove scaling
    avg_d = np.linalg.norm(data, ord=2, axis=1).mean()
    data = data / avg_d
    return data

  def _compute_covariance_matrix(self, x, nx):
    """
    Estimate covariance matrix
    :param x: nd.array, source points, Nx3
    :param x: nd.array, source normals, Nx3
    :return:  Covariance matrix
    """
    # Normalize data
    self.x_norm = self._normalize_points(x)
    # Build F matrix
    xn_cross = np.cross(self.x_norm, nx, axis=1)
    self.Ft = np.concatenate([xn_cross, nx], axis=1)
    return self.Ft.T @ self.Ft

  def _compute_condition_number(self, x, nx):
    """
    Estimate condition number of the covariance matrix for a given set of points
    :param x:   Source points, Kx3
    :param nx:  Source normals, Kx3
    :return: Condition number
    """
    cov_m = self._compute_covariance_matrix(x, nx)
    eigval, eigvec = np.linalg.eigh(cov_m)
    return eigval.max() / eigval.min()




