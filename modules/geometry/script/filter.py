# coding=utf-8
"""
Implement various correspondence rejection method for ICP algorithm

Based on:
 - https://gfx.cs.princeton.edu/proj/iccv05_course/iccv05_icp_gr.pdf
 - http://www.hao-li.com/cs599-ss2015/slides/Lecture05.1.pdf
 - https://link.springer.com/article/10.1007/s10514-013-9327-2
"""
from math import cos
import numpy as np
__author__ = 'Christophe Ecabert'


class ICPCorrespondenceFilter:
  """ Correspondence filtering interface """

  def filter_correspondence(self, x, y, nx, ny):
    """
    Filter the K selected points based on some criterion
    :param x:   Selected points on the source, Kx3
    :param y:   Selected points on the target, Kx3
    :param nx:  Selected normals on the source, Kx3
    :param ny:  Selected normals on the target, Kx3
    :return:  List of index to keep for further processing
    """
    raise NotImplementedError('Must be implemented by subclass')


class ICPDistanceFilter(ICPCorrespondenceFilter):
  """
    Filtering method based on thresholding based on the distance between
    the correspondences.
    """

  def __init__(self, max_distance):
    """
    Constructor
    :param max_distance:  Maximum distance between two points (L2 distance)
    """
    self.max_d = max_distance * max_distance

  def filter_correspondence(self, x, y, nx, ny):
    """
    Filter the K selected points based on some criterion
    :param x:   Selected points on the source, Kx3
    :param y:   Selected points on the target, Kx3
    :param nx:  Selected normals on the source, Kx3
    :param ny:  Selected normals on the target, Kx3
    :return:  List of index to keep for further processing
    """
    dist = ((x - y) ** 2.0).sum(axis=1)
    # Keep value index where distance is smaller than self.factor * median
    keep_idx = np.where(dist < self.max_d)[0]
    return keep_idx


class ICPMedianDistanceFilter(ICPCorrespondenceFilter):
  """
  Filtering method based on thresholding based on the median distance between
  the correspondences.
  """

  def __init__(self, factor):
    """
    Constructor
    :param factor: The factor for correspondence rejection. Points with distance
                    greater than median times factor will be rejected
    """
    self.factor = factor

  def filter_correspondence(self, x, y, nx, ny):
    """
    Filter the K selected points based on some criterion
    :param x:   Selected points on the source, Kx3
    :param y:   Selected points on the target, Kx3
    :param nx:  Selected normals on the source, Kx3
    :param ny:  Selected normals on the target, Kx3
    :return:  List of index to keep for further processing
    """
    # Compute l2 distance
    dist = np.linalg.norm(y - x, axis=1)
    # Get median value
    median = np.median(dist)
    # Keep value index where distance is smaller than self.factor * median
    keep_idx = np.where(dist < self.factor * median)[0]
    return keep_idx


class ICPSurfaceNormalDeviationFilter(ICPCorrespondenceFilter):
  """
  Filter out correspondence based on thresholding based on normal deviation
  between source and target.
  """

  def __init__(self, max_angle):
    """
    Constructor
    :param max_angle: Maximum angle between two normals to be considered as a
                      good match. Angle must be in radian.
    """
    self.threshold = cos(max_angle)

  def filter_correspondence(self, x, y, nx, ny):
    """
    Filter the K selected points based on some criterion
    :param x:   Selected points on the source, Kx3
    :param y:   Selected points on the target, Kx3
    :param nx:  Selected normals on the source, Kx3
    :param ny:  Selected normals on the target, Kx3
    :return:  List of index to keep for further processing
    """
    # Compute angle (i.e. cos) between the normals
    ang = (nx * ny).sum(axis=1)
    # Keep value index where cos(theta) > threshold, 0 == orthogonal
    keep_idx = np.where(ang > self.threshold)[0]
    return keep_idx
