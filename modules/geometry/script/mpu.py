#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 12 16:39:24 2017

@author: gabriel
"""
from itertools import product, combinations
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D  # pylint: disable=unused-import
from mesh import Mesh

# pylint: disable=invalid-name


def ReadMatFromBin(f):
  """ Function to read opencv matrices saved to binary files from LTS5lib

  Parameters:
  -----------
  f: opened file stream
      Stream to read from

  Returns:
  --------
  mat: np.array
      OpenCV Mat as a numpy array
  """
  type_ = np.fromfile(f, dtype=np.intc, count=1)
  rows = np.fromfile(f, dtype=np.intc, count=1)[0]
  cols = np.fromfile(f, dtype=np.intc, count=1)[0]

  if type_ == 5:
    dtype_ = np.single
  elif type_ == 6:
    dtype_ = np.double

  mat = np.fromfile(f, dtype=dtype_, count=rows*cols)
  return np.reshape(mat, (rows, cols), order='C')


def ComputeWeights(r):
  x = 1.5*r + 1.5  # Normalize x in [0; 3]
  arr = np.zeros(r.shape)
  arr[(x <= 0.0) & (x > 3.0)] = 0.0
  arr[(x > 0.0) & (x <= 1.0)] = 0.5 * x[(x > 0.0) & (x <= 1.0)] ** 2
  arr[(x > 1.0) & (x <= 2.0)] = 0.5 * (-2.0 * x[(x > 1.0) & (x <= 2.0)] ** 2 +
                                       6 * x[(x > 1.0) & (x <= 2.0)] - 3)
  arr[(x > 2.0) & (x <= 3.0)] = 0.5 * (3-x[(x > 2.0) & (x <= 3.0)]) * \
                                (3-x[(x > 2.0) & (x <= 3.0)])
  return arr


class BivariatePoly:
  """ Class implementing the bivariate quadratic polynomial approximation """
  def __init__(self):
    self.color = 'r'

  def read(self, f):
    """ Read a BivariatePoly object from binary file

    Parameters:
    -----------
    f: opened file stream
        Stream to read from
    """
    # format
    format_ = np.fromfile(f, dtype=np.ubyte, count=1)
    if format_ == 4:
      dtype_ = np.single
    elif format_ == 8:
      dtype_ = np.double
    # center
    self.center = np.fromfile(f, dtype=dtype_, count=3)
    # radius
    self.radius = np.fromfile(f, dtype=dtype_, count=1)
    # coeffs
    self.coeffs = np.fromfile(f, dtype=dtype_, count=6)
    # basis_change
    self.basis_change = ReadMatFromBin(f)

  def print_infos(self):
    """ Print informations about the estimator"""
    print('    Bivariate quadratic polynomial estimator')
    print('    center = ', self.center)
    print('    radius = ', self.radius)
    print('    coeffs = \n', self.coeffs)
    print('    basis = \n', self.basis_change)

  def evaluateQ(self, x):
    """ Evaluate the value of the function at x

    Parameters:
    -----------
    x: np.array
        Point in space where to evaluate the function
    """
    center = np.reshape(self.center, (3, 1))
    x_tilde = np.dot(self.basis_change, x -
                     np.repeat(center, repeats=x.shape[1], axis=1))
    return x_tilde[2] - (self.coeffs[0] * x_tilde[0]**2 +
                         2 * self.coeffs[1] * x_tilde[0] * x_tilde[1] +
                         self.coeffs[2] * x_tilde[1] ** 2 +
                         self.coeffs[3] * x_tilde[0] +
                         self.coeffs[4] * x_tilde[1] +
                         self.coeffs[5])

  def evaluateSwQ(self, x, SwQ, Sw):
    """ Evaluate the value of the weighted function at x

    Parameters:
    -----------
    x: np.array
        Point in space where to evaluate the function
    """
    center = np.reshape(self.center, (3, 1))
    r = np.linalg.norm(x - center, axis=0) / self.radius
    w = ComputeWeights(r)
    Sw += w
    SwQ += np.multiply(w, self.evaluateQ(x))


class Quadric:
  """ Class implementing the general quadric approximation """
  def __init__(self):
    self.color = 'b'

  def read(self, f):
    """ Read a Quadric object from binary file

    Parameters:
    -----------
    f: opened file stream
        Stream to read from
    """
    # format
    format_ = np.fromfile(f, dtype=np.ubyte, count=1)
    if format_ == 4:
      dtype_ = np.single
    elif format_ == 8:
      dtype_ = np.double
    # center
    self.center = np.fromfile(f, dtype=dtype_, count=3)
    # radius
    self.radius = np.fromfile(f, dtype=dtype_, count=1)
    # A
    self.A = ReadMatFromBin(f)
    # b
    self.b = np.fromfile(f, dtype=dtype_, count=3)
    # c
    self.c = np.fromfile(f, dtype=dtype_, count=1)
    # aux points
    n_aux_points = np.fromfile(f, dtype=np.uint64, count=1)
    self.aux_points = np.reshape(np.fromfile(f, dtype=dtype_,
                                             count=3*n_aux_points),
                                 (n_aux_points, 3))
    self.aux_points = np.transpose(self.aux_points)
    # neighbors of aux points
    if n_aux_points > 0:
      self.nn_aux = []
      for _ in range(n_aux_points):
        n_nn = np.fromfile(f, dtype=np.uint32, count=1)
        self.nn_aux.append(np.reshape(np.fromfile(f, dtype=dtype_,
                                                  count=3*n_nn),
                                      (n_nn, 3)))
        self.nn_aux[-1] = np.transpose(self.nn_aux[-1])
    # aux di
    self.aux_di = np.fromfile(f, dtype=dtype_, count=n_aux_points)

  def print_infos(self):
    """ Print informations about the estimator"""
    print('    Quadric estimator')
    print('    center = ', self.center)
    print('    radius = ', self.radius)
    print('    A = \n', self.A)
    print('    b = \n', self.b)
    print('    c = \n', self.c)
    print('    using', self.aux_points.shape[1], 'aux points:')
    for i, aux_p in enumerate(self.aux_points.T):
      print('   ' + str(aux_p) + '=' + str(self.aux_di[i]))

  def evaluateQ(self, x):
    """ Evaluate the value of the function at x

    Parameters:
    -----------
    x: np.array
        Point in space where to evaluate the function
    """
    return np.diag(np.dot(np.transpose(x), np.dot(self.A, x))) + \
           np.dot(self.b, x) + self.c

  def evaluateSwQ(self, x, SwQ, Sw):
    """ Evaluate the value of the function at x

    Parameters:
    -----------
    x: np.array
        Point in space where to evaluate the function
    """
    center = np.reshape(self.center, (3, 1))
    r = np.linalg.norm(x - center, axis=0) / self.radius
    w = ComputeWeights(r)
    Sw += w
    SwQ += np.multiply(w, self.evaluateQ(x))


class MPUNode:
  """ Class implementing one node in the MPU """
  def __init__(self, location_hash=-1):
    """ Constructor """
    self.location = np.int(location_hash)
    self.has_children = False

  def read(self, f):
    """ Read the node from binary file

    Parameters:
    -----------
    f: opened file stream
        Stream to read from
    """
    # format
    format_ = np.fromfile(f, dtype=np.ubyte, count=1)
    if format_ == 4:
      dtype_ = np.single
    elif format_ == 8:
      dtype_ = np.double
    # bbox
    self.bbox_min = np.fromfile(f, dtype=dtype_, count=3)
    self.bbox_max = np.fromfile(f, dtype=dtype_, count=3)
    # center
    self.center = np.fromfile(f, dtype=dtype_, count=3)
    # diagonal
    self.diagonal = np.fromfile(f, dtype=dtype_, count=1)
    # support radius
    self.radius = np.fromfile(f, dtype=dtype_, count=1)
    # epsilon i
    self.epsiloni = np.fromfile(f, dtype=dtype_, count=1)
    # estimator: type
    self.estimator_type = np.fromfile(f, dtype=np.ubyte, count=1)
    if self.estimator_type == 0:
      pass
    elif self.estimator_type == 1:
      self.estimator = BivariatePoly()
      self.estimator.read(f)
    elif self.estimator_type == 2:
      self.estimator = Quadric()
      self.estimator.read(f)
    else:
      print('ERROR: unknown estimator type')
    # points
    n_points = np.fromfile(f, dtype=np.uint64, count=1)
    self.points = np.reshape(np.fromfile(f, dtype=dtype_,
                                         count=3*n_points),
                             (n_points, 3))
    self.points = np.transpose(self.points)
    # normals
    n_normals = np.fromfile(f, dtype=np.uint64, count=1)
    self.normals = np.reshape(np.fromfile(f, dtype=dtype_,
                                          count=3*n_normals),
                              (n_normals, 3))
    self.normals = np.transpose(self.normals)

  def print_infos(self):
    """ Print all the informations about the node"""
    print('bbox min = ', self.bbox_min)
    print('bbox max = ', self.bbox_max)
    print('center = ', self.center)
    print('diagonal = ', self.diagonal)
    print('radius = ', self.radius)
    print('epsiloni = ', self.epsiloni)
    print('estimator : ', self.estimator_type)
    if self.estimator_type != 0:
      self.estimator.print_infos()
    print('N points (used for approx): ' + str(self.points.shape[1]))

  def level(self):
    """ Returns the level of deepness of the node according to its
    location """
    level = 0
    shift = self.location
    while shift > 1:
      level += 1
      shift = shift >> 3
    return level

  def plot_domain(self, ax):
    """ Plot the domain (bbox) of the implicit surface in the figure

    Parameters:
    -----------
    figure: plt.figure
        A handle to the figure in which to plot
    """
    # Check that it's a cube
    diag = self.bbox_min - self.bbox_max
    assert((diag[0] - diag[1]) < 1e-7 and (diag[1] - diag[2]) < 1e-7)

    estimator_color = self.estimator.color

    # draw cube
    x = [self.bbox_min[0], self.bbox_max[0]]
    y = [self.bbox_min[1], self.bbox_max[1]]
    z = [self.bbox_min[2], self.bbox_max[2]]
    for s, e in combinations(np.array(list(product(x, y, z))), 2):
      if np.sum(np.abs(s-e)) == x[1]-x[0]:
        ax.plot3D(*zip(s, e), color=estimator_color)

  def plot_support_ball(self, ax):
    """ Plot the support ball of the node """
    estimator_color = 'r' if self.estimator_type == 1 else 'b'

    # draw sphere
    u, v = np.mgrid[0:2*np.pi:12j, 0:np.pi:12j]  # pylint: disable=invalid-slice-index
    r = self.radius
    center = self.center
    x = r * np.cos(u) * np.sin(v) + center[0]
    y = r * np.sin(u) * np.sin(v) + center[1]
    z = r * np.cos(v) + center[2]
    ax.plot_wireframe(x, y, z, color=estimator_color)

  def plot_approx_points(self, ax, with_normals=False, with_nn=False):
    """ Plot the points used for computing the approximation in the node

    Parameters:
    -----------
    ax:
        Axes in which to plot

    with_normals: bool (default=False)
        Wether or not to plot the normals of the points

    with_nn: bool (default=False)
        For QUADRIC estimator, wether or not to plot the nearest neighbors
        used to assess the quality of the auxiliary point
    """
    ax.plot(self.points[0, :], self.points[1, :], self.points[2, :],
            '.', color='k')
    if with_normals:
      ax.quiver(self.points[0, ::10], self.points[1, ::10],
                self.points[2, ::10],
                self.normals[0, ::10], self.normals[1, ::10],
                self.normals[2, ::10],
                length=0.03, pivot='tail')
    if self.estimator_type == [2]:
      # For the Quadric, plot the aux points (max. 9)
      n_aux_points = self.estimator.aux_points.shape[1]
      for i in range(n_aux_points):
        p_color = cm.jet(np.float(i)/n_aux_points)
        ax.plot([self.estimator.aux_points[0, i]],
                [self.estimator.aux_points[1, i]],
                [self.estimator.aux_points[2, i]], 'o', color=p_color,
                markersize=12)
        ax.text(self.estimator.aux_points[0, i],
                self.estimator.aux_points[1, i],
                self.estimator.aux_points[2, i],
                str(self.estimator.aux_di[i]), color=p_color)
        if with_nn:
          # Plot the 6 nearest neighbors
          nn = self.estimator.nn_aux[i]
          ax.plot(nn[0, :], nn[1, :], nn[2, :], 'D', color=p_color,
                  markersize=12)

  def plot(self, ax, plot_support_ball=False):
    """ Plot the implicit surface of the node in the figure

    Caution: this plots only the approximation of the node only in the
    bounding box of the node

    Parameters:
    -----------
    figure: plt.figure
        A handle to the figure in which to plot

    plot_support_ball: bool (default=False)
        Wether or not to plot the support ball

    See:
    ----
    http://stackoverflow.com/questions/4680525
    """
    def points_in_circle(p_min, p_max, N, p_center, radius):
      points = np.linspace(p_min, p_max, N)
      points = points[np.abs(points-p_center) <= radius]
      return points

    estimator_color = self.estimator.color

    N_res = 30
    N_slices = 30

    # Check that the domain is a cube
    diag = self.bbox_max - self.bbox_min
    assert((diag[0] - diag[1]) < 1e-7 and (diag[1] - diag[2]) < 1e-7)
    cube_a = 2 * self.radius  # diag[0]
    x0 = self.center[0] - self.radius  # self.bbox_min[0]
    y0 = self.center[1] - self.radius  # self.bbox_min[1]
    z0 = self.center[2] - self.radius  # self.bbox_min[2]

    for z in points_in_circle(z0, z0 + cube_a, N_slices, self.center[2],
                              self.radius):
      x_range = points_in_circle(x0, x0 + cube_a, N_res, self.center[0],
                                 self.radius)
      y_range = points_in_circle(y0, y0 + cube_a, N_res, self.center[1],
                                 self.radius)
      X, Y = np.meshgrid(x_range, y_range)

      N = len(x_range) * len(y_range)
      points = np.vstack((np.reshape(X, (1, N)),
                          np.reshape(Y, (1, N)),
                          np.repeat(z, N)))
      Z = np.reshape(self.estimator.evaluateQ(points),
                     (len(y_range), len(x_range)))
      ax.contour(X, Y, Z+z, [z], zdir='z', colors=estimator_color)

    for y in points_in_circle(y0, y0 + cube_a, N_slices, self.center[1],
                              self.radius):
      x_range = points_in_circle(x0, x0 + cube_a, N_res, self.center[0],
                                 self.radius)
      z_range = points_in_circle(z0, z0 + cube_a, N_res, self.center[2],
                                 self.radius)
      X, Z = np.meshgrid(x_range, z_range)

      N = len(x_range) * len(z_range)
      points = np.vstack((np.reshape(X, (1, N)),
                          np.repeat(y, N),
                          np.reshape(Z, (1, N))))
      Y = np.reshape(self.estimator.evaluateQ(points),
                     (len(z_range), len(x_range)))
      ax.contour(X, Y+y, Z, [y], zdir='y', colors=estimator_color)

    for x in points_in_circle(x0, x0 + cube_a, N_slices, self.center[0],
                              self.radius):
      y_range = points_in_circle(y0, y0 + cube_a, N_res, self.center[1],
                                 self.radius)
      z_range = points_in_circle(z0, z0 + cube_a, N_res, self.center[2],
                                 self.radius)
      Y, Z = np.meshgrid(y_range, z_range)

      N = len(y_range) * len(z_range)
      points = np.vstack((np.repeat(x, N),
                          np.reshape(Y, (1, N)),
                          np.reshape(Z, (1, N))))
      X = np.reshape(self.estimator.evaluateQ(points),
                     (len(z_range), len(y_range)))
      ax.contour(X+x, Y, Z, [x], zdir='x', colors=estimator_color)

    # Plot the bounding box
    self.plot_domain(ax)
    if plot_support_ball:
      self.plot_support_ball(ax)

  def evaluate(self, x, SwQ, Sw):
    """ Evaluate the MPUNode """
    self.estimator.evaluateSwQ(x, SwQ, Sw)


class MPU:
  """ Class implementing the multilevel partition of unity (MPU) """
  def __init__(self):
    """ Constructor of the class """
    self.nodes_ = {}
    self.max_tree_level_ = 0

  def load(self, filepath):
    """ Load a MPU from binary file

    Parameters:
    -----------
    filepath: str
        Path to the binary file to read
    """
    with open(filepath, 'rb') as f:
      # location_hash
      location_hash = np.fromfile(f, dtype=np.uint64, count=1)
      while location_hash:
        # print location_hash
        location_hash = location_hash[0]

        node = MPUNode(location_hash)
        node.read(f)
        # node.Print()
        self.max_tree_level_ = max((self.max_tree_level_,
                                    node.level()))

        self.nodes_[location_hash] = node
        if location_hash >> np.uint64(3):
          self.nodes_[location_hash >> np.uint64(3)].has_children = True

        location_hash = np.fromfile(f, dtype=np.uint64, count=1)
    msg = 'MPU: {} nodes loaded'.format(len(self.nodes_))
    print(msg)
    msg = '\tmax tree level = {}'.format(self.max_tree_level_)
    print(msg)

    self.bbox_min = self.nodes_[1].bbox_min
    self.bbox_max = self.nodes_[1].bbox_max

  def plot_nodes(self, ax, with_points=False, with_support_ball=False,
                 id_node=None):
    """ Plot a MPU of individual nodes

    Parameters:
    -----------
    ax:
        Axes in which to plot
    with_points: bool (default=False)
        Wether or not to plot the points used to compute the estimator
    with_support_ball: bool (default=False)
        Wether or not to plot the support ball of the node
    id_node: list
        List of id of nodes to plot
    """

    x0 = np.Inf
    y0 = np.Inf
    z0 = np.Inf
    x1 = -np.Inf
    y1 = -np.Inf
    z1 = -np.Inf

    if id_node is None:
      for node in self.nodes_.values():
        if node.has_children is False:
          node.print_infos()
          node.plot(ax)
          if with_support_ball:
            node.plot_support_ball(ax)
          if with_points:
            node.plot_approx_points(ax, with_normals=False,
                                    with_nn=True)

      x0 = self.bbox_min[0]
      y0 = self.bbox_min[1]
      z0 = self.bbox_min[2]
      cube_a = self.bbox_max[0] - self.bbox_min[0]
      x1 = x0 + cube_a
      y1 = y0 + cube_a
      z1 = z0 + cube_a

    else:
      for c_id in id_node:
        self.nodes_[c_id].print_infos()
        if self.nodes_[c_id].estimator_type != 0:
          self.nodes_[c_id].plot(ax)
        if with_support_ball:
          self.nodes_[c_id].plot_support_ball(ax)
        if with_points:
          self.nodes_[c_id].plot_approx_points(ax,
                                               with_normals=False,
                                               with_nn=True)

        x0 = min((x0, self.nodes_[c_id].bbox_min[0]))
        y0 = min((y0, self.nodes_[c_id].bbox_min[1]))
        z0 = min((z0, self.nodes_[c_id].bbox_min[2]))
        x1 = max((x1, self.nodes_[c_id].bbox_max[0]))
        y1 = max((y1, self.nodes_[c_id].bbox_max[1]))
        z1 = max((z1, self.nodes_[c_id].bbox_max[2]))

    ax.set_zlim3d(z0, z1)
    ax.set_ylim3d(y0, y1)
    ax.set_xlim3d(x0, x1)
    ax.set_xlabel('X axis')
    ax.set_ylabel('Y axis')
    ax.set_zlabel('Z axis')

  def plot_octree(self, ax, leafs_only=True):
    """ Plot the cells of the octree used for MPU implicit """
    for node in self.nodes_.values():
      if leafs_only:
        if node.has_children is False:
          node.plot_domain(ax)

      else:
        node.plot_domain(ax)

    blue_line = mlines.Line2D([], [], color='b')
    red_line = mlines.Line2D([], [], color='r')
    ax.legend([blue_line, red_line],
              ['General Quadric', 'Bivariate Quadratic Polynomial'])
    ax.set_xlabel('x axis')
    ax.set_ylabel('y axis')
    ax.set_zlabel('z axis')

  def plot(self, ax):
    """ Plot the MPU implicit, with the blending """
    print('domain: bbox_min = ' + str(self.nodes_[1].bbox_min))
    print('        bbox_max = ' + str(self.nodes_[1].bbox_max))

    N_res = 30
    N_slices = 50

    # Check that the domain is a cube
    diag = self.bbox_max - self.bbox_min
    assert((diag[0] - diag[1]) < 1e-7 and (diag[1] - diag[2]) < 1e-7)
    cube_a = diag[0]
    x0 = self.bbox_min[0]
    y0 = self.bbox_min[1]
    z0 = self.bbox_min[2]

    for z in np.linspace(z0, z0 + cube_a, N_slices):
      x_range = np.linspace(x0, x0 + cube_a, N_res)
      y_range = np.linspace(y0, y0 + cube_a, N_res)
      X, Y = np.meshgrid(x_range, y_range)

      N = len(x_range) * len(y_range)
      points = np.vstack((np.reshape(X, (1, N)),
                          np.reshape(Y, (1, N)),
                          np.repeat(z, N)))
      Z = np.reshape(self.evaluate(points),
                     (len(y_range), len(x_range)))
      ax.contour(X, Y, Z+z, [z], zdir='z')

    for y in np.linspace(y0, y0 + cube_a, N_slices):
      x_range = np.linspace(x0, x0 + cube_a, N_res)
      z_range = np.linspace(z0, z0 + cube_a, N_res)
      X, Z = np.meshgrid(x_range, z_range)

      N = len(x_range) * len(y_range)
      points = np.vstack((np.reshape(X, (1, N)),
                          np.repeat(y, N),
                          np.reshape(Z, (1, N))))
      Y = np.reshape(self.evaluate(points),
                     (len(y_range), len(x_range)))
      ax.contour(X, Y+y, Z, [y], zdir='y')

    for x in np.linspace(x0, x0 + cube_a, N_slices):
      y_range = np.linspace(y0, y0 + cube_a, N_res)
      z_range = np.linspace(z0, z0 + cube_a, N_res)
      Y, Z = np.meshgrid(y_range, z_range)

      N = len(y_range) * len(z_range)
      points = np.vstack((np.repeat(x, N),
                          np.reshape(Y, (1, N)),
                          np.reshape(Z, (1, N))))
      X = np.reshape(self.evaluate(points),
                     (len(z_range), len(y_range)))
      ax.contour(X+x, Y, Z, [x], zdir='x')

    # Plot the bbox of the leaf nodes
  #        for node in self.nodes_.values():
  #            if not node.has_children:
  #                node.plot_domain(ax)

  def evaluate(self, x):
    """ Evaluate the value of the implicit function at the coordinates x

    Parameters:
    -----------
    x: np.array
        Array of points to evaluate in the MPU implicit
    """
    Sw = np.zeros((1, x.shape[1]))
    SwQ = np.zeros((1, x.shape[1]))

    for node in self.nodes_.values():
      if not node.has_children:
        node.evaluate(x, SwQ, Sw)

    res = SwQ
    np.seterr(divide='raise')
    res = np.divide(res, Sw)

    return res

  def compute_bbox(self):
    """ Compute the bounding box of the MPU with the current nodes """
    x0 = np.Inf
    y0 = np.Inf
    z0 = np.Inf
    x1 = -np.Inf
    y1 = -np.Inf
    z1 = -np.Inf

    for c_id in self.nodes_.values():
      x0 = min((x0, c_id.bbox_min[0]))
      y0 = min((y0, c_id.bbox_min[1]))
      z0 = min((z0, c_id.bbox_min[2]))
      x1 = max((x1, c_id.bbox_max[0]))
      y1 = max((y1, c_id.bbox_max[1]))
      z1 = max((z1, c_id.bbox_max[2]))

    self.bbox_min = np.array([x0, y0, z0])
    self.bbox_max = np.array([x1, y1, z1])
    print('bbox min = ' + str(self.bbox_min))
    print('bbox max = ' + str(self.bbox_max))


def test01():
  """ arguments for the plot :
      -m mpu_mc2_clean_poly.bin -r target_mesh_clean.ply
  """
  import argparse

  def parse_arg():
    """ Arguments parser configuration """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-m',
                        '--mpu',
                        metavar='PATH',
                        required=True,
                        help='Path to the binary file containing the MPU')
    parser.add_argument('-r',
                        '--reference',
                        metavar='PATH',
                        required=False,
                        help='Path to a .PLY mesh to load as reference')

    # Add more arguments here...
    args = parser.parse_args()

    return args

  args = parse_arg()

  mpu = MPU()
  # /home/gabriel/src/cpp/lts5-face/apps/3DModel/output/mpu.bin
  mpu.load(args.mpu)

  # FIG 1:
  fig1 = plt.figure(num=1)
  ax1 = fig1.add_subplot(111, projection='3d')

  #    mpu.plot_nodes(ax1)
  #    mpu.plot(ax1)
  mpu.plot_octree(ax1)

  if args.reference:
    reference = Mesh()
    # /home/gabriel/src/cpp/lts5-face/apps/3DModel/output/target_fixed_soup.ply
    # /home/gabriel/src/cpp/lts5-face/apps/3DModel/output/marching_cube_mesh.ply
    reference.load(args.reference)
    reference.plot(ax1)

  ax1.set_zlim3d(mpu.bbox_min[2], mpu.bbox_max[2])
  ax1.set_ylim3d(mpu.bbox_min[1], mpu.bbox_max[1])
  ax1.set_xlim3d(mpu.bbox_min[0], mpu.bbox_max[0])

  # FIG 2: First level quadric nodes
  nodes_fig2 = [0b1000, 0b1011]
  fig2 = plt.figure(num=2)
  ax2 = fig2.add_subplot(121, projection='3d')

  mpu.plot_nodes(ax2, with_points=True, with_support_ball=False,
                 id_node=[nodes_fig2[0]])
  ax2.set_title('Node {0[0]:b}'.format(nodes_fig2))

  ax2 = fig2.add_subplot(122, projection='3d')
  mpu.plot_nodes(ax2, with_points=True, with_support_ball=False,
                 id_node=[nodes_fig2[1]])
  ax2.set_title('Node {0[1]:b}'.format(nodes_fig2))

  # FIG 3: Second level quadratic polynomial nodes
  nodes_fig3 = [0b1100, 0b1111]
  fig3 = plt.figure(num=3)
  ax3 = fig3.add_subplot(121, projection='3d')
  mpu.plot_nodes(ax3, with_points=True, with_support_ball=False,
                 id_node=[nodes_fig3[0]])
  ax3.set_title('Node {0[0]:b}'.format(nodes_fig3))

  ax3 = fig3.add_subplot(122, projection='3d')
  mpu.plot_nodes(ax3, with_points=True, with_support_ball=False,
                 id_node=[nodes_fig3[1]])
  ax3.set_title('Node {0[1]:b}'.format(nodes_fig3))

  plt.show()


if __name__ == "__main__":
  test01()
