# coding=utf-8
""" Scaled ICP Registration based on https://arxiv.org/pdf/1705.00086.pdf """
import numpy as _np
from sklearn.neighbors import KDTree
from lts5.geometry import MeshDouble as Mesh
from lts5.geometry.matcher import *
from lts5.geometry.filter import ICPMedianDistanceFilter
from lts5.utils.tools import init_logger

logger = init_logger()


class RunningAverage:

  def __init__(self, window_size=10):
    """
    Constructor
    :param window_size: Window size for averaging
    """
    self._capacity = window_size
    self._size = 0       # Number of value inside accumulator
    self._head = 0       # Index where to insert new data
    self._buffer = _np.zeros((window_size,), dtype=_np.float32)

  def insert(self, value):
    """
    Insert new value inside
    :param value: Value to add in the accumulator
    """
    # Insert data into accumulator
    self._buffer[self._head] = value
    # Update indices
    self._head = (self._head + 1) % self._capacity
    self._size = self._size + 1 if self._size < self._capacity else self._size

  def clear(self):
    """
    Resest accumulator
    """
    self._buffer.fill(0.0)
    self._head = 0
    self._size = 0

  def average(self):
    """
    Compute average value over the accumulated value
    :return:  Average
    """
    if self._size == self._capacity:
      return self._buffer.mean()  # Accumulator full
    elif self._size == 0:
      return 0.0  # Empty accumulator
    else:
      return self._buffer[:self._size].mean()


def _compute_cog_and_max_dims(vertex):
  """
  Compute center of gravity and maximum dims
  :param vertex:  Array of vertices [N, 3]
  :return:  Cog, max_dims
  """
  cog = vertex.mean(axis=0)
  delta = vertex.max(axis=0) - vertex.min(axis=0)
  return cog, delta.max()


def _make_mesh(vertex, tri, name=None):
  m = Mesh()
  m.vertex = vertex
  m.tri = tri
  if name:
    m.save(name)


class SICPRegistration:
  """
  Scaled ICP Registration
  See: https://arxiv.org/pdf/1705.00086.pdf
  """

  def __init__(self,
               source,
               target,
               window_size=5,
               apply_preprocess=True,
               verbose=False):
    """
    Constructor
    :param source:  Source mesh
    :param target:  Target mesh
    :param window_size: Running average accumulator dimension
    :param apply_preprocess: If `True` normalize first the data be removing
                             center of gravity and rescaling to the same size
    :param verbose: If `True` enable output verbose.
    """
    # Get src/tgt vertices
    self._source = source
    self._target = target
    self._preprocess_mesh(source, target, apply_preprocess)
    # Build tree on target
    self.tree = KDTree(self._t_pts)
    self.ra_err = RunningAverage(window_size)
    # Estimated parameters
    self._rmat = _np.eye(3, dtype=_np.float64)
    self._tvec = _np.zeros((3,), dtype=_np.float64)
    self._scale = 1.0
    self._verbose = verbose

  def _preprocess_mesh(self, source, target, preprocess):
    """
    Normalize meshes (i.e. remove mean) and set both surface with similar scale
    :param source:  Source mesh
    :param target:  Target mesh
    :param preprocess: Bool
    """
    # Source, target
    if preprocess:
      self._s_cog, src_delta = _compute_cog_and_max_dims(vertex=source.vertex)
      self._t_cog, tgt_delta = _compute_cog_and_max_dims(vertex=target.vertex)
    else:
      self._s_cog = _np.zeros((3,), dtype=source.vertex.dtype)
      self._t_cog = _np.zeros((3,), dtype=target.vertex.dtype)
      src_delta = 1.0
      tgt_delta = 1.0
    self._s_scale = tgt_delta / src_delta
    self._s_pts = (source.vertex - self._s_cog) * self._s_scale
    self._t_pts = target.vertex - self._t_cog

  def process(self, eps=1e-3, n_iter=100, dmax=100.0):
    """
    Run registration
    :return: Tuple(s, Rmat, Tvec), pts-to-pts error, n_iter
    """

    # Default value
    s = 1.0
    Rmat = _np.identity(3, dtype=_np.float64)
    Tvec = _np.zeros((3,), dtype=_np.float64)

    # Define current surface
    surf = (s * (self._s_pts @ Rmat.T)) + Tvec
    cnt = 0
    pts_to_pts_err = 1e16
    prev_pts_to_pts_err = pts_to_pts_err
    self.ra_err.clear()
    self.ra_err.insert(prev_pts_to_pts_err)
    while self.ra_err.average() > eps and cnt < n_iter:

      # if cnt % 10 == 0:
      #   _make_mesh(surf, self.tri, 'dbg/source_at_{}.obj'.format(cnt))

      # Find correspondence
      # --------------------------------------------
      self._corr = self._find_correspondence(pts=surf, dmax=dmax)
      c_src = self._corr[:, 0]
      c_tgt = self._corr[:, 1]
      # Solve for alignment: s, R, T
      # --------------------------------------------
      Pi = surf[c_src, :]
      Qi = self._t_pts[c_tgt, :]
      Pi_cog = _np.mean(Pi, axis=0)
      Qi_cog = _np.mean(Qi, axis=0)
      Di = Pi - Pi_cog
      Mi = Qi - Qi_cog
      # Rmat
      # --------------------------------------------
      H = _np.zeros((3, 3), dtype=_np.float64)
      for di, mi in zip(Di, Mi):
        H += di.reshape(-1, 1) @ mi.reshape(1, -1)
      H /= Di.shape[0]
      U, S, Vt = _np.linalg.svd(H)
      R_k = Vt.T @ U.T
      # Scale
      # --------------------------------------------
      mt_m = (Mi * Mi).sum() / Mi.shape[0]
      dR_m = (Di * (Mi @ R_k)).sum() / Di.shape[0]
      s_k = mt_m / dR_m
      # Translation
      # --------------------------------------------
      Tvec_k = Qi_cog - (s_k * (Pi @ R_k.T)).mean(axis=0)
      # Update parameters
      Rmat = R_k @ Rmat
      s *= s_k
      Tvec += Tvec_k
      # compute errors
      surf = (s * (self._s_pts @ Rmat.T)) + Tvec
      pts_to_pts_err = _np.mean(_np.linalg.norm(surf[c_src, :] -
                                                self._t_pts[c_tgt, :], axis=1))
      detla_err = _np.abs(pts_to_pts_err - prev_pts_to_pts_err)
      prev_pts_to_pts_err = pts_to_pts_err
      self.ra_err.insert(detla_err)
      cnt += 1
      if self._verbose:
        logger.info('%d/%d Pts-to-pts error: %.6f', cnt, n_iter, pts_to_pts_err)
    # Set parameters
    self._rmat = Rmat
    self._tvec = Tvec
    self._scale = s
    # Done
    return (self.scale,
            self.rotation,
            self.translation), pts_to_pts_err, cnt
  
  @property
  def scale(self):
      return self._scale * self._s_scale

  @property
  def rotation(self):
      return self._rmat
  
  @property
  def translation(self):
      return (self._tvec +
              self._t_cog -
              (self.scale * self.rotation @ self._s_cog))

  @property
  def correspondence(self):
    return self._corr

  def transform(self):
    """
    Apply rigid transformation to the `source` mesh and return the aligned mesh
    :return:  Aligned mesh
    """
    # Perform alignment
    s = self.scale
    Rmat = self.rotation
    Tvec = self.translation
    aligned_src = s * (self._source.vertex @ Rmat.T) + Tvec
    # Create mesh
    m = Mesh()
    m.vertex = aligned_src
    m.tri = self._source.tri
    m.compute_vertex_normal()
    return m

  def _find_correspondence(self, pts, dmax=5.0):
    """
    Find correspondence between the source and the target
    :param pts:         Surface with the current estimation of the alignment
    :param max_radius:  Maximum radius to look for correspondence
    :return:  List of correspondences
    """

    dist, ind = self.tree.query(pts, k=1)
    # Select only the closest one
    selected = (_np.arange(0, pts.shape[0]) if dmax is None else
                _np.where(dist < dmax)[0])
    # Pick selected index: [Source, Target]
    return _np.vstack([selected, ind[selected, 0]]).T


class IterativeClosestPoint:
  """
  Implement ICP Algorithm
  See: https://gfx.cs.princeton.edu/proj/iccv05_course/iccv05_icp_gr.pdf
  """

  def __init__(self,
               selector,
               matcher,
               filter,
               metric):
    """
    Constructor
    :param selector:  Selector instance responcible to select point on the
                      source surface to align
    :param matcher:   Matcher instance finding correspondence on the target
                      surface based on some criterion
    :param filter:    Filter instance, discard pair of point having larger
                      distance than an expected threshold
    :param metric:    ICPMetric instance, error function to minimize.
    """

    self.selector = selector
    self.matcher = matcher
    self.filter = filter
    self.metric = metric
    self.rmat = None
    self.t = None
    self.s = None
    self.corres = None

  def align(self, source, target, n_iter=100):
    """
    Perform rigid alignment
    :param n_iter:  Number of iteration to do
    :param source:  Source mesh to be aligned
    :param target:  Target mesh
    :return:  Error of the minimized loss function at convergence.
    """
    # Define rigid transformation parameters
    self.rmat = _np.copy(self.metric.rotation)
    self.t = _np.copy(self.metric.translation)
    self.s = self.metric.scale
    # Extract data
    p_src = source.vertex
    n_src = source.normal
    p_tgt = target.vertex
    n_tgt = target.normal
    # Do iterations
    costs = [np.inf]
    diff = []
    for k in range(n_iter):
      # Apply estimated transform
      x = (self.s * p_src @ self.rmat.T) + self.t
      nx = n_src @ self.rmat.T
      # Selection
      src_candidate = self.selector.select(x, nx)
      x_select = x[src_candidate, :]
      nx_select = nx[src_candidate, :]
      # Matching
      tgt_candidate = self.matcher.find_correspondence(x_select, nx_select)
      y_select = p_tgt[tgt_candidate, :]
      ny_select = n_tgt[tgt_candidate, :]
      # Reject candidates
      keep_idx = self.filter.filter_correspondence(x_select, y_select,
                                                   nx_select, ny_select)
      self.corres = _np.stack([src_candidate[keep_idx],
                               tgt_candidate[keep_idx]],
                              axis=0)
      x_filter = x_select[keep_idx, :]
      nx_filter = nx_select[keep_idx, :]
      y_filter = y_select[keep_idx, :]
      ny_filter = ny_select[keep_idx, :]
      # Run one step of metric optimization
      self.metric.minimize(x_filter, y_filter, nx_filter, ny_filter)
      # Update estimated rigid parameters
      self.rmat = self.metric.rotation @ self.rmat
      self.t += self.metric.translation
      self.s *= self.metric.scale

      # Error
      x = p_src[src_candidate[keep_idx], :]
      x = self.s * (x @ self.rmat.T) + self.t
      nx = n_src[src_candidate[keep_idx], :]
      nx = nx @ self.rmat.T
      y = p_tgt[tgt_candidate[keep_idx], :]
      ny = n_tgt[tgt_candidate[keep_idx], :]
      d = self.metric.distance(x, y, nx, ny)
      diff.append(costs[-1] - d)
      costs.append(d)
    # Return metric value error
    x = p_src[self.corres[0, :], :]
    x = self.s * (x @ self.rmat.T) + self.t
    nx = n_src[self.corres[0, :], :]
    nx = nx @ self.rmat.T
    y = p_tgt[self.corres[1, :], :]
    ny = n_tgt[self.corres[1, :], :]
    return self.metric.distance(x, y, nx, ny)

  def transform(self, source):
    """
    Apply estimated transformation on a given mesh
    :param source:  Source mesh
    :return:  Aligned mesh
    """
    m = Mesh()
    p = source.vertex
    n = source.normal
    # Transform
    pt = self.s * (p @ self.rmat.T) + self.t
    nt = n @ self.rmat.T
    m.vertex = pt
    m.normal = nt
    m.tri = source.tri
    return m

  @property
  def rotation(self):
    """
    Estimated rotation 3x3 matrix
    :return: np.ndarray
    """
    return self.rmat

  @property
  def translation(self):
    """
    Estimated translation 1x3 matrix
    :return: np.ndarray
    """
    return self.t

  @property
  def scale(self):
    """
    Estimated scaling factor
    :return: float
    """
    return self.s

  @property
  def transformation(self):
    """
    Estimated rigid transformation
    :return:  4x4 Matrix
    """
    trsfrm = _np.zeros((4, 4))
    trsfrm[:3, :3] = self.rmat * self.s
    trsfrm[:3, 3] = self.t
    trsfrm[3, 3] = 1.0
    return trsfrm

  @property
  def correspondences(self):
    """
    Return list of correspondence between source and target surface.
    :return: 2D array, first row == source index, second row == target index
    """
    return self.corres


def _closest_pts_factory(x, y, nx, ny, **kwargs):
  return ICPClosestPointMatcher(y=y)


def _normal_shooting_factory(x, y, nx, ny, **kwargs):
  return ICPNormalShootingMatcher(y=y, **kwargs)


_matcher_ctor = {'closest_point': _closest_pts_factory,
                 'normal_shooting': _normal_shooting_factory}


class RegistrationEvaluator:
  """
  Class implementing protocol to estimated miss-alignment error between two
  surfaces. It finds correspondences in the overlapping region of the two
  meshes and run metric to estimate registration quality.
  """

  def __init__(self,
               matcher,
               metric,
               matcher_kwargs=None):
    """
    Constructor
    :param matcher: str, type of matcher to use to define correspondence.
                    Possible values are: `closest_point` or `normal_shooting`.
    :param metric:  ICPMetric instance to mesure the registration distance
    :param matcher_kwargs: Optional arguments for matcher instance
    """
    if matcher not in _matcher_ctor.keys():
      raise ValueError('Unknown matcher type `{}`, possible values are: '
                       '{}'.format(matcher,
                                   _matcher_ctor.keys()))
    self.matcher_ctor_fn = _matcher_ctor[matcher]
    self.matcher_kwargs = matcher_kwargs or {}
    self.metric = metric
    self.corres = None
    self.per_pts_distance = 0.0
    self.avg_distance = 0.0

  def evaluate(self, x, y, nx, ny, transform=None, error_map=True):
    """
    Run registration evaluation
    :param x:   Source points, Nx3
    :param y:   Target points, Mx3
    :param nx:  Source normals, Nx3
    :param ny:  Target normals, Mx3
    :param transform: Optional 4x4 rigid transformation to apply on source
                      surface before evaluation
    :param error_map: If `True` compute error map for alignment, spatial
      distribution of the error. Set value to -1.0 when there is not match for
      a specific vertex
    :return:  Registration error
    """
    # Apply rigid transform if needed
    if transform is not None:
      rmat = transform[:3, :3]
      t = transform[:3, 3].reshape(1, -3)
      x = x @ rmat.T + t
      nx = nx @ rmat.T
    # Surface are aligned, find correspondences
    ns = x.shape[0]
    nt = y.shape[0]
    p_src = x
    n_src = nx
    p_tgt = y
    n_tgt = ny
    # if ns > nt:
    #   p_src = y
    #   n_src = ny
    #   p_tgt = x
    #   n_tgt = nx
    # else:
    #   p_src = x
    #   n_src = nx
    #   p_tgt = y
    #   n_tgt = ny
    # Build matcher and find correspondence for all pts
    matcher = self.matcher_ctor_fn(p_src,
                                   p_tgt,
                                   n_src,
                                   n_tgt,
                                   **self.matcher_kwargs)
    tgt_candidate = matcher.find_correspondence(p_src, n_src)

    # Measure distance
    x_select = p_src
    nx_select = n_src
    y_select = p_tgt[tgt_candidate, :]
    ny_select = n_tgt[tgt_candidate, :]
    self.corres = np.stack((np.arange(p_src.shape[0]),
                            tgt_candidate),
                           axis=0)
    self.per_pts_distance = self.metric.distance(x=x_select,
                                                 y=y_select,
                                                 nx=nx_select,
                                                 ny=ny_select,
                                                 aggregated=False)

    self.avg_distance = self.per_pts_distance.mean()
    # Error map
    if error_map:
      # ridx = 0 if nt > ns else 1
      e_map = -np.ones(shape=ns)
      idx = self.corres[0, :]
      e_map[idx] = self.per_pts_distance
      return self.avg_distance, e_map
    # Done
    return self.avg_distance


def _register_surface(args):
  """
  Run registration
  :param args:  Parsed arguments
  """

  # Load meshes
  source = Mesh(args.source)
  target = Mesh(args.target)
  # Registrator
  r = SICPRegistration(source=source, target=target)
  r.process(n_iter=200)

  aligned_src = r.transform()
  aligned_src.save('dbg/rigid_aligned.obj')
  target.save('dbg/target.obj')


if __name__ == '__main__':
    from argparse import ArgumentParser

    p = ArgumentParser(description='Isotropic scaled ICP')

    # Inputs
    p.add_argument('--source',
                   type=str,
                   help='Source mesh')
    p.add_argument('--target',
                   type=str,
                   help='Target mesh')
    # Parse
    args = p.parse_args()
    _register_surface(args)



