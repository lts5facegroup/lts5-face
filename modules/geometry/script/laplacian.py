# coding=utf-8
""" Utility function to compute graph laplacian from mesh object """
from scipy.sparse import coo_matrix, diags, eye
import numpy as np
from lts5.geometry import extract_edge_information

__author__ = 'Christophe Ecabert'


class Edge:
  """ Edge between two vertices """

  def __init__(self, v0: int, v1: int, f0: int, f1: int):
    """
    Constructor
    :param v0:  Vertex index of starting point
    :param v1:  Vertex index of ending point
    :param f0:  Face index on one side
    :param f1:  Face index on the other side
    """
    self.v0 = min(v0, v1)
    self.v1 = max(v0, v1)
    self.f0 = f0  # Face index on one side
    self.f1 = f1  # Face index on second side

  def __hash__(self):
    """ Return hash for the edge """
    return hash((self.v0, self.v1))

  def __eq__(self, other):
    if isinstance(other, self.__class__):
      # Ignoring .b attribute
      return self.v0 == other.v0 and self.v1 == other.v1
    else:
      return NotImplemented


def _get_edge_map(tri, n_vertex):
  """
  Build edge map for a given list of triangles
  :param tri:   Array of triangles [N, 3]
  :param n_vertex: Number of vertex in the mesh
  :return:  One-ring neighbour, Set of `Edge`
  """
  edges = []
  v_ring = [[] for _ in range(n_vertex)]
  for k, vertices in enumerate(tri):
    for i in range(3):
      v0 = vertices[i]
      v1 = vertices[(i + 1) % 3]
      edge = Edge(v0, v1, k, -1)
      try:
        idx = edges.index(edge)
        # Edge exists, needs only to update the second face index
        edges[idx].f1 = k
      except ValueError:
        # Add new entry
        edges.append(edge)
        v_ring[v0].append(edge)
        v_ring[v1].append(edge)
  return v_ring, edges


def _get_edges(tri):
  """
  Extract a list of edges from a set of triangles
  :param tri:   Array of triangles [N, 3]
  :return:  Set of edges
  """
  edges = set()
  for v1, v2, v3 in tri:
    edges.add((v1, v2))
    edges.add((v2, v1))
    edges.add((v2, v3))
    edges.add((v3, v2))
    edges.add((v3, v1))
    edges.add((v1, v3))
  return np.asarray([[a, b] for a, b in edges], dtype=np.int32)


def _combinatorial_laplacian(triangles, dtype):
  """
  Compute combinatorial graph laplacian: L = D - A
  :param triangles:  Triangle
  :param dtype:     Data type
  :return:  Laplacian matrix
  """
  # Get edge list
  n_vertex = triangles.max() + 1
  edges = _get_edges(triangles)
  # Build adjacency matrix
  data = np.ones(len(edges), dtype=dtype)
  row = edges[:, 0]
  col = edges[:, 1]
  adj = coo_matrix((data, (row, col)),
                   shape=(n_vertex, n_vertex),
                   dtype=dtype)
  # Build degree matrix
  deg = diags(np.asarray(adj.sum(axis=1)).reshape(-1,))
  # Laplacian: L = D - A
  lap = deg - adj
  return coo_matrix(lap)


def _symmetric_laplacian(triangles, dtype):
  """
  Compute symmetric graph laplacian: L = I - D^-0.5 * A * D^-0.5
  :param trianges:  Array of triangle, [T, 3]
  :param dtype: Data type
  :return:  Laplacian
  """
  # Get edge list
  n_vertex = triangles.max() + 1
  edges = _get_edges(triangles)
  # Build adjacency matrix
  data = np.ones(len(edges), dtype=dtype)
  row = edges[:, 0]
  col = edges[:, 1]
  adj = coo_matrix((data, (row, col)),
                   shape=(n_vertex, n_vertex),
                   dtype=dtype)
  # Degree
  deg = np.asarray(adj.sum(axis=1)).reshape(-1,)
  deg = deg ** -0.5
  deg = diags(deg)
  # Laplacian
  lap = deg.dot(adj).dot(deg)
  lap = eye(n_vertex) - lap
  return coo_matrix(lap)


def _cotan_laplacian(triangles, vertex, dtype):
  """
  Compute cotan graph laplacian:
  :param trianges:  Array of triangle, [T, 3]
  :param vertex: Array of vertex, [N, 3]
  :param dtype: Data type
  :return:  Laplacian
  """
  # Get edge list
  n_vertex = triangles.max() + 1
  edges, vertex_ring = extract_edge_information(triangles, n_vertex)
  # Sparse matrix entry
  ii = []     # Row index
  jj = []     # Col index
  value = []  # Value
  for i in range(n_vertex):
    neighbors = [edges[k] for k in vertex_ring[i]]
    indices = list(map(lambda x: x.v0 if x.v0 != i else x.v1, neighbors))
    weights = []
    z = len(indices)
    ii += [i] * (z + 1)     # repeated row
    jj += indices + [i]     # column indices and this row
    for j in range(z):
      neighbor = neighbors[j]
      faces = [neighbor.f0, neighbor.f1]
      cotangents = []
      for f in faces:
        if f != -1:
          v_other = list(filter(lambda v: v not in [neighbor.v0, neighbor.v1],
                                triangles[f, :]))
          assert len(v_other) == 1, 'Should only have a single vertex'
          v_other = vertex[v_other[0], :]
          end_edge = neighbor.v0 if i != neighbor.v0 else neighbor.v1
          (u, v) = (vertex[i] - v_other, vertex[end_edge] - v_other)
          cotangents.append(np.dot(u, v) / np.sqrt(np.sum(np.square(np.cross(u, v)))))
      # cotangent weights
      weights.append(-1 / len(cotangents) * np.sum(cotangents))
    # n negative weights and row vertex sum
    value += weights + [(-1 * np.sum(weights))]

  # Build laplacian
  return coo_matrix((value, (ii, jj)), shape=(n_vertex, n_vertex), dtype=dtype)


def graph_laplacian(triangle, type='combinatorial', dtype=np.float32, **kwargs):
  """
  Compute graph laplacian for a given mesh
  :param triangle: ndarray of dimensions [T, 3] where `T` is the number of
                   triangles
  :param type:  Laplacian type, 'combinatorial', 'symmetric' or 'cotan'
  :param dtype: Data type
  :param kwargs: extra keyword arguments
  :keyword vertex: Array of vertex needed to compute cotan laplacian, [N, 3]
  :return:  Laplacian as sparse matrix
  """
  if type == 'combinatorial':
    return _combinatorial_laplacian(triangle, dtype)
  elif type == 'symmetric':
    return _symmetric_laplacian(triangle, dtype)
  elif type == 'cotan':
    vertex = kwargs.get('vertex')
    return _cotan_laplacian(triangle, vertex, dtype)
  else:
    raise RuntimeError('Unknown type of laplacian: {}'.format(type))

