# coding=utf-8
"""
Implement pair matching for ICP algorithm:
  - ClosestPoint
  - NormalCompatibility

Based on:
 - https://gfx.cs.princeton.edu/proj/iccv05_course/iccv05_icp_gr.pdf
 - http://www.hao-li.com/cs599-ss2015/slides/Lecture05.1.pdf
 - https://link.springer.com/article/10.1007/s10514-013-9327-2
"""
import numpy as np
from sklearn.neighbors import KDTree


__author__ = 'Christophe Ecabert'


class ICPMatcher:
  """ Interface for pair matching """

  def find_correspondence(self, x, nx):
    """
    Find correspondence for a give set of points / normals
    :param x:   Source points to be matched, Nx3
    :param nx:  Source normals if needed, Nx3
    :return:  List of index on the target surface
    """
    raise NotImplementedError('Must be implemented by subclass')


class ICPClosestPointMatcher(ICPMatcher):
  """ Match point based on distance """

  def __init__(self, y):
    """
    Constructor
    :param y: Targets surface points
    """
    self.tree = KDTree(y)

  def find_correspondence(self, x, nx):
    """
    Find correspondence for a give set of points / normals
    :param x:   Source points to be matched, Nx3
    :param nx:  Source normals if needed, Nx3
    :return:  List of index on the target surface
    """
    dist, idx = self.tree.query(x, k=1, return_distance=True)
    return idx.ravel()


class ICPNormalShootingMatcher(ICPMatcher):
  """
  Match points based on distance to normals computed on the input surface
  based on: pcl::registration::CorrespondenceEstimationNormalShooting
  """

  def __init__(self, y, k_neighbour=10):
    """
    Constructor
    :param y:  Target point, Nx3
    :param k_neighbour: Number of closest neighbour to look for while computing
                        distance.
    """
    self.tree = KDTree(y)
    self.k = k_neighbour
    # Save ref to target points, needed to estimate distance later on.
    self.p_tgt = y

  def find_correspondence(self, x, nx):
    """
    Find correspondence for a give set of points / normals
    :param x:   Source points to be matched, Nx3
    :param nx:  Source normals if needed, Nx3
    :return:  List of index on the target surface
    """
    # Find K closest point
    candidates = self.tree.query(x,
                                 k=self.k,
                                 return_distance=False)
    # Among the K nearest neighbours find the one with minimum perpendicular
    # distance to the normal
    # http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html
    dist = self._compute_distance_to_normal(candidates, x, nx)
    # Pick smallest on
    min_dist_idx = np.argmin(dist, axis=1)
    index = np.take_along_axis(candidates,
                               np.expand_dims(min_dist_idx, axis=-1),
                               axis=-1)
    return index.ravel()

  def _compute_distance_to_normal(self, neighbours, p_src, n_src):
    """
    Compute distance between line formed by normals and a list of neighbours
    :param neighbours:  K closest neighbours
    :param p_src:       Query point on source
    :param n_src:       Query point's normal of the source
    :return:  Distance
    """
    pt = self.p_tgt[neighbours, :]    # [M, 3] -> [N, K, 3]
    ps = p_src.reshape(-1, 1, 3)      # [N, 3] -> [N, 1, 3]
    # Line between target pts and source pts
    V = pt - ps
    # Normals
    N = n_src.reshape(-1, 1, 3)
    #
    C = np.cross(N, V, axis=-1)   # [N, K, 3]
    dist = (C * C).sum(axis=-1)   # [N, K]
    return dist
