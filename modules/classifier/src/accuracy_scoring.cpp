/**
 *  @file   accuracy_scoring.cpp
 *  @brief  Compute accuracy for a given binary classification system
 *
 *  @author Christophe Ecabert
 *  @date   20/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/classifier/accuracy_scoring.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  AccuracyScoring
 * @fn  explicit AccuracyScoring(const int k_fold)
 * @brief Constructor
 * @param[in] n_fold  Number of fold to score
 */
AccuracyScoring::AccuracyScoring(const int n_fold) : BaseScoringMetric(n_fold) {
}

#pragma mark -
#pragma mark Compute

/*
 * @name  Compute
 * @fn    double Compute(const int k_fold)
 * @brief Compute scoring metric for a given fold
 * @param[in] k_fold  Selected fold
 * @return  Score
 */
double AccuracyScoring::Compute(const int k_fold) {
  // Compute accuracy
  return (static_cast<double>(tp_[k_fold] + tn_[k_fold]) /
          static_cast<double>(tp_[k_fold] +
                              tn_[k_fold] +
                              fp_[k_fold] +
                              fn_[k_fold]));
}
}  // namepsace LTS5