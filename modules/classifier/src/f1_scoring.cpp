/**
 *  @file   f1_scoring.cpp
 *  @brief  Compute F1 score for a given binary classification system
 *
 *  @author Christophe Ecabert
 *  @date   20/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/classifier/f1_scoring.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  F1Scoring
 * @fn  explicit F1Scoring(const int k_fold)
 * @brief Constructor
 * @param[in] n_fold  Number of fold to score
 */
F1Scoring::F1Scoring(const int n_fold) : BaseScoringMetric(n_fold) {}

#pragma mark -
#pragma mark Compute

/*
 * @name  Compute
 * @fn    double Compute(void)
 * @brief Compute F1 score
 * @param[in] k_fold  Selected fold
 * @return  F1 score
 */
double F1Scoring::Compute(const int k_fold) {
  // Compute f1
  double precision = (static_cast<double>(tp_[k_fold]) /
                      static_cast<double>(tp_[k_fold] + fp_[k_fold]));
  double recall = (static_cast<double>(tp_[k_fold]) /
                   static_cast<double>(tp_[k_fold] + fn_[k_fold]));
  return 2.0 * (precision * recall) / (precision + recall);
}
}  // namepsace LTS5