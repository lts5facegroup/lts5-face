/**
 *  @file   binary_RF_classifier.cpp
 *  @brief  Binary OpenCV Random Forest classifier warper
 *
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/ml/ml.hpp"
#include "lts5/classifier/binary_RF_classifier.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

void BinaryRFClassifier::Vec1DMat22DMat(const std::vector<cv::Mat>& vec,
                                        cv::Mat* mat) {
  assert(vec.size() > 0);
  assert(vec[0].cols == 1 || vec[0].rows == 1);

  // Convert vectors of 1D Mat to 2D Mat
  int row = static_cast<int>(vec.size());
  int col = vec[0].rows * vec[0].cols;
  int data_type = vec[0].type();
  mat->create(row, col, data_type);

  for (int r = 0 ; r < row; ++r) {
    cv::Mat row_data = vec[r];
    for (int c = 0; c < col; ++c) {
      switch (data_type) {
        case CV_32F:
          mat->at<float>(r,c) = row_data.at<float>(c);
          break;

        case CV_64F:
          mat->at<double>(r,c) = row_data.at<double>(c);

        default:
          break;
      }
    }
  }
}

/*
 *  @name   Vec2Mat
 *  @brief  Utility method to convert 2 vectors of 1D Mat (pos and neg samples
 *          into a 2D Mat of data (rows are samples, columns are features) and
 *          a 1D Mat of labels
 *  @param[in] positive_samples Positive samples
 *  @param[in] negative_samples Negative samples
 *  @param[out] data   2D Mat containing the data
 *  @param[out] labels 1D Mat containing the labels of the data
 */
void BinaryRFClassifier::Vec2Mat(const std::vector<cv::Mat>& positive_samples,
                                 const std::vector<cv::Mat>& negative_samples,
                                 cv::Mat* data, cv::Mat* labels) {
  assert(positive_samples.size() > 0 && negative_samples.size() > 0);
  assert(positive_samples[0].cols == 1 || positive_samples[0].rows == 1);
  assert(negative_samples[0].cols == 1 || negative_samples[0].rows == 1);
  assert(positive_samples[0].rows == negative_samples[0].rows &&
         positive_samples[0].cols == negative_samples[0].cols);
  assert(positive_samples[0].type() == negative_samples[0].type());

  bool convert = false;
  if (positive_samples[0].type() != CV_32F &&
      positive_samples[0].type() != CV_32S) {
    std::cout << "WARNING: The data type needs to be converted..." << std::endl;
    convert = true;
  }

  // Dimensions
  const int k_samples = static_cast<int>(positive_samples.size() +
                                         negative_samples.size());
  const int k_features = static_cast<int>(positive_samples[0].rows *
                                          positive_samples[0].cols);

  cv::Mat pos_train_data;
  cv::Mat neg_train_data;
  Vec1DMat22DMat(positive_samples, &pos_train_data);
  Vec1DMat22DMat(negative_samples, &neg_train_data);
  assert(k_samples == pos_train_data.rows + neg_train_data.rows);

  if (convert) {
    pos_train_data.convertTo(pos_train_data, CV_32FC1);
    neg_train_data.convertTo(neg_train_data, CV_32FC1);
  }

  data->create(k_samples, k_features, CV_32FC1);

  pos_train_data.copyTo(data->rowRange(0, pos_train_data.rows));
  neg_train_data.copyTo(data->rowRange(pos_train_data.rows,
                                       data->rows));

  cv::Mat pos_labels = cv::Mat::ones(pos_train_data.rows, 1, CV_32FC1);
  cv::Mat neg_labels = cv::Mat::zeros(neg_train_data.rows, 1, CV_32FC1);

  labels->create(k_samples, 1, CV_32FC1);
  pos_labels.copyTo(labels->rowRange(0, pos_labels.rows));
  neg_labels.copyTo(labels->rowRange(pos_labels.rows, labels->rows));
}

/*
 *  @name BinaryRFClassifier
 *  @brief Constructor
 */
  BinaryRFClassifier::BinaryRFClassifier(void) : BinaryClassifier() {
  classifier_ = new CvRTrees();
}

/*
 *  @name BinaryRFClassifier
 *  @brief Copy constructor
 */
BinaryRFClassifier::BinaryRFClassifier(const BinaryRFClassifier& other)
: BinaryClassifier(), classifier_(new CvRTrees()) {
  *classifier_ = *(other.classifier_);
  parameters_ = other.parameters_;
}

/*
 *  @name ~BinaryRFClassifier
 *  @brief Destructor
 */
BinaryRFClassifier::~BinaryRFClassifier(void) {
  delete classifier_;
}

/*
 *  @name   Train
 *  @brief  Train binary classifier
 *  @param[in] positive_samples Positive samples
 *  @param[in] negative_samples Negative samples
 */
void BinaryRFClassifier::Train(const std::vector<cv::Mat>& positive_samples,
                               const std::vector<cv::Mat>& negative_samples) {
  cv::Mat train_data;
  cv::Mat labels;
  Vec2Mat(positive_samples, negative_samples, &train_data, &labels);

  if (tflag_ == CV_COL_SAMPLE) {
    cv::transpose(train_data, train_data);
  }

  cv::Mat empty;
  classifier_->train(train_data, tflag_, labels, empty, empty, empty, empty,
                     parameters_);
}

/*
 *  @name   Classify
 *  @brief  Classify new sample
 *
 * This function returns the classification result for a new sample.
 * The sign of the returned value is negative for negative
 * predictions and positive for positive predictions.
 *
 *  @param[in] x Sample to classify
 *  @return Classification result
 */
double BinaryRFClassifier::Classify(const cv::Mat& x) const {
  return static_cast<double>(classifier_->predict(x));
}

/*
 *  @name   Clone
 *  @brief  Clone classifier.
 *  @return Deep copy of classifier
 */
BinaryClassifier<cv::Mat>* BinaryRFClassifier::Clone() const {
  return new BinaryRFClassifier(*this);
}

/*
 *  @name   Load
 *  @brief  Load classifier from file.
 *  @param[in] infile File to load the classifier from.
 */
int BinaryRFClassifier::Load(const std::string& infile) {
  classifier_->load(infile.c_str());
  return 0;
}

/*
 *  @name   Save
 *  @brief  Save classifier to file.
 *  @param[in] outfile File to save the classifier to.
 */
int BinaryRFClassifier::Save(const std::string& outfile) const {
  classifier_->save(outfile.c_str());
  return 0;
}
}  // namespace LTS5