/**
 *  @file   pca.cpp
 *  @brief  PCA methods implementation
 *
 *  @author Gabriel Cuendet
 *  @date   04/06/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

// Macro before cula includes to avoid MSVC C2719 error
#ifdef HAS_CULA_
  #ifdef _WIN32
    #define CULA_USE_CUDA_COMPLEX
  #endif
  #include <cula_lapack.h>
  #include <cula_lapack_device.h>
  #include <cuda_runtime.h>
#endif

#include <typeinfo>
#include <iostream>
#include <string>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/classifier/pca.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
template<typename T> static void ComputePcaLapack(const cv::Mat& data,
                                                  const double variance,
                                                  cv::Mat* eigenvectors,
                                                  cv::Mat* eigenvalues);
template<typename T> static void ComputePcaCula(const cv::Mat& data,
                                                const double variance,
                                                cv::Mat* eigenvectors,
                                                cv::Mat* eigenvalues);
static void ComputePcaOpencv(const cv::Mat& data,
                             const double variance,
                             cv::Mat* eigenvectors,
                             cv::Mat* eigenvalues);

/*
 *  @name ComputePcaLapack
 *  @brief Uses Lapack symetric eigenvalue decomposition to compute PCA
 *  @param[in]    data          Matrix of data. Rows are samples, cols are
 *                              features (or dimensions)
 *  @param[in]    variance      Percentage of variance to keep in ]0.-1.]
 *  @param[out]   eigenvectors  PCA basis, matrix of eigenvectors
 *  @param[out]   eigenvalues   Corresponding eigenvalues, if null not exported
 */
template<typename T>
void ComputePcaLapack(const cv::Mat& data,
                      const double variance,
                      cv::Mat* eigenvectors,
                      cv::Mat* eigenvalues) {
  using TransposeType = typename LinearAlgebra<T>::TransposeType;
  assert(typeid(T) == typeid(float) || typeid(T) == typeid(double));

  const int kType = cv::DataType<T>::type;
  const int kSamples = data.rows;
  const int kDimensions = data.cols;

  // col-vector, will be transpose in cblas_sger (mandatory)
  cv::Mat means(kDimensions, 1, kType);
  for (int i = 0; i < kDimensions; ++i) {
    means.at<T>(i) = cv::mean(data.col(i)).val[0];
  }

  cv::Mat cov_mat(data.cols, data.cols, kType);


  // Option 2 -> Lapack (600-700 s)
  cv::Mat y = cv::Mat::ones(kSamples, 1, kType);

  // Remove mean from sift in order to compute covariance matrix
  const T alpha1 = -1.0;
  cv::Mat data_no_mean = data.clone();
  LTS5::LinearAlgebra<T>::Ger(y, means, alpha1, &data_no_mean);
  // Compute sift covariance matrix
  const T alpha2 = 1.0 / static_cast<T>(data_no_mean.rows);
  const T beta = 0.0;
  LTS5::LinearAlgebra<T>::Gemm(data_no_mean,
                               TransposeType::kTranspose, alpha2,
                               data_no_mean,
                               TransposeType::kNoTranspose, beta,
                               &cov_mat);

  // Compute Eigenspace in order to do PCA
  cv::Mat eigenval(data_no_mean.cols, 1, kType);
  typename LTS5::LinearAlgebra<T>::SymEigenDecomposition sym_eig;
  sym_eig.InitFullComputation(cov_mat);
  sym_eig.Compute();
  sym_eig.GetEigenvalues(&eigenval);

  // Retain only a certain amount of variance
  double total = cv::sum(eigenval).val[0];
  double sum = 0.0;
  int keep_n_eigs = 0;
  if (variance >= 0.0) {
    for (int i = 0; i < eigenval.rows; i++) {
      sum += eigenval.at<T>(i, 0);
      keep_n_eigs++;
      if (sum / total >= variance) {
        break;
      }
    }
  } else {
    keep_n_eigs = static_cast<int>(-variance);
  }
  eigenvectors->create(keep_n_eigs, kDimensions, kType);
  cv::Mat all_eigenvectors;
  sym_eig.GetEigenvectors(&all_eigenvectors);
  *eigenvectors = all_eigenvectors.rowRange(0, keep_n_eigs).clone();
  // Copy eigen value if needed
  if (eigenvalues) {
    eigenval.copyTo(*eigenvalues);
  }
}

#ifdef HAS_CULA_
void CheckCulaStatus(const culaStatus& status) {
  char buf[256];

  if (!status) {
    return;
  }

  std::cout << "CULA error: " << status << std::endl;
  culaGetErrorInfoString(status, culaGetErrorInfo(), buf, sizeof(buf));
  std::cout << buf << std::endl;

  culaShutdown();
  exit(EXIT_FAILURE);
}

void CheckCudaError(const cudaError_t& err) {
  if (!err) {
    return;
  }

  std::cout << "CUDA error: " << err << std::endl;
  std::cout << cudaGetErrorString(err) << std::endl;

  culaShutdown();
  exit(EXIT_FAILURE);
}

void CulaSyev(cv::Mat* A, cv::Mat* W) {
  assert(A->rows == A->cols);
  char jobz = 'V';  // 'V': Compute eigenvalues and eigenvectors
  char uplo = 'U';  // 'U': Upper triangle of A is stored
  int n = A->rows;
  int lda = n;
  W->create(1, n, CV_32F);

  culaStatus status;
  cudaError_t error;

  // Host memory
  float* Ap = A->ptr<float>();
  float* Wp = W->ptr<float>();
  // Device memory
  float* dAp;
  error = cudaMalloc(reinterpret_cast<void**>(&dAp), n * n * sizeof(float));
  CheckCudaError(error);
  float* dWp;
  error = cudaMalloc(reinterpret_cast<void**>(&dWp), 1 * n * sizeof(float));
  CheckCudaError(error);
  // Copy memory
  error = cudaMemcpy(dAp, Ap, n * n * sizeof(float), cudaMemcpyHostToDevice);
  CheckCudaError(error);

  // CULA Syev
  status = culaInitialize();
  CheckCulaStatus(status);
  status = culaDeviceSsyev(jobz, uplo, n, dAp, lda, dWp);
  CheckCulaStatus(status);

  // Copy result back
  error = cudaMemcpy(Ap, dAp, n * n * sizeof(float), cudaMemcpyDeviceToHost);
  CheckCudaError(error);
  error = cudaMemcpy(Wp, dWp, 1 * n * sizeof(float), cudaMemcpyDeviceToHost);
  CheckCudaError(error);

  // Clean up
  error = cudaFree(dAp);
  CheckCudaError(error);
  error = cudaFree(dWp);
  CheckCudaError(error);
  culaShutdown();
}

/*
 *  @name ComputePcaCula
 *  @brief Uses CULA symetric eigenvalue decomposition to compute PCA
 *  @param[in]    data          Matrix of data. Rows are samples, cols are
 *                              features (or dimensions)
 *  @param[in]    variance      Percentage of variance to keep in ]0.-1.]
 *                              if < 0 then define the number of eigenvector to
 *                              keep
 *  @param[out]   eigenvectors  PCA basis, matrix of eigenvectors
 *  @param[out]   eigenvalues   Corresponding eigenvalues, if null not exported
 */
template<typename T>
void ComputePcaCula(const cv::Mat& data,
                    const double variance,
                    cv::Mat* eigenvectors,
                    cv::Mat* eigenvalues) {
  assert(typeid(T) == typeid(float) || typeid(T) == typeid(double));

  const int kType = cv::DataType<T>::type;
  const int kSamples = data.rows;
  const int kDimensions = data.cols;

  // col-vector, will be transpose in cblas_sger (mandatory)
  cv::Mat means(kDimensions, 1, kType);
  for (int i = 0; i < kDimensions; ++i) {
    means.at<T>(i) = cv::mean(data.col(i)).val[0];
  }

  cv::Mat cov_mat(data.cols, data.cols, kType);

  cv::Mat y = cv::Mat::ones(kSamples, 1, kType);
  // Remove mean from sift in order to compute covariance matrix
  const T alpha1 = -1.0;
  cv::Mat data_no_mean = data.clone();
  LTS5::LinearAlgebra::Ger<T>(y, means, alpha1, &data_no_mean);
  // Compute sift covariance matrix
  const T alpha2 = 1.0 / static_cast<T>(data_no_mean.rows);
  const T beta = 0.0;
  LTS5::LinearAlgebra::Gemm<T>(data_no_mean,
                               CBLAS_TRANSPOSE::CblasTrans, alpha2,
                               data_no_mean,
                               CBLAS_TRANSPOSE::CblasNoTrans, beta,
                               &cov_mat);

  cv::Mat eigVals;

  // FIRST CONVERT TO FLOAT
  cov_mat.convertTo(cov_mat, CV_32F);
  CulaSyev(&cov_mat, &eigVals);

  double total = cv::sum(eigVals).val[0];
  double sum = 0.0;
  int nEigs = 0;
  if (variance >= 0.0) {
    for (int i = 0; i < eigVals.cols; i++) {
      double eigVal = eigVals.at<float>(0, eigVals.cols - 1 - i);
      sum += eigVal;
      nEigs++;
      if (sum / total >= variance) {
        break;
      }
    }
  } else {
    nEigs = static_cast<int>(-variance);
  }

  eigenvectors->create(nEigs, kDimensions, kType);
  for (int i = 0; i < nEigs; i++) {
    // CONVERT BACK TO DOUBLE
    cov_mat.row(cov_mat.rows - 1 - i).convertTo(eigenvectors->row(i),
                                                kType);
  }
  // Copy eigen value if needed
  if (eigenvalues) {
    eigenvalues->create(eigVals.cols, 1, kType);
    for (int i = 0; i < eigVals.cols; ++i) {
      eigenvalues->at<T>(i) = static_cast<T>(eigVals.at<float>(eigVals.cols - 1 - i));
    }
  }
}
#endif

/*
 *  @name ComputePcaOpencv
 *  @brief Uses OpenCV PCA class to compute PCA (slow...)
 *  @param[in]    data          Matrix of data. Rows are samples, cols are
 *                              features (or dimensions)
 *  @param[in]    variance      Percentage of variance to keep in ]0.-1.]
 *  @param[out]   eigenvectors  PCA basis, matrix of eigenvectors
 *  @param[out]   eigenvalues   Corresponding eigenvalues, if null not exported
 */
void ComputePcaOpencv(const cv::Mat& data,
                      const double variance,
                      cv::Mat* eigenvectors,
                      cv::Mat* eigenvalues) {
  cv::PCA pca;
  if (variance > 0.0) {
    pca(data, cv::Mat(), cv::PCA::DATA_AS_ROW, variance);

  } else {
    int n_eigenvector = static_cast<int>(-variance);
    pca(data, cv::Mat(), cv::PCA::DATA_AS_ROW, n_eigenvector);
  }
  pca.eigenvectors.copyTo(*eigenvectors);
  if (eigenvalues) {
    pca.eigenvalues.copyTo(*eigenvalues);
  }
}

/*
 *  @name ComputePca
 *  @brief Calls the appropriate implementation of PCA
 *  @param[in]    data          Matrix of data. Rows are samples, cols are
 *                              features (or dimensions)
 *  @param[in]    variance      Percentage of variance to keep in ]0.-1.]
 *  @param[in]    method        Computation method
 *  @param[out]   eigenvectors  PCA basis, matrix of eigenvectors
 *  @param[out] eigenvalues   Corresponding eigenvalues, if null not exported
 */
void ComputePca(const cv::Mat& data,
                const double variance,
                const PcaComputationType method,
                cv::Mat* eigenvectors,
                cv::Mat* eigenvalues) {
  // @TODO: Here we can check which method we should use...
  // Depending on availability of GPU, Memory, etc...
  switch (method) {
    case kDefault:
      ComputePcaOpencv(data, variance, eigenvectors, eigenvalues);
      break;

    case kLapack:
    {
      if (data.type() == CV_32F) {
        ComputePcaLapack<float>(data, variance, eigenvectors, eigenvalues);
      } else if (data.type() == CV_64F) {
        ComputePcaLapack<double>(data, variance, eigenvectors, eigenvalues);
      } else {
        std::cout << "The type of the matrix data should be float or double" <<
        std::endl;
      }
    }
      break;

#ifdef HAS_CULA_
    case kCula:
    {
      // @TODO: Check that it is possible to use the GPU
      if (data.type() == CV_32F) {
        ComputePcaCula<float>(data, variance, eigenvectors, eigenvalues);
      } else if (data.type() == CV_64F) {
        ComputePcaCula<double>(data, variance, eigenvectors, eigenvalues);
      } else {
        std::cerr << "The type of the matrix data should be float or double" <<
        std::endl;
      }
    }
      break;
#endif

    default : throw std::runtime_error("Wrong type of computation (" +
                                       std::string(FUNC_NAME) + ")");
      break;
  }
}

  /*
   *  @name ComputePca
   *  @brief Calls the appropriate implementation of PCA
   *  @param[in]    data          Matrix of data. Rows are samples, cols are
   *                              features (or dimensions)
   *  @param[in]    n_eigenvector Number of eigenvector to keep
   *  @param[in]    method        Computation method
   *  @param[out]   eigenvectors  PCA basis, matrix of eigenvectors
   *  @param[out]   eigenvalues   Corresponding eigenvalues, if null not exported
   */
  void ComputePca(const cv::Mat& data,
                  const int n_eigenvector,
                  const PcaComputationType method,
                  cv::Mat* eigenvectors,
                  cv::Mat* eigenvalues) {
    double variance = - 1.0 * static_cast<double>(n_eigenvector);
    LTS5::ComputePca(data, variance, method, eigenvectors, eigenvalues);
  }
}  // namespace LTS5
