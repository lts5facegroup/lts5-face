/**
 *  @file   binarylinearsvmclassifier.cpp
 *  @brief  Binary linear SVM classifier implementation
 *  @author Hua Gao
 *  @date   23/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#include <iostream>
#include <stdexcept>

#include "linear.h"

#include "lts5/classifier/binary_sparse_linear_svm_classifier.hpp"
#include "lts5/utils/file_io.hpp"

/*
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#define get(m, r, c) GetMatrixValue(r, c, m)

  struct BinarySparseLinearSVMClassifier::Impl {
    LINEAR::parameter           parameters;
    LINEAR::model*              mod;
    int                 r;
    int                 c;
    int                 dim;
    double              bias;
    std::vector<int>         labels;
    std::vector<double>      weights;
    cv::Mat             w;
    double              wbias;

    /*
     *  @name  Impl
     *  @brief Constructor
     */
    Impl()
    : mod(0), r(0), c(0), dim(0), bias(0), wbias(0)
    {}

    /*
     *  @name  cacheW
     *  @brief Copy model weights
     */
    void cacheW() {
      if ((mod != nullptr) &&
          (mod->nr_class != 2 || mod->param.solver_type == LINEAR::MCSVM_CS)) {
        throw std::runtime_error("Only two-class SVMs supported!");
      }
      bias = mod->bias;
      // create implicitly w if not already allocated
      // and copy data from mod->w
      if (mod) {
        cv::Mat(1, mod->nr_feature, CV_64FC1, mod->w).copyTo(w);
      }
      // Bias ?
      if (bias >= 0)
        wbias = bias * mod->w[mod->nr_feature];
    }
  };

/*
 *  @name   BinarySparseLinearSVMClassifier
 *  @brief  Constructor
 *  @param[in] C Value of the slack parameter C
 *  @param[in] type Type of SVM solver
 *  @param[in] eps Stopping criterion threshold
 *  @param[in] positive_weight Penalty weight for positive misclassifications
 *  @param[in] negative_weight Penalty weight for negative misclassifications
 *  @param[in] bias Bias value to use
 */
BinarySparseLinearSVMClassifier::
BinarySparseLinearSVMClassifier(double C,
                                BinaryLinearSVMTraining type,
                                double eps,
                                double positive_weight,
                                double negative_weight,
                                double bias)
  : _pi(new Impl) {
  switch (type)
  {
    case L2_LR:
    case L2R_LR:
      _pi->parameters.solver_type = LINEAR::L2R_LR;
      break;
    case L2LOSS_SVM_DUAL:
    case L2R_L2LOSS_SVC_DUAL:
      _pi->parameters.solver_type = LINEAR::L2R_L2LOSS_SVC_DUAL;
      break;
    case L2LOSS_SVM:
    case L2R_L2LOSS_SVC:
      _pi->parameters.solver_type = LINEAR::L2R_L2LOSS_SVC;
      break;
    case L1LOSS_SVM_DUAL:
    case L2R_L1LOSS_SVC_DUAL:
      _pi->parameters.solver_type = LINEAR::L2R_L1LOSS_SVC_DUAL;
      break;
    case L1R_L2LOSS_SVC:
      _pi->parameters.solver_type = LINEAR::L1R_L2LOSS_SVC;
      break;
    case L1R_LR:
      _pi->parameters.solver_type = LINEAR::L1R_LR;
      break;
    default:
      throw std::invalid_argument("Invalid SVM solver type");
  }

  _pi->parameters.C   = C;
  _pi->parameters.eps = eps;
  _pi->bias           = bias;
  if (positive_weight == 1.0 && negative_weight == 1.0) {
    _pi->parameters.nr_weight       = 0;
    _pi->parameters.weight          = 0;
    _pi->parameters.weight_label    = 0;
  }
  else {
    _pi->labels.resize(2);
    _pi->weights.resize(2);
    _pi->labels[0] = 1;
    _pi->labels[1] = -1;
    _pi->weights[0] = positive_weight;
    _pi->weights[1] = negative_weight;

    _pi->parameters.nr_weight       = 2;
    _pi->parameters.weight          = &_pi->weights[0];
    _pi->parameters.weight_label    = &_pi->labels[0];
  }
}

/*
 *  @name   BinarySparseLinearSVMClassifier
 *  @brief  Copy constructor
 *  @fn BinarySparseLinearSVMClassifier(const BinarySparseLinearSVMClassifier& other)
 */
BinarySparseLinearSVMClassifier::
BinarySparseLinearSVMClassifier(const BinarySparseLinearSVMClassifier& other)
: BinaryClassifier(),
  _pi(new Impl) {
  *_pi = *other._pi;
  _pi->mod = LINEAR::clone_model(other._pi->mod);
  _pi->w = other._pi->w.clone();
  if (_pi->parameters.nr_weight != 0) {
    _pi->parameters.weight          = &_pi->weights[0];
    _pi->parameters.weight_label    = &_pi->labels[0];
  }
}

/*
 *  @name   BinarySparseLinearSVMClassifier
 *  @brief  Assignment operator
 *  @fn BinarySparseLinearSVMClassifier& operator = (const BinarySparseLinearSVMClassifier& other)
 */
BinarySparseLinearSVMClassifier& BinarySparseLinearSVMClassifier
::operator=(const BinarySparseLinearSVMClassifier& other) {
  if (_pi->mod)
    destroy_model(_pi->mod);

  *_pi = *other._pi;
  _pi->mod = LINEAR::clone_model(other._pi->mod);
  _pi->w = other._pi->w.clone();
  if (_pi->parameters.nr_weight != 0) {
    _pi->parameters.weight          = &_pi->weights[0];
    _pi->parameters.weight_label    = &_pi->labels[0];
  }
  return *this;
}

/*
 *  @name ~BinarySparseLinearSVMClassifier
 *  @brief  Destructor
 *  @fn ~BinarySparseLinearSVMClassifier()
 */
BinarySparseLinearSVMClassifier::~BinarySparseLinearSVMClassifier() {
  if (_pi->mod)
    LINEAR::destroy_model(_pi->mod);;
}

/*
 *  @name  Train
 *  @brief Train binary SVM
 *  @param[in] positive_samples Positive samples
 *  @param[in] negative_samples Negative samples
 */
void BinarySparseLinearSVMClassifier::
Train(const std::vector<cv::SparseMat>& positive_samples,
      const std::vector<cv::SparseMat>& negative_samples) {
  size_t length = positive_samples.size() + negative_samples.size();

  if (positive_samples.empty() || negative_samples.empty())
    throw std::invalid_argument("Both positive and negative samples are needed!");

  const int* dims = positive_samples[0].size();
  const size_t nz_count = positive_samples[0].nzcount();
  _pi->r = dims[0];
  _pi->c = dims[1];
  _pi->dim = std::max(_pi->r, _pi->c);
  bool row = _pi->r == 1;

  if (_pi->r != 1 && _pi->c != 1)
    throw std::invalid_argument("Only vectors supported");

  for (size_t i = 0; i < positive_samples.size(); ++i) {
    dims = positive_samples[i].size();
    const size_t nz = positive_samples[i].nzcount();
    if (dims[0] != _pi->r || dims[1] != _pi->c || nz != nz_count)
      throw std::invalid_argument("All training vectors must have same size!");
  }

  for (size_t i = 0; i<negative_samples.size(); ++i) {
    dims = negative_samples[i].size();
    const size_t nz = negative_samples[i].nzcount();
    if (dims[0] != _pi->r || dims[1] != _pi->c || nz != nz_count)
      throw std::invalid_argument("All training vectors must have same size!");
  }

  std::vector<int> y(length);
  std::vector<std::vector<LINEAR::feature_node> > vecs(length);
  std::vector<LINEAR::feature_node*> x(length);

  bool has_bias = _pi->bias >= 0;
  size_t sz = nz_count + 1;//static_cast<size_t>(_pi->dim + 1);
  if (has_bias)
    ++sz;

  int offset = 0;
  for (size_t i=0; i<positive_samples.size(); ++i) {
    vecs[offset].resize(sz);
    auto it = positive_samples[i].begin();
    for (int j = 0; j < nz_count; ++j) {
      const auto* node = it.node();
      vecs[offset][j].index = (row ? node->idx[1] + 1 : node->idx[0] + 1);
      vecs[offset][j].value = it.value<double>();
      ++it;
    }
    if (has_bias) {
      vecs[offset][sz - 2].index = static_cast<int>(nz_count + 1);
      vecs[offset][sz - 2].value = _pi->bias;
    }
    vecs[offset][sz - 1].index = -1;

    x[offset] = &vecs[offset][0];
    y[offset] = 1;
    ++offset;
  }

  for (size_t i=0; i<negative_samples.size(); ++i) {
    vecs[offset].resize(sz);
    auto it = negative_samples[i].begin();
    for (int j = 0; j < nz_count; ++j) {
      const auto* node = it.node();
      vecs[offset][j].index = (row ? node->idx[1] + 1 : node->idx[0] + 1);
      vecs[offset][j].value = it.value<double>();
      ++it;
    }
    if (has_bias) {
      vecs[offset][sz - 2].index = static_cast<int>(nz_count + 1);
      vecs[offset][sz - 2].value = _pi->bias;
    }
    vecs[offset][sz - 1].index = -1;

    x[offset] = &vecs[offset][0];
    y[offset] = -1;
    ++offset;
  }
  // If model already exist -> clear
  if (_pi->mod)
    LINEAR::destroy_model(_pi->mod);
  // Problem
  LINEAR::problem prob = { (int)length, _pi->dim + 1, &y[0], &x[0], _pi->bias };
  if (_pi->bias < 0)
    --prob.n;
  // Train
  _pi->mod = LINEAR::train(&prob, &_pi->parameters);
  // Cache classifier
  _pi->cacheW();
}

/*
 *  @name   Clone
 *  @brief  Clone classifier.
 *  @return Deep copy of classifier
 */
BinaryClassifier<cv::SparseMat>* BinarySparseLinearSVMClassifier::Clone() const {
  return new BinarySparseLinearSVMClassifier(*this);
}

/*
 *  @name   Save
 *  @brief  Save trained SVM model
 *  @param[in] outfile File to save the model to
 */
int BinarySparseLinearSVMClassifier::Save(const std::string& output) const {
  int error = -1;
  if (_pi->mod) {
    std::string out_name;
    size_t pos = output.rfind(".");
    if (pos != std::string::npos) {
      out_name = output.substr(0,pos) + ".bin";
    } else {
      out_name = output + ".bin";
    }
    // Open stream
    std::ofstream output_stream(out_name.c_str(),
                                std::ios_base::out | std::ios_base::binary);
    if (output_stream.is_open()) {
      error = save_binary_model(*(_pi->mod), output_stream, true);
    }
  }
  return error;
}

/*
 *  @name   Save
 *  @brief  Save trained SVM model
 *  @param[in] out_stream   Output stream to a file
 *  @return -1 if error, 0 otherwise
 */
int BinarySparseLinearSVMClassifier::Save(std::ostream& out_stream) const {
  int error = -1;
  if (_pi->mod) {
    error = LINEAR::save_binary_model(*(_pi->mod), out_stream, true);
  } else {
    std::cout << "Error : No classifier to save !" << std::endl;
  }
  return error;
}

/*
 *  @name   Load
 *  @brief  Load saved SVM model
 *  @param[in] infile File to load the model from
 */
int BinarySparseLinearSVMClassifier::Load(const std::string& input) {
  int error = -1;
  // Already loaded ?
  if (_pi->mod) {
    // Remove previous model
    LINEAR::destroy_model(_pi->mod);
  }
  // Load new model
  if (input.rfind(".bin") != std::string::npos) {
    std::ifstream input_stream(input.c_str(),
                               std::ios_base::in | std::ios_base::binary);
    error = this->Load(input_stream);
  } else {
    _pi->mod = LINEAR::load_model(input.c_str());
    _pi->cacheW();
    error = _pi->mod == nullptr ? -1 : 0;
  }
  return error;
}

/*
 *  @name   Load
 *  @brief  Load saved SVM model from binary file
 *  @param[in] input_stream Input stream to load the model from
 *  @return -1 if error, 0 otherwise
 */
int BinarySparseLinearSVMClassifier::Load(std::istream& input_stream){
  int error = -1;
  if (input_stream.good()) {
    int status = ScanStreamForObject(input_stream,
                                     HeaderObjectType::kBinarySparseLinearSvm);
    if (status == 0) {
      // Ok, model already initialized ?
      if (_pi->mod) {
        // Remove previous model
        LINEAR::destroy_model(_pi->mod);
      }
      // Load linear since there is no differences
      _pi->mod = LINEAR::load_binary_model(input_stream);
      _pi->cacheW();
      // Sanity check
      error = _pi->mod == nullptr ? -1 : 0;
    }
  }
  return error;
}

/*
 *  @name   ComputeObjectSize
 *  @brief  Provide classifier memory size in bytes
 *  @return Memory size in bytes
 */
int BinarySparseLinearSVMClassifier::ComputeObjectSize(void) const {
  int size = 0;
  if(_pi->mod) {
    size += LINEAR::compute_model_size(*(_pi->mod));
  } else {
    std::cout << "Error : No classifier !" << std::endl;
  }
  return size;
}

/*
 *  @name Classify
 *  @brief  Classify new sample
 *
 * This function returns the distance to the decision hyperplane scaled
 * by |w|. The sign of the returned value is negative for negative
 * predictions and positive for positive predictions. Its magnitude
 * can be used as a confidence value.
 *
 *  @param[in] x Sample to classify
 *  @return Distance to hyperplane (scaled)
 */
double BinarySparseLinearSVMClassifier::Classify(const cv::SparseMat& x) const {
  double dist = 0;
  bool row = x.size()[0] == 1;
  const double* w_ptr = reinterpret_cast<const double*>(_pi->w.data);
  if (x.type() != _pi->w.type()) {
    // convert to correct type
    cv::SparseMat y;
    x.convertTo(y, _pi->w.type());
    for (auto it = y.begin(); it != y.end(); ++it) {
      const auto* node = it.node();
      const int idx = (row ? node->idx[1] : node->idx[0]);
      dist += it.value<double>() * w_ptr[idx];
    }
  } else {
    for (auto it = x.begin(); it != x.end(); ++it) {
      const auto* node = it.node();
      const int idx = (row ? node->idx[1] : node->idx[0]);
      dist += it.value<double>() * w_ptr[idx];
    }
  }

  if (_pi->bias >= 0)
    dist += _pi->wbias;

  if (_pi->mod->param.solver_type == LINEAR::L2R_LR ||
      _pi->mod->param.solver_type == LINEAR::L1R_LR)
    return 2.0 / ( 1.0 + exp(-dist) ) - 1.0;
  else
    return dist;
}

/*
 *  @name   GetW
 *  @brief  Get vector w
 *  @return w
 */
cv::Mat BinarySparseLinearSVMClassifier::GetW() const {
  return _pi->w.clone();
}

/*
 *  @name   GetBias
 *  @brief  Get bias b
 *  @return b
 */
double BinarySparseLinearSVMClassifier::GetBias() const {
  if (_pi->bias >= 0)
    return _pi->wbias;
  else
    return -1.0;
}

/*
 *  @name   GetFeatureLength
 *  @brief  Provide feature space size
 *  @return Length of the feature
 */
int BinarySparseLinearSVMClassifier::GetFeatureLength() const {
  return std::max(_pi->w.cols, _pi->w.rows);
}

/*
 *  @name set_svm_type
 *  @brief  Set SVM type
 *  @param[in] svm_type   Type of svm
 */
void BinarySparseLinearSVMClassifier::set_svm_type(const BinaryLinearSVMTraining& type) {
  switch (type) {
    case L2_LR:
    case L2R_LR:
      _pi->parameters.solver_type = LINEAR::L2R_LR;
      break;
    case L2LOSS_SVM_DUAL:
    case L2R_L2LOSS_SVC_DUAL:
      _pi->parameters.solver_type = LINEAR::L2R_L2LOSS_SVC_DUAL;
      break;
    case L2LOSS_SVM:
    case L2R_L2LOSS_SVC:
      _pi->parameters.solver_type = LINEAR::L2R_L2LOSS_SVC;
      break;
    case L1LOSS_SVM_DUAL:
    case L2R_L1LOSS_SVC_DUAL:
      _pi->parameters.solver_type = LINEAR::L2R_L1LOSS_SVC_DUAL;
      break;
    case L1R_L2LOSS_SVC:
      _pi->parameters.solver_type = LINEAR::L1R_L2LOSS_SVC;
      break;
    case L1R_LR:
      _pi->parameters.solver_type = LINEAR::L1R_LR;
      break;
    default:
      throw std::invalid_argument("Invalid SVM solver type");
  }
}

/*
 *  @name   set_c_coef
 *  @brief  Set C svm's parameter
 *  @param  c_coef  Value of C
 */
void BinarySparseLinearSVMClassifier::set_c_coef(double c_coef) {
  _pi->parameters.C = c_coef;
}
}
