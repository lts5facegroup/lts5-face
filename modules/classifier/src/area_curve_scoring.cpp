/**
 *  @file   are_curve_scoring.cpp
 *  @brief  Compute area under roc curve
 *          Based on Anil/Hua's work
 *
 *  @author Christophe Ecabert
 *  @date   25/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <algorithm>

#include "lts5/classifier/area_curve_scoring.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  AreaUnderCurveScoring
 * @fn  explicit AreaUnderCurveScoring(const int k_fold)
 * @brief Constructor
 * @param[in] n_fold  Number of fold to score
 */
AreaUnderCurveScoring::AreaUnderCurveScoring(const int n_fold) : BaseScoringMetric(n_fold) {
  // Init prediction container
  pos_pred_.resize(static_cast<std::size_t >(n_fold),
                   std::vector<double>(0, 0.0));
  neg_pred_.resize(static_cast<std::size_t >(n_fold),
                   std::vector<double>(0, 0.0));
  // Setup number of step
  n_step_ = 100;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  AddPositivePrediction
 * @fn void AddPositivePrediction(const int k_fold,
 *                                const double pred,
                                  const double threshold)
 * @brief Add prediction result for positive samples (Fill TP, FN)
 * @param[in] k_fold    Fold  metric to fill
 * @param[in] pred      Prediction value
 * @param[in] threshold Classification threshold (default 0.0)
 */
void AreaUnderCurveScoring::AddPositivePrediction(const int k_fold,
                                                  const double pred,
                                                  const double threshold) {
  pos_pred_[k_fold].push_back(pred);
}

/*
 * @name  AddNegativePrediction
 * @fn void AddNegativePrediction(const int k_fold,
 *                                const double pred,
                                  const double threshold)
 * @brief Add prediction result for negative samples (Fill TN, FP)
 * @param[in] k_fold    Fold  metric to fill
 * @param[in] pred      Prediction value
 * @param[in] threshold Classification threshold (default 0.0)
 */
void AreaUnderCurveScoring::AddNegativePrediction(const int k_fold,
                                                  const double pred,
                                                  const double threshold) {
  neg_pred_[k_fold].push_back(pred);
}

/*
 * @name  Compute
 * @fn    double Compute(const int k_fold)
 * @brief Compute scoring metric for a given fold
 * @param[in] k_fold  Selected fold
 * @return  Score
 */
double AreaUnderCurveScoring::Compute(const int k_fold) {
  // Calculates the area under ROC curve, using the ground truth labels,
  // and the svm predictions nsteps defines how many steps you want to use in
  // the area calculation

  // Find min/max of prediction
  double min_pos_pred = *std::min_element(pos_pred_[k_fold].begin(),
                                          pos_pred_[k_fold].end());
  double max_pos_pred = *std::max_element(pos_pred_[k_fold].begin(),
                                          pos_pred_[k_fold].end());
  double min_neg_pred = *std::min_element(neg_pred_[k_fold].begin(),
                                          neg_pred_[k_fold].end());
  double max_neg_pred = *std::max_element(neg_pred_[k_fold].begin(),
                                          neg_pred_[k_fold].end());
  double min_pred = min_pos_pred < min_neg_pred ? min_pos_pred : min_neg_pred;
  double max_pred = max_pos_pred < max_neg_pred ? max_pos_pred : max_neg_pred;

  // Change decision threshold
  std::vector<double> true_pos_rate;
  std::vector<double> false_pos_rate;
  double step = (max_pred - min_pred) / static_cast<double>(n_step_);
  for (double t = min_pred; t <= max_pred ; t += step) {
    int tp = 0;
    int tn = 0;
    int fp = 0;
    int fn = 0;
    // Loop over positive sample
    auto it = pos_pred_[k_fold].cbegin();
    for ( ; it != pos_pred_[k_fold].cend() ; ++it) {
      if (*it > t) {
        // True positive
        tp++;
      } else {
        // False negative
        fn++;
      }
    }
    it = neg_pred_[k_fold].cbegin();
    for (; it < neg_pred_[k_fold].end() ; ++it) {
      if (*it < t) {
        // True negative
        tn++;
      } else {
        // False positive
        fp++;
      }
    }

    // Compute TPR/FPR
    double tpr = static_cast<double>(tp) / static_cast<double>(tp + fn);
    double fpr = static_cast<double>(fp) / static_cast<double>(fp + tn);

    true_pos_rate.push_back(tpr);
    false_pos_rate.push_back(fpr);
  }
  // approximation of the area under the TPR-FPR curve using Riemann sum
  //compute the areas of the rectangles
  double auc = 0.0;
  for (int i = 0; i < (n_step_ - 1); ++i) {
    auc += ((true_pos_rate[i] + (true_pos_rate[i+1] - true_pos_rate[i])/2) *
            (false_pos_rate[i+1] - false_pos_rate[i]));
  }
  //add the small triangles left on the corners
  auc += ((true_pos_rate[0] * false_pos_rate[0]/2) +
          ((true_pos_rate[n_step_ - 1] + (1 - true_pos_rate[n_step_ - 1])/2) *
          (1 - false_pos_rate[n_step_ - 1])));
  return auc;
}

/*
 * @name  Clear
 * @fn void Clear(void)
 * @brief Reset counters
 */
void AreaUnderCurveScoring::Clear(void) {
  std::size_t size = pos_pred_.size();
  pos_pred_.resize(size, std::vector<double>(0, 0.0));
  size = neg_pred_.size();
  neg_pred_.resize(size, std::vector<double>(0, 0.0));
}
}  // namepsace LTS5
