/**
 *  @file   binarylinearsvmclassifier.cpp
 *  @brief  Binary linear SVM classifier implementation
 *  @author Hua Gao
 *  @date   23/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#include <stdexcept>

#include "lts5/classifier/binarysvmclassifier.hpp"
#include "svm.h"

#define Malloc(type,n) (type *)malloc((n)*sizeof(type))

using namespace std;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
namespace {
  /**
   *  @name  SvmCloneSupportVectors
   *  @brief
   *  @param[in]  other
   *  @param[in]  l
   *  @return
   */
  struct svm_node* SvmCloneSupportVectors(struct svm_node* other, int l) {
    struct svm_node* SV = Malloc(svm_node, l);

    for (int i=0; i<l; i++) {
      SV[i].dim = other[i].dim;
      SV[i].values = Malloc(double, SV[i].dim);
      memcpy(SV[i].values, other[i].values, SV[i].dim * sizeof(double));
    }
    return SV;
  }

  /**
   *  @name   SvmCloneModel
   *  @brief
   *  @param  other
   *  @return
   */
  struct svm_model* SvmCloneModel(const struct svm_model* other) {
    if (!other)
      return NULL;

    svm_model* model = Malloc(svm_model, 1);

    model->param = other->param;
    model->param.weight = 0;
    model->param.weight_label = 0;
    model->param.nr_weight = 0;

    model->nr_class = other->nr_class;
    model->l = other->l;

    model->SV = SvmCloneSupportVectors(other->SV, other->l);

    model->sv_coef = Malloc(double*, model->nr_class - 1);
    for (int i=0; i<model->nr_class - 1; i++) {
      model->sv_coef[i] = Malloc(double, model->l);
      memcpy(model->sv_coef[i], other->sv_coef[i], model->l * sizeof(double));
    }

    int n = model->nr_class * (model->nr_class-1)/2;

    model->rho = Malloc(double, n);
    memcpy(model->rho, other->rho, n * sizeof(double));

    model->probA = Malloc(double, n);
    memcpy(model->probA, other->probA, n * sizeof(double));

    model->probB = Malloc(double, n);
    memcpy(model->probB, other->probB, n * sizeof(double));

    model->label = Malloc(int, model->nr_class);
    memcpy(model->label, other->label, model->nr_class * sizeof(int));

    model->nSV = Malloc(int, model->nr_class);
    memcpy(model->nSV, other->nSV, model->nr_class * sizeof(int));

    model->free_sv = other->free_sv;

    return model;
  }
}

/**
 *  @struct Impl
 *  @brief
 */
struct BinarySVMClassifier::Impl {
  svm_parameter       parameters;
  svm_model*          model;
  int                 r;
  int                 c;
  int                 dim;
  bool                probabilities;
  vector<int>         labels;
  vector<double>      weights;

  /**
   *  @name  Impl
   *  @brief Constructor
   */
  Impl()
  : model(0), r(0), c(0), dim(0), probabilities(false)
  {}
};

/*
 *  @name BinarySVMClassifier
 *  @brief Constructor
 *  @param[in] type Type of SVM formulation
 *  @param[in] slack_param Value of the slack parameter (C or nu)
 *  @param[in] kernel Type of SVM kernel
 *  @param[in] gamma Kernel parameter gamma
 *  @param[in] coef0 Kernel parameter coef0
 *  @param[in] degree Degree of polynomial kernel
 *  @param[in] shrinking Whether shrinking is to be used
 *  @param[in] probability Whether probabilities shall be returned
 *  @param[in] positive_weight Penalty weight for positive misclassifications
 *  @param[in] negative_weight Penalty weight for negative misclassifications
 *  @param[in] eps Stopping criterion threshold
 *  @param[in] cache_size Kernel cache size in MB
 */
BinarySVMClassifier::BinarySVMClassifier(BinarySVMType type,
                                         double slack_param, BinarySVMKernel kernel, double gamma,
                                         double coef0, int degree, bool shrinking, bool probability,
                                         double positive_weight, double negative_weight,
                                         double epsi, double cache_size)
: _pi(new Impl) {
  switch (type) {
    case C_SVM:
      _pi->parameters.svm_type = C_SVM;
      _pi->parameters.C  = slack_param<0 ? 1.0 : slack_param;
      break;
    case NU_SVM:
      _pi->parameters.svm_type = NU_SVM;
      _pi->parameters.nu = slack_param<0 ? 0.5 : slack_param;
      break;
    default:  throw invalid_argument("Invalid SVM type");
      break;
  }

  switch (kernel) {
    case LINEAR:
      _pi->parameters.kernel_type = LINEAR;
      break;
    case POLYNOMIAL:
      _pi->parameters.kernel_type = POLY;
      break;
    case GAUSSIAN:
      _pi->parameters.kernel_type = RBF;
      break;
    case SIGMOID:
      _pi->parameters.kernel_type = SIGMOID;
      break;
    case PRECOMPUTED:
      _pi->parameters.kernel_type = PRECOMPUTED;
      break;
  }

  _pi->parameters.gamma           = gamma;
  _pi->parameters.coef0           = coef0;
  _pi->parameters.degree          = degree;
  _pi->parameters.cache_size      = cache_size;
  _pi->parameters.eps             = epsi;
  _pi->parameters.shrinking       = shrinking;
  _pi->parameters.probability     = probability;

  if (positive_weight == 1.0 && negative_weight == 1.0) {
    _pi->parameters.nr_weight       = 0;
    _pi->parameters.weight          = 0;
    _pi->parameters.weight_label    = 0;
  }
  else {
    _pi->labels.resize(2);
    _pi->weights.resize(2);
    _pi->labels[0] = 1;
    _pi->labels[1] = -1;
    _pi->weights[0] = positive_weight;
    _pi->weights[1] = negative_weight;

    _pi->parameters.nr_weight       = 2;
    _pi->parameters.weight          = &_pi->weights[0];
    _pi->parameters.weight_label    = &_pi->labels[0];
  }
}

/*
 *  @name   BinarySVMClassifier
 *  @brief  Copy constructor
 *  @fn     BinarySVMClassifier(const BinarySVMClassifier& other)
 *  @param[in]  other Other classifier
 */
BinarySVMClassifier::BinarySVMClassifier(const BinarySVMClassifier& other)
: BinaryClassifier(), _pi(new Impl) {
  *_pi = *other._pi;
  _pi->model = SvmCloneModel(other._pi->model);
}


/*
 *  @name   ~BinarySVMClassifier
 *  @brief  Assignment operator
 *  @fn     BinarySVMClassifier& operator = (const BinarySVMClassifier& other)
 *  @param[in]  other   Other classifier
 */
BinarySVMClassifier& BinarySVMClassifier::operator = (const BinarySVMClassifier& other) {
  if (_pi->model)
    svm_free_and_destroy_model(&_pi->model);

  *_pi = *other._pi;

  _pi->model = SvmCloneModel(other._pi->model);

  return *this;
}

/*
 *  @name   ~BinarySVMClassifier
 *  @fn     ~BinarySVMClassifier(void)
 *  @brief  Destructor
 */
BinarySVMClassifier::~BinarySVMClassifier(void) {
  if (_pi->model)
    svm_free_and_destroy_model(&_pi->model);
}

/*
 *  @name   Train
 *  @brief  Train binary SVM
 *  @param[in] positive_samples Positive samples
 *  @param[in] negative_samples Negative samples
 */
void BinarySVMClassifier::Train(const vector<cv::Mat>& positive_samples,
                                const vector<cv::Mat>& negative_samples) {
  int length = (int)positive_samples.size() + (int)negative_samples.size();

  if (positive_samples.empty() || negative_samples.empty())
    throw invalid_argument("Both positive and negative samples are needed!");

  int rows = positive_samples[0].rows;
  int cols = positive_samples[0].cols;
  _pi->dim = max(rows, cols);

  if (rows != 1 && cols != 1)
    throw invalid_argument("Only vectors supported");

  for (size_t i=0; i<positive_samples.size(); ++i) {
    if (positive_samples[i].rows * positive_samples[i].cols != _pi->dim)
      throw invalid_argument("All training vectors must have same size!");
    if (!positive_samples[i].isContinuous())
      throw invalid_argument("Training vectors must be continuous!");
    if (positive_samples[i].type() != CV_64FC1)
      throw invalid_argument("Training vectors must be double precision floating point vectors!");

  }

  for (size_t i=0; i<negative_samples.size(); ++i) {
    if (negative_samples[i].rows * negative_samples[i].cols != _pi->dim)
      throw invalid_argument("All training vectors must have same size!");
    if (!negative_samples[i].isContinuous())
      throw invalid_argument("Training vectors must be continuous!");
    if (negative_samples[i].type() != CV_64FC1)
      throw invalid_argument("Training vectors must be double precision floating point vectors!");

  }

  vector<svm_node>    x(length);
  vector<double>      y(length);

  int offset = 0;
  for (size_t i=0; i<positive_samples.size(); ++i) {
    y[offset] = 1;
    x[offset].dim = _pi->dim;
    x[offset].values = (double*) positive_samples[i].ptr<double>(0);
    ++offset;
  }

  for (size_t i=0; i<negative_samples.size(); ++i) {
    y[offset] = -1;
    x[offset].dim = _pi->dim;
    x[offset].values = (double*) negative_samples[i].ptr<double>(0);
    ++offset;
  }

  svm_problem problem = { length, &y[0], &x[0] };

  // Set default parameter for gamma if necessary
  if (_pi->parameters.gamma < 0)
    _pi->parameters.gamma = 1.0 / _pi->dim;

  if (_pi->model)
    svm_free_and_destroy_model(&_pi->model);

  _pi->model = svm_train(&problem, &_pi->parameters);

  _pi->probabilities = svm_check_probability_model(_pi->model) != 0;

  _pi->labels.resize(2);
  svm_get_labels(_pi->model, &_pi->labels[0]);

  _pi->dim = 0;
  int n_sv = _pi->model->l;
  for (int i=0; i<n_sv; ++i)
    _pi->dim = max(_pi->dim, _pi->model->SV[i].dim);
}

/*
 *  @name   Clone
 *  @brief  Clone classifier.
 *  @return Deep copy of classifier
 */
BinaryClassifier<cv::Mat>* BinarySVMClassifier::Clone() const {
  return new BinarySVMClassifier(*this);
}

/*
 *  @name   Save
 *  @brief  Save trained SVM model
 *  @param[in] outfile File to save the model to
 */
int BinarySVMClassifier::Save(const string& output) const {
  int error = -1;
  if (_pi->model) {
    error = svm_save_model(output.c_str(), _pi->model);
  }
  return error;
}

/*
 *  @name   Load
 *  @brief  Load saved SVM model
 *  @param[in] infile File to load the model from
 */
int BinarySVMClassifier::Load(const string& input) {
  // Check input name
  int error = input.empty() ? -1 : 0;
  if (error == 0) {
    // Save model
    svm_model* new_model = svm_load_model(input.c_str());
    error |= new_model == NULL ? -1 : 0;

    // Check class
    if (svm_get_nr_class(new_model) != 2)
    {
      svm_free_and_destroy_model(&new_model);
      error |= -1;
      //throw runtime_error("Error loading SVM model: Only two-class SVMs supported!");
    }

    // Already initialized ?
    if (_pi->model)
      svm_free_and_destroy_model(&_pi->model);

    // Set new model
    _pi->model = new_model;
    _pi->probabilities = svm_check_probability_model(_pi->model) != 0;
    _pi->dim = 0;
    int n_sv = _pi->model->l;
    for (int i=0; i<n_sv; ++i)
      _pi->dim = max(_pi->dim, _pi->model->SV[i].dim);

    _pi->labels.resize(2);
    svm_get_labels(_pi->model, &_pi->labels[0]);
  }
  return error;
}

/*
 *  @name   Classify
 *  @brief  Classify new sample
 *
 * This function returns the distance to the decision hyperplane scaled
 * by |w|. The sign of the returned value is negative for negative
 * predictions and positive for positive predictions. Its magnitude
 * can be used as a confidence value.
 *
 *  @param[in] x Sample to classify
 *  @return Distance to hyperplane (scaled)
 */
double BinarySVMClassifier::Classify(const cv::Mat& x) const {
  if (x.type() != CV_64FC1)
    throw invalid_argument("Test vectors must be double precision floating point vectors!");
  if (x.rows * x.cols < _pi->dim)
    throw invalid_argument("Test vector must have same size as training vectors!");
  if (!x.isContinuous())
    throw invalid_argument("Test vectors must be continuous!");

  svm_node vec = { x.rows*x.cols, (double*) x.data };

  double dist = 0;
  svm_predict_values(_pi->model, &vec, &dist);
  if (_pi->labels[0] > _pi->labels[1])
    return dist;
  else
    return -dist;
}

/*
 *  @name   GetClassProbabilities
 *  @brief  Get class probabilities
 *  @param[in] x Sample to classify
 *  @param[out] pos_prob Probability of x being a positive sample
 *  @param[out] neg_prob Probability of x being a negative sample
 */
void BinarySVMClassifier::GetClassProbabilities(const cv::Mat& x, double& pos_prob, double& neg_prob) const {
  if (x.type() != CV_64FC1)
    throw invalid_argument("Test vectors must be double precision floating point vectors!");
  if (x.rows * x.cols < _pi->dim)
    throw invalid_argument("Test vector must have same size as training vectors!");
  if (!x.isContinuous())
    throw invalid_argument("Test vectors must be continuous!");
  if (!_pi->probabilities)
    throw invalid_argument("Probabilities must be enabled at training time!");

  svm_node vec = { x.rows*x.cols, (double*) x.data };

  double probs[2];
  svm_predict_probability(_pi->model, &vec, probs);
  if (_pi->labels[0] > _pi->labels[1]) {
    pos_prob = probs[0];
    neg_prob = probs[1];
  }
  else {
    pos_prob = probs[1];
    neg_prob = probs[0];
  }
}

/*
 *  @name   NumPositiveSupportVectors
 *  @brief  Return number of positive support vectors
 *  @return value
 */
int BinarySVMClassifier::NumPositiveSupportVectors() const {
  if (!_pi->model)
    return -1;

  if (_pi->labels[0] > _pi->labels[1])
    return _pi->model->nSV[0];
  else
    return _pi->model->nSV[1];
}

/*
 *  @name   NumNegativeSupportVectors
 *  @brief  Return number of negative support vectors
 *  @return value
 */
int BinarySVMClassifier::NumNegativeSupportVectors() const {
  if (!_pi->model)
    return -1;

  if (_pi->labels[0] > _pi->labels[1])
    return _pi->model->nSV[1];
  else
    return _pi->model->nSV[0];
}
}
