/**
 *  @file   recall_scoring.cpp
 *  @brief  Compute Recall score for a given binary classification system
 *
 *  @author Christophe Ecabert
 *  @date   20/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/classifier/recall_scoring.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  RecallScoring
 * @fn    RecallScoring(void)
 * @brief Constructor
 * @param[in] n_fold  Number of fold to score
 */
RecallScoring::RecallScoring(const int n_fold) : BaseScoringMetric(n_fold) {}

#pragma mark -
#pragma mark Compute

/*
 * @name  Compute
 * @fn    double Compute(void)
 * @brief Compute Recall score
 * @param[in] k_fold  Selected fold
 * @return  Recall
 */
double RecallScoring::Compute(const int k_fold) {
  return (static_cast<double>(tp_[k_fold]) /
          static_cast<double>(tp_[k_fold] + fn_[k_fold]));
}
}  // namepsace LTS5