/**
 *  @file   crossvalidation.cpp
 *  @brief  Cross-validation on binary classifiers implementation
 *  @author Hua Gao
 *  @date   30/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <random>

#include "opencv2/core/core.hpp"

#include "lts5/utils/sys/parallel.hpp"
#include "lts5/classifier/crossvalidation.hpp"
#include "lts5/classifier/binary_sparse_linear_svm_classifier.hpp"
#include "lts5/classifier/accuracy_scoring.hpp"
#include "lts5/classifier/f1_scoring.hpp"
#include "lts5/classifier/precision_scoring.hpp"
#include "lts5/classifier/recall_scoring.hpp"
#include "lts5/classifier/area_curve_scoring.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<typename T>
struct ClassifierSelector {
  /** Classifier instance */
  BinaryClassifier<T>* classifier;

  /**
   * @name  ClassifierSelector
   * @brief Constructor
   * @param[in] c_coef C coefficient
   * @param[in] type Type of loss function
   */
  ClassifierSelector(const double c_coef,const BinaryLinearSVMTraining type);

  /**
   * @name  ~ClassifierSelector
   * @brief Destructor
   */
  ~ClassifierSelector() {
    if (classifier) {
      delete classifier;
      classifier = nullptr;
    }
  }
};

template<>
ClassifierSelector<cv::Mat>::ClassifierSelector(const double c_coef,
                                                const BinaryLinearSVMTraining type) {
  classifier = new BinaryLinearSVMClassifier(c_coef, type);
}

template<>
ClassifierSelector<cv::SparseMat>::ClassifierSelector(const double c_coef,
                                                const BinaryLinearSVMTraining type) {
  classifier = new BinarySparseLinearSVMClassifier(c_coef, type);
}

#pragma mark -
#pragma mark Initialization

/*
 *  @name   BinaryCrossValidation
 *  @fn BinaryCrossValidation(const BinaryClassifier& classifier,
                              const std::vector<T>& positive,
                              const std::vector<T>& negative,
                              const ScoringType scoring_type,
                              int n_folds = 5,
                              bool truly_random = false)
 *  @brief  Constructor
 *  @param[in] classifier   Classifier to use
 *  @param[in] positive     Positive data
 *  @param[in] negative     Negative data
 *  @param[in] scoring_type Type of metric to use for scoring performance
 *  @param[in] n_folds      Number of folds to use
 *  @param[in] truly_random Truly random (not reproducable) CV
 */
template<typename T>
BinaryCrossValidation<T>::BinaryCrossValidation(const BinaryClassifier<T>& classifier,
                                                const std::vector<T>& positive,
                                                const std::vector<T>& negative,
                                                const ScoringType scoring_type,
                                                int n_folds,
                                                bool truly_random)
: pos_folds_(n_folds),
  neg_folds_(n_folds),
  classifier_(classifier.Clone()),
  pos_classification_result_(n_folds),
  neg_classification_result_(n_folds),
  score_metric_(nullptr),
  score_(n_folds),
  mean_score_(-1.0),
  score_stddev_(-1.0) {
  std::vector<T> pos = positive;
  std::vector<T> neg = negative;

  cv::RNG rng(truly_random ? cv::getTickCount() : -1);

  std::random_shuffle(pos.begin(), pos.end(), rng);
  std::random_shuffle(neg.begin(), neg.end(), rng);

  double pos_step = double(pos.size()) / double(n_folds);
  for (int i=0; i<n_folds; ++i) {
    int fold_start = cvRound(i * pos_step);
    int fold_end = cvRound((i+1) * pos_step);
    pos_folds_[i].insert(pos_folds_[i].begin(),
                         pos.begin() + fold_start,
                         pos.begin() + fold_end);
    if (pos_folds_[i].empty())
      throw std::invalid_argument("Not enough positive data for cross validation. Try with less folds.");
  }

  double neg_step = double(neg.size()) / double(n_folds);
  for (int i=0; i<n_folds; ++i) {
    int fold_start = cvRound(i * neg_step);
    int fold_end = cvRound((i+1) * neg_step);
    neg_folds_[i].insert(neg_folds_[i].begin(),
                         neg.begin() + fold_start,
                         neg.begin() + fold_end);
    if (neg_folds_[i].empty())
      throw std::invalid_argument("Not enough negative data for cross validation. Try with less folds.");
  }
  // Scoring metric
  score_.resize(n_folds);
  switch(scoring_type) {
    // Accuracy
    case ScoringType::kAccuracy :
      score_metric_ = new LTS5::AccuracyScoring(n_folds);
      break;
    // F1
    case ScoringType::kF1 :
      score_metric_ = new LTS5::F1Scoring(n_folds);
      break;
    // Precision
    case ScoringType::kPrecion :
      score_metric_ = new LTS5::PrecisionScoring(n_folds);
      break;
    // Recall
    case ScoringType::kRecall :
      score_metric_ = new LTS5::RecallScoring(n_folds);
      break;
    // AUC
    case ScoringType::kAreaUnderCurve :
      score_metric_ = new LTS5::AreaUnderCurveScoring(n_folds);
      break;
    // Unknown type
    default:
      throw std::invalid_argument("Unknown scoring metrics");
  }
}

/*
 *  @name BinaryCrossValidation
 *  @fn BinaryCrossValidation(const BinaryClassifier& classifier,
                              const std::vector< std::vector<T> >& pos_folds,
                              const std::vector< std::vector<T> >& neg_folds,
                              const ScoringType scoring_type);
 *  @brief Constructor
 *  @param[in] classifier Classifier to use
 *  @param[in] pos_folds Positive folds
 *  @param[in] neg_folds Negative folds
 *  @param[in] scoring_type Type of metric to use for scoring performance
 */
template<typename T>
BinaryCrossValidation<T>::BinaryCrossValidation(const BinaryClassifier<T>& classifier,
                                                const std::vector< std::vector<T> >& pos_folds,
                                                const std::vector< std::vector<T> >& neg_folds,
                                                const ScoringType scoring_type)
: pos_folds_(pos_folds),
  neg_folds_(neg_folds),
  classifier_(classifier.Clone()),
  pos_classification_result_(pos_folds.size()),
  neg_classification_result_(neg_folds.size()),
  score_metric_(nullptr),
  score_(pos_folds.size()),
  mean_score_(-1.0),
  score_stddev_(-1.0) {
  // Fold size valid ?
  if (pos_folds.size() != neg_folds.size())
    throw std::invalid_argument("Positive and negative folds have different sizes.");
  // Scoring metric
  score_.resize(pos_folds.size());
  const int n_fold = static_cast<int>(pos_folds.size());
  switch(scoring_type) {
    // Accuracy
    case ScoringType::kAccuracy :
      score_metric_ = new LTS5::AccuracyScoring(n_fold);
      break;
    // F1
    case ScoringType::kF1 :
      score_metric_ = new LTS5::F1Scoring(n_fold);
      break;
    // Precision
    case ScoringType::kPrecion :
      score_metric_ = new LTS5::PrecisionScoring(n_fold);
      break;
    // Recall
    case ScoringType::kRecall :
      score_metric_ = new LTS5::RecallScoring(n_fold);
      break;
    // AUC
    case ScoringType::kAreaUnderCurve :
      score_metric_ = new LTS5::AreaUnderCurveScoring(n_fold);
      break;
      // Unknown type
    default:
      throw std::invalid_argument("Unknown scoring metrics");
  }
}

/*
 *  @name   ~BinaryCrossValidation
 *  @fn ~BinaryCrossValidation()
 *  @brief  Destructor
 */
template<typename T>
BinaryCrossValidation<T>::~BinaryCrossValidation() {
  if (score_metric_) {
    delete score_metric_;
    score_metric_ = nullptr;
  }
}

#pragma mark -
#pragma mark Cross-Validation

/*
 *  @name   RunCrossValidation
 *  @brief  Run cross-validation
 */
template<typename T>
void BinaryCrossValidation<T>::RunCrossValidation() {

  Parallel::For(pos_folds_.size(),
                [&](const size_t& i) {
    std::vector<T> pos_train, neg_train;
    for (int j = 0 ; j < (int)pos_folds_.size() ; ++j) {
      if (j == i) continue;
      pos_train.insert(pos_train.end(), pos_folds_[j].begin(), pos_folds_[j].end());
      neg_train.insert(neg_train.end(), neg_folds_[j].begin(), neg_folds_[j].end());
    }

    std::unique_ptr<BinaryClassifier<T>> c(classifier_->Clone());
    c->Train(pos_train, neg_train);

    pos_classification_result_[i] = std::vector<double>(pos_folds_[i].size());
    for (size_t k = 0 ; k<pos_folds_[i].size() ; ++k) {
      double result = c->Classify(pos_folds_[i][k]);
      pos_classification_result_[i][k] = result;
      score_metric_->AddPositivePrediction(i, result, 0.0);
    }

    neg_classification_result_[i] = std::vector<double>(neg_folds_[i].size());
    for (size_t k = 0 ; k<neg_folds_[i].size() ; ++k) {
      double result = c->Classify(neg_folds_[i][k]);
      neg_classification_result_[i][k] = result;
      score_metric_->AddNegativePrediction(i, result, 0.0);
    }
    score_[i] = score_metric_->Compute(i);
  });

  mean_score_ = std::accumulate(score_.begin(), score_.end(), 0.0);
  mean_score_ /= score_.size();

  score_stddev_ = 0.0;
  for (size_t i = 0; i < score_.size(); ++i) {
    double tmp = score_[i] - mean_score_;
    score_stddev_ += tmp * tmp;
  }
  score_stddev_ /= static_cast<double>(score_.size() - 1);
  score_stddev_ = sqrt(score_stddev_);
}

#pragma mark -
#pragma mark Utility

/*
 *  @name   EstimateLinearCoefficitent
 *  @brief  Determine best C coefficient for SVM classifier using
 *          cross-validation
 *  @param[in]  pos_samples   Positive samples
 *  @param[in]  neg_samples   Negative samples
 *  @param[in]  svm_type      Type of svm used
 *  @param[in]  truly_random  Whether or not to be random
 *  @param[out] best_c        Best C coefficient
 *  @param[out] stats         Cross-validation statistics, if nullptr not use
 *  @return   Training balance (n_neg / n_pos)
 */
template<typename T>
float BinaryCrossValidation<T>::EstimateLinearCoefficient(const std::vector<T>& pos_samples,
                                                          const std::vector<T>& neg_samples,
                                                          const LTS5::BinaryLinearSVMTraining& svm_type,
                                                          const bool truly_random,
                                                          const ScoringType scoring_type,
                                                          double* best_c,
                                                          Stats* stats) {
  // Define search space
  std::vector<double> C_coef;
  for (double c = 0.0001; c <= 0.001; c += 0.0001) {
    C_coef.push_back(c);
  }
  for (double c = 0.001; c <= 0.01; c += 0.001) {
    C_coef.push_back(c);
  }
  for (double c = 0.01; c <= 0.1; c += 0.01) {
    C_coef.push_back(c);
  }
  for (double c = 0.1; c <= 1.0; c += 0.1) {
    C_coef.push_back(c);
  }
  for (double c = 1.0; c <= 100.0; c += 5.0) {
    C_coef.push_back(c);
  }

  // Best result so far
  double best_score = 0.0;
  double best_stderr = 0.0;

  // Training balance
  float pos_weight = (static_cast<float>(neg_samples.size()) /
                      static_cast<float>(pos_samples.size()));
  // Do cross-validation
  static Mutex mutex;
  Parallel::For(C_coef.size(),
                [&](const size_t& i) {
    // Selector
    ClassifierSelector<T> selector(C_coef[i], svm_type);
    LTS5::BinaryCrossValidation<T> cv(*selector.classifier,
                                      pos_samples,
                                      neg_samples,
                                      scoring_type,
                                      5,
                                      truly_random);
    cv.RunCrossValidation();
    {
      ScopedLock lock(mutex);
      if (cv.GetMeanScore() > best_score) {
        best_score = cv.GetMeanScore();
        best_stderr = (cv.GetScoreStdDev() /
                       sqrt(static_cast<double>(cv.GetNumberOfFolds())));
        *best_c = C_coef[i];
      } else if (cv.GetMeanScore() == best_score) {
        double stderror = (cv.GetScoreStdDev() /
                           sqrt(static_cast<double>(cv.GetNumberOfFolds())));
        if (stderror < best_stderr) {
          best_score = cv.GetMeanScore();
          best_stderr = stderror;
          *best_c = C_coef[i];
        }
      }
    }
  });

  if (stats) {
    stats->k_mean_score = best_score;
    stats->k_mean_stddev = best_stderr;
  }
  return pos_weight;
}

/*
 *  @name   GenerateKFold
 *  @brief  Generate randomly k combination of train/test sample indexes.
 *  @param[in]  n_samples   Number of samples
 *  @param[in]  n_folds     Number of folds
 *  @param[out] train_folds Sample indexes for training
 *  @param[out] test_folds  Sample indexes for testing
 */
template<typename T>
void BinaryCrossValidation<T>::GenerateKFold(const std::size_t& n_sample,
                                          const std::size_t& n_folds,
                                          std::vector<std::vector<int>>* train_folds,
                                          std::vector<std::vector<int>>* test_folds) {
  // Is there enough sample ?
  if (n_sample > n_folds) {
    // Init struct
    unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now()
                                          .time_since_epoch()
                                          .count());
    std::mt19937_64 rng(seed);
    train_folds->resize(n_folds);
    test_folds->resize(n_folds);
    // Generate indexes
    std::vector<int> sample = std::vector<int>(n_sample);
    std::iota(sample.begin(), sample.end(), 0);
    // Shuffle them randomly
    std::shuffle(sample.begin(), sample.end(), rng);
    // Create each folds
    const std::size_t fold_size = n_sample / n_folds;
    auto test_it = test_folds->begin();
    auto train_it = train_folds->begin();
    for (int i = 0; i < n_folds; ++i,++test_it,++train_it) {
      // Split train / test
      test_it->insert(test_it->begin(),
                      sample.begin() + (i * fold_size),
                      sample.begin() + (i + 1) * fold_size);
      train_it->insert(train_it->begin(),
                       sample.begin(),
                       sample.begin() + (i * fold_size));
      train_it->insert(train_it->end(),
                       sample.begin() + (i + 1) * fold_size,
                       sample.end());
    }
  }
}

#pragma mark -
#pragma Accessors

/*
 *  @name   GetPositiveClassificationResult
 *  @brief  Get classification result for positive sample
 *  @param[in] fold Fold Number
 *  @param[in] sample Sample number within fold
 *  @return Classification result on requested positive sample
 */
template<typename T>
double BinaryCrossValidation<T>::GetPositiveClassificationResult(int fold,
                                                              int sample) const {
  return pos_classification_result_.at(fold).at(sample);
}

/*
 *  @name   GetNegativeClassificationResult
 *  @brief  Get classification result for negative sample
 *  @param[in] fold Fold Number
 *  @param[in] sample Sample number within fold
 *  @return Classification result on requested negative sample
 */
template<typename T>
double BinaryCrossValidation<T>::GetNegativeClassificationResult(int fold,
                                                              int sample) const {
  return neg_classification_result_.at(fold).at(sample);
}

/*
 *  @name   GetMeanScore
 *  @brief  Get mean score
 *  @return Mean accuracy
 */
template<typename T>
double BinaryCrossValidation<T>::GetMeanScore() const {
  return mean_score_;
}
/*
 *  @name   GetScoreStdDev
 *  @brief  Get score standard deviation
 *  @return score standard deviation
 */
template<typename T>
double BinaryCrossValidation<T>::GetScoreStdDev() const {
  return score_stddev_;
}
/*
 * @name   GetScore
 * @brief  Get score on a fold
 * @param[in] fold Fold number
 * @return Score on requested fold
 */
template<typename T>
double BinaryCrossValidation<T>::GetScore(int fold) const {
  return score_.at(fold);
}
/*
 *  @name  GetNumberOfFolds
 *  @brief Get number of folds
 *  @return Number of folds
 */
template<typename T>
int BinaryCrossValidation<T>::GetNumberOfFolds() const {
  return static_cast<int>(score_.size());
}

#pragma mark -
#pragma mark Instanciation

/** cv::Mat */
template class BinaryCrossValidation<cv::Mat>;
/** cv::SparseMat */
template class BinaryCrossValidation<cv::SparseMat>;

}  // namespace LTS5
