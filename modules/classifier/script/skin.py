# coding=utf-8
"""
Skin classifier based on Bayes classifier with Gaussian Mixture Model

See:
  https://jakevdp.github.io/PythonDataScienceHandbook/05.13-kernel-density-estimation.html
  https://inc.ucsd.edu/mplab/grants/project1/free-software/MPTWebSite/API/mixgauss_8cpp-source.html
"""
import numpy as _np
from sklearn.base import BaseEstimator as _BaseEstimator
from sklearn.base import ClassifierMixin as _ClassifierMixin
from sklearn.mixture import GaussianMixture as _GMM

__author__ = 'Christophe Ecabert'

# Number of skin sample in compaq dataset
# https://www.hpl.hp.com/techreports/Compaq-DEC/CRL-98-11.pdf, Table 1
ns = 80377671
nsk = 854744181
# Mean value for skin model
mean_sk = _np.asarray([[73.53, 29.94, 17.76],
                       [249.71, 233.94, 217.49],
                       [161.68, 116.25, 96.95],
                       [186.07, 136.62, 114.40],
                       [189.26, 98.37, 51.18],
                       [247.00, 152.20, 90.84],
                       [150.10, 72.66, 37.76],
                       [206.85, 171.09, 156.34],
                       [212.78, 152.82, 120.04],
                       [234.87, 175.43, 138.94],
                       [151.19, 97.74, 74.59],
                       [120.52, 77.55, 59.82],
                       [192.20, 119.62, 82.32],
                       [214.29, 136.08, 87.24],
                       [99.57, 54.33, 38.06],
                       [238.88, 203.08, 176.91]], dtype=_np.float64)

# Covariance for skin model
cov_sk = _np.asarray([[765.40, 121.44, 112.80],
                      [39.94, 154.44, 396.05],
                      [291.03, 60.48, 162.85],
                      [274.95, 64.60, 198.27],
                      [633.18, 222.40, 250.69],
                      [65.23, 691.53, 609.92],
                      [408.63, 200.77, 257.57],
                      [530.08, 155.08, 572.79],
                      [160.57, 84.52, 243.90],
                      [163.80, 121.57, 279.22],
                      [425.40, 73.56, 175.11],
                      [330.45, 70.34, 151.82],
                      [152.76, 92.14, 259.15],
                      [204.90, 140.17, 270.19],
                      [448.13, 90.18, 151.29],
                      [178.38, 156.27, 404.99]], dtype=_np.float64)

# Weight for skin model
w_sk = _np.asarray([0.0294,
                    0.0331,
                    0.0654,
                    0.0756,
                    0.0554,
                    0.0314,
                    0.0454,
                    0.0469,
                    0.0956,
                    0.0763,
                    0.1100,
                    0.0676,
                    0.0755,
                    0.0500,
                    0.0667,
                    0.0749], dtype=_np.float64)

# Mean value for skin model
mean_n_sk = _np.asarray([[254.37, 254.41, 253.82],
                         [9.39, 8.09, 8.52],
                         [96.57, 96.95, 91.53],
                         [160.44, 162.49, 159.06],
                         [74.98, 63.23, 46.33],
                         [121.83, 60.88, 18.31],
                         [202.18, 154.88, 91.04],
                         [193.06, 201.93, 206.55],
                         [51.88, 57.14, 61.55],
                         [30.88, 26.84, 25.32],
                         [44.97, 85.96, 131.95],
                         [236.02, 236.27, 230.70],
                         [207.86, 191.20, 164.12],
                         [99.83, 148.11, 188.17],
                         [135.06, 131.92, 123.10],
                         [135.96, 103.89, 66.88]], dtype=_np.float64)

# Covariance for non-skin model
cov_n_sk = _np.asarray([[2.77, 2.81, 5.46],
                        [46.84, 33.59, 32.48],
                        [280.69, 156.79, 436.58],
                        [355.98, 115.89, 591.24],
                        [414.84, 245.95, 361.27],
                        [2502.24, 1383.53, 237.18],
                        [957.42, 1766.94, 1582.52],
                        [562.88, 190.23, 447.28],
                        [344.11, 191.77, 433.40],
                        [222.07, 118.65, 182.41],
                        [651.32, 840.52, 963.67],
                        [225.03, 117.29, 331.95],
                        [494.04, 237.69, 533.52],
                        [955.88, 654.95, 916.70],
                        [350.35, 130.30, 388.43],
                        [806.44, 642.20, 350.36]], dtype=_np.float64)

# Weight for non-skin model
w_n_sk = _np.asarray([0.0637,
                      0.0516,
                      0.0864,
                      0.0636,
                      0.0747,
                      0.0365,
                      0.0349,
                      0.0649,
                      0.0656,
                      0.1189,
                      0.0362,
                      0.0849,
                      0.0368,
                      0.0389,
                      0.0943,
                      0.0477], dtype=_np.float64)


class SkinClassifier(_BaseEstimator, _ClassifierMixin):
  """
  Bayesian classifier with Gaussian Mixture Model used as generative model for
  the underlying classes (i.e. Non-skin, Skin)

  Attributes
  ----------
  """

  def __init__(self, use_equal_prior=False):
    """
    Constructor, initialize GMM with pre-trained weight/mean/covariance
    :param use_equal_prior: If `True` set equal prior probability
    (i.e P(y0) = P(y1) = 0.5)
    """

    def _build_model(weight, mean, cov):
      """
      Initialize a GMM from a given weights, means, covariances matrices.
      See: https://stackoverflow.com/questions/42392887
      :param weight:  Mixing weights
      :param mean:    Gaussian's mean
      :param cov:     Gaussian's covariance
      :return:  GMM Model
      """
      from sklearn.mixture.gaussian_mixture import _compute_precision_cholesky
      m = _GMM(n_components=weight.size,
               covariance_type='diag')
      m.weights_ = weight
      m.means_ = mean
      m.covariances_ = cov
      m.precisions_cholesky_ = _compute_precision_cholesky(cov, 'diag')
      return m

    # Build one GMM for each class: P(x|y)
    self.models_ = [_build_model(weight=w_n_sk, mean=mean_n_sk, cov=cov_n_sk),
                    _build_model(weight=w_sk, mean=mean_sk, cov=cov_sk)]
    # Class priors: P(y)
    if not use_equal_prior:
      self.logpriors_ = _np.log(_np.asarray([nsk, ns],
                                            dtype=_np.float64) / (nsk + ns))
    else:
      self.logpriors_ = _np.log(_np.asarray([0.5, 0.5],
                                            dtype=_np.float64))
    # Class labels
    self.classes_ = _np.asarray([0, 1], dtype=_np.int32)

  def fit(self, X, y):
    """
    Fit new model
    :param X: Features
    :param y: Labels
    :return:  `self`
    """
    raise NotImplementedError('Not supported at the moment')

  def predict_proba(self, X):
    """
    Predict posterior probability of each component given the data.
    :param X: Data
    :return:
    """
    shp = X.shape
    x_norm = X.reshape(-1, 3).astype(_np.float64)
    logprobs = _np.array([model.score_samples(x_norm)
                          for model in self.models_]).T
    result = _np.exp(logprobs + self.logpriors_)
    result /= result.sum(1, keepdims=True)
    # Reshape
    return result.reshape(*shp[:-1], -1)

  def predict(self, X):
    return self.classes_[_np.argmax(self.predict_proba(X), 1)]
