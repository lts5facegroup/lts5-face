//
//  main.cpp
//  test_binary_RF_classifier
//
//  Created by Gabriel Cuendet on 09.07.15.
//  Copyright (c) 2015 Gabriel Cuendet. All rights reserved.
//

#include <iostream>
#include <string>
#include <vector>

#include "opencv2/core/core.hpp"

#include "lts5/utils.hpp"
#include "lts5/classifier.hpp"

int main(int argc, const char * argv[]) {
  if (argc < 2) {
    std::cout << "Usage: " << argv[0] << " csvpath" << std::endl;
    return -1;
  }

  std::string csvpath = argv[1];
  cv::Mat raw;
  LTS5::ReadMatFromCSV<float>(csvpath, &raw);

  // CAUTION: Position of labels and features in the csv are hardcoded!
  int id_label = 1;
  int id_feature_start = 2;
  int id_feature_stop = 31;
  // ----------------------

  cv::Mat labels = raw.col(id_label);
  cv::Mat data = raw.colRange(id_feature_start, id_feature_stop);

  std::vector<cv::Mat> pos_samples;
  std::vector<cv::Mat> neg_samples;

  for (int i = 0; i < raw.rows; ++i) {
    cv::Mat row = raw.row(i).clone();
    if (row.at<float>(id_label) == 1.0) {
      pos_samples.push_back(row.colRange(id_feature_start, id_feature_stop));
    }
    else if (row.at<float>(id_label) == 0.0) {
      neg_samples.push_back(row.colRange(id_feature_start, id_feature_stop));
    }
  }

  LTS5::BinaryRFClassifier classifier;

  /*
  classifier.Train(pos_samples, neg_samples);
  double acc = 0.0;

  const int k_test_samples = 100;
  for (int s = 0; s<k_test_samples; ++s) {
    double score = classifier.Classify(data.row(s));

    acc += 1 - (labels.at<float>(s) == score ? 0.0 : 1.0);
  }

  std::cout << "Accuracy = " << acc/k_test_samples << std::endl;
   */

  LTS5::BinaryCrossValidation cv(classifier, pos_samples, neg_samples, LTS5::BinaryCrossValidation::ScoringType::kAccuracy, 5, true);
  cv.RunCrossValidation();

  std::cout << "Accuracy = " << cv.GetMeanScore() << "  +/- " << cv.GetScoreStdDev() << std::endl;
  return 0;
}
