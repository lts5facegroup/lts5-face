/**
 *  @file   f1_scoring.hpp
 *  @brief  Compute F1 score for a given binary classification system
 *  @ingroup classifier
 *
 *  @author Christophe Ecabert
 *  @date   20/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_F1_SCORING__
#define __LTS5_F1_SCORING__

#include "lts5/utils/library_export.hpp"
#include "lts5/classifier/base_scoring_metric.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   F1Scoring
 * @brief   Compute F1 score for a given binary classification system
 * @author  Christophe Ecabert
 * @date    20/04/16
 * @ingroup classifier
 */
class LTS5_EXPORTS F1Scoring : public LTS5::BaseScoringMetric {
public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  F1Scoring
   * @fn    F1Scoring(void)
   * @brief Constructor
   * @param[in] n_fold  Number of fold to score
   */
  explicit F1Scoring(const int n_fold);

#pragma mark -
#pragma mark Compute

  /**
   * @name  Compute
   * @fn    double Compute(void)
   * @brief Compute f1 score
   * @param[in] k_fold  Selected fold
   * @return  F1 score
   */
  double Compute(const int k_fold);

};

}  // namepsace LTS5

#endif //__LTS5_F1_SCORING__
