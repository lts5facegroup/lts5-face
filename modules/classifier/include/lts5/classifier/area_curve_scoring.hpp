/**
 *  @file   area_curve_scoring.hpp
 *  @brief  Compute area under roc curve
 *          Based on Anil/Hua's work
 *  @ingroup classifier
 *
 *  @author Christophe Ecabert
 *  @date   25/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_AREA_CURVE_SCORING__
#define __LTS5_AREA_CURVE_SCORING__

#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/classifier/base_scoring_metric.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   AreaUnderCurveScoring
 * @brief   Compute area under roc curve
 * @author  Christophe Ecabert
 * @date    25/04/2016
 * @ingroup classifier
 */
class LTS5_EXPORTS AreaUnderCurveScoring : public LTS5::BaseScoringMetric {

#pragma mark -
#pragma mark Initialization
 public:
  /**
   * @name  AreaUnderCurveScoring
   * @fn  explicit AreaUnderCurveScoring(const int n_fold)
   * @brief Constructor
   * @param[in] n_fold  Number of fold to score
   */
  explicit AreaUnderCurveScoring(const int n_fold);

#pragma mark -
#pragma mark Usage

  /**
   * @name  AddPositivePrediction
   * @fn void AddPositivePrediction(const int k_fold,
   *                                const double pred,
                                    const double threshold)
   * @brief Add prediction result for positive samples (Fill TP, FN)
   * @param[in] k_fold    Fold  metric to fill
   * @param[in] pred      Prediction value
   * @param[in] threshold Classification threshold (default 0.0)
   */
  void AddPositivePrediction(const int k_fold,
                             const double pred,
                             const double threshold);

  /**
   * @name  AddNegativePrediction
   * @fn void AddNegativePrediction(const int k_fold,
   *                                const double pred,
                                    const double threshold)
   * @brief Add prediction result for negative samples (Fill TN, FP)
   * @param[in] k_fold    Fold  metric to fill
   * @param[in] pred      Prediction value
   * @param[in] threshold Classification threshold (default 0.0)
   */
  void AddNegativePrediction(const int k_fold,
                             const double pred,
                             const double threshold);

  /**
   * @name  Compute
   * @fn    double Compute(const int k_fold)
   * @brief Compute scoring metric for a given fold
   * @param[in] k_fold  Selected fold
   * @return  Score
   */
  double Compute(const int k_fold);

  /**
   * @name  Clear
   * @fn void Clear(void)
   * @brief Reset counters
   */
  void Clear(void);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  set_number_step
   * @fn  void set_number_step(const int n_step)
   * @brief Set how many steps are used in area calculation
   * @param[in] n_step  Number of steps
   */
  void set_number_step(const int n_step) {
    n_step_ = n_step;
  }

#pragma mark -
#pragma mark Private
 private:
  /** Positive sample prediction result */
  std::vector<std::vector<double>> pos_pred_;
  /** Negative sample prediction result */
  std::vector<std::vector<double>> neg_pred_;
  /** Number of step to use in areae calculation */
  int n_step_;

};

}  // namepsace LTS5

#endif //__LTS5_AREA_CURVE_SCORING__
