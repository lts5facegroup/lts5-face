/**
 *  @file   binaryclassifier.hpp
 *  @brief  Binary classifier definition
 *  @ingroup classifier
 *
 *  @author Hua Gao
 *  @date   23/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#ifndef __LTS5_BINARY_CLASSIFIER__
#define __LTS5_BINARY_CLASSIFIER__

#include <vector>

#include "opencv2/core/core.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  BinaryClassifier
 *  @brief  Abstract class to define basic binary classifier interface
 *  @author Hua Gao
 *  @date   23/04/15
 *  @tparam Type of matrix, either cv::Mat or cv::SparseMat
 *  @ingroup classifier
 */
template<typename T>
class BinaryClassifier {
 public:
  /**
   *  @name ~BinaryClassifier
   *  @fn virtual ~BinaryClassifier()
   *  @brief  Destructor
   */
  virtual ~BinaryClassifier() {}

  /**
   *  @name   Train
   *  @fn virtual void Train(const std::vector<T>& positive_samples,
                             const std::vector<T>& negative_samples) = 0
   *  @brief  Train binary classifier
   *  @param[in] positive_samples Positive samples
   *  @param[in] negative_samples Negative samples
   */
  virtual void Train(const std::vector<T>& positive_samples,
                     const std::vector<T>& negative_samples) = 0;

  /**
   *  @name   Classify
   *  @fn virtual double Classify(const T& x) const = 0
   *  @brief  Classify new sample, This function returns the classification
   *          result for a new sample. The sign of the returned value is
   *          negative for negative predictions and positive for positive
   *          predictions.
   *  @param[in] x Sample to classify
   *  @return Classification result
   */
  virtual double Classify(const T& x) const = 0;

  /**
   *  @name   Clone
   *  @fn virtual BinaryClassifier* Clone() const = 0
   *  @brief  Clone classifier.
   *  @return Deep copy of classifier
   */
  virtual BinaryClassifier* Clone() const = 0;

  /**
   *  @name   Load
   *  @fn virtual int Load(const std::string& infile) = 0
   *  @brief  Load classifier from file.
   *  @param[in] infile File to load the classifier from.
   */
  virtual int Load(const std::string& infile) = 0;

  /**
   *  @name   Save
   *  @fn virtual int Save(const std::string& outfile) const = 0
   *  @brief  Save classifier to file.
   *  @param[in] outfile File to save the classifier to.
   */
  virtual int Save(const std::string& outfile) const = 0;
};
}

#endif /* __LTS5_BINARY_CLASSIFIER__ */
