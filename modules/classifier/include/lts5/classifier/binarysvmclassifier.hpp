/**
 *  @file   binarysvmclassifier.hpp
 *  @brief  Binary SVM classifier definition
 *  @ingroup classifier
 *
 *  @author Hua Gao
 *  @date   23/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#ifndef __LTS5_BINARY_SVM_CLASSIFIER__
#define __LTS5_BINARY_SVM_CLASSIFIER__

#include <memory>

#include "lts5/classifier/binaryclassifier.hpp"
#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @enum   BinarySVMType
 *  @brief  Type of SVM formulation
 *  @ingroup classifier
 */
enum BinarySVMType {
  /** C SVM */
  C_SVM,
  /** Nu SVM */
  NU_SVM
};
/**
 *  @enum   BinarySVMKernel
 *  @brief  SVM kernel function
 *  @ingroup classifier
 */
enum BinarySVMKernel {
  /** Linear Kernel */
  LINEAR,
  /** Polynomial Kernel */
  POLYNOMIAL,
  /** Gaussian Kernel */
  GAUSSIAN,
  /** Sigmoid Kernel */
  SIGMOID,
  /** Precomputed Kernel */
  PRECOMPUTED
};

/**
 *  @class  BinarySVMClassifier
 *  @brief  Class to define binary SVM classifier interface
 *          Binary classifier using SVM classification. This class provides
 *          two-class classification using support vector machines.
 *          It uses LibSVM.
 *  @author Hua Gao
 *  @date   23/04/15
 *  @ingroup classifier
 */
class LTS5_EXPORTS BinarySVMClassifier : public BinaryClassifier<cv::Mat> {
 public:
  /**
   *  @name BinarySVMClassifier
   *  @fn BinarySVMClassifier(BinarySVMType type = C_SVM,
                              double slack_param = -1.0,
                              BinarySVMKernel kernel = GAUSSIAN,
                              double gamma = -1.0, double coef0 = 0.0,
                              int degree = 3, bool shrinking = true,
                              bool probability = false,
                              double positive_weight = 1.0,
                              double negative_weight = 1.0,
                              double eps = 0.001,
                              double cache_size = 100)
   *  @brief Constructor
   *  @param[in] type         Type of SVM formulation
   *  @param[in] slack_param  Value of the slack parameter (C or nu)
   *  @param[in] kernel       Type of SVM kernel
   *  @param[in] gamma        Kernel parameter gamma
   *  @param[in] coef0        Kernel parameter coef0
   *  @param[in] degree       Degree of polynomial kernel
   *  @param[in] shrinking    Whether shrinking is to be used
   *  @param[in] probability  Whether probabilities shall be returned
   *  @param[in] positive_weight Penalty weight for positive misclassifications
   *  @param[in] negative_weight Penalty weight for negative misclassifications
   *  @param[in] eps Stopping criterion threshold
   *  @param[in] cache_size Kernel cache size in MB
   */
  BinarySVMClassifier(BinarySVMType type = C_SVM,
                      double slack_param = -1.0, BinarySVMKernel kernel = GAUSSIAN,
                      double gamma = -1.0, double coef0 = 0.0,
                      int degree = 3, bool shrinking = true,
                      bool probability = false, double positive_weight = 1.0,
                      double negative_weight = 1.0, double eps = 0.001,
                      double cache_size = 100);
  /**
   *  @name   ~BinarySVMClassifier
   *  @fn     ~BinarySVMClassifier(void)
   *  @brief  Destructor
   */
  ~BinarySVMClassifier(void);

  /**
   *  @name   BinarySVMClassifier
   *  @fn     BinarySVMClassifier(const BinarySVMClassifier& other)
   *  @brief  Copy constructor
   *  @param[in]  other Other classifier
   */
  BinarySVMClassifier(const BinarySVMClassifier& other);

  /**
   *  @name   ~BinarySVMClassifier
   *  @fn     BinarySVMClassifier& operator = (const BinarySVMClassifier& other)
   *  @brief  Assignment operator
   *  @param[in]  other   Other classifier
   */
  BinarySVMClassifier& operator = (const BinarySVMClassifier& other);

  /**
   *  @name   Train
   *  @fn void Train(const std::vector<cv::Mat>& positive_samples,
                     const std::vector<cv::Mat>& negative_samples)
   *  @brief  Train binary SVM
   *  @param[in] positive_samples Positive samples
   *  @param[in] negative_samples Negative samples
   */
  void Train(const std::vector<cv::Mat>& positive_samples,
             const std::vector<cv::Mat>& negative_samples);

  /**
   *  @name   Classify
   *  @fn double Classify(const cv::Mat& x) const
   *  @brief  Classify new sample
   * This function returns the distance to the decision hyperplane scaled
   * by |w|. The sign of the returned value is negative for negative
   * predictions and positive for positive predictions. Its magnitude
   * can be used as a confidence value.
   *  @param[in] x Sample to classify
   *  @return Distance to hyperplane (scaled)
   */
  double Classify(const cv::Mat& x) const;

  /**
   *  @name   NumPositiveSupportVectors
   *  @fn int NumPositiveSupportVectors() const
   *  @brief  Return number of positive support vectors
   *  @return value
   */
  int NumPositiveSupportVectors() const;

  /**
   *  @name   NumNegativeSupportVectors
   *  @fn int NumNegativeSupportVectors() const
   *  @brief  Return number of negative support vectors
   *  @return value
   */
  int NumNegativeSupportVectors() const;

  /**
   *  @name   Clone
   *  @fn BinaryClassifier* Clone() const
   *  @brief  Clone classifier
   *  @return Deep copy of classifier
   */
  BinaryClassifier* Clone() const;

  /**
   *  @name   GetClassProbabilities
   *  @fn void GetClassProbabilities(const cv::Mat& x,
                                     double& pos_prob,
                                     double& neg_prob) const
   *  @brief  Get class probabilities
   *  @param[in] x Sample to classify
   *  @param[out] pos_prob Probability of x being a positive sample
   *  @param[out] neg_prob Probability of x being a negative sample
   */
  void GetClassProbabilities(const cv::Mat& x, double& pos_prob, double& neg_prob) const;

  /**
   *  @name   Save
   *  @fn int Save(const std::string& outfile) const
   *  @brief  Save trained SVM model
   *  @param[in] outfile File to save the model to
   */
  int Save(const std::string& outfile) const;

  /**
   *  @name   Load
   *  @fn int Load(const std::string& infile)
   *  @brief  Load saved SVM model
   *  @param[in] infile File to load the model from
   */
  int Load(const std::string& infile);

private:
  struct Impl;
  std::unique_ptr<Impl> _pi;
};
}
#endif /* __LTS5_BINARY_SVM_CLASSIFIER__ */
