/**
 *  @file   base_scoring_metric.hpp
 *  @brief  Define base interface for different scoring methods used to evaluate
 *          BINARY classification performance
 *  @ingroup classifier
 *
 *  @author Christophe Ecabert
 *  @date   20/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_BASE_SCORING_METRIC__
#define __LTS5_BASE_SCORING_METRIC__

#include <cstddef>
#include <vector>
#include <algorithm>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   BaseScoringMetric
 * @brief   Define base interface for different scoring methods used to evaluate
 *          BINARY classification performance
 * @author  Christophe Ecabert
 * @date    20/04/16
 * @ingroup classifier
 */
class LTS5_EXPORTS BaseScoringMetric {

 public:

  /**
   * @enum  ScoringType
   * @brief Type of scoring metrics
   */
  enum ScoringType {
    /** Accuracy */
    kAccuracy,
    /** Precision */
    kPrecion,
    /** Recall */
    kRecall,
    /** F1 */
    kF1,
    /** Area under Curve */
    kAreaUnderCurve
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name  BaseScoringMetric
   * @fn  explicit BaseScoringMetric(const int n_fold)
   * @brief Constructor
   * @param[in] n_fold  Number of fold to score
   */
  explicit BaseScoringMetric(const int n_fold) : tp_(n_fold, 0),
                                                 tn_(n_fold, 0),
                                                 fp_(n_fold, 0),
                                                 fn_(n_fold, 0) {
  }

  /**
   * @name  ~BaseScoringMetric
   * @fn  virtual ~BaseScoringMetric(void)
   * @brief Destructor
   */
  virtual ~BaseScoringMetric(void) {}

  /**
   * @name  BaseScoringMetric
   * @fn  BaseScoringMetric(const BaseScoringMetric& other) = default
   * @brief Copy Constructor
   */
  BaseScoringMetric(const BaseScoringMetric& other) = default;

  /**
   * @name  operator=
   * @fn  BaseScoringMetric& operator=(const BaseScoringMetric& rhs) = default
   * @brief Assignment operator
   */
  BaseScoringMetric& operator=(const BaseScoringMetric& rhs) = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  AddPositivePrediction
   * @fn virtual void AddPositivePrediction(const int k_fold,
   *                                        const double pred,
                                            const double threshold)
   * @brief Add prediction result for positive samples (Fill TP, FN)
   * @param[in] k_fold    Fold  metric to fill
   * @param[in] pred      Prediction value
   * @param[in] threshold Classification threshold (default 0.0)
   */
  virtual void AddPositivePrediction(const int k_fold,
                                     const double pred,
                                     const double threshold) {
    if (pred > threshold) {
      tp_[k_fold]++;
    } else {
      fn_[k_fold]++;
    }
  }

  /**
   * @name  AddNegativePrediction
   * @fn virtual void AddNegativePrediction(const int k_fold,
   *                                        const double pred,
                                            const double threshold)
   * @brief Add prediction result for negative samples (Fill TN, FP)
   * @param[in] k_fold    Fold  metric to fill
   * @param[in] pred      Prediction value
   * @param[in] threshold Classification threshold (default 0.0)
   */
  virtual void AddNegativePrediction(const int k_fold,
                                     const double pred,
                                     const double threshold) {
    if (pred < threshold) {
      tn_[k_fold]++;
    } else {
      fp_[k_fold]++;
    }
  }

  /**
   * @name  Compute
   * @fn    virtual double Compute(const int k_fold) = 0
   * @brief Compute scoring metric for a given fold
   * @param[in] k_fold  Selected fold
   * @return  Score
   */
  virtual double Compute(const int k_fold) = 0;

  /**
   * @name  Clear
   * @fn virtual void Clear(void)
   * @brief Reset counters
   */
  virtual void Clear(void) {
    std::fill(tp_.begin(), tp_.end(), 0);
    std::fill(tn_.begin(), tn_.end(), 0);
    std::fill(fp_.begin(), fp_.end(), 0);
    std::fill(fn_.begin(), fn_.end(), 0);
  }

#pragma mark -
#pragma mark Protected
 protected:

  /** True positive */
  std::vector<std::size_t> tp_;
  /** True negative */
  std::vector<std::size_t> tn_;
  /** False positive */
  std::vector<std::size_t> fp_;
  /** False negative */
  std::vector<std::size_t> fn_;
};

}  // namepsace LTS5

#endif //__LTS5_BASE_SCORING_METRIC__
