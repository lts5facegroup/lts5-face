/**
 *  @file   pca.hpp
 *  @brief  PCA methods implementation
 *  @ingroup classifier
 *
 *  @author Gabriel Cuendet
 *  @date   04/06/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_PCA__
#define __LTS5_PCA__

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @enum PcaComputationType
 *  @brief  List the possible methods to compute PCA
 *  @ingroup classifier
 */
enum PcaComputationType {
  /** Decompose using OpenCV */
  kDefault = 0x01,
  /** Decompose using LAPACK optimized function */
  kLapack = 0x02,
  /** Decompose using CULA optimized function (GPU) */
  kCula = 0x04
};

/**
 *  @name ComputePca
 *  @fn     void ComputePca(const cv::Mat& data,
                             const double variance,
                             const PcaComputationType method,
                             cv::Mat* eigenvectors,
                             cv::Mat* eigenvalues)
 *  @brief  Calls the appropriate implementation of PCA
 *  @ingroup classifier
 *  @param[in]  data          Matrix of data. Rows are samples, cols are
 *                            features (or dimensions)
 *  @param[in]  variance      Percentage of variance to keep in ]0.-1.]
 *  @param[in]  method        Computation method
 *  @param[out] eigenvectors  PCA basis, matrix of eigenvectors
 *  @param[out] eigenvalues   Corresponding eigenvalues, if null not exported
 */
void LTS5_EXPORTS ComputePca(const cv::Mat& data,
                             const double variance,
                             const PcaComputationType method,
                             cv::Mat* eigenvectors,
                             cv::Mat* eigenvalues = nullptr);

/**
 *  @name ComputePca
 *  @fn   void ComputePca(const cv::Mat& data,
                             const int n_eigenvector,
                             const PcaComputationType method,
                             cv::Mat* eigenvectors,
                             cv::Mat* eigenvalues)
 *  @brief Calls the appropriate implementation of PCA
 *  @ingroup classifier
 *  @param[in]  data          Matrix of data. Rows are samples, cols are
 *                              features (or dimensions)
 *  @param[in]  n_eigenvector Number of eigenvector to keep
 *  @param[in]  method        Computation method
 *  @param[out] eigenvectors  PCA basis, matrix of eigenvectors
 *  @param[out] eigenvalues   Corresponding eigenvalues, if null not exported
 */
void LTS5_EXPORTS ComputePca(const cv::Mat& data,
                             const int n_eigenvector,
                             const PcaComputationType method,
                             cv::Mat* eigenvectors,
                             cv::Mat* eigenvalues = nullptr);


}  // namespace LTS5
#endif /* __LTS5_PCA__ */
