/**
 *  @file   lts5/classifier/classifier.hpp
 *  @brief  Regroup all element defined in classifier modules
 *  @author Hua Gao
 *  @date   23/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#ifndef __LTS5_CLASSIFIER__
#define __LTS5_CLASSIFIER__

/** Binary classifier interface */
#include "lts5/classifier/binaryclassifier.hpp"
/** Binary linear svm classifier */
#include "lts5/classifier/binarylinearsvmclassifier.hpp"
/** Binary svm classifier */
#include "lts5/classifier/binarysvmclassifier.hpp"
/** Binary random forest */
#include "lts5/classifier/binary_RF_classifier.hpp"
/** Cross-validation tool for binary classifier */
#include "lts5/classifier/crossvalidation.hpp"
/** PCA */
#include "lts5/classifier/pca.hpp"
/** Accuracy scoring metrics */
#include "lts5/classifier/accuracy_scoring.hpp"
/** F1 scoring metrics */
#include "lts5/classifier/f1_scoring.hpp"
/** Precision scoring metrics */
#include "lts5/classifier/precision_scoring.hpp"
/** Recall scoring metrics */
#include "lts5/classifier/recall_scoring.hpp"

#endif // __LTS5_CLASSIFIER__
