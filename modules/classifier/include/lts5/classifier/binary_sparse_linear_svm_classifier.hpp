/**
 *  @file   binary_sparse_linear_svm_classifier.hpp
 *  @brief  Binary sparse linear SVM classifier definition
 *  @ingroup classifier
 *
 *  @author Hua Gao
 *  @date   23/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#ifndef __LTS5_BINARY_SPARSE_LINEAR_SVM_CLASSIFIER__
#define __LTS5_BINARY_SPARSE_LINEAR_SVM_CLASSIFIER__

#include <memory>
#include <vector>

#include "lts5/classifier/binaryclassifier.hpp"
#include "lts5/classifier/binarylinearsvmclassifier.hpp"
#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BinarySparseLinearSVMClassifier
 *  @brief  Class to define binary linear SVM classifier interface
 *
 *  Binary classifier using linear SVM classification. This class provides
 *  two-class classification using linear support vector machines.
 *  It uses LibLinear.
 *
 *  @author Hua Gao
 *  @date   23/04/15
 *  @ingroup classifier
 */
class LTS5_EXPORTS BinarySparseLinearSVMClassifier : public BinaryClassifier<cv::SparseMat> {
private:
  /**
   *  @struct Impl
   *  @brief  Parameters for liblinear
   */
  struct Impl;
  std::unique_ptr<Impl> _pi;

public:

  /**
   *  @name   BinarySparseLinearSVMClassifier
   *  @fn BinarySparseLinearSVMClassifier(double C = 1.0,
                                    BinaryLinearSVMTraining type = L2R_L2LOSS_SVC,
                                    double eps = 0.0001,
                                    double positive_weight = 1.0,
                                    double negative_weight = 1.0,
                                    double bias = 1.0)
   *  @brief  Constructor
   *  @param[in] C Value of the slack parameter C
   *  @param[in] type Type of SVM solver
   *  @param[in] eps Stopping criterion threshold
   *  @param[in] positive_weight Penalty weight for positive misclassifications
   *  @param[in] negative_weight Penalty weight for negative misclassifications
   *  @param[in] bias Bias value to use
   */
  BinarySparseLinearSVMClassifier(double C = 1.0,
                                  BinaryLinearSVMTraining type = L2R_L2LOSS_SVC,
                                  double eps = 0.0001,
                                  double positive_weight = 1.0,
                                  double negative_weight = 1.0,
                                  double bias = 1.0);

  /**
   *  @name ~BinarySparseLinearSVMClassifier
   *  @fn ~BinarySparseLinearSVMClassifier()
   *  @brief  Destructor
   */
  ~BinarySparseLinearSVMClassifier();

  /**
   *  @name   BinarySparseLinearSVMClassifier
   *  @fn BinarySparseLinearSVMClassifier(const BinarySparseLinearSVMClassifier& other)
   *  @brief  Copy constructor
   */
  BinarySparseLinearSVMClassifier(const BinarySparseLinearSVMClassifier& other);

  /**
   *  @name   BinarySparseLinearSVMClassifier
   *  @fn BinarySparseLinearSVMClassifier& operator = (const BinarySparseLinearSVMClassifier& other)
   *  @brief  Assignment operator
   */
  BinarySparseLinearSVMClassifier& operator=(const BinarySparseLinearSVMClassifier& other);

  /**
   *  @name  Train
   *  @fn void Train(const std::vector<cv::Mat>& positive_samples,
                     const std::vector<cv::Mat>& negative_samples)
   *  @brief Train binary SVM
   *  @param[in] positive_samples Positive samples
   *  @param[in] negative_samples Negative samples
   */
  /*void Train(const std::vector<cv::Mat>& positive_samples,
             const std::vector<cv::Mat>& negative_samples);*/

  /**
   *  @name  Train
   *  @fn void Train(const std::vector<cv::SparseMat>& positive_samples,
                     const std::vector<cv::SparseMat>& negative_samples)
   *  @brief Train binary SVM
   *  @param[in] positive_samples Positive samples
   *  @param[in] negative_samples Negative samples
   */
  void Train(const std::vector<cv::SparseMat>& positive_samples,
             const std::vector<cv::SparseMat>& negative_samples);

  /**
   *  @name Classify
   *  @fn double Classify(const cv::Mat& x) const
   *  @brief  Classify new sample
   *  This function returns the distance to the decision hyperplane scaled
   *  by |w|. The sign of the returned value is negative for negative
   *  predictions and positive for positive predictions. Its magnitude
   *  can be used as a confidence value.
   *  @param[in] x Sample to classify
   *  @return Distance to hyperplane (scaled)
   */
  //double Classify(const cv::Mat& x) const;

  /**
   *  @name Classify
   *  @fn double Classify(const cv::SparseMat& x) const
   *  @brief  Classify new sample
   *  This function returns the distance to the decision hyperplane scaled
   *  by |w|. The sign of the returned value is negative for negative
   *  predictions and positive for positive predictions. Its magnitude
   *  can be used as a confidence value.
   *  @param[in] x Sample to classify
   *  @return Distance to hyperplane (scaled)
   */
  double Classify(const cv::SparseMat& x) const;

  /**
   *  @name   Clone
   *  @fn BinaryClassifier* Clone() const
   *  @brief  Clone classifier.
   *  @return Deep copy of classifier
   */
  BinaryClassifier* Clone() const;

  /**
   *  @name   Save
   *  @fn int Save(const std::string& outfile) const;
   *  @brief  Save trained SVM model
   *  @param[in] outfile File to save the model to
   *  @return -1 if error, 0 otherwise
   */
  int Save(const std::string& outfile) const;

  /**
   *  @name   Save
   *  @fn int Save(std::ostream& out_stream) const
   *  @brief  Save trained SVM model
   *  @param[in] out_stream   Output stream to a file
   *  @return -1 if error, 0 otherwise
   */
  int Save(std::ostream& out_stream) const;

  /**
   *  @name   Load
   *  @fn int Load(const std::string& infile)
   *  @brief  Load saved SVM model
   *  @param[in] infile File to load the model from
   *  @return -1 if error, 0 otherwise
   */
  int Load(const std::string& infile);

  /**
   *  @name   Load
   *  @fn int Load(std::istream& input_stream)
   *  @brief  Load saved SVM model from binary file
   *  @param[in] input_stream Input stream to load the model from
   *  @return -1 if error, 0 otherwise
   */
  int Load(std::istream& input_stream);

  /**
   *  @name   ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Provide classifier memory size in bytes
   *  @return Memory size in bytes
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   GetW
   *  @fn     cv::Mat GetW() const
   *  @brief  Get vector w
   *  @return w
   */
  cv::Mat GetW() const;

  /**
   *  @name   GetBias
   *  @fn     double GetBias() const
   *  @brief  Get bias b
   *  @return b
   */
  double GetBias() const;

  /**
   *  @name   GetFeatureLength
   *  @fn     int GetFeatureLength() const
   *  @brief  Provide feature space size
   *  @return Length of the feature
   */
  int GetFeatureLength() const;

  /**
   *  @name set_svm_type
   *  @fn     void set_svm_type(const BinaryLinearSVMTraining& svm_type)
   *  @brief  Set SVM type
   *  @param[in] svm_type   Type of svm
   */
  void set_svm_type(const BinaryLinearSVMTraining& svm_type);

  /**
   *  @name   set_c_coef
   *  @fn     void set_c_coef(double c_coef)
   *  @brief  Set C svm's parameter
   *  @param  c_coef  Value of C
   */
  void set_c_coef(double c_coef);
};
}
#endif /* __LTS5_BINARY_SPARSE_LINEAR_SVM_CLASSIFIER__ */
