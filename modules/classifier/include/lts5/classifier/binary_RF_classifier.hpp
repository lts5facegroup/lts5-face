/**
 *  @file   binary_RF_classifier.hpp
 *  @brief  Binary OpenCV Random Forest classifier warper
 *  @ingroup classifier
 *
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_BINARY_RF_CLASSIFIER__
#define __LTS5_BINARY_RF_CLASSIFIER__

#include <stdio.h>
#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/ml/ml.hpp"

#include "lts5/classifier/binaryclassifier.hpp"
#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  BinaryRFClassifier
 *  @brief  Binary Random Forest classifier implementation
 *  @author Gabriel Cuendet
 *  @date   07/07/2015
 *  @ingroup classifier
 */
class LTS5_EXPORTS BinaryRFClassifier : public BinaryClassifier<cv::Mat> {
 public:
  static void Vec1DMat22DMat(const std::vector<cv::Mat>& vec, cv::Mat* mat);

  /**
   *  @name   Vec2Mat
   *  @fn static void Vec2Mat(const std::vector<cv::Mat>& positive_samples,
                              const std::vector<cv::Mat>& negative_samples,
                              cv::Mat* data, cv::Mat* labels)
   *  @brief  Utility method to convert 2 vectors of 1D Mat (pos and neg samples
   *          into a 2D Mat of data (rows are samples, columns are features) and
   *          a 1D Mat of labels
   *  @param[in] positive_samples Positive samples
   *  @param[in] negative_samples Negative samples
   *  @param[out] data   2D Mat containing the data
   *  @param[out] labels 1D Mat containing the labels of the data
   */
  static void Vec2Mat(const std::vector<cv::Mat>& positive_samples,
                      const std::vector<cv::Mat>& negative_samples,
                      cv::Mat* data, cv::Mat* labels);

  /**
   *  @name BinaryRFClassifier
   *  @fn BinaryRFClassifier(void)
   *  @brief Constructor
   */
  BinaryRFClassifier(void);

  /**
   *  @name BinaryRFClassifier
   *  @fn BinaryRFClassifier(const BinaryRFClassifier& other)
   *  @brief Copy constructor
   */
  BinaryRFClassifier(const BinaryRFClassifier& other);

  /**
   *  @name ~BinaryRFClassifier
   *  @fn ~BinaryRFClassifier(void)
   *  @brief Destructor
   */
  ~BinaryRFClassifier(void);

  /**
   *  @name   Train
   *  @fn void Train(const std::vector<cv::Mat>& positive_samples,
                     const std::vector<cv::Mat>& negative_samples)
   *  @brief  Train binary classifier
   *  @param[in] positive_samples Positive samples
   *  @param[in] negative_samples Negative samples
   */
  void Train(const std::vector<cv::Mat>& positive_samples,
             const std::vector<cv::Mat>& negative_samples);

  /**
   *  @name   Classify
   *  @fn double Classify(const cv::Mat& x) const
   *  @brief  Classify new sample
   * This function returns the classification result for a new sample.
   * The sign of the returned value is negative for negative
   * predictions and positive for positive predictions.
   *  @param[in] x Sample to classify
   *  @return Classification result
   */
  double Classify(const cv::Mat& x) const;

  /**
   *  @name   Clone
   *  @fn BinaryClassifier* Clone() const
   *  @brief  Clone classifier.
   *  @return Deep copy of classifier
   */
  BinaryClassifier* Clone() const;

  /**
   *  @name   Load
   *  @fn int Load(const std::string& infile)
   *  @brief  Load classifier from file.
   *  @param[in] infile File to load the classifier from.
   */
  int Load(const std::string& infile);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& outfile) const
   *  @brief  Save classifier to file.
   *  @param[in] outfile File to save the classifier to.
   */
  int Save(const std::string& outfile) const;

  /**
   *  @name   parameters
   *  @fn CvRTParams parameters(void) const
   *  @brief  parameters getter
   *  @return parameters
   */
  CvRTParams parameters(void) const {
    return parameters_;
  }

  /**
   *  @name   set_parameters
   *  @fn void set_parameters(CvRTParams rhs)
   *  @brief  parameters setter
   *  @param[in] rhs  parameters
   */
  void set_parameters(CvRTParams rhs) {
    parameters_ = rhs;
  }

 private:
  /** Parameters of the random trees*/
  CvRTParams parameters_;
  /** Specifies the orientation of the data matrix */
  int tflag_ = CV_ROW_SAMPLE;
  /** Classifier */
  CvRTrees* classifier_;
};
}  // namespace LTS5
#endif /* __LTS5_BINARY_RF_CLASSIFIER__ */
