/**
 *  @file   kl_basis_wrapper.cpp
 *  @brief  Wrapper for python interface
 *  @ingroup    python
 *
 *  @author Christophe Ecabert
 *  @date   03/02/2021
 *  Copyright (c) 2021 Christophe Ecabert. All rights reserved.
 */

#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"
#include "pybind11/stl.h"

#include "kernel_wrapper.hpp"

#include "lts5/python/ocv_converter.hpp"
#include "lts5/kernel/kl_basis.hpp"

#include "math_converter.hpp"

namespace py = pybind11;
using namespace pybind11::literals;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

template<int NDim, typename T>
struct DimAndTypeToStr;

#define DIM_AND_TYPE_TO_STR_SPECIALIZATION(ndim, type, suffix)        \
  template<>                                                          \
  struct DimAndTypeToStr<ndim, type> {                                \
    static constexpr const char* const value = suffix;                \
  }

DIM_AND_TYPE_TO_STR_SPECIALIZATION(1, float, "1f");
DIM_AND_TYPE_TO_STR_SPECIALIZATION(2, float, "2f");
DIM_AND_TYPE_TO_STR_SPECIALIZATION(3, float, "3f");
DIM_AND_TYPE_TO_STR_SPECIALIZATION(4, float, "4f");
DIM_AND_TYPE_TO_STR_SPECIALIZATION(1, double, "1d");
DIM_AND_TYPE_TO_STR_SPECIALIZATION(2, double, "2d");
DIM_AND_TYPE_TO_STR_SPECIALIZATION(3, double, "3d");
DIM_AND_TYPE_TO_STR_SPECIALIZATION(4, double, "4d");


template<int NDim, typename T>
void ExposeDiscreteDomain(py::module& m) {
  using PointList = std::vector<PointType<NDim, T>>;
  using TriList = std::vector<Vector3<int>>;
  using DDomBase = DiscreteDomain<NDim, T>;
  using DDomBasePtr = typename DDomBase::Ptr;
  using UnstructDom = UnstructuredPointDomain<NDim, T>;
  using UnstructDomPtr = typename UnstructDom::Ptr;
  using TriMesh = TriangleMesh<T>;
  using TriMeshPtr = typename TriMesh::Ptr;

  // Base class with python
  auto base_name = "DiscreteDomain" + std::string(DimAndTypeToStr<NDim, T>::value);
  py::class_<DDomBase, DDomBasePtr>(m,
                       base_name.c_str(),
                       R"doc(Interface for discrete function domain definition)doc")
                       .def("load",
                            py::overload_cast<const std::string&>(&DDomBase::Load),
                                    "filename"_a,
                                    R"doc(Load domain from file
> filename: Path to file)doc")
          .def("save",
               py::overload_cast<const std::string&>(&DDomBase::Save, py::const_),
                       "filename"_a,
                       R"doc(Save domain to file
> filename: Path to file)doc")
          .def("is_defined_at",
               &DDomBase::IsDefinedAt,
               "x"_a,
               R"doc(Check if function is defined at `x`. Must be implemented
 by subclass
> x: Location where to look for definition)doc")

          .def("find_closest_point",
               &DDomBase::FindClosestPoint,
               "x"_a,
               R"doc(Look for closest point to `x` where function is
 defined. Must be implemented by subclass.
> x: Query points)doc")
          .def("find_nclosest_point",
               &DDomBase::FindNClosestPoint,
               "x"_a,
               "n"_a,
               R"doc(Look for N closest point to `x` where function is
 defined. Must be implemented by subclass.
> x: Query point
> n: Number of points to look for)doc")
          .def("index",
               &DDomBase::Index,
               "x"_a,
               R"doc(Look for index of a given point `x`, if not known,
 returns -1.
> x: Query point)doc")
          .def_property_readonly("size",
                                 &DDomBase::Size,
                                 R"doc(Domain size (i.e. number of points))doc")
          .def_property_readonly("points",
                                 &DDomBase::get_points,
                                 R"doc(Accessor for underlying points in the domain)doc")
;

  // Unstructured
  // https://pybind11.readthedocs.io/en/stable/advanced/smart_ptrs.html
  auto unstruct_name = "UnstructuredPointDomain" + std::string(DimAndTypeToStr<NDim, T>::value);
  py::class_<UnstructDom, DDomBase, UnstructDomPtr>(m,
                                    unstruct_name.c_str(),
                                    R"doc(Domain of definition for function
 defined only on a subset of points)doc")
          .def(py::init([]() -> UnstructDomPtr {
            return UnstructDom::Create();
          }),
               R"doc(Create object wrapped into shared pointer)doc")
          .def(py::init([](const PointList& points) -> UnstructDomPtr {
                 return UnstructDom::Create(points);
               }),
               "points"_a,
               R"doc(Create object wrapped into shared pointer.
> points: Points in the domain)doc");

  // TriangleMesh
  auto trimesh_name = "TriangleMesh" + std::string(DimAndTypeToStr<NDim, T>::value);
  py::class_<TriMesh, DDomBase, TriMeshPtr>(m,
                                            trimesh_name.c_str(),
                                            R"doc(Domain of definition for
 function defined on a mesh. Domain also include the mesh topology.)doc")
          .def(py::init([]() -> TriMeshPtr {
            return TriMesh::Create();
          }),
               R"doc(Create object wrapped into shared pointer)doc")
          .def(py::init([](const PointList& points,
                           const TriList& topology) -> TriMeshPtr {
                 return TriMesh::Create(points, topology);
               }),
               "points"_a,
               "topology"_a,
               R"doc(Create object wrapped into shared pointer.
> points: Point of the mesh (i.e. vertex)
> topology: Mesh topology (i.e. triangulation))doc")
          .def_property_readonly("topology",
                                 &TriMesh::get_triangles,
                                 R"doc(Access domain topology)doc");
}

template<int NDim, typename T>
void ExposeKLBasisGeneric(py::module& m) {
  using DKLBasis = DiscreteKLBasis<NDim, NDim, T>;
  using DDomPtr = typename DKLBasis::DDomPtr;

  using load_fn = int(DKLBasis::*)(const std::string&);
  using save_fn = int(DKLBasis::*)(const std::string&)const;

  // Discrete KL Basis
  auto cls_name = "DiscreteKLBasis" + std::string(DimAndTypeToStr<NDim, T>::value);
  py::class_<DKLBasis> d_kl_basis(m,
                                  cls_name.c_str(),
                                  R"doc(Container for DiscreteEigenPair function)doc");
  d_kl_basis.def(py::init<>(),
                 R"doc(Create an empty KL Basis)doc")

          .def(py::init<const DDomPtr&, const cv::Mat&, const cv::Mat&>(),
                  "domain"_a,
                  "eig_func"_a,
                  "eig_vals"_a,
                  R"doc(Constructor, build `DiscreteKLBasis` from a given domain
 and eigen functions/values pairs.
> domain: Domain of definition of the basis
> eig_func: Eigen function stored in each column
> eig_vals: Eigen values stored on each row)doc")
          .def("load",
               (load_fn)&DKLBasis::Load,
               "filename"_a,
               R"doc(Load from a file
> filename: Path to the file location)doc")
          .def("save",
               (save_fn)&DKLBasis::Save,
               "filename"_a,
               R"doc(Save the object into a file
> filename: File where to dump the object)doc")
          .def("sample",
               [](const DKLBasis& basis) {
                 cv::Mat sample;
                 basis.Sample(&sample);
                 return sample;
               },
               R"doc(Generate random sample from the underlying basis)doc")
               .def("instance",
                    [](const DKLBasis& basis, const cv::Mat& coefficents) {
                      cv::Mat instance;
                      basis.Instance(coefficents, &instance);
                      return instance;
                    },
                    "coefficients"_a,
                    R"doc(Generate an instance of the model from a given set of coefficients
> coefficients: Basis coefficients, considered normally distributed)doc")
          .def("truncate",
               &DKLBasis::Truncate,
               "n"_a,
               R"doc(Reduce basis dimensions by keeping `n` components.
> n: Number of basis to keep)doc")
          .def_property_readonly("eigenvalues",
                                 &DKLBasis::eigen_value_matrix,
                                 R"doc(Access eigen value matrix (i.e. stored as column vector))doc")
          .def_property_readonly("eigenvectors",
                                 &DKLBasis::eigen_function_matrix,
                                 R"doc(Access eigen function matrix (i.e. stored as column))doc");
}


void ExposeDiscreteDomain(py::module& m) {
  ExposeDiscreteDomain<3, float>(m);
  ExposeDiscreteDomain<3, double>(m);
}


void ExposeKLBasis(py::module& m) {
  ExposeKLBasisGeneric<3, float>(m);
  ExposeKLBasisGeneric<3, double>(m);
}


/*
 * @name    AddKernel
 * @fn  void AddKernel(pybind11::module& m)
 * @brief Expose some classes in kernel to python
 * @param[in,out] m Python module where to add the classes
 */
void AddKernel(pybind11::module& m) {
  ExposeDiscreteDomain(m);
  ExposeKLBasis(m);
}


}  // namespace Python
}  // namespace LTS5
