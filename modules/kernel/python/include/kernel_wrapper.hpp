/**
 *  @file   kernel_wrapper.hpp
 *  @brief  Wrapper for python interface
 *  @ingroup    python
 *
 *  @author Christophe Ecabert
 *  @date   14/11/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_KERNEL_WRAPPER__
#define __LTS5_KERNEL_KERNEL_WRAPPER__

#include <string>
#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"

#include "pybind11/pybind11.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    AddKernel
 * @fn  void AddKernel(pybind11::module& m)
 * @brief Expose some classes in kernel to python
 * @param[in,out] m Python module where to add the classes
 */
void AddKernel(pybind11::module& m);

}  // namepsace Python
}  // namepsace LTS5
#endif //__LTS5_KERNEL_KERNEL_WRAPPER__
