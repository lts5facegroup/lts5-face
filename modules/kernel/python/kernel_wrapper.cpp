/**
 *  @file   kernel_wrapper.cpp
 *  @brief  Wrapper for python interface
 *  @ingroup    python
 *
 *  @author Christophe Ecabert
 *  @date   14/11/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <memory>
#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"

#include "pybind11/pybind11.h"

#include "lts5/python/ocv_converter.hpp"
#include "kernel_wrapper.hpp"

namespace py = pybind11;

PYBIND11_MODULE(pykernel, m) {
  // Init numpy
  LTS5::Python::InitNumpyArray();
  // Doc
  m.doc() = R"doc(LTS5 Kernel modules)doc";
  // Expose Kernel module
  LTS5::Python::AddKernel(m);
}
