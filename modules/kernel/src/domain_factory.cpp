/**
 *  @file   domain_factory.hpp
 *  @brief  Utility function to create domain by name/type
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   03/02/21
 *  Copyright (c) 2021 Christophe Ecabert. All rights reserved.
 */

#include "lts5/kernel/domain_factory.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @name  DiscreteDomainProxy
 * @fn    DiscreteDomainProxy()
 * @brief Constructor
 */
template<int NDim, typename T>
DiscreteDomainProxy<NDim, T>::DiscreteDomainProxy() {
  DiscreteDomainFactory<NDim, T>::Get().Register(this);
}



template<int NDim, typename T>
DiscreteDomainFactory<NDim, T>& DiscreteDomainFactory<NDim, T>::Get() {
  static DiscreteDomainFactory<NDim, T> factory;
  return factory;
}

/**
 * @name  Create
 * @fn    DDomPtr Create(const std::string& name) const
 * @brief Create a domain by name
 * @param[in] name    Name of the domain
 * @return    Newly created domain wrapped in shared pointer
 */
template<int NDim, typename T>
typename DiscreteDomainFactory<NDim, T>::DDomPtr
DiscreteDomainFactory<NDim, T>::Create(const std::string& name) const {
  for (const auto& p : proxies_) {
    if (p->Name() == name) {
      return p->Create();
    }
  }
  return DDomPtr();  // Default empty pointer
}

/*
 * @name  Create
 * @fn    DDomPtr Create(const HeaderObjectType& type) const
 * @brief Create a domain by type
 * @param[in] name    Name of the domain
 * @return    Newly created domain wrapped in shared pointer
 */
template<int NDim, typename T>
typename DiscreteDomainFactory<NDim, T>::DDomPtr
DiscreteDomainFactory<NDim, T>::Create(const HeaderObjectType& type) const {
  for (const auto& p : proxies_) {
    if (p->Type() == type) {
      return p->Create();
    }
  }
  return DDomPtr();  // Default empty pointer
}

/*
 * @name  Register
 * @fn    void Register(const std::string& name, const HeaderObjectType& type,
                          RegisterFcn fcn)
 * @brief Register a new proxy for a domain
 * @param[in] name    Name of the domain
 * @param[in] type    Type of the domain
 * @param[in] fcn     Lambda function creating an instance of the specific
 *    domain
 */
template<int NDim, typename T>
void DiscreteDomainFactory<NDim, T>::
Register(const DiscreteDomainProxy<NDim, T>* proxy) {
  proxies_.push_back(proxy);
}

#define FWD_DECLARE_BASE(cls, ndim, type) \
template class cls<ndim, type>

#define FWD_DECLARE_TYPES(cls, type)  \
  FWD_DECLARE_BASE(cls, 1, type);          \
  FWD_DECLARE_BASE(cls, 2, type);          \
  FWD_DECLARE_BASE(cls, 3, type);          \
  FWD_DECLARE_BASE(cls, 4, type);

/** Discrete Domain proxy */
FWD_DECLARE_TYPES(DiscreteDomainProxy, float)
FWD_DECLARE_TYPES(DiscreteDomainProxy, double)
/** Discrete Domain factory */
FWD_DECLARE_TYPES(DiscreteDomainFactory, float)
FWD_DECLARE_TYPES(DiscreteDomainFactory, double)


#undef FWD_DECLARE_BASE
#undef FWD_DECLARE_TYPES
}  // namespace LTS5
