/**
 *  @file   domain.cpp
 *  @brief  Represent region where function is defined
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   11/30/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <fstream>

#include "lts5/utils/file_io.hpp"
#include "lts5/kernel/domain.hpp"
#include "lts5/kernel/domain_factory.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark DomainComposer

/**
 * Constructor
 * @param[in] dom_a   First domain to combine
 * @param[in] dom_b   Second domain to combine
 * @param[in] comp    Method to compare the two domain
 */
template<int NDim, typename T>
DomainComposer<NDim, T>::DomainComposer(const DomType& dom_a,
                                        const DomType& dom_b,
                                        DomComp comp) : dom_a_(dom_a),
                                                        dom_b_(dom_b),
                                                        comp_(std::move(comp)) {
}

template<int NDim, typename T>
typename DomainComposer<NDim, T>::Ptr
DomainComposer<NDim, T>::Intersection(const DomType& dom_a,
                                      const DomType& dom_b) {
  struct make_shared_enabler : public DomainComposer<NDim, T> {
    make_shared_enabler(const DomType& dom_a,
                        const DomType& dom_b,
                        const DomComp& comp) :
            DomainComposer(dom_a, dom_b, comp) {}
  };
  return std::make_shared<make_shared_enabler>(dom_a,
                                               dom_b,
                                               std::logical_and<bool>());
}

template<int NDim, typename T>
typename DomainComposer<NDim, T>::Ptr
DomainComposer<NDim, T>::Union(const DomType& dom_a,
                               const DomType& dom_b) {
  struct make_shared_enabler : public DomainComposer<NDim, T> {
    make_shared_enabler(const DomType& dom_a,
                        const DomType& dom_b,
                        const DomComp& comp) :
            DomainComposer(dom_a, dom_b, comp) {}
  };
  return std::make_shared<make_shared_enabler>(dom_a,
                                               dom_b,
                                               std::logical_or<bool>());
}

#pragma mark -
#pragma mark EuclideanSpace

/**
   * @name  Create
   * @fn    static Ptr Create()
   * @brief Create object wrapped in a shared_ptr.
   * @return    Shared pointer
   */
template<int NDim, typename T>
typename EuclideanSpace<NDim, T>::Ptr EuclideanSpace<NDim, T>::Create() {
  // See: https://stackoverflow.com/a/25069711
  struct make_shared_enabler : public EuclideanSpace<NDim, T> {};
  return std::make_shared<make_shared_enabler>();
}

#pragma mark -
#pragma mark BoxDomain

template<int NDim, typename T>
BoxDomain<NDim, T>::BoxDomain(const PointType<NDim, T>& origin,
                              const PointType<NDim, T>& opposite) :
        org_(origin),
        opposite_(opposite) {
}

template<int NDim, typename T>
typename BoxDomain<NDim, T>::Ptr
BoxDomain<NDim, T>::Create(const PointType<NDim, T>& origin,
                           const PointType<NDim, T>& opposite) {
  // See: https://stackoverflow.com/a/25069711
  struct make_shared_enabler : public BoxDomain<NDim, T> {
    make_shared_enabler(const PointType<NDim, T>& origin,
                        const PointType<NDim, T>& opposite) :
            BoxDomain<NDim, T>::BoxDomain(origin, opposite) {};
  };
  return std::make_shared<make_shared_enabler>(origin, opposite);
}

/**
 * @name  FromPoints
 * @fn    static Ptr FromPoints(const std::vector<PointType<NDim, T>>& points)
 * @brief Create a BoxDomain that encapsulate a given set of points.
 * @param[in] points  List of points.
 * @return    BoxDomain
 */

template<int NDim, typename T>
typename BoxDomain<NDim, T>::Ptr
BoxDomain<NDim, T>::FromPoints(const std::vector<PointType<NDim, T>>& points) {
  // Search for min/max
  PointType<NDim, T> min_p = points.front();
  PointType<NDim, T> max_p = points.back();
  // Iterate over all points
  for (const auto& pts : points) {
    // Compare each components
    for (int i = 0; i < NDim; ++i) {
      // minimum points
      if (pts[i] < min_p[i]) {
        min_p[i] = pts[i];
      }
      // maximum points
      if (pts[i] > max_p[i]) {
        max_p[i] = pts[i];
      }
    }
  }
  // Build domain
  return BoxDomain<NDim, T>::Create(min_p, max_p);
}

template<int NDim, typename T>
bool BoxDomain<NDim, T>::IsDefinedAt(const PointType<NDim, T>& pt) const {
  bool defined = true;
  for (int i = 0; i < NDim; ++i) {
    defined &= this->IsInside(pt, i);
  }
  return defined;
}

template<int NDim, typename T>
T BoxDomain<NDim, T>::Volume() const {
  T v(1.0);
  auto extent = this->Extent();
  for (int i = 0; i < NDim; ++i) {
    v *= extent[i];
  }
  return v;
}

template<int NDim, typename T>
bool BoxDomain<NDim, T>::IsInside(const PointType<NDim, T>& pt, int idx) const {
  return pt[idx] >= org_[idx] && pt[idx] <= opposite_[idx];
}

#pragma mark -
#pragma mark DiscreteDomain

template<int NDim, typename T>
int DiscreteDomain<NDim, T>::Load(const std::string &filename) {
  int err = -1;
  std::ifstream stream(filename.c_str(),
                       std::ios_base::binary | std::ios_base::in);
  if (stream.is_open()) {
    err = this->Load(stream);
  }
  return err;
}

template<int NDim, typename T>
int DiscreteDomain<NDim, T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Load points
    int n_pts = 0;
    stream.read(reinterpret_cast<char*>(&n_pts), sizeof(n_pts));
    this->points_.resize(n_pts);
    stream.read(reinterpret_cast<char*>(this->points_.data()),
                n_pts * sizeof(PtType));
    // Init domain
    this->bbox_ = BoxDomain<NDim,T>::FromPoints(this->points_);
    // Sanity check
    err = stream.good() ? 0 : -1;
  }
  return err;
}

template<int NDim, typename T>
int DiscreteDomain<NDim, T>::Save(const std::string &filename) const {
  int err = -1;
  std::ofstream stream(filename.c_str(),
                       std::ios_base::binary | std::ios_base::out);
  if (stream.is_open()) {
    err = this->Save(stream);
  }
  return err;
}

template<int NDim, typename T>
int DiscreteDomain<NDim, T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Dump points
    int n_pts = this->points_.size();
    stream.write(reinterpret_cast<const char*>(&n_pts), sizeof(n_pts));
    stream.write(reinterpret_cast<const char*>(this->points_.data()),
                 n_pts * sizeof(PtType));
    err = stream.good() ? 0 : -1;
  }
  return err;
}

template<int NDim, typename T>
int DiscreteDomain<NDim, T>::ObjectSize() const {
  int sz = sizeof(int); // #Pts
  sz += this->points_.size() * sizeof(PtType);
  return sz;
}

#pragma mark -
#pragma mark UnstructuredPointDomain

template<int NDim, typename T>
typename UnstructuredPointDomain<NDim, T>::Ptr
UnstructuredPointDomain<NDim, T>::Create() {
  return std::make_shared<UnstructuredPointDomain<NDim, T>>();
}

template<int NDim, typename T>
typename UnstructuredPointDomain<NDim, T>::Ptr
UnstructuredPointDomain<NDim, T>::Create(const PointList& points) {
  return std::make_shared<UnstructuredPointDomain<NDim, T>>(points);
}

template<int NDim, typename T>
UnstructuredPointDomain<NDim, T>::
UnstructuredPointDomain() : DiscreteDomain<NDim, T>() {
}

/*
 * @name  UnstructuredPointDomain
 * @fn    explicit UnstructuredPointDomain(const PointList& points)
 * @brief Constructor
 * @param[in] points  Points in the domain
 */
template<int NDim, typename T>
UnstructuredPointDomain<NDim, T>::
UnstructuredPointDomain(const PointList& points) :
        DiscreteDomain<NDim, T>(points) {
  this->Init();
}

template<int NDim, typename T>
int UnstructuredPointDomain<NDim, T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Search for header
    err = ScanStreamForObject(stream,
                              HeaderObjectType::kUnstructuredPointDomain);
    if (!err) {
      // Load base
      err = DiscreteDomain<NDim, T>::Load(stream);
      // Init
      this->Init();
      // Sanity check
      err |= stream.good() ? 0 : -1;
    }
  }
  return err;
}

template<int NDim, typename T>
int UnstructuredPointDomain<NDim, T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Dump header
    HeaderObjectType type = HeaderObjectType::kUnstructuredPointDomain;
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    // Object size
    int sz = this->ObjectSize();
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Dump points:
    //  Header
    //  #Pts
    //  List of points
    err = DiscreteDomain<NDim, T>::Save(stream);
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

template<int NDim, typename T>
int UnstructuredPointDomain<NDim, T>::ObjectSize() const {
  int sz = DiscreteDomain<NDim, T>::ObjectSize(); // Size of point
  sz += sizeof(int);    // Header of base class
  return sz;
}

template<int NDim, typename T>
void UnstructuredPointDomain<NDim, T>::Init() {
  this->point_map_.clear();
  for (size_t i = 0; i < this->points_.size(); ++i) {
    this->point_map_.emplace(this->points_[i], i);
  }
  // Build kdtree
  this->kdtree_.Build(this->points_);
}

/*
 * @name  IsDefinedAt
 * @fn    bool IsDefinedAt(const PointType<NDim, T>& x) const
 * @brief Check if function is defined at `x`.
 * @return    `true` if function is defined at `x` otherwise `false`.
 */
template<int NDim, typename T>
bool UnstructuredPointDomain<NDim, T>::
IsDefinedAt(const PointType<NDim, T>& x) const {
  // Search if point is part of map
  auto it = this->point_map_.find(x);
  return it != this->point_map_.end();
}

/*
 * @name  FindClosestPoint
 * @fn    PtType FindClosestPoint(const PtType& x) const
 * @brief Look for closest point to `x` where function is defined.
 * @param[in] x   Query points
 * @return    Closest point
 */
template<int NDim, typename T>
typename UnstructuredPointDomain<NDim, T>::PtType
UnstructuredPointDomain<NDim, T>::
FindClosestPoint(const PtType& x) const {
  // Is the `query` point part of the domain ?
  int idx = this->Index(x);
  if (idx == -1) {
    // No, search the closest one using kdree
    return kdtree_.FindNearest(x, 1)[0];
  } else {
    return this->points_[idx];
  }
}

/*
 * @name  FindNClosestPoint
 * @fn    PointList FindNClosestPoint(const PtType& x, size_t n) const
 * @brief Look for N closest point to `x` where function is defined.
 * @param[in] x   Query points
 * @param[in] n   Number of points to look for
 * @return    List closest point
 */
template<int NDim, typename T>
typename UnstructuredPointDomain<NDim, T>::PointList
UnstructuredPointDomain<NDim, T>::
FindNClosestPoint(const PtType& x, size_t n) const {
  return kdtree_.FindNearest(x, n);
}

/*
 * @name  Index
 * @fn    int Index(const PtType& x) const
 * @brief Look for index of a given point `x`, if not knwon, returns -1
 * @param[in] x   Query point
 * @return Index of the query point or -1 if not part of domain
 */
template<int NDim, typename T>
int UnstructuredPointDomain<NDim, T>::Index(const PtType& x) const {
  auto it = point_map_.find(x);
  return it == point_map_.end() ? -1 : it->second;
}

#pragma mark -
#pragma mark TriangleMesh

/*
 * @name  Create
 * @fn    static Ptr Create()
 * @brief Create object wrapped into shared pointer.
 * @return    Object wrapped in shared pointer.
 */
template<typename T>
typename TriangleMesh<T>::Ptr
TriangleMesh<T>::Create() {
  return std::make_shared<TriangleMesh<T>>();
}

/*
 * @name  Create
 * @fn    static Ptr Create()
 * @brief Create object wrapped into shared pointer.
 * @param[in] points      Point of the mesh (i.e. vertex)
 * @param[in] topology    Mesh topology (i.e. triangulation)
 * @return    Object wrapped in shared pointer.
 */
template<typename T>
typename TriangleMesh<T>::Ptr
TriangleMesh<T>::Create(const PointList& points,
                        const TriList& topology) {
  return std::make_shared<TriangleMesh<T>>(points, topology);
}

/*
 * @name  Create
 * @fn    static Ptr Create()
 * @brief Create object wrapped into shared pointer.
 * @param[in] mesh      Mesh instance
 * @return    Object wrapped in shared pointer.
 */
template<typename T>
typename TriangleMesh<T>::Ptr
TriangleMesh<T>::Create(const MeshType& mesh) {
  return std::make_shared<TriangleMesh<T>>(mesh);
}

/*
 * @name  TriangleMesh
 * @fn    TriangleMesh
 * @brief Constructor
 */
template<typename T>
TriangleMesh<T>::TriangleMesh() : UnstructuredPointDomain<3, T>(),
                                  tri_() {
}

/*
 * @name  TriangleMesh
 * @fn    TriangleMesh(const PointList& points,
 *                     const std::vector<TriType>& topology)
 * @brief Constructor
 * @param[in] points      Point of the mesh (i.e. vertex)
 * @param[in] topology    Mesh topology (i.e. triangulation)
 */
template<typename T>
TriangleMesh<T>::TriangleMesh(const PointList& points,
                              const std::vector<TriType>& topology) :
        UnstructuredPointDomain<3, T>(points),
        tri_(topology) {
  // Build bounding sphere tree
  this->bs_tree_.Build(this->points_, this->tri_);
}

/*
 * @name  TriangleMesh
 * @fn    explicit TriangleMesh(const MeshType& mesh)
 * @brief Constructor
 * @param[in] mesh    Mesh instance
 */
template<typename T>
TriangleMesh<T>::TriangleMesh(const MeshType& mesh) :
        TriangleMesh<T>(mesh.get_vertex(), mesh.get_triangle()) {
}

template<typename T>
int TriangleMesh<T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Search for header
    err = ScanStreamForObject(stream, HeaderObjectType::kTriangleMesh);
    if (!err) {
      err = UnstructuredPointDomain<3, T>::Load(stream);
      // Read triangles
      int n_tri = 0;
      stream.read(reinterpret_cast<char*>(&n_tri), sizeof(n_tri));
      tri_.resize(n_tri);
      stream.read(reinterpret_cast<char*>(tri_.data()),
                  n_tri * sizeof(TriType));
      // Build bounding sphere tree
      this->bs_tree_.Build(this->points_, this->tri_);
      // Sanity check
      err |= stream.good() ? 0 : -1;
    }
  }
  return err;
}

template<typename T>
int TriangleMesh<T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Dump header
    HeaderObjectType type = HeaderObjectType::kTriangleMesh;
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    // Object size
    int sz = this->ObjectSize();
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Dump points
    err = UnstructuredPointDomain<3, T>::Save(stream);
    // Dump triangles
    int n_tri = tri_.size();
    stream.write(reinterpret_cast<const char*>(&n_tri), sizeof(n_tri));
    stream.write(reinterpret_cast<const char*>(tri_.data()),
                 n_tri * sizeof(TriType));
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

template<typename T>
int TriangleMesh<T>::ObjectSize() const {
  int sz = UnstructuredPointDomain<3, T>::ObjectSize();
  sz += sizeof(int);   // Header of UnstructDom
  sz += sizeof(int);    // Number of triangle
  sz += tri_.size() * sizeof(TriType);  // Size of all triangles
  return sz;
}

/*
 * @name  FindClosestPoint
 * @fn    PtType FindClosestPoint(const PtType& x) const
 * @brief Look for closest point to `x` where function is defined.
 * @param[in] x   Query points
 * @return    Closest point
 */
template<typename T>
typename TriangleMesh<T>::PtType
TriangleMesh<T>::FindClosestPoint(const PtType& x) const {
  // Check if `x` is part of domain ... if so just skip the search!
  int idx = this->Index(x);
  if (idx == -1) {
    // No, search the closest one using bs-tree
    auto closest_pts = bs_tree_.ClosestPointOnSurface(x);
    return closest_pts.pts;
  } else {
    return this->points_[idx];
  }
}

#pragma mark -
#pragma mark Explicit instantiation

#define FWD_DECLARE_BASE(cls, ndim, type) \
template class cls<ndim, type>

#define FWD_DECLARE_TYPES(cls, type)  \
  FWD_DECLARE_BASE(cls, 1, type);          \
  FWD_DECLARE_BASE(cls, 2, type);          \
  FWD_DECLARE_BASE(cls, 3, type);          \
  FWD_DECLARE_BASE(cls, 4, type);

/** DomainComposer */
FWD_DECLARE_TYPES(DomainComposer, float)
FWD_DECLARE_TYPES(DomainComposer, double)
/** EuclideanSpace */
FWD_DECLARE_TYPES(EuclideanSpace, float)
FWD_DECLARE_TYPES(EuclideanSpace, double)
/** BoxDomain */
FWD_DECLARE_TYPES(BoxDomain, float)
FWD_DECLARE_TYPES(BoxDomain, double)
/** DiscreteDomain - Base */
FWD_DECLARE_TYPES(DiscreteDomain, float)
FWD_DECLARE_TYPES(DiscreteDomain, double)
/** UnstructuredPointDomain */
FWD_DECLARE_TYPES(UnstructuredPointDomain, float)
FWD_DECLARE_TYPES(UnstructuredPointDomain, double)
/** TriangleMesh */
template class TriangleMesh<float>;
template class TriangleMesh<double>;

REGISTER_DISCRETE_DOMAIN(UnstructuredPointDomain,
                         "UnstructuredPointDomain",
                         HeaderObjectType::kUnstructuredPointDomain)

// Special variant for triangle mesh since it's only available for NDim==3
DEFINE_SINGLE_DIMENSION_DISCRETE_DOMAIN_PROXY(TriangleMesh,
                                              "TriangleMesh",
                                              HeaderObjectType::kTriangleMesh)
INSTANTIATE_DISCRETE_DOMAIN_PROXY(TriangleMesh, 3, float, 3f)
INSTANTIATE_DISCRETE_DOMAIN_PROXY(TriangleMesh, 3, double, 3d)


#undef FWD_DECLARE_BASE
#undef FWD_DECLARE_TYPES


}  // namespace LTS5

