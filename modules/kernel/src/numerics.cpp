/**
 *  @file   numerics.cpp
 *  @brief  Numerical utility functions
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   10/02/21
 *  Copyright (c) 2021 Christophe Ecabert. All rights reserved.
 */

#include <unordered_map>
#include <string>

#include "lts5/kernel/numerics.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @struct  BSplineEvaluator
 * @brief   Compute the spline's value based on the order
 * @author  Christophe Ecabert
 * @date    10/02/2021
 * @ingroup kernel
 * @tparam Order    Spline's order
 * @tparam T        Data type
 */
template<int Order, typename T>
struct BSplineEvaluator;

template<typename T>
struct BSplineEvaluator<0, T> {
  T operator()(const T& x) const {
    if (T(-0.5) < x && x < T(0.5)) {
      return T(1.0);
    } else if ((std::abs(x) - T(0.5)) < std::numeric_limits<T>::epsilon()) {
      return T(0.5);
    } else {
      return T(0.0);
    }
  }
};
template<typename T>
struct BSplineEvaluator<1, T> {
  T operator()(const T &x) const {
    if (T(-1.0) <= x && x <= T(0.0)) {
      return T(1.0) + x;
    } else if (T(0.0) < x && x <= T(1.0)) {
      return T(1.0) - x;
    } else {
      return T(0.0);
    }
  }
};
template<typename T>
struct BSplineEvaluator<2, T> {
  T operator()(const T &x) const {
    if (T(-1.5) <= x && x < T(-0.5)) {
      return T(0.5) * (x + T(1.5)) * (x + T(1.5));
    } else if (T(-0.5) <= x && x < T(0.5)) {
      return -(x + T(0.5)) * (x + T(0.5)) + (x - T(0.5)) + T(1.5);
    } else if (x >= T(0.5) && x < T(1.5)) {
      return T(0.5) * (T(1.0) - (x - T(0.5))) * (T(1.0) - (x - T(0.5)));
    } else {
      return T(0.0);
    }
  }
};
template<typename T>
struct BSplineEvaluator<3, T> {
  T operator()(const T &x) const {
    T absX = std::abs(x);

    if (absX >= T(0.0) && absX < T(1.0)) {
      T absXSq = absX * absX;
      T absXCu = absX * absXSq;
      return (T(2.0) / T(3.0)) - absXSq + (T(0.5) * absXCu);
    } else if (absX >= T(1.0) && absX < T(2.0)) {
      T twoMinAbsX = T(2.0) - absX;
      return twoMinAbsX * twoMinAbsX * twoMinAbsX / T(6.0);
    } else {
      return T(0.0);
    }
  }
};

template<typename T>
std::function<T(const T&)> GetSplineEvaluator(size_t order) {
  using FcnType = std::function<T(const T&)>;
  static std::unordered_map<size_t, FcnType> selector =
          {{0, BSplineEvaluator<0 ,T>()},
           {1, BSplineEvaluator<1 ,T>()},
           {2, BSplineEvaluator<2 ,T>()},
           {3, BSplineEvaluator<2 ,T>()}};
  return selector[order];
}

/*
 * @name  BSpline
 * @fn    explicit BSpline(size_t order)
 * @brief Constructor
 * @param[in] order Spline order
 */
template<typename T>
BSpline<T>::BSpline(size_t order) :
        evaluator_fcn_(GetSplineEvaluator<T>(order)) {
  if (order > 3) {
    throw std::runtime_error("BSpline of order: " +
                             std::to_string(order) +
                             " is not implemented!");
  }
}
/*
 * @name  operator()
 * @fn    T operator()(const T& x) const
 * @brief Evaluate spline function at a given `x` value.
 * @param[in] x   Where to evaluate function
 * @return    Value
 */
template<typename T>
T BSpline<T>::operator()(const T& x) const {
  return evaluator_fcn_(x);
}

#pragma mark -
#pragma mark Explicit instantiation

template class BSpline<float>;
template class BSpline<double>;

}  // namespace LTS5

