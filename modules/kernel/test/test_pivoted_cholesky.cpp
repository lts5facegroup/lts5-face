/**
 *  @file   test_pivoted_cholesk.cpp
 *  @brief  Test cholesky decomposition
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   21/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <string>
#include <limits>
#include "gtest/gtest.h"

#include "lts5/kernel/domain.hpp"
#include "lts5/kernel/kernel.hpp"
#include "lts5/kernel/matrix_kernel.hpp"
#include "lts5/kernel/sampler.hpp"
#include "lts5/kernel/pivoted_cholesky.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

using namespace LTS5;

template<typename T>
struct Error {};

template<>
struct Error<float> {
  static constexpr float tol_chol = 1e-5;
  static constexpr float tol_eig = 1e-4;
};
constexpr float Error<float>::tol_chol;
constexpr float Error<float>::tol_eig;
template<>
struct Error<double> {
  static constexpr double tol_chol = 1e-8;
  static constexpr double tol_eig = 1e-8;
};
constexpr double Error<double>::tol_chol;
constexpr double Error<double>::tol_eig;


template<int NDim, typename T>
struct Builder {
  using PointT = PointType<NDim, T>;
  static PointT DefinePt(const T& v) {
    PointT pt;
    for (int k = 0; k < NDim; ++k) {
      pt[k] = v;
    }
    return pt;
  }
};

template<int NDim, typename T>
struct TestParams {
  static constexpr int kDims() {return NDim;};
  using Type = T;
};

template<typename T>
class TestPivotedCholesky : public ::testing::Test {
 public:
  TestPivotedCholesky() = default;
  using DType = typename T::Type;
};

// List of type to test against
using MyTestTypes = ::testing::Types<TestParams<1, float>,
        TestParams<1, double>,
        TestParams<3, float>,
        TestParams<3, double>>;

// Instantiate test suite
TYPED_TEST_CASE(TestPivotedCholesky, MyTestTypes);

TYPED_TEST(TestPivotedCholesky, LowRankCovarianceApproximation) {
  using DType = typename TestFixture::DType;
  using BoxDomType = BoxDomain<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;
  using PtType = PointType<TypeParam::kDims(), DType>;
  using UniSampler = UniformSampler<TypeParam::kDims(), DType>;
  using GausKernel = GaussianKernel<TypeParam::kDims(), DType>;
  using DiagKernel = DiagonalKernel<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using LA = LinearAlgebra<DType>;

  // Sample point in Nd box.
  auto dom = BoxDomType::Create(PtBuilder::DefinePt(DType(0.0)),
                                PtBuilder::DefinePt(DType(1.0)));
  auto sampler = UniSampler(dom, 20);
  std::vector<PtType> pts = sampler.Sample();
  // Define matrix kernel
  auto k = DiagKernel::Create(GausKernel::Create(DType(1.0)));
  // Compute matrix kernel (i.e. cov for a set of points)
  cv::Mat Ktrue = k->Matrix(pts);
  // Decompose
  RelativeTolerance<DType> sc(DType(1e-15));
  auto chol = ApproxCholesky<DType,
                             TypeParam::kDims(),
                             TypeParam::kDims(),
                             DiagKernel>(k, pts, sc);
  { // Cholesky approximation
    auto L = chol.matrix();
    cv::Mat Kpred;
    LA::Gemm(L, LA::TransposeType::kNoTranspose, DType(1.0),
             L, LA::TransposeType::kTranspose, DType(0.0),
             &Kpred);
    // Compute error
    cv::Mat D = Kpred - Ktrue;
    DType err = cv::norm(D);
    EXPECT_LE(err, Error<DType>::tol_chol);
  }
  { // Eigen value/vector approximation
    cv::Mat eigenvector, eigenvalues;
    chol.EigenDecomposition(&eigenvector, &eigenvalues);
    // Regenerate cov matrix
    cv::Mat Kpred = eigenvector * cv::Mat::diag(eigenvalues) * eigenvector.t();
    // Compute error
    cv::Mat D = Kpred - Ktrue;
    DType err = cv::norm(D);
    EXPECT_LE(err, Error<DType>::tol_eig);
  }
}

int main(int argc, const char * argv[]) {
  // Init test
  ::testing::InitGoogleTest(&argc, const_cast<char **>(argv));
  // Run test
return RUN_ALL_TESTS();

}