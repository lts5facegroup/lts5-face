/**
 *  @file   gp_bfm_model_augementation.cpp
 *  @brief  Augment Basel Face model with custom made Gaussian Process. See
 *      "3D Morphable Face Model - Open Frameworks"
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   10/02/21
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <vector>
#include <thread>
#include <chrono>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/sys/file_system.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/kernel/domain.hpp"
#include "lts5/kernel/kernel.hpp"
#include "lts5/kernel/matrix_kernel.hpp"
#include "lts5/kernel/sampler.hpp"
#include "lts5/kernel/smooth_mask.hpp"
#include "lts5/kernel/kernel_approximation.hpp"

#include "lts5/utils/sys/cache.hpp"

using namespace LTS5;


template<typename T>
struct LevelWithScale {
  /**
   * @name  LevelWithScale
   * @fn    LevelWithScale(int lvl, T s)
   * @brief Constructor
   * @param[in] lvl Level
   * @param[in] s   Kernel's scale
   */
  LevelWithScale(int lvl, T s) : level(lvl), scale(s) {}

  /** Level */
  int level;
  /** Scaling */
  T scale;
};

template<typename T>
class SpatiallyVaryingMScaleKernel : public MatrixKernel<3, 3, T> {
 public:
  /** Discrete Domain pointer */
  using DDomPtr = std::shared_ptr<DiscreteDomain<3, T>>;
  /** Point type */
  using PtType = PointType<3, T>;
  /** POinter type */
  using Ptr = std::shared_ptr<SpatiallyVaryingMScaleKernel<T>>;

  /**
   * @name
   * @brief Create kernel wrapped into shared_ptr.
   * @param level_infos
   * @param face_mask_info
   * @param reference
   * @return Shared instance
   */
  static Ptr Create(const std::vector<LevelWithScale<T>>& level_infos,
                    const SurfaceMaskInfo& face_mask_info,
                    const DDomPtr& reference) {
    using K = SpatiallyVaryingMScaleKernel<T>;
    return std::make_shared<K>(level_infos, face_mask_info, reference);
  }

  /**
   * @name  SpatiallyVaryingMScaleKernel
   * @fn    SpatiallyVaryingMScaleKernel(const std::vector<LevelWithScale<T>>& level_infos,
                               const SurfaceMaskInfo& face_mask_info,
                               const DDomPtr& reference)
   * @brief Constructor
   * @param[in] level_infos
   * @param[in] face_mask_info
   * @param[in] reference
   */
  SpatiallyVaryingMScaleKernel(const std::vector<LevelWithScale<T>>& level_infos,
                               const SurfaceMaskInfo& face_mask_info,
                               const DDomPtr& reference);

  /**
   * @name  ~SpatiallyVaryingMScaleKernel
   * @fn    ~SpatiallyVaryingMScaleKernel() override = default
   * @brief Destructor
   */
  ~SpatiallyVaryingMScaleKernel() override = default;

 protected:

  /**
   * @name  k
   * @fn    MatType<ODim, T> k(const PointType<IDim, T>& x,
   *                           const PointType<IDim, T>& y) const override
   * @brief Compute the output scalar value for the kernel between two points
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Value
   */
  MatType<3, T> k(const PtType& x, const PtType& y) const override;

 private:
  using LevelInfo = LevelWithScale<T>;
  using MatKernelPtr = typename MatrixKernel<3, 3, T>::Ptr;

  /** Level information */
  std::vector<LevelInfo> lvl_info_;
  /** Face mask */
  std::vector<SurfaceMask<T>> face_masks_;
  /** BSpline kernel */
  MatKernelPtr spline_;
};

template<typename T>
SpatiallyVaryingMScaleKernel<T>::
SpatiallyVaryingMScaleKernel(const std::vector<LevelWithScale<T>>& level_infos,
                             const SurfaceMaskInfo& face_mask_info,
                             const DDomPtr& reference) :
        MatrixKernel<3, 3, T>(EuclideanSpace<3, T>::Create()),
        lvl_info_(level_infos),
        face_masks_(),
        spline_() {
          using DiagK = DiagonalKernel<3, 3, T>;
  // Build face mask for each level
  for (const auto& lvl : level_infos) {
    face_masks_.emplace_back(face_mask_info.level_mask(),
                             face_mask_info.semantic_mask());
    face_masks_.back().BuildSmoothedRegion(reference,
                                           lvl.level,
                                           T(40.0));
  }
  // Build spline kernel
  spline_ = DiagK::Create(BSplineKernel<3, T>::Create(3, 0));
}

template<typename T>
MatType<3, T>
SpatiallyVaryingMScaleKernel<T>::k(const PtType& x,
                                   const PtType& y) const {
  MatType<3, T> sum;
  for (size_t k = 0; k < lvl_info_.size(); ++k) {
    const auto& lvl = lvl_info_[k];
    const auto& m = face_masks_[k];
    T s = std::pow(T(2.0), lvl.level);
    // Region based weight
    T wx = m(x);
    T wy = m(y);
    // Evaluate spline kernel
    auto xs = x * s;
    auto ys = y * s;
    sum += spline_->operator()(xs, ys) * (lvl.scale * wx * wy);
  }
  return sum;
}

template<typename T>
class FaceKernel : public MatrixKernel<3, 3, T> {
 private:
  using Ksvms = SpatiallyVaryingMScaleKernel<T>;
  using MatKComp = MatrixKernelComposer<3, 3, T>;
  using SymmK = SymmetricKernel<3, 3, T>;
 public:
  /** Discrete Domain pointer */
  using DDomPtr = std::shared_ptr<DiscreteDomain<3, T>>;
  /** Point type */
  using PtType = PointType<3, T>;
  /** Pointer type */
  using Ptr = std::shared_ptr<FaceKernel<T>>;


  /**
   * @name  Create
   * @fn    static Ptr Create(const SurfaceMaskInfo& face_mask_info,
                    const DDomPtr& reference,
                    const size_t& size)
   * @brief Build shared instance kernel
   * @param[in] face_mask_info  Face mask
   * @param[in] reference       Reference surface
   * @param[in] size            Cache size
   * @return    Shared pointer
   */
  static Ptr Create(const SurfaceMaskInfo& face_mask_info,
                    const DDomPtr& reference,
                    const size_t& size) {
    return std::make_shared<FaceKernel<T>>(face_mask_info, reference, size);
  }

  /**
   * @name  FaceKernel
   * @fn    FaceKernel(const SurfaceMaskInfo& face_mask_info,
   *                    const DDomPtr& reference)
   * @brief Constructor
   * @param[in] face_mask_info  Face mask info
   * @param[in] reference       Reference shape
   * @param[in] size            Cache size
   */
  FaceKernel(const SurfaceMaskInfo& face_mask_info,
             const DDomPtr& reference,
             const size_t& size) :
          MatrixKernel<3, 3, T>(EuclideanSpace<3, T>::Create(), size) {
    // Base kernel
    auto spK = Ksvms::Create({{-6, T(128.0)},
                              {-5, T(64.0)},
                              {-4, T(32.0)},
                              {-3, T(10.0)},
                              {-2, T(4.0)}},
                             face_mask_info,
                             reference);
    auto sym_spK = SymmK::Create(spK, 0);
    // Combine base kernel
    // K = sym_spk * 0.7 + spk * 0.3
    k_ =  MatKComp::Add(MatKComp::Scale(spK, T(0.3)),
                        MatKComp::Scale(sym_spK, T(0.7)));
  }

  /**
   * @name  ~FaceKernel
   * @fn    ~FaceKernel() override = default
   * @brief Destructor
   */
  ~FaceKernel() override = default;

 protected:

  /**
  * @name  k
  * @fn    MatType<ODim, T> k(const PointType<IDim, T>& x,
  *                           const PointType<IDim, T>& y) const override
  * @brief Compute the output scalar value for the kernel between two points
  * @param[in] x   First point
  * @param[in] y   Second point
  * @return    Value
  */
  MatType<3, T> k(const PtType& x, const PtType& y) const override {
    return k_->operator()(x, y);
  }

 private:
  using MatKernelPtr = typename MatrixKernel<3, 3, T>::Ptr;

  /** Multiscale spatially varying kernel */
  MatKernelPtr k_;
};

template<typename T>
T ApproximateTotalVariance(const std::shared_ptr<MatrixKernel<3, 3, T>>& kernel,
                           const Mesh<T, Vector3<T>>& mesh,
                           size_t n_pts = 50000) {
  RandomMeshSampler<T> sampler(mesh, n_pts);
  auto points = sampler.Sample();

  auto n_thread = std::max(std::thread::hardware_concurrency(),
                           std::uint32_t(1));
  size_t chunk_size = (n_pts + n_thread - 1) / n_thread;  // divUp
  std::vector<T> covariance(n_thread, T(0.0));
  Parallel::For(n_thread,
                [&](const size_t& k) {
    size_t i_from = k * chunk_size;
    size_t i_to = std::min(n_pts, (k + 1) * chunk_size);
    for (size_t i = i_from; i < i_to; ++i) {
      const auto& p = points[i];
      auto cov = kernel->operator()(p, p);
      covariance[k] += cov.Trace();
    }
  });
  T var = std::accumulate(covariance.begin(), covariance.end(), T(0.0));
  return var / static_cast<T>(n_pts);
}

template<typename T>
void Process(CmdLineParser& parser) {
  using TriDom = TriangleMesh<T>;
  using UnstructDom = UnstructuredPointDomain<3, T>;
  using Mesh_t = Mesh<T, Vector3<T>>;
  using DKLBasis = DiscreteKLBasis<3, 3, T>;
  using NNInterp = NearestNeighborInterpolator<3, 3, T>;
  using CovKernel = KLBasisCovKernel<3, 3, T>;
  using MatKComp = MatrixKernelComposer<3, 3, T>;
  using DiagK = DiagonalKernel<3, 3, T>;
  using GaussK = GaussianKernel<3, T>;
  using SymmK = SymmetricKernel<3, 3, T>;

  // Retrieve parameters
  std::string reference_path, bfm_neutral_path, face_mask_path, output_folder;
  std::string augment_type;
  parser.HasArgument("--mesh", &reference_path);
  parser.HasArgument("--bfm_neutral_path", &bfm_neutral_path);
  parser.HasArgument("--face_mask_path", &face_mask_path);
  parser.HasArgument("--augmentation_type", &augment_type);
  parser.HasArgument("--output", &output_folder);

  // Create destination folder
  auto fs = GetDefaultFileSystem();
  if (!fs->FileExist(output_folder).Good()) {
    fs->CreateDirRecursively(output_folder);
  }

  // Load reference
  Mesh_t mesh;
  int err = mesh.Load(reference_path);
  if (err) {
    LTS5_LOG_ERROR("Unable to load reference mesh: " << reference_path);
    return;
  }
  auto ref = TriDom::Create(mesh.get_vertex(),
                            mesh.get_triangle());

  // Load BFM model into DiscreteKLBasis
  DKLBasis bfm_basis;
  err = bfm_basis.Load(bfm_neutral_path);
  if (err) {
    LTS5_LOG_ERROR("Unable to load BFM basis: " << bfm_neutral_path);
    return;
  }

  // Make them continuous + Create Kpdm kernel
  NNInterp interpolator;
  auto continuous_bfm_basis = bfm_basis.Interpolate(interpolator);
  auto Kbfm = CovKernel::Create(continuous_bfm_basis, 100000);

  // Create FaceKernel
  std::shared_ptr<MatrixKernel<3, 3, T>> Kface;
  std::string model_name;
  if (augment_type == "gerig") {
    SurfaceMaskInfo face_info;
    err = face_info.Load(face_mask_path);
    if (err) {
      LTS5_LOG_ERROR("Unable to load face mask from: " << face_mask_path);
      return;
    }
    Kface = FaceKernel<T>::Create(face_info, ref, 100000);
    model_name = "augmented_face_gerig.bin";
  } else if (augment_type == "luthi") {
    // Model augmentation -> Lüthi et al, 2017
    auto ks = DiagK::Create(GaussK::Create(T(30.0)));
    Kface = SymmK::Create(ks, T(0.7), 0);
    model_name = "augmented_face_sym_luthi.bin";
  } else {
    LTS5_LOG_ERROR("Unknown type of augementation!");
    return;
  }

  // Combine kernels
  auto Kneutral = MatKComp::Add(Kbfm, Kface);
  // Low rank approximation using Pivot-Cholesky method
  auto basis = LowRankKernelApproximation<3, 3, T>(ref,
                                                   Kneutral,
                                                   T(1e-5),
                                                   1000);

  err = basis.Save(Path::Join(output_folder, model_name));
  if (err) {
    LTS5_LOG_ERROR("Unable to save model into: " << output_folder);
  }
}


int main(int argc, const char * argv[]) {
  CmdLineParser parser;
  parser.AddArgument("--mesh",
                     CmdLineParser::ArgState::kNeeded,
                     "Path to reference surface");
  parser.AddArgument("--bfm_neutral_path",
                     CmdLineParser::ArgState::kNeeded,
                     "Location where BFM Model is stored");
  parser.AddArgument("--face_mask_path",
                     CmdLineParser::ArgState::kNeeded,
                     "Location where face mask is stored");
  parser.AddArgument("--augmentation_type",
                     CmdLineParser::ArgState::kNeeded,
                     "Type of augmentation, possible values are "
                     "`gerig` or `luthi`");
  parser.AddArgument("--output",
                     CmdLineParser::ArgState::kNeeded,
                     "Location where to write the data");
  // Parse
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    Process<double>(parser);
  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
}