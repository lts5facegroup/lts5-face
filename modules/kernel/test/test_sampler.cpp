/**
 *  @file   test_sampler.cpp
 *  @brief  Test Sampler classes
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   17/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <string>
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/kernel/sampler.hpp"

using namespace LTS5;
using ::testing::Each;
using ::testing::Lt;
using ::testing::Gt;

static std::string mesh_filename;

template<int NDim, typename T>
struct Builder {
  using PointT = PointType<NDim, T>;
  static PointT DefinePt(const T& v) {
    PointT pt;
    for (int k = 0; k < NDim; ++k) {
      pt[k] = v;
    }
    return pt;
  }
};


template<int NDim, typename T>
struct TestParams {
  static constexpr int kDims() {return NDim;};
  using Type = T;
};

template<typename T>
class TestNdSampler : public ::testing::Test {
 public:
  TestNdSampler() = default;
  using DType = typename T::Type;
};

template<typename T>
class TestMeshSampler : public ::testing::Test {
 public:
  TestMeshSampler() = default;
};

// List of type to test against
using NdTestTypes = ::testing::Types<TestParams<1, float>,
        TestParams<1, double>,
        TestParams<2, float>,
        TestParams<2, double>,
        TestParams<3, float>,
        TestParams<3, double>,
        TestParams<4, float>,
        TestParams<4, double>>;
using MeshTestTypes = ::testing::Types<float, double>;

// Instantiate test suite
TYPED_TEST_CASE(TestNdSampler, NdTestTypes);
TYPED_TEST_CASE(TestMeshSampler, MeshTestTypes);


TYPED_TEST(TestNdSampler, UniformSampler) {
  using DType = typename TestFixture::DType;
  using BoxDomType = BoxDomain<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;
  using UniSampler = UniformSampler<TypeParam::kDims(), DType>;

  // Define domain
  auto dom = BoxDomType::Create(PtBuilder::DefinePt(0.0),
                                PtBuilder::DefinePt(5.0));
  // Sampler with 5 points
  UniSampler sampler(dom, 100);

  { // Sample without proba
    auto samples = sampler.Sample();
    EXPECT_EQ(samples.size(), 100);
    for (const auto &s : samples) {
      EXPECT_TRUE(dom->IsDefinedAt(s));
    }
  }

  { // Sample with proba
    auto samples = sampler.SampleWithProbability();
    auto prob = DType(1.0) / dom->Volume();
    EXPECT_EQ(samples.size(), 100);
    for (const auto &s : samples) {
      EXPECT_TRUE(dom->IsDefinedAt(s.pts));
      EXPECT_NEAR(s.prob, prob, 1e-6);
    }
  }
}

TYPED_TEST(TestMeshSampler, UniformMeshSampler) {
  using DType = TypeParam;
  using Vec3 = Vector3<DType>;
  using MeshSampler = UniformMeshSampler<DType>;
  // Load mesh
  Mesh<DType, Vec3> mesh;
  EXPECT_EQ(mesh.Load(mesh_filename), 0);
  // Create sampler for 200 points
  MeshSampler sampler(mesh, 200);
  // Produce different points when called multiple times
  auto set1 = sampler.Sample();
  auto set2 = sampler.Sample();
  std::vector<DType> sample_difference;
  for (size_t i = 0; i < set1.size(); ++i) {
    const auto& s1 = set1[i];
    const auto& s2 = set2[i];
    auto diff = (s1 - s2).SquaredNorm();
    sample_difference.push_back(diff);
  }
  // `sample_difference` must be > 1e-8 to be considered two different samples
  EXPECT_THAT(sample_difference, Each(Gt(1e-8)));
}

TYPED_TEST(TestMeshSampler, FixedUniformMeshSampler) {
  using DType = TypeParam;
  using Vec3 = Vector3<DType>;
  using MeshSampler = FixedPointsUniformMeshSampler<DType>;
  // Load mesh
  Mesh<DType, Vec3> mesh;
  EXPECT_EQ(mesh.Load(mesh_filename), 0);
  // Create sampler for 200 points
  MeshSampler sampler(mesh, 200);
  // Produce SAME points when called multiple times
  auto set1 = sampler.Sample();
  auto set2 = sampler.Sample();
  std::vector<DType> sample_difference;
  for (size_t i = 0; i < set1.size(); ++i) {
    const auto& s1 = set1[i];
    const auto& s2 = set2[i];
    auto diff = (s1 - s2).SquaredNorm();
    sample_difference.push_back(diff);
  }
  // `sample_difference` must be < 1e-8 to be considered SAME samples
  EXPECT_THAT(sample_difference, Each(Lt(1e-8)));
}


int main(int argc, const char * argv[]) {
  CmdLineParser parser;
  parser.AddArgument("--mesh",
                     CmdLineParser::ArgState::kNeeded,
                     "Path to mesh file");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Get command line arguments
    parser.HasArgument("--mesh", &mesh_filename);
    // Init test
    ::testing::InitGoogleMock(&argc, const_cast<char **>(argv));
    // Run test
    return RUN_ALL_TESTS();
  }
}