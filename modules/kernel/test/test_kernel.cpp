/**
 *  @file   test_kernel.cpp
 *  @brief  Test Domain classes
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   11/30/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#include <type_traits>
#include <memory>

#include "gtest/gtest.h"

#include "lts5/kernel/kernel.hpp"

using namespace LTS5;

template<int NDim, typename T>
struct TestParams {
  static constexpr int kDims() {return NDim;};
  using Type = T;
};

template<int NDim, typename T>
struct Builder {
  using PointT = PointType<NDim, T>;
  static PointT DefinePt(const T& v) {
    PointT pt;
    for (int k = 0; k < NDim; ++k) {
      pt[k] = v;
    }
    return pt;
  }
};


template<typename T>
class TestKernel : public ::testing::Test {
 public:
  TestKernel() = default;
  using DType = typename T::Type;
};

// List of type to test against
using MyTestTypes = ::testing::Types<TestParams<1, float>,
                                     TestParams<1, double>,
                                     TestParams<2, float>,
                                     TestParams<2, double>,
                                     TestParams<3, float>,
                                     TestParams<3, double>,
                                     TestParams<4, float>,
                                     TestParams<4, double>> ;

// Instantiate test suite
TYPED_TEST_CASE(TestKernel, MyTestTypes);

TYPED_TEST(TestKernel, GaussianKernel) {
  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  auto g = GaussType::Create(DType(3.5));
  auto pt1 = PtBuilder::DefinePt(0.1);
  auto pt2 = PtBuilder::DefinePt(1.0);

  // Same points should provide value near 1.0
  EXPECT_NEAR(g->operator()(pt1, pt1),
              1.0,
              1e-8);
  // Point far away should give value near 0.0
  EXPECT_NEAR(g->operator()(pt1, PtBuilder::DefinePt(100.0)),
              0.0,
              1e-8);
  // Some points
  EXPECT_NEAR(g->operator()(pt1, pt2),
            std::exp(-(pt1 - pt2).SquaredNorm() / (DType(3.5) * DType(3.5))),
            1e-8);

}

TYPED_TEST(TestKernel, KernelAddition) {
  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using KComposer = KernelComposer<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Create kernel
  auto g = GaussType::Create(DType(3.5));
  auto g_add = KComposer::Add(g, g);
  auto pt1 = PtBuilder::DefinePt(0.1);
  auto pt2 = PtBuilder::DefinePt(1.0);
  // Evaluate kernel
  DType g_val = g->operator()(pt1, pt2);
  DType g_add_val = g_add->operator()(pt1, pt2);
  EXPECT_NEAR(g_val + g_val, g_add_val, 1e-5);
}

TYPED_TEST(TestKernel, KernelMultiply) {
  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using KComposer = KernelComposer<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Create kernel
  auto g = GaussType::Create(DType(3.5));
  auto g_mult = KComposer::Multiply(g, g);
  auto pt1 = PtBuilder::DefinePt(0.1);
  auto pt2 = PtBuilder::DefinePt(1.0);
  // Evaluate kernel
  DType g_val = g->operator()(pt1, pt2);
  DType g_mult_val = g_mult->operator()(pt1, pt2);
  EXPECT_NEAR(g_val * g_val, g_mult_val, 1e-5);
}

TYPED_TEST(TestKernel, KernelScaling) {
  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using KComposer = KernelComposer<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Create kernel
  auto g = GaussType::Create(DType(3.5));
  auto g_scale = KComposer::Scale(g, 2.0);
  auto pt1 = PtBuilder::DefinePt(0.1);
  auto pt2 = PtBuilder::DefinePt(1.0);
  // Evaluate kernel
  DType g_val = g->operator()(pt1, pt2);
  DType g_scale_val = g_scale->operator()(pt1, pt2);
  EXPECT_NEAR(g_val * 2.0, g_scale_val, 1e-5);
}

template<int NDim, typename T>
struct PtsTransformer {
  PointType<NDim, T> operator()(const PointType<NDim, T>& x) {
    return x * T(2.0);
  }
};

TYPED_TEST(TestKernel, KernelTransform) {
  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using KComposer = KernelComposer<TypeParam::kDims(), DType>;
  using PtsTrsfrmFn = PtsTransformer<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Create kernel
  auto g = GaussType::Create(DType(3.5));
  auto g_scale = KComposer::Transform(g, PtsTrsfrmFn());
  auto pt1 = PtBuilder::DefinePt(0.1);
  auto pt2 = PtBuilder::DefinePt(1.0);
  // Evaluate kernel
  DType g_val = std::exp(-((pt1 - pt2) * DType(2.0)).SquaredNorm() / (3.5 * 3.5));
  DType g_trsfrm_val = g_scale->operator()(pt1, pt2);
  EXPECT_NEAR(g_val, g_trsfrm_val, 1e-5);
}

int main(int argc, const char * argv[]) {
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  return RUN_ALL_TESTS();
}