/**
 *  @file   test_kernel.cpp
 *  @brief  Test Field interpolator class
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   06/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <memory>
#include <random>
#include <chrono>

#include "gtest/gtest.h"

#include "lts5/kernel/field_interpolator.hpp"

using namespace LTS5;

template<int NDim, typename T>
struct TestParams {
  static constexpr int kDims() {return NDim;};
  using Type = T;
};

static unsigned seed() {
  return std::chrono::system_clock::now().time_since_epoch().count();
}

template<int NDim, typename T>
struct Builder {
  using PointT = PointType<NDim, T>;
  using TriType = Triangle<NDim, T>;

  Builder() : Builder(T(0.0), T(1.0)) {}

  Builder(T min_value, T max_value) : Builder(min_value,
                                              max_value,
                                              seed()) {}

  Builder(T min_value, T max_value, unsigned seed) : dist_(min_value,
                                                           max_value),
                                                     gen_(seed) {
  }

  static PointT DefinePt(const T& v) {
    PointT pt;
    for (int i = 0; i < NDim; ++i) {
      pt[i] = v;
    }
    return pt;
  }

  /** Random Scalar */
  T RandomScalar(T offset, T scale) {
    return offset + scale * dist_(gen_);
  }

  /** Generate random point */
  PointT RandomPoint() {
    PointT pt;
    for (int i = 0; i < NDim; ++i) {
      pt[i] = dist_(gen_);
    }
    return pt;
  }

  PointT RandomPoint(T offset, T scale) {
    PointT pt;
    for (int i = 0; i < NDim; ++i) {
      pt[i] = offset + scale * dist_(gen_);
    }
    return pt;
  }

  TriType RandomTri(T offset, T scale) {
    auto a = this->RandomPoint(offset, scale);
    auto b = this->RandomPoint(offset, scale);
    auto c = this->RandomPoint(offset, scale);
    return TriType(a, b, c);
  }

  /** Distribution */
  std::uniform_real_distribution<T> dist_;
  /** Generator */
  std::mt19937 gen_;
};

template<int NDim, typename T>
PointType<NDim, T>
FindClosestPoint(const std::vector<PointType<NDim, T>>& points,
                 PointType<NDim, T>& query) {
  T min_dist = std::numeric_limits<T>::max();
  PointType<NDim, T> closest;
  for (const auto& pt : points) {
    T d = (query - pt).SquaredNorm();
    if (d < min_dist) {
      closest = pt;
      min_dist = d;
    }
  }
  return closest;
}


template<typename T>
class TestFieldInterpolator : public ::testing::Test {
 public:
  TestFieldInterpolator() = default;
  using DType = typename T::Type;
};

// List of type to test against
using MyTestTypes = ::testing::Types<TestParams<1, float>,
        TestParams<1, double>,
        TestParams<2, float>,
        TestParams<2, double>,
        TestParams<3, float>,
        TestParams<3, double>,
        TestParams<4, float>,
        TestParams<4, double>> ;

// Instantiate test suite
TYPED_TEST_CASE(TestFieldInterpolator, MyTestTypes);

TYPED_TEST(TestFieldInterpolator, NearestNeighborInterpolator) {
  using DType = typename TestFixture::DType;
  using PtType = PointType<TypeParam::kDims(), DType>;
  using Scalar = PointType<1, DType>;
  using DDom = UnstructuredPointDomain<TypeParam::kDims(), DType>;
  using DField = DiscreteField<TypeParam::kDims(), 1, DType>; // Scalar field
  using PtBuilder = Builder<TypeParam::kDims(), DType>;
  using Interpolator = NearestNeighborInterpolator<TypeParam::kDims(), 1, DType>;

  PtBuilder builder;

  // Create random data point
  std::vector<PtType> pts;
  std::vector<Scalar> data;
  for (size_t k = 0; k < 100; ++k) {
    auto pt = builder.RandomPoint(DType(-1.0), DType(2.0));  // [-1, 1]
    pts.emplace_back(pt);
    data.emplace_back(pt.x_);
  }
  // Discrete Field: Take point as input and return component `x` as output.
  auto ddom = DDom::Create(pts);
  auto dfield = DField::Create(ddom, data);
  // Interpolate
  Interpolator interpolator;
  auto cfield = interpolator.Interpolate(dfield);
  // Check
  for (size_t k = 0; k < 200; ++k) {
    auto q = builder.RandomPoint(DType(-1.0), DType(2.0));
    // Should be defined everywhere
    EXPECT_TRUE(cfield->IsDefinedAt(q));
    // Get closest scalar
    auto q_value = cfield->operator()(q);
    // True closest point via brute force
    auto closest_pt = FindClosestPoint<TypeParam::kDims(), DType>(pts, q);
    EXPECT_EQ(closest_pt.x_, q_value.x_);
  }
}

int main(int argc, const char * argv[]) {
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  return RUN_ALL_TESTS();
}




