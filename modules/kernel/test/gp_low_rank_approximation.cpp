/**
 *  @file   gp_low_rank_approximation.cpp
 *  @brief  Shows how variance modeled through continuous kernel can be
 *      approximated using GP.
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   07/01/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/sys/file_system.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/kernel/domain.hpp"
#include "lts5/kernel/sampler.hpp"
#include "lts5/kernel/matrix_kernel.hpp"
#include "lts5/kernel/kernel_approximation.hpp"

using namespace LTS5;


template<typename T>
void Process(const CmdLineParser& parser) {
  using Vec3 = Vector3<T>;
  using Mesh_t = Mesh<T, Vec3>;
  using UnstructDom = UnstructuredPointDomain<3, T>;

  std::string filename, output;
  parser.HasArgument("--mesh", &filename);
  parser.HasArgument("--output", &output);
  // Create destination folder
  auto fs = GetDefaultFileSystem();
  if (!fs->FileExist(output).Good()) {
    fs->CreateDirRecursively(output);
  }
  // Load mesh
  Mesh_t mesh;
  int err = mesh.Load(filename);
  if (err) {
    LTS5_LOG_ERROR("Can not open file: " << filename);
    return;
  }
  // Create Domain
  auto domain = TriangleMesh<T>::Create(mesh.get_vertex(),
                                        mesh.get_triangle());
  // Create kernel
  auto ks = GaussianKernel<3, T>::Create(T(30.0));
  auto cov = DiagonalKernel<3, 3, T>::Create(ks);
  auto kl_basis = LowRankKernelApproximation<3, 3, T>(domain,
                                                      cov,
                                                      1e-5,
                                                      1000);
  // Save basis
  kl_basis.Save(Path::Join(output, "discrete_kl_basis.bin"));
  // Generate random sample
  for (int i = 0; i < 10; ++i) {
    cv::Mat rnd_surface;
    Mesh_t m;
    kl_basis.Sample(&rnd_surface);
    int n_vertex = rnd_surface.rows / 3;
    std::vector<Vec3> vertex(reinterpret_cast<Vec3*>(rnd_surface.data),
                             reinterpret_cast<Vec3*>(rnd_surface.data) + n_vertex);
    m.set_vertex(vertex);
    m.set_triangle(mesh.get_triangle());
    m.set_vertex_color(mesh.get_vertex_color());
    m.Save(Path::Join(output, "random_sample" + std::to_string(i) + ".ply"));
  }

}

int main(int argc, const char * argv[]) {
  CmdLineParser parser;
  parser.AddArgument("--mesh",
                     CmdLineParser::ArgState::kNeeded,
                     "Path to reference surface");
  parser.AddArgument("--output",
                     CmdLineParser::ArgState::kNeeded,
                     "Location where to write the data");
  // Parse
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    Process<double>(parser);
  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
}