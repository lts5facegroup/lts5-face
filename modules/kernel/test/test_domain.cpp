/**
 *  @file   test_domain.cpp
 *  @brief  
 *  @ingroup 
 *
 *  @author Christophe Ecabert
 *  @date   11/30/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <random>
#include <chrono>
#include <vector>

#include "gtest/gtest.h"

#include "lts5/geometry/bounding_sphere.hpp"
#include "lts5/kernel/domain.hpp"

using namespace LTS5;

static unsigned seed() {
  return std::chrono::system_clock::now().time_since_epoch().count();
}

template<int NDim, typename T>
struct TestParams {
  static constexpr int kDims() {return NDim;};
  using Type = T;
};

template<int NDim, typename T>
struct Builder {
  using PointT = PointType<NDim, T>;
  using TriType = Triangle<NDim, T>;

  Builder() : Builder(T(0.0), T(1.0)) {}

  Builder(T min_value, T max_value) : Builder(min_value,
                                              max_value,
                                              seed()) {}

  Builder(T min_value, T max_value, unsigned seed) : dist_(min_value,
                                                           max_value),
                                                     gen_(seed) {
  }

  static PointT DefinePt(const T& v) {
    PointT pt;
    for (int i = 0; i < NDim; ++i) {
      pt[i] = v;
    }
    return pt;
  }

  /** Generate random point */
  PointT RandomPoint() {
    PointT pt;
    for (int i = 0; i < NDim; ++i) {
      pt[i] = dist_(gen_);
    }
    return pt;
  }

  PointT RandomPoint(T offset, T scale) {
    PointT pt;
    for (int i = 0; i < NDim; ++i) {
      pt[i] = offset + scale * dist_(gen_);
    }
    return pt;
  }

  TriType RandomTri(T offset, T scale) {
    auto a = this->RandomPoint(offset, scale);
    auto b = this->RandomPoint(offset, scale);
    auto c = this->RandomPoint(offset, scale);
    return TriType(a, b, c);
  }

  /** Distribution */
  std::uniform_real_distribution<T> dist_;
  /** Generator */
  std::mt19937 gen_;
};

template<int NDim, typename T>
PointType<NDim, T>
FindClosestPoint(const std::vector<PointType<NDim, T>>& points,
                 PointType<NDim, T>& query) {
  T min_dist = std::numeric_limits<T>::max();
  PointType<NDim, T> closest;
  for (const auto& pt : points) {
    T d = (query - pt).SquaredNorm();
    if (d < min_dist) {
      closest = pt;
      min_dist = d;
    }
  }
  return closest;
}


template<typename T>
class TestDomain : public ::testing::Test {
 public:
  TestDomain() = default;

  using DType = typename T::Type;
  //using DomainType = LTS5::Domain<T::kDims, DType>;
};

template<typename T>
class TestDomain3D : public ::testing::Test {
 public:
  TestDomain3D() = default;

  using DType = typename T::Type;
};

// List of type to test against
using MyTestTypes = ::testing::Types<TestParams<1, float>,
                                     TestParams<1, double>,
                                     TestParams<2, float>,
                                     TestParams<2, double>,
                                     TestParams<3, float>,
                                     TestParams<3, double>,
                                     TestParams<4, float>,
                                     TestParams<4, double>> ;
using MyTestTypes3D = ::testing::Types<TestParams<3, float>,
                                       TestParams<3, double>> ;

// Instantiate test suite
TYPED_TEST_CASE(TestDomain, MyTestTypes);
TYPED_TEST_CASE(TestDomain3D, MyTestTypes3D);

TYPED_TEST(TestDomain, RealDomain) {
  using DType = typename TestFixture::DType;
  using DomType = LTS5::EuclideanSpace<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Create domain
  auto space = DomType::Create();
  // Point at origin
  EXPECT_TRUE(space->IsDefinedAt(PtBuilder::DefinePt(DType(0.0))));
  // Positive
  EXPECT_TRUE(space->IsDefinedAt(PtBuilder::DefinePt(DType(10.0))));
  // Negative
  EXPECT_TRUE(space->IsDefinedAt(PtBuilder::DefinePt(DType(-10.0))));
}

TYPED_TEST(TestDomain, BoxDomain) {
  using DType = typename TestFixture::DType;
  using DomType = LTS5::BoxDomain<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Create box
  auto dom = DomType::Create(PtBuilder::DefinePt(0.0),
                             PtBuilder::DefinePt(5.0));

  // Extent
  EXPECT_TRUE(dom->Extent() == PtBuilder::DefinePt(5.0));
  // Volume
  EXPECT_EQ(dom->Volume(), std::pow(5.0, TypeParam::kDims()));
  // Inside
  EXPECT_TRUE(dom->IsDefinedAt(PtBuilder::DefinePt(2.5)));
  // Outside
  EXPECT_FALSE(dom->IsDefinedAt(PtBuilder::DefinePt(-2.5)));
  EXPECT_FALSE(dom->IsDefinedAt(PtBuilder::DefinePt(7.5)));
  // On border
  EXPECT_TRUE(dom->IsDefinedAt(PtBuilder::DefinePt(0.0)));
  EXPECT_TRUE(dom->IsDefinedAt(PtBuilder::DefinePt(5.0)));
}

TYPED_TEST(TestDomain, DomainComposerIntersection) {
  using DType = typename TestFixture::DType;
  using DomTypeA = LTS5::EuclideanSpace<TypeParam::kDims(), DType>;
  using DomTypeB = LTS5::BoxDomain<TypeParam::kDims(), DType>;
  using DomComp = LTS5::DomainComposer<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Create box
  auto domA = DomTypeA::Create();
  auto domB = DomTypeB::Create(PtBuilder::DefinePt(0.0),
                               PtBuilder::DefinePt(5.0));
  auto inter = DomComp::Intersection(domA, domB);

  // Inside
  EXPECT_TRUE(inter->IsDefinedAt(PtBuilder::DefinePt(2.5)));
  // Outside of box
  EXPECT_FALSE(inter->IsDefinedAt(PtBuilder::DefinePt(-2.5)));
  EXPECT_FALSE(inter->IsDefinedAt(PtBuilder::DefinePt(7.5)));
  // On border
  EXPECT_TRUE(inter->IsDefinedAt(PtBuilder::DefinePt(0.0)));
  EXPECT_TRUE(inter->IsDefinedAt(PtBuilder::DefinePt(5.0)));
}

TYPED_TEST(TestDomain, DomainComposerUnion) {
  using DType = typename TestFixture::DType;
  using DomTypeA = LTS5::EuclideanSpace<TypeParam::kDims(), DType>;
  using DomTypeB = LTS5::BoxDomain<TypeParam::kDims(), DType>;
  using DomComp = LTS5::DomainComposer<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Create box
  auto domA = DomTypeA::Create();
  auto domB = DomTypeB::Create(PtBuilder::DefinePt(0.0),
                               PtBuilder::DefinePt(5.0));
  auto udom = DomComp::Union(domA, domB);

  // Inside
  EXPECT_TRUE(udom->IsDefinedAt(PtBuilder::DefinePt(2.5)));
  // Outside of box
  EXPECT_TRUE(udom->IsDefinedAt(PtBuilder::DefinePt(-2.5)));
  EXPECT_TRUE(udom->IsDefinedAt(PtBuilder::DefinePt(7.5)));
  // On border
  EXPECT_TRUE(udom->IsDefinedAt(PtBuilder::DefinePt(0.0)));
  EXPECT_TRUE(udom->IsDefinedAt(PtBuilder::DefinePt(5.0)));
}

TYPED_TEST(TestDomain, DomainComposerComposition) {
  using DType = typename TestFixture::DType;
  using DomTypeA = LTS5::EuclideanSpace<TypeParam::kDims(), DType>;
  using DomTypeB = LTS5::BoxDomain<TypeParam::kDims(), DType>;
  using DomComp = LTS5::DomainComposer<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // First domain
  auto domA = DomComp::Union(DomTypeB::Create(PtBuilder::DefinePt(0.0),
                                              PtBuilder::DefinePt(2.0)),
                             DomTypeB::Create(PtBuilder::DefinePt(4.0),
                                              PtBuilder::DefinePt(6.0)));
  // Second domain
  auto domB = DomComp::Intersection(DomTypeA::Create(),
                                    DomTypeB::Create(PtBuilder::DefinePt(-3.0),
                                                     PtBuilder::DefinePt(-1.0)));
  // Combine everything
  auto dom = DomComp::Union(domA, domB);

  // Inside
  EXPECT_TRUE(dom->IsDefinedAt(PtBuilder::DefinePt(-2.0)));
  EXPECT_TRUE(dom->IsDefinedAt(PtBuilder::DefinePt(1.0)));
  EXPECT_TRUE(dom->IsDefinedAt(PtBuilder::DefinePt(5.0)));
  // Outside
  EXPECT_FALSE(dom->IsDefinedAt(PtBuilder::DefinePt(-5.0)));
  EXPECT_FALSE(dom->IsDefinedAt(PtBuilder::DefinePt(3.0)));
  EXPECT_FALSE(dom->IsDefinedAt(PtBuilder::DefinePt(8.0)));
}

TYPED_TEST(TestDomain, PointIndex) {
  using DType = typename TestFixture::DType;
  using PtType = PointType<TypeParam::kDims(), DType>;
  using DomType = UnstructuredPointDomain<TypeParam::kDims(), DType>;
  using PtList = std::vector<PtType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  PtBuilder builder(-1.0, 5.0);
  auto pt1 = builder.RandomPoint();
  auto pt2 = builder.RandomPoint();
  auto pt3 = builder.RandomPoint();
  PtList points = {pt1, pt3, pt2, pt3};
  DomType dom(points);

  // Point inside domain
  EXPECT_EQ(dom.Index(pt1), 0);
  EXPECT_EQ(dom.Index(pt2), 2);
  EXPECT_EQ(dom.Index(pt3), 1);
  // Point outside domain
  EXPECT_EQ(dom.Index(builder.RandomPoint()), -1);
}

TYPED_TEST(TestDomain, DefinedAt) {
  using DType = typename TestFixture::DType;
  using PtType = PointType<TypeParam::kDims(), DType>;
  using DomType = UnstructuredPointDomain<TypeParam::kDims(), DType>;
  using PtList = std::vector<PtType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  PtBuilder builder(-1.0, 5.0);
  auto pt1 = builder.RandomPoint();
  auto pt2 = builder.RandomPoint();
  auto pt3 = builder.RandomPoint();
  PtList points = {pt1, pt3, pt2, pt3};
  DomType dom(points);

  // Point inside domain
  EXPECT_TRUE(dom.IsDefinedAt(pt1));
  EXPECT_TRUE(dom.IsDefinedAt(pt2));
  EXPECT_TRUE(dom.IsDefinedAt(pt3));
  // Point outside domain
  EXPECT_FALSE(dom.IsDefinedAt(builder.RandomPoint()));
}

TYPED_TEST(TestDomain, CLosestPoint) {
  using DType = typename TestFixture::DType;
  using PtType = PointType<TypeParam::kDims(), DType>;
  using DomType = UnstructuredPointDomain<TypeParam::kDims(), DType>;
  using PtList = std::vector<PtType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  PtBuilder builder(-1.0, 5.0);
  auto pt1 = builder.RandomPoint();
  auto pt2 = builder.RandomPoint();
  auto pt3 = builder.RandomPoint();
  PtList points = {pt1, pt3, pt2, pt3};
  DomType dom(points);

  // Point part of domain
  EXPECT_EQ(dom.FindClosestPoint(pt1), pt1);
  // Point NOT part of domain
  auto query = builder.RandomPoint();
  auto true_closest = FindClosestPoint<TypeParam::kDims(), DType>(points,
                                                                  query);
  EXPECT_EQ(dom.FindClosestPoint(query), true_closest);
}

TYPED_TEST(TestDomain, Storage) {
  using DType = typename TestFixture::DType;
  using PtType = PointType<TypeParam::kDims(), DType>;
  using BaseDomType = DiscreteDomain<TypeParam::kDims(), DType>;
  using DomType = UnstructuredPointDomain<TypeParam::kDims(), DType>;
  using PtList = std::vector<PtType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  PtBuilder builder(-1.0, 5.0);
  auto pt1 = builder.RandomPoint();
  auto pt2 = builder.RandomPoint();
  auto pt3 = builder.RandomPoint();
  PtList points = {pt1, pt3, pt2, pt3};
  DomType dom(points);

  {
    // Save
    BaseDomType* dom_ptr = &dom;
    EXPECT_EQ(dom_ptr->Save("points_domain.bin"), 0);
  }
  {
    DomType dom_new;
    BaseDomType* dom_ptr = &dom_new;
    EXPECT_EQ(dom_ptr->Load("points_domain.bin"), 0);
    // Compare points
    EXPECT_EQ(dom_new[0], pt1);
    EXPECT_EQ(dom_new[1], pt3);
    EXPECT_EQ(dom_new[2], pt2);
    EXPECT_EQ(dom_new[3], pt3);
  }
}

TYPED_TEST(TestDomain, TransformPoint) {
  using DType = typename TestFixture::DType;
  using PtType = PointType<TypeParam::kDims(), DType>;
  using DomType = UnstructuredPointDomain<TypeParam::kDims(), DType>;
  using PtList = std::vector<PtType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  PtBuilder builder(-1.0, 5.0);
  auto pt1 = builder.RandomPoint();
  auto pt2 = builder.RandomPoint();
  auto pt3 = builder.RandomPoint();
  PtList points = {pt1, pt3, pt2, pt3};
  DomType dom(points);

  auto fcn = [](const PtType& pt) -> PtType {
    return pt * DType(2.0);
  };

  PtList trsfrm_pts;
  dom.Transform(fcn, &trsfrm_pts);
  EXPECT_EQ(trsfrm_pts.size(), 4);
  EXPECT_EQ(trsfrm_pts[0], pt1 * DType(2.0));
  EXPECT_EQ(trsfrm_pts[1], pt3 * DType(2.0));
  EXPECT_EQ(trsfrm_pts[2], pt2 * DType(2.0));
  EXPECT_EQ(trsfrm_pts[3], pt3 * DType(2.0));
}

TYPED_TEST(TestDomain3D, ClosestPointOnSurface) {
  using DType = typename TestFixture::DType;
  using PtType = PointType<TypeParam::kDims(), DType>;
  using TriType = Triangle<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;
  using UnstructDom = UnstructuredPointDomain<TypeParam::kDims(), DType>;
  using MeshDom = TriangleMesh<DType>;

  // should return an equal or smaller distance when used for points than the
  // findClosestPoint from UnstructuredPoints for triangles

  PtBuilder builder;
  // Create random triangles
  std::vector<TriType> triangles;
  std::vector<PtType> points;
  std::vector<Vector3<int>> topology;
  for (size_t k = 0; k < 100; ++k) {
    auto tri = builder.RandomTri(DType(0.0), DType(1.0));
    triangles.emplace_back(tri);
    points.emplace_back(tri.a_);
    points.emplace_back(tri.b_);
    points.emplace_back(tri.c_);
    topology.emplace_back(3 * k, (3 * k) + 1, (3 * k) + 2);
  }
  // Create domains
  UnstructDom pd(points);
  MeshDom sd(points, topology);
  // Query
  for (size_t k = 0; k < 200; ++k) {
    // Define query points
    auto q = builder.RandomPoint();
    // Nearest point
    auto nearest_pt = pd.FindClosestPoint(q);
    DType nearest_dist = (nearest_pt - q).SquaredNorm();
    // CLosest point on surface
    auto closest_surf_pt = sd.FindClosestPoint(q);
    DType closest_surf_dist = (closest_surf_pt - q).SquaredNorm();
    EXPECT_GE(nearest_dist, closest_surf_dist);
  }
}


int main(int argc, const char * argv[]) {
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  return RUN_ALL_TESTS();
}