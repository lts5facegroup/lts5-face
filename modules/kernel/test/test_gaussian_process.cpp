/**
 *  @file   test_kernel.cpp
 *  @brief  Test Domain classes
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   11/30/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <memory>

#include "gtest/gtest.h"

#include "lts5/kernel/gaussian_process.hpp"

using namespace LTS5;

template<int NDim, typename T>
struct TestParams {
  static constexpr int kDims() {return NDim;};
  using Type = T;
};

template<int NDim, typename T>
struct Builder {
  using PointT = PointType<NDim, T>;
  static PointT DefinePt(const T& v) {
    PointT pt;
    for (int k = 0; k < NDim; ++k) {
      pt[k] = v;
    }
    return pt;
  }
};


template<typename T>
class TestGaussianProcess: public ::testing::Test {
 public:
  TestGaussianProcess() = default;
  using DType = typename T::Type;
};

// List of type to test against
using MyTestTypes = ::testing::Types<TestParams<1, float>,
                                     TestParams<1, double>,
                                     TestParams<3, float>,
                                     TestParams<3, double>>;

// Instantiate test suite
TYPED_TEST_CASE(TestGaussianProcess, MyTestTypes);


TYPED_TEST(TestGaussianProcess, Marginal) {
  using DType = typename TestFixture::DType;
  using PtType = PointType<TypeParam::kDims(), DType>;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using DiagType = DiagonalKernel<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;
  using DomType = BoxDomain<TypeParam::kDims(), DType>;
  using MeanF = ZeroVectorField<TypeParam::kDims(),TypeParam::kDims(), DType>;
  using GPType = GaussianProcess<TypeParam::kDims(), TypeParam::kDims(), DType>;

  // Domain
  auto domain = DomType::Create(PtBuilder::DefinePt(-2.0),
                                PtBuilder::DefinePt(2.0));
  // Mean
  auto mean = std::make_shared<MeanF>();
  // Covariance
  auto cov = DiagType::Create(GaussType::Create(DType(1.0)));
  // Continuous GP
  auto gp = std::make_shared<GPType>(mean, cov);
  // Test points
  std::vector<PtType> pts{PtBuilder::DefinePt(-0.5),
                          PtBuilder::DefinePt(0.5)};
  // Marginalize GP
  auto discrete_gp = gp->Marginal(pts);
  // Compare
  for (size_t i = 0; i < pts.size(); ++i) {
    const auto& pt1 = pts[i];
    // Mean value should be same
    EXPECT_EQ(discrete_gp->Mean(i), gp->Mean(pt1));
    for (size_t j = 0; j < pts.size(); ++j) {
      // Covariance matrix should also be same
      const auto& pt2 = pts[j];
      EXPECT_EQ(discrete_gp->Covariance(i, j),
                gp->Covariance(pt1, pt2));
    }
  }
}
/*
TYPED_TEST(TestKernel, GaussianKernel) {
  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  auto g = GaussType::Create(DType(3.5));
  auto pt1 = PtBuilder::DefinePt(0.1);
  auto pt2 = PtBuilder::DefinePt(1.0);

  // Same points should provide value near 1.0
  EXPECT_NEAR(g->operator()(pt1, pt1),
              1.0,
              1e-8);
  // Point far away should give value near 0.0
  EXPECT_NEAR(g->operator()(pt1, PtBuilder::DefinePt(100.0)),
              0.0,
              1e-8);
  // Some points
  EXPECT_NEAR(g->operator()(pt1, pt2),
            std::exp(-(pt1 - pt2).SquaredNorm() / (DType(3.5) * DType(3.5))),
            1e-8);

}*/


int main(int argc, const char * argv[]) {
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  return RUN_ALL_TESTS();
}