/**
 *  @file   test_matrix_kernel.cpp
 *  @brief  Test Domain classes
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   11/30/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include "gtest/gtest.h"

#include "lts5/kernel/kernel.hpp"
#include "lts5/kernel/matrix_kernel.hpp"


using namespace LTS5;

template<int NDim, typename T>
struct TestParams {
  static constexpr int kDims() {return NDim;};
  using Type = T;
};

template<int NDim, typename T>
struct Builder {
  using PointT = PointType<NDim, T>;
  static PointT DefinePt(const T& v) {
    PointT pt;
    for (int k = 0; k < NDim; ++k) {
      pt[k] = v;
    }
    return pt;
  }
};

template<typename T>
class TestMatrixKernel : public ::testing::Test {
 public:
  TestMatrixKernel() = default;
  using DType = typename T::Type;
};

// List of type to test against
using MyTestTypes = ::testing::Types<TestParams<1, float>,
                                     TestParams<1, double>,
                                     TestParams<2, float>,
                                     TestParams<2, double>,
                                     TestParams<3, float>,
                                     TestParams<3, double>,
                                     TestParams<4, float>,
                                     TestParams<4, double>> ;

// Instantiate test suite
TYPED_TEST_CASE(TestMatrixKernel, MyTestTypes);

TYPED_TEST(TestMatrixKernel, DiagonalGaussianKernel) {
  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using DiagType = DiagonalKernel<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using MatType = typename Matrix<TypeParam::kDims(), DType>::Type;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Setup two kernel
  auto k1 = DiagType::Create(GaussType::Create(DType(3.5)));
  // Two points
  auto pt1 = PtBuilder::DefinePt(0.0);
  auto pt2 = PtBuilder::DefinePt(1.0);

  // Same points should provide value near 1.0
  EXPECT_EQ(k1->operator()(pt1, pt1),
            MatType().Identity());
  // Point far away should give value near 0.0
  EXPECT_EQ(k1->operator()(pt1, PtBuilder::DefinePt(100.0)),
              MatType().Zeros());
  // Some points
  auto s = std::exp(-(pt1 - pt2).SquaredNorm() / (DType(3.5) * DType(3.5)));
  auto gt = MatType().Identity() * s;
  EXPECT_EQ(k1->operator()(pt1, pt2),
            gt);
}

TYPED_TEST(TestMatrixKernel, MatrixKernelAddition) {
  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using DiagType = DiagonalKernel<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using MatType = typename Matrix<TypeParam::kDims(), DType>::Type;
  using KComposer = MatrixKernelComposer<TypeParam::kDims(), TypeParam::kDims(),DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Create kernel
  auto g = DiagType::Create(GaussType::Create(DType(3.5)));
  auto g_add = KComposer::Add(g, g);
  auto pt1 = PtBuilder::DefinePt(0.1);
  auto pt2 = PtBuilder::DefinePt(1.0);
  // Evaluate kernel
  MatType g_val = MatType().Identity() * g->operator()(pt1, pt2);
  MatType  g_add_val = g_add->operator()(pt1, pt2);
  EXPECT_EQ(g_val + g_val, g_add_val);
}

TYPED_TEST(TestMatrixKernel, MatrixKernelMultiply) {
  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using DiagType = DiagonalKernel<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using KComposer = MatrixKernelComposer<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Create kernel
  auto g = DiagType::Create(GaussType::Create(DType(3.5)));
  auto g_mult = KComposer::Multiply(g, g);
  auto pt1 = PtBuilder::DefinePt(0.1);
  auto pt2 = PtBuilder::DefinePt(1.0);
  // Evaluate kernel
  auto g_val = g->operator()(pt1, pt2);
  auto g_mult_val = g_mult->operator()(pt1, pt2);
  // Don't use operator* between two matrix, otherwise will perform matmul
  // instead of element-wise product
  auto gtrue = g_val * g_val(0, 0);
  EXPECT_EQ(gtrue, g_mult_val);
}

TYPED_TEST(TestMatrixKernel, MatrixKernelScaling) {
  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using DiagType = DiagonalKernel<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using KComposer = MatrixKernelComposer<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;

  // Create kernel
  auto g = DiagType::Create(GaussType::Create(DType(3.5)));
  auto g_scale = KComposer::Scale(g, 2.0);
  auto pt1 = PtBuilder::DefinePt(0.1);
  auto pt2 = PtBuilder::DefinePt(1.0);
  // Evaluate kernel
  auto gtrue = g->operator()(pt1, pt2) * DType(2.0);
  auto g_scale_val = g_scale->operator()(pt1, pt2);
  EXPECT_EQ(gtrue, g_scale_val);
}

template<int NDim, typename T>
struct PtsTransformer {
  PointType<NDim, T> operator()(const PointType<NDim, T>& x) {
    return x * T(2.0);
  }
};

TYPED_TEST(TestMatrixKernel, MatrixKernelTransform) {

  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using DiagType = DiagonalKernel<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using MatType = typename Matrix<TypeParam::kDims(), DType>::Type;
  using KComposer = MatrixKernelComposer<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;
  using PtsTrsfrmFn = PtsTransformer<TypeParam::kDims(), DType>;

  // Create kernel
  auto g = DiagType::Create(GaussType::Create(DType(3.5)));
  auto g_trsfrm = KComposer::Transform(g, PtsTrsfrmFn());
  auto pt1 = PtBuilder::DefinePt(0.1);
  auto pt2 = PtBuilder::DefinePt(1.0);
  // Evaluate kernel
  auto s = std::exp(-((pt1 - pt2) * DType(2.0)).SquaredNorm() / (3.5 * 3.5));
  auto gtrue = MatType().Identity() * s;
  auto g_trsfrm_val = g_trsfrm->operator()(pt1, pt2);
  EXPECT_EQ(gtrue, g_trsfrm_val);
}

TYPED_TEST(TestMatrixKernel, DiscreteMatrixKernel) {
  using DType = typename TestFixture::DType;
  using GaussType = GaussianKernel<TypeParam::kDims(), DType>;
  using DiagType = DiagonalKernel<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using MatType = typename Matrix<TypeParam::kDims(), DType>::Type;
  using PtType = PointType<TypeParam::kDims(), DType>;
  using PtBuilder = Builder<TypeParam::kDims(), DType>;
  using DMatType = DiscretizeContinuousKernel<TypeParam::kDims(), TypeParam::kDims(), DType>;
  using DDomType = UnstructuredPointDomain<TypeParam::kDims(), DType>;
  // Continuous matrix kernel
  auto g = DiagType::Create(GaussType::Create(DType(3.5)));

  // Discrete domain
  std::vector<PtType> pts = {PtBuilder::DefinePt(0.1),
                             PtBuilder::DefinePt(100.0)};
  auto dom = std::make_shared<DDomType>(pts);
  // Discrete matrix kernel
  DMatType dg(dom, g);
  // Same points should provide value near 1.0
  EXPECT_EQ(dg(0, 0), MatType().Identity());
  // Point far away should give value near 0.0
  EXPECT_EQ(dg(0, 1), MatType().Zeros());
}

int main(int argc, const char * argv[]) {
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  return RUN_ALL_TESTS();
}