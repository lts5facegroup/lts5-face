/**
 *  @file   gp_3dmm_single_scan.cpp
 *  @brief  Implementation of "Building 3D Morphable Models from a Single Scan"
 *      Sutherland et al.
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   03/02/21
 *  Copyright (c) 2021 Christophe Ecabert. All rights reserved.
 */

#include <memory>
#include <string>
#include <memory>
#include <utility>
#include <cassert>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/utils/sys/file_system.hpp"
#include "lts5/kernel/kernel.hpp"
#include "lts5/kernel/matrix_kernel.hpp"
#include "lts5/kernel/kernel_approximation.hpp"

using namespace LTS5; // Bad practice but ok here since not propagated!

// Pointer
template<typename T>
using MatKernelPtr = std::shared_ptr<MatrixKernel<3, 3, T>>;
template<typename T>
using KernelPtr = std::shared_ptr<Kernel<3, T>>;
template<typename T>
using DDomPtr = std::shared_ptr<DiscreteDomain<3, T>>;
template<typename T>
using SamplerPtr = std::shared_ptr<Sampler<3, T>>;
// Type
template<typename T>
using Mesh_t = Mesh<T, Vector3<T>>;
template<typename T>
using GaussK = GaussianKernel<3, T>;
template<typename T>
using DiagK = DiagonalKernel<3, 3, T>;
template<typename T>
using SymmK = SymmetricKernel<3, 3, T>;
template<typename T>
using UnstructDom = UnstructuredPointDomain<3, T>;


template<int IDim, int ODim, typename T>
class CorrelationKernel : public MatrixKernel<IDim, ODim, T> {
 public:
  /** Matrix kernel pointer */
  using KernelPtr = std::shared_ptr<Kernel<IDim, T>>;
  /** Pointer Type */
  using Ptr = std::shared_ptr<CorrelationKernel<IDim, ODim, T>>;

  static Ptr Create(const KernelPtr& kernel, T factor) {
    using K = CorrelationKernel<IDim, ODim ,T>;
    return std::make_shared<K>(kernel, factor);
  }

  CorrelationKernel(const KernelPtr& kernel, T factor) :
          MatrixKernel<IDim, ODim, T>(kernel->domain()),
          k_(kernel) {
    m_corr_.Identity();
    for (int i = 0; i < ODim; ++i) {
      for (int j = i + 1; j < ODim; ++j) {
        m_corr_(i, j) = factor;
        m_corr_(j, i) = factor;
      }
    }
  }

  ~CorrelationKernel() override = default;

 protected:

  MatType<ODim, T> k(const PointType<IDim, T>& x,
                     const PointType<IDim, T>& y) const override {
    return m_corr_ * k_->operator()(x, y);
  }

 private:
  /** Kernel */
  KernelPtr k_;
  /** Correlation matrix */
  MatType<ODim, T> m_corr_;
};

template<int IDim, int ODim, typename T>
class SymColorCorrelated : public MatrixKernel<IDim, ODim, T> {
 public:
  /** Matrix kernel pointer */
  using MatKernelPtr = std::shared_ptr<MatrixKernel<IDim, ODim, T>>;
  /** Pointer type */
  using Ptr = std::shared_ptr<SymColorCorrelated<IDim, ODim, T>>;

  static Ptr Create(const MatKernelPtr& k, T alpha, size_t axis) {
    using K = SymColorCorrelated<IDim, ODim, T>;
    return std::make_shared<K>(k, alpha, axis);
  }

  SymColorCorrelated(const MatKernelPtr& kernel,
                     T alpha,
                     size_t axis) :
          MatrixKernel<IDim, ODim, T>(kernel->domain()),
          k_(kernel),
          alpha_(alpha),
          axis_(axis) {
  }

  ~SymColorCorrelated() override = default;

 protected:

  MatType<ODim, T> k(const PointType<IDim, T>& x,
                     const PointType<IDim, T>& y) const override {
    auto k_out = k_->operator()(x, y);
    // Symmetric points
    PointType<IDim, T> xs(x);
    xs[axis_] *= T(-1.0);
    auto k_out_sym = k_->operator()(xs, y);
    return k_out + (k_out_sym * alpha_);
  }

 private:
  /** Kernel */
  MatKernelPtr k_;
  /** Balancing term */
  T alpha_;
  /** axis */
  size_t axis_;
};

template<int IDim, int ODim, typename T>
class JointMatrixKernel {
 public:
  /** Matrix kernel */
  using MatKernelPtr = std::shared_ptr<MatrixKernel<IDim, ODim, T>>;
  /** Pointer Type */
  using Ptr = std::shared_ptr<JointMatrixKernel<IDim, ODim, T>>;
  /** Input type - pair of points */
  using PointPair = std::pair<PointType<IDim, T>, PointType<IDim, T>>;

  static Ptr Create(const MatKernelPtr& k_shape, const MatKernelPtr& k_color) {
    using K = JointMatrixKernel<IDim, ODim, T>;
    return std::make_shared<K>(k_shape, k_color);
  }

  JointMatrixKernel(const MatKernelPtr& k_shape,
                    const MatKernelPtr& k_color) : k_shp_(k_shape),
                    k_col_(k_color) {
  }

  ~JointMatrixKernel() = default;

  MatType<ODim, T> k(const PointPair& x,
                     const PointPair& y) const {
    auto x_shp = x.first;
    auto x_col = x.second;
    auto y_shp = y.first;
    auto y_col = y.second;
    auto shp = k_shp_->operator()(x_shp, y_shp);
    auto color = k_col_->operator()(x_col, y_col);
    return (shp + color) * T(0.5);
  }

 private:
  /** Shape kernel */
  MatKernelPtr k_shp_;
  /** Color kernel */
  MatKernelPtr k_col_;
};

template<typename T>
using JointMatKernelPtr = std::shared_ptr<JointMatrixKernel<3, 3, T>>;

template<typename T>
SamplerPtr<T> BuildXyzSampler(const Mesh_t<T>& m, const size_t& n_sample) {
  // Create sampler
  return std::make_shared<RandomMeshSampler<T>>(m, n_sample);
}

template<typename T>
SamplerPtr<T> BuildRgbSampler(const Mesh_t<T>& m, const size_t& n_sample) {
  // Create sampler
  return std::make_shared<RandomMeshColorSampler<T>>(m, n_sample);
}

/**
 * @name    BuildShapeKernel
 * @brief   Build Shape kernel (non-symmetric)
 * @return  Matrix kernel
 * @tparam T    Data type
 */
template<typename T>
KernelPtr<T> BuildStdKernel(T a, T A,
                            T b, T B,
                            T c, T C) {
  // Sigma_std = a Sigma_A + b Sigma_B + c Sigma_C
  // Kind of multi-scale kernel
  auto ka = GaussK<T>::Create(A) * a;
  auto kb = GaussK<T>::Create(B) * b;
  auto kc = GaussK<T>::Create(C) * c;
  auto k = ka + kb + kc;  // Sigma_0
  return k;
}

template<typename T>
KernelPtr<T> BuildRgbKernel(T d, T D) {
  auto kd = GaussK<T>::Create(D) * d;
  return kd;
}

template<typename T>
MatKernelPtr<T> MakeKernelCorrelated(const KernelPtr<T>& k,
                                     T factor) {
  return CorrelationKernel<3, 3, T>::Create(k, factor);
}


template<typename T>
void ApproxSimpleKernel(const std::string& name,
                        const DDomPtr<T>& reference,
                        const MatKernelPtr<T>& kernel,
                        const SamplerPtr<T>& sampler,
                        const std::string& filename,
                        int n_basis = 199) {
  // Approximate kernel
  // NOTE:
  //  - 1200 element approx 3Go
  //  - 1600 element approx 4Go
  //  - 3200 element approx 8Go
  LTS5_LOG_INFO("Approximate kernel: " << name);
  auto sampling_domain = UnstructDom<T>::Create(sampler->Sample());
  auto basis = NystromApproximation<3, 3, T>(reference,
                                             kernel,
                                             sampling_domain,
                                             n_basis);
  int err = basis.Save(filename);
  if (err) {
    LTS5_LOG_ERROR("Unable to save kernel approximation");
  }
}

template<typename T>
void ApproxJointKernel(const std::string& name,
                       const DDomPtr<T>& shp_domain,
                       const DDomPtr<T>& col_domain,
                       const JointMatKernelPtr<T>& kernel,
                       const std::string& filename) {
  using PointPair = std::pair<PointType<3, T>, PointType<3, T>>;
  using PointPairWithDim = std::pair<PointPair, int>;

  // Build vector with value where kernel is evaluated
  const auto& pts = shp_domain->get_points();
  const auto& colors = col_domain->get_points();
  assert(pts.size() == colors.size() && "Shape and Color must have same dims");
  std::vector<PointPairWithDim> pts_with_dims;
  for (size_t k = 0; k < pts.size(); ++k) {
    PointPair p = {pts[k], colors[k]};
    for (int d = 0; d < 3; ++d) {
      pts_with_dims.emplace_back(p, d);
    }
  }
  // Lambda function evaluating kernel
  auto fcn = [&](const PointPairWithDim& x,
                 const PointPairWithDim& y) -> T {
    return kernel->k(x.first, y.first)(x.second, y.second);
  };
  // Stopping criterion
  NumberOfIterationAndRelativeTolerance<T> sc(T(0.01), 3 * 1200);

  // Cholesky decomp
  LTS5_LOG_INFO("Approximate kernel: " << name);
  LTS5_LOG_DEBUG("Low Rank Cholesky Decomposition...");
  auto chol = ApproxCholeskyGeneric<T, PointPairWithDim>(fcn,
                                                         pts_with_dims,
                                                         sc);
  LTS5_LOG_DEBUG("Error: " << chol.error());
  // Compute eigen decomposition
  LTS5_LOG_DEBUG("Eigen vectors/values estimation");
  cv::Mat eigenvector, eigenvalues;
  chol.EigenDecomposition(&eigenvector, &eigenvalues);
  LTS5_LOG_DEBUG("Done");
  // Pack everything into DiscreteKLBasis object.
  // Set `col_domain` as reference since the joint kernel are used to model
  // Appearance even though some parts are based on shape!
  auto basis = DiscreteKLBasis<3, 3, T>(col_domain, eigenvector, eigenvalues);
  int err = basis.Save(filename);
  if (err) {
    LTS5_LOG_ERROR("Unable to save kernel approximation");
  }
}

template<typename T>
void Process(const CmdLineParser& parser) {


  std::string filename, output;
  parser.HasArgument("--mesh", &filename);
  parser.HasArgument("--output", &output);
  // Create destination folder
  auto fs = GetDefaultFileSystem();
  if (!fs->FileExist(output).Good()) {
    fs->CreateDirRecursively(output);
  }

  // Load mesh
  Mesh_t<T> ref_mesh;
  int err = ref_mesh.Load(filename);
  if (!err) {
    // Create shape/color domain
    auto shape_dom = UnstructDom<T>::Create(ref_mesh.get_vertex());
    auto color_dom = UnstructDom<T>::Create(ref_mesh.get_vertex_color());
    // Sampler
    auto xyz_sampler = BuildXyzSampler(ref_mesh, 500);
    auto rgb_sampler = BuildRgbSampler(ref_mesh, 500);
    //  Kernel
    auto Sigma0 = BuildStdKernel(T(7.0), T(100.0),
                                 T(5.0), T(50.0),
                                 T(3.0), T(10.0));
    auto SigmaRgb = BuildRgbKernel(T(0.015), T(0.15));
    auto SigmaXyz = BuildStdKernel(T(0.02), T(500.0),
                                   T(0.01), T(20.0),
                                   T(0.01), T(2.0));

    auto Ks = DiagK<T>::Create(Sigma0);
    auto Ks_sym = SymmK<T>::Create(Ks, T(0.7), 0);
    auto Ka_xyz = DiagK<T>::Create(SigmaXyz);
    auto Ka_rgb = DiagK<T>::Create(SigmaRgb);
    auto Ka = JointMatrixKernel<3, 3, T>::Create(Ka_xyz, Ka_rgb);
    auto Ka_xyz_cor = MakeKernelCorrelated(SigmaXyz, T(0.9375));
    auto Ka_rgb_sym = MakeKernelCorrelated(SigmaRgb, T(0.95));
    auto Ka_xyz_sym = SymColorCorrelated<3, 3, T>::Create(Ka_xyz_cor,
                                                          T(0.7),
                                                          0);
    auto Ka_sym = JointMatrixKernel<3, 3, T>::Create(Ka_xyz_sym,
                                                     Ka_rgb_sym);



    // Approximate shape kernel: Ks
    // --------------------------------------------------------
    ApproxSimpleKernel<T>("Ks",
                          shape_dom,
                          Ks,
                          xyz_sampler,
                          Path::Join(output, "ks.bin"));
    // Approximate shape kernel: Ks_sym
    // --------------------------------------------------------
    ApproxSimpleKernel<T>("Ks_sym",
                          shape_dom,
                          Ks_sym,
                          xyz_sampler,
                          Path::Join(output, "ks_sym.bin"));

    // Approximate color kernel: Ka_xyz
    // --------------------------------------------------------
    // Approximate color kernel: Approximate color based on geometry
    ApproxSimpleKernel<T>("Ka,xyz",
                          shape_dom,
                          Ka_xyz,
                          xyz_sampler,
                          Path::Join(output, "ka_xyz.bin"));

    // Approximate color kernel: Ka_rgb
    // --------------------------------------------------------
    // Approximate color kernel: In color space this time
    ApproxSimpleKernel<T>("Ka,rgb",
                          color_dom,
                          Ka_rgb,
                          rgb_sampler,
                          Path::Join(output, "ka_rgb.bin"));
    // Approximate color kernel: Ka_xyz_cor
    // --------------------------------------------------------
    ApproxSimpleKernel<T>("Ka,xyz_cor",
                          shape_dom,
                          Ka_xyz_cor,
                          xyz_sampler,
                          Path::Join(output, "ka_xyz_cor.bin"));

    // Approximate color kernel: Ka_rgb_sym
    // --------------------------------------------------------
    ApproxSimpleKernel<T>("Ka,rgb_sym",
                          color_dom,
                          Ka_rgb_sym,
                          rgb_sampler,
                          Path::Join(output, "ka_rgb_sym.bin"));
    // Approximate color kernel: Ka_xyz_sym
    // --------------------------------------------------------
    ApproxSimpleKernel<T>("Ka,xyz_sym",
                          shape_dom,
                          Ka_xyz_sym,
                          xyz_sampler,
                          Path::Join(output, "ka_xyz_sym.bin"));
#if 0
    // Approximate joint color kernel: Ka
    // --------------------------------------------------------
    ApproxJointKernel<T>("Ka",
                         shape_dom,
                         color_dom,
                         Ka,
                         Path::Join(output, "ka.bin"));
    // Approximate joint color kernel: Ka_sym
    // --------------------------------------------------------
    ApproxJointKernel<T>("Ka,sym",
                         shape_dom,
                         color_dom,
                         Ka_sym,
                         Path::Join(output, "ka_sym.bin"));
#endif
  } else {
    LTS5_LOG_ERROR("Unable to load reference mesh");
  }
}

int main(int argc, const char * argv[]) {
  CmdLineParser parser;
  parser.AddArgument("--mesh",
                     CmdLineParser::ArgState::kNeeded,
                     "Path to reference surface");
  parser.AddArgument("--output",
                     CmdLineParser::ArgState::kNeeded,
                     "Location where to write the data");
  // Parse
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    Process<double>(parser);
  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
}