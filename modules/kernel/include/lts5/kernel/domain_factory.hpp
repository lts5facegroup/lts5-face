/**
 *  @file   domain_factory.hpp
 *  @brief  Utility function to create domain by name/type
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   03/02/21
 *  Copyright (c) 2021 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_DOMAIN_FACTORY__
#define __LTS5_KERNEL_DOMAIN_FACTORY__

#include <functional>
#include <utility>
#include <vector>
#include <string>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/object_type.hpp"
#include "lts5/kernel/domain.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  DiscreteDomainProxy
 *  @brief  Proxy for registering/creating discrete domain
 *  @author Christophe Ecabert
 *  @date   03/02/21
 *  @ingroup kernel
 *  @tparam NDim    Number of dimension
 *  @tparam T       Data type
 */
template<int NDim, typename T>
class DiscreteDomainProxy {
 public:
  /** Discrete domain pointer */
  using DDomPtr = std::shared_ptr<DiscreteDomain<NDim, T>>;

  /**
   * @name  DiscreteDomainProxy
   * @fn    DiscreteDomainProxy()
   * @brief Constructor
   */
  DiscreteDomainProxy();

  /**
   *  @name   ~DiscreteDomainProxy
   *  @fn     virtual ~DiscreteDomainProxy() = default
   *  @brief  Destructor
   */
  virtual ~DiscreteDomainProxy() = default;

  /**
   *  @name   Create
   *  @fn     virtual DDomPtr Create() const = 0
   *  @brief  Create an instance of the DiscreteDomain attached to this proxy
   *  @return DiscreteDomain instance
   */
  virtual DDomPtr Create() const = 0;

  /**
   *  @name   Name
   *  @fn     virtual const char* Name() const = 0
   *  @brief  Name of the attached discrete domain.
   *  @return Domain's name
   */
  virtual const char* Name() const = 0;

  /**
   *  @name   Name
   *  @fn     virtual HeaderObjectType Type() const = 0
   *  @brief  Type of the attached discrete domain.
   *  @return Domain's type
   */
  virtual HeaderObjectType Type() const = 0;
};

/**
 * @class   DiscreteDomainFactory
 * @brief   Factory class to create DiscreteDomain instance based on name/type
 * @author  Christophe Ecabert
 * @date    03/02/2021
 * @ingroup kernel
 * @tparam NDim Number of dimensions {1, 2, 3, 4}
 * @tparam T    Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS DiscreteDomainFactory {
 public:
  /** Discrete Domain pointer type */
  using DDomPtr = std::shared_ptr<DiscreteDomain<NDim, T>>;

  /**
   * @name  Get
   * @fn    static DiscreteDomainFactory& Get()
   * @brief Accessor for singleton class
   * @return    Factory instance (singleton)
   */
  static DiscreteDomainFactory& Get();

  /**
   * @name  ~DiscreteDomainFactory
   * @fn    ~DiscreteDomainFactory() = default
   * @brief Destructor
   */
  ~DiscreteDomainFactory() = default;

  /**
   * @name  DiscreteDomainFactory
   * @fn    DiscreteDomainFactory(const DiscreteDomainFactory& other) = delete
   * @brief Copy constructor
   * @param[in] other   Object to copy from
   */
  DiscreteDomainFactory(const DiscreteDomainFactory& other) = delete;

  /**
   * @name  operator=
   * @fn    DiscreteDomainFactory& operator=(const DiscreteDomainFactory& rhs)
   *    = delete
   * @brief Assignement operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  DiscreteDomainFactory& operator=(const DiscreteDomainFactory& rhs) = delete;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Create
   * @fn    DDomPtr Create(const std::string& name) const
   * @brief Create a domain by name
   * @param[in] name    Name of the domain
   * @return    Newly created domain wrapped in shared pointer
   */
  DDomPtr Create(const std::string& name) const;

  /**
   * @name  Create
   * @fn    DDomPtr Create(const HeaderObjectType& type) const
   * @brief Create a domain by type
   * @param[in] type    Name of the domain
   * @return    Newly created domain wrapped in shared pointer
   */
  DDomPtr Create(const HeaderObjectType& type) const;

  /**
   * @name  Register
   * @fn    void Register(const DiscreteDomainProxy<NDim, T>* proxy)
   * @brief Register a new proxy for a domain
   * @param[in] proxy   Proxy to add to the factor
   */
  void Register(const DiscreteDomainProxy<NDim, T>* proxy);

 private:

  /**
   * @name  DiscreteDomainFactory
   * @fn    DiscreteDomainFactory() = default
   * @brief Constructor
   */
  DiscreteDomainFactory() = default;

  /** All registered proxy */
  std::vector<const DiscreteDomainProxy<NDim, T>*> proxies_;
};

#define DEFINE_DISCRETE_DOMAIN_PROXY(cls, name, otype)                    \
  template<int NDim, typename T>                                          \
  class cls##Proxy : public DiscreteDomainProxy<NDim, T> {                \
   public:                                                                \
    using DDomPtr = typename DiscreteDomainProxy<NDim, T>::DDomPtr;       \
    cls##Proxy() : DiscreteDomainProxy<NDim, T>() {}                      \
    ~cls##Proxy() override = default;                                     \
    DDomPtr Create() const override {                                     \
      return std::make_shared<cls<NDim, T>>();                            \
    }                                                                     \
    const char* Name() const override { return name; }                    \
    HeaderObjectType Type() const override {return otype; }               \
  };

#define DEFINE_SINGLE_DIMENSION_DISCRETE_DOMAIN_PROXY(cls, name, otype)   \
  template<int NDim, typename T>                                          \
  class cls##Proxy : public DiscreteDomainProxy<NDim, T> {                \
   public:                                                                \
    using DDomPtr = typename DiscreteDomainProxy<NDim, T>::DDomPtr;       \
    cls##Proxy() : DiscreteDomainProxy<NDim, T>() {}                      \
    ~cls##Proxy() override = default;                                     \
    DDomPtr Create() const override {                                     \
      return std::make_shared<cls<T>>();                                  \
    }                                                                     \
    const char* Name() const override { return name; }                    \
    HeaderObjectType Type() const override {return otype; }               \
  };

#define INSTANTIATE_DISCRETE_DOMAIN_PROXY(cls, ndim, type, suffix)        \
  static cls##Proxy<ndim, type> cls##Registrator##suffix;

#define REGISTER_DISCRETE_DOMAIN(cls, name, otype)                        \
  DEFINE_DISCRETE_DOMAIN_PROXY(cls, name, otype)                          \
  INSTANTIATE_DISCRETE_DOMAIN_PROXY(cls, 1, float, 1f)                    \
  INSTANTIATE_DISCRETE_DOMAIN_PROXY(cls, 2, float, 2f)                    \
  INSTANTIATE_DISCRETE_DOMAIN_PROXY(cls, 3, float, 3f)                    \
  INSTANTIATE_DISCRETE_DOMAIN_PROXY(cls, 4, float, 4f)                    \
  INSTANTIATE_DISCRETE_DOMAIN_PROXY(cls, 1, double, 1d)                   \
  INSTANTIATE_DISCRETE_DOMAIN_PROXY(cls, 2, double, 2d)                   \
  INSTANTIATE_DISCRETE_DOMAIN_PROXY(cls, 3, double, 3d)                   \
  INSTANTIATE_DISCRETE_DOMAIN_PROXY(cls, 4, double, 4d)

}  // namespace LTS5
#endif //__LTS5_KERNEL_DOMAIN_FACTORY__
