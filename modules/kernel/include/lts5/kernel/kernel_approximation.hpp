/**
 *  @file   kernel_approximation.hpp
 *  @brief  Perform low rank covariance kernel approximation using pivoted
 *      cholesky decomposition to estimate Eigen vectors and values.
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   07/01/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5__KERNEL_KERNEL_APPROXIMATION__
#define __LTS5__KERNEL_KERNEL_APPROXIMATION__

#include <memory>

#include "lts5/kernel/domain.hpp"
#include "lts5/kernel/matrix_kernel.hpp"
#include "lts5/kernel/kl_basis.hpp"
#include "lts5/kernel/sampler.hpp"
#include "lts5/kernel/pivoted_cholesky.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


/**
 * @name    LowRankKernelApproximation
 * @brief   Perform a low-rank approximation of the Gaussian process using a
 *  pivoted Cholesky approximation. This approximation will automatically
 *  compute the required number of basis functions, to achieve a given
 *  approximation quality.
 * @param[in] domain    The domain on which the approximation is performed.
 *  This can, for example be the points of a mesh or any other suitable domain.
 *  Note that the number of points in this domain influences the approximation
 *  accuracy. As the complexity of this method grows at most linearly with the
 *  number of points, efficient approximations can be computed for domains which
 *  contain  millions of points.
 * @param[in] cov
 * @param[in] relative_tolerance
 * @tparam IDim Number of input's dimension
 * @tparam ODim Number of output's dimension
 * @tparam T    Data type
 * @return  Approximated eigen basis.
 */
template<int IDim, int ODim, typename T>
DiscreteKLBasis<IDim, ODim, T>
LowRankKernelApproximation(const std::shared_ptr<DiscreteDomain<IDim, T>>& domain,
                           const std::shared_ptr<MatrixKernel<IDim, ODim, T>>& cov,
                           T relative_tolerance) {
  return LowRankKernelApproximation(domain, cov, relative_tolerance, -1);
}


template<int IDim, int ODim, typename T>
DiscreteKLBasis<IDim, ODim, T>
LowRankKernelApproximation(const std::shared_ptr<DiscreteDomain<IDim, T>>& domain,
                           const std::shared_ptr<MatrixKernel<IDim, ODim, T>>& cov,
                           T relative_tolerance,
                           int max_iter) {
  using MatK = MatrixKernel<IDim, ODim, T>;
  // Cholesky decomposition
  auto pts = domain->get_points();
  NumberOfIterationAndRelativeTolerance<T> sc(relative_tolerance, max_iter);
  LTS5_LOG_DEBUG("Low Rank Cholesky Decomposition...");
  auto chol = ApproxCholesky<T, IDim, ODim, MatK>(cov,
                                                  pts,
                                                  sc);
  LTS5_LOG_DEBUG("Error: " << chol.error());
  // Compute eigen decomposition
  LTS5_LOG_DEBUG("Eigen vectors/values estimation");
  cv::Mat eigenvector, eigenvalues;
  chol.EigenDecomposition(&eigenvector, &eigenvalues);
  LTS5_LOG_DEBUG("Done");
  // Pack everything into DiscreteKLBasis object.
  return DiscreteKLBasis<IDim, ODim, T>(domain, eigenvector, eigenvalues);
}


/**
 * @name
 * @brief   Computes the leading eigenvalues / eigenfunctions of the integral
 *  operator corresponding to kernel k. The number of leading eigenfunctions is
 *  at most n, where n is the number of points sampled. If the eigenvalues are
 *  decaying quickly, it can be much smaller than n.
 * @param[in] reference Reference shape for basis discretization
 * @param[in] cov       Matrix-valued kernel to approximate
 * @param[in] sampling_points   Domain, which holds the points that are
 *  used to compute the approximation (i.e. Usually randomly selected from
 *  reference domain).
 * @param[in] n_basis_function  If positive, select only a subset  of the
 *  approximated basis. If negative, return all estimated basis
 * @tparam IDim Number of input's dimension
 * @tparam ODim Number of output's dimension
 * @tparam T    Data type
 * @return  The leading eigenvalue / eigenfunction pairs
 */
template<int IDim, int ODim, typename T>
DiscreteKLBasis<IDim, ODim, T>
NystromApproximation(const std::shared_ptr<DiscreteDomain<IDim, T>>& reference,
                     const std::shared_ptr<MatrixKernel<IDim, ODim, T>>& cov,
                     const std::shared_ptr<DiscreteDomain<IDim, T>>& sampling_points,
                     int n_basis_function) {
  using LA = LinearAlgebra<T>;
  using TrType = typename LA::TransposeType;
  // procedure for the nystrom approximation as described in
  // Gaussian Processes for machine Learning (Rasmussen and Williamson)
  const auto& pts_nystrom = sampling_points->get_points();
  T n_nys_pts = static_cast<T>(pts_nystrom.size());
  // we compute the eigenvectors only approximately, to a tolerance of 1e-5. As
  // the nystrom approximation is anyway not exact, this should be sufficient
  // for all practical cases.
  auto chol_decomp = LowRankKernelApproximation<IDim, ODim, T>(sampling_points,
                                                               cov,
                                                               T(1e-5));
  const cv::Mat uMat = chol_decomp.eigen_function_matrix();
  const cv::Mat lambdaMat = chol_decomp.eigen_value_matrix();
  // Compute final eigen function/values pairs
  cv::Mat lambda = lambdaMat / n_nys_pts;
  size_t n_params = 0;
  for (int i = 0; i < lambda.rows; ++i) {
    if (lambda.at<T>(i) >= T(1e-8)) {
      n_params++;
    }
  }
  // Select subsect of basis if asked for
  if (n_basis_function > 0) {
    n_params = std::min(n_params, (size_t)n_basis_function);
    lambda = lambda(cv::Range(0, n_params), cv::Range::all()).clone();
  }
  // phi_i(x) \approx (sqrt(n_nys) / lambda_mat_i) * kX * u_i
  cv::Mat scaling(n_params, 1, cv::DataType<T>::type);
  for (int i = 0; i < n_params; ++i) {
    scaling.at<T>(i) = std::sqrt(n_nys_pts) * (T(1.0) / lambdaMat.at<T>(i));
  }
  cv::Mat W = uMat(cv::Range::all(),
                   cv::Range(0, n_params)).clone();
  W = W * cv::Mat::diag(scaling);
  // Discretize eigen function for a given reference
  const auto& ref_points = reference->get_points();
  cv::Mat eigen_func(ODim * ref_points.size(),
                     n_params,
                     cv::DataType<T>::type);

  auto n_thread = std::max(std::thread::hardware_concurrency(),
                           std::uint32_t(1));
  size_t chunk_size = (ref_points.size() + n_thread - 1) / n_thread;
  Parallel::For(n_thread,
                [&](const size_t& k) {
    size_t i_from = k * chunk_size;
    size_t i_to = std::min((k + 1) * chunk_size, ref_points.size());
    for (size_t i = i_from; i < i_to; ++i) {
      // Reference points
      const auto r_pts = ref_points[i];
      // Evaluate kx @ r_pts
      auto kX = cov->Vector(r_pts, pts_nystrom);  // [ODim, #NystromPts * IDim]
      // Compute phi
      cv::Mat phi_i;
      LA::Gemm(kX, TrType::kNoTranspose, T(1.0),
               W, TrType::kNoTranspose, T(0.0),
               &phi_i);
      cv::Rect eigen_func_block(0,i * ODim, n_params, ODim);
      phi_i.copyTo(eigen_func(eigen_func_block));
    }
  });

  for (size_t i = 0; i < ref_points.size(); ++i) {
    // Reference points
    const auto r_pts = ref_points[i];
    // Evaluate kx @ r_pts
    auto kX = cov->Vector(r_pts, pts_nystrom);  // [ODim, #NystromPts * IDim]
    // Compute phi
    cv::Mat phi_i;
    LA::Gemm(kX, TrType::kNoTranspose, T(1.0),
             W, TrType::kNoTranspose, T(0.0),
             &phi_i);
    cv::Rect eigen_func_block(0,i * ODim, n_params, ODim);
    phi_i.copyTo(eigen_func(eigen_func_block));
  }
  // Pack everything into DiscreteKLBasis
  return DiscreteKLBasis<IDim, ODim, T>(reference,
                                        eigen_func,
                                        lambda);
}

}  // namespace LTS5
#endif // __LTS5__KERNEL_KERNEL_APPROXIMATION__
