/**
 *  @file   field_interpolator.hpp
 *  @brief  Method to interpolates `DiscreteField` into continuous `Field`
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   23/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_FIELD_INTERPOLATOR__
#define __LTS5_KERNEL_FIELD_INTERPOLATOR__

#include <memory>

#include "lts5/utils/library_export.hpp"
#include "lts5/kernel/field.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   FieldInterpolator
 * @brief   Interpolator interface
 * @author  Christophe Ecabert
 * @date    23/12/2020
 * @ingroup kernel
 * @tparam IDim Number of input's dimension
 * @tparam ODim Number of output's dimension
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS FieldInterpolator {
 public:
  /** Continuous Field pointer */
  using FieldPtr = std::shared_ptr<Field<IDim, ODim, T>>;
  /** Discrete Field pointer */
  using DFieldPtr = std::shared_ptr<DiscreteField<IDim, ODim, T>>;

  /**
   * @name  ~FieldInterpolator
   * @fn    virtual ~FieldInterpolator() = default
   * @brief Destructor
   */
  virtual ~FieldInterpolator() = default;

  /**
   * @name  Interpolate
   * @fn    virtual FieldPtr Interpolate(const DFieldPtr& df) const = 0
   * @brief Interpolate a given `DiscreteField` instance
   * @param[in] df  Discrete field to make continuous
   * @return    Continuous field.
   */
  virtual FieldPtr Interpolate(const DFieldPtr& df) const = 0;
};


/**
 * @class   NearestNeighborInterpolator
 * @brief   Nearest neighbor interpolator for discrete field.
 * @author  Christophe Ecabert
 * @date    23/12/2020
 * @ingroup kernel
 * @tparam IDim Number of input's dimension
 * @tparam ODim Number of output's dimension
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS NearestNeighborInterpolator :
        public FieldInterpolator<IDim, ODim, T> {
 public:
  /** Continuous Field pointer */
  using FieldPtr = typename FieldInterpolator<IDim, ODim, T>::FieldPtr;
  /** Discrete Field pointer */
  using DFieldPtr = typename FieldInterpolator<IDim, ODim, T>::DFieldPtr;

  /**
   * @name NearestNeighborInterpolator
   * @fn    NearestNeighborInterpolator() = default
   * @brief Constructor
   */
  NearestNeighborInterpolator() = default;

  /**
   * @name ~NearestNeighborInterpolator
   * @fn    ~NearestNeighborInterpolator() = default
   * @brief Destructor
   */
  ~NearestNeighborInterpolator() = default;

  /**
   * @name  Interpolate
   * @fn    FieldPtr Interpolate(const DFieldPtr& df) const
   * @brief Interpolate a given `DiscreteField` instance
   * @param[in] df  Discrete field to make continuous
   * @return    Continuous field.
   */
  FieldPtr Interpolate(const DFieldPtr& df) const override;
};

/**
 * @class   NearestNeighborFieldFcn
 * @brief   Utility function to interpolate `DiscreteField`.
 * @author  Christophe Ecabert
 * @date    23/12/2020
 * @ingroup kernel
 * @tparam IDim Number of input's dimension
 * @tparam ODim Number of output's dimension
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS NearestNeighborFieldFcn : public FieldFunction<IDim, ODim, T> {
 public:
  /** Discrete field pointer */
  using DFieldPtr = std::shared_ptr<DiscreteField<IDim, ODim, T>>;

  /**
   * @name  NearestNeighborFieldFcn
   * @fn    explicit NearestNeighborFieldFcn(const DDomPtr& domain)
   * @brief Constructor
   * @param[in] df Descrete field
   */
  explicit NearestNeighborFieldFcn(const DFieldPtr& df) : dfield_(df) {
  }

  /**
   * @name  operator()
   * @fn    PointType<ODim, T> operator()(const PointType<IDim, T>& x)
   *    const override
   * @brief Find closest point defined in domain
   * @param[in] x Point to look for closest value.
   * @return    Interpolated value
   */
  PointType<ODim, T> operator()(const PointType<IDim, T>& x) const override {
    // Get domain and find closest point
    const auto& dom = dfield_->get_domain();
    auto closest_pt = dom->FindClosestPoint(x);
    // Get index and query value
    auto index = dom->Index(closest_pt);
    const auto& value = dfield_->get_data();
    return value[index];
  }

 private:
  /** Discrete field */
  DFieldPtr dfield_;
};



#pragma mark -
#pragma mark Implementation


template<int IDim, int ODim, typename T>
typename NearestNeighborInterpolator<IDim, ODim, T>::FieldPtr
NearestNeighborInterpolator<IDim, ODim, T>::
Interpolate(const DFieldPtr& df) const {
  using FieldFcn = NearestNeighborFieldFcn<IDim, ODim, T>;
  // Interpolating function
  auto continuous_fcn = std::make_shared<FieldFcn>(df);
  // New domain of definition -> Continuous
  auto continuous_domain = EuclideanSpace<IDim, T>::Create();
  // Create new continuous field
  return std::make_shared<Field<IDim, ODim, T>>(continuous_domain,
                                                continuous_fcn);
}





}  // namespace LTS5
#endif // __LTS5_KERNEL_FIELD_INTERPOLATOR__
