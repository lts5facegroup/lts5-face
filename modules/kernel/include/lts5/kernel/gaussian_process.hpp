/**
 *  @file   gaussian_process.hpp
 *  @brief  Continuous Gaussian Process
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   18/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_GAUSSIAN_PROCESS__
#define __LTS5_KERNEL_GAUSSIAN_PROCESS__

#include <memory>

#include "lts5/utils/library_export.hpp"
#include "lts5/kernel/domain.hpp"
#include "lts5/kernel/field.hpp"
#include "lts5/kernel/matrix_kernel.hpp"
#include "lts5/kernel/discrete_gaussian_process.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<int NDim, typename T>
using PointType = typename Point<NDim, T>::Type;


/**
 * @class   GaussianProcess
 * @brief   Continuous Gaussian Process
 * @author  Christophe Ecabert
 * @date    18/12/2020
 * @ingroup kernel
 * @tparam IDim Number of input's dimension
 * @tparam ODim Number of output's dimension
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS GaussianProcess {
 public:
  /** Domain pointer */
  using DomainPtr = std::shared_ptr<Domain<IDim, T>>;
  /** Field pointer */
  using FieldPtr = std::shared_ptr<Field<IDim, ODim, T>>;
  /** Covariance kernel matrix */
  using MatKernelPtr = std::shared_ptr<MatrixKernel<IDim, ODim, T>>;
  /** Input point type */
  using InPtType = PointType<IDim, T>;
  /** Output point type */
  using OutPtType = PointType<ODim, T>;
  /** Matrix type */
  using MatType = typename Matrix<ODim, T>::Type;
  /** Discrete Gaussian Process pointer */
  using DGPPtr = std::shared_ptr<DiscreteGaussianProcess<IDim, ODim, T>>;

  /**
   * @name  GaussianProcess
   * @fn    GaussianProcess(const FieldPtr& mean,
   *                        const MatKernelPtr& covariance)
   * @brief Constructor
   * @param[in] mean        Mean value
   * @param[in] covariance  Covariance kernel
   */
  GaussianProcess(const FieldPtr& mean, const MatKernelPtr& covariance);

  /**
   * @name  GaussianProcess
   * @fn    explicit GaussianProcess(const MatKernelPtr& covariance)
   * @brief Constructor, zero mean gaussian process
   * @param[in] covariance  Covariance kernel
   */
  explicit GaussianProcess(const MatKernelPtr& covariance);

  /**
   * @name  Mean
   * @fn    OutPtType Mean(const InPtType& x) const
   * @brief Evaluate mean value at a given point `x`
   * @param[in] x   Location where to evaluate mean
   * @return    Mean at `x`
   */
  OutPtType Mean(const InPtType& x) const;

  /**
   * @name  Covariance
   * @fn    MatType Covariance(const InPtType& x, const InPtType& y) const
   * @brief Evaluate covariance at a given point `x` and `y`
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    cov(x, y)
   */
  MatType Covariance(const InPtType& x, const InPtType& y) const;

  /**
   * @name  Marginal
   * @fn    DGPPtr Marginal(const std::vector<InPtType>& points)
   * @brief Compute the marginal distribution for the given points. The result
   *    is again a Gaussian process whose domain is an unstructured points
   *    domain.
   * @param[in] points  List of point to marginalize
   * @return   DiscreteGaussianProcess instance
   */
  DGPPtr Marginal(const std::vector<InPtType>& points);


 private:
  /** Domain of definition */
  DomainPtr dom_;
  /** Mean */
  FieldPtr mean_;
  /** Covariance */
  MatKernelPtr cov_;
};

#pragma mark -
#pragma mark Implementation

template<int IDim, int ODim, typename T>
GaussianProcess<IDim, ODim, T>::
GaussianProcess(const FieldPtr& mean, const MatKernelPtr& covariance) :
  dom_(DomainComposer<IDim, T>::Intersection(mean->domain(), covariance->domain())),
  mean_(mean),
  cov_(covariance) {
}

template<int IDim, int ODim, typename T>
GaussianProcess<IDim, ODim, T>::
GaussianProcess(const MatKernelPtr& covariance) :
  GaussianProcess(std::make_shared<ZeroVectorField<IDim, ODim, T>>(),
                  covariance) {
}

template<int IDim, int ODim, typename T>
typename GaussianProcess<IDim, ODim, T>::OutPtType
GaussianProcess<IDim, ODim, T>::Mean(const InPtType& x) const {
  return mean_->operator()(x);
}

template<int IDim, int ODim, typename T>
typename GaussianProcess<IDim, ODim, T>::MatType
GaussianProcess<IDim, ODim, T>::Covariance(const InPtType& x,
                                           const InPtType& y) const {
  return cov_->operator()(x, y);
}

template<int IDim, int ODim, typename T>
typename GaussianProcess<IDim, ODim, T>::DGPPtr
GaussianProcess<IDim, ODim, T>::
Marginal(const std::vector<InPtType>& points) {
  using DDomType = UnstructuredPointDomain<IDim, T>;
  using DMeanType = DiscreteField<IDim, ODim, T>;
  using DCovType = DiscretizeContinuousKernel<IDim, ODim, T>;
  using DiscreteGP = DiscreteGaussianProcess<IDim, ODim, T>;
  // Create domain
  auto domain = std::make_shared<DDomType>(points);
  // Mean field
  std::vector<OutPtType> mean_pts;
  for (const auto& pt : points) {
    mean_pts.emplace_back(this->Mean(pt));
  }
  auto mean = std::make_shared<DMeanType>(domain, mean_pts);
  // Covariance
  auto cov = std::make_shared<DCovType>(domain, this->cov_);
  // Create discrete GP
  return std::make_shared<DiscreteGP>(mean, cov);
}


}  // namespace LTS5
#endif // __LTS5_KERNEL_GAUSSIAN_PROCESS__
