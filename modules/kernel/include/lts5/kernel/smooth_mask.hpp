/**
 *  @file   numerics.hpp
 *  @brief  Smooth weighting mask defined on surSurface
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   10/02/21
 *  Copyright (c) 2021 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_SMOOTH_MASK__
#define __LTS5_KERNEL_SMOOTH_MASK__

#include <fstream>
#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/sys/cache.hpp"
#include "lts5/kernel/domain.hpp"
#include "lts5/kernel/kernel.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   SurfaceMaskInfo
 * @brief   Container to load mask info from file
 * @author  Christophe Ecabert
 * @date    10/02/2021
 * @ingroup kernel
 */
class LTS5_EXPORTS SurfaceMaskInfo {
 public:
  /**
   * @name  SurfaceMaskInfo
   * @fn    SurfaceMaskInfo() = default
   * @brief Constructor
   */
  SurfaceMaskInfo() = default;

  /**
   * @name  Load
   * @fn    int Load(const std::string& filename)
   * @brief Load masks from file
   * @param[in] filename  File storing the masks
   * @return    -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream)
   * @brief Load masks from binary stream
   * @param[in] stream  Binary streams from which to load the masks.
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream);

  /**
   * @name  semantic_mask
   * @fn    const std::vector<int>& semantic_mask() const
   * @brief Semantic mask accessor
   * @return    List of indicator
   */
  const std::vector<int>& semantic_mask() const {
    return semantic_mask_;
  }

  /**
   * @name  level_mask
   * @fn    const std::vector<int>& level_mask() const
   * @brief Level mask accessor
   * @return    List of indicator
   */
  const std::vector<int>& level_mask() const {
    return level_mask_;
  }

 private:
  /** Semantic mask */
  std::vector<int> semantic_mask_;
  /** Level mask */
  std::vector<int> level_mask_;
};

int SurfaceMaskInfo::Load(const std::string& filename) {
  int err = -1;
  std::ifstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Load(stream);
  }
  return err;
}

int SurfaceMaskInfo::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Number of points
    int n_pts;
    stream.read(reinterpret_cast<char*>(&n_pts), sizeof(n_pts));
    semantic_mask_.resize(n_pts);
    stream.read(reinterpret_cast<char*>(semantic_mask_.data()),
                n_pts * sizeof(int));
    stream.read(reinterpret_cast<char*>(&n_pts), sizeof(n_pts));
    level_mask_.resize(n_pts);
    stream.read(reinterpret_cast<char*>(level_mask_.data()),
                n_pts * sizeof(int));
    // Sanity check
    err = stream.good() ? 0 : -1;
  }
  return err;
}

/**
 * @class   SurfaceMask
 * @brief   Point weighting scheme based on facial region
 * @author  Christophe Ecabert
 * @date    10/02/2021
 * @ingroup kernel
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS SurfaceMask {
 public:
  /** Point Type */
  using PtType = typename Point<3, T>::Type;
  /** Discrete Domain pointer */
  using DDomPtr = std::shared_ptr<DiscreteDomain<3, T>>;

  /**
   * @name  SurfaceMask
   * @brief Constructor
   */
  SurfaceMask() = default;

  /**
   * @name  SurfaceMask
   * @fn    SurfaceMask(const std::vector<int>& level_mask,
   *    const std::vector<int>& semantic_mask)
   * @brief Constructor
   * @param[in] level_mask      List of level indicator, one for each vertex
   * @param[in] semantic_mask   List of semantic indicator, one for each vertex
   */
  SurfaceMask(const std::vector<int>& level_mask,
              const std::vector<int>& semantic_mask);

  /**
   * @name  BuildSmoothedRegion
   * @fn    int BuildSmoothedRegion(const DDomPtr& reference, int level,
   *    T stddev)
   * @brief Build internal structure for efficient smoothing factor query
   * @param[in] reference   Reference surface, must have the same size as mask
   * @param[in] level       Level
   * @param[in] stddev      Smoothing kernel's standard deviation
   * @return    -1 if error, 0 otherwise
   */
  int BuildSmoothedRegion(const DDomPtr& reference,
                          int level,
                          T stddev);

  /**
   * @name  operator()
   * @fn    T operator()(const PtType& x) const
   * @brief Returns a value in the interval [0,1] indicating whether a point
   *    belongs to the region
   * @param[in] x   Query point
   * @return    Factor
   */
  T operator()(const PtType& x) const;

  /**
   * @name  IsEarRegion
   * @fn    bool IsEarRegion(const size_t& idx) const
   * @brief Indicates if an given `idx` is part of the ear region
   * @param[in] idx Index of query point
   * @return    True if in ear region, False otherwise
   */
  bool IsEarRegion(const size_t& idx) const {
    return semantic_mask_[idx] == 1;
  }

  /**
   * @name  IsLipRegion
   * @fn    bool IsLipRegion(const size_t& idx) const
   * @brief Indicates if an given `idx` is part of the lip region
   * @param[in] idx Index of query point
   * @return    True if in lip region, False otherwise
   */
  bool IsLipRegion(const size_t& idx) const {
    return semantic_mask_[idx] == 2;
  }

  /**
   * @name  IsNoseRegion
   * @fn    bool IsNoseRegion(const size_t& idx) const
   * @brief Indicates if an given `idx` is part of the nose region
   * @param[in] idx Index of query point
   * @return    True if in nose region, False otherwise
   */
  bool IsNoseRegion(const size_t& idx) const {
    return semantic_mask_[idx] == 3;
  }

 private:
  /** Cache type */
  using CacheType = Cache<T(const PtType& x)>;
  /** Kernel pointer */
  using KernelPtr = std::shared_ptr<Kernel<3, T>>;
  /** Semantic mask */
  std::vector<int> semantic_mask_;
  /** Level mask */
  std::vector<int> level_mask_;
  /** Selected region */
  DDomPtr selected_region_;
  /** Label smoother */
  KernelPtr region_smoother_;
  /** Cache */
  std::unique_ptr<CacheType> cache_;
};

template<typename T>
SurfaceMask<T>::SurfaceMask(const std::vector<int>& level_mask,
                            const std::vector<int>& semantic_mask) :
        semantic_mask_(semantic_mask),
        level_mask_(level_mask) {
}

template<typename T>
int SurfaceMask<T>::BuildSmoothedRegion(const DDomPtr& reference,
                                        int level,
                                        T stddev) {
  using UnstructDom = UnstructuredPointDomain<3, T>;
  using GausKernel = GaussianKernel<3, T>;

  int err = -1;
  // #points in reference must be the same as len(semantic_mask_)
  const auto& ref_pts = reference->get_points();
  if (ref_pts.size() == semantic_mask_.size() &&
      ref_pts.size() == level_mask_.size()) {
    // Select points which belong to the selected level
    std::vector<PtType> pts_in_region;
    for (size_t k = 0; k < ref_pts.size(); ++k) {
      const auto& r_pts = ref_pts[k];
      const auto& lvl = level_mask_[k];
      if (lvl >= level) {
        pts_in_region.push_back(r_pts);
      }
    }
    selected_region_ = UnstructDom::Create(pts_in_region);
    region_smoother_ = GausKernel::Create(stddev);
    // use to pass by value, ok since shared_ptr!
    auto region = this->selected_region_;
    auto smoother = this->region_smoother_;
    cache_ = std::make_unique<CacheType>(pts_in_region.size(),
                                         [region, smoother](const PtType& x) {
                                           auto xc = region->FindClosestPoint(x);
                                           return smoother->operator()(xc, x);
                                         });
    err = 0;
  }
  return err;
}

template<typename T>
T SurfaceMask<T>::operator()(const PtType& x) const {
  return cache_->GetOrComputeValue(x);
}

}  // namespace LTS5
#endif // __LTS5_KERNEL_SMOOTH_MASK__
