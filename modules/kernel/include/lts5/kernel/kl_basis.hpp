/**
 *  @file   kl_basis.hpp
 *  @brief  Container for Karhunen-Loève expansion of the Gaussian process
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   06/01/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_KL_BASIS__
#define __LTS5_KERNEL_KL_BASIS__

#include <memory>
#include <cassert>
#include <fstream>

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/kernel/field.hpp"
#include "lts5/kernel/field_interpolator.hpp"
#include "lts5/kernel/domain_factory.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   EigenPair
 * @brief   Represent a continuous eigen pair (i.e. value + vector)
 * @author  Christophe Ecabert
 * @date    06/01/2020
 * @ingroup kernel
 * @tparam IDim     Number of input's dimension
 * @tparam ODim     Number of output's dimension
 * @tparam T        Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS EigenPair {
 public:
  /** Input point type */
  using InPtType = PointType<IDim, T>;
  /** Output point type */
  using OutPtType = PointType<ODim, T>;
  /** Field pointer */
  using FieldPtr = std::shared_ptr<Field<IDim, ODim, T>>;
  /** Domain pointer */
  using DomPtr = std::shared_ptr<Domain<IDim, T>>;

  /**
   * @name  EigenPair
   * @fn    EigenPair(const FieldPtr& eig_function, const T& eig_value)
   * @brief Constructor
   * @param[in] eig_function    Eigen function
   * @param[in] eig_value       Eigen value
   */
  EigenPair(const FieldPtr& eig_function,
            const T& eig_value) : eig_fun_(eig_function),
                                  eig_val_(eig_value) {
  }

  /**
   * @name  IsDefinedAt
   * @fn    bool IsDefinedAt(const PointType<IDim, T>& x) const
   * @brief Check if field is defined at a given location `x`.
   * @param[in] x   Point to check definition
   * @return    `true` if defined, `false` otherwise
   */
  bool IsDefinedAt(const InPtType& x) const {
    return eig_fun_->IsDefinedAt(x);
  }

  /**
   * @name  operator()
   * @fn    ResultT operator()(const PointType<IDim, T>& x) const
   * @brief Evaluate eigen function at location `x`
   * @param[in] x   Location where to evaluate the underlying functino.
   * @return Value
   */
  OutPtType operator()(const InPtType& x) const {
    return eig_fun_->operator()(x);
  }

  /**
   * @name  Function
   * @fn    const FieldPtr& Function() const
   * @brief Get the eigen function.
   * @return    Eigen function
   */
  const FieldPtr& Function() const {
    return eig_fun_;
  }

  /**
   * @name  Value
   * @fn    const T& Value() const
   * @brief Get the eigen value.
   * @return    Eigen value
   */
  const T& Value() const {
    return eig_val_;
  }

  /**
   * @name  domain
   * @fn    DomPtr domain() const
   * @brief Domain of definition of the function
   * @return    Domain
   */
  DomPtr domain() const {
    return eig_fun_->domain();
  }

 private:
  /** Eigen function */
  FieldPtr eig_fun_;
  /** Eigen value */
  T eig_val_;
};


/**
 * @class   DiscreteEigenPair
 * @brief   Represent a discrete eigen pair (i.e. value + vector)
 * @author  Christophe Ecabert
 * @date    06/01/2020
 * @ingroup kernel
 * @tparam IDim     Number of input's dimension
 * @tparam ODim     Number of output's dimension
 * @tparam T        Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS DiscreteEigenPair {
 public:
  /** Input point type */
  using InPtType = PointType<IDim, T>;
  /** Output point type */
  using OutPtType = PointType<ODim, T>;
  /** Discrete Field pointer */
  using DFieldPtr = std::shared_ptr<DiscreteField<IDim, ODim, T>>;
  /** EigenPair type */
  using EigPair = EigenPair<IDim, ODim, T>;
  /** Doscrete Domain pointer */
  using DDomPtr = std::shared_ptr<DiscreteDomain<IDim, T>>;
  /** Base interpolator type */
  using Interpolator = FieldInterpolator<IDim, ODim, T>;

  /**
   * @name  DiscreteEigenPair
   * @fn    DiscreteEigenPair(const DFieldPtr& eig_function, const T& eig_value)
   * @brief Constructor
   * @param[in] eig_function    Eigen function
   * @param[in] eig_value       Eigen value
   */
  DiscreteEigenPair(const DFieldPtr& eig_function,
                    const T& eig_value) : eig_fun_(eig_function),
                                          eig_val_(eig_value) {
  }

  /**
   * @name  IsDefinedAt
   * @fn    bool IsDefinedAt(const PointType<IDim, T>& x) const
   * @brief Check if field is defined at a given location `x`.
   * @param[in] x   Point to check definition
   * @return    `true` if defined, `false` otherwise
   */
  bool IsDefinedAt(const InPtType& x) const {
    return eig_fun_->IsDefinedAt(x);
  }

  /**
   * @name  operator()
   * @fn    ResultT operator()(const PointType<IDim, T>& x) const
   * @brief Evaluate eigen function at location `x`
   * @param[in] x   Location where to evaluate the underlying functino.
   * @return Value
   */
  OutPtType operator()(const InPtType& x) const {
    return eig_fun_->operator()(x);
  }

  /**
   * @name  Function
   * @fn    const DFieldPtr& Function() const
   * @brief Get the eigen function.
   * @return    Eigen function
   */
  const DFieldPtr& Function() const {
    return eig_fun_;
  }

  /**
   * @name  Value
   * @fn    const T& Value() const
   * @brief Get the eigen value.
   * @return    Eigen value
   */
  const T& Value() const {
    return eig_val_;
  }

  /**
   * @name  Interpolate
   * @fn    EigPair Interpolate(const Interpolator& interpolator) const
   * @brief Convert discrete eigen pair into a continuous one through
   *    interpolation
   * @param[in] interpolator Field interpolator instance
   * @return    Continuus
   */
  EigPair Interpolate(const Interpolator& interpolator) const;

  /**
   * @name  domain
   * @fn    DomPtr domain() const
   * @brief Domain of definition of the function
   * @return    Domain
   */
  DDomPtr domain() const {
    return eig_fun_->get_domain();
  }

 private:
  /** Eigen function */
  DFieldPtr eig_fun_;
  /** Eigen value */
  T eig_val_;
};


/**
 * @class   KLBasis
 * @brief   Container for EigenPair function.
 * @author  Christophe Ecabert
 * @date    06/01/2020
 * @ingroup kernel
 * @tparam IDim     Number of input's dimension
 * @tparam ODim     Number of output's dimension
 * @tparam T        Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS KLBasis {
 public:
  /** Eigen pair type */
  using EigPair = EigenPair<IDim, ODim, T>;
  /** Domain pointer */
  using DomPtr = std::shared_ptr<Domain<IDim, T>>;
  /** Pointer type */
  using Ptr = std::shared_ptr<KLBasis<IDim, ODim, T>>;

  /**
   * @name  Create
   * @fn    static Ptr Create()
   * @brief Create object wrapped in a shared_ptr.
   * @return    Shared instance
   */
  static Ptr Create() {
    using K = KLBasis<IDim, ODim, T>;
    return std::make_shared<K>();
  }

  /**
   * @name  Create
   * @fn    static Ptr Create(const std::vector<EigPair>& eig_pairs)
   * @brief Create object wrapped in a shared_ptr.
   * @param[in] eig_pairs List of eigen pair
   * @return    Shared instance
   */
  static Ptr Create(const std::vector<EigPair>& eig_pairs) {
    using K = KLBasis<IDim, ODim, T>;
    return std::make_shared<K>(eig_pairs);
  }

  /**
   * @name  KLBasis
   * @fn    KLBasis() = default
   * @brief Constructor
   */
  KLBasis() = default;

  /**
   * @name  KLBasis
   * @fn    explicit KLBasis(const std::vector<EigPair>& eig_pairs)
   * @brief Constructor
   * @param[in] eig_pairs   List of eigen pair
   */
  explicit KLBasis(const std::vector<EigPair>& eig_pairs) :
          eig_pairs_(eig_pairs) {
  }

  /**
   * @name  Insert
   * @fn    void Insert(const EigPair& eig_pair)
   * @brief Insert new pair of eigen function/value in the basis
   * @param[in] eig_pair    Eigen pair
   */
  void Insert(const EigPair& eig_pair) {
    eig_pairs_.push_back(eig_pair);
  }

  /**
   * @name  Size
   * @fn    size_t Size() const
   * @brief Indicate the number of eigen basis stored
   * @return    Number
   */
  size_t Size() const {
    return eig_pairs_.size();
  }

  /**
   * @name  operator[]
   * @fn    const EigPair& operator[](const size_t& index) const
   * @brief Access a specific eigen pair in the basis
   * @param[in] index   Index to access
   * @return    Eigen pair at given index.
   */
  const EigPair& operator[](const size_t& index) const {
    return eig_pairs_[index];
  }

  /**
   * @name  domain
   * @fn    DomPtr domain() const
   * @brief Domain of definition of the basis
   * @return    Domain
   */
  DomPtr domain() const {
    if (!eig_pairs_.empty()) {
      return eig_pairs_.front().domain();
    } else {
      return EuclideanSpace<IDim, T>::Create();
    }
  }

 private:
  /** Eigen pairs */
  std::vector<EigenPair<IDim, ODim, T>> eig_pairs_;
};


/**
 * @class   DiscreteKLBasis
 * @brief   Container for DiscreteEigenPair function.
 * @author  Christophe Ecabert
 * @date    06/01/2020
 * @ingroup kernel
 * @tparam IDim     Number of input's dimension
 * @tparam ODim     Number of output's dimension
 * @tparam T        Data type
 */
template<int IDim, int ODim, typename T>
class DiscreteKLBasis {
 public:
  /** Discrete Eigen Pair type */
  using DEigPair = DiscreteEigenPair<IDim, ODim, T>;
  /** Discrete domain pointer */
  using DDomPtr = std::shared_ptr<DiscreteDomain<IDim, T>>;
  /** Continuous basis pointer */
  using CKLBasisPtr = std::shared_ptr<KLBasis<IDim, ODim, T>>;
  /** Interpolator */
  using Interpolator = FieldInterpolator<IDim, ODim, T>;

  /**
   * @name  DiscreteKLBasis
   * @fn    DiscreteKLBasis() = default
   * @brief Constructor
   */
  DiscreteKLBasis() = default;

  /**
   * @name  DiscreteKLBasis
   * @fn    Dexplicit DiscreteKLBasis(const std::vector<DEigPair>& eig_pair)
   * @brief Constructor
   * @param[in] eig_pair    List of discrete eigen pairs forming the basis.
   */
  explicit DiscreteKLBasis(const std::vector<DEigPair>& eig_pair);

  /**
   * @name  DiscreteKLBasis
   * @fn    DiscreteKLBasis(const DDomPtr& domain, const cv::Mat& eig_func,
                            const cv::Mat& eig_vals)
   * @brief Constructor
   * @param[in] domain  Domain of definition of the basis
   * @param[in] eig_func Eigen function stored in each column
   * @param[in] eig_vals Eigen values stored on each row
   */
  DiscreteKLBasis(const DDomPtr& domain,
                  const cv::Mat& eig_func,
                  const cv::Mat& eig_vals);

  /**
   * @name  Load
   * @fn    int Load(const std::string& filename)
   * @brief Load from a file
   * @param filename Path to the file location
   * @return -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream)
   * @brief Load from a binary stream
   * @param[in] stream  Binary stream
   * @return -1 if error, 0 otherwise
   */
  int Load(std::istream& stream);

  /**
   * @name  Save
   * @fn    int Save(const std::string& filename) const
   * @brief Save the object into a file
   * @param[in] filename    File where to dump the object
   * @return -1 if error, 0 otherwise
   */
  int Save(const std::string& filename) const;

  /**
   * @name  Save
   * @fn    int Save(std::ostream& stream) const
   * @brief Save the object into a binary stream
   * @param[in] stream    Stream where to dump the object
   * @return -1 if error, 0 otherwise
   */
  int Save(std::ostream& stream) const;

  /**
   * @name  ObjectSize
   * @fn    int ObjectSize() const
   * @brief Compute object size in bytes
   * @return    Object size in bytes
   */
  int ObjectSize() const;

  /**
   * @name  Insert
   * @fn    void Insert(const DEigPair& eig_pair)
   * @brief Insert new pair of eigen function/value in the basis
   * @param[in] eig_pair    Eigen pair
   */
  void Insert(const DEigPair& eig_pair);

  /**
   * @name  Size
   * @fn    size_t Size() const
   * @brief Indicate the number of eigen basis stored
   * @return    Number
   */
  size_t Size() const {
    return eig_pairs_.size();
  }

  /**
   * @name  Interpolate
   * @fn    CKLBasisPtr Interpolate(const Interpolator& interpolator) const
   * @brief Convert discrete basis into continuous one through interpolation.
   * @param[in] interpolator
   * @return Continuous KLBasis object.
   */
  CKLBasisPtr Interpolate(const Interpolator& interpolator) const;

  /**
   * @name  Sample
   * @fn    void Sample(cv::Mat* sample) const
   * @brief Generate random sample from the underlying basis
   * @param[out] sample Generated sample
   */
  void Sample(cv::Mat* sample) const;

  /**
   * @name  Instance
   * @fn    void Instance(const cv::Mat& coefficients, cv::Mat* instance) const
   * @brief Generate an instance of the model from a given set of coefficients
   * @param[in] coefficients    Basis coefficients, considered normally distributed
   * @param[out] instance       Generated instance
   */
  void Instance(const cv::Mat& coefficients, cv::Mat* instance) const;

  /**
   * @name  Truncate
   * @fn    void Truncate(const size_t& n)
   * @brief Reduce basis dimensions by keeping `n` components.
   * @param[in] n   Number of basis to keep
   */
  void Truncate(const size_t& n);

  /**
   * @name  operator[]
   * @fn    const DEigPair& operator[](const size_t& index) const
   * @brief Access a specific eigen pair in the basis
   * @param[in] index   Index to access
   * @return    Eigen pair at given index.
   */
  const DEigPair& operator[](const size_t& index) const {
    return eig_pairs_[index];
  }

  /**
   * @name  eigen_function_matrix
   * @fn    const cv::Mat& eigen_function_matrix() const
   * @brief Access eigen function matrix (i.e. stored as column)
   * @return    Matrix
   */
  const cv::Mat& eigen_function_matrix() const {
    return eig_func_matrix_;
  }

  /**
   * @name  eigen_value_matrix
   * @fn    const cv::Mat& eigen_value_matrix() const
   * @brief Access eigen value matrix (i.e. stored as column vector)
   * @return    Matrix
   */
  const cv::Mat& eigen_value_matrix() const {
    return eig_val_matrix_;
  }

 private:
  /** Discrete domain pointer */
  using DDomRaw = DiscreteDomain<IDim, T>;

  /**
   * @name  Init
   * @brief Initalize internal structure
   * @param[in] eig_pair    List of eigen pairs
   */
  void Init(const std::vector<DEigPair>& eig_pair);

  /**
   * @name  ToMatrix
   * @brief Convert discrete eigen pair into matrix form
   * @param[in] pair    Eigen pair to convert
   * @param[out] eig_func   Eigen function as matrix (column vector)
   * @param[out] eig_val    Eigen function as matrix (1x1 matrix)
   */
  void ToMatrix(const DEigPair& pair, cv::Mat* eig_func, cv::Mat* eig_val);

  /**
   * @name  FromMatrix
   * @brief Convert list of eigen pairs in matrix format into vector of
   *    `DiscreteEigenPair` object
   * @param[in] domain      Domain of definition of the basis
   * @param[in] eig_funcs   Eigen function basis
   * @param[in] eig_vals    Eigen value
   * @param[out] pair       List of DiscreteEigenPair object
   */
  void FromMatrix(const DDomPtr& domain,
                  const cv::Mat& eig_funcs,
                  const cv::Mat& eig_vals,
                  std::vector<DEigPair>* pair);

  /** Eigen pairs */
  std::vector<DEigPair> eig_pairs_;
  /** Reference to discrete domain */
  const DDomRaw* dom_ptr_;
  /** Eigen function as matrix */
  cv::Mat eig_func_matrix_;
  /** Eigen values as matrix (column vector)*/
  cv::Mat eig_val_matrix_;
};



#pragma mark -
#pragma mark Implementation

template<int IDim, int ODim, typename T>
typename DiscreteEigenPair<IDim, ODim, T>::EigPair
DiscreteEigenPair<IDim, ODim, T>::
Interpolate(const Interpolator& interpolator) const {
  // Interpolate eigen function
  auto continuous_eig_fun = interpolator.Interpolate(this->eig_fun_);
  // Create continuous eigenpair
  return EigPair(continuous_eig_fun, this->eig_val_);
}

template<int IDim, int ODim, typename T>
DiscreteKLBasis<IDim, ODim, T>::
DiscreteKLBasis(const std::vector<DEigPair>& eig_pair) : eig_pairs_(eig_pair),
                                                         dom_ptr_(nullptr),
                                                         eig_func_matrix_(),
                                                         eig_val_matrix_() {
  this->Init(eig_pair);
}

template<int IDim, int ODim, typename T>
DiscreteKLBasis<IDim, ODim, T>::
DiscreteKLBasis(const DDomPtr& domain,
                const cv::Mat& eig_func,
                const cv::Mat& eig_vals) : eig_pairs_(),
                                           dom_ptr_(domain.get()),
                                           eig_func_matrix_(eig_func),
                                           eig_val_matrix_(eig_vals) {
  FromMatrix(domain, eig_func, eig_vals, &eig_pairs_);
}

template<int IDim, int ODim, typename T>
int DiscreteKLBasis<IDim, ODim, T>::Load(const std::string& filename) {
  int err = -1;
  std::ifstream stream(filename.c_str(),
                       std::ios_base::in|std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Load(stream);
  }
  return err;
}

template<int IDim, int ODim, typename T>
int DiscreteKLBasis<IDim, ODim, T>::Load(std::istream& stream) {
  using DFieldPtr = DiscreteField<IDim, ODim, T>;
  using Factory = DiscreteDomainFactory<IDim, T>;

  int err = -1;
  if (stream.good()) {
    // Search for header
    err = ScanStreamForObject(stream,
                              HeaderObjectType::kDiscreteKLBasis);
    if (!err) {
      // Read matrix
      err = ReadMatFromBin(stream, &eig_val_matrix_);
      err |= ReadMatFromBin(stream, &eig_func_matrix_);
      // Get type of domain => Peek at stream entry to get type of domain
      auto stream_pos = stream.tellg();
      HeaderObjectType dom_type;
      stream.read(reinterpret_cast<char*>(&dom_type), sizeof(dom_type));
      stream.seekg(stream_pos);   // Roll back to original place
      // Create domain and load
      auto domain = Factory::Get().Create(dom_type);
      err |= domain->Load(stream);
      dom_ptr_ = domain.get();
      // Init output
      eig_pairs_.clear();
      for (int k = 0; k < eig_func_matrix_.cols; ++k) {
        auto dfield = DFieldPtr::Create(domain,
                                        eig_func_matrix_.col(k).clone());
        eig_pairs_.emplace_back(dfield, eig_val_matrix_.at<T>(k));
      }
      // Sanity check
      err |= stream.good() ? 0 : -1;
    }
  }
  return err;
}

template<int IDim, int ODim, typename T>
int DiscreteKLBasis<IDim, ODim, T>::Save(const std::string& filename) const {
  int err = -1;
  std::ofstream stream(filename.c_str(),
                       std::ios_base::out|std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Save(stream);
  }
  return err;
}

template<int IDim, int ODim, typename T>
int DiscreteKLBasis<IDim, ODim, T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Write header
    HeaderObjectType type = HeaderObjectType::kDiscreteKLBasis;
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    // Object size
    int sz = this->ObjectSize();
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Dump matrix
    err = WriteMatToBin(stream, eig_val_matrix_);
    err |= WriteMatToBin(stream, eig_func_matrix_);
    // Assume all pairs shared the same domain!
    err |= eig_pairs_[0].domain()->Save(stream);
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

template<int IDim, int ODim, typename T>
int DiscreteKLBasis<IDim, ODim, T>::ObjectSize() const {
  int sz = sizeof(int); // #Pairs
  sz += 2 * (3 * sizeof(int));  // 2 Matrix header
  sz += eig_val_matrix_.total() * eig_val_matrix_.elemSize();
  sz += eig_func_matrix_.total() * eig_func_matrix_.elemSize();
  sz += eig_pairs_[0].domain()->ObjectSize() + sizeof(int);
  return sz;
}

template<int IDim, int ODim, typename T>
void DiscreteKLBasis<IDim, ODim, T>::
Init(const std::vector<DEigPair>& eig_pair) {
  // Initialize matrix representation
  eig_val_matrix_.create(0 , 0, cv::DataType<T>::type);
  eig_func_matrix_.create(0 , 0, cv::DataType<T>::type);
  std::vector<cv::Mat> eig_funcs;
  for (const auto& p : eig_pair) {
    if (dom_ptr_ == nullptr) {
      dom_ptr_ = p.domain().get();
    }
    cv::Mat eig_fun, eig_val;
    ToMatrix(p, &eig_fun, eig_val);
    eig_val_matrix_.push_back(eig_val);
    eig_funcs.push_back(eig_fun);
  }
  cv::hconcat(eig_funcs, eig_func_matrix_);
}

template<int IDim, int ODim, typename T>
void DiscreteKLBasis<IDim, ODim, T>::ToMatrix(const DEigPair& pair,
                                              cv::Mat* eig_func,
                                              cv::Mat* eig_val) {
  using OutPtType = PointType<ODim, T>;
  // Init container
  int n_elem = pair.domain()->Size();
  eig_func->create(n_elem * ODim, 1, cv::DataType<T>::type);
  eig_val->create(1, 1, cv::DataType<T>::type);
  // Set eigen value
  eig_val->at<T>(0) = pair.Value();
  // Set eigen function
  auto fcn = pair.Function();
  for (int k = 0; k < n_elem; ++k) {
    eig_func->at<OutPtType>(k) = fcn[k];
  }
}

template<int IDim, int ODim, typename T>
void DiscreteKLBasis<IDim, ODim, T>::FromMatrix(const DDomPtr& domain,
                                                const cv::Mat& eig_funcs,
                                                const cv::Mat& eig_vals,
                                                std::vector<DEigPair>* pair) {
  using DFieldPtr = DiscreteField<IDim, ODim, T>;

  assert(eig_funcs.cols == eig_vals.rows &&
         "Mismatch between number of eigen function and values!");
  assert(eig_funcs.rows % ODim == 0 &&
         "Eigen function does not match the output dimension!");
  // Init output
  pair->clear();
  for (int k = 0; k < eig_funcs.cols; ++k) {
    pair->emplace_back(DFieldPtr::Create(domain,
                                         eig_funcs.col(k).clone()),
                       eig_vals.at<T>(k));
  }
}

template<int IDim, int ODim, typename T>
void DiscreteKLBasis<IDim, ODim, T>::Insert(const DEigPair& eig_pair) {
  // First insertion ?
  if (eig_pairs_.empty()) {
    dom_ptr_ = eig_pair.domain().get();
  }
  eig_pairs_.push_back(eig_pair);
  // Add to matrix form as well.
  cv::Mat eig_func, eig_val;
  ToMatrix(eig_pair, &eig_func, &eig_val);
  std::vector<cv::Mat> inputs{eig_func_matrix_, eig_func};
  cv::hconcat(inputs, eig_func_matrix_);
  eig_val_matrix_.push_back(eig_val);
}

template<int IDim, int ODim, typename T>
typename DiscreteKLBasis<IDim, ODim, T>::CKLBasisPtr
DiscreteKLBasis<IDim, ODim, T>::
Interpolate(const Interpolator& interpolator) const {
  using EPair = EigenPair<IDim, ODim, T>;
  auto basis = KLBasis<IDim, ODim, T>::Create();
  for (const auto& p : eig_pairs_) {
    // Interpolate eigen function
    auto cfield = interpolator.Interpolate(p.Function());
    auto value = p.Value();
    // Create continuous
    basis->Insert(EPair(cfield, value));
  }
  return basis;
}

template<int IDim, int ODim, typename T>
void DiscreteKLBasis<IDim, ODim, T>::Sample(cv::Mat* sample) const {
  // Draw random coefficients from N(0, 1)
  cv::Mat coef(eig_func_matrix_.cols, 1, cv::DataType<T>::type);
  cv::randn(coef, T(0.0), T(1.0));
  // Generate intance
  this->Instance(coef, sample);
}

template<int IDim, int ODim, typename T>
void DiscreteKLBasis<IDim, ODim, T>::Instance(const cv::Mat& coefficients,
                                              cv::Mat* instance) const {
  using LA = LinearAlgebra<T>;
  using Tr = typename LA::TransposeType;
  assert(coefficients.rows == eig_func_matrix_.cols &&
         "Number of coefficients does not match the number of basis");
  // I = ref + basis @ diag(eigvalue**0.5) @ coef
  const auto& pts = dom_ptr_->get_points();
  instance->create(eig_func_matrix_.rows, 1, cv::DataType<T>::type);
  // Scale coeffient by stddev (sqrt of eigenvalues)
  cv::Mat coef_scaled(coefficients.rows, 1, cv::DataType<T>::type);
  for (int i = 0; i < coefficients.rows; ++i) {
    coef_scaled.at<T>(i) = (coefficients.at<T>(i) *
                            std::sqrt(eig_val_matrix_.at<T>(i)));
  }
  // Copy ref to destination
  std::copy(reinterpret_cast<const T*>(pts.data()),
            reinterpret_cast<const T*>(pts.data()) + pts.size() * IDim,
            reinterpret_cast<T*>(instance->data));
  // Add product basis @ coef
  LA::Gemv(eig_func_matrix_, Tr::kNoTranspose, T(1.0), coef_scaled,
           T(1.0), instance);
}

template<int IDim, int ODim, typename T>
void DiscreteKLBasis<IDim, ODim, T>::Truncate(const size_t& n) {
  // Resize vectors
  // At least as large as current number of basis
  size_t nb = std::min(n, eig_pairs_.size());
  eig_pairs_.erase(eig_pairs_.begin() + nb, eig_pairs_.end());
  // Shrink matrices
  eig_val_matrix_.resize(n);  // Change number of rows
  eig_func_matrix_ = eig_func_matrix_(cv::Range::all(),
                                      cv::Range(0, n)).clone();
}

}  // namespace LTS5
#endif // __LTS5_KERNEL_KL_BASIS__
