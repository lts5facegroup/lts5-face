/**
 *  @file   sampler.hpp
 *  @brief  Random sampling scheme
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   17/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_SAMPLER__
#define __LTS5_KERNEL_SAMPLER__

#include <random>
#include <chrono>
#include <vector>
#include <algorithm>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/kernel/domain.hpp"
#include "lts5/geometry/mesh.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<int NDim, typename T>
using PointType = typename Point<NDim, T>::Type;

/**
 * @name    seed
 * @brief   Generate seed based on time
 * @return  Seed
 */
static unsigned seed() {
  return std::chrono::system_clock::now().time_since_epoch().count();
}

#pragma mark -
#pragma mark Sampler Interface

/**
 * @class   Sampler
 * @brief   Random sampler interface
 * @author  Christophe Ecabert
 * @date    17/12/2020
 * @ingroup kernel
 * @tparam  NDim    Number dimensions {1, 2, 3, 4}
 * @tparam T        Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS Sampler {
 public:
  /** Point Type */
  using PtType = PointType<NDim, T>;

  /**
   * @struct    PointWithProb
   * @brief Container for point with their probability
   */
  struct PointWithProb {
    /**
     * @name    PointWithProb
     * @brief   Constructor
     * @param[in] point         Point
     * @param[in] probability   Probability
     */
    PointWithProb(const PtType& point,
                  const T& probability) : pts(point),
                                          prob(probability) {}

    /** Point */
    PtType pts;
    /** Probability */
    T prob;
  };

  /**
   * @name  Sampler
   * @fn    Sampler(size_t n_sample, unsigned seed)
   * @brief Constructor
   * @param[in] n_sample    Number of sample to generates each times
   * @param[in] seed    Seed to initialize random generator
   */
  Sampler(size_t n_sample, unsigned seed) : n_sample_(n_sample),
                                            gen_(seed) {
  }

  /**
   * @name  Sampler
   * @fn    explicit Sampler(size_t n_sample)
   * @brief Constructor
   * @param[in] n_sample    Number of sample to generate
   */
  explicit Sampler(size_t n_sample) : Sampler(n_sample, seed()) {}

  /**
   * @name  ~Sampler
   * @fn    virtual ~Sampler() = default
   * @brief Destructor
   */
  virtual ~Sampler() = default;

  /**
   * @name  Sample
   * @fn    std::vector<PtType> Sample()
   * @brief Generate Random sample
   * @return    List of random samples
   */
  std::vector<PtType> Sample() {
    std::vector<PtType> samples;
    for (size_t i = 0; i < n_sample_; ++i) {
      PtType pts;
      T prob;
      // Sample
      this->operator()(&pts, &prob);
      samples.push_back(pts);
    }
    return samples;
  }

  /**
   * @name  SampleWithProbability
   * @fn    std::vector<PointWithProb> SampleWithProbability()
   * @brief Generate Random sample
   * @return    List of random samples with their probability
   */
  std::vector<PointWithProb> SampleWithProbability() {
    std::vector<PointWithProb> samples;
    for (size_t i = 0; i < n_sample_; ++i) {
      PtType pts;
      T prob;
      // Sample
      this->operator()(&pts, &prob);
      samples.emplace_back(pts, prob);
    }
    return samples;
  }

 protected:
  /**
   * @name  operator()
   * @fn    virtual void operator()(PtType* sample, T* prob) = 0
   * @brief Actual sampling function. Must be implemented by subclass.
   * @param[out] sample Generated sample
   * @param[out] prob   Porbability
   */
  virtual void operator()(PtType* sample, T* prob) = 0;

  /** Number of samples generated each time */
  size_t n_sample_;
  /** Random generator */
  std::mt19937 gen_;
};

#pragma mark -
#pragma mark Uniform Sampler

/**
 * @class   UniformSampler
 * @brief   Randomly samples point into a Nd cube.
 * @author  Christophe Ecabert
 * @date    17/12/2020
 * @ingroup kernel
 * @tparam NDim Number of dimensions
 * @tparam T    Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS UniformSampler : public Sampler<NDim, T> {
 public:
  /** Domain type */
  using BoxDomType = std::shared_ptr<BoxDomain<NDim, T>>;
  /** Point type */
  using PtType = PointType<NDim, T>;

  /**
   * @name  UniformSampler
   * @fn    UniformSampler(const BoxDomType& domain, const size_t& n_sample,
                 const unsigned& seed)
   * @brief Sample uniformly from a given box.
   * @param[in] domain      Region where to sample
   * @param[in] n_sample    Number of sample to generate each time
   * @param[in] seed        Random generator's seed.
   */
  UniformSampler(const BoxDomType& domain,
                 const size_t& n_sample,
                 const unsigned& seed);

  /**
   * @name  UniformSampler
   * @fn    UniformSampler(const BoxDomType& domain, const size_t& n_sample)
   * @brief Sample uniformly from a given box.
   * @param[in] domain      Region where to sample
   * @param[in] n_sample    Number of sample to generate each time
   */
  UniformSampler(const BoxDomType& domain,
                 const size_t& n_sample);

  /**
   * @name  ~UniformSampler()
   * @fn    ~UniformSampler() = default
   * @brief Destructor
   */
  ~UniformSampler() = default;

 protected:
  /**
   * @name  operator()
   * @fn    void operator()(PtType* sample, T* prob) override
   * @brief Actual sampling function.
   * @param[out] sample Generated sample
   * @param[out] prob   Porbability
   */
  void operator()(PtType* sample, T* prob) override;

 private:
  /** Domain of sampling */
  BoxDomType dom_;
  /** Distribution */
  std::uniform_real_distribution<T> dist_;
  /** Probability */
  T prob_;
  /** Bias */
  PtType bias_;
  /** Scale */
  PtType scale_;
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<int NDim, typename T>
UniformSampler<NDim, T>::UniformSampler(const BoxDomType& domain,
                                        const size_t& n_sample,
                                        const unsigned& seed) :
        Sampler<NDim, T>(n_sample, seed),
        dom_(domain) {
  // Prob
  prob_ = T(1.0) / dom_->Volume();
  // Bias + scale
  bias_ = dom_->get_origin();
  scale_ = dom_->Extent();
}

template<int NDim, typename T>
UniformSampler<NDim, T>::UniformSampler(const BoxDomType& domain,
                                        const size_t& n_sample) :
        UniformSampler(domain, n_sample, seed()) {
}

template<int NDim, typename T>
void UniformSampler<NDim, T>::operator()(PtType* sample, T* prob) {
  *prob = this->prob_;
  for (int i = 0; i < NDim; ++i) {
    T value = dist_(this->gen_);
    (*sample)[i] = scale_[i] * value + bias_[i];
  }
}

#endif  // DOXYGEN_SHOULD_SKIP_THIS

#pragma mark -
#pragma mark Random Mesh Sampler

/**
 * @class   RandomMeshSampler
 * @brief   Randomly select points from a given mesh.
 * @author  Christophe Ecabert
 * @date    17/12/2020
 * @ingroup kernel
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS RandomMeshSampler : public Sampler<3, T> {
 public:
  /** Point type */
  using PtType = PointType<3, T>;
  /** Mesh type */
  using Mesh_t = Mesh<T, Vector3<T>>;

  /**
   * @name  RandomMeshSampler
   * @fn    RandomMeshSampler(const Mesh_t& mesh, const size_t& n_sample,
                              const unsigned& seed)
   * @brief Constructor
   * @param[in] mesh        Mesh
   * @param[in] n_sample    Number of point to sample at each time
   * @param[in] seed        Random generator's seed.
   */
  RandomMeshSampler(const Mesh_t& mesh,
                    const size_t& n_sample,
                    const unsigned& seed);

  /**
   * @name  RandomMeshSampler
   * @fn    RandomMeshSampler(const Mesh_t& mesh, const size_t& n_sample)
   * @brief Constructor
   * @param[in] mesh        Mesh
   * @param[in] n_sample    Number of point to sample at each time
   */
  RandomMeshSampler(const Mesh_t& mesh,
                    const size_t& n_sample);

 protected:
  /**
   * @name  operator()
   * @fn    void operator()(PtType* sample, T* prob) override
   * @brief Actual sampling function.
   * @param[out] sample Generated sample
   * @param[out] prob   Porbability
   */
  void operator()(PtType* sample, T* prob) override;

 private:
  /** Mesh's points */
  std::vector<PtType> points_;
  /** Sampling distribution */
  std::uniform_int_distribution<size_t> dist_;
  /** probability */
  T prob_;
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<typename T>
RandomMeshSampler<T>::RandomMeshSampler(const Mesh_t& mesh,
                                        const size_t& n_sample,
                                        const unsigned& seed) :
        Sampler<3, T>(n_sample, seed),
        points_(mesh.get_vertex()),
        dist_(0, points_.size()) {
  prob_ = T(1.0) / mesh.Area();
}

template<typename T>
RandomMeshSampler<T>::RandomMeshSampler(const Mesh_t& mesh,
                                        const size_t& n_sample) :
        RandomMeshSampler(mesh, n_sample, seed()) {
}

template<typename T>
void RandomMeshSampler<T>::operator()(PtType* sample, T* prob) {
  size_t rnd_idx = dist_(this->gen_);
  *sample = points_[rnd_idx];
  *prob = prob_;
}

#endif  // DOXYGEN_SHOULD_SKIP_THIS

#pragma mark -
#pragma mark Random Mesh Sampler

/**
 * @class   RandomMeshColorSampler
 * @brief   Randomly select color from a given mesh.
 * @author  Christophe Ecabert
 * @date    09/02/2021
 * @ingroup kernel
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS RandomMeshColorSampler : public Sampler<3, T> {
 public:
  /** Point type */
  using PtType = PointType<3, T>;
  /** Mesh type */
  using Mesh_t = Mesh<T, Vector3<T>>;

  /**
   * @name  RandomMeshColorSampler
   * @fn    RandomMeshColorSampler(const Mesh_t& mesh, const size_t& n_sample,
                              const unsigned& seed)
   * @brief Constructor
   * @param[in] mesh        Mesh
   * @param[in] n_sample    Number of point to sample at each time
   * @param[in] seed        Random generator's seed.
   */
  RandomMeshColorSampler(const Mesh_t& mesh,
                         const size_t& n_sample,
                         const unsigned& seed);

  /**
   * @name  RandomMeshColorSampler
   * @fn    RandomMeshColorSampler(const Mesh_t& mesh, const size_t& n_sample)
   * @brief Constructor
   * @param[in] mesh        Mesh
   * @param[in] n_sample    Number of point to sample at each time
   */
  RandomMeshColorSampler(const Mesh_t& mesh,
                         const size_t& n_sample);

 protected:
  /**
   * @name  operator()
   * @fn    void operator()(PtType* sample, T* prob) override
   * @brief Actual sampling function.
   * @param[out] sample Generated sample
   * @param[out] prob   Porbability
   */
  void operator()(PtType* sample, T* prob) override;

 private:
  /** Mesh's colors */
  std::vector<PtType> colors_;
  /** Sampling distribution */
  std::uniform_int_distribution<size_t> dist_;
  /** probability */
  T prob_;
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<typename T>
RandomMeshColorSampler<T>::RandomMeshColorSampler(const Mesh_t& mesh,
                                                  const size_t& n_sample,
                                                  const unsigned& seed) :
        Sampler<3, T>(n_sample, seed),
        colors_(mesh.get_vertex_color()),
        dist_(0, colors_.size()) {
  prob_ = T(1.0) / mesh.Area();
}

template<typename T>
RandomMeshColorSampler<T>::RandomMeshColorSampler(const Mesh_t& mesh,
                                                  const size_t& n_sample) :
        RandomMeshColorSampler(mesh, n_sample, seed()) {
}

template<typename T>
void RandomMeshColorSampler<T>::operator()(PtType* sample, T* prob) {
  size_t rnd_idx = dist_(this->gen_);
  *sample = colors_[rnd_idx];
  *prob = prob_;
}

#endif  // DOXYGEN_SHOULD_SKIP_THIS

#pragma mark -
#pragma mark Uniform Mesh Sampler

/**
 * @class   UniformMeshSampler
 * @brief   Uniformly select points from a given mesh.
 * @author  Christophe Ecabert
 * @date    18/12/2020
 * @ingroup kernel
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS UniformMeshSampler : public Sampler<3, T> {
 public:
  /** Point type */
  using PtType = PointType<3, T>;
  /** Mesh type */
  using Mesh_t = Mesh<T, Vector3<T>>;

  /**
   * @name  UniformMeshSampler
   * @fn    UniformMeshSampler(const Mesh_t& mesh, const size_t& n_sample,
                              const unsigned& seed)
   * @brief Constructor
   * @param[in] mesh        Mesh
   * @param[in] n_sample    Number of point to sample at each time
   * @param[in] seed        Random generator's seed.
   */
  UniformMeshSampler(const Mesh_t& mesh,
                     const size_t& n_sample,
                     const unsigned& seed);

  /**
   * @name  UniformMeshSampler
   * @fn    UniformMeshSampler(const Mesh_t& mesh, const size_t& n_sample)
   * @brief Constructor
   * @param[in] mesh        Mesh
   * @param[in] n_sample    Number of point to sample at each time
   */
  UniformMeshSampler(const Mesh_t& mesh,
                     const size_t& n_sample);

 protected:
  /**
   * @name  operator()
   * @fn    void operator()(PtType* sample, T* prob) override
   * @brief Actual sampling function.
   * @param[out] sample Generated sample
   * @param[out] prob   Porbability
   */
  void operator()(PtType* sample, T* prob) override;

 private:
  /** Triangle type */
  using Tri = typename Mesh_t::Triangle;

  /**
   * @name  SamplePointInTriangle
   * @fn    PtType SamplePointInTriangle(const size_t& index)
   * @param[in] index Triangle index to sample from
   * @return    Sampled point lying on given triangle
   */
  PtType SamplePointInTriangle(const size_t& index);

  /**
   * @name  ComputeAccumulatedArea
   * @fn    void ComputeAccumulatedArea()
   * @brief Compute accumulated triangle area
   */
  void ComputeAccumulatedArea();

  /**
   * @name  TriArea
   * @fn    T TriArea(const size_t& index) const
   * @brief Compute area of a given triangle
   * @param[in] index   Triangle index
   * @return    Area
   */
  T TriArea(const size_t& index) const;

  /** Mesh's points */
  std::vector<PtType> points_;
  /** Mesh's triangle */
  std::vector<Tri> tri_;
  /** Accumulated triangle's area */
  std::vector<T> accumulated_area_;
  /** Sampling distribution */
  std::uniform_real_distribution<T> dist_;
  /** Mesh's area */
  T area_;
  /** probability */
  T prob_;
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<typename T>
UniformMeshSampler<T>::UniformMeshSampler(const Mesh_t& mesh,
                                          const size_t& n_sample,
                                          const unsigned& seed) :
        Sampler<3, T>(n_sample, seed),
        points_(mesh.get_vertex()),
        tri_(mesh.get_triangle()),
        accumulated_area_(),
        dist_(T(0.0), T(1.0)),
        prob_(0.0) {
  // Compute accumulated area
  this->ComputeAccumulatedArea();
  // Init proba + area
  area_ = accumulated_area_.back();
  prob_ = T(1.0) / area_;
}

template<typename T>
UniformMeshSampler<T>::UniformMeshSampler(const Mesh_t& mesh,
                                          const size_t& n_sample) :
        UniformMeshSampler(mesh, n_sample, seed()) {
}

template<typename T>
void UniformMeshSampler<T>::operator()(PtType* sample, T* prob) {
  // Sample
  T rnd_value = dist_(this->gen_) * area_;
  auto it = std::lower_bound(accumulated_area_.begin(),
                             accumulated_area_.end(),
                             rnd_value);
  auto index = std::distance(accumulated_area_.begin(), it);
  assert(index >= 0 && index < accumulated_area_.size() &&
         "Sampled index outside of valid range");
  // Randomly sample point in the selected triangle
  *sample = SamplePointInTriangle(index);
  *prob = prob_;
}

template<typename T>
typename UniformMeshSampler<T>::PtType
UniformMeshSampler<T>::SamplePointInTriangle(const size_t& index) {
  // Get vertex
  const auto& tri = tri_[index];
  const auto& v0 = points_[tri.x_];
  const auto& v1 = points_[tri.y_];
  const auto& v2 = points_[tri.z_];
  // Random sampling with barycentric coordinates
  T u = dist_(this->gen_);  // [0, 1]
  T d = dist_(this->gen_);  // [0, 1]
  T v = d + u <= T(1.0) ? d : T(1.0) - u;
  return (v0 * u) + (v1 * v) + (v2 * (T(1.0) - (u + v)));
}

template<typename T>
T UniformMeshSampler<T>::TriArea(const size_t &index) const {
  const auto& tri = tri_[index];
  const auto& v0 = points_[tri.x_];
  const auto& v1 = points_[tri.y_];
  const auto& v2 = points_[tri.z_];
  // Area, should we use heron's formula ???
  return T(0.5) * ((v1 - v0) ^ (v2 - v0)).Norm();
}

template<typename T>
void UniformMeshSampler<T>::ComputeAccumulatedArea() {
  accumulated_area_.clear();
  for (size_t i = 0; i < tri_.size(); ++i) {
    if (i == 0) {
      accumulated_area_.push_back(TriArea(i));
    } else {
      T sum = accumulated_area_.back();
      sum += TriArea(i);
      accumulated_area_.push_back(sum);
    }
  }
}

#endif  // DOXYGEN_SHOULD_SKIP_THIS

#pragma mark -
#pragma mark Fixed Point Uniform Mesh Sampler

/**
 * @class   FixedPointsUniformMeshSampler
 * @brief   Uniformly select points from a given mesh. However, if called
 *  multiple times, will always output the same samples
 * @author  Christophe Ecabert
 * @date    18/12/2020
 * @ingroup kernel
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS FixedPointsUniformMeshSampler : public Sampler<3, T> {
 public:
  /** Point type */
  using PtType = PointType<3, T>;
  /** Mesh type */
  using Mesh_t = Mesh<T, Vector3<T>>;

  /**
   * @name  FixedPointsUniformMeshSampler
   * @fn    FixedPointsUniformMeshSampler(const Mesh_t& mesh,
   *                                      const size_t& n_sample,
   *                                      const unsigned& seed)
   * @brief Constructor
   * @param[in] mesh        Mesh to sample
   * @param[in] n_sample    Number of point to sample each time
   * @param[in] seed        Random generator's seed.
   */
  FixedPointsUniformMeshSampler(const Mesh_t& mesh,
                                const size_t& n_sample,
                                const unsigned& seed);

  /**
   * @name  FixedPointsUniformMeshSampler
   * @fn    FixedPointsUniformMeshSampler(const Mesh_t& mesh,
   *                                      const size_t& n_sample)
   * @brief Constructor
   * @param[in] mesh        Mesh to sample
   * @param[in] n_sample    Number of point to sample each time
   */
  FixedPointsUniformMeshSampler(const Mesh_t& mesh,
                                const size_t& n_sample);

 protected:
  /**
   * @name  operator()
   * @fn    void operator()(PtType* sample, T* prob) override
   * @brief Actual sampling function.
   * @param[out] sample Generated sample
   * @param[out] prob   Porbability
   */
  void operator()(PtType* sample, T* prob) override;

 private:
  /** Samples */
  std::vector<PtType> samples_;
  /** probability */
  T prob_;
};

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<typename T>
FixedPointsUniformMeshSampler<T>::
FixedPointsUniformMeshSampler(const Mesh_t& mesh,
                              const size_t& n_sample,
                              const unsigned& seed) :
        Sampler<3, T>(n_sample, seed) {
  // Create sampler and use it once
  UniformMeshSampler<T> sampler(mesh, n_sample, seed);
  samples_ = sampler.Sample();
  // Init prob
  prob_ = T(1.0) / mesh.Area();
}

template<typename T>
FixedPointsUniformMeshSampler<T>::
FixedPointsUniformMeshSampler(const Mesh_t& mesh,
                              const size_t& n_sample) :
        FixedPointsUniformMeshSampler(mesh, n_sample, seed()) {
}

template<typename T>
void FixedPointsUniformMeshSampler<T>::operator()(PtType* sample, T* prob) {
  static size_t counter = 0;
  *sample = samples_[counter];
  *prob = prob_;
  counter++;
  if (counter >= this->n_sample_) {
    counter = 0;
  }
}

#endif  // DOXYGEN_SHOULD_SKIP_THIS

}  // namespace LTS5
#endif // __LTS5_KERNEL_SAMPLER__
