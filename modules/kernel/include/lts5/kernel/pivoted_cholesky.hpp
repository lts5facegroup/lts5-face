/**
 *  @file   pivoted_cholesky.hpp
 *  @brief  Tools to approximate eigenvalue / eigenvector from Positive Semi-
 *      Definite (PSD) covariance matrix
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   18/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_PIVOTED_CHOLESKY__
#define __LTS5_KERNEL_PIVOTED_CHOLESKY__

#include <vector>
#include <cassert>
#include <functional>
#include <numeric>
#include <memory>
#include <type_traits>
#include <limits>
#include <thread>

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/utils/sys/parallel.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<int NDim, typename T>
using PointType = typename Point<NDim, T>::Type;

#pragma mark -
#pragma mark Storage

/**
 * @class   PivotedCholeskyStorage
 * @brief   Store column of triangular matrix estimated during cholesky
 *  decomposition
 * @author  Christophe Ecabert
 * @date    18/12/2020
 * @ingroup kernel
 * @tparam T        Data type
 */
template<typename T>
class LTS5_EXPORTS PivotedCholeskyStorage {
 public:
  /**
   * @name  PivotedCholeskyStorage
   * @fn    explicit PivotedCholeskyStorage(int n_row)
   * @brief Constructor
   * @param[in] n_row Number of rows in the lower triangular matrix
   */
  explicit PivotedCholeskyStorage(int n_row);

  /**
   * @name  operator()
   * @fn    T operator()(int row, int col) const
   * @brief Access element at position `row` and `col`
   * @param[in] row Row index
   * @param[in] col Column index
   * @return    Matrix value
   */
  T operator()(int row, int col) const;

  /**
   * @name  operator()
   * @fn    cv::Mat operator()(int col) const
   * @brief Access a given column.
   * @param[in] col Column index to acces
   * @return    Column
   */
  cv::Mat operator()(int col) const;

  /**
   * @name  Set
   * @fn    void Set(const int& col, const cv::Mat& value)
   * @brief Set a new column in the matrix
   * @param[in] col     Column index to set
   * @param[in] value   Value to insert in the matrix
   */
  void Set(const int& col, const cv::Mat& value);

  /**
   * @name  AsMatrix
   * @fn    cv::Mat AsMatrix() const
   * @brief Convert into a dense matrix
   * @return    Lower triangular matrix
   */
  cv::Mat AsMatrix() const;

 private:
  /** Column storage */
  std::vector<cv::Mat> col_;
  /** Number of rows */
  int n_row_;
};

#pragma mark -
#pragma mark Pivoted Cholesky Decomposition

/**
 * @class   StoppingCriterion
 * @brief   Stopping criterion interface for PivotedCholesky decomposition
 * @author  Christophe Ecabert
 * @date    18/12/2020
 * @ingroup kernel
 * @tparam T        Data type
 */
template<typename T>
class LTS5_EXPORTS StoppingCriterion {
 public:
  virtual ~StoppingCriterion() = default;
  virtual T Tolerance(const T& intial_error) const = 0;
  virtual int Iteration() const = 0;
};

/**
 * @class   AbsoluteTolerance
 * @brief   Stopping criterion based on absolute tolerance / error
 * @author  Christophe Ecabert
 * @date    18/12/2020
 * @ingroup kernel
 * @tparam T        Data type
 */
template<typename T>
class AbsoluteTolerance : public StoppingCriterion<T> {
 public:
  /**
   * @name  AbsoluteTolerance
   * @fn    explicit AbsoluteTolerance(const T tol)
   * @brief Constructor
   * @param[in] tol Tolerance to reach
   */
  explicit AbsoluteTolerance(const T tol) : tol_(tol) {}

  /**
   * @name  Tolerance
   * @fn    T Tolerance(const T& initial_error) const override
   * @brief Tolerance to reach
   * @param[in] initial_error   Initial error when starting the decomposition
   * @return    Tolerance to reach
   */
  T Tolerance(const T& initial_error) const override {
    return tol_;
  }

  /**
   * @name  Iteration
   * @fn    int Iteration() const override
   * @brief No limitation on the number of iterations
   * @return    -1, no limitation
   */
  int Iteration() const override {
    return -1;
  }
 private:
  /** Tolerance */
  T tol_;
};

/**
 * @class   RelativeTolerance
 * @brief   Stopping criterion based on relative tolerance / error
 * @author  Christophe Ecabert
 * @date    18/12/2020
 * @ingroup kernel
 * @tparam T        Data type
 */
template<typename T>
class RelativeTolerance : public StoppingCriterion<T> {
 public:
  /**
   * @name  RelativeTolerance
   * @fn    explicit RelativeTolerance(const T relative_tol)
   * @brief Constructor
   * @param[in] relative_tol Relative tolerance to reach
   */
  explicit RelativeTolerance(const T relative_tol) : tol_(relative_tol) {}

  /**
   * @name  Tolerance
   * @fn    T Tolerance(const T& initial_error) const override
   * @brief Relative tolerance to reach
   * @param[in] initial_error   Initial error when starting the decomposition
   * @return    Tolerance to reach
   */
  T Tolerance(const T& initial_error) const override {
    return tol_ * initial_error;
  }

  /**
   * @name  Iteration
   * @fn    int Iteration() const override
   * @brief No limitation on the number of iterations
   * @return    -1, no limitation
   */
  int Iteration() const override {
    return -1;
  }
 private:
  /** Tolerance */
  T tol_;
};

/**
 * @class   NumberOfIteration
 * @brief   Stopping criterion based on number of iteration
 * @author  Christophe Ecabert
 * @date    18/12/2020
 * @ingroup kernel
 * @tparam T        Data type
 */
template<typename T>
class NumberOfIteration : public StoppingCriterion<T> {
 public:
  /**
   * @name  NumberOfIteration
   * @fn    explicit NumberOfIteration(int n_iter)
   * @brief Constructor
   * @param[in] n_iter Maximum number of iterator to perform
   */
  explicit NumberOfIteration(int n_iter) : n_iter_(n_iter) {}

  /**
   * @name  Tolerance
   * @fn    T Tolerance(const T& initial_error) const override
   * @brief Relative tolerance to reach
   * @param[in] initial_error   Initial error when starting the decomposition
   * @return    Tolerance to reach
   */
  T Tolerance(const T& initial_error) const override {
    return T(1e-15);
  }

  /**
   * @name  Iteration
   * @fn    int Iteration() const override
   * @brief Number of iterations allowed to do.
   * @return   Number of iteration
   */
  int Iteration() const override {
    return n_iter_;
  }
 private:
  /** Maximum number of iteration */
  int n_iter_;
};

/**
 * @class   NumberOfIterationAndRelativeTolerance
 * @brief   Stopping criterion based on number of iteration and relative
 *  tolerance. Stop will be triggerd if one of the two conditions is reached
 * @author  Christophe Ecabert
 * @date    18/12/2020
 * @ingroup kernel
 * @tparam T        Data type
 */
template<typename T>
class NumberOfIterationAndRelativeTolerance : public StoppingCriterion<T> {
 public:
  /**
   * @name  NumberOfIterationAndRelativeTolerance
   * @fn    NumberOfIterationAndRelativeTolerance
   * @brief Constructor
   * @param[in] tol     Relative error at which to stop
   * @param[in] n_iter  Maximum number of iterator to perform
   */
  NumberOfIterationAndRelativeTolerance(T tol,
                                        int n_iter) : n_iter_(n_iter),
                                                      tol_(tol){
  }

  /**
   * @name  Tolerance
   * @fn    T Tolerance(const T& initial_error) const override
   * @brief Compute tolerance to reach
   * @param[in] initial_error   Initial error when starting the decomposition
   * @return    Tolerance to reach
   */
  T Tolerance(const T& initial_error) const override {
    return initial_error * tol_;
  }

  /**
   * @name  Iteration
   * @fn    int Iteration() const override
   * @brief Number of iterations allowed to do.
   * @return   Number of iteration
   */
  int Iteration() const override {
    return n_iter_;
  }

 private:
  /** Maximum number of iteration */
  int n_iter_;
  /** Tolerance */
  T tol_;
};


/**
 * @class   PivCholeskyDecomposition
 * @brief   Result object for the pivoted cholesky of a matrix A
 * @author  Christophe Ecabert
 * @date    21/12/2020
 * @ingroup kernel
 * @tparam T        Data type
 */
template<typename T>
class LTS5_EXPORTS PivCholeskyDecomposition {
 public:
  /**
   * @name  PivCholeskyDecomposition
   * @fn    PivCholeskyDecomposition(const cv::Mat& l_mat,
                                     const std::vector<int>& pivot,
                                     const T& error)
   * @brief Constructor
   * @param[in] l_mat   The (first m columns) of a lower triangular matrix L,
   *    for which \f$ LL' = A_m \approx A \f$.
   * @param[in] pivot   List of pivot
   * @param[in] error   Approximation error
   */
  PivCholeskyDecomposition(const cv::Mat& l_mat,
                           const std::vector<int>& pivot,
                           const T& error) : l_(l_mat),
                                             p_(pivot),
                                             e_(error) {}

   /**
    * @name EigenDecomposition
    * @fn   int EigenDecomposition(cv::Mat* vectors, cv::Mat* values) const
    * @brief Perform eigen decomposition of the underlying pivoted cholesky
    *   decomposition
    * @see "Pivoted Cholesky decomposition by cross approximation for efficient
    *   solution of kernel systems" Liu et al, section 5.1
    * @param[out] vectors   Eigenvectors
    * @param[out] values    Eigenvalues
    * @return   -1 if error, 0 otherwise
    */
  int EigenDecomposition(cv::Mat* vectors, cv::Mat* values) const;

  /**
   * @name  matrix
   * @fn    const cv::Mat& matrix() const
   * @brief Triangular matrix
   * @return    matrix
   */
  const cv::Mat& matrix() const {
    return l_;
  }

  /**
   * @name  pivot
   * @fn    const std::vector<int>& pivot() const
   * @brief Decomposition pivot
   * @return    vector
   */
  const std::vector<int>& pivot() const {
    return p_;
  }

  /**
   * @name  error
   * @fn    T error() const
   * @brief Approximation error
   * @return    Error
   */
  T error() const {
    return e_;
  }

 private:
  /** Triangular matrix */
  cv::Mat l_;
  /** List of pivots */
  std::vector<int> p_;
  /** Approximation error */
  T e_;
};

/**
 * @name  ApproxCholeskGeneric
 * @fn    PivCholeskyDecomposition<T> ApproxCholeskyGeneric(
                      const std::function<T(const InType&, const InType&)>& fcn,
                      const std::vector<InType>& xs,
                      const StoppingCriterion<T>& sc)
 * @brief Compute cholesky decomposition. Based on "On the low-rank
 *  approximation by the pivoted Cholesky decomposition" Harbrecht et al.
 * @see https://www.sciencedirect.com/science/article/pii/S0168927411001814,
 *  algorithm 1
 * @param[in] fcn Kernel function
 * @param[in] xs  List of points where to evaluate kernel
 * @param[in] sc  Stopping criterion
 * @tparam T      Data type
 * @tparam InType Kernel input data type (i.e. Point Nd)
 * @return    Decomposition
 */
template<typename T, typename InType>
PivCholeskyDecomposition<T> LTS5_EXPORTS
ApproxCholeskyGeneric(const std::function<T(const InType&, const InType&)>& fcn,
                      const std::vector<InType>& xs,
                      const StoppingCriterion<T>& sc);

/**
 * @name  ApproxCholesky
 * @fn    PivCholeskyDecomposition<T> ApproxCholesky(
 *                              const std::shared_ptr<MatKernel>& k,
 *                              const std::vector<PointType<IDim, T>>& xs,
 *                              const StoppingCriterion<T>& sc)
 * @brief Compute cholesky decomposition for matrix-valued kernel
 * @param[in] k   Matrix-valued kernel to decompose
 * @param[in] xs  List of points to use
 * @param[in] sc  Stopping criterion
 * @tparam T      Data type
 * @tparam IDim   Number of input's dimension
 * @tparam ODim   Number of output's dimension
 * @tparam MatKernel  Type of matrix-valued kernel
 * @return    Decomposition
 */
template<typename T, int IDim, int ODim, class MatKernel>
PivCholeskyDecomposition<T> LTS5_EXPORTS
ApproxCholesky(const std::shared_ptr<MatKernel>& k,
               const std::vector<PointType<IDim, T>>& xs,
               const StoppingCriterion<T>& sc);

/**
 * @name  ApproxCholesky
 * @fn    PivCholeskyDecomposition<T> ApproxCholesky(const cv::Mat& k,
 *                                          const StoppingCriterion<T>& sc)
 * @brief Compute cholesky decomposition for square matrix, must be PSD.
 * @param[in] k   Matrix to decompose
 * @param[in] sc  Stopping criterion
 * @tparam T      Data type
 * @return    Decomposition
 */
template<typename T>
PivCholeskyDecomposition<T> LTS5_EXPORTS
ApproxCholesky(const cv::Mat& k, const StoppingCriterion<T>& sc);


/**
 * @struct  PointWithDim
 * @brief Container for points and corresponding dimension of interest
 * @tparam IDim Number of input's dimension
 * @tparam T    Data type
 */
template<int IDim, typename T>
struct LTS5_EXPORTS PointWithDim {
  /**
   * @name  PointWithDim
   * @brief Constructor
   * @param[in] point       Location where to evaluate kernel
   * @param[in] dimension   Dimension to extract
   */
  PointWithDim(const PointType<IDim, T>& point,
               int dimension) : pts(point),
                                dim(dimension) {}
  /** Point */
  PointType<IDim, T> pts;
  /** Dimension of interest */
  int dim;
};


#pragma mark -
#pragma mark Implementation

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<typename T>
int PivCholeskyDecomposition<T>::EigenDecomposition(cv::Mat* vectors,
                                                    cv::Mat* values) const {
  using LA = LinearAlgebra<T>;
  using Tr = typename LA::TransposeType;
  using QRDecomp = typename LA::QRDecomposition;
  using QRMultType = typename QRDecomp::MultType;
  using SVD = typename LA::SvdDecomposition;
  using SvdType = typename LA::Lapack::SVDType;
  // QR Decomposition: L = QR
  // Use compact representation otherwise reach memory error!
  QRDecomp qr(l_);
  cv::Mat R;
  qr.GetComponents(false, nullptr, &R);
  // SVD(R R') = U Lambda U' where:
  //  Lambda == eigenvalues of LL'
  //  Phi = Q U  == eigenvectors of LL'
  cv::Mat RRt;
  LA::Gemm(R, Tr::kNoTranspose, T(1.0),
           R, Tr::kTranspose, T(0.0),
           &RRt);
  SVD svd;
  svd.Init(RRt, SvdType::kStd);
  svd.Compute();
  cv::Mat U, S;
  svd.Get(&U, &S, nullptr);
  // Add missing rows due to compact decomposition. This is valid since the
  // remaining eigenvalues are zeros.
  cv::Mat Uaug(l_.rows, l_.cols, cv::DataType<T>::type, T(0.0));
  U.copyTo(Uaug(cv::Range(0, U.rows), cv::Range::all()));
  // Eigen values, order ?
  *values = S.diag(0).clone();
  // Eigen vector, order ?
  int err = qr.MultiplyBy(QRMultType::kPost, false, &Uaug);
  *vectors = Uaug;
  // Done
  return err;
}

template<typename T, typename InType>
PivCholeskyDecomposition<T>
ApproxCholeskyGeneric(const std::function<T(const InType&, const InType&)>& fcn,
                      const std::vector<InType>& xs,
                      const StoppingCriterion<T>& sc) {
  // Initialize computation
  const size_t n = xs.size();
  std::vector<int> p(n);
  std::iota(p.begin(), p.end(), 0);  // pivot
  std::vector<T> d;                         // Diagonal of matrix-value fcn.
  for (const auto& pts : xs) {
    d.push_back(fcn(pts, pts));
  }
  // Initial error: L1 norm of diag
  T tr = std::accumulate(d.begin(),
                         d.end(),
                         T(0.0),
                         [](const T& a, const T& b) -> T {
                           return a + std::abs(b);
                         });
  size_t k = 0;
  const T tolerance = sc.Tolerance(tr);
  int maxNumEigenfunctions = sc.Iteration();
  if (maxNumEigenfunctions < 0) {
    maxNumEigenfunctions = n;
  }
  LTS5_LOG_DEBUG("Initial estimation error: " << tr);
  LTS5_LOG_DEBUG("Targeted estimation error: " << tolerance);
  LTS5_LOG_DEBUG("Max #Basis: " << maxNumEigenfunctions << " / " << n);
  PivotedCholeskyStorage<T> storage(n);
  // Start iteration
  while (k < n && k < maxNumEigenfunctions && tr > tolerance) {
    if (k % 100 == 0) {
      LTS5_LOG_DEBUG("Approximation error: " << tr);
      LTS5_LOG_DEBUG("Process column: " << k);
    }
    // Initialize column to zeros
    cv::Mat S = cv::Mat::zeros(n, 1, cv::DataType<T>::type);

    // Find pivot, i.e. column with the largest error
    size_t pivl = 0;
    T max_error = -std::numeric_limits<T>::max();
    for (size_t i = k; i < n; ++i) {
      if (d[p[i]] > max_error) {
        pivl = i;
        max_error = d[p[i]];
      }
    }
    // Swap pivot
    std::swap(p[k], p[pivl]);
    // Compute L_m, pi_m
    T D = std::sqrt(d[p[k]]);
    S.at<T>(p[k]) = D;

    auto n_thread = std::max(std::thread::hardware_concurrency(),
                             std::uint32_t(1));  // Can return 0!
    if (k == 0) {
      LTS5_LOG_DEBUG("Compute using " << n_thread << " threads");
    }

    for (size_t c = 0; c < k; ++c) {
      T tmp = storage(p[k], c);
//      for (size_t r = k + 1; r < n; ++r) {
//        S.at<T>(p[r]) += storage(p[r], c) * tmp;
//      }

      // shift range [k+1, n[, to [0, n-k-1[
      auto chunk_size = (n - k - 1 + n_thread - 1) / n_thread;
      Parallel::For(n_thread/*n - k - 1*/,
                    [&](const size_t& r) {
                      size_t i_from = r * chunk_size;
                      size_t i_to = std::min((r + 1) * chunk_size, n - k - 1);
                      for (size_t i = i_from; i < i_to; ++i) {
                        size_t rp = i + k + 1;  // move back to original range
                        S.at<T>(p[rp]) += storage(p[rp], c) * tmp;
                      }
                    });
    }

    size_t chunk_size = (n - k - 1) + (n_thread - 1) / n_thread;  // divUp
    std::vector<T> chunk_sum(n_thread, T(0.0));
    Parallel::For(n_thread,
                  [&](const size_t& r) {
      size_t i_from = (r * chunk_size) + k + 1;       // [k+1, n[
      size_t i_to = ((r + 1) * chunk_size) + k + 1;
      i_to = i_to > n ? n : i_to;   // Avoid going too far
      for (size_t m = i_from; m < i_to; ++m) {
        S.at<T>(p[m]) = (fcn(xs[p[m]], xs[p[k]]) - S.at<T>(p[m])) / D;
        d[p[m]] -= (S.at<T>(p[m]) * S.at<T>(p[m]));
        chunk_sum[r] += d[p[m]];
      }
    });
    d[p[k]] -= (S.at<T>(p[k]) * S.at<T>(p[k]));
    // Update error
    tr = d[p[k]] + std::accumulate(chunk_sum.begin(), chunk_sum.end(), T(0.0));
    // Set column of L.
    storage.Set(k, S);
    k += 1;
  }
  // Done return
  return PivCholeskyDecomposition<T>(storage.AsMatrix(), p, tr);
}

template<typename T, int IDim, int ODim, class MatKernel>
PivCholeskyDecomposition<T> LTS5_EXPORTS
ApproxCholesky(const std::shared_ptr<MatKernel>& k,
               const std::vector<PointType<IDim, T>>& xs,
               const StoppingCriterion<T>& sc) {
  static_assert(std::is_base_of<MatrixKernel<IDim, ODim, T>, MatKernel>::value,
                "Kernel `k` must be derived from `MatrixKernel<IDim, ODim, T>`");
  // Define access function for matrix kernel
  auto fcn = [&](const PointWithDim<IDim, T>& x,
                 const PointWithDim<IDim, T>& y) -> T {
    return k->operator()(x.pts, y.pts)(x.dim, y.dim);
  };
  // Convert point into `PointWithDim`
  std::vector<PointWithDim<IDim, T>> xs_with_dims;
  for (const auto& pts : xs) {
    for (int i = 0; i < ODim; ++i) {
      xs_with_dims.emplace_back(pts, i);
    }
  }
  // Run decomposition
  return ApproxCholeskyGeneric<T, PointWithDim<IDim, T>>(fcn, xs_with_dims, sc);
}

template<typename T>
PivCholeskyDecomposition<T>
ApproxCholesky(const cv::Mat& k, const StoppingCriterion<T>& sc) {
  assert(k.rows == k.cols && "Matrix must be square!");
  // Define access function for matrix
  auto fcn = [&](int i, int j) -> T {
    return k.at<T>(i, j);
  };
  std::vector<int> index(k.cols);
  std::iota(index.begin(), index.end(), 0);
  // Run decomposition
  return ApproxCholeskyGeneric<T, int>(fcn, index, sc);
}


template<typename T>
PivotedCholeskyStorage<T>::PivotedCholeskyStorage(int n_row) : n_row_(n_row) {
}

template<typename T>
T PivotedCholeskyStorage<T>::operator()(int row, int col) const {
  assert(row >= 0 && row < n_row_ && "Row index is too large");
  assert(col >= 0 && col < col_.size() && "Column index is too large");
  // Access element
  return col_[col].template at<T>(row);
}

template<typename T>
cv::Mat PivotedCholeskyStorage<T>::operator()(int col) const {
  assert(col >= 0 && col < col_.size() && "Column index is too large");
  return col_[col];
}

template<typename T>
void PivotedCholeskyStorage<T>::Set(const int& col, const cv::Mat& value) {
  assert(col >= 0 && col < col_.size() + 1 && "Column index is too large");
  assert(value.cols == 1 && value.rows == n_row_ &&
         "Value's dimensions does not match previous entries");
  if (col < col_.size()) {
    col_[col] = value.clone();
  } else {
    // Add new column
    col_.emplace_back(value.clone());
  }
}

template<typename T>
cv::Mat PivotedCholeskyStorage<T>::AsMatrix() const {
  cv::Mat mat(n_row_, col_.size(), cv::DataType<T>::type);
  for (int i = 0; i < n_row_; ++i) {
    for (int j = 0; j < col_.size(); ++j) {
      mat.at<T>(i, j) = col_[j].template at<T>(i);
    }
  }
  return mat;
}

#endif  // DOXYGEN_SHOULD_SKIP_THIS

}  // namespace LTS5
#endif // __LTS5_KERNEL_PIVOTED_CHOLESKY__
