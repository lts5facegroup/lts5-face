/**
 *  @file   domain.hpp
 *  @brief  Represent region where function is defined
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   11/30/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_KERNEL_DOMAIN__
#define __LTS5_KERNEL_DOMAIN__

#include <memory>
#include <vector>
#include <unordered_map>
#include <functional>
#include <iostream>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/hash.hpp"
#include "lts5/geometry/kdtree.hpp"
#include "lts5/geometry/bounding_sphere.hpp"
#include "lts5/geometry/mesh.hpp"

#pragma mark -
#pragma mark Hash function for Point

namespace std {
template<typename T>
struct hash<LTS5::Vector1<T>> {
  inline size_t operator()(const LTS5::Vector1<T>& value) const {
    size_t seed = 0;
    LTS5::HashCombine(value.x_, seed);
    return seed;
  }
};
template<typename T>
struct hash<LTS5::Vector2<T>> {
  inline size_t operator()(const LTS5::Vector2<T>& value) const {
    size_t seed = 0;
    LTS5::HashCombine(value.x_, seed);
    LTS5::HashCombine(value.y_, seed);
    return seed;
  }
};
template<typename T>
struct hash<LTS5::Vector3<T>> {
  inline size_t operator()(const LTS5::Vector3<T>& value) const {
    size_t seed = 0;
    LTS5::HashCombine(value.x_, seed);
    LTS5::HashCombine(value.y_, seed);
    LTS5::HashCombine(value.z_, seed);
    return seed;
  }
};
template<typename T>
struct hash<LTS5::Vector4<T>> {
  inline size_t operator()(const LTS5::Vector4<T>& value) const {
    size_t seed = 0;
    LTS5::HashCombine(value.x_, seed);
    LTS5::HashCombine(value.y_, seed);
    LTS5::HashCombine(value.z_, seed);
    LTS5::HashCombine(value.w_, seed);
    return seed;
  }
};
}

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<int NDim, typename T>
using PointType = typename Point<NDim, T>::Type;

/**
 * @class   Domain
 * @brief   Interface for function domain definition
 * @author  Christophe Ecabert
 * @date    30/11/2020
 * @ingroup kernel
 * @tparam NDim Number of dimensions {1, 2, 3, 4}
 * @tparam T    Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS Domain {
 public:

  /**
   * @name  ~Domain
   * @fn    virtual ~Domain() = default
   * @brief Destructor
   */
  virtual ~Domain() = default;

  /**
   * @name  IsDefinedAt
   * @fn    virtual bool IsDefinedAt(const PointType<NDim, T>& pt) const = 0;
   * @brief Check if point is part of the domain
   * @param[in] pt Point ({1,2,3,4}D) to test against
   * @return    True if part of the domain, False otherwise
   */
  virtual bool IsDefinedAt(const PointType<NDim, T>& pt) const = 0;
};

/**
 * @class   DomainComposer
 * @brief   Helper class to combine domain together
 * @author  Christophe Ecabert
 * @date    30/11/2020
 * @ingroup kernel
 */
template<int NDim, typename T>
class LTS5_EXPORTS DomainComposer : public Domain<NDim, T> {
 public:
  /** Pointer type */
  using Ptr = std::shared_ptr<DomainComposer<NDim, T>>;
  /** Domain Type */
  using DomType = std::shared_ptr<Domain<NDim, T>>;
  /** Domain comparator */
  using DomComp = std::function<bool(const bool&, const bool&)>;

  /**
   * @name  DomainComposer
   * @fn    DomainComposer(const DomainComposer& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  DomainComposer(const DomainComposer& other) = delete;

  /**
   * @name  operator=
   * @fn    DomainComposer& operator=(const DomainComposer& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned operator
   */
  DomainComposer& operator=(const DomainComposer& rhs) = delete;

  /**
   * @name  Intersection
   * @fn    static Ptr Intersection(const DomType& dom_a, const DomType& dom_b)
   * @brief Define the intersection between two given domains.
   * @param[in] dom_a First domain
   * @param[in] dom_b Second domain
   * @return Intersection between domain A and B.
   */
  static Ptr Intersection(const DomType& dom_a, const DomType& dom_b);

  /**
   * @name  Union
   * @fn    static Ptr Union(const DomType& dom_a, const DomType& dom_b)
   * @brief Define the union between two given domains.
   * @param[in] dom_a First domain
   * @param[in] dom_b Second domain
   * @return Union between domain A and B.
   */
  static Ptr Union(const DomType& dom_a, const DomType& dom_b);

  /**
   * @name  IsDefinedAt
   * @fn    bool IsDefinedAt(const PointType<NDim, T>& pt) const
   * @brief Check if point is part of the domain
   * @param[in] pt Point ({1,2,3,4}D) to test against
   * @return    True if part of the domain, False otherwise
   */
  bool IsDefinedAt(const PointType<NDim, T>& pt) const override {
    return comp_(dom_a_->IsDefinedAt(pt), dom_b_->IsDefinedAt(pt));
  }

 private:
  /**
   * Constructor
   * @param[in] dom_a   First domain to combine
   * @param[in] dom_b   Second domain to combine
   * @param[in] comp    Method to compare the two domain
   */
  DomainComposer(const DomType& dom_a, const DomType& dom_b, DomComp comp);

  /** First domain */
  DomType dom_a_;
  /** Second domain */
  DomType dom_b_;
  /** Logical comparator function */
  DomComp comp_;
};


/**
 * @class   EuclideanSpace
 * @brief   Real value Nd space
 * @author  Christophe Ecabert
 * @date    30/11/2020
 * @ingroup kernel
 * @tparam  NDim    Number of component
 * @tparam  T       Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS EuclideanSpace : public Domain<NDim, T> {
 public:
  /** Pointer type */
  using Ptr = std::shared_ptr<EuclideanSpace<NDim, T>>;

  /**
   * @name  Create
   * @fn    static Ptr Create()
   * @brief Create object wrapped in a shared_ptr.
   * @return    Shared pointer
   */
  static Ptr Create();

  /**
   * @name  EuclideanSpace
   * @fn    EuclideanSpace(const EuclideanSpace& other) = delete
   * @brief Copy constructor
   * @param[in] other   Object to copy from
   */
  EuclideanSpace(const EuclideanSpace& other) = delete;

  /**
   * @name  operator=
   * @fn    EuclideanSpace& operator=(const EuclideanSpace& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs   Object to assign from
   * @return Newly assigned object
   */
  EuclideanSpace& operator=(const EuclideanSpace& rhs) = delete;

  /**
   * @name  IsDefinedAt
   * @fn    bool IsDefinedAt(const PointType<NDim, T>& pt) const;
   * @brief Check if point is part of the domain
   * @param[in] pt Point ({1,2,3,4}D) to test against
   * @return    True if part of the domain, False otherwise
   */
  bool IsDefinedAt(const PointType<NDim, T>& pt) const override {
    return true;
  }

 private:
  /**
   * @name  EuclideanSpace
   * @fn    EuclideanSpace() = default
   * @brief Constructor
   */
  EuclideanSpace() = default;
};

/**
 * @class   BoxDomain
 * @brief   Nd space delimitated by a box
 * @date    30/11/2020
 * @author  Christophe Ecabert
 * @ingroup kernel
 * @tparam  NDim    Number of components
 * @tparam  TR      Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS BoxDomain : public Domain<NDim, T> {
 public:
  /** Pointer type */
  using Ptr = std::shared_ptr<BoxDomain<NDim, T>>;

  /**
   * @name  Create
   * @fn    static Ptr Create(const PointType<NDim, T>& origin,
                                    const PointType<NDim, T>& opposite)
   * @brief Create object wrapped in a shared_ptr.
   * @param[in] origin      Box origin
   * @param[in] opposite    Opposite corner
   * @return    Shared pointer
   */
  static Ptr Create(const PointType<NDim, T>& origin,
                    const PointType<NDim, T>& opposite);

  /**
   * @name  FromPoints
   * @fn    static Ptr FromPoints(const std::vector<PointType<NDim, T>>& points)
   * @brief Create a BoxDomain that encapsulate a given set of points.
   * @param[in] points  List of points.
   * @return    BoxDomain
   */
  static Ptr FromPoints(const std::vector<PointType<NDim, T>>& points);

  /**
   * @name  BoxDomain
   * @fn    BoxDomain(const BoxDomain& other) = delete
   * @brief Copy constructor
   * @param[in] other   Object to copy from
   */
  BoxDomain(const BoxDomain& other) = delete;

  /**
   * @name  operator=
   * @fn    BoxDomain& operator=(const BoxDomain& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs   Object to assign from
   * @return Newly assigned object
   */
  BoxDomain& operator=(const BoxDomain& rhs) = delete;

  /**
   * @name  IsDefinedAt
   * @fn    bool IsDefinedAt(const PointType<NDim, T>& pt) const;
   * @brief Check if point is part of the domain
   * @param[in] pt Point ({1,2,3,4}D) to test against
   * @return    True if part of the domain, False otherwise
   */
  bool IsDefinedAt(const PointType<NDim, T>& pt) const override;

  /**
   * @name      Extent
   * @fn        PointType<NDim, T> Extent() const
   * @brief     Provide box "width"
   * @return    Domain size
   */
  PointType<NDim, T> Extent() const {
    return opposite_ - org_;
  }

  /**
   * @name  Volume
   * @brief Compute domain volume
   * @return    Volume
   */
  T Volume() const;

  /**
   * @name  get_origin
   * @fn    const PointType<NDim, T>& get_origin() const
   * @brief Box domain origin
   * @return    Origin
   */
  const PointType<NDim, T>& get_origin() const {
    return org_;
  }

  /**
   * @name  get_opposite
   * @fn    const PointType<NDim, T>& get_opposite() const
   * @brief Box domain opposite
   * @return    Opposite
   */
  const PointType<NDim, T>& get_opposite() const {
    return opposite_;
  }

 private:
  /**
   * @name  BoxDomain
   * @fn    BoxDomain(const PointType<NDim, T>& origin,
   *                  const PointType<NDim, T>& opposite)
   * @brief Constructor
   * @param[in] origin      Box origin
   * @param[in] opposite    Opposite corner
   */
  BoxDomain(const PointType<NDim, T>& origin,
            const PointType<NDim, T>& opposite);

  /**
   * @name  IsInside
   * @brief Check if a given point lays inside the domain box
   * @param[in] pt  Point to check against
   * @param[in] idx Component index to check
   * @return    True if inside, false otherwise
   */
  bool IsInside(const PointType<NDim, T>& pt, int idx) const;

  /** Origin */
  PointType<NDim, T> org_;
  /** Opposite */
  PointType<NDim, T> opposite_;
};

/**
 * @class   DiscreteDomain
 * @brief   Interface for discrete function domain definition
 * @author  Christophe Ecabert
 * @date    15/12/2020
 * @ingroup kernel
 * @tparam NDim Number of dimensions {1, 2, 3, 4}
 * @tparam T    Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS DiscreteDomain {
 public:
  /** Point */
  using PtType = PointType<NDim, T>;
  /** List of point */
  using PointList = std::vector<PtType>;
  /** BoxDomain pointer type */
  using BoxDomPtr = std::shared_ptr<BoxDomain<NDim, T>>;
  /** Transformation function */
  using TrsfrmFcn = std::function<PtType(const PtType&)>;
  /** Pointer type */
  using Ptr = std::shared_ptr<DiscreteDomain<NDim, T>>;

  /**
   * @name  DiscreteDomain
   * @fn    DiscreteDomain() = default
   * @brief Constructor, empty domain
   */
  DiscreteDomain() = default;

  /**
   * @name  DiscreteDomain
   * @fn    explicit DiscreteDomain(const PointList& points)
   * @brief Constructor
   * @param[in] points  Point where function is defined
   */
  explicit DiscreteDomain(const PointList& points) :
          points_(points),
          bbox_(BoxDomain<NDim,T>::FromPoints(points)) {
  }

  /**
   * @name  DiscreteDomain
   * @fn    DiscreteDomain(const DiscreteDomain& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  DiscreteDomain(const DiscreteDomain& other) = delete;

  /**
   * @name  operator=
   * @fn    DiscreteDomain& operator=(const DiscreteDomain& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  DiscreteDomain& operator=(const DiscreteDomain& rhs) = delete;

  /**
   * @name  ~DiscreteDomain
   * @fn    virtual ~DiscreteDomain() = default
   * @brief Destructor
   */
  virtual ~DiscreteDomain() = default;

  /**
   * @name  Load
   * @fn    int Load(const std::string& filename)
   * @brief Load domain from file
   * @param[in] filename    Path to file
   * @return    -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   * @name  Load
   * @fn    virtual int Load(std::istream& stream) = 0
   * @brief Load domain from a binary stream
   * @param[in] stream  Stream to read
   * @return    -1 if error, 0 otherwise
   */
  virtual int Load(std::istream& stream) = 0;

  /**
   * @name  Save
   * @fn    int Save(const std::string& filename) const
   * @brief Save domain to file
   * @param[in] filename    Path to file
   * @return    -1 if error, 0 otherwise
   */
  int Save(const std::string& filename) const;

  /**
   * @name  Save
   * @fn    virtual int Save(std::ostream& stream) const = 0;
   * @brief Save domain to a binary stream
   * @param[in] stream  Stream to read
   * @return    -1 if error, 0 otherwise
   */
  virtual int Save(std::ostream& stream) const = 0;

  /**
   * @name  ObjectSize
   * @fn    virtual int ObjectSize() const = 0
   * @brief Compute object size in bytes
   * @return    Object size in bytes
   */
  virtual int ObjectSize() const = 0;

  /**
   * @name  IsDefinedAt
   * @fn    virtual bool IsDefinedAt(const PointType<NDim, T>& x) const = 0
   * @brief Check if function is defined at `x`. Must be implemented by subclass
   * @param[in] x Location where to look for definition
   * @return    `true` if function is defined at `x` otherwise `false`.
   */
  virtual bool IsDefinedAt(const PointType<NDim, T>& x) const = 0;

  /**
   * @name  FindClosestPoint
   * @fn    virtual PtType FindClosestPoint(const PtType& x) const = 0
   * @brief Look for closest point to `x` where function is defined. Must be
   *    implemented by subclass
   * @param[in] x   Query points
   * @return    Closest point
   */
  virtual PtType FindClosestPoint(const PtType& x) const = 0;

  /**
   * @name  FindNClosestPoint
   * @fn    virtual PointList FindNClosestPoint(const PtType& x,
   *                                            size_t n) const = 0
   * @brief Look for N closest point to `x` where function is defined. Must be
   *    implemented by subclass
   * @param[in] x   Query point
   * @param[in] n   Number of points to look for
   * @return    List closest point
   */
  virtual PointList FindNClosestPoint(const PtType& x, size_t n) const = 0;

  /**
   * @name  Index
   * @fn    virtual int Index(const PtType& x) const = 0
   * @brief Look for index of a given point `x`, if not known, returns -1
   * @param[in] x   Query point
   * @return Index of the query point or -1 if not part of domain
   */
  virtual int Index(const PtType& x) const = 0;

  /**
   * @name  Transform
   * @fn    void Transform(const TrsfrmFcn& fcn, PointList* points) const
   * @brief Apply a given transform on the points of the domain.
   * @param[in] fcn     Function to apply on the domain
   * @param[in] points  Transformed points
   */
  void Transform(const TrsfrmFcn& fcn, PointList* points) const {
    points->clear();
    for (const auto& p : points_) {
      points->emplace_back(fcn(p));
    }
  }

  /**
   * @name  Size
   * @fn    size_t Size() const
   * @brief Domain size (i.e. number of points)
   * @return Number of point in the domain
   */
  size_t Size() const {
    return points_.size();
  }

  /**
   * @name  get_points
   * @fn    const PointList& get_points() const
   * @brief Accessor for underlying points in the domain
   * @return List of points in the domain.
   */
  const PointList& get_points() const {
    return points_;
  }

  /**
   * @name  operator[]
   * @fn    const PtType& operator[](int idx) const
   * @brief Access element at position `idx`.
   * @param[in] idx Index of the element to access.
   * @return    Element at position `idx`
   */
  const PtType& operator[](int idx) const {
    return points_[idx];
  }

  /**
   * @name  bbox
   * @fn const BoxDomPtr& bbox() const
   * @brief Return the smallest continuous box domain that fully contains all
   *    the domain points.
   * @return    BoxDomain shared pointer
   */
  const BoxDomPtr& bbox() const {
    return bbox_;
  }

 protected:
  /** Points */
  std::vector<PtType> points_;
  /** Domain bounding box */
  BoxDomPtr bbox_;
};

#pragma mark -
#pragma mark UnsrtucturedPointDomain

/**
 * @class   UnstructuredPointDomain
 * @brief   Domain of definition for function defined only on a subset of points
 * @author  Christophe Ecabert
 * @date    15/12/2020
 * @ingroup kernel
 * @tparam NDim Number of dimensions {1, 2, 3, 4}
 * @tparam T    Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS UnstructuredPointDomain : public DiscreteDomain<NDim, T> {
 public:
  /** Point */
  using PtType = PointType<NDim, T>;
  /** List of point */
  using PointList = std::vector<PtType>;
  /** Pointer */
  using Ptr = std::shared_ptr<UnstructuredPointDomain<NDim, T>>;

  /**
   * @name  Create
   * @fn    static Ptr Create()
   * @brief Create object wrapped into shared pointer.
   * @return    Object wrapped in shared pointer.
   */
  static Ptr Create();

  /**
   * @name  Create
   * @fn    static Ptr Create(const PointList& points)
   * @brief Create object wrapped into shared pointer.
   * @param[in] points  Points in the domain
   * @return    Object wrapped in shared pointer.
   */
  static Ptr Create(const PointList& points);

  /**
   * @name  UnstructuredPointDomain
   * @fn    UnstructuredPointDomain()
   * @brief Constructor
   */
  UnstructuredPointDomain();

  /**
   * @name  UnstructuredPointDomain
   * @fn    explicit UnstructuredPointDomain(const PointList& points)
   * @brief Constructor
   * @param[in] points  Points in the domain
   */
  explicit UnstructuredPointDomain(const PointList& points);

  using DiscreteDomain<NDim, T>::Load;
  using DiscreteDomain<NDim, T>::Save;

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream) override
   * @brief Load domain from a binary stream
   * @param[in] stream  Stream to read
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream) override;

  /**
   * @name  Save
   * @fn    int Save(std::ostream& stream) const override
   * @brief Save domain to a binary stream
   * @param[in] stream  Stream to read
   * @return    -1 if error, 0 otherwise
   */
  int Save(std::ostream& stream) const override;

  /**
   * @name  ObjectSize
   * @fn    int ObjectSize() const override
   * @brief Compute object size in bytes
   * @return    Object size in bytes
   */
  int ObjectSize() const override;

  /**
   * @name  IsDefinedAt
   * @fn    bool IsDefinedAt(const PointType<NDim, T>& x) const
   * @brief Check if function is defined at `x`.
   * @return    `true` if function is defined at `x` otherwise `false`.
   */
  bool IsDefinedAt(const PointType<NDim, T>& x) const override;

  /**
   * @name  FindClosestPoint
   * @fn    PtType FindClosestPoint(const PtType& x) const override
   * @brief Look for closest point to `x` where function is defined.
   * @param[in] x   Query points
   * @return    Closest point
   */
  PtType FindClosestPoint(const PtType& x) const override;

  /**
   * @name  FindNClosestPoint
   * @fn    PointList FindNClosestPoint(const PtType& x, size_t n) const
   *    override
   * @brief Look for N closest point to `x` where function is defined.
   * @param[in] x   Query points
   * @param[in] n   Number of points to look for
   * @return    List closest point
   */
  PointList FindNClosestPoint(const PtType& x, size_t n) const override;

  /**
   * @name  Index
   * @fn    int Index(const PtType& x) const override
   * @brief Look for index of a given point `x`, if not known, returns -1
   * @param[in] x   Query point
   * @return Index of the query point or -1 if not part of domain
   */
  int Index(const PtType& x) const override;

 private:

  /**
   * @name  Init
   * @brief Init internal structure
   */
  void Init();

  /** KDTree */
  using Tree = KDTree<NDim, T>;
  /** Point maps */
  using PointMap = std::unordered_map<PointType<NDim, T>, size_t>;
  /** KDTree */
  Tree kdtree_;
  /** Point maps */
  PointMap point_map_;
};

/**
 * @class   TriangleMesh
 * @brief   Domain of definition for function defined on a mesh. Domain also
 *  include the mesh topology.
 * @author  Christophe Ecabert
 * @date    29/12/2020
 * @ingroup kernel
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS TriangleMesh : public UnstructuredPointDomain<3, T> {
 public:
  /** Point */
  using PtType = PointType<3, T>;
  /** List of point */
  using PointList = std::vector<PtType>;
  /** Triangles */
  using TriType = Vector3<int>;
  /** Triangle list */
  using TriList = std::vector<TriType>;
  /** Mesh type */
  using MeshType = Mesh<T>;
  /** Ptr */
  using Ptr = std::shared_ptr<TriangleMesh<T>>;

  /**
   * @name  Create
   * @fn    static Ptr Create()
   * @brief Create object wrapped into shared pointer.
   * @return    Object wrapped in shared pointer.
   */
  static Ptr Create();

  /**
   * @name  Create
   * @fn    static Ptr Create()
   * @brief Create object wrapped into shared pointer.
   * @param[in] points      Point of the mesh (i.e. vertex)
   * @param[in] topology    Mesh topology (i.e. triangulation)
   * @return    Object wrapped in shared pointer.
   */
  static Ptr Create(const PointList& points, const TriList& topology);

  /**
   * @name  Create
   * @fn    static Ptr Create()
   * @brief Create object wrapped into shared pointer.
   * @param[in] mesh      Mesh instance
   * @return    Object wrapped in shared pointer.
   */
  static Ptr Create(const MeshType& mesh);


  /**
   * @name  TriangleMesh
   * @fn    TriangleMesh
   * @brief Constructor
   */
  TriangleMesh();

  /**
   * @name  TriangleMesh
   * @fn    TriangleMesh(const PointList& points,
   *                     const TriList& topology)
   * @brief Constructor
   * @param[in] points      Point of the mesh (i.e. vertex)
   * @param[in] topology    Mesh topology (i.e. triangulation)
   */
  TriangleMesh(const PointList& points, const TriList& topology);

  /**
   * @name  TriangleMesh
   * @fn    explicit TriangleMesh(const MeshType& mesh)
   * @brief Constructor
   * @param[in] mesh    Mesh instance
   */
  explicit TriangleMesh(const MeshType& mesh);

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream) override
   * @brief Load domain from a binary stream
   * @param[in] stream  Stream to read
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream) override;

  /**
   * @name  Save
   * @fn    int Save(std::ostream& stream) const override
   * @brief Save domain to a binary stream
   * @param[in] stream  Stream to read
   * @return    -1 if error, 0 otherwise
   */
  int Save(std::ostream& stream) const override;

  /**
   * @name  ObjectSize
   * @fn    int ObjectSize() const override
   * @brief Compute object size in bytes
   * @return    Object size in bytes
   */
  int ObjectSize() const override;

  /**
   * @name  FindClosestPoint
   * @fn    PtType FindClosestPoint(const PtType& x) const override
   * @brief Look for closest point to `x` where function is defined.
   * @param[in] x   Query points
   * @return    Closest point
   */
  PtType FindClosestPoint(const PtType& x) const override;

  /**
   * @name  get_triangles
   * @fn    const std::vector<TriType>& get_triangles() const
   * @brief Access domain topology
   * @return    List of triangles
   */
  const std::vector<TriType>& get_triangles() const {
    return tri_;
  }

 private:
  /** Mesh topology */
  std::vector<TriType> tri_;
  /** Bounding sphere tree */
  BoundingSphereTree<T> bs_tree_;
};

}  // namespace LTS5
#endif  // __LTS5_KERNEL_DOMAIN__
