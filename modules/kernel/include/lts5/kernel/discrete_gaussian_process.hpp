/**
 *  @file   discrete_gaussian_process.hpp
 *  @brief  Discrete Gaussian Process
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   29/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_DISCRETE_GAUSSIAN_PROCESS__
#define __LTS5_DISCRETE_GAUSSIAN_PROCESS__

#include <memory>
#include <cassert>

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/kernel/domain.hpp"
#include "lts5/kernel/field.hpp"
#include "lts5/kernel/matrix_kernel.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   DiscreteGaussianProcess
 * @brief   Discrete Gaussian Process
 * @author  Christophe Ecabert
 * @date    29/12/2020
 * @ingroup kernel
 * @tparam IDim Number of input's dimension
 * @tparam ODim Number of output's dimension
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS DiscreteGaussianProcess {
 public:
  /** Discrete Domain pointer */
  using DDomainPtr = std::shared_ptr<DiscreteDomain<IDim, T>>;
  /** Discrete Field pointer */
  using DFieldPtr = std::shared_ptr<DiscreteField<IDim, ODim, T>>;
  /** Covariance kernel matrix */
  using DMatKernelPtr = std::shared_ptr<DiscreteMatrixKernel<IDim, ODim, T>>;
  /** Input point type */
  using InPtType = PointType<IDim, T>;
  /** Output point type */
  using OutPtType = PointType<ODim, T>;
  /** Matrix type */
  using MatType = typename Matrix<ODim, T>::Type;

  /**
   * @name  DiscreteGaussianProcess
   * @fn    DiscreteGaussianProcess(const DFieldPtr& mean,
   *                        const DMatKernelPtr& covariance)
   * @brief Constructor
   * @param[in] mean        Mean value
   * @param[in] covariance  Covariance kernel
   */
  DiscreteGaussianProcess(const DFieldPtr& mean,
                          const DMatKernelPtr& covariance);

  /**
   * @name  Mean
   * @fn    OutPtType Mean(const size_t& index) const
   * @brief Evaluate mean value at a given index
   * @param[in] index  Location where to evaluate mean
   * @return    Mean at `x`
   */
  OutPtType Mean(const size_t& index) const;

  /**
   * @name  Covariance
   * @fn    MatType Covariance(const size_t& i, const size_t& j) const
   * @brief Evaluate covariance at a given index `i` and `j`
   * @param[in] i   First point index
   * @param[in] j   Second point index
   * @return    cov(i, j)
   */
  MatType Covariance(const size_t& i, const size_t& j) const;

 private:
  /** Domain of definition */
  DDomainPtr dom_;
  /** Mean */
  DFieldPtr mean_;
  /** Covariance */
  DMatKernelPtr cov_;
};

#pragma mark -
#pragma mark Implementation

template<int IDim, int ODim, typename T>
DiscreteGaussianProcess<IDim, ODim, T>::
DiscreteGaussianProcess(const DFieldPtr& mean,
                        const DMatKernelPtr& covariance) :
        dom_(mean->get_domain()),
        mean_(mean),
        cov_(covariance) {
  if (mean_->get_domain() != cov_->get_domain()) {
    throw std::runtime_error("Domain of definition of `mean` and `covariance` "
                             "are not the same");
  }
}

template<int IDim, int ODim, typename T>
typename DiscreteGaussianProcess<IDim, ODim, T>::OutPtType
DiscreteGaussianProcess<IDim, ODim, T>::Mean(const size_t& index) const {
  return mean_->operator[](index);
}

template<int IDim, int ODim, typename T>
typename DiscreteGaussianProcess<IDim, ODim, T>::MatType
DiscreteGaussianProcess<IDim, ODim, T>::
Covariance(const size_t& i, const size_t& j) const {
  return cov_->operator()(i, j);
}

}  // namespace LTS5
#endif //__LTS5_DISCRETE_GAUSSIAN_PROCESS__
