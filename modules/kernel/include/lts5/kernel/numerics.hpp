/**
 *  @file   numerics.hpp
 *  @brief  Numerical utility functions
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   10/02/21
 *  Copyright (c) 2021 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_NUMERICS__
#define __LTS5_KERNEL_NUMERICS__

#include <cmath>
#include <limits>
#include <functional>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   BSpline
 * @brief   Univariate BSpline function
 * @author  Christophe Ecabert
 * @date    10/02/2021
 * @ingroup kernel
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS BSpline {
 public:
  /**
   * @name  BSpline
   * @fn    explicit BSpline(size_t order)
   * @brief Constructor
   * @param[in] order Spline order
   */
  explicit BSpline(size_t order);

  /**
   * @name  operator()
   * @fn    T operator()(const T& x) const
   * @brief Evaluate spline function at a given `x` value.
   * @param[in] x   Where to evaluate function
   * @return    Value
   */
  T operator()(const T& x) const;

 private:
  /** Evaluation function */
  std::function<T(const T&)> evaluator_fcn_;
};
}  // namespace LTS5
#endif // __LTS5_KERNEL_NUMERICS__
