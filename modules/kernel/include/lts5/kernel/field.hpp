/**
 *  @file   field.hpp
 *  @brief  Represent a function from points to values, together with a domain
 *      on which the function is defined.
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   17/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_FIELD__
#define __LTS5_KERNEL_FIELD__

#include <memory>
#include <cassert>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/kernel/domain.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<int NDim, typename T>
using PointType = typename Point<NDim, T>::Type;

/**
 * @class   FieldFunction
 * @brief   Implement a function mapping points into values.
 * @author  Christophe Ecabert
 * @date    17/12/2020
 * @ingroup kernel
 * @tparam IDim     Number of input's dimension
 * @tparam ODim     Number of output's dimension
 * @tparam T        Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS FieldFunction {
 public:
  /**
   * @name  ~FieldFunction
   * @fn    virtual ~FieldFunction() = default
   * @brief Destructor
   */
  virtual ~FieldFunction() = default;

  /**
   * @name  operator()
   * @fn    virtual PointType<ODim, T> operator()(const PointType<IDim, T>& x) const = 0
   * @brief Evaluate the underlying function at position `x`.
   * @param[in] x   Location where to evaluate the function
   * @return    Results
   */
  virtual PointType<ODim, T> operator()(const PointType<IDim, T>& x) const = 0;
};

/**
 * @class   FieldFunction
 * @brief   Region with function mapping points into values.
 * @author  Christophe Ecabert
 * @date    17/12/2020
 * @tparam IDim     Number of input's dimension
 * @tparam IDim     Number of output's dimension
 * @tparam T        Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS Field {
 public:
  /** Function ptr */
  using FcnType = std::shared_ptr<FieldFunction<IDim, ODim, T>>;
  /** Domain ptr */
  using DomType = std::shared_ptr<Domain<IDim, T>>;

  /**
   * @name  Field
   * @fn    Field(const DomType& domain, const FcnType& fcn)
   * @brief Constructor
   * @param[in] domain  Domain definition of the function
   * @param[in] fcn Mapping function to apply
   */
  Field(const DomType& domain, const FcnType& fcn) : dom_(domain),
                                                     fcn_(fcn) {
  }

  /**
   * @name  IsDefinedAt
   * @fn    bool IsDefinedAt(const PointType<IDim, T>& x) const
   * @brief Check if field is defined at a given location `x`.
   * @param[in] x   Point to check definition
   * @return    `true` if defined, `false` otherwise
   */
  bool IsDefinedAt(const PointType<IDim, T>& x) const {
    return dom_->IsDefinedAt(x);
  }

  /**
   * @name  operator()
   * @fn    PointType<ODim, T> operator()(const PointType<IDim, T>& x) const
   * @brief Evaluate field at location `x`
   * @param[in] x   Location where to evaluate the underlying functino.
   * @return Value
   */
  PointType<ODim, T> operator()(const PointType<IDim, T>& x) const {
    if (!this->IsDefinedAt(x)) {
      throw std::runtime_error("Point `x` outside domain of definition!");
    }
    return fcn_->operator()(x);
  }

  /**
   * @name domain
   * @fn    DomType domain() const
   * @brief Provide domain of definition of the field
   * @return    Domain
   */
  DomType domain() const {
    return dom_;
  }

 private:
  /** Domain */
  DomType dom_;
  /** Mapping function */
  FcnType fcn_;
};

/**
 * @class   ZeroVectorField
 * @brief   Field mapping any point into zero vector.
 * @author  Christophe Ecabert
 * @date    17/12/2020
 * @ingroup kernel
 * @tparam IDim Number of input's dimension
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS ZeroVectorField : public Field<IDim, ODim, T> {
 public:
  /** Base class type */
  using BaseType = Field<IDim, ODim, T>;
  /** Point Type */
  using PtType = PointType<IDim, T>;

  /**
   * @name  ZeroVectorField
   * @fn    ZeroVectorField()
   * @brief Constructor
   */
  ZeroVectorField() : BaseType(EuclideanSpace<IDim, T>::Create(),
                               std::make_shared<ZeroVectorFcn>()) {}

 private:
  /**
   * @class   ZeroVectorFcn
   * @brief   Function mapping any value to a zero vector.
   * @author  Christophe Ecabert
   * @date    17/12/2020
   * @tparam IDim Number of input's dimensions
   * @tparam ODim Number of output's dimensions
   * @tparam T    Data type
   */
  class ZeroVectorFcn : public FieldFunction<IDim, ODim, T> {
   public:
    /**
     * @name    ZeroVectorFcn
     * @fn  ZeroVectorFcn()
     * @brief Constructor
     */
    ZeroVectorFcn() : value_() {}
    /**
     * @name  operator()
     * @fn PtType operator()(const PointType<IDim, T>& x) const override
     * brief  Function mapping any value to a zero vector.
     * @param[in] x   Location where to evaluate the function.
     * @return    Zero vector.
     */
    PointType<ODim, T> operator()(const PointType<IDim, T>& x) const override {
      return value_;
    }

   private:
    /** Value */
    PointType<ODim, T> value_;
  };
};

/**
 * @class   DiscreteField
 * @brief   Represent a function from points to values, together with a domain
 *      on which the function is defined.
 * @author  Christophe Ecabert
 * @date    23/12/2020
 * @ingroup kernel
 * @tparam IDim     Number of input's dimension
 * @tparam ODim     Number of output's dimension
 * @tparam T        Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS DiscreteField {
 public:
  /** Discrete domain pointer */
  using DDomPtr = std::shared_ptr<DiscreteDomain<IDim, T>>;
  /** Input point type */
  using InPtType = PointType<IDim, T>;
  /** Output point type */
  using OutPtType = PointType<ODim, T>;
  /** Pointer */
  using DFieldPtr = std::shared_ptr<DiscreteField<IDim, ODim, T>>;


  /**
   * @name  Create
   * @fn    static DFieldPtr Create(const DDomPtr& domain,
                                    const std::vector<OutPtType>& data)
   * @brief Create object wrapped into shared pointer.
   * @param[in] domain  Discrete domain
   * @param[in] data    Data for the corresponding points in the domain.
   * @return    Wrapped object
   */
  static DFieldPtr Create(const DDomPtr& domain,
                          const std::vector<OutPtType>& data) {
    return std::make_shared<DiscreteField<IDim, ODim, T>>(domain,
            data);
  }

  /**
   * @name  Create
   * @fn    static DFieldPtr Create(const DDomPtr& domain, const cv::Mat& data)
   * @brief Create object wrapped into shared pointer.
   * @param[in] domain  Discrete domain
   * @param[in] data    Data for the corresponding points in the domain.
   * @return    Wrapped object
   */
  static DFieldPtr Create(const DDomPtr& domain,
                          const cv::Mat& data) {
    return std::make_shared<DiscreteField<IDim, ODim, T>>(domain,
                                                          data);
  }

  /**
   * @name  DiscreteField
   * @fn    DiscreteField(const DDomPtr &domain,
                const std::vector<OutPtType> &data)
   * @brief Constructor
   * @param[in] domain  Discrete domain
   * @param[in] data    Data for the corresponding points in the domain.
   */
  DiscreteField(const DDomPtr &domain,
                const std::vector<OutPtType> &data) : domain_(domain),
                                                     data_(data) {
  }

  /**
   * @name  DiscreteField
   * @fn    DiscreteField(const DDomPtr& domain,
                          const cv::Mat& data)
   * @brief Constructor
   * @param[in] domain  Discrete domain
   * @param[in] data    Vector storing stack points
   */
  DiscreteField(const DDomPtr &domain, const cv::Mat& data) :
          domain_(domain),
          data_() {
    assert(data.rows % ODim == 0 && "Data must be a multiple of output dims.");
    int n_elem = data.rows / ODim;
    for (int i = 0; i < n_elem; ++i) {
      data_.emplace_back(data.at<OutPtType>(i));
    }
  }
  /**
   * @name  IsDefinedAt
   * @fn    bool IsDefinedAt(const size_t& index) const
   * @brief Check if field is defined for a given index.
   * @param[in] index   Index to check for definition.
   * @return    `True` if defined, `false` otherwise.
   */
  bool IsDefinedAt(const size_t &index) const {
    return index >= 0 && index < data_.size();
  }

  /**
   * @name  operator[]
   * @fn    const OutPtType& operator[](const size_t &index) const
   * @brief Access data at a given index.
   * @param[in] index   Index to query
   * @return    Corresponding data for the given index
   */
  const OutPtType& operator[](const size_t &index) const {
    return data_[index];
  }

  /**
   * @name  get_points
   * @fn    const std::vector<InPtType>& get_points() const
   * @brief Access location (i.e. points) where field is defined.
   * @return    List of points
   */
  const std::vector<InPtType> &get_points() const {
    return domain_->get_points();
  }

  /**
   * @name  get_data
   * @fn    const std::vector<OutPtType>& get_data() const
   * @brief Access data for the corresponding underlying points.
   * @return    List of data values.
   */
  const std::vector<OutPtType>& get_data() const {
    return data_;
  }

  /**
   * @name  get_domain
   * @fn    const DDomPtr& get_domain() const
   * @brief Return domain of definition
   * @return    Domain
   */
  const DDomPtr& get_domain() const {
    return domain_;
  }

 private:
  /** Domain */
  DDomPtr domain_;
  /** Data */
  std::vector<OutPtType> data_;
};

}  // namespace LTS5
#endif // __LTS5_KERNEL_FIELD__
