/**
 *  @file   kernel.hpp
 *  @brief  Gaussian kernel abstraction
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   01/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_KERNEL__
#define __LTS5_KERNEL_KERNEL__

#include <cmath>
#include <memory>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/sys/cache.hpp"
#include "lts5/kernel/domain.hpp"
#include "lts5/kernel/numerics.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<int NDim, typename T>
using PointType = typename Point<NDim, T>::Type;

/**
 * @class   Kernel
 * @brief   Interface for GP covariance kernel definition
 * @author  Christophe Ecabert
 * @date    30/11/2020
 * @ingroup kernel
 * @tparam NDim Number of dimensions {1, 2, 3, 4}
 * @tparam T    Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS Kernel {
 public:
  /** Domain Type */
  using DomType = std::shared_ptr<Domain<NDim, T>>;
  /** Data type */
  using Type = T;
  /** Number of dimensions */
  static const int kDims = NDim;
  /** Point type */
  using PtType = PointType<NDim, T>;

  /**
   * @name  Kernel
   * @fn    explicit Kernel(const DomType& domain)
   * @brief Construction
   * @param[in] domain Domain of definition of the kernel
   */
  explicit Kernel(const DomType& domain) : Kernel<NDim, T>(domain, 0) {}

  /**
   * @name  Kernel
   * @fn    Kernel(const DomType& domain, size_t cache_size)
   * @brief Constructor
   * @param[in] domain      Domain of definition of the kernel
   * @param[in] cache_size  Cache size
   */
  Kernel(const DomType& domain, size_t cache_size) :
          domain_(domain),
          cache_(),
          cache_size_(cache_size) {
    using Cache_t = Cache<T(const PtType&, const PtType&)>;
    cache_ = std::make_unique<Cache_t>(cache_size,
                                       [this](const PtType& x,
                                              const PtType& y) -> T {
                                         return this->k(x, y);
                                       });
  }

  /**
   * @name  ~Kernel
   * @fn    ~Kernel() = default
   * @brief Destructor
   */
  virtual ~Kernel() = default;

  /**
   * @name  operator()
   * @fn    T operator()(const PointType<NDim, T>& x,
                    const PointType<NDim, T>& y) const
   * @brief Compute the scalar value for the underlying kernel between two
   *    points. Check that inputs are in the domain of definition of the kernel
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    k(x, y)
   */
  T operator()(const PointType<NDim, T>& x,
               const PointType<NDim, T>& y) const {
    if (domain_->IsDefinedAt(x) && domain_->IsDefinedAt(y)) {
      if (cache_size_) {
        return cache_->GetOrComputeValue(x, y);
      } else {
        return this->k(x, y);
      }
    } else {
      if (!domain_->IsDefinedAt(x)) {
        throw std::runtime_error("`x` is not part of the domain");
      } else {
        throw std::runtime_error("`y` is not part of the domain");
      }
    }
  }

  /**
   * @name  domain
   * @fn    const DomType& domain() const
   * @brief  Provide domain of definition of the kernel
   * @return    Shared domain instance
   */
  const DomType& domain() const {
    return domain_;
  }

 protected:
  /**
   * @name  k
   * @fn    virtual T k(const PointType<NDim, T>& x,
   *                    const PointType<NDim, T>& y) const = 0
   * @brief Compute the output scalar value for the kernel between two points
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Value
   */
  virtual T k(const PointType<NDim, T>& x,
              const PointType<NDim, T>& y) const = 0;

 private:
  using CacheType = Cache<T(const PtType&, const PtType&)>;
  /** Function definition domain */
  DomType domain_;
  /** Cache */
  std::unique_ptr<CacheType> cache_;
  /** Cache size */
  size_t cache_size_;
};

/**
 * @enum  KernelComposerType
 * @brief Type of composition for `Kernel` object
 */
enum KernelComposerType {
  /** Kernel Addition */
  kAdd,
  /** Kernel Multiplication */
  kMultiply,
  /** Kernel Scaling */
  kScale,
  /** Kernel Transform */
  kTransform
};

/**
 * @struct  KernelComposerSelector
 * @brief   Select merge operation to apply to fuse two kernel outputs
 * @date 01/12/2020
 * @ingroup kernel
 * @tparam NDim Number of dimension
 * @tparam T    Data type
 */
template<int NDim, typename T, KernelComposerType Op>
struct KernelComposerSelector;

template<int NDim, typename T>
struct KernelComposerSelector<NDim, T, KernelComposerType::kAdd> {
  T operator()(const std::shared_ptr<Kernel<NDim, T>>& k1,
               const std::shared_ptr<Kernel<NDim, T>>& k2,
               const T& scale,
               const PointType<NDim, T>& x,
               const PointType<NDim, T>& y) {
    return k1->operator()(x, y) + k2->operator()(x, y);
  }
};

template<int NDim, typename T>
struct KernelComposerSelector<NDim, T, KernelComposerType::kMultiply> {
  T operator()(const std::shared_ptr<Kernel<NDim, T>>& k1,
               const std::shared_ptr<Kernel<NDim, T>>& k2,
               const T& scale,
               const PointType<NDim, T>& x,
               const PointType<NDim, T>& y) {
    return k1->operator()(x, y) * k2->operator()(x, y);
  }
};

template<int NDim, typename T>
struct KernelComposerSelector<NDim, T, KernelComposerType::kScale> {
  T operator()(const std::shared_ptr<Kernel<NDim, T>>& k1,
               const std::shared_ptr<Kernel<NDim, T>>& k2,
               const T& scale,
               const PointType<NDim, T>& x,
               const PointType<NDim, T>& y) {
    return k1->operator()(x, y) * scale;
  }
};

template<int NDim, typename T>
struct KernelComposerSelector<NDim, T, KernelComposerType::kTransform> {
  T operator()(const std::shared_ptr<Kernel<NDim, T>>& k1,
               const std::shared_ptr<Kernel<NDim, T>>& k2,
               const T& scale,
               const PointType<NDim, T>& x,
               const PointType<NDim, T>& y) {
    return k1->operator()(x, y);
  }
};

/**
 * @struct  PtsTransformerDefault
 * @brief   Default transformation applied on kernel inputs, i.e. identity
 *  mapping
 * @date 01/12/2020
 * @ingroup kernel
 * @tparam NDim Number of dimension
 * @tparam T    Data type
 */
template<int NDim, typename T>
struct LTS5_EXPORTS PtsTransformerDefault {
  PointType<NDim, T> operator()(const PointType<NDim, T>& x) {
    return x;
  }
};

/**
 * @class   KernelComposerBase
 * @brief   Base class to combine two kernels into a single one.
 * @author  Christophe Ecabert
 * @date    01/12/2020
 * @ingroup kernel
 * @tparam NDim Number of dimensions
 * @tparam T    Data type
 */
template<int NDim, typename T, KernelComposerType Op>
class LTS5_EXPORTS KernelComposerBase : public Kernel<NDim, T> {
 public:
  /** Pointer type */
  using Ptr = std::shared_ptr<KernelComposerBase<NDim, T, Op>>;
  /** Kernel type */
  using KernelType = std::shared_ptr<Kernel<NDim, T>>;
  /** Domain type */
  using DomType = std::shared_ptr<Domain<NDim, T>>;
  /** Point transform function */
  using PtTrsfrmFn = std::function<PointType<NDim, T>(const PointType<NDim, T>&)>;

  /**
   * @name  KernelComposerBase
   * @fn    KernelComposerBase(const KernelType& k1, const KernelType& k2,
   *                        const DomType& domain, const T& scale,
   *                        PtTrsfrmFn fn)
   * @brief Constructor
   * @param[in] k1  First kernel
   * @param[in] k2  Second kernel
   * @param[in] domain  Domain of definition of `k1` and `k2`
   * @param[in] scale   Scaling factor
   * @param[in] fn  Function to transform point before estimating kernel
   */
  KernelComposerBase(const KernelType& k1,
                     const KernelType& k2,
                     const DomType& domain,
                     const T& scale,
                     PtTrsfrmFn fn) : Kernel<NDim, T>(domain),
                                      k1_(k1),
                                      k2_(k2),
                                      scale_(scale),
                                      fn_(fn) {}

 protected:
  /**
   * @name  k
   * @fn    T k(const PointType<NDim, T>& x, const PointType<NDim, T>& y) const
   * @brief Compute the output scalar value for the kernel between two points
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Value
   */
  T k(const PointType<NDim, T>& x,
      const PointType<NDim, T>& y) const override {
    // Apply transform
    auto xt = fn_(x);
    auto yt = fn_(y);
    return KernelComposerSelector<NDim, T, Op>()(k1_, k2_, scale_, xt, yt);
  }

 private:
  /** First kernel */
  KernelType k1_;
  /** Second kernel */
  KernelType k2_;
  /** Scalar */
  T scale_;
  /** Transform */
  PtTrsfrmFn fn_;
};

/**
 * @class   KernelComposer
 * @brief   Factory class to combine two kernels into a single one.
 * @author  Christophe Ecabert
 * @date    01/12/2020
 * @ingroup kernel
 * @tparam NDim Number of dimensions
 * @tparam T    Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS KernelComposer {
 public:
  /** Kernel type */
  using KernelType = std::shared_ptr<Kernel<NDim, T>>;
  /** Domain type */
  using DomType = std::shared_ptr<Domain<NDim, T>>;
  /** Point transform function */
  using PtTrsfrmFn = std::function<PointType<NDim, T>(const PointType<NDim, T>&)>;
  /** Base pointer type */
  template<KernelComposerType OpType>
  using BaseType = KernelComposerBase<NDim, T, OpType>;
  /** Add composition */
  using AddPtr = std::shared_ptr<BaseType<KernelComposerType::kAdd>>;
  /** Multiply composition */
  using MultiplyPtr = std::shared_ptr<BaseType<KernelComposerType::kMultiply>>;
  /** Scale composition */
  using ScalePtr = std::shared_ptr<BaseType<KernelComposerType::kScale>>;
  /** Scale composition */
  using TransformPtr = std::shared_ptr<BaseType<KernelComposerType::kTransform>>;
  /**
   * @name  Add
   * @fn    static Ptr Add(const KernelType& k1, const KernelType& k2)
   * @brief Add two kernels together
   * @param[in] k1  First kernel
   * @param[in] k2  Second kernel
   * @return Kernel computing k(x,y) = k1(x, y) + k2(x, y)
   */
  static AddPtr Add(const KernelType& k1, const KernelType& k2) {
    using KType = KernelComposerBase<NDim, T, KernelComposerType::kAdd>;
    auto domain = DomainComposer<NDim, T>::Intersection(k1->domain(),
                                                        k2->domain());
    return std::make_shared<KType>(k1,
                                   k2,
                                   domain,
                                   T(1.0),
                                   PtsTransformerDefault<NDim, T>());
  }

  /**
   * @name  Multiply
   * @fn    static Ptr Multiply(const KernelType& k1, const KernelType& k2)
   * @brief Multiply two kernels together
   * @param[in] k1  First kernel
   * @param[in] k2  Second kernel
   * @return Kernel computing k(x,y) = k1(x, y) * k2(x, y)
   */
  static MultiplyPtr Multiply(const KernelType& k1, const KernelType& k2) {
    using KType = KernelComposerBase<NDim, T, KernelComposerType::kMultiply>;
    auto domain = DomainComposer<NDim, T>::Intersection(k1->domain(),
                                                        k2->domain());
    return std::make_shared<KType>(k1,
                                   k2,
                                   domain,
                                   T(1.0),
                                   PtsTransformerDefault<NDim, T>());
  }

  /**
   * @name  Multiply
   * @fn    static Ptr Scale(const KernelType& k1, const T& scale)
   * @brief Scale a kernel by a scalar value
   * @param[in] k1 First kernel
   * @param[in] scale  Scalar value
   * @return Kernel computing k(x,y) = k1(x, y) * scale
   */
  static ScalePtr Scale(const KernelType& k1, const T& scale) {
    using KType = KernelComposerBase<NDim, T, KernelComposerType::kScale>;
    return std::make_shared<KType>(k1,
                                   KernelType(),
                                   k1->domain(),
                                   scale,
                                   PtsTransformerDefault<NDim, T>());
  }

  /**
   * @name  Transform
   * @fn    static Ptr Transform(const KernelType& k1, PtTrsfrmFn fn);
   * @brief Scale a kernel by a scalar value
   * @param[in] k1 First kernel
   * @param[in] fn Function to apply to `x` and `y` before evaluating `k`
   * @return Kernel computing k(x,y) = k1(fn(x), fn(y))
   */
  static TransformPtr Transform(const KernelType& k1, PtTrsfrmFn fn) {
    using KType = KernelComposerBase<NDim, T, KernelComposerType::kTransform>;
    return std::make_shared<KType>(k1,
                                   KernelType(),
                                   k1->domain(),
                                   T(1.0),
                                   fn);
  }
};

template<typename KernelType>
std::shared_ptr<Kernel<KernelType::kDims, typename KernelType::Type>>
operator+(const std::shared_ptr<KernelType>& k1,
          const std::shared_ptr<KernelType>& k2) {
  using KComp = KernelComposer<KernelType::kDims, typename KernelType::Type>;
  return KComp::Add(k1, k2);
}

template<typename KernelType>
std::shared_ptr<Kernel<KernelType::kDims, typename KernelType::Type>>
operator*(const std::shared_ptr<KernelType>& k1,
          const std::shared_ptr<KernelType>& k2) {
  using KComp = KernelComposer<KernelType::kDims, typename KernelType::Type>;
  return KComp ::Multiply(k1, k2);
}

template<typename KernelType>
std::shared_ptr<Kernel<KernelType::kDims, typename KernelType::Type>>
operator*(const std::shared_ptr<KernelType>& k1,
          const typename KernelType::Type& s) {
  using KComp = KernelComposer<KernelType::kDims, typename KernelType::Type>;
  return KComp::Scale(k1, s);
}


/**
 * @class   GaussianKernel
 * @brief   Gaussian kernel
 * @author  Christophe Ecabert
 * @date    01/12/2020
 * @ingroup kernel
 * @tparam NDim Number of dimensions
 * @tparam T    Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS GaussianKernel : public Kernel<NDim, T> {
 public:
  /** Pointer type */
  using Ptr = std::shared_ptr<GaussianKernel<NDim, T>>;

  /**
   * @name  Create
   * @fn    static Ptr Create(const T& stddev)
   * @brief Create object wrapped in a shared_ptr.
   * @param[in] stddev  Standard deviation of the gaussian
   * @return    Shared instance
   */
  static Ptr Create(const T& stddev) {
    return std::make_shared<GaussianKernel<NDim, T>>(stddev);
  }

  /**
   * @name  Create
   * @fn    static Ptr Create(const T& stddev, const size_t& cache_size)
   * @brief Create object wrapped in a shared_ptr.
   * @param[in] stddev  Standard deviation of the gaussian
   * @param[in] cache_size  Cache size
   * @return    Shared instance
   */
  static Ptr Create(const T& stddev, const size_t& cache_size) {
    return std::make_shared<GaussianKernel<NDim, T>>(stddev, cache_size);
  }

  /**
   * @name  GaussianKernel
   * @fn    explicit GaussianKernel(const T& stddev)
   * @brief Constructor
   * @param[in] stddev Standard deviation of the gaussian
   */
  explicit GaussianKernel(const T& stddev);

  /**
   * @name  GaussianKernel
   * @fn    explicit GaussianKernel(const T& stddev, const size_t& cache_size)
   * @brief Constructor with cache mechanism enable
   * @param[in] stddev Standard deviation of the gaussian
   * @param[in] cache_size  Cache size
   */
  GaussianKernel(const T& stddev, const size_t& cache_size);

  /**
   * @name  ~GaussianKernel
   * @fn    ~GaussianKernel() override = default
   * @brief Destructor
   */
  ~GaussianKernel() override = default;

 protected:
  /**
   * @name  k
   * @fn    T k(const PointType<NDim, T>& x, const PointType<NDim, T>& y) const
   * @brief Compute the output scalar value for the kernel between two points
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Value
   */
  T k(const PointType<NDim, T>& x, const PointType<NDim, T>& y) const override;

 private:
  /** Variance */
  T var_;
};

template<int NDim, typename T>
GaussianKernel<NDim, T>::GaussianKernel(const T &stddev) :
        GaussianKernel<NDim, T>(stddev, 0) {}

template<int NDim, typename T>
GaussianKernel<NDim, T>::GaussianKernel(const T &stddev,
                                        const size_t& cache_size) :
        Kernel<NDim, T>(EuclideanSpace<NDim, T>::Create(),
                        cache_size),
        var_(stddev * stddev) {}

template<int NDim, typename T>
T GaussianKernel<NDim, T>::k(const PointType<NDim, T>& x,
                             const PointType<NDim, T>& y) const {
  T r = (x - y).SquaredNorm();
  return std::exp(-r / var_);
}

/**
 * @class   UnivariateBSpline
 * @brief   Multi-scale B-spline kernel function (Sec IIIC, "Morphable Face
 *  Models - An Open Framework")
 * @author  Christophe Ecabert
 * @date    10/02/2021
 * @ingroup kernel
 * @tparam NDim Number of dimensions
 * @tparam T    Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS UnivariateBSpline;

template<typename T>
class LTS5_EXPORTS UnivariateBSpline<1, T> {
 public:
  /** Point type */
  using PtType = PointType<1, T>;
  /**
   * @name  UnivariateBSpline
   * @brief Constructor
   * @param[in] order   Spline's order
   * @param[in] scale   Spline's scale
   */
  UnivariateBSpline(const int& order,
                    const int& scale) : spline_(order),
                                        order_(order),
                                        c_(T(0.0)),
                                        O_(T(0.0)),
                                        two_j_(T(0.0)) {
    c_ = std::pow(T(2.0), static_cast<T>(scale));
    O_ = T(0.5) * static_cast<T>(order + 1);
    two_j_ = c_;
  }
  /**
   * @name  operator()
   * @fn    T operator()(const PtType& x, const PtType& y) const
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Spline's value at (x,y)
   */
  T operator()(const PtType& x, const PtType& y) const {
    int kl_x = static_cast<int>(std::ceil(std::max(x[0], y[0]) * c_ - O_));
    T kll_x = std::min(x[0], y[0]) * c_ - O_;
    int ku_x = static_cast<int>(std::floor(kll_x + order_ + T(1.0)));
    auto xVec_j = x * two_j_;
    auto yVec_j = y * two_j_;
    T sum_j = T(0.0);
    for (int kx = kl_x; kx <= ku_x; ++kx) {
      sum_j += spline_(xVec_j[0] - kx) * spline_(yVec_j[0] - kx);
    }
    return sum_j;
  }
 private:
  /** Spline function */
  BSpline<T> spline_;
  /** Spline's order */
  int order_;
  /** Constant - C */
  T c_;
  /** Constant - O */
  T O_;
  /** Constant - 2^j */
  T two_j_;
};

template<typename T>
class LTS5_EXPORTS UnivariateBSpline<2, T> {
 public:
  /** Point type */
  using PtType = PointType<2, T>;
  /**
   * @name  UnivariateBSpline
   * @brief Constructor
   * @param[in] order   Spline's order
   * @param[in] scale   Spline's scale
   */
  UnivariateBSpline(const int& order,
                    const int& scale) : spline_(order),
                                        order_(order),
                                        c_(T(0.0)),
                                        O_(T(0.0)),
                                        two_j_(T(0.0)) {
    c_ = std::pow(T(2.0), static_cast<T>(scale));
    O_ = T(0.5) * static_cast<T>(order + 1);
    two_j_ = c_;
  }
  /**
   * @name  operator()
   * @fn    T operator()(const PtType& x, const PtType& y) const
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Spline's value at (x,y)
   */
  T operator()(const PtType& x, const PtType& y) const {
    int kl_x = static_cast<int>(std::ceil(std::max(x[0], y[0]) * c_ - O_));
    int kl_y = static_cast<int>(std::ceil(std::max(x[1], y[1]) * c_ - O_));
    T kll_x = std::min(x[0], y[0]) * c_ - O_;
    T kll_y = std::min(x[1], y[1]) * c_ - O_;
    int ku_x = static_cast<int>(std::floor(kll_x + order_ + T(1.0)));
    int ku_y = static_cast<int>(std::floor(kll_y + order_ + T(1.0)));
    auto xVec_j = x * two_j_;
    auto yVec_j = y * two_j_;
    T sum_j = T(0.0);
    for (int kx = kl_x; kx <= ku_x; ++kx) {
      for (int ky = kl_y; ky <= ku_y; ++ky) {
        sum_j += (spline_(xVec_j[0] - kx) * spline_(xVec_j[1] - ky) *
                  spline_(yVec_j[0] - kx) * spline_(yVec_j[1] - ky));
      }
    }
    return sum_j;
  }
 private:
  /** Spline function */
  BSpline<T> spline_;
  /** Spline's order */
  int order_;
  /** Constant - C */
  T c_;
  /** Constant - O */
  T O_;
  /** Constant - 2^j */
  T two_j_;
};

template<typename T>
class LTS5_EXPORTS UnivariateBSpline<3, T> {
 public:
  /** Point type */
  using PtType = PointType<3, T>;
  /**
   * @name  UnivariateBSpline
   * @brief Constructor
   * @param[in] order   Spline's order
   * @param[in] scale   Spline's scale
   */
  UnivariateBSpline(const int& order,
                    const int& scale) : spline_(order),
                                        order_(order),
                                        c_(T(0.0)),
                                        O_(T(0.0)),
                                        two_j_(T(0.0)) {
    c_ = std::pow(T(2.0), static_cast<T>(scale));
    O_ = T(0.5) * static_cast<T>(order + 1);
    two_j_ = c_;
  }
  /**
   * @name  operator()
   * @fn    T operator()(const PtType& x, const PtType& y) const
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Spline's value at (x,y)
   */
  T operator()(const PtType& x, const PtType& y) const {
    int kl_x = static_cast<int>(std::ceil(std::max(x[0], y[0]) * c_ - O_));
    int kl_y = static_cast<int>(std::ceil(std::max(x[1], y[1]) * c_ - O_));
    int kl_z = static_cast<int>(std::ceil(std::max(x[2], y[2]) * c_ - O_));
    T kll_x = std::min(x[0], y[0]) * c_ - O_;
    T kll_y = std::min(x[1], y[1]) * c_ - O_;
    T kll_z = std::min(x[2], y[2]) * c_ - O_;
    int ku_x = static_cast<int>(std::floor(kll_x + order_ + T(1.0)));
    int ku_y = static_cast<int>(std::floor(kll_y + order_ + T(1.0)));
    int ku_z = static_cast<int>(std::floor(kll_z + order_ + T(1.0)));
    auto xVec_j = x * two_j_;
    auto yVec_j = y * two_j_;
    T sum_j = T(0.0);
    for (int kx = kl_x; kx <= ku_x; ++kx) {
      for (int ky = kl_y; ky <= ku_y; ++ky) {
        for (int kz = kl_z; kz <= ku_z; ++kz) {
          sum_j += (spline_(xVec_j[0] - kx) * spline_(xVec_j[1] - ky) *
                    spline_(xVec_j[2] - kz) *
                    spline_(yVec_j[0] - kx) * spline_(yVec_j[1] - ky) *
                    spline_(yVec_j[2] - kz));
        }
      }
    }
    return sum_j;
  }
 private:
  /** Spline function */
  BSpline<T> spline_;
  /** Spline's order */
  int order_;
  /** Constant - C */
  T c_;
  /** Constant - O */
  T O_;
  /** Constant - 2^j */
  T two_j_;
};

/**
 * @class   BSplineKernel
 * @brief   BSpline-based scalar kernel
 * @author  Christophe Ecabert
 * @date    10/02/2021
 * @ingroup kernel
 * @tparam  NDim Number of dimensions
 * @tparam  T    Data type
 */
template<int NDim, typename T>
class LTS5_EXPORTS BSplineKernel : public Kernel<NDim, T> {
 public:
  /** Pointer type */
  using Ptr = std::shared_ptr<BSplineKernel<NDim, T>>;

  /**
   * @name  Create
   * @fn    static Ptr Create(const int& order, const int& scale)
   * @brief Create object wrapped in a shared_ptr.
   * @param[in] order   Order
   * @param[in] scale   Scale
   * @return    Shared instance
   */
  static Ptr Create(const int& order, const int& scale) {
    return std::make_shared<BSplineKernel<NDim, T>>(order, scale);
  }

  /**
   * @name  Create
   * @fn    static Ptr Create(const int& order, const int& scale,
   *    const size_t& size)
   * @brief Create object wrapped in a shared_ptr.
   * @param[in] order   Order
   * @param[in] scale   Scale
   * @param[in] size    Cache size
   * @return    Shared instance
   */
  static Ptr Create(const int& order, const int& scale, const size_t& size) {
    return std::make_shared<BSplineKernel<NDim, T>>(order, scale);
  }

  /**
   * @name  BSplineKernel
   * @fn    explicit BSplineKernel(const int& order, const int& scale)
   * @param[in] order   Order
   * @param[in] scale   Scale
   */
  BSplineKernel(const int& order, const int& scale);

  /**
   * @name  BSplineKernel
   * @fn    explicit BSplineKernel(const int& order, const int& scale,
   *    const size_t& size)
   * @param[in] order   Order
   * @param[in] scale   Scale
   * @param[in] size    Cache size
   */
  BSplineKernel(const int& order, const int& scale, const size_t& size);

  /**
   * @name  ~BSplineKernel
   * @fn    ~BSplineKernel() override = default
   * @brief Destructor
   */
  ~BSplineKernel() override = default;

 protected:
  /**
   * @name  k
   * @fn    T k(const PointType<NDim, T>& x, const PointType<NDim, T>& y) const
   * @brief Compute the output scalar value for the kernel between two points
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Value
   */
  T k(const PointType<NDim, T>& x, const PointType<NDim, T>& y) const override;

 private:
  /** Univariate BSpline */
  UnivariateBSpline<NDim, T> spline_;
};

template<int NDim, typename T>
BSplineKernel<NDim, T>::BSplineKernel(const int& order,
                                      const int& scale) :
        BSplineKernel<NDim, T>(order, scale, 0) {
}

template<int NDim, typename T>
BSplineKernel<NDim, T>::BSplineKernel(const int& order,
                                      const int& scale,
                                      const size_t& size) :
        Kernel<NDim, T>(EuclideanSpace<NDim, T>::Create(), size),
        spline_(order, scale) {
}

template<int NDim, typename T>
T BSplineKernel<NDim, T>::k(const PointType<NDim, T>& x,
                            const PointType<NDim, T>& y) const {
  return spline_(x, y);
}


}  // namespace LTS5
#endif  // __LTS5_KERNEL_KERNEL__
