/**
 *  @file   matrix_kernel.hpp
 *  @brief  Represent matrix-valued covariance kernel
 *  @ingroup kernel
 *
 *  @author Christophe Ecabert
 *  @date   16/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_KERNEL_MATRIX_KERNEL__
#define __LTS5_KERNEL_MATRIX_KERNEL__

#include <memory>

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/math/matrix.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/utils/sys/cache.hpp"
#include "lts5/kernel/domain.hpp"
#include "lts5/kernel/kernel.hpp"
#include "lts5/kernel/kl_basis.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<int NDim, typename T>
using PointType = typename Point<NDim, T>::Type;

template<int ODim, typename T>
using MatType = typename Matrix<ODim, T>::Type;

/**
 * @name    OuterProduct
 * @fn  MatType<NDim, T> OuterProduct(const PointType<NDim, T>& x,
                                      const PointType<NDim, T>& y)
 * @brief Compute outer product between two vectors `x` and `y`.
 *  res = x * y'
 * @param[in] x First vector
 * @param[in] y Second vector
 * @tparam NDim Number of dimension in vector
 * @tparam T    Data type
 * @return  Outer product, square matrix
 */
template<int NDim, typename T>
MatType<NDim, T> OuterProduct(const PointType<NDim, T>& x,
                              const PointType<NDim, T>& y) {
  MatType<NDim, T> res;
  for (int i = 0; i < NDim; ++i) {
    for (int j = 0; j < NDim; ++j) {
      res(i, j) = x[i] * y[j];
    }
  }
  return res;
}

#pragma mark -
#pragma mark MatrixKernel

/**
 * @class   MatrixKernel
 * @brief   Interface for GP cov kernel definition for matrix value function
 * @author  Christophe Ecabert
 * @date    16/12/2020
 * @ingroup kernel
 * @tparam IDim Input dimensions: Vector{1, 2, 3, 4}
 * @tparam ODim Output dimensions: Matrix{1, 2, 3, 4}
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class MatrixKernel {
 public:
  /** Point type */
  using PtType = PointType<IDim, T>;
  /** Domain Type */
  using DomPtr = std::shared_ptr<Domain<IDim, T>>;
  /** Pointer type */
  using Ptr = std::shared_ptr<MatrixKernel<IDim, ODim, T>>;

  /**
   * @name  MatrixKernel
   * @fn    explicit MatrixKernel(const DomPtr& domain)
   * @brief Construction
   * @param[in] domain Domain of definition of the kernel
   */
  explicit MatrixKernel(const DomPtr& domain) :
          MatrixKernel<IDim, ODim, T>(domain, 0) {
  }

  /**
   * @name  MatrixKernel
   * @fn    explicit MatrixKernel(const DomPtr& domain, const size_t& size)
   * @brief Construction
   * @param[in] domain Domain of definition of the kernel
   * @param[in] size Cache size to use to lazily evaluate kernel, set to `0` to
   *  disable caching
   */
  MatrixKernel(const DomPtr& domain, const size_t& size) :
          domain_(domain),
          cache_(),
          cache_size_(size) {
    using OutType = MatType<ODim, T>;
    cache_ = std::make_unique<CacheType>(size,
                                         [this](const PtType& x,
                                                const PtType& y) -> OutType {
                                           return this->k(x, y);
                                         });
  }

  /**
   * @name  ~MatrixKernel
   * @fn    ~MatrixKernel() = default
   * @brief Destructor
   */
  virtual ~MatrixKernel() = default;

  /**
   * @name  operator()
   * @fn    MatType<ODim, T> T operator()(const PointType<NDim, T>& x,
                                          const PointType<NDim, T>& y) const
   * @brief Compute the scalar value for the underlying kernel between two
   *    points. Check that inputs are in the domain of definition of the kernel
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    k(x, y)
   */
  MatType<ODim, T> operator()(const PointType<IDim, T>& x,
                              const PointType<ODim, T>& y) const {
    if (domain_->IsDefinedAt(x) && domain_->IsDefinedAt(y)) {
      if (cache_size_) {
        return cache_->GetOrComputeValue(x, y);
      } else {
        return this->k(x, y);
      }
    } else {
      if (!domain_->IsDefinedAt(x)) {
        throw std::runtime_error("`x` is not part of the domain");
      } else {
        throw std::runtime_error("`y` is not part of the domain");
      }
    }
  }

  /**
   * @name  Matrix
   * @fn    cv::Mat Matrix(const std::vector<PointType<IDim, T>>& xs) const
   * @brief Compute covariance matrix for a given list of points
   * @param[in] xs List of points to use to build covariance matrix
   * @return    Matrix
   */
  cv::Mat Matrix(const std::vector<PointType<IDim, T>>& xs) const;

  /**
   * @name  Vector
   * @fn    cv::Mat Vector(const PointType<IDim, T>& x,
                 const std::vector<PointType<IDim, T>>& xs) const
   * @brief For every domain point x in the list, we compute the kernel vector
   *    kx = (k(x, x1), ... k(x, xm))
   * @param[in] x   Constant point
   * @param[in] xs  List of point to evaluate
   * @return    kx matrix
   */
  cv::Mat Vector(const PointType<IDim, T>& x,
                 const std::vector<PointType<IDim, T>>& xs) const;

  /**
   * @name  domain
   * @fn    const DomPtr& domain() const
   * @brief  Provide domain of definition of the kernel
   * @return    Shared domain instance
   */
  const DomPtr& domain() const {
    return domain_;
  }

  /**
   * @name  output_dim
   * @fn    size_t output_dim() const
   * @brief Dimensions of the output matrix
   * @return    Number of dimension (i.e. ODim)
   */
  size_t output_dim() const {
    return ODim;
  }

 protected:
  /**
   * @name  k
   * @fn    virtual MatType<ODim, T> k(const PointType<IDim, T>& x,
   *                                   const PointType<IDim, T>& y) const = 0
   * @brief Compute the output scalar value for the kernel between two points
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Value
   */
  virtual MatType<ODim, T> k(const PointType<IDim, T>& x,
                             const PointType<IDim, T>& y) const = 0;

 private:
  using CacheType = Cache<MatType<ODim, T>(const PtType&, const PtType&)>;
  /** Domain */
  DomPtr domain_;
  /** Cache */
  std::unique_ptr<CacheType> cache_;
  /** Cache size */
  size_t cache_size_;
};

#pragma mark -
#pragma mark MatrixKernelComposer

/**
 * @struct  MatKernelComposerSelector
 * @brief   Select merge operation to apply to fuse two kernel outputs
 * @date 16/12/2020
 * @ingroup kernel
 * @tparam IDim Number of input's dimension
 * @tparam ODim Number of output's dimension
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T, KernelComposerType Op>
struct MatKernelComposerSelector;

template<int IDim, int ODim, typename T>
struct MatKernelComposerSelector<IDim, ODim, T, KernelComposerType::kAdd> {
  MatType<ODim, T>
  operator()(const std::shared_ptr<MatrixKernel<IDim, ODim, T>>& k1,
             const std::shared_ptr<MatrixKernel<IDim, ODim, T>>& k2,
             const T& scale,
             const PointType<IDim, T>& x,
             const PointType<IDim, T>& y) {
    return k1->operator()(x, y) + k2->operator()(x, y);
  }
};

template<int IDim, int ODim, typename T>
struct MatKernelComposerSelector<IDim, ODim, T, KernelComposerType::kMultiply> {
  MatType<ODim, T>
  operator()(const std::shared_ptr<MatrixKernel<IDim, ODim, T>>& k1,
             const std::shared_ptr<MatrixKernel<IDim, ODim, T>>& k2,
             const T& scale,
             const PointType<IDim, T>& x,
             const PointType<IDim, T>& y) {
    // Element-wise matrix product
    auto k1_out = k1->operator()(x, y);
    auto k2_out = k2->operator()(x, y);
    return k1_out.ElementWiseProduct(k2_out);
  }
};

template<int IDim, int ODim, typename T>
struct MatKernelComposerSelector<IDim, ODim, T, KernelComposerType::kScale> {
  MatType<ODim, T>
  operator()(const std::shared_ptr<MatrixKernel<IDim, ODim, T>>& k1,
             const std::shared_ptr<MatrixKernel<IDim, ODim, T>>& k2,
             const T& scale,
             const PointType<IDim, T>& x,
             const PointType<IDim, T>& y) {
    return k1->operator()(x, y) * scale;
  }
};

template<int IDim, int ODim, typename T>
struct MatKernelComposerSelector<IDim, ODim, T, KernelComposerType::kTransform> {
  MatType<ODim, T>
  operator()(const std::shared_ptr<MatrixKernel<IDim, ODim, T>>& k1,
             const std::shared_ptr<MatrixKernel<IDim, ODim, T>>& k2,
             const T& scale,
             const PointType<IDim, T>& x,
             const PointType<IDim, T>& y) {
    return k1->operator()(x, y);
  }
};

/**
 * @class   MatrixKernelComposerBase
 * @brief   Base class to combine two MatrixKernel into a single one.
 * @author  Christophe Ecabert
 * @date    16/12/2020
 * @ingroup kernel
 * @tparam IDim Number of intput's dimensions
 * @tparam ODim Number of output's dimensions
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T, KernelComposerType Op>
class MatrixKernelComposerBase : public MatrixKernel<IDim, ODim, T> {
 public:
  using BaseType = MatrixKernelComposerBase<IDim, ODim, T, Op>;
  /** Pointer type */
  using Ptr = std::shared_ptr<BaseType>;
  /** Kernel type */
  using KernelType = std::shared_ptr<MatrixKernel<IDim, ODim, T>>;
  /** Domain type */
  using DomPtr = std::shared_ptr<Domain<IDim, T>>;
  /** Point transform function */
  using PtTrsfrmFn = std::function<PointType<IDim, T>(const PointType<IDim, T>&)>;

  /**
   * @name  MatrixKernelComposerBase
   * @fn    MatrixKernelComposerBase(const KernelType& k1, const KernelType& k2,
   *                        const DomPtr& domain, const T& scale,
   *                        PtTrsfrmFn fn)
   * @brief Constructor
   * @param[in] k1  First kernel
   * @param[in] k2  Second kernel
   * @param[in] domain  Domain of definition of `k1` and `k2`
   * @param[in] scale   Scaling factor
   * @param[in] fn  Function to transform point before estimating kernel
   */
  MatrixKernelComposerBase(const KernelType& k1,
                           const KernelType& k2,
                           const DomPtr& domain,
                           const T& scale,
                           PtTrsfrmFn fn) : MatrixKernel<IDim, ODim, T>(domain),
                                            k1_(k1),
                                            k2_(k2),
                                            scale_(scale),
                                            fn_(fn) {}

 protected:
  /**
   * @name  k
   * @fn    T k(const PointType<NDim, T>& x, const PointType<NDim, T>& y) const
   * @brief Compute the output scalar value for the kernel between two points
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Value
   */
  MatType<ODim, T> k(const PointType<IDim, T>& x,
                     const PointType<IDim, T>& y) const override {
    using SelectorType = MatKernelComposerSelector<IDim, ODim, T, Op>;
    // Apply transform
    auto xt = fn_(x);
    auto yt = fn_(y);
    return SelectorType()(k1_, k2_, scale_, xt, yt);
  }

 private:
  /** First kernel */
  KernelType k1_;
  /** Second kernel */
  KernelType k2_;
  /** Scalar */
  T scale_;
  /** Transform */
  PtTrsfrmFn fn_;
};

/**
 * @class   MatrixKernelComposer
 * @brief   Factory class to combine two kernels into a single one.
 * @author  Christophe Ecabert
 * @date    01/12/2020
 * @ingroup kernel
 * @tparam NDim Number of dimensions
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class MatrixKernelComposer {
 public:
  /** Kernel type */
  using KernelType = std::shared_ptr<MatrixKernel<IDim, ODim, T>>;
  /** Domain type */
  using DomPtr = std::shared_ptr<Domain<IDim, T>>;
  /** Point transform function */
  using PtTrsfrmFn = std::function<PointType<IDim, T>(const PointType<IDim, T>&)>;
  /** Base pointer type */
  template<KernelComposerType OpType>
  using BaseType = MatrixKernelComposerBase<IDim, ODim, T, OpType>;
  /** Add composition */
  using AddPtr = std::shared_ptr<BaseType<KernelComposerType::kAdd>>;
  /** Multiply composition */
  using MultiplyPtr = std::shared_ptr<BaseType<KernelComposerType::kMultiply>>;
  /** Scale composition */
  using ScalePtr = std::shared_ptr<BaseType<KernelComposerType::kScale>>;
  /** Scale composition */
  using TransformPtr = std::shared_ptr<BaseType<KernelComposerType::kTransform>>;
  /**
   * @name  Add
   * @fn    static Ptr Add(const KernelType& k1, const KernelType& k2)
   * @brief Add two kernels together
   * @param[in] k1  First kernel
   * @param[in] k2  Second kernel
   * @return Kernel computing k(x,y) = k1(x, y) + k2(x, y)
   */
  static AddPtr Add(const KernelType& k1, const KernelType& k2) {
    using KType = BaseType<KernelComposerType::kAdd>;
    auto domain = DomainComposer<IDim, T>::Intersection(k1->domain(),
                                                        k2->domain());
    return std::make_shared<KType>(k1,
                                   k2,
                                   domain,
                                   T(1.0),
                                   PtsTransformerDefault<IDim, T>());
  }

  /**
   * @name  Multiply
   * @fn    static Ptr Multiply(const KernelType& k1, const KernelType& k2)
   * @brief Multiply two kernels together (element wise)
   * @param[in] k1  First kernel
   * @param[in] k2  Second kernel
   * @return Kernel computing k(x,y) = k1(x, y) * k2(x, y)
   */
  static MultiplyPtr Multiply(const KernelType& k1, const KernelType& k2) {
    using KType = BaseType<KernelComposerType::kMultiply>;
    auto domain = DomainComposer<IDim, T>::Intersection(k1->domain(),
                                                        k2->domain());
    return std::make_shared<KType>(k1,
                                   k2,
                                   domain,
                                   T(1.0),
                                   PtsTransformerDefault<IDim, T>());
  }

  /**
   * @name  Multiply
   * @fn    static Ptr Scale(const KernelType& k1, const T& scale)
   * @brief Scale a kernel by a scalar value
   * @param[in] k1 First kernel
   * @param[in] scale  Scalar value
   * @return Kernel computing k(x,y) = k1(x, y) * scale
   */
  static ScalePtr Scale(const KernelType& k1, const T& scale) {
    using KType = BaseType<KernelComposerType::kScale>;
    return std::make_shared<KType>(k1,
                                   KernelType(),
                                   k1->domain(),
                                   scale,
                                   PtsTransformerDefault<IDim, T>());
  }

  /**
   * @name  Transform
   * @fn    static Ptr Transform(const KernelType& k1, PtTrsfrmFn fn);
   * @brief Scale a kernel by a scalar value
   * @param[in] k1 First kernel
   * @param[in] fn Function to apply to `x` and `y` before evaluating `k`
   * @return Kernel computing k(x,y) = k1(fn(x), fn(y))
   */
  static TransformPtr Transform(const KernelType& k1, PtTrsfrmFn fn) {
    using KType = BaseType<KernelComposerType::kTransform>;
    return std::make_shared<KType>(k1,
                                   KernelType(),
                                   k1->domain(),
                                   T(1.0),
                                   fn);
  }
};

#pragma mark -
#pragma mark DiagonalKernel

/**
 * @class   DiagonalKernel
 * @brief   Transform a scalar-value kernel into a matrix-value kernel
 * @author  Christophe Ecabert
 * @date    16/12/2020
 * @ingroup kernel
 * @tparam IDim Number of input's dimensions
 * @tparam ODim Number of output's dimensions
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class DiagonalKernel : public MatrixKernel<IDim, ODim, T> {
 public:
  /** Kernel pointer */
  using ScalarKernelPtr = std::shared_ptr<Kernel<IDim, T>>;
  /** Pointer type */
  using Ptr = std::shared_ptr<DiagonalKernel<IDim, ODim, T>>;

  /**
   * @name  Create
   * @fn    static Ptr Create(const ScalarKernelPtr& kernel)
   * @brief Create object wrapped in a shared_ptr.
   * @param[in] kernel  Scalar kernel to transform into `MatrixKernel`
   * @return    Shared instance
   */
  static Ptr Create(const ScalarKernelPtr& kernel);

  /**
   * @name  Create
   * @fn    static Ptr Create(const ScalarKernelPtr& kernel, const size_t& size)
   * @brief Create object wrapped in a shared_ptr.
   * @param[in] kernel  Scalar kernel to transform into `MatrixKernel`
   * @param[in] size    Cache size
   * @return    Shared instance
   */
  static Ptr Create(const ScalarKernelPtr& kernel, const size_t& size);

  /**
   * @name  DiagonalKernel
   * @fn    explicit DiagonalKernel(const ScalarKernelPtr& kernel)
   * @brief Constructor
   * @param[in] kernel  Scalar kernel to transform into `MatrixKernel`
   */
  explicit DiagonalKernel(const ScalarKernelPtr& kernel);

  /**
   * @name  DiagonalKernel
   * @fn    DiagonalKernel(const ScalarKernelPtr& kernel, const size_t& size)
   * @brief Constructor
   * @param[in] kernel  Scalar kernel to transform into `MatrixKernel`
   * @param[in] size    Cache size
   */
  DiagonalKernel(const ScalarKernelPtr& kernel, const size_t& size);

  /**
   * @name  ~DiagonalKernel
   * @fn    ~DiagonalKernel() override = default
   * @brief Destructor
   */
  ~DiagonalKernel() override = default;

 protected:
  /**
   * @name  k
   * @fn    T k(const PointType<NDim, T>& x, const PointType<NDim, T>& y) const
   * @brief Compute the output matrix value for the kernel between two points
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Value
   */
  MatType<ODim, T> k(const PointType<IDim, T>& x,
                     const PointType<IDim, T>& y) const override;

 private:
  /** Scalar kernel */
  ScalarKernelPtr k_;
  /** Identity matrix */
  MatType<ODim, T> identity_;
};


/**
 * @class   SymmetricKernel
 * @brief   Transform a matrix-value kernel into a symmetric matrix-kernel.
 *  Deformation is symmetric around a given axis. Note it assumed that the
 *  center of symmetry is located at 0.0.
 *  See: https://scalismo.org/docs/tutorials/tutorial7#building-more-interesting-kernels
 * @author  Christophe Ecabert
 * @date    04/02/2021
 * @ingroup kernel
 * @tparam IDim Number of input's dimensions
 * @tparam ODim Number of output's dimensions
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS SymmetricKernel : public MatrixKernel<IDim, ODim, T> {
 public:
  /** Matrix kernel pointer */
  using MatKernelPtr = std::shared_ptr<MatrixKernel<IDim, ODim, T>>;
  /** Pointer type */
  using Ptr = std::shared_ptr<SymmetricKernel<IDim, ODim, T>>;

  /**
   * @name  Create
   * @fn    static Ptr Create(const MatKernelPtr& kernel, T alpha, size_t axis)
   * @brief Create object wrapped in a shared_ptr.
   * @param[in] kernel  Matrix-valued kernel to make symmetric
   * @param[in] alpha   Balancing term
   * @param[in] axis    Dimensions index around which to make the symmetry
   * @return    Shared instance
   */
  static Ptr Create(const MatKernelPtr& kernel,
                    T alpha,
                    size_t axis);

  /**
   * @name  Create
   * @fn    static Ptr Create(const MatKernelPtr& kernel, T alpha, size_t axis,
   *    size_t size)
   * @brief Create object wrapped in a shared_ptr.
   * @param[in] kernel  Matrix-valued kernel to make symmetric
   * @param[in] alpha   Balancing term
   * @param[in] axis    Dimensions index around which to make the symmetry
   * @param[in] size    Cache size
   * @return    Shared instance
   */
  static Ptr Create(const MatKernelPtr& kernel,
                    T alpha,
                    size_t axis,
                    size_t size);

  /**
   * @name  Create
   * @fn    static Ptr Create(const MatKernelPtr& kernel, size_t axis)
   * @brief Create object wrapped in a shared_ptr.
   * @param[in] kernel  Matrix-valued kernel to make symmetric
   * @param[in] axis    Dimensions index around which to make the symmetry
   * @return    Shared instance
   */
  static Ptr Create(const MatKernelPtr& kernel, size_t axis);

  /**
   * @name  SymmetricKernel
   * @fn    SymmetricKernel(const MatKernelPtr& kernel, T alpha, size_t axis)
   * @brief Constructor
   * @param[in] kernel  Scalar-valued kernel to make symmetric
   * @param[in] alpha   Balancing term
   * @param[in] axis    Dimensions index around which to make the symmetry
   */
  SymmetricKernel(const MatKernelPtr& kernel, T alpha, size_t axis);

  /**
   * @name  SymmetricKernel
   * @fn    SymmetricKernel(const MatKernelPtr& kernel, T alpha, size_t axis,
                  size_t size)
   * @brief Constructor
   * @param[in] kernel  Scalar-valued kernel to make symmetric
   * @param[in] alpha   Balancing term
   * @param[in] axis    Dimensions index around which to make the symmetry
   * @param[in] size    Cache size
   */
  SymmetricKernel(const MatKernelPtr& kernel,
                  T alpha,
                  size_t axis,
                  size_t size);

  /**
   * @name  SymmetricKernel
   * @fn    SymmetricKernel(const MatKernelPtr& kernel, size_t axis)
   * @brief Constructor
   * @param[in] kernel  Scalar-valued kernel to make symmetric
   * @param[in] axis    Dimensions index around which to make the symmetry
   */
  SymmetricKernel(const MatKernelPtr& kernel,
                  size_t axis);

  /**
   * @name  ~SymmetricKernel
   * @fn    ~SymmetricKernel() override = default
   * @brief Destructor
   */
  ~SymmetricKernel() override = default;

 protected:
  /**
   * @name  k
   * @fn    T k(const PointType<NDim, T>& x, const PointType<NDim, T>& y) const
   * @brief Compute the output matrix value for the kernel between two points
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Value
   */
  MatType<ODim, T> k(const PointType<IDim, T>& x,
                     const PointType<IDim, T>& y) const override;

 private:

  /**
   * @name  SymmetricPoint
   * @fn    PointType<IDim, T> SymmetricPoint(const PointType<IDim, T>& p) const
   * @brief Symmetrize a given point
   * @param[in] p   Point to be symmetrize
   * @return    Symmetrize points
   */
  PointType<IDim, T> SymmetricPoint(const PointType<IDim, T>& p) const;

  /** Matrix-valued kernel */
  MatKernelPtr k_;
  /** Symmetric identity matrix */
  MatType<ODim, T> i_bar_;
  /** Symmetry axis index */
  size_t axis_;
  /** Balancing term */
  T alpha_;
};


template<int IDim, int ODim, typename T>
class LTS5_EXPORTS KLBasisCovKernel : public MatrixKernel<IDim, ODim, T> {
 public:
  /** KLBasis pointer */
  using KLBasisPtr = std::shared_ptr<KLBasis<IDim, ODim, T>>;
  /** Pointer type */
  using Ptr = std::shared_ptr<KLBasisCovKernel<IDim, ODim, T>>;

  /**
   * @name  Create
   * @fn    static Ptr Create(const KLBasisPtr& basis)
   * @brief Create a kernel wrapped into a shared_ptr.
   * @param[in] basis   Continuous basis to transform into kernel
   * @return    Shared instance
   */
  static Ptr Create(const KLBasisPtr& basis);

  /**
   * @name  Create
   * @fn    static Ptr Create(const KLBasisPtr& basis, const size_t& size)
   * @brief Create a kernel wrapped into a shared_ptr.
   * @param[in] basis   Continuous basis to transform into kernel
   * @param[in] size    Cache size
   * @return    Shared instance
   */
  static Ptr Create(const KLBasisPtr& basis, const size_t& size);

  /**
   * @name  KLBasisCovKernel
   * @fn    explicit KLBasisCovKernel(const KLBasisPtr& basis)
   * @brief Constructor
   * @param[in] basis KLBasis
   */
  explicit KLBasisCovKernel(const KLBasisPtr& basis);

  /**
   * @name  KLBasisCovKernel
   * @fn  KLBasisCovKernel(const KLBasisPtr& basis, const size_t& size)
   * @brief Constructor
   * @param[in] basis   KLBasis
   * @param[in] size    Cache size
   */
  KLBasisCovKernel(const KLBasisPtr& basis, const size_t& size);

  /**
   * @name  ~KLBasisCovKernel
   * @fn    ~KLBasisCovKernel() = default
   * @brief Destructor
   */
  ~KLBasisCovKernel() = default;

 protected:
  /**
   * @name  k
   * @fn    T k(const PointType<NDim, T>& x, const PointType<NDim, T>& y) const
   * @brief Compute the output matrix value for the kernel between two points
   * @param[in] x   First point
   * @param[in] y   Second point
   * @return    Value
   */
  MatType<ODim, T> k(const PointType<IDim, T>& x,
                     const PointType<IDim, T>& y) const override;

 private:
  /** Basis */
  KLBasisPtr basis_;
};


/**
 * @class   DiscreteMatrixKernel
 * @brief   Interface for discrete GP cov kernel definition for matrix value
 *  function
 * @author  Christophe Ecabert
 * @date    29/12/2020
 * @ingroup kernel
 * @tparam IDim Input dimensions: Vector{1, 2, 3, 4}
 * @tparam ODim Output dimensions: Matrix{1, 2, 3, 4}
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS DiscreteMatrixKernel {
 public:
  /** Domain Type */
  using DDomPtr = std::shared_ptr<DiscreteDomain<IDim, T>>;

  /**
   * @name  DiscreteMatrixKernel
   * @fn    explicit DiscreteMatrixKernel(const DomPtr& domain)
   * @brief Construction
   * @param[in] domain Domain of definition of the kernel
   */
  explicit DiscreteMatrixKernel(const DDomPtr& domain) : domain_(domain) {}

  /**
  * @name  ~DiscreteMatrixKernel
  * @fn    ~DiscreteMatrixKernel() = default
  * @brief Destructor
  */
  virtual ~DiscreteMatrixKernel() = default;

  /**
   * @name  operator()
   * @fn    MatType<ODim, T> T operator()(const size_t& i,
   *                                      const size_t& j) const
   * @brief Compute the scalar value for the underlying kernel between two
   *    points. Check that inputs are in the domain of definition of the kernel
   * @param[in] i   First point index
   * @param[in] j   Second point index
   * @return    k(x, y)
   */
  MatType<ODim, T> operator()(const size_t& i, const size_t& j) const {
    if (i < domain_->Size() && j < domain_->Size()) {
      return this->k(i, j);
    } else {
      if (i >= domain_->Size()) {
        throw std::runtime_error("`i` is not a valid index");
      } else {
        throw std::runtime_error("`j` is not a valid index");
      }
    }
  }

  /**
   * @name  get_domain
   * @fn    const DDomPtr& get_domain() const
   * @brief Return domain of definition
   * @return    Domain
   */
  const DDomPtr& get_domain() const {
    return domain_;
  }

 protected:
  /**
   * @name  k
   * @fn    virtual MatType<ODim, T> k(const size_t& i,
   *                                   const size_t& j) const = 0
   * @brief Compute the output scalar value for the kernel between two points
   * @param[in] i   First point index
   * @param[in] j   Second point index
   * @return    Value
   */
  virtual MatType<ODim, T> k(const size_t& i, const size_t& j) const = 0;

  /** Domain */
  DDomPtr domain_;
};

/**
 * @class   DiscretizeContinuousKernel
 * @brief   Discrete matrix-valued kernel from a continuous kernel
 * @author  Christophe Ecabert
 * @date    29/12/2020
 * @ingroup kernel
 * @tparam IDim Input dimensions: Vector{1, 2, 3, 4}
 * @tparam ODim Output dimensions: Matrix{1, 2, 3, 4}
 * @tparam T    Data type
 */
template<int IDim, int ODim, typename T>
class LTS5_EXPORTS
DiscretizeContinuousKernel : public DiscreteMatrixKernel<IDim, ODim, T> {
 public:
  /** Matrix-valued kernel pointer */
  using MatKernlPtr = std::shared_ptr<MatrixKernel<IDim, ODim, T>>;
  /** Discrete domain pointer */
  using DDomPtr = std::shared_ptr<DiscreteDomain<IDim, T>>;

  /**
   * @name  DiscretizeContinuousKernel
   * @fn    DiscretizeContinuousKernel(const DDomPtr& domain,
                                    const MatKernlPtr& kernel)
   * @brief Constructor
   * @param[in] domain  Discrete domain where to evaluate the kernel
   * @param[in] kernel  Matrix kernel to discretize
   */
  DiscretizeContinuousKernel(const DDomPtr& domain,
                             const MatKernlPtr& kernel) :
          DiscreteMatrixKernel<IDim, ODim, T>(domain),
          kernel_(kernel) {
  }

  /**
   * @name  ~DiscretizeContinuousKernel
   * @fn    ~DiscretizeContinuousKernel() = default
   * @brief Destructor
   */
  ~DiscretizeContinuousKernel() = default;

 protected:
  /**
   * @name  k
   * @fn    virtual MatType<ODim, T> k(const size_t& i,
   *                                   const size_t& j) const = 0
   * @brief Compute the output scalar value for the kernel between two points
   * @param[in] i   First point index
   * @param[in] j   Second point index
   * @return    Value
   */
  MatType<ODim, T> k(const size_t& i, const size_t& j) const {
    auto x = this->domain_->operator[](i);
    auto y = this->domain_->operator[](j);
    return kernel_->operator()(x, y);
  }

 private:
  /** Continuous kernel */
  MatKernlPtr kernel_;
};

#pragma mark -
#pragma mark Implementation

#ifndef DOXYGEN_SHOULD_SKIP_THIS

template<int IDim, int ODim, typename T>
cv::Mat MatrixKernel<IDim, ODim, T>::
Matrix(const std::vector<PointType<IDim, T>>& xs) const {
  int sz = ODim * xs.size();
  cv::Mat K = cv::Mat::zeros(sz, sz, cv::DataType<T>::type);
  for (int i = 0; i < xs.size(); ++i) {
    for (int j = i; j < xs.size(); ++j) {
      // Evaluate kernel for pair of points {i,j}
      auto kxixj = this->k(xs[i], xs[j]);
      // Fill output matrix
      for (int di = 0; di < ODim; ++di) {
        for (int dj = 0; dj < ODim; ++dj) {
          int r_idx = (i * ODim) + di;
          int c_idx = (j * ODim) + dj;
          assert(r_idx < sz && "Row index is too large");
          assert(c_idx < sz && "Col index is too large");
          K.at<T>(r_idx, c_idx) = kxixj(di, dj);
          K.at<T>((j * ODim) + dj, (i * ODim) + di) = kxixj(di, dj);
        }
      }
    }
  }
  return K;
}

template<int IDim, int ODim, typename T>
cv::Mat MatrixKernel<IDim, ODim, T>::
Vector(const PointType<IDim, T>& x,
       const std::vector<PointType<IDim, T>>& xs) const {
  int sz = ODim * xs.size();
  cv::Mat kx(ODim, sz, cv::DataType<T>::type);
  Parallel::For(xs.size(),
                [&](const size_t& j) {
                  auto kxxj = this->operator()(x, xs[j]);   // Matrix
                  for (int di = 0; di < ODim; ++di) {
                    for (int dj = 0; dj < ODim; ++dj) {
                      kx.at<T>(di, (ODim * j) + dj) = kxxj(di, dj);
                    }
                  }
                });
  return kx;
}

template<int IDim, int ODim, typename T>
typename DiagonalKernel<IDim, ODim, T>::Ptr
DiagonalKernel<IDim, ODim, T>::Create(const ScalarKernelPtr& kernel) {
  return std::make_shared<DiagonalKernel<IDim, ODim, T>>(kernel);
}

template<int IDim, int ODim, typename T>
typename DiagonalKernel<IDim, ODim, T>::Ptr
DiagonalKernel<IDim, ODim, T>::Create(const ScalarKernelPtr& kernel,
                                      const size_t& size) {
  return std::make_shared<DiagonalKernel<IDim, ODim, T>>(kernel, size);
}

template<int IDim, int ODim, typename T>
DiagonalKernel<IDim, ODim, T>::
DiagonalKernel(const ScalarKernelPtr& kernel) :
        DiagonalKernel<IDim, ODim, T>(kernel, 0) {
}

template<int IDim, int ODim, typename T>
DiagonalKernel<IDim, ODim, T>::
DiagonalKernel(const ScalarKernelPtr& kernel, const size_t& size) :
        MatrixKernel<IDim, ODim, T>(kernel->domain(), size),
        k_(kernel) {
  identity_.Identity();
}
template<int IDim, int ODim, typename T>
MatType<ODim, T>
DiagonalKernel<IDim, ODim, T>::k(const PointType<IDim, T>& x,
                                 const PointType<IDim, T>& y) const {
  return identity_ * k_->operator()(x, y);
}

template<int IDim, int ODim, typename T>
typename SymmetricKernel<IDim, ODim, T>::Ptr
SymmetricKernel<IDim, ODim, T>::Create(const MatKernelPtr& kernel,
                                        T alpha,
                                        size_t axis) {
  using KernelType = SymmetricKernel<IDim, ODim, T>;
  return std::make_shared<KernelType>(kernel, alpha, axis, 0);
}

template<int IDim, int ODim, typename T>
typename SymmetricKernel<IDim, ODim, T>::Ptr
SymmetricKernel<IDim, ODim, T>::Create(const MatKernelPtr& kernel,
                                        T alpha,
                                        size_t axis,
                                        size_t size) {
  using KernelType = SymmetricKernel<IDim, ODim, T>;
  return std::make_shared<KernelType>(kernel, alpha, axis, size);
}

template<int IDim, int ODim, typename T>
typename SymmetricKernel<IDim, ODim, T>::Ptr
SymmetricKernel<IDim, ODim, T>::Create(const MatKernelPtr& kernel,
                                        size_t axis) {
  using KernelType = SymmetricKernel<IDim, ODim, T>;
  return std::make_shared<KernelType>(kernel, axis);
}

template<int IDim, int ODim, typename T>
SymmetricKernel<IDim, ODim, T>::
SymmetricKernel(const MatKernelPtr& kernel,
                T alpha,
                size_t axis,
                size_t size) : MatrixKernel<IDim, ODim, T>(kernel->domain(),
                                                           size),
                               k_(kernel),
                               axis_(axis),
                               alpha_(alpha) {
  // Init flip matrix
  i_bar_.Identity();
  i_bar_(axis, axis) = T(-1.0);
  i_bar_ = i_bar_ * this->alpha_;
}

template<int IDim, int ODim, typename T>
SymmetricKernel<IDim, ODim, T>::
SymmetricKernel(const MatKernelPtr& kernel,
                size_t axis) : SymmetricKernel<IDim, ODim, T>(kernel,
                                                              T(1.0),
                                                              axis,
                                                              0) {
}

template<int IDim, int ODim, typename T>
MatType<ODim, T> SymmetricKernel<IDim, ODim, T>::
k(const PointType<IDim, T>& x,
  const PointType<IDim, T>& y) const {
  auto k = k_->operator()(x, y);    // Matrix
  auto ybar = this->SymmetricPoint(y);
  auto k_bar = i_bar_ * k_->operator()(x, ybar);
  return k + k_bar;
}

template<int IDim, int ODim, typename T>
PointType<IDim, T> SymmetricKernel<IDim, ODim, T>::
SymmetricPoint(const PointType<IDim, T>& p) const {
  PointType<IDim, T> sp(p);
  sp[axis_] *= T(-1.0);
  return sp;
}


template<int IDim, int ODim, typename T>
typename KLBasisCovKernel<IDim, ODim, T>::Ptr
KLBasisCovKernel<IDim, ODim, T>::Create(const KLBasisPtr& basis) {
  using K = KLBasisCovKernel<IDim, ODim, T>;
  return std::make_shared<K>(basis);
}

template<int IDim, int ODim, typename T>
typename KLBasisCovKernel<IDim, ODim, T>::Ptr
KLBasisCovKernel<IDim, ODim, T>::Create(const KLBasisPtr& basis,
                                        const size_t& size) {
  using K = KLBasisCovKernel<IDim, ODim, T>;
  return std::make_shared<K>(basis, size);
}

template<int IDim, int ODim, typename T>
KLBasisCovKernel<IDim, ODim, T>::
KLBasisCovKernel(const KLBasisPtr& basis) :
        KLBasisCovKernel<IDim, ODim, T>(basis, 0) {
}

template<int IDim, int ODim, typename T>
KLBasisCovKernel<IDim, ODim, T>::
KLBasisCovKernel(const KLBasisPtr& basis, const size_t& size) :
        MatrixKernel<IDim, ODim, T>(basis->domain(), size),
        basis_(basis) {
}

template<int IDim, int ODim, typename T>
MatType<ODim, T>
KLBasisCovKernel<IDim, ODim, T>::k(const PointType<IDim, T>& x,
                                   const PointType<IDim, T>& y) const {
  MatType<ODim, T> outer;
  for (size_t k = 0; k < basis_->Size(); ++k) {
    const auto& pair = basis_->operator[](k);
    auto px = pair(x);  // Call underlying eigen function
    auto py = pair(y);
    outer += OuterProduct<IDim, T>(px, py) * pair.Value();
  }
  return outer;
}

#endif  // DOXYGEN_SHOULD_SKIP_THIS

}  // namespace LTS5
#endif // __LTS5_KERNEL_MATRIX_KERNEL__
