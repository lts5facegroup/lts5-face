/**
 *  @file   levenberg_marquardt_solver.cpp
 *  @brief  Gauss-Newton solver
 *
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>

#include "Eigen/Core"

#include "lts5/optimization/levenberg_marquardt_solver.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name LevenbergMarquardtSolver
 *  @fn LevenbergMarquardtSolver(void)
 *  @brief  Constructor
 */
template<class T>
LevenbergMarquardtSolver<T>::LevenbergMarquardtSolver(void) {}
/*
 *  @name ~LevenbergMarquardtSolver
 *  @fn ~LevenbergMarquardtSolver(void)
 *  @brief  Destructor
 */
template<class T>
LevenbergMarquardtSolver<T>::~LevenbergMarquardtSolver(void) {}

/*
 *  @name Minimize
 *  @fn virtual bool Minimize(const BaseProblem<T>& problem,
                             Matrix* theta0) = 0
 *  @brief  Minimize the given #problem
 *  @param[in]      problem Problem to minimize
 *  @param[in,out]  theta0  Starting parameter's value
 *  @return True if converge, false otherwise
 */
template <class T>
bool LevenbergMarquardtSolver<T>::Minimize(const BaseProblem<T>& problem,
                                           Matrix* theta0) {
  using TransposeType = typename LinearAlgebra<T>::TransposeType;
  int n_iter = 0;
  Matrix f;                                   // Cost function
  Matrix J;                                   // Jacobian matrix
  Matrix A = Matrix(theta0->rows(), theta0->rows());
  Matrix h_lm(theta0->rows(), 1);             // Update
  Matrix B;
  T cost = -1.0;
  T old_cost = -1.0;
  // Loop
  do {
    // Estimate Cost
    problem.EvaluateObjectiveFunction(*theta0, &f);
    old_cost = f.norm();
    // Estimate jacobian + JtJ
    problem.EvaluateGradient(*theta0, &J);
    // Regularization : (JtJ + mu * I)
    LTS5::LinearAlgebra<T>::Gemm(J,
                                 TransposeType::kTranspose,
                                 1.0,
                                 J,
                                 TransposeType::kNoTranspose,
                                 this->option_.damping_mu,
                                 &A);
    // Compute update by solving : A * h_gn = -Jt * f;
    int err_inv = LTS5::LinearAlgebra<T>::InverseMatrix(A, &A);
    if (!err_inv) {
      // A has inv(A) inside, compute update : h_lm = - A * Jt * f;
      LTS5::LinearAlgebra<T>::Gemm(A,
                                   TransposeType::kNoTranspose,
                                   -1.0,
                                   J,
                                   TransposeType::kTranspose,
                                   0.0,
                                   &B);
      LTS5::LinearAlgebra<T>::Gemv(B,
                                   TransposeType::kNoTranspose,
                                   1.0,
                                   f,
                                   0.0,
                                   &h_lm);
      // Update
      *theta0 = *theta0 + h_lm;
      n_iter++;
      problem.EvaluateObjectiveFunction(*theta0, &cost);
    } else {
      std::cout << "Unable to invert (JtJ + mu * I)" << std::endl;
      break;
    }
  } while (n_iter < this->option_.max_iter &&
           (std::abs(cost - old_cost) > this->option_.error_rate));
  std::cout << "#iter : " << n_iter << std::endl;
  return true;
}

template class LTS5_EXPORTS LevenbergMarquardtSolver<double>;
template class LTS5_EXPORTS LevenbergMarquardtSolver<float>;
}  // namespace LTS5
