/**
 *  @file   lagrange_dual_problem.cpp
 *  @brief  Lagrange dual problem definition for optimization of type :
 *          B* = argmin 0.5 || X - B*S ||^2
 *          subject to || B(:, j) || < c
 *
 *  @author Christophe Ecabert
 *  @date   22/12/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include "Eigen/Dense"

#include "lts5/optimization/lagrange_dual_problem.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name    LagrangeDualProblem
 *  @brief  Constructor
 */
template<class T>
LagrangeDualProblem<T>::LagrangeDualProblem(const Matrix& S,
                                            const Matrix& X) : X_(X), S_(S) {
  XSt_ = X * S_.transpose();
  SSt_ = S_ * S_.transpose();
  trXXt_ = 0.0;
  const int n_elem = static_cast<int>(X_.cols() * X_.rows());
  const T* data_ptr = X_.data();
  for (int i = 0; i < n_elem; ++i) {
    trXXt_ += data_ptr[i] * data_ptr[i];
  }
  // Default value
  c_ = 1.0;
}

/*
 *  @name ~LagrangeDualProblem
 *  @brief  Destructor
 */
template<class T>
LagrangeDualProblem<T>::~LagrangeDualProblem(void) {
}

/*
 *  @name EvaluateObjectiveFunction
 *  @fn EvaluateObjectiveFunction(const Matrix& theta, double* cost) = 0
 *  @brief  Evaluate the objective function with parameters #theta
 *  @param[in]  theta     Parameters to use to evaluate objective function
 *  @param[out] cost      Objective function cost
 *  @return Cost for parameters #theta
 */
template<class T>
void LagrangeDualProblem<T>::EvaluateObjectiveFunction(const Matrix& x,
                                                       T* cost) const {
  // Compute objective value at x
  using TransposeType = typename LTS5::LinearAlgebra<T>::TransposeType;
  assert(x.cols() == 1);
  int L = static_cast<int>(X_.rows());
  int M = static_cast<int>(S_.rows());
  // Compute inverse
  Matrix sst = SSt_;
  for (int i = 0; i < M; ++i) {
    sst(i, i) += x(i, 0);
  }
  //SSt_inv = sst.inverse();
  LTS5::LinearAlgebra<T>::InverseSymMatrix(sst, &SSt_inv);
  // Compute objective value
  Matrix A;
  Matrix C;
  if (L > M) {
    LTS5::LinearAlgebra<T>::Gemm(XSt_,
                                 TransposeType::kTranspose,
                                 1.0,
                                 XSt_,
                                 TransposeType::kNoTranspose,
                                 0.0,
                                 &A);
    LTS5::LinearAlgebra<T>::Gemm(SSt_inv,
                                 TransposeType::kNoTranspose,
                                 1.0,
                                 A,
                                 TransposeType::kNoTranspose,
                                 0.0,
                                 &C);
    T tr = 0.0;
    for (int i = 0 ; i < C.rows() ; i++) {
      tr += -C(i, i);
    }
    *cost = tr + trXXt_ - c_ * x.sum();
  } else {
    LTS5::LinearAlgebra<T>::Gemm(SSt_inv,
                                 TransposeType::kNoTranspose,
                                 1.0,
                                 XSt_,
                                 TransposeType::kTranspose,
                                 0.0,
                                 &A);
    LTS5::LinearAlgebra<T>::Gemm(XSt_,
                                 TransposeType::kNoTranspose,
                                 1.0,
                                 A,
                                 TransposeType::kNoTranspose,
                                 0.0,
                                 &C);
    T tr = 0.0;
    for (int i = 0 ; i < C.rows() ; i++) {
      tr += -C(i, i);
    }
    *cost = tr + trXXt_ - c_ * x.sum();
  }
  *cost *= -1.0;
}

/*
 *  @name EvaluateObjectiveFunction
 *  @fn EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost)
 *  @brief  Evaluate the vector objective function with parameters #theta
 *  @param[in]  theta     Parameters to use to evaluate objective function
 *  @param[out] cost      Objective function cost vector
 *  @return Cost for parameters #theta
 */
template<class T>
void LagrangeDualProblem<T>::EvaluateObjectiveFunction(const Matrix& x,
                                                       Matrix* cost) const {
  std::cout << "Not supported" << std::endl;
}

/*
 *  @name EvaluateGradient
 *  @fn void EvaluateGradient(const Matrix& theta, Matrix* jacob)
 *  @brief  Evaluate the function's gradient with parameters #theta
 *  @param[in]  theta     Position where to evaluate objective function
 *  @param[out] jacob     Jacobian matrix for parameters #theta
 */
template<class T>
void LagrangeDualProblem<T>::EvaluateGradient(const Matrix& x,
                                              Matrix* jacob) const {
  using TransposeType = typename LTS5::LinearAlgebra<T>::TransposeType;
  // Init matrix
  jacob->resize(S_.rows(), 1);
  // Compute grad
  //XStSSt_inv = XSt_ * SSt_inv;
  LTS5::LinearAlgebra<T>::Gemm(XSt_,
                               TransposeType::kNoTranspose,
                               1.0,
                               SSt_inv,
                               TransposeType::kNoTranspose,
                               0.0,
                               &XStSSt_inv);
  for (int c = 0; c < XStSSt_inv.cols(); ++c) {
    double sum_sq = 0.0;
    for (int r = 0; r < XStSSt_inv.rows(); ++r) {
      sum_sq += XStSSt_inv(r, c) * XStSSt_inv(r, c);
    }
    (*jacob)(c, 0) = c_ - sum_sq;
  }
}

/*
 *  @name EvaluateHessian
 *  @fn void EvaluateHessian(const Matrix& x, Matrix* hessian)
 *  @brief  Evaluate the function's hessian matrix with parameters #theta
 *  @param[in]  theta   Parameters used to evalute hessian matrix
 *  @param[out] hessian Hessian value for parameters #theta
 */
template<class T>
void LagrangeDualProblem<T>::EvaluateHessian(const Matrix& x,
                                             Matrix* hessian) const {
  using TransposeType = typename LTS5::LinearAlgebra<T>::TransposeType;
  hessian->resize(SSt_inv.rows(), SSt_inv.cols());
  Matrix tt; // = XStSSt_inv.transpose() * XStSSt_inv;
  LTS5::LinearAlgebra<T>::Gemm(XStSSt_inv,
                               TransposeType::kTranspose,
                               1.0,
                               XStSSt_inv,
                               TransposeType::kNoTranspose,
                               0.0,
                               &tt);
  for (int r = 0; r < SSt_inv.rows(); ++r) {
    for (int c = 0; c < SSt_inv.cols(); ++c) {
      (*hessian)(r, c) = 2.0 * tt(r, c) * SSt_inv(r, c);
     }
  }
}

template class LTS5_EXPORTS LagrangeDualProblem<double>;
template class LTS5_EXPORTS LagrangeDualProblem<float>;

}  // namespace LTS5
