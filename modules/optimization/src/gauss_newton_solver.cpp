/**
 *  @file   gauss_newton_solver.cpp
 *  @brief  Gauss-Newton solver
 *
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>

#include "lts5/optimization/gauss_newton_solver.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name GaussNewtonSolver
 *  @fn GaussNewtonSolver(void)
 *  @brief  Constructor
 */
template<class T>
GaussNewtonSolver<T>::GaussNewtonSolver(void) {}

/*
 *  @name ~GaussNewtonSolver
 *  @fn ~GaussNewtonSolver(void)
 *  @brief  Destructor
 */
template<class T>
GaussNewtonSolver<T>::~GaussNewtonSolver(void) {}

/*
 *  @name Minimize
 *  @fn virtual bool Minimize(const BaseProblem<T>& problem,
                             Matrix* theta0) = 0
 *  @brief  Minimize the given #problem
 *  @param[in]      problem Problem to minimize
 *  @param[in,out]  theta0  Starting parameter's value
 *  @return True if converge, false otherwise
 */
template<class T>
bool GaussNewtonSolver<T>::Minimize(const BaseProblem<T>& problem,
                                         Matrix* theta0) {
  using TransposeType = typename LinearAlgebra<T>::TransposeType;
  int n_iter = 0;
  Matrix f;         // Cost function
  Matrix J;         // Jacobian matrix
  Matrix JtJ;       // Jacobian transpose * jacobian
  Matrix JtJJt;      // inv(JtJ) * Jt
  Matrix h_gn = Matrix(theta0->rows(), 1);      // Update
  T rate = this->option_.learning_rate;
  T cost = -1.0;
  T old_cost = -1.0;
  do {
    problem.EvaluateObjectiveFunction(*theta0, &old_cost);
    // Estimate cost
    problem.EvaluateObjectiveFunction(*theta0, &f);
    // Estimate jacobian
    problem.EvaluateGradient(*theta0, &J);
    // Compute JtJ
    LTS5::LinearAlgebra<T>::Gemm(J,
                                 TransposeType::kTranspose,
                                 1.0,
                                 J,
                                 TransposeType::kNoTranspose,
                                 0.0,
                                 &JtJ);
    // Compute update by solving : JtJ * h_gn = -Jt * f;
    int err_inv = LTS5::LinearAlgebra<T>::InverseMatrix(JtJ, &JtJ);
    if (!err_inv) {
      // JtJ has inv(JtJ) inside
      LTS5::LinearAlgebra<T>::Gemm(JtJ,
                                   TransposeType::kNoTranspose,
                                   -1.0,
                                   J,
                                   TransposeType::kTranspose,
                                   0.0,
                                   &JtJJt);

      LTS5::LinearAlgebra<T>::Gemv(JtJJt,
                                   TransposeType::kNoTranspose,
                                   1.0,
                                   f,
                                   0.0,
                                   &h_gn);
      // Update
      *theta0 = *theta0 + rate * h_gn;
      n_iter++;
      problem.EvaluateObjectiveFunction(*theta0, &cost);

    } else {
      std::cout << "Unable to invert JtJ" << std::endl;
      break;
    }
  } while (n_iter < this->option_.max_iter &&
           (std::abs(cost - old_cost) > this->option_.error_rate));
  std::cout << "#iter : " << n_iter << std::endl;
  return true;
}

template class LTS5_EXPORTS LTS5::GaussNewtonSolver<float>;
template class LTS5_EXPORTS LTS5::GaussNewtonSolver<double>;
}  // namespace LTS5
