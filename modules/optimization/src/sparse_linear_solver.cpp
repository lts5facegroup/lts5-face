/**
 *  @file   sparse_linear_solver.cpp
 *  @brief  Solve linear system or minimize a quadratic form. Can be used for
 *          UV unwrapping.
 *          Based on OpenNL/CGAL libraries
 *
 *  @author Christophe Ecabert
 *  @date   13/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <iostream>

#include "lts5/optimization/sparse_linear_solver.hpp"
#include "lts5/optimization/sparse_superlu_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/**
 *  @name SparseLinearSolver
 *  @fn SparseLinearSolver(void)
 *  @brief  Constructor
 */
template<typename T>
SparseLinearSolver<T>::SparseLinearSolver(void) {}

/**
 *  @name SparseLinearSolver
 *  @fn SparseLinearSolver(const int n_variables)
 *  @brief  Constructor
 *  @param[in]  n_variables Number of variables in the system
 */
template<typename T>
SparseLinearSolver<T>::SparseLinearSolver(const int n_variables) {
  this->Initialize(n_variables);
}

/**
 *  @name Initialize
 *  @fn void Initialize(const int n_variables)
 *  @brief  Initialize system
 *  @param[in]  n_variables Number of variables in the system
 */
template<typename T>
void SparseLinearSolver<T>::Initialize(const int n_variables) {
  state_ = SparseLinearSolver<T>::State::kInitial;
  least_squares_ = false;
  n_variables_ = n_variables;
  variables_ = new SparseLinearSolver<T>::Variables[n_variables_];
  A_ = nullptr;
  b_ = nullptr;
  x_ = nullptr;
  solver_ = new LTS5::SuperLUSolver<T>();
}

/**
 *  @name ~SparseLinearSolver
 *  @fn ~SparseLinearSolver(void)
 *  @brief  Destructor
 */
template<typename T>
SparseLinearSolver<T>::~SparseLinearSolver(void) {
  // Clear
  delete [] variables_;
  delete A_;
  delete b_;
  delete x_;
  delete solver_;
}

#pragma mark -
#pragma mark Process

/**
 *  @name Solve
 *  @fn int Solve(void)
 *  @brief  Solve system
 *  @return 0 if success, -1 otherwise
 */
template<typename T>
int SparseLinearSolver<T>::Solve(void) {
  // Check that system is constructed
  CheckState(kConstructed);

  // Solve system : Ax = b
  int error = solver_->Solve(*A_, *b_, x_);

  // Put solution into internal representtions
  VectorToVariables();

  // Switch state
  Transition(kConstructed, kSolved);

  // Clean up
  delete A_; A_ = nullptr;
  delete b_; b_ = nullptr;
  delete x_; x_ = nullptr;

  // Return solver output flag
  return error;
}


/**
 *  @name BeginSystem
 *  @fn void BeginSystem(void)
 *  @brief  Initialize a new system
 */
template<typename T>
void SparseLinearSolver<T>::BeginSystem(void) {
  // Start at first line
  current_row_ = 0;
  state_ = kInitial;
  Transition(kInitial, kInSystem);
  // Enumerate free variables
  int index = 0;
  for (int i = 0; i < n_variables_; ++i) {
    Variables& v = this->variable(i);
    if (!v.is_locked()) {
      v.set_index(index);
      index++;
    }
  }
  int n = index;
  A_ = new SparseMatrix<T>(n);
  b_ = new DenseVector<T>(n);
  x_ = new DenseVector<T>(n);
  for (int i = 0; i < n; ++i) {
    (*x_)[i] = 0;
    (*b_)[i] = 0;
  }
  VariablesToVector();
}

/**
 *  @name BeginRow
 *  @fn void BeginRow(void)
 *  @brief  Initialize a new row in the system
 */
template<typename T>
void SparseLinearSolver<T>::BeginRow(void) {
  // Update state
  Transition(kInSystem, kInRow);
  // Clear data
  af_.clear();
  if_.clear();
  al_.clear();
  xl_.clear();
  bk_ = 0;
}

/**
 *  @name SetRightHandSide
 *  @fn void SetRightHandSide(const T value)
 *  @brief  Set the value of the right hand side of the system
 *  @param[in]  value
 */
template<typename T>
void SparseLinearSolver<T>::SetRightHandSide(const T value) {
  // Check status
  CheckState(kInRow);
  // update value
  bk_ = value;
}

/**
 *  @name AddCoefficient
 *  @fn void AddCoefficient(const int variable_index, const T coeff_value)
 *  @brief  Add coefficient into the system
 *  @param[in]  variable_index  Index of the corresponding variables
 *  @param[in]  coeff_value     Value of the coefficient
 */
template<typename T>
void SparseLinearSolver<T>::AddCoefficient(const int variable_index,
                                     const T coeff_value) {
  // Check status
  CheckState(kInRow);
  // Get corresponding variables
  const Variables& v = this->variable(variable_index);
  if (v.is_locked()) {
    al_.push_back(coeff_value);
    xl_.push_back(v.value());
  } else {
    af_.push_back(coeff_value);
    if_.push_back(v.index());
  }
}

/**
 *  @name EndRow
 *  @fn void EndRow(void)
 *  @brief  Close a new row in the system
 */
template<typename T>
void SparseLinearSolver<T>::EndRow(void) {
  // LS ?
  int nf = static_cast<int>(af_.size());
  int nl = static_cast<int>(al_.size());
  if (least_squares_) {
    // LS, solve : A'A x = A'b
    for (int i = 0; i < nf; ++i) {
      for (int j = 0; j < nf; ++j) {
        //a_row,col = a_row,col + value
        A_->add_coeff(if_[i], if_[j], af_[i] * af_[j]);
      }
    }
    T S = -bk_;
    for (int j = 0; j < nl; ++j) {
      S += al_[j] * xl_[j];
    }
    for (int i = 0; i < nf; ++i) {
      (*b_)[if_[i]] -= af_[i] * S;
    }
  } else {
    // No LS, solve A x = b
    for (int i = 0; i < nf; ++i) {
      A_->add_coeff(current_row_, if_[i], af_[i]);
    }
    (*b_)[current_row_] = bk_;
    for (int i = 0; i < nl; ++i) {
      (*b_)[current_row_] -= al_[i] * xl_[i];
    }
  }
  current_row_++;
  Transition(kInRow, kInSystem);
}

/**
 *  @name EndSystem
 *  @fn void EndSystem(void)
 *  @brief  Close the system
 */
template<typename T>
void SparseLinearSolver<T>::EndSystem(void) {
  Transition(kInSystem, kConstructed);
}

#pragma mark -
#pragma mark Private

/**
 *  @name VectorToVariables
 *  @fn void VectorToVariables(void)
 *  @brief  Convert vector to internal representation
 */
template<typename T>
void SparseLinearSolver<T>::VectorToVariables(void) {
  for (int i = 0; i < n_variables_; ++i) {
    Variables& v = this->variable(i);
    if (!v.is_locked()) {
      v.set_value((*x_)[v.index()]);
    }
  }
}

/**
 *  @name VariablesToVector
 *  @fn void VariablesToVector(void)
 *  @brief  Convert internal representation into vector
 */
template<typename T>
void SparseLinearSolver<T>::VariablesToVector(void) {
  for (int i = 0; i < n_variables_; ++i) {
    const Variables& v = this->variable(i);
    if (!v.is_locked()) {
      (*x_)[v.index()] = v.value();
    }
  }
}

#pragma mark -
#pragma mark Explicit declaration

template class LTS5_EXPORTS SparseLinearSolver<float>;
template class LTS5_EXPORTS SparseLinearSolver<double>;


}  // namespace LTS5