/**
 *  @file   robust_matrix_completion.cpp
 *  @brief  Matrix completion algorithm based on :
 *          Robust Principal Component Analysis with Missing Data
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   28/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <limits>

#include "lts5/optimization/robust_matrix_completion.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Parameters

/*
 * @name    Params
 * @fn      Params(void)
 * @brief   Constructor
 */
template<typename T>
MatrixCompletionParams<T>::MatrixCompletionParams(void) : mu(1e-4),
                                                          mu_max(1e10),
                                                          rho(1.1),
                                                          lambda(-1.0),
                                                          tol(1e-5),
                                                          rank(0),
                                                          max_iter(500) {
}

/*
 * @name    MatrixCompletionParams
 * @fn      MatrixCompletionParams(const Matrix& data)
 * @brief   Constructor, initialize parameters for a given dataset
 * @param[in] data    Data to reconstruct
 */
template<typename T>
MatrixCompletionParams<T>::MatrixCompletionParams(const Matrix& data) :
        mu(1e-4),
        mu_max(1e10),
        rho(1.1),
        lambda(-1.0),
        tol(1e-5),
        rank(0),
        max_iter(500) {
  this->Init(data);
}

/*
 * @name  Init
 * @fn    void Init(const Matrix& data)
 * @brief Initialize parameters for a given dataset
 * @param[in] data    Data to reconstruct
 */
template<typename T>
void MatrixCompletionParams<T>::Init(const Matrix &data) {
  // Lambda
  if (lambda < T(0.0)) {
    this->InitLambda(data);
  }
  // Rank
  if (this->rank == 0) {
    this->InitRank(data);
  }
}

/*
 * @name    Init
 * @fn  void InitLambda(const Matrix& data)
 * @brief Initialize lambda parameters for a given dataset
 * @param[in] data Dataset to complete
 */
template<typename T>
void MatrixCompletionParams<T>::InitLambda(const Matrix &data) {
  // Lambda
  auto m = static_cast<T>(data.rows());
  auto n = static_cast<T>(data.cols());
  lambda = T(1.0) / std::sqrt(std::max(m, n));
}

/*
 * @name    Init
 * @fn  void InitRank(const Matrix& data)
 * @brief Initialize rank parameters for a given dataset
 * @param[in] data Dataset to complete
 */
template<typename T>
void MatrixCompletionParams<T>::InitRank(const Matrix &data) {
  // Rank
  using Lapack = typename LTS5::LinearAlgebra<T>::Lapack;
  using SVD = typename LTS5::LinearAlgebra<T>::DCSvdDecomposition;
  SVD svd;
  svd.Init(data, Lapack::SVDType::kStd);
  svd.Compute();
  this->rank = 0;
  for (int i = 0; i < svd.p_.kMin; ++i) {
    if (svd.p_.kS[i] > T(0.0)) {
      this->rank += 1;
    }
  }
  LTS5_LOG_INFO("Rank upper bounds : " << this->rank);
}


#pragma mark -
#pragma mark Matrix Completion - Non Convex

/*
 * @name  MatrixCompletionMF
 * @fn    MatrixCompletionMF(void) = default
 * @brief Constructor
 */
template<typename T>
MatrixCompletionMF<T>::MatrixCompletionMF(void) : initialized_(false) {
}

/*
 * @name  Init
 * @fn    void Init(const Matrix& obs)
 * @brief Initialize internal state for a given observation. Override previous
 *        initialization
 * @param[in] obs Observed data
 */
template<typename T>
void MatrixCompletionMF<T>::Init(const Matrix& obs) {
  // Init parameters
  p_.Init(obs);
  // Set dimension
  m_ = static_cast<int>(obs.rows());
  n_ = static_cast<int>(obs.cols());
  // Init container (dimension)
  G_.setIdentity(m_, p_.rank);
  H_.setZero(n_, p_.rank);
  E_.setZero(m_, n_);
  Y_.setZero(m_, n_);
  P_.setZero(m_, n_);
  PH_.setZero(P_.rows(), H_.cols());
  PtG_.setZero(P_.cols(), G_.cols());
  // Init svds
  svd_ph_.Init(PH_, LA::Lapack::SVDType::kStd);
  svd_spectral_.Init(PtG_, LA::Lapack::SVDType::kStd);
  // Done
  initialized_ = true;
}

/*
 * @name Solve
 * @fn void Solve(const Matrix& obs, Matrix* rec, Matrix* outliner)
 * @brief     Iteratively reconstruct observation
 * @param[in] obs         Data observed
 * @param[out] rec        Completed data
 * @param[out] outliner   Outliners
 */
template<typename T>
int MatrixCompletionMF<T>::Solve(const Matrix& obs,
                                 Matrix* rec,
                                 Matrix* outliner) {
  // Initialized ?
  if (!initialized_) {
    this->Init(obs);
  }
  // Define mask
  this->UpdateOmega(obs);
  // Initialize containers
  G_.setIdentity();
  H_.setZero();
  E_.setZero();
  Y_.setZero();
  // Init var
  size_t cnt = 0;
  T mu = p_.mu;
  T cost = std::numeric_limits<T>::max();
  // Start iteration
  while(cnt < p_.max_iter) {
    // Compute P
    P_ = obs - E_ + (Y_ / mu);
    // Update G
    this->UpdateG();
    // Update H
    this->UpdateH(T(1.0) / mu);
    // Update E
    this->UpdateE(obs, mu, p_.lambda / mu);
    // Update Y (Lagrange multiplier)
    Diff_ = obs - (G_ * H_.transpose()) - E_;
    Y_ += mu * Diff_;
    // Update mu
    mu = std::min(p_.rho * mu, p_.mu_max);
    // Update cost and counter for convergence check
    cost = Diff_.norm();
    LTS5_LOG_DEBUG("it : " << cnt << ", cost : " << cost);
    if (cost < p_.tol) {
      // Reach convergence
      break;
    }
    cnt += 1;
  }
  // Finalize E_[omegac] = 0.0
  // omega has 1 where data are present and 0 elsewhere, therefore element-wise
  // product will set the part that is not belong to omega (omegac) to zero.
  E_ = E_.cwiseProduct(omega_);
  // Return data
  *outliner = E_;
  *rec = G_ * H_.transpose();
  // Check if reach max iter or converged
  int ret = cnt == p_.max_iter ? -1 : 0;
  return ret;
}

/**
 * @name  UpdateOmega
 * @fn    void UpdateOmega(const Matrix& obs)
 * @brief Initialize mask indicating where observed data are (binary mask)
 * @param[in] obs Observed data with missing element marked as 0
 */
template<typename T>
void MatrixCompletionMF<T>::UpdateOmega(const Matrix& obs) {
  omega_ = (obs.array() != T(0.0)).select(Matrix::Ones(obs.rows(), obs.cols()),
                                          Matrix::Zero(obs.rows(), obs.cols()));
}

/*
 * @name  UpdateG
 * @fn    void UpdateG(void)
 * @brief Update G
 */
template<typename T>
void MatrixCompletionMF<T>::UpdateG(void) {
  static Matrix U, Vt;
  // Compute PH
  LA::Gemm(P_, TType::kNoTranspose, T(1.0),
           H_, TType::kNoTranspose, T(0.0),
           &PH_);
  // Compute G*
  svd_ph_.Update(PH_);
  svd_ph_.Compute();
  svd_ph_.Get(&U, nullptr, &Vt);
  LA::Gemm(U, TType::kNoTranspose, T(1.0),
           Vt, TType::kNoTranspose, T(0.0),
           &G_);
}

/*
 * @name  UpdateH
 * @fn    void UpdateH(void)
 * @brief Update H with spectral soft-thresholding
 * @param[in] thresh Spectral threshold
 */
template<typename T>
void MatrixCompletionMF<T>::UpdateH(const T thresh) {
  static Matrix U, S, Vt;
  // Compute P_' * G_;
  LA::Gemm(P_, TType::kTranspose, T(1.0),
           G_, TType::kNoTranspose, T(0.0),
           &PtG_);
  // Perform SVD
  svd_spectral_.Update(PtG_);
  svd_spectral_.Compute();
  svd_spectral_.Get(&U, &S, &Vt);
  // Filter
  for (int i = 0; i < svd_spectral_.p_.kMin; ++i) {
    S(i, i) = std::max(S(i, i) - thresh, T(0.0));
  }
  // Reconstruct
  //TODO: (christophe) Use lapack here when working
  H_ = U * (S * Vt);
}

/*
 * @name  UpdateE
 * @fn    void UpdateE(const Matrix& Z, const T mu, const T thresh)
 * @brief Update E with soft-thresholding
 * @param[in] Z       Observed data
 * @param[in] mu      Regularizer (dynamic)
 * @param[in] thresh Spectral threshold
 */
template<typename T>
void MatrixCompletionMF<T>::UpdateE(const Matrix& Z,
                                    const T mu,
                                    const T thresh) {
  static Matrix D;
  D = Z - (G_ * H_.transpose()) + (Y_ / mu);
  assert(D.rows() == E_.rows() && D.cols() == E_.cols());
  for(int c = 0; c < D.cols(); ++c) {
    for (int r = 0; r < D.rows(); ++r) {
      // In omega set ?
      if (omega_(r, c) != 0.f) {
        // Apply soft-threshold
        T Dij = D(r, c);
        E_(r, c) = (Dij > thresh ?
                    Dij - thresh :
                    Dij < -thresh ?
                    Dij + thresh :
                    T(0.0));
      } else {
        E_(r, c) = D(r, c);
      }
    }
  }
}

#pragma mark -
#pragma mark Matrix Completion - Convex

/*
 * @name  MatrixCompletion
 * @fn    MatrixCompletion(void) = default
 * @brief Constructor
 */
template<typename T>
MatrixCompletion<T>::MatrixCompletion(void) : initialized_(false) {
}

/*
 * @name  Init
 * @fn    void Init(const Matrix& obs)
 * @brief Initialize internal state for a given observation. Override previous
 *        initialization
 * @param[in] obs Observed data
 */
template<typename T>
void MatrixCompletion<T>::Init(const Matrix& obs) {
  // Init parameters
  p_.rank = 1;  // Not needed here, therefore put value bigger than 0 to avoid
  // automatic computation
  p_.Init(obs);
  // Set dimension
  m_ = static_cast<int>(obs.rows());
  n_ = static_cast<int>(obs.cols());
  // Init container (dimension)
  T_.setZero(m_, n_);
  E_.setZero(m_, n_);
  Y_.setZero(m_, n_);
  // Init svds
  svd_spectral_.Init(T_, LA::Lapack::SVDType::kStd);
  // Done
  initialized_ = true;
}

/*
 * @name Solve
 * @fn void Solve(const Matrix& obs, Matrix* rec, Matrix* outliner)
 * @brief     Iteratively reconstruct observation
 * @param[in] obs         Data observed
 * @param[out] rec        Completed data
 * @param[out] outliner   Outliners
 */
template<typename T>
int MatrixCompletion<T>::Solve(const Matrix& obs,
                                 Matrix* rec,
                                 Matrix* outliner) {
  // Initialized ?
  if (!initialized_) {
    this->Init(obs);
  }
  // Define mask
  this->UpdateOmega(obs);
  // Initialize containers
  T_.setZero();
  E_.setZero();
  Y_.setZero();
  // Init var
  size_t cnt = 0;
  T mu = p_.mu;
  T cost = std::numeric_limits<T>::max();
  // Start iteration
  while(cnt < p_.max_iter) {
    // Compute P
    T_ = obs - E_ + (Y_ / mu);
    // Update X
    this->UpdateX(T(1.0) / mu);
    // Update E
    this->UpdateE(obs, mu, p_.lambda / mu);
    // Update Y (Lagrange multiplier)
    Diff_ = obs - X_ - E_;
    Y_ += mu * Diff_;
    // Update mu
    mu = std::min(p_.rho * mu, p_.mu_max);
    // Update cost and counter for convergence check
    cost = Diff_.norm();
    LTS5_LOG_DEBUG("it : " << cnt << ", cost : " << cost);
    if (cost < p_.tol) {
      // Reach convergence
      break;
    }
    cnt += 1;
  }
  // Finalize E_[omegac] = 0.0
  // omega has 1 where data are present and 0 elsewhere, therefore element-wise
  // product will set the part that is not belong to omega (omegac) to zero.
  E_ = E_.cwiseProduct(omega_);
  // Return data
  *outliner = E_;
  *rec = X_;
  // Check if reach max iter or converged
  int ret = cnt == p_.max_iter ? -1 : 0;
  return ret;
}

/*
 * @name  UpdateOmega
 * @fn    void UpdateOmega(const Matrix& obs)
 * @brief Initialize mask indicating where observed data are (binary mask)
 * @param[in] obs Observed data with missing element marked as 0
 */
template<typename T>
void MatrixCompletion<T>::UpdateOmega(const Matrix& obs) {
  omega_ = (obs.array() != T(0.0)).select(Matrix::Ones(obs.rows(), obs.cols()),
                                          Matrix::Zero(obs.rows(), obs.cols()));
}

/*
 * @name  UpdateX
 * @fn    void UpdateX(const T thresh)
 * @brief Update X
 * @param[in] thresh Spectral treshold
 */
template<typename T>
void MatrixCompletion<T>::UpdateX(const T thresh) {
  static Matrix U, S, Vt;
  // Perform SVD
  svd_spectral_.Update(T_);
  svd_spectral_.Compute();
  svd_spectral_.Get(&U, &S, &Vt);
  // Filter
  for (int i = 0; i < svd_spectral_.p_.kMin; ++i) {
    S(i, i) = std::max(S(i, i) - thresh, T(0.0));
  }
  // Reconstruct
  //TODO: (christophe) Use lapack here when working
  X_ = U * (S * Vt);
}

/*
 * @name  UpdateE
 * @fn    void UpdateE(const Matrix& Z, const T mu, const T thresh)
 * @brief Update E with soft-thresholding
 * @param[in] Z       Observed data
 * @param[in] mu      Regularizer (dynamic)
 * @param[in] thresh Spectral threshold
 */
template<typename T>
void MatrixCompletion<T>::UpdateE(const Matrix& Z, const T mu, const T thresh) {
  static Matrix D;
  D = Z - X_ + (Y_ / mu);
  assert(D.rows() == E_.rows() && D.cols() == E_.cols());
  for(int c = 0; c < D.cols(); ++c) {
    for (int r = 0; r < D.rows(); ++r) {
      // In omega set ?
      if (omega_(r, c) != 0.f) {
        // Apply soft-threshold
        T Dij = D(r, c);
        E_(r, c) = (Dij > thresh ?
                    Dij - thresh :
                    Dij < -thresh ?
                    Dij + thresh :
                    T(0.0));
      } else {
        E_(r, c) = D(r, c);
      }
    }
  }
}

#pragma mark -
#pragma mark Explicit instantiation

/** Float */
template class MatrixCompletionParams<float>;
/** Double */
template class MatrixCompletionParams<double>;

/** Float */
template class MatrixCompletionMF<float>;
/** Double */
template class MatrixCompletionMF<double>;

/** Float */
template class MatrixCompletion<float>;
/** Double */
template class MatrixCompletion<double>;

}  // namepsace LTS5