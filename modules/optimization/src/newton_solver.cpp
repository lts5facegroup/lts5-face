/**
 *  @file   newton_solver.hpp
 *  @brief  Newton solver
 *
 *  @author Christophe Ecabert
 *  @date   04/01/16
 *  Copyright (c) 2016 Ecabert Christophe. All rights reserved.
 */

#include "Eigen/Core"

#include "lts5/optimization/newton_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name NewtonSolver
 *  @fn NewtonSolver(void)
 *  @brief  Constructor
 */
template<class T>
NewtonSolver<T>::NewtonSolver(void) {}

/*
 *  @name ~NewtonSolver
 *  @fn ~NewtonSolver(void)
 *  @brief  Destructor
 */
template<class T>
NewtonSolver<T>::~NewtonSolver(void) {}

/*
 *  @name Minimize
 *  @fn virtual bool Minimize(const BaseProblem<T>& problem,
                             Matrix* theta0) = 0
 *  @brief  Minimize the given #problem
 *  @param[in]      problem Problem to minimize
 *  @param[in,out]  theta0  Starting parameter's value
 *  @return True if converge, false otherwise
 */
template<class T>
bool NewtonSolver<T>::Minimize(const BaseProblem<T>& problem,
                               Matrix* theta0) {
  int n_iter = 0;
  Matrix grad;    // Gradient
  Matrix hess;    // Hessian
  Matrix h;       // Coefficient update
  /*lapack_int nrhs = static_cast<lapack_int>(theta0->cols());

  T cost = -1.0; // Current cost
  T old_cost = -1.0;
  char uplo = 'L';
  do {
    // Estimate cost
    problem.EvaluateObjectiveFunction(*theta0, &old_cost);
    // Estimate gradient
    problem.EvaluateGradient(*theta0, &grad);
    // Estimate hessian
    problem.EvaluateHessian(*theta0, &hess);
    // Compute update h by solving H * h = - grad
    // if fail (H not positive definite), h = -grad
    // Solve using cholesky decomp
    lapack_int N = static_cast<lapack_int>(hess.rows());
    lapack_int info = -1;
    if (sizeof(T) == 4) {
      spotrf_(&uplo,
              &N,
              reinterpret_cast<lapack_flt*>(hess.data()),
              &N,
              &info);
    } else {
      dpotrf_(&uplo,
              &N,
              reinterpret_cast<lapack_dbl*>(hess.data()),
              &N,
              &info);
    }
    // Positive definite ?
    if (info == 0) {
      // Yes,
      h = -grad;
      if (sizeof(T) == 4) {
        spotrs_(&uplo,
                &N,
                &nrhs,
                reinterpret_cast<lapack_flt*>(hess.data()),
                &N,
                reinterpret_cast<lapack_flt*>(h.data()),
                &N,
                &info);
      } else {
        dpotrs_(&uplo,
                &N,
                &nrhs,
                reinterpret_cast<lapack_dbl*>(hess.data()),
                &N,
                reinterpret_cast<lapack_dbl*>(h.data()),
                &N,
                &info);
      }
    } else {
      // No (something went wrong), steepest descent update like
      h = -grad;
    }
    // Update parameters
    *theta0 += this->option_.learning_rate * h;
    // Evaluate objective function
    problem.EvaluateObjectiveFunction(*theta0, &cost);
    n_iter++;
  } while (n_iter < this->option_.max_iter &&
           std::abs(cost - old_cost) > this->option_.error_rate);
  return (n_iter != (this->option_.max_iter - 1));*/
  return (n_iter != 0);
}

template class LTS5_EXPORTS NewtonSolver<double>;
template class LTS5_EXPORTS NewtonSolver<float>;

}  // namespace LTS5
