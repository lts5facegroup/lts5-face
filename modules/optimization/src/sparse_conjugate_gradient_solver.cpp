/**
 *  @file   sparse_conjugate_gradient_solver.cpp
 *  @brief  The Conjugate Gradient algorithm WITHOUT preconditioner:
 *            Ashby, Manteuffel, Saylor
 *              A taxononmy for conjugate gradient methods
 *              SIAM J Numer Anal 27, 1542-1568 (1990)
 *          Based on OpenNL library
 *
 *  @author Christophe Ecabert
 *  @date   13/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <assert.h>

#include "lts5/optimization/sparse_conjugate_gradient_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name ConjugateGradientSolver
 *  @fn ConjugateGradientSolver(void)
 *  @brief  Constructor
 */
template<typename T>
ConjugateGradientSolver<T>::ConjugateGradientSolver(void) {
  this->epsilon_ = 1e-6;
  this->max_iteration_ = 0;
  this->unwrap_ = true;
}

#pragma mark -
#pragma mark Process

/*
 *  @name Solve
 *  @fn bool Solve(const LTS5::SparseMatrix<T>& A,
 *                 const LTS5::DenseVector<T>& b,
 *                 LTS5::DenseVector<T>* x)
 *  @brief  Solve sparse system Ax = b
 *  @param[in]  A System Matrix
 *  @param[in]  b System vector
 *  @param[out] x Solution vector
 *  @return 0 if success, -1 otherwise
 */
template<typename T>
int ConjugateGradientSolver<T>::Solve(const LTS5::SparseMatrix<T>& A,
                                      const LTS5::DenseVector<T>& b,
                                      LTS5::DenseVector<T>* x) {
  // Get square matrix dimension
  int n = A.dimension();
  // Sanity check
  assert(n > 0);

  // Define maximum number of iterations if needed
  if (this->max_iteration_ == 0) {
    this->max_iteration_ = 5 * n;
  }

  // Initialize variables
  LTS5::DenseVector<T> g(n);
  LTS5::DenseVector<T> r(n);
  LTS5::DenseVector<T> p(n);
  // Iteration number
  int its = 0;
  T t = -1, tau = -1, sig = -1, rho = -1, gam = -1;
  T bnorm2 = DenseVector<T>::dot(b, b);
  // Error to reach
  T err = this->epsilon_ * this->epsilon_ * bnorm2;

  // Residu g = b - A*x
  g = A * *x;
  DenseVector<T>::axpy(-1, b, &g);
  DenseVector<T>::scal(-1, &g);
  // Initially, r = g = b - A*x
  DenseVector<T>::copy(g, &r);  // r = g

  // Initial error, gg = < g | g >
  T gg = DenseVector<T>::dot(g, g);

  // Start looping
  while (gg > err && its < this->max_iteration_) {
    p = A * r;
    rho = DenseVector<T>::dot(p, p);
    sig = DenseVector<T>::dot(r, p);
    tau = DenseVector<T>::dot(g, r);
    assert(sig != 0.0);
    t = tau/sig;
    DenseVector<T>::axpy(t, r, x);
    DenseVector<T>::axpy(-t, p, &g);
    assert(tau != 0.0);
    gam = (t * t * rho - tau) / tau;
    DenseVector<T>::scal(gam, &r);
    DenseVector<T>::axpy(1, g, &r);
    gg = DenseVector<T>::dot(g, g); // Update error gg = (g|g)
    ++its;
  }
  // Done
  return (gg <= err ? 0 : -1);
}

#pragma mark -
#pragma mark Explicit declaration

template class LTS5::ConjugateGradientSolver<float>;
template class LTS5::ConjugateGradientSolver<double>;

}  // namespace LTS5