/**
 *  @file   feature_sign_solver.cpp
 *  @brief  Feature Sign search algorithm, solve :
 *          x* = argmin|| y - Ax ||^2 + gamma * |x|
 *
 *  @see    Efficient Sparse Coding Algorithms, H.Lee et al
 *  @author Christophe Ecabert
 *  @date   17/12/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <limits>
#include <numeric>

#include "Eigen/LU"

#include "lts5/optimization/feature_sign_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


/*
 *  @name FeatureSignSolver
 *  @fn FeatureSignSolver(void)
 *  @brief  Constructor
 */
template<class T>
FeatureSignSolver<T>::FeatureSignSolver(void) {}

/**
 *  @name ~FeatureSignSolver
 *  @fn ~FeatureSignSolver(void)
 *  @brief  Destructor
 */
template<class T>
FeatureSignSolver<T>::~FeatureSignSolver(void) {}

/**
 *  @name Minimize
 *  @fn bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override
 *  @brief  Minimize the given #problem
 *  @param[in]      problem Problem to minimize
 *  @param[in,out]  theta0  Starting parameter's value
 *  @return True if converge, false otherwise
 */
template<class T>
bool FeatureSignSolver<T>::Minimize(const BaseProblem<T>& problem,
                                         Matrix* theta0) {
  // Initialize - Step 1
  // -----------------------------------------------------------
  Matrix x = Matrix::Zero(theta0->rows(), 1);
  Matrix theta_sign = Matrix::Zero(theta0->rows(), 1);
  Matrix active_set = Matrix::Zero(theta0->rows(), 1);
  Matrix grad;
  const Matrix& A = problem.get_A();
  const Matrix& AtA = problem.get_AtA();
  const Matrix& Aty = problem.get_Aty();

  // Matrix rank
  int n_obs = -1;
  int n_var = -1;
  problem.get_dimensions(&n_obs, &n_var);
  const int rankA = std::min(n_obs - 10, n_var - 10);

  // Start to loop
  std::vector<int> act_idx_0;
  std::vector<int> act_idx_1;
  bool optimum = false;
  for (int k = 0; k < this->option_.max_iter; ++k) {
    // Step 2,
    // -----------------------------------------------------------
    this->Find(active_set, 0.0, &act_idx_0);
    problem.EvaluateGradient(x, &grad);
    this->Sign(x, &theta_sign);
    // Find idx + value : max( |grad(x)|) where x_i == 0
    int max_idx = -1;
    T max_value = -1.0;
    this->MaxAbs(grad, act_idx_0, &max_value, &max_idx);
    // Check for optimum
    if (max_idx != -1 && max_value >= this->option_.gamma) {
      // Active index
      active_set(act_idx_0[max_idx], 0) = 1;
      theta_sign(act_idx_0[max_idx], 0) = -((0.0 < grad(act_idx_0[max_idx], 0)) -
                                            (grad(act_idx_0[max_idx], 0) < 0.0));
    } else {
      if (optimum) {
        break;
      }
    }
    this->Find(active_set, 1.0, &act_idx_1);
    // Perform some check
    if (act_idx_1.size() > rankA) {
      std::cout << "Warning : sparsity penalty is too small, too many coefficients are activated" << std::endl;
      return false;
    }
    if (act_idx_1.empty()) {
      *theta0 = x;
      return true;
    }

    // Loop
    for (int kk = 0; kk < this->option_.max_iter ; ++kk) {
      if (act_idx_1.empty()) {
        *theta0 = x;
        return true;
      }
      // Step 3, Feature-Sign Step
      // -----------------------------------------------------------
      // Define sub problem (from active set)
      const int n_sub = static_cast<int>(act_idx_1.size());
      Matrix x_sub = Matrix(n_sub, 1);
      Matrix theta_sign_sub = Matrix(n_sub, 1);
      Matrix Aty_sub = Matrix(n_sub, 1);
      Matrix AtA_sub = Matrix(n_sub, n_sub);
      for (int i = 0; i < n_sub; ++i) {
        const int idx = act_idx_1[i];
        x_sub(i, 0) = x(idx, 0);
        theta_sign_sub(i, 0) = theta_sign(idx, 0);
        Aty_sub(i, 0) = Aty(idx, 0);
        for (int ii = 0; ii < n_sub; ++ii) {
          AtA_sub(i, ii) = AtA(idx, act_idx_1[ii]);
        }
      }
      // TODO: (Christophe) Optimize matrix inversion + product later on
      // Comput anatically solution
      Matrix x_new = AtA_sub.inverse() * (Aty_sub - ((this->option_.gamma * 0.5) * theta_sign_sub));
      const int n_val = static_cast<int>(x_new.rows());
      int i = 0;
      for (; i < n_val; ++i) {
        T sign_x = (0.0 < x_sub(i, 0)) - (x_sub(i, 0) < 0.0);
        T sign_x_new = (0.0 < x_new(i, 0)) - (x_new(i, 0) < 0.0);
        if (sign_x != sign_x_new) {
          break;
        }
      }
      // Optimum ?
      if (i == n_val) {
        // Update solution
        optimum = true;
        for (i = 0; i < n_val; ++i) {
          x(act_idx_1[i], 0) = x_new(i, 0);
          (*theta0)(act_idx_1[i], 0) = x_new(i, 0);
        }
        break;
      }
      // Do line search x -> x_new
      T lsearch = 0.0;
      std::vector<T> progress(n_val + 1);
      Matrix diff_x = x_new - x_sub;
      for (i = 0; i < n_val; ++i) {
        progress[i] = -x_sub(i, 0) / diff_x(i, 0);
      }
      progress[i] = 1.0;
      T a = 0.0;
      T fobj_lsearch = 0.0;
      for (int r = 0; r < A.rows(); ++r) {
        T elem_val = 0.0;
        for (int c = 0; c < act_idx_1.size(); ++c) {
          elem_val += A(r, act_idx_1[c]) * diff_x(c, 0);
          if (r == 0) {
            fobj_lsearch += std::abs(x_sub(c, 0));
          }
        }
        a += elem_val * elem_val;
      }
      a *= 0.5;
      fobj_lsearch *= this->option_.gamma;
      Matrix b = (x_sub.transpose() * (AtA_sub * diff_x));
      b -= diff_x.transpose() * Aty_sub;
      assert(b.cols() == b.rows() && b.cols() == 1);
      // Sort progress
      std::vector<int> sort_idx;
      this->SortIndexes(progress, &sort_idx);
      std::vector<int> remove_idx;
      for (i = 0; i < progress.size(); ++i) {
        T t = progress[sort_idx[i]];
        if (t > 0.0 && t <= 1) {
          Matrix s_temp = x_sub + diff_x;
          T fobj_temp = a * t * t + b(0, 0) * t;
          T sum = 0.0;
          for (int p = 0; p < s_temp.rows(); ++p) {
            sum += std::abs(s_temp(p, 0));
          }
          fobj_temp += this->option_.gamma * sum;
          // Update
          if (fobj_temp < fobj_lsearch) {
            fobj_lsearch = fobj_temp;
            lsearch = t;
            if (t < 1) {
              remove_idx.push_back(sort_idx[i]);
            }
          } else if (fobj_temp > fobj_lsearch) {
            break;
          } else {
            int cnt_zero = 0;
            for (int p = 0; p < x_sub.rows(); ++p) {
              cnt_zero = x_sub(p, 0) == 0 ? cnt_zero + 1 : cnt_zero;
            }
            if (cnt_zero == 0) {
              lsearch = t;
              fobj_lsearch = fobj_temp;
              if (t < 1) {
                remove_idx.push_back(sort_idx[i]);
              }
            }
          }
        }
      }
      if (lsearch > 0) {
        // Update x
        x_new = x_sub + lsearch * diff_x;
        for (int p = 0; p < act_idx_1.size(); ++p) {
          x(act_idx_1[p], 0) = x_new(p, 0);
          theta_sign(act_idx_1[p], 0) = (0.0 < x_new(p, 0)) - (x_new(p, 0) < 0.0);
        }
      }
      // if x encounters zero along the line search, then remove it from
      // active set
      if (lsearch > 0 && lsearch < 1) {
        for (i = 0; i < act_idx_1.size(); ) {
          if (std::abs(x(act_idx_1[i], 0)) < std::numeric_limits<T>::epsilon()) {
            x(act_idx_1[i], 0) = 0.0;
            theta_sign(act_idx_1[i], 0) = 0.0;
            active_set(act_idx_1[i], 0) = 0.0;
            act_idx_1.erase(act_idx_1.begin() + i);
          } else {
            ++i;
          }
        }
      }
    }
  }
  *theta0 = x;
  return optimum;
}

/*
 *  @name Find
 *  @fn void Find(const Matrix& vector, const T value, std::vector<int>* index)
 *  @brief  Find the indexes where a given value stays
 *  @param[in]  vector  Vector to search in
 *  @param[in]  value   Value to look for
 *  @param[out] index   Index where to find the actual value
 */
template<class T>
void FeatureSignSolver<T>::Find(const Matrix& vector,
                                const T value,
                                std::vector<int>* index) {
  const int n_row = static_cast<int>(vector.rows());
  index->clear();
  for (int i = 0; i < n_row; ++i) {
    if (vector(i, 0) == value) {
      index->push_back(i);
    }
  }
}

/*
 *  @name Sign
 *  @fn void Sign(const Matrix& x, Matrix* sign_x)
 *  @brief  Compute element-wise the sign of a vector #x (-1, 0, 1)
 *  @param[in]  x       Vector to determine the sign
 *  @param[out] sign_x  Sign of #x (-1, 0, 1)
 */
template<class T>
void FeatureSignSolver<T>::Sign(const Matrix& x, Matrix* sign_x) {
  const int n_row = static_cast<int>(x.rows());
  sign_x->resize(n_row, 1);
  for (int i = 0 ; i < n_row; ++i) {
    (*sign_x)(i, 0) = (T(0) < x(i, 0)) - (x(i, 0) < T(0));
  }
}

/*
 *  @name MaxAbs
 *  @fn void MaxAbs(const Matrix& x, const std::vector<int>& list_idx, T* value, int* idx)
 *  @brief  Find the maximum absolute value of a vector for a given list of element
 *  @param[in]  x         Vector to search in
 *  @param[in]  list_idx  List of element of interest
 *  @param[out] value     Maximum value
 *  @param[out] idx       Corresponding index
 */
template<class T>
void FeatureSignSolver<T>::MaxAbs(const Matrix& x,
                                  const std::vector<int>& list_idx,
                                  T* value,
                                  int* idx) {
  const int n_elem = static_cast<int>(list_idx.size());
  T max_val = std::numeric_limits<T>::lowest();
  for (int i = 0; i < n_elem; ++i) {
    T val = std::abs(x(list_idx[i], 0));
    if (val > max_val) {
      max_val = val;
      *value = val;
      *idx = i;
    }
  }
}

/*
 *  @name SortIndexes
 *  @fn void SortIndexes(const std::vector<T>& values, std::vector<int>* sord_idx)
 *  @brief Sort vector by increasing value
 *  @param[in]  values    Vector to sort
 *  @param[out] sort_idx  Vector of sorted indexes
 */
template<class T>
void FeatureSignSolver<T>::SortIndexes(const std::vector<T>& values,
                                       std::vector<int>* sort_idx) {
  // Init index vector
  sort_idx->resize(values.size());
  // Fill [0, ..., k]
  std::iota(sort_idx->begin(), sort_idx->end(), static_cast<int>(0));
  // Sort value
  std::sort(sort_idx->begin(),
            sort_idx->end(),
            [&values](size_t a, size_t b) -> bool {return values[a] < values[b];});
}

template class LTS5_EXPORTS LTS5::FeatureSignSolver<float>;
template class LTS5_EXPORTS LTS5::FeatureSignSolver<double>;


}  // namespace LTS5
