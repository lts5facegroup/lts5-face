/**
 *  @file   lagrange_dual_solver.cpp
 *  @brief  Lagrange dual solver definition for optimization of type :
 *          B* = argmin 0.5 || X - B*S ||^2
 *          subject to || B(:, j) || < c
 *
 *  @author Christophe Ecabert
 *  @date   22/12/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <limits>
#include <numeric>
#include <random>
#include <fstream>
#include <chrono>

#include "Eigen/LU"

#include "lts5/optimization/lagrange_dual_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


/*
 *  @name LagrangeDualSolver
 *  @fn LagrangeDualSolver(void)
 *  @brief  Constructor
 */
template<class T>
LagrangeDualSolver<T>::LagrangeDualSolver(void) {
  solver_ = new LTS5::NewtonSolver<T>();
  internal_option_.learning_rate = 1.0;
  solver_->set_option(internal_option_);
}

/*
 *  @name ~LagrangeDualSolver
 *  @fn ~LagrangeDualSolver(void)
 *  @brief  Destructor
 */
template<class T>
LagrangeDualSolver<T>::~LagrangeDualSolver(void) {
  delete solver_;
}

/*
 *  @name Minimize
 *  @fn bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override
 *  @brief  Minimize the given #problem
 *  @param[in]      problem Problem to minimize
 *  @param[in,out]  theta0  Starting parameter's value
 *  @return True if converge, false otherwise
 */
template<class T>
bool LagrangeDualSolver<T>::Minimize(const BaseProblem<T>& problem,
                                         Matrix* theta0) {
  // Initialize dual variable
  int L = -1;
  int M = -1;
  problem.get_dimensions(&L, &M);
  std::uniform_real_distribution<T> dist(0.0, 1.0);
  auto seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::mt19937_64 gen(seed);
  Matrix dual = Matrix(M, 1);
  for (int i = 0; i < M; ++i) {
    dual(i, 0) = dist(gen);
  }

  // Minimize dual problem
  bool ret = solver_->Minimize(problem, &dual);


  // Recover basis
  Matrix S = problem.get_A();
  Matrix XSt = problem.get_y() * S.transpose();
  Matrix tt = S * S.transpose();
  for (int i = 0; i < tt.rows(); ++i) {
    tt(i, i) += dual(i, 0);
  }

  Matrix B = tt.inverse() * XSt.transpose();
  *theta0 = B.transpose();


  return ret;
}

template class LTS5_EXPORTS LagrangeDualSolver<double>;
template class LTS5_EXPORTS LagrangeDualSolver<float>;

}  // namespace LTS5