/**
 *  @file   gradient_descent.cpp
 *  @brief  Gradient-Descent solver
 *
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <iostream>

#include "Eigen/Core"

#include "lts5/optimization/gradient_descent_solver.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name GradientDescentSolver
 *  @fn GradientDescentSolver(void)
 *  @brief  Constructor
 */
template<class T>
GradientDescentSolver<T>::GradientDescentSolver(void) {}

/*
 *  @name ~GradientDescentSolver
 *  @fn ~GradientDescentSolver(void)
 *  @brief  Destructor
 */
template<class T>
GradientDescentSolver<T>::~GradientDescentSolver(void) {}

/*
 *  @name Minimize
 *  @fn virtual bool Minimize(const BaseProblem<T>& problem,
                             Matrix& theta0) = 0
 *  @brief  Minimize the given #problem
 *  @param[in]      problem Problem to minimize
 *  @param[in,out]  theta0  Starting parameter's value
 *  @return True if converge, false otherwise
 */
template<class T>
bool GradientDescentSolver<T>::Minimize(const BaseProblem<T>& problem,
                                        Matrix* theta0) {
  int n_iter = 0;
  //T rate = this->option_.learning_rate;
  Matrix jacobian;
  Matrix numgrad;
  T cost = -1.0;
  T old_cost = -1.0;
  do {
    T rate = 1.0;
    problem.EvaluateObjectiveFunction(*theta0, &old_cost);
    // Get jacobian matrix
    problem.EvaluateGradient(*theta0, &jacobian);
    assert(jacobian.cols() == 1);
    // Update parameters
    *theta0 = *theta0 - rate * jacobian;

    // @TODO: (Gabriel) Pass this as a parameter
    T beta = 0.8;
    problem.EvaluateObjectiveFunction(*theta0, &cost);
    while (cost > old_cost - rate/2.0 * jacobian.squaredNorm()) {
      rate *= beta;
      *theta0 = *theta0 - rate * jacobian;
      old_cost = cost;
      problem.EvaluateObjectiveFunction(*theta0, &cost);
    }

    // Increase cnt
    n_iter++;
    //problem.EvaluateObjectiveFunction(*theta0, &cost);
  } while (n_iter < this->option_.max_iter &&
           (std::abs(cost - old_cost) > this->option_.error_rate));
  std::cout << "#iter : " << n_iter << std::endl;
  return true;
}

template<class T>
void GradientDescentSolver<T>::ComputeNumericalGradient(const BaseProblem<T>& problem,
                                                        const Matrix& theta,
                                                        Matrix* numgrad) {
  int n = static_cast<int>(theta.rows());
  numgrad->resize(n,1);
  T eps = 1e-4;
  T uloss = 0.0;
  T lloss = 0.0;
  Matrix perturbation = Matrix::Zero(n, 1);
  for (int i = 0; i < n; ++i) {
    // Set perturbation
    perturbation(i, 0) = eps;
    // Estimate objective function at theta + perturbation
    problem.EvaluateObjectiveFunction(theta + perturbation, &uloss);
    // Estimate objective function at theta - perturbation
    problem.EvaluateObjectiveFunction(theta - perturbation, &lloss);
    // Compute numerical gradient
    (*numgrad)(i, 0) = (uloss - lloss) / (2.0 * eps);
    // Remove pertubation for this component
    perturbation(i, 0) = 0.0;
  }
}

template class LTS5_EXPORTS GradientDescentSolver<double>;
template class LTS5_EXPORTS GradientDescentSolver<float>;

}  // namespace LTS5
