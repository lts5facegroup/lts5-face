/**
 *  @file   sparse_superlu_solver.hpp
 *  @brief  Interface to call SuperLU solver to solve sparse system : Ax = B
 *          Based on OpenNL library
 *
 *  @author Christophe Ecabert
 *  @date   12/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <cstdlib>
#include <assert.h>
#include <iostream>


//#include "slu_ddefs.h"    // Double definition
//#include "slu_sdefs.h"    // Single definition

/* SuperLU Definition here to avoid including "slu_ddefs.h" and "slu_sdefs.h"
   and avoid name mangling */

/* Define my integer type int_t */
typedef int int_t; /* default */
#include "supermatrix.h"
//#include "slu_machines.h"
#include "slu_util.h"

#ifdef __cplusplus
extern "C" {
#endif
  /* Create Sparse matrix - double */
  extern void
  dCreate_CompCol_Matrix(SuperMatrix *, int, int, int, double *,
                         int *, int *, Stype_t, Dtype_t, Mtype_t);
  /* Create Sparse matrix - float */
  extern void
  sCreate_CompCol_Matrix(SuperMatrix *, int, int, int, float *,
                         int *, int *, Stype_t, Dtype_t, Mtype_t);
  /* Create Dense matrix - double */
  extern void
  dCreate_Dense_Matrix(SuperMatrix *, int, int, double *, int,
                       Stype_t, Dtype_t, Mtype_t);
  /* Create Dense matrix - float */
  extern void
  sCreate_Dense_Matrix(SuperMatrix *, int, int, float *, int,
                       Stype_t, Dtype_t, Mtype_t);
  /* Solver - double */
  /*extern void
  pdgssv(int_t nprocs, SuperMatrix *A, int_t *perm_c, int_t *perm_r,
         SuperMatrix *L, SuperMatrix *U, SuperMatrix *B, int_t *info );*/

  extern void
  dgssv(superlu_options_t * option, SuperMatrix * A, int * perm_c, int * perm_r,
        SuperMatrix * L, SuperMatrix * U, SuperMatrix * B, SuperLUStat_t * stat,
        int * info);

  /* Solver - float */
  /*extern void
  psgssv(int_t nprocs, SuperMatrix *A, int_t *perm_c, int_t *perm_r,
         SuperMatrix *L, SuperMatrix *U, SuperMatrix *B, int_t *info );*/
  extern void
  sgssv(superlu_options_t * option, SuperMatrix * A, int *perm_c, int *perm_r,
        SuperMatrix * L, SuperMatrix * U, SuperMatrix *B, SuperLUStat_t * stat,
        int *info);


  /* Expert solver - float */
  /*extern void
  psgssvx(int_t nprocs, superlumt_options_t *superlumt_options, SuperMatrix *A,
          int_t *perm_c, int_t *perm_r, equed_t *equed, float *R, float *C,
          SuperMatrix *L, SuperMatrix *U,
          SuperMatrix *B, SuperMatrix *X, float *recip_pivot_growth,
          float *rcond, float *ferr, float *berr,
          superlu_memusage_t *superlu_memusage, int_t *info);*/
  /* Parameters query */
  extern int_t sp_ienv(int_t ispec);
  /* Clean up */
  extern void Destroy_SuperMatrix_Store(SuperMatrix *);
  extern void Destroy_SuperNode_Matrix(SuperMatrix *);
  extern void Destroy_CompCol_Matrix(SuperMatrix *);
  extern void get_perm_c(int_t ispec, SuperMatrix *A, int_t *perm_c);

  extern void    StatInit(SuperLUStat_t *);
  extern void    StatFree(SuperLUStat_t *);
  extern void    set_default_options(superlu_options_t *options);

  extern int_t sPrint_CompCol_Matrix(SuperMatrix *A);
#ifdef __cplusplus
}
#endif

#include "lts5/optimization/sparse_superlu_solver.hpp"
#include "lts5/utils/math/dense_vector.hpp"
#include "lts5/utils/math/sparse_matrix.hpp"

#define SUPERLU_NEW_ARRAY(T,NB)   (T*)(calloc((NB),sizeof(T)))
#define SUPERLU_DELETE_ARRAY(x)   free(x); x = NULL

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name SuperLUSolver
 *  @fn SuperLUSolver(void)
 *  @brief  Default constructor
 */
template<typename T>
SuperLUSolver<T>::SuperLUSolver() {}

#pragma mark -
#pragma mark Process
/*
 *  @name Solve
 *  @fn bool Solve(const MATRIX& A, const VECTOR& b, const VECTOR* x)
 *  @brief  Solve sparse system Ax = b
 *  @param[in]  A System Matrix
 *  @param[in]  b System vector
 *  @param[out] x Solution vector
 *  @return 0 if success, -1 otherwise
 */
template<typename T>
int SuperLUSolver<T>::Solve(const LTS5::SparseMatrix<T>& A,
                            const LTS5::DenseVector<T>& b,
                            LTS5::DenseVector<T>* x) {
  // Define some variables
  T* b_ptr = b.data();
  T* x_ptr = x->data();

  // Compressed Row Storage matrix representation
  int n = A.dimension();
  int nnz = A.nnz();   // Number of non-zero element
  int* xa = SUPERLU_NEW_ARRAY(int, n + 1);
  T* a = SUPERLU_NEW_ARRAY(T, nnz);
  int* asub = SUPERLU_NEW_ARRAY(int, nnz);

  // Permutation vector
  int* perm_r = SUPERLU_NEW_ARRAY(int, n);
  int* perm_c = SUPERLU_NEW_ARRAY(int, n);

  // SuperLU variables
  SuperMatrix A_lu, B_lu;   // System
  SuperMatrix L_lu, U_lu;   // Inverse of A
  int_t info = 0;           // Status code
  SuperLUStat_t stat;       // Stats
  StatInit(&stat);
  superlu_options_t options;  // Options
  DNformat *vals = nullptr; // access to result
  T *rvals  = nullptr;      // access to result

  // Temporary variables
  //const typename MATRIX::Row& Ri = nullptr;
  int i,jj,count ;

  /*
   * Step 1: convert matrix A into SuperLU compressed column
   *   representation.
   * -------------------------------------------------------
   */
  count = 0;
  for (i = 0; i < n; ++i) {
    const typename SparseMatrix<T>::Row& Ri = A.row(i);
    xa[i] = count;
    for (jj = 0; jj < Ri.size(); ++jj) {
      a[count] = Ri[jj].value_;
      asub[count] = Ri[jj].index_;
      count++;
    }
  }
  xa[n] = nnz;

  /*
   * Rem: SuperLU does not support symmetric storage.
   * In fact, for symmetric matrix, what we need
   * is a SuperLLt algorithm (SuperNodal sparse Cholesky),
   * but it does not exist, anybody wants to implement it ?
   * However, this is not a big problem (SuperLU is just
   * a superset of what we really need.
   */
  if (sizeof(T) == 4) {
    // Float
    sCreate_CompCol_Matrix(&A_lu, n, n, nnz,
                           reinterpret_cast<float*>(a),
                           asub,
                           xa,
                           SLU_NR,  /* Row_wise, no supernode */
                           SLU_S,   /* floats                 */
                           SLU_GE); /* general storage        */
    /* Step 2: create vector */
    sCreate_Dense_Matrix(&B_lu, n, 1, reinterpret_cast<float*>(b_ptr),
                         n,
                         SLU_DN,
                         SLU_S,
                         SLU_GE);
  } else {
    // Double
    dCreate_CompCol_Matrix(&A_lu, n, n, nnz, reinterpret_cast<double*>(a),
                           asub,
                           xa,
                           SLU_NR,  /* Row_wise, no supernode */
                           SLU_D,   /* double                 */
                           SLU_GE); /* general storage        */
    /* Step 2: create vector */
    dCreate_Dense_Matrix(&B_lu, n, 1, reinterpret_cast<double*>(b_ptr),
                         n,
                         SLU_DN,
                         SLU_D,
                         SLU_GE);
  }
  /*
   * Get column permutation vector perm_c[], according to permc_spec:
   *   permc_spec = 0: natural ordering
   *   permc_spec = 1: minimum degree ordering on structure of A'*A
   *   permc_spec = 2: minimum degree ordering on structure of A'+A
   *   permc_spec = 3: approximate minimum degree for unsymmetric matrices
   *  See : superlu_mt/examples/pslinsol.c
   */
  get_perm_c(1, &A_lu, perm_c);


  /* Step 3: set SuperLU options
   * ------------------------------
   */
  set_default_options(&options);
  options.ColPerm = COLAMD;
  options.RowPerm = NOROWPERM;
  options.ParSymbFact = NO;
  options.ReplaceTinyPivot = NO;
  options.SolveInitialized = NO;
  options.RefineInitialized = NO;
  options.ILU_DropRule = 0;
  options.ILU_DropTol = 0;
  options.ColPerm = COLAMD;

  /* Step 4: call SuperLU main routine
   * ---------------------------------
   */
  if (sizeof(T) == 4) {
    // Float
    //psgssv(1, &A_lu, perm_c, perm_r, &L_lu, &U_lu, &B_lu, &info);
    sgssv(&options, &A_lu, perm_c, perm_r, &L_lu, &U_lu, &B_lu, &stat, &info);
  } else {
    // Double
    //pdgssv(1, &A_lu, perm_c, perm_r, &L_lu, &U_lu, &B_lu, &info);
    dgssv(&options, &A_lu, perm_c, perm_r, &L_lu, &U_lu, &B_lu, &stat, &info);
  }


  /* Step 5: get the solution
   * ------------------------
   * Fortran-type column-wise storage
   */
  vals = reinterpret_cast<DNformat*>(B_lu.Store);
  rvals = reinterpret_cast<T*>(vals->nzval);
  if (info == 0) {
    for (i = 0; i < n; ++i) {
      x_ptr[i] = rvals[i];
    }
  }

  /* Step 6: cleanup
   * ---------------
   */
  /*
   *  For these two ones, only the "store" structure
   * needs to be deallocated (the arrays have been allocated
   * by us).
   */
  Destroy_SuperMatrix_Store(&A_lu);
  Destroy_SuperMatrix_Store(&B_lu);

  /*
   *   These ones need to be fully deallocated (they have been
   * allocated by SuperLU).
   */
  Destroy_SuperNode_Matrix(&L_lu);
  Destroy_CompCol_Matrix(&U_lu);

  StatFree(&stat);

  SUPERLU_DELETE_ARRAY(xa);
  SUPERLU_DELETE_ARRAY(a);
  SUPERLU_DELETE_ARRAY(asub);
  SUPERLU_DELETE_ARRAY(perm_r);
  SUPERLU_DELETE_ARRAY(perm_c);

  return (info == 0 ? 0 : -1);
}

#pragma mark -
#pragma mark Explicit Definition

template class SuperLUSolver<float>;
template class SuperLUSolver<double>;


}  // namespace LTS5