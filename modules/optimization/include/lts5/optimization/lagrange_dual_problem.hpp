/**
 *  @file   lagrange_dual_problem.hpp
 *  @brief  Lagrange dual problem definition for optimization of type :
 *          B* = argmin 0.5 || X - B*S ||^2
 *          subject to || B(:, j) || < c
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   22/12/15
 *  @see    "Efficient sparse coding algorithms" by H.Lee et al.
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_LAGRANGE_DUAL_PROBLEM__
#define __LTS5_LAGRANGE_DUAL_PROBLEM__

#include "lts5/utils/library_export.hpp"
#include "lts5/optimization/base_optimization_problem.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  LagrangeDualProblem
 *  @brief  Lagrange dual problem definition for optimization of type :
 *          B* = argmin 0.5 || X - B*S ||^2
 *          subject to || B(:, j) || < c
 *  @author Christophe Ecabert
 *  @date   22/12/15
 *  @see    "Efficient sparse coding algorithms" by H.Lee et al.
 *  @ingroup optimization
 */
template<class T>
class LTS5_EXPORTS LagrangeDualProblem : public BaseProblem<T> {

 public :

  using Matrix = typename LTS5::BaseProblem<T>::Matrix;

  /**
   *  @name LagrangeDualProblem
   *  @brief  Constructor
   */
  LagrangeDualProblem(const Matrix& S, const Matrix& X);

  /**
   *  @name ~LagrangeDualProblem
   *  @brief  Destructor
   */
  ~LagrangeDualProblem(void);

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn void EvaluateObjectiveFunction(const Matrix& x, T* cost) const
   *  @brief  Evaluate the objective function with parameters \p theta
   *  @param[in]  x         Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost
   */
  void EvaluateObjectiveFunction(const Matrix& x, T* cost) const override;

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn void EvaluateObjectiveFunction(const Matrix& x, Matrix* cost) const
   *  @brief  Evaluate the vector objective function with parameters \p theta
   *  @param[in]  x         Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost vector
   */
  void EvaluateObjectiveFunction(const Matrix& x, Matrix* cost) const override;

  /**
   *  @name EvaluateGradient
   *  @fn void EvaluateGradient(const Matrix& x, Matrix* jacob) const
   *  @brief  Evaluate the function's gradient with parameters \p theta
   *  @param[in]  x         Position where to evaluate objective function
   *  @param[out] jacob     Jacobian matrix for parameters \p theta
   */
  void EvaluateGradient(const Matrix& x, Matrix* jacob) const override;

  /**
   *  @name EvaluateHessian
   *  @fn void EvaluateHessian(const Matrix& x, Matrix* hessian)
   *  @brief  Evaluate the function's hessian matrix with parameters \p theta
   *  @param[in]  x       Parameters used to evalute hessian matrix
   *  @param[out] hessian Hessian value for parameters \p theta
   */
  void EvaluateHessian(const Matrix& x, Matrix* hessian) const override;

  /**
   *  @name get_dimensions
   *  @fn virtual void get_dimensions(int* n_row, int* n_col) const
   *  @brief  Provide problem dimensions
   *  @param[out] n_row   Number of row in the system (Observables)
   *  @param[out] n_col   Number of column in the system (Variables)
   */
  void get_dimensions(int* n_row, int* n_col) const override {
    *n_row = static_cast<int>(X_.rows());
    *n_col = static_cast<int>(S_.rows());
  };

  /**
   *  @name get_A
   *  @fn virtual Matrix& get_A(void) const = 0
   *  @brief  Provide access to system matrix ()
   *  @return System Matrix
   */
  const Matrix& get_A(void) const override {
    return S_;
  }

  /**
   *  @name get_AtA
   *  @fn virtual Matrix& get_AtA(void) const = 0
   *  @brief  Provide access to system matrix ()
   *  @return At * A
   */
  const Matrix& get_AtA(void) const override {
    throw std::runtime_error("Not supported");
  }

  /**
   *  @name get_y
   *  @fn virtual Matrix& get_y(void) = 0
   *  @brief Provide access to y
   *  @return y
   */
  const Matrix& get_y(void) const override {
    return X_;
  }

  /**
   *  @name get_Aty
   *  @fn virtual Matrix& get_Aty(void) const = 0
   *  @brief Provide access to At * y
   *  @return At * y
   */
  const Matrix& get_Aty(void) const override {
    throw std::runtime_error("Not supported");
  }

 private :

  /** Target */
  Matrix X_;
  /** Coefficients */
  Matrix S_;
  /** Precomputed : X * S' */
  Matrix XSt_;
  /** Precomputed : S * S' */
  Matrix SSt_;
  /** Backup : inv(S * S' + diag(lamdba)) */
  mutable Matrix SSt_inv;
  // See (mutable) : https://msdn.microsoft.com/en-us/library/4h2h0ktk(v=vs.80).aspx
  /** Backup : XSt * SSt_inv */
  mutable Matrix XStSSt_inv;
  /** Precomputed : Tr(XXt) */
  mutable T trXXt_;
  /** Upper bounds */
  T c_;
};
}  // namespace LTS5
#endif /* __LTS5_LAGRANGE_DUAL_PROBLEM__ */
