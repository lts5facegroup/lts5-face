/**
 *  @file   levenberg_marquardt_solver.hpp
 *  @brief  Levenberg-Marquardt solver
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_LEVENBERG_MARQUARDT_SOLVER__
#define __LTS5_LEVENBERG_MARQUARDT_SOLVER__

#include "lts5/utils/library_export.hpp"
#include "lts5/optimization/base_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  LevenbergMarquardtSolver
 *  @brief  Solver using Levenberg-Marquardt method in the optimization process
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  @see  http://www.ltu.se/cms_fs/1.51590!/nonlinear_least_squares.pdf
 *  @ingroup optimization
 */
template<class T>
class LTS5_EXPORTS LevenbergMarquardtSolver : public LTS5::BaseSolver<T> {
 public :

  using Matrix = typename LTS5::BaseSolver<T>::Matrix;
  /**
   *  @name LevenbergMarquardtSolver
   *  @fn LevenbergMarquardtSolver(void)
   *  @brief  Constructor
   */
  LevenbergMarquardtSolver(void);

  /**
   *  @name ~LevenbergMarquardtSolver
   *  @fn ~LevenbergMarquardtSolver(void)
   *  @brief  Destructor
   */
  ~LevenbergMarquardtSolver(void);

  /**
   *  @name Minimize
   *  @fn bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override
   *  @brief  Minimize the given \p problem
   *  @param[in]      problem Problem to minimize
   *  @param[in,out]  theta0  Starting parameter's value
   *  @return True if converge, false otherwise
   */
  bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override;

};
}  // namespace LTS5
#endif /* __LTS5_LEVENBERG_MARQUARDT_SOLVER__ */
