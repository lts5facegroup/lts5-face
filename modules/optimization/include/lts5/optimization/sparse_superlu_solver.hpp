/**
 *  @file   sparse_superlu_solver.hpp
 *  @brief  Interface to call SuperLU solver to solve sparse system : Ax = B
 *          Based on OpenNL library
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   12/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_SPARSE_SUPERLU_SOLVER__
#define __LTS5_SPARSE_SUPERLU_SOLVER__

#include "lts5/utils/library_export.hpp"
#include "lts5/optimization/base_sparse_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  SuperLUSolver
 *  @brief  Interface to call SuperLU sparse solver.
 *          Based on OpenNL/CGAL library
 *  @author Christophe Ecabert
 *  @date   12/10/15
 *  @ingroup optimization
 */
template<typename T>
class LTS5_EXPORTS SuperLUSolver : public BaseSparseSolver<T> {

 public :

#pragma mark -
#pragma mark Initialization
  /**
   *  @name SuperLUSolver
   *  @fn SuperLUSolver(void)
   *  @brief  Default constructor
   */
  SuperLUSolver(void);

#pragma mark -
#pragma mark Process
  /**
   *  @name Solve
   *  @fn bool Solve(const MATRIX& A, const VECTOR& b, const VECTOR* x)
   *  @brief  Solve sparse system Ax = b
   *  @param[in]  A System Matrix
   *  @param[in]  b System vector
   *  @param[out] x Solution vector
   *  @return 0 if success, -1 otherwise
   */
  int Solve(const LTS5::SparseMatrix<T>& A,
            const LTS5::DenseVector<T>& b,
            LTS5::DenseVector<T>* x) override;
};

}  // namespace LTS5
#endif /* __LTS5_SPARSE_SUPERLU_SOLVER__ */
