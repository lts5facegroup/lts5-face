/**
 *  @file   base_sparse_solver.hpp
 *  @brief  Base class for linear solver
 *          Based on OpenNL library
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   13/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_BASE_SPARSE_SOLVER__
#define __LTS5_BASE_SPARSE_SOLVER__

#include "lts5/utils/math/dense_vector.hpp"
#include "lts5/utils/math/sparse_matrix.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BaseSparseSolver
 *  @brief  Interface for Solver system.
 *          Based on OpenNL/CGAL library
 *  @author Christophe Ecabert
 *  @date   13/10/15
 *  @ingroup optimization
 */
template<class T>
class BaseSparseSolver {

 public :
  /**
   *  @name ~BaseSparseSolver
   *  @fn virtual ~BaseSparseSolver(void)
   *  @brief  Destructor
   */
  virtual ~BaseSparseSolver(void){}

  /**
   *  @name Solve
   *  @fn bool Solve(const LTS5::SparseMatrix<T>& A,
                     const LTS5::DenseVector<T>& b,
                     LTS5::DenseVector<T>* x)
   *  @brief  Solve sparse system Ax = b
   *  @param[in]  A System Matrix
   *  @param[in]  b System vector
   *  @param[out] x Solution vector
   *  @return 0 if success, -1 otherwise
   */
  virtual int Solve(const LTS5::SparseMatrix<T>& A,
                    const LTS5::DenseVector<T>& x,
                    LTS5::DenseVector<T> * b) = 0;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name set_epsilon
   *  @fn virtual void set_epsilon(const T epsilon)
   *  @brief  Set new value for epsilon
   *  @param[in]  epsilon New value
   */
  virtual void set_epsilon(const T epsilon) { epsilon_ = epsilon; }

  /**
   *  @name get_epsilon
   *  @fn virtual T get_epsilon(void) const
   *  @brief  Give the value of epsilon
   *  @return  Value of epsilon
   */
  virtual T get_epsilon(void) const { return epsilon_;}

  /**
   *  @name set_max_iteration
   *  @fn virtual void set_max_iteration(const int max_iteration)
   *  @brief  Set number max of iteration
   *  @param[in]  max_iteration   Maximum number of iteration
   */
  virtual void set_max_iteration(const int max_iteration) {
    max_iteration_ = max_iteration;
  }

  /**
   *  @name get_max_iteration
   *  @fn virtual int set_max_iteration(void) const
   *  @brief  Give maximum number of iteration
   *  @return Maximum number of iteration
   */
  virtual int set_max_iteration(void) const {
    return max_iteration_;
  }

  /**
   *  @name set_unwrap
   *  @fn virtual void set_unwrap(const bool unwrap)
   *  @brief  Indicate if unwrapping is needed
   *  @param[in]  unwrap   True when unwrapping
   */
  virtual void set_unwrap(const bool unwrap) {
    unwrap_ = unwrap;
  }

  /**
   *  @name get_unwrap
   *  @fn virtual bool get_unwrap(void) const
   *  @brief  Indicate if unwraping is needed
   *  @return True if needed
   */
  virtual bool get_unwrap(void) const {
    return unwrap_;
  }

 protected :
  /** Epsilon */
  T epsilon_;
  /** Maximum number of iteration */
  int max_iteration_;
  /** Indicte to unwrap or not */
  bool unwrap_;
};
}  // namespace LTS5
#endif /* __LTS5_BASE_SPARSE_SOLVER__ */