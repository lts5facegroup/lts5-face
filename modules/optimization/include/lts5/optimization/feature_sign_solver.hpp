/**
 *  @file   feature_sign_solver.hpp
 *  @brief  Feature Sign search algorithm, solve :
 *          x* = argmin|| y - Ax ||^2 + gamma * |x|
 *  @ingroup optimization
 *
 *  @see    Efficient Sparse Coding Algorithms, H.Lee et al
 *  @author Christophe Ecabert
 *  @date   17/12/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_FEATURE_SIGN_SOLVER__
#define __LTS5_FEATURE_SIGN_SOLVER__

#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/optimization/base_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  FeatureSignSolver
 *  @brief  Feature Sign search algorithm, solve :
 *          x* = argmin|| y - Ax ||^2 + gamma * |x|
 *  @see    Efficient Sparse Coding Algorithms, H.Lee et al
 *  @author Christophe Ecabert
 *  @date   17/12/15
 *  @ingroup optimization
 */
template<class T>
class LTS5_EXPORTS FeatureSignSolver : public BaseSolver<T> {

 public :

  using Matrix = typename LTS5::BaseSolver<T>::Matrix;

  /**
   *  @name FeatureSignSolver
   *  @fn FeatureSignSolver(void)
   *  @brief  Constructor
   */
  FeatureSignSolver(void);

  /**
   *  @name ~FeatureSignSolver
   *  @fn ~FeatureSignSolver(void)
   *  @brief  Destructor
   */
  ~FeatureSignSolver(void);

  /**
   *  @name Minimize
   *  @fn bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override
   *  @brief  Minimize the given \p problem
   *  @param[in]      problem Problem to minimize
   *  @param[in,out]  theta0  Starting parameter's value
   *  @return True if converge, false otherwise
   */
  bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override;

 private :

  /**
   *  @name Find
   *  @fn void Find(const Matrix& vector, const T value, std::vector<int>* index)
   *  @brief  Find the indexes where a given value stays
   *  @param[in]  vector  Vector to search in
   *  @param[in]  value   Value to look for
   *  @param[out] index   Index where to find the actual value
   */
  void Find(const Matrix& vector, const T value, std::vector<int>* index);

  /**
   *  @name Sign
   *  @fn void Sign(const Matrix& x, Matrix* sign_x)
   *  @brief  Compute element-wise the sign of a vector \p x (-1, 0, 1)
   *  @param[in]  x       Vector to determine the sign
   *  @param[out] sign_x  Sign of \p x (-1, 0, 1)
   */
  void Sign(const Matrix& x, Matrix* sign_x);

  /**
   *  @name MaxAbs
   *  @fn void MaxAbs(const Matrix& x, const std::vector<int>& list_idx, T* value, int* idx)
   *  @brief  Find the maximum absolute value of a vector for a given list of element
   *  @param[in]  x         Vector to search in
   *  @param[in]  list_idx  List of element of interest
   *  @param[out] value     Maximum value
   *  @param[out] idx       Corresponding index
   */
  void MaxAbs(const Matrix& x, const std::vector<int>& list_idx, T* value, int* idx);

  /**
   *  @name SortIndexes
   *  @fn void SortIndexes(const std::vector<T>& values, std::vector<int>* sort_idx)
   *  @brief Sort vector by increasing value
   *  @param[in]  values    Vector to sort
   *  @param[out] sort_idx  Vector of sorted indexes
   */
  void SortIndexes(const std::vector<T>& values, std::vector<int>* sort_idx);


};
}  // namespace LTS5
#endif /* __LTS5_FEATURE_SIGN_SOLVER__ */
