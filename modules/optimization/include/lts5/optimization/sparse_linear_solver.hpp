/**
 *  @file   sparse_linear_solver.hpp
 *  @brief  Solve linear system or minimize a quadratic form. Can be used for
 *          UV unwrapping.
 *          Based on OpenNL/CGAL libraries
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   13/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_SPARSE_LINEAR_SOLVER__
#define __LTS5_SPARSE_LINEAR_SOLVER__

#include <assert.h>
#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/dense_vector.hpp"
#include "lts5/utils/math/sparse_matrix.hpp"
#include "lts5/optimization/base_sparse_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  SparseLinearSolver
 *  @brief  Solves a linear system or minimizes a quadratic form.
 *          Can be used for UV unwrapping.
 *          Based on OpenNL/CGAL libraries
 *  @author Christophe Ecabert
 *  @date   13/10/15
 *  @ingroup optimization
 */
template<typename T>
class LTS5_EXPORTS SparseLinearSolver {

#pragma mark -
#pragma mark Type defintion
 public :

  /**
   *  @enum State
   *  @brief  List of possible state that solver can use.
   */
  enum State {
    /** Initial */
    kInitial,
    /** Defining system */
    kInSystem,
    /** Defining Row */
    kInRow,
    /** System has been constructed */
    kConstructed,
    /** System is solved */
    kSolved
  };

  /**
   *  @class  Variables
   *  @brief  Represent varaible of the system as (index, value, locked) tuples
   *          Based on OpenNL/CGAL libraries
   *  @author Christophe Ecabert
   *  @date   13/10/15
   */
  class Variables {

   public :
    /**
     *  @name Variables
     *  @fn Variables(void)
     *  @brief  Default constructor
     */
    Variables(void) : index_(-1), x_(0), locked_(false) {}

    /**
     *  @name value
     *  @fn T value(void) const
     *  @brief  Provides variable's coefficient
     *  @return Value
     */
    T value(void) const {return x_;}

    /**
     *  @name is_locked
     *  @fn bool is_locked(void) const
     *  @brief  Indicate current locking state
     *  @return True if locked, False otherwise
     */
    bool is_locked(void) const {return locked_;}

    /**
     *  @name index
     *  @fn int index(void) const
     *  @brief  Give the variable index in the system
     *  @return Index integer
     */
    int index(void) const { assert(index_ != -1); return index_;}

    /**
     *  @name set_value
     *  @fn void set_value(const T x_in)
     *  @brief  Set variable's value
     *  @param[in] x_in Value
     */
    void set_value(const T x_in) { x_ = x_in ; }

    /**
     *  @name set_value
     *  @fn void set_index(const int index_in)
     *  @brief  Set variable's value
     *  @param[in]  index_in  Index
     */
    void set_index(const int index_in) { index_ = index_in;}

    /**
     *  @name
     *  @fn void lock(void)
     *  @brief  Lock variable
     */
    void lock(void) { locked_ = true ; }

    /**
     *  @name unlock
     *  @fn void unlock(void)
     *  @brief  Unlock variable
     */
    void unlock(void) { locked_ = false ; }

   private :

    /** Index */
    int index_;
    /** Value */
    T x_;
    /** Locking state */
    bool locked_;
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name SparseLinearSolver
   *  @fn SparseLinearSolver(void)
   *  @brief  Constructor
   */
  SparseLinearSolver(void);

  /**
   *  @name SparseLinearSolver
   *  @fn SparseLinearSolver(const int n_variables)
   *  @brief  Constructor
   *  @param[in]  n_variables Number of variables in the system
   */
  SparseLinearSolver(const int n_variables);

  /**
   *  @name Initialize
   *  @fn void Initialize(const int n_variables)
   *  @brief  Initialize system
   *  @param[in]  n_variables Number of variables in the system
   */
  void Initialize(const int n_variables);

  /**
   *  @name ~SparseLinearSolver
   *  @fn ~SparseLinearSolver(void)
   *  @brief  Destructor
   */
  ~SparseLinearSolver(void);

#pragma mark -
#pragma mark Process

  /**
   *  @name Solve
   *  @fn int Solve(void)
   *  @brief  Solve system
   *  @return 0 if success, -1 otherwise
   */
  int Solve(void);


  /**
   *  @name BeginSystem
   *  @fn void BeginSystem(void)
   *  @brief  Initialize a new system
   */
  void BeginSystem(void);

  /**
   *  @name BeginRow
   *  @fn void BeginRow(void)
   *  @brief  Initialize a new row in the system
   */
  void BeginRow(void);

  /**
   *  @name SetRightHandSide
   *  @fn void SetRightHandSide(const T value)
   *  @brief  Set the value of the right hand side of the system
   *  @param[in]  value Value to set
   */
  void SetRightHandSide(const T value);

  /**
   *  @name AddCoefficient
   *  @fn void AddCoefficient(const int variable_index, const T coeff_value)
   *  @brief  Add coefficient into the system
   *  @param[in]  variable_index  Index of the corresponding variables
   *  @param[in]  coeff_value     Value of the coefficient
   */
  void AddCoefficient(const int variable_index, const T coeff_value);

  /**
   *  @name EndRow
   *  @fn void EndRow(void)
   *  @brief  Close a new row in the system
   */
  void EndRow(void);

  /**
   *  @name EndSystem
   *  @fn void EndSystem(void)
   *  @brief  Close the system
   */
  void EndSystem(void);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name set_least_square
   *  @fn void set_least_square(const bool is_least_square)
   *  @brief  Inidicate if system is least square
   *  @param[in]  is_least_square True if LS, false otherwise
   */
  void set_least_square(const bool is_least_square) {
    least_squares_ = is_least_square;
  }

  /**
   *  @name get_n_variables
   *  @fn int get_n_variables(void) const
   *  @brief  Provide sysem size
   *  @return Size of the system
   */
  int get_n_variables(void) const { return n_variables_;}

  /**
   *  @name variable
   *  @fn Variables& variable(const int index)
   *  @brief  Provide access to system variables
   *  @param[in]  index Index of the variable to acces
   *  @return Reference to internal variable
   */
  Variables& variable(const int index) {
    assert(index < n_variables_);
    return variables_[index];
  }

  /**
   *  @name variable
   *  @fn const Variables& variable(const int index) const
   *  @brief  Provide access to system variables
   *  @param[in]  index Index of the variable to acces
   *  @return Reference to internal variable
   */
  const Variables& variable(const int index) const {
    assert(index < n_variables_);
    return variables_[index];
  }

  /**
   *  @name set_unwrap
   *  @fn void set_unwrap(const bool unwrap)
   *  @brief  Set if unwraping is needed
   *  @@param[in] unwrap True if needed
   */
  void set_unwrap(const bool unwrap) {
    solver_->set_unwrap(unwrap);
  }

  /**
   *  @name get_unwrap
   *  @fn bool get_unwrap(void) const
   *  @brief  Indicate if unwraping is needed
   *  @return True if needed
   */
  bool get_unwrap(void) const {
    return solver_->get_unwrap();
  }

#pragma mark -
#pragma mark Private

 private :
  /** Least square problem indicator */
  bool least_squares_;
  /** Number of variables in the system */
  int n_variables_;
  /** List of variables */
  typename LTS5::SparseLinearSolver<T>::Variables* variables_;
  /** System state */
  typename LTS5::SparseLinearSolver<T>::State state_;
  /** Current row index counter */
  int current_row_;
  /** System's coefficient free value */
  std::vector<T> af_;
  /** System's coefficient free indexes */
  std::vector<int> if_;
  /** System's coefficient lock values */
  std::vector<T> al_;
  /** X coefficient lock values */
  std::vector<T> xl_;
  /** */
  T bk_;

  /** System sparse Matrix */
  LTS5::SparseMatrix<T>* A_;
  /** Solution vector */
  LTS5::DenseVector<T>* x_;
  /** Response vector */
  LTS5::DenseVector<T>* b_;
  /** Solver */
  LTS5::BaseSparseSolver<T>* solver_;

  /**
   *  @name CheckState
   *  @fn void CheckState(const State s)
   *  @brief  Check current system's state
   *  @param[in]  s State to check against
   */
  void CheckState(const State s) {
    assert(state_ == s);
  }

  /**
   *  @name Transition
   *  @fn void Transition(const LTS5::SparseLinearSolver<T>::State from,
                          const LTS5::SparseLinearSolver<T>::State to)
   *  @brief  Check current system's state
   *  @param[in]  from  Source state (current)
   *  @param[in]  to    Destination state
   */
  void Transition(const State from,
                  const State to) {
    CheckState(from);
    state_ = to;
  }

  /**
   *  @name VectorToVariables
   *  @fn void VectorToVariables(void)
   *  @brief  Convert vector to internal representation
   */
  void VectorToVariables(void);

  /**
   *  @name VariablesToVector
   *  @fn void VariablesToVector(void)
   *  @brief  Convert internal representation into vector
   */
  void VariablesToVector(void);
};

}  // namespace LTS5
#endif /* __LTS5_SPARSE_LINEAR_SOLVER__ */
