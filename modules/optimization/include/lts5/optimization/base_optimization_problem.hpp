/**
 *  @file   base_optimization_problem.hpp
 *  @brief  Interface for optimization problem
 *  @ingroup optimization
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_BASE_OPTIMIZATION_PROBLEM__
#define __LTS5_BASE_OPTIMIZATION_PROBLEM__

#include <iostream>

#include "Eigen/Core"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BaseProblem
 *  @brief  Interface for optimization problem
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  @ingroup optimization
 */
template<class T>
class BaseProblem {

 public :
  using Matrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;

#pragma mark -
#pragma mark Initialization

  /**
   *  @name ~BaseProblem
   *  @fn virtual ~BaseProblem(void)
   *  @brief  Destructor
   */
  virtual ~BaseProblem(void) {}

#pragma mark -
#pragma mark Process

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn virtual void EvaluateObjectiveFunction(const Matrix& theta, T* cost) const = 0
   *  @brief  Evaluate the objective function with parameters \p theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost
   */
  virtual void EvaluateObjectiveFunction(const Matrix& theta, T* cost) const = 0;

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn virtual void EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost) const = 0
   *  @brief  Evaluate the objective function with parameters \p theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost vector
   */
  virtual void EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost) const = 0;

  /**
   *  @name EvaluateGradient
   *  @fn virtual void EvaluateGradient(const Matrix& theta, Matrix* jacob) = 0
   *  @brief  Evaluate the function's gradient with parameters \p theta
   *  @param[in]  theta     Position where to evaluate objective function
   *  @param[out] jacob     Jacobian matrix for parameters theta
   */
  virtual void EvaluateGradient(const Matrix& theta, Matrix* jacob) const = 0;

  /**
   *  @name EvaluateHessian
   *  @fn virtual void EvaluateHessian(const Matrix& x, Matrix* hessian) = 0
   *  @brief  Evaluate the function's hessian matrix with parameters \p theta
   *  @param[in]  theta   Parameters used to evalute hessian matrix
   *  @param[out] hessian Hessian value for parameters theta
   */
  virtual void EvaluateHessian(const Matrix& theta, Matrix* hessian) const = 0;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name get_dimensions
   *  @fn virtual void get_dimensions(int* n_row, int* n_col) const = 0
   *  @brief  Provide problem dimensions
   *  @param[out] n_row   Number of row in the system (Observables)
   *  @param[out] n_col   Number of column in the system (Variables)
   */
  virtual void get_dimensions(int* n_row, int* n_col) const = 0;

  /**
   *  @name get_A
   *  @fn virtual Matrix& get_A(void) = 0
   *  @brief  Provide access to system matrix ()
   *  @return System Matrix
   */
  virtual const Matrix& get_A(void) const = 0;

  /**
   *  @name get_AtA
   *  @fn virtual Matrix& get_AtA(void) = 0
   *  @brief  Provide access to system matrix ()
   *  @return At * A
   */
  virtual const Matrix& get_AtA(void) const = 0;

  /**
   *  @name get_y
   *  @fn virtual Matrix& get_y(void) = 0
   *  @brief Provide access to y
   *  @return y
   */
  virtual const Matrix& get_y(void) const = 0;

  /**
   *  @name get_Aty
   *  @fn virtual Matrix& get_Aty(void) = 0
   *  @brief Provide access to At * y
   *  @return At * y
   */
  virtual const Matrix& get_Aty(void) const = 0;


#pragma mark -
#pragma mark Utility

  /**
   *  @name CheckGradient
   *  @fn virtual bool CheckGradient(const Matrix& theta)
   *  @param[in]  theta Parameters to use to check gradient
   *  @return True if gradient is correct, false otherwise
   */
  virtual bool CheckGradient(const Matrix& theta) {
    std::cout << "Not implemented yet !!!" << std::endl;
    return false;
  }

};
}  // namespace LTS5
#endif /* __LTS5_BASE_OPTIMIZATION_PROBLEM__ */
