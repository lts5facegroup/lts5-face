/**
 *  @file   lts5/optimization/optimization.hpp
 *  @brief  Optimization module
 *
 *  @author Christophe Ecabert
 *  @date   22/12/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_OPTIMIZATION__
#define __LTS5_OPTIMIZATION__

/** SuperLU Solver interface */
#include "lts5/optimization/sparse_superlu_solver.hpp"
/** Base optimization problem */
#include "lts5/optimization/base_optimization_problem.hpp"
/** Gradient-descent solver */
#include "lts5/optimization/gradient_descent_solver.hpp"
/** Gauss-Newton solver */
#include "lts5/optimization/gauss_newton_solver.hpp"
/** Levenberg-Marquardt solver */
#include "lts5/optimization/levenberg_marquardt_solver.hpp"

#endif /* __LTS5_OPTIMIZATION__ */
