/**
 *  @file   base_solver.hpp
 *  @brief  Interface for optimization solver
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_BASE_SOLVER__
#define __LTS5_BASE_SOLVER__

#include "lts5/utils/library_export.hpp"
#include "lts5/optimization/base_optimization_problem.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BaseSolver
 *  @brief  Interface for solver definition
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  @ingroup optimization
 */
template<class T>
class BaseSolver {
 public :

#pragma mark -
#pragma mark Type defintion
  /** Matrix type */
  using Matrix = typename BaseProblem<T>::Matrix;

  /**
   *  @struct SolverOption
   *  @brief  Define parameters for solver
   */
  struct LTS5_EXPORTS SolverOption {
    /** Gradient tolerence, used when checking gradient estimation */
    T grad_tol;
    /** Maximum iteration */
    int max_iter;
    /** Error rate */
    T error_rate;
    /** Learning rate */
    T learning_rate;
    /** Lev-Marquardt damping parameter */
    T damping_mu;
    /** Feature-Sign Search algo, regularization parameters */
    T gamma;

    /**
     *  @name SolverOption
     *  @fn SolverOption(void)
     *  @brief  Constructor
     */
    SolverOption(void) : grad_tol(1e-4),
                         max_iter(100),
                         error_rate(1e-6),
                         learning_rate(1e-2),
                         damping_mu(1e-2),
                         gamma(0.4) {
    }
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name ~BaseSolver
   *  @fn virtual ~BaseSolver(void)
   *  @brief  Destructor
   */
  virtual ~BaseSolver(void) {}

#pragma mark -
#pragma mark Process


  /**
   *  @name Minimize
   *  @fn virtual bool Minimize(const BaseProblem<T>& problem,
                                Matrix* theta0) = 0
   *  @brief  Minimize the given \p problem
   *  @param[in]      problem Problem to minimize
   *  @param[in,out]  theta0  Starting parameter's value
   *  @return True if converge, false otherwise
   */
  virtual bool Minimize(const BaseProblem<T>& problem,
                        Matrix* theta0) = 0;

  /**
   *  @name set_option
   *  @fn virtual void set_option(const SolverOption& option)
   *  @brief  Set new solver's option
   *  @param[in]  option  Set of new options
   */
  virtual void set_option(const SolverOption& option) {
    option_ = option;
  }

#pragma mark -
#pragma mark Member

  /** Solver option */
  SolverOption option_;
};
}  // namespace LTS5
#endif /* __LTS5_BASE_SOLVER__ */
