/**
 *  @file   robust_matrix_completion.hpp
 *  @brief  Matrix completion algorithm based on :
 *          Robust Principal Component Analysis with Missing Data
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   28/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __ROBUST_MATRIX_COMPLETION__
#define __ROBUST_MATRIX_COMPLETION__

#include "Eigen/Dense"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Parameters

/**
 * @class  MatrixCompletionParams
 * @brief  Configuration parameters
 * @author Christophe Ecabert
 * @date   28/07/2017
 * @ingroup optimization
 */
template<typename T>
class MatrixCompletionParams {
 public:

  /** Matrix Type */
  using Matrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;

  /**
   * @name    MatrixCompletionParams
   * @fn      MatrixCompletionParams(void)
   * @brief   Constructor
   */
  MatrixCompletionParams(void);

  /**
   * @name    MatrixCompletionParams
   * @fn      explicit MatrixCompletionParams(const Matrix& data)
   * @brief   Constructor, initialize parameters for a given dataset
   * @param[in] data    Data to reconstruct
   */
  explicit MatrixCompletionParams(const Matrix& data);

  /**
   * @name  Init
   * @fn    void Init(const Matrix& data)
   * @brief Initialize parameters for a given dataset
   * @param[in] data    Data to reconstruct
   */
  void Init(const Matrix& data);

  /**
   * @name    Init
   * @fn  void InitLambda(const Matrix& data)
   * @brief Initialize lambda parameters for a given dataset
   * @param[in] data Dataset to complete
   */
  void InitLambda(const Matrix& data);

  /**
   * @name    Init
   * @fn  void InitRank(const Matrix& data)
   * @brief Initialize rank parameters for a given dataset
   * @param[in] data Dataset to complete
   */
  void InitRank(const Matrix& data);

  /** Mu initial */
  T mu;
  /** Mu max */
  T mu_max;
  /** Rho - rate [1.0. 1.1] */
  T rho;
  /** Lambda - regularizer : 1.0 / sqrt(max(m, n) */
  T lambda;
  /** Convergence tolerance : 1e-4 */
  T tol;
  /** Rank */
  size_t rank;
  /** Maximum iteration */
  size_t max_iter;
};


#pragma mark -
#pragma mark Reconstruction - Non Convex

/**
 * @class   MatrixCompletionMF
 * @brief   Non convex robust matrix completion based on :
 *          Robust Principal Component Analysis with Missing Data
 *          Missing data are signaled by -1.0 value
 * @author  Christophe Ecabert
 * @date    28/07/2017
 * @ingroup optimization
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS MatrixCompletionMF {
 public:

#pragma mark -
#pragma mark Type Definitions

  /** Matrix type */
  using Matrix = typename Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;



#pragma mark -
#pragma mark Initialization

  /**
   * @name  MatrixCompletionMF
   * @fn    MatrixCompletionMF(void) = default
   * @brief Constructor
   */
  MatrixCompletionMF(void);

  /**
   * @name  MatrixCompletionMF
   * @fn    MatrixCompletionMF(const MatrixCompletionMF& other) = delete
   * @brief Copy Constructor
   * @param[in] other   Object to copy from
   */
  MatrixCompletionMF(const MatrixCompletionMF& other) = delete;

  /**
   * @name  MatrixCompletionMF
   * @fn    MatrixCompletionMF(MatrixCompletionMF&& other) = delete
   * @brief Move constructor
   * @param[in] other Object to move
   */
  MatrixCompletionMF(MatrixCompletionMF&& other) = delete;

  /**
   * @name operator=
   * @fn    MatrixCompletionMF& operator=(const MatrixCompletionMF<T>& rhs)=delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  MatrixCompletionMF& operator=(const MatrixCompletionMF<T>& rhs) = delete;

  /**
   * @name operator=
   * @fn    MatrixCompletionMF& operator=(MatrixCompletionMF<T>&& rhs) = delete
   * @brief Move assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  MatrixCompletionMF& operator=(MatrixCompletionMF<T>&& rhs) = delete;

  /**
   * @name  MatrixCompletionMF
   * @fn    ~MatrixCompletionMF(void)
   * @brief Destructor
   */
  ~MatrixCompletionMF(void) = default;

  /**
   * @name  Init
   * @fn    void Init(const Matrix& obs)
   * @brief Initialize internal state for a given observation. Override previous
   *        initialization
   * @param[in] obs Observed data
   */
  void Init(const Matrix& obs);

#pragma mark -
#pragma mark Usage

  /**
   * @name Solve
   * @fn int Solve(const Matrix& obs, Matrix* rec, Matrix* outliner)
   * @brief     Iteratively reconstruct observation
   * @param[in] obs         Data observed
   * @param[out] rec        Completed data
   * @param[out] outliner   Outliners
   * @return 0 if converged, -1 otherwise
   */
  int Solve(const Matrix& obs, Matrix* rec, Matrix* outliner);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_parameters
   * @fn    MatrixCompletionParams<T>& get_parameters(void)
   * @brief Provide access to parameters, can modify them
   * @return    Parameters
   */
  MatrixCompletionParams<T>& get_parameters(void) {
    return p_;
  }

  /**
   * @name  get_parameters
   * @fn    const MatrixCompletionParams<T>& get_parameters(void) const
   * @brief Provide access to parameters
   * @return    Parameters
   */
  const MatrixCompletionParams<T>& get_parameters(void) const {
    return p_;
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  UpdateOmega
   * @fn    void UpdateOmega(const Matrix& obs)
   * @brief Initialize mask indicating where observed data are (binary mask)
   * @param[in] obs Observed data with missing element marked as 0
   */
  void UpdateOmega(const Matrix& obs);

  /**
   * @name  UpdateG
   * @fn    void UpdateG(void)
   * @brief Update G
   */
  void UpdateG(void);

  /**
   * @name  UpdateH
   * @fn    void UpdateH(void)
   * @brief Update H with spectral soft-thresholding
   * @param[in] thresh Spectral threshold
   */
  void UpdateH(const T thresh);

  /**
   * @name  UpdateE
   * @fn    void UpdateE(const Matrix& Z, const T mu, const T thresh)
   * @brief Update E with soft-thresholding
   * @param[in] Z       Observed data
   * @param[in] mu      Regularizer (dynamic)
   * @param[in] thresh Spectral threshold
   */
  void UpdateE(const Matrix& Z, const T mu, const T thresh);

  /** SVD Type */
  using SVD = typename LTS5::LinearAlgebra<T>::DCSvdDecomposition;
  /** Linear algebra */
  using LA = typename LTS5::LinearAlgebra<T>;
  /** Transpose type */
  using TType = typename LTS5::LinearAlgebra<T>::TransposeType;

  /** Observed data mask */
  Matrix omega_;
  /** P */
  Matrix P_;
  /** G */
  Matrix G_;
  /** H */
  Matrix H_;
  /** E */
  Matrix E_;
  /** Y */
  Matrix Y_;
  /** P_ * H_ */
  Matrix PH_;
  /** P_' * G */
  Matrix PtG_;
  /** Diff */
  Matrix Diff_;
  /** SVD - PH */
  SVD svd_ph_;
  /** SVD - Spectral thresh */
  SVD svd_spectral_;
  /** Parameters */
  MatrixCompletionParams<T> p_;
  /** Number of rows */
  int m_;
  /** Number of columns */
  int n_;
  /** Initialized */
  bool initialized_;
};

#pragma mark -
#pragma mark Reconstruction - Convex

/**
 * @class   MatrixCompletion
 * @brief   Convex robust matrix completion based on :
 *          Robust Principal Component Analysis with Missing Data
 *          Missing data are signaled by -1.0 value
 * @author  Christophe Ecabert
 * @date    28/07/2017
 * @ingroup optimization
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS MatrixCompletion {
 public:

#pragma mark -
#pragma mark Type Definitions

  /** Matrix type */
  using Matrix = typename Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;



#pragma mark -
#pragma mark Initialization

  /**
   * @name  MatrixCompletion
   * @fn    MatrixCompletion(void) = default
   * @brief Constructor
   */
  MatrixCompletion(void);

  /**
   * @name  MatrixCompletion
   * @fn    MatrixCompletion(const MatrixCompletion& other) = delete
   * @brief Copy Constructor
   * @param[in] other   Object to copy from
   */
  MatrixCompletion(const MatrixCompletion& other) = delete;

  /**
   * @name  MatrixCompletion
   * @fn    MatrixCompletion(MatrixCompletion&& other) = delete
   * @brief Move constructor
   * @param[in] other Object to move
   */
  MatrixCompletion(MatrixCompletion&& other) = delete;

  /**
   * @name operator=
   * @fn    MatrixCompletion& operator=(const MatrixCompletion<T>& rhs)=delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  MatrixCompletion& operator=(const MatrixCompletion<T>& rhs) = delete;

  /**
   * @name operator=
   * @fn    MatrixCompletion& operator=(MatrixCompletion<T>&& rhs) = delete
   * @brief Move assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  MatrixCompletion& operator=(MatrixCompletion<T>&& rhs) = delete;

  /**
   * @name  MatrixCompletion
   * @fn    ~MatrixCompletion(void)
   * @brief Destructor
   */
  ~MatrixCompletion(void) = default;

  /**
   * @name  Init
   * @fn    void Init(const Matrix& obs)
   * @brief Initialize internal state for a given observation. Override previous
   *        initialization
   * @param[in] obs Observed data
   */
  void Init(const Matrix& obs);

#pragma mark -
#pragma mark Usage

  /**
   * @name Solve
   * @fn int Solve(const Matrix& obs, Matrix* rec, Matrix* outliner)
   * @brief     Iteratively reconstruct observation
   * @param[in] obs         Data observed
   * @param[out] rec        Completed data
   * @param[out] outliner   Outliners
   * @return 0 if converged, -1 otherwise
   */
  int Solve(const Matrix& obs, Matrix* rec, Matrix* outliner);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_parameters
   * @fn    MatrixCompletionParams<T>& get_parameters(void)
   * @brief Provide access to parameters, can modify them
   * @return    Parameters
   */
  MatrixCompletionParams<T>& get_parameters(void) {
    return p_;
  }

  /**
   * @name  get_parameters
   * @fn    const MatrixCompletionParams<T>& get_parameters(void) const
   * @brief Provide access to parameters
   * @return    Parameters
   */
  const MatrixCompletionParams<T>& get_parameters(void) const {
    return p_;
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  UpdateOmega
   * @fn    void UpdateOmega(const Matrix& obs)
   * @brief Initialize mask indicating where observed data are (binary mask)
   * @param[in] obs Observed data with missing element marked as 0
   */
  void UpdateOmega(const Matrix& obs);

  /**
   * @name  UpdateX
   * @fn    void UpdateX(const T thresh)
   * @brief Update X
   * @param[in] thresh Spectral treshold
   */
  void UpdateX(const T thresh);

  /**
   * @name  UpdateE
   * @fn    void UpdateE(const Matrix& Z, const T mu, const T thresh)
   * @brief Update E with soft-thresholding
   * @param[in] Z       Observed data
   * @param[in] mu      Regularizer (dynamic)
   * @param[in] thresh Spectral threshold
   */
  void UpdateE(const Matrix& Z, const T mu, const T thresh);

  /** SVD Type */
  using SVD = typename LTS5::LinearAlgebra<T>::DCSvdDecomposition;
  /** Linear algebra */
  using LA = typename LTS5::LinearAlgebra<T>;
  /** Transpose type */
  using TType = typename LTS5::LinearAlgebra<T>::TransposeType;

  /** Observed data mask */
  Matrix omega_;
  /** X */
  Matrix X_;
  /** E */
  Matrix E_;
  /** Y */
  Matrix Y_;
  /** T */
  Matrix T_;
  /** Diff */
  Matrix Diff_;
  /** SVD - Spectral thresh */
  SVD svd_spectral_;
  /** Parameters */
  MatrixCompletionParams<T> p_;
  /** Number of rows */
  int m_;
  /** Number of columns */
  int n_;
  /** Initialized */
  bool initialized_;
};

}  // namepsace LTS5
#endif //__ROBUST_MATRIX_COMPLETION__
