/**
 *  @file   gauss_newton_solver.hpp
 *  @brief  Gauss-Newton solver
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_GAUSS_NEWTON_SOLVER__
#define __LTS5_GAUSS_NEWTON_SOLVER__

#include "lts5/utils/library_export.hpp"
#include "lts5/optimization/base_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  GaussNewtonSolver
 *  @brief  Solver using Gauss-Newton method in the optimization process
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  @see  http://www.ltu.se/cms_fs/1.51590!/nonlinear_least_squares.pdf
 *  @ingroup optimization
 */
template<class T>
class LTS5_EXPORTS GaussNewtonSolver : public LTS5::BaseSolver<T> {
 public :

  using Matrix = typename LTS5::BaseSolver<T>::Matrix;
  /**
   *  @name GaussNewtonSolver
   *  @fn GaussNewtonSolver(void)
   *  @brief  Constructor
   */
  GaussNewtonSolver(void);

  /**
   *  @name ~GaussNewtonSolver
   *  @fn ~GaussNewtonSolver(void)
   *  @brief  Destructor
   */
  ~GaussNewtonSolver(void);

  /**
   *  @name Minimize
   *  @fn bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override
   *  @brief  Minimize the given \p problem
   *  @param[in]      problem Problem to minimize
   *  @param[in,out]  theta0  Starting parameter's value
   *  @return True if converge, false otherwise
   */
  bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override;
};
}  // namespace LTS5
#endif /* __LTS5_GAUSS_NEWTON_SOLVER__ */
