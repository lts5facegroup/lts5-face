/**
 *  @file   lagrange_dual_solver.hpp
 *  @brief  Lagrange dual solver definition for optimization of type :
 *          B* = argmin 0.5 || X - B*S ||^2
 *          subject to || B(:, j) || < c
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   22/12/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_LAGRANGE_DUAL_SOLVER__
#define __LTS5_LAGRANGE_DUAL_SOLVER__

#include "lts5/utils/library_export.hpp"
#include "lts5/optimization/base_solver.hpp"
#include "lts5/optimization/newton_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @name   LagrangeDualSolver
 *  @brief  Lagrange dual solver definition for optimization of type :
 *          B* = argmin 0.5 || X - B*S ||^2
 *          subject to || B(:, j) || < c
 *  @author Christophe Ecabert
 *  @date   22/12/15
 *  @ingroup optimization
 */
template<class T>
class LTS5_EXPORTS LagrangeDualSolver : public LTS5::BaseSolver<T> {

 public :
  using Matrix = typename LTS5::BaseSolver<T>::Matrix;

  /**
   *  @name LagrangeDualSolver
   *  @fn LagrangeDualSolver(void)
   *  @brief  Constructor
   */
  LagrangeDualSolver(void);

  /**
   *  @name ~LagrangeDualSolver
   *  @fn ~LagrangeDualSolver(void)
   *  @brief  Destructor
   */
  ~LagrangeDualSolver(void);

  /**
   *  @name Minimize
   *  @fn bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override
   *  @brief  Minimize the given \p problem
   *  @param[in]      problem Problem to minimize
   *  @param[in,out]  theta0  Starting parameter's value
   *  @return True if converge, false otherwise
   */
  bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override;

  /**
   *  @name set_internal_learning_rate
   *  @fn oid set_internal_learning_rate(const T rate)
   *  @brief      Setup internal solver learning rate
   *  @param[in]  rate new learning rate
   */
  void set_internal_learning_rate(const T rate) {
    internal_option_.learning_rate = rate;
    solver_->set_option(internal_option_);
  }

 private :

  /** Newton solver */
  LTS5::NewtonSolver<T>* solver_;
  /** Internal solver option */
  typename LTS5::BaseSolver<T>::SolverOption internal_option_;

};
}  // namespace LTS5
#endif /* __LTS5_LAGRANGE_DUAL_SOLVER__ */
