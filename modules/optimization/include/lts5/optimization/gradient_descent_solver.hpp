/**
 *  @file   gradient_descent_solver.hpp
 *  @brief  Gradient-Descent solver
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_GRADIENT_DESCENT_SOLVER__
#define __LTS5_GRADIENT_DESCENT_SOLVER__

#include "lts5/utils/library_export.hpp"
#include "lts5/optimization/base_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  GradientDescentSolver
 *  @brief  Solver using Gradient-Desent method in the optimization process
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  @ingroup optimization
 */
template<class T>
class LTS5_EXPORTS GradientDescentSolver : public LTS5::BaseSolver<T> {
 public :

  using Matrix = typename LTS5::BaseSolver<T>::Matrix;

  /**
   *  @name GradientDescentSolver
   *  @fn GradientDescentSolver(void)
   *  @brief  Constructor
   */
  GradientDescentSolver(void);

  /**
   *  @name ~GradientDescentSolver
   *  @fn ~GradientDescentSolver(void)
   *  @brief  Destructor
   */
  ~GradientDescentSolver(void);

  /**
   *  @name Minimize
   *  @fn bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override
   *  @brief  Minimize the given \p problem
   *  @param[in]      problem Problem to minimize
   *  @param[in,out]  theta0  Starting parameter's value
   *  @return True if converge, false otherwise
   */
  bool Minimize(const BaseProblem<T>& problem, Matrix* theta0) override;

 private :

  void ComputeNumericalGradient(const BaseProblem<T>& problem,
                                const Matrix& theta,
                                Matrix* numgrad);
};
}  // namespace LTS5
#endif /* __LTS5_GRADIENT_DESCENT_SOLVER__ */
