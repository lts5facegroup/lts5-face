/**
 *  @file   sparse_conjugate_gradient_solver.hpp
 *  @brief  The Conjugate Gradient algorithm WITHOUT preconditioner:
 *            Ashby, Manteuffel, Saylor
 *              A taxononmy for conjugate gradient methods
 *              SIAM J Numer Anal 27, 1542-1568 (1990)
 *          Based on OpenNL library
 *  @ingroup optimization
 *
 *  @author Christophe Ecabert
 *  @date   13/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */



#ifndef __LTS5_SPARSE_CONJUGATE_GRADIENT_SOLVER__
#define __LTS5_SPARSE_CONJUGATE_GRADIENT_SOLVER__

#include "lts5/utils/library_export.hpp"
#include "lts5/optimization/base_sparse_solver.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   ConjugateGradientSolver
 * @brief   Sparse Conjugate Gradient solver WITHOUT preconditioner:
 * @author  Christophe Ecabert
 * @date    13/10/15
 * @ingroup optimization
 */
template<typename T>
class LTS5_EXPORTS ConjugateGradientSolver : public LTS5::BaseSparseSolver<T>  {

#pragma mark -
#pragma mark Initialization

 public :
  /**
   *  @name ConjugateGradientSolver
   *  @fn ConjugateGradientSolver(void)
   *  @brief  Constructor
   */
  ConjugateGradientSolver(void);

#pragma mark -
#pragma mark Process

  /**
   *  @name Solve
   *  @fn bool Solve(const LTS5::SparseMatrix<T>& A,
   *                 const LTS5::DenseVector<T>& b,
   *                 LTS5::DenseVector<T>* x)
   *  @brief  Solve sparse system Ax = b
   *  @param[in]  A System Matrix
   *  @param[in]  b System vector
   *  @param[out] x Solution vector
   *  @return 0 if success, -1 otherwise
   */
  int Solve(const LTS5::SparseMatrix<T>& A,
            const LTS5::DenseVector<T>& b,
            LTS5::DenseVector<T>* x) override;
};

}  // namespace LTS5
#endif /* __LTS5_SPARSE_CONJUGATE_GRADIENT_SOLVER__ */
