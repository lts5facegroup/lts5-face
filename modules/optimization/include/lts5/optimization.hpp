/**
 *  @file   lts5/optimization.hpp
 *  @brief  Optimization module
 *
 *  @author Gabriel Cuendet
 *  @date   11/05/16
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include "lts5/optimization/optimization.hpp"
