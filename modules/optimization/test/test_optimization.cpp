//
//  test_optimization.cpp
//  LTS5-Lib
//
//  Created by Christophe Ecabert on 20/11/15.
//  Copyright © 2015 Ecabert Christophe. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <random>
#include <chrono>
#include <stdexcept>

#include "Eigen/Core"

#include "lts5/optimization/gauss_newton_solver.hpp"
#include "lts5/optimization/newton_solver.hpp"
#include "lts5/optimization/gradient_descent_solver.hpp"
#include "lts5/optimization/levenberg_marquardt_solver.hpp"

/**
 *  @class  LogisticRegression
 *  @brief  Test case for optimization module debugging
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  @see    https://en.wikipedia.org/wiki/Logistic_regression
 */
template<class T>
class LogisticRegression : public LTS5::BaseProblem<T> {
 public :

  using Matrix = typename LTS5::BaseProblem<T>::Matrix;

  /**
   *  @name LogisticRegression
   *  @brief  Constructor
   */
  LogisticRegression(const Matrix& X, const Matrix& y) :
  X_(X), y_(y), is_vector_cost_(false) {}

  /**
   *  @name ~LogisticRegression
   *  @brief  Destructor
   */
  ~LogisticRegression(void) {}

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn EvaluateObjectiveFunction(const Matrix& theta, double* cost) = 0
   *  @brief  Evaluate the objective function with parameters #theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost
   *  @return Cost for parameters #theta
   */
  void EvaluateObjectiveFunction(const Matrix& theta, T* cost) const {
    // Access element
    Matrix val = Matrix(X_.rows(), 1);
    cblas_dgemv(CBLAS_ORDER::CblasColMajor,
                CBLAS_TRANSPOSE::CblasNoTrans,
                (int)X_.rows(),
                (int)X_.cols(),
                1.0,
                static_cast<const lapack_dbl*>(X_.data()),
                (int)X_.rows(),
                static_cast<const lapack_dbl*>(theta.data()),
                1,
                0.0,
                static_cast<lapack_dbl*>(val.data()),
                1);
    double sum = 0.0;
    double value = 0.0;
    for (int i = 0; i < val.rows(); ++i) {
      value = (1.0 / (1.0 + std::exp(-val(i, 0)))) - y_(i, 0);
      sum += value * value;
    }
    *cost = sum / (2.0 * X_.rows());
  }

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost)
   *  @brief  Evaluate the vector objective function with parameters #theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost vector
   *  @return Cost for parameters #theta
   */
  void EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost) const {
    // Setup cost vector
    cost->resize(X_.rows(), 1);
    // Compute cost
    Matrix val = Matrix(X_.rows(), 1);
    cblas_dgemv(CBLAS_ORDER::CblasColMajor,
                CBLAS_TRANSPOSE::CblasNoTrans,
                (int)X_.rows(),
                (int)X_.cols(),
                1.0,
                static_cast<const lapack_dbl*>(X_.data()),
                (int)X_.rows(),
                static_cast<const lapack_dbl*>(theta.data()),
                1,
                0.0,
                static_cast<lapack_dbl*>(val.data()),
                1);
    for (int i = 0; i < val.rows(); ++i) {
      (*cost)(i, 0) = (1.0 / (1.0 + std::exp(-val(i, 0)))) - y_(i, 0);
    }
  };

  /**
   *  @name EvaluateGradient
   *  @fn void EvaluateGradient(const Matrix& theta, Matrix* jacob)
   *  @brief  Evaluate the function's gradient with parameters #theta
   *  @param[in]  theta     Position where to evaluate objective function
   *  @param[out] jacob     Jacobian matrix for parameters #theta
   */
  void EvaluateGradient(const Matrix& theta, Matrix* jacob) const {
    if (!is_vector_cost_) {
      // Allocate memory if not already done
      jacob->resize(theta.rows(), 1);
      jacob->setZero();
      // Compute jacobian matrix
      const Matrix p = 1.0 / (1.0 + exp(-(X_ * theta).array()));
      for (int i = 0; i < X_.rows(); ++i) {
        *jacob += (p(i, 0) - y_(i, 0)) * (p(i, 0) * (1.0 - p(i, 0))) * X_.row(i).transpose();
      }
      *jacob /= X_.rows();
    } else {
      // Allocate memory if not already done
      jacob->resize(X_.rows(), theta.rows());
      // Compute Jacobian
      const Matrix p = 1.0 / (1.0 + exp(-(X_ * theta).array()));
      const Matrix h_theta = p.array() * (1.0 - p.array());
      for (int c = 0; c < X_.cols(); ++c) {
        jacob->col(c) = h_theta.array() * X_.col(c).array();
      }
    }
  }

  /**
   *  @name EvaluateHessian
   *  @fn void EvaluateHessian(const Matrix& x, Matrix* hessian)
   *  @brief  Evaluate the function's hessian matrix with parameters #theta
   *  @param[in]  theta   Parameters used to evalute hessian matrix
   *  @param[out] hessian Hessian value for parameters #theta
   */
  void EvaluateHessian(const Matrix& theta, Matrix* hessian) const {}

  /**
   *  @name get_dimensions
   *  @fn virtual void get_dimensions(int* n_row, int* n_col) const = 0
   *  @brief  Provide problem dimensions
   *  @param[out] n_row   Number of row in the system (Observables)
   *  @param[out] n_col   Number of column in the system (Variables)
   */
  void get_dimensions(int* n_row, int* n_col) const {
    *n_row = X_.rows();
    *n_col = X_.cols();
  }

  /**
   *  @name get_A
   *  @fn virtual Matrix& get_A(void) = 0
   *  @brief  Provide access to system matrix ()
   *  @return System Matrix
   */
  const Matrix& get_A(void) const {
    return X_;
  }

  /**
   *  @name get_AtA
   *  @fn virtual Matrix& get_AtA(void) = 0
   *  @brief  Provide access to system matrix ()
   *  @return At * A
   */
  const Matrix& get_AtA(void) const {
    return X_.transpose() * X_;
  };

  /**
   *  @name get_y
   *  @fn virtual Matrix& get_y(void) = 0
   *  @brief Provide access to y
   *  @return y
   */
  const Matrix& get_y(void) const {
    return y_;
  }

  /**
   *  @name get_Aty
   *  @fn virtual Matrix& get_Aty(void) = 0
   *  @brief Provide access to At * y
   *  @return vector At * y
   */
  const Matrix& get_Aty(void) const {
    return X_.transpose() * y_;
  };


  /** Indicate if vector cost function */
  bool is_vector_cost_;

 private :

  /** Data */
  Matrix X_;
  /** Target */
  Matrix y_;
};

/**
 *  @class  ULogisticRegression
 *  @brief  Test case for optimization module debugging
 *          Evaluate Grad/ObjFcn for underdetermined system
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  @see    https://en.wikipedia.org/wiki/Logistic_regression
 */
template<class T>
class ULogisticRegression : public LTS5::BaseProblem<T> {
  public :

  using Matrix = typename LTS5::BaseProblem<T>::Matrix;

  /**
   *  @name LogisticRegression
   *  @brief  Constructor
   */
  ULogisticRegression(const Matrix& X, const Matrix& y) :
  is_vector_cost_(false) {
    XtX_ = X.transpose() * X;
    Xty_ = X.transpose() * y;
  }

  /**
   *  @name ~LogisticRegression
   *  @brief  Destructor
   */
  ~ULogisticRegression(void) {}

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn EvaluateObjectiveFunction(const Matrix& theta, double* cost) = 0
   *  @brief  Evaluate the objective function with parameters #theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost
   *  @return Cost for parameters #theta
   */
  void EvaluateObjectiveFunction(const Matrix& theta, T* cost) const {
    // Access element
    Matrix val = Matrix(XtX_.rows(), 1);
    cblas_dgemv(CBLAS_ORDER::CblasColMajor,
                CBLAS_TRANSPOSE::CblasNoTrans,
                (int)XtX_.rows(),
                (int)XtX_.cols(),
                1.0,
                static_cast<const lapack_dbl*>(XtX_.data()),
                (int)XtX_.rows(),
                static_cast<const lapack_dbl*>(theta.data()),
                1,
                0.0,
                static_cast<lapack_dbl*>(val.data()),
                1);
    double sum = 0.0;
    double value = 0.0;
    for (int i = 0; i < val.rows(); ++i) {
      value = (1.0 / (1.0 + std::exp(-val(i, 0)))) - Xty_(i, 0);
      sum += value * value;
    }
    *cost = sum / (2.0 * XtX_.rows());
  }

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost)
   *  @brief  Evaluate the vector objective function with parameters #theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost vector
   *  @return Cost for parameters #theta
   */
  void EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost) const {
    // Setup cost vector
    cost->resize(XtX_.rows(), 1);
    // Compute cost
    Matrix val = Matrix(XtX_.rows(), 1);
    cblas_dgemv(CBLAS_ORDER::CblasColMajor,
                CBLAS_TRANSPOSE::CblasNoTrans,
                (int)XtX_.rows(),
                (int)XtX_.cols(),
                1.0,
                static_cast<const lapack_dbl*>(XtX_.data()),
                (int)XtX_.rows(),
                static_cast<const lapack_dbl*>(theta.data()),
                1,
                0.0,
                static_cast<lapack_dbl*>(val.data()),
                1);
    for (int i = 0; i < val.rows(); ++i) {
      (*cost)(i, 0) = (1.0 / (1.0 + std::exp(-val(i, 0)))) - Xty_(i, 0);
    }
  };

  /**
   *  @name EvaluateGradient
   *  @fn void EvaluateGradient(const Matrix& theta, Matrix* jacob)
   *  @brief  Evaluate the function's gradient with parameters #theta
   *  @param[in]  theta     Position where to evaluate objective function
   *  @param[out] jacob     Jacobian matrix for parameters #theta
   */
  void EvaluateGradient(const Matrix& theta, Matrix* jacob) const {
    if (!is_vector_cost_) {
      // Allocate memory if not already done
      jacob->resize(theta.rows(), 1);
      jacob->setZero();
      // Compute jacobian matrix
      const Matrix p = 1.0 / (1.0 + exp(-(XtX_ * theta).array()));
      for (int i = 0; i < XtX_.rows(); ++i) {
        *jacob += (p(i, 0) - Xty_(i, 0)) * (p(i, 0) * (1.0 - p(i, 0))) * XtX_.row(i).transpose();
      }
      *jacob /= XtX_.rows();
    } else {
      // Allocate memory if not already done
      jacob->resize(XtX_.rows(), theta.rows());
      // Compute Jacobian
      const Matrix p = 1.0 / (1.0 + exp(-(XtX_ * theta).array()));
      const Matrix h_theta = p.array() * (1.0 - p.array());
      for (int c = 0; c < XtX_.cols(); ++c) {
        jacob->col(c) = h_theta.array() * XtX_.col(c).array();
      }
    }
  }

  /**
   *  @name EvaluateHessian
   *  @fn void EvaluateHessian(const Matrix& x, Matrix* hessian)
   *  @brief  Evaluate the function's hessian matrix with parameters #theta
   *  @param[in]  theta   Parameters used to evalute hessian matrix
   *  @param[out] hessian Hessian value for parameters #theta
   */
  void EvaluateHessian(const Matrix& theta, Matrix* hessian) const {}

  /**
   *  @name get_dimensions
   *  @fn virtual void get_dimensions(int* n_row, int* n_col) const = 0
   *  @brief  Provide problem dimensions
   *  @param[out] n_row   Number of row in the system (Observables)
   *  @param[out] n_col   Number of column in the system (Variables)
   */
  void get_dimensions(int* n_row, int* n_col) const {
    *n_row = XtX_.rows();
    *n_col = XtX_.cols();
  }

  /**
   *  @name get_A
   *  @fn virtual Matrix& get_matrix_A(void) = 0
   *  @brief  Provide access to system matrix ()
   *  @return System Matrix
   */
  const Matrix& get_A(void) const {
    return XtX_;
  }

  /**
   *  @name get_AtA
   *  @fn virtual Matrix& get_AtA(void) = 0
   *  @brief  Provide access to system matrix ()
   *  @return At * A
   */
  const Matrix& get_AtA(void) const {
    return XtX_.transpose() * XtX_;
  };

  /**
   *  @name get_y
   *  @fn virtual Matrix& get_y(void) = 0
   *  @brief Provide access to y
   *  @return y
   */
  const Matrix& get_y(void) const {
    return Xty_;
  }

  /**
   *  @name get_Aty
   *  @fn virtual Matrix& get_Aty(void) = 0
   *  @brief Provide access to At * y
   *  @return vector At * y
   */
  const Matrix& get_Aty(void) const {
    return XtX_.transpose() * Xty_;
  };

  /** Indicate if vector cost function */
  bool is_vector_cost_;

  private :

  /** Data */
  Matrix XtX_;
  /** Target */
  Matrix Xty_;
};


/**
 *  @class  BoothProblem
 *  @brief  Test case for optimization module debugging
 *  @author Christophe Ecabert
 *  @date   04/01/16
 *  @see    https://en.wikipedia.org/wiki/Test_functions_for_optimization
 */
template<class T>
class BoothProblem : public LTS5::BaseProblem<T> {
  public :

  using Matrix = typename LTS5::BaseProblem<T>::Matrix;

  /**
   *  @name BoothProblem
   *  @brief  Constructor
   */
  BoothProblem(void) {}

  /**
   *  @name ~BoothProblem
   *  @brief  Destructor
   */
  ~BoothProblem(void) {}

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn EvaluateObjectiveFunction(const Matrix& theta, double* cost) = 0
   *  @brief  Evaluate the objective function with parameters #theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost
   *  @return Cost for parameters #theta
   */
  void EvaluateObjectiveFunction(const Matrix& x, T* cost) const {
    // Compute cost function
    double x0 = x(0,0);
    double x1 = x(1,0);
    *cost = ((x0 + 2 * x1 - 7) * (x0 + 2 * x1 - 7) +
             (2 * x0 + x1 -5) * (2 * x0 + x1 - 5));
  }

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost)
   *  @brief  Evaluate the vector objective function with parameters #theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost vector
   *  @return Cost for parameters #theta
   */
  void EvaluateObjectiveFunction(const Matrix& x, Matrix* cost) const {
    std::cout << "Not supported !" << std::endl;
  }

  /**
   *  @name EvaluateGradient
   *  @fn void EvaluateGradient(const Matrix& theta, Matrix* jacob)
   *  @brief  Evaluate the function's gradient with parameters #theta
   *  @param[in]  theta     Position where to evaluate objective function
   *  @param[out] jacob     Jacobian matrix for parameters #theta
   */
  void EvaluateGradient(const Matrix& x, Matrix* jacob) const {
    jacob->resize(2, 1);
    double x0 = x(0,0);
    double x1 = x(1,0);
    (*jacob)(0,0) = 10 * x0 + 8 * x1 - 34;
    (*jacob)(1,0) = 8 * x0 + 10 * x1 - 38;
  }

  /**
   *  @name EvaluateHessian
   *  @fn void EvaluateHessian(const Matrix& x, Matrix* hessian)
   *  @brief  Evaluate the function's hessian matrix with parameters #theta
   *  @param[in]  theta   Parameters used to evalute hessian matrix
   *  @param[out] hessian Hessian value for parameters #theta
   */
  void EvaluateHessian(const Matrix& x, Matrix* hessian) const {
    hessian->resize(2, 2);
    (*hessian)(0, 0) = 10;
    (*hessian)(0, 1) = 8;
    (*hessian)(1, 0) = 8;
    (*hessian)(1, 1) = 10;
  }

  /**
   *  @name get_dimensions
   *  @fn virtual void get_dimensions(int* n_row, int* n_col) const = 0
   *  @brief  Provide problem dimensions
   *  @param[out] n_row   Number of row in the system (Observables)
   *  @param[out] n_col   Number of column in the system (Variables)
   */
  void get_dimensions(int* n_row, int* n_col) const {
    std::cout << "Not supported !" << std::endl;
  };

  /**
   *  @name get_A
   *  @fn virtual Matrix& get_A(void) const = 0
   *  @brief  Provide access to system matrix ()
   *  @return System Matrix
   */
  const Matrix& get_A(void) const {
    throw std::runtime_error("Not supported !");
  }

  /**
   *  @name get_AtA
   *  @fn virtual Matrix& get_AtA(void) const = 0
   *  @brief  Provide access to system matrix ()
   *  @return At * A
   */
  const Matrix& get_AtA(void) const {
    throw std::runtime_error("Not supported !");
  }

  /**
   *  @name get_y
   *  @fn virtual Matrix& get_y(void) = 0
   *  @brief Provide access to y
   *  @return y
   */
  const Matrix& get_y(void) const {
    throw std::runtime_error("Not supported !");
  }

  /**
   *  @name get_Aty
   *  @fn virtual Matrix& get_Aty(void) const = 0
   *  @brief Provide access to At * y
   *  @return At * y
   */
  const Matrix& get_Aty(void) const {
    throw std::runtime_error("Not supported !");
  }

  void set_y(const Matrix& y) {
    throw std::runtime_error("Not supported !");
  }
};






int main(int argc, const char * argv[]) {
  // Init
  using Matrix = LTS5::BaseProblem<double>::Matrix;
  int n_loop = 1;
//  srand((unsigned int) time(0));
  srand((unsigned int)100);

  for (int i = 0; i < n_loop; ++i) {
    // Variables
    const int n_params = 20;
    const int n_uparams = 80;

    // Option
    LTS5::BaseSolver<double>::SolverOption option;
    option.learning_rate = 400e-1;
    option.max_iter = 100;
    LTS5::BaseSolver<double>::SolverOption uoption;
    uoption.learning_rate = 2;
    uoption.max_iter = 100;


    // Create true parameters
    Matrix true_theta = Matrix::Random(n_params, 1);
    Matrix true_utheta = Matrix::Random(n_uparams, 1);

    // Generate data
    Matrix X = Matrix::Random(50, n_params);
    Matrix uX = Matrix::Random(50, n_uparams);
    Matrix y =  1.0/(1.0 + exp(-(X*true_theta).array()));
    Matrix uy =  1.0/(1.0 + exp(-(uX*true_utheta).array()));

    // Define optimization problem
    LogisticRegression<double>* prob = new LogisticRegression<double>(X, y);
    ULogisticRegression<double>* uprob = new ULogisticRegression<double>(uX, uy);



    // Create solver
    LTS5::GradientDescentSolver<double>* solver = new LTS5::GradientDescentSolver<double>();
    LTS5::GaussNewtonSolver<double>* solver_gn = new LTS5::GaussNewtonSolver<double>();
    LTS5::LevenbergMarquardtSolver<double>* solver_lm = new LTS5::LevenbergMarquardtSolver<double>();



    // optimize
    Matrix theta0 = Matrix::Zero(n_params, 1);
    Matrix utheta0 = Matrix::Zero(n_uparams, 1);
    Matrix theta0_gn = theta0;
    Matrix theta0_lm = theta0;

//    std::cout << "******* Gradient-Descent *******" << std::endl;
//    solver->set_option(option);
//    bool ret = solver->Minimize(*prob, &theta0);
//
//    //std::cout << "Solution : \n" << theta0 << std::endl;
//    //std::cout << "Real solution : \n" << true_theta << std::endl;
//    std::cout << "Solution diff : " << (theta0 - true_theta).norm() << std::endl;

    std::cout << "******* Gradient-Descent Underdetermined *******" << std::endl;
    solver->set_option(uoption);
    solver->Minimize(*uprob, &utheta0);

    //std::cout << "Solution : \n" << theta0 << std::endl;
    //std::cout << "Real solution : \n" << true_theta << std::endl;
    std::cout << "Solution diff : " << (utheta0 - true_utheta).norm() << std::endl;

    std::cout << utheta0 << std::endl;
    std::cout << "************" << std::endl;
    std::cout << true_utheta << std::endl;

    std::cout << "******* Gauss-Newton *******" << std::endl;
    prob->is_vector_cost_ = true;
    option.learning_rate = 1e-1;
    option.max_iter = 100;
    solver_gn->set_option(option);
    solver_gn->Minimize(*prob, &theta0_gn);
    //std::cout << "Solution : \n" << theta0_gn << std::endl;
    //std::cout << "Real solution : \n" << true_theta << std::endl;
    std::cout << "Solution diff : " << (theta0_gn - true_theta).norm() << std::endl;

    std::cout << "******* Levenberg-Marquardt *******" << std::endl;
    prob->is_vector_cost_ = true;
    option.learning_rate = 1e-1;
    option.damping_mu = 5e-4;
    option.max_iter = 100;
    solver_lm->set_option(option);
    solver_lm->Minimize(*prob, &theta0_lm);
    //std::cout << "Solution : \n" << theta0_gn << std::endl;
    //std::cout << "Real solution : \n" << true_theta << std::endl;
    std::cout << "Solution diff : " << (theta0_lm - true_theta).norm() << std::endl;

    // Clean up
    delete solver;
    delete solver_gn;
    delete solver_lm;
    delete prob;
    delete uprob;
  }

  BoothProblem<double>* prob = new BoothProblem<double>();
  LTS5::NewtonSolver<double>* solver = new LTS5::NewtonSolver<double>();

  LTS5::BaseSolver<double>::SolverOption option;
  option.learning_rate = 1.0;
  solver->set_option(option);

  Matrix x = Matrix::Zero(2, 1);
  solver->Minimize(*prob, &x);

  std::cout << "Solution found : \n" << x << std::endl;

  delete prob;
  delete solver;


  return 0;
}