/**
 *  @file   matrix_completion.cpp
 *  @brief  Matrix completion sample
 *
 *  @author Christophe Ecabert
 *  @date   28/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */


#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/optimization/robust_matrix_completion.hpp"

int main(int argc, const char * argv[]) {
  // Type
  using T = double;
  using MCf = typename LTS5::MatrixCompletionMF<T>;
  using Matrixf = Eigen::MatrixXd;

  // Parser
  LTS5::CmdLineParser parser;
  parser.AddArgument("-i",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Input image");
  int err = parser.ParseCmdLine(argc, argv);
  if (err == 0) {
    // Get args
    std::string input, output;
    parser.HasArgument("-i", &input);
    // Load image + convert to float
    cv::Mat img;
    int frmt = sizeof(T) == 4 ? CV_32FC1 : CV_64FC1;
    cv::imread(input, cv::IMREAD_GRAYSCALE).convertTo(img, frmt);
    cv::Mat img_col = img.t();  // Col-major layout
    // Convert data + normalize
    Matrixf Zi = Eigen::Map<Matrixf>(reinterpret_cast<T*>(img_col.data),
                                   img.rows,
                                   img.cols);
    Zi = Zi / T(255.0);
    // Create mask + Add perturbation
    Matrixf mask = (Matrixf::Random(Zi.rows(), Zi.cols()) * T(0.5) +
                    Matrixf::Constant(Zi.rows(), Zi.cols(), T(0.5)));
    mask = (mask.array() >= T(0.4)).select(Matrixf::Ones(Zi.rows(), Zi.cols()),
                                           Matrixf::Zero(Zi.rows(), Zi.cols()));
    Matrixf Z = Zi.cwiseProduct(mask);
    // Show data
    cv::Mat in = cv::Mat(Z.cols(), Z.rows(), frmt, (void*)Z.data()).t();
    cv::imshow("Input", in);
    cv::waitKey(30);
    // Regenarate data
    MCf rmcmf;
    Matrixf rec, outliner;
    rmcmf.Solve(Z, &rec, &outliner);
    // Display reconstruction
    cv::Mat ocv_rec = cv::Mat(rec.cols(),
                              rec.rows(),
                              frmt,
                              (void*)rec.data()).t();
    cv::Mat ocv_outliner = cv::Mat(outliner.cols(),
                                   outliner.rows(),
                                   frmt,
                                   (void*)outliner.data()).t();
    cv::imshow("Reconstruction", ocv_rec);
    cv::imshow("Outliner", ocv_outliner);
    cv::waitKey(0);
  } else {
    LTS5_LOG_ERROR("Unable to parse command line !");
  }
  return err;
}
