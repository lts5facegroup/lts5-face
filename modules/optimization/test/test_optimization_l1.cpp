//
//  test_optimization.cpp
//  LTS5-Lib
//
//  Created by Christophe Ecabert on 20/11/15.
//  Copyright © 2015 Ecabert Christophe. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <random>
#include <chrono>

#include "Eigen/Dense"

#include "lts5/utils/math/linear_algebra_types.hpp"
#include "lts5/optimization/feature_sign_solver.hpp"
#include "lts5/optimization/lagrange_dual_problem.hpp"
#include "lts5/optimization/lagrange_dual_solver.hpp"

/**
 *  @class  LeastSquareProblem
 *  @brief  Test case for optimization module debugging
 *  @author Christophe Ecabert
 *  @date   20/11/15
 *  @see    https://en.wikipedia.org/wiki/Logistic_regression
 */
template<class T>
class LeastSquareProblem : public LTS5::BaseProblem<T> {
 public :

  using Matrix = typename LTS5::BaseProblem<T>::Matrix;

  /**
   *  @name LeastSquareProblem
   *  @brief  Constructor
   */
  LeastSquareProblem(const Matrix& A, const Matrix& y) :
  A_(A), AtA_(A.transpose() * A), y_(y), Aty_(A.transpose() * y) {}

  /**
   *  @name ~LeastSquareProblem
   *  @brief  Destructor
   */
  ~LeastSquareProblem(void) {}

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn EvaluateObjectiveFunction(const Matrix& theta, double* cost) = 0
   *  @brief  Evaluate the objective function with parameters #theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost
   *  @return Cost for parameters #theta
   */
  void EvaluateObjectiveFunction(const Matrix& x, T* cost) const {
    // Compute cost function
    Matrix diff = y_ - (A_ * x);
    Matrix tmp = diff.transpose() * diff;
    *cost = tmp(0, 0);
  }

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost)
   *  @brief  Evaluate the vector objective function with parameters #theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost vector
   *  @return Cost for parameters #theta
   */
  void EvaluateObjectiveFunction(const Matrix& x, Matrix* cost) const {
  }

  /**
   *  @name EvaluateGradient
   *  @fn void EvaluateGradient(const Matrix& theta, Matrix* jacob)
   *  @brief  Evaluate the function's gradient with parameters #theta
   *  @param[in]  theta     Position where to evaluate objective function
   *  @param[out] jacob     Jacobian matrix for parameters #theta
   */
  void EvaluateGradient(const Matrix& x, Matrix* jacob) const {
    *jacob = AtA_ * x - Aty_;
  }

  /**
   *  @name EvaluateHessian
   *  @fn void EvaluateHessian(const Matrix& x, Matrix* hessian)
   *  @brief  Evaluate the function's hessian matrix with parameters #theta
   *  @param[in]  theta   Parameters used to evalute hessian matrix
   *  @param[out] hessian Hessian value for parameters #theta
   */
  void EvaluateHessian(const Matrix& x, Matrix* hessian) const {}

  /**
   *  @name get_dimensions
   *  @fn virtual void get_dimensions(int* n_row, int* n_col) const = 0
   *  @brief  Provide problem dimensions
   *  @param[out] n_row   Number of row in the system (Observables)
   *  @param[out] n_col   Number of column in the system (Variables)
   */
  void get_dimensions(int* n_row, int* n_col) const {
    *n_row = static_cast<int>(A_.rows());
    *n_col = static_cast<int>(A_.cols());
  };

  /**
   *  @name get_A
   *  @fn virtual Matrix& get_A(void) const = 0
   *  @brief  Provide access to system matrix ()
   *  @return System Matrix
   */
  const Matrix& get_A(void) const {
    return A_;
  }

  /**
   *  @name get_AtA
   *  @fn virtual Matrix& get_AtA(void) const = 0
   *  @brief  Provide access to system matrix ()
   *  @return At * A
   */
  const Matrix& get_AtA(void) const {
    return AtA_;
  }

  /**
   *  @name get_y
   *  @fn virtual Matrix& get_y(void) = 0
   *  @brief Provide access to y
   *  @return y
   */
  const Matrix& get_y(void) const {
    return y_;
  }

  /**
   *  @name get_Aty
   *  @fn virtual Matrix& get_Aty(void) const = 0
   *  @brief Provide access to At * y
   *  @return At * y
   */
  const Matrix& get_Aty(void) const {
    return Aty_;
  }

  void set_y(const Matrix& y) {
    y_ = y;
    Aty_ = A_.transpose() * y_;
  }

 private :

  /** Data */
  Matrix A_;
  /** Precomputed AtA */
  Matrix AtA_;
  /** Target */
  Matrix y_;
  /** Precomputed Aty */
  Matrix Aty_;
};


int main(int argc, const char * argv[]) {
  // Init

  using Mat = LTS5::BaseProblem<double>::Matrix;

  // Load data
  Mat B;
  Mat X;
  Mat S;

  std::ifstream in("data.bin",std::ios_base::binary);

  int n_row = -1;
  int n_col = -1;
  // Problem || X - Bs || + gamma |s|
  // Basis
  in.read(reinterpret_cast<char*>(&n_row), sizeof(n_row));
  in.read(reinterpret_cast<char*>(&n_col), sizeof(n_col));
  B = Mat(n_row, n_col);
  in.read(reinterpret_cast<char*>(B.data()), n_col * n_row * sizeof(double));
  // X
  in.read(reinterpret_cast<char*>(&n_row), sizeof(n_row));
  in.read(reinterpret_cast<char*>(&n_col), sizeof(n_col));
  X = Mat(n_row, n_col);
  in.read(reinterpret_cast<char*>(X.data()), n_col * n_row * sizeof(double));
  // S
  in.read(reinterpret_cast<char*>(&n_row), sizeof(n_row));
  in.read(reinterpret_cast<char*>(&n_col), sizeof(n_col));
  S = Mat(n_row, n_col);
  in.read(reinterpret_cast<char*>(S.data()), n_col * n_row * sizeof(double));
  in.close();



  // Create problem + Solver
  LTS5::BaseSolver<double>::SolverOption opt;
  opt.gamma = 0.0001;
  LeastSquareProblem<double>* prob = new LeastSquareProblem<double>(B, X.col(0));
  LTS5::FeatureSignSolver<double>* solver = new LTS5::FeatureSignSolver<double>();
  solver->set_option(opt);

  // Solve
  Mat S_opt = Mat(S.rows(), S.cols());
  Mat x_min = Mat(B.cols(), 1);
  double mean_err = 0.0;
  for (int i = 0; i < X.cols(); ++i) {
    // Set proper sample
    prob->set_y(X.col(i));
    // Minimize
    solver->Minimize(*prob, &x_min);
    // Copy solution into matrix
    S_opt.col(i) = x_min;
    // Print out error
    double err = (x_min - S.col(i)).norm();
    mean_err += err;
    std::cout << err << std::endl;
  }
  std::cout << "Mean err : " << mean_err / X.cols() << std::endl;


  // Test basis recovery
  LTS5::LagrangeDualProblem<double>* prob_dual = new LTS5::LagrangeDualProblem<double>(S, X);
  LTS5::LagrangeDualSolver<double>* dual_solver = new LTS5::LagrangeDualSolver<double>();
  dual_solver->set_internal_learning_rate(0.1);
  // Recover basis
  Mat B_star = Mat::Zero(X.rows(), S.cols());

  double basis_err = 0.0;
  for (int i = 0; i < X.cols(); ++i) {
    // Recover basis
    dual_solver->Minimize(*prob_dual, &B_star);
    // Diff
    double err = (B - B_star).norm();
    basis_err += err;
    std::cout << "Basis err : " << err << std::endl;
  }
  std::cout << "Mean basis err : " << basis_err / X.cols() << std::endl;






  // Clean up
  delete prob;
  delete prob_dual;
  delete solver;
  delete dual_solver;

  return 0;
}