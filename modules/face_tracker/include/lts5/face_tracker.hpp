/**
 *  @file   lts5/face_tracker.hpp
 *
 *  @author Christophe Ecabert
 *  @date   22/04/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include "lts5/face_tracker/face_tracker.hpp"