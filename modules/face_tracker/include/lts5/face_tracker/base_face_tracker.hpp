/**
 *  @file   "lts5/face_tracker/base_face_tracker.hpp"
 *  @brief  Base Face tracker definition
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   22/04/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_BASE_FACE_TRACKER__
#define __LTS5_BASE_FACE_TRACKER__

#include <string>
#include <vector>
#include <iostream>
#include <limits>

#include "opencv2/core.hpp"

#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  BaseFaceTracker
 *  @brief  Abstract class to define basic face tracker interface
 *  @author Christophe Ecabert
 *  @date   22/04/15
 *  @ingroup face_tracker
 *  @tparam ImgType Image data type
 *  @tparam ShapeType Shape data type
 */
template<typename ImgType, typename ShapeType>
class BaseFaceTracker {
 public:

  /**
   *  @name ~BaseFaceTracker
   *  @fn   virtual ~BaseFaceTracker()
   *  @brief  Destructor
   */
  virtual ~BaseFaceTracker() = default;

  /**
   *  @name   Load
   *  @fn virtual int Load(const std::string& model_filename) = 0
   *  @brief  Load the tracker model. This must be override by subclass
   *  @param[in]  model_filename      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  virtual int Load(const std::string& model_filename) = 0;

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& model_filename,
                       const std::string& face_detector_config)
   *  @brief  Load the LBF model and Viola Jones face detector
   *  @param[in]  model_filename          Path to the model
   *  @param[in]  face_detector_config    Configuration file for
   *                                      Viola & Jones face detector
   *  @return -1 if errors, 0 otherwise
   */
  virtual int Load(const std::string& model_filename,
                   const std::string& face_detector_config) {
    std::cout << "Warning, Not implemented" << std::endl;
    return -1;
  }

  /**
   *  @name   Save
   *  @fn virtual int Save(const std::string& model_name) = 0
   *  @brief  Save the trained model in file. This must be override by subclass
   *  @param[in] model_name   Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  virtual int Save(const std::string& model_name) = 0;

  /**
   *  @name   Detect
   *  @fn virtual int Detect(const ImgType& image, ShapeType* shape) = 0
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks. This must be override by subclass
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  virtual int Detect(const ImgType& image, ShapeType* shape) = 0;

  /**
   *  @name   Detect
   *  @fn virtual int Detect(const ImgType& image, const cv::Rect& face_region,
                             ShapeType* shape) = 0
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks. This must be override by subclass
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face boundingg box
   *  @param[out] shape           Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  virtual int Detect(const ImgType& image,
                     const cv::Rect& face_region,
                     ShapeType* shape) = 0;

  /**
   *  @name   Track
   *  @fn virtual int Track(const ImgType& image, ShapeType* shape) = 0
   *  @brief  Tracking method, given an input image (from a sequence), provides
   *          a set of landmarks. This must be override by subclass
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  virtual int Track(const ImgType& image, ShapeType* shape) = 0;

  /**
   *  @name   Train
   *  @fn virtual void Train(const std::string& folder) = 0
   *  @brief  Tracker training method. This must be override by subclass
   *  @param[in]  folder  Location of images/annotations
   */
  virtual void Train(const std::string& folder) = 0;

  /**
   *  @name get_score_threshold
   *  @fn virtual double get_score_threshold() const
   *  @brief  Get the actual decision threshold
   *  @return Decision threshold
   */
  virtual double get_score_threshold() const {
    LTS5_LOG_WARNING("Not implemented, should be derived by user");
    return std::numeric_limits<double>::infinity();
  }

  /**
   *  @name set_score_threshold
   *  @fn virtual void set_score_threshold(const double threshold)
   *  @brief  Set new decision threshold
   *  @param[in]  threshold   Decision threshold
   */
  virtual void set_score_threshold(const double threshold) {
    LTS5_LOG_WARNING("Not implemented, should be derived by user");
  }

  /**
   * @name  get_score
   * @fn    virtual double get_score() const
   * @brief Provide assessment score
   * @return    Assessment score
   */
  virtual double get_score() const {
    LTS5_LOG_WARNING("Not implemented, should be derived by user");
    return std::numeric_limits<double>::quiet_NaN();
  }
};

/**
 * @class   TrackerProxy
 * @brief   Interface for registration mechanism for face tracker type
 * @author  Christophe Ecabert
 * @date    14/08/17
 * @ingroup face_tracker
 */
class TrackerProxy {
 public:

  /**
   * @name  TrackerProxy
   * @fn    TrackerProxy()
   * @brief Constructor
   */
  TrackerProxy();

  /**
   * @name  ~TrackerProxy
   * @fn    ~TrackerProxy()
   * @brief Destructor
   */
  virtual ~TrackerProxy() = default;

  /**
   * @name  Create
   * @fn    virtual BaseFaceTracker<cv::Mat, cv::Mat>* Create() const = 0
   * @brief Create an instance of BaseFaceTracker
   * @return    Tracker instance with proper type
   */
  virtual BaseFaceTracker<cv::Mat, cv::Mat>* Create() const = 0;

  /**
   *  @name Name
   *  @fn virtual const char* Name() const = 0
   *  @brief  Return the name for a given type of tracker
   *  @return Tracker name (i.e. sdm, lbf, ...)
   */
  virtual const char* Name() const = 0;
};

}  // namespace LTS5
#endif  /* __LTS5_BASE_FACE_TRACKER__ */



















