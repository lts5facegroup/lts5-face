/**
 *  @file   "lts5/face_tracker/sdm_tracker.hpp"
 *  @brief  SDM tracker class definition
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   22/04/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_SDM_TRACKER__
#define __LTS5_SDM_TRACKER__

#include <vector>
#include <limits>
#include <string>

#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/ssift.hpp"
#include "lts5/face_detector/base_face_detector.hpp"
#include "lts5/face_tracker/face_tracker_assessment.hpp"
#include "lts5/face_tracker/base_face_tracker.hpp"
#include "lts5/classifier/pca.hpp"

namespace LTS5 {
/**
 *  @class  SdmTracker
 *  @brief  SDM-facetracker implementation
 *  @author Hua Gao / Christophe Ecabert
 *  @date   22/04/15
 *  @see    https://courses.cs.washington.edu/courses/cse590v/13au/intraface.pdf
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS SdmTracker : public BaseFaceTracker<cv::Mat, cv::Mat> {
 public :

  /**
   * @enum	FaceDetectorType
   * @brief	Type of face detector to use
   */
  enum FaceDetectorType {
    kCascadeVJ = 0,
    kPartsBased
  };

  /**
   *  @struct SdmTrackerParameters
   *  @brief  User configuration for sdm tracker
   */
  struct SdmTrackerParameters {
    /** How many stages are part of the model */
    int number_of_stage;
    /** If TRUE, align initial shape including rotation otherwise no */
    bool align_with_rotation;
    /** Size of the normalized face region */
    float face_region_width;
    /** Size of the sift patch */
    int sift_size;
    /** Apply coarse to fine search */
    bool coarse_to_fine_search;
    /** How many stages are coarse */
    int number_of_coarse_stages;
    /** How many passes are done for initialization */
    int number_of_pass;
    /** Indicate at which pass the iteration start */
    int starting_pass;
    /** Features descriptor parameters */
    SSift::SSiftParameters sift_parameters;
    /** Number of montecarlo samples used for training */
    int montecarlo_samples;
    /** Initialization of the random number generator, true random if -1 */
    int montecarlo_init_random;
    /** Number of attempt for Montecarlo sampling */
    int montecarlo_sampling_attempt;
    /** Statistics X scaling factors */
    double montecarlo_offsetX_std_scaling;
    /** Statistics Y scaling factors */
    double montecarlo_offsetY_std_scaling;
    /** Statistics SCALE scaling factors */
    double montecarlo_scale_std_scaling;
    /** Statistics scaling factors for tracking quality assessment training */
    double montecarlo_tracking_std_scaling;
    /** Computation methods */
    LTS5::PcaComputationType train_computation_mod;
    /** How many variance is retain */
    double retain_variance;
    /** Training statistics, mean_s, std_s, mean_tx, std_tx, mean_ty, std_ty */
    double training_statistics[6];
    /** Wheter or not to flip the training images */
    bool flip_images;

    /**
     *  @name   SdmTrackerParameters
     *  @fn SdmTrackerParameters()
     *  @brief  Constructor
     */
    SdmTrackerParameters() {
      number_of_stage = 4;
      align_with_rotation = true;
      face_region_width = 200.f;
      sift_size = 32;
      coarse_to_fine_search = true;
      number_of_coarse_stages = 2;
      number_of_pass = 2;
      starting_pass = 0;
      // Training related
      montecarlo_samples = 10;
      montecarlo_init_random = -1;
      montecarlo_sampling_attempt = 10;
      montecarlo_offsetX_std_scaling = 0.5;
      montecarlo_offsetY_std_scaling = 0.5;
      montecarlo_scale_std_scaling = 0.5;
      montecarlo_tracking_std_scaling = 1.0;
      train_computation_mod = kLapack;
      retain_variance = 0.98;
      training_statistics[0] = std::numeric_limits<double>::quiet_NaN();
      training_statistics[1] = std::numeric_limits<double>::quiet_NaN();
      training_statistics[2] = std::numeric_limits<double>::quiet_NaN();
      training_statistics[3] = std::numeric_limits<double>::quiet_NaN();
      training_statistics[4] = std::numeric_limits<double>::quiet_NaN();
      training_statistics[5] = std::numeric_limits<double>::quiet_NaN();
      flip_images = false;
    }

    /**
     *  @name   ComputeHeaderSize
     *  @fn int ComputeHeaderSize() const
     *  @brief  Compute the size of the header in byte
     *  @return Length in bytes
     */
    int ComputeHeaderSize() const {
      int size = 0;
      /** Add number of int */
      size += 11 * sizeof(int);
      /** Add number of float */
      size += 1 * sizeof(float);
      /** Add number of double */
      size += 11 * sizeof(double);
      /** Sift config */
      size += sift_parameters.ComputeObjectSize();
      return size;
    }

    SdmTrackerParameters& operator=(const SdmTrackerParameters& rhs) {
      if (this != &rhs) {
        this->number_of_stage = rhs.number_of_stage;
        this->align_with_rotation = rhs.align_with_rotation;
        this->face_region_width = rhs.face_region_width;
        this->sift_size = rhs.sift_size;
        this->coarse_to_fine_search = rhs.coarse_to_fine_search;
        this->number_of_coarse_stages = rhs.number_of_coarse_stages;
        this->number_of_pass = rhs.number_of_pass;
        this->starting_pass = rhs.starting_pass;
        this->sift_parameters = rhs.sift_parameters;
        this->montecarlo_samples = rhs.montecarlo_samples;
        this->montecarlo_init_random = rhs.montecarlo_init_random;
        this->montecarlo_sampling_attempt = rhs.montecarlo_sampling_attempt;
        this->montecarlo_offsetX_std_scaling = rhs.montecarlo_offsetX_std_scaling;
        this->montecarlo_offsetY_std_scaling = rhs.montecarlo_offsetY_std_scaling;
        this->montecarlo_scale_std_scaling = rhs.montecarlo_scale_std_scaling;
        this->montecarlo_tracking_std_scaling = rhs.montecarlo_tracking_std_scaling;
        this->train_computation_mod = rhs.train_computation_mod;
        this->retain_variance = rhs.retain_variance;
        this->training_statistics[0] = rhs.training_statistics[0];
        this->training_statistics[1] = rhs.training_statistics[1];
        this->training_statistics[2] = rhs.training_statistics[2];
        this->training_statistics[3] = rhs.training_statistics[3];
        this->training_statistics[4] = rhs.training_statistics[4];
        this->training_statistics[5] = rhs.training_statistics[5];
        this->flip_images = rhs.flip_images;
      }
      return *this;
    }

    /**
     *  @name WriteParameterHeader
     *  @fn int WriteParameterHeader(std::ostream& output_stream) const
     *  @brief  Dump tracker parameters into a given stream.
     *  @param[in]  output_stream Stream where data will be written
     *  @return -1 If error, 0 otherwise
     */
    int WriteParameterHeader(std::ostream& output_stream) const {
      int error = -1;
      if (output_stream.good()) {
        int dummy = 0;
        // Tracker
        // --------------------------------------------------------------
        // Number of stage
        output_stream.write(reinterpret_cast<const char*>(&number_of_stage),
                            sizeof(number_of_stage));
        // Number of pass used durint init
        output_stream.write(reinterpret_cast<const char*>(&number_of_pass),
                            sizeof(number_of_pass));
        // Coarse to fine approach flag
        dummy = coarse_to_fine_search ? 1 : 0;
        output_stream.write(reinterpret_cast<const char*>(&dummy),
                            sizeof(dummy));
        // Number of coarse pass
        output_stream.write(reinterpret_cast<const char*>(&number_of_coarse_stages),
                            sizeof(number_of_coarse_stages));
        // Starting pass
        output_stream.write(reinterpret_cast<const char*>(&starting_pass),
                            sizeof(starting_pass));
        // Face region width (image normalization)
        output_stream.write(reinterpret_cast<const char*>(&face_region_width),
                            sizeof(face_region_width));
        // Alignment init
        dummy = align_with_rotation ? 1 : 0;
        output_stream.write(reinterpret_cast<const char*>(&dummy),
                            sizeof(dummy));
        // Sift features descriptor
        // --------------------------------------------------------------
        // Sift size
        output_stream.write(reinterpret_cast<const char*>(&sift_size),
                            sizeof(sift_size));
        // Sift configuration
        error = sift_parameters.WriteParameterHeader(output_stream);

        // Training, just in case of
        // --------------------------------------------------------------
        // montecarlo_samples
        output_stream.write(reinterpret_cast<const char*>(&montecarlo_samples),
                            sizeof(montecarlo_samples));
        // montecarlo_init_random
        output_stream.write(reinterpret_cast<const char*>(&montecarlo_init_random),
                            sizeof(montecarlo_init_random));
        // montecarlo_sampling_attempt
        output_stream.write(reinterpret_cast<const char*>(&montecarlo_sampling_attempt),
                            sizeof(montecarlo_sampling_attempt));
        // montecarlo_offsetX_std_scaling
        output_stream.write(reinterpret_cast<const char*>(&montecarlo_offsetX_std_scaling),
                            sizeof(montecarlo_offsetX_std_scaling));
        // montecarlo_offsetY_std_scaling
        output_stream.write(reinterpret_cast<const char*>(&montecarlo_offsetY_std_scaling),
                            sizeof(montecarlo_offsetY_std_scaling));
        // montecarlo_scale_std_scaling
        output_stream.write(reinterpret_cast<const char*>(&montecarlo_scale_std_scaling),
                            sizeof(montecarlo_scale_std_scaling));
        // montecarlo_tracking_std_scaling
        output_stream.write(reinterpret_cast<const char*>(&montecarlo_tracking_std_scaling),
                            sizeof(montecarlo_tracking_std_scaling));
        // train_computation_mod
        dummy = static_cast<int>(train_computation_mod);
        output_stream.write(reinterpret_cast<const char*>(&dummy),
                            sizeof(dummy));
        // retain_variance
        output_stream.write(reinterpret_cast<const char*>(&retain_variance),
                            sizeof(retain_variance));
        // Training statistics
        output_stream.write(reinterpret_cast<const char*>(&training_statistics[0]),
                            6*sizeof(training_statistics[0]));
        // Done
        error |= output_stream.good() ? 0 : -1;
      }
      return error;
    }

    /**
     *  @name   ReadParameterHeader
     *  @fn int ReadParameterHeader(std::istream& input_stream)
     *  @brief  Initialize parameters structures from input stream
     *  @param[in]  input_stream  Stream from where to read data
     *  @return -1 If error, 0 otherwise
     */
    int ReadParameterHeader(std::istream& input_stream) {
      int error = -1;
      if (input_stream.good()) {
        // Load data
        int dummy = -1;
        // Tracker
        // --------------------------------------------------------------
        // Number of stage
        input_stream.read(reinterpret_cast<char*>(&number_of_stage),
                          sizeof(number_of_stage));
        // Number of pass used durint init
        input_stream.read(reinterpret_cast<char*>(&number_of_pass),
                          sizeof(number_of_pass));
        // Coarse to fine approach flag
        input_stream.read(reinterpret_cast<char*>(&dummy), sizeof(dummy));
        coarse_to_fine_search = (dummy == 1);
        // Number of coarse pass
        input_stream.read(reinterpret_cast<char*>(&number_of_coarse_stages),
                          sizeof(number_of_coarse_stages));
        // Starting pass
        input_stream.read(reinterpret_cast<char*>(&starting_pass),
                          sizeof(starting_pass));
        // Face region width (image normalization)
        input_stream.read(reinterpret_cast<char*>(&face_region_width),
                          sizeof(face_region_width));
        // Alignment init
        input_stream.read(reinterpret_cast<char*>(&dummy), sizeof(dummy));
        align_with_rotation = (dummy == 1);
        // Sift features descriptor
        // --------------------------------------------------------------
        // Sift size
        input_stream.read(reinterpret_cast<char*>(&sift_size),
                          sizeof(sift_size));
        // Sift descirptor
        error = sift_parameters.ReadParameterHeader(input_stream);

        // Training, just in case of
        // --------------------------------------------------------------
        // montecarlo_samples
        input_stream.read(reinterpret_cast<char*>(&montecarlo_samples),
                          sizeof(montecarlo_samples));
        // montecarlo_init_random
        input_stream.read(reinterpret_cast<char*>(&montecarlo_init_random),
                          sizeof(montecarlo_init_random));
        // montecarlo_sampling_attempt
        input_stream.read(reinterpret_cast<char*>(&montecarlo_sampling_attempt),
                          sizeof(montecarlo_sampling_attempt));
        // montecarlo_offsetX_std_scaling
        input_stream.read(reinterpret_cast<char*>(&montecarlo_offsetX_std_scaling),
                          sizeof(montecarlo_offsetX_std_scaling));
        // montecarlo_offsetY_std_scaling
        input_stream.read(reinterpret_cast<char*>(&montecarlo_offsetY_std_scaling),
                          sizeof(montecarlo_offsetY_std_scaling));
        // montecarlo_scale_std_scaling
        input_stream.read(reinterpret_cast<char*>(&montecarlo_scale_std_scaling),
                          sizeof(montecarlo_scale_std_scaling));
        // montecarlo_tracking_std_scaling
        input_stream.read(reinterpret_cast<char*>(&montecarlo_tracking_std_scaling),
                          sizeof(montecarlo_tracking_std_scaling));
        // train_computation_mod
        input_stream.read(reinterpret_cast<char*>(&dummy), sizeof(int));
        train_computation_mod = static_cast<LTS5::PcaComputationType>(dummy);
        // retain_variance
        input_stream.read(reinterpret_cast<char*>(&retain_variance),
                          sizeof(retain_variance));
        // Training statistics
        input_stream.read(reinterpret_cast<char*>(&training_statistics[0])
                          , 6 * sizeof(training_statistics[0]));
        // Done
        error |= input_stream.good() ? 0 : -1;
      }
      return error;
    }
  };

  /**
   *  @struct SortRectBySize
   *  @brief  Used to oreder rectangle by size (decreasing)
   */
  struct SortRectBySize {
    inline bool operator()(const cv::Rect& lhs, const cv::Rect& rhs) {
      return lhs.area() > rhs.area();
    }
  };

 private :
 /**
   *  @typedef  SdmModelConstIt_t
   *  @brief    Const iterator for sdm model
   */
  using SdmModelConstIt_t = std::vector<cv::Mat>::const_iterator;
  /**
   *  @typedef  SdmModelConstIt_t
   *  @brief    Const iterator for sdm model
   */
  using KeyPointIt_t = std::vector<cv::KeyPoint>::iterator;

  /** Tracker parameters */
  SdmTrackerParameters tracker_parameters_;
  /** Number of points tracked by the model */
  int num_points_;
  /** Model meanshape */
  cv::Mat sdm_meanshape_;
  /** Initial shape used to start alignment */
  cv::Mat initial_shape_;
  /** Shape recovered at previous iteration, used for tracking */
  cv::Mat previous_shape_;
  /** Tracking indicator flag */
  bool is_tracking_;
  /** SDM-facetracker model */
  std::vector<cv::Mat> sdm_model_;
  /** Viola-Jones face detector */
  //cv::CascadeClassifier* face_detector_;
  /** Face detector type */
  FaceDetectorType face_det_type_;
  /** Face detector */
  BaseFaceDetector* face_detector_;
  /** Postion of the detected face */
  std::vector<cv::Rect> faces_location_;
  /** Region selected for the alignment */
  cv::Rect face_region_;
  /** Shape center of gravity */
  cv::Point2f shape_center_gravity_;
  /** Grayscale images where face-tracker is applied */
  cv::Mat working_img_;
  /** SIFT features */
  cv::Mat sift_features_;
  /** Inner landmarks SIFT features */
  cv::Mat inner_sift_features_;
  /** Position where SIFT features will be extracted */
  std::vector<cv::KeyPoint> sift_extraction_location_;
  /** Sift descriptor parameters */
  SSift::SSiftParameters sift_desciptor_params_;
  /** Tracking quality assessment */
  LTS5::BaseFaceTrackerAssessment<cv::Mat,cv::Mat>* tracking_assessment_;
  /** Indicate if tracker need to take care of assessor or not */
  bool release_assessor_;

 public :

#pragma mark -
#pragma mark Initialization
  /**
   *  @name   SdmTracker
   *  @fn SdmTracker()
   *  @brief  Constructor
   */
  SdmTracker();

  /**
   *  @name   SdmTracker
   *  @fn SdmTracker(const FaceDetectorType& face_det_type)
   *  @brief  Constructor
   *  @param[in] face_det_type Type of the face det to instantiate (see enum)
   */
  explicit SdmTracker(const FaceDetectorType& face_det_type);

  /**
   *  @name   ~SdmTracker
   *  @fn ~SdmTracker()
   *  @brief  Destructor
   */
  ~SdmTracker() override;

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& model_filename) override
   *  @brief  Load the SDM model
   *  @param[in]  model_filename      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename) override;

  /**
   *  @name   Load
   *  @fn     int Load(std::istream& model_stream, const bool& is_binary)
   *  @brief  Load the SDM model
   *  @param[in]  model_stream      Stream to the model
   *  @param[in]  is_binary         Indicate if stream is open as binary
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& model_stream, const bool& is_binary);

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& model_filename,
              const std::string& face_detector_config) override
   *  @brief  Load the SDM model and Viola Jones face detector
   *  @param[in]  model_filename          Path to the model
   *  @param[in]  face_detector_config    Configuration file for
   *                                      Viola & Jones face detector
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename,
           const std::string& face_detector_config) override;

  /**
   *  @name   Load
   *  @fn     int Load(std::istream& model_stream,
                       const bool& is_binary,
                       const std::string& face_detector_config)
   *  @brief  Load the SDM model and Viola Jones face detector
   *  @param[in]  model_stream          Stream to the model
   *  @param[in]  is_binary             Indicate if stream is open as binary
   *  @param[in]  face_detector_config  Configuration file for Viola & Jones
   *                                    face detector
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& model_stream,
           const bool& is_binary,
           const std::string& face_detector_config);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& model_name) override
   *  @brief  Save the trained model in file.
   *  @param  [in]    model_name      Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  int Save(const std::string& model_name) override;

  /**
   *  @name   Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Write the object into a binary stream
   *  @param[in]  out_stream  Binary stream to files
   *  @return -1 if error, 0 otherwise.
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name   ComputeObjectSize
   *  @fn int ComputeObjectSize() const
   *  @brief  Compute the memory used by object
   *  @return Size in bytes
   */
  int ComputeObjectSize() const;

  #pragma mark -
  #pragma mark Tracking

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image, cv::Mat* shape) override
   *  @brief  Dectection method, given a still image, provides a set
   *          of landmarks.
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Detect(const cv::Mat& image, cv::Mat* shape) override;

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image,
                     const cv::Rect& face_region,
                     cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face boundingg box
   *  @param[out] shape           Set of landmarks
   *  @return     -1 if error, 0 otherwise
   */
  int Detect(const cv::Mat& image,
             const cv::Rect& face_region,
             cv::Mat* shape) override;

  /**
   *  @name   Track
   *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
   *  @brief  Tracking method, given an input image
   *  @param[in]      image   Input image
   *  @param[in,out]  shape   Set of landmarks
   *  @return         -1 if error, 0 otherwise
   */
  int Track(const cv::Mat& image, cv::Mat* shape) override;

  /**
   *  @name   Track
   *  @fn int Track(const cv::Mat& image,
                    const cv::Rect& face_region,
                    cv::Mat* shape)
   *  @brief  Tracking method, given an input image
   *  @param[in]  image       Input image
   *  @param[in]  face_region Location of the head used to initialize
   *                                    tracking;
   *  @param[in,out] shape       Set of landmarks
   *  @return    -1 if error, 0 otherwise
   */
  int Track(const cv::Mat& image,
            const cv::Rect& face_region,
            cv::Mat* shape);

  /**
   *  @name   ResetTracker
   *  @fn void ResetTracker()
   *  @brief  Reset current tracking workspace
   */
  void ResetTracker();

  /**
   * @name CheckQuality
   * @fn bool CheckQuality(const cv::Mat& image, const cv::Mat& shape)
   * @brief   Check quality of the shape
   * @param[in]   image   Input imate
   * @param[in]   shape   Detected shape to test
   * @return  True if good, false otherwise
   */
  bool CheckQuality(const cv::Mat& image, const cv::Mat& shape);

  #pragma mark -
  #pragma mark Accessors

  /**
   *  @name   get_tracker_parameters
   *  @fn SdmTrackerParameters get_tracker_parameters() const
   *  @brief  Provide the actual configuration of the tracker
   *  @return Struct with tracker parameters
   */
  SdmTrackerParameters get_tracker_parameters() const {
    return tracker_parameters_;
  }

  /**
   *  @name   get_sift_features
   *  @fn cv::Mat get_sift_features() const
   *  @brief  Provide access to features extracted by the tracker
   *  @return Matrix of SIFT features
   */
  cv::Mat get_sift_features() const { return sift_features_;}

  /**
   *  @name   get_inner_sift_features
   *  @fn cv::Mat get_inner_sift_features() const
   *  @brief  Provide access to features corresponding to inner landmarks
   *  @return Matrix of SIFT features
   */
  cv::Mat get_inner_sift_features() const {
    return inner_sift_features_;
  }

  /**
   *  @name   get_sift_features
   *  @fn cv::Mat get_sift_features(const cv::Mat& image, const cv::Mat& shape) const
   *  @brief  Provide feature extracted by given shape
   *  @param[in]  image   input image
   *  @param[in]  shape   input shape
   *  @return Matrix of SIFT features
   */
  cv::Mat get_sift_features(const cv::Mat& image, const cv::Mat& shape) const;

  /**
   * @name  get_meanshape
   * @fn    cv::Mat get_meanshape() const
   * @brief Provide face meanshape
   * @return    Face tracker meanshape
   */
  cv::Mat get_meanshape() const {
    return sdm_meanshape_;
  }

  /**
   *  @name   Get_sift_descriptor_parameters
   *  @fn SSift::SSiftParameters Get_sift_descriptor_parameters() const
   *  @brief  Give the parameters of the sift descriptor
   *  @return Struct with SIFT descriptor configuration
   */
  SSift::SSiftParameters Get_sift_descriptor_parameters() const {
    return sift_desciptor_params_;
  }

  /**
   *  @name get_score_threshold
   *  @fn double get_score_threshold() const override
   *  @brief  Get the actual decision threshold
   *  @return Decision threshold
   */
  double get_score_threshold() const override {
    if (tracking_assessment_) {
      return tracking_assessment_->get_score_threshold();
    } else {
      LTS5_LOG_WARNING("Tracker not initialized");
      return std::numeric_limits<double>::quiet_NaN();
    }
  }

  /**
   *  @name set_score_threshold
   *  @fn void set_score_threshold(const double threshold) override
   *  @brief  Set new decision threshold
   *  @param[in]  threshold   Decision threshold
   */
  void set_score_threshold(const double threshold) override {
    if (tracking_assessment_) {
      tracking_assessment_->set_score_threshold(threshold);
    } else {
      LTS5_LOG_WARNING("Tracker not initialized");
    }
  }

  /**
   * @name  get_score
   * @fn    double get_score() const override
   * @brief Provide assessment score
   * @return    Assessment score
   */
  double get_score() const override{
    if (tracking_assessment_) {
      return tracking_assessment_->get_score();
    } else {
      LTS5_LOG_WARNING("Tracker not initialized");
      return std::numeric_limits<double>::quiet_NaN();
    }
  }

  /**
   *  @name   is_tracking
   *  @fn bool is_tracking() const
   *  @brief  Give tracking status
   *  @return true if tracking false otherwise
   */
  bool is_tracking() const {
    return is_tracking_;
  }

  /**
   *  @name   set_tracker_parameters
   *  @fn void set_tracker_parameters(const SdmTrackerParameters& tracker_params)
   *  @brief  Set face tracker configuration
   *  @param[in]  tracker_params  Configuration
   */
  void set_tracker_parameters(const SdmTrackerParameters& tracker_params) {
    tracker_parameters_ = tracker_params;
  }

  /**
   *  @name   set_sift_descriptor_parameters
   *  @fn void set_sift_descriptor_parameters(const SSift::SSiftParameters& sift_params)
   *  @brief  Set SIFT configuration
   *  @param[in]  sift_params Sift descriptor configuration
   */
  void set_sift_descriptor_parameters(const SSift::SSiftParameters& sift_params) {
    sift_desciptor_params_ = sift_params;
  }

  /**
   *  @name   set_tracking_assessment
   *  @fn void set_tracking_assessment(const BaseFaceTrackerAssessment<cv::Mat,cv::Mat>* assessor)
   *  @brief  Set the type of quality assessment use by tracker
   *  @param  assessor  Quality assessor object
   */
  void set_tracking_assessment(const BaseFaceTrackerAssessment<cv::Mat,cv::Mat>* assessor) {
    tracking_assessment_ = const_cast<BaseFaceTrackerAssessment<cv::Mat,cv::Mat>*>(assessor);
    release_assessor_ = false;
  }

  #pragma mark -
  #pragma mark Training

  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder) override
   *  @brief  SDM training method
   *  @param[in]  folder  Location of images/annotations
   */
  void Train(const std::string& folder) override;

  #pragma mark -
  #pragma mark Utility

  /**
   *  @name   draw_shape
   *  @fn static cv::Mat draw_shape(const cv::Mat& img,
                                    const cv::Mat& shape,
                                    const cv::Scalar& color)
   *  @brief  Draw shape on image
   *  @param[in]  img     Input image
   *  @param[in]  shape   Shape to draw
   *  @param[in]  color   Color of the shape
   *  @return Copy of the image with drawing
   */
  static cv::Mat draw_shape(const cv::Mat& img,
                            const cv::Mat& shape,
                            const cv::Scalar& color);

  #pragma mark -
  #pragma mark Private

 private :
  /**
   *  @name   ComputeInitialShape
   *  @fn void ComputeInitialShape(const cv::Mat& meanshape,
                                   const cv::Rect& bounding_box,
                                   cv::Mat* initial_shape)
   *  @brief  Aligned the meanshape inside the bounding box
   *  @param[in]  meanshape       Model meanshape
   *  @param[in]  bounding_box    Region of interest
   *  @param[out] initial_shape   Initial shape
   */
  void ComputeInitialShape(const cv::Mat& meanshape,
                           const cv::Rect& bounding_box,
                           cv::Mat* initial_shape);

  /**
   *  @name   ComputeInitialShape
   *  @fn void ComputeInitialShape(const cv::Mat& meanshape,
                                   const cv::Rect& bounding_box,
                                   const cv::Point2d& delta,
                                   cv::Mat* initial_shape)
   *  @brief  Aligned the meanshape inside the bounding box
   *  @param[in]  meanshape       Model meanshape
   *  @param[in]  bounding_box    Region of interest
   *  @param[in]  delta           Additionnal translation
   *  @param[out] initial_shape   Initial shape
   */
  void ComputeInitialShape(const cv::Mat& meanshape,
                           const cv::Rect& bounding_box,
                           const cv::Point2d& delta,
                           cv::Mat* initial_shape);

  /**
   *  @name   AlignFaceWithScale
   *  @fn cv::Mat AlignFaceWithScale(const cv::Mat& image)
   *  @brief  Face alignement with SDM model
   *  @param[in]  image   Input image
   *  @return Return the new shape
   */
  cv::Mat AlignFaceWithScale(const cv::Mat& image);

  /**
   *  @name   AlignFaceWithScaleAndRotation
   *  @fn cv::Mat AlignFaceWithScaleAndRotation(const cv::Mat& image)
   *  @brief  Face alignement with SDM model
   *  @param[in]  image   Input image
   *  @return Return the new shape
   */
  cv::Mat AlignFaceWithScaleAndRotation(const cv::Mat& image);

  /**
   *  @name AlignMeanshapeToCurrent
   *  @fn cv::Mat AlignMeanshapeToCurrent(const cv::Mat& current_shape)
   *  @brief  Align the meanshape on the shape recovered at previous
   *          frame
   *  @param[in]  current_shape   Shape revovered at previous frame
   *  @return Aligned shape
   */
  cv::Mat AlignMeanshapeToCurrent(const cv::Mat& current_shape);
};

/**
 * @class   SDMProxy
 * @brief   Interface for registration mechanism for face tracker type
 * @author  Christophe Ecabert
 * @date    14/08/17
 * @ingroup face_tracker
 */
class SDMProxy : public TrackerProxy {
 public:

  /**
   * @name  SDMProxy
   * @fn    SDMProxy()
   * @brief Constructor
   */
  SDMProxy() = default;

  /**
   * @name  ~SDMProxy
   * @fn    ~SDMProxy()
   * @brief Destructor
   */
  ~SDMProxy() override;

  /**
   * @name  Create
   * @fn    BaseFaceTracker<cv::Mat, cv::Mat>* Create() const override
   * @brief Create an instance of BaseFaceTracker
   * @return    Tracker instance with proper type
   */
  BaseFaceTracker<cv::Mat, cv::Mat>* Create() const override;

  /**
   *  @name Name
   *  @fn const char* Name() const override
   *  @brief  Return the name for a given type of tracker
   *  @return Tracker name (i.e. sdm, lbf, ...)
   */
  const char* Name() const override;
};

}  // namespace LTS5
#endif /* __LTS5_SDM_TRACKER__ */
