/**
 *  @file   "lts5/face_tracker/mstream_sdm_tracker.hpp"
 *  @brief  Multistream face tracker interface definition
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   03/04/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __MSTREAM_FACE_TRACKER__
#define __MSTREAM_FACE_TRACKER__

#include "lts5/face_tracker/base_face_tracker.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  MStreamFaceTracker
 *  @brief  Multistream face tracker interface
 *  @author Christophe Ecabert
 *  @date   03/04/17
 *  @ingroup face_tracker
 *  @tparam ImgType Image data type
 *  @tparam ShapeType Shape data type
 */
template<typename ImgType, typename ShapeType>
class MStreamFaceTracker : public BaseFaceTracker<ImgType, ShapeType> {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  ~MStreamFaceTracker
   * @fn    ~MStreamFaceTracker(void)
   * @brief Destructor
   */
  virtual ~MStreamFaceTracker(void) {}

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Detect
   *  @fn virtual int Detect(const std::vector<ImgType>&images,
                             std::vector<ShapeType>* shapes)
   *  @brief  Dectection method, given a multiples image, provides a set of
   *          landmarks for each image. This must be override by subclass
   *  @param[in]  images   List of input images
   *  @param[out] shapes   List of set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  virtual int Detect(const std::vector<ImgType>& images,
                     std::vector<ShapeType>* shapes) = 0;

  /**
   *  @name   Detect
   *  @fn virtual int Detect(const std::vector<ImgType>& images,
                             const std::vector<cv::Rect>& face_regions,
                             std::vector<ShapeType>* shapes)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks. This must be override by subclass
   *  @param[in]  images           List of input image
   *  @param[in]  face_regions     List of face boundingg box
   *  @param[out] shapes           List of set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  virtual int Detect(const std::vector<ImgType>& images,
                     const std::vector<cv::Rect>& face_regions,
                     std::vector<ShapeType>* shapes) = 0;

  /**
   *  @name   Track
   *  @fn virtual int Track(const std::vector<ImgType>& images,
                            std::vector<ShapeType>* shapes)
   *  @brief  Tracking method, given an input image (from a sequence), provides
   *          a set of landmarks. This must be override by subclass
   *  @param[in]  images   List of input image
   *  @param[out] shapes   List of set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  virtual int Track(const std::vector<ImgType>& images,
                    std::vector<ShapeType>* shapes) = 0;
};

/**
 * @class   MTrackerProxy
 * @brief   Interface for registration mechanism for face tracker type
 * @author  Christophe Ecabert
 * @date    14/08/17
 * @ingroup face_tracker
 */
class MTrackerProxy {
 public:

  /**
   * @name  MTrackerProxy
   * @fn    MTrackerProxy(void)
   * @brief Constructor
   */
  MTrackerProxy(void);

  /**
   * @name  ~MTrackerProxy
   * @fn    ~MTrackerProxy(void)
   * @brief Destructor
   */
  virtual ~MTrackerProxy(void) = default;

  /**
   * @name  Create
   * @fn    virtual MStreamFaceTracker<cv::Mat, cv::Mat>* Create(const int N) const = 0
   * @brief Create an instance of MStreamFaceTracker
   * @param[in] N number of stream
   * @return    Tracker instance with proper type
   */
  virtual MStreamFaceTracker<cv::Mat, cv::Mat>* Create(const int N) const = 0;

  /**
   *  @name Name
   *  @fn virtual const char* Name(void) const = 0
   *  @brief  Return the name for a given type of tracker
   *  @return Tracker name (i.e. sdm, lbf, ...)
   */
  virtual const char* Name(void) const = 0;
};

}  // namepsace LTS5

#endif //__MSTREAM_FACE_TRACKER__
