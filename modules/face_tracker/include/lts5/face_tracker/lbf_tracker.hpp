/**
 *  @file   lbf_tracker.hpp
 *  @brief  LBF face tracker
 *  @ingroup face_tracker
 *
 *  @author Guillaume Jaume, Christophe Ecabert
 *  @date   27/02.2017
 *  Copyright (c) 2017 Guillaume Jaume. All rights reserved.
 */

#ifndef LTS5_LBF_TRACKER_HPP
#define LTS5_LBF_TRACKER_HPP

#include <cstdio>
#include <string>
#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/objdetect/objdetect.hpp"

#include "lts5/face_tracker/base_face_tracker.hpp"
#include "lts5/face_tracker/face_tracker_assessment.hpp"

namespace LTS5 {

/** Type forwarding */
class LBFCascade;

/**
 * @class   LBFTracker
 * @brief   Face alignment using Local Binary Feature and Regression scheme
 * @author  Guillaume Jaume, Christophe Ecabert
 * @date    13/03/2017
 * @ingroup face_tracker
 */
class LTS5_EXPORTS LBFTracker : public BaseFaceTracker<cv::Mat, cv::Mat> {
 public :

#pragma mark -
#pragma mark Initialization

  /**
  *  @name   LbfTrainer
  *  @fn LBFTracker()
  *  @brief  Constructor
  */
  LBFTracker();

  /**
  *  @name   ~LBFTracker
  *  @brief  Destructor
  */
  ~LBFTracker() override;


  /**
   * @name Load
   * @brief read the model for tracking
   * @param model_filename  Path to LBF model
   * @return error
   */
  int Load(const std::string &model_filename) override;

  /**
   *  @name   Load
   *  @fn int Load(const std::string &model_filename,
                    const std::string &face_detector_config) override
   *  @brief  Load the tracker model.
   *  @param[in]  model_filename        Path to LBF model
   *  @param[in]  face_detector_config  Path to face detector model
   *  @return -1 if errors, 0 otherwise
  */
  int Load(const std::string &model_filename,
           const std::string &face_detector_config) override;

  /**
   * @name Load
   * @brief read the model for tracking
   * @param stream  Binary stream pointing to model
   * @return error
   */
  int Load(std::istream& stream);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& model_name) override
   *  @brief  Save the trained model in file.
   *  @param[in] model_name   Name of the file
   *  @return -1 in case of error, otherwise 0
  */
  int Save(const std::string &model_name) override;

  /**
   *  @name   Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Write the object into a binary stream
   *  @param[in]  out_stream  Binary stream to files
   *  @return -1 if error, 0 otherwise.
   */
  int Write(std::ostream &out_stream) const;

  /**
   *  @name   ComputeObjectSize
   *  @fn int ComputeObjectSize() const
   *  @brief  Compute the memory used by object
   *  @return Size in bytes
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Detect
   *  @fn     int Detect(const cv::Mat& image, cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Detect(const cv::Mat &image, cv::Mat *shape) override;

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image, const cv::Rect& face_region,
                             cv::Mat* shape) override
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face bounding box
   *  @param[out] shape           Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Detect(const cv::Mat &image,
          const cv::Rect &face_region,
          cv::Mat *shape) override;


  /**
   *  @name   Track
   *  @fn int Track(const cv::Mat& image, const cv::Rect& face_region,
   *                cv::Mat* shape)
   *  @brief  Tracking method, given an input image (from a sequence), provides
   *          a set of landmarks.
   *  @param[in]  image   Input image
   *  @param[in] face_region    Location of the face
   *  @param[out] shape   Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Track(const cv::Mat &image, const cv::Rect &face_region, cv::Mat *shape);


  /**
   *  @name   Track
   *  @fn int Track(const cv::Mat& image, cv::Mat* shape) override
   *  @brief  Tracking method, given an input image (from a sequence), provides
   *          a set of landmarks.
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Track(const cv::Mat &image, cv::Mat *shape) override;


  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder) override
   *  @brief Not implemented in tracker
   *  @param[in]  folder  Location of images/annotations
   */
  void Train(const std::string &folder) override;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name get_score_threshold
   *  @fn double get_score_threshold() const override
   *  @brief  Get the actual decision threshold
   *  @return Decision threshold
   */
  double get_score_threshold() const override {
    if (tracking_assessment_) {
      return tracking_assessment_->get_score_threshold();
    } else {
      LTS5_LOG_WARNING("Tracker not initialized");
      return std::numeric_limits<double>::quiet_NaN();
    }
  }

  /**
   *  @name set_score_threshold
   *  @fn void set_score_threshold(const double threshold) override
   *  @brief  Set new decision threshold
   *  @param[in]  threshold   Decision threshold
   */
  void set_score_threshold(const double threshold) override {
    if (tracking_assessment_) {
      tracking_assessment_->set_score_threshold(threshold);
    } else {
      LTS5_LOG_WARNING("Tracker not initialized");
    }
  }

  /**
   * @name  get_score
   * @fn    double get_score() const override
   * @brief Provide assessment score
   * @return    Assessment score
   */
  double get_score() const override {
    if (tracking_assessment_) {
    return tracking_assessment_->get_score();
    } else {
      LTS5_LOG_WARNING("Tracker not initialized");
      return std::numeric_limits<double>::quiet_NaN();
    }
  }

#pragma mark -
#pragma mark Utility

  /**
   *  @name   draw_shape
   *  @fn static cv::Mat draw_shape(const cv::Mat& img,
                                    const cv::Mat& shape,
                                    const cv::Scalar& color)
   *  @brief  Draw shape on image
   *  @param[in]  img     Input image
   *  @param[in]  shape   Shape to draw
   *  @param[in]  color   Color of the shape
   *  @return Copy of the image with drawing
   */
  static cv::Mat draw_shape(const cv::Mat &img,
                            const cv::Mat &shape,
                            const cv::Scalar &color);

  /**
   *  @struct SortRectBySize
   *  @brief  Used to order rectangle by size (decreasing)
   *  ie. align the larger face
   */
  struct SortRectBySize {
    inline bool operator()(const cv::Rect &lhs, const cv::Rect &rhs) {
      return lhs.area() > rhs.area();
    }
  };

#pragma mark -
#pragma mark Private

 private:

  /**
   *  @name   ComputeInitialShape
   *  @brief  Aligned the meanshape inside the bounding box
   *  @param[in]  meanshape       Model meanshape
   *  @param[in]  bounding_box    Region of interest
   *  @param[out] shape           Initial shape
   */
  void ComputeInitialShape(const cv::Mat &meanshape,
                           const cv::Rect &bbox,
                           cv::Mat *shape);

  /**
   * @name  AlignMeanshapeToCurrent
   * @fn    void AlignMeanshapeToCurrent(const cv::Mat& current_shape,
                               const cv::Mat& meanshape,
                               cv::Mat* shape)
   * @brief Place the \p meanshape at the position of \p current_shape
   * @param[in] current_shape   Current shape
   * @param[in] meanshape       Meanshape
   * @param[out] shape          New shape (transformed meanshape)
   */
  void AlignMeanshapeToCurrent(const cv::Mat& current_shape,
                               const cv::Mat& meanshape,
                               cv::Mat* shape);

  /** lbf_cascador to Predict shape */
  LBFCascade *lbf_cascade_;
  /** Viola-Jones face detector */
  cv::CascadeClassifier *face_detector_;
  /** am I currently tracking ? */
  bool is_tracking_;
  /** Tracking quality assessment */
  LTS5::BaseFaceTrackerAssessment<cv::Mat, cv::Mat>* tracking_assessment_;
  /** Meanshape */
  cv::Mat meanshape_;
  /** initial shape calculated from previous frame */
  //cv::Mat initial_shape_;
  /** previous_frame used to estimate the new one */
  cv::Mat previous_shape_;
  /** Working image - grayscale */
  cv::Mat working_img_;
  /** Shape's center of gravity */
  cv::Point2f shape_center_gravity_;
};

/**
 * @class   LBFProxy
 * @brief   Interface for registration mechanism for face tracker type
 * @author  Christophe Ecabert
 * @date    14/08/17
 * @ingroup face_tracker
 */
class LBFProxy : public TrackerProxy {
 public:

  /**
   * @name  LBFProxy
   * @fn    LBFProxy()
   * @brief Constructor
   */
  LBFProxy() = default;

  /**
   * @name  ~LBFProxy
   * @fn    ~LBFProxy()
   * @brief Destructor
   */
  ~LBFProxy() override;

  /**
   * @name  Create
   * @fn    BaseFaceTracker<cv::Mat, cv::Mat>* Create() const override
   * @brief Create an instance of BaseFaceTracker
   * @return    Tracker instance with proper type
   */
  BaseFaceTracker<cv::Mat, cv::Mat>* Create() const override;

  /**
   *  @name Name
   *  @fn const char* Name() const override
   *  @brief  Return the name for a given type of tracker
   *  @return Tracker name (i.e. sdm, lbf, ...)
   */
  const char* Name() const override;
};

}  // namespace LTS5
#endif //LTS5_LBF_TRACKER_HPP
