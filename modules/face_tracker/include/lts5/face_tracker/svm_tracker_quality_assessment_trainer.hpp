/**
 *  @file   svm_tracker_quality_assessment_trainer.hpp
 *  @brief  SVM Quality assessment trainer
 *  @ingroup face_tracker
 *
 *  @author Gabriel Cuendet
 *  @date   02/06/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_SVM_TRACKER_ASSESSMENT_TRAINER__
#define __LTS5_SVM_TRACKER_ASSESSMENT_TRAINER__

#include <vector>
#include <string>

#include "opencv2/core/core.hpp"

#include "lts5/classifier/binarylinearsvmclassifier.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/base_tracker_quality_assessment_trainer.hpp"
#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  SvmTrackerQualityAssessmentTrainer
 *  @brief  SVM Quality Assessment trainer implementation
 *  @author Gabriel Cuendet
 *  @date   03/06/15
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS SvmTrackerQualityAssessmentTrainer :
public BaseTrackerQualityAssessmentTrainer {
 public :

  /** MultiArray similar to boost::multi_array<T> */
  template<typename T>
  using MultiArray = std::vector<std::vector<T>>;
  
#pragma mark -
#pragma mark Constructors and Destructors
  /**
   *  @name   SvmTrackerQualityAssessmentTrainer
   *  @fn SvmTrackerQualityAssessmentTrainer();
   *  @brief  Constructor
   */
  SvmTrackerQualityAssessmentTrainer();

  /**
   *  @name   SvmTrackerQualityAssessmentTrainer
   *  @fn SvmTrackerQualityAssessmentTrainer(const LTS5::SdmTracker::SdmTrackerParameters& tracker_parameters,
                                             const std::vector<std::string>& images_files,
                                             const std::vector<bool>& all_flip,
                                             const std::vector<cv::Mat>& all_annotations,
                                             const cv::Mat& mean_shape,
                                             const std::vector<cv::Rect>& faces_rectangles,
                                             const cv::Vec3d& detector_stds,
                                             const std::vector<cv::Size>& images_sizes)
   *  @brief  Constructor
   *  @param[in]  tracker_parameters  Tracker parameters
   *  @param[in]  images_files        Images path
   *  @param[in]  all_flip            Flag to indicate flip
   *  @param[in]  all_annotations     Image annotations
   *  @param[in]  mean_shape          Meanshape
   *  @param[in]  faces_rectangles    Detection rectangles
   *  @param[in]  detector_stds       Detector stdev
   *  @param[in]  images_sizes        Image sizes
   */
  SvmTrackerQualityAssessmentTrainer(const LTS5::SdmTracker::SdmTrackerParameters& tracker_parameters,
                                     const std::vector<std::string>& images_files,
                                     const std::vector<bool>& all_flip,
                                     const std::vector<cv::Mat>& all_annotations,
                                     const cv::Mat& mean_shape,
                                     const std::vector<cv::Rect>& faces_rectangles,
                                     const cv::Vec3d& detector_stds,
                                     const std::vector<cv::Size>& images_sizes);

  /**
   *  @name   ~SvmTrackerQualityAssessmentTrainer
   *  @fn ~SvmTrackerQualityAssessmentTrainer() override
   *  @brief  Destructor
   */
  ~SvmTrackerQualityAssessmentTrainer() override;

#pragma mark -
#pragma mark Train and Save methods
  /**
   *  @name   Save
   *  @fn int Save(const std::string& model_name) override
   *  @brief  Save the trained model in file.
   *  @param  [in]    model_name      Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  int Save(const std::string& model_name) override;

  /**
   *  @name   Save
   *  @fn int Save(std::ostream& output_stream) override
   *  @brief  Write the assessment model to file stream.
   *  @param[in]  output_stream  File stream for writing the model
   *  @return -1 if errors, 0 otherwise
   */
  int Save(std::ostream& output_stream) override;

  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder)
   *  @brief  SVM quality checker training method
   *  @param[in]  folder  Location of images/annotations
   */
  void Train(const std::string& folder);

  /**
   *  @name   Train
   *  @fn void Train() override
   *  @brief  SVM  quality checker training method
   */
  void Train() override;

 private:

  /**
   *  @name   ExtractPositiveFeatures
   *  @fn void ExtractPositiveFeatures(cv::Mat* features, cv::Mat* labels)
   *  @brief  Extract SIFT features for positive examples (the annotations)
   *  @param[out]  features  Matrix holding the computed features
   *  @param[out]  labels    Vector of positive labels (1.0)
   */
  void ExtractPositiveFeatures(cv::Mat* features, cv::Mat* labels);

  /**
   *  @name   ExtractNegativeFeatures
   *  @fn void ExtractNegativeFeatures(const MultiArray<cv::Rect>& montecarlo_rects,
                                       cv::Mat* features, cv::Mat* labels)
   *  @brief  Extract SIFT features for negative examples (the perturbed mean
   *          shapes)
   *  @param[in] montecarlo_rects  Perturbed detector rectangles
   *  @param[out]  features  Matrix holding the computed features
   *  @param[out]  labels    Vector of negative labels (1.0)
   */
  void ExtractNegativeFeatures(const MultiArray<cv::Rect>& montecarlo_rects,
                               cv::Mat* features, cv::Mat* labels);

  /**
   *  @name   EstimteParametersLinear
   *  @fn float EstimateParametersLinear(double* best_c)
   *  @param[out] best_c  Best C coefficient for linear SVM
   */
  float EstimateParametersLinear(double* best_c);

  /** Tracker parameters */
  LTS5::SdmTracker::SdmTrackerParameters tracker_parameters_;
  /** Images files */
  std::vector<std::string> images_files_;
  /** Whether or not to flip the images */
  std::vector<bool> all_flip_;
  /** Annotations */
  std::vector<cv::Mat> all_annotations_;
  /** Mean shape of the annotations */
  cv::Mat mean_shape_;
  /** Rectangles returned by the face detector */
  std::vector<cv::Rect> faces_rectangles_;
  /** Face detector standard deviations (x, y, scale) */
  cv::Vec3d detector_stds_;
  /** Sizes of all the images */
  std::vector<cv::Size> images_sizes_;
  /** Positive samples */
  std::vector<cv::Mat> positive_samples_;
  /** Negative examples */
  std::vector<cv::Mat> negative_samples_;
  /** SVM type */
  BinaryLinearSVMTraining svm_type_;
  /** Binary linear SVM classifier  */
  BinaryLinearSVMClassifier* svm_classifier_;
  /** Linear svm model for face tracker assessment */
  cv::Mat svm_model_w_;
  /** Threshold of the classifier */
  double score_threshold_;
  /** PCA eigenvectors computed from SIFT features */
  cv::Mat pca_basis_;
};
}  // namespace LTS5

#endif /* __LTS5_SVM_TRACKER_ASSESSMENT_TRAINER__ */
