/**
 *  @file   lbf_tracker_pimpl.hpp
 *  @brief  PIMPL definition for LBFTracker class
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   21/03/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LBF_TRACKER_PIMPL__
#define __LBF_TRACKER_PIMPL__

#include <iostream>
#include <vector>

#include "opencv2/core.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark RandomTree (Hidden class- PIMPL)

/**
 * @class   RandomTree
 * @brief   Random Tree training class to learn "Local Binary Feature"
 * @author  Guillaume Jaume, Christophe Ecabert
 * @date    13/03/2017
 * @ingroup face_tracker
 */
class RandomTree {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  RandomTree
   * @fn    RandomTree() = default;
   * @brief Constructor
   */
  RandomTree() = default;

  /**
   * @name  ~RandomTree
   * @fn    ~RandomTree() = default;
   * @brief Destructor
   */
  ~RandomTree() = default;

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream)
   * @brief Load a random tree from a given stream
   * @param[in] stream  Binary stream from where tree is loaded
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Write
   * @fn    int Write(std::ostream& stream)
   * @brief Write this random tree into a given stream
   * @param[in] stream  Stream where to ouput the object
   * @return    -1 if error, 0 otherwise
   */
  int Write(std::ostream& stream) const;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize() const
   * @brief Compute memory need for this RandomTree
   * @return    Object size in byte
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Usage

  /**
   * @name  get_feats
   * @return Sampling location
   */
  const cv::Mat& get_feats() const {
    return feats_;
  }

  /**
   * @name  get_threshold
   * @return Threshold
   */
  const std::vector<int>& get_threshold() const {
    return thresholds_;
  }

#pragma mark -
#pragma mark Private

 private:
  /** landmark corresponding to current tree */
  int landmark_id_;
  /** Tree depth */
  int tree_depth_;
  /** matrix of features (px to take with respect to each landmark)*/
  cv::Mat feats_;
  /**  vector of threshold for each node  */
  std::vector<int> thresholds_;
};

#pragma mark -
#pragma mark RandomForest (Hidden class- PIMPL)


/**
 * @class   RandomForest
 * @brief   Random Forest training class to learn "Local Binary Feature"
 * @author  Guillaume Jaume, Christophe Ecabert
 * @date    13/03/2017
 * @ingroup face_tracker
 */
class RandomForest {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name RandomForest
   * @fn RandomForest() = default
   * @brief constructor
   */
  RandomForest() = default;

  /**
   * @name ~RandomForest
   * @fn ~RandomForest() = default
   * @brief Destructor
   */
  ~RandomForest() = default;

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream)
   * @brief Init random forest from file
   * @param[in] stream  Binary stream to load from
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream);

#pragma mark -
#pragma mark Usage

  /**
   * @name  ComputeLBF
   * @brief Compute feature for a given shape/image
   * @param[in] image           Input image
   * @param[in] current_shape   Current shape
   * @param[in] transform       Transformation from current_shape to mean-shape
   * @param[in] inner_shape     Flag to indicate if we compute only for
   *                            the innershape
   * @param[out] lbf            Computed features
   */
  void ComputeLBF(const cv::Mat& image,
                  const cv::Mat& current_shape,
                  const cv::Mat& transform,
                  const bool inner_shape,
                  cv::SparseMat* lbf);

  /**
   * @name
   * @brief Dump into a given stream
   * @param[in] stream  Stream where to dump data
   * @return    -1 if error, 0 otherwise
   */
  int Write(std::ostream& stream) const;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize() const
   * @brief Compute memory need for this RandomForest
   * @return    Object size in byte
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Private
 private:
  /** number of landmark per image*/
  int n_landmark_;
  /** Number of tree */
  int n_tree_;
  /** Tree depth */
  int tree_depth_;
  /** vector of tree (to create a forest) */
  std::vector<std::vector<RandomTree>> random_trees_;
};

#pragma mark -
#pragma mark LBFCascade - (Hidden class- PIMPL)

/**
 * @class   LBFCascade
 * @brief   Training class to learn feature descriptor + global shape regressor
 * @author  Guillaume Jaume, Christophe Ecabert
 * @date    13/03/2017
 * @ingroup face_tracker
 */
class LBFCascade {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name LBFCascade()
   * @brief Constructor
   */
  LBFCascade();

  /**
   * @name ~LBFCascade()
   * @brief Destructor
   */
  ~LBFCascade();

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream)
   * @brief Init LBFCascade from file
   * @param[in] stream  Binary stream to load from
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Predict
   * @fn    void Predict(const cv::Mat& image, cv::Mat* shape)
   * @brief Predict the shape given an image
   * @param[in] image           Image where to perform alignment
   * @param[in] meanshape       Meanshape
   * @param[in,out] shape       Updated Shape from alignment
   */
  void Predict(const cv::Mat& image,
               const cv::Mat& meanshape,
               cv::Mat* shape);

  /**
   * @name  Write
   * @brief Dump into a given stream
   * @param[in] stream  Stream where to dump data
   * @return    -1 if error, 0 otherwise
   */
  int Write(std::ostream& stream) const;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize() const
   * @brief Compute memory need for this LBFCascade
   * @return    Object size in byte
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Private
 private:

  /** number of stages (increments delta_shape) */
  int n_stages_;
  /** number of landmarks per image */
  int n_landmark_;
  /** number of tree */
  int n_tree_;
  /** Tree depth */
  int tree_depth_;
  /** vector of pointer on RandomForest */
  std::vector<RandomForest*> random_forests_;
  /** vector of regression for each stage */
  std::vector<cv::Mat> gl_regression_weights_;

  /**
   * @name  ComputeAndUpdateShape
   * @fn    void ComputeAndUpdateShape(const int stage,
                                       const cv::Mat& transform,
                                       const cv::SparseMat& lbf_feature,
                                       cv::Mat* shape)
   * @brief Compute shape update with learned regression and update a given
   *        shape
   * @param[in] stage           Current stage
   * @param[in] transform       Transform from mean to shape
   * @param[in] lbf_feature     Feature to use to predict update
   * @param[in,out] shape       Shape to be updated with delta predicted
   */
  void ComputeAndUpdateShape(const int stage,
                             const cv::Mat& transform,
                             const cv::SparseMat& lbf_feature,
                             cv::Mat* shape);
};


}  // namepsace LTS5
#endif //__LBF_TRACKER_PIMPL__
