/**
 *  @file   tracker_factory.hpp
 *  @brief  Instantiate face tracker for a given type
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   14/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TRACKER_FACTORY__
#define __LTS5_TRACKER_FACTORY__

#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/face_tracker/base_face_tracker.hpp"
#include "lts5/face_tracker/mstream_face_tracker.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   TrackerFactory
 * @brief   Instantiate face tracker for a given type/name
 * @author  Christophe Ecabert
 * @date    14/08/2017
 * @ingroup face_tracker
 */
class LTS5_EXPORTS TrackerFactory {
 public:

  /** Tracker type */
  using Tracker = BaseFaceTracker<cv::Mat, cv::Mat>;

  /**
   * @name  Get
   * @fn    static TrackerFactory& Get(void)
   * @brief Provide single instance of factory
   * @return    Factory instance
   */
  static TrackerFactory& Get(void);

  /**
   * @name  ~TrackerFactory
   * @fn    ~TrackerFactory(void)
   * @brief Destructor
   */
  ~TrackerFactory(void) = default;

  /**
   * @name  TrackerFactory
   * @fn    TrackerFactory(const TrackerFactory& other) = delete
   * @brief Copy constructor
   * @param[in] other   Objec to copy from
   */
  TrackerFactory(const TrackerFactory& other) = delete;

  /**
   *  @name operator=
   *  @fn TrackerFactory& operator=(const TrackerFactory& rhs) = delete
   *  @brief  Assignment operator
   *  @param[in]  rhs Object to assign from
   *  @return Newly assigned operator
   */
  TrackerFactory& operator=(const TrackerFactory& rhs) = delete;

  /**
   *  @name CreateByName
   *  @fn Tracker* CreateByName(const std::string& name)
   *  @brief  Create a tracker based on the name
   *  @param[in]  name  Tracker name
   *  @return Pointer to an instance of the given type, nullptr if type is
   *  unknown
   */
  Tracker* CreateByName(const std::string& name);

  /**
   *  @name Register
   *  @fn void Register(const TrackerProxy* object)
   *  @brief  Register a type of tracker with a given proxy.
   *  @param[in]  object  Object to register
   */
  void Register(const TrackerProxy* object);

 private:

  /**
   * @name  TrackerFactory
   * @fn    TrackerFactory(void)
   * @brief Constructor
   */
  TrackerFactory(void) = default;

  /** List of registered proxy */
  std::vector<const TrackerProxy*> proxies_;
};

/**
 * @class   MTrackerFactory
 * @brief   Instantiate face tracker for a given type/name
 * @author  Christophe Ecabert
 * @date    14/08/2017
 * @ingroup face_tracker
 */
class LTS5_EXPORTS MTrackerFactory {
 public:

  /** Tracker type */
  using Tracker = MStreamFaceTracker<cv::Mat, cv::Mat>;

  /**
   * @name  Get
   * @fn    static MTrackerFactory& Get(void)
   * @brief Provide single instance of factory
   * @return    Factory instance
   */
  static MTrackerFactory& Get(void);

  /**
   * @name  ~MTrackerFactory
   * @fn    ~MTrackerFactory(void)
   * @brief Destructor
   */
  ~MTrackerFactory(void) = default;

  /**
   * @name  MTrackerFactory
   * @fn    MTrackerFactory(const MTrackerFactory& other) = delete
   * @brief Copy constructor
   * @param[in] other   Objec to copy from
   */
  MTrackerFactory(const MTrackerFactory& other) = delete;

  /**
   *  @name operator=
   *  @fn MTrackerFactory& operator=(const MTrackerFactory& rhs) = delete
   *  @brief  Assignment operator
   *  @param[in]  rhs Object to assign from
   *  @return Newly assigned operator
   */
  MTrackerFactory& operator=(const MTrackerFactory& rhs) = delete;

  /**
   *  @name CreateByName
   *  @fn Tracker* CreateByName(const std::string& extension)
   *  @brief  Create a tracker based on the name
   *  @param[in] N      Number of stream needed
   *  @param[in]  name  Tracker name
   *  @return Pointer to an instance of the given type, nullptr if type is
   *  unknown
   */
  Tracker* CreateByName(const size_t N, const std::string& name);

  /**
   *  @name Register
   *  @fn void Register(const MTrackerProxy* object)
   *  @brief  Register a type of tracker with a given proxy.
   *  @param[in]  object  Object to register
   */
  void Register(const MTrackerProxy* object);

 private:

  /**
   * @name  MTrackerFactory
   * @fn    MTrackerFactory(void)
   * @brief Constructor
   */
  MTrackerFactory(void) = default;

  /** List of registered proxy */
  std::vector<const MTrackerProxy*> proxies_;
};

}  // namepsace LTS5
#endif //__LTS5_TRACKER_FACTORY__
