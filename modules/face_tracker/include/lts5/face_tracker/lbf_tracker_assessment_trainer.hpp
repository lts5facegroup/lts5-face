/**
 *  @file   lbf_tracker_assessment_trainer.hpp
 *  @brief  Tracker quality assessment trainer for LBF tracker
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   20/03/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LBF_TRACKER_ASSESSMENT_TRAINER__
#define __LBF_TRACKER_ASSESSMENT_TRAINER__

#include "lts5/utils/library_export.hpp"
#include "lts5/face_tracker/base_tracker_quality_assessment_trainer.hpp"
#include "lts5/classifier/binary_sparse_linear_svm_classifier.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Forwarding

/** RandomForest train */
class RandomForestTrainer;
/** GroundTruth */
struct GroundTruth;

/**
 * @class   LBFTrackerAssessmentTrainer
 * @brief   Tracker quality assessment for LBFTracker
 * @author  Christophe Ecabert
 * @date    20/03/2017
 * @ingroup face_tracker
 */
class LTS5_EXPORTS LBFTrackerAssessmentTrainer : public BaseTrackerQualityAssessmentTrainer {
 public:

#pragma mark -
#pragma mark Type definition

#pragma mark -
#pragma mark Initialization

  /**
   * @name  LBFTrackerAssessmentTrainer
   * @fn    LBFTrackerAssessmentTrainer(void)
   * @brief Constructor
   * @param[in] gtruth          List of ground truth
   * @param[in] tracker_shape   List of shape outputed by LBF tracker
   * @param[in] descriptor      Festure extractor
   * @param[in] meanshape       Meanshape
   * @param[in] stdev           Face detector statistics
   * @param[in] stdev_crossing  Where std should cross (1, 2, 3 time std)
   */
  LBFTrackerAssessmentTrainer(const std::vector<GroundTruth>& gtruth,
                              const std::vector<cv::Mat>& tracker_shape,
                              const RandomForestTrainer& descriptor,
                              const cv::Mat& meanshape,
                              const cv::Vec3d& stdev,
                              const double stdev_crossing);

  /**
   * @name  ~LBFTrackerAssessmentTrainer
   * @fn    ~LBFTrackerAssessmentTrainer(void)
   * @brief Destructor
   */
  ~LBFTrackerAssessmentTrainer(void);

  /**
   *  @name   Save
   *  @fn  int Save(const std::string& model_name)
   *  @brief  Save the trained model in file. This must be override by
   *          subclass
   *  @param  [in]    model_name      Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  int Save(const std::string& model_name);

  /**
   *  @name   Save
   *  @fn int Save(std::ostream& output_stream)
   *  @brief  Write the assessment model to file stream. This must be
   *          override by subclass
   *  @param[in]  output_stream  File stream for writing the model
   *  @return -1 if errors, 0 otherwise
   */
  int Save(std::ostream& output_stream);

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Train
   *  @fn void Train(void)
   *  @brief  SDM training method
   */
  void Train(void);

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  GeneratePerturbation
   * @fn    void GeneratePerturbation(void)
   * @brief Generate perturbation of the annotation based on face detector
   *        statistics.
   */
  void GeneratePerturbation(void);

  /**
   * @name  ExtractFeatures
   * @fn    void ExtractFeatures(std::vector<cv::SparseMat>* pos_feature,
                       std::vector<cv::SparseMat>* neg_feature)
   * @brief Extract LBF features
   * @param[out] pos_feature positive extracted features
   * @param[out] neg_feature negative extracted features
   */
  void ExtractFeatures(std::vector<cv::SparseMat>* pos_feature,
                       std::vector<cv::SparseMat>* neg_feature);

  /**
   * @name  TrainClassifier
   * @fn    void TrainClassifier(const std::vector<cv::SparseMat>& pos_feature,
                                 const std::vector<cv::SparseMat>& neg_feature)
   * @brief Train classifier
   * @param[in] pos_feature positive extracted features
   * @param[in] neg_feature negative extracted features
   */
  void TrainClassifier(const std::vector<cv::SparseMat>& pos_feature,
                       const std::vector<cv::SparseMat>& neg_feature);

  /**
   * @name  TestClassifier
   * @fn    void TestClassifier(const std::vector<cv::SparseMat>& pos_feature,
                                const std::vector<cv::SparseMat>& neg_feature)
   * @brief Test classifier on training set
   * @param[in] pos_feature positive extracted features
   * @param[in] neg_feature negative extracted features
   */
  void TestClassifier(const std::vector<cv::SparseMat>& pos_feature,
                      const std::vector<cv::SparseMat>& neg_feature);



  /** Classifier */
  BinarySparseLinearSVMClassifier* classifier_;
  /** Feature descriptor */
  RandomForestTrainer* descriptor_;
  /** Ground truth */
  std::vector<GroundTruth>* gtruth_;
  /** Tracker shape */
  std::vector<cv::Mat>* tracker_shapes_;
  /** Shape */
  std::vector<cv::Mat> shapes_;
  /** Meanshape */
  cv::Mat meanshape_;
  /** Standard dev */
  cv::Vec3d stdev_;
  /** Sigma crossing */
  double crossing_;
};

}  // namepsace LTS5


#endif //__LBF_TRACKER_ASSESSMENT_TRAINER__
