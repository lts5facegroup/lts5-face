/**
 *  @file   "lts5/face_tracker/svm_tracker_assessment.hpp"
 *  @brief  face tracker assessment based on svm classifier
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   02/06/15
*  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_SVM_TRACKER_ASSESSMENT__
#define __LTS5_SVM_TRACKER_ASSESSMENT__

#include <string>

#include "lts5/face_tracker/face_tracker_assessment.hpp"
#include "lts5/utils/library_export.hpp"
#include "lts5/utils/ssift.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  SvmTrackerAssessment
 *  @brief  Face tracker quality assessment
 *  @author Christophe Ecabert
 *  @date   02/06/15
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS SvmTrackerAssessment : public BaseFaceTrackerAssessment<cv::Mat,cv::Mat> {
 public :

#pragma mark -
#pragma mark Initialization
  /**
   *  @name   SvmTrackerAssessment
   *  @fn SvmTrackerAssessment(const cv::Mat& meanshape,
                               const LTS5::SSift::SSiftParameters& sift_parameters,
                               const int edge,
                               const int sift_size)
   *  @brief  Constructor
   *  @param[in]    meanshape       Face model meanshape
   *  @param[in]    sift_parameters Sift descriptor parameters
   *  @param[in]    edge            Normalized image size.
   *  @param[in]    sift_size       Sift descriptor dimension
   */
  SvmTrackerAssessment(const cv::Mat& meanshape,
                       const LTS5::SSift::SSiftParameters& sift_parameters,
                       const int edge,
                       const int sift_size);

  /**
   *  @name   ~SvmTrackerAssessment
   *  @fn ~SvmTrackerAssessment(void)
   *  @brief  Destructor
   */
  ~SvmTrackerAssessment(void);

  /**
   *  @name   Load
   *  @brief  Load the assessment model from file
   *  @fn int Load(const std::string& model_name)
   *  @param[in]  model_name      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_name);

  /**
   *  @name   Load
   *  @fn int Load(std::istream& input_stream, const bool& is_binary)
   *  @brief  Load the assessment model from file stream
   *  @param[in]  input_stream  File stream for reading the model
   *  @param[in]  is_binary     Indicate if stream is open has binary
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& input_stream, const bool& is_binary);

  /**
   *  @name   Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Copy the object into a binary stream
   *  @param[in]  out_stream  Output binary stream
   *  @return -1 if error, 0 otherwise
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the object size in bytes
   *  @return Object memory size
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Assess
   *  @fn double Assess(const cv::Mat& image, const cv::Mat& shape)
   *  @brief  Assessment method, given the current shape and image
   *  @param[in]  image   Input image
   *  @param[in]  shape     Current shape tracked by the face tracker
   *  @return   scalar score, the larger the better
   */
  double Assess(const cv::Mat& image, const cv::Mat& shape);

#pragma mark -
#pragma mark Accessor

  /**
   *  @name   set_score_threshold
   *  @fn void set_score_threshold(const double threshold)
   *  @brief  Set classification threshold
   *  @param[in]  threshold Classification threshold
   */
  void set_score_threshold(const double threshold) {
    score_threshold_ = threshold;
  }

  /**
   *  @name get_score_threshold
   *  @fn virtual double get_score_threshold(void) const = 0
   *  @brief  Get the current decision threshold
   *  @return  Current decision threshold
   */
  double get_score_threshold(void) const {
    return score_threshold_;
  }

 private :

  /**
   * @name  ExtractFeature
   * @fn    void ExtractFeature(const cv::Mat& image, const cv::Mat& shape)
   * @brief Extract sift feature at specific location
   * @param[in] image   Input image
   * @param[in] shape   Current position of tracked landmarks
   */
  void ExtractFeature(const cv::Mat& image, const cv::Mat& shape);

  /**  linear svm model for face tracker assessment */
  cv::Mat svm_model_w_;
  /** Decision threshold */
  double score_threshold_ = -0.3;
  /** Reference shape */
  cv::Mat reference_shape_;
  /** Image size */
  int edge_;
  /** SSift size */
  int sift_size_;
  /** SSift configuration */
  SSift::SSiftParameters sift_parameters_;
  /** Features */
  cv::Mat features_;
};
}  // namespace LTS5

#endif /* __LTS5_SVM_TRACKER_ASSESSMENT__ */
