/**
 *  @file   const_tracker_quality_assessment_trainer.hpp
 *  @brief  Face tracker CONST quality assessment trainer class definition
 *  @ingroup face_tracker
 *
 *  @author Gabriel Cuendet
 *  @date   09/06/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_CONST_TRACKER_ASSESSMENT_TRAINER__
#define __LTS5_CONST_TRACKER_ASSESSMENT_TRAINER__

#include <stdio.h>
#include <string>

#include "lts5/face_tracker/base_tracker_quality_assessment_trainer.hpp"
#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  ConstTrackerQualityAssessmentTrainer
 *  @brief  CONST Quality Assessment trainer implementation
 *  @author Gabriel Cuendet
 *  @date   09/06/15
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS ConstTrackerQualityAssessmentTrainer :
public BaseTrackerQualityAssessmentTrainer {
 public :
#pragma mark -
#pragma mark Constructors and Destructors
  /**
   *  @name   ConstTrackerQualityAssessmentTrainer
   *  @fn ConstTrackerQualityAssessmentTrainer(void)
   *  @brief  Constructor
   */
  ConstTrackerQualityAssessmentTrainer(void) : detect_(false) {}

  /**
   *  @name   ConstTrackerQualityAssessmentTrainer
   *  @fn ConstTrackerQualityAssessmentTrainer(bool detect)
   *  @brief  Constructor
   *  @param[in] detect  Wheter or not to always reset the tracker
   */
  ConstTrackerQualityAssessmentTrainer(bool detect) : detect_(detect) {}

  /**
   *  @name   ~ConstTrackerQualityAssessmentTrainer
   *  @fn ~ConstTrackerQualityAssessmentTrainer()
   *  @brief  Destructor
   */
  ~ConstTrackerQualityAssessmentTrainer() {}

  /**
   *  @name   Save
   *  @brief  Save the trained model in file.
   *  @fn int Save(const std::string& model_name)
   *  @param  [in]    model_name      Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  int Save(const std::string& model_name);

  /**
   *  @name   Save
   *  @fn int Save(std::ostream& output_stream)
   *  @brief  Write the assessment model to file stream.
   *  @param[in]  output_stream  File stream for writing the model
   *  @return -1 if errors, 0 otherwise
   */
  int Save(std::ostream& output_stream);

  /**
   *  @name   Train
   *  @fn void Train(void)
   *  @brief  Does not do anything, but need to be declared (virtual in base class)
   */
  void Train(void) {}

 private :
  /** <  State indicating whether or not it will indicate to reset tracker */
  bool detect_;
};
}  // namespace LTS5

#endif /* __LTS5_CONST_TRACKER_ASSESSMENT_TRAINER__ */
