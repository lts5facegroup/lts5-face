/**
 *  @file   lts5/face_tracker/face_tracker.hpp
 *  @brief  Face tracker module
 *
 *  @author Christophe Ecabert
 *  @date   22/04/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_FACE_TRACKER__
#define __LTS5_FACE_TRACKER__

/** Base class for face tracker */
#include "lts5/face_tracker/base_face_tracker.hpp"
/** SDM face tracker */
#include "lts5/face_tracker/sdm_tracker.hpp"
/** Point distribution model */
#include "lts5/face_tracker/point_distribution_model.hpp"
/** Base class for face tracker quality assessment */
#include "lts5/face_tracker/face_tracker_assessment.hpp"
/** Face tracker const quality assessment */
#include "lts5/face_tracker/const_tracker_assessment.hpp"
/** Face tracker SVM quality assessment */
#include "lts5/face_tracker/svm_tracker_assessment.hpp"
/** Face tracker CPR */
#include "lts5/face_tracker/cpr_tracker.hpp"

#endif
