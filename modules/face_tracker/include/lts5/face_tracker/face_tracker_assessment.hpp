/**
 *  @file   face_tracker_assessment.hpp
 *  @brief  face tracker assessment class definition
 *  @ingroup face_tracker
 *
 *  @author Hua Gao
 *  @date   01/05/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#ifndef __LTS5_FACE_TRACKER_ASSESSMENT__
#define __LTS5_FACE_TRACKER_ASSESSMENT__

#include "opencv2/core/core.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BaseFaceTrackerAssessment
 *  @brief  Face tracker assessment class
 *  @author Christophe Ecabert
 *  @date   02/06/15
 *  @tparam ImgType Type of Image data (cv::Mat, cv::gpu::GpuMat)
 *  @tparam ShapeType Type of shape data (cv::Mat, LTS5::GPUMatrix<double>)
 *  @ingroup face_tracker
 */
template<typename ImgType, typename ShapeType>
class BaseFaceTrackerAssessment {
 public :

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   ~BaseFaceTrackerAssessment
   *  @fn virtual ~BaseFaceTrackerAssessment(void)
   *  @brief  Destructor
   */
  virtual ~BaseFaceTrackerAssessment(void) {}

  /**
   *  @name   Load
   *  @fn virtual int Load(const std::string& model_name) = 0
   *  @brief  Load the assessment model from file
   *  @param[in]  model_name      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  virtual int Load(const std::string& model_name) = 0;

  /**
   *  @name   Load
   *  @fn virtual int Load(std::istream& input_stream,const bool& is_binary) = 0
   *  @brief  Load the assessment model from file stream
   *  @param[in]  input_stream  File stream for reading the model
   *  @param[in]  is_binary     Indicate if stream is open has binary
   *  @return -1 if errors, 0 otherwise
   */
  virtual int Load(std::istream& input_stream,const bool& is_binary) = 0;

  /**
   *  @name   Write
   *  @fn virtual int Write(std::ostream& out_stream) const = 0
   *  @brief  Copy the object into a binary stream
   *  @param[in]  out_stream  Output binary stream
   *  @return -1 if error, 0 otherwisestd::cout << "Y wrong" << std::endl;
   */
  virtual int Write(std::ostream& out_stream) const = 0;

  /**
   *  @name ComputeObjectSize
   *  @fn virtual int ComputeObjectSize(void) const = 0
   *  @brief  Compute the object size in bytes
   *  @return Object memory size
   */
  virtual int ComputeObjectSize(void) const = 0;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Assess
   *  @fn virtual double Assess(const ImgType& image,
                                const ShapeType& shape) = 0
   *  @brief  Assessment method, given the current shape and image
   *  @param[in]  image   Input image
   *  @param[in]  shape     Current shape tracked by the face tracker
   *  @return   scalar score, the larger the better
   */
  virtual double Assess(const ImgType& image, const ShapeType& shape) = 0;

#pragma mark -
#pragma mark Usage

  /**
   *  @name set_score_threshold
   *  @fn virtual void set_score_threshold(double threshold) = 0
   *  @brief  Set up a new decision threshold
   *  @param[in]  threshold   New decision threshold
   */
  virtual void set_score_threshold(const double threshold) = 0;

  /**
   *  @name get_score_threshold
   *  @fn virtual double get_score_threshold(void) const = 0
   *  @brief  Get the current decision threshold
   *  @return  Current decision threshold
   */
  virtual double get_score_threshold(void) const = 0;

  /**
   * @name  get_score
   * @fn    vitual double get_score(void) const
   * @brief Provide assessment score
   * @return    Assessment score
   */
  virtual double get_score(void) const {
    return score_;
  }

#pragma mark -
#pragma mark Protected
 protected:
  /** Assessment score */
  double score_;
};
}  // namespace LTS5
#endif /* __LTS5_FACE_TRACKER_ASSESSMENT__ */
