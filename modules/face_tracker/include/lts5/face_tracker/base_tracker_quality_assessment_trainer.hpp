/**
 *  @file   base_tracker_quality_assessment_trainer.hpp
 *  @brief  Face tracker quality assessment trainer class definition
 *  @ingroup face_tracker
 *
 *  @author Gabriel Cuendet
 *  @date   02/06/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_BASE_TRACKER_QUALITY_ASSESSMENT_TRAINER__
#define __LTS5_BASE_TRACKER_QUALITY_ASSESSMENT_TRAINER__

#include <stdio.h>
#include <string>

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
  /**
   *  @class  BaseTrackerQualityAssessmentTrainer
   *  @brief  Face tracker quality assessment trainer class definition
   *  @author Gabriel Cuendet
   *  @date   02/06/15
   *  @ingroup face_tracker
   */
  class BaseTrackerQualityAssessmentTrainer {
   public :

    /**
     *  @name ~BaseTrackerQualityAssessmentTrainer
     *  @fn   virtual ~BaseTrackerQualityAssessmentTrainer()
     *  @brief  Destructor
     */
    virtual ~BaseTrackerQualityAssessmentTrainer() {}

#pragma mark -
#pragma mark Train and Save methods
    /**
     *  @name   Save
     *  @fn virtual int Save(const std::string& model_name) = 0
     *  @brief  Save the trained model in file. This must be override by
     *          subclass
     *  @param  [in]    model_name      Name of the file
     *  @return -1 in case of error, otherwise 0
     */
    virtual int Save(const std::string& model_name) = 0;

    /**
     *  @name   Save
     *  @fn virtual int Save(std::ostream& output_stream) = 0
     *  @brief  Write the assessment model to file stream. This must be
     *          override by subclass
     *  @param[in]  output_stream  File stream for writing the model
     *  @return -1 if errors, 0 otherwise
     */
    virtual int Save(std::ostream& output_stream) = 0;

    /**
     *  @name   Train
     *  @fn virtual void Train(void) = 0
     *  @brief  SDM training method
     */
    virtual void Train(void) = 0;
  };
}  // namespace LTS5

#endif /* __LTS5_BASE_TRACKER_QUALITY_ASSESSMENT_TRAINER__ */
