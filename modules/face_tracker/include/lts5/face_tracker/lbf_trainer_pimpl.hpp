/**
 *  @file   lbf_trainer_pimpl.hpp
 *  @brief  PIMPL definition for LBFTrainer
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   21/03/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LBF_TRAINER_PIMPL__
#define __LBF_TRAINER_PIMPL__

#include "lts5/face_tracker/lbf_trainer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark GroundTruth (Hidden class- PIMPL)

/**
 * @struct    GroundTruth
 * @brief Container for image, shape, flip information storing
 * @ingroup face_tracker
 */
struct GroundTruth {
  /** Path to image */
  std::string image_path;
  /** Path to annotation */
  std::string annotation_path;
  /** Image size */
  cv::Size size;
  /** Face region */
  cv::Rect face_region;
  /** Image patch */
  cv::Mat image_patch;
  /** Shape */
  cv::Mat shape;
  /** Label, positive/negative */
  int label;
  /** Flip indicator */
  bool is_flipped;

  /**
   * @name    GroundTruth
   * @brief   Constructor
   */
  GroundTruth(void) : label(0), is_flipped(false) {}

  /**
   * @name    operator<<
   * @brief   Output stream
   * @param[in] os    Output stream, where to write data
   * @param[in] gt    Element to write into the stream
   * @return  stream
   */
  friend std::ostream&operator<<(std::ostream& os, const GroundTruth& gt) {
    // Dump image path
    size_t len = gt.image_path.length();
    os << len;
    os.write(reinterpret_cast<const char*>(gt.image_path.data()), len);
    // Dump annotation path
    len = gt.annotation_path.length();
    os << len;
    os.write(reinterpret_cast<const char*>(gt.annotation_path.data()), len);
    // Size
    os.write(reinterpret_cast<const char*>(&gt.size.width),
             sizeof(gt.size.width));
    os.write(reinterpret_cast<const char*>(&gt.size.height),
             sizeof(gt.size.height));
    // Face region
    os.write(reinterpret_cast<const char*>(&gt.face_region.x),
             sizeof(gt.face_region.x));
    os.write(reinterpret_cast<const char*>(&gt.face_region.y),
             sizeof(gt.face_region.y));
    os.write(reinterpret_cast<const char*>(&gt.face_region.width),
             sizeof(gt.face_region.width));
    os.write(reinterpret_cast<const char*>(&gt.face_region.height),
             sizeof(gt.face_region.height));
    // Label
    os.write(reinterpret_cast<const char*>(&gt.label),
             sizeof(gt.label));
    return os;
  }

  /**
   * @name    operator>>
   * @brief   Output stream
   * @param[in] is      Output stream, where to write data
   * @param[out] gt     Element to read from the stream
   * @return  stream
   */
  friend std::istream& operator>>(std::istream& is, GroundTruth& gt) {
    // Dump image path
    size_t len;
    is >> len;
    if (is.good()) {
      gt.image_path.resize(len);
      is.read(reinterpret_cast<char*>(&gt.image_path[0]), len);
      // Dump annotation path
      is >> len;
      gt.annotation_path.resize(len);
      is.read(reinterpret_cast<char*>(&gt.annotation_path[0]), len);
      // Size
      is.read(reinterpret_cast<char*>(&gt.size.width),
              sizeof(gt.size.width));
      is.read(reinterpret_cast<char*>(&gt.size.height),
              sizeof(gt.size.height));
      // Face region
      is.read(reinterpret_cast<char*>(&gt.face_region.x),
              sizeof(gt.face_region.x));
      is.read(reinterpret_cast<char*>(&gt.face_region.y),
              sizeof(gt.face_region.y));
      is.read(reinterpret_cast<char*>(&gt.face_region.width),
              sizeof(gt.face_region.width));
      is.read(reinterpret_cast<char*>(&gt.face_region.height),
              sizeof(gt.face_region.height));
      // Label
      is.read(reinterpret_cast<char*>(&gt.label),
              sizeof(gt.label));
    }
    return is;
  }
};

#pragma mark -
#pragma mark RandomTreeTrainer (Hidden class- PIMPL)

/** Pimpl implementation for specific hidden class such as :
 *  - RandomTreeTrainer
 *  - RandomForestTrainer
 */

/**
 * @class   RandomTreeTrainer
 * @brief   Random Tree training class to learn "Local Binary Feature"
 * @author  Guillaume Jaume, Christophe Ecabert
 * @date    27/02.2017
 * @ingroup face_tracker
 */
class RandomTreeTrainer {
 public:

#pragma mark -
#pragma mark Type definition
  /** User defined tracker parameters */
  using Parameters = LTS5::LBFTrainer::LBFTrainerParameters;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  RandomTreeTrainer
   * @fn    RandomTreeTrainer(void);
   * @brief Constructor
   */
  RandomTreeTrainer(void);

  /**
   * @name  ~RandomTreeTrainer
   * @fn    ~RandomTreeTrainer(void);
   * @brief Destructor
   */
  ~RandomTreeTrainer(void);

  /**
   * @name Init
   * @brief Initialize Tree
   * @param[in] landmark_id     Id of the corresponding landmark [0, 67]
   * @param[in] params          User defined parameters
   */
  void Init(const int landmark_id, const Parameters& params);

#pragma mark -
#pragma mark Usage

  /**
   * @name Train
   * @brief train each tree by constructing the optimal split in the tree
   * @param[in] ground_truth    List of samples
   * @param[in] current_shapes  List of current shape
   * @param[in] delta_shapes    List of ideal delta shape for each image at
   *                            each stage
   * @param[in] mean_shape      Normalized mean shape
   * @param[in] index           ?
   * @param[in] stage           stage i
   */
  void Train(const std::vector<GroundTruth>& ground_truth,
             const std::vector<cv::Mat>& current_shapes,
             const std::vector<cv::Mat>& delta_shapes,
             const cv::Mat& mean_shape,
             const std::vector<int>& index,
             const int stage);

  /**
   * @name  Write
   * @fn    int Write(std::ostream& stream)
   * @brief Write this random tree into a given stream
   * @param[in] stream  Stream where to ouput the object
   * @return    -1 if error, 0 otherwise
   */
  int Write(std::ostream& stream) const;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize(void) const
   * @brief Compute memory need for this RandomTree
   * @return    Object size in byte
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_feats
   * @return Sampling location
   */
  const cv::Mat& get_feats(void) const {
    return feats_;
  }

  /**
   * @name  get_threshold
   * @return Threshold
   */
  const std::vector<int>& get_threshold(void) const {
    return thresholds_;
  }


#pragma mark -
#pragma mark Private
 private:

  /**
   * @name SplitNode
   * @brief determime the optimal thrsh and pixel to take with respect to each
   *        landmark to decrease the variance (ie. split that classifes images in
   *        2 categories with max information gain )
   * @param ground_truth    List of samples
   * @param current_shapes  List of current shape
   * @param delta_shapes    List of ideal delta shape for each image at
   *                        each stage
   * @param mean_shape      Normalized mean shape
   * @param root            ?
   * @param idx             Index of the node under split
   * @param stage           Stage i
   */
  void SplitNode(const std::vector<GroundTruth>& ground_truth,
                 const std::vector<cv::Mat>& current_shapes,
                 const cv::Mat& delta_shapes,
                 const cv::Mat& mean_shape,
                 const std::vector<int>& root,
                 const int idx,
                 const int stage);

  /**
   * @name CalcVariance
   * @fn double CalcVariance(const cv::Mat& vec) const
   * @brief calculate variance of a matrix
   * @param[in] vec  Mat
   * @return variance
   */
  double CalcVariance(const cv::Mat& vec) const;

  /**
   * @name CalcVariance
   * @fn double CalcVariance(const std::vector<double>& vec) const
   * @brief calculate variance of a matrix
   * @param[in] vec  Mat
   * @return variance
   */
  double CalcVariance(const std::vector<double>& vec) const;

  /** landmark corresponding to current tree */
  int landmark_id_;
  /** matrix of features (px to take with respect to each landmark)*/
  cv::Mat feats_;
  /**  vector of threshold for each node  */
  std::vector<int> thresholds_;
  /** User defined parameters */
  Parameters params_;
};

#pragma mark -
#pragma mark RandomForestTrainer (Hidden class- PIMPL)

/**
 * @class   RandomForestTrainer
 * @brief   Random Forest training class to learn "Local Binary Feature"
 * @author  Guillaume Jaume, Christophe Ecabert
 * @date    27/02.2017
 * @ingroup face_tracker
 */
class RandomForestTrainer {
 public:

#pragma mark -
#pragma mark Type definition
  /** User defined LBF tracker parameters */
  using Parameter = LTS5::LBFTrainer::LBFTrainerParameters;

#pragma mark -
#pragma mark Initialization

  /**
   * @name RandomForestTrainer
   * @fn RandomForestTrainer(void)
   * @brief constructor
   */
  RandomForestTrainer(void);

  /**
   * @name ~RandomForestTrainer
   * @fn ~RandomForestTrainer(void)
   * @brief Destructor
   */
  ~RandomForestTrainer(void);

  /**
   * @name  Init
   * @param[in] n_landmark  Number of landmark in the shape
   * @param[in] params      User defined parameters
   */
  void Init(const int n_landmark,
            const Parameter& params);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Train
   * @brief Train the whole forest
   * @param ground_truth    List of ground truth samples
   * @param current_shapes  List of shape for a given stage
   * @param delta_shapes    List of delta shape
   * @param mean_shape      Normalized meanshape
   * @param stage           Current stage
   */
  void Train(const std::vector<GroundTruth>& ground_truth,
             const std::vector<cv::Mat>& current_shapes,
             const std::vector<cv::Mat>& delta_shapes,
             const cv::Mat& mean_shape,
             const int stage);


  /**
   * @name  ComputeLBF
   * @brief Compute feature for a given shape/image
   * @param[in] ground_truth    One sample
   * @param[in] current_shape   Current shape
   * @param[in] meanshape       Reference shape
   * @param[in] inner_shape     Flag to indicate if we compute only for
   *                            the innershape
   * @param[out] lbf            Computed features
   */
  void ComputeLBF(const GroundTruth& ground_truth,
                  const cv::Mat& current_shape,
                  const cv::Mat& meanshape,
                  const bool inner_shape,
                  cv::SparseMat* lbf);

  /**
   * @name  Write
   * @brief Dump into a given stream
   * @param[in] stream  Stream where to dump data
   * @return    -1 if error, 0 otherwise
   */
  int Write(std::ostream& stream) const;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize(void) const
   * @brief Compute memory need for this RandomForest
   * @return    Object size in byte
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Private
 private:
  /** number of landmark per image*/
  int n_landmark_;
  /** User defined parameters */
  Parameter params_;
  /** vector of tree (to create a forest) */
  std::vector<std::vector<RandomTreeTrainer>> random_trees_;
};

#pragma mark -
#pragma mark LBFCascadeTrainer - (Hidden class- PIMPL)

/**
 * @class   LBFCascadeTrainer
 * @brief   Training class to learn feature descriptor + global shape regressor
 * @author  Guillaume Jaume, Christophe Ecabert
 * @date    27/02.2017
 * @ingroup face_tracker
 */
class LBFCascadeTrainer {
 public:

#pragma mark -
#pragma mark Type definition

  /** User defined parameters */
  using Parameters = LTS5::LBFTrainer::LBFTrainerParameters;

#pragma mark -
#pragma mark Initialization

  /**
   * @name LBFCascadeTrainer()
   * @brief Constructor
   */
  LBFCascadeTrainer(void);

  /**
   * @name ~LBFCascadeTrainer()
   * @brief Destructor
   */
  ~LBFCascadeTrainer(void);

  /**
   * @name Init
   * @fn void Init(const int n_stage, const int n_landmark)
   * @brief set size of mean_shape, random_forest, regression param
   * @param[in] n_stage Number of stage in the cascade
   * @param[in] n_landmark Number of landmark in the shape
   * @param[in] parameters  User defined parameters
   */
  void Init(const int n_stage,
            const int n_landmark,
            const Parameters& parameters);

#pragma mark -
#pragma mark Usage

  /**
   * @name
   * @brief Train feature descriptor + global regressor
   * @param[in] ground_truth    List of ground truth sample
   * @param[in] meanshape       Normalized shape
   * @param[in,out] current_shape   List of current shape in the cascade
   */
  void Train(const std::vector<GroundTruth>& ground_truth,
             const cv::Mat& meanshape,
             std::vector<cv::Mat>* current_shape);

  /**
   * @name  Write
   * @brief Dump into a given stream
   * @param[in] stream  Stream where to dump data
   * @return    -1 if error, 0 otherwise
   */
  int Write(std::ostream& stream) const;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize(void) const
   * @brief Compute memory need for this LBFCascadeTrainer
   * @return    Object size in byte
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Accessor

  /**
   * @name  get_descriptor
   * @brief Give access to descriptor for a given \p level
   * @param[in] level   Descriptor's level
   * @return    Feature descpriptor or nullptr if outside range
   */
  RandomForestTrainer* get_descriptor(const size_t level) const {
    if (level < random_forests_.size()) {
      return random_forests_[level];
    } else {
      return nullptr;
    }
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name GRegressionTrain
   * @brief from lbfs determine W global by solving opt prob with dual coord descent meth
   * @param[in] lbfs        List of LBF features
   * @param[in] delta_shape List of delta shape ideal
   * @param[in] stage       Current stage
   */
  void GRegressionTrain(const std::vector<cv::SparseMat>& lbf_features,
                        const std::vector<cv::Mat>& delta_shape,
                        const int stage);

  /**
   * @name GRegressionPredict
   * @param[in] lbf     Feature to do regression
   * @param[in] stage   Current stage
   * @param[out] delta  Delta shape for a given stage
   */
  void GRegressionPredict(const cv::SparseMat& lbf,
                          const int stage,
                          cv::Mat* delta);

  /**
   * @name  ComputeDeltas
   * @brief Compute delta shape from curr to ground truth
   * @param[in] ground_truth    Target shape
   * @param[in] current_shape   Current shape
   * @param[in] meanshape       Normalized meanshape
   * @param[out] delta_shape    Delta shape (distance to recover for next stage)
   */
  void ComputeDelta(const std::vector<GroundTruth>& ground_truth,
                    const std::vector<cv::Mat> current_shape,
                    const cv::Mat& meanshape,
                    std::vector<cv::Mat>* delta_shape);

  /** User defined parameters */
  Parameters params_;
  /** number of stages (increments delta_shape) */
  int n_stages_;
  /** number of landmarks per image */
  int n_landmark_;
  /** vector of pointer on RandomForestTrainer (thresh, pixel) */
  std::vector<RandomForestTrainer*> random_forests_;
  /** vector of regression for each stage */
  std::vector<cv::Mat> gl_regression_weights_;
};

}  // namepsace LTS5
#endif //__LBF_TRAINER_PIMPL__
