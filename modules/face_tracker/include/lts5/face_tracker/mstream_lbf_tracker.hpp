/**
 *  @file   mstream_lbf_tracker.hpp
 *  @brief  LBFTRacker for multiple image stream.
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   23/03/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __MSTREAM_LBF_TRACKER__
#define __MSTREAM_LBF_TRACKER__

#include <vector>

#include "opencv2/objdetect/objdetect.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_tracker/mstream_face_tracker.hpp"
#include "lts5/face_tracker/face_tracker_assessment.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Forwad descriptor */
class LBFCascade;

/**
 *  @class  MStreamLBFTracker
 *  @brief  LBF tracker implementation
 *  @author Christophe Ecabert
 *  @date   23/03/17
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS MStreamLBFTracker :
        public MStreamFaceTracker<cv::Mat, cv::Mat> {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   MStreamLBFTracker
   *  @fn MStreamLBFTracker(const size_t n_stream)
   *  @brief  Constructor
   *  @param[in]  n_stream  Number of image to track
   */
  explicit MStreamLBFTracker(const size_t n_stream);

  /**
   *  @name   MStreamLBFTracker
   *  @fn MStreamLBFTracker(const MStreamLBFTracker& other) = delete
   *  @brief  Copy Constructor disable
   *  @param[in]  other   Object to copy from
   */
  MStreamLBFTracker(const MStreamLBFTracker& other) = delete;

  /**
   *  @name   operator=
   *  @fn MStreamLBFTracker& operator=(const MStreamLBFTracker& rhs) = delete
   *  @brief  Assignment operator
   *  @param[in]  rhs   Object to assign from
   *  @return Assigned face tracker
   */
  MStreamLBFTracker& operator=(const MStreamLBFTracker& rhs) = delete;

  /**
   *  @name   ~MStreamLBFTracker
   *  @fn ~MStreamLBFTracker()
   *  @brief  Destructor
   */
  ~MStreamLBFTracker();

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& model_filename);
   *  @brief  Load the LBF model
   *  @param[in]  model_filename      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename);

  /**
   *  @name   Load
   *  @fn     int Load(std::istream& stream)
   *  @brief  Load the LBF model
   *  @param[in]  stream      Stream to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& stream);

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& model_filename,
              const std::string& face_detector_config)
   *  @brief  Load the LBF model and Viola Jones face detector
   *  @param[in]  model_filename          Path to the model
   *  @param[in]  face_detector_config    Configuration file for
   *                                      Viola & Jones face detector
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename,
           const std::string& face_detector_config);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& model_name)
   *  @brief  Save the trained model in file.
   *  @param  [in]    model_name      Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  int Save(const std::string& model_name);

  /**
   *  @name   Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Write the object into a binary stream
   *  @param[in]  out_stream  Binary stream to files
   *  @return -1 if error, 0 otherwise.
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name   ComputeObjectSize
   *  @fn int ComputeObjectSize() const
   *  @brief  Compute the memory used by object
   *  @return Size in bytes
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Tracking

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image, cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set
   *          of landmarks.
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::Mat& image, cv::Mat* shape);

  /**
   *  @name   Detect
   *  @fn int Detect(const std::vector<cv::Mat>&images,
                     std::vector<cv::Mat>* shapes)
   *  @brief  Dectection method, given a multiples image, provides a set of
   *          landmarks for each image. This must be override by subclass
   *  @param[in]  images   List of input images
   *  @param[out] shapes   List of set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const std::vector<cv::Mat>& images,
             std::vector<cv::Mat>* shapes);

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image,
                     const cv::Rect& face_region,
                     cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face boundingg box
   *  @param[out] shape           Set of landmarks
   *  @return     >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::Mat& image,
             const cv::Rect& face_region,
             cv::Mat* shape);

  /**
   *  @name   Detect
   *  @fn int Detect(const std::vector<cv::Mat>& images,
                     const std::vector<cv::Rect>& face_regions,
                     std::vector<cv::Mat>* shapes)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks. This must be override by subclass
   *  @param[in]  images           List of input image
   *  @param[in]  face_regions     List of face boundingg box
   *  @param[out] shapes           List of set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const std::vector<cv::Mat>& images,
             const std::vector<cv::Rect>& face_regions,
             std::vector<cv::Mat>* shapes);

  /**
   *  @name   Track
   *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
   *  @brief  Tracking method, given an input image
   *  @param[in]      image   Input image
   *  @param[in,out]  shape   Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Track(const cv::Mat& image, cv::Mat* shape);

  /**
   *  @name   Track
   *  @fn int Track(const std::vector<cv::Mat>& images,
                    std::vector<cv::Mat>* shapes)
   *  @brief  Tracking method, given an input image (from a sequence), provides
   *          a set of landmarks. This must be override by subclass
   *  @param[in]  images   List of input image
   *  @param[out] shapes   List of set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Track(const std::vector<cv::Mat>& images,
            std::vector<cv::Mat>* shapes);

#pragma mark -
#pragma mark Training

  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder)
   *  @brief  LBF training method
   *  @param[in]  folder  Location of images/annotations
   */
  void Train(const std::string& folder);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name get_score_threshold
   *  @fn double get_score_threshold() const
   *  @brief  Get the actual decision threshold
   *  @return Decision threshold
   */
  double get_score_threshold() const {
    return tracking_assessment_->get_score_threshold();
  }

  /**
   *  @name   is_tracking
   *  @fn bool is_tracking(const int n_stream) const
   *  @brief  Give tracking status
   *  @param[in]  n_stream  Current stream
   *  @return true if tracking false otherwise
   */
  bool is_tracking(const int n_stream) const {
    return is_tracking_[n_stream];
  }

  /**
   *  @name set_score_threshold
   *  @fn void set_score_threshold(const double threshold)
   *  @brief  Set new decision threshold
   *  @param[in]  threshold   Decision threshold
   */
  void set_score_threshold(const double threshold) {
    return tracking_assessment_->set_score_threshold(threshold);
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image,const size_t n_stream, cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set
   *          of landmarks.
   *  @param[in]  image     Input image
   *  @param[in]  n_stream  Current stream index
   *  @param[out] shape     Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::Mat& image, const size_t n_stream, cv::Mat* shape);

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image,
                     const cv::Rect& face_region,
                     const size_t n_stream,
                     cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face boundingg box
   *  @param[in]  n_stream        Current stream index
   *  @param[out] shape           Set of landmarks
   *  @return     >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::Mat& image,
             const cv::Rect& face_region,
             const size_t n_stream,
             cv::Mat* shape);

  /**
   *  @name   Track
   *  @fn int Track(const cv::Mat& image,const size_t n_stream, cv::Mat* shape)
   *  @brief  Tracking method, given an input image
   *  @param[in] image   Input image
   *  @param[in] n_stream   Current stream index
   *  @param[in,out]  shape   Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Track(const cv::Mat& image, const size_t n_stream, cv::Mat* shape);

  /**
   *  @name   ComputeInitialShape
   *  @brief  Aligned the meanshape inside the bounding box
   *  @param[in]  meanshape       Model meanshape
   *  @param[in]  bounding_box    Region of interest
   *  @param[out] shape           Initial shape
   */
  void ComputeInitialShape(const cv::Mat &meanshape,
                           const cv::Rect &bbox,
                           cv::Mat *shape);

  /**
   * @name  AlignMeanshapeToCurrent
   * @fn    void AlignMeanshapeToCurrent(const cv::Mat& current_shape,
                               const cv::Mat& meanshape,
                               cv::Mat* shape)
   * @brief Place the \p meanshape at the position of \p current_shape
   * @param[in] current_shape   Current shape
   * @param[in] meanshape       Meanshape
   * @param[out] shape          New shape (transformed meanshape)
   */
  void AlignMeanshapeToCurrent(const cv::Mat& current_shape,
                               const cv::Mat& meanshape,
                               cv::Mat* shape);

  
  /** Number of tracked stream */
  size_t n_stream_;
  /** lbf_cascador to Predict shape */
  LBFCascade *lbf_cascade_;
  /** Viola-Jones face detector */
  cv::CascadeClassifier* face_detector_;
  /** Tracking quality assessment */
  LTS5::BaseFaceTrackerAssessment<cv::Mat,cv::Mat>* tracking_assessment_;
  /** am I currently tracking ? */
  std::vector<bool> is_tracking_;
  /** Meanshape */
  cv::Mat meanshape_;
  /** previous_frame used to estimate the new one */
  std::vector<cv::Mat> previous_shape_;
  /** Working image - grayscale */
  std::vector<cv::Mat> working_img_;
  /** Shape's center of gravity */
  std::vector<cv::Point2f> shape_center_gravity_;
};

/**
 * @class   MLBFProxy
 * @brief   Interface for registration mechanism for face tracker type
 * @author  Christophe Ecabert
 * @date    14/08/17
 * @ingroup face_tracker
 */
class MLBFProxy : public MTrackerProxy {
 public:

  /**
   * @name  MLBFProxy
   * @fn    MLBFProxy()
   * @brief Constructor
   */
  MLBFProxy() = default;

  /**
   * @name  ~MLBFProxy
   * @fn    ~MLBFProxy() override
   * @brief Destructor
   */
  ~MLBFProxy() override;

  /**
   * @name  Create
   * @fn    MStreamFaceTracker<cv::Mat, cv::Mat>* Create(const int N) const override
   * @brief Create an instance of MStreamFaceTracker
   * @param[in] N number of stream
   * @return    Tracker instance with proper type
   */
  MStreamFaceTracker<cv::Mat, cv::Mat>* Create(const int N) const override;

  /**
   *  @name Name
   *  @fn const char* Name() const override
   *  @brief  Return the name for a given type of tracker
   *  @return Tracker name (i.e. sdm, lbf, ...)
   */
  const char* Name() const override;
};

}  // namepsace LTS5

#endif //__MSTREAM_LBF_TRACKER__
