/**
 *  @file   intraface_tracker.hpp
 *  @brief  Face tracker wrapper for Intraface implementation
 *  @ingroup face_tracker
 *
 *  @author Ecabert Christophe
 *  @date   14/12/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_INTRAFACE_TRACKER__
#define __LTS5_INTRAFACE_TRACKER__

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/core/core.hpp"
#include "FaceAlignment.h"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_tracker/base_face_tracker.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  IntrafaceTracker
 *  @brief  Wrapper for Intraface face tracker
 *  @author Christophe Ecabert
 *  @date   14/12/15
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS IntrafaceTracker : public BaseFaceTracker<cv::Mat, cv::Mat> {

 public :

  /**
   *  @name IntrafaceTracker
   *  @fn IntrafaceTracker(void)
   *  @brief  Constructor
   */
  IntrafaceTracker(void);

  /**
   *  @name ~IntrafaceTracker
   *  @fn ~IntrafaceTracker(void)
   *  @brief  Destructor
   */
  ~IntrafaceTracker(void);

  /**
   *  @name   Load
   *  @fn int Load(const std::string& model_filename)
   *  @brief  Load the tracker model. This must be override by subclass
   *  @param[in]  model_filename      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename) override;

  /**
   *  @name   Load
   *  @fn int Load(const std::string& detection_model,
                   const std::string& tracking_model,
                   const std::string& f_detector_model)
   *  @brief  Load the tracker model. This must be override by subclass
   *  @param[in]  detection_model     Path to the detection model
   *  @param[in]  tracking_model      Path to the tracking model
   *  @param[in]  f_detector_model    Face detector configuration
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& detection_model,
           const std::string& tracking_model,
           const std::string& f_detector_model);

  /**
   *  @name   Load
   *  @fn int Load(const std::string& detection_model,
                   const std::string& tracking_model)
   *  @brief  Load the tracker model. This must be override by subclass
   *  @param[in]  detection_model      Path to the detection model
   *  @param[in]  tracking_model       Path to the tracking model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& detection_model,
           const std::string& tracking_model);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& model_name)
   *  @brief  Save the trained model in file. This must be override by subclass
   *  @param[in] model_name   Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  int Save(const std::string& model_name) override;

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image, cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks. This must be override by subclass
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Detect(const cv::Mat& image, cv::Mat* shape) override;

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image, const cv::Rect& face_region,
                     cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks. This must be override by subclass
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face boundingg box
   *  @param[out] shape           Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Detect(const cv::Mat& image, const cv::Rect& face_region,
             cv::Mat* shape) override;

  /**
   *  @name   Track
   *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
   *  @brief  Tracking method, given an input image (from a sequence), provides
   *          a set of landmarks. This must be override by subclass
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Track(const cv::Mat& image, cv::Mat* shape) override;

  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder)
   *  @brief  Tracker training method. This must be override by subclass
   *  @param[in]  folder  Location of images/annotations
   */
  void Train(const std::string& folder) override;

  #pragma mark -
  #pragma mark Utility

  /**
   *  @name   draw_shape
   *  @fn static cv::Mat draw_shape(const cv::Mat& img,
                                    const cv::Mat& shape,
                                    const cv::Scalar& color)
   *  @brief  Draw shape on image
   *  @param[in]  img     Input image
   *  @param[in]  shape   Shape to draw
   *  @param[in]  color   Color of the shape
   *  @return Copy of the image with drawing
   */
  static cv::Mat draw_shape(const cv::Mat& img,
                            const cv::Mat& shape,
                            const cv::Scalar& color);

 private :

  /** Descriptor */
  INTRAFACE::XXDescriptor* xxd_;
  /** Tracker */
  INTRAFACE::FaceAlignment* tracker_;
  /** Current shape */
  cv::Mat shape_;
  /** Previous shape */
  cv::Mat previous_shape_;
  /** Working image */
  cv::Mat working_image_;
  /** Tracking flag */
  bool is_tracking_;
  /** Scoring threshold */
  float score_thresh_ = 0.2f;
  /** Face detector */
  cv::CascadeClassifier* f_detector_;

};
}  // namespace LTS5
#endif /* __LTS5_INTRAFACE_TRACKER__ */
