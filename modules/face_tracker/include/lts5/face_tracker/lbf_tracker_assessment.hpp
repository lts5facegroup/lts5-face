/**
 *  @file   lbf_tracker_assessment.hpp
 *  @brief  LBFTracker assessment class
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   21/03/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LBF_TRACKER_ASSESSMENT__
#define __LBF_TRACKER_ASSESSMENT__

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_tracker/face_tracker_assessment.hpp"
#include "lts5/classifier/binary_sparse_linear_svm_classifier.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Descriptor */
class RandomForest;

/**
 *  @class  LBFTrackerAssessment
 *  @brief  Face tracker quality assessment
 *  @author Christophe Ecabert
 *  @date   21/03/17
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS LBFTrackerAssessment : public BaseFaceTrackerAssessment<cv::Mat,cv::Mat> {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  LBFTrackerAssessment
   * @fn    LBFTrackerAssessment(const cv::Mat& meanshape)
   * @brief Constructor
   * @param[in] meanshape   Meanshape
   */
  LBFTrackerAssessment(const cv::Mat& meanshape);

  /**
   * @name  ~LBFTrackerAssessment
   * @fn    ~LBFTrackerAssessment(void)
   * @brief Destructor
   */
  ~LBFTrackerAssessment(void);

  /**
   *  @name   Load
   *  @fn int Load(const std::string& model_name)
   *  @brief  Load the assessment model from file
   *  @param[in]  model_name      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_name);

  /**
   *  @name   Load
   *  @fn int Load(std::istream& input_stream,const bool& is_binary)
   *  @brief  Load the assessment model from file stream
   *  @param[in]  input_stream  File stream for reading the model
   *  @param[in]  is_binary     Indicate if stream is open has binary
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& input_stream,const bool& is_binary);

  /**
   *  @name   Write
   *  @fn int Write(std::ofstream& out_stream) const
   *  @brief  Copy the object into a binary stream
   *  @param[in]  out_stream  Output binary stream
   *  @return -1 if error, 0 otherwisestd::cout << "Y wrong" << std::endl;
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the object size in bytes
   *  @return Object memory size
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Assess
   *  @fn double Assess(const cv::Mat& image, const cv::Mat& shape)
   *  @brief  Assessment method, given the current shape and image
   *  @param[in]  image   Input image
   *  @param[in]  shape     Current shape tracked by the face tracker
   *  @return   scalar score, the larger the better
   */
  double Assess(const cv::Mat& image, const cv::Mat& shape);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name set_score_threshold
   *  @fn void set_score_threshold(double threshold)
   *  @brief  Set up a new decision threshold
   *  @param[in]  threshold   New decision threshold
   */
  void set_score_threshold(const double threshold) {
    score_threshold_ = threshold;
  }

  /**
   *  @name get_score_threshold
   *  @fn double get_score_threshold(void) const
   *  @brief  Get the current decision threshold
   *  @return  Current decision threshold
   */
  double get_score_threshold(void) const {
    return score_threshold_;
  }

#pragma mark -
#pragma mark Private
 private:
  /** Score threshold */
  double score_threshold_;
  /** Binary classifier */
  BinarySparseLinearSVMClassifier* classifier_;
  /** Descriptor */
  RandomForest* descriptor_;
  /** Meanshape */
  cv::Mat meanshape_;
};
}  // namepsace LTS5
#endif //__LBF_TRACKER_ASSESSMENT__
