/**
 *  @file   lbf_trainer.hpp
 *  @brief  Training class for LBF face tracker
 *  @ingroup face_tracker
 *
 *  @author Guillaume Jaume, Christophe Ecabert
 *  @date   27/02.2017
 *  Copyright (c) 2017 Guillaume Jaume. All rights reserved.
 */

#ifndef LTS5_LBF_TRAINER_HPP
#define LTS5_LBF_TRAINER_HPP

#include <cstdio>
#include <string>
#include <ctime>

#include "opencv2/highgui.hpp"
#include "opencv2/objdetect.hpp"

#include "lts5/face_detector/base_face_detector.hpp"
#include "lts5/face_tracker/base_tracker_quality_assessment_trainer.hpp"
#include "lts5/face_tracker/lbf_tracker.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */

namespace LTS5 {

/** Forward declaration */
class LBFCascadeTrainer;
/** Ground Truth */
struct GroundTruth;

/**
 * @class   LBFTrainer
 * @brief   Training class for LBF face tracker
 * @author  Guillaume Jaume, Christophe Ecabert
 * @date    27/02.2017
 * @ingroup face_tracker
 */
class  LBFTrainer {

public :

#pragma mark -
#pragma mark Type definition

 /**
  *  @struct LBFTrainerParameters
  *  @brief  User configuration for LBF tracker
  */
  struct LBFTrainerParameters {
    /** number of stage in the system (shape increment)*/
    int n_stages;
    /** numbe of tree per landmark in the system*/
    int n_tree;
    /** depth of each tree */
    int tree_depth;
    /** vector of px (features)*/
    std::vector<int> feats_m;
    /** vector of radius (ie where to find the possible position
     * of the new landmark) */
    std::vector<double> radius_m;
    /** random forest param how much of the dataset do we use each time*/
    double bagging_overlap;
    /** Number of montecarlo samples used for training */
    int montecarlo_samples;
    /** Number of attempt for Montecarlo sampling */
    int montecarlo_sampling_attempt;
    /** Statistics X scaling factors */
    double montecarlo_offsetX_std_scaling;
    /** Statistics Y scaling factors */
    double montecarlo_offsetY_std_scaling;
    /** Statistics SCALE scaling factors */
    double montecarlo_scale_std_scaling;
    /** Training statistics, mean_s, std_s, mean_tx, std_tx, mean_ty, std_ty */
    double training_statistics[6];

    /**
     * @name    LBFTrainerParameters
     * @brief   Default constructor
     */
    LBFTrainerParameters(void) {
      n_stages = 5;
      n_tree = 5;
      tree_depth = 5;
      this->feats_m = { 500, 500, 500, 300, 300, 300, 200, 200, 200, 100 };
      this->radius_m = { 0.3, 0.2, 0.15, 0.12, 0.10, 0.10,
                          0.08, 0.06, 0.06, 0.05 };
      bagging_overlap = 0.4;
      // Training related
      montecarlo_samples = 10;
      montecarlo_sampling_attempt = 10;
      montecarlo_offsetX_std_scaling = 0.1;
      montecarlo_offsetY_std_scaling = 0.1;
      montecarlo_scale_std_scaling = 0.1;
      training_statistics[0] = std::numeric_limits<double>::quiet_NaN();
      training_statistics[1] = std::numeric_limits<double>::quiet_NaN();
      training_statistics[2] = std::numeric_limits<double>::quiet_NaN();
      training_statistics[3] = std::numeric_limits<double>::quiet_NaN();
      training_statistics[4] = std::numeric_limits<double>::quiet_NaN();
      training_statistics[5] = std::numeric_limits<double>::quiet_NaN();
    }
  };

#pragma mark -
#pragma mark Initialization

  /**
    *  @name   LBFTrainer
    *  @fn LBFTrainer(void)
    *  @brief  Constructor
  */
  LBFTrainer(const std::string& face_detector_config);


  /**
    *  @name   ~LBFTrainer
    *  @fn ~LBFTrainer(void)
    *  @brief  Destructor
   */
  ~LBFTrainer(void);

#pragma mark -
#pragma mark Usage

  /**
   * @name Train
   * @brief Train data (load data, data augmentation, call lbf::Train)
   * @param[in] folder  Dataset folder
   * @param[in,out] params  Trainer parameters
   * @return return -1 of error, 0 otherwise
   */
  int Train(const std::string& folder, LBFTrainerParameters* params);

  /**
   * @name  Write
   * @brief Write model into binary stream
   * @param[in] stream    stream where to dump model
   * @return    -1 if error, 0 otherwise
   */
  int Write(std::ostream& stream) const;

#pragma mark -
#pragma mark Private
 private :

  /**
   * @name  LoadGroundTruth
   * @brief Load image ground truth (path, annotation)
   * @param[in] folder  Root folder
   * @param[out] data   Ground truth
   * @return    -1 if error, 0 otherwise
   */
  int LoadGroundTruth(const std::string& folder,
                      std::vector<GroundTruth>* data);

  /**
   * @name  DetectFace
   * @brief Run face detector on training sample
   * @param[in,out] train_sample    List of training sample
   * @return     -1 if error, 0 otherwise
   */
  int DetectFace(std::vector<GroundTruth>* train_sample);

  /**
   *  @name       PruneDetections
   *  @brief      Amongst (potentially) several detections in an image,
   *              returns the one which center is closest to the annotation
   *              center
   *  @param[in]  annotation      Annotation
   *  @param[in]  detections      Vector of Rect returned by the face detector
   *  @return     Detection which center is closest to the annotation center
   *              or an empty Rect
   */
  cv::Rect PruneDetections(const cv::Mat& annotation,
                           const std::vector<cv::Rect>& detections);

  /**
   * @name  ComputeMeanshape
   * @brief Compute meanshape over all training samples
   * @param[in] samples    Training samples
   */
  void ComputeMeanshape(const std::vector<GroundTruth>& samples);

  /**
   *  @name   ComputeStatistics
   *  @brief  Compute the means and standard deviations of the difference
   *          between the annotations and the face detector along scale, x and y
   *          dimensions
   *  @param[in]      samples      Training samples
   *  @param[in]      mean_shape   Mean shape of the detections
   *  @param[out]     means        Mean along each of the three dimensions:
   *                               scale, x-dimension and y-dimension
   *  @param[out]     stds         Standard deviations along each of the three
   *                               dimensions: scale, x-dimension and
   *                               y-dimension.
   */
  void ComputeStatistics(const std::vector<GroundTruth>& samples,
                         const cv::Mat& meanshape,
                         cv::Mat* means,
                         cv::Mat* stds);

  /**
   * @name  AugmentDataset
   * @brief Generate starting shape through data augmentation
   * @param[in] meanshape       Normalized meanshape
   * @param[in] params          User defined parameters
   * @param[in] stdev           Standard deviation of the perturbation
   * @param[in,out] samples     List of training sample / target
   * @param[out] start_shape    Starting shape
   */
  void AugmentDataset(const cv::Mat& meanshape,
                      const LBFTrainerParameters& params,
                      const cv::Vec3d& stdev,
                      std::vector<GroundTruth>* samples,
                      std::vector<cv::Mat>* start_shape);

  /** Trainer parameters */
  LBFTrainerParameters tracker_parameters_;
  /** Viola-Jones face detector*/
  LTS5::BaseFaceDetector* face_detector_;
  /** Ground truth */
  std::vector<GroundTruth> images_;
  /** Meanshape */
  cv::Mat meanshape_;
  /**  Cascade trainer */
  LBFCascadeTrainer* cascade_trainer_;
  /** Tracker assessment */
  BaseTrackerQualityAssessmentTrainer* assessment_trainer_;
};
}

#endif //LTS5_LBF_TRAINER_HPP
