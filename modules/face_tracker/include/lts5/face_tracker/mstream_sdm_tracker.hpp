/**
 *  @file   "lts5/face_tracker/mstream_sdm_tracker.hpp"
 *  @brief  SDM Face tracker for multiple image stream
 *          One tracking model for multiple input
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   04/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __MSTREAM_SDM_TRACKER__
#define __MSTREAM_SDM_TRACKER__

#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/face_tracker/mstream_face_tracker.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  MStreamSdmTracker
 *  @brief  SDM-facetracker implementation
 *  @author Christophe Ecabert
 *  @date   04/05/16
 *  @see    https://courses.cs.washington.edu/courses/cse590v/13au/intraface.pdf
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS MStreamSdmTracker :
        public MStreamFaceTracker<cv::Mat, cv::Mat> {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   MStreamSdmTracker
   *  @fn MStreamSdmTracker(const int n_stream)
   *  @brief  Constructor
   *  @param[in]  n_stream  Number of image to track
   */
  explicit MStreamSdmTracker(const int n_stream);

  /**
   *  @name   MStreamSdmTracker
   *  @fn MStreamSdmTracker(const MStreamSdmTracker& other) = delete
   *  @brief  Copy Constructor disable
   *  @param[in]  other   Object to copy from
   */
  MStreamSdmTracker(const MStreamSdmTracker& other) = delete;

  /**
   *  @name   operator=
   *  @fn MStreamSdmTracker& operator=(const MStreamSdmTracker& rhs) = delete
   *  @brief  Assignment operator
   *  @param[in]  rhs   Object to assign from
   *  @return Assigned face tracker
   */
  MStreamSdmTracker& operator=(const MStreamSdmTracker& rhs) = delete;

  /**
   *  @name   ~MStreamSdmTracker
   *  @fn ~MStreamSdmTracker()
   *  @brief  Destructor
   */
  ~MStreamSdmTracker() override;

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& model_filename) override
   *  @brief  Load the SDM model
   *  @param[in]  model_filename      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename) override;

  /**
   *  @name   Load
   *  @fn     int Load(std::ifstream& model_stream, const bool& is_binary)
   *  @brief  Load the SDM model
   *  @param[in]  model_stream      Stream to the model
   *  @param[in]  is_binary         Indicate if stream is open as binary
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::ifstream& model_stream, const bool& is_binary);

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& model_filename,
              const std::string& face_detector_config) override
   *  @brief  Load the SDM model and Viola Jones face detector
   *  @param[in]  model_filename          Path to the model
   *  @param[in]  face_detector_config    Configuration file for
   *                                      Viola & Jones face detector
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename,
           const std::string& face_detector_config) override;

  /**
   *  @name   Load
   *  @fn     int Load(std::ifstream& model_stream,
                       const bool& is_binary,
                       const std::string& face_detector_config)
   *  @brief  Load the SDM model and Viola Jones face detector
   *  @param[in]  model_stream          Stream to the model
   *  @param[in]  is_binary             Indicate if stream is open as binary
   *  @param[in]  face_detector_config  Configuration file for Viola & Jones
   *                                    face detector
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::ifstream& model_stream,
           const bool& is_binary,
           const std::string& face_detector_config);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& model_name) override
   *  @brief  Save the trained model in file.
   *  @param  [in]    model_name      Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  int Save(const std::string& model_name) override;

  /**
   *  @name   Write
   *  @fn int Write(std::ofstream& out_stream) const
   *  @brief  Write the object into a binary stream
   *  @param[in]  out_stream  Binary stream to files
   *  @return -1 if error, 0 otherwise.
   */
  int Write(std::ofstream& out_stream) const;

  /**
   *  @name   ComputeObjectSize
   *  @fn int ComputeObjectSize() const
   *  @brief  Compute the memory used by object
   *  @return Size in bytes
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Tracking

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image, cv::Mat* shape) override
   *  @brief  Dectection method, given a still image, provides a set
   *          of landmarks.
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::Mat& image, cv::Mat* shape) override;

  /**
   *  @name   Detect
   *  @fn int Detect(const std::vector<cv::Mat>&images,
                     std::vector<cv::Mat>* shapes) override
   *  @brief  Dectection method, given a multiples image, provides a set of
   *          landmarks for each image. This must be override by subclass
   *  @param[in]  images   List of input images
   *  @param[out] shapes   List of set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const std::vector<cv::Mat>& images,
             std::vector<cv::Mat>* shapes) override;

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image,
                     const cv::Rect& face_region,
                     cv::Mat* shape) override
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face boundingg box
   *  @param[out] shape           Set of landmarks
   *  @return     >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::Mat& image,
             const cv::Rect& face_region,
             cv::Mat* shape) override;

  /**
   *  @name   Detect
   *  @fn int Detect(const std::vector<cv::Mat>& images,
                     const std::vector<cv::Rect>& face_regions,
                     std::vector<cv::Mat>* shapes) override
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks. This must be override by subclass
   *  @param[in]  images           List of input image
   *  @param[in]  face_regions     List of face boundingg box
   *  @param[out] shapes           List of set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const std::vector<cv::Mat>& images,
             const std::vector<cv::Rect>& face_regions,
             std::vector<cv::Mat>* shapes) override;

  /**
   *  @name   Track
   *  @fn int Track(const cv::Mat& image, cv::Mat* shape) override
   *  @brief  Tracking method, given an input image
   *  @param[in]      image   Input image
   *  @param[in,out]  shape   Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Track(const cv::Mat& image, cv::Mat* shape) override;

  /**
   *  @name   Track
   *  @fn int Track(const std::vector<cv::Mat>& images,
                    std::vector<cv::Mat>* shapes) override
   *  @brief  Tracking method, given an input image (from a sequence), provides
   *          a set of landmarks. This must be override by subclass
   *  @param[in]  images   List of input image
   *  @param[out] shapes   List of set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Track(const std::vector<cv::Mat>& images,
            std::vector<cv::Mat>* shapes) override;

#pragma mark -
#pragma mark Training

  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder)
   *  @brief  SDM training method
   *  @param[in]  folder  Location of images/annotations
   */
  void Train(const std::string& folder) override;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   get_tracker_parameters
   *  @fn SdmTracker::SdmTrackerParameters get_tracker_parameters() const
   *  @brief  Provide the actual configuration of the tracker
   *  @return Struct with tracker parameters
   */
  SdmTracker::SdmTrackerParameters get_tracker_parameters() const {
    return tracker_parameters_;
  }

  /**
   *  @name   get_sift_features
   *  @fn cv::Mat get_sift_features(const cv::Mat& image, const cv::Mat& shape) const
   *  @brief  Provide feature extracted by given shape
   *  @param[in]  image   input image
   *  @param[in]  shape   input shape
   *  @return Matrix of SIFT features
   */
  cv::Mat get_sift_features(const cv::Mat& image, const cv::Mat& shape) const;

  /**
   *  @name get_score_threshold
   *  @fn double get_score_threshold() const override
   *  @brief  Get the actual decision threshold
   *  @return Decision threshold
   */
  double get_score_threshold() const override {
    return tracking_assessment_->get_score_threshold();
  }

  /**
   *  @name   is_tracking
   *  @fn bool is_tracking(const int n_stream) const
   *  @brief  Give tracking status
   *  @param[in]  n_stream  Current stream
   *  @return true if tracking false otherwise
   */
  bool is_tracking(const int n_stream) const {
    return is_tracking_[n_stream];
  }

  /**
   *  @name   set_tracker_parameters
   *  @fn void set_tracker_parameters(const SdmTracker::SdmTrackerParameters& tracker_params)
   *  @brief  Set face tracker configuration
   *  @param[in]  tracker_params  Configuration
   */
  void set_tracker_parameters(const SdmTracker::SdmTrackerParameters& tracker_params) {
    tracker_parameters_ = tracker_params;
  }

  /**
   *  @name set_score_threshold
   *  @fn void set_score_threshold(const double threshold) override
   *  @brief  Set new decision threshold
   *  @param[in]  threshold   Decision threshold
   */
  void set_score_threshold(const double threshold) override {
    return tracking_assessment_->set_score_threshold(threshold);
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image,const int n_stream, cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set
   *          of landmarks.
   *  @param[in]  image     Input image
   *  @param[in]  n_stream  Current stream index
   *  @param[out] shape     Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::Mat& image, const int n_stream, cv::Mat* shape);

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image,
                     const cv::Rect& face_region,
                     const int n_stream,
                     cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face boundingg box
   *  @param[in]  n_stream        Current stream index
   *  @param[out] shape           Set of landmarks
   *  @return     >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::Mat& image,
             const cv::Rect& face_region,
             const int n_stream,
             cv::Mat* shape);

  /**
   *  @name   Track
   *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
   *  @brief  Tracking method, given an input image
   *  @param[in]      image   Input image
   *  @param[in,out]  shape   Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Track(const cv::Mat& image, const int n_stream, cv::Mat* shape);

  /**
   *  @name   ComputeInitialShape
   *  @fn void ComputeInitialShape(const cv::Mat& meanshape,
                                   const cv::Rect& bounding_box,
                                   cv::Mat* initial_shape)
   *  @brief  Aligned the meanshape inside the bounding box
   *  @param[in]  meanshape       Model meanshape
   *  @param[in]  bounding_box    Region of interest
   *  @param[out] initial_shape   Initial shape
   */
  void ComputeInitialShape(const cv::Mat& meanshape,
                           const cv::Rect& bounding_box,
                           cv::Mat* initial_shape);

  /**
   *  @name   AlignFaceWithScale
   *  @fn cv::Mat AlignFaceWithScale(const cv::Mat& image, const int n_stream)
   *  @brief  Face alignement with SDM model
   *  @param[in]  image     Input image
   *  @param[in]  n_stream  Current stream
   *  @return Return the new shape
   */
  cv::Mat AlignFaceWithScale(const cv::Mat& image, const int n_stream);

  /**
   *  @name   AlignFaceWithScaleAndRotation
   *  @fn cv::Mat AlignFaceWithScaleAndRotation(const cv::Mat& image,
                                                const int n_stream)
   *  @brief  Face alignement with SDM model
   *  @param[in]  image     Input image
   *  @param[in]  n_stream  Current stream
   *  @return Return the new shape
   */
  cv::Mat AlignFaceWithScaleAndRotation(const cv::Mat& image,
                                        const int n_stream);

  /**
   *  @name AlignMeanshapeToCurrent
   *  @fn cv::Mat AlignMeanshapeToCurrent(const cv::Mat& current_shape)
   *  @brief  Align the meanshape on the shape recovered at previous
   *          frame
   *  @param[in]  current_shape   Shape revovered at previous frame
   *  @return Aligned shape
   */
  cv::Mat AlignMeanshapeToCurrent(const cv::Mat& current_shape);

  /** Number of tracked stream */
  int n_stream_;
  /** Tracker parameters */
  SdmTracker::SdmTrackerParameters tracker_parameters_;
  /** Number of points tracked by the model */
  int num_points_;
  /** Model meanshape */
  cv::Mat sdm_meanshape_;
  /** Initial shape used to start alignment */
  std::vector<cv::Mat> initial_shape_;
  /** Shape recovered at previous iteration, used for tracking */
  std::vector<cv::Mat> previous_shape_;
  /** Tracking indicator flag */
  std::vector<bool> is_tracking_;
  /** SDM-facetracker model */
  std::vector<cv::Mat> sdm_model_;
  /** Viola-Jones face detector */
  cv::CascadeClassifier* face_detector_;
  /** Region selected for the alignment */
  std::vector<cv::Rect> face_region_;
  /** Shape center of gravity */
  std::vector<cv::Point2f> shape_center_gravity_;
  /** Grayscale images where face-tracker is applied */
  std::vector<cv::Mat> working_img_;
  /** SIFT features */
  std::vector<cv::Mat> sift_features_;
  /** Inner landmarks SIFT features */
  std::vector<cv::Mat> inner_sift_features_;
  /** Position where SIFT features will be extracted */
  std::vector<std::vector<cv::KeyPoint>> sift_extraction_location_;
  /** Sift descriptor parameters */
  SSift::SSiftParameters sift_desciptor_params_;
  /** Tracking quality assessment */
  LTS5::BaseFaceTrackerAssessment<cv::Mat,cv::Mat>* tracking_assessment_;
  /** Indicate if tracker need to take care of assessor or not */
  bool release_assessor_;

};

/**
 * @class   MSDMProxy
 * @brief   Interface for registration mechanism for face tracker type
 * @author  Christophe Ecabert
 * @date    14/08/17
 * @ingroup face_tracker
 */
class MSDMProxy : public MTrackerProxy {
 public:

  /**
   * @name  MSDMProxy
   * @fn    MSDMProxy()
   * @brief Constructor
   */
  MSDMProxy() = default;

  /**
   * @name  ~MSDMProxy
   * @fn    ~MSDMProxy() override
   * @brief Destructor
   */
  ~MSDMProxy() override;

  /**
   * @name  Create
   * @fn    MStreamFaceTracker<cv::Mat, cv::Mat>* Create(const int N) const override
   * @brief Create an instance of MStreamFaceTracker
   * @param[in] N number of stream
   * @return    Tracker instance with proper type
   */
  MStreamFaceTracker<cv::Mat, cv::Mat>* Create(const int N) const override;

  /**
   *  @name Name
   *  @fn const char* Name() const override
   *  @brief  Return the name for a given type of tracker
   *  @return Tracker name (i.e. sdm, lbf, ...)
   */
  const char* Name() const override;
};

}  // namepsace LTS5
#endif //__MSTREAM_SDM_TRACKER__
