/**
 *  @file   sdm_trainer.hpp
 *  @brief  SDM trainer class definition
 *  @ingroup face_tracker
 *
 *  @author Gabriel Cuendet
 *  @date   24/04/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_SDM_TRAINER__
#define __LTS5_SDM_TRAINER__

#include <stdio.h>
#include <time.h>
#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/features2d.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/object_type.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/base_tracker_quality_assessment_trainer.hpp"
#include "lts5/face_detector.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  SdmTrainer
 *  @brief  SDM-facetracker training implementation
 *  @author Hua Gao / Gabriel Cuendet
 *  @date   24/04/15
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS SdmTrainer {
 public :

  /** MultiArray similar to boost::multi_array<T> */
  template<typename T>
  using MultiArray = std::vector<std::vector<T>>;

#pragma mark -
#pragma mark Static methods
  /**
   *  @name       PruneDetections
   *  @fn static cv::Rect PruneDetections(const cv::Mat& annotation,
                                          const std::vector<cv::Rect>& detections)
   *  @brief      Amongst (potentially) several detections in an image,
   *              returns the one which center is closest to the annotation
   *              center
   *  @param[in]  annotation      Annotation
   *  @param[in]  detections      Vector of Rect returned by the face detector
   *  @return     Detection which center is closest to the annotation center
   *              or an empty Rect
   */
  static cv::Rect PruneDetections(const cv::Mat& annotation,
                                  const std::vector<cv::Rect>& detections);

  /**
   *  @name   DetectInImages
   *  @fn static int DetectInImages(LTS5::BaseFaceDetector* face_detector,
                                    std::vector<std::string>* images_files,
                                    std::vector<bool>* all_flip,
                                    std::vector<cv::Mat>* all_annotations,
                                    std::vector<cv::Size>* images_sizes,
                                    std::vector<cv::Rect>* faces_rectangles)
   *  @brief  Detect the objects in all images, from a list of files. Remove
   *          the filename from the list if no object is detected in the image
   *          and (optional) loads annotations
   *  @return -1 if error, 0 otherwise
   */
  static int DetectInImages(LTS5::BaseFaceDetector* face_detector,
                            std::vector<std::string>* images_files,
                            std::vector<bool>* all_flip,
                            std::vector<cv::Mat>* all_annotations,
                            std::vector<cv::Size>* images_sizes,
                            std::vector<cv::Rect>* faces_rectangles);

  /**
   *  @name   LoadAnnotations
   *  @fn static int LoadAnnotations(const std::vector<std::string>& images_files,
                                     const std::vector<bool>& all_flip,
                                     std::vector<cv::Mat>* all_annotations)
   *  @brief  Loads the annotations from the list of image files
   *          (image_files_).
   *  @return -1 if error, 0 otherwise
   */
  static int LoadAnnotations(const std::vector<std::string>& images_files,
                             const std::vector<bool>& all_flip,
                             std::vector<cv::Mat>* all_annotations);

  /**
   *  @name   LoadAnnotations
   *  @fn static int LoadAnnotations(const std::unordered_set<int>& indices,
                                     const std::vector<std::string>& images_files,
                                     const std::vector<bool>& all_flip,
                                     std::vector<cv::Mat>* all_annotations)
   *  @brief  Loads the annotations from the list of image files
   *          (image_files_). Loads ONLY the indices specified in indices!
   *  @return
   */
  static int LoadAnnotations(const std::unordered_set<int>& indices,
                              const std::vector<std::string>& images_files,
                              const std::vector<bool>& all_flip,
                              std::vector<cv::Mat>* all_annotations);

  /**
   *  @name   ComputeDetectionsStatistics
   *  @fn static int ComputeDetectionsStatistics(const std::vector<cv::Rect>& detections,
                                                 const std::vector<cv::Mat>& annotations,
                                                 const cv::Mat& mean_shape,
                                                 cv::Mat* means, cv::Mat* stds)
   *  @brief  Compute the means and standard deviations of the difference
   *          between the annotations and the face detector along scale, x and
   *          y dimensions
   *  @param[in]      detections   Detected rectangles
   *  @param[in]      annotations  Annotated shapes
   *  @param[in]      mean_shape   Mean shape of the detections
   *  @param[out]     means        Mean along each of the three dimensions:
   *                               scale, x-dimension and y-dimension
   *  @param[out]     stds         Standard deviations along each of the three
   *                               dimensions: scale, x-dimension and
   *                               y-dimension.
   *  @return
   */
  static int ComputeDetectionsStatistics(const std::vector<cv::Rect>& detections,
                                         const std::vector<cv::Mat>& annotations,
                                         const cv::Mat& mean_shape,
                                         cv::Mat* means, cv::Mat* stds);

  /**
   *  @name   SampleMonteCarloRectangles
   *  @fn static int SampleMonteCarloRectangles(const cv::Vec3d& stds,
                                                const int& n_samples,
                                                const std::vector<cv::Rect>& rectangles,
                                                const std::vector<cv::Size>& images_sizes,
                                                const int& max_iter,
                                                const int& init_random,
                                                MultiArray<cv::Rect>*
                                                montecarlo_rects)
   *  @brief
   *  @param[in]  stds             Vec3d containing the standard deviation
   *                                   of the gaussian distributions from
   *                                   which to sample:
   *                                       1) the scale variation,
   *                                       2) the x-offset and
   *                                       3) the y-offset
   *  @param[in]  n_samples        Number of samples to generate for each rect
   *  @param[in]  rectangles       Input rectangles
   *  @param[in]  images_sizes     Sizes of all images
   *  @param[in]  max_iter         Maximum number of invalid samples
   *                                   (rect outside image) before replacing
   *                                   by the mean of the distribution
   *  @param[in]  init_random      Integer to initialize the RNG (true random
   *                               if it is -1)
   *  @param[out] montecarlo_rects
   */
  static void SampleMonteCarloRectangles(const cv::Vec3d& stds,
                                         const int& n_samples,
                                         const std::vector<cv::Rect>& rectangles,
                                         const std::vector<cv::Size>& images_sizes,
                                         const int& max_iter,
                                         const int& init_random,
                                         MultiArray<cv::Rect>* montecarlo_rects);

#pragma mark -
#pragma mark Constructors and desrtuctor

  /**
   *  @name   SdmTrainer
   *  @fn SdmTrainer()
   *  @brief  Constructor
   */
  SdmTrainer();

  /**
   *  @name   SdmTrainer
   *  @fn explicit SdmTrainer(const std::string& face_detector_config)
   *  @brief  Constructor
   */
  explicit SdmTrainer(const std::string& face_detector_config);

  /**
   *  @name   ~SdmTrainer
   *  @fn ~SdmTrainer()
   *  @brief  Destructor
   */
  ~SdmTrainer();

#pragma mark -
#pragma mark Getters and Setters
  /**
   *  @name   face_detector
   *  @fn LTS5::BaseFaceDetector* face_detector()
   *  @brief  Face detector getter
   */
  LTS5::BaseFaceDetector* face_detector();

  /**
   *  @name   set_face_detector
   *  @fn void set_face_detector(LTS5::BaseFaceDetector* face_detector)
   *  @brief  Face detector setter
   */
  void set_face_detector(LTS5::BaseFaceDetector* face_detector);

  /**
   *  @name   set_training_parameters
   *  @fn void set_training_parameters(const LTS5::SdmTracker::SdmTrackerParameters&
                                       train_parameters)
   *  @brief  Allow user to change training parameters
   *  @param[in]  train_parameters  Training parameters
   */
  void set_training_parameters(const LTS5::SdmTracker::SdmTrackerParameters&
                               train_parameters) {
    tracker_parameters_ = train_parameters;
  }

  /**
   *  @name   set_tracking_assessment
   *  @fn void set_tracking_assessment(const BaseTrackerQualityAssessmentTrainer*
                                       trainer)
   *  @brief  Set the type of quality assessment to train
   *  @param  trainer  TrackerQualityAssessmentTrainer object
   */
  void set_tracking_assessment(const BaseTrackerQualityAssessmentTrainer*
                               trainer) {
    tracking_assessment_trainer_ =
     const_cast<BaseTrackerQualityAssessmentTrainer*>(trainer);
    release_assessor_ = false;
  }

#pragma mark -
#pragma mark Train and Save methods

  /**
   *  @name   Save
   *  @fn int Save(const std::string& model_name)
   *  @brief  Save the trained model in file. This must be override by subclass
   *  @param  [in]    model_name      Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  int Save(const std::string& model_name);

  /**
   *  @name   Save
   *  @fn int Save(std::ostream& output_stream)
   *  @brief  Save the trained model in file.
   *  @param  output_stream Stream to a given binary file
   *  @return -1 if error, 0 otherwise
   */
  int Save(std::ostream& output_stream);

  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder)
   *  @brief  SDM training method
   *  @param[in]  folder  Location of images/annotations
   */
  void Train(const std::string& folder);

  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder,
                     const LTS5::HeaderObjectType& assessment_type)
   *  @brief  SDM training method
   *  @param[in]  folder  Location of images/annotations
   *  @param[in]  assessment_type  Type of assessment to train
   */
  void Train(const std::string& folder,
             const LTS5::HeaderObjectType& assessment_type);

  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder, const bool& flip_images,
                     const std::unordered_set<int>& indices,
                     const LTS5::HeaderObjectType& assessment_type)
   *  @brief  SDM training method
   *  @param[in]  folder        Location of images/annotations
   *  @param[in]  flip_images   Wheter or not to flip the images
   *  @param[in]  indices       Shape indices to take into account from
   *                            the annotations.
   *  @param[in]  assessment_type   Type of the assessment to train
   */
  void Train(const std::string& folder, const bool& flip_images,
             const std::unordered_set<int>& indices,
             const LTS5::HeaderObjectType& assessment_type);

#pragma mark -
#pragma mark Utility methods

  /**
   *  @name   ConvertModelCharToBin
   *  @fn static int ConvertModelCharToBin(const std::string& sdm_model_name,
                                           const bool has_pose_estmimation_model,
                                           const std::string& tracking_assessment_model,
                                           const double& score_threshold,
                                           const LTS5::SdmTracker::SdmTrackerParameters& tracker_config)
   *  @brief  Convert old char model into new binary one
   *  @param[in]  sdm_model_name              Char sdm model
   *  @param[in]  has_pose_estmimation_model  3d pdm is included or not ?
   *  @param[in]  tracking_assessment_model   Tracker quality assessment model
   *  @param[in]  score_threshold             Classifier threshold
   *  @param[in]  tracker_config              Tracker configuration used in
   *                                          training
   *  @return   -1 if error, 0 otherwise
   */
  static int ConvertModelCharToBin(const std::string& sdm_model_name,
                                   const bool has_pose_estmimation_model,
                                   const std::string& tracking_assessment_model,
                                   const double& score_threshold,
                                   const LTS5::SdmTracker::SdmTrackerParameters& tracker_config);

#pragma mark -
#pragma mark Private methods

 private:
  /**
   *  @name   ComputeInitialShapes
   *  @fn int ComputeInitialShapes(const MultiArray<cv::Rect>&
                                   montecarlo_rects)
   *  @brief Given a rectangle and the mean shape, initialize the shape
   *  @param[in]  montecarlo_rects  Rectangles sampled arounf face detected
   */
  void ComputeInitialShapes(const MultiArray<cv::Rect>& montecarlo_rects);

  /**
   *  @name   ComputeSIFT
   *  @fn void ComputeSIFTAndDeltaX(const int& stage, cv::Mat* all_delta_x,
                                    std::vector<cv::Mat>* all_transforms,
                                    cv::Mat* sift_features)
   *  @brief  Compute SIFT features from the images and the sampled rectangles
   *  @param[out] all_delta_x     Differences between shape and annotations
   *  @param[out] all_transforms  Vector of transforms for each image
   *  @param[out] sift_features   Features vector
   *  @param[in]  stage           Current stage
   */
  void ComputeSIFTAndDeltaX(const int& stage, cv::Mat* all_delta_x,
                            std::vector<cv::Mat>* all_transforms,
                            cv::Mat* sift_features);

  /**
   *  @name   ComputeSIFTPca
   *  @fn void ComputeSIFTPca(const cv::Mat& sift, cv::Mat* pca_sift)
   *  @brief  Compute PCA on the SIFT features AND project SIFT on the PCA basis
   *  @param[in]    sift        sift features
   *  @param[out]   pca_sift    Resulting projection
   */
  void ComputeSIFTPca(const cv::Mat& sift, cv::Mat* pca_sift);

  /**
   *  @name   ProjectSIFTOnPca
   *  @fn void ProjectSIFTOnPca(const cv::Mat& sift, cv::Mat* pca_sift)
   *  @brief  Project the SIFT features on the PCA basis
   *  @param[in]    sift features matrix (can be BIG!)
   *  @param[out]   pca_sift features projected on PCA basis
   */
  void ProjectSIFTOnPca(const cv::Mat& sift, cv::Mat* pca_sift);

# pragma mark -
# pragma mark Private attributes

  /** Tracker parameters */
  LTS5::SdmTracker::SdmTrackerParameters tracker_parameters_;
  /** SDM-facetracker model */
  std::vector<cv::Mat> sdm_model_;
  /** Viola-Jones face detector*/
  LTS5::BaseFaceDetector* face_detector_;
  /** Number of valid images */
  int n_images_;
  /** Number of shape's points */
  int n_shape_points_;
  /** List of path to the images */
  std::vector<std::string> images_files_;
  /** Vector indicating if the image should be flipped or not */
  std::vector<bool> all_flip_;
  /** Set of points to take into account from the annotations */
  std::unordered_set<int> shape_indices_;
  /** Sizes of all the images in images_files_ */
  std::vector<cv::Size> images_sizes_;
  /** Postion of the detected face */
  std::vector<cv::Rect> faces_rectangles_;
  /** Annotations corresponding to images in images_files_ */
  std::vector<cv::Mat> all_annotations_;
  /** Intermediary shapes at STAGE */
  std::vector<cv::Mat> all_shapes_;
  /** Mean shape */
  cv::Mat mean_shape_;
  /** PCA eigenvectors computed from SIFT features */
  cv::Mat pca_basis_;
  /** Tracker quality Assessment Trainer */
  BaseTrackerQualityAssessmentTrainer* tracking_assessment_trainer_;
  /** Indicate if trainer needs to take care of assessor or not */
  bool release_assessor_;
};
}  // namespace LTS5
#endif /* __LTS5_SDM_TRAINER__ */
