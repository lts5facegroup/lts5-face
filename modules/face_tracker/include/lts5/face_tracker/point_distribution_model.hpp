/**
 *  @file   point_distribution_model.hpp
 *  @brief  Point distribution model definition
 *  @ingroup face_tracker
 *
 *  @author Hua Gao
 *  @date   30/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#ifndef __LTS5_POINT_DISTRIBUTION_MODEL__
#define __LTS5_POINT_DISTRIBUTION_MODEL__

#include <fstream>
#include <string>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  PointDistributionModel
 *  @brief  PDM implementation
 *  @author Hua Gao
 *  @date   30/04/15
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS PointDistributionModel {
 public:

#pragma mark -
#pragma mark Type definition

  /**
   *  @struct PDMParameters
   *  @brief  Structure encapsulating PDM parameters
   */
  struct PDMParameters {
    /** Normalize image width */
    int normalize_image_width;
    /** Normalize image height */
    int normalize_image_height;
    /** Normalize face scale */
    double normalize_face_scale;
    /** Extra shift */
    double extra_face_shift_x;
    /** Extra shift */
    double extra_face_shift_y;

    /**
     *  @name   PDMParameters
     *  @fn PDMParameters(void)
     *  @brief  Constructor
     */
    PDMParameters(void) {
      normalize_image_width = 200;
      normalize_image_height = 200;
      normalize_face_scale = 4.0;
      extra_face_shift_x = 0.0;
      extra_face_shift_y = 0.0;
    }

    /**
     *  @name   ComputeHeaderSize
     *  @fn int ComputeObjectSize(void) const
     *  @brief  Compute the size of the header in byte
     *  @return Length in bytes
     */
    int ComputeObjectSize(void) const {
      //NOTE Leave it like that to be compliant with old model
      int size = sizeof(normalize_image_width);
      size += sizeof(normalize_face_scale);
      return size;
    }

    /**
     *  @name WriteParameterHeader
     *  @fn int WriteParameterHeader(std::ostream& output_stream) const
     *  @brief  Dump tracker parameters into a given stream.
     *  @param[in]  output_stream Stream where data will be written
     *  @return -1 If error, 0 otherwise
     */
    int WriteParameterHeader(std::ostream& output_stream) const {
      int error = -1;
      //NOTE Leave it like that to be compliant with old model
      if (output_stream.good()) {
        output_stream.write(reinterpret_cast<const char*>(&normalize_image_width),
                            sizeof(normalize_image_width));
        output_stream.write(reinterpret_cast<const char*>(&normalize_face_scale),
                            sizeof(normalize_face_scale));
        // Done, sanity check
        error = output_stream.good() ? 0 : -1;
      }
      return error;
    }

    /**
     *  @name   ReadParameterHeader
     *  @fn int ReadParameterHeader(std::istream& input_stream)
     *  @brief  Initialize parameters structures from input stream
     *  @param[in]  input_stream  Stream from where to read data
     *  @return -1 If error, 0 otherwise
     */
    int ReadParameterHeader(std::istream& input_stream) {
      int error = -1;
      if (input_stream.good()) {
        input_stream.read(reinterpret_cast<char*>(&normalize_image_width),
                          sizeof(normalize_image_width));
        normalize_image_height = normalize_image_width;
        input_stream.read(reinterpret_cast<char*>(&normalize_face_scale),
                          sizeof(normalize_face_scale));
        // Done, sanity check
        error = input_stream.good() ? 0 : -1;
      }
      return error;
    }
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   PointDistributionModel
   *  @fn PointDistributionModel()
   *  @brief  Constructor
   */
  PointDistributionModel();

  /**
   *  @name  PointDistributionModel
   *  @fn explicit PointDistributionModel(const std::string& model_name)
   *  @brief Constructor
   *  @param[in] model_name file name of the model
   */
  explicit PointDistributionModel(const std::string& model_name);

  /**
   *  @name   PointDistributionModel
   *  @fn PointDistributionModel(const PointDistributionModel& other)
   *  @brief  Copy constructor
   */
  PointDistributionModel(const PointDistributionModel& other);

  /**
   *  @name   operator=
   *  @fn PointDistributionModel& operator=(const PointDistributionModel& other)
   *  @brief  Assignment operator
   *  @return Copy of the object
   */
  PointDistributionModel& operator=(const PointDistributionModel& other);

  /**
   *  @name   ~PointDistributionModel
   *  @fn ~PointDistributionModel()
   *  @brief  Destructor
   */
  ~PointDistributionModel();

  /**
   *  @name   Load
   *  @fn int Load(const std::string& model_name)
   *  @brief  Load model data from file
   *  @param[in] model_name file name of the model
   *  @return -1 if error, 0 otherwise
   */
  int Load(const std::string& model_name);

  /**
   *  @name   Load
   *  @fn int Load(std::istream& input_stream, const bool is_binary)
   *  @brief  Load model data from stream
   *  @param[in]  input_stream  input file stream
   *  @param[in]  is_binary     Binary input stream
   *  @return -1 if error, 0 otherwise
   */
  int Load(std::istream& input_stream, const bool is_binary);

  /**
   *  @name   Write
   *  @fn int Write(std::ofstream& out_stream) const
   *  @brief  Write the object into a binary stream
   *  @param[in]  out_stream  Binary stream to files
   *  @return -1 if error, 0 otherwise.
   */
  int Write(std::ofstream& out_stream) const;

  /**
   *  @name   ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the memory used by object
   *  @return Size in bytes
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   CalcParams
   *  @fn cv::Mat CalcParams(const cv::Mat &s, cv::Mat* p_local, cv::Mat* p_globl)
   *  @brief  Calculate rigid and non-rigid model parameters given 2D shape
   *  @param[in] s         input 2D shape vector
   *  @param[out] p_local  non-rigid shape parameter
   *  @param[out] p_globl  rigid shape parameter
   *  @return mat
   */
  cv::Mat CalcParams(const cv::Mat &s, cv::Mat* p_local, cv::Mat* p_globl);

  /**
   *  @name   CalcShape2D
   *  @fn void CalcShape2D(const cv::Mat &p_local, const cv::Mat &p_globl, cv::Mat* s)
   *  @brief  Calculate 2D shape from given rigid and non-rigid parameters
   *  @param[in]  p_local  non-rigid shape parameter
   *  @param[in]  p_globl  rigid shape parameter
   *  @param[out] s        output 2D shape vector
   */
  void CalcShape2D(const cv::Mat &p_local, const cv::Mat &p_globl, cv::Mat* s);

  /**
   *  @name   CalcPoseAngle
   *  @fn void CalcPoseAngle(const cv::Mat& s,
                             double* pitch,
                             double* yaw,
                             double* roll)
   *  @brief  Calculate 3D head pose angles with given 2D shape
   *  @param[in]  s      2D shape vector
   *  @param[out] pitch  pitch angle
   *  @param[out] yaw    yaw angle
   *  @param[out] roll   roll angle
   */
  void CalcPoseAngle(const cv::Mat& s, double* pitch, double* yaw,
                     double* roll);

  /**
   *  @name  NormSDMImage
   *  @fn void NormSDMImage(const cv::Mat& image,
                            const cv::Mat& shape,
                            cv::Mat* norm_image,
                            cv::Mat* norm_shape)
   *  @brief Normalize face image (scaling, traslation, rotation)
   *  @param[in]  image             input image
   *  @param[in]  shape             input shape
   *  @param[in]  only_inner_shape  If true normalize only the inner shape
   *  @param[out] norm_shape  normalized shape
   *  @param[out] norm_image  normalized image
   */
  void NormSDMImage(const cv::Mat& image,
                    const cv::Mat& shape,
                    const bool only_inner_shape,
                    cv::Mat* norm_image,
                    cv::Mat* norm_shape);

  void NormSDMImage_intra(const cv::Mat& image,
                           const cv::Mat& shape,
                           cv::Mat* norm_image,
                           cv::Mat* norm_shape);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_pdm_configuration
   *  @fn void set_pdm_configuration(const PDMParameters& pdm_configuration)
   *  @brief  Set a new PDM configuration
   *  @param[in] pdm_configuration New PDM configuration
   */
  void set_pdm_configuration(const PDMParameters& pdm_configuration) {
    pdm_configuration_ = pdm_configuration;
    // Parameter change, reset reference shape
    reference_shape_.release();
  }

  /**
   *  @name   get_num_points
   *  @fn int get_num_points() const
   *  @brief  Get number of points in the model
   *  @return value
   */
  int get_num_points() const {
    return num_points_;
  };

  /**
   *  @name   get_pdm_configuration
   *  @fn const PDMParameters& get_pdm_configuration(void) const
   *  @brief  Provide PDM configuration
   *  @return PDM configuration
   */
  const PDMParameters& get_pdm_configuration(void) const {
    return pdm_configuration_;
  }

 private:

  /**
   *  @name TransRefShape
   *  @fn void TransRefShape(cv::Mat* shape)
   *  @brief Translate shape by removing min_x/min_y (bring to zero)
   *  @param[in,out]  shape   Shape
   */
  void TransRefShape(cv::Mat* shape);

  /**
   *  @name   EstimateSimilarityTransform_refactored
   *  @fn void EstimateSimilarityTransform(const cv::Mat& src_pts,
                                           const cv::Mat& dst_pts,
                                           cv::Mat* transformation)
   *  @brief  Compute similarity transform between two sets of points
   *  @param[in]  src_pts           Source points, [1xN] or [Nx1]
   *  @param[in]  dst_pts           Destination points, [1xN] or [Nx1]
   *  @param[out] transformation    Transformation between src/dst
   */
  void EstimateSimilarityTransform(const cv::Mat& src_pts,
                                   const cv::Mat& dst_pts,
                                   cv::Mat* transformation);

  /**  eigen vector of the shape model */
  cv::Mat eigen_vectors_;
  /**  mean shape */
  cv::Mat means_;
  /**  eigen value of the shape model */
  cv::Mat eigen_values_;
  /**  number of points in the shape model */
  int num_points_;
  /**  rigid shape parameter */
  cv::Mat p_globl_;
  /**  non-rigid shape parameter */
  cv::Mat p_local_;
  /** PDM Parameters */
  PDMParameters pdm_configuration_;
  /** Reference shape */
  cv::Mat reference_shape_;
};
}  // namespace LTS5
#endif /* __LTS5_POINT_DISTRIBUTION_MODEL__ */
