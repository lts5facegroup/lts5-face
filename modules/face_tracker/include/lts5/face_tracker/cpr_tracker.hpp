/**
 *  @file   cpr_tracker.hpp
 *  @brief  CPR tracker class definition
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   09/11/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_CPR_TRACKER__
#define __LTS5_CPR_TRACKER__

#include <iostream>
#include <random>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_tracker/base_face_tracker.hpp"
#include "lts5/face_detector/base_face_detector.hpp"
#include "lts5/regressor/cascaded_random_fern.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  CPRTracker
 *  @brief  Face tracker based on Cascaded Pose Regressor
 *  @author Christophe Ecabert
 *  @date   09/11/15
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS CPRTracker : public BaseFaceTracker<cv::Mat, cv::Mat> {

#pragma mark -
#pragma mark  Initialization
 public :
  /**
   *  @name   CPRTracker
   *  @fn CPRTracker(void)
   *  @brief  Constructor
   */
  CPRTracker(void);

  /**
   *  @name   ~CPRTracker
   *  @fn ~CPRTracker(void)
   *  @brief  Destructor
   */
  ~CPRTracker(void);

  /**
   *  @name   Load
   *  @fn int Load(const std::string& model_filename)
   *  @brief  Load the tracker model.
   *  @param[in]  model_filename      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename);

  /**
   *  @name   Load
   *  @fn int Load(std::istream& input_stream)
   *  @brief  Load the tracker model.
   *  @param[in]  input_stream      Stream from where to load the object
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& input_stream);

  /**
   *  @name   Load
   *  @fn int Load(const std::string& model_filename,
                   const std::string& f_detector_config)
   *  @brief  Load the tracker model.
   *  @param[in]  model_filename      Path to the model
   *  @param[in]  f_detector_config   Path to the face detector configuration
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename,
           const std::string& f_detector_config);

  /**
   *  @name   Load
   *  @fn int Load(std::istream& input_stream,
                   const std::string& f_detector_config)
   *  @brief  Load the tracker model.
   *  @param[in]  input_stream      Stream from where to load model
   *  @param[in]  f_detector_config Path to the face detector configuration
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& input_stream,
           const std::string& f_detector_config);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& model_name)
   *  @brief  Save the trained model in file.
   *  @param[in] model_name   Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  int Save(const std::string& model_name);

  /**
   *  @name   Write
   *  @fn int Write(std::ostream& out_stream)
   *  @brief  Write the trained model into a given stream.
   *  @param[in] out_stream   Stream where to write the object
   *  @return -1 in case of error, otherwise 0
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name   ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the memory used by object
   *  @return Size in bytes
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Process

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image, cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Detect(const cv::Mat& image, cv::Mat* shape);

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image,
                     const cv::Rect& face_region,
                     cv::Mat* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face boundingg box
   *  @param[out] shape           Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Detect(const cv::Mat& image,
             const cv::Rect& face_region,
             cv::Mat* shape);

  /**
   *  @name   Track
   *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
   *  @brief  Tracking method, given an input image (from a sequence), provides
   *          a set of landmarks.
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Track(const cv::Mat& image, cv::Mat* shape);

  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder)
   *  @brief  Tracker training method. This must be override by subclass
   *  @param[in]  folder  Location of images/annotations
   */
  void Train(const std::string& folder);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name set_face_detector
   *  @fn void set_face_detector(const LTS5::BaseFaceDetector& f_detector)
   *  @brief  Set new face detector
   *  @param[in]  f_detector  New face detector to use
   */
  void set_face_detector(const LTS5::BaseFaceDetector& f_detector) {
    if (f_detector_ != nullptr) {
      delete f_detector_;
    }
    f_detector_ = const_cast<LTS5::BaseFaceDetector*>(&f_detector);
    own_detector_ = false;
  }

#pragma mark -
#pragma mark Utility

  /**
   *  @name DrawShape
   *  @fn static cv::Mat DrawShape(const cv::Mat& image, const cv::Mat& shape)
   *  @brief  Draw shape on image
   *  @param[in]  image   Image to draw on
   *  @param[in]  shape   Shape to draw
   *  @return Canvas with image and shape on it
   */
  static cv::Mat DrawShape(const cv::Mat& image, const cv::Mat& shape);

#pragma mark -
#pragma mark Private
 private :

  /**
   *  @name AlignShape
   *  @fn void AlignShape(cv::Mat* shape)
   *  @brief  Align shape
   *  @param[out] shape Aligned shape
   */
  void AlignShape(cv::Mat* shape);



  /** Working image */
  cv::Mat working_image_;
  /** Tracking flag */
  bool is_tracking_;
  /** Meanshape */
  cv::Mat mean_shape_;
  /** Number of points */
  int n_points_;
  /** Initial shape */
  cv::Mat initial_shape_;
  /** Cascaded Fern */
  std::vector<LTS5::CascadedRandomFern*> cascaded_fern_;
  /** Face detector */
  LTS5::BaseFaceDetector* f_detector_;
  /** Detector owner */
  bool own_detector_;
  /** Shape center gravity */
  cv::Point2d shape_center_gravity_;
  /** Face region */
  cv::Rect face_region_;
  /** Unifrom distribution */
  std::uniform_int_distribution<int> uni_dist_;
  /** Number of initialization */
  int n_initialization_;
  /** Landmarks */
  cv::Mat current_shape_;
  /** Previous shape */
  cv::Mat previous_shape_;

};
}  // namespace LTS5
#endif /* __LTS5_CPR_TRACKER__ */
