/**
 *  @file   cpr_trainer.hpp
 *  @brief  CPR trainer class definition
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   30/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_CPR_TRAINER__
#define __LTS5_CPR_TRAINER__

#include <vector>
#include <string>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/regressor/cascaded_random_fern_trainer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  CPRTrainer
 *  @brief  Cascaded Pose Regressor (CPR) training class
 *  @author Christophe Ecabert
 *  @date   30/10/15
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS CPRTrainer {

 public :

#pragma mark -
#pragma mark Initialization

  /**
   *  @name CPRTrainer
   *  @fn CPRTrainer(const std::string& f_detector_config)
   *  @brief  Constructor
   *  @param[in]  f_detector_config Face detector config
   */
  explicit CPRTrainer(const std::string& f_detector_config);

  /**
   *  @name ~CPRTrainer
   *  @fn ~CPRTrainer(void)
   *  @brief  Destructor
   */
  ~CPRTrainer(void);

#pragma mark -
#pragma mark Train

  /**
   *  @name Train
   *  @fn int Train(const std::string& data_folder,
                   const int n_fern_fst_lvl,
                   const int n_fern_sec_lvl,
                   const int n_candidate_pixel,
                   const int n_feat_fern,
                   const double shrinkage_coef,
                   const int n_data_augmentation)
   *  @brief  Train CPR face tracker
   *  @param[in]  data_folder         Root folder where image and annotation are
   *                                  stored
   *  @param[in]  n_fern_fst_lvl      Number of cascaded random fern to train
   *  @param[in]  n_fern_sec_lvl      Number of fern in each cascade
   *  @param[in]  n_candidate_pixel   Feature selection pool size
   *  @param[in]  n_feat_fern         Number of feature to select
   *  @param[in]  shrinkage_coef      Shrinkage coefficient (fern training)
   *  @param[in]  n_data_augmentation How many time samples are augmented
   *  @return -1 if error, 0 otherwise
   */
  int Train(const std::string& data_folder,
            const int n_fern_fst_lvl,
            const int n_fern_sec_lvl,
            const int n_candidate_pixel,
            const int n_feat_fern,
            const double shrinkage_coef,
            const int n_data_augmentation);

  /**
   *  @name Save
   *  @fn int Save(const std::string& filename) const
   *  @brief  Save Random fern cascade into a given file
   *  @param[in]  filename  File where to dump object
   *  @return -1 if error, 0 otherwise
   */
  int Save(const std::string& filename) const;

  /**
   *  @name Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Write Random fern cascade into a given stream
   *  @param[in]  out_stream  Stream where to dump object
   *  @return -1 if error, 0 otherwise
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the size (in byte) of memory used by the object
   */
  int ComputeObjectSize(void) const;


#pragma mark -
#pragma mark Private

 private :

  /** Face detector */
  std::string face_detector_config_;
  /** List of images */
  std::vector<std::string> image_list_;
  /** List of annotation file */
  std::vector<std::string> annotation_list_;
  /** Ground truth */
  std::vector<cv::Mat> ground_truth_;
  /** Face bounding boxes */
  std::vector<cv::Rect> face_bbox_;
  /** Cascade trainer */
  std::vector<LTS5::CascadedRandomFernTrainer*> fern_cascade_;
  /** Mean shape */
  cv::Mat mean_shape_;
  /** Augmented shape */
  cv::Mat augmented_ground_truth_;
  /** Augemented image list */
  std::vector<std::string> augmented_image_list_;
  /** Augemented bbox */
  std::vector<cv::Rect> augmented_face_bbox_;
  /** Current shapes */
  cv::Mat current_shape_;

  /**
   *  @name
   *  @fn LoadAnnotation(std::vector<cv::Mat>* annotations,
                         std::vector<cv::Mat>* centroids)
   *  @brief  Load annotation and compute centroid
   *  @param[out] annotations Ground truth shape
   *  @param[out] centroids   Shape centroid
   *  @return -1 if error, 0 otherwise
   */
  int LoadAnnotation(std::vector<cv::Mat>* annotations,
                     std::vector<cv::Mat>* centroids);

  /**
   *  @name ComputeMeanShape
   *  @fn void ComputeMeanShape(void)
   *  @brief  Compute training meanshape
   *  @param[in]  ground_truth  Shape ground truth
   *  @param[in]  face_bbox     Face bounding box
   */
  void ComputeMeanShape(const std::vector<cv::Mat>& ground_truth,
                        const std::vector<cv::Rect>& face_bbox);

  /**
   *  @name GenerateFaceboundingBox
   *  @fn int GenerateFaceboundingBox(std::vector<cv::Mat>* annotations,
                                       std::vector<cv::Mat>* centroids,
                                       std::vector<cv::Rect>* face_bbox)
   *  @brief  Run face detector to generate face bounding boxes
   *  @param[in,out]  annotations   list
   *  @param[in,out]  centroids     Annotation centroids
   *  @param[out]     face_bbox     Face bounding box
   *  @return -1 if error, 0 otherwise
   */
  int GenerateFaceboundingBox(std::vector<cv::Mat>* annotations,
                              std::vector<cv::Mat>* centroids,
                              std::vector<cv::Rect>* face_bbox);

};
}  // namespace LTS5
#endif /* __LTS5_CPR_TRAINER__ */
