/**
 *  @file   lbf_trainer.cpp
 *  @brief  Training class for LBF face tracker
 *
 *  @author Guillaume Jaume, Christophe Ecabert
 *  @date   27/02.2017
 *  Copyright (c) 2017 Guillaume Jaume. All rights reserved.
 */

#include <iostream>
#include <vector>
#include <chrono>
#include <numeric>
#include <algorithm>
#include <random>

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include "linear_lbf.h"

#include "lts5/face_tracker/lbf_trainer.hpp"
#include "lts5/face_tracker/lbf_trainer_pimpl.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/face_detector/cascade_face_detector.hpp"
#include "lts5/face_tracker/lbf_tracker_assessment_trainer.hpp"

namespace LTS5 {

/**
 * @name    ApplyPermutation
 * @brief   Apply a given permutation to a vector
 * @param[in] perm  Permutation
 * @param[in,out] vector    Vector to permutate
 */
template <typename T>
void ApplyPermutation(const std::vector<std::size_t>& perm,
                      std::vector<T>* vector) {
  std::vector<bool> done(vector->size());
  for (std::size_t i = 0; i < vector->size(); ++i) {
    if (done[i]) {
      continue;
    }
    done[i] = true;
    std::size_t prev_j = i;
    std::size_t j = perm[i];
    while (i != j) {
      std::swap(vector->at(prev_j), vector->at(j));
      done[j] = true;
      prev_j = j;
      j = perm[j];
    }
  }
}

#pragma mark -
#pragma mark Initialization

/*
 * @name  RandomTreeTrainer
 * @fn    RandomTreeTrainer(void);
 * @brief Constructor
 */
RandomTreeTrainer::RandomTreeTrainer(void) : landmark_id_(0) {
}

/*
 * @name  ~RandomTreeTrainer
 * @fn    ~RandomTreeTrainer(void);
 * @brief Destructor
 */
RandomTreeTrainer::~RandomTreeTrainer(void) {
}

/*
 * @name Init
 * @brief Initialize Tree
 * @param[in] landmark_id     Id of the corresponding landmark [0, 67]
 * @param[in] params          User defined parameters
 */
void RandomTreeTrainer::Init(const int landmark_id,
                             const Parameters& params) {
  // Copy parameters
  this->params_ = params;
  // Setup landmark ID + depth + feats + thresh
  this->landmark_id_ = landmark_id;
  int n_nodes = 1 << params_.tree_depth;
  this->feats_ = cv::Mat(n_nodes, 4, CV_64FC1, cv::Scalar_<double>(0.0));
  this->thresholds_.resize(static_cast<size_t>(n_nodes));
}

#pragma mark -
#pragma mark Usage

/*
 * @name Train
 * @brief train each tree by constructing the optimal split in the tree
 * @param[in] ground_truth    List of samples
 * @param[in] current_shapes  List of current shape
 * @param[in] delta_shapes    List of ideal delta shape for each image at
 *                            each stage
 * @param[in] mean_shape      Normalized mean shape
 * @param[in] index           ?
 * @param[in] stage           stage i
 */
void RandomTreeTrainer::Train(const std::vector<GroundTruth>& ground_truth,
                              const std::vector<cv::Mat>& current_shapes,
                              const std::vector<cv::Mat>& delta_shapes,
                              const cv::Mat& mean_shape,
                              const std::vector<int>& index,
                              const int stage) {
  // Compute delta_shape for this specific landmark (ID)
  int n_landmark = std::max(mean_shape.rows, mean_shape.cols) / 2;
  int n_shape = static_cast<int>(delta_shapes.size());
  cv::Mat dx_landmark(n_shape, 2, CV_64FC1);
  for (int i = 0; i < n_shape; ++i) {
    const auto& shape = delta_shapes[i];
    dx_landmark.at<double>(i, 0) = shape.at<double>(landmark_id_);
    dx_landmark.at<double>(i, 1) = shape.at<double>(landmark_id_ + n_landmark);
  }
  this->SplitNode(ground_truth,
                  current_shapes,
                  dx_landmark,
                  mean_shape,
                  index,
                  1,
                  stage);
}

/*
 * @name SplitNode
 * @brief determime the optimal thrsh and pixel to take with respect to each
 *        landmark to decrease the variance (ie. split that classifies images in
 *        2 categories with max information gain )
 * @param ground_truth    List of samples
 * @param current_shapes  List of current shape
 * @param delta_shapes    List of ideal delta shape for each image at
 *                        each stage
 * @param mean_shape      Normalized mean shape
 * @param root            ?
 * @param idx             Index of the node under split
 * @param stage           Stage i
 */
void RandomTreeTrainer::SplitNode(const std::vector<GroundTruth>& ground_truth,
                                  const std::vector<cv::Mat>& current_shapes,
                                  const cv::Mat& delta_shapes,
                                  const cv::Mat& mean_shape,
                                  const std::vector<int>& root,
                                  const int idx,
                                  const int stage) {
  // Check if it reach the end of the recursion
  const int n_landmark = std::max(mean_shape.rows, mean_shape.cols) / 2;
  const auto N = root.size();
  if (N == 0) {
    thresholds_[idx] = 0;
    feats_.row(0).setTo(0.0);
    std::vector<int> left, right;
    // split left and right child in DFS
    if (2 * idx < feats_.rows / 2) {
      SplitNode(ground_truth,
                current_shapes,
                delta_shapes,
                mean_shape,
                left,
                2 * idx,
                stage);
    }
    if (2 * idx + 1 < feats_.rows / 2) {
      SplitNode(ground_truth,
                current_shapes,
                delta_shapes,
                mean_shape,
                right,
                2 * idx + 1,
                stage);
    }
  } else {
    // Feature sampling / selection
    const int n_sampling = this->params_.feats_m[stage];
    const double sampling_radius = this->params_.radius_m[stage];
    // Pool of features
    cv::Mat candidates(n_sampling, 4, CV_64FC1);
    cv::RNG rng(static_cast<uint64>(cv::getTickCount()));
    // generate feature pool /repeat process feat_m times
    for (int p = 0; p < n_sampling; ++p) {
      double x1, y1, x2, y2;
      x1 = rng.uniform(-1., 1.);
      y1 = rng.uniform(-1., 1.);
      x2 = rng.uniform(-1., 1.);
      y2 = rng.uniform(-1., 1.);
      if (x1 * x1 + y1 * y1 > 1. || x2 * x2 + y2 * y2 > 1.) {
        p--;
        continue;
      }
      candidates.at<double>(p, 0) = x1 * sampling_radius;
      candidates.at<double>(p, 1) = y1 * sampling_radius;
      candidates.at<double>(p, 2) = x2 * sampling_radius;
      candidates.at<double>(p, 3) = y2 * sampling_radius;
    }
    // calc features, pixel difference
    cv::Mat densities(n_sampling, static_cast<int>(N), CV_32SC1);
    cv::Mat transform;
    for (int i = 0; i < N; ++i) {
      const auto& root_idx = root[i];
      const auto& curr_shape = current_shapes[root_idx];
      // Load images
      const auto& sample = ground_truth[root_idx];
      cv::Mat curr_image = sample.image_patch;
      const double image_w = static_cast<double>(curr_image.cols - 1);
      const double image_h = static_cast<double>(curr_image.rows - 1);
      // Get similarity transform between the meanshape and the target sample
      ComputeSimilarityTransform(mean_shape, curr_shape, &transform);
      const double* t_ptr = reinterpret_cast<const double*>(transform.data);
      // Sample image with each potential candidate
      for (int c = 0; c < n_sampling; ++c) {
        const double x1 = candidates.at<double>(c, 0);
        const double y1 = candidates.at<double>(c, 1);
        const double x2 = candidates.at<double>(c, 2);
        const double y2 = candidates.at<double>(c, 3);
        // Move to image domain
        double x1t = t_ptr[0] * x1 + t_ptr[1] * y1;
        double y1t = t_ptr[3] * x1 + t_ptr[4] * y1;
        double x2t = t_ptr[0] * x2 + t_ptr[1] * y2;
        double y2t = t_ptr[3] * x2 + t_ptr[4] * y2;
        // Add to curr shape
        x1t += curr_shape.at<double>(this->landmark_id_);
        y1t += curr_shape.at<double>(this->landmark_id_ + n_landmark);
        x2t += curr_shape.at<double>(this->landmark_id_);
        y2t += curr_shape.at<double>(this->landmark_id_ + n_landmark);
        // Check if they are in image plane
        x1t = std::max(0.0, std::min(x1t, image_w));
        y1t = std::max(0.0, std::min(y1t, image_h));
        x2t = std::max(0.0, std::min(x2t, image_w));
        y2t = std::max(0.0, std::min(y2t, image_h));
        // Sample image
        int p1 = static_cast<int>(curr_image.at<uchar>(int(y1t), int(x1t)));
        int p2 = static_cast<int>(curr_image.at<uchar>(int(y2t), int(x2t)));
        densities.at<int>(c, i) = p1 - p2;
      }
    }
    // Sort features
    cv::Mat densities_sorted;
    cv::sort(densities,
             densities_sorted,
             cv::SORT_EVERY_ROW + cv::SORT_ASCENDING);
    //select a feat which reduces maximum variance
    double var_all = (this->CalcVariance(delta_shapes.col(0)) +
                      this->CalcVariance(delta_shapes.col(1)));
    var_all *= static_cast<double>(N);
    double var_reduce_max = 0.0;
    int thresh = 0;
    int feat_id = 0;
    std::vector<double> left_x, left_y, right_x, right_y;
    for (int j = 0; j < n_sampling; ++j) {
      // Split each sample with random threshold
      left_x.clear();
      left_y.clear();
      right_x.clear();
      right_y.clear();
      int sel = static_cast<int>(N * rng.uniform(0.05, 0.95));
      int thr = densities_sorted.at<int>(j, sel);
      for (int i = 0; i < N; i++) {
        const int& root_idx = root[i];
        if (densities.at<int>(j, i) < thr) {
          left_x.push_back(delta_shapes.at<double>(root_idx, 0));
          left_y.push_back(delta_shapes.at<double>(root_idx, 1));
        } else {
          right_x.push_back(delta_shapes.at<double>(root_idx, 0));
          right_y.push_back(delta_shapes.at<double>(root_idx, 1));
        }
      }
      // Recompute variance
      double var_left = this->CalcVariance(left_x);
      var_left += this->CalcVariance(left_y);
      var_left *= static_cast<double>(left_x.size());
      double var_right = this->CalcVariance(right_x);
      var_right += this->CalcVariance(right_y);
      var_right *= static_cast<double>(right_x.size());
      double var_reduce = var_all - (var_left + var_right);
      if (var_reduce > var_reduce_max) {
        var_reduce_max = var_reduce;
        thresh = thr;
        feat_id = j;
      }
    }
    // Save best one
    this->thresholds_[idx] = thresh;
    this->feats_.at<double>(idx, 0) = candidates.at<double>(feat_id, 0);
    this->feats_.at<double>(idx, 1) = candidates.at<double>(feat_id, 1);
    this->feats_.at<double>(idx, 2) = candidates.at<double>(feat_id, 2);
    this->feats_.at<double>(idx, 3) = candidates.at<double>(feat_id, 3);
    // Generate left and right child
    std::vector<int> left, right;
    for (int i = 0; i < N; ++i) {
      if (densities.at<int>(feat_id, i) < thresh) {
        left.push_back(root[i]);
      } else {
        right.push_back(root[i]);
      }
    }
    // Split left and right child
    if (2 * idx < feats_.rows / 2) {
      SplitNode(ground_truth,
                current_shapes,
                delta_shapes,
                mean_shape,
                left,
                2 * idx,
                stage);
    }
    if (2 * idx + 1 < feats_.rows / 2) {
      SplitNode(ground_truth,
                current_shapes,
                delta_shapes,
                mean_shape,
                right,
                2 * idx + 1, stage);
    }
  }
}

/*
 * @name CalcVariance
 * @fn double CalcVariance(const cv::Mat &vec)
 * @brief calculate variance of a matrix
 * @param[in] vec  Mat
 * @return variance
 */
double RandomTreeTrainer::CalcVariance(const cv::Mat& vec) const {
  double m1 = cv::mean(vec)[0];
  double m2 = cv::mean(vec.mul(vec))[0];
  double variance = m2 - m1*m1;
  return variance;
}

/*
 * @name CalcVariance
 * @fn double CalcVariance(const cv::Mat &vec)
 * @brief calculate variance of a matrix
 * @param[in] vec  Mat
 * @return variance
 */
double RandomTreeTrainer::CalcVariance(const std::vector<double>& vec) const {
  if (vec.size() == 0) return 0.;
  cv::Mat_<double> vec_(vec);
  double m1 = cv::mean(vec_)[0];
  double m2 = cv::mean(vec_.mul(vec_))[0];
  double variance = m2 - m1*m1;
  return variance;
}

/*
 * @name  Write
 * @fn    int Write(std::ostream& stream)
 * @brief Write this random tree into a given stream
 * @param[in] stream  Stream where to ouput the object
 * @return    -1 if error, 0 otherwise
 */
int RandomTreeTrainer::Write(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Dump landmark ID
    stream.write(reinterpret_cast<const char*>(&this->landmark_id_),
                 sizeof(this->landmark_id_));
    // Tree depth
    stream.write(reinterpret_cast<const char*>(&params_.tree_depth),
                 sizeof(params_.tree_depth));
    // Features + threshold
    int n_node = 1 << params_.tree_depth;
    for (int i = 1; i < n_node / 2; ++i) {
      // sampling location (feats)
      stream.write(reinterpret_cast<const char*>(feats_.ptr<double>(i)),
                   feats_.cols * feats_.elemSize());
      // thresh
      stream.write(reinterpret_cast<const char*>(&thresholds_[i]),
                   sizeof(thresholds_[0]));
    }
    err = stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize(void) const
 * @brief Compute memory need for this RandomTree
 * @return    Object size in byte
 */
int RandomTreeTrainer::ComputeObjectSize(void) const {
  // Ints
  int sz = sizeof(this->landmark_id_);
  sz += sizeof(params_.tree_depth);
  // Feature matrix (sampling target)
  int n_node = 1 << params_.tree_depth;
  for (int i = 1; i < n_node / 2; ++i) {
    // Feats + thresh
    sz += feats_.cols * feats_.elemSize() + sizeof(thresholds_[0]);
  }
  return sz;
}

#pragma mark -
#pragma mark Initialization

/*
 * @name RandomForestTrainer
 * @fn RandomForestTrainer(void)
 * @brief constructor
 */
RandomForestTrainer::RandomForestTrainer(void) {
}

/*
 * @name ~RandomForestTrainer
 * @fn ~RandomForestTrainer(void)
 * @brief Destructor
 */
RandomForestTrainer::~RandomForestTrainer(void) {
}

/*
 * @name  Init
 * @param[in] n_landmark  Number of landmark in the shape
 * @param[in] params      User defined parameters
 */
void RandomForestTrainer::Init(const int n_landmark,
                               const Parameter& params) {
  this->n_landmark_ = n_landmark;
  this->params_ = params;
  // Init forest
  this->random_trees_.resize(static_cast<size_t>(this->n_landmark_));
  for (int i = 0; i < this->n_landmark_; ++i) {
    auto& tree = random_trees_[i];
    tree.resize(static_cast<size_t>(params_.n_tree));
    for (int j = 0; j < params_.n_tree; ++j) {
      tree[j].Init(i, this->params_);
    }
  }
}

/*
 * @name  Train
 * @brief Train the whole forest
 * @param ground_truth    List of ground truth samples
 * @param current_shapes  List of shape for a given stage
 * @param delta_shapes    List of delta shape
 * @param mean_shape      Normalized meanshape
 * @param stage           Current stage
 */
void RandomForestTrainer::Train(const std::vector<GroundTruth>& ground_truth,
                                const std::vector<cv::Mat>& current_shapes,
                                const std::vector<cv::Mat>& delta_shapes,
                                const cv::Mat& mean_shape,
                                const int stage) {
  // Get number of sample
  int N = static_cast<int>(ground_truth.size());
  // Define overlapping ratio
  int Q = N;
  int div = static_cast<int>(std::round((1.0 - params_.bagging_overlap) * params_.n_tree));
  div = div == 0 ? 1 : div;
  Q /= div;

  // Train each landmarks
  Parallel::For(this->n_landmark_,
                [&](const size_t& i) {
    std::vector<int> root;
    auto& tree = this->random_trees_[i];
    for (int t = 0; t < params_.n_tree; ++t) {
      int l_bound = static_cast<int>(std::floor(t * Q -
                                                t * Q * params_.bagging_overlap));
      int start = std::max(0, l_bound);
      int u_bound = start + Q + 1;
      int end = std::min(u_bound, N);
      int L = end - start;
      root.resize(static_cast<size_t>(L));
      for (int k = 0; k < L; k++) {
        root[k] = start + k;
      }
      tree[t].Train(ground_truth,
                    current_shapes,
                    delta_shapes,
                    mean_shape,
                    root,
                    stage);
    }
    std::cout << "\tTrain " << i << "th landmark done" << std::endl;
  });
}

/*
 * @name  ComputeLBF
 * @brief Compute feature for a given shape/image
 * @param[in] ground_truth    One sample
 * @param[in] current_shape   Current shape
 * @param[in] meanshape       Reference shape
 * @param[in] inner_shape     Flag to indicate if we compute only for
 *                            the innershape
 * @param[out] lbf            Computed features
 */
void RandomForestTrainer::ComputeLBF(const GroundTruth& ground_truth,
                                     const cv::Mat& current_shape,
                                     const cv::Mat& meanshape,
                                     const bool inner_shape,
                                     cv::SparseMat* lbf) {
  // Init output
  assert(n_landmark_ == 68);
  const int n_landmark = inner_shape ? n_landmark_ - 19 : n_landmark_;
  const int F = n_landmark * params_.n_tree * (1 << (params_.tree_depth - 1));
  const int dims[] = {1, F};
  lbf->create(2, dims, CV_64FC1);
  // Load image
  cv::Mat image = ground_truth.image_patch;
  assert(image.channels() == 1);
  // Get similarity transform
  cv::Mat transform;
  ComputeSimilarityTransform(meanshape, current_shape, &transform);
  const auto* t_ptr = reinterpret_cast<const double*>(transform.data);
  // Generate LBF
  int base = 1 << (params_.tree_depth - 1);
  static Mutex mutex;
  Parallel::For(n_landmark_,
                [&](const size_t& i) {
    int p = i;
    // Extract only innershape if needed
    bool skip_update = false;
    if (inner_shape) {
      if (i < 17 || i == 60 || i == 64) {
        skip_update = true;
      } else {
        // Adjust index
        if (i >= 17) {
          p -= 17;
        }
        if (i > 60) {
          p -= 1;
        }
        if (i > 64) {
          p -= 1;
        }
      }
    }
    if (!skip_update) {
      const auto &trees = random_trees_[i];
      const auto *cs_ptr = reinterpret_cast<const double *>(current_shape.data);
      const auto image_w = static_cast<double>(image.cols - 1);
      const auto image_h = static_cast<double>(image.rows - 1);
      for (int j = 0; j < params_.n_tree; ++j) {
        const auto &t = trees[j];
        const auto &feats = t.get_feats();
        const auto &thresh = t.get_threshold();
        int code = 0;
        int idx = 1;
        // Go through the tree
        for (int k = 1; k < params_.tree_depth; ++k) {
          // Get sampling location
          const auto &x1 = feats.at<double>(idx, 0);
          const auto &y1 = feats.at<double>(idx, 1);
          const auto &x2 = feats.at<double>(idx, 2);
          const auto &y2 = feats.at<double>(idx, 3);
          // Move them into image space
          double x1t = t_ptr[0] * x1 + t_ptr[1] * y1;
          double x2t = t_ptr[0] * x2 + t_ptr[1] * y2;
          double y1t = t_ptr[3] * x1 + t_ptr[4] * y1;
          double y2t = t_ptr[3] * x2 + t_ptr[4] * y2;
          // Add to current location
          x1t += cs_ptr[i];
          x2t += cs_ptr[i];
          y1t += cs_ptr[i + n_landmark_];
          y2t += cs_ptr[i + n_landmark_];
          // Check image boundaries
          x1t = std::max(0.0, std::min(x1t, image_w));
          x2t = std::max(0.0, std::min(x2t, image_w));
          y1t = std::max(0.0, std::min(y1t, image_h));
          y2t = std::max(0.0, std::min(y2t, image_h));
          // Sample image
          int density = (image.at<uchar>(int(y1t), int(x1t)) -
                         image.at<uchar>(int(y2t), int(x2t)));
          code <<= 1;
          if (density < thresh[idx]) {
            idx = 2 * idx;
          } else {
            code += 1;
            idx = 2 * idx + 1;
          }
        }
        // critical section
        {
          ScopedLock lock(mutex);
          lbf->ref<double>(0,
                           (p * params_.n_tree + j) * base + code) = 1.f;
        }
        //lbf->at<int>(i * params_.n_tree + j) = (i * params_.n_tree + j) * base + code;
      }
    }
  });
}

/*
 * @name
 * @brief Dump into a given stream
 * @param[in] stream  Stream where to dump data
 * @return    -1 if error, 0 otherwise
 */
int RandomForestTrainer::Write(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // N landmarks
    stream.write(reinterpret_cast<const char*>(&this->n_landmark_),
                 sizeof(this->n_landmark_));
    // N tree
    stream.write(reinterpret_cast<const char*>(&params_.n_tree),
                 sizeof(params_.n_tree));
    // Each tree depth
    stream.write(reinterpret_cast<const char*>(&params_.tree_depth),
                 sizeof(params_.tree_depth));
    // Dump each trees
    for (int i = 0; i < this->n_landmark_; ++i) {
      const auto& tree = this->random_trees_[i];
      for (int j = 0; j < params_.n_tree; ++j) {
        tree[j].Write(stream);
      }
    }
    err = stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize(void) const
 * @brief Compute memory need for this RandomForest
 * @return    Object size in byte
 */
int RandomForestTrainer::ComputeObjectSize(void) const {
  int sz = sizeof(this->n_landmark_);
  sz += sizeof(params_.n_tree);
  sz += sizeof(params_.tree_depth);
  // compute size for each tree
  for (int i = 0; i < this->n_landmark_; ++i) {
    const auto& tree = this->random_trees_[i];
    for (int j = 0; j < params_.n_tree; ++j) {
      sz += tree[j].ComputeObjectSize();
    }
  }
  return sz;
}

#pragma mark -
#pragma mark Initialization

/*
 * @name LBFCascadeTrainer()
 * @brief Constructor
 */
LBFCascadeTrainer::LBFCascadeTrainer(void) {}

/*
 * @name ~LBFCascadeTrainer()
 * @brief Destructor
 */
LBFCascadeTrainer::~LBFCascadeTrainer(void) {
  for (auto* f : random_forests_) {
    delete f;
    f = nullptr;
  }
}

/*
 * @name Init
 * @fn void Init(const int n_stage, const int n_landmark)
 * @brief set size of mean_shape, random_forest, regression param
 * @param[in] n_stage Number of stage in the cascade
 * @param[in] n_landmark Number of landmark in the shape
 * @param[in] parameters  User defined parameters
 */
void LBFCascadeTrainer::Init(const int n_stage,
                             const int n_landmark,
                             const Parameters& parameters) {
  this->n_stages_ = n_stage;
  this->n_landmark_ = n_landmark;
  this->params_ = parameters;
  // Init forest
  random_forests_.resize(static_cast<size_t>(n_stages_));
  for (int i = 0; i < n_stages_; ++i) {
    auto& f = random_forests_[i];
    f = new RandomForestTrainer();
    f-> Init(n_landmark_, params_);
  }
  // Init regression
  gl_regression_weights_.resize(static_cast<size_t>(n_stages_));
  int F = n_landmark_ * params_.n_tree * (1 << (params_.tree_depth - 1));
  for (int i = 0; i < n_stages_; i++) {
    gl_regression_weights_[i].create(2 * n_landmark_, F, CV_64FC1);
  }
}

/*
 * @name
 * @brief Train feature descriptor + global regressor
 * @param[in] ground_truth    List of ground truth sample
 * @param[in] meanshape       Normalized shape
 * @param[in,out] current_shape   List of current shape in the cascade
 */
void LBFCascadeTrainer::Train(const std::vector<GroundTruth>& ground_truth,
                              const cv::Mat& meanshape,
                              std::vector<cv::Mat>* current_shape) {
  const auto N = ground_truth.size();
  // Train each stage
  std::vector<cv::Mat> delta_shapes;
  std::vector<cv::SparseMat> lbf_feature;
  for(int k = 0; k < params_.n_stages; ++k) {
    auto* forest = random_forests_[k];
    // Compute delta shape
    this->ComputeDelta(ground_truth,
                       *current_shape,
                       meanshape,
                       &delta_shapes);
    // Train feature descriptor
    std::cout << "Start train descriptor of "<< k << "th stage" << std::endl;
    forest->Train(ground_truth,
                  *current_shape,
                  delta_shapes,
                  meanshape,
                  k);
    std::cout << "End of train descriptor of "<< k << "th stage" << std::endl;

    // Generate lbf of every train data
    lbf_feature.resize(N);
    for (size_t i = 0; i < N; ++i) {
      forest->ComputeLBF(ground_truth[i],
                         current_shape->at(i),
                         meanshape,
                         false,
                         &lbf_feature[i]);
    }
    // Learn global regression
    std::cout << "Train regression of "<< k << "th stage" << std::endl;
    GRegressionTrain(lbf_feature, delta_shapes, k);
    std::cout << "End of train regression of "<< k << "th stage" << std::endl;
    // update current_shapes with learned regressor
    Parallel::For(N,
                  [&](const size_t& i) {
      // Estimate shape update
      cv::Mat delta;
      this->GRegressionPredict(lbf_feature[i], k, &delta);
      // Compute similarity
      cv::Mat transform;
      auto& curr_shape = current_shape->at(i);
      ComputeSimilarityTransform(meanshape, curr_shape, &transform);
      // Apply transform onto delta shape
      const auto* t_ptr = reinterpret_cast<const double*>(transform.data);
      for (int j = 0; j < n_landmark_; ++j) {
        // Get delta for this landmarks
        const double dx = delta.at<double>(j);
        const double dy = delta.at<double>(j + n_landmark_);
        // Transform to image space
        double dxt = t_ptr[0] * dx + t_ptr[1] * dy;
        double dyt = t_ptr[3] * dx + t_ptr[4] * dy;
        // Add to current shape
        curr_shape.at<double>(j) += dxt;
        curr_shape.at<double>(j + n_landmark_) += dyt;
      }
    });
  }
}

/*
 * @name  Write
 * @brief Dump into a given stream
 * @param[in] stream  Stream where to dump data
 * @return    -1 if error, 0 otherwise
 */
int LBFCascadeTrainer::Write(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Parameters
    stream.write(reinterpret_cast<const char*>(&params_.n_stages),
                 sizeof(params_.n_stages));
    stream.write(reinterpret_cast<const char*>(&params_.n_tree),
                 sizeof(params_.n_tree));
    stream.write(reinterpret_cast<const char*>(&params_.tree_depth),
                 sizeof(params_.tree_depth));
    stream.write(reinterpret_cast<const char*>(&n_landmark_),
                 sizeof(n_landmark_));
    // Every stage
    for (int i = 0; i < n_stages_; ++i) {
      // Write forest
      err |= random_forests_[i]->Write(stream);
      // Write regressor
      err |= LTS5::WriteMatToBin(stream, gl_regression_weights_[i]);
    }
    err = stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize(void) const
 * @brief Compute memory need for this LBFCascadeTrainer
 * @return    Object size in byte
 */
int LBFCascadeTrainer::ComputeObjectSize(void) const {
  int sz = sizeof(params_.n_stages);
  sz += sizeof(params_.n_tree);
  sz += sizeof(params_.tree_depth);
  sz += sizeof(n_landmark_);
  // Every stage
  for (int i = 0; i < n_stages_; ++i) {
    // Forest
    sz += random_forests_[i]->ComputeObjectSize();
    // Regressor
    sz += 3 * sizeof(int);
    sz += (gl_regression_weights_[i].total() *
           gl_regression_weights_[i].elemSize());
  }
  return sz;
}

/*
 * @name GRegressionTrain
 * @brief from lbfs determine W global by solving opt prob with dual coord descent meth
 * @param[in] lbfs        List of LBF features
 * @param[in] delta_shape List of delta shape ideal
 * @param[in] stage       Current stage
 */
void LBFCascadeTrainer::GRegressionTrain(const std::vector<cv::SparseMat>& lbf_features,
                                         const std::vector<cv::Mat>& delta_shape,
                                         const int stage) {
  int N = static_cast<int>(lbf_features.size());
  int M = static_cast<int>(lbf_features[0].nzcount());
  int F = n_landmark_ * params_.n_tree *(1 << (params_.tree_depth - 1));
  int landmark_n = n_landmark_;
  /*int F = tracker_parameters_.landmark_n_*tracker_parameters_
          .tree_n_*(1 << (tracker_parameters_.tree_depth_ - 1));
  int landmark_n = tracker_parameters_.landmark_n_;*/
  // prepare linear regression params X and Y
  struct LINEAR_LBF::feature_node **X = (struct LINEAR_LBF::feature_node **)malloc(N * sizeof(struct LINEAR_LBF::feature_node *));
  double **Y = (double **)malloc(landmark_n * 2 * sizeof(double *));
  for (int i = 0; i < N; i++) {
    X[i] = (struct LINEAR_LBF::feature_node *)malloc((M + 1) * sizeof(struct LINEAR_LBF::feature_node));
    auto it = lbf_features[i].begin();
    for (int j = 0; j < M; j++) {
      const auto* node = it.node();
      X[i][j].index = node->idx[1] + 1; // index starts from 1
      X[i][j].value = 1;
      ++it;
    }
    X[i][M].index = -1;
    X[i][M].value = -1.0;
  }
  for (int i = 0; i < landmark_n; i++) {
    Y[2 * i] = (double *)malloc(N*sizeof(double));
    Y[2 * i + 1] = (double *)malloc(N*sizeof(double));
    for (int j = 0; j < N; j++) {
      Y[2 * i][j] = delta_shape[j].at<double>(i);
      Y[2 * i + 1][j] = delta_shape[j].at<double>(i+landmark_n);
    }
  }
  // train every landmark
  struct LINEAR_LBF::problem prob;
  struct LINEAR_LBF::parameter param;
  prob.l = N;
  prob.n = F;
  prob.x = X;
  prob.bias = -1;
  param.solver_type = LINEAR_LBF::L2R_L2LOSS_SVR_DUAL;
  param.C = 1. / N;
  param.p = 0;
  param.eps = 0.00001;
  cv::Mat_<double> weight(2 * landmark_n, F);
  Parallel::For(landmark_n,
                [&](const size_t& i) {

#define FREE_MODEL(model)   \
    free(model->w);         \
    free(model->label);     \
    free(model)

    std::cout << "\ttrain " << i << "th landmark " <<std::endl;
    struct LINEAR_LBF::problem prob_ = prob;
    prob_.y = Y[2 * i];
    LINEAR_LBF::check_parameter(&prob_, &param);
    struct LINEAR_LBF::model *model = LINEAR_LBF::train(&prob_, &param);
    for (int j = 0; j < F; j++) {
      weight(2 * i, j) = LINEAR_LBF::get_decfun_coef(model, j + 1, 0);
    }
    FREE_MODEL(model);

    prob_.y = Y[2 * i + 1];
    LINEAR_LBF::check_parameter(&prob_, &param);
    model = LINEAR_LBF::train(&prob_, &param);
    for (int j = 0; j < F; j++) {
      weight(2 * i + 1, j) = LINEAR_LBF::get_decfun_coef(model, j + 1, 0);
    }
    FREE_MODEL(model);
#undef FREE_MODEL
  });
  gl_regression_weights_[stage] = weight;
  // free
  for (int i = 0; i < N; i++) free(X[i]);
  for (int i = 0; i < 2 * landmark_n; i++) free(Y[i]);
  free(X);
  free(Y);
}

/*
 * @name GRegressionPredict
 * @param[in] lbf     Feature to do regression
 * @param[in] stage   Current stage
 * @param[out] delta  Delta shape for a given stage
 */
void LBFCascadeTrainer::GRegressionPredict(const cv::SparseMat& lbf,
                                           const int stage,
                                           cv::Mat* delta) {
  const auto& w = gl_regression_weights_[stage];
  const double *wx_ptr = nullptr;
  const double *wy_ptr = nullptr;
  delta->create(n_landmark_ * 2, 1, CV_64FC1);
  // Update using learned regression
  for (int i = 0; i < n_landmark_; i++) {
    wx_ptr = w.ptr<double>(2 * i);
    wy_ptr = w.ptr<double>(2 * i + 1);
    double x = 0, y = 0;
    for (auto it = lbf.begin(); it != lbf.end(); ++it) {
      const auto* node = it.node();
      x += wx_ptr[node->idx[1]];
      y += wy_ptr[node->idx[1]];
    }
    delta->at<double>(i) = x;
    delta->at<double>(i + n_landmark_) = y;
  }
}

/*
 * @name  ComputeDeltas
 * @brief Compute delta shape from curr to ground truth
 * @param[in] ground_truth    Target shape
 * @param[in] current_shape   Current shape
 * @param[in] meanshape       Normalized meanshape
 * @param[out] delta_shape    Delta shape (distance to recover for next stage)
 */
void LBFCascadeTrainer::ComputeDelta(const std::vector<GroundTruth>& ground_truth,
                       const std::vector<cv::Mat> current_shape,
                       const cv::Mat& meanshape,
                       std::vector<cv::Mat>* delta_shape) {
  // setup output
  const auto N = ground_truth.size();
  delta_shape->resize(N);
  for (size_t i = 0; i < N; ++i) {
    // Compute delta in image space
    cv::Mat delta = ground_truth[i].shape - current_shape[i];
    // Move to normalized space
    cv::Mat transform;
    ComputeSimilarityTransform(current_shape[i], meanshape, &transform);
    const double* t_ptr = reinterpret_cast<const double*>(transform.data);
    auto& d_shape = delta_shape->at(i);
    d_shape.create(2 * n_landmark_, 1, delta.type());
    for (int j = 0; j < n_landmark_; ++j) {
      const double dx = delta.at<double>(j);
      const double dy = delta.at<double>(j + n_landmark_);
      const double dxt = t_ptr[0] * dx + t_ptr[1] * dy;
      const double dyt = t_ptr[3] * dx + t_ptr[4] * dy;
      d_shape.at<double>(j) = dxt;
      d_shape.at<double>(j + n_landmark_) = dyt;
    }
  }
}

#pragma mark -
#pragma mark LBFTrainer

#pragma mark Initialization

/*
 *  @name   LBFTrainer
 *  @brief  Constructor
 */
LBFTrainer::LBFTrainer(const std::string& face_detector_config) {
  face_detector_ = new LTS5::CascadeFaceDetector(face_detector_config);
  LTS5::CascadeFaceDetector::CascadeDetectorParameters parameters;
  parameters.scale_factor = 1.05;
  parameters.min_neighbors = 2;
  parameters.flags = 0;
  parameters.min_size = cv::Size(20, 20);
  face_detector_->set_parameters(parameters);
  cascade_trainer_ = new LTS5::LBFCascadeTrainer();
  assessment_trainer_ = nullptr;
}

/*
 *  @name   ~LBFTrainer
 *  @brief  Destructor
 */
LBFTrainer::~LBFTrainer(void) {
  if (face_detector_) {
    delete face_detector_;
    face_detector_ = nullptr;
  }
  if (cascade_trainer_) {
    delete cascade_trainer_;
    cascade_trainer_ = nullptr;
  }
  if (assessment_trainer_) {
    delete assessment_trainer_;
    assessment_trainer_ = nullptr;
  }
}

#pragma mark Usage

/*
 * @name Train
 * @brief Train data (load data, data augmentation, call lbf::Train)
 * @param[in] folder  Dataset folder
 * @param[in,out] params  Trainer parameters
 * @return return -1 of error, 0 otherwise
 */
int LBFTrainer::Train(const std::string& folder, LBFTrainerParameters* params) {
  // Load ground truth
  int err = this->LoadGroundTruth(folder, &images_);
  if (!err) {
    // First, load one image at a time, detect the face (if not already
    // provided) and:
    // * if the face is detected, store the detection rectangle
    // * if not, remove that image from the "dataset"
    err = this->DetectFace(&images_);
    if (!err) {
      // Then, compute the mean shape
      this->ComputeMeanshape(images_);
      // Compute detection statistics
      cv::Mat mean, stdev;
      this->ComputeStatistics(images_, meanshape_, &mean, &stdev);
      // Save statistics
      params->training_statistics[0] = mean.at<cv::Vec3d>(0)[0];
      params->training_statistics[1] = stdev.at<cv::Vec3d>(0)[0];
      params->training_statistics[2] = mean.at<cv::Vec3d>(0)[1];
      params->training_statistics[3] = stdev.at<cv::Vec3d>(0)[1];
      params->training_statistics[4] = mean.at<cv::Vec3d>(0)[2];
      params->training_statistics[5] = stdev.at<cv::Vec3d>(0)[2];
      // Monte Carlo sampling of the face detector rectangles!
      stdev.at<cv::Vec3d>(0, 0)[0] *= params->montecarlo_scale_std_scaling;
      stdev.at<cv::Vec3d>(0, 0)[1] *= params->montecarlo_offsetX_std_scaling;
      stdev.at<cv::Vec3d>(0, 0)[2] *= params->montecarlo_offsetY_std_scaling;

      // Generate starting shape
      std::vector<cv::Mat> starting_shapes;
      this->AugmentDataset(meanshape_,
                           *params,
                           stdev,
                           &images_,
                           &starting_shapes);
      assert(images_.size() == starting_shapes.size());
      // Shuffle data
      std::vector<size_t> perm(images_.size());
      std::iota(perm.begin(), perm.end(), 0);
      auto seed = std::chrono::system_clock::now().time_since_epoch().count();
      std::mt19937 engine(seed);
      std::shuffle(perm.begin(), perm.end(), engine);
      LTS5::ApplyPermutation(perm, &images_);
      LTS5::ApplyPermutation(perm, &starting_shapes);
      // Train LBFTracker
      const int n_landmark = std::max(meanshape_.cols, meanshape_.rows) / 2;
      cascade_trainer_->Init(params->n_stages, n_landmark, *params);
      cascade_trainer_->Train(images_, meanshape_, &starting_shapes);
      // Train assessment
      if (assessment_trainer_) {
        delete assessment_trainer_;
      }
      const size_t level = 0;//static_cast<size_t>(params->n_stages - 1);
      const auto& descriptor = *(cascade_trainer_->get_descriptor(level));
      assessment_trainer_ = new LBFTrackerAssessmentTrainer(images_,
                                                            starting_shapes,
                                                            descriptor,
                                                            meanshape_,
                                                            stdev,
                                                            0.75);
      assessment_trainer_->Train();
      err = 0;
    } else {
      std::cout << "Unable to run face detector" << std::endl;
    }
  } else {
    std::cout << "Unable to load data from : " << folder << std::endl;
  }
  return err;
}

/*
 * @name  Write
 * @brief Write model into binary stream
 * @param[in] stream    stream where to dump model
 * @return    -1 if error, 0 otherwise
 */
int LBFTrainer::Write(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Define object size
    int sz = 3 * sizeof(int);
    sz += meanshape_.total() * meanshape_.elemSize();
    sz += cascade_trainer_->ComputeObjectSize();
    // Write
    int dummy = static_cast<int>(HeaderObjectType::kLBFTracker);
    stream.write(reinterpret_cast<const char*>(&dummy),
                 sizeof(dummy));
    stream.write(reinterpret_cast<const char*>(&sz),
                 sizeof(sz));
    // Meanshape
    err = LTS5::WriteMatToBin(stream, meanshape_);
    // Tracker
    err |= cascade_trainer_->Write(stream);
    // Assessment
    err |= assessment_trainer_->Save(stream);
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

#pragma mark Private

/*
 * @name  LoadGroundTruth
 * @brief Load image ground truth (path, annotation)
 * @param[in] folder  Root folder
 * @param[out] data   Ground truth
 * @return    -1 if error, 0 otherwise
 */
int LBFTrainer::LoadGroundTruth(const std::string& folder,
                                std::vector<GroundTruth>* data) {
  // check if cache file exist
  int err = -1;
  bool has_cache = std::ifstream("lbf_cache.bin", std::ios_base::binary).is_open();
  if (!has_cache) {
    // Scan folder
    std::vector<std::string> paths = LTS5::ScanImagesInFolder(folder, true);
    err = paths.empty() ? -1 : 0;
    if (!err) {
      const auto n = paths.size();
      data->resize(n);
      const auto tenth = n / 10;
      // Load annotation
      for (size_t s = 0; s < n; ++s) {
        if (s != 0 && ((s % tenth) == 0)) {
          std::cout << "Loaded " << s << " images out of " << n << std::endl;
        }
        const auto& file = paths[s];
        auto& g_truth = data->at(s);
        // Set path to image
        g_truth.image_path = file;
        // Set no flip
        g_truth.is_flipped = false;
        // Retrieve image dimension
        cv::Mat img = cv::imread(g_truth.image_path);
        g_truth.size = cv::Size(img.cols, img.rows);
        assert(g_truth.size.height != 0 && g_truth.size.width != 0);
        // Set annotation
        const auto p = file.rfind('.');
        if (p != std::string::npos) {
          g_truth.annotation_path = file.substr(0, p) + ".pts";
          LTS5::LoadPts(g_truth.annotation_path, g_truth.shape);
        } else {
          err = -1;
          std::cout << "\tCan not find annotation for : " << file << std::endl;
        }
      }
    } else {
      std::cout << "Error, no image founded in " << folder << std::endl;
    }
  } else {
    std::cout << "Use cached data !" << std::endl;
    std::ifstream cache("lbf_cache.bin",
                        std::ios_base::in | std::ios_base::binary);
    while (!cache.eof()) {
      GroundTruth sample;
      cache >> sample;
      if (cache.good()) {
        data->push_back(sample);
      }
    }
    cache.close();
    err = 0;
  }
  return err;
}

/*
 * @name  DetectFace
 * @brief Run face detector on training sample
 * @return     -1 if error, 0 otherwise
 */
int LBFTrainer::DetectFace(std::vector<GroundTruth>* train_sample) {
  int err = -1;
  if (!train_sample->empty() && !face_detector_->empty()) {
    // Cache data ?
    std::ofstream cache;
    const auto total = 2 * train_sample->size();
    bool has_cache = train_sample->at(0).face_region != cv::Rect();
    if (!has_cache) {
      cache.open("lbf_cache.bin",
                 std::ios_base::out|std::ios_base::binary);
      std::cout << "Run face detector ..." << std::endl;
    }
    // Loop over all training samples
    for (auto it = train_sample->begin(); it != train_sample->end(); ) {
      // Load image
      cv::Mat img = cv::imread(it->image_path);
      if (img.channels() != 1) {
        cv::cvtColor(img, img, cv::COLOR_BGR2GRAY);
      }
      if (!has_cache) {
        // Run detection
        cv::Rect face_rect;
        std::vector<cv::Rect> face_detections;
        face_detector_->Detect(img, &face_detections);
        // Prune detection, rectangle should include annotation
        face_rect = this->PruneDetections(it->shape, face_detections);
        // Keep it ?
        if (face_rect.width != 0 && face_rect.height != 0) {
          it->face_region = face_rect;
          // Copy image patch
          img.copyTo(it->image_patch);
          // Cache it
          cache << *it << std::flush;
          // Go to next
          ++it;
        } else {
          // Drop sample
          std::cout << "\tdrop : " << it->image_path << std::endl;
          it = train_sample->erase(it);
        }
      } else {
        // Load annotation
        LTS5::LoadPts(it->annotation_path, it->shape);
        // Copy image patch
        img.copyTo(it->image_patch);
        // Go to next
        ++it;
      }
    }
    cache.close();
    // Add flip
    const auto n_train = train_sample->size();
    for (size_t s = 0; s < n_train; ++s) {
      // Access already loaded data
      const auto& g_truth = train_sample->at(s);
      // Create flipped version
      GroundTruth groundTruth;
      groundTruth.image_path = g_truth.image_path;
      groundTruth.annotation_path = g_truth.annotation_path;
      groundTruth.is_flipped = !g_truth.is_flipped;
      groundTruth.size = g_truth.size;
      // Flip annotation
      groundTruth.shape = LTS5::FlipShape(g_truth.shape, g_truth.size);
      // Flip detection
      const int w = g_truth.image_patch.cols - 1;
      groundTruth.face_region.x = w - (g_truth.face_region.x +
                                       g_truth.face_region.width);
      groundTruth.face_region.y = g_truth.face_region.y;
      groundTruth.face_region.width = g_truth.face_region.width;
      groundTruth.face_region.height = g_truth.face_region.height;
      // Flip patch
      cv::flip(g_truth.image_patch, groundTruth.image_patch, 1);
      // Add to pool
      train_sample->push_back(groundTruth);
    }
    const auto valid = train_sample->size();
    std::cout << 100.0 * valid / total << "% (" << valid << "/" << total;
    std::cout << ") faces detected" << std::endl;
    err = 0;
  }
  return err;
}

/*
   * @name  ComputeMeanshape
   * @brief Compute meanshape over all training samples
   * @param[in] samples    Training samples
   */
void LBFTrainer::ComputeMeanshape(const std::vector<GroundTruth>& samples) {
  const auto n_sample = samples.size();
  assert(n_sample > 0);

  // Initialize mean shape
  const int rows = samples[0].shape.rows;
  const int cols = samples[0].shape.cols;
  const int type = samples[0].shape.type();
  meanshape_.create(rows, cols, type);
  meanshape_.setTo(0.0);
  for (size_t i = 0; i < n_sample; ++i) {
    const auto& shape = samples[i].shape;
    const auto& rect = samples[i].face_region;
    assert(rect.width > 0.001 && rect.height > 0.001);
    meanshape_ += LTS5::ImageAbsToRectRel(rect, shape);
  }
  meanshape_ /= static_cast<double>(n_sample);
}

/*
 *  @name   ComputeStatistics
 *  @brief  Compute the means and standard deviations of the difference
 *          between the annotations and the face detector along scale, x and y
 *          dimensions
 *  @param[in]      samples      Training samples
 *  @param[in]      mean_shape   Mean shape of the detections
 *  @param[out]     means        Mean along each of the three dimensions:
 *                               scale, x-dimension and y-dimension
 *  @param[out]     stds         Standard deviations along each of the three
 *                               dimensions: scale, x-dimension and
 *                               y-dimension.
 */
void LBFTrainer::ComputeStatistics(const std::vector<GroundTruth>& samples,
                                   const cv::Mat& meanshape,
                                   cv::Mat* means,
                                   cv::Mat* stds) {
  const int n_sample = static_cast<int>(samples.size());
  cv::Mat shape_differences(n_sample, 1, CV_64FC3);
  means->zeros(1, 1, CV_64FC3);
  stds->zeros(1, 1, CV_64FC3);
  // Loop over samples
  for (int i = 0; i < n_sample; i++) {
    auto& sample = samples[i];
    cv::Mat ann = LTS5::ImageAbsToRectRel(sample.face_region, sample.shape);
    auto simT = LTS5::ComputeRigidTransform(meanshape, ann);
    const double s = std::sqrt(simT[0] * simT[0] + simT[1] * simT[1]);
    shape_differences.at<cv::Vec3d>(i)[0] = s;
    shape_differences.at<cv::Vec3d>(i)[1] = simT[2];
    shape_differences.at<cv::Vec3d>(i)[2] = simT[3];
  }
  cv::meanStdDev(shape_differences, *means, *stds);
  std::cout << "\tscale : " << means->at<cv::Vec3d>(0, 0)[0] << " +/- ";
  std::cout << stds->at<cv::Vec3d>(0, 0)[0] << std::endl;
  std::cout << "\tx-dim : " << means->at<cv::Vec3d>(0, 0)[1] << " +/- ";
  std::cout << stds->at<cv::Vec3d>(0, 0)[1] << std::endl;
  std::cout << "\ty-dim : " << means->at<cv::Vec3d>(0, 0)[2] << " +/- ";
  std::cout << stds->at<cv::Vec3d>(0, 0)[2] << std::endl;
}

/*
 * @name  AugmentDataset
 * @brief Generate starting shape through data augmentation
 * @param[in] meanshape       Normalized meanshape
 * @param[in] params          User defined parameters
 * @param[in,out] samples     List of training sample / target
 * @param[out] start_shape    Starting shape
 */
void LBFTrainer::AugmentDataset(const cv::Mat& meanshape,
                                const LBFTrainerParameters& params,
                                const cv::Vec3d& stdev,
                                std::vector<GroundTruth>* samples,
                                std::vector<cv::Mat>* start_shape) {
  // Generate random perturbation of a given rectangle
  cv::RNG rng(static_cast<uint64>(cv::getTickCount()));

  auto random_rect = [&rng](const cv::Rect& rect,
                            const cv::Vec3d& stds,
                            const int n_attempt,
                            const cv::Size& img_dim)->cv::Rect {
    int cnt = 0;
    cv::Rect perturbation;
    while (cnt < n_attempt) {
      // Generate random parameters
      double scale = 1 + rng.gaussian(stds[0]);
      const double tx = rng.gaussian(stds[1]) * rect.width;
      const double ty = rng.gaussian(stds[2]) * rect.height;

      double x = rect.x + (1-scale) * rect.width/2.0 + tx;
      double y = rect.y + (1-scale) * rect.height/2.0 + ty;
      double w = scale * rect.width;
      double h = scale * rect.height;

      // Inside image ?
      if (x < 0 || y < 0 || x + w >= img_dim.width || y + h >= img_dim.height) {
        if (cnt + 1 == n_attempt) {
          // Reach end
          std::cout <<  "Failed to randomly generate a rectangle inside";
          std::cout << " the image boundaries after " << n_attempt;
          std::cout << " trials. Manual rectangle is used" << std::endl;
          // Check scale (to avoid the rect being bigger than the image)
          // If too big, set it to 1! (not perturbed)
          if ((img_dim.width - w) < 0 ||
              (img_dim.height - h) < 0) {
            scale = 1.0;
            w = scale * rect.width;
            h = scale * rect.height;
          }
          // Then shift back the rect inside the image
          if (x < 0) {
            x = 0;
          } else if (x + w >= img_dim.width) {
            x = img_dim.width - 1 - w;
          }
          if (y < 0) {
            y = 0;
          } else if (y + h >= img_dim.height) {
            y = img_dim.height - 1 - h;
          }
          perturbation.x = static_cast<int>(x);
          perturbation.y = static_cast<int>(y);
          perturbation.width = static_cast<int>(w);
          perturbation.height = static_cast<int>(h);
          // we now have: x > 0 and x < image_width - w
          //              y > 0 and y < image_height - h
          break;
        }
      } else {
        perturbation.x = static_cast<int>(x);
        perturbation.y = static_cast<int>(y);
        perturbation.width = static_cast<int>(w);
        perturbation.height = static_cast<int>(h);
        break;
      }
      cnt++;
    }
    return perturbation;
  };

  // Loop over all sample
  const auto n_sample = samples->size();
  for (int j = 0; j < params.montecarlo_samples; ++j) {
    // Generate sample
    for (size_t i = 0; i < n_sample; ++i) {
      auto& sample = samples->at(i);
      cv::Rect perturbation = random_rect(sample.face_region,
                                          stdev,
                                          params.montecarlo_sampling_attempt,
                                          sample.size);
      cv::Mat s_shape = LTS5::RectRelToImageAbs(perturbation, meanshape);
      start_shape->push_back(s_shape);
      if (j > 0) {
        auto s = sample;
        s.label = -1;
        samples->push_back(s);
      } else {
        sample.label = 1;
      }
    }
  }
}

/*
  *  @name       PruneDetections
  *  @brief      Amongst (potentially) several detections in an image,
  *              returns the one which center is closest to the annotation
  *              center
  *  @param[in]  annotation      Annotation
  *  @param[in]  detections      Vector of Rect returned by the face detector
  *  @return     Detection which center is closest to the annotation center
  *              or an empty Rect
  */
cv::Rect LBFTrainer::PruneDetections(const cv::Mat& annotation,
                                     const std::vector<cv::Rect>& detections) {
  cv::Rect return_rect;
  std::vector<cv::Rect> candidates_rect;  // Rects that have shape center
  // inside
  for (int j = 0; j < detections.size(); j++) {
    cv::Point2d center = LTS5::GetCenterPoint(annotation);
    if (center.x > detections[j].x &&
        center.x < detections[j].x + detections[j].width &&
        center.y > detections[j].y &&
        center.y < detections[j].y + detections[j].height) {
      candidates_rect.push_back(detections[j]);
    }
  }

  if (candidates_rect.size() == 1) {
    return_rect = candidates_rect[0];
  } else if (candidates_rect.size() > 1) {
    int minIdx = 0;
    double minDist = DBL_MAX;
    for (int j = 0; j < candidates_rect.size(); j++) {
      cv::Point2d rectangle_center(candidates_rect[j].x +
                                   candidates_rect[j].width / 2.0,
                                   candidates_rect[j].y +
                                   candidates_rect[j].height / 2.0);
      cv::Point2d pt1 = LTS5::GetCenterPoint(annotation);

      double dist = sqrt((rectangle_center.x - pt1.x) *
                         (rectangle_center.x - pt1.x) +
                         (rectangle_center.y - pt1.y) *
                         (rectangle_center.y - pt1.y));

      if (dist < minDist) {
        minIdx = j;
        minDist = dist;
      }
    }
    return_rect = candidates_rect[minIdx];
  }
  return return_rect;
}
}  // namespace LTS5
