/**
*  @file   svm_tracker_assessment.cpp
*  @brief  face tracker assessment class implementation
*
*  @author Hua Gao / Christophe Ecabert
*  @date   01/05/15
*  Copyright (c) 2015 Hua Gao. All rights reserved.
*/


#include <fstream>
#include <limits>
#include <iostream>
#include <string>

#include "lts5/face_tracker/svm_tracker_assessment.hpp"
#include "lts5/utils/utils.hpp"

/**
*  @namespace  LTS5
*  @brief      LTS5 laboratory dev space
*/
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   SvmTrackerAssessment
 *  @fn SvmTrackerAssessment(void)
 *  @brief  Constructor
 *  @param[in]    meanshape       Face model meanshape
 *  @param[in]    sift_parameters Sift descriptor parameters
 *  @param[in]    edge            Normalized image size.
 *  @param[in]    sift_size       Sift descriptor dimension
 */
SvmTrackerAssessment::SvmTrackerAssessment(const cv::Mat& meanshape,
                                           const LTS5::SSift::SSiftParameters& sift_parameters,
                                           const int edge,
                                           const int sift_size) {
  // Set parameters
  sift_parameters_ = sift_parameters;
  sift_size_ = sift_size;
  // Compute reference shape
  edge_ = edge;
  reference_shape_ = meanshape * edge_ + edge_/2.0;
}

/*
 *  @name   ~SvmTrackerAssessment
 *  @brief  Destructor
 */
SvmTrackerAssessment::~SvmTrackerAssessment(void) {
}

/*
 *  @name   Load
 *  @brief  Load the assessment model from file
 *  @param[in]  model_filename      Path to the model
 *  @return -1 if errors, 0 otherwise
 */
int SvmTrackerAssessment::Load(const std::string& model_name) {
  int error = -1;
  const bool is_binary = model_name.find(".bin") != std::string::npos;
  std::ios_base::openmode flag = (is_binary ?
                                  std::ios_base::in|std::ios_base::binary :
                                  std::ios_base::in);
  std::ifstream input_stream(model_name.c_str(), flag);
  // Open ?
  if (input_stream.is_open()) {
    if (is_binary) {
      int status = ScanStreamForObject(input_stream,
                                       HeaderObjectType::kSvmTrackerAssessment);
      if (status == 0) {
        error = this->Load(input_stream, is_binary);
      }
    } else {
      // Bias
      double svm_bias = -1;
      input_stream >> svm_bias;
      // Weight
      cv::Mat svm_w;
      error = LTS5::ReadMatFromChar(input_stream, &svm_w);
      svm_model_w_.create(1, svm_w.cols + 1, CV_64FC1);
      svm_model_w_.at<double>(0, svm_w.cols) = svm_bias;
      svm_w.copyTo(svm_model_w_.colRange(0, svm_w.cols));
    }
  }
  return error;
}

/*
 *  @name   Load
 *  @brief  Load the assessment model from file stream
 *  @param[in]  input_stream  File stream for reading the model
 *  @param[in]  is_binary     Indicate if stream is open has binary
 *  @return -1 if errors, 0 otherwise
 */
int SvmTrackerAssessment::Load(std::istream& input_stream,
                               const bool& is_binary) {
  int error = -1;
  if (input_stream.good()) {
    cv::Mat svm_w;
    double b;
    if (is_binary) {
      // Bin
      error = LTS5::ReadMatFromBin(input_stream, &svm_model_w_);
      input_stream.read(reinterpret_cast<char*>(&score_threshold_),
                        sizeof(score_threshold_));
    } else {
      // Get bias
      input_stream >> b;
      // Get weights
      error = LTS5::ReadMatFromChar(input_stream, &svm_w);

      svm_model_w_ = cv::Mat(1, svm_w.cols + 1, CV_64FC1);
      svm_model_w_.at<double>(0, svm_w.cols) = b;
      svm_w.copyTo(svm_model_w_.colRange(0, svm_w.cols));
    }
  }
  return error;
}

/*
 *  @name   Write
 *  @brief  Copy the object into a binary stream
 *  @param[in]  out_stream  Output binary stream
 *  @return -1 if error, 0 otherwise
 */
int SvmTrackerAssessment::Write(std::ostream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Ok
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kSvmTrackerAssessment);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type),
                     sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size),
                     sizeof(obj_size));
    // svm
    error = LTS5::WriteMatToBin(out_stream, svm_model_w_);
    // threshold
    out_stream.write(reinterpret_cast<const char*>(&score_threshold_),
                     sizeof(score_threshold_));
    // Done, sanity check
    error |= out_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name ComputeObjectSize
 *  @brief  Compute the object size in bytes
 *  @return Object memory size
 */
int SvmTrackerAssessment::ComputeObjectSize(void) const {
  int size = 0;
  // Matrix header
  size += 3 * sizeof(int);
  size += svm_model_w_.total() * svm_model_w_.elemSize();
  // Threshold
  size += sizeof(double);
  return size;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Assess
 *  @fn double Assess(const cv::Mat& image, const cv::Mat& shape)
 *  @brief  Assessment method, given the current shape and image
 *  @param[in]  image   Input image
 *  @param[in]  shape     Current shape tracked by the face tracker
 *  @return   scalar score, the larger the better
 */
double SvmTrackerAssessment::Assess(const cv::Mat& image,
                                    const cv::Mat& shape) {
  double score = -std::numeric_limits<double>::max();
  if (!image.empty() && !shape.empty()) {
    // Extract feature
    this->ExtractFeature(image, shape);
    // Classify
    cv::Mat mw = svm_model_w_.colRange(0, features_.cols);
    this->score_ = mw.dot(features_) + svm_model_w_.at<double>(0,
                                                        features_.cols);
    score = this->score_ > score_threshold_ ? 1.0 : -1.0;
  }
  return score;
}

/**
 * @name  ExtractFeature
 * @fn    void ExtractFeature(const cv::Mat& image, const cv::Mat& shape)
 * @brief Extract sift feature at specific location
 * @param[in] image   Input image
 * @param[in] shape   Current position of tracked landmarks
 */
void SvmTrackerAssessment::ExtractFeature(const cv::Mat& image,
                                          const cv::Mat& shape) {
  // Normalize shape and image in order to extract significant features
  cv::Mat normalized_shape = shape.clone();
  cv::Mat normalized_image, dummy, transform;
  NormalizeScaleAndRotation(reference_shape_,
                            edge_,
                            image,
                            &normalized_image,
                            &normalized_shape,
                            &dummy, &transform);
  std::vector<cv::KeyPoint> sift_extraction_location;
  int num_points = shape.rows/2;
  double x = 0.0;
  double y = 0.0;
  for (int p = 0; p < num_points; p++) {
    // only inner face landmarks and without 60 and 64 (inside lip cornner)
    if (p < 17 || p == 60 || p == 64) {
      continue;
    }
    x = normalized_shape.at<double>(p, 0);
    y = normalized_shape.at<double>(p + num_points, 0);
    cv::KeyPoint kp(cvRound(x), cvRound(y), sift_size_);
    sift_extraction_location.push_back(kp);
  }
  cv::Mat features;
  SSift::ComputeDescriptorOpt(normalized_image,
                              sift_extraction_location,
                              sift_parameters_,
                              &features);
  features.convertTo(features_, CV_64FC1);
  features_ = features_.reshape(features_.channels(), 1);
}



}  // namespace LTS5
