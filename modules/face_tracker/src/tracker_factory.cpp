/**
 *  @file   tracker_factory.cpp
 *  @brief  Instantiate face tracker for a given type
 *  @ingroup    face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   14/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/face_tracker/tracker_factory.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name  Get
 * @fn    static TrackerFactory& Get(void)
 * @brief Provide single instance of factory
 * @return    Factory instance
 */
TrackerFactory&  TrackerFactory::Get(void) {
  static TrackerFactory factory;
  return factory;
}

/*
 *  @name CreateByName
 *  @fn Image* CreateByExtension(const std::string& extension)
 *  @brief  Create an image based on the extension type
 *  @param[in]  extension  Image extension (type)
 *  @return Pointer to an instance of the given type, nullptr if type is
 *  unknown
 */
TrackerFactory::Tracker* TrackerFactory::CreateByName(const std::string& name) {
  Tracker* tracker = nullptr;
  for (auto& p : proxies_) {
    if (strcmp(p->Name(), name.c_str()) == 0) {
      tracker = p->Create();
      break;
    }
  }
  return tracker;
}

/*
 *  @name Register
 *  @fn void Register(const TrackerProxy* object)
 *  @brief  Register a type of tracker with a given proxy.
 *  @param[in]  object  Object to register
 */
void TrackerFactory::Register(const TrackerProxy* object) {
  proxies_.push_back(object);
}

/*
 * @name  Get
 * @fn    static MTrackerFactory& Get(void)
 * @brief Provide single instance of factory
 * @return    Factory instance
 */
MTrackerFactory&  MTrackerFactory::Get(void) {
  static MTrackerFactory factory;
  return factory;
}

/*
 *  @name CreateByName
 *  @fn Tracker* CreateByName(const std::string& extension)
 *  @brief  Create a tracker based on the name
 *  @param[in] N      Number of stream needed
 *  @param[in]  name  Tracker name
 *  @return Pointer to an instance of the given type, nullptr if type is
 *  unknown
 */
MTrackerFactory::Tracker* MTrackerFactory::CreateByName(const size_t N,
                                                        const std::string& name) {
  Tracker* tracker = nullptr;
  for (auto& p : proxies_) {
    if (strcmp(p->Name(), name.c_str()) == 0) {
      tracker = p->Create(N);
      break;
    }
  }
  return tracker;
}

/*
 *  @name Register
 *  @fn void Register(const MTrackerProxy* object)
 *  @brief  Register a type of tracker with a given proxy.
 *  @param[in]  object  Object to register
 */
void MTrackerFactory::Register(const MTrackerProxy* object) {
  proxies_.push_back(object);
}

}  // namepsace LTS5
