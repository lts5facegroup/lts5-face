/**
 *  @file   mstream_lbf_tracker.cpp
 *  @brief  LBFTRacker for multiple image stream.
 *
 *  @author Christophe Ecabert
 *  @date   23/03/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <fstream>

#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/face_tracker/mstream_lbf_tracker.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/face_tracker/lbf_tracker_pimpl.hpp"
#include "lts5/face_tracker/lbf_tracker_assessment.hpp"
#include "lts5/face_tracker/lbf_tracker.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name   MStreamLBFTracker
 *  @fn MStreamLBFTracker(const size_t n_stream)
 *  @brief  Constructor
 *  @param[in]  n_stream  Number of image to track
 */
MStreamLBFTracker::MStreamLBFTracker(const size_t n_stream) :
        n_stream_(n_stream),
        lbf_cascade_(nullptr),
        face_detector_(nullptr),
        tracking_assessment_(nullptr),
        is_tracking_(n_stream_, false),
        previous_shape_(n_stream_),
        working_img_(n_stream_),
        shape_center_gravity_(n_stream_, cv::Point2f(-1.f, -1.f))  {
}

/*
 *  @name   ~MStreamLBFTracker
 *  @fn ~MStreamLBFTracker()
 *  @brief  Destructor
 */
MStreamLBFTracker::~MStreamLBFTracker() {
  if (lbf_cascade_) {
    delete lbf_cascade_;
    lbf_cascade_ = nullptr;
  }
  if (face_detector_) {
    delete face_detector_;
    face_detector_ = nullptr;
  }
  if (tracking_assessment_) {
    delete tracking_assessment_;
    tracking_assessment_ = nullptr;
  }
}

/*
 *  @name   Load
 *  @fn     int Load(const std::string& model_filename);
 *  @brief  Load the LBF model
 *  @param[in]  model_filename      Path to the model
 *  @return -1 if errors, 0 otherwise
 */
int MStreamLBFTracker::Load(const std::string& model_filename) {
  int err = -1;
  std::ifstream stream(model_filename.c_str(),
                       std::ios_base::in | std::ios_base::binary);
  if (stream.is_open()) {
    // Load
    err = this->Load(stream);
    // Close
    stream.close();
  }
  return err;
}

/*
 *  @name   Load
 *  @fn     int Load(const std::string& model_filename,
            const std::string& face_detector_config)
 *  @brief  Load the LBF model and Viola Jones face detector
 *  @param[in]  model_filename          Path to the model
 *  @param[in]  face_detector_config    Configuration file for
 *                                      Viola & Jones face detector
 *  @return -1 if errors, 0 otherwise
 */
int MStreamLBFTracker::Load(const std::string& model_filename,
         const std::string& face_detector_config) {
  // Init LBF tracker
  int err = this->Load(model_filename);
  // Init Viola-Jones detector
  if (face_detector_) {
    face_detector_->load(model_filename);
  } else {
    face_detector_ = new cv::CascadeClassifier(face_detector_config);
  }
  err = !face_detector_->empty() ? err : -1;
  return err;
}

/*
 *  @name   Load
 *  @fn     int Load(std::ifstream& stream)
 *  @brief  Load the LBF model
 *  @param[in]  model_stream      Stream to the model
 *  @return -1 if errors, 0 otherwise
 */
int MStreamLBFTracker::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Seek for LBF tracker
    int status = LTS5::ScanStreamForObject(stream,
                                           HeaderObjectType::kLBFTracker);
    if (status == 0) {
      // Check of already init
      if (lbf_cascade_ != nullptr) {
        delete lbf_cascade_;
        lbf_cascade_ = nullptr;
      }
      // Read meanshape
      err = ReadMatFromBin(stream, &meanshape_);
      // Init cascade
      lbf_cascade_ = new LBFCascade();
      err |= lbf_cascade_->Load(stream);
      // Tracker assessment
      if (tracking_assessment_ != nullptr) {
        delete tracking_assessment_;
        tracking_assessment_ = nullptr;
      }
      tracking_assessment_ = new LBFTrackerAssessment(meanshape_);
      err |= tracking_assessment_->Load(stream, true);
    }
  }
  return err;
}

/*
 *  @name   Save
 *  @fn int Save(const std::string& model_name)
 *  @brief  Save the trained model in file.
 *  @param  [in]    model_name      Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int MStreamLBFTracker::Save(const std::string& model_name) {
  int err = -1;
  std::ofstream stream(model_name.c_str(),
                       std::ios_base::out | std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Write(stream);
  }
  return err;
}

/*
 *  @name   Write
 *  @fn int Write(std::ostream& out_stream) const
 *  @brief  Write the object into a binary stream
 *  @param[in]  out_stream  Binary stream to files
 *  @return -1 if error, 0 otherwise.
 */
int MStreamLBFTracker::Write(std::ostream& out_stream) const {
  int err = -1;
  if (out_stream.good()) {
    // Define object size
    int sz = 3 * sizeof(int);
    sz += meanshape_.total() * meanshape_.elemSize();
    sz += lbf_cascade_->ComputeObjectSize();
    // Write
    int dummy = static_cast<int>(HeaderObjectType::kLBFTracker);
    out_stream.write(reinterpret_cast<const char*>(dummy),
                     sizeof(dummy));
    out_stream.write(reinterpret_cast<const char*>(sz),
                     sizeof(sz));
    // Meanshape
    err |= LTS5::WriteMatToBin(out_stream, meanshape_);
    // Tracker
    err |= lbf_cascade_->Write(out_stream);
    err |= out_stream.good() ? 0 : -1;
  }
  return err;
}

/*
 *  @name   ComputeObjectSize
 *  @fn int ComputeObjectSize() const
 *  @brief  Compute the memory used by object
 *  @return Size in bytes
 */
int MStreamLBFTracker::ComputeObjectSize() const {
  int sz = 3 * sizeof(int);
  sz += meanshape_.total() * meanshape_.elemSize();
  sz += lbf_cascade_->ComputeObjectSize();
  return sz;
}

#pragma mark -
#pragma mark Tracking

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set
 *          of landmarks.
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamLBFTracker::Detect(const cv::Mat& image, cv::Mat* shape) {
  return this->Detect(image, 0, shape);
}

/*
 *  @name   Detect
 *  @fn int Detect(const std::vector<cv::Mat>&images,
                   std::vector<cv::Mat>* shapes)
 *  @brief  Dectection method, given a multiples image, provides a set of
 *          landmarks for each image. This must be override by subclass
 *  @param[in]  images   List of input images
 *  @param[out] shapes   List of set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamLBFTracker::Detect(const std::vector<cv::Mat>& images,
                              std::vector<cv::Mat>* shapes) {
  assert(images.size() == n_stream_);
  shapes->resize(n_stream_);
  int err = 0;
  for (size_t i = 0; i < n_stream_; ++i) {
    err |= this->Detect(images[i], i, &shapes->at(i));
  }
  return err;
}

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image,
                   const cv::Rect& face_region,
                   cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks.
 *  @param[in]  image           Input image
 *  @param[in]  face_region     Face boundingg box
 *  @param[out] shape           Set of landmarks
 *  @return     >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamLBFTracker::Detect(const cv::Mat& image,
                              const cv::Rect& face_region,
                              cv::Mat* shape) {
  return this->Detect(image, face_region, 0, shape);
}

/*
 *  @name   Detect
 *  @fn int Detect(const std::vector<cv::Mat>& images,
                   const std::vector<cv::Rect>& face_regions,
                   std::vector<cv::Mat>* shapes)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks. This must be override by subclass
 *  @param[in]  images           List of input image
 *  @param[in]  face_regions     List of face boundingg box
 *  @param[out] shapes           List of set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamLBFTracker::Detect(const std::vector<cv::Mat>& images,
                              const std::vector<cv::Rect>& face_regions,
                              std::vector<cv::Mat>* shapes) {
  assert((images.size() == n_stream_) && face_regions.size() == n_stream_);
  shapes->resize(n_stream_);
  // Loop over all image stream
  int err = 0;
  for (size_t i = 0; i < n_stream_; ++i) {
    err |= this->Detect(images[i], face_regions[i], i, &shapes->at(i));
  }
  return err;
}

/*
 *  @name   Track
 *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Tracking method, given an input image
 *  @param[in]      image   Input image
 *  @param[in,out]  shape   Set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamLBFTracker::Track(const cv::Mat& image, cv::Mat* shape) {
  return this->Track(image, 0, shape);
}

/*
 *  @name   Track
 *  @fn int Track(const std::vector<cv::Mat>& images,
                  std::vector<cv::Mat>* shapes)
 *  @brief  Tracking method, given an input image (from a sequence), provides
 *          a set of landmarks. This must be override by subclass
 *  @param[in]  images   List of input image
 *  @param[out] shapes   List of set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamLBFTracker::Track(const std::vector<cv::Mat>& images,
                             std::vector<cv::Mat>* shapes) {
  assert(images.size() == n_stream_);
  shapes->resize(n_stream_);
  int err = 0;
  for (size_t i = 0; i < n_stream_; ++i) {
    err |= this->Track(images[i], i, &shapes->at(i));
  }
  return err;
}

#pragma mark -
#pragma mark Training

/*
 *  @name   Train
 *  @fn void Train(const std::string& folder)
 *  @brief  LBF training method
 *  @param[in]  folder  Location of images/annotations
 */
void MStreamLBFTracker::Train(const std::string& folder) {}

#pragma mark -
#pragma mark Private

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image,const size_t n_stream, cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set
 *          of landmarks.
 *  @param[in]  image     Input image
 *  @param[in]  n_stream  Current stream index
 *  @param[out] shape     Set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamLBFTracker::Detect(const cv::Mat& image,
                              const size_t n_stream,
                              cv::Mat* shape) {
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_[n_stream], cv::COLOR_BGR2GRAY);
  } else {
    working_img_[n_stream] = image;
  }
  // Downsample image to speedup computation
  float aspect_ratio = ((float)working_img_[n_stream].rows /
                        (float)working_img_[n_stream].cols);
  int img_height = static_cast<int>(320.f * aspect_ratio);
  cv::Mat downsample_input;
  cv::resize(working_img_[n_stream],
             downsample_input,
             cv::Size(320, img_height));
  float scale = (static_cast<float>(working_img_[n_stream].cols) /
                 static_cast<float>(downsample_input.cols));
  // Scan image for faces
  int err = 0;
  cv::Rect face_region;
  std::vector<cv::Rect> faces_location;
  // Lock
  face_detector_->detectMultiScale(downsample_input, faces_location,
                                   1.1, 2, 0,
                                   cv::Size((200.0f / scale),
                                            (200.0f / scale)));
  if (faces_location.size() > 0) {
    // Detector has return something
    // Do we have already have a good fit ?
    if (shape_center_gravity_[n_stream].x < 0 &&
        shape_center_gravity_[n_stream].y < 0) {
      // No previous good fit, use largest rectangle
      std::sort(faces_location.begin(),
                faces_location.end(),
                LBFTracker::SortRectBySize());
      face_region = faces_location[0];
    } else {
      //  Use face rect closest to the previous one
      float min_dist = std::numeric_limits<float>::max();
      cv::Point2f current_center;
      cv::Point2f face_displacement;
      float norm_face_displacement = -1.f;

      // Get through the list
      for (cv::Rect rect : faces_location) {
        current_center = (rect.br() + rect.tl()) * 0.5f * scale;
        face_displacement = shape_center_gravity_[n_stream] - current_center;
        norm_face_displacement = std::sqrt(face_displacement.x *
                                           face_displacement.x +
                                           face_displacement.y *
                                           face_displacement.y);
        // Closest ?
        if (norm_face_displacement < min_dist) {
          min_dist = norm_face_displacement;
          face_region = rect;
        }
      }
    }
    // Convert back to original image coordinate
    int x = static_cast<int>(face_region.x * scale);
    int y = static_cast<int>(face_region.y * scale);
    int w = static_cast<int>(face_region.width * scale);
    int h = static_cast<int>(face_region.height * scale);
    face_region.x = x;
    face_region.y = y;
    face_region.width = w;
    face_region.height = h;
    // Can track landmarks, create inital shape. If size is Ok does nothing
    shape->create(meanshape_.rows, meanshape_.cols, meanshape_.type());
    previous_shape_[n_stream].create(meanshape_.rows,
                                     meanshape_.cols,
                                     meanshape_.type());
    // Compute initial shape
    this->ComputeInitialShape(meanshape_, face_region, shape);
    //  Face alignment
    lbf_cascade_->Predict(working_img_[n_stream], meanshape_, shape);
    // Save shape as previous
    previous_shape_[n_stream] = shape->clone();
  } else {
    err |= (0x01<<n_stream);
  }
  return err;
}

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image,
                   const cv::Rect& face_region,
                   const size_t n_stream,
                   cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks.
 *  @param[in]  image           Input image
 *  @param[in]  face_region     Face boundingg box
 *  @param[in]  n_stream        Current stream index
 *  @param[out] shape           Set of landmarks
 *  @return     >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamLBFTracker::Detect(const cv::Mat& image,
                              const cv::Rect& face_region,
                              const size_t n_stream,
                              cv::Mat* shape) {
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_[n_stream], cv::COLOR_BGR2GRAY);
  } else {
    working_img_[n_stream] = image;
  }
  // Scan image for faces
  int err = 0;
  // Can track landmarks, create inital shape. If size is Ok does nothing
  shape->create(meanshape_.rows, meanshape_.cols, meanshape_.type());
  previous_shape_[n_stream].create(meanshape_.rows,
                                   meanshape_.cols,
                                   meanshape_.type());
  // Initial shape
  this->ComputeInitialShape(meanshape_, face_region, shape);
  // Alignment
  lbf_cascade_->Predict(working_img_[n_stream], meanshape_, shape);
  // Save shape as previous, deep copy
  previous_shape_[n_stream] = shape->clone();
  return err;
}

/*
 *  @name   Track
 *  @fn int Track(const cv::Mat& image,const size_t n_stream, cv::Mat* shape)
 *  @brief  Tracking method, given an input image
 *  @param[in]      image   Input image
 *  @param[in,out]  shape   Set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamLBFTracker::Track(const cv::Mat& image,
                             const size_t n_stream,
                             cv::Mat* shape) {
  int err = -1;
  // Image format is correct ?
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_[n_stream], cv::COLOR_BGR2GRAY);
  } else {
    working_img_[n_stream] = image;
  }
  // Have we already detect something ?
  if (!is_tracking_[n_stream]) {
    // Call for detection
    err = this->Detect(working_img_[n_stream], n_stream, shape);
    is_tracking_[n_stream] = true;
  } else {
    // Tracking
    previous_shape_[n_stream] = shape->clone();
    this->AlignMeanshapeToCurrent(previous_shape_[n_stream],
                                  meanshape_,
                                  shape);
    lbf_cascade_->Predict(working_img_[n_stream], meanshape_, shape);
    err = 0;
  }
  // still tracking ?
  is_tracking_[n_stream] = tracking_assessment_->Assess(working_img_[n_stream],
                                                        *shape) > 0.0;
  if (!is_tracking_[n_stream]) {
    *shape = cv::Mat();
    is_tracking_[n_stream] = false;
  } else {
    // Compute shape center of gravity
    const int n = std::max(meanshape_.rows, meanshape_.cols) / 2;
    shape_center_gravity_[n_stream].x = 0;
    shape_center_gravity_[n_stream].y = 0;
    for (int i = 0; i < n; ++i) {
      shape_center_gravity_[n_stream].x += shape->at<double>(i);
      shape_center_gravity_[n_stream].y += shape->at<double>(i + n);
    }
    shape_center_gravity_[n_stream].x /= n;
    shape_center_gravity_[n_stream].y /= n;
  }
  return err;
}

/*
 *  @name   ComputeInitialShape
 *  @brief  Aligned the meanshape inside the bounding box
 *  @param[in]  meanshape       Model meanshape
 *  @param[in]  bounding_box    Region of interest
 *  @param[out] shape           Initial shape
 */
void MStreamLBFTracker::ComputeInitialShape(const cv::Mat &meanshape,
                                            const cv::Rect &bbox,
                                            cv::Mat *shape) {
  // Goes through all points
  int n = std::max(meanshape.rows, meanshape.cols) / 2;
  const double* ms_ptr = reinterpret_cast<const double*>(meanshape.data);
  double* s_ptr = reinterpret_cast<double*>(shape->data);
  for (int p = 0 ; p < n ; ++p) {
    s_ptr[p] = bbox.x + ms_ptr[p] * bbox.width;
    s_ptr[p + n] = bbox.y + ms_ptr[p + n] * bbox.height;
  }
}

/*
 * @name  AlignMeanshapeToCurrent
 * @fn    void AlignMeanshapeToCurrent(const cv::Mat& current_shape,
                             const cv::Mat& meanshape,
                             cv::Mat* shape)
 * @brief Place the \p meanshape at the position of \p current_shape
 * @param[in] current_shape   Current shape
 * @param[in] meanshape       Meanshape
 * @param[out] shape          New shape (transformed meanshape)
 */
void MStreamLBFTracker::AlignMeanshapeToCurrent(const cv::Mat& current_shape,
                                                const cv::Mat& meanshape,
                                                cv::Mat* shape) {
  // Init
  shape->create(meanshape.rows, meanshape.cols, meanshape.type());
  double* s_ptr = reinterpret_cast<double*>(shape->data);
  const double* ms_ptr = reinterpret_cast<const double*>(meanshape.data);
  // Compute similarity
  cv::Mat transform;
  ComputeSimilarityTransform(meanshape, current_shape, &transform);
  const double* t_ptr = reinterpret_cast<const double*>(transform.data);
  // Apply
  int n = std::max(meanshape.rows, meanshape.cols) / 2;
  for (int i = 0; i < n; ++i) {
    const double x = ms_ptr[i];
    const double y = ms_ptr[i + n];
    s_ptr[i] = t_ptr[0] * x + t_ptr[1] * y + t_ptr[2];
    s_ptr[i + n] = t_ptr[3] * x + t_ptr[4] * y + t_ptr[5];
  }
}

/**
 * @name  ~MLBFProxy
 * @fn    ~MLBFProxy() override
 * @brief Destructor
 */
MLBFProxy::~MLBFProxy() {}

/**
 * @name  Create
 * @fn    MStreamFaceTracker<cv::Mat, cv::Mat>* Create(const int N) const override
 * @brief Create an instance of MStreamFaceTracker
 * @param[in] N number of stream
 * @return    Tracker instance with proper type
 */
MStreamFaceTracker<cv::Mat, cv::Mat>* MLBFProxy::Create(const int N) const {
  return new MStreamLBFTracker(N);
};

/**
 *  @name Name
 *  @fn const char* Name() const override
 *  @brief  Return the name for a given type of tracker
 *  @return Tracker name (i.e. sdm, lbf, ...)
 */
const char* MLBFProxy::Name() const {
  return "multi_lbf";
}

/** Explicit registration */
MLBFProxy mlbf_proxy;

}  // namepsace LTS5
