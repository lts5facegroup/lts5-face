/**
 *  @file   point_distribution_model.cpp
 *  @brief  Point distribution model implementation
 *
 *  @author Hua Gao
 *  @date   30/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#include <iostream>
#include <limits>
#include <string>
#include <vector>
#include <assert.h>

#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

#define db at<double>

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization
/*
 *  @name   PointDistributionModel
 *  @brief  Constructor
 */
PointDistributionModel::PointDistributionModel() {
}

/*
 *  @name  PointDistributionModel
 *  @brief Constructor
 *  @param[in] model_name file name of the model
 */
PointDistributionModel::PointDistributionModel(const std::string& model_name) {
  Load(model_name);
}

/*
 *  @name   PointDistributionModel
 *  @brief  Copy constructor
 */
PointDistributionModel::PointDistributionModel(const PointDistributionModel& other) {
  eigen_vectors_ = other.eigen_vectors_.clone();
  eigen_values_ = other.eigen_values_.clone();
  means_ = other.means_.clone();
  num_points_ = other.num_points_;
  p_local_ = other.p_local_.clone();
  p_globl_ = other.p_globl_.clone();
}

/*
 *  @name   operator=
 *  @brief  Assignment operator
 *  @return Copy of the object
 */
PointDistributionModel& PointDistributionModel::operator=(const PointDistributionModel& other) {
  eigen_vectors_ = other.eigen_vectors_.clone();
  eigen_values_ = other.eigen_values_.clone();
  means_ = other.means_.clone();
  num_points_ = other.num_points_;
  p_local_ = other.p_local_.clone();
  p_globl_ = other.p_globl_.clone();
  return *this;
}

/*
 *  @name   ~PointDistributionModel
 *  @brief  Destructor
 */
PointDistributionModel::~PointDistributionModel() {
}

/*
 *  @name   Load
 *  @brief  Load model data from file
 *  @param[in] model_name file name of the model
 *  @return
 */
int PointDistributionModel::Load(const std::string& model_name) {
  int error = -1;
  const bool is_binary = (model_name.find(".bin") != std::string::npos);
  std::ios_base::openmode flag = (is_binary ?
                                  std::ios_base::in | std::ios_base::binary :
                                  std::ios_base::in);
  std::ifstream ifs(model_name.c_str(), flag);
  if(ifs.is_open()) {
    int status = LTS5::ScanStreamForObject(ifs,
                                           HeaderObjectType::kPointDistributionModel);
    if (status == 0) {
      error = this->Load(ifs, is_binary);
      ifs.close();
    }
  }
  return error;
}

/*
 *  @name   LoadModel
 *  @brief  Load model data from stream
 *  @param[in]  input_stream  input file stream
 *  @param[in]  is_binary     Binary input stream
 *  @return
 */
int PointDistributionModel::Load(std::istream& input_stream,
                                 const bool is_binary) {
  int error = -1;
  if (input_stream.good()) {
    if (is_binary) {
      // Binary stream
      error = LTS5::ReadMatFromBin(input_stream, &eigen_vectors_);
      error |= LTS5::ReadMatFromBin(input_stream, &eigen_values_);
      error |= LTS5::ReadMatFromBin(input_stream, &means_);
    } else {
      // Old char type
      bool readType = true;
      if (readType) {
        int type;
        input_stream >> type;
        assert(type == 0);
      }

        // t, r, c
        // model stored: r, c, t
        // attention, change
        error = ReadMatFromChar(input_stream, &eigen_vectors_);
        error |= ReadMatFromChar(input_stream, &eigen_values_);
        error |= ReadMatFromChar(input_stream, &means_);
    }
    num_points_ = means_.rows/3;
    p_local_ = cv::Mat(eigen_vectors_.cols, 1, CV_64F, cv::Scalar(0));
    p_globl_ = cv::Mat(6, 1, CV_64F, cv::Scalar(0));
  }
  return error;
}

/*
 *  @name   Write
 *  @brief  Write the object into a binary stream
 *  @param[in]  out_stream  Binary stream to files
 *  @return -1 if error, 0 otherwise.
 */
int PointDistributionModel::Write(std::ofstream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Ok can write -> Header
    int obj_type = static_cast<int>(HeaderObjectType::kPointDistributionModel);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type),
                     sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size),
                     sizeof(obj_size));
    // Eigen vectors
    error = LTS5::WriteMatToBin(out_stream, eigen_vectors_);
    // Eigen values
    error |= LTS5::WriteMatToBin(out_stream, eigen_values_);
    // Mean
    error |= LTS5::WriteMatToBin(out_stream, means_);
    // Done sanity check
    error |= out_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name   ComputeObjectSize
 *  @brief  Compute the memory used by object
 *  @return Size in bytes
 */
int PointDistributionModel::ComputeObjectSize(void) const {
  // Matrix headers
  int size = 3 * 3 * sizeof(int);
  // Matrix size
  size += eigen_vectors_.total() * eigen_vectors_.elemSize();
  size += eigen_values_.total() * eigen_values_.elemSize();
  size += means_.total() * means_.elemSize();
  return size;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   CalcParams
 *  @brief  Calculate rigid and non-rigid model parameters given 2D shape
 *  @param[in] s         input 2D shape vector
 *  @param[out] p_local  non-rigid shape parameter
 *  @param[out] p_globl  rigid shape parameter
 *  @return
 */
cv::Mat PointDistributionModel::CalcParams(const cv::Mat &s,
                                           cv::Mat *plocal,
                                           cv::Mat *pglobl) {
  using TransposeType = LTS5::LinearAlgebra<double>::TransposeType;
  assert((s.type() == CV_64F) &&
         (s.rows == 2*(means_.rows/3)) &&
         (s.cols == 1));

  if ((pglobl->rows != 6) ||
      (pglobl->cols != 1) ||
      (pglobl->type() != CV_64F)) {
    pglobl->create(6, 1, CV_64F);
  }

  int j;
  int n = means_.rows/3;  // 66

  double si;
  double scale = 0.0;
  double pitch = 0.0;
  double yaw = 0.0;
  double roll = 0.0;
  double tx = 0.0;
  double ty = 0.0;
  double Tx = 0.0;
  double Ty = 0.0;
  double Tz = 0.0;

  cv::Mat R(3, 3, CV_64F);
  cv::Mat z(n, 1, CV_64F);
  cv::Mat t(3, 1, CV_64F);
  cv::Mat p(eigen_vectors_.cols, 1, CV_64F);
  cv::Mat r = R.row(2);
  cv::Mat S(n, 3, CV_64F);

  cv::Mat S_  =  means_.clone();
  plocal->create(eigen_vectors_.cols, 1, CV_64F);  // 24x1
  *plocal = 0.0;
  const double* t_ptr = reinterpret_cast<const double*>(t.data);
  const double* R_ptr = reinterpret_cast<const double*>(R.data);
  for (int iter = 0; iter < 100; iter++) {
    // usually converges in ~6 iterations
    S_ = means_.clone();
    LTS5::LinearAlgebra<double>::Gemv(eigen_vectors_,
                                      TransposeType::kNoTranspose,
                                      1.0,
                                      *plocal,
                                      1.0,
                                      &S_);
    // s:init 2D shape (transformed reference shape according to the detected
    //   face rectangle)
    // S_:3D shape (3D mean shape if iter=0, fitted 3D shape if iter != 0)
    AlignShapes3D2D(s, S_, &scale, &pitch, &yaw, &roll, &tx, &ty);

    // Compute Rotation matrix from pitch, roll and yaw
    Euler2Rot(pitch, yaw, roll, &R);

    // Compute z by using 3D shape, scale parameter and the 2nd row of Rotation
    // matrix. z might be computed to form the corresponding 3rd dimension in
    // order to be used with 2D shape below ???????
    S = (S_.reshape(1, 3));  // S_.reshape(1,3) -> 3 x 66 O(0)
    z = scale * r * S;

    si = 1.0/scale;
    Tx = -si * (R.db(0, 0) * tx + R.db(1, 0) * ty);
    Ty = -si * (R.db(0, 1) * tx + R.db(1, 1) * ty);
    Tz = -si * (R.db(0, 2) * tx + R.db(1, 2) * ty);

    for (j = 0; j < n; j++) {
      // update 3D shape by using 2D shape, z and Rotation matrix
      t.db(0, 0) = s.db(j);
      t.db(1, 0) = s.db(j + n);
      t.db(2, 0) = z.db(j);
      S_.db(j, 0) = (si * (t_ptr[0] * R_ptr[0] +
                           t_ptr[1] * R_ptr[3] +
                           t_ptr[2] * R_ptr[6]) + Tx);
      S_.db(j + n, 0) = (si * (t_ptr[0] * R_ptr[1] +
                               t_ptr[1] * R_ptr[4] +
                               t_ptr[2] * R_ptr[7]) + Ty);
      S_.db(j + n*2, 0) = (si * (t_ptr[0] * R_ptr[2] +
                                 t_ptr[1] * R_ptr[5] +
                                 t_ptr[2] * R_ptr[8]) + Tz);
    }

    // Compute plocal by considering the difference bw 3D shape and mean 3D
    // shape, but 3D shape requires 2D shape, z and Rotation matrix
    // So, the update of plocal depends on 2D shape too!
    //*plocal = eigen_vectors_t_ * (S_ - means_);
    LTS5::LinearAlgebra<double>::Gemv(eigen_vectors_,
                                      TransposeType::kTranspose,
                                      1.0,
                                      (S_ - means_),
                                      0.0,
                                      plocal);
    if (iter > 0) {
      // if there is no big change, it is converged and stop iterating.
      // Usually, it takes ~6 iterations to converge
      if (LTS5::LinearAlgebra<double>::L2Norm(*plocal - p) < 1.0e-5)
        break;
    }
    plocal->copyTo(p);
  }
  pglobl->db(0, 0) = scale;
  pglobl->db(1, 0) = pitch;
  pglobl->db(2, 0) = yaw;
  pglobl->db(3, 0) = roll;
  pglobl->db(4, 0) = tx;
  pglobl->db(5, 0) = ty;

  return S_;
}

/*
 *  @name   CalcPoseAngle
 *  @brief  Calculate 3D head pose angles with given 2D shape
 *  @param[in]  s      2D shape vector
 *  @param[out] pitch  pitch angle
 *  @param[out] yaw    yaw angle
 *  @param[out] roll   roll angle
 *  @return
 */
void PointDistributionModel::CalcPoseAngle(const cv::Mat& s,
                                           double* pitch,
                                           double* yaw,
                                           double* roll) {
  int nPoints = s.rows/2;
  cv::Mat shape66(0, 1, s.type());
  int off_set = 0;

  for (int p = 0; p < nPoints * 2; p++) {
    if (p != 60 - off_set &&
        p != 64- off_set &&
        p != 60 - off_set + nPoints &&
        p != 64 - off_set + nPoints) {
      shape66.push_back(s.row(p));
    }
  }

  // estimate and draw pose ...
  cv::Mat pl, pg;
  cv::Mat S3d = CalcParams(shape66, &pl, &pg);

  *roll = -pg.db(3, 0);
  *yaw = pg.db(2, 0);
  *pitch = pg.db(1, 0);
}



/*
 *  @name   CalcShape2D
 *  @brief  Calculate 2D shape from given rigid and non-rigid parameters
 *  @param[out] s        output 2D shape vector
 *  @param[in]  p_local  non-rigid shape parameter
 *  @param[in]  p_globl  rigid shape parameter
 *  @return
 */
void PointDistributionModel::CalcShape2D(const cv::Mat &plocal,
                                         const cv::Mat &pglobl,
                                         cv::Mat* s) {
  using TransposeType = LTS5::LinearAlgebra<double>::TransposeType;
  assert((plocal.type() == CV_64F) &&
         (pglobl.type() == CV_64F));
  assert((plocal.rows == eigen_values_.cols) && (plocal.cols == 1));
  assert((pglobl.rows == 6) && (pglobl.cols == 1));

  int n = means_.rows/3;
  double a = pglobl.db(0);  // scaling factor
  double x = pglobl.db(4);  // x offset
  double y = pglobl.db(5);  // y offset

  cv::Mat R(3, 3, CV_64F);
  // get the rotation matrix from the euler angles
  Euler2Rot(pglobl.db(1), pglobl.db(2), pglobl.db(3), &R);
  // get the 3D shape of the object
  cv::Mat S = means_.clone();
  LTS5::LinearAlgebra<double>::Gemv(eigen_vectors_,
                                    TransposeType::kNoTranspose,
                                    1.0,
                                    plocal,
                                    1.0,
                                    &S);
  // create the 2D shape matrix
  s->create( num_points_ * 2, 1, CV_64FC1);
  const double* rot_ptr = reinterpret_cast<const double*>(R.data);
  for (int i = 0; i < num_points_; i++) {
    // Transform this using the weak-perspective mapping to 2D from 3D
    // ref. paper: Nonrigid structure from motion, PAMI'08
    s->db(i) = a * (rot_ptr[0] * S.db(i, 0) +
                    rot_ptr[1] * S.db(i + n, 0) +
                    rot_ptr[2] * S.db(i + n * 2, 0) ) + x;
    s->db(i+n) = a * (rot_ptr[3] * S.db(i,0) +
                      rot_ptr[4] * S.db(i + n, 0) +
                      rot_ptr[5] * S.db(i + n * 2, 0) ) + y;
  }
}

/*
 *  @name TransRefShape
 *  @brief Translate shape by removing min_x/min_y (bring to zero)
 *  @param[in,out]  shape   Shape
 */
void PointDistributionModel::TransRefShape(cv::Mat* shape) {
  int npts = shape->rows / 2;
  double min_x = std::numeric_limits<double>::max();
  double max_x = std::numeric_limits<double>::lowest();
  double min_y = std::numeric_limits<double>::max();
  double max_y = std::numeric_limits<double>::lowest();
  double x = 0.0;
  double y = 0.0;

  for (int i = 0; i < npts; i++) {
    x = shape->at<double>(i);
    y = shape->at<double>(i + npts);
    // X
    min_x = x < min_x ? x : min_x;
    max_x = x > max_x ? x : max_x;
    // Y
    min_y = y < min_y ? y : min_y;
    max_y = y > max_y ? y : max_y;
  }
  double x_offset = ((pdm_configuration_.normalize_image_width -
                     (max_x - min_x)) / 2.0) - min_x;
  x_offset += pdm_configuration_.extra_face_shift_x;
  double y_offset = ((pdm_configuration_.normalize_image_height -
                     (max_y - min_y)) / 2.0) - min_y;
  y_offset += pdm_configuration_.extra_face_shift_y;

  for (int i = 0; i < npts; i++) {
    shape->at<double>(i) += x_offset;
    shape->at<double>(i + npts) += y_offset;
  }
}

/*
 *  @name   EstimateSimilarityTransform_refactored
 *  @brief  Compute similarity transform between two sets of points
 *  @param[in]  src_pts           Source points, [1xN] or [Nx1]
 *  @param[in]  dst_pts           Destination points, [1xN] or [Nx1]
 *  @param[out] transformation    Transformation between src/dst
 *  @return Normalized image
 */
void PointDistributionModel::EstimateSimilarityTransform(const cv::Mat& src_pts,
                                                         const cv::Mat& dst_pts,
                                                         cv::Mat* transformation) {
  assert(((src_pts.cols == 1) && (src_pts.rows > 4)) ||
         ((src_pts.rows == 1) && (src_pts.cols > 4)));
  assert(((dst_pts.cols == 1) && (dst_pts.rows > 4)) ||
         ((dst_pts.rows == 1) && (dst_pts.cols > 4)));
  assert((src_pts.rows == dst_pts.rows) && (src_pts.cols == dst_pts.cols));

  // Copied from OpenCV 2.3.1, Tuned by Christophe Ecabert
  double sa[16], sb[4];
  memset(sa, 0, sizeof(sa));
  memset(sb, 0, sizeof(sb));

  int n_points = std::max(src_pts.cols, src_pts.rows) / 2;
  const double* src_ptr = reinterpret_cast<const double*>(src_pts.data);
  const double* dst_ptr = reinterpret_cast<const double*>(dst_pts.data);
  double a_x, a_y;
  double b_x, b_y;
  for (int i = 0; i < n_points; ++i) {
    a_x = *src_ptr;
    a_y = *(src_ptr + n_points);
    b_x = *dst_ptr;
    b_y = *(dst_ptr + n_points);
    ++src_ptr;
    ++dst_ptr;

    sa[0] += a_x * a_x + a_y * a_y;
    sa[2] += a_x;
    sa[3] += a_y;

    sa[5] += a_x * a_x + a_y * a_y;
    sa[6] += -a_y;
    sa[7] += a_x;

    sa[8] += a_x;
    sa[9] += -a_y;
    sa[10] += 1;

    sa[12] += a_y;
    sa[13] += a_x;
    sa[15] += 1;

    sb[0] += a_x * b_x + a_y * b_y;
    sb[1] += a_x * b_y - a_y * b_x;
    sb[2] += b_x;
    sb[3] += b_y;
  }
  // Solve linear least square
  cv::Mat A = cv::Mat(4, 4, CV_64FC1, sa);
  cv::Mat B = cv::Mat(4, 1, CV_64FC1, sb);
  cv::Mat M = cv::Mat(4, 1, CV_64FC1);
  LinearAlgebra<double>::LinearSolver solver;
  solver.Solve(A, B, &M);

  transformation->create(2, 3, CV_64FC1);
  double* t_ptr = reinterpret_cast<double*>(transformation->data);
  const double* m_ptr = reinterpret_cast<const double*>(M.data);
  t_ptr[0] = m_ptr[0];
  t_ptr[1] = -m_ptr[1];
  t_ptr[2] = m_ptr[2];
  t_ptr[3] = m_ptr[1];
  t_ptr[4] = m_ptr[0];
  t_ptr[5] = m_ptr[3];
}

/*
 *  @name  NormSDMImage
 *  @fn void NormSDMImage(const cv::Mat& image,
                           const cv::Mat& shape,
                           cv::Mat* norm_image,
                           cv::Mat* norm_shape)
 *  @brief Normalize face image (scaling, traslation, rotation)
 *  @param[in]  image             input image
 *  @param[in]  shape             input shape
 *  @param[in]  only_inner_shape  If true normalize only the inner shape
 *  @param[out] norm_shape  normalized shape
 *  @param[out] norm_image  normalized image
 */
void PointDistributionModel::NormSDMImage(const cv::Mat& image,
                                          const cv::Mat& shape,
                                          const bool only_inner_shape,
                                          cv::Mat* norm_image,
                                          cv::Mat* norm_shape) {
  // get reference shape (i.e. in normalized pose)
  assert((shape.rows == 136 && shape.cols == 1) ||
         (shape.rows == 1 && shape.cols == 136));

  // Reference shape already build ?
  if (reference_shape_.empty()) {
    p_local_.setTo(0.0);
    double* pg_ptr = reinterpret_cast<double*>(p_globl_.data);
    pg_ptr[0] = pdm_configuration_.normalize_face_scale;
    pg_ptr[1] = 0.0;
    pg_ptr[2] = 0.0;
    pg_ptr[3] = 0.07;
    pg_ptr[4] = 0.0;
    pg_ptr[5] = 0.0;
    // Generate shape
    CalcShape2D(p_local_, p_globl_, &reference_shape_);
    // Center it in the normalized space
    TransRefShape(&reference_shape_);
  }
  // Take inner shape from both
  cv::Mat inner_ref_shape = cv::Mat(98, 1, CV_64FC1);
  cv::Mat inner_shape = cv::Mat(inner_ref_shape.rows,
                                inner_ref_shape.cols,
                                CV_64FC1);
  int p_ref = 0;
  int p = 0;
  for (int i = 17; i < 68; ++i) {
    if (i < 66) {
      inner_ref_shape.at<double>(p_ref) = reference_shape_.at<double>(i);
      inner_ref_shape.at<double>(p_ref + 49) = reference_shape_.at<double>(i + 66);
      p_ref++;
    }
    if (i == 60 || i == 64) {continue; }
    inner_shape.at<double>(p) = shape.at<double>(i);
    inner_shape.at<double>(p + 49) = shape.at<double>(i + 68);
    p++;
  }
  assert(p == p_ref);

  // Estimate transformation, between actual shape and normalized one
  cv::Mat transform;
  cv::Mat inv_transform;
  this->EstimateSimilarityTransform(inner_ref_shape, inner_shape, &transform);
  cv::invertAffineTransform(transform, inv_transform);
  // Apply transformation to the image -> normalized face
  norm_image->create(pdm_configuration_.normalize_image_height,
                     pdm_configuration_.normalize_image_width,
                     image.type());
  cv::warpAffine(image,
                 *norm_image,
                 transform,
                 norm_image->size(),
                 cv::INTER_LINEAR|cv::WARP_INVERSE_MAP,
                 cv::BORDER_REPLICATE);
  // Apply transformation on shape
  int n_points = only_inner_shape ? 49 : std::max(shape.cols, shape.rows) / 2;
  norm_shape->create(n_points * 2, 1, CV_64F);
  const double* m_ptr = reinterpret_cast<const double*>(inv_transform.data);
  double* dst_ptr = reinterpret_cast<double*>(norm_shape->data);
  const double* src_ptr = (only_inner_shape ?
                           reinterpret_cast<const double*>(inner_shape.data) :
                           reinterpret_cast<const double*>(shape.data));
  double a = *m_ptr++;
  double b = *m_ptr++;
  double tx = *m_ptr++;
  double c = *m_ptr++;
  double d = *m_ptr++;
  double ty = *m_ptr++;
  for (int i = 0; i < n_points; ++i) {
    dst_ptr[i] = a * src_ptr[i] + b * src_ptr[i + n_points] + tx;
    dst_ptr[i + n_points] = c * src_ptr[i] + d * src_ptr[i + n_points] + ty;
  }
}

/**
 *  @name  NormSDMImage
 *  @fn void NormSDMImage(const cv::Mat& image,
                           const cv::Mat& shape,
                           cv::Mat* norm_image,
                           cv::Mat* norm_shape)
 *  @brief Normalize face image (scaling, traslation, rotation)
 *  @param[in]  image             input image
 *  @param[in]  shape             input shape
 *  @param[in]  only_inner_shape  If true normalize only the inner shape
 *  @param[out] norm_shape  normalized shape
 *  @param[out] norm_image  normalized image
 */
void PointDistributionModel::NormSDMImage_intra(const cv::Mat& image,
                                                const cv::Mat& shape,
                                                cv::Mat* norm_image,
                                                cv::Mat* norm_shape) {
  // get reference shape (i.e. in normalized pose)
  assert((shape.rows == 98 && shape.cols == 1) ||
         (shape.rows == 1 && shape.cols == 98));

  // Reference shape already build ?
  if (reference_shape_.empty()) {
    p_local_.setTo(0.0);
    double* pg_ptr = reinterpret_cast<double*>(p_globl_.data);
    pg_ptr[0] = pdm_configuration_.normalize_face_scale;
    pg_ptr[1] = 0.0;
    pg_ptr[2] = 0.0;
    pg_ptr[3] = 0.07;
    pg_ptr[4] = 0.0;
    pg_ptr[5] = 0.0;
    // Generate shape
    CalcShape2D(p_local_, p_globl_, &reference_shape_);
    // Center it in the normalized space
    TransRefShape(&reference_shape_);
  }

  // Take inner shape from both
  cv::Mat inner_ref_shape = cv::Mat(98, 1, CV_64FC1);
  int p_ref = 0;
  for (int i = 17; i < 68; ++i) {
    if (i < 66) {
      inner_ref_shape.at<double>(p_ref) = reference_shape_.at<double>(i);
      inner_ref_shape.at<double>(p_ref + 49) = reference_shape_.at<double>(i + 66);
      p_ref++;
    }
  }

  // Estimate transformation, between actual shape and normalized one
  cv::Mat transform;
  cv::Mat inv_transform;
  this->EstimateSimilarityTransform(inner_ref_shape, shape, &transform);
  cv::invertAffineTransform(transform, inv_transform);
  // Apply transformation to the image -> normalized face
  norm_image->create(pdm_configuration_.normalize_image_height,
                     pdm_configuration_.normalize_image_width,
                     image.type());
  cv::warpAffine(image,
                 *norm_image,
                 transform,
                 norm_image->size(),
                 cv::INTER_LINEAR|cv::WARP_INVERSE_MAP,
                 cv::BORDER_REPLICATE);
  // Apply transformation on shape
  int n_points =  std::max(shape.cols, shape.rows) / 2;
  norm_shape->create(n_points * 2, 1, CV_64F);
  const double* m_ptr = reinterpret_cast<const double*>(inv_transform.data);
  double* dst_ptr = reinterpret_cast<double*>(norm_shape->data);
  const double* src_ptr = reinterpret_cast<const double*>(shape.data);
  double a = *m_ptr++;
  double b = *m_ptr++;
  double tx = *m_ptr++;
  double c = *m_ptr++;
  double d = *m_ptr++;
  double ty = *m_ptr++;
  for (int i = 0; i < n_points; ++i) {
    dst_ptr[i] = a * src_ptr[i] + b * src_ptr[i + n_points] + tx;
    dst_ptr[i + n_points] = c * src_ptr[i] + d * src_ptr[i + n_points] + ty;
  }
}
}  // namespace LTS5