/**
 *  @file   svm_tracker_quality_assessment_trainer.cpp
 *  @brief  Face tracker SVM quality assessment trainer class definition
 *
 *  @author Gabriel Cuendet
 *  @date   02/06/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#include <time.h>
#include <string>
#include <vector>
#include <fstream>

#include "lts5/face_tracker/svm_tracker_quality_assessment_trainer.hpp"
#include "lts5/face_tracker/sdm_trainer.hpp"
#include "lts5/classifier/pca.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/utils/image_transforms.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/classifier/crossvalidation.hpp"

/*
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
#pragma mark -
#pragma mark Constructors and Destructors
/*
 *  @name   SvmTrackerQualityAssessmentTrainer
 *  @brief  Constructor
 */
SvmTrackerQualityAssessmentTrainer::SvmTrackerQualityAssessmentTrainer(void) {
  svm_type_ = LTS5::L2R_L2LOSS_SVC;
  svm_classifier_ = new BinaryLinearSVMClassifier();
}

/*
 *  @name   SvmTrackerQualityAssessmentTrainer
 *  @brief  Constructor
 */
SvmTrackerQualityAssessmentTrainer::
  SvmTrackerQualityAssessmentTrainer(const LTS5::SdmTracker::SdmTrackerParameters& tracker_parameters,
                                     const std::vector<std::string>& images_files,
                                     const std::vector<bool>& all_flip,
                                     const std::vector<cv::Mat>& all_annotations,
                                     const cv::Mat& mean_shape,
                                     const std::vector<cv::Rect>& faces_rectangles,
                                     const cv::Vec3d& detector_stds,
                                     const std::vector<cv::Size>& images_sizes) {
  tracker_parameters_ = tracker_parameters;
  images_files_ = images_files;
  all_flip_ = all_flip;
  all_annotations_ = all_annotations;
  mean_shape_ = mean_shape;
  faces_rectangles_ = faces_rectangles;
  detector_stds_ = detector_stds;
  images_sizes_ = images_sizes;

  svm_type_ = LTS5::L2R_L2LOSS_SVC;
  svm_classifier_ = new BinaryLinearSVMClassifier();
  // @TODO: (Gabriel) Hardcoded magical value to modify at some point...
  score_threshold_ = -0.3;
}

/*
 *  @name   ~SvmTrackerQualityAssessmentTrainer
 *  @brief  Destructor
 */
SvmTrackerQualityAssessmentTrainer::~SvmTrackerQualityAssessmentTrainer(void) {
  delete svm_classifier_;
}

#pragma mark -
#pragma mark Train and Save methods
/*
 *  @name   Save
 *  @brief  Save the trained model in file.
 *  @param  [in]    model_name      Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int SvmTrackerQualityAssessmentTrainer::Save(const std::string& model_name) {
  int error = -1;
  // Check name validity, and add .bin extension if none provided. If there
  // is already one, remove it and append the new one.
  std::string final_name;
  int position_dot = static_cast<int>(model_name.rfind("."));
  if (position_dot == std::string::npos) {
    // No ext
    final_name = model_name + ".bin";
  } else {
    // Existing extension -> remove it and add .bin
    final_name = model_name.substr(0, position_dot) + ".bin";
  }
  // Write binary file
  std::ofstream output_stream(final_name,
                              std::ios_base::out | std::ios_base::binary);
  if (output_stream.is_open()) {
    error = this->Save(output_stream);
  }
  return error;
}

/*
 *  @name   Save
 *  @brief  Write the assessment model to file stream.
 *  @param[in]  output_stream  File stream for writing the model
 *  @return -1 if errors, 0 otherwise
 */
int SvmTrackerQualityAssessmentTrainer::Save(std::ostream& output_stream) {
  int error = -1;
  if (output_stream.good()) {
    // Stream ok -> compute + write object size
    int object_size = 0;

    // Matrix type + dimensions
    object_size += 3 * sizeof(int);
    // Matrix elements
    object_size += (svm_model_w_.total() * svm_model_w_.elemSize());
    // Threshold size
    object_size += sizeof(double);

    int dummy = static_cast<int>(LTS5::HeaderObjectType::kSvmTrackerAssessment);
    output_stream.write(reinterpret_cast<const char*>(&dummy), sizeof(dummy));
    output_stream.write(reinterpret_cast<const char*>(&object_size),
                        sizeof(object_size));
    // Write svm_model_w
    error = LTS5::WriteMatToBin(output_stream, svm_model_w_);
    // Write threshold
    output_stream.write(reinterpret_cast<const char*>(&score_threshold_),
                        sizeof(score_threshold_));
  }
  return error;
}

/*
 *  @name   Train
 *  @brief  SVM  quality checker training method (independant from sdm)
 *  @param[in]  folder  Location of images/annotations
 */
void SvmTrackerQualityAssessmentTrainer::Train(const std::string& folder) {
  // TODO: (Gabriel) Implement!
}

/*
 *  @name   Train
 *  @brief  SVM  quality checker training method
 */
void SvmTrackerQualityAssessmentTrainer::Train() {
  using TransposeType = LTS5::LinearAlgebra<double>::TransposeType;
  const int kFaces = static_cast<int>(all_annotations_.size());
  const int kPoints = mean_shape_.rows / 2;

  std::cout <<
  "***************\nTraining the tracker quality assessment system..." <<
  std::endl;

  // Monte Carlo sampling of the face detector rectangles!
  detector_stds_ *= tracker_parameters_.montecarlo_tracking_std_scaling;
  MultiArray<cv::Rect>
  montecarlo_rects(kFaces,
                   std::vector<cv::Rect>(tracker_parameters_.montecarlo_samples));

  LTS5::SdmTrainer::SampleMonteCarloRectangles(detector_stds_,
                                               tracker_parameters_.montecarlo_samples,
                                               faces_rectangles_, images_sizes_,
                                               tracker_parameters_.montecarlo_sampling_attempt,
                                               tracker_parameters_.montecarlo_init_random,
                                               &montecarlo_rects);

  cv::Mat sift_features = cv::Mat::zeros(kFaces *
                                         (tracker_parameters_.montecarlo_samples + 1),
                                         tracker_parameters_.sift_parameters.sift_dim * kPoints,
                                         CV_64FC1);
  cv::Mat labels = cv::Mat::zeros(kFaces *
                                  (tracker_parameters_.montecarlo_samples + 1),
                                  1, CV_64FC1);

  // Generating headers for pos and neg samples and labels from full matrices
  cv::Mat pos_sift_features = sift_features.rowRange(0, kFaces);
  cv::Mat neg_sift_features = sift_features.rowRange(kFaces,
                                                     sift_features.rows);
  cv::Mat pos_labels = labels.rowRange(0, kFaces);
  cv::Mat neg_labels = labels.rowRange(kFaces, labels.rows);

  cv::Mat pca_sift;

  // Compute features
  std::cout << "   Extracting positive and negative features..." << std::endl;
  ExtractPositiveFeatures(&pos_sift_features, &pos_labels);
  ExtractNegativeFeatures(montecarlo_rects, &neg_sift_features, &neg_labels);

  // Compute PCA
  std::cout << "   Computing PCA on the features..." << std::endl;
  time_t t1, t2;
  time(&t1);
  LTS5::ComputePca(sift_features, tracker_parameters_.retain_variance,
                   tracker_parameters_.train_computation_mod, &pca_basis_);
  time(&t2);
  std::cout << "   Duration: " << t2 - t1 << "s" << std::endl;
  std::cout << "   PCA eigenvectors: " << pca_basis_.rows << "x" <<
  pca_basis_.cols << std::endl;

  std::cout << "   Projecting SIFT features on the PCA basis..." << std::endl;
  const double alpha = 1.0;
  const double beta = 0.0;
  LTS5::LinearAlgebra<double>::Gemm(sift_features,
                                    TransposeType::kNoTranspose,
                                    alpha, pca_basis_,
                                    TransposeType::kTranspose, beta,
                                    &pca_sift);

  for (int i = 0; i < labels.rows; i++) {
    if (labels.at<double>(i, 0) > 0)
      positive_samples_.push_back(pca_sift.row(i));
    else
      negative_samples_.push_back(pca_sift.row(i));
  }

  double best_c;
  std::cout << "   Training binary SVM classifier..." << std::endl;
  time(&t1);
  EstimateParametersLinear(&best_c);
  std::cout << "      Parameters estimated!\n      best C = " << best_c <<
  std::endl;

  svm_classifier_ = new BinaryLinearSVMClassifier(best_c, svm_type_);

  svm_classifier_->Train(positive_samples_, negative_samples_);
  time(&t2);
  std::cout << "   Duration: " << t2 - t1 << "s" << std::endl;

  cv::Mat w = svm_classifier_->GetW();
  double b = svm_classifier_->GetBias();

  // correct??
  cv::Mat svm_w = w * pca_basis_;
  // Append bias b at the end of w
  svm_model_w_.create(1, svm_w.cols + 1, CV_64FC1);
  svm_w.copyTo(svm_model_w_.colRange(0, svm_w.cols));
  svm_model_w_.at<double>(0, svm_w.cols) = b;

  std::cout << "Done\n***************" <<
  std::endl;
}

/*
 *  @name   ExtractPositiveFeatures
 *  @brief  Extract SIFT features for positive examples (the annotations)
 *  @param[out]  features  Matrix holding the computed features
 *  @param[out]  Vector of positive labels (1.0)
 */
void SvmTrackerQualityAssessmentTrainer::ExtractPositiveFeatures(cv::Mat* features, cv::Mat* labels) {
  assert(images_files_.size() == all_annotations_.size());
  const int kImages = static_cast<int>(images_files_.size());
  std::cout << "   Extracting positive features for " << kImages << " images..."
  << std::endl;

  cv::Mat ones_ = cv::Mat::ones(kImages, 1, CV_64FC1);
  ones_.copyTo(*labels);

  for (int i = 0; i < kImages; ++i) {
    cv::Mat image = cv::imread(images_files_[i]);
    cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
    if (all_flip_[i]) {
      cv::flip(image, image, 1);
    }

    cv::Mat norm_shape = mean_shape_.clone();
    norm_shape *= tracker_parameters_.face_region_width;
    norm_shape += tracker_parameters_.face_region_width / 2;

    cv::Mat transform;
    cv::Mat annotation = all_annotations_[i].clone();
    cv::Mat dummy;
    LTS5::NormalizeScaleAndRotation(norm_shape,
                                    tracker_parameters_.face_region_width,
                                    image, &image, &annotation, &dummy,
                                    &transform);

    cv::Mat sift;
    std::vector<cv::KeyPoint> keypoints = LTS5::KeypointsFromShape(annotation,
                                                                   tracker_parameters_.sift_size);
    SSift::ComputeDescriptorOpt(image, keypoints,
                                tracker_parameters_.sift_parameters,
                                &sift);
    sift = sift.reshape(sift.channels(), 1 /*row*/);
    sift.convertTo(features->row(i), CV_64FC1);
  }
}

/*
 *  @name   ExtractNegativeFeatures
 *  @brief  Extract SIFT features for negative examples (the perturbed mean
 *          shapes)
 *  @param[in] montecarlo_rects  Perturbed detector rectangles
 *  @param[out]  features  Matrix holding the computed features
 *  @param[out]  Vector of negative labels (1.0)
 */
  void SvmTrackerQualityAssessmentTrainer::
  ExtractNegativeFeatures(const MultiArray<cv::Rect>& montecarlo_rects,
                          cv::Mat* features,
                          cv::Mat* labels) {
  assert(images_files_.size() == all_annotations_.size());
  const int kImages = static_cast<int>(images_files_.size());

  cv::Mat zeros_ = cv::Mat::zeros(kImages * tracker_parameters_.montecarlo_samples,
                                  1,
                                  CV_64FC1);
  zeros_.copyTo(*labels);

  int edge = tracker_parameters_.face_region_width;

  for (int i = 0; i < kImages; i++) {
    cv::Mat image = cv::imread(images_files_[i]);
    cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
    if (all_flip_[i]) {
      cv::flip(image, image, 1);
    }

    cv::Mat norm_shape = mean_shape_.clone();
    norm_shape *= edge;
    norm_shape += edge/2;

    cv::Mat transform;
    cv::Mat annotation = all_annotations_[i].clone();
    cv::Mat dummy;
    LTS5::NormalizeScaleAndRotation(norm_shape, edge, image, &image,
                                    &annotation, &dummy, &transform);

    cv::Mat sift;

    for (int j = 0; j < tracker_parameters_.montecarlo_samples; j++) {
      cv::Mat shape = LTS5::RectRelToImageAbs(montecarlo_rects[i][j],
                                              mean_shape_);
      shape = TransformShape(transform, shape);  //< 136 rows x 1 col

      std::vector<cv::KeyPoint> keypoints = KeypointsFromShape(shape,
                                                               tracker_parameters_.sift_size);
      SSift::ComputeDescriptorOpt(image,
                                  keypoints,
                                  tracker_parameters_.sift_parameters,
                                  &sift);
      sift = sift.reshape(sift.channels(), 1 /*row*/);
      sift.convertTo(features->row(i), CV_64FC1);
    }
  }
}

/*
 *  @name   EstimteParametersLinear
 */
float SvmTrackerQualityAssessmentTrainer::EstimateParametersLinear(double* best_c) {
  std::vector<double> C;

  for (double c = 0.0001; c <= 0.001; c += 0.0001)
    C.push_back(c);
  for (double c = 0.001; c <= 0.01; c += 0.001)
    C.push_back(c);
  for (double c = 0.01; c <= 0.1; c += 0.01)
    C.push_back(c);
  for (double c = 0.1; c <= 1.0; c += 0.1)
    C.push_back(c);
  for (double c = 1; c <= 100; c += 5)
    C.push_back(c);

  double best_accuracy = 0;
  double best_stderr = 0;

  float pos_weight =  (negative_samples_.size() + 0.0) / (positive_samples_.size() + 0.0);
  static Mutex mutex;
  Parallel::For(C.size(),
                [&](const size_t& i) {
    BinaryLinearSVMClassifier svm(C[i], svm_type_);
    BinaryCrossValidation<cv::Mat> cv(svm,
                                      positive_samples_,
                                      negative_samples_,
                                      BaseScoringMetric::ScoringType::kAccuracy,
                                      5,
                                      true);
    cv.RunCrossValidation();
    {
      ScopedLock lock(mutex);
      if (cv.GetMeanScore() > best_accuracy) {
        best_accuracy = cv.GetMeanScore();
        best_stderr = cv.GetScoreStdDev() /
        sqrt(static_cast<double>(cv.GetNumberOfFolds()));
        *best_c = C[i];
      } else if (cv.GetMeanScore() == best_accuracy) {
        double stderror = cv.GetScoreStdDev() /
        sqrt(static_cast<double>(cv.GetNumberOfFolds()));
        if (stderror < best_stderr) {
          best_accuracy = cv.GetMeanScore();
          best_stderr = stderror;
          *best_c = C[i];
        }
      }
    }
  });
  return pos_weight;
}
}  // namespace LTS5
