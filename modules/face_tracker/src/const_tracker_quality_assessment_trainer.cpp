/**
 *  @file   const_tracker_quality_assessment_trainer.cpp
 *  @brief  Face tracker CONST quality assessment trainer class definition
 *
 *  @author Gabriel Cuendet
 *  @date   02/06/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#include <fstream>
#include <string>

#include "lts5/face_tracker/const_tracker_quality_assessment_trainer.hpp"
#include "lts5/utils/file_io.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/*
 *  @name   Save
 *  @brief  Save the trained model in file.
 *  @param  [in]    model_name      Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int ConstTrackerQualityAssessmentTrainer::Save(const std::string& model_name) {
  int error = -1;
  // Check name validity, and add .bin extension if none provided. If there
  // is already one, remove it and append the new one.
  std::string final_name;
  int position_dot = static_cast<int>(model_name.rfind("."));
  if (position_dot == std::string::npos) {
    // No ext
    final_name = model_name + ".bin";
  } else {
    // Existing extension -> remove it and add .bin
    final_name = model_name.substr(0, position_dot) + ".bin";
  }
  // Write binary file
  std::ofstream output_stream(final_name,
                              std::ios_base::out | std::ios_base::binary);
  if (output_stream.is_open()) {
    error = this->Save(output_stream);
  }
  return error;
}

/*
 *  @name   Save
 *  @brief  Write the assessment model to file stream.
 *  @param[in]  output_stream  File stream for writing the model
 *  @return -1 if errors, 0 otherwise
 */
int ConstTrackerQualityAssessmentTrainer::Save(std::ostream& output_stream) {
  int error = -1;
  if (output_stream.good()) {
    // Stream ok -> compute + write object size
    int object_size = 0;
    // bool size
      object_size += sizeof(int);
      int dummy = static_cast<int>(LTS5::HeaderObjectType::kConstTrackerAssessment);
      output_stream.write(reinterpret_cast<const char*>(&dummy), sizeof(dummy));
      output_stream.write(reinterpret_cast<const char*>(&object_size),
                          sizeof(object_size));
      // Write bool
      int state = detect_ ? 1 : 0;
      output_stream.write(reinterpret_cast<const char*>(&state),
                          sizeof(state));
      error = output_stream.good() ? 0 : -1;
  }
  return error;
}
}  // namespace LTS5
