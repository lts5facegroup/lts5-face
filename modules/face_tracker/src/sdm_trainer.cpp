/**
 *  @file   sdm_trainer.cpp
 *  @brief  SDM trainer class implementation
 *
 *  @author Gabriel Cuendet
 *  @date   02/06/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#include <algorithm>
#include <vector>
#include <string>

#include "opencv2/imgproc.hpp"

#include "lts5/face_tracker/sdm_trainer.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/utils/image_transforms.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/classifier/pca.hpp"
#include "lts5/face_tracker/const_tracker_quality_assessment_trainer.hpp"
#include "lts5/face_tracker/svm_tracker_quality_assessment_trainer.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/face_tracker/svm_tracker_assessment.hpp"
#include "lts5/face_detector/cascade_face_detector.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Static methods
/*
 *  @name   LoadAnnotations
 *  @brief  Loads the annotations from the list of image files
 *          (image_files_).
 *  @return -1 if error, 0 otherwise
 */
int SdmTrainer::LoadAnnotations(const std::vector<std::string>& images_files,
                                const std::vector<bool>& all_flip,
                                std::vector<cv::Mat>* all_annotations) {
  all_annotations->clear();

  if (images_files.size() != all_flip.size() && !all_flip.empty()) {
    std::cout << "Images files list and flips are NOT the same size" <<
    std::endl;
    return -1;
  }

  for (int i(0); i < images_files.size(); i++) {
    cv::Mat annotation;
    std::string filename = images_files[i];
    size_t pos = filename.rfind('.');
    std::string pts_filename = filename.substr(0, pos) + ".pts";
    LTS5::LoadPts(pts_filename, annotation);

    if (all_flip.size() != 0 && all_flip[i]) {
      cv::Mat image = cv::imread(filename);
      cv::Size image_size = image.size();
      cv::Mat temp_annotations;
      temp_annotations = LTS5::FlipShape(annotation, image_size);
      annotation = temp_annotations.clone();
    }

    all_annotations->push_back(annotation);
  }
  return 0;
}

/*
 *  @name   LoadAnnotations
 *  @brief  Loads the annotations from the list of image files
 *          (image_files_). Loads ONLY the indices specified in indices!
 *  @return -1 if error, 0 otherwise
 */
int SdmTrainer::LoadAnnotations(const std::unordered_set<int>& indices,
                                const std::vector<std::string>& images_files,
                                const std::vector<bool>& all_flip,
                                std::vector<cv::Mat>* all_annotations) {
  all_annotations->clear();

  if (images_files.size() != all_flip.size() && all_flip.size() > 0) {
    std::cout << "Images files list and flips are NOT the same size" <<
    std::endl;
    return -1;
  }

  for (int i(0); i < images_files.size(); i++) {
    cv::Mat annotation;

    std::string filename = images_files[i];
    size_t pos = filename.rfind('.');
    std::string pts_filename = filename.substr(0, pos) + ".pts";
    LTS5::LoadPts(pts_filename, indices, annotation);

    if (all_flip.size() != 0 && all_flip[i]) {
      cv::Mat image = cv::imread(filename);
      cv::Size image_size = image.size();
      cv::Mat temp_annotations;
      temp_annotations = LTS5::FlipShape(annotation, image_size);
      annotation = temp_annotations.clone();
    }

    all_annotations->push_back(annotation);
  }
  return 0;
}

/*
 *  @name       PruneDetections
 *  @brief      Amongst (potentially) several detections in an image,
 *              returns the one which center is closest to the annotation
 *              center
 *  @param[in]  annotation      Annotation
 *  @param[in]  detections      Vector of Rect returned by the face detector
 *  @return     Detection which center is closest to the annotation center
 *              or an empty Rect
 */
cv::Rect SdmTrainer::PruneDetections(const cv::Mat& annotation,
                                      const std::vector<cv::Rect>& detections) {
  cv::Rect return_rect;
  std::vector<cv::Rect> candidates_rect;  // Rects that have shape center
  // inside
  for (int j = 0; j < detections.size(); j++) {
    cv::Point2d center = LTS5::GetCenterPoint(annotation);
    if (center.x > detections[j].x &&
        center.x < detections[j].x + detections[j].width &&
        center.y > detections[j].y &&
        center.y < detections[j].y + detections[j].height) {
      candidates_rect.push_back(detections[j]);
    }
  }

  if (candidates_rect.size() == 1) {
    return_rect = candidates_rect[0];
  } else if (candidates_rect.size() > 1) {
    int minIdx = 0;
    double minDist = DBL_MAX;
    for (int j = 0; j < candidates_rect.size(); j++) {
      cv::Point2d rectangle_center(candidates_rect[j].x +
                                   candidates_rect[j].width / 2.0,
                                   candidates_rect[j].y +
                                   candidates_rect[j].height / 2.0);
      cv::Point2d pt1 = LTS5::GetCenterPoint(annotation);

      double dist = sqrt((rectangle_center.x - pt1.x) *
                         (rectangle_center.x - pt1.x) +
                         (rectangle_center.y - pt1.y) *
                         (rectangle_center.y - pt1.y));

      if (dist < minDist) {
        minIdx = j;
        minDist = dist;
      }
    }
    return_rect = candidates_rect[minIdx];
  }
  return return_rect;
}

/*
 *  @name   DetectInImages
 *  @brief  Detect the objects in all images, from a list of files. Remove
 *          the filename from the list if no object is detected in the image.
 *  @return -1 if error, 0 otherwise
 */
int SdmTrainer::DetectInImages(LTS5::BaseFaceDetector* face_detector,
                               std::vector<std::string>* images_files,
                               std::vector<bool>* all_flip,
                               std::vector<cv::Mat>* all_annotations,
                               std::vector<cv::Size>* images_sizes,
                               std::vector<cv::Rect>* faces_rectangles) {
  if (face_detector->empty()) {
    std::cerr << "The face detector has not been loaded!" << std::endl;
    return -1;
  }

  if (all_annotations->size() != images_files->size() &&
      all_annotations->size() != 0) {
    std::cout <<
    "Annotations vector is not empty and not the same size as images files!"
    << std::endl;
    return -1;
  }

  images_sizes->clear();
  faces_rectangles->clear();

  const int k_total_images = static_cast<int>(images_files->size());
  std::cout << "Detecting faces in " << k_total_images << " images..." <<
  std::endl;

  time_t t1, t2;
  time(&t1);
  for (int i(0); i < images_files->size(); ) {
    cv::Mat annotation;
    if (all_annotations->size() != 0) {
      annotation = (*all_annotations)[i];
    }
    cv::Mat image;

    std::string filename = (*images_files)[i];
    // Loading grayscale images or converting them is NOT the same...
    image = cv::imread(filename);
    //cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
    if ((*all_flip)[i])
      cv::flip(image, image, 1);
    std::vector<cv::Rect> face_detections;
    face_detector->Detect(image, &face_detections);
    cv::Rect face_rect;
    if (all_annotations->size() != 0) {
      face_rect = LTS5::SdmTrainer::PruneDetections(annotation,
                                                    face_detections);
    } else {
      if (face_detections.size() > 0) {
        face_rect = face_detections[0];  //< Arbitrarily taking first rect
      }
    }

    // The face is detected, let's store the detection rectangle
    if (face_rect.width > 0 && face_rect.height > 0) {
      faces_rectangles->push_back(face_rect);
      images_sizes->push_back(image.size());

      i++;
    } else {
      // The face is not detected, let's remove that image from the vector of
      // images and the annotations from all_annotations_.
      images_files->erase(images_files->begin() + i);
      if (all_annotations->size() != 0)
        all_annotations->erase(all_annotations->begin() + i);
      all_flip->erase(all_flip->begin() + i);
    }
  }

  time(&t2);
  std::cout << "   Duration: " << t2 - t1 << "s" << std::endl;
  const int k_valid_images = static_cast<int>(images_files->size());
  std::cout << "   " << 100.0 * k_valid_images / k_total_images << "% (" <<
  k_valid_images << "/" << k_total_images << ") faces detected" << std::endl;

  return 0;
}

/*
 *  @name   ComputeDetectionsStatistics
 *  @brief  Compute the means and standard deviations of the difference
 *          between the annotations and the face detector along scale, x and y
 *          dimensions
 *  @param[in]      detections   Detected rectangles
 *  @param[in]      annotations  Annotated shapes
 *  @param[in]      mean_shape   Mean shape of the detections
 *  @param[out]     means        Mean along each of the three dimensions:
 *                               scale, x-dimension and y-dimension
 *  @param[out]     stds         Standard deviations along each of the three
 *                               dimensions: scale, x-dimension and
 *                               y-dimension.
 *  @return
 */
int SdmTrainer::ComputeDetectionsStatistics(const std::vector<cv::Rect>&
                                            detections,
                                            const std::vector<cv::Mat>&
                                            annotations,
                                            const cv::Mat& mean_shape,
                                            cv::Mat* means, cv::Mat* stds) {
  assert(annotations.size() == detections.size());
  const int kImages = static_cast<int>(annotations.size());

  // Scale and translation for MC
  std::cout <<
  "Computing normalized statistics on the face detector consistency..." <<
  std::endl;

  cv::Mat shape_differences(kImages, 1, CV_64FC3);
  means->zeros(1, 1, CV_64FC3);
  stds->zeros(1, 1, CV_64FC3);

  for (int i = 0; i < kImages; i++) {
    cv::Mat annotation = LTS5::ImageAbsToRectRel(detections[i],
                                                 annotations[i]);

    // calcSim returns [a, b, tx, ty]
    cv::Vec<double, 4> simT = LTS5::ComputeRigidTransform(mean_shape,
                                                          annotation);
    double scale = sqrt(simT[0] * simT[0] + simT[1] * simT[1]);
    double tx = simT[2];
    double ty = simT[3];

    shape_differences.at<cv::Vec3d>(i, 0)[0] = scale;
    shape_differences.at<cv::Vec3d>(i, 0)[1] = tx;
    shape_differences.at<cv::Vec3d>(i, 0)[2] = ty;
  }

  cv::meanStdDev(shape_differences, *means, *stds);
  std::cout << "   scale : " << means->at<cv::Vec3d>(0, 0)[0] << " +/- " <<
  stds->at<cv::Vec3d>(0, 0)[0] << std::endl;
  std::cout << "   x-dim : " << means->at<cv::Vec3d>(0, 0)[1] << " +/- " <<
  stds->at<cv::Vec3d>(0, 0)[1] << std::endl;
  std::cout << "   y-dim : " << means->at<cv::Vec3d>(0, 0)[2] << " +/- " <<
  stds->at<cv::Vec3d>(0, 0)[2] << std::endl;
  shape_differences.release();

  return 0;
}

/*
 *  @name   SampleMonteCarloRectangles
 *  @brief
 *  @param[in]  stds             Vec3d containing the standard deviation
 *                                   of the gaussian distributions from
 *                                   which to sample:
 *                                       1) the scale variation,
 *                                       2) the x-offset and
 *                                       3) the y-offset
 *  @param[in]  n_samples        Number of samples to generate for each rect
 *  @param[in]  rectangles       Input rectangles
 *  @param[in]  images_sizes     Sizes of all images
 *  @param[in]  max_iter         Maximum number of invalid samples
 *                                   (rect outside image) before replacing
 *                                   by the mean of the distribution
 *  @param[in]  init_random      Integer to initialize the RNG (true random if
 *                               it is -1)
 *  @param[out] montecarlo_rects
 */
void SdmTrainer::SampleMonteCarloRectangles(const cv::Vec3d& stds,
                                            const int& n_samples,
                                            const std::vector<cv::Rect>&
                                            rectangles,
                                            const std::vector<cv::Size>&
                                            images_sizes,
                                            const int& max_iter,
                                            const int& init_random,
                                            MultiArray<cv::Rect>* montecarlo_rects) {
  assert(images_sizes.size() == rectangles.size());
  const int kImages = static_cast<int>(rectangles.size());

  // Generate MC initializations
  std::cout << "Generating " << n_samples <<
  " Monte Carlo rectangles for each of the " << kImages << " images..." <<
  std::endl;
  std::cout << "   scale: Gaussian, sigma = " << stds[0] << std::endl;
  std::cout << "      tx: Gaussian, sigma = " << stds[1] << std::endl;
  std::cout << "      ty: Gaussian, sigma = " << stds[2] << std::endl;

  int initialization = (init_random == -1 ?
                        static_cast<int>(cv::getTickCount()) :
                        init_random);
  cv::RNG rng(initialization);  // (getTickCount());
  for (int i = 0; i < kImages; i++) {
    for (int j = 0; j < n_samples; j++) {
      int n_failures = 0;

      double scale = 1 + rng.gaussian(stds[0]);
      double tx = rng.gaussian(stds[1]) * rectangles[i].width;
      double ty = rng.gaussian(stds[2]) * rectangles[i].height;

      double x = rectangles[i].x + (1-scale) * rectangles[i].width/2.0 + tx;
      double y = rectangles[i].y + (1-scale) * rectangles[i].height/2.0 + ty;
      double w = scale * rectangles[i].width;
      double h = scale * rectangles[i].height;
      if (x < 0 || x + w >= images_sizes[i].width ||
          y < 0 || y + h >= images_sizes[i].height) {
        // y < 0 || y + 1.2*h >= images_sizes[i].height  in original code
        n_failures++;
        if (n_failures > max_iter) {
          std::cout <<
          "   Failed to randomly generate a rectangle inside the image " <<
          "boundaries after " << max_iter << " trials." <<
          "\n   => Manual rectangle is used" <<
          std::endl;
          // Check scale (to avoid the rect being bigger than the image)
          // If too big, set it to 1! (not perturbed)
          if ((images_sizes[i].width - w) < 0 ||
             (images_sizes[i].height - h) < 0) {
            scale = 1.0;
            w = scale * rectangles[i].width;
            h = scale * rectangles[i].height;
          }
          // Then shift back the rect inside the image
          if (x < 0)
            x = 0;
          else if (x + w >= images_sizes[i].width)
            x = images_sizes[i].width - 1 - w;
          if (y < 0)
            y = 0;
          else if (y + h >= images_sizes[i].height)
            y = images_sizes[i].height -1 - h;

          (*montecarlo_rects)[i][j] = cv::Rect(x, y, w, h);
          // we now have: x > 0 and x < image_width - w
          //              y > 0 and y < image_height - h
          break;
        }
        j--;
      } else {
        (*montecarlo_rects)[i][j] = cv::Rect(x, y, w, h);
        n_failures = 0;
      }
    }
  }
}

# pragma mark -
# pragma mark Public methods

/*
 *  @name   SdmTrainer
 *  @brief  Constructor
 */
SdmTrainer::SdmTrainer() : tracking_assessment_trainer_(nullptr) {
}

/*
 *  @name   SdmTrainer
 *  @brief  Constructor
 */
SdmTrainer::SdmTrainer(const std::string& face_detector_config) {
  int error = 0;
  tracking_assessment_trainer_ = nullptr;

  /* Init Viola-Jones detector
  face_detector_ = new cv::CascadeClassifier(face_detector_config);
  error = !face_detector_->empty() ? error : -1;
   */
  face_detector_ = new LTS5::CascadeFaceDetector(face_detector_config);
  error = !face_detector_->empty() ? error : -1;
}

/*
 *  @name   ~SdmTrainer
 *  @brief  Destructor
 */
SdmTrainer::~SdmTrainer() {
  // Clean up
  if (!face_detector_) {
    delete face_detector_;
  }
  if (!tracking_assessment_trainer_ && release_assessor_) {
    delete tracking_assessment_trainer_;
  }
}

#pragma mark -
#pragma mark Getters and Setters

LTS5::BaseFaceDetector* SdmTrainer::face_detector() {
  return this->face_detector_;
}

void SdmTrainer::set_face_detector(LTS5::BaseFaceDetector* face_detector) {
  if (this->face_detector_ != NULL) {
    delete face_detector_;
    face_detector_ = NULL;
  }

  this->face_detector_ = face_detector;
}

#pragma mark -
#pragma mark Train and Save methods

/*
 *  @name   Save
 *  @brief  Save the trained model in file. This must be override by subclass
 *  @param  [in]    model_name      Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int SdmTrainer::Save(const std::string& model_name) {
  int error = -1;
  // Check name validity, and add .bin extension if none provided. If there
  // is already one, remove it and append the new one.
  std::string final_name;
  int position_dot = static_cast<int>(model_name.rfind("."));
  if (position_dot == std::string::npos) {
    // No ext
    final_name = model_name + ".bin";
  } else {
    // Existing extension -> remove it and add .bin
    final_name = model_name.substr(0, position_dot) + ".bin";
  }
  // Write binary file
  std::ofstream output_stream(final_name,
                              std::ios_base::out | std::ios_base::binary);
  if (output_stream.is_open()) {
    error = this->Save(output_stream);
    output_stream.close();
  } else {
    std::cout << "Cannot open output stream " << final_name << std::endl;
  }
  return error;
}

/*
 *  @name   Save
 *  @brief  Save the trained model in file.
 *  @param  output_stream Stream to a given binary file
 *  @return -1 if error, 0 otherwise
 */
int SdmTrainer::Save(std::ostream& output_stream) {
  int error = -1;
  if (output_stream.good()) {
    // Stream ok -> compute + write object size
    int object_size = tracker_parameters_.ComputeHeaderSize();
    for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
      // Matrix type + dimensions
      object_size += 3 * sizeof(int);
      // Matrix elements
      object_size += (sdm_model_[i].total() * sdm_model_[i].elemSize());
    }
    int dummy = static_cast<int>(LTS5::HeaderObjectType::kSdm);
    output_stream.write(reinterpret_cast<const char*>(&dummy),
                        sizeof(dummy));
    output_stream.write(reinterpret_cast<const char*>(&object_size),
                        sizeof(object_size));
    // Save header
    error = tracker_parameters_.WriteParameterHeader(output_stream);
    // Write meanshape
    error |= LTS5::WriteMatToBin(output_stream, mean_shape_);
    // Write regressor
    for (int i = 0 ; i < tracker_parameters_.number_of_stage ; ++i) {
      error |= LTS5::WriteMatToBin(output_stream, sdm_model_[i]);
    }
    // Write tracker assessment here
    error |= tracking_assessment_trainer_->Save(output_stream);
  }
  return error;
}

/*
 *  @name   Train
 *  @brief  SDM training method
 *  @param[in]  folder        Location of images/annotations
 *  @param[in]  flip_images   Wheter or not to flip the images
 *  @param[in]  indices       Shape indices to take into account from
 *                            the annotations.
 *  @param[in]  assessment_type   Type of the assessment to train
 */
void SdmTrainer::Train(const std::string& folder, const bool& flip_images,
                       const std::unordered_set<int>& indices,
                       const LTS5::HeaderObjectType& assessment_type) {
  tracker_parameters_.flip_images = flip_images;
  this->shape_indices_ = indices;
  SdmTrainer::Train(folder, assessment_type);
}

/*
 *  @name   Train
 *  @brief  SDM training method
 *  @param[in]  folder  Location of images/annotations
 */
void SdmTrainer::Train(const std::string& folder) {
  auto assessment_type = LTS5::HeaderObjectType::kConstTrackerAssessment;
  SdmTrainer::Train(folder, assessment_type);
}

void SdmTrainer::Train(const std::string& folder,
                       const LTS5::HeaderObjectType& assessment_type) {
  //  Parse the folder for images
  images_files_ = LTS5::ScanImagesInFolder(folder);
  std::sort(images_files_.begin(), images_files_.end());
  const int k_total_images = static_cast<int>(images_files_.size());
  std::cout << k_total_images << " images found in the folder" << std::endl;
  all_flip_ = std::vector<bool>(k_total_images, false);

  if (tracker_parameters_.flip_images) {
    std::vector<std::string> flipped_images = images_files_;
    std::vector<bool> flips = all_flip_;
    for (int i=0; i < flipped_images.size(); i++) {
      images_files_.push_back(flipped_images[i]);
      all_flip_.push_back(!flips[i]);
    }
    std::cout << "   Images are flipped, resulting in " << 2*k_total_images <<
    " images" << std::endl;
  }

  // Load corresponding annotations (either all or only a subset of landmarks)
  if (shape_indices_.empty()) {
    LoadAnnotations(images_files_, all_flip_, &all_annotations_);
  } else {
    LoadAnnotations(shape_indices_, images_files_, all_flip_,
                                &all_annotations_);
  }
  const int k_total_annotations = static_cast<int>(all_annotations_.size());
  std::cout << k_total_annotations << " annotations found in the folder" <<
  std::endl;

  // First, load one image at a time, detect the face (if not already
  // provided) and:
  // * if the face is detected, store the detection rectangle
  // * if not, remove that image from the "dataset"
  DetectInImages(face_detector_, &images_files_, &all_flip_,
                 &all_annotations_, &images_sizes_, &faces_rectangles_);
  n_images_ = static_cast<int>(images_files_.size());
  n_shape_points_ = all_annotations_[0].rows/2;

  // Then, compute the mean shape
  LTS5::ComputeRelativeMeanShape(all_annotations_, faces_rectangles_,
                                 &mean_shape_);

  //  Compute the face detector statistics
  cv::Mat means, stds;
  ComputeDetectionsStatistics(faces_rectangles_, all_annotations_,
                              mean_shape_, &means, &stds);
  // Save statistics
  tracker_parameters_.training_statistics[0] = means.at<cv::Vec3d>(0, 0)[0];
  tracker_parameters_.training_statistics[1] = stds.at<cv::Vec3d>(0, 0)[0];
  tracker_parameters_.training_statistics[2] = means.at<cv::Vec3d>(0, 0)[1];
  tracker_parameters_.training_statistics[3] = stds.at<cv::Vec3d>(0, 0)[1];
  tracker_parameters_.training_statistics[4] = means.at<cv::Vec3d>(0, 0)[2];
  tracker_parameters_.training_statistics[5] = stds.at<cv::Vec3d>(0, 0)[2];

  cv::Mat stds_assessment = stds.clone();

  // Monte Carlo sampling of the face detector rectangles!
  stds.at<cv::Vec3d>(0, 0)[0] *= tracker_parameters_.montecarlo_scale_std_scaling;
  stds.at<cv::Vec3d>(0, 0)[1] *= tracker_parameters_.montecarlo_offsetX_std_scaling;
  stds.at<cv::Vec3d>(0, 0)[2] *= tracker_parameters_.montecarlo_offsetY_std_scaling;

  size_t n_sample = (size_t)tracker_parameters_.montecarlo_samples;
  MultiArray<cv::Rect> montecarlo_rects((size_t)n_images_,
                                        std::vector<cv::Rect>(n_sample));
  SampleMonteCarloRectangles(stds,
                             tracker_parameters_.montecarlo_samples,
                             faces_rectangles_,
                             images_sizes_,
                             tracker_parameters_.montecarlo_sampling_attempt,
                             tracker_parameters_.montecarlo_init_random,
                             &montecarlo_rects);

  //  Compute shapes (at STAGE 0, it's just the mean shape placed in the rect)
  //  But at subsequent STAGES, those are updated
  ComputeInitialShapes(montecarlo_rects);

  // START LOOP
  sdm_model_.clear();
  sdm_model_.reserve(tracker_parameters_.number_of_stage);
  for (int loop = 0; loop < tracker_parameters_.number_of_stage; loop++) {
    std::cout << "\nSTART LOOP " << loop << "\n------------" << std::endl;

    std::vector<cv::Mat> all_transforms;
    cv::Mat all_delta_x;
    cv::Mat sift_features = cv::Mat::zeros(n_images_ * tracker_parameters_.
                                           montecarlo_samples,
                                           tracker_parameters_.
                                           sift_parameters.sift_dim *
                                           n_shape_points_, CV_64FC1);
    // Compute SIFT features for all shapes and save in bin-file!
    ComputeSIFTAndDeltaX(loop, &all_delta_x, &all_transforms, &sift_features);

    cv::Mat pca_sift;
    ComputeSIFTPca(sift_features, &pca_sift);

    // Do the regression with all_delta_x and pca_sift -> get R
    double gamma = 50.0 + 50.0 * loop;
    // Combine b to R: The bias terms b
    cv::Mat Phi = cv::Mat::ones(all_delta_x.rows, pca_sift.cols + 1,
                                pca_sift.type());
    pca_sift.copyTo(Phi.colRange(0, pca_sift.cols));

    // Tikhonov regularization
    cv::Mat A = Phi.t() * Phi + gamma * cv::Mat::eye(Phi.cols, Phi.cols,
                                                     Phi.type());
    cv::Mat B = Phi.t() * all_delta_x;
    cv::Mat Rpca;

    std::cout << "Solving Linear Least Squares with Lapack..." << std::endl;
    LTS5::LinearAlgebra<double>::LinearSolver linear_least_squares;
    linear_least_squares.Solve(A, B, &Rpca);

    // Backproject R to SIFT space from PCA space, using pca_basis_
    cv::Mat solution(2*n_shape_points_, n_shape_points_ *
                     tracker_parameters_.sift_parameters.sift_dim + 1,
                     CV_64FC1);
    cv::Mat R = (Rpca.rowRange(0, Rpca.rows-1)).t() * pca_basis_;
    R.copyTo(solution.colRange(0, n_shape_points_ * tracker_parameters_.
                               sift_parameters.sift_dim));
    cv::Mat bias = Rpca.row(Rpca.rows-1).t();
    bias.copyTo(solution.col(solution.cols-1));
    // Save the model (stage), update the shape and loop
    // You can update the shape either in the PCA space or in the SIFT
    // space... Rpca and pca_sift are available
    sdm_model_.push_back(solution.t());

    std::cout << "Update the shapes with the computed regression R..." <<
    std::endl;
    for (int i = 0; i < n_images_; i++) {
      for (int j = 0; j < tracker_parameters_.montecarlo_samples; j++) {
        cv::Mat update = Phi.row(i * tracker_parameters_.
                                 montecarlo_samples + j) * Rpca;
        cv::Mat transform = all_transforms[i * tracker_parameters_.
                                           montecarlo_samples + j].clone();
        transform.at<double>(0, 2) = 0.0;
        transform.at<double>(1, 2) = 0.0;
        LTS5::TransformShape(transform, &update);
        all_shapes_[i*tracker_parameters_.montecarlo_samples+j] += update.t();
      }
    }
  }

  // HERE ASSESSMENT TRAINER
  switch (assessment_type) {
    case HeaderObjectType::kConstTrackerAssessment: {
      tracking_assessment_trainer_ = new ConstTrackerQualityAssessmentTrainer();
      release_assessor_ = true;
    }
      break;

    case HeaderObjectType::kSvmTrackerAssessment: {
      cv::Vec3d detector_stds = stds_assessment.at<cv::Vec3d>(0, 0);
      tracking_assessment_trainer_ =
      new SvmTrackerQualityAssessmentTrainer(tracker_parameters_,
                                             images_files_, all_flip_,
                                             all_annotations_, mean_shape_,
                                             faces_rectangles_,
                                             detector_stds, images_sizes_);
    }
      break;

    default: {
      std::cerr << "Wrong parameter for QualityAssessment type! Using Const."
      << std::endl;
      tracking_assessment_trainer_ = new ConstTrackerQualityAssessmentTrainer();
      release_assessor_ = true;
    }
      break;
  }

  tracking_assessment_trainer_->Train();
}

#pragma mark -
#pragma mark Utility methods

/*
 *  @name   ConvertModelCharToBin
 *  @brief  Convert old char model into new binary one
 *  @param[in]  sdm_model_name            Char sdm model
 *  @param[in]  has_pose_estmimation_model
 *  @param[in]  quality_thresh            SVM quality threshold value
 *  @param[in]  tracking_assessment_model Tracker quality assessment char model
 *  @param[in]  tracker_config            Tracker configuration used to train
 *  @return   -1 if error, 0 otherwise
 */
int SdmTrainer::ConvertModelCharToBin(const std::string& sdm_model_name,
                                      const bool has_pose_estmimation_model,
                                      const std::string& tracking_assessment_model,
                                      const double& score_threshold,
                                      const LTS5::SdmTracker::SdmTrackerParameters&
                                      tracker_config) {
  int error = -1;
  // SDM
  auto* face_tracker = new LTS5::SdmTracker();
  // PDM
  LTS5::PointDistributionModel* pose_estimation = nullptr;
  if (has_pose_estmimation_model) {
    pose_estimation = new LTS5::PointDistributionModel();
  }
  // SVM
  LTS5::SvmTrackerAssessment* tracking_quality = nullptr;
  // Open file
  std::ifstream input_stream(sdm_model_name, std::ios_base::in);
  if (input_stream.is_open()) {
    // Ok can read mean shape
    error = face_tracker->Load(input_stream, false);
    // Load 3D PDM if needed
    if (has_pose_estmimation_model) {
      error |= pose_estimation->Load(input_stream, false);
    }
    // Nothing more in the file
    input_stream.close();
    // Init tracker assessment
    auto tracker_param = face_tracker->get_tracker_parameters();
    auto sift_param = face_tracker->Get_sift_descriptor_parameters();
    cv::Mat meanshape = face_tracker->get_meanshape();
    tracking_quality = new LTS5::SvmTrackerAssessment(meanshape,
                                                      sift_param,
                                                      tracker_param.face_region_width,
                                                      tracker_param.sift_size);
    // Load svm
    error |= tracking_quality->Load(tracking_assessment_model);
    // Default => -0.3
    tracking_quality->set_score_threshold(score_threshold);
    // Done, close input
    input_stream.close();
  }

  if (error == 0) {
    // Can convert
    std::string model_name;
    size_t ext_pos = sdm_model_name.rfind('.');
    if (ext_pos != std::string::npos) {
      // Need to remove current extension
      model_name = sdm_model_name.substr(0, ext_pos) + ".bin";
    } else {
      model_name = sdm_model_name + "bin";
    }
    // Open stream
    std::ofstream out_stream(model_name.c_str(),
                             std::ios_base::out|std::ios_base::binary);
    error = -1;
    if (out_stream.is_open()) {
      // Write Sdm
      error = face_tracker->Write(out_stream);
      // Write SVM
      error |= tracking_quality->Write(out_stream);
      // Write PDM
      if (has_pose_estmimation_model) {
        error |= pose_estimation->Write(out_stream);
      }
      // Done, sanity check
      error |= out_stream.good() ? 0 : -1;
      out_stream.close();
    }
  }
  delete face_tracker;
  delete pose_estimation;
  delete tracking_quality;
  return error;
}

# pragma mark -
# pragma mark Private methods

void SdmTrainer::
ComputeInitialShapes(const MultiArray<cv::Rect>& montecarlo_rects) {
  std::cout << "Computing initial shapes from the mean shape and the Monte "
  << "Carlo rectangles..." << std::endl;
  all_shapes_.clear();

  for (int i = 0; i < n_images_; i++) {
    for (int j = 0; j < tracker_parameters_.montecarlo_samples; j++) {
      cv::Mat shape = LTS5::RectRelToImageAbs(montecarlo_rects[i][j],
                                              mean_shape_);
      all_shapes_.push_back(shape);
    }
  }
}

/*
 *  @name   ComputeSIFT
 *  @brief  Compute SIFT features from the images and the sampled rectangles
 *  @param[in]  stage           Current stage
 *  @param[out] all_delta_x     Differences between shape and annotations
 *  @param[out] all_transforms  Vector of transforms for each image
 *  @param[out] sift_features   Features vector
 *  @return
 */
void SdmTrainer::ComputeSIFTAndDeltaX(const int& stage , cv::Mat* all_delta_x,
                                      std::vector<cv::Mat>* all_transforms,
                                      cv::Mat* sift_features) {
  assert(images_files_.size() == all_annotations_.size() &&
         images_files_.size() == n_images_);

  // Init holder
  all_transforms->clear();
  all_delta_x->create(n_images_ * tracker_parameters_.montecarlo_samples,
                      2 * n_shape_points_, CV_64FC1);
  sift_features->create(n_images_ * tracker_parameters_.montecarlo_samples,
                        tracker_parameters_.sift_parameters.sift_dim *
                        n_shape_points_, CV_64FC1);

  std::cout << "Computing SIFT features on " << n_images_ << " images..." <<
  std::endl;
  time_t t1, t2;
  time(&t1);

  int edge = tracker_parameters_.face_region_width;
  if (tracker_parameters_.coarse_to_fine_search &&
      stage < tracker_parameters_.number_of_coarse_stages) {
    edge /= 2;
  }

  // Avoid creating many matrices with same dim and type
  cv::Mat shape;
  cv::Mat transform;
  cv::Mat inverse_transform;

  cv::Mat norm_shape = mean_shape_.clone();
  norm_shape *= edge;
  norm_shape += edge/2;  // CHECK

  for (int i = 0; i < n_images_; i++) {
    // Load images
    // Loading grayscale images or converting them is NOT the same...
    cv::Mat image = cv::imread(images_files_[i]);
    cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
    if (all_flip_[i]) {
      cv::Mat image_flip;
      cv::flip(image, image_flip, 1);
      image = image_flip.clone();
    }
    // precompute sift matrix index
    int sift_row_idx = i * tracker_parameters_.montecarlo_samples;
    // Do montecarlo sampling
    for (int j = 0; j < tracker_parameters_.montecarlo_samples; j++) {
      //  annotation is: 136 rows x 1 col
      cv::Mat annotation = all_annotations_[i].clone();
      shape = all_shapes_[i * tracker_parameters_.montecarlo_samples +
                          j].clone();

      cv::Mat norm_image;
      LTS5::NormalizeScaleAndRotation(norm_shape, edge, image, &norm_image,
                                      &shape, &annotation, &transform);

      // cv::invertAffineTransform(transform, inverse_transform);
      all_transforms->push_back(transform);

      // LTS5::TransformShape(inverse_transform, &shape);  //< 136 rows x 1col
      cv::Mat delta_x = (annotation - shape).t();
      delta_x.copyTo(all_delta_x->row(i *
                                      tracker_parameters_.montecarlo_samples +
                                      j));

      // DEBUG PURPOSE
      // Visualization of the sampled shape (form Monte Carlo rectangles)
      if (true) {
        cv::Mat im_debug;
        im_debug = norm_image.clone();
        for (int p = 0; p < annotation.rows/2; p++) {
          cv::Point2d pt(annotation.at<double>(p, 0),
          annotation.at<double>(p + annotation.rows/2, 0));
          cv::Scalar color(0.5);
          cv::circle(im_debug, pt, 1, color, -1);
        }

        for (int p = 0; p < shape.rows/2; p++) {
          cv::Point2d pt(shape.at<double>(p, 0),
                         shape.at<double>(p+shape.rows/2, 0));
          cv::Scalar color(0.6);
          cv::circle(im_debug, pt, 2, color, -1);
        }
        std::string filename = "temp/im_" + std::to_string(stage) + "_" +
        std::to_string(i * tracker_parameters_.montecarlo_samples + j) +
        ".jpg";
        cv::imwrite(filename, im_debug);
      }
      //

      cv::Mat sift;
      std::vector<cv::KeyPoint> keypoints = LTS5::KeypointsFromShape(shape,
                                              tracker_parameters_.sift_size);
      SSift::ComputeDescriptorOpt(norm_image,
                                  keypoints,
                                  tracker_parameters_.sift_parameters,
                                  &sift);
      sift = sift.reshape(sift.channels(), 1 /*row*/);
      sift.convertTo(sift_features->row(sift_row_idx + j), CV_64FC1);
    }
  }
  time(&t2);
  std::cout << "   Duration: " << t2 - t1 << "s" << std::endl;
}

/*
 *  @name   ComputeSIFTPca
 *  @brief  Compute PCA on the SIFT features AND project SIFT on the PCA basis
 *  @param[in]    sift        sift features
 *  @param[out]   pca_sift    Resulting projection
 */
void SdmTrainer::ComputeSIFTPca(const cv::Mat& sift, cv::Mat* pca_sift) {
  std::cout << "Comptute PCA on the SIFT features..." << std::endl;
  std::cout << "   (dimensions of the input data: "<< sift.rows << " x " <<
  sift.cols << ", type " << sift.type() << ")" << std::endl;

  assert(tracker_parameters_.sift_parameters.sift_dim * n_shape_points_ ==
         sift.cols);

  time_t t1, t2;
//    cv::Mat cov_mat(sift.cols, sift.cols, sift.type());

  // Select proper computation mode
  time(&t1);
  LTS5::ComputePca(sift, tracker_parameters_.retain_variance,
                   tracker_parameters_.train_computation_mod, &pca_basis_);
  time(&t2);

//    switch (tracker_parameters_.train_computation_mod) {
//      case SdmTracker::kDefault :
//      {
//        // Option 3 -> Works! (9171s = 152m for 7860x8704)
//        time(&t1);
//        LTS5::ComputePcaOpencv(sift, tracker_parameters_.retain_variance,
//                               &pca_basis_);
//        time(&t2);
//      }
//        break;
//
//      case SdmTracker::kLapack :
//      {
//        // Option 2 -> Lapack (600-700 s)
//        time(&t1);
//        LTS5::ComputePcaLapack<double>(sift,
//                                       tracker_parameters_.retain_variance,
//                                       &pca_basis_);
//        time(&t2);
//      }
//        break;
//
//#ifdef _HAS_CULA
//      case SdmTracker::kCula :
//      {
//        std::cout << "   Calculating covariance matrix..." << std::endl;
//        time(&t1);
//        std::cout << "   CULA symmetric eigen-decomposition..." << std::endl;
//        LTS5::ComputePcaCula<double>(sift, tracker_parameters_.retain_variance,
//                                     &pca_basis_);
//        time(&t2);
//      }
//        break;
//#endif
//
//      default : throw std::runtime_error("Wrong type of computation (" +
//                                         std::string(FUNC_NAME) + ")");
//        break;
//    }

  std::cout << "   Duration: " << t2 - t1 << "s" << std::endl;
  std::cout << "   PCA eigenvectors: " << pca_basis_.rows << "x" <<
  pca_basis_.cols << std::endl;

  ProjectSIFTOnPca(sift, pca_sift);
}

/*
 *  @name   ProjectSIFTOnPca
 *  @brief  Project the SIFT features on the PCA basis
 */
void SdmTrainer::ProjectSIFTOnPca(const cv::Mat& sift, cv::Mat* pca_sift) {
  using TransposeType = LTS5::LinearAlgebra<double>::TransposeType;
  std::cout << "Projecting SIFT features on the PCA basis..." << std::endl;
  pca_sift->create(sift.rows, pca_basis_.rows, sift.type());
  time_t t1, t2;
  time(&t1);

  const double alpha = 1.0;
  const double beta = 0.0;
  LTS5::LinearAlgebra<double>::Gemm(sift, TransposeType::kNoTranspose,
                                    alpha, pca_basis_,
                                    TransposeType::kTranspose, beta,
                                    pca_sift);
  time(&t2);
  std::cout << "   Duration: " << t2 - t1 << "s" << std::endl;
}

}  // namespace LTS5

