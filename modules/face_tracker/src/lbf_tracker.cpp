/**
 *  @file   lbf_tracker.cpp
 *  @brief  LBF face tracker
 *
 *  @author Guillaume Jaume, Christophe Ecabert
 *  @date   27/02.2017
 *  Copyright (c) 2017 Guillaume Jaume. All rights reserved.
 */

#include <fstream>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/face_tracker/lbf_tracker.hpp"
#include "lts5/face_tracker/lbf_tracker_pimpl.hpp"
#include "lts5/face_tracker/lbf_tracker_assessment.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/sys/parallel.hpp"

namespace LTS5 {

#pragma mark -
#pragma mark RandomTree (Hidden class- PIMPL)

/*
 * @name  Load
 * @fn    int Load(std::istream& stream)
 * @brief Load a random tree from a given stream
 * @param[in] stream  Binary stream from where tree is loaded
 * @return    -1 if error, 0 otherwise
 */
int RandomTree::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Read landmark ID
    stream.read(reinterpret_cast<char*>(&landmark_id_), sizeof(landmark_id_));
    // Tree depth
    stream.read(reinterpret_cast<char*>(&tree_depth_), sizeof(tree_depth_));
    // Features + threshold
    int n_node = 1 << tree_depth_;
    feats_.create(n_node, 4, CV_64FC1);
    feats_.setTo(0.0);
    thresholds_.resize(n_node);
    for (int i = 1; i < n_node / 2; i++) {
      stream.read(reinterpret_cast<char*>(feats_.ptr<double>(i)),
                  feats_.cols * feats_.elemSize());
      stream.read(reinterpret_cast<char*>(&thresholds_[i]),
                  sizeof(thresholds_[0]));
    }
    err = stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  Write
 * @fn    int Write(std::ostream& stream)
 * @brief Write this random tree into a given stream
 * @param[in] stream  Stream where to ouput the object
 * @return    -1 if error, 0 otherwise
 */
int RandomTree::Write(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Dump landmark ID
    stream.write(reinterpret_cast<const char*>(&this->landmark_id_),
                 sizeof(this->landmark_id_));
    // Tree depth
    stream.write(reinterpret_cast<const char*>(&tree_depth_),
                 sizeof(tree_depth_));
    // Features + threshold
    int n_node = 1 << tree_depth_;
    for (int i = 1; i < n_node / 2; ++i) {
      // sampling location (feats)
      stream.write(reinterpret_cast<const char*>(feats_.ptr<double>(i)),
                   feats_.cols * feats_.elemSize());
      // thresh
      stream.write(reinterpret_cast<const char*>(&thresholds_[i]),
                   sizeof(thresholds_[0]));

    }
    err = stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize() const
 * @brief Compute memory need for this RandomTree
 * @return    Object size in byte
 */
int RandomTree::ComputeObjectSize() const {
  // Ints
  int sz = sizeof(this->landmark_id_);
  sz += sizeof(tree_depth_);
  // Feature matrix (sampling target)
  int n_node = 1 << tree_depth_;
  for (int i = 1; i < n_node / 2; ++i) {
    // Feats + thresh
    sz += feats_.cols * feats_.elemSize() + sizeof(thresholds_[0]);
  }
  return sz;
}

#pragma mark -
#pragma mark RandomForest (Hidden class- PIMPL)

/*
 * @name  Load
 * @fn    int Load(std::istream& stream)
 * @brief Init random forest from file
 * @param[in] stream  Binary stream to load from
 * @return    -1 if error, 0 otherwise
 */
int RandomForest::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    err = 0;
    // Read N landmark
    stream.read(reinterpret_cast<char*>(&n_landmark_), sizeof(n_landmark_));
    // Number of three
    stream.read(reinterpret_cast<char*>(&n_tree_), sizeof(n_tree_));
    // Tree's depth
    stream.read(reinterpret_cast<char*>(&tree_depth_), sizeof(tree_depth_));
    // load each trees
    random_trees_.resize(static_cast<size_t>(n_landmark_));
    for (int i = 0; i < n_landmark_; ++i) {
      auto& trees = random_trees_[i];
      trees.resize(static_cast<size_t>(n_tree_));
      for (int j = 0; j < n_tree_; ++j) {
        err |= trees[j].Load(stream);
      }
    }
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeLBF
 * @brief Compute feature for a given shape/image
 * @param[in] image           Input image
 * @param[in] current_shape   Current shape
 * @param[in] transform       Transformation from current_shape to mean-shape
 * @param[in] inner_shape     Flag to indicate if we compute only for
 *                            the innershape
 * @param[out] lbf            Computed features
 */
void RandomForest::ComputeLBF(const cv::Mat& image,
                              const cv::Mat& current_shape,
                              const cv::Mat& transform,
                              const bool inner_shape,
                              cv::SparseMat* lbf) {
  // Init output
  assert(n_landmark_ == 68);
  const int n_landmark = inner_shape ? n_landmark_ - 19 : n_landmark_;
  const int F = n_landmark * n_tree_ * (1 << (tree_depth_ - 1));
  const int dims[] = {1, F};
  lbf->create(2, dims, CV_64FC1);
  // Get similarity transform
  const auto* t_ptr = reinterpret_cast<const double*>(transform.data);
  // Generate LBF
  int base = 1 << (tree_depth_ - 1);

  static Mutex mutex;
  Parallel::For(n_landmark_,
                [&](const size_t& i) {
    // Extract only innershape if needed
    if (inner_shape) {
      LTS5_LOG_ERROR("Not supported");
    }
    const auto& trees = random_trees_[i];
    const auto* cs_ptr = reinterpret_cast<const double*>(current_shape.data);
    const auto image_w = static_cast<double>(image.cols - 1);
    const auto image_h = static_cast<double>(image.rows - 1);
    for (int j = 0; j < n_tree_; ++j) {
      const auto& t = trees[j];
      const auto& feats = t.get_feats();
      const auto& thresh = t.get_threshold();
      int code = 0;
      int idx = 1;
      // Go through the tree
      for (int k = 1; k < tree_depth_; ++k) {
        // Get sampling location
        const auto& x1 = feats.at<double>(idx, 0);
        const auto& y1 = feats.at<double>(idx, 1);
        const auto& x2 = feats.at<double>(idx, 2);
        const auto& y2 = feats.at<double>(idx, 3);
        // Move them into image space
        double x1t = t_ptr[0] * x1 + t_ptr[1] * y1;
        double x2t = t_ptr[0] * x2 + t_ptr[1] * y2;
        double y1t = t_ptr[3] * x1 + t_ptr[4] * y1;
        double y2t = t_ptr[3] * x2 + t_ptr[4] * y2;
        // Add to current location
        x1t += cs_ptr[i];
        x2t += cs_ptr[i];
        y1t += cs_ptr[i + n_landmark_];
        y2t += cs_ptr[i + n_landmark_];
        // Check image boundaries
        x1t = std::max(0.0, std::min(x1t, image_w));
        x2t = std::max(0.0, std::min(x2t, image_w));
        y1t = std::max(0.0, std::min(y1t, image_h));
        y2t = std::max(0.0, std::min(y2t, image_h));
        // Sample image
        int density = (image.at<uchar>(int(y1t), int(x1t)) -
                       image.at<uchar>(int(y2t), int(x2t)));
        code <<= 1;
        if (density < thresh[idx]) {
          idx = 2 * idx;
        } else {
          code += 1;
          idx = 2 * idx + 1;
        }
      }

      {
        ScopedLock lock(mutex);
        lbf->ref<double>(0, (i * n_tree_ + j) * base + code) = 1.0;
      }
      //lbf->at<int>(i * n_tree_ + j) = (i * n_tree_ + j) * base + code;
    }
  });
}

/*
 * @name
 * @brief Dump into a given stream
 * @param[in] stream  Stream where to dump data
 * @return    -1 if error, 0 otherwise
 */
int RandomForest::Write(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // N landmarks
    stream.write(reinterpret_cast<const char*>(&this->n_landmark_),
                 sizeof(this->n_landmark_));
    // N tree
    stream.write(reinterpret_cast<const char*>(&n_tree_),
                 sizeof(n_tree_));
    // Each tree depth
    stream.write(reinterpret_cast<const char*>(&tree_depth_),
                 sizeof(tree_depth_));
    // Dump each trees
    for (int i = 0; i < this->n_landmark_; ++i) {
      const auto& tree = this->random_trees_[i];
      for (int j = 0; j < n_tree_; ++j) {
        tree[j].Write(stream);
      }
    }
    err = stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize() const
 * @brief Compute memory need for this RandomForest
 * @return    Object size in byte
 */
int RandomForest::ComputeObjectSize() const {
  int sz = sizeof(this->n_landmark_);
  sz += sizeof(n_tree_);
  sz += sizeof(tree_depth_);
  // compute size for each tree
  for (int i = 0; i < this->n_landmark_; ++i) {
    const auto& tree = this->random_trees_[i];
    for (int j = 0; j < n_tree_; ++j) {
      sz += tree[j].ComputeObjectSize();
    }
  }
  return sz;
}

#pragma mark -
#pragma mark LBFCascade - (Hidden class- PIMPL)

/*
 * @name LBFCascade()
 * @brief Constructor
 */
LBFCascade::LBFCascade() {
}

/*
 * @name ~LBFCascade()
 * @brief Destructor
 */
LBFCascade::~LBFCascade() {
  if (!random_forests_.empty()) {
    for (auto& f : random_forests_) {
      delete f;
      f = nullptr;
    }
  }
}

/*
 * @name  Load
 * @fn    int Load(std::istream& stream)
 * @brief Init LBFCascade from file
 * @param[in] stream  Binary stream to load from
 * @return    -1 if error, 0 otherwise
 */
int LBFCascade::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    err = 0;
    // Read number of stage
    stream.read(reinterpret_cast<char*>(&n_stages_), sizeof(n_stages_));
    // Read number of tree
    stream.read(reinterpret_cast<char*>(&n_tree_), sizeof(n_tree_));
    // Read tree depth
    stream.read(reinterpret_cast<char*>(&tree_depth_), sizeof(tree_depth_));
    // Read number of landmark
    stream.read(reinterpret_cast<char*>(&n_landmark_), sizeof(n_landmark_));
    // Clean up if already loaded
    if (!random_forests_.empty()) {
      for (auto& f : random_forests_) {
        delete f;
        f = nullptr;
      }
    }
    // Loop over all stages
    random_forests_.resize(static_cast<size_t>(n_stages_), nullptr);
    gl_regression_weights_.resize(static_cast<size_t>(n_stages_));
    for (int i = 0; i < n_stages_; ++i) {
      // Read forest
      random_forests_[i] = new RandomForest();
      err |= random_forests_[i]->Load(stream);
      // Read regressor
      err |= LTS5::ReadMatFromBin(stream, &gl_regression_weights_[i]);
    }
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  Predict
 * @fn    void Predict(const cv::Mat& image, cv::Mat* shape)
 * @brief Predict the shape given an image
 * @param[in] image           Image where to perform alignment
 * @param[in] meanshape       Meanshape
 * @param[in,out] shape       Updated Shape from alignment
 */
void LBFCascade::Predict(const cv::Mat& image,
                         const cv::Mat& meanshape,
                         cv::Mat* shape) {
  // Loop over all stage
  cv::SparseMat lbf_features;
  cv::Mat transform;
  for (int k = 0; k < n_stages_; ++k) {
    // Compute transforms
    ComputeSimilarityTransform(meanshape, *shape, &transform);
    // Generate LBF
    random_forests_[k]->ComputeLBF(image,
                                   *shape,
                                   transform,
                                   false,
                                   &lbf_features);
    // Compute shape update with regression and update shape
    this->ComputeAndUpdateShape(k,
                                transform,
                                lbf_features,
                                shape);
  }
}

/*
 * @name  Write
 * @brief Dump into a given stream
 * @param[in] stream  Stream where to dump data
 * @return    -1 if error, 0 otherwise
 */
int LBFCascade::Write(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Parameters
    stream.write(reinterpret_cast<const char*>(&n_stages_),
                 sizeof(n_stages_));
    stream.write(reinterpret_cast<const char*>(&n_tree_),
                 sizeof(n_tree_));
    stream.write(reinterpret_cast<const char*>(&tree_depth_),
                 sizeof(tree_depth_));
    stream.write(reinterpret_cast<const char*>(&n_landmark_),
                 sizeof(n_landmark_));
    // Every stage
    for (int i = 0; i < n_stages_; ++i) {
      // Write forest
      err |= random_forests_[i]->Write(stream);
      // Write regressor
      err |= LTS5::WriteMatToBin(stream, gl_regression_weights_[i]);
    }
    err = stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize() const
 * @brief Compute memory need for this LBFCascade
 * @return    Object size in byte
 */
int LBFCascade::ComputeObjectSize() const {
  int sz = sizeof(n_stages_);
  sz += sizeof(n_tree_);
  sz += sizeof(tree_depth_);
  sz += sizeof(n_landmark_);
  // Every stage
  for (int i = 0; i < n_stages_; ++i) {
    // Forest
    sz += random_forests_[i]->ComputeObjectSize();
    // Regressor
    sz += 3 * sizeof(int);
    sz += (gl_regression_weights_[i].total() *
           gl_regression_weights_[i].elemSize());
  }
  return sz;
}

/*
 * @name  ComputeAndUpdateShape
 * @fn    void ComputeAndUpdateShape(const int stage,
                                     const cv::Mat& transform,
                                     const cv::SparseMat& lbf_feature,
                                     cv::Mat* shape)
 * @brief Compute shape update with learned regression and update a given
 *        shape
 * @param[in] stage           Current stage
 * @param[in] transform       Transform from mean to shape
 * @param[in] lbf_feature     Feature to use to predict update
 * @param[in,out] shape       Shape to be updated with delta predicted
 */
void LBFCascade::ComputeAndUpdateShape(const int stage,
                                       const cv::Mat& transform,
                                       const cv::SparseMat& lbf_feature,
                                       cv::Mat* shape) {
  cv::Mat& weight = gl_regression_weights_[stage];
  const auto* t_ptr = reinterpret_cast<const double*>(transform.data);
  //const int* lbf_ptr = reinterpret_cast<const int*>(lbf_feature.data);
  auto* s_ptr = reinterpret_cast<double*>(shape->data);
  int n = std::max(shape->rows, shape->cols) / 2;
  Parallel::For(n,
                [&](const size_t& i) {
    // Compute shape update update
    double x = 0.0, y = 0.0;
    const double* wx_ptr = weight.ptr<double>(2 * i);
    const double* wy_ptr = weight.ptr<double>(2 * i + 1);
    /*for (int j = 0; j < lbf_feature.cols; ++j) {
      x += wx_ptr[lbf_ptr[j]];
      y += wy_ptr[lbf_ptr[j]];
    }*/
    for (auto it = lbf_feature.begin(); it != lbf_feature.end(); ++it) {
      // Recover index
      const cv::SparseMat::Node* node = it.node();
      const int idx = node->idx[1];
      x += wx_ptr[idx];
      y += wy_ptr[idx];
    }
    // Apply transform
    const double xt = t_ptr[0] * x + t_ptr[1] * y;
    const double yt = t_ptr[3] * x + t_ptr[4] * y;
    // Update shape
    s_ptr[i] += xt;
    s_ptr[i + n] += yt;
  });
}


#pragma mark -
#pragma mark LBFTracker

#pragma mark -
#pragma mark Initialization

/*
 *  @name   LbfTrainer
 *  @fn LBFTracker()
 *  @brief  Constructor
 */
LBFTracker::LBFTracker() : lbf_cascade_(nullptr),
                           face_detector_(nullptr),
                           is_tracking_(false),
                           tracking_assessment_(nullptr) {
  shape_center_gravity_ = cv::Point2f(-1.f, -1.f);
}

/*
 *  @name   ~LBFTracker
 *  @brief  Destructor
 */
LBFTracker::~LBFTracker() {
  // Clean up
  if (face_detector_) {
    delete face_detector_;
    face_detector_ = nullptr;
  }
  if (lbf_cascade_) {
    delete lbf_cascade_;
    lbf_cascade_ = nullptr;
  }
  if (tracking_assessment_) {
    delete tracking_assessment_;
    tracking_assessment_ = nullptr;
  }
}

/*
 * @name Load
 * @brief read the model for tracking
 * @param model_filename
 * @return error
 */
int LBFTracker::Load(const std::string& model_filename) {
  int err = -1;
  std::ifstream stream(model_filename.c_str(),
                       std::ios_base::in | std::ios_base::binary);
  if (stream.is_open()) {
    // Load
    err = this->Load(stream);
    // Close
    stream.close();
  }
  return err;
}

/*
 *  @name   Load
 *  @brief  Load the tracker model
 *  @param[in]  model_filename      Path to the model
 * @param[in] face_detector_config (xml file)
 *  @return -1 if errors, 0 otherwise
 */
int LBFTracker::Load(const std::string& model_filename,
                     const std::string& face_detector_config) {
  // Init LBF tracker
  int err = this->Load(model_filename);
  // Init Viola-Jones detector
  face_detector_ = new cv::CascadeClassifier(face_detector_config);
  err = !face_detector_->empty() ? err : -1;
  return err;
}

/*
 * @name Load
 * @brief read the model for tracking
 * @param stream  Binary stream pointing to model
 * @return error
 */
int LBFTracker::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Seek for LBF tracker
    int status = LTS5::ScanStreamForObject(stream,
                                           HeaderObjectType::kLBFTracker);
    if (status == 0) {
      // Check of already init
      if (lbf_cascade_) {
        delete lbf_cascade_;
        lbf_cascade_ = nullptr;
      }
      // Read meanshape
      err = ReadMatFromBin(stream, &meanshape_);
      // Init cascade
      lbf_cascade_ = new LBFCascade();
      err |= lbf_cascade_->Load(stream);
      // Tracker assessment
      if (tracking_assessment_) {
        delete tracking_assessment_;
        tracking_assessment_ = nullptr;
      }
      tracking_assessment_ = new LBFTrackerAssessment(meanshape_);
      err |= tracking_assessment_->Load(stream, true);
    }
  }
  return err;
}

/*
 *  @name   Save
 *  @fn     Save(const std::string& model_name)
 *  @brief  Save the trained model in file. This must be override by subclass
 *  @param[in] model_name   Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int LBFTracker::Save(const std::string& model_name) {
  int err = -1;
  std::ofstream stream(model_name.c_str(),
                       std::ios_base::out | std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Write(stream);
  }
  return err;
}

/*
 *  @name   Write
 *  @fn int Write(std::ostream& out_stream) const
 *  @brief  Write the object into a binary stream
 *  @param[in]  out_stream  Binary stream to files
 *  @return -1 if error, 0 otherwise.
 */
int LBFTracker::Write(std::ostream& out_stream) const {
  int err = -1;
  if (out_stream.good()) {
    // Define object size
    int sz = 3 * sizeof(int);
    sz += meanshape_.total() * meanshape_.elemSize();
    sz += lbf_cascade_->ComputeObjectSize();
    // Write
    int dummy = static_cast<int>(HeaderObjectType::kLBFTracker);
    out_stream.write(reinterpret_cast<const char*>(&dummy),
                     sizeof(dummy));
    out_stream.write(reinterpret_cast<const char*>(&sz),
                     sizeof(sz));
    // Meanshape
    err |= LTS5::WriteMatToBin(out_stream, meanshape_);
    // Tracker
    err |= lbf_cascade_->Write(out_stream);
    err |= out_stream.good() ? 0 : -1;
  }
  return err;
}

/*
 *  @name   ComputeObjectSize
 *  @fn int ComputeObjectSize() const
 *  @brief  Compute the memory used by object
 *  @return Size in bytes
 */
int LBFTracker::ComputeObjectSize() const {
  int sz = 3 * sizeof(int);
  sz += meanshape_.total() * meanshape_.elemSize();
  sz += lbf_cascade_->ComputeObjectSize();
  return sz;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Detect
 *  @fn      int Detect(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks. This must be override by subclass
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int LBFTracker::Detect(const cv::Mat& image, cv::Mat* shape) {
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_, cv::COLOR_BGR2GRAY);
  } else {
    working_img_ = image;
  }
  // Downsample image to speedup computation
  float aspect_ratio = (float)working_img_.rows / (float)working_img_.cols;
  int img_height = static_cast<int>(320.f * aspect_ratio);
  cv::Mat downsample_input;
  cv::resize(working_img_, downsample_input, cv::Size(320, img_height));
  float scale = static_cast<float>(working_img_.cols) / static_cast<float>(downsample_input.cols);
  // Scan image for faces
  int err = -1;
  cv::Rect face_region;
  std::vector<cv::Rect> faces_location;
  face_detector_->detectMultiScale(downsample_input, faces_location,
                                   1.1, 2, 0,
                                   cv::Size((200.0f/scale), (200.0f/scale)));
  if (!faces_location.empty()) {
    // Detector has return something
    // Do we have already have a good fit ?
    if (shape_center_gravity_.x < 0 && shape_center_gravity_.y < 0) {
      // No previous good fit, use largest rectangle
      std::sort(faces_location.begin(),
                faces_location.end(),
                LBFTracker::SortRectBySize());
      face_region = faces_location[0];
    } else {
      //  Use face rect closest to the previous one
      float min_dist = std::numeric_limits<float>::max();
      cv::Point2f current_center;
      cv::Point2f face_displacement;
      float norm_face_displacement = -1.f;

      // Get through the list
      for (cv::Rect rect : faces_location) {
        current_center = (rect.br() + rect.tl()) * 0.5f * scale;
        face_displacement = shape_center_gravity_ - current_center;
        norm_face_displacement = std::sqrt(face_displacement.x *
                                           face_displacement.x +
                                           face_displacement.y *
                                           face_displacement.y);
        // Closest ?
        if (norm_face_displacement < min_dist) {
          min_dist = norm_face_displacement;
          face_region = rect;
        }
      }
    }
    // Convert back to original image coordinate
    int x = static_cast<int>(face_region.x * scale);
    int y = static_cast<int>(face_region.y * scale);
    int w = static_cast<int>(face_region.width * scale);
    int h = static_cast<int>(face_region.height * scale);
    face_region.x = x;
    face_region.y = y;
    face_region.width = w;
    face_region.height = h;
    // Can track landmarks, create inital shape. If size is Ok does nothing
    shape->create(meanshape_.rows, meanshape_.cols, meanshape_.type());
    previous_shape_.create(meanshape_.rows, meanshape_.cols, meanshape_.type());
    // Compute initial shape
    this->ComputeInitialShape(meanshape_, face_region, shape);
    //  Face alignment
    lbf_cascade_->Predict(working_img_, meanshape_, shape);
    // Save shape as previous
    previous_shape_ = shape->clone();
    // Check alignment quality
    err = tracking_assessment_->Assess(working_img_, *shape) > 0.0 ? 0 : -1;
    if (err) {
      *shape = cv::Mat();
    }
  }
  return err;
}


/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image, const cv::Rect& face_region,
                        cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks. This must be override by subclass
 *  @param[in]  image           Input image
 *  @param[in]  face_region     Face boundingg box
 *  @param[out] shape           Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int LBFTracker::Detect(const cv::Mat &image,
                       const cv::Rect &face_region,
                       cv::Mat* shape) {
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_, cv::COLOR_BGR2GRAY);
  } else {
    working_img_ = image;
  }
  // Scan image for faces
  int err = -1;
  // Can track landmarks, create inital shape. If size is Ok does nothing
  shape->create(meanshape_.rows, meanshape_.cols, meanshape_.type());
  previous_shape_.create(meanshape_.rows, meanshape_.cols, meanshape_.type());
  // Initial shape
  this->ComputeInitialShape(meanshape_, face_region, shape);
  // Alignment
  lbf_cascade_->Predict(working_img_, meanshape_, shape);
  // Save shape as previous, deep copy
  previous_shape_ = shape->clone();
  // Check alignment quality
  err = tracking_assessment_->Assess(working_img_, *shape) > 0.0 ? 0 : -1;
  if (err) {
    *shape = cv::Mat();
  }
  return err;
}

/*
 *  @name   Track
 *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Tracking method, given an input image (from a sequence), provides
 *          a set of landmarks.
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return -1 if error, 0 otherwise
*/
int LBFTracker::Track(const cv::Mat& image, cv::Mat* shape) {

  int err = -1;
  // Image format is correct ?
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_, cv::COLOR_BGR2GRAY);
  } else {
    working_img_ = image;
  }
  // Have we already detect something ?
  if (!is_tracking_) {
    // Call for detection
    err = this->Detect(working_img_, shape);
    is_tracking_ = err == 0;
  } else {
    // Tracking
    previous_shape_ = *shape;
    this->AlignMeanshapeToCurrent(previous_shape_,
                                  meanshape_,
                                  shape);
    lbf_cascade_->Predict(working_img_, meanshape_, shape);
    // Check alignment quality
    is_tracking_ = tracking_assessment_->Assess(working_img_, *shape) > 0.0;
    err = 0;
  }
  // still tracking ?
  if (!is_tracking_) {
    *shape = cv::Mat();
    is_tracking_ = false;
  } else {
    // Save shape
    previous_shape_ = shape->clone();
    // Compute shape center of gravity
    const int n = std::max(meanshape_.rows, meanshape_.cols) / 2;
    shape_center_gravity_.x = 0;
    shape_center_gravity_.y = 0;
    for (int i = 0; i < n; ++i) {
      shape_center_gravity_.x += shape->at<double>(i);
      shape_center_gravity_.y += shape->at<double>(i + n);
    }
    shape_center_gravity_.x /= n;
    shape_center_gravity_.y /= n;
  }
  return err;
}

/*
 *  @name   Track
 *  @fn int Track(const cv::Mat& image, const cv::Rect& face_region,
 *                cv::Mat* shape)
 *  @brief  Tracking method, given an input image (from a sequence), provides
 *          a set of landmarks.
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return -1 if error, 0 otherwise
*/
int LBFTracker::Track(const cv::Mat& image,
                      const cv::Rect& face_region,
                      cv::Mat* shape) {
  // Have we already detect something ?
  int err = -1;
  if (!is_tracking_) {
    // Call for detection
    err = this->Detect(image, face_region, shape);
    is_tracking_ = true;
  } else {
    // Tracking
    previous_shape_ = *shape;
    this->AlignMeanshapeToCurrent(previous_shape_,
                                  meanshape_,
                                  shape);
    lbf_cascade_->Predict(working_img_, meanshape_, shape);
    err = 0;
  }
  // Still tracking ?
  is_tracking_ = tracking_assessment_->Assess(working_img_, *shape) > 0.0;
  if (!is_tracking_) {
    *shape = cv::Mat();
    is_tracking_ = false;
  }
  return err;
}


/**
*  @name   Train
*  @fn void Train(const std::string& folder)
*  @brief  Tracker training method
*  @param[in]  folder  Location of images/annotations
*/
void LBFTracker::Train(const std::string& folder) {}

#pragma mark -
#pragma mark Utility

/*
 *  @name   draw_shape
 *  @brief  Draw shape on image
 *  @param[in]  img     Input image
 *  @param[in]  shape   Shape to draw
 *  @param[in]  color   Color of the shape
 *  @return Copy of the image with drawing
 */
cv::Mat LBFTracker::draw_shape(const cv::Mat& img,
                               const cv::Mat& shape,
                               const cv::Scalar& color) {
  cv::Mat canvas = img.clone();
  if (canvas.channels() == 1) {
    cv::cvtColor(canvas, canvas, cv::COLOR_GRAY2BGR);
  }
  int nPoints = shape.rows / 2;

  for (int p = 0; p < nPoints; p++) {
    int x = cvRound(shape.at<double>(p, 0));
    int y = cvRound(shape.at<double>(p + nPoints, 0));
    cv::circle(canvas, cv::Point(x, y), 1, color, -1, cv::LINE_AA);
  }
  return canvas;
}

#pragma mark -
#pragma mark Private

/*
 *  @name   ComputeInitialShape
 *  @brief  Aligned the meanshape inside the bounding box
 *  @param[in]  meanshape       Model meanshape
 *  @param[in]  bounding_box    Region of interest
 *  @param[out] shape           Initial shape
 */
void LBFTracker::ComputeInitialShape(const cv::Mat& meanshape,
                                     const cv::Rect& bbox,
                                     cv::Mat* shape) {
  // Goes through all points
  int n = std::max(meanshape.rows, meanshape.cols) / 2;
  const double* ms_ptr = reinterpret_cast<const double*>(meanshape.data);
  double* s_ptr = reinterpret_cast<double*>(shape->data);
  for (int p = 0 ; p < n ; ++p) {
    s_ptr[p] = bbox.x + ms_ptr[p] * bbox.width;
    s_ptr[p + n] = bbox.y + ms_ptr[p + n] * bbox.height;
  }
}
/*
 * @name  AlignMeanshapeToCurrent
 * @fn    void AlignMeanshapeToCurrent(const cv::Mat& current_shape,
                             const cv::Mat& meanshape,
                             cv::Mat* shape)
 * @brief Place the \p meanshape at the position of \p current_shape
 * @param[in] current_shape   Current shape
 * @param[in] meanshape       Meanshape
 * @param[out] shape          New shape (transformed meanshape)
 */
void LBFTracker::AlignMeanshapeToCurrent(const cv::Mat& current_shape,
                                         const cv::Mat& meanshape,
                                         cv::Mat* shape) {
  // Init
  shape->create(meanshape.rows, meanshape.cols, meanshape.type());
  double* s_ptr = reinterpret_cast<double*>(shape->data);
  const double* ms_ptr = reinterpret_cast<const double*>(meanshape.data);
  // Compute similarity
  cv::Mat transform;
  ComputeSimilarityTransform(meanshape, current_shape, &transform);
  const double* t_ptr = reinterpret_cast<const double*>(transform.data);
  // Apply
  int n = std::max(meanshape.rows, meanshape.cols) / 2;
  for (int i = 0; i < n; ++i) {
    const double x = ms_ptr[i];
    const double y = ms_ptr[i + n];
    s_ptr[i] = t_ptr[0] * x + t_ptr[1] * y + t_ptr[2];
    s_ptr[i + n] = t_ptr[3] * x + t_ptr[4] * y + t_ptr[5];
  }
}

/*
 * @name  ~LBFProxy
 * @fn    ~LBFProxy()
 * @brief Destructor
 */
LBFProxy::~LBFProxy() {}

/**
 * @name  Create
 * @fn    BaseFaceTracker<cv::Mat, cv::Mat>* Create() const override
 * @brief Create an instance of BaseFaceTracker
 * @return    Tracker instance with proper type
 */
BaseFaceTracker<cv::Mat, cv::Mat>* LBFProxy::Create() const {
  return new LBFTracker();
};

/**
 *  @name Name
 *  @fn const char* Name() const override
 *  @brief  Return the name for a given type of tracker
 *  @return Tracker name (i.e. sdm, lbf, ...)
 */
const char* LBFProxy::Name() const {
  return "lbf";
}

/** Explicit registration */
LBFProxy lbf_proxy;

}  // namespace LTS5


