/**
 *  @file   base_face_tracker.cpp
 *  @brief  Base Face tracker definition
 *  @ingroup    face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   14/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/face_tracker/base_face_tracker.hpp"
#include "lts5/face_tracker/mstream_face_tracker.hpp"
#include "lts5/face_tracker/tracker_factory.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name  TrackerProxy
 * @fn    TrackerProxy(void)
 * @brief Constructor
 */
TrackerProxy::TrackerProxy(void) {
  TrackerFactory::Get().Register(this);
}


/*
 * @name  TrackerProxy
 * @fn    TrackerProxy(void)
 * @brief Constructor
 */
MTrackerProxy::MTrackerProxy(void) {
  MTrackerFactory::Get().Register(this);
}

}  // namepsace LTS5
