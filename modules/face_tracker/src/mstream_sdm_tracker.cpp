/**
 *  @file   mstream_sdm_tracker.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   04/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifdef __APPLE__
#include <dispatch/dispatch.h>
#endif

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/image_transforms.hpp"
#include "lts5/face_tracker/mstream_sdm_tracker.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/face_tracker/const_tracker_assessment.hpp"
#include "lts5/face_tracker/svm_tracker_assessment.hpp"

#include "lts5/utils/math/linear_algebra_wrapper.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   MStreamSdmTracker
 *  @fn MStreamSdmTracker(const int n_stream)
 *  @brief  Constructor
 *  @param[in]  n_stream  Number of image to track
 */
MStreamSdmTracker::MStreamSdmTracker(const int n_stream) : n_stream_(n_stream),
     num_points_(-1),
     initial_shape_(static_cast<size_t>(n_stream_)),
     previous_shape_(static_cast<size_t>(n_stream_)),
     is_tracking_(static_cast<size_t>(n_stream_), false),
     face_detector_(nullptr),
     face_region_(static_cast<size_t>(n_stream_)),
     shape_center_gravity_(static_cast<size_t>(n_stream_), cv::Point2f(-1.f, -1.f)),
     working_img_(static_cast<size_t>(n_stream_)),
     sift_features_(static_cast<size_t>(n_stream_)),
     inner_sift_features_(static_cast<size_t>(n_stream_)),
     tracking_assessment_(nullptr),
     release_assessor_(true) {
}

/*
 *  @name   ~MStreamSdmTracker
 *  @fn ~MStreamSdmTracker(void)
 *  @brief  Destructor
 */
MStreamSdmTracker::~MStreamSdmTracker(void) {
  // Clean up
  if (!face_detector_) {
    delete face_detector_;
    face_detector_ = nullptr;
  }
  if (!tracking_assessment_ && release_assessor_) {
    delete tracking_assessment_;
    tracking_assessment_ = nullptr;
  }
}

/*
 *  @name   Load
 *  @fn     int Load(const std::string& model_filename);
 *  @brief  Load the SDM model
 *  @param[in]  model_filename      Path to the model
 *  @return -1 if errors, 0 otherwise
 */
int MStreamSdmTracker::Load(const std::string& model_filename) {
  // Binary model ?
  int error = -1;
  const bool is_binary = model_filename.find(".bin") != std::string::npos;
  std::ios_base::openmode flag = (is_binary ?
                                  std::ios_base::in|std::ios_base::binary :
                                  std::ios_base::in);
  std::ifstream input_stream(model_filename.c_str(), flag);
  if (input_stream.is_open()) {
    int status = LTS5::ScanStreamForObject(input_stream,
                                           HeaderObjectType::kSdm);
    if (status == 0) {
      error = this->Load(input_stream, is_binary);
    }
  }
  return error;
}

/*
 *  @name   Load
 *  @fn     int Load(std::ifstream& model_stream, const bool& is_binary)
 *  @brief  Load the SDM model
 *  @param[in]  model_stream      Stream to the model
 *  @param[in]  is_binary         Indicate if stream is open as binary
 *  @return -1 if errors, 0 otherwise
 */
int MStreamSdmTracker::Load(std::ifstream& model_stream,
                            const bool& is_binary) {
  // Check if it is a binary stream or not
  int error;
  if (is_binary) {
    // Reader header
    error = this->tracker_parameters_.ReadParameterHeader(model_stream);
    // Bin
    error |= LTS5::ReadMatFromBin(model_stream, &sdm_meanshape_);
    // Define number of tracked points
    num_points_ = sdm_meanshape_.rows/2;
    // Init feature extraction tree, aka keypoints
    sift_extraction_location_.resize(static_cast<size_t>(n_stream_),
                                     std::vector<cv::KeyPoint>(static_cast<size_t>(num_points_),
                                                               cv::KeyPoint()));
    // Load sdm
    sdm_model_.clear();
    sdm_model_.reserve(static_cast<size_t>(tracker_parameters_.number_of_stage));
    for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
      cv::Mat stage_model;
      error &= LTS5::ReadMatFromBin(model_stream, &stage_model);
      sdm_model_.push_back(stage_model);
      if (error != 0)
        break;
    }
    // Tracker assessment present ?
    HeaderObjectType object_type = static_cast<HeaderObjectType>(0);
    int object_size = 0;
    char dummy = 0;
    bool keep_reading = true;
    while (model_stream.good() && keep_reading) {
      model_stream.read(reinterpret_cast<char*>(&object_type), sizeof(object_type));
      model_stream.read(reinterpret_cast<char*>(&object_size), sizeof(object_size));
      switch (object_type) {
        // Svm
        case LTS5::HeaderObjectType::kSvmTrackerAssessment :
          tracking_assessment_ = new SvmTrackerAssessment(sdm_meanshape_,
                                                          sift_desciptor_params_,
                                                          tracker_parameters_.face_region_width,
                                                          tracker_parameters_.sift_size);
          release_assessor_ = true;
          keep_reading = false;
          break;

          // Const
        case LTS5::HeaderObjectType::kConstTrackerAssessment :
          tracking_assessment_ = new ConstTrackerAssessment(true);
          release_assessor_ = true;
          keep_reading = false;
          break;

        default :  // No supported type -> Fast forward to next object;
          model_stream.read(&dummy, object_size);
          break;
      }
    }
    // Is assessor still not defined ?
    if (tracking_assessment_ == nullptr) {
      // Instantial constant assessor
      tracking_assessment_ = new ConstTrackerAssessment(true);
    }
    // Load assessor
    tracking_assessment_->Load(model_stream, is_binary);
  } else {
    // Char
    error = LTS5::ReadMatFromChar(model_stream, &sdm_meanshape_);
    // Define number of tracked points
    num_points_ = sdm_meanshape_.rows/2;
    // Init feature extraction tree, aka keypoints
    sift_extraction_location_.clear();
    sift_extraction_location_.resize(static_cast<size_t>(n_stream_),
                                     std::vector<cv::KeyPoint>(static_cast<size_t>(num_points_),
                                                               cv::KeyPoint()));
    sdm_model_.clear();
    sdm_model_.reserve(static_cast<size_t>(tracker_parameters_.number_of_stage));
    for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
      cv::Mat stage_model;
      error &= LTS5::ReadMatFromChar(model_stream, &stage_model);
      sdm_model_.push_back(stage_model);
      if (error != 0)
        break;
    }
    // Create assessor
    tracking_assessment_ = new LTS5::ConstTrackerAssessment(true);
  }
  return error;
}

/*
 *  @name   Load
 *  @fn     int Load(const std::string& model_filename,
            const std::string& face_detector_config)
 *  @brief  Load the SDM model and Viola Jones face detector
 *  @param[in]  model_filename          Path to the model
 *  @param[in]  face_detector_config    Configuration file for
 *                                      Viola & Jones face detector
 *  @return -1 if errors, 0 otherwise
 */
int MStreamSdmTracker::Load(const std::string& model_filename,
                            const std::string& face_detector_config) {
  // Load SDM
  int error = this->Load(model_filename);
  // Init Viola-Jones detector
  face_detector_ = new cv::CascadeClassifier(face_detector_config);
  error = !face_detector_->empty() ? error : -1;
  return error;
}

/*
 *  @name   Load
 *  @fn     int Load(std::ifstream& model_stream,
                     const bool& is_binary,
                     const std::string& face_detector_config)
 *  @brief  Load the SDM model and Viola Jones face detector
 *  @param[in]  model_stream          Stream to the model
 *  @param[in]  is_binary             Indicate if stream is open as binary
 *  @param[in]  face_detector_config  Configuration file for Viola & Jones
 *                                    face detector
 *  @return -1 if errors, 0 otherwise
 */
int MStreamSdmTracker::Load(std::ifstream& model_stream,
                            const bool& is_binary,
                            const std::string& face_detector_config) {
  int error = -1;
  // Load SDM
  if (model_stream.is_open()) {
    error = this->Load(model_stream, is_binary);
  }
  // Init Viola-Jones detector
  face_detector_ = new cv::CascadeClassifier(face_detector_config);
  error |= !face_detector_->empty() ? error : -1;

  return error;
}

/*
 *  @name   Save
 *  @fn int Save(const std::string& model_name)
 *  @brief  Save the trained model in file.
 *  @param  [in]    model_name      Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int MStreamSdmTracker::Save(const std::string& model_name) {
  return -1;
}

/*
 *  @name   Write
 *  @fn int Write(std::ofstream& out_stream) const
 *  @brief  Write the object into a binary stream
 *  @param[in]  out_stream  Binary stream to files
 *  @return -1 if error, 0 otherwise.
 */
int MStreamSdmTracker::Write(std::ofstream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Ok
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kSdm);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type), sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size), sizeof(obj_size));
    // Parameters header
    error = tracker_parameters_.WriteParameterHeader(out_stream);
    // Mean shape
    error |= LTS5::WriteMatToBin(out_stream, sdm_meanshape_);
    // Regressor
    for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
      error |= LTS5::WriteMatToBin(out_stream, sdm_model_[i]);
    }
    // Quality assessment
    error |= tracking_assessment_->Write(out_stream);
    // Done sanity check,
    error |= out_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name   ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the memory used by object
 *  @return Size in bytes
 */
int MStreamSdmTracker::ComputeObjectSize(void) const {
  int size = 0;
  // Configuration header
  size = tracker_parameters_.ComputeHeaderSize();
  // Meanshape
  size += 3 * sizeof(int);
  size += sdm_meanshape_.total() * sdm_meanshape_.elemSize();
  // Regressor
  for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
    // Matrix header
    size += 3 * sizeof(int);
    size += sdm_model_[i].total() * sdm_model_[i].elemSize();
  }
  return size;
}

#pragma mark -
#pragma mark Tracking

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set
 *          of landmarks.
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int MStreamSdmTracker::Detect(const cv::Mat& image, cv::Mat* shape) {
  return this->Detect(image, 0, shape);
}

/*
 *  @name   Detect
 *  @fn int Detect(const std::vector<cv::Mat>&images,
                   std::vector<cv::Mat>* shapes)
 *  @brief  Dectection method, given a multiples image, provides a set of
 *          landmarks for each image. This must be override by subclass
 *  @param[in]  images   List of input images
 *  @param[out] shapes   List of set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::Detect(const std::vector<cv::Mat>& images,
                              std::vector<cv::Mat>* shapes) {
  assert(images.size() == n_stream_);
  shapes->resize(static_cast<size_t>(n_stream_));
  // Loop over all image stream
  int err = 0;
  for (int i = 0; i < n_stream_; ++i) {
    err |= this->Detect(images[i], i, &((*shapes)[i]));
  }
  return err;
}

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image,
                   const cv::Rect& face_region,
                   cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks.
 *  @param[in]  image           Input image
 *  @param[in]  face_region     Face boundingg box
 *  @param[out] shape           Set of landmarks
 *  @return     -1 if error, 0 otherwise
 */
int MStreamSdmTracker::Detect(const cv::Mat& image,
                              const cv::Rect& face_region,
                              cv::Mat* shape) {
  return this->Detect(image, face_region, 0, shape);
}

/*
 *  @name   Detect
 *  @fn int Detect(const std::vector<cv::Mat>& images,
                   const std::vector<cv::Rect>& face_regions,
                   std::vector<cv::Mat>* shapes)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks. This must be override by subclass
 *  @param[in]  images           List of input image
 *  @param[in]  face_regions     List of face boundingg box
 *  @param[out] shapes           List of set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int MStreamSdmTracker::Detect(const std::vector<cv::Mat>& images,
                              const std::vector<cv::Rect>& face_regions,
                              std::vector<cv::Mat>* shapes) {
  assert((images.size() == n_stream_) && face_regions.size() == n_stream_);
  shapes->resize(static_cast<std::size_t >(n_stream_));
  // Loop over all image stream
  int err = 0;
  for (int i = 0; i < n_stream_; ++i) {
    err |= this->Detect(images[i], face_regions[i], i, &((*shapes)[i]));
  }
  return err;
}

/*
 *  @name   Track
 *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Tracking method, given an input image
 *  @param[in]      image   Input image
 *  @param[in,out]  shape   Set of landmarks
 *  @return         -1 if error, 0 otherwise
 */
int MStreamSdmTracker::Track(const cv::Mat& image, cv::Mat* shape) {
  return this->Track(image, 0, shape);
}

/*
 *  @name   Track
 *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Tracking method, given an input image (from a sequence), provides
 *          a set of landmarks. This must be override by subclass
 *  @param[in]  images   List of input image
 *  @param[out] shapes   List of set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int MStreamSdmTracker::Track(const std::vector<cv::Mat>& images,
                             std::vector<cv::Mat>* shapes) {
  assert(images.size() == n_stream_);
  shapes->resize(static_cast<size_t>(n_stream_));
  int err = 0;
  for (int i = 0; i < n_stream_; ++i) {
    err |= this->Track(images[i], static_cast<int>(i), &((*shapes)[i]));
  }
  return err;
}

#pragma mark -
#pragma mark Training

/*
 *  @name   Train
 *  @fn void Train(const std::string& folder)
 *  @brief  SDM training method
 *  @param[in]  folder  Location of images/annotations
 */
void MStreamSdmTracker::Train(const std::string& folder) {}

#pragma mark -
#pragma mark Accessors

/**
 *  @name   get_sift_features
 *  @fn cv::Mat get_sift_features(const cv::Mat& image, const cv::Mat& shape) const
 *  @brief  Provide feature extracted by given shape
 *  @param[in]  image   input image
 *  @param[in]  shape   input shape
 *  @return Matrix of SIFT features
 */
cv::Mat MStreamSdmTracker::get_sift_features(const cv::Mat& image,
                                             const cv::Mat& shape) const {
  if (image.empty() || shape.empty()) {
    return cv::Mat();
  }
  cv::Mat sift_features;
  int edge = static_cast<int>(tracker_parameters_.face_region_width);
  cv::Mat normalized_shape = shape.clone();
  cv::Mat reference_shape = sdm_meanshape_ * edge + edge/2;
  cv::Mat normalized_image, dummy, transform;
  NormalizeScaleAndRotation(reference_shape, edge, image,
                            &normalized_image, &normalized_shape,
                            &dummy, &transform);
  std::vector<cv::KeyPoint> sift_extraction_location;
  int num_points = shape.rows/2;
  double x = 0.0;
  double y = 0.0;
  for (int p = 0; p < num_points; p++) {
    // only inner face landmarks and without 60 and 64 (inside lip cornner)
    if (p < 17 || p == 60 || p == 64) {
      continue;
    }
    x = normalized_shape.at<double>(p, 0);
    y = normalized_shape.at<double>(p + num_points, 0);
    cv::KeyPoint kp(cvRound(x), cvRound(y), tracker_parameters_.sift_size);
    sift_extraction_location.push_back(kp);
  }

  SSift::ComputeDescriptorOpt(normalized_image, sift_extraction_location,
                              sift_desciptor_params_,
                              &sift_features);
  sift_features.convertTo(sift_features, CV_64FC1);
  sift_features = sift_features.reshape(sift_features.channels(), 1);
  return sift_features;
}

#pragma mark -
#pragma mark Private

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image,const int n_stream, cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set
 *          of landmarks.
 *  @param[in]  image     Input image
 *  @param[in]  n_stream  Current stream index
 *  @param[out] shape     Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int MStreamSdmTracker::Detect(const cv::Mat& image,
                              const int n_stream,
                              cv::Mat* shape) {
  int err = 0;
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_[n_stream], cv::COLOR_BGR2GRAY);
  } else {
    working_img_[n_stream] = image;
  }
  // Downsample image to speedup computation
  float aspect_ratio = (static_cast<float>(working_img_[n_stream].rows) /
                        static_cast<float>(working_img_[n_stream].cols));
  int img_height = static_cast<int>(320.f * aspect_ratio);
  cv::Mat downsample_input;
  cv::resize(working_img_[n_stream], downsample_input, cv::Size(320, img_height));
  float scale = (static_cast<float>(working_img_[n_stream].cols) /
                 static_cast<float>(downsample_input.cols));
  std::vector<cv::Rect> face_location;
  // Lock
  face_detector_->detectMultiScale(downsample_input, face_location,
                                   1.1,
                                   2,
                                   0 /*cv::CASCADE_SCALE_IMAGE*/,
                                   cv::Size(static_cast<int>(200.f/scale),
                                            static_cast<int>(200.f/scale)));
  if (face_location.size() > 0) {
    // Detector has return something
    // Do we have already have a good fit ?
    if ((shape_center_gravity_[n_stream].x < 0) &&
        (shape_center_gravity_[n_stream].y < 0)){
      // No previous good fit, use largest rectangle
      std::sort(face_location.begin(),
                face_location.end(),
                SdmTracker::SortRectBySize());
      face_region_[n_stream] = face_location[0];
    } else {
      //  Use face rect closest to the previous one
      float min_dist = std::numeric_limits<float>::max();
      cv::Point2f current_center;
      cv::Point2f face_displacement;
      float norm_face_displacement;
      // Get through the list
      for (cv::Rect rect : face_location) {
        current_center = (rect.br() + rect.tl()) * 0.5f * scale;
        face_displacement = shape_center_gravity_[n_stream] - current_center;
        norm_face_displacement = std::sqrt(face_displacement.x *
                                           face_displacement.x +
                                           face_displacement.y *
                                           face_displacement.y);
        // Closest ?
        if (norm_face_displacement < min_dist) {
          min_dist = norm_face_displacement;
          face_region_[n_stream] = rect;
        }
      }
    }
    // Convert back to original image coordinate
    face_region_[n_stream].x = static_cast<int>(face_region_[n_stream].x * scale);
    face_region_[n_stream].y = static_cast<int>(face_region_[n_stream].y * scale);
    face_region_[n_stream].width = static_cast<int>(face_region_[n_stream].width * scale);
    face_region_[n_stream].height = static_cast<int>(face_region_[n_stream].height * scale);
    // Can track landmarks, create inital shape. If size is Ok does nothing
    initial_shape_[n_stream].create(num_points_ * 2, 1, CV_64F);
    previous_shape_[n_stream].create(num_points_ * 2, 1, CV_64F);
    // Compute initial shape
    this->ComputeInitialShape(sdm_meanshape_,
                              face_region_[n_stream],
                              &initial_shape_[n_stream]);
    //  Face alignment
    if (tracker_parameters_.align_with_rotation) {
      *shape = this->AlignFaceWithScaleAndRotation(working_img_[n_stream],
                                                   n_stream);
    } else {
      *shape = this->AlignFaceWithScale(working_img_[n_stream], n_stream);
    }
    // Save shape as previous
    previous_shape_[n_stream] = shape->clone();
    // Done
  } else {
    err |= (0x01<<n_stream);
  }
  return err;
}

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image,
                   const cv::Rect& face_region,
                   const int n_stream,
                   cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks.
 *  @param[in]  image           Input image
 *  @param[in]  face_region     Face boundingg box
 *  @param[in]  n_stream        Current stream index
 *  @param[out] shape           Set of landmarks
 *  @return     >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::Detect(const cv::Mat& image,
                              const cv::Rect& face_region,
                              const int n_stream,
                              cv::Mat* shape) {
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_[n_stream], cv::COLOR_BGR2GRAY);
  } else {
    working_img_[n_stream] = image;
  }
  // Can track landmarks, create inital shape. If size is Ok does nothing
  initial_shape_[n_stream].create(num_points_ * 2, 1, CV_64F);
  previous_shape_[n_stream].create(num_points_ * 2, 1, CV_64F);
  // Align init shape with bbox
  this->ComputeInitialShape(sdm_meanshape_,
                            face_region,
                            &initial_shape_[n_stream]);
  //  Face alignment
  if (tracker_parameters_.align_with_rotation) {
    *shape = this->AlignFaceWithScaleAndRotation(working_img_[n_stream],
                                                 n_stream);
  } else {
    *shape = this->AlignFaceWithScale(working_img_[n_stream], n_stream);
  }
  // Save shape as previous, deep copy
  previous_shape_[n_stream] = shape->clone();
  // Done
  return 0;
}

/**
 *  @name   Track
 *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Tracking method, given an input image
 *  @param[in]      image   Input image
 *  @param[in,out]  shape   Set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::Track(const cv::Mat& image,
                             const int n_stream,
                             cv::Mat* shape) {
  int err = 0;
  // Convert to gray scale first
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_[n_stream], cv::COLOR_BGR2GRAY);
  } else {
    working_img_[n_stream] = image;
  }
  // Track
  if (!is_tracking_[n_stream]) {
    // Call for detection
    err = this->Detect(working_img_[n_stream], n_stream, shape);
    is_tracking_[n_stream] = true;
  } else {
    // Tracking
    //previous_shape_ = *shape;
    initial_shape_[n_stream] = this->AlignMeanshapeToCurrent(previous_shape_[n_stream]);
    *shape = this->AlignFaceWithScaleAndRotation(working_img_[n_stream],
                                                 n_stream);
    err &= (0x00<<n_stream);
  }
  // Still tracking ?
  //inner_sift_features_[n_stream] = this->get_sift_features(image, *shape);
  is_tracking_[n_stream] = tracking_assessment_->Assess(working_img_[n_stream],
                                                        *shape) > 0.0;
  if (!is_tracking_[n_stream]) {
    *shape = cv::Mat();
  } else {
    // Save shape
    previous_shape_[n_stream] = shape->clone();
    // Compute shape center of gravity
    shape_center_gravity_[n_stream].x = 0;
    shape_center_gravity_[n_stream].y = 0;
    for (int i = 0; i < num_points_; ++i) {
      shape_center_gravity_[n_stream].x += shape->at<double>(i);
      shape_center_gravity_[n_stream].y += shape->at<double>(i + num_points_);
    }
    shape_center_gravity_[n_stream].x /= num_points_;
    shape_center_gravity_[n_stream].y /= num_points_;
  }
  return err;
}

/*
 *  @name   ComputeInitialShape
 *  @fn void ComputeInitialShape(const cv::Mat& meanshape,
                                 const cv::Rect& bounding_box,
                                 cv::Mat* initial_shape)
 *  @brief  Aligned the meanshape inside the bounding box
 *  @param[in]  meanshape       Model meanshape
 *  @param[in]  bounding_box    Region of interest
 *  @param[out] initial_shape   Initial shape
 */
void MStreamSdmTracker::ComputeInitialShape(const cv::Mat& meanshape,
                                            const cv::Rect& bounding_box,
                                            cv::Mat* initial_shape) {
  // Goes through all points
  for (int p = 0 ; p < num_points_ ; ++p) {
    initial_shape->at<double>(p) = bounding_box.x + meanshape.at<double>(p) *
                                                    bounding_box.width;
    initial_shape->at<double>(p + num_points_) = bounding_box.y +
                                                 meanshape.at<double>(p + num_points_) *
                                                 bounding_box.height;
  }
}

/*
 *  @name   AlignFaceWithScale
 *  @fn cv::Mat AlignFaceWithScale(const cv::Mat& image, const int n_stream)
 *  @brief  Face alignement with SDM model
 *  @param[in]  image     Input image
 *  @param[in]  n_stream  Current stream
 *  @return Return the new shape
 */
cv::Mat MStreamSdmTracker::AlignFaceWithScale(const cv::Mat& image,
                                              const int n_stream) {
  cv::Mat new_shape = initial_shape_[n_stream].clone();
  cv::Mat reference_shape;
  cv::Mat normalized_image;
  cv::Mat inverse_transform;
  cv::Mat dummy;
  const int number_pass = this->tracker_parameters_.number_of_pass;
  const int starting_pass = tracker_parameters_.starting_pass;
  for (int pass = starting_pass; pass < number_pass; ++pass) {
    // Goes through each stage
    std::vector<cv::Mat>::const_iterator sdm_model_it = sdm_model_.begin();
    for (int stage = 0;
         stage < tracker_parameters_.number_of_stage;
         ++stage, ++sdm_model_it) {
      // Coarse to fine approach ?
      int edge = static_cast<int>(tracker_parameters_.face_region_width);
      if (tracker_parameters_.coarse_to_fine_search &&
          stage < tracker_parameters_.number_of_coarse_stages) {
        edge /= 2;
      }
      // Normalize face
      cv::Mat normalized_shape = new_shape.clone();
      reference_shape = sdm_meanshape_ * edge + edge/2;
      NormalizeScale(reference_shape, edge, image,
                     &normalized_image,
                     &normalized_shape,
                     &dummy,
                     &inverse_transform);
      // Sift extraction + update key points
      double x = 0.0;
      double y = 0.0;
      auto key_point_it = sift_extraction_location_[n_stream].begin();
      for (int p = 0; key_point_it != sift_extraction_location_[n_stream].end();
           ++key_point_it, ++p) {
        x = normalized_shape.at<double>(p);
        y = normalized_shape.at<double>(p + num_points_);
        key_point_it->pt.x = cvRound(x);
        key_point_it->pt.y = cvRound(y);
        key_point_it->size = tracker_parameters_.sift_size;
      }
      SSift::ComputeDescriptorOpt(normalized_image,
                                  sift_extraction_location_[n_stream],
                                  sift_desciptor_params_,
                                  &sift_features_[n_stream]);
      sift_features_[n_stream].convertTo(sift_features_[n_stream], CV_64FC1);
      sift_features_[n_stream] = sift_features_[n_stream].reshape(sift_features_[n_stream].channels(),
                                                                  1/*row*/);
      // Calculate + apply shape update
      cv::Mat sift_ext = cv::Mat::ones(sift_features_[n_stream].rows,
                                       sift_features_[n_stream].cols + 1,
                                       sift_features_[n_stream].type());
      sift_features_[n_stream].copyTo(sift_ext.colRange(0, sift_features_[n_stream].cols));
      /* cblas version */
      using LA = LinearAlgebra<double>;
      using TType = typename LA::TransposeType;
      LA::Gemv(*sdm_model_it,
               TType::kTranspose,
               1.0,
               sift_ext,
               1.0,
               &normalized_shape);
      /*cblas_dgemv(CBLAS_ORDER::CblasRowMajor, CBLAS_TRANSPOSE::CblasTrans,
                  (const int)sdm_model_it->rows,
                  (const int)sdm_model_it->cols,
                  1.0,
                  reinterpret_cast<double*>(sdm_model_it->data),
                  (const int)sdm_model_it->cols,
                  (const double*)sift_ext.data,
                  1, 1.0,
                  reinterpret_cast<double*>(normalized_shape.data),
                  1);*/

      //  Back-project shape update to original coordinates
      new_shape = LTS5::TransformShape(inverse_transform, normalized_shape);
    }
    //  Normalize init shape
    if (pass != number_pass - 1) {
      //  Normalize rotation only if angle > 10°
      cv::Vec4d similarity_transform;
      ComputeSimilarityTransform(new_shape,
                                 initial_shape_[n_stream],
                                 &similarity_transform);
      double angle = atan2(similarity_transform[1],
                           similarity_transform[0]) * 180 / CV_PI;
      if (std::abs(angle) > 10) {
        //  Normalize scale & rotation
        cv::Mat new_init_shape = initial_shape_[n_stream].clone();
        new_shape = new_init_shape;
      } else {
        //  Normalize only scale
        double scale = 1.0 / sqrt((similarity_transform[0] *
                                   similarity_transform[0]) +
                                  (similarity_transform[1] *
                                   similarity_transform[1]));
        cv::Point2d image_center(image.cols / 2.0, image.rows / 2.0);
        for (int p = 0; p < num_points_; p++) {
          new_shape.at<double>(p, 0) = (initial_shape_[n_stream].at<double>(p, 0) -
                                        image_center.x) * scale +
                                       image_center.x;
          new_shape.at<double>(p + num_points_, 0) =
                  (initial_shape_[n_stream].at<double>(p + num_points_, 0) -
                   image_center.y) * scale + image_center.y;
        }
      }
    }
  }
  return new_shape;
}

/*
 *  @name   AlignFaceWithScaleAndRotation
 *  @fn cv::Mat AlignFaceWithScaleAndRotation(const cv::Mat& image,
                                              const int n_stream)
 *  @brief  Face alignement with SDM model
 *  @param[in]  image     Input image
 *  @param[in]  n_stream  Current stream
 *  @return Return the new shape
 */
cv::Mat MStreamSdmTracker::AlignFaceWithScaleAndRotation(const cv::Mat& image,
                                                         const int n_stream) {
  cv::Mat new_shape = initial_shape_[n_stream].clone();
  cv::Mat reference_shape;
  cv::Mat normalized_image;
  cv::Mat transform;
  cv::Mat dummy;
  const int number_pass = this->tracker_parameters_.number_of_pass;
  const int starting_pass = tracker_parameters_.starting_pass;
  // Goes through each stage
  std::vector<cv::Mat>::const_iterator sdm_model_it = sdm_model_.begin();
  for (int stage = 0;
       stage < tracker_parameters_.number_of_stage;
       stage++, ++sdm_model_it) {
    int edge = static_cast<int>(tracker_parameters_.face_region_width);
    if (tracker_parameters_.coarse_to_fine_search &&
        stage < tracker_parameters_.number_of_coarse_stages) {
      edge /= 2;
    }
    for (int pass = starting_pass; pass < number_pass; pass++) {
      //  Normalize face
      cv::Mat normalized_shape = new_shape.clone();
      reference_shape = sdm_meanshape_ * edge + edge/2;
      NormalizeScaleAndRotation(reference_shape, edge, image,
                                &normalized_image, &normalized_shape,
                                &dummy, &transform);
      // Sift extraction + update key points
      double x = 0.0;
      double y = 0.0;
      auto key_point_it = sift_extraction_location_[n_stream].begin();
      for (int p = 0;
           key_point_it != sift_extraction_location_[n_stream].end();
           ++key_point_it, ++p) {
        x = normalized_shape.at<double>(p);
        y = normalized_shape.at<double>(p + num_points_);
        key_point_it->pt.x = cvRound(x);
        key_point_it->pt.y = cvRound(y);
        key_point_it->size = tracker_parameters_.sift_size;
      }
      SSift::ComputeDescriptorOpt(normalized_image,
                                  sift_extraction_location_[n_stream],
                                  sift_desciptor_params_,
                                  &sift_features_[n_stream]);
      sift_features_[n_stream].convertTo(sift_features_[n_stream], CV_64FC1);
      sift_features_[n_stream] = sift_features_[n_stream].reshape(sift_features_[n_stream].channels(), 1);
      //  Calculate + apply shape update
      cv::Mat sift_ext = cv::Mat::ones(sift_features_[n_stream].rows,
                                       sift_features_[n_stream].cols + 1,
                                       sift_features_[n_stream].type());
      sift_features_[n_stream].copyTo(sift_ext.colRange(0,
                                                        sift_features_[n_stream].cols));
      /* cblas version */
      using LA = LinearAlgebra<double>;
      using TType = typename LA::TransposeType;
      LA::Gemv(*sdm_model_it,
               TType::kTranspose,
               1.0,
               sift_ext,
               1.0,
               &normalized_shape);
      
      /*cblas_dgemv(CBLAS_ORDER::CblasRowMajor, CBLAS_TRANSPOSE::CblasTrans,
                  sdm_model_it->rows,
                  sdm_model_it->cols,
                  1.0,
                  reinterpret_cast<double*>(sdm_model_it->data),
                  sdm_model_it->cols,
                  (const double*)sift_ext.data,
                  1, 1.0,
                  reinterpret_cast<double*>(normalized_shape.data),
                  1);*/
      if (pass == 0) {
        //  Back-project shape to original coordinates
        cv::Mat temp_shape = LTS5::TransformShape(transform,
                                                  normalized_shape);
        //  Normalize pose
        cv::Vec<double, 4> sim_transfo;
        ComputeSimilarityTransform(new_shape, temp_shape, &sim_transfo);

        cv::Mat transform_t = (cv::Mat_<double>(2, 3) << sim_transfo[0],
                -sim_transfo[1], sim_transfo[2],
                sim_transfo[1], sim_transfo[0],
                sim_transfo[3]);
        new_shape = LTS5::TransformShape(transform_t, new_shape);
      } else {
        //  Back-project shape to original coordinates
        new_shape = LTS5::TransformShape(transform, normalized_shape);
      }
    }
  }
  return new_shape;
}

/*
 *  @name AlignMeanshapeToCurrent
 *  @fn cv::Mat AlignMeanshapeToCurrent(const cv::Mat& current_shape)
 *  @brief  Align the meanshape on the shape recovered at previous
 *          frame
 *  @param[in]  current_shape   Shape revovered at previous frame
 *  @return Aligned shape
 */
cv::Mat MStreamSdmTracker::AlignMeanshapeToCurrent(const cv::Mat& current_shape) {
  // Compute similarity
  cv::Vec4d similarity_transform;
  ComputeSimilarityTransform(sdm_meanshape_,
                             current_shape,
                             &similarity_transform);
  cv::Mat transform = (cv::Mat_<double>(2, 3) << similarity_transform[0],
          -similarity_transform[1], similarity_transform[2],
          similarity_transform[1], similarity_transform[0],
          similarity_transform[3]);
  // Return the transform shape
  return LTS5::TransformShape(transform, sdm_meanshape_);
}

/*
 * @name  ~MSDMProxy
 * @fn    ~MSDMProxy(void) override
 * @brief Destructor
 */
MSDMProxy::~MSDMProxy(void) {}

/*
 * @name  Create
 * @fn    MStreamFaceTracker<cv::Mat, cv::Mat>* Create(const int N) const override
 * @brief Create an instance of MStreamFaceTracker
 * @param[in] N number of stream
 * @return    Tracker instance with proper type
 */
MStreamFaceTracker<cv::Mat, cv::Mat>* MSDMProxy::Create(const int N) const {
  return new MStreamSdmTracker(N);
};

/**
 *  @name Name
 *  @fn const char* Name(void) const override
 *  @brief  Return the name for a given type of tracker
 *  @return Tracker name (i.e. sdm, lbf, ...)
 */
const char* MSDMProxy::Name(void) const {
  return "multi_sdm";
}

/** Explicit registration */
MSDMProxy msdm_proxy;

}  // namepsace LTS5
