/**
 *  @file   cpr_tracker.cpp
 *  @brief  CPR tracker class implementation
 *
 *  @author Christophe Ecabert
 *  @date   09/11/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <chrono>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/face_tracker/cpr_tracker.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/face_detector/cascade_face_detector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

  using Vec2 = LTS5::Vector2<double>;

#pragma mark -
#pragma mark  Initialization

/*
 *  @name   CPRTracker
 *  @fn CPRTracker(void)
 *  @brief  Constructor
 */
CPRTracker::CPRTracker(void) :
  is_tracking_(false),
  cascaded_fern_(0),
  f_detector_(nullptr),
  own_detector_(false),
  shape_center_gravity_(-1, -1),
  n_initialization_(5) {
}

/*
 *  @name   ~CPRTracker
 *  @fn ~CPRTracker(void)
 *  @brief  Destructor
 */
CPRTracker::~CPRTracker(void) {
  // Clear cascades
  for (int i = 0; i < cascaded_fern_.size(); ++i) {
    if (cascaded_fern_[i]) {
      delete cascaded_fern_[i];
    }
  }
  // Clear face detector
  if (f_detector_ != nullptr && own_detector_) {
    delete f_detector_;
  }
}

/*
 *  @name   Load
 *  @fn int Load(const std::string& model_filename)
 *  @brief  Load the tracker model.
 *  @param[in]  model_filename      Path to the model
 *  @return -1 if errors, 0 otherwise
 */
int CPRTracker::Load(const std::string& model_filename) {
  int error = -1;
  // Open stream
  std::ifstream input_stream(model_filename, std::ios_base::binary);
  if (input_stream.is_open()) {
    error = LTS5::ScanStreamForObject(input_stream,
                                      HeaderObjectType::kCPRTracker);
    if (!error) {
      error = this->Load(input_stream);
      input_stream.close();
    }
  }
  return error;
}

/*
 *  @name   Load
 *  @fn int Load(std::istream& model_filename)
 *  @brief  Load the tracker model.
 *  @param[in]  input_stream      Stream from where to load the object
 *  @return -1 if errors, 0 otherwise
 */
int CPRTracker::Load(std::istream& input_stream) {
  int error = -1;
  if (input_stream.good()) {
    // Load meanshape
    error = LTS5::ReadMatFromBin(input_stream, &mean_shape_);
    // Load inital shape
    error |= LTS5::ReadMatFromBin(input_stream, &initial_shape_);
    n_points_ = mean_shape_.cols / 2;
    // Init distribution
    uni_dist_ = std::uniform_int_distribution<int>(0, initial_shape_.rows - 1);
    // Read number of cascade
    int n_cascade = -1;
    input_stream.read(reinterpret_cast<char*>(&n_cascade), sizeof(n_cascade));
    cascaded_fern_.clear();
    for (int i = 0; i < n_cascade; ++i) {
      error |= LTS5::ScanStreamForObject(input_stream,
                                         HeaderObjectType::kCascadedRandomFern);
      cascaded_fern_.push_back(new LTS5::CascadedRandomFern());
      error |= cascaded_fern_[i]->Load(input_stream);
      cascaded_fern_[i]->set_mean_shape(mean_shape_);
    }
  }
  return error;
}

/*
 *  @name   Load
 *  @fn int Load(const std::string& model_filename,
                 const std::string& f_detector_config)
 *  @brief  Load the tracker model.
 *  @param[in]  model_filename      Path to the model
 *  @param[in]  f_detector_config   Path to the face detector configuration
 *  @return -1 if errors, 0 otherwise
 */
int CPRTracker::Load(const std::string& model_filename,
                     const std::string& f_detector_config) {
  // Load tracker
  int error = this->Load(model_filename);
  // Load face detector
  if (!error) {
    LTS5::CascadeFaceDetector::CascadeDetectorParameters param;
    param.min_neighbors = 3;
    param.min_size = cv::Size(20, 20);
    param.max_size = cv::Size(0, 0);
    f_detector_ = new LTS5::CascadeFaceDetector(f_detector_config);
    own_detector_ = true;
    error = f_detector_->empty() ? -1 : 0;
    f_detector_->set_parameters(param);
  }
  return error;
}

/*
 *  @name   Load
 *  @fn int Load(std::istream& input_stream,
                 const std::string& f_detector_config)
 *  @brief  Load the tracker model.
 *  @param[in]  input_stream      Stream from where to load model
 *  @param[in]  f_detector_config Path to the face detector configuration
 *  @return -1 if errors, 0 otherwise
 */
int CPRTracker::Load(std::istream& input_stream,
                     const std::string& f_detector_config) {
  // Move to CPR
  int error = LTS5::ScanStreamForObject(input_stream,
                                        HeaderObjectType::kCPRTracker);
  if (!error) {
    // Load tracker
    error = this->Load(input_stream);
    // Load face detector
    f_detector_ = new LTS5::CascadeFaceDetector(f_detector_config);
    error |= f_detector_->empty() ? -1 : 0;
    own_detector_ = true;
  }
  return error;
}

/*
 *  @name   Save
 *  @fn int Save(const std::string& model_name)
 *  @brief  Save the trained model in file.
 *  @param[in] model_name   Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int CPRTracker::Save(const std::string& model_name) {
  return -1;
}

/*
 *  @name   Write
 *  @fn int Write(std::ostream& out_stream)
 *  @brief  Write the trained model into a given stream.
 *  @param[in] out_stream   Stream where to write the object
 *  @return -1 in case of error, otherwise 0
 */
int CPRTracker::Write(std::ostream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Compute obj header
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kCPRTracker);
    int obj_size = this->ComputeObjectSize();
    // Write
    out_stream.write(reinterpret_cast<char*>(&obj_type), sizeof(obj_type));
    out_stream.write(reinterpret_cast<char*>(&obj_size), sizeof(obj_size));
    error = out_stream.good() ? 0 : -1;
    // Mean_shape
    error |= LTS5::WriteMatToBin(out_stream, mean_shape_);
    // Write ground truth
    error |= LTS5::WriteMatToBin(out_stream, initial_shape_);
    // Write number of cascade
    const int n_cascade = static_cast<int>(cascaded_fern_.size());
    out_stream.write(reinterpret_cast<const char*>(&n_cascade), sizeof(n_cascade));
    for (int p = 0; p < n_cascade; ++p) {
      // Write cascade
      error |= cascaded_fern_[p]->Write(out_stream);
    }
  }
  return error;
}

/*
 *  @name   ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the memory used by object
 *  @return Size in bytes
 */
int CPRTracker::ComputeObjectSize(void) const {
  int size = 0;
  // Centered ground truth, header + data
  size = static_cast<int>(initial_shape_.total() *
                          initial_shape_.elemSize());
  size += 3 * sizeof(int);
  // Mean shape
  size += mean_shape_.total() * mean_shape_.elemSize();
  size += 3 * sizeof(int);
  // Add number of cascade
  int n_cascade = static_cast<int>(cascaded_fern_.size());
  size += sizeof(n_cascade);
  // Add cascade
  for (int i = 0; i < n_cascade; ++i) {
    size += 2 * sizeof(int);
    size += cascaded_fern_[i]->ComputeObjectSize();
  }
  return size;
}

#pragma mark -
#pragma mark Process

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks.
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int CPRTracker::Detect(const cv::Mat& image, cv::Mat* shape) {
  int error = -1;
  // Convert image if needed
  if (image.channels() != 1) {
    cv::cvtColor(image, working_image_, cv::COLOR_BGR2GRAY);
  } else {
    working_image_ = image;
  }
  // Down sample to speed
  std::vector<cv::Rect> face_location;
  f_detector_->Detect(working_image_, &face_location);
  if (face_location.size() > 0) {
    // Do we have already have a good fit ?
    if (shape_center_gravity_.x < 0 && shape_center_gravity_.y < 0) {
      // No previous good fit, use largest rectangle
      std::sort(face_location.begin(),
                face_location.end(),
                [](const cv::Rect& a, const cv::Rect& b)-> bool{
                  return a.area() > b.area();
                });
      face_region_ = face_location[0];
      error = 0;
    } else {
      //  Use face rect closest to the previous one
      double min_dist = std::numeric_limits<double>::max();
      cv::Point2d current_center;
      cv::Point2d face_displacement;
      double norm_face_displacement = -1.f;
      // Loop over all bounding box
      for (cv::Rect& rect : face_location) {
        current_center = (rect.br() + rect.tl()) * 0.5;
        face_displacement = shape_center_gravity_ - current_center;
        norm_face_displacement = (face_displacement.x *
                                  face_displacement.x +
                                  face_displacement.y *
                                  face_displacement.y);
        // Closest ?
        if (norm_face_displacement < min_dist) {
          min_dist = norm_face_displacement;
          face_region_ = rect;
          error = 0;
        }
      }
    }
    // shape
    this->AlignShape(shape);
  }
  return error;
}

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image,
                   const cv::Rect& face_region,
                   cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks.
 *  @param[in]  image           Input image
 *  @param[in]  face_region     Face boundingg box
 *  @param[out] shape           Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int CPRTracker::Detect(const cv::Mat& image,
                       const cv::Rect& face_region,
                       cv::Mat* shape) {
  int error = -1;
  if (image.channels() != 1) {
    cv::cvtColor(image, working_image_, cv::COLOR_BGR2GRAY);
  } else {
    working_image_ = image;
  }
  // Call alignment
  face_region_ = face_region;
  this->AlignShape(shape);
  // Done
  error = 0;
  return error;
}

/*
 *  @name   Track
 *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Tracking method, given an input image (from a sequence), provides
 *          a set of landmarks.
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int CPRTracker::Track(const cv::Mat& image, cv::Mat* shape) {
  int error = -1;
  if (!is_tracking_) {
    is_tracking_ = (this->Detect(image, shape) == 0);
  } else {
    // TODO: Shift face_bbox with shape ...



  }
  return error;
}

/*
 *  @name   Train
 *  @fn void Train(const std::string& folder)
 *  @brief  Tracker training method. This must be override by subclass
 *  @param[in]  folder  Location of images/annotations
 */
void CPRTracker::Train(const std::string& folder) {
  std::cout << "Not implemented in this class" << std::endl;
}

/*
 *  @name AlignShape
 *  @fn void AlignShape(cv::Mat* shape)
 *  @brief  Align shape
 *  @param[out] shape Aligned shape
 */
void CPRTracker::AlignShape(cv::Mat* shape) {

  // Do N initialization
  cv::Mat current_shape;
  cv::Mat center_shape;
  cv::Mat align_shape = cv::Mat(n_initialization_, n_points_ * 2, CV_64FC1);
  shape->create(n_points_ * 2, 1, CV_64FC1);
  for (int i = 0; i < n_initialization_; ++i) {
    // Select starting randomly from training set
    std::mt19937_64 gen(static_cast<unsigned>(std::
                                                         chrono::
                                                         system_clock::now()
                                                         .time_since_epoch()
                                                         .count()));
    int idx = uni_dist_(gen);
    initial_shape_.row(idx).copyTo(current_shape);
    // Place shape at correct location
    LTS5::PlaceShapeXY<double>(current_shape, face_region_, &current_shape);
    // Pass through all stage of regression
    for (int f = 0; f < cascaded_fern_.size(); ++f) {
      cv::Mat shape_update = cv::Mat::zeros(1, 2 * n_points_, CV_64FC1);
      cascaded_fern_[f]->Predict(working_image_,
                                 face_region_,
                                 current_shape,
                                 &shape_update);
      // Center current shape
      LTS5::NormalizeShapeXY<double>(current_shape, face_region_, &center_shape);
      // Update center shape
      center_shape = center_shape + shape_update;
      // Place back at location
      LTS5::PlaceShapeXY<double>(center_shape, face_region_, &current_shape);
    }
    // Average all prediction
    // TODO: (Christophe) Test with median
    current_shape.copyTo(align_shape.row(i));
  }

  // Take median value
  std::vector<double> value(n_initialization_, -1.0);
  for (int i = 0; i < current_shape.cols; ++i) {
    for (int j = 0; j < n_initialization_; ++j) {
      value[j] = align_shape.at<double>(j, i);
    }
    auto first = value.begin();
    auto last = value.end();
    auto mid = first + (last - first)/2;
    std::nth_element(first, mid, last);
    shape->at<double>(i) = *mid;
  }
  //*shape = *shape / n_initialization_;
}

#pragma mark -
#pragma mark Utility

/*
 *  @name DrawShape
 *  @fn static cv::Mat DrawShape(const cv::Mat& image, const cv::Mat& shape)
 *  @brief  Draw shape on image
 *  @param[in]  image   Image to draw on
 *  @param[in]  shape   Shape to draw
 *  @return Canvas with image and shape on it
 */
cv::Mat CPRTracker::DrawShape(const cv::Mat& image, const cv::Mat& shape) {
  cv::Mat canvas;
  if (image.channels() != 3 && image.channels() != 4) {
    cv::cvtColor(image, canvas, cv::COLOR_GRAY2BGR);
  } else {
    image.copyTo(canvas);
  }
  const Vec2* shape_ptr = reinterpret_cast<Vec2*>(shape.data);
  int n_pts = std::max(shape.cols, shape.rows) / 2;
  for (int p = 0; p < n_pts; ++p) {
    const Vec2& pts = shape_ptr[p];
    cv::circle(canvas, cv::Point(pts.x_, pts.y_), 1, CV_RGB(0, 255, 0), -1);
  }
  return canvas;
}

}  // namespace LTS5
