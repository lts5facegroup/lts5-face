/**
*  @file   sdm_tracker.cpp
*  @brief  SDM face-tracker implementation
*
*  @author Christophe Ecabert
*  @date   22/04/15
*  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
*/

#include "opencv2/highgui.hpp"

#include <fstream>
#include <limits>
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/image_transforms.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/svm_tracker_assessment.hpp"
#include "lts5/face_tracker/const_tracker_assessment.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/face_detector/cascade_face_detector.hpp"
//#include "lts5/face_detector/partsbased_face_detector.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   SdmTracker
 *  @brief  Constructor
 */
SdmTracker::SdmTracker() : is_tracking_(false),
                               face_detector_(nullptr),
                               tracking_assessment_(nullptr) {
  shape_center_gravity_ = cv::Point2f(-1.f, -1.f);
  face_det_type_ = kCascadeVJ;
}

/*
 *  @name   SdmTracker
 *  @fn SdmTracker(const FaceDetectorType& face_det_type)
 *  @brief  Constructor
 *  @param[in] face_det_type Type of the face det to instantiate (see enum)
 */
SdmTracker::SdmTracker(const FaceDetectorType& face_det_type)
: is_tracking_(false), face_detector_(nullptr),
tracking_assessment_(nullptr) {
  shape_center_gravity_ = cv::Point2f(-1.f, -1.f);
  face_det_type_ = face_det_type;
}

/*
 *  @name   ~SdmTracker
 *  @brief  Destructor
 */
SdmTracker::~SdmTracker() {
  // Clean up
  if (face_detector_) {
    delete face_detector_;
  }
  if (tracking_assessment_ && release_assessor_) {
    delete tracking_assessment_;
  }
}

/*
 *  @name   Load
 *  @brief  Load the SDM model
 *  @param[in]  model_filename      Path to the model
 *  @return -1 if errors, 0 otherwise
 */
int SdmTracker::Load(const std::string& model_filename) {
  // Binary model ?
  int error = -1;
  const bool is_binary = model_filename.find(".bin") != std::string::npos;
  std::ios_base::openmode flag = (is_binary ?
                                  std::ios_base::in|std::ios_base::binary :
                                  std::ios_base::in);
  std::ifstream input_stream(model_filename.c_str(), flag);
  if (input_stream.is_open()) {
    int status = LTS5::ScanStreamForObject(input_stream,
                                           HeaderObjectType::kSdm);
    if (status == 0) {
      error = this->Load(input_stream, is_binary);
    }
  }
  return error;
}

/*
 *  @name   Load
 *  @brief  Load the SDM model
 *  @param[in]  model_stream      Stream to the model
 *  @param[in]  is_binary         Indicate if stream is open as binary
 *  @return -1 if errors, 0 otherwise
 */
int SdmTracker::Load(std::istream& model_stream, const bool& is_binary) {
  // Check if it is a binary stream or not
  int error = -1;
  if (is_binary) {
    // Reader header
    error = this->tracker_parameters_.ReadParameterHeader(model_stream);
    // Bin
    error |= LTS5::ReadMatFromBin(model_stream, &sdm_meanshape_);
    // Define number of tracked points
    num_points_ = sdm_meanshape_.rows/2;
    // Init feature extraction tree, aka keypoints
    sift_extraction_location_.clear();
    sift_extraction_location_.reserve(num_points_);
    for (int p = 0; p < num_points_; ++p) {
      sift_extraction_location_.push_back(cv::KeyPoint());
    }
    // Load sdm
    sdm_model_.clear();
    sdm_model_.reserve(tracker_parameters_.number_of_stage);
    for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
      cv::Mat stage_model;
      error &= LTS5::ReadMatFromBin(model_stream, &stage_model);
      sdm_model_.push_back(stage_model);
      if (error != 0)
        break;
    }
    // Tracker assessment present ?
    HeaderObjectType object_type = static_cast<HeaderObjectType>(0);
    int object_size = 0;
    char dummy = 0;
    bool keep_reading = true;
    while (model_stream.good() && keep_reading) {
      model_stream.read(reinterpret_cast<char*>(&object_type), sizeof(object_type));
      model_stream.read(reinterpret_cast<char*>(&object_size), sizeof(object_size));
      switch (object_type) {
          // Svm
        case LTS5::HeaderObjectType::kSvmTrackerAssessment :
          tracking_assessment_ = new SvmTrackerAssessment(sdm_meanshape_,
                                                          sift_desciptor_params_,
                                                          tracker_parameters_.face_region_width,
                                                          tracker_parameters_.sift_size);
          release_assessor_ = true;
          keep_reading = false;
          break;

          // Const
        case LTS5::HeaderObjectType::kConstTrackerAssessment :
          tracking_assessment_ = new ConstTrackerAssessment(true);
          release_assessor_ = true;
          keep_reading = false;
          break;

        default :  // No supported type -> Fast forward to next object;
          model_stream.read(&dummy, object_size);
          break;
      }
    }
    // Is assessor still not defined ?
    if (tracking_assessment_ == nullptr) {
      // Instantial constant assessor
      tracking_assessment_ = new ConstTrackerAssessment(true);
    }
    // Load assessor
    tracking_assessment_->Load(model_stream, is_binary);
  } else {
    // Char
    error = LTS5::ReadMatFromChar(model_stream, &sdm_meanshape_);
    // Define number of tracked points
    num_points_ = sdm_meanshape_.rows/2;
    // Init feature extraction tree, aka keypoints
    sift_extraction_location_.clear();
    sift_extraction_location_.reserve(num_points_);
    for (int p = 0; p < num_points_; ++p) {
      sift_extraction_location_.push_back(cv::KeyPoint());
    }
    sdm_model_.clear();
    sdm_model_.reserve(tracker_parameters_.number_of_stage);
    for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
      cv::Mat stage_model;
      error &= LTS5::ReadMatFromChar(model_stream, &stage_model);
      sdm_model_.push_back(stage_model);
      if (error != 0)
        break;
    }
    // Create assessor
    tracking_assessment_ = new LTS5::ConstTrackerAssessment(true);
  }
  return error;
}

/*
 *  @name   Load
 *  @brief  Load the SDM model and Viola Jones face detector
 *  @param[in]  model_filename          Path to the model
 *  @param[in]  face_detector_config    Configuration file for Viola & Jones
 *                                      face detector
 *  @return -1 if errors, 0 otherwise
 */
int SdmTracker::Load(const std::string& model_filename,
                     const std::string& face_detector_config) {
  // Load SDM
  int error = this->Load(model_filename);

  if (face_detector_) {
    delete face_detector_;
  }

  switch (face_det_type_) {
    case kCascadeVJ: {
      // Init Viola-Jones detector
      face_detector_ = new CascadeFaceDetector(face_detector_config);
      CascadeFaceDetector::CascadeDetectorParameters params;
      params.downsample_image = true;
      params.scale_factor = 1.1;
      params.min_neighbors = 2;
      params.flags = /*cv::CASCADE_SCALE_IMAGE*/ 0;
      face_detector_->set_parameters(params);
      break;
    }
    /*case kPartsBased: {
      face_detector_ = new PartsBasedFaceDetector(face_detector_config);
      break;
    }*/
  }
  error = !face_detector_->empty() ? error : -1;

  return error;
}

/*
 *  @name   Load
 *  @brief  Load the SDM model and Viola Jones face detector
 *  @param[in]  model_filename          Stream to the model
 *  @param[in]  face_detector_config    Configuration file for
 *                                      Viola & Jones face detector
 *  @return -1 if errors, 0 otherwise
 */
int SdmTracker::Load(std::istream& model_stream,
                     const bool& is_binary,
                     const std::string& face_detector_config) {
  int error = -1;
  // Load SDM
  if (model_stream.good()) {
    error = this->Load(model_stream, is_binary);
  }

  if (face_detector_) {
    delete face_detector_;
  }

  switch (face_det_type_) {
    case kCascadeVJ: {
      // Init Viola-Jones detector
      face_detector_ = new CascadeFaceDetector(face_detector_config);
      CascadeFaceDetector::CascadeDetectorParameters params;
      params.downsample_image = true;
      params.scale_factor = 1.1;
      params.min_neighbors = 2;
      params.flags = /*cv::CASCADE_SCALE_IMAGE*/ 0;
      face_detector_->set_parameters(params);
      break;
    }
    /*case kPartsBased: {
      face_detector_ = new PartsBasedFaceDetector(face_detector_config);
      break;
    }*/
  }
  error |= !face_detector_->empty() ? error : -1;
  return error;
}

/*
 *  @name   Save
 *  @brief  Save the trained model in file. This must be override by subclass
 *  @param  [in]    model_name      Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int SdmTracker::Save(const std::string& model_name) {
  return -1;
}

/*
 *  @name   Write
 *  @brief  Write the object into a binary stream
 *  @param[in]  out_stream  Binary stream to files
 *  @return -1 if error, 0 otherwise.
 */
int SdmTracker::Write(std::ostream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Ok
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kSdm);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type), sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size), sizeof(obj_size));
    // Parameters header
    error = tracker_parameters_.WriteParameterHeader(out_stream);
    // Mean shape
    error |= LTS5::WriteMatToBin(out_stream, sdm_meanshape_);
    // Regressor
    for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
      error |= LTS5::WriteMatToBin(out_stream, sdm_model_[i]);
    }
    // Quality assessment
    error |= tracking_assessment_->Write(out_stream);
    // Done sanity check,
    error |= out_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name   ComputeObjectSize
 *  @brief  Compute the memory used by object
 *  @return Size in bytes
 */
int SdmTracker::ComputeObjectSize() const {
  int size = 0;
  // Configuration header
  size = tracker_parameters_.ComputeHeaderSize();
  // Meanshape
  size += 3 * sizeof(int);
  size += sdm_meanshape_.total() * sdm_meanshape_.elemSize();
  // Regressor
  for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
    // Matrix header
    size += 3 * sizeof(int);
    size += sdm_model_[i].total() * sdm_model_[i].elemSize();
  }
  return size;
}

#pragma mark -
#pragma mark Tracking


/*
 *  @name   Detect
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks. This must be override by subclass
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int SdmTracker::Detect(const cv::Mat& image, cv::Mat* shape) {
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_, cv::COLOR_BGR2GRAY);
  } else {
    working_img_ = image;
  }
  // Scan image for faces
  int error = -1;
  faces_location_.resize(0);
  if (face_detector_) {
    face_detector_->Detect(working_img_, &faces_location_);
  }
  if (!faces_location_.empty()) {
    // Detector has return something
    // Do we have already have a good fit ?
    if (shape_center_gravity_.x < 0 && shape_center_gravity_.y < 0) {
      // No previous good fit, use largest rectangle
      std::sort(faces_location_.begin(),
                faces_location_.end(),
                SdmTracker::SortRectBySize());
      face_region_ = faces_location_[0];
    } else {
      //  Use face rect closest to the previous one
      float min_dist = std::numeric_limits<float>::max();
      cv::Point2f current_center;
      cv::Point2f face_displacement;
      float norm_face_displacement = -1.f;

      // Get through the list
      for (const cv::Rect& rect : faces_location_) {
        current_center = (rect.br() + rect.tl()) * 0.5f;
        face_displacement = shape_center_gravity_ - current_center;
        norm_face_displacement = std::sqrt(face_displacement.x *
                                           face_displacement.x +
                                           face_displacement.y *
                                           face_displacement.y);
        // Closest ?
        if (norm_face_displacement < min_dist) {
          min_dist = norm_face_displacement;
          face_region_ = rect;
        }
      }
    }
    // Can track landmarks, create inital shape. If size is Ok does nothing
    initial_shape_.create(num_points_ * 2, 1, CV_64F);
    previous_shape_.create(num_points_ * 2, 1, CV_64F);
    // Compute initial shape
    this->ComputeInitialShape(sdm_meanshape_, face_region_, &initial_shape_);
    //  Face alignment
    if (tracker_parameters_.align_with_rotation) {
      *shape = this->AlignFaceWithScaleAndRotation(working_img_);
    } else {
      *shape = this->AlignFaceWithScale(working_img_);
    }
    // Save shape as previous
    previous_shape_ = shape->clone();
    // Check alignment quality
    error = tracking_assessment_->Assess(working_img_, *shape) > 0.0 ? 0 : -1;
    if (error) {
      *shape = cv::Mat();
    }
  }
  return error;
}

/*
 *  @name   Detect
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks. This must be override by subclass
 *  @param[in]  image           Input image
 *  @param[in]  face_region     Face boundingg box
 *  @param[out] shape           Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int SdmTracker::Detect(const cv::Mat& image,
                       const cv::Rect& face_region,
                       cv::Mat* shape) {
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_, cv::COLOR_BGR2GRAY);
  } else {
    working_img_ = image;
  }
  // Scan image for faces
  int error = -1;
  // Can track landmarks, create inital shape. If size is Ok does nothing
  initial_shape_.create(num_points_ * 2, 1, CV_64F);
  previous_shape_.create(num_points_ * 2, 1, CV_64F);
  this->ComputeInitialShape(sdm_meanshape_, face_region, &initial_shape_);
  //  Face alignment
  if (tracker_parameters_.align_with_rotation) {
    *shape = this->AlignFaceWithScaleAndRotation(working_img_);
  } else {
    *shape = this->AlignFaceWithScale(working_img_);
  }
  // Save shape as previous, deep copy
  previous_shape_ = shape->clone();
  // Check alignment quality
  error = tracking_assessment_->Assess(working_img_, *shape) > 0.0 ? 0 : -1;
  if (error) {
    *shape = cv::Mat();
  }
  return error;
}

/*
 *  @name   Track
 *  @brief  Tracking method, given an input image
 *  @param[in]  image   Input image
 *  @param[in,out] shape   Set of landmarks
 *  @return    -1 if error, 0 otherwise
 */
int SdmTracker::Track(const cv::Mat& image, cv::Mat* shape) {
  int error = -1;
  // Convert to grayscale first
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_, cv::COLOR_BGR2GRAY);
  } else {
    working_img_ = image;
  }
  // Have we already detect something ?
  if (!is_tracking_) {
    // Call for detection
    error = this->Detect(working_img_, shape);
    is_tracking_ = error == 0;
  } else {
    // Tracking
    previous_shape_ = *shape;
    initial_shape_ = this->AlignMeanshapeToCurrent(previous_shape_);
    *shape = this->AlignFaceWithScaleAndRotation(working_img_);
    // Check quality
    is_tracking_ = tracking_assessment_->Assess(working_img_, *shape) > 0.0;
    error = 0;
  }

  // Define ouput
  if (!is_tracking_) {
    *shape = cv::Mat();
    is_tracking_ = false;
  } else {
    // Save shape
    previous_shape_ = shape->clone();
    // Compute shape center of gravity
    shape_center_gravity_.x = 0;
    shape_center_gravity_.y = 0;
    for (int i = 0; i < num_points_; ++i) {
      shape_center_gravity_.x += shape->at<double>(i);
      shape_center_gravity_.y += shape->at<double>(i + num_points_);
    }
    shape_center_gravity_.x /= num_points_;
    shape_center_gravity_.y /= num_points_;
  }
  return error;
}

/*
 *  @name   Track
 *  @brief  Tracking method, given an input image
 *  @param[in]  image       Input image
 *  @param[in]  face_region Location of the head used to initialize
 tracking;
 *  @param[in,out] shape       Set of landmarks
 *  @return    -1 if error, 0 otherwise
 */
int SdmTracker::Track(const cv::Mat& image,
                      const cv::Rect& face_region,
                      cv::Mat* shape) {
  int error = -1;
  // Convert to grayscale first
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_, cv::COLOR_BGR2GRAY);
  } else {
    working_img_ = image;
  }
  // Have we already detect something ?
  if (!is_tracking_) {
    // Call for detection
    error = this->Detect(working_img_, face_region, shape);
    is_tracking_ = error == 0;
  } else {
    // Tracking
    previous_shape_ = *shape;
    initial_shape_ = this->AlignMeanshapeToCurrent(previous_shape_);
    *shape = this->AlignFaceWithScaleAndRotation(working_img_);
    // Check quality
    is_tracking_ = tracking_assessment_->Assess(working_img_, *shape) > 0.0;
    error = 0;
  }
  // Still tracking ?
  if (!is_tracking_) {
    *shape = cv::Mat();
    is_tracking_ = false;
  }
  return error;
}

/*
 *  @name   ResetTracker
 *  @brief  Reset current tracking workspace
 */
void SdmTracker::ResetTracker() {
  is_tracking_ = false;
}
/*
 * @name CheckQuality
 * @fn bool CheckQuality(const cv::Mat& image, const cv::Mat& shape)
 * @brief   Check quality of the shape
 * @param[in]   image   Input imate
 * @param[in]   shape   Detected shape to test
 * @return  True if good, false otherwise
 */
bool SdmTracker::CheckQuality(const cv::Mat& image, const cv::Mat& shape) {
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_, cv::COLOR_BGR2GRAY);
  } else {
    working_img_ = image;
  }
  // Still tracking ?
  //inner_sift_features_ = this->get_sift_features(working_img_, shape);
  return tracking_assessment_->Assess(working_img_, shape) > 0.0;
}

#pragma mark -
#pragma mark Training
/*
 *  @name   Train
 *  @brief  SDM training method
 *  @param[in]  folder  Location of images/annotations
 */
void SdmTracker::Train(const std::string& folder) {
}

#pragma mark -
#pragma mark Private

/*
 *  @name   ComputeInitialShape
 *  @brief  Aligned the meanshape inside the bounding box
 *  @param[in]  meanshape       Model meanshape
 *  @param[in]  bounding_box    Region of interest
 *  @param[out] initial_shape   Initial shape
 */
void SdmTracker::ComputeInitialShape(const cv::Mat& meanshape,
                                     const cv::Rect& bounding_box,
                                     cv::Mat* initial_shape) {
  // Goes through all points
  for (int p = 0 ; p < num_points_ ; ++p) {
    initial_shape->at<double>(p) = bounding_box.x + meanshape.at<double>(p) *
    bounding_box.width;
    initial_shape->at<double>(p + num_points_) = bounding_box.y +
    meanshape.at<double>(p + num_points_) *
    bounding_box.height;
  }
}

/*
 *  @name   ComputeInitialShape
 *  @brief  Aligned the meanshape inside the bounding box
 *  @param[in]  meanshape       Model meanshape
 *  @param[in]  bounding_box    Region of interest
 *  @param[in]  delta           Additionnal translation
 *  @param[out] initial_shape   Initial shape
 */
void SdmTracker::ComputeInitialShape(const cv::Mat& meanshape,
                                     const cv::Rect& bounding_box,
                                     const cv::Point2d& delta,
                                     cv::Mat* initial_shape) {
  // Goes through all points
  for (int p = 0 ; p < num_points_ ; ++p) {
    initial_shape->at<double>(p) = bounding_box.x + meanshape.at<double>(p) *
    bounding_box.width + delta.x;
    initial_shape->at<double>(p + num_points_) = bounding_box.y +
    meanshape.at<double>(p + num_points_) * bounding_box.height +
    delta.y;
  }
}

/*
 *  @name   AlignFaceWithScale
 *  @brief  Face alignement with SDM model
 *  @param[in]  image   Input image
 *  @return New shape
 */
cv::Mat SdmTracker::AlignFaceWithScale(const cv::Mat& image) {
  using LA = LinearAlgebra<double>;
  using TransposeType = LinearAlgebra<double>::TransposeType;
  const int number_pass = 2;
  cv::Mat new_shape = initial_shape_.clone();
  cv::Mat reference_shape;
  cv::Mat normalized_image;
  cv::Mat inverse_transform;
  cv::Mat dummy;
  for (int pass = 0; pass < number_pass; ++pass) {
    // Goes through each stage
    SdmModelConstIt_t sdm_model_it = sdm_model_.begin();
    for (int stage = 0;
         stage < tracker_parameters_.number_of_stage;
         ++stage, ++sdm_model_it) {
      // Coarse to fine approach ?
      int edge = tracker_parameters_.face_region_width;
      if (tracker_parameters_.coarse_to_fine_search &&
          stage < tracker_parameters_.number_of_coarse_stages) {
        edge /= 2;
      }

      // Normalize face
      cv::Mat normalized_shape = new_shape.clone();
      reference_shape = sdm_meanshape_ * edge + edge/2;
      NormalizeScale(reference_shape, edge, image,
                     &normalized_image,
                     &normalized_shape,
                     &dummy,
                     &inverse_transform);

      // Sift extraction + update key points
      double x = 0.0;
      double y = 0.0;
      KeyPointIt_t key_point_it = sift_extraction_location_.begin();
      for (int p = 0; key_point_it != sift_extraction_location_.end();
           ++key_point_it, ++p) {
        x = normalized_shape.at<double>(p);
        y = normalized_shape.at<double>(p + num_points_);
        key_point_it->pt.x = cvRound(x);
        key_point_it->pt.y = cvRound(y);
        key_point_it->size = tracker_parameters_.sift_size;
      }
      SSift::ComputeDescriptorOpt(normalized_image, sift_extraction_location_,
                                  sift_desciptor_params_,
                                  &sift_features_);
      sift_features_.convertTo(sift_features_, CV_64FC1);
      sift_features_ = sift_features_.reshape(sift_features_.channels(),
                                              1/*row*/);

      // Calculate + apply shape update
      cv::Mat sift_ext = cv::Mat::ones(sift_features_.rows,
                                       sift_features_.cols + 1,
                                       sift_features_.type());
      sift_features_.copyTo(sift_ext.colRange(0, sift_features_.cols));

      /* cblas version */
      LA::Gemv(*sdm_model_it,
               TransposeType::kTranspose,
               1.0,
               sift_ext,
               1.0,
               &normalized_shape);
      /*cblas_dgemv(CBLAS_ORDER::CblasRowMajor, CBLAS_TRANSPOSE::CblasTrans,
                  (const int)sdm_model_it->rows,
                  (const int)sdm_model_it->cols,
                  1.0,
                  reinterpret_cast<double*>(sdm_model_it->data),
                  (const int)sdm_model_it->cols,
                  (const double*)sift_ext.data,
                  1, 1.0,
                  reinterpret_cast<double*>(normalized_shape.data),
                  1);*/

      //  Back-project shape update to original coordinates
      new_shape = LTS5::TransformShape(inverse_transform, normalized_shape);
    }

    //  Normalize init shape
    if (pass != number_pass - 1) {
      //  Normalize rotation only if angle > 10°
      cv::Vec4d similarity_transform;
      ComputeSimilarityTransform(new_shape,
                                 initial_shape_,
                                 &similarity_transform);
      double angle = atan2(similarity_transform[1],
                           similarity_transform[0]) * 180 / CV_PI;
      if (std::abs(angle) > 10) {
        //  Normalize scale & rotation
        cv::Mat new_init_shape = initial_shape_.clone();
        new_shape = new_init_shape;
      } else {
        //  Normalize only scale
        double scale = 1.0 / sqrt((similarity_transform[0] *
                                   similarity_transform[0]) +
                                  (similarity_transform[1] *
                                   similarity_transform[1]));
        cv::Point2d image_center(image.cols / 2.0, image.rows / 2.0);
        for (int p = 0; p < num_points_; p++) {
          new_shape.at<double>(p, 0) = (initial_shape_.at<double>(p, 0) -
                                        image_center.x) * scale +
          image_center.x;
          new_shape.at<double>(p + num_points_, 0) =
          (initial_shape_.at<double>(p + num_points_, 0) -
           image_center.y) * scale + image_center.y;
        }
      }
    }
  }
  return new_shape;
}

/*
 *  @name   AlignFaceWithScaleAndRotation
 *  @brief  Face alignement with SDM model
 *  @param[in]  image   Input image
 *  @return New shape
 */
cv::Mat SdmTracker::AlignFaceWithScaleAndRotation(const cv::Mat& image) {
  using LA = LinearAlgebra<double>;
  using TransposeType = LinearAlgebra<double>::TransposeType;
  cv::Mat new_shape = initial_shape_.clone();
  cv::Mat reference_shape;
  cv::Mat normalized_image;
  cv::Mat transform;
  cv::Mat dummy;
  const int number_pass = this->tracker_parameters_.number_of_pass;
  const int starting_pass = tracker_parameters_.starting_pass;

  // Goes through each stage
  std::vector<cv::Mat>::const_iterator sdm_model_it = sdm_model_.begin();
  for (int stage = 0;
       stage < tracker_parameters_.number_of_stage;
       stage++, ++sdm_model_it) {
    int edge = tracker_parameters_.face_region_width;
    if (tracker_parameters_.coarse_to_fine_search &&
        stage < tracker_parameters_.number_of_coarse_stages) {
      edge /= 2;
    }
    for (int pass = starting_pass; pass < number_pass; pass++) {
      //  Normalize face
      cv::Mat normalized_shape = new_shape.clone();
      reference_shape = sdm_meanshape_ * edge + edge/2;
      NormalizeScaleAndRotation(reference_shape, edge, image,
                                &normalized_image, &normalized_shape,
                                &dummy, &transform);
      // Sift extraction + update key points
      double x = 0.0;
      double y = 0.0;
      KeyPointIt_t key_point_it = sift_extraction_location_.begin();
      for (int p = 0; key_point_it != sift_extraction_location_.end();
           ++key_point_it, ++p) {
        x = normalized_shape.at<double>(p);
        y = normalized_shape.at<double>(p + num_points_);
        key_point_it->pt.x = cvRound(x);
        key_point_it->pt.y = cvRound(y);
        key_point_it->size = tracker_parameters_.sift_size;
      }
      SSift::ComputeDescriptorOpt(normalized_image, sift_extraction_location_,
                                  sift_desciptor_params_,
                                  &sift_features_);
      sift_features_.convertTo(sift_features_, CV_64FC1);
      sift_features_ = sift_features_.reshape(sift_features_.channels(), 1);

      //  Calculate + apply shape update
      cv::Mat sift_ext = cv::Mat::ones(sift_features_.rows,
                                       sift_features_.cols + 1,
                                       sift_features_.type());
      sift_features_.copyTo(sift_ext.colRange(0, sift_features_.cols));

      /* cblas version */
      LA::Gemv(*sdm_model_it,
               TransposeType::kTranspose,
               1.0,
               sift_ext,
               1.0,
               &normalized_shape);
      /*cblas_dgemv(CBLAS_ORDER::CblasRowMajor, CBLAS_TRANSPOSE::CblasTrans,
                  sdm_model_it->rows,
                  sdm_model_it->cols,
                  1.0,
                  reinterpret_cast<double*>(sdm_model_it->data),
                  sdm_model_it->cols,
                  (const double*)sift_ext.data,
                  1, 1.0,
                  reinterpret_cast<double*>(normalized_shape.data),
                  1);*/

      if (pass == 0) {
        //  Back-project shape to original coordinates
        cv::Mat temp_shape = LTS5::TransformShape(transform,
                                                  normalized_shape);
        //  Normalize pose
        cv::Vec<double, 4> sim_transfo;
        ComputeSimilarityTransform(new_shape, temp_shape, &sim_transfo);

        cv::Mat transform_t = (cv::Mat_<double>(2, 3) << sim_transfo[0],
                               -sim_transfo[1], sim_transfo[2],
                               sim_transfo[1], sim_transfo[0],
                               sim_transfo[3]);

        new_shape = LTS5::TransformShape(transform_t, new_shape);
      } else {
        //  Back-project shape to original coordinates
        new_shape = LTS5::TransformShape(transform, normalized_shape);
      }
    }
  }
  return new_shape;
}

/*
 *  @name   get_sift_features
 *  @brief  Provide feature extracted by given shape
 *  @param[in]  image   input image
 *  @param[in]  shape   input shape
 *  @return Matrix of SIFT features
 */
cv::Mat SdmTracker::get_sift_features(const cv::Mat& image,
                                    const cv::Mat& shape) const {
  if (image.empty() || shape.empty()) {
    return cv::Mat();
  }
  cv::Mat sift_features;
  int edge = tracker_parameters_.face_region_width;
  cv::Mat normalized_shape = shape.clone();
  cv::Mat reference_shape = sdm_meanshape_ * edge + edge/2.0;
  cv::Mat normalized_image, dummy, transform;
  NormalizeScaleAndRotation(reference_shape, edge, image,
                            &normalized_image, &normalized_shape,
                            &dummy, &transform);


  std::vector<cv::KeyPoint> sift_extraction_location;
  int num_points = shape.rows/2;
  double x = 0.0;
  double y = 0.0;
  for (int p = 0; p < num_points; p++) {
    // only inner face landmarks and without 60 and 64 (inside lip cornner)
    if (p < 17 || p == 60 || p == 64) {
      continue;
    }
    x = normalized_shape.at<double>(p, 0);
    y = normalized_shape.at<double>(p + num_points, 0);
    cv::KeyPoint kp(cvRound(x), cvRound(y), tracker_parameters_.sift_size);
    sift_extraction_location.push_back(kp);
  }

  SSift::ComputeDescriptorOpt(normalized_image, sift_extraction_location,
                              sift_desciptor_params_,
                              &sift_features);
  sift_features.convertTo(sift_features, CV_64FC1);
  sift_features = sift_features.reshape(sift_features.channels(), 1);

  return sift_features;
}

/*
 *  @name AlignMeanshapeToCurrent
 *  @brief  Align the meanshape on the shape recovered at previous
 *          frame
 *  @param[in]  current_shape   Shape revovered at previous frame
 *  @return Aligned shape
 */
cv::Mat SdmTracker::AlignMeanshapeToCurrent(const cv::Mat& current_shape) {
  // Compute similarity
  cv::Vec4d similarity_transform;
  ComputeSimilarityTransform(sdm_meanshape_,
                             current_shape,
                             &similarity_transform);
  cv::Mat transform = (cv::Mat_<double>(2, 3) << similarity_transform[0],
                       -similarity_transform[1], similarity_transform[2],
                       similarity_transform[1], similarity_transform[0],
                       similarity_transform[3]);
  // Return the transform shape
  return LTS5::TransformShape(transform, sdm_meanshape_);
}



/*
 *  @name   draw_shape
 *  @brief  Draw shape on image
 *  @param[in]  img     Input image
 *  @param[in]  shape   Shape to draw
 *  @param[in]  color   Color of the shape
 *  @return Copy of the image with drawing
 */
cv::Mat SdmTracker::draw_shape(const cv::Mat& img,
                               const cv::Mat& shape,
                               const cv::Scalar& color) {
  cv::Mat canvas = img.clone();
  if (canvas.channels() == 1) {
    cv::cvtColor(canvas, canvas, cv::COLOR_GRAY2BGR);
  }
  int nPoints = shape.rows / 2;
  for (int p = 0; p < nPoints; p++) {
    int x = cvRound(shape.at<double>(p, 0));
    int y = cvRound(shape.at<double>(p + nPoints, 0));
    cv::circle(canvas, cv::Point(x, y), 2, color, -1, cv::LINE_AA);
  }
  return canvas;
}

/*
 * @name  ~SDMProxy
 * @fn    ~SDMProxy()
 * @brief Destructor
 */
SDMProxy::~SDMProxy() {}

/**
 * @name  Create
 * @fn    BaseFaceTracker<cv::Mat, cv::Mat>* Create() const override
 * @brief Create an instance of BaseFaceTracker
 * @return    Tracker instance with proper type
 */
BaseFaceTracker<cv::Mat, cv::Mat>* SDMProxy::Create() const {
  return new SdmTracker();
};

/**
 *  @name Name
 *  @fn const char* Name() const override
 *  @brief  Return the name for a given type of tracker
 *  @return Tracker name (i.e. sdm, lbf, ...)
 */
const char* SDMProxy::Name() const {
  return "sdm";
}

/** Explicit registration */
SDMProxy sdm_proxy;


}  // namespace LTS5









