/**
 *  @file   lbf_tracker_assessment_trainer.cpp
 *  @brief  Tracking assessment training class
 *
 *  @author Christophe Ecabert
 *  @date   20/03/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <fstream>
#include <random>
#include <chrono>

#include "lts5/face_tracker/lbf_tracker_assessment_trainer.hpp"
#include "lts5/face_tracker/lbf_trainer_pimpl.hpp"
#include "lts5/classifier/crossvalidation.hpp"
#include "lts5/classifier/accuracy_scoring.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/utils/object_type.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  LBFTrackerAssessmentTrainer
 * @fn    LBFTrackerAssessmentTrainer(void)
 * @brief Constructor
 * @param[in] gtruth      List of ground truth
 * @param[in] descriptor  Festure extractor
 * @param[in] meanshape   Meanshape
 */
LBFTrackerAssessmentTrainer::
LBFTrackerAssessmentTrainer(const std::vector<GroundTruth>& gtruth,
                            const std::vector<cv::Mat>& tracker_shape,
                            const RandomForestTrainer& descriptor,
                            const cv::Mat& meanshape,
                            const cv::Vec3d& stdev,
                            const double stdev_crossing) {
  classifier_ = nullptr;
  gtruth_ = const_cast<std::vector<GroundTruth>*>(&gtruth);
  tracker_shapes_ = const_cast<std::vector<cv::Mat>*>(&tracker_shape);
  descriptor_ = const_cast<RandomForestTrainer*>(&descriptor);
  meanshape_ = meanshape;
  stdev_ = stdev;
  crossing_ = stdev_crossing;
}

/*
 * @name  ~LBFTrackerAssessmentTrainer
 * @fn    ~LBFTrackerAssessmentTrainer(void)
 * @brief Destructor
 */
LBFTrackerAssessmentTrainer::~LBFTrackerAssessmentTrainer(void) {

}

/*
 *  @name   Save
 *  @fn  int Save(const std::string& model_name)
 *  @brief  Save the trained model in file. This must be override by
 *          subclass
 *  @param  [in]    model_name      Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int LBFTrackerAssessmentTrainer::Save(const std::string& model_name) {
  int err = -1;
  std::ofstream stream(model_name.c_str(),
                       std::ios_base::out | std::ios_base::binary);
  if (stream.is_open()) {
    // Save object
    err = this->Save(stream);
    // Close stream
    stream.close();
  }
  return err;
}

/*
 *  @name   Save
 *  @fn int Save(std::ostream& output_stream)
 *  @brief  Write the assessment model to file stream. This must be
 *          override by subclass
 *  @param[in]  output_stream  File stream for writing the model
 *  @return -1 if errors, 0 otherwise
 */
int LBFTrackerAssessmentTrainer::Save(std::ostream& output_stream) {
  int err = -1;
  if (output_stream.good()) {
    // Stream ok -> compute + write object size
    int sz = descriptor_->ComputeObjectSize();
    sz += classifier_->ComputeObjectSize();
    int obj_type = static_cast<int>(HeaderObjectType::kLBFTrackerAssessment);
    // Dump object info
    output_stream.write(reinterpret_cast<const char*>(&obj_type),
                        sizeof(obj_type));
    output_stream.write(reinterpret_cast<const char*>(&sz),
                        sizeof(sz));
    // Write descriptor
    err = descriptor_->Write(output_stream);
    // Write classifer
    err |= classifier_->Save(output_stream);
    // Sanity check
    err |= output_stream.good() ? 0 : -1;
  }
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Train
 *  @fn void Train(void)
 *  @brief  SDM training method
 */
void LBFTrackerAssessmentTrainer::Train(void) {
  // Generate shape
  std::cout << "Generate pertubation" << std::endl;
  this->GeneratePerturbation();
  // Extract features
  std::vector<cv::SparseMat> pos_samples, neg_samples;
  std::cout << "Extract features" << std::endl;
  this->ExtractFeatures(&pos_samples, &neg_samples);
  // Train classifier
  std::cout << "Train assessment" << std::endl;
  this->TrainClassifier(pos_samples, neg_samples);
  // Test
  std::cout << "Test assessment" << std::endl;
  this->TestClassifier(pos_samples, neg_samples);
}

#pragma mark -
#pragma mark Private

/*
 * @name  GeneratePerturbation
 * @brief Generate perturbation of the annotation based on face detector
 *        statistics.
 */
void LBFTrackerAssessmentTrainer::GeneratePerturbation(void) {
  // Loop over all ground truth
  shapes_.clear();
  for (size_t i = 0; i < gtruth_->size(); ++i) {
    const auto& sample = gtruth_->at(i);
    const auto& track_shape = tracker_shapes_->at(i);
    cv::Mat perturbation;
    if (sample.label > 0) {
      // positive no perturbation need
      //sample.shape.copyTo(perturbation);
      track_shape.copyTo(perturbation);
    } else {
      // negative need perturbation
      // Setup distributions
      std::normal_distribution<double> dist_s(crossing_ * stdev_[0] * 2.0,
                                              stdev_[0]);
      std::normal_distribution<double> dist_tx(crossing_ * stdev_[1] * 2.0,
                                               stdev_[1]);
      std::normal_distribution<double> dist_ty(crossing_ * stdev_[2] * 2.0,
                                               stdev_[2]);
      std::uniform_real_distribution<double> dist_sign(-1.0, 1.0);
      // Setup engine, can use single engine for mulitple distribution
      // http://stackoverflow.com/questions/9870541
      auto seed = std::chrono::system_clock::now().time_since_epoch().count();
      std::mt19937_64 engine(seed);
      // Generate tx, ty, scale
      const double sign_x = dist_sign(engine) >= 0.0 ? 1.0 : -1.0;
      const double sign_y = dist_sign(engine) >= 0.0 ? 1.0 : -1.0;
      const double sign_s = dist_sign(engine) >= 0.0 ? 1.0 : -1.0;
      const double tx = dist_tx(engine);
      const double ty = dist_ty(engine);
      const double s = dist_s(engine);
      // move to image space
      cv::Mat transform;
      //ComputeSimilarityTransform(meanshape_, sample.shape, &transform);
      ComputeSimilarityTransform(meanshape_, track_shape, &transform);
      const double* t_ptr = reinterpret_cast<const double*>(transform.data);
      double tx_t = t_ptr[0] * tx + t_ptr[1] * ty;
      double ty_t = t_ptr[3] * tx + t_ptr[4] * ty;
      //cv::Point2d cog = GetCenterPoint(sample.shape);
      cv::Point2d cog = GetCenterPoint(track_shape);
      // Apply perturbation
      perturbation.create(track_shape.rows,
                          track_shape.cols,
                          track_shape.type());
      const int n = std::max(perturbation.rows, perturbation.cols) / 2;
      for (int p = 0; p < n; ++p) {
        double x = track_shape.at<double>(p);
        double y = track_shape.at<double>(p + n);
        double xt = (1.0 + sign_s * s) * (x - cog.x) + sign_x * tx_t + cog.x;
        double yt = (1.0 + sign_s * s) * (y - cog.y) +  sign_y * ty_t + cog.y;
        perturbation.at<double>(p) = xt;
        perturbation.at<double>(p + n) = yt;
      }


      /*cv::Mat canvas;
      cv::cvtColor(sample.image_patch, canvas, cv::COLOR_GRAY2BGR);

      for (int i = 0; i < 68; ++i) {
        cv::circle(canvas, cv::Point(perturbation.at<double>(i),
                                     perturbation.at<double>(i+68)),
                   2,
                   CV_RGB(255,255,0),
                   -1);

        cv::circle(canvas, cv::Point(sample.shape.at<double>(i),
                                     sample.shape.at<double>(i+68)),
                   2,
                   CV_RGB(0,255,0),
                   -1);
      }

      cv::imshow("perturbation", canvas);
      cv::waitKey(10);*/
    }
    // Push perturbation
    shapes_.push_back(perturbation);
  }
}

/*
 * @name  ExtractFeatures
 * @fn    void ExtractFeatures(std::vector<cv::SparseMat>* pos_feature,
                     std::vector<cv::SparseMat>* neg_feature)
 * @brief Extract LBF features
 * @param[out] pos_feature positive extracted features
 * @param[out] neg_feature negative extracted features
 */
void LBFTrackerAssessmentTrainer::
ExtractFeatures(std::vector<cv::SparseMat>* pos_feature,
                std::vector<cv::SparseMat>* neg_feature) {
  // Loop over all samples
  for (size_t i = 0; i < gtruth_->size(); ++i) {
    const cv::Mat& shape = shapes_[i];
    const auto& sample = gtruth_->at(i);
    cv::SparseMat feature;
    descriptor_->ComputeLBF(sample, shape, meanshape_, true, &feature);
    if (sample.label > 0) {
      pos_feature->push_back(feature);
    } else {
      neg_feature->push_back(feature);
    }
  }
}

/*
 * @name  TrainClassifier
 * @fn    void TrainClassifier(const std::vector<cv::SparseMat>& pos_feature,
                               const std::vector<cv::SparseMat>& neg_feature)
 * @brief Train classifier
 * @param[in] pos_feature positive extracted features
 * @param[in] neg_feature negative extracted features
 */
void LBFTrackerAssessmentTrainer::
TrainClassifier(const std::vector<cv::SparseMat>& pos_feature,
                const std::vector<cv::SparseMat>& neg_feature) {
  using CrossValidation = BinaryCrossValidation<cv::SparseMat>;
  using SvmTrain = LTS5::BinaryLinearSVMTraining;
  using ScoringType = BinaryCrossValidation<cv::SparseMat>::ScoringType;
  using Stats = BinaryCrossValidation<cv::SparseMat>::Stats;
  // Estimate parameters
  double best_c = 0;
  Stats cv_stats;
  SvmTrain cost_type = SvmTrain::L1R_L2LOSS_SVC;
  CrossValidation::EstimateLinearCoefficient(pos_feature,
                                             neg_feature,
                                             cost_type,
                                             true,
                                             ScoringType::kAccuracy,
                                             &best_c,
                                             &cv_stats);
  std::cout << "\tCross-validation :" << std::endl;
  std::cout << "\t\tC : " << best_c << std::endl;
  std::cout << "\t\tAcc : " << cv_stats.k_mean_score << " +/- ";
  std::cout << cv_stats.k_mean_stddev << std::endl;

  // retrain with best C
  if (classifier_) {
    delete classifier_;
  }
  classifier_ = new BinarySparseLinearSVMClassifier(best_c, cost_type);
  classifier_->Train(pos_feature, neg_feature);
}

/*
 * @name  TestClassifier
 * @fn    void TestClassifier(const std::vector<cv::SparseMat>& pos_feature,
                              const std::vector<cv::SparseMat>& neg_feature)
 * @brief Test classifier on training set
 * @param[in] pos_feature positive extracted features
 * @param[in] neg_feature negative extracted features
 */
void LBFTrackerAssessmentTrainer::
TestClassifier(const std::vector<cv::SparseMat>& pos_feature,
               const std::vector<cv::SparseMat>& neg_feature) {
  // Test performance on training set
  // Define scoring metric for a single fold
  AccuracyScoring scoring(1);
  for (size_t i = 0; i < pos_feature.size(); ++i) {
    double score = classifier_->Classify(pos_feature[i]);
    scoring.AddPositivePrediction(0, score, 0.0);
  }
  for (size_t i = 0; i < neg_feature.size(); ++i) {
    double score = classifier_->Classify(neg_feature[i]);
    scoring.AddNegativePrediction(0, score, 0.0);
  }
  // Compute metric
  std::cout << "\tTraining accuracy : " << scoring.Compute(0) << std::endl;
}

}  // namepsace LTS5