/**
 *  @file   lbf_tracker_assessment.cpp
 *  @brief  LBFTracker assessment class
 *
 *  @author Christophe Ecabert
 *  @date   21/03/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <fstream>
#include <limits>

#include "lts5/face_tracker/lbf_tracker_assessment.hpp"
#include "lts5/face_tracker/lbf_tracker_pimpl.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  LBFTrackerAssessment
 * @fn
 * @brief Constructor
 * @param[in] meanshape   Meanshape
 */
LBFTrackerAssessment::LBFTrackerAssessment(const cv::Mat& meanshape) {
  // defautl threshold
  score_threshold_ = 0.0;
  // Empty classifier
  classifier_ = new BinarySparseLinearSVMClassifier();
  // Empty descriptor
  descriptor_ = new RandomForest();
  // Copy meanshpe
  meanshape.copyTo(meanshape_);
}

/*
 * @name  ~LBFTrackerAssessment
 * @fn    ~LBFTrackerAssessment(void)
 * @brief Destructor
 */
LBFTrackerAssessment::~LBFTrackerAssessment(void) {
  if (classifier_) {
    delete classifier_;
    classifier_ = nullptr;
  }
  if (descriptor_) {
    delete descriptor_;
    descriptor_ = nullptr;
  }
}

/*
 *  @name   Load
 *  @fn int Load(const std::string& model_name)
 *  @brief  Load the assessment model from file
 *  @param[in]  model_name      Path to the model
 *  @return -1 if errors, 0 otherwise
 */
int LBFTrackerAssessment::Load(const std::string& model_name) {
  int err = -1;
  std::ifstream stream(model_name.c_str(),
                       std::ios_base::in | std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Load(stream, true);
  }
  return err;
}

/*
 *  @name   Load
 *  @fn int Load(std::istream& input_stream,const bool& is_binary)
 *  @brief  Load the assessment model from file stream
 *  @param[in]  input_stream  File stream for reading the model
 *  @param[in]  is_binary     Indicate if stream is open has binary
 *  @return -1 if errors, 0 otherwise
 */
int LBFTrackerAssessment::Load(std::istream& input_stream,
                               const bool& is_binary) {
  int err = -1;
  if (input_stream.good()) {
    // Seek for object
    int e = LTS5::ScanStreamForObject(input_stream,
                                      HeaderObjectType::kLBFTrackerAssessment);
    if (!e) {
      // Load descriptor
      err = descriptor_->Load(input_stream);
      // Load classifier
      err |= classifier_->Load(input_stream);
      // Sanity check
      err |= input_stream.good() ? 0 : -1;
    }
  }
  return err;
}

/*
 *  @name   Write
 *  @fn int Write(std::ofstream& out_stream) const
 *  @brief  Copy the object into a binary stream
 *  @param[in]  out_stream  Output binary stream
 *  @return -1 if error, 0 otherwisestd::cout << "Y wrong" << std::endl;
 */
int LBFTrackerAssessment::Write(std::ostream& out_stream) const {
  int err = -1;
  if (out_stream.good()) {
    // Type + size
    int sz = this->ComputeObjectSize();

    int type = static_cast<int>(HeaderObjectType::kLBFTrackerAssessment);
    // Write header
    out_stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    out_stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Write descriptor
    err = descriptor_->Write(out_stream);
    // Write classifer
    err |= classifier_->Save(out_stream);
    // Sanity check
    err |= out_stream.good() ? 0 : -1;
  }
  return err;
}

/*
 *  @name ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the object size in bytes
 *  @return Object memory size
 */
int LBFTrackerAssessment::ComputeObjectSize(void) const {
  int sz = descriptor_->ComputeObjectSize();
  sz += classifier_->ComputeObjectSize();
  return sz;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Assess
 *  @fn double Assess(const cv::Mat& image, const cv::Mat& shape)
 *  @brief  Assessment method, given the current shape and image
 *  @param[in]  image   Input image
 *  @param[in]  shape     Current shape tracked by the face tracker
 *  @return   scalar score, the larger the better
 */
double LBFTrackerAssessment::Assess(const cv::Mat& image,
                                    const cv::Mat& shape) {
  double score = -std::numeric_limits<double>::max();
  if (!image.empty() && !shape.empty()) {
    // Extract feature
    cv::Mat transform;
    cv::SparseMat lbf;
    ComputeSimilarityTransform(meanshape_, shape, &transform);
    descriptor_->ComputeLBF(image, shape, transform, true, &lbf);
    // Classify
    this->score_ = classifier_->Classify(lbf);
    score = this->score_ > score_threshold_ ? 1.0 : -1.0;
  }
  return score;
}

}  // namepsace LTS5