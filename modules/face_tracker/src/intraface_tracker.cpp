/**
 *  @file   intraface_tracker.cpp
 *  @brief  Face tracker wrapper for Intraface implementation
 *  @author Ecabert Christophe
 *  @date   14/12/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>

#include "lts5/face_tracker/intraface_tracker.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name IntrafaceTracker
 *  @fn IntrafaceTracker(void)
 *  @brief  Constructor
 */
IntrafaceTracker::IntrafaceTracker(void) :
#ifdef HAS_INTRAFACE
  xxd_(nullptr),
  tracker_(nullptr),
#endif
  is_tracking_(false),
  f_detector_(nullptr) {}

/*
 *  @name ~IntrafaceTracker
 *  @fn ~IntrafaceTracker(void)
 *  @brief  Destructor
 */
IntrafaceTracker::~IntrafaceTracker(void) {
  if (tracker_) {
    delete tracker_;
    tracker_ = nullptr;
  }
  if (xxd_) {
    delete xxd_;
    xxd_ = nullptr;
  }
  if (f_detector_) {
    delete f_detector_;
  }
}

/*
 *  @name   Load
 *  @fn int Load(const std::string& model_filename)
 *  @brief  Load the tracker model. This must be override by subclass
 *  @param[in]  model_filename      Path to the model
 *  @return -1 if errors, 0 otherwise
 */
int IntrafaceTracker::Load(const std::string& model_filename) {
  std::cout << "Mehtod Not Supported by this Tracker ! " << std::endl;
  return -1;
}

/*
*  @name   Load
*  @fn int Load(const std::string& detection_model,
const std::string& tracking_model
*  @brief  Load the tracker model. This must be override by subclass
*  @param[in]  detection_model      Path to the detection model
*  @param[in]  tracking_model       Path to the tracking model
*  @return -1 if errors, 0 otherwise
*/
int IntrafaceTracker::Load(const std::string& detection_model,
                           const std::string& tracking_model,
                           const std::string& f_detector_model) {
  int error = -1;
  if (f_detector_) {
    delete f_detector_;
  }
  // Load face detector
  f_detector_ = new cv::CascadeClassifier(f_detector_model);
  error = f_detector_->empty() ? -1 : 0;
  // Load tracker
  error |= this->Load(detection_model, tracking_model);

  return error;
}

/*
 *  @name   Load
 *  @fn int Load(const std::string& detection_model,
 const std::string& tracking_model
 *  @brief  Load the tracker model. This must be override by subclass
 *  @param[in]  detection_model      Path to the detection model
 *  @param[in]  tracking_model       Path to the tracking model
 *  @return -1 if errors, 0 otherwise
 */
int IntrafaceTracker::Load(const std::string& detection_model,
                           const std::string& tracking_model) {
  int error = -1;
#ifdef HAS_INTRAFACE
  // Create descriptor
  xxd_ = new INTRAFACE::XXDescriptor(4);
  // Initialized tracker
  tracker_ = new INTRAFACE::FaceAlignment(detection_model,
                                          tracking_model,
                                          xxd_);
  error = tracker_->Initialized() ? 0 : -1;

  std::cout << "Error value : " << error << (!error ? " Intraface OK" : "Intraface KO") <<std::endl;
#endif 
  return error;
}

/*
 *  @name   Save
 *  @fn int Save(const std::string& model_name)
 *  @brief  Save the trained model in file. This must be override by subclass
 *  @param[in] model_name   Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int IntrafaceTracker::Save(const std::string& model_name) {
  std::cout << "Mehtod Not Supported by this Tracker ! " << std::endl;
  return -1;
}

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks. This must be override by subclass
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int IntrafaceTracker::Detect(const cv::Mat& image, cv::Mat* shape) {
  int error = -1;
  // Image need color conversion ?
  if (image.channels() != 1) {
    cv::cvtColor(image, working_image_, CV_BGR2GRAY);
  } else {
    working_image_ = image;
  }
  // Run face detector
  std::vector<cv::Rect> face_region;
  cv::Rect face_location;
  f_detector_->detectMultiScale(working_image_,
                                face_region,
                                1.2, 2, 0,
                                cv::Size(200, 200));
  if (!face_region.empty()) {
    // Find the biggest
    face_location = *std::max_element(face_region.begin(),
                                      face_region.end(),
                                      [](const cv::Rect& r1,
                                         const cv::Rect r2) -> bool {
                                          return r1.height < r2.height;
                                      });
    // Detect
    error = this->Detect(working_image_, face_location, shape);
  } else {
    shape->release();
  }
  return error;
}

/*
 *  @name   Detect
 *  @fn int Detect(const cv::Mat& image, const cv::Rect& face_region,
 cv::Mat* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks. This must be override by subclass
 *  @param[in]  image           Input image
 *  @param[in]  face_region     Face boundingg box
 *  @param[out] shape           Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int IntrafaceTracker::Detect(const cv::Mat& image,
                             const cv::Rect& face_region,
                             cv::Mat* shape) {
  int error = -1;
#ifdef HAS_INTRAFACE
  if (image.channels() != 1) {
    cv::cvtColor(image, working_image_, CV_BGR2GRAY);
  } else {
    working_image_ = image;
  }
  // Track
  cv::Mat landmarks;
  float score;
  INTRAFACE::IFRESULT err = tracker_->Detect(working_image_,
                                             face_region,
                                             shape_,
                                             score);
  if (err != INTRAFACE::IF_OK || score < score_thresh_) {
    // Fail
    shape->release();
    is_tracking_ = false;
    error = -1;
  } else {
    // Success, convert to SDM shape
    int n_pts = shape_.cols;
    shape->create(2 * n_pts, 1, CV_64FC1);
    for (int i = 0; i < n_pts; ++i) {
      shape->at<double>(i) = shape_.at<float>(0, i);
      shape->at<double>(i + n_pts) = shape_.at<float>(1, i);
    }
    // Setup tracking
    is_tracking_ = true;
    previous_shape_ = shape_.clone();
    error = 0;
  }
#endif
  return error;
}

/*
 *  @name   Track
 *  @fn int Track(const cv::Mat& image, cv::Mat* shape)
 *  @brief  Tracking method, given an input image (from a sequence), provides
 *          a set of landmarks. This must be override by subclass
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int IntrafaceTracker::Track(const cv::Mat& image, cv::Mat* shape) {
  int error = -1;
#ifdef HAS_INTRAFACE
  if (image.channels() != 1) {
    cv::cvtColor(image, working_image_, CV_BGR2GRAY);
  } else {
    working_image_ = image;
  }
  if (is_tracking_) {
    float score;
    INTRAFACE::IFRESULT err = tracker_->Track(working_image_,
                                              previous_shape_,
                                              shape_,
                                              score);


    if (err == INTRAFACE::IF_OK && score > score_thresh_) {
      error = 0;
      is_tracking_ = true;
      previous_shape_ = shape_.clone();
      // Convert to SDM shape
      int n_pts = shape_.cols;
      shape->create(2 * n_pts, 1, CV_64FC1);
      for (int i = 0; i < n_pts; ++i) {
        shape->at<double>(i) = shape_.at<float>(0, i);
        shape->at<double>(i + n_pts) = shape_.at<float>(1, i);
      }
    } else {
      shape->release();
      is_tracking_ = false;
    }
  } else {
    // Run detection
    this->Detect(working_image_, shape);
  }
#endif
  return error;
}

/*
 *  @name   Train
 *  @fn void Train(const std::string& folder)
 *  @brief  Tracker training method. This must be override by subclass
 *  @param[in]  folder  Location of images/annotations
 */
void IntrafaceTracker::Train(const std::string& folder) {
  std::cout << "Mehtod Not Supported by this Tracker ! " << std::endl;
}

#pragma mark -
#pragma mark Utility

/**
 *  @name   draw_shape
 *  @fn static cv::Mat draw_shape(const cv::Mat& img,
                                  const cv::Mat& shape,
                                  const cv::Scalar& color)
 *  @brief  Draw shape on image
 *  @param[in]  img     Input image
 *  @param[in]  shape   Shape to draw
 *  @param[in]  color   Color of the shape
 *  @return Copy of the image with drawing
 */
cv::Mat IntrafaceTracker::draw_shape(const cv::Mat& img,
                                     const cv::Mat& shape,
                                     const cv::Scalar& color) {
  cv::Mat canvas = img.clone();
  if (canvas.channels() == 1) {
    cv::cvtColor(canvas, canvas, CV_GRAY2BGR);
  }
  int nPoints = shape.rows / 2;
  for (int p = 0; p < nPoints; p++) {
    int x = cvRound(shape.at<double>(p, 0));
    int y = cvRound(shape.at<double>(p + nPoints, 0));
    cv::circle(canvas, cv::Point(x, y), 2, color, -1, CV_AA);
  }
  return canvas;
}


}  // namespace LTS5
