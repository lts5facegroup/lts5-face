/**
 *  @file   cpr_trainer.cpp
 *  @brief  CPR trainer class definition
 *
 *  @author Christophe Ecabert
 *  @date   30/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <fstream>
#include <iostream>
#include <random>
#include <chrono>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/face_tracker/cpr_trainer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

  using Vec2 = LTS5::Vector2<double>;

#pragma mark -
#pragma mark Initialization

/*
 *  @name CPRTrainer
 *  @fn CPRTrainer(const std::string& f_detector_config)
 *  @brief  Constructor
 */
CPRTrainer::CPRTrainer(const std::string& f_detector_config) :
face_detector_config_(f_detector_config) {}

/*
 *  @name ~CPRTrainer
 *  @fn ~CPRTrainer(void)
 *  @brief  Destructor
 */
CPRTrainer::~CPRTrainer(void) {
  for (int i = 0; i < fern_cascade_.size(); ++i) {
    delete fern_cascade_[i];
  }
}

#pragma mark -
#pragma mark Train

/*
 *  @name Train
 *  @fn void Train(const std::string& data_folder,
                   const int n_fern_fst_lvl,
                   const int n_fern_sec_lv,
                   const int n_candidate_pixel,
                   const int n_feat_fern,
                   const double shrinkage_coef,
                   const int n_data_augmentation)
 *  @brief  Train CPR face tracker
 *  @param[in]  data_folder         Root folder where image and annotation are
 *                                  stored
 *  @param[in]  n_fern_fst_lvl      Number of cascaded random fern to train
 *  @param[in]  n_fern_sec_lvl      Number of fern in each cascade
 *  @param[in]  n_candidate_pixel   Feature selection pool size
 *  @param[in]  n_feat_fern         Number of feature to select
 *  @param[in]  shrinkage_coef      Shrinkage coefficient (fern training)
 *  @param[in]  n_data_augmentation How many time samples are augmented
 */
int CPRTrainer::Train(const std::string& data_folder,
                       const int n_fern_fst_lvl,
                       const int n_fern_sec_lvl,
                       const int n_candidate_pixel,
                       const int n_feat_fern,
                       const double shrinkage_coef,
                       const int n_data_augmentation) {
  int error = -1;
  // Scan folder for images + annotation
  // -------------------------------------------------------------------
  std::cout << "Scan folder for images/annotations ..." << std::endl;
  LTS5::GetFileListing(image_list_, data_folder, "", ".png");
  LTS5::GetFileListing(image_list_, data_folder, "", ".jpg");
  LTS5::GetFileListing(annotation_list_, data_folder, "", ".pts");
  std::sort(image_list_.begin(), image_list_.end());
  std::sort(annotation_list_.begin(), annotation_list_.end());
  // Same amount of images + annotations ?
  // -------------------------------------------------------------------
  if (image_list_.size() != annotation_list_.size()) {
    std::cout << "Detected image and annotation do not match !" << std::endl;
    return error;
  }
  // Load annotations
  // -------------------------------------------------------------------
  std::vector<cv::Mat> centroids;
  error = this->LoadAnnotation(&ground_truth_, &centroids);
  // Generate Bounding box
  // -------------------------------------------------------------------
  if (!error) {
    error = this->GenerateFaceboundingBox(&ground_truth_,
                                          &centroids,
                                          &face_bbox_);
    if (!error) {
      // Compute mean_shape
      this->ComputeMeanShape(ground_truth_, face_bbox_);
      // Data augmentation
      // -------------------------------------------------------------------
      const int n_sample = static_cast<int>(ground_truth_.size());
      unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now()
                                            .time_since_epoch()
                                            .count());
      std::mt19937_64 gen(seed);
      std::uniform_int_distribution<int> dist(0, n_sample - 1);
      augmented_ground_truth_.create(n_sample * n_data_augmentation,
                                    ground_truth_[0].cols,
                                    ground_truth_[0].type());
      current_shape_.create(n_sample * n_data_augmentation,
                            ground_truth_[0].cols,
                            ground_truth_[0].type());
      augmented_image_list_.clear();
      augmented_face_bbox_.clear();
      cv::Mat shape;
      for (int s = 0; s < n_sample; ++s) {
        for (int k = 0; k < n_data_augmentation; ++k) {
          int idx = s;
          int gidx = s * n_data_augmentation + k;
          do {
            idx = dist(gen);
          }while (idx == s && n_sample != 1);
          // Image
          augmented_image_list_.push_back(image_list_[s]);
          // Ground truth
          ground_truth_[s].copyTo(augmented_ground_truth_.row(gidx));
          // Bbox
          augmented_face_bbox_.push_back(face_bbox_[s]);
          // Current shape, randomly selected from other training examples
          LTS5::NormalizeShapeXY<double>(ground_truth_[idx],
                                         face_bbox_[idx],
                                         &shape);
          LTS5::PlaceShapeXY<double>(shape, face_bbox_[s], &shape);
          // Put shape into current shape pool
          shape.copyTo(current_shape_.row(gidx));
        }
      }
      // Training
      // -------------------------------------------------------------------
      if (!fern_cascade_.empty()) {
        for (int i = 0; i < fern_cascade_.size(); ++i) {
          delete fern_cascade_[i];
        }
      }
      fern_cascade_.clear();
      cv::Mat shape_update; // Prediction
      cv::Mat norm_shape;
      cv::Mat curr_shape;
      for (int fe = 0; fe < n_fern_fst_lvl; ++fe) {

        std::cout << "diff " << cv::norm(current_shape_.row(0) - augmented_ground_truth_.row(0)) << std::endl;

        std::cout << "\tTraining fern cascade : " << fe << std::endl;
        fern_cascade_.push_back(new LTS5::CascadedRandomFernTrainer());
        fern_cascade_[fe]->Train(augmented_image_list_,
                                 current_shape_,
                                 augmented_ground_truth_,
                                 augmented_face_bbox_,
                                 mean_shape_,
                                 n_fern_sec_lvl,
                                 n_candidate_pixel,
                                 n_feat_fern,
                                 shrinkage_coef,
                                 &shape_update);
        // Update current shape
        for (int s = 0; s < current_shape_.rows; ++s) {
          // Bring shape into norm ref
          curr_shape = current_shape_.row(s);
          LTS5::NormalizeShapeXY<double>(curr_shape,
                                         augmented_face_bbox_[s],
                                         &norm_shape);
          // apply update
          norm_shape += shape_update.row(s);

          // Put back into image space
          LTS5::PlaceShapeXY<double>(norm_shape,
                                     augmented_face_bbox_[s],
                                     &curr_shape);


          /*if (s == 0) {
            cv::Mat img = cv::imread(augmented_image_list_[s]);
            cv::Mat gt = augmented_ground_truth_.row(s);
            LTS5::Vector2<double>* b_ptr = (LTS5::Vector2<double>*)gt.data;
            LTS5::Vector2<double>* n_ptr = (LTS5::Vector2<double>*)curr_shape.data;

            for (int p = 0; p < 68; ++p) {
              cv::circle(img, cv::Point(b_ptr[p].x_, b_ptr[p].y_), 2, CV_RGB(0, 0, 255), -1);
              cv::circle(img, cv::Point(n_ptr[p].x_, n_ptr[p].y_), 2, CV_RGB(0, 255, 0), -1);
            }

            cv::rectangle(img, augmented_face_bbox_[s], CV_RGB(0, 255, 0));


            cv::imshow("Update", img);
            cv::waitKey();
          }*/
        }
      }
      std::cout << "CPR Trained (" << (error ? "ERROR" : "SUCCESS") << ")" << std::endl;
    }
  }
  return error;
}

/*
 *  @name Save
 *  @fn int Save(std::string& filename) const
 *  @brief  Save Random fern cascade into a given file
 *  @param[in]  out_stream  File where to dump object
 *  @retrun -1 if error, 0 otherwise
 */
int CPRTrainer::Save(const std::string& filename) const {
  int error = -1;
  size_t pos = filename.rfind(".");
  std::string fname;
  if (pos != std::string::npos) {
    fname = filename.substr(0, pos) + ".bin";
  } else {
    fname = filename + ".bin";
  }
  // Open stream
  std::ofstream out_stream(fname.c_str(), std::ios_base::binary);
  if (out_stream.is_open()) {
    // Write
    error = this->Write(out_stream);
    out_stream.close();
  }
  std::cout << "CPR Saved (" << (error ? "ERROR" : "SUCCESS") << ")" << std::endl;
  return error;
}

/*
 *  @name Write
 *  @fn int Write(std::ostream& out_stream) const
 *  @brief  Write Random fern cascade into a given stream
 *  @param[in]  out_stream  Stream where to dump object
 *  @retrun -1 if error, 0 otherwise
 */
int CPRTrainer::Write(std::ostream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Compute centered ground truth
    cv::Mat gt_center = cv::Mat(static_cast<int>(ground_truth_.size()),
                                ground_truth_[0].cols,
                                ground_truth_[0].type());
    for (int s = 0; s < ground_truth_.size(); ++s) {
      cv::Mat shape = gt_center.row(s);
      LTS5::NormalizeShapeXY<double>(ground_truth_[s], face_bbox_[s], &shape);
    }
    // Compute obj header
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kCPRTracker);
    int obj_size = this->ComputeObjectSize();
    // Write
    out_stream.write(reinterpret_cast<char*>(&obj_type), sizeof(obj_type));
    out_stream.write(reinterpret_cast<char*>(&obj_size), sizeof(obj_size));
    error = out_stream.good() ? 0 : -1;
    // Mean_shape
    error |= LTS5::WriteMatToBin(out_stream, mean_shape_);
    // Write ground truth
    error |= LTS5::WriteMatToBin(out_stream, gt_center);
    // Write number of cascade
    const int n_cascade = static_cast<int>(fern_cascade_.size());
    out_stream.write(reinterpret_cast<const char*>(&n_cascade), sizeof(n_cascade));
    for (int p = 0; p < n_cascade; ++p) {
      // Write cascade
      error |= fern_cascade_[p]->Write(out_stream);
    }
  }
  return error;
}

/*
 *  @name ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the size (in byte) of memory used by the object
 */
int CPRTrainer::ComputeObjectSize(void) const {
  int size = 0;
  // Centered ground truth, header + data
  size = static_cast<int>(ground_truth_[0].total() *
                          ground_truth_[0].elemSize() * ground_truth_.size());
  size += 3 * sizeof(int);
  // Mean shape
  size += mean_shape_.total() * mean_shape_.elemSize();
  size += 3 * sizeof(int);
  // Add number of cascade
  int n_cascade = static_cast<int>(fern_cascade_.size());
  size += sizeof(n_cascade);
  // Add cascade
  for (int i = 0; i < n_cascade; ++i) {
    size += 2 * sizeof(int);
    size += fern_cascade_[i]->ComputeObjectSize();
  }
  return size;
}

/*
 *  @name
 *  @fn
 *  @brief  Load annotation and compute centroid
 *  @param[out] annotations Ground truth shape
 *  @param[out] centroids   Shape centroid
 */
int CPRTrainer::LoadAnnotation(std::vector<cv::Mat>* annotations,
                               std::vector<cv::Mat>* centroids) {
  int error = -1;
  if (!annotation_list_.empty()) {
    // Loop over
    annotations->clear();
    centroids->clear();
    for (int i = 0; i < annotation_list_.size(); ++i) {
      // Open file
      std::ifstream in_stream(annotation_list_[i].c_str());
      if (!in_stream.is_open()) {
        std::cout << "Unable to open : " << annotation_list_[i] << std::endl;
        return error;
      }
      // Parse file
      int n_pts = -1;
      std::string line;
      std::string dummy;
      std::stringstream s_stream;
      double x = -1.0, y = -1.0;
      double sum_x = 0.0, sum_y = 0.0;
      double* data_ptr = nullptr;
      while (in_stream.good()) {
        std::getline(in_stream, line);
        if (line.length() != 0) {
          s_stream.str(line);
          // Look for #pts
          if (n_pts == -1) {
            s_stream >> dummy;
            if (dummy == "n_points:") {
              s_stream >> n_pts;
              annotations->push_back(cv::Mat(1, 2 * n_pts, CV_64FC1));
              centroids->push_back(cv::Mat(1, 2, CV_64FC1));
              data_ptr = annotations->at(i).ptr<double>();
            }
          } else {
            // Skip curly brackets
            if (line[0] != '{' && line[0] != '}') {
              s_stream >> x >> y;
              *data_ptr++ = x;
              *data_ptr++ = y;
              sum_x += x;
              sum_y += y;
            }
          }
          s_stream.clear();
        }
      }
      in_stream.close();
      
      // Centroid
      if (sum_x != 0.0 && sum_y != 0.0 && n_pts != -1) {
        centroids->at(i).at<double>(0) = sum_x / n_pts;
        centroids->at(i).at<double>(1) = sum_y / n_pts;
      }
    }
    std::cout << "Annotations loaded ..." << std::endl;
    error = 0;
  }
  return error;
}

/*
 *  @name GenerateFaceboundingBox
 *  @fn void GenerateFaceboundingBox(void)
 *  @brief  Run face detector to generate face bounding boxes
 *  @param[in,out]  Annotation list
 *  @param[in,out]  centroids Annotation centroids
 *  @param[out] face_bbox Face bounding box
 *  @return -1 if error, 0 otherwise
 */
int CPRTrainer::GenerateFaceboundingBox(std::vector<cv::Mat>* annotations,
                                        std::vector<cv::Mat>* centroids,
                                        std::vector<cv::Rect>* face_bbox) {
  int error = -1;
  // Load face detector
  cv::CascadeClassifier f_detector;
  f_detector.load(face_detector_config_);
  if (!f_detector.empty()) {
    std::cout << "Run face detector ..." << std::endl;
    // Loaded, loop over image
    face_bbox->clear();
    for (int i = 0; i < image_list_.size(); ++i) {
      // Load image
      cv::Mat img = cv::imread(image_list_[i]);
      if (img.channels() != 1) {
        cv::cvtColor(img, img, cv::COLOR_BGR2GRAY);
      }
      // Get corresponding centroids
      const double c_x = centroids->at(i).at<double>(0);
      const double c_y = centroids->at(i).at<double>(1);
      // Run detector
      std::vector<cv::Rect> face_box;
      std::vector<cv::Rect> candidate;
      f_detector.detectMultiScale(img,
                                  face_box,
                                  1.1,
                                  3.0,
                                  cv::CASCADE_SCALE_IMAGE,
                                  cv::Size(30, 30));
      if (face_box.empty()) {
        std::cout << "\tNo face detected in : " << image_list_[i] << std::endl;
        // Remove this image
        image_list_.erase(std::next(image_list_.begin(), i));
        annotation_list_.erase(std::next(annotation_list_.begin(), i));
        centroids->erase(std::next(centroids->begin(), i));
        annotations->erase(std::next(annotations->begin(), i));
        i--;
      } else {
        for (int k = 0; k < face_box.size(); ++k) {
          cv::Rect& r = face_box[k];
          if ((c_x > r.x && c_x < (r.x + r.width)) &&
              (c_y > r.y && c_y < (r.y + r.height))) {
            candidate.push_back(face_box[k]);
          }
        }
        if (candidate.empty()) {
          std::cout << "\tWrong face location (" << image_list_[i] << ")" << std::endl;
          // Remove this image
          image_list_.erase(std::next(image_list_.begin(), i));
          annotation_list_.erase(std::next(annotation_list_.begin(), i));
          centroids->erase(std::next(centroids->begin(), i));
          annotations->erase(std::next(annotations->begin(), i));
          i--;
        } else {
          face_bbox->push_back(candidate[0]);
        }
      }
    }
    error = 0;
  }
  return error;
}

/*
 *  @name ComputeMeanShape
 *  @fn void ComputeMeanShape(void)
 *  @brief  Compute training meanshape
 *  @param[in]  ground_truth  Shape ground truth
 *  @param[in]  face_bbox     Face bounding box
 */
void CPRTrainer::ComputeMeanShape(const std::vector<cv::Mat>& ground_truth,
                                  const std::vector<cv::Rect>& face_bbox) {
  std::cout << "Compute mean shape ..." << std::endl;
  // Init mean_shape
  mean_shape_.create(1, ground_truth[0].cols, ground_truth[0].type());
  mean_shape_.setTo(0.0);
  Vec2* mean_ptr = reinterpret_cast<Vec2*>(mean_shape_.data);
  // Accumulate over all shape
  const int n_sample = static_cast<int>(ground_truth.size());
  const int n_pts = ground_truth[0].cols / 2;
  for (int i = 0; i < n_sample; ++i) {
    const cv::Rect& f_box = face_bbox[i];
    const double c_x = (2.0 * static_cast<double>(f_box.x) +
                        static_cast<double>(f_box.width)) / 2.0;
    const double c_y = (2.0 * static_cast<double>(f_box.y) +
                        static_cast<double>(f_box.height)) / 2.0;
    const double bbox_w = f_box.width / 2.0;
    const double bbox_h = f_box.height / 2.0;
    Vec2* shape_ptr = reinterpret_cast<Vec2*>(ground_truth[i].data);
    // Center
    for (int i = 0; i < n_pts; ++i) {
      const Vec2& pts = shape_ptr[i];
      mean_ptr[i].x_ += (pts.x_ - c_x) / bbox_w;
      mean_ptr[i].y_ += (pts.y_ - c_y) / bbox_h;
    }
  }
  // Average
  mean_shape_ /= ground_truth.size();
}
}  // namespace LTS5
