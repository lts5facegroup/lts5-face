/**
 *  @file   generic_tracker_wrapper.hpp
 *  @brief  Generic Face tracker python wrapper
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   06/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __GENERIC_TRACKER_WRAPPER__
#define __GENERIC_TRACKER_WRAPPER__

#include <limits>
#include <iostream>

#include "pybind11/pybind11.h"
#include "opencv2/core.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @class   FaceTrackerWrapper
 * @brief   Python wrapper interface for Face Tracker models
 * @authors Gabriel Cuendet / Christophe Ecabert
 * @date    06/07/2017
 * @ingroup face_tracker
 * @tparam  TrackerType Type of tracker to export
 */
template<typename TrackerType>
class FaceTrackerWrapper {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  FaceTrackerWrapper
   * @fn    FaceTrackerWrapper()
   * @brief Constructor
   */
  FaceTrackerWrapper();

  /**
   * @name  ~FaceTrackerWrapper
   * @fn    ~FaceTrackerWrapper()
   * @brief Destructor
   */
  ~FaceTrackerWrapper();

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& tracker,
              const std::string& detector)
   *  @brief  Load the tracker model and Viola Jones face detector
   *  @param[in]  tracker     Path to the model
   *  @param[in]  detector    Configuration file for Viola & Jones face detector
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& tracker, const std::string& detector);

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& tracker)
   *  @brief  Load the tracker model without face detector
   *  @param[in]  tracker     Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& tracker);

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Detect
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks
   *  @param[in]  image           Input image
   *  @return  Set of landmarks
   */
  cv::Mat Detect(const cv::Mat& image);

  /**
   *  @name   Detect
   *  @brief  Dectection method, given a still image + face location, provides
   *          a set of landmarks
   *  @param[in]  image Input image
   *  @param[in]  roi   Face location
   *  @return  Set of landmarks
   */
  cv::Mat Detect(const cv::Mat& image, const cv::Rect& roi);

  /**
   *  @name   Track
   *  @fn cv::Mat Track(const cv::Mat& image)
   *  @brief  Tracking method, given an input image
   *  @param[in]      image   Input image
   *  @return Set of landmarks
   */
  cv::Mat Track(const cv::Mat& image);

  /**
   *  @name   Track
   *  @fn cv::Mat Track(const cv::Mat& image, const cv::Rect& roi)
   *  @brief  Tracking method, given an input image
   *  @param[in]  image   Input image
   *  @param[in]  roi   Face location
   *  @return Set of landmarks
   */
  cv::Mat Track(const cv::Mat& image, const cv::Rect& roi);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name get_score_threshold
   *  @fn double get_score_threshold() const
   *  @brief  Get the actual decision threshold
   *  @return Decision threshold
   */
  double get_score_threshold() {
    if (instance_) {
      return instance_->get_score_threshold();
    } else {
      std::cout << "Throw runtime_error" << std::endl;
      throw std::runtime_error("Tracker not initialized");
    }
  }

  /**
   *  @name set_score_threshold
   *  @fn void set_score_threshold(double threshold)
   *  @brief  Set new decision threshold
   *  @param[in]  threshold   Decision threshold
   */
  void set_score_threshold(double threshold) {
    if (instance_) {
      instance_->set_score_threshold(threshold);
    } else {
      std::cout << "Throw runtime_error" << std::endl;
      throw std::runtime_error("Tracker not initialized");
    }
  }

  /**
   * @name  get_score
   * @fn    double get_score() const
   * @brief Return tracking score
   * @return    Score
   */
  double get_score() const {
    if (instance_) {
      return instance_->get_score();
    } else {
      std::cout << "Throw runtime_error" << std::endl;
      throw std::runtime_error("Tracker not initialized");
    }
  }

#pragma mark -
#pragma mark Private

 private:
  /** Sdm tracker instance */
  TrackerType* instance_;
  /** Shape */
  cv::Mat shape_;
};

/**
 * @name    AddFaceTracker
 * @fn  void AddFaceTracker(pybind11::module& m)
 * @brief Expose face tracker classes to python
 * @param[in,out] m Python module where to expose the classes
 */
void AddFaceTracker(pybind11::module& m);

}  // namepsace Python
}  // namepsace LTS5
#endif //__GENERIC_TRACKER_WRAPPER__
