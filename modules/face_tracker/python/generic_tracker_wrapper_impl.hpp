/**
 *  @file   generic_tracker_wrapper_impl.hpp
 *  @brief  Face tracker python wrapper
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   06/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/python/py_message.hpp"
#include "lts5/python/ocv_converter.hpp"
#include "lts5/python/scoped_gil.hpp"

#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/lbf_tracker.hpp"

#include "include/generic_tracker_wrapper.hpp"

namespace py = pybind11;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

#pragma mark -
#pragma mark Initialization

/*
 * @name  FaceTrackerWrapper
 * @fn    FaceTrackerWrapper(void)
 * @brief Constructor
 */
template<class TrackerType>
FaceTrackerWrapper<TrackerType>::FaceTrackerWrapper(void) : instance_(nullptr) {
  instance_ = new TrackerType();
}

/*
 * @name  ~FaceTrackerWrapper
 * @fn    ~FaceTrackerWrapper(void)
 * @brief Destructor
 */
template<class TrackerType>
FaceTrackerWrapper<TrackerType>::~FaceTrackerWrapper(void) {
  if (instance_) {
    delete instance_;
    instance_ = nullptr;
  }
}

/*
 *  @name   Load
 *  @fn     int Load(const std::string& tracker,
            const std::string& detector)
 *  @brief  Load the SDM model and Viola Jones face detector
 *  @param[in]  tracker     Path to the model
 *  @param[in]  detector    Configuration file for Viola & Jones face detector
 *  @return -1 if errors, 0 otherwise
 */
template<class TrackerType>
int FaceTrackerWrapper<TrackerType>::Load(const std::string& tracker,
                                          const std::string& detector) {
  int err = -1;
  if (instance_) {
    err = instance_->Load(tracker, detector);
  } else {
    PyErrorMessage("Tracker uninitialized\n");
  }
  return err;
}

/*
 *  @name   Load
 *  @fn     int Load(const std::string& tracker)
 *  @brief  Load the tracker model without face detector
 *  @param[in]  tracker     Path to the model
 *  @return -1 if errors, 0 otherwise
 */
template<class TrackerType>
int FaceTrackerWrapper<TrackerType>::Load(const std::string& tracker) {
  int err = -1;
  if (instance_) {
    err = instance_->Load(tracker);
  } else {
    PyErrorMessage("Tracker uninitialized\n");
  }
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Detect
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks
 *  @param[in]  image           Input image
 *  @return  Set of landmarks
 */
template<class TrackerType>
cv::Mat FaceTrackerWrapper<TrackerType>::Detect(const cv::Mat& image) {
  if (instance_) {
    py::gil_scoped_acquire acquire;
    instance_->Detect(image, &shape_);
  } else {
    PyErrorMessage("Tracker uninitialized\n");
  }
  return shape_;
}

/*
 *  @name   Detect
 *  @brief  Dectection method, given a still image + face location, provides
 *          a set of landmarks
 *  @param[in]  image Input image
 *  @param[in]  roi   Face location
 *  @return  Set of landmarks
 */
template<class TrackerType>
cv::Mat FaceTrackerWrapper<TrackerType>::Detect(const cv::Mat& image,
                                                const cv::Rect& roi) {
  if (instance_) {
    py::gil_scoped_acquire acquire;
    instance_->Detect(image, roi, &shape_);
  } else {
    PyErrorMessage("Tracker uninitialized\n");
  }
  return shape_;
}

/*
 *  @name   Track
 *  @fn PyObject* Track(PyObject* image)
 *  @brief  Tracking method, given an input image
 *  @param[in]      image   Input image
 *  @return Set of landmarks
 */
template<class TrackerType>
cv::Mat FaceTrackerWrapper<TrackerType>::Track(const cv::Mat& image) {
  if (instance_) {
    py::gil_scoped_acquire acquire;
    instance_->Track(image, &shape_);
  } else {
    PyErrorMessage("Tracker uninitialized\n");
  }
  return shape_;
}

/*
 *  @name   Track
 *  @fn cv::Mat Track(const cv::Mat& image, const cv::Rect& roi)
 *  @brief  Tracking method, given an input image
 *  @param[in]  image   Input image
 *  @param[in]  roi   Face location
 *  @return Set of landmarks
 */
template<class TrackerType>
cv::Mat FaceTrackerWrapper<TrackerType>::Track(const cv::Mat& image,
                                               const cv::Rect& roi) {
  if (instance_) {
    py::gil_scoped_acquire acquire;
    instance_->Track(image, roi, &shape_);
  } else {
    PyErrorMessage("Tracker uninitialized\n");
  }
  return shape_;
}

template<class T>
struct TrackerDoc;

template<> struct TrackerDoc<LTS5::SdmTracker> {
  static constexpr const char* doc = R"doc(SDM face-tracker)doc";
};
template<> struct TrackerDoc<LTS5::LBFTracker> {
  static constexpr const char* doc = R"doc(LBF face-tracker)doc";
};

/**
 * @name    ExposeTracker
 * @brief Expose a face tracker class
 * @tparam T    Tracker type
 * @param[in, out] m    Python module
 * @param[in] name  Name of the classe
 */
template<typename T>
void ExposeTracker(py::module& m, const std::string& name) {
  using Tracker = FaceTrackerWrapper<T>;

  // Expose class
  // http://pybind11.readthedocs.io/en/stable/classes.html
  py::class_<Tracker>(m,
                      name.c_str(),
                      "Doc")
  // CTOR
          .def(py::init<>(),
               R"doc(Create a tracker instance)doc")
          .def("load",
               (int(Tracker::*)(const std::string&, const std::string&))&Tracker::Load,
               py::arg("tracker"),
               py::arg("detector"),
               R"doc(Initialize face tracker with a given model/detector)doc")
          .def("load",
               (int(Tracker::*)(const std::string&))&Tracker::Load,
               py::arg("tracker"),
               R"doc(Initialize face tracker with a given model)doc")
                  // Mehtod
          .def("detect",
               [](Tracker& tracker, const cv::Mat& image) -> cv::Mat {
                 py::gil_scoped_release gil;
                 return tracker.Detect(image);
               },
               py::arg("image"),
               py::return_value_policy::reference,
               R"doc(Detect facial landmarks on a single image.)doc")
          .def("detect",
               [](Tracker& tracker,
                  const cv::Mat& image,
                  const cv::Rect& region) -> cv::Mat {
                 py::gil_scoped_release gil;
                 return tracker.Detect(image, region);
               },
               py::arg("image"),
               py::arg("region"),
               py::return_value_policy::reference,
               R"doc(Detect facial landmarks on a single image at a given location)doc")
          .def("track",
               [](Tracker& tracker, const cv::Mat& image) -> cv::Mat {
                 py::gil_scoped_release gil;
                 return tracker.Track(image);
               },
               py::arg("image"),
               py::return_value_policy::reference,
               R"doc(Track facial landmarks on a given image from a series)doc")
          .def("track",
               [](Tracker& tracker,
                  const cv::Mat& image,
                  const cv::Rect& region) -> cv::Mat {
                 py::gil_scoped_release gil;
                 return tracker.Track(image, region);
               },
               py::arg("image"),
               py::arg("region"),
               py::return_value_policy::reference,
               R"doc(Track facial landmarks on a given image from a series at a given locaiton)doc")
          .def_property("thresh",
                        &Tracker::get_score_threshold,
                        &Tracker::set_score_threshold,
                        py::return_value_policy::copy,
                        R"doc(Get/set threshold resetting tracking)doc")
          .def_property_readonly("score",
                                 &Tracker::get_score,
                                 py::return_value_policy::copy,
                                 R"doc(Provide tracking quality)doc");
}


/**
 * @name    AddFaceTracker
 * @fn  void AddFaceTracker(pybind11::module& m)
 * @brief Expose face tracker classes to python
 * @param[in,out] m Python module where to expose the classes
 */
void AddFaceTracker(pybind11::module& m) {
  // Expose SDM
  ExposeTracker<LTS5::SdmTracker>(m, "SDMTracker");
  // Expose LBF
  ExposeTracker<LTS5::LBFTracker>(m, "LBFTracker");
}

}  // namepsace Python
}  // namepsace LTS5
