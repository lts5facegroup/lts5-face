/**
 *  @file   face_tracker_wrapper.cpp
 *  @brief  Convert numpy array into opencv matrix
 *  @see https://github.com/yati-sagade/opencv-ndarray-conversion/
 *  @ingroup python
 *
 *  @author Gabriel Cuendet / Christophe Ecabert
 *  @date   05/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <memory>
#include <cmath>

#include "pybind11/pybind11.h"

#include "lts5/python/ocv_converter.hpp"

// Add implementation
#include "generic_tracker_wrapper_impl.hpp"

namespace py = pybind11;

PYBIND11_MODULE(pyface_tracker, m) {

  // Init numpy array
  // Py_Initialize();
  //LTS5::Python::InitThread();
  LTS5::Python::InitNumpyArray();       // Init numpy

  // Doc
  m.doc() = R"doc(LTS5 Face Tracker modules)doc";

  // Expose face tracker
  LTS5::Python::AddFaceTracker(m);
}