# coding=utf-8
""" Sample showing how to use LTS5 SdmTracker from python side """
import argparse
import cv2
import lts5.utils as utils
import lts5.face_tracker as ft
__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
# Sanity check
utils.check_py_version(3, 0)
# Define Logger
logger = utils.init_logger()

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='SdmTracker example')
  parser.add_argument('-i',
                      type=str,
                      required=True,
                      dest='video',
                      help='Path to video file')
  parser.add_argument('-t',
                      type=str,
                      required=True,
                      dest='tracker',
                      help='Path to tracker model file')
  parser.add_argument('-f',
                      type=str,
                      required=True,
                      dest='fdetect',
                      help='Path to face detector model file')
  args = parser.parse_args()

  # Open video
  reader = cv2.VideoCapture(args.video)
  if reader.isOpened():
    # Create tracker
    tracker = ft.SDMTracker()
    err = tracker.load(tracker=args.tracker, detector=args.fdetect)
    tracker.thresh = -1.8
    if not err:
      logger.info('Tracker successfully loaded')
      process = True
      while process:
        # Load image
        success, img = reader.read()
        if not success:
          break
        # Track
        shape = tracker.track(image=img)
        # Display if success
        if shape is not None:
          for i in range(0, int(shape.shape[0] / 2)):
            cv2.circle(img,
                       (shape[i], shape[i + 68]),
                       2,
                       (0, 255, 0),
                       -1)
        cv2.imshow('Tracking', img)
        key = cv2.waitKey(5)
        if chr(key & 0xFF) == 'q':
          process = False
    else:
      logger.error('Can not load face tracker')
    # Clear video
    reader.release()
  else:
    logger.error('Can not load video : %s', args.video)
