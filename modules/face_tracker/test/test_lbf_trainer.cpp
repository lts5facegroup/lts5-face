/**
 *  @file   test_lbf_trainer.cpp
 *  @brief  Executable to train a LBF face tracker
 *
 *  @author Guillaume Jaume, Christophe Ecabert
 *  @date   27/02.2017
 *  Copyright (c) 2017 Guillaume Jaume. All rights reserved.
 */

#include <iostream>
#include <fstream>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/face_tracker/lbf_trainer.hpp"

int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("-i",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Input folder where data are stored");
  parser.AddArgument("-f",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face detector model");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Output model name");
  parser.AddArgument("-nstage",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Number of stages (def: 5)");
  parser.AddArgument("-ntree",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Number of trees per landmarks (def: 5)");
  parser.AddArgument("-dtree",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Tree depth (def: 5)");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    std::string input_folder;
    std::string f_detector_model;
    std::string output_filename;
    std::string n_stage, n_tree, d_tree;
    parser.HasArgument("-i", &input_folder);
    parser.HasArgument("-f", &f_detector_model);
    parser.HasArgument("-o", &output_filename);
    parser.HasArgument("-nstage", &n_stage);
    parser.HasArgument("-ntree", &n_tree);
    parser.HasArgument("-dtree", &d_tree);
    // Set parameters if value are provided
    LTS5::LBFTrainer::LBFTrainerParameters parameters;
    if (!n_stage.empty()) {
      parameters.n_stages = std::atoi(n_stage.c_str());
    }
    if (!n_tree.empty()) {
      parameters.n_tree = std::atoi(n_tree.c_str());
    }
    if (!d_tree.empty()) {
      parameters.tree_depth = std::atoi(d_tree.c_str());
    }
    LTS5::LBFTrainer* trainer = new LTS5::LBFTrainer(f_detector_model);
    err = trainer->Train(input_folder, &parameters);
    if (!err) {
      // Save
      std::ofstream stream(output_filename.c_str(),
                           std::ios_base::out | std::ios_base::binary);
      err = trainer->Write(stream);
      stream.close();
    } else {
      std::cout << "Error while training" << std::endl;
    }
    delete trainer;
    std::cout << "Done : " << (err == 0 ? "Success" : "Error") << std::endl;
  } else {
    parser.PrintHelp();
  }
  return err;
}
