//
//  test_sdm_detecter.cpp
//  LTS5-Dev
//
//  Created by Gabriel Cuendet on 07.05.15.
//  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
//

#include <iostream>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"


int main(int argc, const char * argv[])
{
  if (argc < 3) {
    std::cout << "Usage : " << argv[0] <<
    " tracker_model face_detector image_file_1"  << std::endl;
    return -1;
  }

  //Create tracker
  LTS5::SdmTracker* face_tracker = new LTS5::SdmTracker();
  int error = face_tracker->Load(argv[1], argv[2]);

  //Load image
  cv::Mat input_image = cv::imread(argv[3],cv::IMREAD_COLOR);

  //Detect
  cv::Mat landmarks_set;
  error = face_tracker->Detect(input_image, &landmarks_set);

  //Show
  cv::Mat canvas = face_tracker->draw_shape(input_image, landmarks_set,
                                            CV_RGB(0, 255, 0));
  std::string image_name(argv[3]);
  std::string output_path;

  auto idx = image_name.rfind(".");
  if (idx != std::string::npos) {
    output_path = image_name.substr(0, idx) + "_fit.jpg";
  } else {
    output_path = image_name + "_fit.jpg";
  }

  cv::imwrite(output_path, canvas);
  cv::imshow("Result", canvas);
  cv::waitKey();

  delete face_tracker;

  return error;
}