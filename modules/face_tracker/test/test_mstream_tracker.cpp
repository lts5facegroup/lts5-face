//
//  main.cpp
//  MStreamSdmTracker
//
//  Created by Christophe Ecabert on 22/04/15.
//  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
//

#include <iostream>

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/utils/time_lapse.hpp"
#include "lts5/face_tracker/tracker_factory.hpp"

int main(int argc, const char * argv[]) {

  LTS5::CmdLineParser parser;
  parser.AddArgument("-v",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "List of videos to tracker (separator=;)");
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Sdm tracker model");
  parser.AddArgument("-f",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face detector model (VJ)");
  parser.AddArgument("-type",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Type of tracker (multi_lbf, multi_sdm)");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Get video pathes
    std::string video_path;
    std::vector<std::string> videos;
    parser.HasArgument("-v", &video_path);
    LTS5::SplitString(video_path, ";", &videos);

    // Open them
    std::vector<cv::VideoCapture*> reader;
    for (int i = 0; i < videos.size(); ++i) {
      // Try to open
      cv::VideoCapture* cap = new cv::VideoCapture(videos[i]);
      if (!cap->isOpened()) {
        cap->release();
        delete cap;
      } else {
        reader.push_back(cap);
      }
    }

    if (reader.size() > 0) {
      // Load tracker
      std::string sdm_model_path;
      std::string f_detector_path;
      std::string type;
      parser.HasArgument("-t", &sdm_model_path);
      parser.HasArgument("-f", &f_detector_path);
      parser.HasArgument("-type", &type);

      int n_view = static_cast<int>(reader.size());
      LTS5::MStreamFaceTracker<cv::Mat, cv::Mat>* tracker = nullptr;
      tracker = LTS5::MTrackerFactory::Get().CreateByName(reader.size(), type);
      if (tracker != nullptr) {
        err = tracker->Load(sdm_model_path,
                            f_detector_path);
        tracker->set_score_threshold(-2.5);
      } else {
        std::cout << "Unknown tracker type" << std::endl;
        err = -1;
      }
      if (!err) {
        // Create window
        std::vector<std::string> win_name;
        for (int i = 0; i < n_view; ++i) {
          win_name.push_back("Stream " + std::to_string(i));
          cv::namedWindow(win_name[i]);
        }
        // Process
        bool process = true;
        std::vector<cv::Mat> data(n_view);
        std::vector<cv::Mat> shapes;
        auto tracker_fcn = [&](void) -> void {
          tracker->Track(data, &shapes);
        };
        while(process) {
          // Load image
          for (int i = 0; i < n_view; ++i) {
             reader[i]->read(data[i]);
          }

          // Track + time
          auto t_ms = LTS5::Measure<>::Duration(tracker_fcn);
          std::cout << "\tcapture time : " << t_ms.count() << " ms" << std::endl;

          // Show
          for (int i = 0; i < n_view; ++i) {
            if (!shapes[i].empty()) {
              const int n_pts = shapes[i].rows / 2;
              for (int j = 0; j < n_pts; ++j) {
                cv::circle(data[i],
                           cv::Point(shapes[i].at<double>(j),
                                     shapes[i].at<double>(j + n_pts)),
                           2,
                           CV_RGB(0, 255, 0),
                           -1);
              }
            }
            cv::imshow(win_name[i], data[i]);
          }

          // Leave
          char key = static_cast<char>(cv::waitKey(5));
          if (key == 'q') {
            process = false;
          }
        }
      } else {
        std::cout << "Error, can not load face tracker" << std::endl;
      }
      // Clean up readers
      for (int i = 0; i < reader.size(); ++i) {
        reader[i]->release();
        delete reader[i];
      }
      // clean up tracker
      if (tracker) {
        delete tracker;
      }
    } else {
      err = -1;
      std::cout << "Error, no video loaded" << std::endl;
    }
  } else {
    std::cout << "Error, unable to parse cmd line" << std::endl;
  }
  return err;
}
