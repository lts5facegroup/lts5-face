/**
 *  @file   test_lbf_tracker.hpp
 *  @brief  LBF tracker example
 *
 *  @author Christophe Ecabert
 *  @date   13/03/17
 *  Copyright (c) 2017 Ecabert Christophe. All rights reserved.
 */

#include <iostream>
#include <fstream>

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/face_tracker/lbf_tracker.hpp"
#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"

int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face tracker model");
  parser.AddArgument("-f",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face detector model");
  parser.AddArgument("-i",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Input : video file OR \"webcam\"");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Get param
    std::string tracker_model;
    std::string f_detect_model;
    std::string input;
    parser.HasArgument("-t", &tracker_model);
    parser.HasArgument("-f", &f_detect_model);
    parser.HasArgument("-i", &input);

    // Load input
    cv::VideoCapture cap;
    if (input == "webcam") {
      cap.open(0);
    } else {
      cap.open(input);
    }
    std::vector<std::string> images = LTS5::ScanImagesInFolder(input);
    if (cap.isOpened() || !images.empty()) {
      // Load tracker
      int cnt = 0;
      LTS5::LBFTracker* tracker = new LTS5::LBFTracker();
      err = tracker->Load(tracker_model, f_detect_model);
      tracker->set_score_threshold(-1.0);
      if (!err) {
        bool process = true;
        cv::Mat frame;
        cv::Mat shape;
        cv::Mat canvas;
        while ((cap.grab() || !images.empty()) && process) {
          // Load image
          if (images.empty()) {
            cap.retrieve(frame);
          } else {
            frame = cv::imread(images[cnt++]);
            process = (cnt != images.size());
          }
          // Track
          tracker->Track(frame, &shape);
          // Display
          if (!shape.empty()) {
            canvas = tracker->draw_shape(frame, shape, CV_RGB(0, 255, 0));
            /*cv::Mat truth;
            LTS5::LoadPts("test/image_0847.pts", truth);
            for (int i = 0; i < 68; ++i) {
              cv::circle(canvas,
                         cv::Point(truth.at<double>(i),
                                   truth.at<double>(i + 68)),
                         1,
                         CV_RGB(255, 255, 0));
            }*/
          } else {
            canvas = frame;
          }
          cv::imshow("Tracking", canvas);
          char key = static_cast<char>(cv::waitKey(0));
          if (key == 'q') {
            process = false;
            err = 0;
          }
        }
      } else {
        std::cout << "Error : Can not load face tracker" << std::endl;
      }
      // Clean up
      delete tracker;
      cap.release();
    } else {
      err = -1;
      std::cout << "Error : Unable to open input : " << input << std::endl;
    }
  } else {
    std::cout << "Unable to parse command line" << std::endl;
    parser.PrintHelp();
  }
  return err;
}
