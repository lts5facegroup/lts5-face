/**
 *  @file   test_sdm_trainer.cpp
 *  @brief
 *
 *  @author Gabriel Cuendet
 *  @date   24/04/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#include <iostream>
#include <unordered_set>

#include "opencv2/core/core.hpp"
#include "lts5/face_tracker/sdm_trainer.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/utils/object_type.hpp"
#include "lts5/face_detector/cascade_face_detector.hpp"
#include "lts5/face_detector/partsbased_face_detector.hpp"

int main(int argc, const char * argv[]) {

  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] <<
    " image_folder model_file [face_boundingbox_file]" << std::endl;
    return -1;
  }

  // @TODO: (Gabriel) Implement the option to provide face bounding boxes

  std::unordered_set<int> indices;
  bool flip = false;
  LTS5::HeaderObjectType assessment_type = LTS5::HeaderObjectType::kSvmTrackerAssessment;


  LTS5::SdmTrainer* trainer = new LTS5::SdmTrainer("models/haarcascade_frontalface_alt2.xml");
  LTS5::CascadeFaceDetector::CascadeDetectorParameters detector_params;
  detector_params.flags = cv::CASCADE_SCALE_IMAGE;

  //LTS5::SdmTrainer* trainer = new LTS5::SdmTrainer();
  //LTS5::PartsBasedFaceDetector face_detector("models/Face_99filters.xml");
  LTS5::SdmTracker::SdmTrackerParameters tracker_params;
  tracker_params.train_computation_mod = LTS5::kCula ;
  tracker_params.montecarlo_offsetX_std_scaling = 0.697948729;//0.56314551;
  tracker_params.montecarlo_offsetY_std_scaling = 0.692167981;//0.49415904;
  tracker_params.montecarlo_scale_std_scaling = 0.568526811; //0.38307425;
  tracker_params.montecarlo_sampling_attempt = 10000;
  // tracker_params.montecarlo_init_random = -1;

  //trainer->set_face_detector(&face_detector);
  trainer->set_training_parameters(tracker_params);
  trainer->Train(argv[1], flip, indices, assessment_type);
  int error = trainer->Save(argv[2]);
  std::cout << "Training finish with : "
            << (error == 0 ? "Success" : "Error") << std::endl;

  delete trainer;
  return 0;
}
