//
//  test_cpr_trainer.cpp
//  LTS5-Lib
//
//  Created by Christophe Ecabert on 30/10/15.
//  Copyright © 2015 Ecabert Christophe. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <string>

#include "opencv2/core/core.hpp"

#include "lts5/face_tracker/cpr_trainer.hpp"
#include "lts5/utils/file_io.hpp"


#include "lts5/utils/cmd_parser.hpp"

/**
 *  @name PrintUsage
 *  @fn void PrintUsage(const int argc, const char * argv[])
 *  @brief  Print how to use executable
 *  @param[in]  argc  Number of args
 *  @param[in]  argv  List of args
 */
void PrintUsage(const int argc, const char * argv[]);

/**
 *  @name   ReadNode
 *  @brief  Search a key in a given node and output its value if present
 *  @param[in]  node    Node to search in
 *  @param[in]  key     Key to search for
 *  @param[out] value   Value associated for the given key
 */
//template<class T>
//void ReadNode(const cv::FileNode& node, const std::string& key, T* value);

int main(int argc, const char * argv[]) {
  if (argc != 3) {
    PrintUsage(argc, argv);
    return -1;
  }
  // Parse config
  cv::FileStorage storage(argv[1], cv::FileStorage::READ);
  if (!storage.isOpened()) {
    std::cout << "Unable to open configuration" << std::endl;
    return -1;
  }
  // Config
  std::string root_folder;
  int n_fern_fst_lvl = -1;
  int n_fern_sec_lvl = -1;
  int n_candidate_pixel = -1;
  int n_feat_fern = -1;
  double shrinkage_coef = -1.0;
  int n_data_augmentation = -1;
  int error = -1;
  std::string f_detector_condig;
  // Get root node
  cv::FileNode root = storage.getFirstTopLevelNode();
  LTS5::ReadNode(root, "data_folder", &root_folder);
  LTS5::ReadNode(root, "n_fern_fst_lvl", &n_fern_fst_lvl);
  LTS5::ReadNode(root, "n_fern_sec_lvl", &n_fern_sec_lvl);
  LTS5::ReadNode(root, "n_candidate", &n_candidate_pixel);
  LTS5::ReadNode(root, "n_feat_fern", &n_feat_fern);
  LTS5::ReadNode(root, "shrinkage_coef", &shrinkage_coef);
  LTS5::ReadNode(root, "n_data_augmentation", &n_data_augmentation);
  LTS5::ReadNode(root, "f_detector", &f_detector_condig);
  storage.release();

  // Create trainer
  LTS5::CPRTrainer* cpr = new LTS5::CPRTrainer(f_detector_condig);
  // train
  error = cpr->Train(root_folder,
                     n_fern_fst_lvl,
                     n_fern_sec_lvl,
                     n_candidate_pixel,
                     n_feat_fern,
                     shrinkage_coef,
                     n_data_augmentation);
  // Save
  if (!error) {
    cpr->Save(argv[2]);
  }
  // Cleaning
  delete cpr;

  return error;
}

/*
 *  @name PrintUsage
 *  @fn void PrintUsage(const int argc, const char * argv[])
 *  @brief  Print how to use executable
 *  @param[in]  argc  Number of args
 *  @param[in]  argv  List of args
 */
void PrintUsage(const int argc, const char * argv[]) {
  std::cout << "Usage : " << argv[0] << " <ConfigFile> <ModelName>" << std::endl;
  std::cout <<"<ConfigFile> Configuration file (.yaml)" << std::endl;
  std::cout <<"<ModelName> Model name" << std::endl;
}

/*
 *  @name   ReadNode
 *  @brief  Search a key in a given node and output its value if present
 *  @param[in]  node    Node to search in
 *  @param[in]  key     Key to search for
 *  @param[out] value   Value associated for the given key
 */
/*template<class T>
void ReadNode(const cv::FileNode& node, const std::string& key, T* value) {
  if (!node[key].empty()) {
    node[key] >> *value;
  }
}*/