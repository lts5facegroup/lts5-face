/**
 *  @file   test_head_pose_tracker.cpp
 *  @brief  Track head pose
 *
 *  @author Christophe Ecabert
 *  @date   11/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/file_io.hpp"

int main(int argc, const char * argv[]) {

  // Handle executable arguments
  LTS5::CmdLineParser parser;
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face tracker model");
  parser.AddArgument("-f",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face detector model");
  parser.AddArgument("-p",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Path to Point Distribution Model");
  parser.AddArgument("-i",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Input : video file, folder OR \"webcam\"");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Folder where to place the result of the tracking");
  parser.AddArgument("-th",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Tracker threshold");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Get param
    std::string tracker_model;
    std::string f_detect_model;
    std::string pose_model;
    std::string input, output, thresh;
    parser.HasArgument("-t", &tracker_model);
    parser.HasArgument("-f", &f_detect_model);
    parser.HasArgument("-p", &pose_model);
    parser.HasArgument("-i", &input);
    parser.HasArgument("-th", &thresh);
    parser.HasArgument("-o", &output);

    // Check if output is needed
    std::ofstream writer;
    if (!output.empty()) {
      // Write header
      writer.open(output.c_str(), std::ios_base::out);
      if (writer.is_open()) {
        writer << "ImagePath,Pitch,Yaw,Roll" << std::endl;
      } else {
        LTS5_LOG_WARNING("Unable to create file in : " << output);
      }
    }

    // Try to open video
    cv::VideoCapture cap;
    if (input == "webcam") {
      cap.open(0);
    } else {
      cap.open(input);
    }
    // Scan for images as well and check what we got
    std::vector<std::string> images = LTS5::ScanImagesInFolder(input, true);
    if (cap.isOpened() || !images.empty()) {
      // Load stuff
      LTS5::SdmTracker* tracker = new LTS5::SdmTracker();
      err = tracker->Load(tracker_model, f_detect_model);
      if (!thresh.empty()) {
        double thr = std::atof(thresh.c_str());
        tracker->set_score_threshold(thr);
      }
      LTS5::PointDistributionModel* pdm = new LTS5::PointDistributionModel();
      err |= pdm->Load(pose_model);
      if (!err) {
        // Start processing
        bool process = true;
        cv::Mat frame, shape;
        double roll = 0.0, pitch = 0.0, yaw = 0.0;
        size_t cnt = 0;
        std::string fname;
        while ((cap.grab() || !images.empty()) && process) {
          // Load data
          if (images.empty()) {
            fname = std::to_string(cnt);
            std::cout << "Load : Frame #" << fname << std::endl;
            cap.retrieve(frame);
            ++cnt;
          } else {
            fname = images[cnt++];
            std::cout << "Load : " << fname << std::endl;
            frame = cv::imread(fname);
            process = (cnt != images.size());
          }
          // Convert
          if (frame.channels() != 1) {
            cv::cvtColor(frame, frame, cv::COLOR_BGR2GRAY);
          }
          // Track
          tracker->Track(frame, &shape);
          if (shape.empty()) {
            tracker->Detect(frame, &shape);
            std::cout << "RESET " << tracker->get_score() << std::endl;
          }

          if (!shape.empty()) {
            // head pose
            pdm->CalcPoseAngle(shape, &pitch, &yaw, &roll);
            // Output
            std::cout << "Pose : Pitch " << pitch;
            std::cout << " Yaw " << yaw;
            std::cout << " Roll " << roll << std::endl;
            // Dump into file ?
            if (writer.is_open()) {
              writer << fname << "," << pitch << "," << yaw << ",";
              writer << roll << std::endl;
            }
          } else {
            // Dump into file ?
            if (writer.is_open()) {
              double v = std::numeric_limits<double>::quiet_NaN();
              writer << fname << "," << v << "," << v << ",";
              writer << v << std::endl;
            }
          }
        }
      } else {
        LTS5_LOG_ERROR("Unable to load face tracker and/or pose estimator");
      }
      delete tracker;
      delete pdm;
    } else {
      err = -1;
      LTS5_LOG_ERROR("Unable to open : " << input);
    }
    // Clean up
    cap.release();
    if (writer.is_open()) {
      writer.close();
    }
  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
  return err;
}