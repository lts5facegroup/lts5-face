//
//  test_cpr_tracker.cpp
//  LTS5-Lib
//
//  Created by Christophe Ecabert on 09/11/15.
//  Copyright © 2015 Ecabert Christophe. All rights reserved.
//

#include <stdio.h>
#include <iostream>

#include "opencv2/highgui/highgui.hpp"

#include "lts5/face_tracker/cpr_tracker.hpp"

int main(int argc, const char * argv[]) {
  if (argc != 4) {
    std::cout << argv[0] << " <CPRModel> <FDetector> <ImagePath>" << std::endl;
    return -1;
  }

  // Create Tracker
  LTS5::CPRTracker* tracker = new LTS5::CPRTracker();
  int error = tracker->Load(argv[1], argv[2]);
  if (!error) {
    // Load image
    cv::Mat img = cv::imread(argv[3]);
    if (!img.empty()) {
      // Detect
      cv::Mat shape;
      tracker->Detect(img, &shape);


      if (!shape.empty()) {
        cv::Mat canvas = LTS5::CPRTracker::DrawShape(img, shape);
        cv::imshow("Shape", canvas);
        cv::waitKey();
      }
    }



  } else {
    std::cout << "Unable to load face tracker ..." << std::endl;
  }


  return error;
}