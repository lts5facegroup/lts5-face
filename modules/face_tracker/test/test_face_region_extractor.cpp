/**
 *  @file   test_face_region_extractor.cpp
 *  @brief  Testing target for face region extraction from video. Region and
 *          facial landmarks are exported into .csv file. Aligned face is also
 *          saved as image sequence
 *  @author Christophe Ecabert
 *  @date   06/10/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>
#include <iostream>

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/sys/file_system.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"

/**
 *  @struct ExtractorPath
 *  @brief  Path to models
 */
struct ExtractorPath {
  /** Path to sdm model */
  std::string sdm_model_path;
  /** Path to face detector model */
  std::string face_detect_model_path;
  /** Path to PDM model */
  std::string pdm_model_path;
  /** Export image flag */
  int export_image;
};

/**
 *  @name printUsage
 *  @fn void printUsage(int argc, const char * argv[])
 *  @brief  Display how the executable should be called
 *  @param[in]  argc  Number of parameters
 *  @param[in]  argv  List of parameters
 */
void printUsage(int argc, const char * argv[]);

/**
 *  @name   ReadNode
 *  @brief  Search a key in a given node and output its value if present
 *  @param[in]  node    Node to search in
 *  @param[in]  key     Key to search for
 *  @param[out] value   Value associated for the given key
 */
template<class T>
void ReadNode(const cv::FileNode& node, const std::string& key, T* value);

/**
 *  @name ParseConfiguration
 *  @fn int ParseConfiguration(const std::String& config_file,
                               ExtractorPath* extractor_config)
 *  @brief  Parse configuration
 *  @param[in]  config_file       Configuration file
 *  @param[out] extractor_config  Structure with all path to different model
 */
int ParseConfiguration(const std::string& config_file,
                       ExtractorPath* extractor_config);






int main(int argc, const char * argv[]) {
  // Test if correctly called
  if (argc != 4) {
    printUsage(argc, argv);
    return -1;
  }

  // Ok, parse config file
  ExtractorPath extractor_path;
  int error = ParseConfiguration(argv[2], &extractor_path);
  if (!error) {
    // Instantiate SDM
    auto* face_tracker = new LTS5::SdmTracker();
    error = face_tracker->Load(extractor_path.sdm_model_path,
                               extractor_path.face_detect_model_path);
    face_tracker->set_score_threshold(-0.6);
    // Instantiate PDM
    LTS5::PointDistributionModel::PDMParameters pdm_parameters;
    auto* face_alignment = new LTS5::PointDistributionModel();
    error |= face_alignment->Load(extractor_path.pdm_model_path);
    pdm_parameters.normalize_image_width = 400;
    pdm_parameters.normalize_image_height = 400;
    pdm_parameters.normalize_face_scale = 7.0;
    // Define entries
    std::string input_name = std::string(argv[1]);

    auto* fs = LTS5::GetDefaultFileSystem();
    bool is_dir = fs->IsDirectory(input_name).Good();
    std::vector<std::string> image_path;
    if (is_dir) {
      image_path = LTS5::ScanImagesInFolder(input_name, true);
    }
    std::string output_folder = std::string(argv[3]);
    if (!fs->FileExist(output_folder).Good()) {
      fs->CreateDirRecursively(output_folder);
    }
    // Ok ?
    if (!error) {
      face_alignment->set_pdm_configuration(pdm_parameters);
      // done
      std::cout << "Extractor loaded ! " << std::endl;
      // Try to open video and report file
      cv::VideoCapture cap(input_name);
      std::ofstream out_stream(output_folder + "/_region_data.csv");
      if ((cap.isOpened() || !image_path.empty()) && out_stream.is_open()) {
        // Video is open
        cv::Mat frame;
        int n_frame = 0;
        cv::Mat working_image;
        cv::Mat shape_68pt;
        cv::Mat norm_working_image;
        cv::Mat norm_shape;
        // Loop until video is done
        while (cap.grab() || n_frame < image_path.size()) {
          // Still frames
          std::cout << "Process frame #" << n_frame << std::endl;
          if (is_dir) {
            frame = cv::imread(image_path[n_frame]);
          } else {
            cap.retrieve(frame);
          }
          // Convert to greyscale
          if (frame.channels() != 1) {
            cv::cvtColor(frame, working_image, cv::COLOR_BGR2GRAY);
          } else {
            working_image = frame;
          }
          // Track
          face_tracker->Track(working_image, &shape_68pt);
          if (!face_tracker->is_tracking()) {
            face_tracker->Detect(working_image, &shape_68pt);
          }

          // Alignment
          if (!shape_68pt.empty()) {
            face_alignment->NormSDMImage(working_image,
                                         shape_68pt,
                                         false,
                                         &norm_working_image,
                                         &norm_shape);
            // Define ROI
            int n_point = norm_shape.rows / 2;
            // Front
            double front_roi_h = 30.0;
            cv::Rect_<double> front_roi(norm_shape.at<double>(19),
                                        norm_shape.at<double>(19 + n_point) -
                                        10.0 - front_roi_h,
                                        norm_shape.at<double>(24) -
                                        norm_shape.at<double>(19),
                                        front_roi_h);

            // Left
            double nose_margin = 20.0;
            double left_w = (norm_shape.at<double>(31) - nose_margin -
                             norm_shape.at<double>(4));
            double left_h = (norm_shape.at<double>(50 + n_point) -
                             norm_shape.at<double>(29 + n_point));
            cv::Rect_<double> left_roi(norm_shape.at<double>(4),
                                       norm_shape.at<double>(29 + n_point),
                                       left_w,
                                       left_h);
            // Right
            double right_w = (norm_shape.at<double>(12) - nose_margin -
                              norm_shape.at<double>(35));
            double right_h = (norm_shape.at<double>(52 + n_point) -
                              norm_shape.at<double>(29 + n_point));
            cv::Rect_<double> right_roi(norm_shape.at<double>(35) + nose_margin,
                                        norm_shape.at<double>(29 + n_point),
                                        right_w,
                                        right_h);

            // DUMP INTO FILES
            if (n_frame == 0) {
              // Write header
              out_stream << "#,image_name,front_x,y,w,h,left_x,y,w,h,right_x,y,w,h";
              for (int i = 0; i < n_point; ++i) {
                out_stream << ",,,,";
              }
              out_stream << std::endl;
            }
            // Write normalized image
            std::string num = std::to_string(n_frame);
            std::string img_name = ("frame_" + std::string("00000000",
                                                           8 - num.length()) +
                                    num + ".bmp");
            if ((extractor_path.export_image & 0x01) == 0x01) {
              // Export norm image
              cv::imwrite(output_folder + img_name, norm_working_image);
            }
            if ((extractor_path.export_image & 0x02) == 0x02) {
              // Export input image
              cv::imwrite(output_folder + img_name, working_image);
            }
            // Frame number
            out_stream << n_frame << "," << img_name << ",";
            // Front
            out_stream << front_roi.x << "," << front_roi.y << ",";
            out_stream << front_roi.width << "," << front_roi.height << ",";
            // Left
            out_stream << left_roi.x << "," << left_roi.y << ",";
            out_stream << left_roi.width << "," << left_roi.height << ",";
            // Right
            out_stream << right_roi.x << "," << right_roi.y << ",";
            out_stream << right_roi.width << "," << right_roi.height;
            // Pts norm
            for (int i = 0; i < n_point; ++i) {
              out_stream << "," << norm_shape.at<double>(i);
              out_stream << "," << norm_shape.at<double>(i + n_point);
            }
            // Pts raw
            for (int i = 0; i < n_point; ++i) {
              out_stream << "," << shape_68pt.at<double>(i);
              out_stream << "," << shape_68pt.at<double>(i + n_point);
            }
            out_stream << std::endl;
            out_stream.flush();

            /*
             cv::Mat canvas = facetracker->draw_shape(norm_working_image,
             norm_shape,
             CV_RGB(0, 255, 0));
             // Front
             cv::rectangle(canvas, front_roi, CV_RGB(0, 255, 0));
             // Left
             cv::rectangle(canvas, left_roi, CV_RGB(0, 255, 0));
             // Right
             cv::rectangle(canvas, right_roi, CV_RGB(0, 255, 0));
             cv::imshow("DBG", canvas);
             cv::waitKey();
             */
          } else {
            std::cout << "Unable to track frame : " << n_frame << std::endl;
          }
          n_frame++;
        }
        // Close everything
        cap.release();
        out_stream.close();
      } else {
        std::cout << "Unable to open video/report file : " <<
        input_name << " / " <<
        output_folder + "/region_data.csv" <<
        std::endl;
        error = -2;
      }
    } else {
      std::cout << "Unable to load SDM/PDM ! " << std::endl;
    }
    // Clean up !
    delete face_tracker;
    delete face_alignment;
    // Done
    std::cout << "Done : " << (error == 0 ? "SUCCESS" : "ERROR") << std::endl;
    return error;
  }
}

/*
 *  @name printUsage
 *  @fn void printUsage(int argc, const char * argv[])
 *  @brief  Display how the executable should be called
 *  @param[in]  argc  Number of parameters
 *  @param[in]  argv  List of parameters
 */
void printUsage(int argc, const char * argv[]) {
  std::cout << "Usage : " << std::endl;
  std::cout << argv[0] << " <Source> <ConfigFile> <OutputFolder>" << std::endl;
  std::cout << "<Source> Video source file / Image folder" << std::endl;
  std::cout << "<ConfigFile> Configuration file (.yaml)" << std::endl;
  std::cout << "<OutputFolder> Where to ouput generated files" << std::endl;
}

/*
 *  @name   ReadNode
 *  @brief  Search a key in a given node and output its value if present
 *  @param[in]  node    Node to search in
 *  @param[in]  key     Key to search for
 *  @param[out] value   Value associated for the given key
 */
template<class T>
void ReadNode(const cv::FileNode& node, const std::string& key, T* value) {
  if (!node[key].empty()) {
    node[key] >> *value;
  }
}

/*
 *  @name ParseConfiguration
 *  @fn int ParseConfiguration(const std::String& config_file,
                               ExtractorPath* extractor_config)
 *  @brief  Parse configuration
 *  @param[in]  config_file       Configuration file
 *  @param[out] extractor_config  Structure with all path to different model
 */
int ParseConfiguration(const std::string& config_file,
                       ExtractorPath* extractor_config) {
  // Open file
  int error = -1;
  cv::FileStorage storage(config_file, cv::FileStorage::READ);
  if (storage.isOpened()) {
    // Get root
    cv::FileNode root = storage.getFirstTopLevelNode();
    // Read config, SDM
    ReadNode(root, "sdm_model", &(extractor_config->sdm_model_path));
    // PDM
    ReadNode(root, "pdm_model", &(extractor_config->pdm_model_path));
    // Face detector
    ReadNode(root, "face_detect_model", &(extractor_config->
                                          face_detect_model_path));
    // Face export
    ReadNode(root, "export_image", &(extractor_config->export_image));
    // Done
    error = 0;
  }
  return error;
}
