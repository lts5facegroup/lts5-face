/**
 *  @file   face_cropping.cpp
 *  @brief  Executable used to crop images from face alignment dataset.
 *          Generate cropped images with their respective ground truth
 *
 *  @author Christophe Ecabert
 *  @date   01/05/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <cstdio>

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/utils/sys/file_system.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"


int main(int argc, const char * argv[]) {

  // Cmd parser
  using ArgState = LTS5::CmdLineParser::ArgState;
  LTS5::CmdLineParser parser;
  // Input folder
  parser.AddArgument("-i",
                     ArgState::kNeeded,
                     "Input image folder with annotation");
  // Meanshape
  parser.AddArgument("-p",
                     ArgState::kNeeded,
                     "Point distribution model");
  // Cropped image dimensions h x w
  parser.AddArgument("-d",
                     ArgState::kNeeded,
                     "Cropped image dimension + scale : 'HxWsXXdxXXdyYY'");
  // Cropped image dimensions h x w
  parser.AddArgument("-f",
                     ArgState::kOptional,
                     "Flip the images");
  // Output folder
  parser.AddArgument("-o",
                     ArgState::kNeeded,
                     "Output folder");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Query values + check validity
    std::string in_folder, pdm_path, dimension_str, output_folder, flipped;
    parser.HasArgument("-i", &in_folder);
    parser.HasArgument("-p", &pdm_path);
    parser.HasArgument("-d", &dimension_str);
    parser.HasArgument("-o", &output_folder);
    parser.HasArgument("-f", &flipped);

    // Check dimension
    LTS5::PointDistributionModel::PDMParameters pdm_param;
    int n = sscanf(dimension_str.c_str(),
                   "%dx%ds%lfdx%lfdy%lf",
                   &pdm_param.normalize_image_height,
                   &pdm_param.normalize_image_width,
                   &pdm_param.normalize_face_scale,
                   &pdm_param.extra_face_shift_x,
                   &pdm_param.extra_face_shift_y);
    if (n == 5) {
      // Scan for images
      std::vector<std::string> images = LTS5::ScanImagesInFolder(in_folder,
                                                                 true);
      if (!images.empty()) {
        // Load PDM
        LTS5::PointDistributionModel pdm;
        err = pdm.Load(pdm_path);
        pdm.set_pdm_configuration(pdm_param);
        if (!err) {
          // Check if destination exist
          auto sz = output_folder.length();
          if (sz != 0 && output_folder[sz - 1] != '/') {
            output_folder += "/";
          }
          auto* fs = LTS5::GetDefaultFileSystem();
          if (!fs->FileExist(output_folder).Good()) {
            fs->CreateDirRecursively(output_folder);
          }
          // Result holder
          std::vector<std::string> img_list;
          std::vector<cv::Mat> shape_list;

          // Loop over all images
          std::string dir, file, ext;
          int n_pts = 0;
          for (int i = 0; i < images.size(); ++i) {
            // Load points
            const std::string& img_name = images[i];
            LTS5::ExtractDirectory(img_name, &dir, &file, &ext);
            std::string pts_file = dir + file + ".pts";
            cv::Mat pts;
            LTS5::LoadPts(pts_file, pts);
            n_pts = std::max(pts.cols, pts.rows) / 2;
            // Load image
            cv::Mat img = cv::imread(img_name);
            // Crop
            cv::Mat cropped_img, transformed_annotation;
            pdm.NormSDMImage(img,
                             pts,
                             false,
                             &cropped_img,
                             &transformed_annotation);

            if (!cropped_img.empty() && !transformed_annotation.empty()) {
              // Save image
              cv::imwrite(output_folder + file + "_align.png", cropped_img);
              // Save image name, annotation
              img_list.push_back(file + "_align.png");
              shape_list.push_back(transformed_annotation.clone());
            } else {
              LTS5_LOG_ERROR("Can not crop : " << img_name);
            }

            // Generate flip
            if (!flipped.empty() && !cropped_img.empty()) {
              cv::Mat flip_cropped_img;
              cv::flip(cropped_img, flip_cropped_img, 1);
              cv::Size sz(flip_cropped_img.cols, flip_cropped_img.rows);
              cv::Mat flip_shape = LTS5::FlipShape(transformed_annotation, sz);
              // Save image
              cv::imwrite(output_folder + file + "_flip_align.png",
                          flip_cropped_img);
              // Save image name, annotation
              img_list.push_back(file + "_flip_align.png");
              shape_list.push_back(flip_shape.clone());
            } else {
              LTS5_LOG_ERROR("Can not flip : " << img_name);
            }
          }

          // Dump results
          std::string label_f = output_folder + "/label_align.txt";
          std::ofstream stream(label_f.c_str());
          if (stream.is_open()) {
            // Dump number of sample
            stream << img_list.size() << std::endl;
            // Dump header
            for (int p = 0; p < n_pts; ++p) {
              stream << "x" + std::to_string(p);
              stream << " y" + std::to_string(p) + " ";
            }
            stream << std::endl;
            // Dump data
            for (int p = 0; p < img_list.size(); ++p) {
              stream << img_list[p];
              const cv::Mat& s = shape_list[p];
              for (int k = 0; k < n_pts; ++k) {
                int x = static_cast<int>(s.at<double>(k));
                int y = static_cast<int>(s.at<double>(k + n_pts));
                stream << " " << x << " " << y;
              }
              stream << std::endl;
            }



            // Done
            stream.close();
          } else {
            err = -1;
            LTS5_LOG_ERROR("Can not open file : " << label_f);
          }
        } else {
          LTS5_LOG_ERROR("Ccan not load point distribution model");
        }
      } else {
        err = -1;
        LTS5_LOG_ERROR("No images founded in " << in_folder);
      }
    } else {
      err = -1;
      LTS5_LOG_ERROR("Image dimension is not correct !");
    }
  } else {
    LTS5_LOG_WARNING("Unable to parse command line");
  }
  return err;
}
