/**
 *  @file   test_sdm_tracker.hpp
 *  @brief  SDM tracker exmaple
 *
 *  @author Christophe Ecabert
 *  @date   22/04/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <iostream>
#include <fstream>

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/utils/sys/file_system.hpp"

int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face tracker model");
  parser.AddArgument("-f",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face detector model");
  parser.AddArgument("-i",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Input : video file OR \"webcam\"");
  parser.AddArgument("-thr",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Tracker threshold");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Folder where to place the result of the tracking");
  parser.AddArgument("-p",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Path to Point Distribution Model (Head pose estimation)");
  parser.AddArgument("-sel",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Range selection, format : 'start;stop'");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Get param
    std::string tracker_model;
    std::string f_detect_model;
    std::string input, output, pose, selection, thresh;
    parser.HasArgument("-t", &tracker_model);
    parser.HasArgument("-f", &f_detect_model);
    parser.HasArgument("-i", &input);
    parser.HasArgument("-thr", &thresh);
    parser.HasArgument("-o", &output);
    parser.HasArgument("-p", &pose);
    parser.HasArgument("-sel", &selection);

    // Check if output is needed
    std::ofstream pts_writer;
    if (!output.empty()) {
      // Init folder
      output = output.back() != '/' ? output + "/" : output;
      auto* fs = LTS5::GetDefaultFileSystem();
      if (!fs->FileExist(output).Good()) {
        fs->CreateDirRecursively(output);
        fs->CreateDirRecursively(output + "images/");
      }
      // Init file + write header
      auto fname = output + "label.csv";
      pts_writer.open(fname.c_str(), std::ios_base::out);
      if (pts_writer.is_open()) {
        pts_writer << "ImageName;";
        pts_writer << "Frame#;";
        for (int i = 0; i < 68; ++i) {
          auto idx = std::to_string(i);
          pts_writer << "x" << idx << ";y" << idx << ";";
        }
        // Pose ?
        if (!pose.empty()) {
          pts_writer << "Pitch;Yaw;Roll;";
        }
        pts_writer << std::endl;
      } else {
        LTS5_LOG_WARNING("Can not open output file : " << fname);
      }
    }
    // Only range ?
    int start = -1, stop = -1;
    if (!selection.empty()) {
      std::vector<std::string> parts;
      LTS5::SplitString(selection, ";", &parts);
      if (!parts.empty()) {
        start = atoi(parts[0].c_str());
        stop = atoi(parts[1].c_str());
      }
    }

    // Load input
    cv::VideoCapture cap;
    if (input == "webcam") {
      cap.open(0);
      start = -1;
      stop = -1; // Selection not allowed when using webcam.
    } else {
      cap.open(input);
    }
    std::vector<std::string> images = LTS5::ScanImagesInFolder(input);
    if (cap.isOpened() || !images.empty()) {
      // Load tracker
      int cnt = 0;
      LTS5::SdmTracker* tracker = new LTS5::SdmTracker();
      err = tracker->Load(tracker_model, f_detect_model);
      if (!thresh.empty()) {
        double thr = std::atof(thresh.c_str());
        tracker->set_score_threshold(thr);
      }
      // Head pose if needed
      LTS5::PointDistributionModel* pdm = nullptr;
      if (!pose.empty()) {
        pdm = new LTS5::PointDistributionModel();
        err |= pdm->Load(pose);
      }
      if (!err) {
        bool process = true;
        cv::Mat frame;
        cv::Mat shape;
        cv::Mat canvas;
        std::size_t frame_cnt = 0;
        // need subselection ?
        if (start != -1 && stop != -1) {
          if (images.empty()) {
            while (cnt++ < (start - 1)) cap.grab();
          } else {
            int sz = static_cast<int>(images.size());
            cnt = start < sz ? start : sz - 1;
            stop = stop < sz ? stop : sz;
          }
        } else {
          stop = images.empty() ? -1 : static_cast<int>(images.size());
        }
        // Process
        while ((cap.grab() || !images.empty()) && process) {
          // Load image
          if (images.empty()) {
            cap.retrieve(frame);
            ++cnt;
            if (stop != -1) {
              process = cnt != stop;
            }
          } else {
            frame = cv::imread(images[cnt++]);
            process = (cnt != stop);
          }
          // Track
          auto t0 = cv::getTickCount();


          tracker->Track(frame, &shape);

          auto t1 = cv::getTickCount();

          std::cout << "Time : " << ((t1 - t0) * 1000.0)/cv::getTickFrequency() << " ms" << std::endl;

          // Head pose
          double roll = 0.0, pitch = 0.0, yaw = 0.0;
          if (pdm && !shape.empty()) {
            pdm->CalcPoseAngle(shape, &pitch, &yaw, &roll);
          }
          // Write output ?
          if (pts_writer.is_open()) {
            // Image name
            auto num = std::to_string(frame_cnt);
            auto img_name = ("images/frame_" +
                             std::string(6 - num.length(), '0') + num + ".png");
            pts_writer << img_name << ";";
            // Frame#
            pts_writer << std::to_string(cnt) << ";";
            // Pts
            int n_pts = std::max(shape.rows, shape.cols) / 2;
            bool shape_valid = n_pts == 68;
            for (int i = 0; i < 68; ++i) {
                pts_writer << (shape_valid ? shape.at<double>(i) : -1.0) << ";";
                pts_writer << (shape_valid ?
                               shape.at<double>(i + n_pts) :
                               -1.0) << ";";
            }
            // Add pose
            if (pdm) {
              double nan = std::numeric_limits<double>::quiet_NaN();
              pts_writer << (shape.empty() ? nan : pitch) << ";";
              pts_writer << (shape.empty() ? nan : yaw) << ";";
              pts_writer << (shape.empty() ? nan : roll) << ";";
            }
            pts_writer << std::endl;
            // Save image
            cv::imwrite(output + img_name, frame);
          }
          // Display
          if (!shape.empty()) {
            canvas = tracker->draw_shape(frame, shape, CV_RGB(0, 255, 0));
          } else {
            canvas = frame;
          }
          cv::imshow("Tracking", canvas);
          char key = static_cast<char>(cv::waitKey(5));
          if (key == 'q') {
            process = false;
            err = 0;
          }
          ++frame_cnt;
        }

        // Cleaning
        if (pts_writer.is_open()) {
          pts_writer.close();
        }
      } else {
        LTS5_LOG_ERROR("Can not load face tracker");
      }
      // Clean up
      delete tracker;
      if (pdm) {
        delete pdm;
      }
      cap.release();
    } else {
      err = -1;
      LTS5_LOG_ERROR("Unable to open input : " << input);
    }
  } else {
    LTS5_LOG_INFO("Unable to parse command line");
    parser.PrintHelp();
  }
  return err;
}
