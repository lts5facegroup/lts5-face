#! /bin/bash
# Parse command line, see http://stackoverflow.com/questions/192249
if [[ $# -eq 0 ]]; then
	echo "Push documentation to LTS5 repository, option :"
	echo "	-a, --auth 			Autentification token"
	echo "	-b, --build 			Build folder"
	exit
fi

while [[ $# -gt 1 ]]
do
key="$1"
# List all possible entry here
case $key in
    -a|--auth)
    OAUTH_TOKEN="$2"
    shift # past argument
    ;;
    -b|--build)
    BUILD_DIR="$2"
    shift # past argument
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

echo "Token : $OAUTH_TOKEN"
echo "Build dir : $BUILD_DIR"

# Clone current doc
git clone "https://x-token-auth:$OAUTH_TOKEN@bitbucket.org/lts5facegroup/lts5facegroup.bitbucket.io.git" "$BUILD_DIR/DOC"
git remote set-url origin git@bitbucket.org:lts5facegroup/lts5facegroup.bitbucket.io.git
chmod 777 "$BUILD_DIR/DOC"
# Copy install doc
cp --recursive /root/Install/share/doc/lts5*/. $BUILD_DIR/DOC
# Commit 
cd $BUILD_DIR/DOC && git add . && git commit -m "CHG: Update online documentation" && git push origin/master