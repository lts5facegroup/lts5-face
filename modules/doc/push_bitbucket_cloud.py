# -*- coding: utf-8 -*-
import sys
import argparse
import subprocess
from subprocess import call
import shlex
import requests
__author__ = 'Christophe Ecabert'


if __name__ == '__main__':
    # Cmd line parser
    parser = argparse.ArgumentParser(description='Documentation deployment helper')
    # OAuth key
    parser.add_argument('-k',
                        type=str,
                        required=True,
                        dest='oauth_key',
                        help='Autentification Key')
    # OAuth secret
    parser.add_argument('-s',
                        type=str,
                        required=True,
                        dest='oauth_secret',
                        help='Authentification secret')
    # Installation location
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='install_dir',
                        help='Documentation installation directory')
    # Parse
    args = parser.parse_args()
    # Query authentification token, REST call
    auth = (args.oauth_key, args.oauth_secret)
    payload = {'grant_type': 'client_credentials'}
    addr = 'https://bitbucket.org/site/oauth2/access_token'
    r = requests.post(url=addr, auth=auth, data=payload).json()
    oauth_token = r['access_token']
    # Clone current doc
    print('Cloning doc repo')
    addr = 'https://x-token-auth:' + oauth_token + \
           '@bitbucket.org/lts5facegroup/lts5facegroup.bitbucket.io.git'
    cmd = shlex.split('git clone ' + addr + ' --depth=1 DOC')
    err = subprocess.Popen(cmd).wait()
    # Remove current doc
    print('Remove old documentation')
    cmd = 'rm -rf DOC/LTS5Doc/{search,*.png,*.css,*.js,*.html}'
    err |= subprocess.Popen(cmd, shell=True).wait()
    # Git config
    cmd = shlex.split('git config user.email "lts5_doc@epfl.ch"')
    err |= subprocess.Popen(cmd, cwd='DOC/LTS5Doc/').wait()
    cmd = shlex.split('git config user.name "LTS5 Doc"')
    err |= subprocess.Popen(cmd, cwd='DOC/LTS5Doc/').wait()
    # Copy
    print('Copying new doc')
    cmd = 'cp --recursive ' + args.install_dir + ' DOC/LTS5Doc'
    err |= subprocess.Popen(cmd, shell=True).wait()
     # Add changes
    print('Add changes')
    cmd = shlex.split('git add .')
    err |= subprocess.Popen(cmd, cwd='DOC/LTS5Doc/').wait()
    # Commit
    print('Commit changes')
    cmd = shlex.split('git commit -m "CHG: Update Documentation"')
    err |= subprocess.Popen(cmd, cwd='DOC/LTS5Doc/').wait()
    # Push
    print('Push online')
    cmd = shlex.split('git push -f origin master')
    err |= subprocess.Popen(cmd, cwd='DOC/LTS5Doc/').wait()
    # Done
    sys.exit(err)