## Overview 

Documentation for LTS5 C++ library.


## Dependencies

Two types of dependencies are treated within the LTS5 library, the one that can be built on the fly (*i.e. googletest, tinyxml*) and the one that need to be already built on the system (*i.e. OpenCV, Boost*).

TBD

## Quick Links

- Main website:  [lts5](http://lts5www.epfl.ch)
- Developer Zone : [bitbucket](https://bitbucket.org/lts5facegroup/lts5-face/)



## Contributors

- H. Gao [hua.gao@epfl.ch](mailto:hua.gao@epfl.ch)
- G. Cuendet [gabriel.cuendet@epfl.ch](mailto:gabriel.cuendet@epfl.ch)
- M. Zimmerman [marina.zimmermann@epfl.ch](mailto:marina.zimmermann@epfl.ch)
- C. Ecabert [christophe.ecabert@epfl.ch](mailto:christophe.ecabert@epfl.ch)

