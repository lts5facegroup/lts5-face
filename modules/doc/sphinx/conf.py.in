# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# Idiot's guide to python doucmentation with Sphinx
# https://samnicholls.net/2016/06/15/how-to-sphinx-readthedocs/

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import pt_lightning_sphinx_theme
sys.path.insert(0, os.path.abspath(os.path.join('@DOXYREST_SHARE_FOLDER@', 'sphinx')))
# Add python modules
lts5_modules = ['tensorflow_op', 'utils']
for m in lts5_modules:
  sys.path.insert(1, os.path.abspath(os.path.join('@CMAKE_SOURCE_DIR@', 'modules', m, 'script')))

# -- Project information -----------------------------------------------------

project = 'LTS5 Face Group Library'
copyright = '2022, LTS5 Contributors'
author = 'Hua Gao <email>, Gabriel Cuendet <email>, Marina Zimmermann <email>, Christophe Ecabert <email>'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['doxyrest', 
              'cpplexer',
              #'sphinx.ext.autodoc',
              #'sphinx.ext.autosummary',
              #'sphinx.ext.autosectionlabel',
              'sphinx.ext.mathjax',                     # Render math equation with MathJax
              'sphinx.ext.githubpages',                 # Generate for github pages
              'pt_lightning_sphinx_theme.extensions.lightning']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = None

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'pt_lightning_sphinx_theme'
html_theme_path = [pt_lightning_sphinx_theme.get_html_theme_path()]

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
html_theme_options = {
    "pytorch_project": "https://www.epfl.ch/labs/lts5",
    #"canonical_url": "https://www.epfl.ch/labs/lts5",
    "collapse_navigation": False,
    "display_version": True,
    "logo_only": True,
}

html_logo = "_static/images/logo.svg"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_templates', '_static']

# -- EXTENSIONS: sphinx.ext.autodoc -------------------------------------------------
autodoc_member_order = "groupwise"
autoclass_content = "both"
# the options are fixed and will be soon in release,
#  see https://github.com/sphinx-doc/sphinx/issues/5459
autodoc_default_options = {
    "members": None,
    "methods": None,
    # 'attributes': None,
    "special-members": "__call__",
    "exclude-members": "_abc_impl",
    "show-inheritance": True,
    "private-members": True,
    "noindex": True,
}
autosummary_generate = True

# Sphinx will add “permalinks” for each heading and description environment as paragraph signs that
#  become visible when the mouse hovers over them.
# This value determines the text for the permalink; it defaults to "¶". Set it to None or the empty
#  string to disable permalinks.
# https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-html_add_permalinks
html_permalinks = True
html_permalinks_icon = "¶"

# True to prefix each section label with the name of the document it is in, followed by a colon.
#  For example, index:Introduction for a section called Introduction that appears in document index.rst.
#  Useful for avoiding ambiguity when the same section heading appears in different documents.
# http://www.sphinx-doc.org/en/master/usage/extensions/autosectionlabel.html
autosectionlabel_prefix_document = True

def setup(app):
    # this is for hiding doctest decoration,
    # see: http://z4r.github.io/python/2011/12/02/hides-the-prompts-and-output/
    app.add_js_file("copybutton.js")
    app.add_css_file("main.css")