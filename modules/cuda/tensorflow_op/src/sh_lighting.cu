/**
 *  @file   sh_lighting.cu
 *  @brief  Spherical Harmonics Lighting
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/7/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include "cuda_runtime.h"
#include "cuda.h"

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"

#include "lts5/tensorflow_op/sh_lighting.hpp"
#include "lts5/tensorflow_op/sh_lighting_grad.hpp"
#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/cuda/utils/math/device_math.hpp"

#include "lts5/cuda/utils/safe_call.hpp"

using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Vector 3 */
template<typename T>
using Vec3 = typename MathTypes<T>::Vec3;
/** Const Vector 3 */
template<typename T>
using CVec3 = typename MathTypes<T>::ConstVec3;

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {


template<typename T, int N>
__device__ __forceinline__
Vec3<T> ComputeSHCoef(const T* sh_basis, const T* p, const bool& clipped) {
  using Math = CUDA::Math<T>;
  Vec3<T> sh_coef = Pack3(T(0.0));
  // Compute coefficient
  #pragma unroll
  for (int k = 0; k < N; ++k) {
    sh_coef.x += sh_basis[k] * p[k];
    sh_coef.y += sh_basis[k] * p[k + N];
    sh_coef.z += sh_basis[k] * p[k + (2 * N)];
  }
  if (clipped) {
    sh_coef.x = Math::Max(sh_coef.x, T(0.0));
    sh_coef.y = Math::Max(sh_coef.y, T(0.0));
    sh_coef.z = Math::Max(sh_coef.z, T(0.0));
  }
  return sh_coef;
}


/**
 * @name    ComputeSHCoef
 * @brief Compute spherical harmonic coefficient from normal + parameters
 * @param[in] n Surface normal
 * @param[in] p Illumination coefficients
 * @param[in] clipped   True indicate value will be clipped
 * @return  SH coefficient
 * @tparam T    Data type
 */
template<typename T>
__device__ __forceinline__
Vec3<T> ComputeSHCoef(CVec3<T>& n, const T* p, const bool& clipped) {
  // Define basis
  const T sh_basis[9] = {T(1.0),
                         n.x,
                         n.y,
                         n.z,
                         n.x * n.y,
                         n.x * n.z,
                         n.y * n.z,
                         (n.x * n.x) - (n.y * n.y),
                         (T(3.0) * n.z * n.z) - T(1.0)};
  return ComputeSHCoef<T, 9>(&sh_basis[0], p, clipped);
}


/**
 * @name    SHLightingKernel
 * @brief Compute SH lighting
 * @param[in] reflectance   Input color
 * @param[in] normal        Surface normals
 * @param[in] w_illu        Illumination parameters
 * @param[in] n_texel       Number of texel
 * @param[out] color        Color with illumination
 * @tparam T    Data type
 */
template<typename T>
__global__
void SHLightingKernel(CVec3<T>* reflectance,
                      CVec3<T>* normal,
                      const T* w_illu,
                      int n_texel,
                      Vec3<T>* color) {
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;

  if (ix >= n_texel) return;

  // Compute SH coefficients
  const auto& r = reflectance[iy * n_texel + ix];
  const auto& n = normal[iy * n_texel + ix];
  const T* wi_ptr = &(w_illu[iy * 27]);
  auto sh_coef = ComputeSHCoef<T>(n, wi_ptr, true);
  // Apply lighting
  color[iy * n_texel + ix] = Pack<T>(r.x * sh_coef.x,
                                     r.y * sh_coef.y,
                                     r.z * sh_coef.z);
}

/**
 * @name    SHLightingGradCKernel
 * @brief Compute SH Lighting gradient (reflectance, normal)
 * @param[in] g_color       Back-propagated gradient
 * @param[in] reflectance   Reflectance array
 * @param[in] normal        Normal array
 * @param[in] w_illu        Illumination parameters array
 * @param[in] n_texel       Number of texel
 * @param[out] grad_color   Grad with respect to reflectance
 * @param[out] grad_normal  Grad with respect to normals
 * @tparam T    Data type
 */
template<typename T>
__global__
void SHLightingGradCKernel(CVec3<T>* g_color,
                           CVec3<T>* reflectance,
                           CVec3<T>* normal,
                           const T* w_illu,
                           int n_texel,
                           Vec3<T>* grad_color,
                           Vec3<T>* grad_normal) {
  using Math = CUDA::Math<T>;
  // Indices
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;  // [0, N]
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;  // [0, K] K == BSize

  // Check overflow
  if (ix >= n_texel) return;

  // Compute gradient related to 'reflectance' and 'normal'
  CVec3<T> r = reflectance[(iy * n_texel) + ix];    // Reflectance
  CVec3<T> n = normal[(iy * n_texel) + ix];         // Normal
  CVec3<T>& gcolor = g_color[(iy * n_texel) + ix];  // Back-propagated grad
  const T* wi = &(w_illu[(iy * 27)]);
  // Compute sh_basis
  const T sh_basis[] = {T(1.0),
                        n.x,
                        n.y,
                        n.z,
                        n.x * n.y,
                        n.x * n.z,
                        n.y * n.z,
                        (n.x * n.x) - (n.y * n.y),
                        (T(3.0) * n.z * n.z) - T(1.0)};
  CVec3<T> sh_coef = ComputeSHCoef<T, 9>(sh_basis, wi, false);
  Vec3<T> sh_coef_clip = Pack<T>(Math::Max(sh_coef.x, T(0.0)),
                                 Math::Max(sh_coef.y, T(0.0)),
                                 Math::Max(sh_coef.z, T(0.0)));
  // With respect to normals
  // grad_norm = r * d(max(...)) * d(phi(n)wi)
  // d(max(...)) = ... > 0.0 ? 1.0 : 0.0
  const T d_basis_dn[] {T(0.0), T(1.0), T(0.0), T(0.0), n.y, n.z, T(0.0), n.x + n.x, T(0.0),
                        T(0.0), T(0.0), T(1.0), T(0.0), n.x, T(0.0), n.z, -(n.y + n.y), T(0.0),
                        T(0.0), T(0.0), T(0.0), T(1.0), T(0.0), n.x, n.y, T(0.0), T(6.0) * n.z};
  Vec3<T> dmax_dn = Pack<T>(sh_coef.x > T(0.0) ? T(1.0) : T(0.0),
                            sh_coef.y > T(0.0) ? T(1.0) : T(0.0),
                            sh_coef.z > T(0.0) ? T(1.0) : T(0.0));
  Vec3<T> dphi_dn[3];
  for (int c = 0; c < 3; ++c) {
    dphi_dn[c] = Pack3(T(0.0));  // each row is the derivative with respect to nx, ny, nz
    for (int k = 0; k < 9; ++k) {
      dphi_dn[c].x += d_basis_dn[(c * 9) + k] * wi[k];       // d{r,g,b} / d_n{x,y,z}
      dphi_dn[c].y += d_basis_dn[(c * 9) + k] * wi[9 + k];
      dphi_dn[c].z += d_basis_dn[(c * 9) + k] * wi[18 + k];
    }
  }
  // Stack grad:  grad = dphi_dn @ (dmax_dn * (r * gcolor))
  CVec3<T> g0 = dmax_dn * (r  * gcolor);
  Vec3<T> g1;
  g1.x = dot(dphi_dn[0], g0);
  g1.y = dot(dphi_dn[1], g0);
  g1.z = dot(dphi_dn[2], g0);
  grad_normal[(iy * n_texel) + ix] = g1;
  // With respect to reflectance
  grad_color[(iy * n_texel) + ix] = sh_coef_clip * gcolor;
}

template<typename T>
__global__
void SetValue(const T value, const int n, T* array) {
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (ix < n) {
    array[ix] = value;
  }
}

/**
 * @name    SHLightingGradPKernel
 * @brief Compute SH Lighting gradient (parameters)
 * @param[in] g_color       Back-propagated gradient
 * @param[in] reflectance   Reflectance array
 * @param[in] normal        Normal array
 * @param[in] w_illu        Illumination parameters array
 * @param[in] n_texel       Number of texel
 * @param[in] n_illu        Number of illumination parameters
 * @param[out] grad_wi      Grad with respect to illumination parameters
 * @tparam T    Data type
 * @tparam Bs   Block size
 */
template<typename T>
__global__
void SHLightingGradPKernel(CVec3<T>* g_color,
                           CVec3<T>* reflectance,
                           CVec3<T>* normal,
                           const T* w_illu,
                           int n_texel,
                           int n_illu,
                           T* grad_wi) {
  using Math = CUDA::Math<T>;
  // Indices
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;  // [0, N]
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;  // [0, Ni]
  int ik = (blockIdx.z * blockDim.z) + threadIdx.z;  // [0, K] K == BSize

  // Check boundaries
  if (ix >= n_texel || iy >= n_illu) return;

  // Compute gradient related to 'reflectance' and 'normal'
  CVec3<T>& r = reflectance[(ik * n_texel) + ix];    // Reflectance
  CVec3<T>& n = normal[(ik * n_texel) + ix];         // Normal
  CVec3<T>& gcolor = g_color[(ik * n_texel) + ix];  // Back-propagated grad
  const T* wi = &(w_illu[(ik * n_illu)]);
  // Compute sh_basis
  const T sh_basis[] = {T(1.0),
                        n.x,
                        n.y,
                        n.z,
                        n.x * n.y,
                        n.x * n.z,
                        n.y * n.z,
                        (n.x * n.x) - (n.y * n.y),
                        (T(3.0) * n.z * n.z) - T(1.0)};
  CVec3<T> sh_coef = ComputeSHCoef<T, 9>(sh_basis, wi, false);
  // With respect to illumination parameters
  // grad_w = d(phi(n)wi)_dwi * (dmax_dwi * r * gcolor)
  // d(max(...)) = ... > 0.0 ? 1.0 : 0.0
  Vec3<T> dmax_dx = Pack<T>(sh_coef.x > T(0.0) ? T(1.0) : T(0.0),
                            sh_coef.y > T(0.0) ? T(1.0) : T(0.0),
                            sh_coef.z > T(0.0) ? T(1.0) : T(0.0));
  Vec3<T> dphi_dwi = Pack<T>(0.0);
  T* dphi_dwi_ptr = &(dphi_dwi.x);
  int n_band = iy / 9;
  int c_idx = iy % 9;
  dphi_dwi_ptr[n_band] = sh_basis[c_idx];

  // Stack grad:  grad = dphi_dn @ (dmax_dn * (r * gcolor))
  T g = dot(dphi_dwi, (dmax_dx * (r * gcolor)));
  atomicAdd(&(grad_wi[(ik * n_illu) + iy]), g);
}

template<typename T, int Bsz>
__global__
void SHLightingGradPKernelV2(CVec3<T>* g_color,
                             CVec3<T>* reflectance,
                             CVec3<T>* normal,
                             const T* w_illu,
                             int n_texel,
                             int n_illu,
                             T* grad_wi) {
  using Math = CUDA::Math<T>;
  // Indices
  int tidx = threadIdx.x;
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;  // [0, N]
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;  // [0, K] K == BSize
//  int ik = (blockIdx.z * blockDim.z) + threadIdx.z;  // [0, K] K == BSize

  __shared__ T grad_component[Bsz];
  grad_component[tidx] = T(0.0);  // Initialize grad accumulator

  // Check boundaries
  if (ix >= n_texel /*|| iy >= n_illu*/) return;

  CVec3<T> r = reflectance[(iy * n_texel) + ix];    // Reflectance
  CVec3<T> n = normal[(iy * n_texel) + ix];         // Normal
  const T* wi = &(w_illu[(iy * n_illu)]);           // Illumination parameters

  // Compute sh_basis
  const T sh_basis[] = {T(1.0),
                        n.x,
                        n.y,
                        n.z,
                        n.x * n.y,
                        n.x * n.z,
                        n.y * n.z,
                        (n.x * n.x) - (n.y * n.y),
                        (T(3.0) * n.z * n.z) - T(1.0)};
  CVec3<T> sh_coef = ComputeSHCoef<T, 9>(sh_basis, wi, false);

  // With respect to illumination parameters
  // grad_w = d(phi(n)wi)_dwi * (dmax_dwi * r * gcolor)
  // d(max(...)) = ... > 0.0 ? 1.0 : 0.0
//  Vec3<T> dmax_dx = Pack<T>(sh_coef.x > T(0.0) ? T(1.0) : T(0.0),
//                            sh_coef.y > T(0.0) ? T(1.0) : T(0.0),
//                            sh_coef.z > T(0.0) ? T(1.0) : T(0.0));
  CVec3<T> gcolor = g_color[(iy * n_texel) + ix];  // Back-propagated grad
//  CVec3<T> gpart = (dmax_dx * (r * gcolor));
  CVec3<T> gpart = (r * gcolor);
  Vec3<T> dphi_dwi = Pack3(T(0.0));
  T* dphi_dwi_ptr = &(dphi_dwi.x);
  for (int i = 0; i < n_illu; ++i) {
    int n_band = i / 9;
    int c_idx = i % 9;
    dphi_dwi_ptr[n_band] = sh_basis[c_idx];
    // Stack grad:  grad = dphi_dn @ (dmax_dn * (r * gcolor))
    grad_component[tidx] = dot(dphi_dwi, gpart);
    __syncthreads();
    // Every thread in block have compute dphi_dwi. Do reduction in shared
    // memory for this component
    // Do reduction in shared memory
    for (unsigned int s = blockDim.x / 2; s > 0; s >>=1) {
      if (tidx < s) {
        grad_component[tidx] += grad_component[tidx + s];
      }
      __syncthreads();
    }

    // Add everything into global memory
    if (tidx == 0) {
      atomicAdd(&(grad_wi[(iy * n_illu) + i]), grad_component[0]);
    }
    // Set entry back to zero for further processing
    dphi_dwi_ptr[n_band] = T(0.0);
  }
}


}  // namespace device


/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/*
 * @class   SHLightingHelper
 * @brief   Compute rotation matrix using rodrigues formula
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam T        Data type
 */
template<typename T>
struct SHLightingHelper<GPUDevice, T> {
  /*
   * @name  operator()
   * @brief Compute lighting approximation
   * @param[in] ctx     Op's context
   * @param[in] aldebo  Reflectance without lighting
   * @param[in] normal  Surface's normals
   * @param[in] w_illu  Illumination parameters (SH weights)
   * @param[out] color  Color with illumination applied on it
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* aldebo,
                 const tf::Tensor* normal,
                 const tf::Tensor* w_illu,
                 tf::Tensor* color) {
    // Acces raw pointer
    const auto* r_ptr = reinterpret_cast<CVec3<T>*>(aldebo->flat<T>().data());
    const auto* n_ptr = reinterpret_cast<CVec3<T>*>(normal->flat<T>().data());
    const auto* wi_ptr = w_illu->flat<T>().data();
    auto* c_ptr = reinterpret_cast<Vec3<T>*>(color->flat<T>().data());
    // Dimensions
    int bsize = static_cast<int>(aldebo->dim_size(0));
    int n_texel = static_cast<int>(aldebo->dim_size(1) / 3);
    dim3 thd(1024, 1);
    dim3 bck((n_texel + 1023) / 1024, bsize);
    auto& stream = ctx->eigen_gpu_device().stream();
    device::SHLightingKernel<T><<<bck, thd, 0, stream>>>(r_ptr,
                                                         n_ptr,
                                                         wi_ptr,
                                                         n_texel,
                                                         c_ptr);
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

/**
 * @class   SHLightingGradientHelper
 * @brief   Compute SH lighting gradient
 * @author  Christophe Ecabert
 * @date    15/07/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct SHLightingGradientHelper<GPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute SH lighting gradient
   * @param[in] ctx     Op's context
   * @param[in] g_color Back-propagated gradient
   * @param[in] aldebo  Reflectance without lighting
   * @param[in] normal  Surface's normals
   * @param[in] w_illu  Illumination parameters (SH weights)
   * @param[out] grad_color   Chained gradient with respect to color
   * @param[out] grad_normal  Chained gradient with respect to surface's normal
   * @param[out] grad_w       Chained gradient with respect to w_illu
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_color,
                 const tf::Tensor* aldebo,
                 const tf::Tensor* normal,
                 const tf::Tensor* w_illu,
                 tf::Tensor* grad_color,
                 tf::Tensor* grad_normal,
                 tf::Tensor* grad_w) {
    // Acces raw pointer
    const auto* gc_ptr = reinterpret_cast<CVec3<T>*>(g_color->flat<T>().data());
    const auto* r_ptr = reinterpret_cast<CVec3<T>*>(aldebo->flat<T>().data());
    const auto* n_ptr = reinterpret_cast<CVec3<T>*>(normal->flat<T>().data());
    const auto* wi_ptr = w_illu->flat<T>().data();
    auto* g_col_ptr = reinterpret_cast<Vec3<T>*>(grad_color->flat<T>().data());
    auto* g_nrm_ptr = reinterpret_cast<Vec3<T>*>(grad_normal->flat<T>().data());
    auto* g_w_ptr = grad_w->flat<T>().data();
    // Dimensions
    int bsize = static_cast<int>(aldebo->dim_size(0));
    int n_texel = static_cast<int>(aldebo->dim_size(1) / 3);
    int n_illu = static_cast<int>(w_illu->dim_size(1));  // == 27
    auto& stream = ctx->eigen_gpu_device().stream();
    {  // Reflectance + normal gradient
      dim3 thd(1024, 1);
      dim3 bck((n_texel + 1023) / 1024, bsize);
      device::SHLightingGradCKernel<T> <<<bck, thd, 0, stream>>>(gc_ptr,
              r_ptr,
              n_ptr,
              wi_ptr,
              n_texel,
              g_col_ptr,
              g_nrm_ptr);
    }
    {  // Paramters gradient
      dim3 thd(1024, 1, 1);
      dim3 bck((n_texel + 1023) / 1024, bsize, 1);
      int n_value = bsize * n_illu;
      device::SetValue<T><<<n_value, 1, 0, stream>>>(T(0.0), n_value, g_w_ptr);
      device::SHLightingGradPKernelV2<T, 1024><<<bck, thd, 0, stream>>>(gc_ptr,
              r_ptr,
              n_ptr,
              wi_ptr,
              n_texel,
              n_illu,
              g_w_ptr);
    }
    // Sanity check
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

#pragma mark -
#pragma mark Explicit Instantiation

/** Float */
template struct SHLightingHelper<GPUDevice, float>;
template struct SHLightingGradientHelper<GPUDevice, float>;
/** Double */
#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
template struct SHLightingHelper<GPUDevice, double>;
template struct SHLightingGradientHelper<GPUDevice, double>;
#endif

}  // namespace Functor
}  // namespace LTS5

