/**
 *  @file   renderer_op.cu
 *  @brief  RenderingOp
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   22/7/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include "cuda_runtime.h"
#include "cuda.h"

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"

#include "lts5/tensorflow_op/renderer_op_grad.hpp"
#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/cuda/utils/math/device_math.hpp"


using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Vector 3 */
template<typename T>
using Vec3 = typename MathTypes<T>::Vec3;
/** Const Vector 3 */
template<typename T>
using CVec3 = typename MathTypes<T>::ConstVec3;
/** Const Vector 3 int */
using CVec3i = typename MathTypes<int>::ConstVec3;
/** Const Vector 4 */
template<typename T>
using CVec4 = typename MathTypes<T>::ConstVec4;

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {


template<typename T>
__global__
void RenderingGradientKernel(CVec3<T>* g_image,
                             CVec3<T>* vertex,
                             CVec3<T>* color,
                             CVec3i* triangle,
                             CVec4<T>* bcoord,
                             CVec3<T>* g_spatial,
                             const T focal,
                             const int height,
                             const int width,
                             const int n_vertex,
                             Vec3<T>* grad_vertex,
                             Vec3<T>* grad_color) {
  using Math = CUDA::Math<T>;

  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;   // [0 - W]
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;   // [0 - H]
  int ik = (blockIdx.z * blockDim.z) + threadIdx.z;   // [0 - Batch]

  // Check boundaries
  if (ix >= width || iy >= height) return;

  // Define pointer for current slice (ik)
  const int wh = width * height;
  const int p_idx = iy * width + ix;            // Current pixel flat index
  const auto* v_ptr = &(vertex[ik * n_vertex]);    // Vertex
  const auto* gi_ptr = &(g_image[ik * wh]);        // Back-prop grad
  const auto* bc_ptr = &(bcoord[ik * wh]);         // Barycentric coordinates
  const auto* gx_ptr = &(g_spatial[(2 * ik) * wh]);         // Spatial grad X
  const auto* gy_ptr = &(g_spatial[((2 * ik) + 1) * wh]);   // Spatial grad Y

  auto* gv_ptr = &(grad_vertex[(ik * n_vertex)]);  // dImage_dVertex
  auto* gc_ptr = &(grad_color[(ik * n_vertex)]);   // dImage_dColor

  // Get bcoords for pixel p(iy, ix)
  const auto& bc = bc_ptr[p_idx];
  int tri_idx = static_cast<int>(Math::Round(bc.x));
  if (tri_idx > 0) {
    // Inside a triangle
    tri_idx -= 1;
    CVec3i tri = triangle[tri_idx];
    // Back-propagated grad
    CVec3<T> bgrad = gi_ptr[p_idx];
    // Image plane spatial gradient
    CVec3<T> gx = gx_ptr[p_idx];
    CVec3<T> gy = gy_ptr[p_idx];
    const int* tri_ptr = &(tri.x);
    const T* bc_comp_ptr = &(bc.y);

//    printf("(%02d, %03d, %03d) gx: %.3f, %.3f, %.3f\n", tri_idx, iy, ix, gx.x, gx.y, gx.z);
//    printf("(%02d, %03d, %03d) gy: %.3f, %.3f, %.3f\n", tri_idx, iy, ix, gy.x, gy.y, gy.z);

    for (int k = 0; k < 3; ++k) {
      int m = tri_ptr[k];
      // Color gradient
      // t = alpha * t0 + beta * t1 + gamma * t2
      //    dt_dt0 = alpha * bgrad
      //    dt_dt1 = beta * bgrad
      //    dt_dt1 = gamma * bgrad
      auto g_color = bc_comp_ptr[k] * bgrad;
      atomicAdd(&(gc_ptr[m].x), g_color.x);
      atomicAdd(&(gc_ptr[m].y), g_color.y);
      atomicAdd(&(gc_ptr[m].z), g_color.z);
      // Vertex gradient
      //  v = alpha * v0 + beta * v1 + gamma * v2
      CVec3<T> v = v_ptr[m];
//      Vec3<T> du_dx = Pack<T>(0.0); // Grad proj
//      du_dx.x = bc_comp_ptr[k] * (focal / v.z);
//      du_dx.z = -bc_comp_ptr[k] * ((focal * v.x) / (v.z * v.z));
//      Vec3<T> dv_dx = Pack<T>(0.0); // Grad proj
//      dv_dx.y = bc_comp_ptr[k] * (focal / v.z);
//      dv_dx.z = -bc_comp_ptr[k] * ((focal * v.y) / (v.z * v.z));

      Vec3<T> du_dx = Pack3(T(0.0)); // Grad proj
      du_dx.x = -bc_comp_ptr[k] * (focal / v.z);
      du_dx.z = bc_comp_ptr[k] * ((focal * v.x) / (v.z * v.z));
      Vec3<T> dv_dx = Pack3(T(0.0)); // Grad proj
      dv_dx.y = bc_comp_ptr[k] * (focal / v.z);
      dv_dx.z = -bc_comp_ptr[k] * ((focal * v.y) / (v.z * v.z));




      // Jacob
      Mat3x3<T> J(gx.x * du_dx.x + gy.x * dv_dx.x,    // dr_dx
                  gx.y * du_dx.x + gy.y * dv_dx.x,    // dg_dx
                  gx.z * du_dx.x + gy.z * dv_dx.x,    // db_dx
                  gx.x * du_dx.y + gy.x * dv_dx.y,    // dr_dy
                  gx.y * du_dx.y + gy.y * dv_dx.y,    // dg_dy
                  gx.z * du_dx.y + gy.z * dv_dx.y,    // db_dy
                  gx.x * du_dx.z + gy.x * dv_dx.z,    // dr_dz
                  gx.y * du_dx.z + gy.y * dv_dx.z,    // dg_dz
                  gx.z * du_dx.z + gy.z * dv_dx.z);   // db_dz
      auto g_vertex = J * bgrad;


      atomicAdd(&(gv_ptr[m].x), g_vertex.x);
      atomicAdd(&(gv_ptr[m].y), g_vertex.y);
      atomicAdd(&(gv_ptr[m].z), g_vertex.z);
    }
  }
}



/**
 * @name    SetValue
 * @brief Initialize array to a given value
 * @param[in] value Value to set
 * @param[in] n     Array dimensions
 * @param[out] gvertex  Vertex array
 * @param[out] gcolor   Color array
 * @tparam T    Data type
 */
template<typename T>
__global__
void SetValue(const T value, const int n, Vec3<T>* gvertex, Vec3<T>* gcolor) {
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;
  if (ix < n) {
    gvertex[(iy * n) + ix] = Pack3(value);
    gcolor[(iy * n) + ix] = Pack3(value);
  }
}

}  // namespace device


/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   RenderingGradientHelper
 * @brief   Compute renderer gradient with respecto to vertex/color
 * @author  Christophe Ecabert
 * @date    22/07/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct RenderingGradientHelper<GPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute renderer gradient
   * @param[in] ctx             Op's context
   * @param[in] g_image         Back propagated gradient
   * @param[in] vertex          Input vertex
   * @param[in] color           Input color
   * @param[in] triangle        Surface's triangulation
   * @param[in] bcoord          Barycentric coordinates
   * @param[in] spatial_grad    Image spatial gradient
   * @param[in] focal           Focal length
   * @param[out] grad_vertex    Grad dImage_dVertex
   * @param[out] grad_color     Grad dImage_dColor
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_image,
                 const tf::Tensor* vertex,
                 const tf::Tensor* color,
                 const tf::Tensor* triangle,
                 const tf::Tensor* bcoord,
                 const tf::Tensor* spatial_grad,
                 const T& focal,
                 tf::Tensor* grad_vertex,
                 tf::Tensor* grad_color) {
    // Acces raw pointer
    auto* gi_ptr = reinterpret_cast<CVec3<T>*>(g_image->template flat<T>().data());
    auto* v_ptr = reinterpret_cast<CVec3<T>*>(vertex->template flat<T>().data());
    auto* c_ptr = reinterpret_cast<CVec3<T>*>(color->template flat<T>().data());
    auto* bc_ptr = reinterpret_cast<CVec4<T>*>(bcoord->template flat<T>().data());
    auto* tri_ptr = reinterpret_cast<CVec3i*>(triangle->template flat<int>().data());
    auto* sp_ptr = reinterpret_cast<CVec3<T>*>(spatial_grad->template flat<T>().data());
    auto* g_v_ptr = reinterpret_cast<Vec3<T>*>(grad_vertex->template flat<T>().data());
    auto* g_c_ptr = reinterpret_cast<Vec3<T>*>(grad_color->template flat<T>().data());
    // Dimensions
    int n_img = vertex->dim_size(0);
    int n_vertex = vertex->dim_size(1) / 3;
    int height = g_image->dim_size(1);
    int width = g_image->dim_size(2);
    auto& stream = ctx->eigen_gpu_device().stream();
    { // Set to zero
      dim3 thd(1024, 1);
      dim3 bck((n_vertex + 1023) / 1024, n_img);
      device::SetValue<T><<<bck, thd, 0, stream>>>(T(0.0),
              n_vertex,
              g_v_ptr,
              g_c_ptr);
    }
    { // Compute grad
      dim3 thd(32, 32, 1);
      dim3 bck((height + 31) / 32, (width + 31) / 32, n_img);
      device::RenderingGradientKernel<T><<<bck, thd, 0, stream>>>(gi_ptr,
              v_ptr,
              c_ptr,
              tri_ptr,
              bc_ptr,
              sp_ptr,
              focal,
              height,
              width,
              n_vertex,
              g_v_ptr,
              g_c_ptr);
    }
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};


#pragma mark -
#pragma mark Explicit Instantiation

/** Float */
template struct RenderingGradientHelper<GPUDevice, float>;
/** Double */
#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
template struct RenderingGradientHelper<GPUDevice, double>;
#endif

}  // namespace Functor
}  // namespace LTS5

