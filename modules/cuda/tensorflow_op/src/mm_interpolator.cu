/**
 *  @file   mm_interpolator.cu
 *  @brief  Morphable model Interpolator class. Compute pixel's barycentric
 *          coordinates on the image
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   5/3/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/cuda/tensorflow_op/mm_interpolator.hpp"
#include "lts5/cuda/tensorflow_op/mm_renderer_utils.hpp"
#include "lts5/cuda/utils/math/device_math.hpp"
#include "lts5/cuda/utils/safe_call.hpp"

#include "cuda.h"
#include "cuda_gl_interop.h"


#include "lts5/utils/process_error.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/matrix.hpp"
#include "lts5/utils/math/quaternion.hpp"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @name    ShuffleKernel
 * @brief Reshuffle vertex/normal with corresponding triangulation
 * @tparam T    Data type
 * @param[in] tri    Buffer holding the triangulation
 * @param[in] buffer Data to reshuffle
 * @param[in] N      Number of triangle
 * @param[out] out   Buffer storing the transformed input
 */
template<typename T>
__global__ void ShuffleKernel(typename MathTypes<int>::ConstVec3* tri,
                              typename MathTypes<T>::ConstVec3* buffer,
                              const int N,
                              typename MathTypes<T>::Vec3* out) {
  using Vec3i = typename MathTypes<int>::Vec3;
  using Vec3 = typename MathTypes<T>::Vec3;
  // Get index
  int idx =  blockIdx.x * blockDim.x + threadIdx.x;
  // Do computation
  if (idx < N) {
    // Read tri
    const Vec3i& t = tri[idx];
    // Read vertices
    out[3 * idx] = buffer[t.x];
    out[(3 * idx) + 1] = buffer[t.y];
    out[(3 * idx) + 2] = buffer[t.z];
  }
}

/*
 * @name  operator()
 * @fn    int operator()(tensorflow::OpKernelContext* ctx, const Tensor* buffer,
               const Tensor* triangle, T* ogl_buffer)
 * @brief Reorganise a buffer from a shared topology to a splitted one based
 *        on a triangulation
 * @param[in] ctx Op context
 * @param[in] buffer
 * @param[in] triangle
 * @param[out] ogl_buffer
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int MMShuffleOglBuffer<GPUDevice, T>::operator()(tf::OpKernelContext* ctx,
                                                 const Tensor* buffer,
                                                 const Tensor* triangle,
                                                 T* ogl_buffer) {
  using CVec3i = typename MathTypes<int>::ConstVec3;
  using CVec3 = typename MathTypes<T>::ConstVec3;
  using Vec3 = typename MathTypes<T>::Vec3;
  DCHECK_EQ(buffer->dim_size(0), 1);
  // Access pointer
  const auto* tri_ptr = reinterpret_cast<CVec3i*>(triangle->flat<int>().data());
  const auto* src_ptr = reinterpret_cast<CVec3*>(buffer->unaligned_flat<T>().data());
  auto* dst_ptr = reinterpret_cast<Vec3*>(ogl_buffer);
  // Launch kernel
  const int N = triangle->dim_size(0);
  const auto& device = ctx->eigen_gpu_device();
  const int thd = 1024;
  const int bck = (buffer->dim_size(1) + 1023) / 1024;
  ShuffleKernel<T><<<bck, thd, 0, device.stream()>>>(tri_ptr, src_ptr, N, dst_ptr);
  // Done
  return 0;
}
}  // namespace Functor

/** Vertex Shader */
static constexpr const char* vs_str_ridig =
        "#version 330\n"
        "layout (location = 0) in vec3 vertex;\n"
        "layout (location = 1) in vec3 normal;\n"
        "layout (location = 2) in vec2 tcoord;\n"
        "layout (std140) uniform Transform {\n"
        "  mat4 model;\n"
        "  mat4 projection;\n"
        "};\n"
        "out VS_OUT {\n"
        "  vec3 normal0;\n"
        "  flat vec2 tcoord0;\n"
        "} vs_out;\n"
        "void main() {\n"
        "  gl_Position = projection * model * vec4(vertex, 1.f);\n"
        "  vs_out.normal0 = normal;\n"
        "  vs_out.tcoord0 = tcoord;\n"
        "}";
static constexpr const char* vs_str =
        "#version 330\n"
        "layout (location = 0) in vec3 vertex;\n"
        "layout (location = 1) in vec3 normal;\n"
        "layout (location = 2) in vec2 tcoord;\n"
        "uniform mat4 projection;\n"
        "out VS_OUT {\n"
        "  vec3 normal0;\n"
        "  flat vec2 tcoord0;\n"
        "} vs_out;\n"
        "void main() {\n"
        "  gl_Position = projection * vec4(vertex, 1.f);\n"
        "  vs_out.normal0 = normal;\n"
        "  vs_out.tcoord0 = tcoord;\n"
        "}";

/** Geometry shader */
static constexpr const char* gs_str =
        "#version 330\n"
        "layout (triangles) in;\n"
        "layout (triangle_strip, max_vertices = 3) out;\n"
        "in VS_OUT {\n"
        "  vec3 normal0;\n"
        "  flat vec2 tcoord0;\n"
        "} gs_in[];\n"
        "out vec3 bary0;\n"
        "out vec3 normal1;\n"
        "flat out vec2 tcoord1;\n"
        "void main() {\n"
        "  for (int i = 0; i < 3; ++i) {\n"
        "    gl_Position = gl_in[i].gl_Position;\n"
        "    normal1 = gs_in[i].normal0;\n"
        "    tcoord1 = gs_in[i].tcoord0;\n"
        "    bary0 = vec3(0.f);\n"
        "    bary0[i] = 1.f;\n"
        "    EmitVertex();\n"
        "  }\n"
        "  EndPrimitive();\n"
        "}";

/** Fragment shader - GPU */
static constexpr const char* fs_str =
        "#version 330\n"
        "in vec3 bary0;\n"
        "in vec3 normal1;\n"
        "flat in vec2 tcoord1;\n"
        "out vec4 fragColor;\n"
        "void main() {\n"
        "  fragColor = vec4(tcoord1.x, bary0.x, bary0.y, bary0.z);\n"
        "}";

#pragma mark -
#pragma mark Device code

/*
 * @name  MMInterpShader
 * @fn    MMInterpShader()
 * @brief Constructor
 */
template<typename T>
MMInterpShader<GPUDevice, T>::
MMInterpShader(const MMRendererParameters<T>& p,
               const bool& with_rigid) : shader_(nullptr),
                                         ubo_(nullptr),
                                         resource_(nullptr),
                                         focal_(p.focal),
                                         has_rigid_(with_rigid) {
  using Mat4 = Matrix4x4<T>;
  try {
    std::vector<OGLShader> shaders;
    auto vs = has_rigid_ ? vs_str_ridig : vs_str;
    shaders.emplace_back(vs, OGLShader::ShaderType::kVertex);
    shaders.emplace_back(gs_str, OGLShader::ShaderType::kGeometry);
    shaders.emplace_back(fs_str, OGLShader::ShaderType::kFragment);
    // Define attributes
    using Attributes = OGLProgram::Attributes;
    using Types = OGLProgram::AttributeType;
    std::vector<Attributes> attributes;
    attributes.emplace_back(Types::kPoint, 0, "vertex");
    attributes.emplace_back(Types::kNormal, 1, "normal");
    attributes.emplace_back(Types::kTexCoord, 2, "tcoord");
    shader_ = new OGLProgram(shaders, attributes);
  } catch (const LTS5::ProcessError &e) {
    std::string msg = "OpenGL failed to initialize shader: ";
    msg += std::string(e.what());
    throw LTS5::ProcessError(ProcessError::ProcessErrorEnum::kErr,
                             msg,
                             FUNC_NAME);
  }
  if (has_rigid_) {
    // Define ubo binding point
    int err = this->shader_->SetUniformBlockBinding("Transform", 0);
    if (err) {
      LTS5_LOG_ERROR("Can not set UBO binding point");
    }
    // Create Uniform buffer
    ubo_ = new UniformBufferObject();
    err |= ubo_->Create({{0, 2 * 16 * sizeof(T)}});  // 2x[4x4] matrix of T
    if (!err) {
      // Add projection matrix since it has to be initialized once.
      // http://kgeorge.github.io/2014/03/08/calculating-opengl-perspective-matrix-from-opencv-intrinsic-matrix
      Mat4 P;  // Projection matrix
      T cx = static_cast<T>(p.width) / T(2.0);
      T cy = static_cast<T>(p.height) / T(2.0);
      P(0, 0) = p.focal / cx;
      P(1, 1) = p.focal / cy;
      P(2, 2) = -(p.far + p.near) / (p.far - p.near);
      P(2, 3) = -(T(2.0) * p.far * p.near) / (p.far - p.near);
      P(3, 2) = T(-1.0);
      auto Pt = P.Transpose();
      err = ubo_->AddData(0, 16 * sizeof(T), 16 * sizeof(T), Pt.Data());
      if (err) {
        LTS5_LOG_ERROR("Can not initialize projection matrix");
      }
      // Register resource to CUDA
      cudaSafeCall(cudaGraphicsGLRegisterBuffer(&resource_,
                                                ubo_->get_object(0),
                                                cudaGraphicsRegisterFlagsNone));
    } else {
      LTS5_LOG_ERROR("Can not initialize UBO");
    }
  } else {
    Mat4 P;  // Projection matrix
    T cx = static_cast<T>(p.width) / T(2.0);
    T cy = static_cast<T>(p.height) / T(2.0);
    P(0, 0) = p.focal / cx;
    P(1, 1) = p.focal / cy;
    P(2, 2) = -(p.far + p.near) / (p.far - p.near);
    P(2, 3) = -(T(2.0) * p.far * p.near) / (p.far - p.near);
    P(3, 2) = T(-1.0);
    shader_->Use();
    shader_->SetUniform("projection", P);
    shader_->StopUsing();
  }
}

/*
 * @name  ~MMInterpShader
 * @fn    ~MMInterpShader() override = default
 * @brief Destructor
 */
template<typename T>
MMInterpShader<GPUDevice, T>::~MMInterpShader() {
  if (resource_) {
    // Unregister
    cudaSafeCall(cudaGraphicsUnregisterResource(resource_));
  }
  delete ubo_;
  delete shader_;
}

/*
 * @name  UpdateModelTransform
 * @fn    int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                                   const tensorflow::Tensor& parameter,
                                   const tensorflow::int64& idx)
 * @brief Update model transformation matrix from camera parameters
 * @param[in] ctx Op context
 * @param[in] parameter Morphable model parameters
 * @param[in] idx Index where camera parameters start
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MMInterpShader<GPUDevice, T>::
UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                     const tensorflow::Tensor& parameter,
                     const tensorflow::int64& idx) {
  // Map uniform
  T* ubo_ptr;
  size_t buff_sz = 0;
  auto& stream = ctx->eigen_gpu_device().stream();
  // Map OpenGL Buffer for writing
  cudaSafeCall(cudaGraphicsMapResources(1, &resource_, stream));
  cudaSafeCall(cudaGraphicsResourceGetMappedPointer((void**)&ubo_ptr,
                                                    &buff_sz,
                                                    resource_));
  // Update data from parameters
  const T* p_ptr = parameter.unaligned_flat<T>().data();
  p_ptr = &p_ptr[idx];
  device::UpdateModelTransform<T><<<1, 16, 0, stream>>>(p_ptr, focal_, ubo_ptr);
  // Unmap resource
  cudaSafeCall(cudaGraphicsUnmapResources(1, &resource_, stream));
  // Done
  return 0;
}


#pragma mark -
#pragma mark Explicit Instantiation

/** MMShuffleOglBuffer - GPU - float */
template class Functor::MMShuffleOglBuffer<GPUDevice, float>;
/** MMShuffleOglBuffer - GPU - double */
template class Functor::MMShuffleOglBuffer<GPUDevice, double>;

/** Interpolation shader - Float */
template class MMInterpShader<GPUDevice, float>;
/** Interpolation shader - Float */
template class MMInterpShader<GPUDevice, double>;

}  // namespace LTS5