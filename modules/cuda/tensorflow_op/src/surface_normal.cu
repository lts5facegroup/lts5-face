/**
 *  @file   surface_normal.cpp
 *  @brief  Compute surface's normals of a given "mesh"
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/8/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include "cuda_runtime.h"
#include "cuda.h"

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"

#include "lts5/tensorflow_op/surface_normal.hpp"
#include "lts5/tensorflow_op/surface_normal_grad.hpp"
#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/cuda/utils/safe_call.hpp"


using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Vector 3 */
template<typename T>
using Vec3 = typename MathTypes<T>::Vec3;
/** Const Vector 3 */
template<typename T>
using CVec3 = typename MathTypes<T>::ConstVec3;

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

template<class T>
struct item_return{ typedef T type; };

template<typename T>
__forceinline__ __device__
typename item_return<T>::type AsInt(T value);

template<>
struct item_return<float>{ typedef int type; };
template<>
struct item_return<double>{ typedef long long type; };

template<>
__forceinline__ __device__
item_return<float>::type AsInt<float>(float value) {
  return __float_as_int(value);
}

template<>
__forceinline__ __device__
item_return<double>::type AsInt<double>(double value) {
  return __double_as_longlong(value);
}

/**
 * @name    NormalKernel
 * @brief   Compute surface's normal
 * @param[in] vertex    Surface vertices
 * @param[in] conn      Connectivity matrix
 * @param[in] n_vertex  Number of rows in connectivity matrix
 * @param[in] n_conn    Number of columns in connectivity matrix
 * @param[out] normal   Surface's normals
 * @param[out] scaling Normal's length before normalization to unit length
 * @tparam T    Data type
 */
template<typename T>
__global__
void NormalKernel(CVec3<T>* vertex,
                  const int* conn,
                  const int n_vertex,
                  const int n_conn,
                  Vec3<T>* normal,
                  T* scaling) {
  int ix =  blockIdx.x * blockDim.x + threadIdx.x;
  int iy =  blockIdx.y * blockDim.y + threadIdx.y;

  if (ix >= n_vertex) return;

  // Compute all normals of the one-ring neighbour of the ith vertex
  const int n_edge = conn[ix * n_conn];
  const int* edges = &(conn[ix * n_conn + 1]);
  Vec3<T> wn = Pack3(T(0.0));
  const auto& A = vertex[iy * n_vertex + ix];
  for (int j = 0; j < n_edge; j += 2) {
    const auto& B = vertex[iy * n_vertex + edges[j]];
    const auto& C = vertex[iy * n_vertex + edges[j + 1]];
    // Define edges AB, AC
    auto AB = B - A;
    auto AC = C - A;
    // Compute surface's normal (triangle ABC)
    Vec3<T> n = cross(AB, AC);
    n = normalize(n);
    // Stack each face contribution and weight with angle
    AB = normalize(AB);
    AC = normalize(AC);
    T ddot = dot(AB, AC);
    ddot = ddot > T(1.0) ? T(1.0) : ddot;
    ddot = ddot < T(-1.0) ? T(-1.0) : ddot;
    const T angle = std::acos(ddot);
    wn += (n * angle);
  }
  // normalize and set
  normal[iy * n_vertex + ix] = n_edge != 0 ? normalize(wn) : Pack3(T(0.0));
  scaling[iy * n_vertex + ix] = norm(wn);
}

/**
 * @name
 * @brief Compute edge between two vertices
 * @param[in] vertex    Vertex array
 * @param[in] mid       Index of starting point
 * @param[in] to        Index of ending point
 * @param[out] edge     Edge
 * @param[out] grad_mid Gradient with respect to initial point
 * @param[out] grad_to  Gradient with respect to final point
 * @tparam T    Data type
 */
template<typename T>
__device__ __forceinline__
void ComputeEdge(CVec3<T>* vertex,
                 const int& mid,
                 const int& to,
                 Vec3<T>* edge,
                 Mat3x3<T>* grad_mid,
                 Mat3x3<T>* grad_to) {
  // Compute edge -> forward
  *edge = vertex[to] - vertex[mid];
  // Derivative
  *grad_mid = -Mat3x3<T>::Identity();
  *grad_to = Mat3x3<T>::Identity();
}

/**
 * @name    NormalizeVector
 * @brief Normalize a vector to unit length
 * @param[in] v     Vector to normalize
 * @param[out] vn   Normalized vector
 * @param[out] grad Gradient
 * @tparam T    Data type
 */
template<typename T>
__device__ __forceinline__
void NormalizeVector(CVec3<T>& v,
                     Vec3<T>* vn,
                     Mat3x3<T>* grad) {
  const T n = std::sqrt((v.x * v.x) +
                        (v.y * v.y) +
                        (v.z * v.z)) +
              std::numeric_limits<T>::epsilon();
  // normalize
  *vn = Pack<T>(v.x / n, v.y / n, v.z / n);
  // Derivative
  const T n3 = n * n * n;
  *grad = Mat3x3<T>((T(1.0) / n) - ((v.x * v.x) / n3),
                    -(v.x * v.y) / n3,
                    -(v.x * v.z) / n3,
                    -(v.y * v.x) / n3,
                    (T(1.0) / n) - ((v.y * v.y) / n3),
                    -(v.y * v.z) / n3,
                    -(v.z * v.x) / n3,
                    -(v.z * v.y) / n3,
                    (T(1.0) / n) - ((v.z * v.z) / n3));
}

/**
 * @name    CrossProduct
 * @brief Compute cross product and its derivative
 * @param[in] ei_k          First edge  (vk - vi)
 * @param[in] ei_k1         Second edge (vk1 - vi)
 * @param[in] dei_k_dvk     Derivative of the first edge with respect to vk
 * @param[in] dei_k_dvi     Derivative of the first edge with respect to vi
 * @param[in] dei_k1_dvk1   Derivative of the second edge with respect to vk1
 * @param[in] dei_k1_dvi    Derivative of the second edge with respect to vi
 * @param[out] nti          Cross product of ei_k x ei_k1
 * @param[out] dnti_dvi     Derivative of the cross product with respect to
 *                          middle vertex
 * @param[out] dnti_dvk     Derivative of the cross product with respect to
 *                          the vertex at the end of the first edge (ei_k).
 * @param[out] dnti_dvk1    Derivative of the cross product with respect to
 *                          the vertex at the end of the second edge (ei_k1).
 * @tparam T Data term
 */
template<typename T>
__device__ __forceinline__
void CrossProduct(CVec3<T>& ei_k,
                  CVec3<T>& ei_k1,
                  const Mat3x3<T>& dei_k_dvk,
                  const Mat3x3<T>& dei_k_dvi,
                  const Mat3x3<T>& dei_k1_dvk1,
                  const Mat3x3<T>& dei_k1_dvi,
                  Vec3<T>* nti,
                  Mat3x3<T>* dnti_dvi,
                  Mat3x3<T>* dnti_dvk,
                  Mat3x3<T>* dnti_dvk1) {
  // Cross product
  *nti = cross(ei_k, ei_k1);
  // Derivative d(A x B)_dvi == dnti_dvi
  Vec3<T> dnti_dvix = ((Mat3x3<T>::Skew(dei_k_dvi.a,
                                        dei_k_dvi.d,
                                        dei_k_dvi.g) * ei_k1) +
                       (Mat3x3<T>::Skew(-dei_k1_dvi.a,
                                        -dei_k1_dvi.d,
                                        -dei_k1_dvi.g) * ei_k));
  Vec3<T> dnti_dviy = ((Mat3x3<T>::Skew(dei_k_dvi.b,
                                        dei_k_dvi.e,
                                        dei_k_dvi.h) * ei_k1) +
                       (Mat3x3<T>::Skew(-dei_k1_dvi.b,
                                        -dei_k1_dvi.e,
                                        -dei_k1_dvi.h) * ei_k));
  Vec3<T> dnti_dviz = ((Mat3x3<T>::Skew(dei_k_dvi.c,
                                        dei_k_dvi.f,
                                        dei_k_dvi.i) * ei_k1) +
                       (Mat3x3<T>::Skew(-dei_k1_dvi.c,
                                        -dei_k1_dvi.f,
                                        -dei_k1_dvi.i) * ei_k));
  dnti_dvi->a = dnti_dvix.x;
  dnti_dvi->b = dnti_dviy.x;
  dnti_dvi->c = dnti_dviz.x;
  dnti_dvi->d = dnti_dvix.y;
  dnti_dvi->e = dnti_dviy.y;
  dnti_dvi->f = dnti_dviz.y;
  dnti_dvi->g = dnti_dvix.z;
  dnti_dvi->h = dnti_dviy.z;
  dnti_dvi->i = dnti_dviz.z;
  // Derivative d(A x B)_dvk == dnti_dvk
  // Derivative of dei_k_dvk1 = 0
  Vec3<T> dnti_dvkx = (Mat3x3<T>::Skew(dei_k_dvk.a,
                                       dei_k_dvk.d,
                                       dei_k_dvk.g) * ei_k1);
  Vec3<T> dnti_dvky = (Mat3x3<T>::Skew(dei_k_dvk.b,
                                       dei_k_dvk.e,
                                       dei_k_dvk.h) * ei_k1);
  Vec3<T> dnti_dvkz = (Mat3x3<T>::Skew(dei_k_dvk.c,
                                       dei_k_dvk.f,
                                       dei_k_dvk.i) * ei_k1);
  dnti_dvk->a = dnti_dvkx.x;
  dnti_dvk->b = dnti_dvky.x;
  dnti_dvk->c = dnti_dvkz.x;
  dnti_dvk->d = dnti_dvkx.y;
  dnti_dvk->e = dnti_dvky.y;
  dnti_dvk->f = dnti_dvkz.y;
  dnti_dvk->g = dnti_dvkx.z;
  dnti_dvk->h = dnti_dvky.z;
  dnti_dvk->i = dnti_dvkz.z;
  // Derivative d(A x B)_dvk1 == dnti_dvk1
  // Derivative of dei_k1_dvk = 0
  Vec3<T> dnti_dvk1x = (Mat3x3<T>::Skew(-dei_k1_dvk1.a,
                                        -dei_k1_dvk1.d,
                                        -dei_k1_dvk1.g) * ei_k);
  Vec3<T> dnti_dvk1y = (Mat3x3<T>::Skew(-dei_k1_dvk1.b,
                                        -dei_k1_dvk1.e,
                                        -dei_k1_dvk1.h) * ei_k);
  Vec3<T> dnti_dvk1z = (Mat3x3<T>::Skew(-dei_k1_dvk1.c,
                                        -dei_k1_dvk1.f,
                                        -dei_k1_dvk1.i) * ei_k);
  dnti_dvk1->a = dnti_dvk1x.x;
  dnti_dvk1->b = dnti_dvk1y.x;
  dnti_dvk1->c = dnti_dvk1z.x;
  dnti_dvk1->d = dnti_dvk1x.y;
  dnti_dvk1->e = dnti_dvk1y.y;
  dnti_dvk1->f = dnti_dvk1z.y;
  dnti_dvk1->g = dnti_dvk1x.z;
  dnti_dvk1->h = dnti_dvk1y.z;
  dnti_dvk1->i = dnti_dvk1z.z;
}

/**
 * @name    DotProduct
 * @brief   Compute dot product between two normalized edge `ekt` and `ek1t`
 * @param[in] ekt           First normalized edge
 * @param[in] ek1t          Second normalized edge
 * @param[in] dekt_dvi      Derivative of first edge with respect to vi
 * @param[in] dekt_dvk      Derivative of first edge with respect to vk
 * @param[in] dek1t_dvi     Derivative of second edge with respect to vi
 * @param[in] dek1t_dvk1    Derivative of second edge with respect to vk1
 * @param[out] dotp         Dot product value
 * @param[out] ddot_dvi     Derivative of the dot product w.r.t vi
 * @param[out] ddot_dvk     Derivative of the dot product w.r.t vk
 * @param[out] ddot_dvk1    Derivative of the dot product w.r.t vk1
 * @tparam T    Data type
 */
template<typename T>
__device__ __forceinline__
void DotProduct(CVec3<T>& ekt,
                CVec3<T>& ek1t,
                const Mat3x3<T>& dekt_dvi,
                const Mat3x3<T>& dekt_dvk,
                const Mat3x3<T>& dek1t_dvi,
                const Mat3x3<T>& dek1t_dvk1,
                T* dotp,
                Vec3<T>* ddot_dvi,
                Vec3<T>* ddot_dvk,
                Vec3<T>* ddot_dvk1) {
  // Dot product
  *dotp = dot(ekt, ek1t);
  // Derivative ddot_dvi
  *ddot_dvi = (dekt_dvi * ek1t) + (dek1t_dvi * ekt);
  *ddot_dvk = (dekt_dvk * ek1t);  // dek1t_dvk = 0
  *ddot_dvk1 = (dek1t_dvk1 * ekt);  // dek1t_dvk = 0
}

/**
 * @name    NormalGradientKernel
 * @brief compute the gradient of the surface normals with respect to vertex
 * @param[in] g_normal  Back propagated gradient up to now
 * @param[in] vertex    Vertex array
 * @param[in] conn      Connectivity matrix
 * @param[in] n_vertex  Number of vertex/normals
 * @param[in] n_conn    Width of connectivity matrix
 * @param[in] normal    Surface's normals
 * @param[in] scaling   Surface's normal length before normalization
 * @param[in] grad      Computed grad
 * @tparam T    Data type
 */
template<typename T>
__global__ void NormalGradientKernel(CVec3<T>* g_normal,
                                     CVec3<T>* vertex,
                                     const int* conn,
                                     const int n_vertex,
                                     const int n_conn,
                                     CVec3<T>* normal,
                                     const T* scaling,
                                     Vec3<T>* grad) {
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;   // [0 - N]
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;   // [0 - Batch]

  if (ix >= n_vertex) return; // First normal only for testing

  // Compute normal + gradient at the same time
  const int n_edge = conn[ix * n_conn];
  const int* edges = &(conn[(ix * n_conn) + 1]);

  CVec3<T>* v_ptr = &(vertex[iy * n_vertex]);
  Vec3<T>* g_ptr = &(grad[iy * n_vertex]);
  CVec3<T> gn = g_normal[(iy * n_vertex) + ix]; // Back-propagate gradient
  // Skip computation if back-propagated grad is null
  auto grad_all_in = AsInt<T>(gn.x) | AsInt<T>(gn.y) |AsInt<T>(gn.z);
  if (grad_all_in == 0) {
    return;
  }
  // Compute jacobian of the final normalization
  Vec3<T> n = normal[(iy * n_vertex) + ix] * scaling[(iy * n_vertex) + ix];
  Vec3<T> n_norm;
  Mat3x3<T> dni_dnit;
  NormalizeVector<T>(n, &n_norm, &dni_dnit);
  Mat3x3<T> dAB_dvi, dAB_dvk, dAB_dvk1;
  for (int j = 0; j < n_edge; j += 2) {
    // index
    const int mid = ix;
    const int k = edges[j];
    const int k1 = edges[j + 1];
    // Compute edges
    Vec3<T> ek, ek1;
    Mat3x3<T> dek_dvi, dek_dvk, dek1_dvi, dek1_dvk1;
    ComputeEdge<T>(v_ptr, mid, k, &ek, &dek_dvi, &dek_dvk);
    ComputeEdge<T>(v_ptr, mid, k1, &ek1, &dek1_dvi, &dek1_dvk1);
    // Compute surface's normal (triangle ABC)
    Vec3<T> nt;
    Mat3x3<T> dnt_dvi, dnt_dvk, dnt_dvk1;
    CrossProduct<T>(ek, ek1,
                    dek_dvk, dek_dvi, dek1_dvk1, dek1_dvi,
                    &nt,
                    &dnt_dvi, &dnt_dvk, &dnt_dvk1);
    // Normalize nt
    Vec3<T> nt_nrm;
    Mat3x3<T> dnt_nrm_d_nt;
    NormalizeVector<T>(nt, &nt_nrm, &dnt_nrm_d_nt);
    // Chain rule for dn_dvi, dn_dvk, dn_dvk1
    Mat3x3<T> dn_dvi, dn_dvk, dn_dvk1;
    dn_dvi = dnt_nrm_d_nt * dnt_dvi;
    dn_dvk = dnt_nrm_d_nt * dnt_dvk;
    dn_dvk1 = dnt_nrm_d_nt * dnt_dvk1;
    // Compute weighting factor B = acos(ek * ek1) and its gradient
    // dB_dvi, dB_dvk, dB_dvk1
    // Normalize edges
    Mat3x3<T> dekt_dek, dek1t_dek1;
    Vec3<T> ek_nrm, ek1_nrm;
    NormalizeVector<T>(ek, &ek_nrm, &dekt_dek);
    NormalizeVector<T>(ek1, &ek1_nrm, &dek1t_dek1);
    // Chain rule for dekt_dvi, dekt_dvk dek1t_dvi, dek1t_dvk1
    Mat3x3<T> dekt_dvi, dekt_dvk, dek1t_dvi, dek1t_dvk1;
    dekt_dvi = dekt_dek * dek_dvi;
    dekt_dvk = dekt_dek * dek_dvk;
    dek1t_dvi = dek1t_dek1 * dek1_dvi;
    dek1t_dvk1 = dek1t_dek1 * dek1_dvk1;
    T ddot;
    Vec3<T> ddot_vi, ddot_vk, ddot_vk1;
    DotProduct<T>(ek_nrm, ek1_nrm,
                  dekt_dvi, dekt_dvk, dek1t_dvi, dek1t_dvk1,
                  &ddot,
                  &ddot_vi, &ddot_vk, &ddot_vk1);
//    const T dddot = ddot;
    ddot = ddot > T(1.0) ? T(1.0) : ddot;
    ddot = ddot < T(-1.0) ? T(-1.0) : ddot;
    const T B = std::acos(ddot);
    // Chain gradient for dB_dvi  dB_dvk  dB_dvk1
    Vec3<T> dB_dvi, dB_dvk, dB_dvk1;
    T dacos_dx = T(-1.0) / std::sqrt(
            T(1.0) - (ddot * ddot) + std::numeric_limits<T>::epsilon());
    dB_dvi = dacos_dx * ddot_vi;
    dB_dvk = dacos_dx * ddot_vk;
    dB_dvk1 = dacos_dx * ddot_vk1;
    // Finalize
    // dAB_dvi = dn_dvi * B + A * dB_dvi
    // dAB_dvk = dn_dvk * B + A * dB_dvk
    // dAB_dvk1 = dn_dvk1 * B + A * dB_dvk1
    dAB_dvi = (dn_dvi * B) +
              Mat3x3<T>((nt_nrm.x * dB_dvi.x),
                        (nt_nrm.x * dB_dvi.y),
                        (nt_nrm.x * dB_dvi.z),
                        (nt_nrm.y * dB_dvi.x),
                        (nt_nrm.y * dB_dvi.y),
                        (nt_nrm.y * dB_dvi.z),
                        (nt_nrm.z * dB_dvi.x),
                        (nt_nrm.z * dB_dvi.y),
                        (nt_nrm.z * dB_dvi.z));
    dAB_dvk = (dn_dvk * B) +
              Mat3x3<T>((nt_nrm.x * dB_dvk.x),
                        (nt_nrm.x * dB_dvk.y),
                        (nt_nrm.x * dB_dvk.z),
                        (nt_nrm.y * dB_dvk.x),
                        (nt_nrm.y * dB_dvk.y),
                        (nt_nrm.y * dB_dvk.z),
                        (nt_nrm.z * dB_dvk.x),
                        (nt_nrm.z * dB_dvk.y),
                        (nt_nrm.z * dB_dvk.z));
    dAB_dvk1 = (dn_dvk1 * B) +
               Mat3x3<T>((nt_nrm.x * dB_dvk1.x),
                         (nt_nrm.x * dB_dvk1.y),
                         (nt_nrm.x * dB_dvk1.z),
                         (nt_nrm.y * dB_dvk1.x),
                         (nt_nrm.y * dB_dvk1.y),
                         (nt_nrm.y * dB_dvk1.z),
                         (nt_nrm.z * dB_dvk1.x),
                         (nt_nrm.z * dB_dvk1.y),
                         (nt_nrm.z * dB_dvk1.z));
    // Add outter normalization
    dAB_dvi = dni_dnit * dAB_dvi;
    dAB_dvk = dni_dnit * dAB_dvk;
    dAB_dvk1 = dni_dnit * dAB_dvk1;
    // Stack into grad
    // g = dAB_dvi * gn;
    //NOTE:  Use transpose instead of reverse matrix product due to vector
    Vec3<T> g = dAB_dvi.Transpose() * gn;
    atomicAdd(&(g_ptr[mid].x), g.x);
    atomicAdd(&(g_ptr[mid].y), g.y);
    atomicAdd(&(g_ptr[mid].z), g.z);
    // g = dAB_dvk * gn;
    g = dAB_dvk.Transpose() * gn;
    atomicAdd(&(g_ptr[k].x), g.x);
    atomicAdd(&(g_ptr[k].y), g.y);
    atomicAdd(&(g_ptr[k].z), g.z);
    // g = dAB_dvk1 * gn;
    g = dAB_dvk1.Transpose() * gn;
    atomicAdd(&(g_ptr[k1].x), g.x);
    atomicAdd(&(g_ptr[k1].y), g.y);
    atomicAdd(&(g_ptr[k1].z),   g.z);
  }
}

/**
 * @name    SetValue
 * @brief   Initialize an array to a given value
 * @param[in] value     Value to set
 * @param[in] n         Array width
 * @param[out] array    Array pointer
 * @tparam T    Data type
 */
template<typename T>
__global__
void SetValue(const T value, const int n, Vec3<T>* array) {
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;
  if (ix < n) {
    array[(iy * n) + ix] = Pack3(value);
  }
}


}  // namespace device


/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/*
 * @class   NormalGeneratorV2
 * @brief   Compute surface's normals
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct NormalGeneratorV2<GPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in,out] ctx Op's context
   * @param[in] vertex  Surface's vertcies
   * @param[in] conn    Connectivity
   * @param[out] normal Surface's normals computed
   * @param[out] scaling Normal's length before normalization to unit length
   */
  int operator()(tf::OpKernelContext* ctx,
                  const tf::Tensor* vertex,
                  const tf::Tensor* conn,
                  tf::Tensor* normal,
                  tf::Tensor* scaling) {
    // Acces raw pointer
    auto* v_ptr = reinterpret_cast<CVec3<T>*>(vertex->template flat<T>().data());
    const int* conn_ptr = conn->template flat<int>().data();
    auto* n_ptr = reinterpret_cast<Vec3<T>*>(normal->template flat<T>().data());
    auto* s_ptr = scaling->template flat<T>().data();
    // Dimensions
    int n_img = vertex->dim_size(0);
    int n_vertex = vertex->dim_size(1);
    int n_conn = conn->dim_size(1);
    auto& stream = ctx->eigen_gpu_device().stream();
    dim3 thd(1024, 1);
    dim3 bck((n_vertex + 1023) / 1024, n_img);
    device::NormalKernel<T><<<bck, thd, 0, stream>>>(v_ptr,
            conn_ptr,
            n_vertex,
            n_conn,
            n_ptr,
            s_ptr);
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

/**
 * @class   NormalGradientHelper
 * @brief   Compute surface's normals gradient
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct NormalGradientHelper<GPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in,out] ctx Op's context
   * @param[in] g_normal  Back propagated gradient
   * @param[in] vertex  Surface's vertices
   * @param[in] conn    Connectivity
   * @param[in] normal  Surface's normals
   * @param[in] scaling Surface's normals scaling (norm before normalization)
   * @param[out] grad   Surface's normals gradient
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_normal,
                 const tf::Tensor* vertex,
                 const tf::Tensor* conn,
                 const tf::Tensor* normal,
                 const tf::Tensor* scaling,
                 tf::Tensor* grad) {
    // Acces raw pointer
    auto* gn_ptr = reinterpret_cast<CVec3<T>*>(g_normal->template flat<T>().data());
    const int* conn_ptr = conn->template flat<int>().data();
    auto* v_ptr = reinterpret_cast<CVec3<T>*>(vertex->template flat<T>().data());
    auto* n_ptr = reinterpret_cast<CVec3<T>*>(normal->template flat<T>().data());
    auto* s_ptr = scaling->template flat<T>().data();
    auto* g_ptr = reinterpret_cast<Vec3<T>*>(grad->template flat<T>().data());
    // Dimensions
    int n_img = vertex->dim_size(0);
    int n_normal = vertex->dim_size(1);
    int n_conn = conn->dim_size(1);
    auto& stream = ctx->eigen_gpu_device().stream();
    dim3 thd(256, 1);
    dim3 bck(((n_normal) + 255) / 256, n_img);
    // Set grad to zero
    device::SetValue<T><<<bck, thd, 0, stream>>>(T(0.0), n_normal, g_ptr);
    device::NormalGradientKernel<T><<<bck, thd, 0, stream>>>(gn_ptr,
            v_ptr,
            conn_ptr,
            n_normal,
            n_conn,
            n_ptr,
            s_ptr,
            g_ptr);
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

#pragma mark -
#pragma mark Explicit Instantiation

/** Float */
template struct NormalGeneratorV2<GPUDevice, float>;
template struct NormalGradientHelper<GPUDevice, float>;
/** Double */
#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
template struct NormalGeneratorV2<GPUDevice, double>;
template struct NormalGradientHelper<GPUDevice, double>;
#endif

}  // namespace Functor
}  // namespace LTS5

