/**
 *  @file   device_utils.cu
 *  @brief  Helper tools for handling various device
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   03/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/register_types.h"

#include "lts5/cuda/tensorflow_op/device_utils.hpp"
#include "lts5/tensorflow_op/device_utils.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      Helper function
 */
namespace Functor {



/*
 * @name  Synchronize
 * @fn    static void Synchronize(const GPUDevice& d)
 * @param[in] d   Device to synchronize
 * @brief Synchronize the device
 */
void DeviceHelper<GPUDevice>::Synchronize(const GPUDevice& d) {
  d.synchronize();
}

/*
 * @name  Ok
 * @fn    static bool Ok(const GPUDevice& d)
 * @brief Check if everything's godd for a given device
 * @param[in] d   Device to check
 * @return    True if Ok, False otherwise
 */
bool DeviceHelper<GPUDevice>::Ok(const GPUDevice& d) {
  return d.ok();
}

}  // namepsace Functor
}  // namespace LTS5