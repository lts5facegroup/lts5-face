/**
 *  @file   mm_mask.cu
 *  @brief  Mask generator for Morphable model
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   10/4/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include "cuda_runtime.h"
#include "cuda.h"

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"


#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
#include "lts5/cuda/utils/math/device_math.hpp"
#include "lts5/cuda/utils/safe_call.hpp"

#include "lts5/utils/logger.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/tensorflow_op/mm_mask.hpp"

using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Constant Vector3 T type */
template<typename T>
using CVec3 = typename MathTypes<T>::ConstVec3;
/** Constant Vector4 T type */
template<typename T>
using CVec4 = typename MathTypes<T>::ConstVec4;
/** Constant Vector3 integer type */
using CVec3i = typename MathTypes<int>::ConstVec3;
/** Vector3 type */
template<typename T>
using Vec3 = typename MathTypes<T>::Vec3;

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {


#pragma mark -
#pragma mark Mask Generator Kernel

/**
 * @name    InterpVector
 * @brief Barycentric interpolation
 * @param[in] bc    Barycentric coordinate
 * @param[in] tris  List of triangles
 * @param[in] array Array storing the value to interpolate
 * @return interpolated value
 */
template<typename T>
__device__
Vec3<T> InterpVector(CVec3<T>& bc, CVec3i* tris, CVec3<T>* array) {
  // Access interpolant
  const int tidx = static_cast<int>(CUDA::Math<T>::Round(bc.x));
  const auto& tri = tris[tidx];
  // Interpolate
  const auto& v0 = array[tri.x];
  const auto& v1 = array[tri.y];
  const auto& v2 = array[tri.z];
  const T& alpha = bc.y;
  const T& beta = bc.z;
  const T gamma = (T(1.0) - alpha - beta);
  Vec3<T> value;
  value.x = (v0.x * alpha) + (v1.x * beta) + (v2.x * gamma);
  value.y = (v0.y * alpha) + (v1.y * beta) + (v2.y * gamma);
  value.z = (v0.z * alpha) + (v1.z * beta) + (v2.z * gamma);
  return value;
}

/**
 * @name    FgBgMaskKernel
 * @brief   Compute Foreground/Background mask
 * @param[in] bcoord Barycentric coordinate (Slice)
 * @param[in] height Image's height
 * @param[in] width Image's width
 * @param[out] mask Generated mask (Slice)
 * @tparam T    Data type
 */
template<typename T>
__global__
void FgBgMaskKernel(CVec4<T>* bcoord, int height, int width, Vec3<T>* mask) {
  using Math = LTS5::CUDA::Math<T>;
  // Define pos
  const int i = blockIdx.y * blockDim.y + threadIdx.y;
  const int j = blockIdx.x * blockDim.x + threadIdx.x;
  const int k = blockIdx.z * blockDim.z + threadIdx.z;

  if (i < height && j < width) {
    // Get barycentric coordinates
    const int idx = (k * width * height) + (i * width) + j;
    const auto& bc = bcoord[idx];
    const int tri_idx = static_cast<int>(Math::Round(bc.x));
    if (tri_idx > 0) {
      // Foreground
      mask[idx] = Pack3(T(1.0));
    } else {
      // Backgrond
      mask[idx] = Pack3(T(0.0));
    }
  }
}

#pragma mark -
#pragma mark Mask Generator

/*
 * @struct  MMMaskGenerator
 * @brief   Generate standard deviation mask
 * @author  Christophe Ecabert
 * @date    04.10.18
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template< typename T>
struct MMMaskGenerator<GPUDevice, T> {

  /** Tensor type */
  using Tensor = tensorflow::Tensor;
  /** Context type */
  using OpKernelContext = tensorflow::OpKernelContext;
  /** Storage type */
  using Storage = PersistentStorage<T>;

  /**
   * @name operator()
   * @brief Compute texture standard deviation mask
   * @param[in] ctx Op context
   * @param[in] param Morphable model's parameters
   * @param[in] storage Persistent storage
   * @param[in] n_std   How much std to take into accound while generating the
   *                    mask
   * @param[in] mask    Generated mask with min/max texture value explainable by
   *                    the model
   */
  void operator()(OpKernelContext* ctx, const Tensor& bcoord, Tensor* mask) {

    //LTS5::CUDA::Trace trace("Mask", 4);
    // Loop over all images in a batch
    auto& stream = ctx->eigen_gpu_device().stream();
    // Image properties
    auto n_img = bcoord.dim_size(0);
    auto h = static_cast<int>(bcoord.dim_size(1));
    auto w = static_cast<int>(bcoord.dim_size(2));
    const auto* bc_ptr = reinterpret_cast<CVec4<T>*>(bcoord.flat<T>().data());
    auto* mask_ptr = reinterpret_cast<Vec3<T>*>(mask->flat<T>().data());


    // Define lauch param
    dim3 thd(32, 32, 1);
    dim3 bck((h + 31) / 32, (w + 31) / 32, n_img);
    FgBgMaskKernel<T><<<bck, thd, 0, stream>>>(bc_ptr, h, w, mask_ptr);


    // Loop over images of batch
    /*for (auto k = 0; k < n_img; ++k) {
      // Kernel's input / output
      auto bc_slice = bcoord.Slice(k, k + 1);
      auto mask_slice = mask->Slice(k, k + 1);
      const auto* bc_ptr = reinterpret_cast<CVec3<T>*>(bc_slice.unaligned_flat<T>().data());
      auto* mask_ptr = reinterpret_cast<Vec3<T>*>(mask_slice.unaligned_flat<T>().data());
      // Define lauch param
      dim3 thd(32, 32);
      dim3 bck((h + 31) / 32, (w + 31) / 32);
      FgBgMaskKernel<T><<<bck, thd, 0, device.stream()>>>(bc_ptr, h, w, mask_ptr);

      // Dump mask
      {
        using ConstTensorMap = typename tensorflow::TTypes<T>::ConstTensor;
        using TensorMap = typename tensorflow::TTypes<T>::Tensor;
        cv::Mat mask_cpu(mask_slice.dim_size(1), mask_slice.dim_size(2) * mask_slice.dim_size(3), cv::DataType<T>::type);
        auto in = ConstTensorMap(mask_slice.unaligned_flat<T>().data(), mask_slice.unaligned_flat<T>().size());
        auto out = TensorMap((T*)mask_cpu.data, mask_cpu.rows * mask_cpu.cols * 3);
        LTS5::Functor::DenseOp<Eigen::GpuDevice, T, LTS5::Functor::DenseOpType::kD2HCopy> copy;
        copy(device, in, out);
        LTS5::Functor::DeviceHelper<Eigen::GpuDevice>::Synchronize(device);
        LTS5::SaveMatToBin("dbg/mask_" + std::to_string(k) + ".bin", mask_cpu);
      }
    }*/
  }
};

#pragma mark -
#pragma mark Explicit Instantiation


/** Float */
template struct MMMaskGenerator<GPUDevice, float>;
/** Double */
template struct MMMaskGenerator<GPUDevice, double>;

}  // namespace Functor
}  // namespace LTS5

