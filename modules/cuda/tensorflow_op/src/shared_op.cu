/**
 *  @file   shared_op.cpp
 *  @brief
 *  @ingroup
 *
 *  @author Christophe Ecabert
 *  @date   05/12/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

// Register the GPU kernels.
#define EIGEN_USE_GPU
#include "lts5/tensorflow_op/shared_op.hpp"

using GPUDevice = Eigen::GpuDevice;


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 *  @namespace  device
 *  @brief      CUDA device code dev space
 */
namespace device {

#pragma mark -
#pragma mark Device

// Define the CUDA kernel.
template <typename T>
__global__ void ExampleCudaKernel(const int size, const T* in, T* out) {
  for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < size;
       i += blockDim.x * gridDim.x) {
    out[i] = 2 * __ldg(in + i);
  }
}

}  // namepsace device
}  // namepsace CUDA

#pragma mark -
#pragma mark Implementation

/*
 * @name  operator()
 * @fn void operator()(const Eigen::GpuDevice& d, int size, const T* in, T* out)
 * @brief Do the computation
 * @param[in] d   Device object
 * @param[in] size    Input size
 * @param[in] in      Input data
 * @param[out] out    Output data (computed from \p in)
 */
template <typename T>
void ExampleFunctor<GPUDevice, T>::operator()(const GPUDevice& d,
                                              int size,
                                              const T* in,
                                              T* out) {
  // Launch the cuda kernel.
  //
  // See core/util/cuda_kernel_helper.h for example of computing
  // block count and thread_per_block count.
  int block_count = 1024;
  int thread_per_block = 20;
  CUDA::device::ExampleCudaKernel<T><<<block_count, thread_per_block, 0, d.stream()>>>(size, in, out);
  LOG(INFO) << "Run gpu" << std::endl;
}


#pragma mark -
#pragma mark Explicit Instantiation

// Explicitly instantiate functors for the types of OpKernels registered.
template struct ExampleFunctor<GPUDevice, tensorflow::int32>;
template struct ExampleFunctor<GPUDevice, float>;
template struct ExampleFunctor<GPUDevice, double>;


}  // namespace LTS5