/**
 *  @file   rasterizer_v2.cu
 *  @brief  OpenGL-bassed rasterizer
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   11/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_CUDA_TENSORFLOW_OP_RASTERIZER_V2__
#define __LTS5_CUDA_TENSORFLOW_OP_RASTERIZER_V2__

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op_kernel.h"

#include "cuda_gl_interop.h"

#include "lts5/utils/logger.hpp"
#include "lts5/tensorflow_op/rasterizer_v2.hpp"
#include "lts5/cuda/utils/safe_call.hpp"

using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

template<typename T>
__global__
void Transfer3DTexture(cudaTextureObject_t tex,
                       int height,
                       int width,
                       int channels,
                       T* output) {
  // Get index
  unsigned int ix = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int iy = blockIdx.y * blockDim.y + threadIdx.y;
  unsigned int iz = blockIdx.z * blockDim.z + threadIdx.z;
  unsigned int wh = width * height;
  if (ix < width && iy < height) {
    // flip output index around y-axis since opengl origin is at bottom-left
    unsigned int iyp = (height - iy - 1);
    float4 value = tex3D<float4>(tex,
                                 float(ix) + 0.5f,
                                 float(iy) + 0.5f,
                                 float(iz));
    float* v_ptr = &value.x;
    unsigned int index = ((iz * wh) + (iyp * width) + ix) * channels;
    for (int i = 0; i < channels; ++i) {
      output[index + i] = static_cast<T>(v_ptr[i]);
    }
  }
}

}  // namespace device

/**
 * @class   OGLResources
 * @brief   Interface with OpenGL buffer to transfer data
 * @author  Christophe Ecabert
 * @date    12/11/2020
 * @tparam Device   Device on which to run the computation
 * @tparam T    Data type
 */
template<typename T>
class OGLResources<GPUDevice, T> {
 public:
  /** Type of buffer */
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;

  /**
   * @name  OGLResources
   * @fn    OGLResources()
   * @brief Constructor
   */
  OGLResources() : resource_(nullptr), buffer_(0) {}

  /**
   * @name  ~OGLResources
   * @fn    ~OGLResources()
   * @brief Destructor
   */
  ~OGLResources() {
    this->UnRegister();
  }

  /**
   * @name  Register
   * @brief Register an existing OpenGL buffer
   * @param[in] buffer Buffer index to register
   * @param[in] type    Type of buffer to regiuster
   * @return    -1 if error, 0 otherwise
   */
  int Register(const unsigned int& buffer, const BufferType& type) {
    // Already have a registered resource ?
    this->UnRegister();
    // Register
    buffer_ = buffer;
    int err = cudaSafeCall(cudaGraphicsGLRegisterBuffer(&resource_,
                                      buffer_,
                                      cudaGraphicsRegisterFlagsWriteDiscard));
    return err;
  }

  /**
   * @name  UnRegister
   * @brief Unregister the underlying resources
   */
  void UnRegister() {
    if (resource_) {
      cudaSafeCall(cudaGraphicsUnregisterResource(resource_));
      resource_ = nullptr;
      buffer_ = 0;
    }
  }

  /**
   * @name  Upload
   * @brief Upload data to the buffer
   * @param[in] ctx     Op's context
   * @param[in] data    Data to transfer
   * @return -1 if error, 0 otherwise
   */
  int Upload(tf::OpKernelContext* ctx, const tf::Tensor& data) {
    // Stream
    const auto& stream = ctx->eigen_gpu_device().stream();
    // Data size
    int data_sz = data.NumElements() * sizeof(T);
    // Map
    void* dst_ptr = nullptr;
    size_t buff_sz = 0;
    int err = cudaSafeCall(cudaGraphicsMapResources(1,
                                                    &resource_,
                                                    stream));
    err |= cudaSafeCall(cudaGraphicsResourceGetMappedPointer(&dst_ptr,
                                                             &buff_sz,
                                                             resource_));
    // Copy data
    if (buff_sz < data_sz) {
      ctx->CtxFailure(__FILE__,
                      __LINE__,
                      tf::errors::Internal("Mapped GL buffer (",
                                           buff_sz,
                                           ") is too small for: ",
                                           data_sz));
      return -1;
    }
    auto src_ptr = reinterpret_cast<const void*>(data.flat<T>().data());
    err |= cudaSafeCall(cudaMemcpyAsync(dst_ptr,
                                        src_ptr,
                                        data_sz,
                                        cudaMemcpyDeviceToDevice,
                                        stream));
    // Unmap + sync
    err |= cudaSafeCall(cudaGraphicsUnmapResources(1, &resource_, stream));
    // Done
    return err;
  }

 private:
  /** Resource */
  cudaGraphicsResource_t resource_;
  /** Buffer */
  unsigned int buffer_;
};

/**
 * @class   OGLImage
 * @brief   Interface with OpenGL texture to transfer data
 * @author  Christophe Ecabert
 * @date    12/11/2020
 * @tparam Device   Device on which to run the computation
 * @tparam T    Data type
 */
template<typename T>
class OGLImage<GPUDevice, T> {
 public:
  /**
   * @name  OGLImage
   * @fn    OGLImage()
   * @brief Constructor
   */
  OGLImage() : resource_(nullptr), buffer_(0) {}

  /**
   * @name  ~OGLImage
   * @fn    ~OGLImage()
   * @brief Destructor
   */
  ~OGLImage() {
    this->UnRegister();
  }

  /**
   * @name  Register
   * @brief Register an existing OpenGL buffer
   * @param[in] buffer Buffer index to register
   * @return    -1 if error, 0 otherwise
   */
  int Register(const unsigned int& buffer) {
    // Already have a registered resource ?
    this->UnRegister();
    // Register
    buffer_ = buffer;
    int err = cudaSafeCall(cudaGraphicsGLRegisterImage(&resource_,
                                           buffer_,
                                           0x806F,   // GL_TEXTURE_3D
                                           cudaGraphicsRegisterFlagsReadOnly));
    return err;
  }

  /**
   * @name  UnRegister
   * @brief Unregister the underlying resources
   */
  void UnRegister() {
    if (resource_) {
      cudaSafeCall(cudaGraphicsUnregisterResource(resource_));
      resource_ = nullptr;
      buffer_ = 0;
    }
  }

  /**
   * @name  Upload
   * @brief Upload data to the buffer
   * @param[in] ctx     Op's context
   * @param[in] data    4D Tensor where to transfer the data [Batch, H, W, 4]
   * @return -1 if error, 0 otherwise
   */
  int Download(tf::OpKernelContext* ctx, tf::Tensor* data) {
    // Get stream
    const auto& stream = ctx->eigen_gpu_device().stream();
    // Copy opengl buffers to output tensor.
    cudaArray_t array = nullptr;
    cudaChannelFormatDesc arrayDesc = {};
    cudaExtent arrayExt = {};
    // Map
    int err = cudaSafeCall(cudaGraphicsMapResources(1,
                                                    &resource_,
                                                    stream));
    err |= cudaSafeCall(cudaGraphicsSubResourceGetMappedArray(&array,
                                                              resource_,
                                                              0,
                                                              0));
    err |= cudaSafeCall(cudaArrayGetInfo(&arrayDesc,
                                         &arrayExt,
                                         NULL,
                                         array));
    // Sanity check
    int depth = data->dim_size(0);
    int height = data->dim_size(1);
    int width = data->dim_size(2);
    if (arrayDesc.f != cudaChannelFormatKindFloat) {
      ctx->CtxFailure(__FILE__,
                      __LINE__,
                      tf::errors::Internal("CUDA mapped array data kind mismatch"));
      return -1;
    }
    if (!(arrayDesc.x == 32 &&
          arrayDesc.y == 32 &&
          arrayDesc.z == 32 &&
          arrayDesc.w == 32)) {
      ctx->CtxFailure(__FILE__,
                      __LINE__,
                      tf::errors::Internal("CUDA mapped array data width mismatch"));
      return -1;
    }
    if (!(arrayExt.width >= width &&
          arrayExt.height >= height &&
          arrayExt.depth >= depth)) {
      ctx->CtxFailure(__FILE__,
                      __LINE__,
                      tf::errors::Internal("CUDA mapped array extent mismatch"));
      return -1;
    }

    // Create resource descriptor for cudaArray
    cudaResourceDesc resDesc;
    memset(&resDesc, 0, sizeof(resDesc));
    resDesc.resType = cudaResourceTypeArray;
    resDesc.res.array.array = array;
    // Create texture descriptor
    cudaTextureDesc texDesc;
    memset(&texDesc, 0, sizeof(texDesc));
    texDesc.readMode = cudaReadModeElementType;
    // create texture object
    cudaTextureObject_t tex;
    err |= cudaSafeCall(cudaCreateTextureObject(&tex,
                                                &resDesc,
                                                &texDesc,
                                                nullptr));
    // Copy back to tensor
    auto* dst_ptr = data->flat<T>().data();
    int n = (int) data->dim_size(0);
    int h = (int) data->dim_size(1);
    int w = (int) data->dim_size(2);
    int c = (int) data->dim_size(3);
    dim3 thd(32, 32, 1);
    dim3 bck((w + 31) / 32, (h + 31) / 32, n);
    void* args[] = {&tex, &h, &w, &c, &dst_ptr};
    void* func = (void*)device::Transfer3DTexture<T>;
    err |= cudaSafeCall(cudaLaunchKernel(func, bck, thd, args, 0, stream));
    // Destroy texture to avoid leaking objects
    err |= cudaSafeCall(cudaDestroyTextureObject(tex));

    // Copy data, will be flipped due to different memory origin between cuda
    // opengl (OpenGL is bottom left origin, cuda is top left).

//    auto dst_ptr = reinterpret_cast<void*>(data->flat<T>().data());
//    cudaMemcpy3DParms p = {0};
//    p.srcArray = array;
//    p.dstPtr.ptr = dst_ptr;
//    p.dstPtr.pitch = width * 4 * sizeof(T);
//    p.dstPtr.xsize = width;
//    p.dstPtr.ysize = height;
//    p.extent.width = width;
//    p.extent.height = height;
//    p.extent.depth = depth;
//    p.kind = cudaMemcpyDeviceToDevice;
//    err |= cudaSafeCall(cudaMemcpy3DAsync(&p, stream));

    // Unmap, will sync opengl/cuda op internally.
    err |= cudaSafeCall(cudaGraphicsUnmapResources(1, &resource_, stream));
    return err;
  }

 private:
  /** Resource */
  cudaGraphicsResource_t resource_;
  /** Buffer */
  unsigned int buffer_;
};


#pragma mark -
#pragma mark Explicit Instantiation

template class OGLResources<GPUDevice, int>;
template class OGLResources<GPUDevice, float>;
template class OGLResources<GPUDevice, double>;

template class OGLImage<GPUDevice, int>;
template class OGLImage<GPUDevice, float>;
template class OGLImage<GPUDevice, double>;

}  // namespace LTS5
#endif  // __LTS5_CUDA_TENSORFLOW_OP_RASTERIZER_V2__
