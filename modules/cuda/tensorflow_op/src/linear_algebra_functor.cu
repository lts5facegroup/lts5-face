/**
 *  @file   linear_algebra_functor.cu
 *  @brief
 *  @ingroup
 *
 *  @author Christophe Ecabert
 *  @date   18/12/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <cassert>

#define EIGEN_USE_GPU
#ifdef DEBUG
#define NDEBUG
#endif
// Workaround for string_view class in order to build properly in debug.
// Otherwise run into:
// string_view.h(496): error: cannot use an entity undefined in device code
#include "lts5/cuda/tensorflow_op/linear_algebra_functor.hpp"

#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/platform/stream_executor.h"
#include "tensorflow/stream_executor/stream.h"
#include "tensorflow/stream_executor/blas.h"
#include "tensorflow/stream_executor/device_memory.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      Helper function
 */
namespace Functor {

namespace tf = tensorflow;

/**
 * @name    AsDeviceMemory
 * @brief   Convert a raw pointer into a DeviceMemory object
 * @tparam T Data type
 * @param[in] cuda_memory   Pointer to device memory
 * @param[in] size          Buffer dimension
 * @return GPU DeviceMemory object
 */
template <typename T>
inline perftools::gputools::DeviceMemory<T> AsDeviceMemory(const T* cuda_memory,
                                                           tf::uint64 size) {
  perftools::gputools::DeviceMemoryBase wrapped(const_cast<T*>(cuda_memory),
                                                size * sizeof(T));
  perftools::gputools::DeviceMemory<T> typed(wrapped);
  return typed;
}

#pragma mark -
#pragma mark Implementation

template<typename T>
void LinearAlgebra<GPUDevice, T>::Gemv(Context* ctx,
                                       const Tensor& A,
                                       const bool& trans,
                                       const T &alpha,
                                       const Tensor &x,
                                       const T &beta,
                                       Tensor *y) {
  using Transpose = perftools::gputools::blas::Transpose;
  // Query stream
  auto* stream = ctx->op_device_context()->stream();
  OP_REQUIRES(ctx,
              stream != nullptr,
              tf::errors::Internal("No GPU stream available."));
  tf::uint64 dp_first = trans ? 0 : 1;
  // Query dim
  tf::uint64 m = A.dim_size(1 - dp_first);
  tf::uint64 n = x.dim_size(1); // Should be equal to one
  assert(n==1);
  tf::uint64 k = A.dim_size(dp_first);
  bool trans_a = dp_first == 0;
  // Converted parameters
  tf::uint64 mp = trans_a ? m : k;
  tf::uint64 np = trans_a ? k : m;
  Transpose trans_p = !trans_a  ? Transpose::kTranspose : Transpose::kNoTranspose;
  // Query data
  auto A_ptr = AsDeviceMemory(A.template unaligned_flat<T>().data(),
                              A.template unaligned_flat<T>().size());
  auto X_ptr = AsDeviceMemory(x.template unaligned_flat<T>().data(),
                              x.template unaligned_flat<T>().size());
  auto y_ptr = AsDeviceMemory(y->template unaligned_flat<T>().data(),
                              y->template unaligned_flat<T>().size());
  // Launch computation
  bool status = stream->ThenBlasGemv(trans_p, mp, np, alpha, A_ptr, mp , X_ptr, 1, beta, &y_ptr, 1).ok();
  if (!status) {
    ctx->SetStatus(tf::errors::Internal("Blas GEMV with failed:  m=", m, ", n=", n));
  }
}

template<typename T>
void LinearAlgebra<GPUDevice, T>::Gemv(Context* ctx,
                                       TFConstMatrix& A,
                                       const bool& trans,
                                       const T& alpha,
                                       TFConstFlat& x,
                                       const T& beta,
                                       TFFlat* y) {
  using Transpose = perftools::gputools::blas::Transpose;
  // Query stream
  auto* stream = ctx->op_device_context()->stream();
  OP_REQUIRES(ctx,
              stream,
              tf::errors::Internal("No GPU stream available."));
  tf::uint64 dp_first = trans ? 0 : 1;
  // Query dim
  tf::uint64 m = A.dimension(1 - dp_first);
  tf::uint64 k = A.dimension(dp_first);
  bool trans_a = dp_first == 0;
  // Converted parameters
  tf::uint64 mp = trans_a ? m : k;
  tf::uint64 np = trans_a ? k : m;
  Transpose trans_p = !trans_a  ? Transpose::kTranspose : Transpose::kNoTranspose;
  // Query data
  auto A_ptr = AsDeviceMemory(A.data(),
                              A.size());
  auto X_ptr = AsDeviceMemory(x.data(),
                              x.size());
  auto y_ptr = AsDeviceMemory(y->data(),
                              y->size());
  // Launch computation
  bool status = stream->ThenBlasGemv(trans_p, mp, np, alpha, A_ptr, mp , X_ptr, 1, beta, &y_ptr, 1).ok();
  if (!status) {
    ctx->SetStatus(tf::errors::Internal("Blas GEMV with failed:  m=", mp, ", n=", np));
  }
}

template<typename T>
void LinearAlgebra<GPUDevice, T>::Gemv(Context* ctx,
                                       TFConstMatrix& A,
                                       const bool& trans,
                                       const T& alpha,
                                       TFConstUnFlat& x,
                                       const T& beta,
                                       TFUnFlat* y) {
  using Transpose = perftools::gputools::blas::Transpose;
  // Query stream
  auto* stream = ctx->op_device_context()->stream();
  OP_REQUIRES(ctx,
              stream,
              tf::errors::Internal("No GPU stream available."));
  tf::uint64 dp_first = trans ? 0 : 1;
  // Query dim
  tf::uint64 m = A.dimension(1 - dp_first);
  tf::uint64 k = A.dimension(dp_first);
  bool trans_a = dp_first == 0;
  // Converted parameters
  tf::uint64 mp = trans_a ? m : k;
  tf::uint64 np = trans_a ? k : m;
  Transpose trans_p = !trans_a  ? Transpose::kTranspose : Transpose::kNoTranspose;
  // Query data
  auto A_ptr = AsDeviceMemory(A.data(),
                              A.size());
  auto X_ptr = AsDeviceMemory(x.data(),
                              x.size());
  auto y_ptr = AsDeviceMemory(y->data(),
                              y->size());
  // Launch computation
  bool status = stream->ThenBlasGemv(trans_p, mp, np, alpha, A_ptr, mp , X_ptr, 1, beta, &y_ptr, 1).ok();
  if (!status) {
    ctx->SetStatus(tf::errors::Internal("Blas GEMV with failed:  m=", mp, ", n=", np));
  }
}

template<typename T>
void LinearAlgebra<GPUDevice, T>::Gemm(Context* ctx,
                                       const Tensor& A,
                                       const bool& trans_a,
                                       const T& alpha,
                                       const Tensor& B,
                                       const bool& trans_b,
                                       const T& beta,
                                       Tensor* C) {
  using Transpose = perftools::gputools::blas::Transpose;
  // Check that the dimensions of the two matrices are valid.
  // Taken from
  // https://github.com/tensorflow/tensorflow/blob/master/tensorflow/core/kernels/matmul_op.cc
  OP_REQUIRES(ctx,
              tf::TensorShapeUtils::IsMatrix(A.shape()),
              tf::errors::InvalidArgument("Tensor A is not a matrix"));
  OP_REQUIRES(ctx,
              tf::TensorShapeUtils::IsMatrix(B.shape()),
              tf::errors::InvalidArgument("Tensor B is not a matrix"));
  Eigen::array<Eigen::IndexPair<Eigen::DenseIndex>, 1> dim_pair;
  dim_pair[0].first = trans_a ? 0 : 1;
  dim_pair[0].second = trans_b ? 1 : 0;
  int a_dim_remaining = 1 - dim_pair[0].first;
  int b_dim_remaining = 1 - dim_pair[0].second;
  int crow = A.dim_size(a_dim_remaining);
  int ccol = B.dim_size(b_dim_remaining);
  OP_REQUIRES(ctx,
              A.dim_size(dim_pair[0].first) == B.dim_size(dim_pair[0].second),
              tf::errors::InvalidArgument("Matrix size-incompatible: A: ", A.shape().DebugString(),
                                          ", B: ", B.shape().DebugString()));
  OP_REQUIRES(ctx,
              crow == C->dim_size(0) && ccol == C->dim_size(1),
              tf::errors::InvalidArgument("Matrix C has wrong dimension: ",
                                          C->shape().DebugString()));

  // Query stream
  auto* stream = ctx->op_device_context()->stream();
  OP_REQUIRES(ctx,
              stream,
              tf::errors::Internal("No GPU stream available."));
  // Query dim
  // C = A x B
  // where A, B and C are assumed to be in column major.
  // We want the output to be in row-major, so we can compute
  // C' = B' x A' (' stands for transpose)

  tf::uint64 m = A.dim_size(1 - dim_pair[0].first);
  tf::uint64 k = A.dim_size(dim_pair[0].first);
  tf::uint64 n = B.dim_size(1 - dim_pair[0].second);;
  tf::uint64 lda = dim_pair[0].first == 0 ? m : k;
  tf::uint64 ldb = dim_pair[0].second == 1 ? k : n;
  tf::uint64 ldc = n;
  // Transpose
  Transpose ta = dim_pair[0].first == 0 ? Transpose::kTranspose : Transpose::kNoTranspose;
  Transpose tb = dim_pair[0].second == 1 ? Transpose::kTranspose : Transpose::kNoTranspose;
  // Query data
  auto A_ptr = AsDeviceMemory(A.template unaligned_flat<T>().data(),
                              A.template unaligned_flat<T>().size());
  auto B_ptr = AsDeviceMemory(B.template unaligned_flat<T>().data(),
                              B.template unaligned_flat<T>().size());
  auto C_ptr = AsDeviceMemory(C->template unaligned_flat<T>().data(),
                              C->template unaligned_flat<T>().size());
  // Launch computation
  bool status = stream->ThenBlasGemm(tb,
                                     ta,
                                     n,
                                     m,
                                     k,
                                     alpha,
                                     B_ptr,
                                     ldb,
                                     A_ptr,
                                     lda,
                                     beta,
                                     &C_ptr,
                                     ldc).ok();
  if (!status) {
    ctx->SetStatus(tf::errors::Internal("Blas GEMM with failed:  m=", m,
                                        ", n=", n,
                                        " k=", k,
                                        " lda=", lda,
                                        " ldb=", ldb,
                                        " ldc=", ldc));
  }
}

template<typename T>
void LinearAlgebra<GPUDevice, T>::Gemm(Context* ctx,
                                       const T* a_ptr,
                                       const uint64_t& a_row,
                                       const uint64_t& a_col,
                                       const uint64_t& a_stride,
                                       const bool& trans_a,
                                       const T& alpha,
                                       const T* b_ptr,
                                       const uint64_t& b_row,
                                       const uint64_t& b_col,
                                       const uint64_t& b_stride,
                                       const bool& trans_b,
                                       const T& beta,
                                       Tensor* C) {
  using TType = perftools::gputools::blas::Transpose;
  using DeviceMem = stream_executor::DeviceMemory<T>;
  // Define output dimensions
  // Adapted from:
  // https://github.com/tensorflow/tensorflow/blob/master/tensorflow/core/kernels/matmul_op.cc
  std::pair<tf::uint64, tf::uint64> dim_pair;
  dim_pair.first = trans_a ? 0 : 1;
  dim_pair.second = trans_b ? 1 : 0;
  tf::uint64 a_dim[] = {a_row, a_col};
  tf::uint64 b_dim[] = {b_row, b_col};
  tf::uint64 a_dim_remain = 1 - dim_pair.first;
  tf::uint64 b_dim_remain = 1 - dim_pair.second;
  tf::uint64 c_row = a_dim[a_dim_remain];
  tf::uint64 c_col = b_dim[b_dim_remain];

  tf::uint64 m = a_dim[a_dim_remain];
  tf::uint64 k = a_dim[dim_pair.first];
  tf::uint64 n = b_dim[b_dim_remain];
  tf::uint64 lda = dim_pair.first == 0 ? m : a_stride;     // To adjust with a_stride
  tf::uint64 ldb = dim_pair.second == 1 ? b_stride : n;    // To adjust with b_stride
  tf::uint64 ldc = n;


  OP_REQUIRES(ctx,
              a_dim[dim_pair.first] == b_dim[dim_pair.second],
              tf::errors::InvalidArgument("Matrix size-incompatible: A: ", a_row, "x", a_col,
                                          ", B: ", b_row, "x", b_col));
  OP_REQUIRES(ctx,
              c_row == C->dim_size(0) && c_col == C->dim_size(1),
              tf::errors::InvalidArgument("Matrix C has wrong dimension: ",
                                          C->shape().DebugString()));





  // Transpose
  TType ta = trans_a ? TType::kTranspose : TType::kNoTranspose;
  TType tb = trans_b ? TType::kTranspose : TType::kNoTranspose;
  // Convert to device memory object
  auto* c_ptr = C->template flat<T>().data();
  tf::uint64 a_nb_bytes = (((a_row - 1) * a_stride) + a_col) * sizeof(T);
  tf::uint64 b_nb_bytes = (((a_row - 1) * a_stride) + a_col) * sizeof(T);
  tf::uint64 c_nb_bytes = c_row * c_col * sizeof(T);
  auto A_ptr = DeviceMem::MakeFromByteSize((void*)a_ptr, a_nb_bytes);
  auto B_ptr = DeviceMem::MakeFromByteSize((void*)b_ptr, b_nb_bytes);
  auto C_ptr = DeviceMem::MakeFromByteSize((void*)c_ptr, c_nb_bytes);


  // Launch computation
  auto* stream = ctx->op_device_context()->stream();
  bool status = stream->ThenBlasGemm(tb,
                                     ta,
                                     n,
                                     m,
                                     k,
                                     alpha,
                                     B_ptr,
                                     ldb,
                                     A_ptr,
                                     lda,
                                     beta,
                                     &C_ptr,
                                     ldc).ok();
  if (!status) {
    ctx->SetStatus(tf::errors::Internal("Blas GEMM with failed:  m=", m,
                                        ", n=", n,
                                        " k=", k,
                                        " lda=", lda,
                                        " ldb=", ldb,
                                        " ldc=", ldc));
  }
}




#pragma mark -
#pragma mark Explicit Instantiation

/** LinearAlgebra - Float */
template class LinearAlgebra<GPUDevice, float>;
/** LinearAlgebra - Float */
template class LinearAlgebra<GPUDevice, double>;

}  // namepsace Functor
}  // namespace LTS5