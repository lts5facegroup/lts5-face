/**
 *  @file   renderer_op.cu
 *  @brief  RenderingOp
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   22/7/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include "cuda_runtime.h"
#include "cuda.h"

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"

#include "lts5/tensorflow_op/layers/landmark_projector.hpp"
#include "lts5/tensorflow_op/layers/landmark_projector_grad.hpp"
#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/cuda/utils/math/device_math.hpp"

using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Vector 2 */
template<typename T>
using Vec2 = typename MathTypes<T>::Vec2;
/** Const Vector 2 */
template<typename T>
using CVec2 = typename MathTypes<T>::ConstVec2;
/** Vector 3 */
template<typename T>
using Vec3 = typename MathTypes<T>::Vec3;
/** Const Vector 3 */
template<typename T>
using CVec3 = typename MathTypes<T>::ConstVec3;
/** Const Vector 3 int */
using CVec3i = typename MathTypes<int>::ConstVec3;
/** Const Vector 4 */
template<typename T>
using CVec4 = typename MathTypes<T>::ConstVec4;

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

/**
 * @name    SetValue
 * @brief   Initialize an array to a given value
 * @param[in] value     Value to set
 * @param[in] n         Array width
 * @param[out] array    Array pointer
 * @tparam T    Data type
 */
template<typename T>
__global__
void SetValue(const T value, const int n, Vec3<T>* array) {
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;
  if (ix < n) {
    array[(iy * n) + ix] = Pack3<T>(value);
  }
}

/**
 * @name    Project
 * @brief   Project landmarks
 * @param[in] vertex    Vertex array
 * @param[in] normal    Normal array
 * @param[in] indices   Indices array
 * @param[in] n_idx     Number of landmarks
 * @param[in] n_col     Number of columns in `indices` array
 * @param[in] n_vertex  Number of vertex in `vertex` array
 * @param[in] focal     Focal length
 * @param[in] cx        mid point - x
 * @param[in] cy        mid point - y
 * @param[in] height    Image height
 * @param[in] top_left  If true convert to top left coordinate system
 * @param[out] landmark Projected landmarks
 * @param[out] picked   Selected vertex index for projection
 * @tparam T    Data type
 */
template<typename T>
__global__
void Project(const CVec3<T>* vertex,
             const CVec3<T>* normal,
             const int* indices,
             const int n_idx,
             const int n_col,
             const int n_vertex,
             const T focal,
             const T cx,
             const T cy,
             const T height,
             const bool top_left,
             Vec2<T>* landmark,
             int* picked) {
  // Index
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;  // [0, Idx[
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;  // [0, Batch]

  if (ix >= n_idx) return;

  // Seek vertex index
  const int n_candidate = indices[ix * n_col];
  const int* idx_ptr = &(indices[(ix * n_col) + 1]);
  int pick = n_candidate == 1 ? idx_ptr[0] : -1;
  if (pick == -1) {
    // Need to select proper vertex to project from candidates
    T min_ang = T(2.0);
    for (int j = 0; j < n_candidate; ++j) {
      const int idx = idx_ptr[j];
      const auto n = normal[(iy * n_vertex) + idx];
      auto vert = vertex[(iy * n_vertex) + idx];
      vert = normalize(vert);
      // Compute "angle" between ray that goes from o-to-v and vn
      T ang = std::abs(dot(vert, n));
      if (ang < min_ang) {
        min_ang = ang;
        pick = idx;
      }
    }
  }
  assert(pick != -1);

  picked[(iy * n_idx) + ix] = pick;
  // Project selected vertex
  const auto vert = vertex[(iy * n_vertex) + pick];
  T u = focal * (vert.x / -vert.z) + cx;
  T v = focal * (vert.y / -vert.z) + cy;
  if (top_left) {
    v = height - v;
  }
  landmark[(iy * n_idx) + ix] = Pack<T>(u, v);
}

/**
 * @name    get_rotation_angle
 * @brief Get rotation angle around `y` axis
 * @param[in] rvec Rotation in axis-angle representation
 * @return  Rotation angle, float
 * @tparam T Data type
 */
template<typename T>
__device__
T get_rotation_angle(CVec3<T>& rvec) {
  T ang = std::sqrt((rvec.x * rvec.x) + (rvec.y * rvec.y) + (rvec.z * rvec.z));
  if (ang < std::numeric_limits<T>::epsilon()) {
    // Identity
    return T(0.0);
  } else {
    // Convert to angles
    T ct = std::cos(ang);
    T st = std::sin(ang);
    T c1 = T(1.0) - ct;
    T i_ang = ang ? T(1.0) / ang : T(0.0);
    T rx = rvec.x * i_ang;
    T ry = rvec.y * i_ang;
    T rz = rvec.z * i_ang;
    // Define part of Rmat
    T m0 = ct + (c1 * rx * rx);
    T m3 = (c1 * ry * rx) + (st * rz);
    T m6 = (c1 * rz * rx) - (st * ry);
    // Compute euler's angles
    return std::atan2(-m6, std::sqrt((m0 * m0) + (m3 * m3)));
  }
}

/**
 * @name    Project
 * @brief   Project landmarks
 * @param[in] vertex    Vertex array
 * @param[in] normal    Normal array
 * @param[in] indices   Indices array
 * @param[in] rvec      Rotation angle
 * @param[in] n_idx     Number of landmarks
 * @param[in] n_col     Number of columns in `indices` array
 * @param[in] n_vertex  Number of vertex in `vertex` array
 * @param[in] focal     Focal length
 * @param[in] cx        mid point - x
 * @param[in] cy        mid point - y
 * @param[in] height    Image height
 * @param[in] thresh    Frontal detection threshold
 * @param[in] top_left  If true convert to top left coordinate system
 * @param[out] landmark Projected landmarks
 * @param[out] picked   Selected vertex index for projection
 * @tparam T    Data type
 */
template<typename T>
__global__
void ProjectV2(const CVec3<T>* vertex,
               const CVec3<T>* normal,
               const int* indices,
               const CVec3<T>* rvec,
               const int n_idx,
               const int n_col,
               const int n_vertex,
               const T focal,
               const T cx,
               const T cy,
               const T height,
               const T thresh,
               const bool top_left,
               Vec2<T>* landmark,
               int* picked) {
  // Index
  __shared__ T head_ang;
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;  // [0, Idx[
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;  // [0, Batch]

  if (ix == 0) {
    // Get face rotation around `y` axis
    head_ang = get_rotation_angle<T>(rvec[iy]);
  }

  __syncthreads();

  if (ix >= n_idx) return;

  // Select proper vertex
  // -----------------------------------------------
  const int n_candidate = indices[ix * n_col];
  const int* idx_ptr = &(indices[(ix * n_col) + 1]);
  int pick = n_candidate == 1 ? idx_ptr[0] : -1;
  if (pick == -1) {
    // Dynamic landmarks
    if (std::abs(head_ang) < thresh) {
      // Considered as frontal, pick first one
      pick = idx_ptr[0];
    } else {
      // Not facing camera
      T min_ang = T(2.0);
      if (head_ang > T(0.0)) {
        // Right visible
        // ix < 9 are fixed
        // ix > 9 are dynamic
        if (ix < 9) {
          pick = idx_ptr[0];
        } else {
          for (int j = 0; j < n_candidate; ++j) {
            const int idx = idx_ptr[j];
            const auto n = normal[(iy * n_vertex) + idx];
            auto vert = vertex[(iy * n_vertex) + idx];
            vert = normalize(vert);
            // Compute "angle" between ray that goes from o-to-v and vn
            T ang = std::abs(dot(vert, n));
            if (ang < min_ang) {
              min_ang = ang;
              pick = idx;
            }
          }
        }
      } else {
        // Left visible
        // ix > 9 are fixed
        // ix < 9 are dynamic
        if (ix > 9) {
          pick = idx_ptr[0];
        } else {
          for (int j = 0; j < n_candidate; ++j) {
            const int idx = idx_ptr[j];
            const auto n = normal[(iy * n_vertex) + idx];
            auto vert = vertex[(iy * n_vertex) + idx];
            vert = normalize(vert);
            // Compute "angle" between ray that goes from o-to-v and vn
            T ang = std::abs(dot(vert, n));
            if (ang < min_ang) {
              min_ang = ang;
              pick = idx;
            }
          }
        }
      }
    }
  }
  assert(pick != -1);
  picked[(iy * n_idx) + ix] = pick;

  // Project selected vertex
  // -----------------------------------------------
  const auto vert = vertex[(iy * n_vertex) + pick];
  T u = focal * (vert.x / -vert.z) + cx;
  T v = focal * (vert.y / -vert.z) + cy;
  if (top_left) {
    v = height - v;
  }
  landmark[(iy * n_idx) + ix] = Pack<T>(u, v);
}


template<typename T>
__global__
void ProjectionGrad(CVec2<T>* g_landmark,
                    CVec3<T>* vertex,
                    const int* picked,
                    const int n_idx,
                    const int n_vertex,
                    const T focal,
                    const bool top_left,
                    Vec3<T>* grad) {
  // Index
  int ix = (blockIdx.x * blockDim.x) + threadIdx.x;  // [0, Idx[
  int iy = (blockIdx.y * blockDim.y) + threadIdx.y;  // [0, Batch]

  if (ix >= n_idx) return;

  // Back-propagated grad
  CVec2<T> bgrad = g_landmark[(iy * n_idx) + ix];
  // Vertex index selected
  const int v_idx = picked[(iy * n_idx) + ix];
  const int o_idx = (iy * n_vertex) + v_idx;
  // Grad
  CVec3<T> v = vertex[o_idx];
  Vec3<T> du_dx = Pack3<T>(0.0); // du_dV{x,y,z}
  du_dx.x = -focal / v.z;
  du_dx.z = ((focal * v.x) / (v.z * v.z));
  Vec3<T> dv_dx = Pack3<T>(0.0); // dv_dV{x,y,z}
  dv_dx.y = -focal / v.z;
  dv_dx.z = ((focal * v.y) / (v.z * v.z));
  // Adjust for top_left system
  if (top_left) {
    dv_dx.y *= T(-1.0);
    dv_dx.z *= T(-1.0);
  }
  auto g = Pack<T>((du_dx.x * bgrad.x) + (dv_dx.x * bgrad.y),
                   (du_dx.y * bgrad.x) + (dv_dx.y * bgrad.y),
                   (du_dx.z * bgrad.x) + (dv_dx.z * bgrad.y));
  grad[o_idx] = g;
}

}  // namespace device


/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {


/**
 * @class   LandmarkGenerator
 * @brief   Project 3D points to image plane using pinhole camera model
 * @author  Christophe Ecabert
 * @date    16/08/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct LandmarkGenerator<GPUDevice, T> {
  /**
   * @name  operator()
   * @brief Project landmarks to image plane
   * @param[in,out] ctx     Op's context
   * @param[in] vertex      Surface's vertices
   * @param[in] vertex      Surface's normal
   * @param[in] indices     Vertex indices to project
   * @param[in] focal       Focal length
   * @param[in] height      Image height
   * @param[in] width       Image width
   * @param[in] top_left    If `True` convert to top left coordinate system
   * @param[out] landmark   Surface's normals computed
   * @param[out] picked     Vertex index selected for projection
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* vertex,
                 const tf::Tensor* normal,
                 const tf::Tensor* indices,
                 const T& focal,
                 const T& height,
                 const T& width,
                 const bool& top_left,
                 tf::Tensor* landmark,
                 tf::Tensor* picked) {
    // Acces raw pointer
    const auto* v_ptr = reinterpret_cast<CVec3<T>*>(vertex->flat<T>().data());
    const auto* n_ptr = reinterpret_cast<CVec3<T>*>(normal->flat<T>().data());
    const auto* idx_ptr = indices->flat<int>().data();
    auto* lms_ptr = reinterpret_cast<Vec2<T>*>(landmark->flat<T>().data());
    auto* pick_ptr = reinterpret_cast<int*>(picked->flat<int>().data());
    // Dimensions
    int bsize = static_cast<int>(vertex->dim_size(0));
    int n_vertex = static_cast<int>(vertex->dim_size(1));
    int n_idx = static_cast<int>(indices->dim_size(0));
    int n_col = static_cast<int>(indices->dim_size(1));
    dim3 thd(32, 1);
    dim3 bck((n_idx + 31) / 32, bsize);
    auto& stream = ctx->eigen_gpu_device().stream();
    // Compute
    const T cx = width / T(2.0);
    const T cy = height / T(2.0);
    device::Project<T><<<bck, thd, 0, stream>>>(v_ptr,
            n_ptr,
            idx_ptr,
            n_idx,
            n_col,
            n_vertex,
            focal,
            cx,
            cy,
            height,
            top_left,
            lms_ptr,
            pick_ptr);
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

/**
 * @class   LandmarkGeneratorV2
 * @brief   Project 3D points to image plane using pinhole camera model
 * @author  Christophe Ecabert
 * @date    16/08/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct LandmarkGeneratorV2<GPUDevice, T> {
  /**
   * @name  operator()
   * @brief Project landmarks to image plane
   * @param[in,out] ctx     Op's context
   * @param[in] vertex      Surface's vertices
   * @param[in] vertex      Surface's normal
   * @param[in] indices     Vertex indices to project
   * @param[in] rvec        Rotation vector
   * @param[in] focal       Focal length
   * @param[in] height      Image height
   * @param[in] width       Image width
   * @param[in] thresh      Head rotation threshold for frontal detection
   * @param[in] top_left    If `True` convert to top left coordinate system
   * @param[out] landmark   Surface's normals computed
   * @param[out] picked     Vertex index selected for projection
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* vertex,
                 const tf::Tensor* normal,
                 const tf::Tensor* indices,
                 const tf::Tensor* rvec,
                 const T& focal,
                 const T& height,
                 const T& width,
                 const T& thresh,
                 const bool& top_left,
                 tf::Tensor* landmark,
                 tf::Tensor* picked) {
    // Acces raw pointer
    const auto* v_ptr = reinterpret_cast<CVec3<T>*>(vertex->flat<T>().data());
    const auto* n_ptr = reinterpret_cast<CVec3<T>*>(normal->flat<T>().data());
    const auto* idx_ptr = indices->flat<int>().data();
    const auto* rvec_ptr = reinterpret_cast<CVec3<T>*>(rvec->flat<T>().data());
    auto* lms_ptr = reinterpret_cast<Vec2<T>*>(landmark->flat<T>().data());
    auto* pick_ptr = reinterpret_cast<int*>(picked->flat<int>().data());
    // Dimensions
    int bsize = static_cast<int>(vertex->dim_size(0));
    int n_vertex = static_cast<int>(vertex->dim_size(1));
    int n_idx = static_cast<int>(indices->dim_size(0));
    int n_col = static_cast<int>(indices->dim_size(1));
    dim3 thd(32, 1);
    dim3 bck((n_idx + 31) / 32, bsize);
    auto& stream = ctx->eigen_gpu_device().stream();
    // Compute
    const T cx = width / T(2.0);
    const T cy = height / T(2.0);
    device::ProjectV2<T><<<bck, thd, 0, stream>>>(v_ptr,
            n_ptr,
            idx_ptr,
            rvec_ptr,
            n_idx,
            n_col,
            n_vertex,
            focal,
            cx,
            cy,
            height,
            thresh,
            top_left,
            lms_ptr,
            pick_ptr);
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

/**
 * @class   LandmarkGradientHelper
 * @brief   Projection gradient helper
 * @author  Christophe Ecabert
 * @date    16/08/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct LandmarkGradientHelper<GPUDevice, T> {
  /**
   * @name  operator()
   * @brief Project landmarks to image plane
   * @param[in,out] ctx     Op's context
   * @param[in] g_landmark  Back-propagated gradient
   * @param[in] vertex      Surface's vertex
   * @param[in] picked      Vertex indices to selected for projection
   * @param[in] focal       Focal length
   * @param[in] top_left    If `True` convert to top left coordinate system
   * @param[out] grad       Op's gradient
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_landmark,
                 const tf::Tensor* vertex,
                 const tf::Tensor* picked,
                 const T& focal,
                 const bool& top_left,
                 tf::Tensor* grad) {
    // Acces raw pointer
    const auto* g_lms_ptr = reinterpret_cast<CVec2<T>*>(g_landmark->flat<T>().data());
    const auto* v_ptr = reinterpret_cast<CVec3<T>*>(vertex->flat<T>().data());
    const int* sel_ptr = picked->flat<int>().data();
    auto* grad_ptr = reinterpret_cast<Vec3<T>*>(grad->flat<T>().data());


    // Dimensions
    // g_landmark:  [Batch, K, 2]
    // vertex:      [Batch, N, 3]
    // picked:      [Batch, K]
    // grad:        [Batch, N, 3]
    int bsize = static_cast<int>(vertex->dim_size(0));
    int n_vertex = static_cast<int>(vertex->dim_size(1));
    int n_idx = static_cast<int>(picked->dim_size(1));
    auto& stream = ctx->eigen_gpu_device().stream();
    // Set to zero
    {
      dim3 thd(1024, 1);
      dim3 bck(((n_vertex) + 511) / 512, bsize);
      device::SetValue<T><<<bck, thd, 0, stream>>>(T(0.0), n_vertex, grad_ptr);
    }
    // Compute grad
    {
      dim3 thd(32, 1);
      dim3 bck((n_idx + 31) / 32, bsize);
      device::ProjectionGrad<T><<<bck, thd, 0, stream>>>(g_lms_ptr,
                                                         v_ptr,
                                                         sel_ptr,
                                                         n_idx,
                                                         n_vertex,
                                                         focal,
                                                         top_left,
                                                         grad_ptr);
    }
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

#pragma mark -
#pragma mark Explicit Instantiation

/** Float */
template struct LandmarkGenerator<GPUDevice, float>;
template struct LandmarkGeneratorV2<GPUDevice, float>;
template struct LandmarkGradientHelper<GPUDevice, float>;
/** Double */
template struct LandmarkGenerator<GPUDevice, double>;
template struct LandmarkGeneratorV2<GPUDevice, double>;
template struct LandmarkGradientHelper<GPUDevice, double>;

}  // namespace Functor
}  // namespace LTS5

