/**
 *  @file   rasterizer_op.cu
 *  @brief  CUDA implementation of rasterizer gradient
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/27/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <limits>
#include "cuda_runtime.h"
#include "cuda.h"

#include "opencv2/core.hpp"

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"

#include "lts5/utils/logger.hpp"
#include "lts5/tensorflow_op/layers/rasterizer_op.hpp"
#include "lts5/tensorflow_op/layers/rasterizer_op_grad.hpp"

#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
#include "lts5/cuda/tensorflow_op/device_utils.hpp"

using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


// Jacobian formalism
// ----------------------------
// Numerator layout: https://en.wikipedia.org/wiki/Matrix_calculus
//         | dy1_dx1  ...  dy1_dxn |
// dy_dx = |          ...          |
//         | dym_dx1  ...  dym_dxn |
//
//    x ---- Inputs --->
//    |
// outputs
//    |
//    v


/** Vector 2 */
template<typename T>
using Vec2 = typename MathTypes<T>::Vec2;
/** Vector 3 */
template<typename T>
using Vec3 = typename MathTypes<T>::Vec3;
/** Vector 4 */
template<typename T>
using Vec4 = typename MathTypes<T>::Vec4;
/** Const Vector 2 */
template<typename T>
using CVec2 = typename MathTypes<T>::ConstVec2;
/** Const Vector 3 */
template<typename T>
using CVec3 = typename MathTypes<T>::ConstVec3;
/** Const Vector int3 */
using CVec3i = typename MathTypes<int>::ConstVec3;
/** Const Vector 4 */
template<typename T>
using CVec4 = typename MathTypes<T>::ConstVec4;

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

template<class T>
struct item_return{ typedef T type; };

template<typename T>
__forceinline__ __device__
typename item_return<T>::type AsInt(T value);

template<>
struct item_return<float>{ typedef int type; };
template<>
struct item_return<double>{ typedef long long type; };

template<>
__forceinline__ __device__
item_return<float>::type AsInt<float>(float value) {
  return __float_as_int(value);
}

template<>
__forceinline__ __device__
item_return<double>::type AsInt<double>(double value) {
  return __double_as_longlong(value);
}

/**
 * @name    EyeToClip
 * @brief   Project a given vertex from eye space (i.e. camera space) into clip
 *        space.
 * @param[in] proj  Projection matrix
 * @param[in] x     Vertex to project
 * @param[out] v    Projected vertex
 * @param[out] dv_dx    Derivative with respect to input vertex `x`
 * @tparam T Data type
 */
template<typename T>
__device__ __forceinline__
void EyeToClip(const Mat4x4<T>& proj,
               const Vec3<T>& x,
               Vec4<T>* v,
               Mat4x3<T>* dv_dx) {
  // Project
  *v = proj * Pack(x.x, x.y, x.z, T(1.0));
  // Grad
  *dv_dx = Mat4x3<T>(proj.a, T(0.0), T(0.0),
                     T(0.0), proj.f, T(0.0),
                     T(0.0), T(0.0), proj.k,
                     T(0.0), T(0.0), T(-1.0));
}

/**
 * @name    ClipToNdc
 * @brief   Transform vertex from clip space to NDC
 * @param[in] x     Point in clip space
 * @param[out] v    Transformed `x` in NDC space
 * @param[out] dv_dx    Derivative of `v` with respect to `x`
 * @tparam T Data type
 */
template<typename T>
__device__ __forceinline__
void ClipToNdc(const Vec4<T>& x, Vec3<T>* v, Mat3x4<T>* dv_dx) {
  // Convert to NDC
  const T i_w = T(1.0) / (x.w + std::numeric_limits<T>::epsilon());
  v->x = x.x * i_w;
  v->y = x.y * i_w;
  v->z = x.z * i_w;
  // Jacobian
  const T i_w2 = -(i_w * i_w);
  *dv_dx = Mat3x4<T>(i_w, T(0.0), T(0.0), x.x * i_w2,
                     T(0.0), i_w, T(0.0), x.y * i_w2,
                     T(0.0), T(0.0), i_w, x.z * i_w2);
}

/**
 * @name NdcToScreen
 * @brief Convert point in `ndc` space into `screen` space. Drop the `z`
 *  component and only compute values for `x` and `y`. Same goes for jacobian.
 * @param[in] x         Point in `ndc` space
 * @param[in] width     Image width
 * @param[in] height    Image height
 * @param[out] v        Point in `screen` space
 * @param[out] dv_dx    Derivative of `v` with respect to `x`.
 * @tparam T Data type
 */
template<typename T>
__device__ __forceinline__
void NdcToScreen(const Vec3<T>& x,
                 const T& width,
                 const T& height,
                 Vec2<T>* v,
                 Mat2x3<T>* dv_dx) {
  // NDC -> Screen (linear mapping)
  const T half_w = width / T(2.0);
  const T half_h = height / T(2.0);
  v->x = (x.x * half_w) + half_w;
  v->y = -(x.y * half_h) + half_h;
  // Jacobian
  *dv_dx = Mat2x3<T>(half_w, T(0.0), T(0.0),
                     T(0.0), -half_h, T(0.0));
}

template<typename T>
__device__ __forceinline__
T SafeDiv(const T& x, const T& y) {
  T sign_y = y >= T(0.0) ? T(1.0): T(-1.0);
  return x / (y + sign_y * 1e-10);
}

/**
 * @name
 * @brief Compute barycentric coordinate for a given point inside a triangle
 * @param v1 First point in screen space
 * @param v2 Second point in screen space
 * @param v3 Thrid point in screen space
 * @param p  Point for which to compute barycentric coordinates
 * @param bc    Computed barycentric
 * @param dbc_dv1   Jacobian of `bc` with respect to `v1`
 * @param dbc_dv2   Jacobian of `bc` with respect to `v2`
 * @param dbc_dv3   Jacobian of `bc` with respect to `v3`
 * @tparam T Data type
 */
template<typename T>
__device__ __forceinline__
void Barycentrics(const Vec2<T>& v1,
                  const Vec2<T>& v2,
                  const Vec2<T>& v3,
                  const Vec2<T>& p,
                  Vec3<T>* bc,
                  Mat3x2<T>* dbc_dv1,
                  Mat3x2<T>* dbc_dv2,
                  Mat3x2<T>* dbc_dv3) {
  // Compute barycentric coordinates
  const T den = ((v2.y - v3.y) * (v1.x - v3.x)) -
                ((v3.x - v2.x) * (v3.y - v1.y));
                //std::numeric_limits<T>::epsilon();
  const T bc1_num = ((v2.y - v3.y) * (p.x - v3.x)) +
                    ((v3.x - v2.x) * (p.y - v3.y));
  bc->x = SafeDiv(bc1_num, den);          // bc1
  const T bc2_num = ((v3.y - v1.y) * (p.x - v3.x)) +
                    ((v1.x - v3.x) * (p.y - v3.y));
  bc->y = SafeDiv(bc2_num, den);          // bc2
  bc->z = 1.0 - (bc->x + bc->y);  // bc3
  // Jacobian
  // bc1_dv1
  dbc_dv1->a = -bc->x * SafeDiv((v2.y - v3.y), den);
  dbc_dv1->b = -bc->x * SafeDiv((v3.x - v2.x), den);
  // bc2_dv1
  dbc_dv1->c = SafeDiv(((p.y - v3.y) - (bc->y * (v2.y - v3.y))), den);
  dbc_dv1->d = SafeDiv(((v3.x - p.x) - (bc->y * (v3.x - v2.x))), den);
  // bc3_dv1
  dbc_dv1->e = -(dbc_dv1->a + dbc_dv1->c);
  dbc_dv1->f = -(dbc_dv1->b + dbc_dv1->d);

  // bc1_dv2
  dbc_dv2->a = SafeDiv(((v3.y - p.y) - (bc->x * (v3.y - v1.y))), den);
  dbc_dv2->b = SafeDiv(((p.x - v3.x) - (bc->x * (v1.x - v3.x))), den);
  // bc2_dv2
  dbc_dv2->c = -bc->y * SafeDiv((v3.y - v1.y), den);
  dbc_dv2->d = -bc->y * SafeDiv((v1.x - v3.x), den);
  // bc3_dv2
  dbc_dv2->e = -(dbc_dv2->a + dbc_dv2->c);
  dbc_dv2->f = -(dbc_dv2->b + dbc_dv2->d);

  // bc1_dv3
  dbc_dv3->a = SafeDiv(((p.y - v2.y) - (bc->x * (v1.y - v2.y))), den);
  dbc_dv3->b = SafeDiv(((v2.x - p.x) - (bc->x * (v2.x - v1.x))), den);
  // bc2_dv3
  dbc_dv3->c = SafeDiv(((v1.y - p.y) - bc->y * (v1.y - v2.y)), den);
  dbc_dv3->d = SafeDiv(((p.x - v1.x) - bc->y * (v2.x - v1.x)), den);
  // bc3_dv3
  dbc_dv3->e = -(dbc_dv3->a + dbc_dv3->c);
  dbc_dv3->f = -(dbc_dv3->b + dbc_dv3->d);
}

/**
 * @name sign
 * @brief Implement sign function: https://stackoverflow.com/a/4609795
 *
 * @param val   Value
 * @return Sign of the value
 * @tparam T    Data type
 */
template <typename T>
__device__ __forceinline__
T sign(const T& val) {
  return static_cast<T>((T(0) < val) - (val < T(0)));
}

/**
 * @name    NormalizeL1
 * @brief Compute l1 normalization of a given vector `x` and its jacobian
 * @param[in] x     Vector to normalize
 * @param[out] v    Normalized vector
 * @param[out] dv_dx    Jacobian matrix
 * @tparam T    Data type
 */
template<typename T>
__device__ __forceinline__
void NormalizeL1(const Vec3<T>& x, Vec3<T>* v, Mat3x3<T>* dv_dx) {
  // Normalize
  const T norm = (std::abs(x.x) + std::abs(x.y) + std::abs(x.z));
  v->x = SafeDiv(x.x, norm);
  v->y = SafeDiv(x.y, norm);
  v->z = SafeDiv(x.z, norm);
  // Jacobian
  Vec3<T> sign_x = Pack(sign(x.x), sign(x.y), sign(x.z));
  const T norm2 = norm * norm;
  // dVx_d{xyz}
  dv_dx->a = SafeDiv((norm - (x.x * sign_x.x)), norm2);
  dv_dx->b = -SafeDiv((x.x * sign_x.y), norm2);
  dv_dx->c = -SafeDiv((x.x * sign_x.z), norm2);
  // dVy_d{xyz}
  dv_dx->d = -SafeDiv((x.y * sign_x.x),norm2);
  dv_dx->e = SafeDiv((norm - (x.y * sign_x.y)), norm2);
  dv_dx->f = -SafeDiv((x.y * sign_x.z), norm2);
  // dVz_d{xyz}
  dv_dx->g = -SafeDiv((x.z * sign_x.x), norm2);
  dv_dx->h = -SafeDiv((x.z * sign_x.y), norm2);
  dv_dx->i = SafeDiv((norm - (x.z * sign_x.z)), norm2);
}

template<typename T>
__device__ __forceinline__
void PerspectiveCorrection(const Vec3<T>& bcoords,
                           const Vec3<T>& v_w,
                           const Mat3x4<T>* dbc_dv_clip,
                           Vec3<T>* bc_corr,
                           Mat3x4<T>* dbcc_dv) {
  // Apply correction
  Mat3x3<T> dnorm_dbc_corr;
  Vec3<T> bc = Pack(bcoords.x / v_w.x,
                    bcoords.y / v_w.y,
                    bcoords.z / v_w.z);
  NormalizeL1(bc, bc_corr, &dnorm_dbc_corr);
  // Jacobians
  Vec3<T> v_w2 = Pack(v_w.x * v_w.x, v_w.y * v_w.y, v_w.z * v_w.z);
  for (int k = 0; k < 3; ++k) {
    const auto& g_bc = dbc_dv_clip[k];
    const T d = k == 0 ?
                SafeDiv((g_bc.d * v_w.x) - bcoords.x, v_w2.x):
                SafeDiv(g_bc.d, v_w.x);
    const T h = k == 1 ?
                SafeDiv(((g_bc.h * v_w.y) - bcoords.y), v_w2.y) :
                SafeDiv(g_bc.h, v_w.y);
    const T l = k == 2 ?
                SafeDiv(((g_bc.l * v_w.z) - bcoords.z), v_w2.z) :
                SafeDiv(g_bc.l, v_w.z);
    Mat3x4<T> grad(SafeDiv(g_bc.a, v_w.x),
                   SafeDiv(g_bc.b, v_w.x),
                   SafeDiv(g_bc.c, v_w.x),
                   d,
                   SafeDiv(g_bc.e, v_w.y),
                   SafeDiv(g_bc.f, v_w.y),
                   SafeDiv(g_bc.g, v_w.y),
                   h,
                   SafeDiv(g_bc.i, v_w.z),
                   SafeDiv(g_bc.j, v_w.z),
                   SafeDiv(g_bc.k, v_w.z),
                   l);
    dbcc_dv[k] = (dnorm_dbc_corr * grad);
  }
}

/**
 * @name BuildProjectionMatrix
 * @brief Build perspective projection matrix
 * @param p Rasterizer hyper-parameters
 * @param mat   Perspective matrix
 * @tparam T    Data type
 */
template<typename T>
__device__ __forceinline__
void BuildProjectionMatrix(const RasterizerParameters<T>& p, Mat4x4<T>* mat) {
  mat->a = (T(2.0) * p.focal) / static_cast<T>(p.width);
  mat->f = (T(2.0) * p.focal) / static_cast<T>(p.height);
  mat->k = -(p.far + p.near) / (p.far - p.near);
  mat->l = -(T(2.0) * p.far * p.near) / (p.far - p.near);
  mat->o = T(-1.0);
}

template<typename T>
__global__
void Eye2Clip(CVec3<T>* vertex,
              const T* focal,
              T c,
              T d,
              T width,
              T height,
              int n_vertex,
              Vec4<T>* v_clip) {
  unsigned int ix = (blockIdx.x * blockDim.x) + threadIdx.x;   // [0 - N]
  unsigned int iy = (blockIdx.y * blockDim.y) + threadIdx.y;   // [0 - Batch]

  // Check boundaries
  if (ix >= n_vertex) return;

  // Data for current slice (iy)
  auto v = vertex[(iy * n_vertex) + ix]; // input vertex
  const T f = focal[iy];
  // Project
  Vec4<T> vc = Pack(T(2.0) * f * v.x / width,
                    T(2.0) * f * v.y / height,
                    c * v.z + d,
                    -v.z);
  v_clip[(iy * n_vertex) + ix] = vc;

  /*if (std::isnan(vc.x) || std::isnan(vc.y) || std::isnan(vc.z) || std::isnan(vc.w)) {
    printf("%d, %d, v=%.3f, %.3f, %.3f, v_clip=%.3f, %.3f, %.3f, %.3f\n",
           iy, ix,
           v.x, v.y, v.z,
           vc.x, vc.y, vc.z, vc.w);
  }*/
}

template<typename T>
__global__
void Clip2Screen(CVec4<T>* vertex,
                 T width,
                 T height,
                 int n_vertex,
                 bool top_left,
                 Vec2<T>* v_screen) {
  unsigned int ix = (blockIdx.x * blockDim.x) + threadIdx.x;   // [0 - N]
  unsigned int iy = (blockIdx.y * blockDim.y) + threadIdx.y;   // [0 - Batch]

  // Check boundaries
  if (ix >= n_vertex) return;

  // Transform
  auto v_clip = vertex[(iy * n_vertex) + ix]; // input vertex

  // Clip -> NDC
  Vec2<T> v_ndc = Pack(v_clip.x / v_clip.w,
                       v_clip.y / v_clip.w);
  // NDC -> Screen
  Vec2<T> b = Pack(T(0.5) * width,
                   T(0.5) * height);
  Vec2<T> s = Pack(T(0.5) * width,
                   top_left ? T(-0.5) * height : T(0.5) * height);
  Vec2<T> lm = Pack(s.x * v_ndc.x + b.x,
                    s.y * v_ndc.y + b.y);
  v_screen[(iy * n_vertex) + ix] = lm;

  /*if (std::isnan(lm.x) || std::isnan(lm.y)) {
    printf("%d, %d, lm=%.3f, %.3f, v_ndc=%.3f, %.3f, v_clip=%.3f, %.3f, %.3f, %.3f\n",
           iy, ix,
           lm.x, lm.y,
           v_ndc.x, v_ndc.y,
           v_clip.x, v_clip.y, v_clip.z, v_clip.w);
  }*/


}


template<typename T>
__global__
void RasterizerGradKernel(CVec4<T>* image,
                          CVec4<T>* vertex,
                          CVec3i* triangle,
                          CVec4<T>* bp_grad,
                          int n_vertex,
                          T width,
                          T height,
                          Vec4<T>* grad) {

  unsigned int ix = (blockIdx.x * blockDim.x) + threadIdx.x;   // [0 - W]
  unsigned int iy = (blockIdx.y * blockDim.y) + threadIdx.y;   // [0 - H]
  unsigned int ik = (blockIdx.z * blockDim.z) + threadIdx.z;   // [0 - Batch]


  // Check boundaries
  if (ix >= width || iy >= height) return;

  // Define pointer for current slice (ik)
  const int wh = width * height;
  const int p_idx = iy * width + ix;   // Current pixel flat index
  auto* v_ptr = &(vertex[ik * n_vertex]);     // Vertex
  auto* im_ptr = &(image[ik * wh]);           // Rasterized image
  auto* bpg_ptr = &(bp_grad[ik * wh]);        // Back-propagated gradient
  auto* gv_ptr = &(grad[(ik * n_vertex)]);    // dBcoords_dVertex

  // Is current pixel on rasterized surface ?
  int tri_idx = static_cast<int>(im_ptr[p_idx].x);
  if (tri_idx > 0) {
    // Back-propagated gradient
    const Vec4<T> bgrad = bpg_ptr[p_idx];
    auto grad_all_bpg = AsInt<T>(bgrad.y) | AsInt<T>(bgrad.z) | AsInt<T>(bgrad.w);
    if (grad_all_bpg == 0) {
      return;
    }

    // Yes -> compute barycentric coordinate + derivative
    // Get vertex (in eye space)
    const auto& tri = triangle[tri_idx - 1];
    const auto& v1_clip = v_ptr[tri.x];
    const auto& v2_clip = v_ptr[tri.y];
    const auto& v3_clip = v_ptr[tri.z];

    Vec3<T> bcoords;
    Mat3x4<T> dbc_dv_clip[3];

    {   // Make temporary subsection to avoid too much register usage ...

      /*// Eye -> Clip
      // --------------------------------------------------
      Mat4x3<T> dv1_clip_dv1;
      Mat4x3<T> dv2_clip_dv2;
      Mat4x3<T> dv3_clip_dv3;
      EyeToClip(perspective, v1, &v1_clip, &dv1_clip_dv1);
      EyeToClip(perspective, v2, &v2_clip, &dv2_clip_dv2);
      EyeToClip(perspective, v3, &v3_clip, &dv3_clip_dv3);*/

      // Clip -> NDC
      // --------------------------------------------------
      Vec3<T> v1_ndc;
      Vec3<T> v2_ndc;
      Vec3<T> v3_ndc;
      Mat3x4<T> dv1_ndc_dv1_clip;
      Mat3x4<T> dv2_ndc_dv2_clip;
      Mat3x4<T> dv3_ndc_dv3_clip;
      ClipToNdc(v1_clip, &v1_ndc, &dv1_ndc_dv1_clip);
      ClipToNdc(v2_clip, &v2_ndc, &dv2_ndc_dv2_clip);
      ClipToNdc(v3_clip, &v3_ndc, &dv3_ndc_dv3_clip);

      // NDC -> Screen
      // --------------------------------------------------
      Vec2<T> v1_scr;
      Vec2<T> v2_scr;
      Vec2<T> v3_scr;
      Mat2x3<T> dv1_scr_dv1_ndc;
      Mat2x3<T> dv2_scr_dv2_ndc;
      Mat2x3<T> dv3_scr_dv3_ndc;
      NdcToScreen(v1_ndc, (T) width, (T) height, &v1_scr, &dv1_scr_dv1_ndc);
      NdcToScreen(v2_ndc, (T) width, (T) height, &v2_scr, &dv2_scr_dv2_ndc);
      NdcToScreen(v3_ndc, (T) width, (T) height, &v3_scr, &dv3_scr_dv3_ndc);

      // Barycentric coordinates
      // --------------------------------------------------
      Vec2<T> pts = Pack((T) ix + T(0.5), (T) iy + T(0.5));
      Mat3x2<T> dbc_dv1_scr;
      Mat3x2<T> dbc_dv2_scr;
      Mat3x2<T> dbc_dv3_scr;
      Barycentrics(v1_scr, v2_scr, v3_scr,
                   pts,
                   &bcoords,
                   &dbc_dv1_scr, &dbc_dv2_scr, &dbc_dv3_scr);

      // Gradient
      // -------------------------------
      //    dbc          dbc       dv1_scr     dv1_ndc
      // ---------- = --------- * --------- * ----------
      //  dv1_clip     dv1_scr     dv1_ndc     dv1_clip
      dbc_dv_clip[0] = (dbc_dv1_scr * dv1_scr_dv1_ndc) * dv1_ndc_dv1_clip;
      dbc_dv_clip[1] = (dbc_dv2_scr * dv2_scr_dv2_ndc) * dv2_ndc_dv2_clip;
      dbc_dv_clip[2] = (dbc_dv3_scr * dv3_scr_dv3_ndc) * dv3_ndc_dv3_clip;
    }

    // Barycentrics perspective correction
    // --------------------------------------------------
    Vec3<T> bc_corr;
    Mat3x4<T> dbc_corr_dv_clip[3];
    PerspectiveCorrection(bcoords,
                          Pack<T>(v1_clip.w, v2_clip.w, v3_clip.w),
                          &dbc_dv_clip[0],
                          &bc_corr,
                          &dbc_corr_dv_clip[0]);
    // Back-propagate gradient to vertex
    // --------------------------------------------------
    //   dL    |   dL      dL      dL   |'
    // ----- = | ------, ------, ------ |
    //  dBc    |  dBc1    dBc2    dBc2  |
    Vec3<T> dL_dbc = Pack(bgrad.y, bgrad.z, bgrad.w);

    // dL_dvc1
    // dbc_corr_dv[k] hols the following entries
    //
    // |  dBc0  dBc0  dBc0  |
    // |  ----  ----  ----  |
    // |  dVkx  dVky  dVkz  |
    // |                    |
    // |  dBc1  dBc1  dBc1  |
    // |  ----  ----  ----  |
    // |  dVkx  dVky  dVkz  |
    // |                    |
    // |  dBc2  dBc2  dBc2  |
    // |  ----  ----  ----  |
    // |  dVkx  dVky  dVkz  |
    // Therefore it needs to be transposed to have the full derivative!
    Vec4<T> dL_dv = Transpose(dbc_corr_dv_clip[0]) * dL_dbc;
    atomicAdd(&(gv_ptr[tri.x].x), dL_dv.x);
    atomicAdd(&(gv_ptr[tri.x].y), dL_dv.y);
    atomicAdd(&(gv_ptr[tri.x].z), dL_dv.z);
    atomicAdd(&(gv_ptr[tri.x].w), dL_dv.w);

    /*if (std::isnan(dL_dv.x) || std::isnan(dL_dv.y) || std::isnan(dL_dv.z) || std::isnan(dL_dv.w)) {
      printf("dBc_dCLip_0\n %3.f, %3.f, %3.f, %3.f\n %3.f, %3.f, %3.f, %3.f\n %3.f, %3.f, %3.f, %3.f\n",
             dbc_corr_dv_clip[0].a, dbc_corr_dv_clip[0].b, dbc_corr_dv_clip[0].c, dbc_corr_dv_clip[0].d,
             dbc_corr_dv_clip[0].e, dbc_corr_dv_clip[0].f, dbc_corr_dv_clip[0].g, dbc_corr_dv_clip[0].h,
             dbc_corr_dv_clip[0].i, dbc_corr_dv_clip[0].j, dbc_corr_dv_clip[0].j, dbc_corr_dv_clip[0].k);

    }*/

    // dL_dv2
    dL_dv = Transpose(dbc_corr_dv_clip[1]) * dL_dbc;
    atomicAdd(&(gv_ptr[tri.y].x), dL_dv.x);
    atomicAdd(&(gv_ptr[tri.y].y), dL_dv.y);
    atomicAdd(&(gv_ptr[tri.y].z), dL_dv.z);
    atomicAdd(&(gv_ptr[tri.y].w), dL_dv.w);

    /*if (std::isnan(dL_dv.x) || std::isnan(dL_dv.y) || std::isnan(dL_dv.z) || std::isnan(dL_dv.w)) {
      printf("dBc_dCLip_1\n %3.f, %3.f, %3.f, %3.f\n %3.f, %3.f, %3.f, %3.f\n %3.f, %3.f, %3.f, %3.f\n",
             dbc_corr_dv_clip[1].a, dbc_corr_dv_clip[1].b, dbc_corr_dv_clip[1].c, dbc_corr_dv_clip[1].d,
             dbc_corr_dv_clip[1].e, dbc_corr_dv_clip[1].f, dbc_corr_dv_clip[1].g, dbc_corr_dv_clip[1].h,
             dbc_corr_dv_clip[1].i, dbc_corr_dv_clip[1].j, dbc_corr_dv_clip[1].j, dbc_corr_dv_clip[1].k);

    }*/

    // dL_dv3
    dL_dv = Transpose(dbc_corr_dv_clip[2]) * dL_dbc;
    atomicAdd(&(gv_ptr[tri.z].x), dL_dv.x);
    atomicAdd(&(gv_ptr[tri.z].y), dL_dv.y);
    atomicAdd(&(gv_ptr[tri.z].z), dL_dv.z);
    atomicAdd(&(gv_ptr[tri.z].w), dL_dv.w);

    /*if (std::isnan(dL_dv.x) || std::isnan(dL_dv.y) || std::isnan(dL_dv.z) || std::isnan(dL_dv.w)) {
      printf("dBc_dCLip_2\n %3.f, %3.f, %3.f, %3.f\n %3.f, %3.f, %3.f, %3.f\n %3.f, %3.f, %3.f, %3.f\n",
             dbc_corr_dv_clip[2].a, dbc_corr_dv_clip[2].b, dbc_corr_dv_clip[2].c, dbc_corr_dv_clip[2].d,
             dbc_corr_dv_clip[2].e, dbc_corr_dv_clip[2].f, dbc_corr_dv_clip[2].g, dbc_corr_dv_clip[2].h,
             dbc_corr_dv_clip[2].i, dbc_corr_dv_clip[2].j, dbc_corr_dv_clip[2].j, dbc_corr_dv_clip[2].k);
    }*/

  }
}

/**
 * @name    SetValue
 * @brief Initialize 2D array to a given value
 * @param[in] value Value to set
 * @param[in] n     Array dimensions (Last)
 * @param[out] array  2D array
 * @tparam T    Data type
 */
template<typename T>
__global__
void SetValue(const T value, const int n, Vec3<T>* array) {
  unsigned int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  unsigned int iy = (blockIdx.y * blockDim.y) + threadIdx.y;
  if (ix < n) {
    array[(iy * n) + ix] = Pack3(value);
  }
}

template<typename T>
__global__
void SetValue(const T value, const int n, Vec4<T>* array) {
  unsigned int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  unsigned int iy = (blockIdx.y * blockDim.y) + threadIdx.y;
  if (ix < n) {
    array[(iy * n) + ix] = Pack4<T>(value);
  }
}

template<typename T>
__global__
void SetValue(const T value, const int n, T* array) {
  unsigned int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (ix < n) {
    array[ix] = value;
  }
}

template<typename T, int Bsz>
__global__
void Eye2ClipGradKernel(CVec3<T>* vertex,
                        const T* focal,
                        CVec4<T>* grad_in,
                        T c,
                        T d,
                        T width,
                        T height,
                        int n_vertex,
                        Vec3<T>* grad_vertex,
                        T* grad_focal) {
  unsigned int ix = (blockIdx.x * blockDim.x) + threadIdx.x;   // [0 - N]
  unsigned int iy = (blockIdx.y * blockDim.y) + threadIdx.y;   // [0 - Batch]

  // Shared storage for gradient with respect to focal
  __shared__ T grad_f[Bsz];
  grad_f[threadIdx.x] = T(0.0);    // Initialize grad accumulator

  // Check boundaries
  if (ix >= n_vertex) return;

  const unsigned int idx = (iy * n_vertex) + ix;
  // gin == dL_dVclip_x, dL_dVclip_y, dL_dVclip_z, dL_dVclip_w
  const auto gin = grad_in[idx];
  // Current focal
  const T f = focal[iy];

  auto all_grad_in = (AsInt<T>(gin.x) | AsInt<T>(gin.y) |
                      AsInt<T>(gin.z) | AsInt<T>(gin.w));
  if (all_grad_in == 0) {
    // Skip unnecessary calculation since input is null.sss
    return;
  }

  // Compute `grad_vertex`
  // ---------------------------------
  //  dL       dL      dVclip
  // ---- = ------- * --------
  //  dV     dVclip      dV
  Vec3<T> dl_dv = Pack(T(2.0) * f * gin.x / width,
                       T(2.0) * f * gin.y / height,
                       (c * gin.z) - gin.w);
  grad_vertex[idx] = dl_dv;

  /*if (std::isnan(dl_dv.x) || std::isnan(dl_dv.y) || std::isnan(dl_dv.z)) {
    printf("dl_dv=%.3f, %.3f, %.3f\n", dl_dv.x, dl_dv.y, dl_dv.z);
  }*/

  // Compute `grad_focal`
  // ---------------------------------
  const auto v = vertex[idx];
  grad_f[threadIdx.x] = ((T(2.0) * v.x * gin.x / width) +
                         (T(2.0) * v.y * gin.y / height));
  // Do reduction in shared memory
  __syncthreads();
  for (unsigned int s = blockDim.x / 2; s > 0; s >>=1) {
    if (threadIdx.x < s) {
      grad_f[threadIdx.x] += grad_f[threadIdx.x + s];
    }
    __syncthreads();
  }
  // Add everything into global memory
  if (threadIdx.x == 0) {
    atomicAdd(&grad_focal[iy], grad_f[0]);

    /*if (std::isnan(grad_f[0])) {
      printf("dl_df=%.3f,\n", grad_f[0]);
    }*/
  }
}

template<typename T>
__global__
void Clip2ScreenGrad(CVec4<T>* vertex,
                      CVec2<T>* grad_in,
                      T width,
                      T height,
                      bool top_left,
                      int n_vertex,
                      Vec4<T>* grad_vertex) {
  unsigned int ix = (blockIdx.x * blockDim.x) + threadIdx.x;   // [0 - N]
  unsigned int iy = (blockIdx.y * blockDim.y) + threadIdx.y;   // [0 - Batch]

  // Check boundaries
  if (ix >= n_vertex) return;

  const unsigned int idx = (iy * n_vertex) + ix;
  // gin == dL_dVscr_x, dL_dVscr_y
  const auto gin = grad_in[idx];
  const auto v_clip = vertex[idx];

  // Scaling factor
  Vec2<T> s = Pack(T(0.5) * width,
                   top_left ? T(-0.5) * height : T(0.5) * height);
  // dL_dVclip
  Vec4<T> dl_dvc = Pack(SafeDiv(s.x * gin.x, v_clip.w),
                        SafeDiv(s.y * gin.y, v_clip.w),
                        T(0.0),
                        SafeDiv(((-s.x * v_clip.x * gin.x) +
                                 (-s.y * v_clip.y * gin.y)),
                                (v_clip.w * v_clip.w)));
  grad_vertex[idx] = dl_dvc;

  /*if (std::isnan(dl_dvc.x) || std::isnan(dl_dvc.y) || std::isnan(dl_dvc.z) || std::isnan(dl_dvc.w)) {
    printf("dL_dVclip=%3.f, %3.f, %3.f, %3.f\n",
           dl_dvc.x, dl_dvc.y, dl_dvc.z, dl_dvc.w);
  }*/


}


}  // namespace device

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   EyeToClipHelper
 * @brief   Compute perspective projection similar to OpenGL pipeline.
 * @author  Christophe Ecabert
 * @date    30/09/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct EyeToClipHelper<GPUDevice, T> {

  /**
   * @name  operator()
   * #@brief    Project vertex from `eye` space into `clip` space
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex to project, [B, N, 3]
   * @param[in] focal       Focal length to use, [B, 1]
   * @param[in] near        Near plane
   * @param[in] far         Far plane
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] v_clip      Projected vertex in clip space
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& focal,
                 const T& near,
                 const T& far,
                 const T& width,
                 const T& height,
                 tf::Tensor* v_clip) {
    // Acces raw pointer
    auto* v_ptr = reinterpret_cast<CVec3<T>*>(vertex.flat<T>().data());
    auto* f_ptr = focal.flat<T>().data();
    auto* vc_ptr = reinterpret_cast<Vec4<T>*>(v_clip->template flat<T>().data());
    // Dimensions
    int n_img = vertex.dim_size(0);
    int n_vertex = vertex.dim_size(1);
    auto& stream = ctx->eigen_gpu_device().stream();
    // Precompute some values of the projection matrix
    const T c = -(far + near) / (far - near);
    const T d = -(T(2.0) * far * near) / (far - near);
    {
      dim3 thd(1024, 1);
      dim3 bck((n_vertex + 1023) / 1024, n_img);
      device::Eye2Clip<<<bck, thd, 0, stream>>>(v_ptr,
              f_ptr,
              c,
              d,
              width,
              height,
              n_vertex,
              vc_ptr);
    }
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

/**
 * @class   ClipToScreenHelper
 * @brief   Transform vertex in `clip` space into `screen` space
 * @author  Christophe Ecabert
 * @date    01/10/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct ClipToScreenHelper<GPUDevice, T> {

  /**
   * @name  operator()
   * @brief Transform vertex in `clip` into `screen` space.
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex in clip space
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] top_left    True if origin is located at top left corner,
   *    otherwise it is bottom left
   * @param[out] v_screen   Vertex in screen space
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const T& width,
                 const T& height,
                 const bool& top_left,
                 tf::Tensor* v_screen) {
    // Acces raw pointer
    auto* vc_ptr = reinterpret_cast<CVec4<T>*>(vertex.flat<T>().data());
    auto* v_scr_ptr = reinterpret_cast<Vec2<T>*>(v_screen->template flat<T>().data());
    // Dimensions
    int n_img = vertex.dim_size(0);
    int n_vertex = vertex.dim_size(1);
    auto& stream = ctx->eigen_gpu_device().stream();
    {
      dim3 thd(1024, 1);
      dim3 bck((n_vertex + 1023) / 1024, n_img);
      device::Clip2Screen<<<bck, thd, 0, stream>>>(vc_ptr,
              width,
              height,
              n_vertex,
              top_left,
              v_scr_ptr);
    }
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

/**
 * @struct   EyeToClipGradientHelper
 * @brief   EyeToCLip gradient
 * @author  Christophe Ecabert
 * @date    30/09/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct EyeToClipGradientHelper<GPUDevice, T> {

  /**
   * @name  operator()
   * #@brief    Compute projection derivative
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex to project, [B, N, 3]
   * @param[in] focal       Focal length to use, [B, 1]
   * @param[in] grad_in     Back-propagated gradient, [B, N, 4]
   * @param[in] near        Near plane
   * @param[in] far         Far plane
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] grad_vertex Gradient with respect to vertex
   * @param[in] grad_focal  Gradient with respect to focal
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& focal,
                 const tf::Tensor& grad_in,
                 const T& near,
                 const T& far,
                 const T& width,
                 const T& height,
                 tf::Tensor* grad_vertex,
                 tf::Tensor* grad_focal) {
    // Acces raw pointer
    auto* v_ptr = reinterpret_cast<CVec3<T>*>(vertex.flat<T>().data());
    auto* f_ptr = focal.flat<T>().data();
    auto* gin_ptr = reinterpret_cast<CVec4<T>*>(grad_in.flat<T>().data());
    auto* grad_v_ptr = reinterpret_cast<Vec3<T>*>(grad_vertex->template flat<T>().data());
    auto* grad_f_ptr = grad_focal->template flat<T>().data();
    // Dimensions
    int n_img = vertex.dim_size(0);
    int n_vertex = vertex.dim_size(1);
    // Precompute some values of the projection matrix
    const T c = -(far + near) / (far - near);
    const T d = -(T(2.0) * far * near) / (far - near);
    auto& stream = ctx->eigen_gpu_device().stream();
    {

      // Set buffer to zero for grad_vertex
      size_t n_elem = grad_vertex->TotalBytes();
      cudaSafeCall(cudaMemsetAsync((void*)grad_v_ptr, 0, n_elem, stream));
      // Set buffer to zero for grad_focal
      n_elem = grad_focal->TotalBytes();
      cudaSafeCall(cudaMemsetAsync((void*)grad_f_ptr, 0, n_elem, stream));
      // Compute actual gradient
      dim3 thd(1024, 1);
      dim3 bck((n_vertex + 1023) / 1024, n_img);
      device::Eye2ClipGradKernel<T, 1024><<<bck, thd, 0, stream>>>(v_ptr,
              f_ptr,
              gin_ptr,
              c,
              d,
              width,
              height,
              n_vertex,
              grad_v_ptr,
              grad_f_ptr);
    }
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

/**
 * @struct   ClipToScreenGradientHelper
 * @brief   Clip to screen gradient
 * @author  Christophe Ecabert
 * @date    01/10/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct ClipToScreenGradientHelper<GPUDevice, T> {

  /**
   * @name  operator()
   * #@brief    Compute projection derivative
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex to project, [B, N, 4]
   * @param[in] grad_in     Back-propagated gradient, [B, N, 2]
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] top_left    True if origin is located at top left corner,
   *    otherwise it is bottom left
   * @param[in] grad_vertex Gradient with respect to vertex
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& grad_in,
                 const T& width,
                 const T& height,
                 const bool& top_left,
                 tf::Tensor* grad_vertex) {
    // Acces raw pointer
    auto* v_ptr = reinterpret_cast<CVec4<T>*>(vertex.flat<T>().data());
    auto* gin_ptr = reinterpret_cast<CVec2<T>*>(grad_in.flat<T>().data());
    auto* grad_v_ptr = reinterpret_cast<Vec4<T>*>(grad_vertex->template flat<T>().data());
    // Dimensions
    int n_img = vertex.dim_size(0);
    int n_vertex = vertex.dim_size(1);
    auto& stream = ctx->eigen_gpu_device().stream();
    {
      dim3 thd(1024, 1);
      dim3 bck((n_vertex + 1023) / 1024, n_img);
      device::Clip2ScreenGrad<<<bck, thd, 0, stream>>>(v_ptr,
              gin_ptr,
              width,
              height,
              top_left,
              n_vertex,
              grad_v_ptr);
    }
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};


/**
 * @class   RasterizerGradientHelper
 * @brief   Rasterizer gradient
 * @author  Christophe Ecabert
 * @date    27/07/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct RasterizerGradientHelper<GPUDevice, T> {

  /**
   * @name operator()
   * @brief Compute rasterizer gradient
   * @param[in] ctx Ops context
   * @param[in] vertex  Surface vertex
   * @param[in] triangle  Surface triangles
   * @param[in] image  Rasterized image (0: tri idx, 1: bc1, 2: bc2, 3: Mask)
   * @param[in] g_bcoords Back-propagated gradient
   * @param[out] grad Computed gradient
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& triangle,
                 const tf::Tensor& image,
                 const tf::Tensor& g_bcoords,
                 tf::Tensor* grad) {
    // Acces raw pointer
    auto* im_ptr = reinterpret_cast<CVec4<T>*>(image.flat<T>().data());
    auto* v_ptr = reinterpret_cast<CVec4<T>*>(vertex.flat<T>().data());
    auto* tri_ptr = reinterpret_cast<CVec3i*>(triangle.flat<int>().data());
    auto* g_bc_ptr = reinterpret_cast<CVec4<T>*>(g_bcoords.flat<T>().data());
    auto* gv_ptr = reinterpret_cast<Vec4<T>*>(grad->template flat<T>().data());
    // Dimensions
    int n_img = vertex.dim_size(0);
    int n_vertex = vertex.dim_size(1);
    int height = image.dim_size(1);
    int width = image.dim_size(2);
    auto& stream = ctx->eigen_gpu_device().stream();
    { // Set to zero
      dim3 thd(1024, 1);
      dim3 bck((n_vertex + 1023) / 1024, n_img);
      device::SetValue<T><<<bck, thd, 0, stream>>>(T(0.0),
                                                   n_vertex,
                                                   gv_ptr);
    }
    { // Compute grad
      dim3 thd(16, 16, 1);
      dim3 bck((height + 15) / 16, (width + 15) / 16, n_img);
      device::RasterizerGradKernel<T><<<bck, thd, 0, stream>>>(im_ptr,
              v_ptr,
              tri_ptr,
              g_bc_ptr,
              n_vertex,
              width,
              height,
              gv_ptr);
    }
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

#pragma mark -
#pragma mark Explicit Instantiation

/** Float */
template struct EyeToClipHelper<GPUDevice, float>;
template struct ClipToScreenHelper<GPUDevice, float>;
template struct RasterizerGradientHelper<GPUDevice, float>;
template struct EyeToClipGradientHelper<GPUDevice, float>;
template struct ClipToScreenGradientHelper<GPUDevice, float>;
/** Double */
template struct EyeToClipHelper<GPUDevice, double>;
template struct ClipToScreenHelper<GPUDevice, double>;
template struct ClipToScreenGradientHelper<GPUDevice, double>;
#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
template struct RasterizerGradientHelper<GPUDevice, double>;
template struct EyeToClipGradientHelper<GPUDevice, double>;
#endif

}  // namespace Functor
}  // namespace LTS5