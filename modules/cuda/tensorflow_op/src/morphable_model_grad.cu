/**
 *  @file   morphable_model_grad.cpp
 *  @brief  
 *  @ingroup 
 *
 *  @author Christophe Ecabert
 *  @date   7/5/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/cuda/tensorflow_op/linear_algebra_functor.hpp"

#include "lts5/tensorflow_op/morphable_model_grad.hpp"
#include "lts5/tensorflow_op/mm_wrapper.hpp"

namespace tf = tensorflow;

using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {


/*
 * @struct  TextureGradientHelper
 * @brief   Functor helping at the computation of the operation grad
 * @author  Christophe Ecabert
 * @date    04/07/19
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct TextureGradientHelper<GPUDevice, T> {

  /** Model */
  using Model = SharedMorphableModelContainerV2<T>;

  /*
   * @name
   * @brief Compute derivate of the color with respect to the texture parameters
   * @param[in,out] ctx Op's context
   * @param[in] w_tex   Texture's parameters
   * @param[in] g_color Backpropagated gradient
   * @param[in] grad    Newly computed gradient include appearance derivative
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* w_tex,
                 const tf::Tensor* g_color,
                 tf::Tensor* grad) {
    using LA = Functor::LinearAlgebra<GPUDevice , T>;
    // Access shared model
    Model* model = nullptr;
    auto* rm = ctx->resource_manager();
    auto s = rm->Lookup(rm->default_container(),
                        "SharedMorphableModel",
                        &model);
    ctx->SetStatus(s);
    tf::core::ScopedUnref scoped_ref(model);
    if (s.ok()) {
      // Compute derivative -> Ushp
      const auto* Ut = model->get_var_tex(ctx);
      LA::Gemm(ctx, *g_color, false, T(1.0), *Ut, false, T(0.0), grad);
    }
    return s.ok() ? 0 : -1;
  }
};

/*
 * @struct  ShapeGradientHelper
 * @brief   Functor helping at the computation of the operation grad
 * @author  Christophe Ecabert
 * @date    04/07/19
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct ShapeGradientHelper<GPUDevice, T> {
  /** Model */
  using Model = SharedMorphableModelContainerV2<T>;
  /*
   * @name  operator()
   * @brief Compute derivate of the shape with respect to the shape parameters
   * @param[in,out] ctx Op's context
   * @param[in] w_shp       Shape's parameters
   * @param[in] g_vertex    Back-propagated gradient
   * @param[in] grad        Newly computed gradient include shape derivative
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* w_shp,
                 const tf::Tensor* g_vertex,
                 tf::Tensor* grad) {
    using LA = Functor::LinearAlgebra<GPUDevice , T>;
    // Access shared model
    Model* model = nullptr;
    auto* rm = ctx->resource_manager();
    auto s = rm->Lookup(rm->default_container(),
                        "SharedMorphableModel",
                        &model);
    ctx->SetStatus(s);
    tf::core::ScopedUnref scoped_ref(model);
    if (s.ok()) {
      // Compute derivative -> Ushp
      const auto* Us = model->get_var_shape(ctx);
      LA::Gemm(ctx, *g_vertex, false, T(1.0), *Us, false, T(0.0), grad);
    }
    return s.ok() ? 0 : -1;
  }
};

#pragma mark -
#pragma mark Explicit instantiation

/** TextureGradientHelper - float */
template struct TextureGradientHelper<GPUDevice, float>;
/** ShapeGradientHelper - float */
template struct ShapeGradientHelper<GPUDevice, float>;
/** TextureGradientHelper - double */
template struct TextureGradientHelper<GPUDevice, double>;
/** ShapeGradientHelper - double */
template struct ShapeGradientHelper<GPUDevice, double>;

}  // namespace Functor
}  // namespace LTS5