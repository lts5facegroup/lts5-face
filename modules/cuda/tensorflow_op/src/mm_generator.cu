/**
 *  @file   mm_generator.cu
 *  @brief
 *  @ingroup
 *
 *  @author Christophe Ecabert
 *  @date   24/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <vector>

#include "cuda_runtime.h"
#include "cuda.h"

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"

#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/tensorflow_op/mm_generator.hpp"
#include "lts5/tensorflow_op/tensor_utils.hpp"
#include "lts5/tensorflow_op/device_utils.hpp"
#include "lts5/tensorflow_op/mm_storage.hpp"
#include "lts5/cuda/utils/math/device_math.hpp"



using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Vector 3 */
template<typename T>
using Vec3 = typename MathTypes<T>::Vec3;
/** Const Vector 3 */
template<typename T>
using CVec3 = typename MathTypes<T>::ConstVec3;
/** Const int Vector 3 */
using CVec3i = MathTypes<int>::ConstVec3;
/** Const int Vector 3 */
using Vec3i = MathTypes<int>::Vec3;

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @struct  InputBufferRef
 * @brief   Holds reference (input) for finalizing morphable model
 * @tparam T    Data type
 */
template<typename T>
struct InputBufferRef {
  /** Type */
  using CType = typename MathTypes<T>::ConstType;

  /** Connectivity */
  const int* conn;
  /** Conn dimensions */
  int rows;
  int cols;
  /** Cog */
  Vec3<T> cog;
  /** Vertex buffer */
  CVec3<T>* vertex;
  /** Texture buffer */
  CVec3<T>* texture;
  /** Camera parameter */
  CType* cam;
};

/**
 * @struct  OutputBufferRef
 * @brief   Holds reference (output) for finalizing morphable model
 * @tparam T    Data type
 */
template<typename T>
struct OutputBufferRef {
  /** Transformed surface normal */
  Vec3<T>* normal;
  /** OpenGL Vertex buffer */
  Vec3<T>* ogl_vertex;
  /** OpenGL Normal buffer */
  Vec3<T>* ogl_normal;
  /** OpenGL Color buffer */
  Vec3<T>* ogl_color;
};


/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

/**
 * @name    GeneratorInitializerKernel
 * @param[in] in    Input value
 * @param[in] n     Number of value to copy
 * @param[in] out   Output array where to copy data
 * @tparam T    Data type
 */
template<typename T>
__global__
void GeneratorInitializerKernel(const T* in,
                                const int n,
                                const int k,
                                T* out) {
  int ix = blockIdx.x * blockDim.x + threadIdx.x;
  if (ix < n) {
    // Fill one column of output array
    for (int p = 0; p < k; ++p) {
      int oidx = (p * n) + ix;
      out[oidx] = in[ix];
    }
  }
}

/**
 * @name    GeneratorNormalizerKernel
 * @brief   Remove center of gravity from a given shape
 * @param[in] N Number of vertex
 * @param[in] K Number of shapes
 * @param[in] cog   Center of gravity value
 * @param[in,out] shape Shape
 * @tparam T     Data type
 */
template<typename T>
__global__
void GeneratorNormalizerKernel(const int N,
                               const int K,
                               const Vec3<T> cog,
                               Vec3<T>* shape) {
  int ix = blockIdx.x * blockDim.x + threadIdx.x;
  if (ix < N) {
    // Remove cog for this column
    for (int p = 0; p < K; ++p) {
      int oidx = (p * N) + ix;
      shape[oidx] = shape[oidx] - cog;
    }
  }
}

/**
 * @name    NormalKernel
 * @brief   Compute surface's normal
 * @param[in] vertex    Surface vertices
 * @param[in] conn      Connectivity matrix
 * @param[in] n_vertex  Number of rows in connectivity matrix
 * @param[in] n_conn    Number of columns in connectivity matrix
 * @param[out] normal   Surface's normals
 * @tparam T    Data type
 */
template<typename T>
__global__
void NormalKernel(CVec3<T>* vertex,
                  const int* conn,
                  const int n_vertex,
                  const int n_conn,
                  Vec3<T>* normal) {
  int ix =  blockIdx.x * blockDim.x + threadIdx.x;
  int iy =  blockIdx.y * blockDim.y + threadIdx.y;

  if (ix >= n_vertex) return;

  // Compute all normals of the one-ring neighbour of the ith vertex
  const int n_edge = conn[ix * n_conn];
  const int* edges = &(conn[ix * n_conn + 1]);
  Vec3<T> wn = Pack3(T(0.0));
  const auto& A = vertex[iy * n_vertex + ix];
  for (int j = 0; j < n_edge; j += 2) {
    const auto& B = vertex[iy * n_vertex + edges[j]];
    const auto& C = vertex[iy * n_vertex + edges[j + 1]];
    // Define edges AB, AC
    auto AB = B - A;
    auto AC = C - A;
    // Compute surface's normal (triangle ABC)
    Vec3<T> n = cross(AB, AC);
    n = normalize(n);
    // Stack each face contribution and weight with angle
    AB = normalize(AB);
    AC = normalize(AC);
    T ddot = dot(AB, AC);
    ddot = ddot > T(1.0) ? T(1.0) : ddot;
    ddot = ddot < T(-1.0) ? T(-1.0) : ddot;
    const T angle = std::acos(ddot);
    wn += (n * angle);
  }
  // normalize and set
  normal[iy * n_vertex + ix] = normalize(wn);
}

/**
 * @name    TriangleKernel
 * @brief Copy triangulation from one buffer to another one
 * @param[in] tri_in    Input triangulation
 * @param[out] tri_out  Output
 */
__global__
void TriangleKernel(CVec3i* tri_in, Vec3i* tri_out, int n_tri) {
  int ix =  blockIdx.x * blockDim.x + threadIdx.x;
  if (ix >= n_tri) return;
  tri_out[ix] = tri_in[ix];
}
}  // namespace device



/**
 * @class   MMGeneratorInitializer
 * @brief   Device specific shape/tex generator initialization. Copy mean shape
 *          tex
 * @author  Christophe Ecabert
 * @date    30/01/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class MMGeneratorInitializer<GPUDevice, T> {
 public:
  /** Tensor type */
  using Tensor = tensorflow::Tensor;

  /**
   * @name  operator()
   * @brief Copy a given tensor in all rows of the output tensor. Used to copy
   *        mean shape/tex
   * @param[in] ctx         Kernel context
   * @param[in] value       Vector to copy on each row
   * @param[out] output     Where to copy the data
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const Tensor& value,
                 Tensor* output) {
    DCHECK_EQ(value.dim_size(0), output->dim_size(1));

    // Access data
    const int N = static_cast<int>(value.dim_size(0));
    const int n_rep = static_cast<int>(output->dim_size(0));  // Number of rep
    const auto* value_ptr = value.flat<T>().data();
    auto* out_ptr = output->flat<T>().data();

    // Launch computation
    int thd = 1024;
    int bck = (N + 1023) / 1024;
    auto& stream = ctx->eigen_gpu_device().stream();
    device::GeneratorInitializerKernel<T><<<bck, thd, 0, stream>>>(value_ptr,
            N,
            n_rep,
            out_ptr);
    return 0;
  }
};

/**
 * @class   MMGeneratorNormalizer
 * @brief   Device specific shape/tex generator normalizer. Remove center
 *          gravity of a given shape
 * @author  Christophe Ecabert
 * @date    30/01/2019
 * @ingroup tensorflow_op
 * @tparam T        Data type
 */
template<typename T>
class MMGeneratorNormalizer<GPUDevice, T> {
 public:
  /** Tensor type */
  using Tensor = tensorflow::Tensor;

  /**
   * @name  operator()
   * @brief Remove center of gravity from a given shape
   * @param[in] ctx         Kernel context
   * @param[in] cog         Center of gravity
   * @param[in,out] shape   Shape from which to remove cog
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const std::vector<T>& cog,
                 Tensor* shape) {
    // How many vertex
    const int n_vertex = (int)shape->dim_size(1) / 3;
    const int n_shape = (int)shape->dim_size(0);  // Number of shapes
    Vec3<T> c = Pack<T>(cog[0], cog[1], cog[2]);
    auto* shp_ptr = reinterpret_cast<Vec3<T>*>(shape->flat<T>().data());
    // Launch computation
    int thd = 1024;
    int bck = (n_vertex + 1023) / 1024;
    auto& stream = ctx->eigen_gpu_device().stream();
    device::GeneratorNormalizerKernel<T><<<thd, bck, 0, stream>>>(n_vertex,
            n_shape,
            c,
            shp_ptr);
    return 0;
  }
};


/**
 * @class   NormalGenerator
 * @brief   Compute surface's normals
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct NormalGenerator<GPUDevice, T> {

  /** Storage type */
  using Storage = PersistentStorage<T>;

  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in] ctx         Op's context
   * @param[in,out] storage Morphable Model shared storage
   */
  void operator()(tensorflow::OpKernelContext* ctx, Storage* storage) {
    // Retrieve tensors
    const auto* vertex = storage->shape.AccessTensor(ctx);
    const auto* conn = storage->conn.AccessTensor(ctx);
    auto* normal = storage->normal.AccessTensor(ctx);
    // Acces raw pointer
    auto* v_ptr = reinterpret_cast<CVec3<T>*>(vertex->template flat<T>().data());
    const int* conn_ptr = conn->template flat<int>().data();
    auto* n_ptr = reinterpret_cast<Vec3<T>*>(normal->template flat<T>().data());
    // Dimensions
    int n_img = vertex->dim_size(0);
    int n_vertex = vertex->dim_size(1) / 3;
    int n_conn = conn->dim_size(1);
    auto& stream = ctx->eigen_gpu_device().stream();
    dim3 thd(1024, 1);
    dim3 bck((n_vertex + 1023) / 1024, n_img);
    device::NormalKernel<T><<<bck, thd, 0, stream>>>(v_ptr,
            conn_ptr,
            n_vertex,
            n_conn,
            n_ptr);
  }

  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in,out] ctx Op's context
   * @param[in] vertex  Surface's vertcies
   * @param[in] conn    Connectivity
   * @param[out] normal Surface's normals computed
   */
  void operator()(tf::OpKernelContext* ctx,
                  const tf::Tensor* vertex,
                  const tf::Tensor* conn,
                  tf::Tensor* normal) {
    // Acces raw pointer
    auto* v_ptr = reinterpret_cast<CVec3<T>*>(vertex->flat<T>().data());
    const int* conn_ptr = conn->flat<int>().data();
    auto* n_ptr = reinterpret_cast<Vec3<T>*>(normal->flat<T>().data());
    // Dimensions
    int n_img = vertex->dim_size(0);
    int n_vertex = vertex->dim_size(1) / 3;
    int n_conn = conn->dim_size(1);
    auto& stream = ctx->eigen_gpu_device().stream();
    dim3 thd(1024, 1);
    dim3 bck((n_vertex + 1023) / 1024, n_img);
    device::NormalKernel<T><<<bck, thd, 0, stream>>>(v_ptr,
                                                     conn_ptr,
                                                     n_vertex,
                                                     n_conn,
                                                     n_ptr);
  }
};

/**
 * @class   TriangleGenerator
 * @brief   Copy triangulation to output
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam T        Data type
 */
template<typename T>
struct TriangleGenerator<GPUDevice, T> {
  /**
   * @name operator()
   * @brief Copy triangulation to output
   * @param[in,out] ctx
   * @param[in] triangulation
   * @param[out] tri
   */
  void operator()(tf::OpKernelContext* ctx,
                  const tf::Tensor* triangulation,
                  tf::Tensor* tri) {
    // Acces raw pointer
    auto* tri_ptr = reinterpret_cast<CVec3i*>(triangulation->flat<int>().data());
    auto* out_ptr = reinterpret_cast<Vec3i*>(tri->flat<int>().data());
    // Dimensions
    int n_tri = triangulation->dim_size(0);
    auto& stream = ctx->eigen_gpu_device().stream();
    dim3 thd(1024);
    dim3 bck((n_tri + 1023) / 1024);
    device::TriangleKernel<<<bck, thd, 0, stream>>>(tri_ptr, out_ptr, n_tri);
  }
};

#pragma mark -
#pragma mark Explicit Instantiation

/** NormalGenerator - float */
template struct NormalGenerator<GPUDevice, float>;
/** NormalGenerator - double */
template struct NormalGenerator<GPUDevice, double>;

/** TriangleGenerator - float */
template struct TriangleGenerator<GPUDevice, float>;
/** TriangleGenerator - double */
template struct TriangleGenerator<GPUDevice, double>;



/** MMFinalizer - GPU - float */
template class MMFinalizer<GPUDevice, float>;
/** MMFinalizer - GPU - double */
template class MMFinalizer<GPUDevice, double>;

/** MMGeneratorInitializer - Float */
template class MMGeneratorInitializer<GPUDevice, float>;
/** MMGeneratorInitializer - Double */
template class MMGeneratorInitializer<GPUDevice, double>;

/** MMGeneratorNormalizer- Float */
template class MMGeneratorNormalizer<GPUDevice, float>;
/** MMGeneratorNormalizer - Double */
template class MMGeneratorNormalizer<GPUDevice, double>;

}  // namespace Functor
}  // namespace LTS5
