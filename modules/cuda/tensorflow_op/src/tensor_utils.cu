/**
 *  @file   tensor_utils.cu
 *  @brief  Utility function for Matrix
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   20/12/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      Helper function
 */
namespace Functor {

/** GPU Device Abstraction */
using GPUDevice = Eigen::GpuDevice;


/*
 * @name  operator()
 * @brief Add a tensor to another one
 * @param[in] d           Device
 * @param[in] to_add      Tensor to add
 * @param[in,out] params  Tensor with the added value (+=)
 */
template <typename T>
void DenseOp<GPUDevice, T, DenseOpType::kAdd>::operator()(const GPUDevice& d,
                                                          typename tensorflow::TTypes<T>::ConstFlat to_add,
                                                          typename tensorflow::TTypes<T>::Flat params) {
  params.device(d) += to_add;
}

/*
 * @name  operator()
 * @brief Add a tensor to another one - Unaligned
 * @param[in] d           Device
 * @param[in] to_add      Tensor to add
 * @param[in,out] params  Tensor with the added value (+=)
 */
template <typename T>
void DenseOp<GPUDevice, T, DenseOpType::kAdd>::operator()(const GPUDevice& d,
                                                          typename tensorflow::TTypes<T>::UnalignedConstFlat to_add,
                                                          typename tensorflow::TTypes<T>::UnalignedFlat params) {
  params.device(d) += to_add;
}

/*
 * @name  operator()
 * @brief Subtract a tensor to another one
 * @param[in] d           Device
 * @param[in] to_sub      Tensor to subtract
 * @param[in,out] params  Tensor with the subtracted value (-=)
 */
template <typename T>
void DenseOp<GPUDevice, T, DenseOpType::kSub>::operator()(const GPUDevice& d,
                                                          typename tensorflow::TTypes<T>::ConstFlat to_sub,
                                                          typename tensorflow::TTypes<T>::Flat params
) {
  params.device(d) -= to_sub;
}

/*
 * @name  operator()
 * @brief Subtract a tensor to another one - Unaligned
 * @param[in] d           Device
 * @param[in] to_sub      Tensor to subtract
 * @param[in,out] params  Tensor with the subtracted value (-=)
 */
template <typename T>
void DenseOp<GPUDevice, T, DenseOpType::kSub>::operator()(const GPUDevice& d,
                                                          typename tensorflow::TTypes<T>::UnalignedConstFlat to_sub,
                                                          typename tensorflow::TTypes<T>::UnalignedFlat params
) {
  params.device(d) -= to_sub;
}

/*
 * @name  operator()
 * @brief Copy a tensor to another one (device to device)
 * @param[in] d           Device
 * @param[in] to_copy     Tensor to copy (device)
 * @param[in,out] params  Tensor with the copied value (device)
 */
template <typename T>
void DenseOp<GPUDevice, T, DenseOpType::kD2DCopy>::operator()(const GPUDevice& d,
                                                              typename tensorflow::TTypes<T>::ConstFlat to_copy,
                                                              typename tensorflow::TTypes<T>::Flat params) {
  params.device(d) = to_copy;
}

/*
 * @name  operator()
 * @brief Copy a tensor to another one (device to device) - Unaligned
 * @param[in] d           Device
 * @param[in] to_copy     Tensor to copy (device)
 * @param[in,out] params  Tensor with the copied value (device)
 */
template <typename T>
void DenseOp<GPUDevice, T, DenseOpType::kD2DCopy>::operator()(const GPUDevice& d,
                                                              typename tensorflow::TTypes<T>::UnalignedConstFlat to_copy,
                                                              typename tensorflow::TTypes<T>::UnalignedFlat params) {
  params.device(d) = to_copy;
}



/*
 * @name  operator()
 * @brief Copy a tensor to another one from host to device (only copy when
 *        used with CPU device)
 * @param[in] d           Device
 * @param[in] to_copy     Tensor to copy  (host)
 * @param[in,out] params  Tensor with the copied value (device)
 */
template <typename T>
void DenseOp<GPUDevice, T, DenseOpType::kH2DCopy>::operator()(const GPUDevice& d,
                                                              typename tensorflow::TTypes<T>::ConstFlat to_copy,
                                                              typename tensorflow::TTypes<T>::Flat params) {
  d.memcpyHostToDevice(params.data(),
                       to_copy.data(),
                       to_copy.size() * sizeof(T));
}

/*
 * @name  operator()
 * @brief Copy a tensor to another one from host to device (only copy when
 *        used with CPU device) - Unaligned
 * @param[in] d           Device
 * @param[in] to_copy     Tensor to copy  (host)
 * @param[in,out] params  Tensor with the copied value (device)
 */
template<typename T>
void DenseOp<GPUDevice, T, DenseOpType::kH2DCopy>::operator()(const GPUDevice& d,
                                                              typename tensorflow::TTypes<T>::UnalignedConstFlat to_copy,
                                                              typename tensorflow::TTypes<T>::UnalignedFlat params) {
  d.memcpyHostToDevice(params.data(),
                       to_copy.data(),
                       to_copy.size() * sizeof(T));
}



/*
 * @name  operator()
 * @brief Copy a tensor to another one from device to host (only copy when
 *        used with CPU device)
 * @param[in] d           Device
 * @param[in] to_copy     Tensor to copy  (device)
 * @param[in,out] params  Tensor with the copied value (host)
 */
template <typename T>
void DenseOp<GPUDevice, T, DenseOpType::kD2HCopy>::operator()(const GPUDevice& d,
                                                              typename tensorflow::TTypes<T>::ConstFlat to_copy,
                                                              typename tensorflow::TTypes<T>::Flat params) {
  d.memcpyDeviceToHost(params.data(),
                       to_copy.data(),
                       to_copy.size() * sizeof(T));
}

/*
* @name  operator()
* @brief Copy a tensor to another one from device to host (only copy when
*        used with CPU device) - Unaligned
* @param[in] d           Device
* @param[in] to_copy     Tensor to copy  (device)
* @param[in,out] params  Tensor with the copied value (host)
*/
template <typename T>
void DenseOp<GPUDevice, T, DenseOpType::kD2HCopy>::operator()(const GPUDevice& d,
                                                              typename tensorflow::TTypes<T>::UnalignedConstFlat to_copy,
                                                              typename tensorflow::TTypes<T>::UnalignedFlat params) {
  d.memcpyDeviceToHost(params.data(),
                       to_copy.data(),
                       to_copy.size() * sizeof(T));
}



#pragma mark -
#pragma mark Explicit Instantiation

/** DenseOp functor - UInt8 */
template struct DenseOp<GPUDevice, tensorflow::uint8, DenseOpType::kAdd>;
template struct DenseOp<GPUDevice, tensorflow::uint8, DenseOpType::kSub>;
template struct DenseOp<GPUDevice, tensorflow::uint8, DenseOpType::kD2DCopy>;
template struct DenseOp<GPUDevice, tensorflow::uint8, DenseOpType::kH2DCopy>;
template struct DenseOp<GPUDevice, tensorflow::uint8, DenseOpType::kD2HCopy>;

/** DenseOp functor - UInt32 */
template struct DenseOp<GPUDevice, tensorflow::uint32, DenseOpType::kAdd>;
template struct DenseOp<GPUDevice, tensorflow::uint32, DenseOpType::kSub>;
template struct DenseOp<GPUDevice, tensorflow::uint32, DenseOpType::kD2DCopy>;
template struct DenseOp<GPUDevice, tensorflow::uint32, DenseOpType::kH2DCopy>;
template struct DenseOp<GPUDevice, tensorflow::uint32, DenseOpType::kD2HCopy>;

/** DenseOp functor - Int */
template struct DenseOp<GPUDevice, int, DenseOpType::kAdd>;
template struct DenseOp<GPUDevice, int, DenseOpType::kSub>;
template struct DenseOp<GPUDevice, int, DenseOpType::kD2DCopy>;
template struct DenseOp<GPUDevice, int, DenseOpType::kH2DCopy>;
template struct DenseOp<GPUDevice, int, DenseOpType::kD2HCopy>;
/** DenseOp functor - Float */
template struct DenseOp<GPUDevice, float, DenseOpType::kAdd>;
template struct DenseOp<GPUDevice, float, DenseOpType::kSub>;
template struct DenseOp<GPUDevice, float, DenseOpType::kD2DCopy>;
template struct DenseOp<GPUDevice, float, DenseOpType::kH2DCopy>;
template struct DenseOp<GPUDevice, float, DenseOpType::kD2HCopy>;
/** DenseOp functor - Double */
template struct DenseOp<GPUDevice, double, DenseOpType::kAdd>;
template struct DenseOp<GPUDevice, double, DenseOpType::kSub>;
template struct DenseOp<GPUDevice, double, DenseOpType::kD2DCopy>;
template struct DenseOp<GPUDevice, double, DenseOpType::kH2DCopy>;
template struct DenseOp<GPUDevice, double, DenseOpType::kD2HCopy>;

}  // namepsace Functor
}  // namespace LTS5