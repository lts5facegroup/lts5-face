/**
 *  @file   mm_renderer_utils.cpp
 *  @brief  Utility function for mm_renderer
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   5/2/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#define EIGEN_USE_GPU

#include <iomanip>

#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/cuda/tensorflow_op/mm_renderer_utils.hpp"

#include "cuda.h"
#include "cuda_gl_interop.h"

#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/math/device_math.hpp"
#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/utils/math/matrix.hpp"

#include "lts5/cuda/tensorflow_op/device_utils.hpp"
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"


using GPUDevice = Eigen::GpuDevice;
namespace tf = tensorflow;

/**
*  @namespace  LTS5
*  @brief      LTS5 laboratory dev space
*/
namespace LTS5 {

/**
 * @enum UBOType
 * @brief Possible ubo selection
 */
enum UBOType {
  /** Model Transform */
  kModel = 0,
  /** Color transform */
  kColor = 1
};

/** Vec2 integer */
using Vec2i = typename MathTypes<int>::Vec2;
/** Const Vec2 integer */
using CVec2i = typename MathTypes<int>::ConstVec2;
/** Vec3 T */
template<typename T>
using Vec3 = typename MathTypes<T>::Vec3;
/** Vec3 int */
using Vec3i = typename MathTypes<int>::Vec3;
/** Const Vec3 integer */
using CVec3i = typename MathTypes<int>::ConstVec3;
/** Vec4 T */
template<typename T>
using Vec4 = typename MathTypes<T>::Vec4;
/** Const Vec3 T */
template<typename T>
using CVec3 = typename MathTypes<T>::ConstVec3;
/** Const Vec4 T */
template<typename T>
using CVec4 = typename MathTypes<T>::ConstVec4;
/** Vec2 unsigned integer */
using Vec2ui = typename MathTypes<unsigned int>::Vec2;



#pragma mark -
#pragma mark Device code

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

/**
 * @name    UpdateModelTransform
 * @brief   Update Opengl model transformation matrix
 * @param[in] src   Rigid transformation parameters
 * @param[in] focal Focal length
 * @param[out] dst  Uniform buffer object (mapped)
 * @tparam T    Data type
 */
template<typename T>
__global__
void UpdateModelTransform(const T* src, T focal, T* dst) {
  //using Math = LTS5::CUDA::Math<double>;
  const auto i = threadIdx.x;
#ifndef USE_EULER_ANGLES
  // Access rotation element (axis)
  double vx = __ldg(&src[0]);
  double vy = __ldg(&src[1]);
  double vz = __ldg(&src[2]);
  double ang = std::sqrt((vx*vx) + (vy*vy) + (vz*vz));
  bool is_id = ang < std::numeric_limits<double>::epsilon();
  // Convert to angles
  double ct = std::cos(ang);
  double st = std::sin(ang);
  double c1 = 1.0 - ct;
  double i_ang = ang ? 1.0 / ang : 0.0;
  vx *= i_ang;
  vy *= i_ang;
  vz *= i_ang;
  // Fill matrix, column major style
  switch (i) {
    // 1st col
    case 0: dst[i] = is_id ? T(1.0) : static_cast<T>(ct + (c1 * vx * vx)); break;
    case 1: dst[i] = is_id ? T(0.0) : static_cast<T>((c1 * vy * vx) + (st * vz)); break;
    case 2: dst[i] = is_id ? T(0.0) : static_cast<T>((c1 * vz * vx) - (st * vy)); break;
    case 3: dst[i] = T(0.0); break;
      // 2nd col
    case 4: dst[i] = is_id ? T(0.0) : static_cast<T>((c1 * vx * vy) - (st * vz)); break;
    case 5: dst[i] = is_id ? T(1.0) : static_cast<T>(ct + (c1 * vy * vy)); break;
    case 6: dst[i] = is_id ? T(0.0) : static_cast<T>((c1 * vz * vy) + (st * vx)); break;
    case 7: dst[i] = T(0.0); break;
      // 3rd col
    case 8: dst[i] = is_id ? T(0.0) : static_cast<T>((c1 * vx * vz) + (st * vy)); break;
    case 9: dst[i] = is_id ? T(0.0) : static_cast<T>((c1 * vy * vz) - (st * vx)); break;
    case 10: dst[i] = is_id ? T(1.0) : static_cast<T>(ct + (c1 * vz * vz)); break;
    case 11: dst[i] = T(0.0); break;
      //   4th col
    case 12: dst[i] = src[3] * focal * T(0.01); break;
    case 13: dst[i] = src[4] * focal * T(0.01); break;
    case 14: dst[i] = src[5] * focal * T(0.1); break;
    case 15: dst[i] = T(1.0); break;
  }
#else
  // Compute component of rotation matrix
  const T cg = CUDA::Math<T>::Cos(src[0]);
  const T sg = CUDA::Math<T>::Sin(src[0]);
  const T ct = CUDA::Math<T>::Cos(src[1]);
  const T st = CUDA::Math<T>::Sin(src[1]);
  const T cp = CUDA::Math<T>::Cos(src[2]);
  const T sp = CUDA::Math<T>::Sin(src[2]);

  // Fill matrix
  switch (i) {
    case 0: dst[i] = ct * cg; break;
    case 1: dst[i] = ct * sg; break;
    case 2: dst[i] = -st; break;
    case 3: dst[i] = T(0.0); break;
    case 4: dst[i] = (sp * st * cg) - (cp * sg); break;
    case 5: dst[i] = (sp * st * sg) + (cp * cg); break;
    case 6: dst[i] = sp * ct; break;
    case 7: dst[i] = T(0.0); break;
    case 8: dst[i] = (sp * sg) + (cp * st * cg); break;
    case 9: dst[i] = (cp * st * sg) - (sp * cg); break;
    case 10: dst[i] = cp * ct; break;
    case 11: dst[i] = T(0.0); break;
    case 12: dst[i] = src[3] * focal; break;
    case 13: dst[i] = src[4] * focal; break;
    case 14: dst[i] = src[5] * focal; break;
    case 15: dst[i] = T(1.0); break;
  }
#endif

  /*// Print
  __syncthreads();
  if (i == 0) {
    printf("Pcam (R/I): %.8f, %.8f, %.8f\n", src[0], src[1], src[2]);
    printf("Rotation matrix + Translation\n");
    printf("R%d: %.7e, %.7e, %.7e, %.7e\n", 0, dst[0], dst[4], dst[8], dst[12]);
    printf("R%d: %.7e, %.7e, %.7e, %.7e\n", 1, dst[1], dst[5], dst[9], dst[13]);
    printf("R%d: %.7e, %.7e, %.7e, %.7e\n", 2, dst[2], dst[6], dst[10], dst[14]);
    printf("R%d: %.7e, %.7e, %.7e, %.7e\n", 3, dst[3], dst[7], dst[11], dst[15]);
  }*/
}

/**
 * @name  UpdateColorTransform
 * @brief Update ColorTransformation uniform for fragment shader
 * @param[in] src Color transformation parameters
 * @param[out] dst Uniform buffer object (mapped)
 * @tparam T    Data type
 */
template<typename T>
__global__
void UpdateColorTransform(const T* src, T* dst) {
  using Math = CUDA::Math<T>;
  const auto i = threadIdx.x;
  // Compute component of color transformation
  /*const T& gr = Math::Max(src[0], T(0.0));  // Gain Red
  const T& gg = Math::Max(src[1], T(0.0));  // Gain Green
  const T& gb = Math::Max(src[2], T(0.0));  // Gain Blue
  const T& c = Math::Max(Math::Min(src[6], T(1.0)), T(0.0));   // c in [0, 1]
  const T luminance_r = ((T(1.0) - c) * T(0.3));  // Luminance Red
  const T luminance_g = ((T(1.0) - c) * T(0.59)); // Luminance Green
  const T luminance_b = ((T(1.0) - c) * T(0.11)); // Luminance Blue*/


  const T gr = T(1.0);  // Gain Red
  const T gg = T(1.0);  // Gain Green
  const T gb = T(1.0);  // Gain Blue
  const T c = T(1.0);   // Constrast
  const T luminance_r = T(0.0);  // Luminance Red
  const T luminance_g = T(0.0); // Luminance Green
  const T luminance_b = T(0.0); // Luminance Blue


  // Fill matrix
  switch (i) {
    case 0: dst[i] = gr * (c + luminance_r); break;
    case 1: dst[i] = gg * luminance_r; break;
    case 2: dst[i] = gb * luminance_r; break;
    case 3: dst[i] = T(0.0); break;
    case 4: dst[i] = gr * luminance_g; break;
    case 5: dst[i] = gg * (c + luminance_g); break;
    case 6: dst[i] = gb * luminance_g; break;
    case 7: dst[i] = T(0.0); break;
    case 8: dst[i] = gr * luminance_b; break;
    case 9: dst[i] = gg * luminance_b; break;
    case 10: dst[i] = gb * (c + luminance_b); break;
    case 11: dst[i] = T(0.0); break;
    case 12: dst[i] = T(0.0); break;  //src[3]; break;
    case 13: dst[i] = T(0.0); break;  //src[4]; break;
    case 14: dst[i] = T(0.0); break;  //src[5]; break;
    case 15: dst[i] = T(1.0); break;
  }

  // printf("Ctrsm_%i: %.3f\n", i, dst[i]);
}

/**
 * @name  UpdateSHParameter
 * @brief Update Spherical Harmonic parameters uniform for fragment shader
 * @param[in] src SH parameters
 * @param[out] dst Uniform buffer object (mapped)
 * @tparam T    Data type
 * @tparam BSize    Dimension of a band
 */
template<typename T, int BSize>
__global__
void UpdateSHParameter(const T* src, T* dst) {
  const auto i = threadIdx.x;  // [0, 9[
  dst[(4 * i) + 0] = src[i];
  dst[(4 * i) + 1] = src[BSize + i];
  dst[(4 * i) + 2] = src[(2*BSize) + i];
  dst[(4 * i) + 3] = T(0.0);
}

/**
 * @name    TransferTextureKernel
 * @brief Transfer OpenGL to tensor
 * @param tex   Cuda texture object linked to opengl texture
 * @param width     Texture's with
 * @param height    Texture's height
 * @param channels  Number of channels in the destination tensor (3,4)
 * @param output    Tensor where to place the OpenGL texture
 * @tparam T    Data type
 */
template<typename T>
__global__
void TransferTextureKernel(cudaTextureObject_t tex,
                           const int width,
                           const int height,
                           const int channels,
                           T* output) {
  // Get index
  int u =  blockIdx.x * blockDim.x + threadIdx.x;
  int v =  blockIdx.y * blockDim.y + threadIdx.y;
  if (u < width && v < height) {
    // flip output index around y-axis since opengl origin is at bottom-left
    int vp = (height - v - 1);
    int index = ((vp * width) + u) * channels;
    float4 value = tex2D<float4>(tex, float(u) + 0.5f, float(v) + 0.5f);
    float* v_ptr = &value.x;
    for (int i = 0; i < channels; ++i) {
      output[index + i] = static_cast<T>(v_ptr[i]);
    }
  }
}

template<typename T>
__global__
void TransferTextureKernel(cudaTextureObject_t tex,
                           CVec3<T>* bg,
                           const int width,
                           const int height,
                           Vec4<T>* output) {
  // Get index
  int u =  blockIdx.x * blockDim.x + threadIdx.x;
  int v =  blockIdx.y * blockDim.y + threadIdx.y;
  if (u < width && v < height) {
    // flip output index around y-axis since opengl origin is at bottom-left
    int vp = (height - v - 1);
    int index_fg = ((vp * width) + u);
    float4 value = tex2D<float4>(tex, float(u) + 0.5f, float(v) + 0.5f);
    if (value.w > T(0.0)) {
      // Foreground
      output[index_fg] = Pack<T>(value.x, value.y, value.z, T(1.0));
    } else {
      // backgrond
      const auto& bg_value = bg[index_fg];
      output[index_fg] = Pack<T>(bg_value.x, bg_value.y, bg_value.z, T(0.0));
    }
  }
}

/**
 * @name    TransferTextureKernel
 * @brief Transfer OpenGL to tensor
 * @param tex   Cuda texture object linked to opengl texture
 * @param width     Texture's with
 * @param height    Texture's height
 * @param channels  Number of channels in the destination tensor (3,4)
 * @param bnd       Boundary map
 * @tparam T    Data type
 */
template<typename T>
__global__
void TransferEdgeTextureKernel(cudaTextureObject_t tex,
                               const int width,
                               const int height,
                               tf::uint8* bnd) {
  // Get index
  int u =  blockIdx.x * blockDim.x + threadIdx.x;
  int v =  blockIdx.y * blockDim.y + threadIdx.y;
  if (u < width && v < height) {
    // flip output index around y-axis since opengl origin is at bottom-left
    int vp = (height - v - 1);
    int index = ((vp * width) + u);
    float4 tex_v = tex2D<float4>(tex, float(u) + 0.5f, float(v) + 0.5f);
//    float4 tex_v = tex2D<float4>(tex, float(u), float(v));
    tf::uint8 p_type;
    if (tex_v.w > 0.75f) {
      p_type = PixelType::kInterior;
    } else if (tex_v.w > 0.25f) {
      p_type = PixelType::kBoundary;
    } else {
      p_type = PixelType::kExterior;
    }
    bnd[index] = p_type;
  }
}

/**
 * @name    UpdateOglBufferKernel
 * @brief Copy surface's properties to opengl buffer
 * @param[in] v
 * @param[in] n
 * @param[in] vc
 * @param[in] n_vertex
 * @param[out] v_ogl
 * @param[out] n_ogl
 * @param[out] vc_ogl
 * @tparam T    Data type
 */
template<typename T>
__global__
void UpdateOglBufferKernel(CVec3<T>* v, CVec3<T>* n, CVec3<T>* vc,
                           const int n_vertex,
                           Vec3<T>* v_ogl, Vec3<T>* n_ogl, Vec3<T>* vc_ogl) {
  const int ix = blockIdx.x * blockDim.x + threadIdx.x;

  if (ix >= n_vertex) return;

  // Copy properties
  v_ogl[ix] = v[ix];
  n_ogl[ix] = n[ix];
  vc_ogl[ix] = vc[ix];
}

/**
 * @name    UpdateEdgeKernel
 * @brief   Update OpenGL element buffer
 * @param[in] edges         Edge's index to be pushed
 * @param[in] n_edge        Number of edges to update
 * @param[out] ogl_buffer   OGL Buffer where to place the edge's index
 */
__global__
void UpdateEdgeKernel(const unsigned int* edges,
                      const int n_edge,
                      const int offset,
                      unsigned int* ogl_buffer) {
  const int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (ix >= n_edge) return;
  // Update edge list
  ogl_buffer[offset + 2*ix] = edges[2*ix];
  ogl_buffer[offset + 2*ix + 1] = edges[2*ix + 1];
}

/**
 * @name
 * @brief Update OpenGL triangles
 * @param[in] tri       Triangles to copy
 * @param[in] n_tri     Number of triangles to copy
 * @param[out] ogl_buffer   OpenGL buffer where to dump triangles
 */
__global__
void UpdateTriangleKernel(CVec3i* tri, const int n_tri, Vec3i* ogl_buffer) {
  const int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (ix >= n_tri) return;
  // Update tri
  ogl_buffer[ix] = tri[ix];
}

/**
 * @name    UpdateAttributeKernel
 * @brief Update OpenGL attributes
 * @param[in] attribute  Attributes to copy
 * @param[in] length  Number of element to copy
 * @param[in] size  Size of a single attributes to be copied
 * @param[out] ogl_buffer   OpenGL buffer where to dump triangles
 */
template<typename T>
__global__
void UpdateAttributeKernel(const T* attribute,
                           const int length,
                           const int size,
                           T* ogl_buffer) {
  const int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  if (ix >= length) return;
  // Update attributes
  for (int k = 0; k < size; ++k) {
    int idx = (ix * size) + k;
    ogl_buffer[idx] = attribute[idx];
  }
}

}  // namespace device

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @struct  FSRenderSelector
 * @brief   Shader selector for a given device known at compilation.
 *          GPU Specialization
 * @author  Christophe Ecabert
 * @date    8/9/18
 * @tparam D    Device on which the computation is run
 */
template<>
struct FSRenderSelector<GPUDevice> {
  static constexpr const char* str =
          "#version 330\n"
          "in vec3 normal0;\n"
          "in vec3 color0;\n"
          "layout (std140) uniform ColorTransform {\n"
          "  mat4 M;\n"
          "  vec4 sh_coef[9];\n"
          "};\n"
          "out vec4 fragColor;\n"
          "vec3 ComputeShCoef(in vec3 n, in vec4 sh_coef[9]) {\n"
          "  vec3 sh = vec3(0.f, 0.f, 0.f);\n"
          "  float i_coef[9];\n"
          "  i_coef[0] = 1.f;\n"
          "  i_coef[1] = n.x;\n"
          "  i_coef[2] = n.y;\n"
          "  i_coef[3] = n.z;\n"
          "  i_coef[4] = n.x * n.y;\n"
          "  i_coef[5] = n.x * n.z;\n"
          "  i_coef[6] = n.y * n.z;\n"
          "  i_coef[7] = (n.x * n.x) - (n.y * n.y);\n"
          "  i_coef[8] = (3.f * n.z * n.z) - 1.f;\n"
          "  for (int k = 0; k < 9; ++k) {\n"
          "    sh += i_coef[k] * sh_coef[k].xyz;\n"
          "  }\n"
          "  return clamp(sh, 0.f, 100.f);\n"
          "}\n"
          "void main() {\n"
          "  vec3 sh = ComputeShCoef(normal0, sh_coef);\n"
          "  vec4 c0 = vec4(sh * color0, 1.f);\n"
          "  vec4 cf = M * c0;\n"
          "  fragColor = clamp(cf, 0.f, 1.f);\n"
          "}";
};

/**
 * @class   MapOGLResource
 * @brief   Map a specific OpenGL Ressource to a pointer accessible for a given
 *          device
 *          GPU - Specialization
 * @author  Christophe Ecabert
 * @date    15/01/2018
 * @tparam T        Data type
 */
template<typename RType, typename MType>
class MapOGLResource<GPUDevice, RType, MType> {
 public:

  /** Renderer type */
  using Renderer = LTS5::OGLOffscreenRenderer<RType, LTS5::Vector3<RType>>;
  /** Buffer Type */
  using BufferIdx = typename Renderer::BufferIdx;

  /**
   * @name  MapOGLResource
   * @fn    MapOGLResource(const BufferIdx& resource,
   *                       const Renderer* renderer)
   * @brief Constructor
   * @param[in] resource    Resource index to map
   * @param[in] renderer    Renderer
   */
  MapOGLResource() : gl_resource_(nullptr) {}

  /**
   * @name  Init
   * @fn void Init(const BufferIdx& resource, const Renderer* renderer)
   * @brief Initialize mapper
   * @param[in] resource    Resource index to map
   * @param[in] renderer    Renderer
   */
  void Init(const BufferIdx& resource, const Renderer* renderer) {
    // Already have a registered resource ?
    if (gl_resource_) {
      cudaSafeCall(cudaGraphicsUnregisterResource(gl_resource_));
      gl_resource_ = nullptr;
    }
    // Register
    GLuint buffID = renderer->get_ogl_buffer(resource);
    cudaSafeCall(cudaGraphicsGLRegisterBuffer(&gl_resource_,
                                              buffID,
                                              cudaGraphicsRegisterFlagsNone));
  }

  /**
   * @name  ~MapOGLResource
   * @fn    ~MapOGLResource()
   * @brief Destructor
   */
  ~MapOGLResource() {
    // Unregister
    if (gl_resource_) {
      cudaSafeCall(cudaGraphicsUnregisterResource(gl_resource_));
    }
  }

  /**
   * @name  Map
   * @fn    MType* Map(tf::OpKernelContext* ctx)
   * @brief Map opengl resource to a pointer usable for this device
   * @param[in] ctx Op context
   * @return    Pointer to the mapped resource if success, nullptr otherwise
   */
  MType* Map(tf::OpKernelContext* ctx) {
    // Map OpenGL Buffer for writing
    if (gl_resource_) {
      void* ptr;
      size_t buff_sz = 0;
      auto &stream = ctx->eigen_gpu_device().stream();
      cudaSafeCall(cudaGraphicsMapResources(1, &gl_resource_, stream));
      cudaSafeCall(cudaGraphicsResourceGetMappedPointer(&ptr,
                                                        &buff_sz,
                                                        gl_resource_));
      return reinterpret_cast<MType *>(ptr);
    } else {
      return nullptr;
    }
  }

  /**
   * @name  Unmap
   * @fn    void Unmap(tf::OpKernelContext* ctx)
   * @brief Unmap the underlying resources
   * @param[in] ctx Op context
   */
  void Unmap(tf::OpKernelContext* ctx) {
    // Unmap resource
    if (gl_resource_) {
      auto &stream = ctx->eigen_gpu_device().stream();
      cudaSafeCall(cudaGraphicsUnmapResources(1, &gl_resource_, stream));
    }
  }

 private:
  /** Graphical Resource */
  cudaGraphicsResource* gl_resource_;
};


/*
 * @class   DownloadImage
 * @brief   Retrieve image from an OpenGL renderer a place it into a tensor
 * @author  Christophe Ecabert
 * @date    25/01/18
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class DownloadImage<GPUDevice, T> {
 public:
  /** Renderer type */
  using Renderer = LTS5::OGLOffscreenRenderer<T, LTS5::Vector3<T>>;

  /**
   * @name  DownloadImage
   * @fn    explicit DownloadImage(const Renderer* renderer)
   * @brief Constructor
   * @param[in] renderer Render object to download from
   */
  DownloadImage() {}

  /**
   * @name  Init
   * @fn    void Init(const Renderer* renderer)
   * @brief Initialize downloader object
   * @param[in] renderer Render object to download from
   */
  void Init(const Renderer* renderer) {
    // Get offscreen buffer
    using BufferType = OGLIOBuffer::BufferType;
    auto* buffer = renderer->get_buffer();
    auto res = buffer->GetOglBuffer(BufferType::kRGBAf);
    // Register
    cudaSafeCall(cudaGraphicsGLRegisterImage(&resource_,
                                             (GLuint)res,
                                             GL_TEXTURE_2D,
                                             cudaGraphicsMapFlagsReadOnly));

  }

  /**
   * @name  ~DownloadImage
   * @fn    ~DownloadImage()
   * @brief Destrutcor
   */
  ~DownloadImage() {
    // Unregister
    cudaSafeCall(cudaGraphicsUnregisterResource(resource_));
  }

  /*
   * @name  operator()
   * @fn    int operator()(tensorflow::OpKernelContext* ctx, tensorflow::Tensor* image)
   * @brief Copy image to the tensor
   * @param[in] ctx Op context
   * @param[out] image  Image where to copy the data
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx, tensorflow::Tensor* image) {
    int err = -1;
    DCHECK_EQ(image->dims(), 4);
    DCHECK_EQ(image->dim_size(0), 1);
    // Get device pointer to the ogl resource -> map
    cudaArray* ogl_array = nullptr;
    auto& stream = ctx->eigen_gpu_device().stream();
    cudaSafeCall(cudaGraphicsMapResources(1, &resource_, stream));
    cudaSafeCall(cudaGraphicsSubResourceGetMappedArray(&ogl_array,
                                                       resource_,
                                                       0,
                                                       0));
    // Create resource descriptor for cudaArray
    cudaResourceDesc resDesc;
    memset(&resDesc, 0, sizeof(resDesc));
    resDesc.resType = cudaResourceTypeArray;  // cudaArray
    resDesc.res.array.array = ogl_array;
    // Create texture descriptor
    cudaTextureDesc texDesc;
    memset(&texDesc, 0, sizeof(texDesc));
    texDesc.readMode = cudaReadModeElementType;
    // create texture object
    cudaTextureObject_t tex;
    cudaSafeCall(cudaCreateTextureObject(&tex, &resDesc, &texDesc, nullptr));
    // Copy back to tensor
    auto* t_ptr = image->unaligned_flat<T>().data();
    int h = (int) image->dim_size(1);
    int w = (int) image->dim_size(2);
    int c = (int) image->dim_size(3);
    dim3 thd(32, 32, 1);
    dim3 bck((w + 31) / 32, (h + 31) / 32, 1);
    device::TransferTextureKernel<T> <<<bck, thd, 0, stream>>>(tex,
                                                               w,
                                                               h,
                                                               c,
                                                               t_ptr);
    // Delete texture object + unmap
    cudaSafeCall(cudaDestroyTextureObject(tex));
    cudaSafeCall(cudaGraphicsUnmapResources(1, &resource_, stream));
    // Sanity check
    err = cudaGetLastError() == cudaSuccess ? 0 : -1;
    return err;
  }

  /*
   * @name  operator()
   * @fn    int operator()(tf::OpKernelContext* ctx,
                           const tf::Tensor& background,
                           tf::Tensor* image)
   * @brief Transfer image from OpenGL buffer to cuda array and sitch background
   *        at the same time.
   * @param[in] ctx
   * @param[in] background
   * @param[out] image
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& background,
                 tf::Tensor* image) {
    int err = -1;
    DCHECK_EQ(image->dims(), 4);
    DCHECK_EQ(image->dim_size(0), 1);
    // Get device pointer to the ogl resource -> map
    cudaArray* ogl_array = nullptr;
    auto& stream = ctx->eigen_gpu_device().stream();
    cudaSafeCall(cudaGraphicsMapResources(1, &resource_, stream));
    cudaSafeCall(cudaGraphicsSubResourceGetMappedArray(&ogl_array,
                                                       resource_,
                                                       0,
                                                       0));
    // Create resource descriptor for cudaArray
    cudaResourceDesc resDesc;
    memset(&resDesc, 0, sizeof(resDesc));
    resDesc.resType = cudaResourceTypeArray;  // cudaArray
    resDesc.res.array.array = ogl_array;
    // Create texture descriptor
    cudaTextureDesc texDesc;
    memset(&texDesc, 0, sizeof(texDesc));
    texDesc.readMode = cudaReadModeElementType;
    // create texture object
    cudaTextureObject_t tex;
    cudaSafeCall(cudaCreateTextureObject(&tex, &resDesc, &texDesc, nullptr));
    // Copy back to tensor
    auto* bg_ptr = reinterpret_cast<CVec3<T>*>(background.unaligned_flat<T>().data());
    auto* t_ptr = reinterpret_cast<Vec4<T>*>(image->unaligned_flat<T>().data());
    int h = (int) image->dim_size(1);
    int w = (int) image->dim_size(2);
    dim3 thd(32, 32);
    dim3 bck((w + 31) / 32, (h + 31) / 32);
    device::TransferTextureKernel<T> <<<bck, thd, 0, stream>>>(tex,
                                                               bg_ptr,
                                                               w,
                                                               h,
                                                               t_ptr);
    // Delete texture object + unmap
    cudaSafeCall(cudaDestroyTextureObject(tex));
    cudaSafeCall(cudaGraphicsUnmapResources(1, &resource_, stream));
    // Sanity check
    err = cudaGetLastError() == cudaSuccess ? 0 : -1;
    return err;
  }

 private:
  /** Cuda resource */
  cudaGraphicsResource* resource_;
};

/**
 * @class   DownloadBoundaryImage
 * @brief   Retrieve edge image from an OpenGL renderer a place it into a tensor
 * @author  Christophe Ecabert
 * @date    15/04/19
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class DownloadBoundaryImage<GPUDevice, T>{
 public:
  /** Renderer type */
  using Renderer = LTS5::OGLOffscreenRenderer<T, LTS5::Vector3<T>>;

  /**
   * @name  DownloadBoundaryImage
   * @fn    explicit DownloadImage()
   * @brief Constructor
   */
  DownloadBoundaryImage() {}

  /**
   * @name  ~DownloadBoundaryImage
   * @fn    ~DownloadBoundaryImage()
   * @brief Destrutcor
   */
  ~DownloadBoundaryImage() {
    // Unregister
    cudaSafeCall(cudaGraphicsUnregisterResource(resource_));
  }

  /**
   * @name  Init
   * @fn    void Init(const Renderer* renderer)
   * @brief Initialize downloader object
   * @param[in] renderer Render object to download from
   */
  void Init(const Renderer* renderer) {
    // Get offscreen buffer
    using BufferType = OGLIOBuffer::BufferType;
    auto* buffer = renderer->get_buffer();
    auto res_color = buffer->GetOglBuffer(BufferType::kRGBAf);
    // Register color
    cudaSafeCall(cudaGraphicsGLRegisterImage(&resource_,
                                             (GLuint)res_color,
                                             GL_TEXTURE_2D,
                                             cudaGraphicsMapFlagsReadOnly));
  }

  /**
   * @name  operator()
   * @fn    int operator()(tensorflow::OpKernelContext* ctx,
                           tensorflow::Tensor* boundaries)
   * @brief Copy image to the tensor
   * @param[in] ctx Op context
   * @param[out] boundaries  Image where to copy the data
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 tensorflow::Tensor* boundaries) {
    int err = -1;
    DCHECK_EQ(boundaries->dims(), 3);
    DCHECK_EQ(boundaries->dim_size(0), 1);


    // Get device pointer to the ogl resource -> map
    auto& stream = ctx->eigen_gpu_device().stream();
    cudaArray* ogl_color_array = nullptr;
    cudaSafeCall(cudaGraphicsMapResources(1, &resource_, stream));
    cudaSafeCall(cudaGraphicsSubResourceGetMappedArray(&ogl_color_array,
                                                       resource_,
                                                       0,
                                                       0));
    // Create resource/texture descriptor for cudaArray
    // Color
    cudaResourceDesc resColorDesc;
    memset(&resColorDesc, 0, sizeof(resColorDesc));
    resColorDesc.resType = cudaResourceTypeArray;
    resColorDesc.res.array.array = ogl_color_array;
    // Create texture descriptor
    cudaTextureDesc texColorDesc;
    memset(&texColorDesc, 0, sizeof(texColorDesc));
    texColorDesc.readMode = cudaReadModeElementType;
    cudaTextureObject_t texColor;
    cudaSafeCall(cudaCreateTextureObject(&texColor,
                                         &resColorDesc,
                                         &texColorDesc,
                                         nullptr));
    // Copy back to tensor
    auto* t_ptr = boundaries->unaligned_flat<tf::uint8>().data();
    int h = (int) boundaries->dim_size(1);
    int w = (int) boundaries->dim_size(2);
    int c = (int) boundaries->dim_size(3);
    dim3 thd(32, 32, 1);
    dim3 bck((w + 31) / 32, (h + 31) / 32, 1);
    device::TransferEdgeTextureKernel<T><<<bck, thd, 0, stream>>>(texColor,
            w,
            h,
            t_ptr);
    // Delete texture object + unmap
    cudaSafeCall(cudaDestroyTextureObject(texColor));
    cudaSafeCall(cudaGraphicsUnmapResources(1, &resource_, stream));
    // Sanity check
    err = cudaGetLastError() == cudaSuccess ? 0 : -1;
    return err;
  }

 private:
  /** Cuda resource */
  cudaGraphicsResource* resource_;
};


/**
 * @class   UpdateOglBuffer
 * @brief   Update VAO buffer (vertex, normal, color)
 * @author  Christophe Ecabert
 * @date    15/04/18
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class UpdateOglBuffer<GPUDevice, T> {
 public:
  /** Ogl Mapper */
  using Mapper = MapOGLResource<GPUDevice, T, T>;

  /*
   * @name  operator()
   * @brief Update opengl buffers (VAOs)
   * @param[in] ctx Op's context
   * @param[in] vertex  Vertex array (slice)
   * @param[in] normal  Normal array (slice)
   * @param[in] color   Color array (slice)
   * @param[in] vertex_map  Vertex mapper to OpenGL VAO's buffer
   * @param[in] normal_map  Normal mapper to OpenGL VAO's buffer
   * @param[in] vertex_color_map    Vertex Color mapper to OpenGL VAO's buffer
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& normal,
                 const tf::Tensor& color,
                 Mapper* vertex_map,
                 Mapper* normal_map,
                 Mapper* vertex_color_map) {
    int err = -1;
    // map pointer
    T* v_map_ptr = vertex_map->Map(ctx);
    T* n_map_ptr = normal_map->Map(ctx);
    T* vc_map_ptr = vertex_color_map->Map(ctx);
    if (v_map_ptr != nullptr && n_map_ptr != nullptr && vc_map_ptr != nullptr) {
      // Define raw ptr
      CVec3<T>* v_ptr = reinterpret_cast<CVec3<T>*>(vertex.template unaligned_flat<T>().data());
      CVec3<T>* n_ptr = reinterpret_cast<CVec3<T>*>(normal.template unaligned_flat<T>().data());
      CVec3<T>* vc_ptr = reinterpret_cast<CVec3<T>*>(color.template unaligned_flat<T>().data());
      Vec3<T>* v_ogl_ptr = reinterpret_cast<Vec3<T>*>(v_map_ptr);
      Vec3<T>* n_ogl_ptr = reinterpret_cast<Vec3<T>*>(n_map_ptr);
      Vec3<T>* vc_ogl_ptr = reinterpret_cast<Vec3<T>*>(vc_map_ptr);
      // Define dimension
      auto& stream = ctx->eigen_gpu_device().stream();
      const int n_vertex = vertex.dim_size(1) / 3;
      int thd = 1024;
      int bck = (n_vertex + 1023) / 1024;
      device::UpdateOglBufferKernel<T><<<bck, thd, 0, stream>>>(v_ptr,
                                                                n_ptr,
                                                                vc_ptr,
                                                                n_vertex,
                                                                v_ogl_ptr,
                                                                n_ogl_ptr,
                                                                vc_ogl_ptr);
      err = cudaGetLastError() == cudaSuccess ? 0 : -1;
    }
    // Unmap + sync
    vertex_map->Unmap(ctx);
    normal_map->Unmap(ctx);
    vertex_color_map->Unmap(ctx);
    return err;
  }
};


/**
 * @class   UpdateOglEdgeIndex
 * @brief   Update VBO's element index from a given array
 * @author  Christophe Ecabert
 * @date    15/04/18
 * @tparam T        Data type
 */
template<typename T>
class UpdateOglEdgeIndex<GPUDevice, T> {
 public:
  /** Ogl Mapper */
  using Mapper = MapOGLResource<GPUDevice, T, unsigned int>;
  /**
   * @name  operator()
   * @param[in] ctx         Op's context
   * @param[in] edge_list   List of edges to push to OpenGL
   * @param[in] offset      Offset from where to start in the index array
   * @param[in] mapper      Mapper to OpenGL's VBO
   * @return    Number of element pushed to the index array
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const tensorflow::Tensor& edge_list,
                 const size_t& offset,
                 Mapper& mapper) {
    using CTMap32U = typename tensorflow::TTypes<tf::uint32>::ConstTensor;
    using TMap32U = typename tensorflow::TTypes<tf::uint32>::Tensor;
    using CopyObj = Functor::DenseOp<GPUDevice, tf::uint32, Functor::DenseOpType::kD2HCopy>;
    // Get number of silhouette edges
    unsigned int n_edge = 0;
    auto* elist_ptr = edge_list.unaligned_flat<tf::uint32>().data();
    auto in = CTMap32U(elist_ptr, 1);
    auto out = TMap32U(&n_edge, 1);
    CopyObj copy;
    auto& device = ctx->eigen_device<GPUDevice>();
    copy(device, in, out);
    Functor::DeviceHelper<GPUDevice>::Synchronize(device);
    // Push edges
    if (n_edge > 0) {
      // Get pointers
      elist_ptr = &elist_ptr[1];  // Skip first element
      auto* vbo_ptr = mapper.Map(ctx);
      // Copy
      int thd = 256;
      int bck = (n_edge + 255) / 256;
      auto stream = ctx->eigen_gpu_device().stream();
      device::UpdateEdgeKernel<<<bck, thd, 0, stream>>>(elist_ptr,
              n_edge,
              offset,
              vbo_ptr);
      // Done, unmap
      mapper.Unmap(ctx);
    }
    return n_edge;
  }
};

/**
 * @class   UploadTriangle
 * @brief   Update VAO with triangles
 * @author  Christophe Ecabert
 * @date    15/07/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class UploadTriangle<GPUDevice, T> {
 public:
  /** Ogl Face Mapper */
  using Mapper = MapOGLResource<GPUDevice, T, unsigned int>;

  /**
   * @name  operator()
   * @param[in] ctx         Op's context
   * @param[in] edge_list   Triangles to upload
   * @param[in] mapper      Mapper to OpenGL's VBO
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& triangle,
                 Mapper& mapper) {
    int err = -1;
    // map pointer
    auto* tri_map_ptr = mapper.Map(ctx);
    if (tri_map_ptr != nullptr) {
      // Define raw ptr
      CVec3i* tri_ptr = reinterpret_cast<CVec3i*>(triangle.flat<tf::int32>().data());
      Vec3i* tri_ogl_ptr = reinterpret_cast<Vec3i*>(tri_map_ptr);
      // Define dimension
      auto& stream = ctx->eigen_gpu_device().stream();
      const int n_tri = triangle.dim_size(0);
      int thd = 1024;
      int bck = (n_tri + 1023) / 1024;
      device::UpdateTriangleKernel<<<bck, thd, 0, stream>>>(tri_ptr,
                                                            n_tri,
                                                            tri_ogl_ptr);
      // Sanity check
      err = cudaGetLastError() == cudaSuccess ? 0 : -1;
    }
    mapper.Unmap(ctx);
    return err;
  }
};

/**
 * @class   UploadAttribute
 * @brief   Update VAO with a given attributes (vertex, normal, color, ...)
 * @author  Christophe Ecabert
 * @date    15/07/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class UploadAttribute<GPUDevice, T> {
 public:
  /** Ogl Face Mapper */
  using Mapper = MapOGLResource<GPUDevice, T, T>;

  /**
   * @name  operator()
   * @param[in] ctx         Op's context
   * @param[in] attribute   Attributes to upload [1, N, 3/4]
   * @param[in] mapper      Mapper to OpenGL's VBO
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& attributes,
                 Mapper& mapper) {
    int err = -1;
    // map pointer
    T* attr_map_ptr = mapper.Map(ctx);
    if (attr_map_ptr != nullptr) {
      // Define raw ptr
      const T* attr_ptr = attributes.unaligned_flat<T>().data();
      T* attr_ogl_ptr = attr_map_ptr;
      // Define dimension
      auto& stream = ctx->eigen_gpu_device().stream();
      const int length = attributes.dim_size(1);
      const int attr_sz = attributes.dim_size(2);
      int thd = 1024;
      int bck = (length + 1023) / 1024;
      device::UpdateAttributeKernel<T><<<bck, thd, 0, stream>>>(attr_ptr,
                                                                length,
                                                                attr_sz,
                                                                attr_ogl_ptr);
      // Sanity check
      err = cudaGetLastError() == cudaSuccess ? 0 : -1;
    }
    mapper.Unmap(ctx);
    return err;
  }
};


}  // namepsace Functor

#pragma mark -
#pragma mark Forward Rendering Shader



/**
 * @name  MMRenderingShader
 * @fn    MMRenderingShader(const MMRendererParameters<T>& p)
 * @brief Constructor
 * @param[in] p   Rendering parameters
 */
template<typename T>
MMRenderingShader<GPUDevice, T>::
MMRenderingShader(const MMRendererParameters<T>& p) : ubo_(nullptr),
                                                      resource_{nullptr},
                                                      shader_(nullptr),
                                                      focal_(p.focal) {
  using Mat4 = Matrix4x4<T>;
  try {
    std::vector<OGLShader> shaders;
    shaders.emplace_back(OGLShader(vs_str, OGLShader::ShaderType::kVertex));
    shaders.emplace_back(OGLShader(Functor::FSRenderSelector<GPUDevice>::str,
                                   OGLShader::ShaderType::kFragment));
    // Define attributes
    using Attributes = OGLProgram::Attributes;
    using Types = OGLProgram::AttributeType;
    std::vector<Attributes> attributes;
    attributes.emplace_back(Types::kPoint, 0, "vertex");
    attributes.emplace_back(Types::kNormal, 1, "normal");
    attributes.emplace_back(Types::kColor, 2, "color");
    this->shader_ = new OGLProgram(shaders, attributes);
  } catch (const LTS5::ProcessError &e) {
    std::string msg = "OpenGL failed to initialize shader: ";
    msg += std::string(e.what());
    throw LTS5::ProcessError(ProcessError::ProcessErrorEnum::kErr,
                             msg,
                             FUNC_NAME);
  }
  // Define binding point
  int err = this->shader_->SetUniformBlockBinding("Transform", 0);
  err |= this->shader_->SetUniformBlockBinding("ColorTransform", 1);
  if (err) {
    LTS5_LOG_ERROR("Can not set UBO binding point");
  }
  // Create Uniform buffer
  ubo_ = new UniformBufferObject();
  // Bind0: 2x[4x4] matrix of T
  // Bind1: 1x[4x4] matrix of T + 9 * vec4 of T -> (ColorTransform + Sh coef)
  err |= ubo_->Create({{0, 2 * 16 * sizeof(T)},
                       {1, (16 + (9 * 4)) * sizeof(T)}});
  if (!err) {
    // Add projection matrix since it has to be initialized once.
    // http://kgeorge.github.io/2014/03/08/calculating-opengl-perspective-matrix-from-opencv-intrinsic-matrix
    Mat4 P;  // Projection matrix
    T cx = static_cast<T>(p.width) / T(2.0);
    T cy = static_cast<T>(p.height) / T(2.0);
    P(0, 0) = p.focal / cx;
    P(1, 1) = p.focal / cy;
    P(2, 2) = -(p.far + p.near) / (p.far - p.near);
    P(2, 3) = -(T(2.0) * p.far * p.near) / (p.far - p.near);
    P(3, 2) = T(-1.0);
    auto Pt = P.Transpose();
    err = ubo_->AddData(0, 16 * sizeof(T), 16 * sizeof(T), Pt.Data());
    if (err) {
      LTS5_LOG_ERROR("Can not initialize projection matrix");
    }

    // Register resource to CUDA
    cudaSafeCall(cudaGraphicsGLRegisterBuffer(&resource_[UBOType::kModel],
                                              ubo_->get_object(0),
                                              cudaGraphicsRegisterFlagsNone));
    cudaSafeCall(cudaGraphicsGLRegisterBuffer(&resource_[UBOType::kColor],
                                              ubo_->get_object(1),
                                              cudaGraphicsRegisterFlagsNone));
  } else {
    LTS5_LOG_ERROR("Can not initialize UBO");
  }
}

/*
 * @name  ~MMRenderingShader
 * @fn    ~MMRenderingShader() override
 * @brief Destructor
 */
template<typename T>
MMRenderingShader<GPUDevice, T>::~MMRenderingShader() {
  if (resource_) {
    // Unregister
    cudaSafeCall(cudaGraphicsUnregisterResource(resource_[UBOType::kModel]));
    cudaSafeCall(cudaGraphicsUnregisterResource(resource_[UBOType::kColor]));
  }
  delete ubo_;
  delete shader_;
}

/*
 * @name  UpdateModelTransform
 * @fn    int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                                   const tensorflow::Tensor& parameter,
                                   const tensorflow::int64& ct_idx,
                                   const tensorflow::int64& cam_idx)
 * @brief Update model transformation matrix from camera parameters
 * @param[in] ctx Op context
 * @param[in] parameter Morphable model parameters
 * @param[in] i_idx   Index where illumination parameters start
 * @param[in] ct_idx  Index where color transformation parameters start
 * @param[in] cam_idx Index where camera parameters start
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MMRenderingShader<GPUDevice, T>::
UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                     const tensorflow::Tensor& parameter,
                     const tensorflow::int64& i_idx,
                     const tensorflow::int64& ct_idx,
                     const tensorflow::int64& cam_idx) {
  // Get underlying stream
  auto& stream = ctx->eigen_gpu_device().stream();
  // Map OpenGL Buffer for writing
  cudaSafeCall(cudaGraphicsMapResources(2, &resource_[0], stream));
  // Get pointers
  T* ubo_cam_ptr;
  T* ubo_ct_ptr;
  size_t cam_buff_sz = 0;
  size_t ct_buff_sz = 0;
  cudaSafeCall(cudaGraphicsResourceGetMappedPointer((void**)&ubo_cam_ptr,
                                                    &cam_buff_sz,
                                                    resource_[UBOType::kModel]));
  cudaSafeCall(cudaGraphicsResourceGetMappedPointer((void**)&ubo_ct_ptr,
                                                    &ct_buff_sz,
                                                    resource_[UBOType::kColor]));
  // Update data from parameters
  const T* p_ptr = parameter.unaligned_flat<T>().data();
  const T* i_ptr = &p_ptr[i_idx];
  const T* ct_ptr = &p_ptr[ct_idx];
  const T* cam_ptr = &p_ptr[cam_idx];
  device::UpdateModelTransform<T><<<1, 16, 0, stream>>>(cam_ptr,
                                                        focal_,
                                                        ubo_cam_ptr);
  // Update color transform: Linear + offset
  device::UpdateColorTransform<T><<<1, 16, 0, stream>>>(ct_ptr, ubo_ct_ptr);
  // Update sh parameters, move after color transformation (16), matrix4
  ubo_ct_ptr = &ubo_ct_ptr[16];
  device::UpdateSHParameter<T, 9><<<1, 9, 0, stream>>>(i_ptr, ubo_ct_ptr);
  // Unmap resource, ensure sync
  cudaSafeCall(cudaGraphicsUnmapResources(2, &resource_[0], stream));
  // Done
  return 0;
}


#pragma mark -
#pragma mark Explicit Instantiation

/** Float - device - ModelTransform */
template __global__
void device::UpdateModelTransform<float>(const float*, float, float*);
/** Float - device - ModelTransform */
template __global__
void device::UpdateModelTransform<double>(const double*, double, double*);

/** Float renderer, Float mapping - Mapper */
template class Functor::MapOGLResource<GPUDevice, float, float>;
/** Double renderer, Double mapping - Mapper */
template class Functor::MapOGLResource<GPUDevice, double, double>;
/** Float renderer, int mapping - Mapper */
template class Functor::MapOGLResource<GPUDevice, float, unsigned int>;
/** Double renderer, int mapping - Mapper */
template class Functor::MapOGLResource<GPUDevice, double, unsigned int>;

/** Float - Download */
template class Functor::DownloadImage<GPUDevice, float>;
/** Double - Download */
template class Functor::DownloadImage<GPUDevice, double>;

/** Float - Download */
template class Functor::DownloadBoundaryImage<GPUDevice, float>;
/** Double - Download */
template class Functor::DownloadBoundaryImage<GPUDevice, double>;

/** Float - UpdateOglBuffer */
template class Functor::UpdateOglBuffer<GPUDevice, float>;
/** Float - UpdateOglBuffer */
template class Functor::UpdateOglBuffer<GPUDevice, double>;

/** FLoat - UpdateOglEdgeIndex */
template class Functor::UpdateOglEdgeIndex<GPUDevice, float>;
/** FLoat - UpdateOglEdgeIndex */
template class Functor::UpdateOglEdgeIndex<GPUDevice, double>;

/** FLoat - UploadTriangle */
template class Functor::UploadTriangle<GPUDevice, float>;
/** FLoat - UpdateOglEdgeIndex */
template class Functor::UploadTriangle<GPUDevice, double>;

/** FLoat - UploadAttribute */
template class Functor::UploadAttribute<GPUDevice, float>;
/** FLoat - UpdateOglEdgeIndex */
template class Functor::UploadAttribute<GPUDevice, double>;

/** Float - Forward Rendering Shader */
template class MMRenderingShader<GPUDevice, float>;
/** Double - Forward Rendering Shader */
template class MMRenderingShader<GPUDevice, double>;

}  // namespace LTS5