/**
 *  @file   mm_image_processor.cpp
 *  @brief  Utility class to compute various data in the image space
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   4/9/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <cmath>

#include "cuda.h"
#include "cuda_runtime.h"
#include "device_atomic_functions.h"


#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor.h"

#include "lts5/tensorflow_op/mm_image_processor.hpp"
#include "lts5/tensorflow_op/mm_renderer_utils.hpp"
#include "lts5/utils/logger.hpp"

#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/cuda/utils/math/device_math.hpp"
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"

using GPUDevice = Eigen::GpuDevice;
namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Type Definition

/** Const Vec3 */
template<typename T> using CVec3 = typename MathTypes<T>::ConstVec3;
/** Const Vec4 */
template<typename T> using CVec4 = typename MathTypes<T>::ConstVec4;
/** Vec3 */
template<typename T> using Vec3 = typename MathTypes<T>::Vec3;
/** Vec4 */
template<typename T> using Vec4 = typename MathTypes<T>::Vec4;
/** Constant Vector3 integer type */
using CVec3i = typename MathTypes<int>::ConstVec3;
/** Constant Vector2 integer type */
using CVec2i = typename MathTypes<int>::ConstVec2;

#pragma mark -
#pragma mark Device Kernels

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

/**
 * @name    FillTransformationMatrixByRow
 * @brief   Compute Rotation matrix + Translation from camera parameters and
 *          store it by row.
 * @param[in] cam   Camera parameters
 * @param[in] focal Focal length
 * @param[out] trsfrm   Rigid tranformation [R|T] (Rotation stored by row)
 * @tparam T    Data type
 */
template<typename T>
__device__ __forceinline__
void FillTransformationMatrixByRow(const T* cam,
                                   const T& focal,
                                   Vec3<T>* trsfrm) {
#ifndef USE_EULER_ANGLES
  // Access rotation element (axis)
  double vx = __ldg(&cam[0]);
  double vy = __ldg(&cam[1]);
  double vz = __ldg(&cam[2]);
  double ang = std::sqrt((vx*vx) + (vy*vy) + (vz*vz));
  if (ang < std::numeric_limits<double>::epsilon()) {
    // Identity
    trsfrm[0] = Pack<T>(1.0, 0.0, 0.0);
    trsfrm[1] = Pack<T>(0.0, 1.0, 0.0);
    trsfrm[2] = Pack<T>(0.0, 0.0, 1.0);
  } else {
    // Convert to angles
    double ct = std::cos(ang);
    double st = std::sin(ang);
    double c1 = 1.0 - ct;
    double i_ang = ang ? 1.0 / ang : 0.0;
    vx *= i_ang;
    vy *= i_ang;
    vz *= i_ang;
    // First row
    trsfrm[0].x = static_cast<T>(ct + (c1 * vx * vx));
    trsfrm[0].y = static_cast<T>((c1 * vx * vy) - (st * vz));
    trsfrm[0].z = static_cast<T>((c1 * vx * vz) + (st * vy));
    // Second row
    trsfrm[1].x = static_cast<T>((c1 * vy * vx) + (st * vz));
    trsfrm[1].y = static_cast<T>(ct + (c1 * vy * vy));
    trsfrm[1].z = static_cast<T>((c1 * vy * vz) - (st * vx));
    // Third row
    trsfrm[2].x = static_cast<T>((c1 * vz * vx) - (st * vy));
    trsfrm[2].y = static_cast<T>((c1 * vz * vy) + (st * vx));
    trsfrm[2].z = static_cast<T>(ct + (c1 * vz * vz));
  }
#else
  // Input format: [Gamma, Theta, Phi]
  const T cg = Math::Cos(cam[0]);
  const T sg = Math::Sin(cam[0]);
  const T ct = Math::Cos(cam[1]);
  const T st = Math::Sin(cam[1]);
  const T cp = Math::Cos(cam[2]);
  const T sp = Math::Sin(cam[2]);
  // First row
  trsfrm[0].x = ct * cg;
  trsfrm[0].y = (sp * st * cg) - (cp * sg);
  trsfrm[0].z = (sp * sg) + (cp * st * cg);
  // Second row
  trsfrm[1].x = ct * sg;
  trsfrm[1].y = (sp * st * sg) + (cp * cg);
  trsfrm[1].z = (cp * st * sg) - (sp * cg);
  // Third row
  trsfrm[2].x = -st;
  trsfrm[2].y = sp * ct;
  trsfrm[2].z = cp * ct;
#endif
  // Translation
  trsfrm[3].x = cam[3] * focal;
  trsfrm[3].y = cam[4] * focal;
  trsfrm[3].z = cam[5] * focal;
}

/**
 * @name    TransformVertex
 * @brief   Apply a given rigid transformation on a vertex
 * @param[in] v         Vertex to be transformed
 * @param[in] trsrm     Rigid transformation to apply
 * @tparam T    Data type
 * @return  Transformed vertex
 */
template<typename T>
__device__ __forceinline__
Vec3<T> TransformVertex(CVec3<T>& v, CVec3<T>* trsrm) {
  Vec3<T> vt;
  vt.x = dot(trsrm[0], v);
  vt.y = dot(trsrm[1], v);
  vt.z = dot(trsrm[2], v);
  vt += trsrm[3];
  return vt;
}

/**
 * @name    IsBoundaryKernel
 * @brief Define if pixels belong to the object boundary or not
 * @param bcoords   Barycentric coordinates
 * @param width     Image width
 * @param height    Image height
 * @param bnd       Boundary mask (binary)
 * @tparam T    Data type
 */
template<typename T>
__global__
void IsBoundaryKernel(CVec4<T>* bcoords,
                      int width,
                      int height,
                      tf::uint8* bnd) {
  // Define indices
  const int j = blockIdx.x * blockDim.x + threadIdx.x;
  const int i = blockIdx.y * blockDim.y + threadIdx.y;
  const int k = blockIdx.z * blockDim.z + threadIdx.z;
  // within image boundaries ?
  if (j < width && i < height) {
    // Index offset
    const int map[] = {-width,
                       -1,
                       1,
                       width};
    // upper bound
    const int wh = width * height;
    const int idx = (k * wh) + (i * width) + j;  // Flatten index
    // Check neighbours
    if (bcoords[idx].x != T(0.0)) {
      // Loop over 4 neighboring pixels
      bool is_bnd = false;
      for (int n = 0; n < 4; ++n) {
        int p = idx + map[n];
        if (p >= (k * wh) && p < ((k + 1) * wh)) {
          // inside image, check if on column 0 or w-1
          int c = (p % wh) % width;
          if (c == 0 && k == 1) continue;
          if (c == (width - 1) && k == 2) continue;
          // check bcoord value
          if (bcoords[p].x == T(0.0)) {
            is_bnd = true;
            break;
          }
        }
      }
      // all neighbours are nonzero
      bnd[idx] = (is_bnd ? PixelType::kBoundary : PixelType::kInterior);
    } else {
      // Is exterior
      bnd[idx] = PixelType::kExterior;
    }
  }
}

/**
 * @name    ResetEdgeCounterKernel
 * @param[in] n_edge        Number of edges
 * @param[out] edge_list    Array storying edge indexes
 */
__global__
void ResetEdgeCounterKernel(const int n_edge, unsigned int* edge_list) {
  int idx = threadIdx.x * n_edge;
  edge_list[idx] = 0;
}

template<typename T>
__device__ __forceinline__
T IsFaceVisible(CVec3i& f, CVec3<T>* v) {
  // Compute COG + transform it
  CVec3<T> v0 = v[f.x];
  CVec3<T> v1 = v[f.y];
  CVec3<T> v2 = v[f.z];
  Vec3<T> cog = (v0 + v1 + v2) * T(1.0 / 3.0);
  // Compute triangle normal using cross product
  Vec3<T> n = cross(v1 - v0, v2 - v0);
  n = normalize(n);
  // Visibility check
  return dot(n, cog);
}

/**
 * @name    SilhouetteEdgeIdxKernel
 * @brief Detect edge on the silhouette of the mesh
 * @param[in] v         List of vertices
 * @param[in] n         List of normals
 * @param[in] f         List of triangle
 * @param[in] fpe       Face per edge map.
 * @param[in] vpe       Vertex per edge map.
 * @param[in] n_edge    Maximum number of edges to test
 * @param[in] n_face    Upper bound for triangle index, if need to check all of
 *                      them, set to -1
 * @param[in,out] edges List if edge index that lies on the silhouette
 * @tparam T Data type
 */
template<typename T>
__global__
void SilhouetteEdgeIdxKernel(CVec3<T>* v,
                             CVec3i* f,
                             CVec2i* fpe,
                             CVec2i* vpe,
                             int n_edge,
                             int n_face,
                             unsigned int* edges) {
  // Index
  const int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
  const int iy = (blockIdx.y * blockDim.y) + threadIdx.y;

  if (ix >= n_edge) return;


  CVec2i& faces = fpe[ix];
  // Get face index for the current edge. Check for boundary edge as well.
  int fpe_0 = faces.x;
  int fpe_1 = faces.y == -1 ? fpe_0 : faces.y;

  // Is visbility check required ?
  //  n_face == -1: Check all the faces
  //  n_face == N : Only check the first N faces
  if (n_face == -1 || min(fpe_0, fpe_1) < n_face) {
    // Boundary edge ?
    T visi;
    if (fpe_0 == fpe_1) {
      visi = T(-1.0);
    } else {
      // Visibility of first face
      if (n_face == -1 || fpe_1 < n_face) {
        visi = IsFaceVisible<T>(f[fpe_0], v);
        visi *= IsFaceVisible<T>(f[fpe_1], v);
      } else {
        visi = T(-1.0); // fpe_1 > n_face is like having a boundary edge
      }
    }
    // Visible ?
    if (visi <= T(0.0)) {
      // Read counter for this image
      auto *edge_list = &edges[iy * (n_edge * 2 + 1)];
      int idx = atomicAdd(edge_list, 1);
      idx = (idx * 2) + 1;
      edges[idx] = static_cast<unsigned int>(vpe[ix].x);
      edges[idx + 1] = static_cast<unsigned int>(vpe[ix].y);
    }
  }
}

/**
 * @name    FgBgMaskKernel
 * @brief   Compute Foreground/Background mask
 * @param[in] bcoord Barycentric coordinate (Slice)
 * @param[in] height Image's height
 * @param[in] width Image's width
 * @param[out] mask Generated mask (Slice)
 * @tparam T    Data type
 */
template<typename T>
__global__
void FgBgMaskKernel(CVec4<T>* bcoord, int height, int width, T* mask) {
  using Math = LTS5::CUDA::Math<T>;
  // Define pos
  const int i = blockIdx.y * blockDim.y + threadIdx.y;
  const int j = blockIdx.x * blockDim.x + threadIdx.x;
  const int k = blockIdx.z * blockDim.z + threadIdx.z;

  if (i < height && j < width) {
    // Get barycentric coordinates
    const int idx = (k * width * height) + (i * width) + j;
    const auto& bc = bcoord[idx];
    const int tri_idx = static_cast<int>(Math::Round(bc.x));
    if (tri_idx > 0) {    // Triangle index start at 1 !!!
      // Foreground
      mask[idx] = T(1.0);
    } else {
      // Backgrond
      mask[idx] = T(0.0);
    }
  }
}

/**
 * @name    GradImageX
 * @brief   Compute image gradient in X direction. Pay attention to interior /
 *          exterior pixels as well as boundary one
 * @param[in] img  Image
 * @param[in] bnd  Boundary mask
 * @param[in] ix  Row index
 * @param[in] smIdx Shared memory index
 * @param[in] width  Image width
 * @tparam T    Data type
 * @return  Gradient in the X direction for a given pixel location
 */
template<typename T>
__device__ __forceinline__
Vec3<T> GradImageX(CVec3<T>* img,
                   const tf::uint8* bnd,
                   int ix,
                   int smIdx,
                   int width) {
  // Early stop, pixel not on a 3D object (i.e. bnd[i] == kExterior)
  if (bnd[smIdx] != PixelType::kExterior) {
    // On surface
    const int n_idx = smIdx + 1;
    const int p_idx = smIdx - 1;
    if (bnd[smIdx] == PixelType::kInterior) {
      // Inside an object or 1px outside boundary, similar mechanic apply.
      // The filter should not cross a boundary pixel.
      if (ix == 0) {
        // First col
        bool next_is_b = bnd[n_idx] == PixelType::kBoundary;
        // Zero or Forward difference
        return next_is_b ? Pack3(T(0.0)) : img[n_idx] - img[smIdx];
      } else if (ix == (width - 1)) {
        // Last col
        bool prev_is_b = bnd[p_idx] == PixelType::kBoundary;
        // Zero or Backward difference
        return prev_is_b ? Pack3(T(0.0)) : img[smIdx] - img[p_idx];
      } else {
        // Standard
        bool next_is_b = bnd[n_idx] == PixelType::kBoundary;
        bool prev_is_b = bnd[p_idx] == PixelType::kBoundary;
        if (next_is_b && prev_is_b) {
          return Pack3(T(0.0));
        } else if (next_is_b) {
          return (img[smIdx] - img[p_idx]);  // Backward difference
        } else if (prev_is_b) {
          return (img[n_idx] - img[smIdx]);  // Forward difference
        } else {
          return (img[n_idx] - img[p_idx]) * T(0.5);   // Central difference
        }
      }
    } else {
      // On the boundaries
      // NOTE:
      // Gradient is mutiplied by 2 to compensate for bias. Central difference
      // speard gradient over two pixels at edges, therefore multiplie by 2 to
      // have both contribution in our single pixel boudary.
      // See:
      // https://github.com/mattloper/opendr/blob/bc16a6a51771d6e062d088ba5cede66649b7c7ec/opendr/common.py#L50
      if (ix == 0) {
        return (img[n_idx] - img[smIdx]);  // Forward difference
      } else if (ix == (width - 1)) {
        return (img[smIdx] - img[p_idx]); // Backward difference
      } else {

        bool n_bnd = bnd[n_idx] == PixelType::kBoundary;
        bool n_bg = bnd[n_idx] == PixelType::kExterior;
        bool p_bnd = bnd[p_idx] == PixelType::kBoundary;
        bool p_bg = bnd[p_idx] == PixelType::kExterior;

        if ((p_bg && n_bnd) || (p_bnd && n_bnd) || (n_bg && p_bnd)) {
          return Pack3(T(0.0));
        } else {
          return (img[n_idx] - img[p_idx]) * T(0.5);   // Central difference
        }
      }
    }
  } else {
    // Exterior
    return Pack3(T(0.0));
  }
}

/**
 * @name    GradImageY
 * @brief   Compute image gradient in Y direction. Pay attention to interior /
 *          exterior pixels as well as boundary one
 * @param[in] img   Image
 * @param[in] bnd   Boundary mask
 * @param[in] iy    Row index
 * @param[in] smIdx Shared memory index
 * @param[in] smStride  Shared memory stride
 * @return Y gradient
 * @tparam T    Data type
 * @return  Gradient in the X direction for a given pixel location
 */
template<typename T>
__device__ __forceinline__
Vec3<T> GradImageY(CVec3<T>* img,
                   const tf::uint8* bnd,
                   int iy,
                   int smIdx,
                   int height,
                   int smStride) {
  // Early stop, pixel not on a 3D object (i.e. bnd[i] == kExterior)
  if (bnd[smIdx] != PixelType::kExterior) {
    // On surface
    const int n_idx = smIdx + smStride;
    const int p_idx = smIdx - smStride;
    if (bnd[smIdx] == PixelType::kInterior) {
      // Interior
      if (iy == 0) {
        // First row
        bool next_is_b = bnd[n_idx] == PixelType::kBoundary;
        // Zero or Forward difference
        return next_is_b ? Pack3(T(0.0)) : img[n_idx] - img[smIdx];
      } else if (iy == (height - 1)) {
        // Last row
        bool prev_is_b = bnd[p_idx] == PixelType::kBoundary;
        // Zero or Backward difference
        return prev_is_b ? Pack3(T(0.0)) : img[smIdx] - img[p_idx];
      } else {
        // Standard
        bool next_is_b = bnd[n_idx] == PixelType::kBoundary;
        bool prev_is_b = bnd[p_idx] == PixelType::kBoundary;
        if (next_is_b && prev_is_b) {
          return Pack3(T(0.0));
        } else if (next_is_b) {
          return (img[smIdx] - img[p_idx]);  // Backward difference
        } else if (prev_is_b) {
          return (img[n_idx] - img[smIdx]);  // Forward difference
        } else {
          return (img[n_idx] - img[p_idx]) * T(0.5);   // Central difference
        }
      }
    } else {
      // Boundary
      // NOTE:
      // Gradient is mutiplied by 2 to compensate for bias. Central difference
      // speard gradient over two pixels at edges, therefore multiplie by 2 to
      // have both contribution in our single pixel boudary.
      // See:
      // https://github.com/mattloper/opendr/blob/bc16a6a51771d6e062d088ba5cede66649b7c7ec/opendr/common.py#L50
      if (iy == 0) {
        return (img[n_idx] - img[smIdx]);  // Forward difference
      } else if (iy == (height - 1)) {
        return (img[smIdx] - img[p_idx]); // Backward difference
      } else {
        bool n_bnd = bnd[n_idx] == PixelType::kBoundary;
        bool n_bg = bnd[n_idx] == PixelType::kExterior;
        bool p_bnd = bnd[p_idx] == PixelType::kBoundary;
        bool p_bg = bnd[p_idx] == PixelType::kExterior;
        if ((p_bg && n_bnd) || (p_bnd && n_bnd) || (n_bg && p_bnd)) {
          return Pack3(T(0.0));
        } else {
          return (img[n_idx] - img[p_idx]) * T(0.5);   // Central difference
        }
      }
    }
  } else {
    // Exterior
    return Pack3(T(0.0));
  }
}

/**
 * @name SpatialGradientKernel
 * @brief   Compute image spatial gradient
 * @param[in] im_synth    Synthetic Image (RGBA)
 * @param[in] im_label    True Image (Label)
 * @param[in] bnd   Object boundaries
 * @param[in] width Image width
 * @param[in] height Image height
 * @param[out] gx_ptr  Spatial gradient X component
 * @param[out] gy_ptr  Spatial gradient Y component
 * @tparam T    Data type
 * @tparam Bx   Block X dimensions
 * @tparam By   Block Y dimensions
 */
template<typename T, int Bx, int By>
__global__
void SpatialGradientKernel(CVec4<T>* im_synth,
                           CVec3<T>* im_label,
                           const tf::uint8* bnd,
                           int width,
                           int height,
                           Vec3<T>* g_ptr) {
  using Math = CUDA::Math<T>;
  // Shared memory: (block_dim + 2) x (block_dim + 2)
  // Cache image + boundaries
  __shared__ Vec3<T> smImg[(Bx + 2) * (By + 2)];
  __shared__ tf::uint8 smBnd[(Bx + 2) * (By + 2)];
  // Copy data inside block to middle of shared memory
  int ix = blockIdx.x * blockDim.x + threadIdx.x;
  int iy = blockIdx.y * blockDim.y + threadIdx.y;
  int ik = blockIdx.z * blockDim.z + threadIdx.z;

  // Out of image boundaries
  if (ix >= width || iy >= height) return;

  // position within global memory array
  const int wh = width * height;
  const int im_pos = min(ix, width - 1) + min(iy, height - 1) * width;
  const int pos = (ik * wh) + im_pos;   // Global
  // position within shared memory array
  int shMemPos = threadIdx.x + 1 + (threadIdx.y + 1) * (Bx + 2);
  // Load data to shared memory. Load tile being processed
  smBnd[shMemPos] = bnd[pos];
  auto cond = (smBnd[shMemPos] == PixelType::kExterior ||
               smBnd[shMemPos] == PixelType::kBoundary);
  smImg[shMemPos] = cond ?
                    im_label[pos] :
                    Pack<T>(im_synth[pos].x, im_synth[pos].y, im_synth[pos].z);
  // Handle necessary neighbours
  // We clamp out-of-range coordinates. It is equivalent to mirroring
  // because we access data only one step away from borders.
  if (threadIdx.y == 0) {
    // Begin of the tile
    const int bsx = blockIdx.x * blockDim.x;
    const int bsy = blockIdx.y * blockDim.y;
    // element position within tile
    int x, y;
    // element position within linear array:
    //    gm - global memory
    //    sm - shared memory
    int gmPos, smPos;
    x = min(bsx + int(threadIdx.x), width - 1);  // Clamp at image far right
    y = max(bsy - 1, 0);  // row just below the tile
    gmPos = (ik * wh) + (y * width) + x;
    smPos = threadIdx.x + 1;
    // Row before tile
    smBnd[smPos] = bnd[gmPos];
    cond = (smBnd[smPos] == PixelType::kExterior ||
            smBnd[smPos] == PixelType::kBoundary);
    smImg[smPos] = cond ?
                   im_label[gmPos] :
                   Pack<T>(im_synth[gmPos].x,
                           im_synth[gmPos].y,
                           im_synth[gmPos].z);
    // row after the tile
    y = min(bsy + By, height - 1);
    smPos += (By + 1) * (Bx + 2);
    gmPos = (ik * wh) + (y * width) + x;
    smBnd[smPos] = bnd[gmPos];
    cond = (smBnd[smPos] == PixelType::kExterior ||
            smBnd[smPos] == PixelType::kBoundary);
    smImg[smPos] = cond ?
                   im_label[gmPos] :
                   Pack<T>(im_synth[gmPos].x,
                           im_synth[gmPos].y,
                           im_synth[gmPos].z);
  } else if (threadIdx.y == 1) {
    // Begin of the tile
    const int bsx = blockIdx.x * blockDim.x;
    const int bsy = blockIdx.y * blockDim.y;
    // element position within tile
    int x, y;
    // element position within linear array:
    //    gm - global memory
    //    sm - shared memory
    int gmPos, smPos;
    // Use threadIdx.x since threadIdx.y == 0
    y = min(bsy + int(threadIdx.x), height - 1);
    // column to the left
    x = max(bsx - 1, 0);
    smPos = Bx + 2 + threadIdx.x * (Bx + 2);
    gmPos = (ik * wh) + (y * width) + x;
    // check if we are within tile
    if (threadIdx.x < By) {
      smBnd[smPos] = bnd[gmPos];
      cond = (smBnd[smPos] == PixelType::kExterior ||
              smBnd[smPos] == PixelType::kBoundary);
      smImg[smPos] = cond ?
                     im_label[gmPos] :
                     Pack<T>(im_synth[gmPos].x,
                             im_synth[gmPos].y,
                             im_synth[gmPos].z);
      // column to the right
      x = min(bsx + Bx, width - 1);
      gmPos = (ik * wh) + (y * width) + x;
      smPos += Bx + 1;
      smBnd[smPos] = bnd[gmPos];
      cond = (smBnd[smPos] == PixelType::kExterior ||
              smBnd[smPos] == PixelType::kBoundary);
      smImg[smPos] = cond ?
                     im_label[gmPos] :
                     Pack<T>(im_synth[gmPos].x,
                             im_synth[gmPos].y,
                             im_synth[gmPos].z);
    }
  }
  // Sync
  __syncthreads();

  // Image grad
  auto gx = GradImageX<T>(&smImg[0], &smBnd[0], ix, shMemPos, width);
  auto gy = GradImageY<T>(&smImg[0], &smBnd[0], iy, shMemPos, height, Bx+2);
  // Dump into final tensor
  int idx_x = ((2 * ik) * wh) + (iy * width) + ix;
  int idx_y = (((2 * ik) + 1) * wh) + (iy * width) + ix;
  g_ptr[idx_x] = -gx;     // Grad X
  g_ptr[idx_y] = -gy;    // Grad Y
}

}  // namespace device

#pragma mark -
#pragma mark Caller

/**
 * @struct  MMImageProcessor
 * @brief   Utility class to compute various data in the image space.
 *          Specialisation for GPU device
 * @author  Christophe Ecabert
 * @date    09.04.19
 * @ingroup tensorflow_op
 * @tparam T        Data type
 */
template<typename T>
class MMImageProcessor<GPUDevice, T> {
 public:
  /** Tensor type */
  using Tensor = tensorflow::Tensor;
  /** Context type */
  using OpKernelContext = tensorflow::OpKernelContext;

  /**
   * @name    ComputeSilhouetteEdges
   * @brief   Define the type of pixel for a rendered image
   *          {Exterior, Interior, Border}
   * @param[in] ctx   Operation context
   * @param[in] bcoords   Barycentric coordinates
   * @param[out] boundaries    Computed boundaries
   * @return -1 if error, 0 otherwise
   */
  static int ComputeSilhouetteEdges(OpKernelContext* ctx,
                                    const Tensor& bcoords,
                                    Tensor* boundaries) {
    // Allocate output
    auto& shp = bcoords.shape();
    auto s = ctx->allocate_temp(tf::DataTypeToEnum<tf::uint8>::value,
                                {shp.dim_size(0),
                                 shp.dim_size(1),
                                 shp.dim_size(2)},
                                boundaries);
    if (!s.ok()) {
      ctx->CtxFailureWithWarning(__FILE__, __LINE__, s);
      return -1;
    }
    // Compute boundaries
    const auto& device = ctx->eigen_gpu_device();
    const auto& stream = device.stream();
    // Access data
    auto *bc_ptr = reinterpret_cast<CVec4<T> *>(bcoords.flat<T>().data());
    auto *bnd_ptr = boundaries->flat<tf::uint8>().data();
    // Boundaries computation
    auto b_col = shp.dim_size(2);
    auto b_row = shp.dim_size(1);
    auto b_depth = shp.dim_size(0);
    dim3 thd(32, 32, 1);
    dim3 bck_b((b_col + 31) / 32, (b_row + 31) / 32, b_depth);
    device::IsBoundaryKernel<T><<<bck_b, thd, 0, stream>>>(bc_ptr,
            b_col,
            b_row,
            bnd_ptr);

    // DEBUG
#ifdef DUMP_STUFF
    for (auto k = 0; k < bcoords.dim_size(0); ++k) {
      // Access slice
      {
        auto bnd_slice = boundaries->Slice(k, k + 1);
        using CTMap8U = typename tensorflow::TTypes<tf::uint8>::ConstTensor;
        using TMap8U = typename tensorflow::TTypes<tf::uint8>::Tensor;
        cv::Mat bnd_cpu(bnd_slice.dim_size(1),
                        bnd_slice.dim_size(2),
                        CV_8UC1);
        auto in = CTMap8U(bnd_slice.unaligned_flat<tf::uint8>().data(),
                          bnd_slice.unaligned_flat<tf::uint8>().size());
        auto out = TMap8U((tf::uint8 *) bnd_cpu.data,
                          bnd_cpu.rows * bnd_cpu.cols);
        LTS5::Functor::DenseOp<Eigen::GpuDevice, tf::uint8, LTS5::Functor::DenseOpType::kD2HCopy> copy;
        copy(device, in, out);
        LTS5::Functor::DeviceHelper<Eigen::GpuDevice>::Synchronize(device);
        LTS5::SaveMatToBin("dbg/bnd_" + std::to_string(k) + ".bin", bnd_cpu);
      }
    }
#endif

    return 0;
  }

  /**
   * @name  ComputeSilhouetteEdges
   * @brief Compute vertex indexes located on the contour of the surface
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex array slice
   * @param[in] normal      Normal array slice
   * @param[in] triangle    Triangles
   * @param[in] fpe         Face-per-edge map
   * @param[in] vpe         Vertex-per-edge map
   * @param[in] n_face      Number of face to check, if all set -1
   * @param[out] silhouette_edge_idx    Silhouette indexes
   * @return -1 if error, 0 otherwise
   */
  static int ComputeSilhouetteEdges(OpKernelContext* ctx,
                                    const Tensor& vertex,
                                    const Tensor& triangle,
                                    const Tensor& fpe,
                                    const Tensor& vpe,
                                    const int& n_face,
                                    Tensor* silhouette_edge_idx) {
    // Allocate output
    auto n_edge = vpe.dim_size(0);
    auto s = ctx->allocate_temp(tf::DataTypeToEnum<tf::uint32>::value,
                                {1, 1 + tf::int64(n_edge * 2)},
                                silhouette_edge_idx);
    if (!s.ok()) {
      ctx->CtxFailureWithWarning(__FILE__, __LINE__, s);
      return -1;
    }

    // Compute silhouette edges
    // Raw ptr
    auto* v_ptr = reinterpret_cast<CVec3<T>*>(vertex.unaligned_flat<T>().data());
    auto* fpe_ptr = reinterpret_cast<CVec2i*>(fpe.flat<int>().data());
    auto* vpe_ptr = reinterpret_cast<CVec2i*>(vpe.flat<int>().data());
    auto* tri_ptr = reinterpret_cast<CVec3i*>(triangle.flat<int>().data());
    auto* silh_edge_ptr = silhouette_edge_idx->flat<unsigned int>().data();
    // Dims
    dim3 thd(1024);  // Process 1024 edges per block
    dim3 bck((n_edge + 1023) / 1024);
    const auto& device = ctx->eigen_gpu_device();
    const auto& stream = device.stream();
    // Init counter
    device::ResetEdgeCounterKernel<<<1, 1, 0, stream>>>(1 + (n_edge * 2),
                                                        silh_edge_ptr);
    // Compute silhouette's edges
    device::SilhouetteEdgeIdxKernel<T><<<bck, thd, 0, stream>>>(v_ptr,
                                                                tri_ptr,
                                                                fpe_ptr,
                                                                vpe_ptr,
                                                                n_edge,
                                                                n_face,
                                                                silh_edge_ptr);
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }

  /**
   * @name  CorrectBoundaryMap
   * @brief Correct rendered edge map. This is needed since rendered boundary
   *        map are one pixel off.
   * @param[in] ctx         Op's context
   * @param[in] image_edge  Rendered edges
   * @param[out] boundaries Corrected boundary map
   * @return -1 if error, 0 otherwise
   */
  static int CorrectBoundaryMap(OpKernelContext* ctx,
                                const Tensor& image_edge,
                                Tensor* boundaries) {
    return  -1;
  }

  /**
   * @name ComputeSpatialGradient
   * @brief Compute image spatial gradient in both direction {x,y}
   * @param[in] ctx   Operation context
   * @param[in] img_synth Color images from which to compute gradient (Synth)
   * @param[in] img_label True images from which to compute gradient, used for
   *                      border in order to have proper derivative (influenced
   *                      by background)
   * @param[in] boundaries  Object boundaries map
   * @param[out] grad Gradient in X/Y direction
   * @return -1 if error, 0 otherwise
   */
  static int ComputeSpatialGradient(OpKernelContext* ctx,
                                    const Tensor& img_synth,
                                    const Tensor& img_label,
                                    const Tensor& boundaries,
                                    Tensor* grad) {

//    {
//      using CTMap8U = typename tensorflow::TTypes<tf::uint8>::ConstTensor;
//      using TMap8U = typename tensorflow::TTypes<tf::uint8>::Tensor;
//      auto device = ctx->eigen_gpu_device();
//      cv::Mat bnd_cpu(boundaries.dim_size(1),
//                      boundaries.dim_size(2),
//                      CV_8UC1);
//      auto in = CTMap8U(boundaries.flat<tf::uint8>().data(),
//                        boundaries.flat<tf::uint8>().size());
//      auto out = TMap8U((tf::uint8 *) bnd_cpu.data,
//                        bnd_cpu.rows * bnd_cpu.cols);
//      LTS5::Functor::DenseOp<Eigen::GpuDevice, tf::uint8, LTS5::Functor::DenseOpType::kD2HCopy> copy;
//      copy(device, in, out);
//      LTS5::Functor::DeviceHelper<Eigen::GpuDevice>::Synchronize(device);
//      LTS5::SaveMatToBin("dbg/bnd0.bin", bnd_cpu);
//    }

    // Get device
    const auto& stream = ctx->eigen_gpu_device().stream();
    // Compute gradient
    const auto* im_synth_ptr = reinterpret_cast<CVec4<T>*>(img_synth.unaligned_flat<T>().data());
    const auto* im_label_ptr = reinterpret_cast<CVec3<T>*>(img_label.unaligned_flat<T>().data());
    const auto* bnd_ptr = boundaries.unaligned_flat<tf::uint8>().data();
    auto* g_ptr = reinterpret_cast<Vec3<T>*>(grad->unaligned_flat<T>().data());
    // Define grid
    auto n_img = static_cast<int>(img_synth.dim_size(0));
    auto im_h = static_cast<int>(img_synth.dim_size(1));
    auto im_w = static_cast<int>(img_synth.dim_size(2));
    dim3 thd(32, 32, 1);
    dim3 bck((im_w + 31) / 32, (im_h + 31) / 32, n_img);
    // Gradient
    device::SpatialGradientKernel<T, 32, 32><<<bck, thd, 0, stream>>>(im_synth_ptr,
            im_label_ptr,
            bnd_ptr,
            im_w,
            im_h,
            g_ptr);
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }

  /**
   * @name ComputeObjectMask
   * @brief Compute binary mask where the object lies
   * @param[in] ctx     Op context
   * @param[in] bcoord  Barycentric coordinates (4D Tensor)
   * @param[in] mask    Generated mask with min/max texture value explainable by
   *                    the model
   * @return -1 if error, 0 otherwise
   */
  static int ComputeObjectMask(OpKernelContext* ctx,
                               const Tensor& bcoord,
                               Tensor* mask) {
    auto& stream = ctx->eigen_gpu_device().stream();
    // Image properties
    auto n_img = bcoord.dim_size(0);
    auto h = static_cast<int>(bcoord.dim_size(1));
    auto w = static_cast<int>(bcoord.dim_size(2));
    const auto* bc_ptr = reinterpret_cast<CVec4<T>*>(bcoord.flat<T>().data());
    auto* mask_ptr = mask->flat<T>().data();
    // Define launch params
    dim3 thd(32, 32, 1);
    dim3 bck((h + 31) / 32, (w + 31) / 32, n_img);
    device::FgBgMaskKernel<T><<<bck, thd, 0, stream>>>(bc_ptr, h, w, mask_ptr);
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};



#pragma mark -
#pragma mark Explicit Instantiation

/** Float - GPU */
template class MMImageProcessor<GPUDevice, float>;
/** Double - GPU */
template class MMImageProcessor<GPUDevice, double>;

}  // namespace LTS5
