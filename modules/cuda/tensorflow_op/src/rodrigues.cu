/**
 *  @file   rodrigues.cu
 *  @brief  Compute rotation matrix using Rodrigues formula
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/8/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */


#include "cuda_runtime.h"
#include "cuda.h"

#if defined(__CUDA_ARCH__) && __CUDA_ARCH__ < 600
__device__ double atomicAdd(double* address, double val) {
    unsigned long long int* address_as_ull = (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
                __double_as_longlong(val + __longlong_as_double(assumed)));
    } while (assumed != old);
    return __longlong_as_double(old);
}
#endif

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"

#include "lts5/tensorflow_op/rodrigues.hpp"
#include "lts5/tensorflow_op/rodrigues_grad.hpp"

#include "lts5/utils/logger.hpp"

using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

template<typename T>
__device__ __forceinline__
T SafeDiv(const T& x, const T& y) {
  T sign_y = y >= T(0.0) ? T(1.0): T(-1.0);
  return x / (y + sign_y * 1e-10);
}

/**
 * @name    RodriguesToRMat
 * @brief Compute rotation matrix using rodrigues formula
 * @param[in] rvec  Rodrigues vector
 * @param[out] rmat Rotation matrix
 * @tparam T    Data type
 */
template<typename T>
__global__
void RodriguesToRMat(const T* rvec, T* rmat) {
  const auto i = threadIdx.x;
  const auto j = threadIdx.y;

  // Access rotation element (axis)
  double vx = __ldg(&rvec[(j * 3) + 0]);
  double vy = __ldg(&rvec[(j * 3) + 1]);
  double vz = __ldg(&rvec[(j * 3) + 2]);
  double ang = std::sqrt((vx*vx) + (vy*vy) + (vz*vz));
  bool is_id = ang < std::numeric_limits<double>::epsilon();
  // Convert to angles
  double ct = std::cos(ang);
  double st = std::sin(ang);
  double c1 = 1.0 - ct;
  double i_ang = ang ? 1.0 / ang : 0.0;
  vx = vx * i_ang;
  vy = vy * i_ang;
  vz = vz * i_ang;
  // Fill matrix, column major style
  T* dst = &(rmat[(j * 9) + i]);
  switch (i) {
    // First row
    case 0: *dst = is_id ? T(1.0) : static_cast<T>(ct + (c1 * vx * vx)); break;
    case 1: *dst = is_id ? T(0.0) : static_cast<T>((c1 * vx * vy) - (st * vz)); break;
    case 2: *dst = is_id ? T(0.0) : static_cast<T>((c1 * vx * vz) + (st * vy)); break;
    // Second row
    case 3: *dst = is_id ? T(0.0) : static_cast<T>((c1 * vy * vx) + (st * vz)); break;
    case 4: *dst = is_id ? T(1.0) : static_cast<T>(ct + (c1 * vy * vy)); break;
    case 5: *dst = is_id ? T(0.0) : static_cast<T>((c1 * vy * vz) - (st * vx)); break;
    // Third row
    case 6: *dst = is_id ? T(0.0) : static_cast<T>((c1 * vz * vx) - (st * vy)); break;
    case 7: *dst = is_id ? T(0.0) : static_cast<T>((c1 * vz * vy) + (st * vx)); break;
    case 8: *dst = is_id ? T(1.0) : static_cast<T>(ct + (c1 * vz * vz)); break;

    if (std::isnan(*dst)) {
      printf("%d, rmat=%.3f, v=%.3f, %.3f, %.3f\n", i, *dst, vx, vy, vz);
    }
  }
}

/**
 * @name    RodriguesGradient
 * @brief Compute Rodrigues gradient
 * @param[in] rvec      Rotation vector [N x 3]
 * @param[in] g_rmat    Back-propagated gradient [N x 3 x 3]
 * @param[out] grad     Chained gradient [N x 3]
 * @tparam T    Data type
 */
template<typename T>
__global__
void RodriguesGradient(const T* rvec,
                       const T* g_rmat,
                       T* grad) {
  // Do stuff here
  int ix = threadIdx.x;  // [0, 8]
  int iy = threadIdx.y;  // [0, B]
  // Access rotation element (axis)
  double qx = __ldg(&rvec[(iy * 3) + 0]);
  double qy = __ldg(&rvec[(iy * 3) + 1]);
  double qz = __ldg(&rvec[(iy * 3) + 2]);
  double ang = std::sqrt((qx*qx) + (qy*qy) + (qz*qz));
  if (ang < std::numeric_limits<double>::epsilon()) {
    const double j[] = {0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0,  // dR_dqx
                        0.0, 0.0, 1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0,  // dR_dqy
                        0.0, -1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0   // dR_dqz
                        };
    // Compute grad
    #pragma unroll
    for(int k = 0; k < 3; ++k ) {
      const T v = static_cast<T>(j[(k * 9) + ix] * g_rmat[(iy * 9) + ix]);
      atomicAdd(&grad[(iy * 3) + k], v * g_rmat[(iy * 9) + ix]);
    }
  } else {
    // Get angle + axis
    double ct = std::cos(ang);
    double st = std::sin(ang);
    double c1 = 1.0 - ct;
    double i_ang = ang ? 1.0 / ang : 0.0;
    qx = qx * i_ang;
    qy = qy * i_ang;
    qz = qz * i_ang;
    // Compute Jacobian
    const double qqt[] = {qx*qx, qx*qy, qx*qz, qx*qy, qy*qy, qy*qz, qx*qz, qy*qz, qz*qz};
    const double q_x[] = {0.0, -qz,  qy, qz, 0.0, -qx, -qy,  qx, 0.0}; // Cross-product operator
    const double I[] = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0}; // Identity matrix
    const double dqqt[] = {qx+qx, qy, qz, qy, 0.0, 0.0, qz, 0.0, 0.0,
                           0.0, qx, 0.0, qx, qy+qy, qz, 0.0, qz, 0.0,
                           0.0, 0.0, qx, 0.0, 0.0, qy, qx, qy, qz+qz };
    const double d_q_x[] = {0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0,
                            0.0, 0.0, 1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0,
                            0.0, -1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    #pragma unroll
    for(int k = 0; k < 3; ++k ) {
      double qi = k == 0 ? qx : (k == 1 ? qy : qz);
      double a0 = -st * qi;
      double a1 = (st - (2.0 * c1 * i_ang)) * qi;
      double a2 = (c1 * i_ang);
      double a3 = (ct - (st * i_ang)) * qi;
      double a4 = (st * i_ang);
      // Compute grad
      const T v = static_cast<T>((a0 * I[ix]) +
                                 (a1 * qqt[ix]) + (a2 * dqqt[(k * 9) + ix]) +
                                 (a3 * q_x[ix]) + (a4 * d_q_x[(k * 9) + ix]));
      const T g = v * g_rmat[(iy * 9) + ix];
      atomicAdd(&grad[(iy * 3) + k], g);
    }
  }
}

template<typename T>
__global__
void SetValue(const T value, T* array) {
  array[threadIdx.x] = value;
}

}  // namespace device


/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/*
 * @class   RodriguesHelper
 * @brief   Compute rotation matrix using rodrigues formula
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct RodriguesHelper<GPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute rotation matrix using Rodrigues formula
   * @param[in] ctx     Op's context
   * @param[in] rvec    Rotation vector
   * @param[out] rmat   Rotation matrix
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* rvec,
                 tf::Tensor* rmat) {
    // Acces raw pointer
    const auto* rvec_ptr = rvec->flat<T>().data();
    auto* rmat_ptr = rmat->flat<T>().data();
    // Dimensions
    int n_rvec = rvec->dim_size(0);
    dim3 thd(9, n_rvec);
    dim3 bck(1);
    auto& stream = ctx->eigen_gpu_device().stream();
    device::RodriguesToRMat<T><<<bck, thd, 0, stream>>>(rvec_ptr, rmat_ptr);
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

/*
 * @class   RodriguesGradientHelper
 * @brief   Compute rotation matrix gradient
 * @author  Christophe Ecabert
 * @date    15/07/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct RodriguesGradientHelper<GPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute rotation matrix using Rodrigues formula
   * @param[in] ctx             Op's context
   * @param[in] rvec            Rotation vector
   * @param[in] g_rmat          Back-propagated gradient
   * @param[out] grad           Rotation matrix gradient []
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* rvec,
                 const tf::Tensor* g_rmat,
                 tf::Tensor* grad) {
    // Acces raw pointer
    const auto* rvec_ptr = rvec->flat<T>().data();
    const auto* g_rmat_ptr = g_rmat->flat<T>().data();
    auto* g_ptr = grad->flat<T>().data();
    // Dimensions
    int n_rvec = rvec->dim_size(0);
    dim3 thd(9, n_rvec);
    auto& stream = ctx->eigen_gpu_device().stream();
    // Fill grad mat with zeros
    int n_value = n_rvec * 3;
    device::SetValue<T><<<1, n_value, 0, stream>>>(T(0.0), g_ptr);
    // Compute gradient
    device::RodriguesGradient<T><<<1, thd, 0, stream>>>(rvec_ptr,
            g_rmat_ptr,
            g_ptr);
    return cudaGetLastError() == cudaSuccess ? 0 : -1;
  }
};

#pragma mark -
#pragma mark Explicit Instantiation

/** Float */
template struct RodriguesHelper<GPUDevice, float>;
template struct RodriguesGradientHelper<GPUDevice, float>;
/** Double */
#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
template struct RodriguesHelper<GPUDevice, double>;
template struct RodriguesGradientHelper<GPUDevice, double>;
#endif

}  // namespace Functor
}  // namespace LTS5

