/**
 *  @file   lts5/cuda/tensorflow_op/tensor_utils.hpp
 *  @brief  Utility function for Matrix
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   20/12/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "lts5/tensorflow_op/tensor_utils.hpp"



/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      Helper function
 */
namespace Functor {

/** GPU Device Abstraction */
using GPUDevice = Eigen::GpuDevice;

template <typename T>
struct DenseOp<GPUDevice, T, DenseOpType::kAdd> {

  /**
   * @name  operator()
   * @brief Add a tensor to another one
   * @param[in] d           Device
   * @param[in] to_add      Tensor to add
   * @param[in,out] params  Tensor with the added value (+=)
   */
  void operator()(const GPUDevice& d,
                  typename tensorflow::TTypes<T>::ConstFlat to_add,
                  typename tensorflow::TTypes<T>::Flat params);

  /**
   * @name  operator()
   * @brief Add a tensor to another one - Unaligned
   * @param[in] d           Device
   * @param[in] to_add      Tensor to add
   * @param[in,out] params  Tensor with the added value (+=)
   */
  void operator()(const GPUDevice& d,
                  typename tensorflow::TTypes<T>::UnalignedConstFlat to_add,
                  typename tensorflow::TTypes<T>::UnalignedFlat params);
};

template <typename T>
struct DenseOp<GPUDevice, T, DenseOpType::kSub> {

  /**
   * @name  operator()
   * @brief Subtract a tensor to another one
   * @param[in] d           Device
   * @param[in] to_sub      Tensor to subtract
   * @param[in,out] params  Tensor with the subtracted value (-=)
   */
  void operator()(const GPUDevice& d,
                  typename tensorflow::TTypes<T>::ConstFlat to_sub,
                  typename tensorflow::TTypes<T>::Flat params);

  /**
   * @name  operator()
   * @brief Subtract a tensor to another one - Unaligned
   * @param[in] d           Device
   * @param[in] to_sub      Tensor to subtract
   * @param[in,out] params  Tensor with the subtracted value (-=)
   */
  void operator()(const GPUDevice& d,
                  typename tensorflow::TTypes<T>::UnalignedConstFlat to_sub,
                  typename tensorflow::TTypes<T>::UnalignedFlat params);
};

template <typename T>
struct DenseOp<GPUDevice, T, DenseOpType::kD2DCopy> {

  /**
   * @name  operator()
   * @brief Copy a tensor to another one (device to device)
   * @param[in] d           Device
   * @param[in] to_copy     Tensor to copy (device)
   * @param[in,out] params  Tensor with the copied value (device)
   */
  void operator()(const GPUDevice& d,
                  typename tensorflow::TTypes<T>::ConstFlat to_copy,
                  typename tensorflow::TTypes<T>::Flat params);

  /**
   * @name  operator()
   * @brief Copy a tensor to another one (device to device) - Unaligned
   * @param[in] d           Device
   * @param[in] to_copy     Tensor to copy (device)
   * @param[in,out] params  Tensor with the copied value (device)
   */
  void operator()(const GPUDevice& d,
                  typename tensorflow::TTypes<T>::UnalignedConstFlat to_copy,
                  typename tensorflow::TTypes<T>::UnalignedFlat params);
};

template <typename T>
struct DenseOp<GPUDevice, T, DenseOpType::kH2DCopy> {

  /**
   * @name  operator()
   * @brief Copy a tensor to another one from host to device (only copy when
   *        used with CPU device)
   * @param[in] d           Device
   * @param[in] to_copy     Tensor to copy  (host)
   * @param[in,out] params  Tensor with the copied value (device)
   */
  void operator()(const GPUDevice& d,
                  typename tensorflow::TTypes<T>::ConstFlat to_copy,
                  typename tensorflow::TTypes<T>::Flat params);

  /**
   * @name  operator()
   * @brief Copy a tensor to another one from host to device (only copy when
   *        used with CPU device) - Unaligned
   * @param[in] d           Device
   * @param[in] to_copy     Tensor to copy  (host)
   * @param[in,out] params  Tensor with the copied value (device)
   */
  void operator()(const GPUDevice& d,
                  typename tensorflow::TTypes<T>::UnalignedConstFlat to_copy,
                  typename tensorflow::TTypes<T>::UnalignedFlat params);
};

template <typename T>
struct DenseOp<GPUDevice, T, DenseOpType::kD2HCopy> {

  /**
   * @name  operator()
   * @brief Copy a tensor to another one from device to host (only copy when
   *        used with CPU device)
   * @param[in] d           Device
   * @param[in] to_copy     Tensor to copy  (device)
   * @param[in,out] params  Tensor with the copied value (host)
   */
  void operator()(const GPUDevice& d,
                  typename tensorflow::TTypes<T>::ConstFlat to_copy,
                  typename tensorflow::TTypes<T>::Flat params);

  /**
  * @name  operator()
  * @brief Copy a tensor to another one from device to host (only copy when
  *        used with CPU device) - Unaligned
  * @param[in] d           Device
  * @param[in] to_copy     Tensor to copy  (device)
  * @param[in,out] params  Tensor with the copied value (host)
  */
  void operator()(const GPUDevice& d,
                  typename tensorflow::TTypes<T>::UnalignedConstFlat to_copy,
                  typename tensorflow::TTypes<T>::UnalignedFlat params);
};

}  // namepsace Functor
}  // namespace LTS5
