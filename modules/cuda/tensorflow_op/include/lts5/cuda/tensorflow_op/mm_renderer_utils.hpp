/**
 *  @file   lts5/cuda/tensorflow_op/mm_renderer_utils.hpp
 *  @brief  Utility function for mm_renderer
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   11/29/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_CUDA_MM_RENDERER_UTILS__
#define __LTS5_CUDA_MM_RENDERER_UTILS__

#include "device_launch_parameters.h"

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"


#include "lts5/tensorflow_op/mm_renderer_utils.hpp"

using GPUDevice = Eigen::GpuDevice;

/** Forward declare */
struct cudaGraphicsResource;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

/**
 * @name    UpdateModelTransform
 * @brief   Update Opengl model transformation matrix
 * @param[in] src   Rigid transformation parameters
 * @param[in] focal Focal length
 * @param[out] dst  Uniform buffer object (mapped)
 * @tparam T    Data type
 */
template<typename T>
__global__
void UpdateModelTransform(const T* src, T focal, T* dst);

}  // namespace device

/**
 * @class   MMRenderingShader
 * @brief   Shading program for image rendering
 * @author  Christophe Ecabert
 * @date    03/05/18
 * @ingroup tensorflow
 */
template<typename T>
class MMRenderingShader<GPUDevice, T> {
 public:

  /**
   * @name  MMRenderingShader
   * @fn    MMRenderingShader(const MMRendererParameters<T>& p)
   * @brief Constructor
   * @param[in] p   Rendering parameters
   */
  explicit MMRenderingShader(const MMRendererParameters<T>& p);

  /**
   * @name  ~MMRenderingShader
   * @fn    ~MMRenderingShader() override
   * @brief Destructor
   */
  ~MMRenderingShader();

  /**
   * @name  UpdateModelTransform
   * @fn    int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                                     const tensorflow::Tensor& parameter,
                                     const tensorflow::int64& ct_idx,
                                     const tensorflow::int64& cam_idx)
   * @brief Update model transformation matrix from camera parameters
   * @param[in] ctx Op context
   * @param[in] parameter Morphable model parameters
   * @param[in] i_idx   Index where illumination parameters start
   * @param[in] ct_idx  Index where color transformation parameters start
   * @param[in] cam_idx Index where camera parameters start
   * @return    -1 if error, 0 otherwise
   */
  int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                           const tensorflow::Tensor& parameter,
                           const tensorflow::int64& i_idx,
                           const tensorflow::int64& ct_idx,
                           const tensorflow::int64& cam_idx);

  /**
   * @name  get_shader
   * @fn    const OGLProgram& get_shader() const
   * @brief Give shading program
   * @return    Shader
   */
  const OGLProgram& get_shader() const {
    return *shader_;
  }

  /**
   * @name  get_shader
   * @fn    OGLProgram& get_shader()
   * @brief Give shading program
   * @return    Shader
   */
  OGLProgram& get_shader() {
    return *shader_;
  }

 private:

  /** Vertex Shader */
  static constexpr const char *vs_str =
          "#version 330\n"
          "layout (location = 0) in vec3 vertex;\n"
          "layout (location = 1) in vec3 normal;\n"
          "layout (location = 2) in vec3 color;\n"
          "layout (std140) uniform Transform {\n"
          "  mat4 model;\n"
          "  mat4 projection;\n"
          "};\n"
          "out vec3 normal0;\n"
          "out vec3 color0;\n"
          "void main() {\n"
          "  gl_Position = projection * model * vec4(vertex, 1.f);\n"
          "  color0 = color;\n"
          "  normal0 = (model * vec4(normal, 0.f)).xyz;\n"
          "}";
  /** Uniform Buffer Object */
  UniformBufferObject* ubo_;
  /** Cuda resource */
  cudaGraphicsResource* resource_[2];
  /** Shading program */
  OGLProgram* shader_;
  /** Focal length */
  T focal_;
};

}  // namespace LTS5
#endif  // __LTS5_CUDA_MM_RENDERER_UTILS__
