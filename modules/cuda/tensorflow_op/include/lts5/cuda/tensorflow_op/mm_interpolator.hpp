/**
 *  @file   lts5/cuda/tensorflow_op/mm_interpolator.hpp
 *  @brief  Morphable model Interpolator class. Compute pixel's barycentric
 *          coordinates on the image
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   5/3/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_CUDA_MM_INTERPOLATOR__
#define __LTS5_CUDA_MM_INTERPOLATOR__

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor.h"

#include "lts5/tensorflow_op/mm_interpolator.hpp"

using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {


/**
 * @class   MMShuffleOglBuffer
 * @brief   Reorganise mesh from a shared topology to a splitted one
 * @author  Christophe Ecabert
 * @date    03/05/18
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class MMShuffleOglBuffer<GPUDevice, T> {
 public:

  /** Tensor type */
  using Tensor = tensorflow::Tensor;

  /**
   * @name  operator()
   * @fn    int operator()(tensorflow::OpKernelContext* ctx, const Tensor* buffer,
                 const Tensor* triangle, T* ogl_buffer)
   * @brief Reorganise a buffer from a shared topology to a splitted one based
   *        on a triangulation
   * @param[in] ctx Op context
   * @param[in] buffer
   * @param[in] triangle
   * @param[out] ogl_buffer
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const Tensor* buffer,
                 const Tensor* triangle,
                 T* ogl_buffer);
};

}  // namespace Functor

/**
 * @class   MMInterpShader
 * @brief   Shading program for interpolation - CPU Specialization
 * @author  Christophe Ecabert
 * @date    03/05/18
 * @ingroup tensforflow_op
 * @tparam T    Data type
 */
template<typename T>
class MMInterpShader<GPUDevice, T> {
 public:
  /**
   * @name  MMInterpShader
   * @fn    MMInterpShader(const MMRendererParameters<T>& p,
                           const bool& with_rigid)
   * @brief Constructor
   * @param[in] p Rendering parameters
   * @param[in] with_rigid  Flag to add the rigid transform in the shader
   */
  explicit MMInterpShader(const MMRendererParameters<T>& p,
                          const bool& with_rigid);

  /**
   * @name  ~MMInterpShader
   * @fn    ~MMInterpShader() override = default
   * @brief Destructor
   */
  ~MMInterpShader();

  /**
   * @name  UpdateModelTransform
   * @fn    int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                                     const tensorflow::Tensor& parameter,
                                     const tensorflow::int64& idx)
   * @brief Update model transformation matrix from camera parameters
   * @param[in] ctx Op context
   * @param[in] parameter Morphable model parameters
   * @param[in] idx Index where camera parameters start
   * @return    -1 if error, 0 otherwise
   */
  int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                           const tensorflow::Tensor& parameter,
                           const tensorflow::int64& idx);

  /**
   * @name  get_shader
   * @fn    const OGLProgram& get_shader() const
   * @brief Give shading program
   * @return    Shader
   */
  const OGLProgram& get_shader() const {
    return *shader_;
  }

  /**
   * @name  get_shader
   * @fn    OGLProgram& get_shader()
   * @brief Give shading program
   * @return    Shader
   */
  OGLProgram& get_shader() {
    return *shader_;
  }

 private:
  /** Shader */
  OGLProgram* shader_;
  /** UBO */
  UniformBufferObject* ubo_;
  /** Cuda resource */
  cudaGraphicsResource* resource_;
  /** Focal length */
  T focal_;
  /** Flag */
  bool has_rigid_;
};
}  // namespace LTS5
#endif  // __LTS5_CUDA_MM_INTERPOLATOR__
