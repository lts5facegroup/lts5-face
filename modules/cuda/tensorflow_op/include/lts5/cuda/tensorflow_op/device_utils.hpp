/**
 *  @file   "lts5/cuda/tensorflow_op/device_utils.hpp"
 *  @brief  Helper tools for handling various device
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   03/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_CUDA_DEVICE_UTILS__
#define __LTS5_CUDA_DEVICE_UTILS__

#define EIGEN_USE_GPU
#include "tensorflow/core/framework/register_types.h"

#include "lts5/tensorflow_op/device_utils.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      Helper function
 */
namespace Functor {

/** GPU Device Abstraction */
using GPUDevice = Eigen::GpuDevice;

template<>
struct DeviceHelper<GPUDevice> {

  /*
   * @name  Synchronize
   * @fn    static void Synchronize(const GPUDevice& d)
   * @param[in] d   Device to synchronize
   * @brief Synchronize the device
   */
  static void Synchronize(const GPUDevice& d);

  /*
   * @name  Ok
   * @fn    static bool Ok(const GPUDevice& d)
   * @brief Check if everything's godd for a given device
   * @param[in] d   Device to check
   * @return    True if Ok, False otherwise
   */
  static bool Ok(const GPUDevice& d);
};

}  // namepsace Functor
}  // namespace LTS5

#endif  // __LTS5_CUDA_DEVICE_UTILS__