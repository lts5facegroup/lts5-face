/**
 *  @file   "lts5/cuda/tensorflow_op/linear_algebra_functor.hpp"
 *  @brief  Abstraction layer for tensorflow linear algebra function
 *  @ingroup    cuda
 *
 *  @author Christophe Ecabert
 *  @date   18/12/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_CUDA_LINEAR_ALGEBRA_FUNCTOR__
#define __LTS5_TF_CUDA_LINEAR_ALGEBRA_FUNCTOR__



#define EIGEN_USE_GPU
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"


#include "lts5/utils/library_export.hpp"
#include "lts5/tensorflow_op/linear_algebra_functor.hpp"


using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {



/**
 * @class   LinearAlgebra
 * @brief   Abstraction layer for tensorflow linear algebra function
 *          Partial specialization for GPU
 * @author  Christophe Ecabert
 * @date    18/12/2017
 * @ingroup tensorflow_op
 * @tparam Device   Device on which to run computation
 * @tparam T        Data type
 */
template<typename T>
class LinearAlgebra<GPUDevice, T> {
 public:

#pragma mark -
#pragma mark Type Definition

  /** Op Kernel Context */
  using Context = tensorflow::OpKernelContext;
  /** Tensor type */
  using Tensor = tensorflow::Tensor;
  /** Tensorflow tensor mapped as const matrix */
  using TFConstMatrix = typename tensorflow::TTypes<T>::ConstMatrix;
  /** Tensorflow tensor mapped as const flat array */
  using TFConstFlat = typename tensorflow::TTypes<T>::ConstFlat;
  /** Tensorflow tensor mapped as const unaligned flat array */
  using TFConstUnFlat = typename tensorflow::TTypes<T>::UnalignedConstFlat;
  /** Tensorflow tensor mapped as flat array */
  using TFFlat = typename tensorflow::TTypes<T>::Flat;
  /** Tensorflow tensor mapped as unaligned flat array */
  using TFUnFlat = typename tensorflow::TTypes<T>::UnalignedFlat;

  /**
   * @name  Gemv
   * @fn    static void Gemv(Context* ctx,
                             const Tensor& A,
                             const bool& trans,
                             const T& alpha,
                             const Tensor& x,
                             const T& beta,
                             Tensor* y)
   * @brief Multiplies a matrix by a vector
   *          Y = a * AX + b Y
   *        ASSUME Outputs is already allocated with proper dimensions
   * @param[in] ctx     Kernel Op context
   * @param[in] A       Tensor A
   * @param[in] trans   Flag indicating transpose
   * @param[in] alpha   Scaling factor
   * @param[in] x       Vector X
   * @param[in] beta    Scaling factor
   * @param[in,out] y   Output vector
   */
  static void Gemv(Context* ctx,
                   const Tensor& A,
                   const bool& trans,
                   const T& alpha,
                   const Tensor& x,
                   const T& beta,
                   Tensor* y);

  /**
   * @name  Gemv
   * @fn    static void Gemv(Context* ctx,
                             TFConstMatrix& A,
                             const bool& trans,
                             const T& alpha,
                             TFConstFlat& x,
                             const T& beta,
                             TFFlat* y)
   * @brief Multiplies a matrix by a vector
   *          Y = a * AX + b Y
   *        ASSUME Outputs is already allocated with proper dimensions
   * @param[in] ctx     Kernel Op context
   * @param[in] A       Tensor A
   * @param[in] trans   Flag indicating transpose
   * @param[in] alpha   Scaling factor
   * @param[in] x       Vector X
   * @param[in] beta    Scaling factor
   * @param[in,out] y   Output vector
   */
  static void Gemv(Context* ctx,
                   TFConstMatrix& A,
                   const bool& trans,
                   const T& alpha,
                   TFConstFlat& x,
                   const T& beta,
                   TFFlat* y);

  /**
   * @name  Gemv
   * @fn    static void Gemv(Context* ctx,
                             TFConstMatrix& A,
                             const bool& trans,
                             const T& alpha,
                             TFConstUnFlat& x,
                             const T& beta,
                             TFUnFlat* y)
   * @brief Multiplies a matrix by a vector
   *          Y = a * AX + b Y
   *        ASSUME Outputs is already allocated with proper dimensions
   * @param[in] ctx     Kernel Op context
   * @param[in] A       Tensor A
   * @param[in] trans   Flag indicating transpose
   * @param[in] alpha   Scaling factor
   * @param[in] x       Vector X
   * @param[in] beta    Scaling factor
   * @param[in,out] y   Output vector
   */
  static void Gemv(Context* ctx,
                   TFConstMatrix& A,
                   const bool& trans,
                   const T& alpha,
                   TFConstUnFlat& x,
                   const T& beta,
                   TFUnFlat* y);

  /**
   * @name  Gemm
   * @fn    static void Gemm(Context* ctx,
                   const Tensor& A,
                   const bool& trans_a,
                   const T& alpha,
                   const Tensor& B,
                   const bool& trans_b,
                   const T& beta,
                   Tensor* C)
   * @brief Compute the product between two matrices
   *          C = a * AB + b * C
   *        ASSUME Outputs is already allocated with proper dimensions
   * @param[in] ctx     Kernel Op context
   * @param[in] A       Tensor A
   * @param[in] trans_a Transpose of A
   * @param[in] alpha   Scaling factor
   * @param[in] B       Tensor B
   * @param[in] trans_b Transpose of A
   * @param[in] beta    Scaling factor
   * @param[in,out] C
   */
  static void Gemm(Context* ctx,
                   const Tensor& A,
                   const bool& trans_a,
                   const T& alpha,
                   const Tensor& B,
                   const bool& trans_b,
                   const T& beta,
                   Tensor* C);

  /**
 * @name  Gemm
 * @brief Compute the product between two matrices
 *          C = a * AB + b * C
 *        ASSUME Outputs is already allocated with proper dimensions
 * @param[in] ctx     Kernel Op context
 * @param[in] a_ptr   Pointer to tensor A data
 * @param[in] a_row   Number of rows in tensor A
 * @param[in] a_col   Number of cols in tensor A
 * @param[in] a_stride Stride of tensor A
 * @param[in] trans_a Transpose of A
 * @param[in] alpha   Scaling factor
 * @param[in] b_ptr   Pointer to tensor B data
 * @param[in] b_row   Number of rows in tensor B
 * @param[in] b_col   Number of cols in tensor B
 * @param[in] b_stride Stride of tensor B
 * @param[in] trans_b Transpose of B
 * @param[in] beta    Scaling factor
 * @param[in,out] C   Output matrix
 */
  static void Gemm(Context* ctx,
                   const T* a_ptr,
                   const uint64_t& a_row,
                   const uint64_t& a_col,
                   const uint64_t& a_stride,
                   const bool& trans_a,
                   const T& alpha,
                   const T* b_ptr,
                   const uint64_t& b_row,
                   const uint64_t& b_col,
                   const uint64_t& b_stride,
                   const bool& trans_b,
                   const T& beta,
                   Tensor* C);
};

}  // namepsace Functor
}  // namespace LTS5
#endif //__LTS5_TF_CUDA_LINEAR_ALGEBRA_FUNCTOR__
