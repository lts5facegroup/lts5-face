/**
 *  @file   cuda/utils/separable_filter.hpp
 *  @brief  Separable Filter engine for GPU
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   13/10/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_SEPARABLE_FILTER__
#define __LTS5_SEPARABLE_FILTER__

#include "opencv2/core/cuda.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 * @class   SeparableFilter
 * @brief   Separable Filter engine for GPU
 * @author  Christophe Ecabert
 * @date    13/10/2016
 * @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS SeparableFilter {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  SeparableFilter
   * @fn    SeparableFilter(void)
   * @brief Constructor
   */
  SeparableFilter(void);

  /**
   * @name  SeparableFilter
   * @fn    SeparableFilter(const T* kernel,
                            const int size,
                            const cv::cuda::Stream& stream)
   * @brief Constructor
   * @param[in] kernel  Kerne data
   * @param[in] size    Kernel size
   * @param[in] stream  Stream to run on
   */
  SeparableFilter(const T* kernel,
                  const int size,
                  const cv::cuda::Stream& stream);

  /**
   * @name  SeparableFilter
   * @fn    SeparableFilter(const SeparableFilter& other)
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  SeparableFilter(const SeparableFilter& other);

  /**
   * @name  operator=
   * @fn    SeparableFilter& operator=(const SeparableFilter& rhs)
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  SeparableFilter& operator=(const SeparableFilter& rhs);

  /**
   * @name  ~SeparableFilter
   * @fn    ~SeparableFilter(void)
   * @brief Destructor
   */
  ~SeparableFilter(void);

#pragma mark -
#pragma mark Usage

  /**
   * @name  SetKernel
   * @fn    void SetKernel(const T* kernel,
                           const int size,
                           const cv::cuda::Stream& stream,)
   * @brief Load kernel into GPU (symbol constant memory)
   * @param[in] kernel  Kerne data
   * @param[in] size    Kernel size
   * @param[in] stream  Cuda stream to use to run operation
   */
  void SetKernel(const T *kernel, const int size, const cv::cuda::Stream& stream);

  /**
   * @name  Apply
   * @fn    void Apply(const cv::cuda::GpuMat& source,
                       const cv::cuda::Stream& stream,
                       cv::cuda::GpuMat* dest)
   * @brief Apply filter
   * @param[in] source  Image to filter (Dimension must be a multiple of 64)
   * @param[in] stream  Stream on where to launch the computation
   * @param[out] dest    Filtered image
   */
  void Apply(const cv::cuda::GpuMat& source,
             const cv::cuda::Stream& stream,
             cv::cuda::GpuMat* dest);

  /**
   * @name  Apply
   * @fn    void Apply(const cv::cuda::GpuMat& source,
                       const cv::cuda::Stream& stream,
                       cv::cuda::GpuMat* buffer,
                       cv::cuda::GpuMat* dest)
   * @brief Apply filter
   * @param[in] source  Image to filter (Dimension must be a multiple of 64)
   * @param[in] stream  Stream on where to launch the computation
   * @param[in] buffer  Temporary buffer where to store partia
   * @param[out] dest    Filtered image
   */
  void Apply(const cv::cuda::GpuMat& source,
             const cv::cuda::Stream& stream,
             cv::cuda::GpuMat* buffer,
             cv::cuda::GpuMat* dest);

  /**
   * @name  ApplyRow
   * @fn    void ApplyRow(const cv::cuda::GpuMat& source,
                          const cv::cuda::Stream& stream,
                          cv::cuda::GpuMat* dest)
   * @brief Apply filter on the row
   * @param[in] source  Image to filter (Dimension must be a multiple of 64)
   * @param[in] stream  Stream on where to launch the computation
   * @param[out] dest    Filtered image
   */
  void ApplyRow(const cv::cuda::GpuMat& source,
                const cv::cuda::Stream& stream,
                cv::cuda::GpuMat* dest);

  /**
   * @name  ApplyRowBatched
   * @fn    void ApplyRowBatched(const cv::cuda::GpuMat* sources,
                                 const size_t n_batch,
                                 const size_t batch_size,
                                 const cv::cuda::Stream& stream,
                                 cv::cuda::GpuMat* dest)
   * @brief Apply row filter on every images
   * @param[in] sources     Array of images to filter
   * @param[in] n_batch     Current batch index
   * @param[in] batch_size  Batch size, number of image to filter
   * @param[in] stream      Stream on which to run computation
   * @param[in,out] dest    Array of filtered images
   */
  void ApplyRowBatched(const cv::cuda::GpuMat* sources,
                       const size_t n_batch,
                       const size_t batch_size,
                       const cv::cuda::Stream& stream,
                       cv::cuda::GpuMat* dest);

  /**
   * @name  ApplyColumn
   * @fn    void ApplyColumn(const cv::cuda::GpuMat& source,
                             const cv::cuda::Stream& stream,
                             cv::cuda::GpuMat* dest)
   * @brief Apply filter on the column
   * @param[in] source  Image to filter (Dimension must be a multiple of 64)
   * @param[in] stream  Stream on where to launch the computation
   * @param[out] dest    Filtered image
   */
  void ApplyColumn(const cv::cuda::GpuMat& source,
                   const cv::cuda::Stream& stream,
                   cv::cuda::GpuMat* dest);

  /**
   * @name  ApplyColumnBatched
   * @fn    void ApplyColumnBatched(const cv::cuda::GpuMat* sources,
                                 const size_t n_batch,
                                 const size_t batch_size,
                                 const cv::cuda::Stream& stream,
                                 cv::cuda::GpuMat* dest)
   * @brief Apply row filter on every images
   * @param[in] sources     Array of images to filter
   * @param[in] n_batch     Current batch index
   * @param[in] batch_size  Batch size, number of image to filter
   * @param[in] stream      Stream on which to run computation
   * @param[in,out] dest    Array of filtered images
   */
  void ApplyColumnBatched(const cv::cuda::GpuMat* sources,
                          const size_t n_batch,
                          const size_t batch_size,
                          const cv::cuda::Stream& stream,
                          cv::cuda::GpuMat* dest);

#pragma mark -
#pragma mark Private

 private:
  /** Kernel dimension */
  int kernel_size_;
  /** Kernel radius */
  int kernel_radius_;
  /** Filter buffer */
  cv::cuda::GpuMat buffer_;

};

}  // namepsace CUDA
}  // namepsace LTS5
#endif //__LTS5_SEPARABLE_FILTER__
