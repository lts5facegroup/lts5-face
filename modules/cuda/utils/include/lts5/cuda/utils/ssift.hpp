/**
 *  @file   cuda/utils/ssift.hpp
 *  @brief  Compute SIFT descriptor on GPU
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   04/05/17
 *  Copyright (c) 2017 Ecabert Christophe. All rights reserved.
 */

#ifndef __CUDA_SSIFT__
#define __CUDA_SSIFT__

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/ssift.hpp"
#include "lts5/cuda/utils/matrix.hpp"
#include "lts5/cuda/utils/separable_filter.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 *  @class  GPUSSift
 *  @brief  Simple SIFT descriptor w/o scale space and orientation
 *  @author Unknown
 *  @date   04/05/17
 *  @ingroup utils
 */
class LTS5_EXPORTS GPUSSift {
 public:

  /**
   * @struct    KernelParameter
   * @brief   SSiftKernelParameter
   */
  struct KernelParameter {
    /** Bin per rad */
    float bin_per_rad;
    /** Exp scale */
    float exp_scale;
    /** Sift descriptor width */
    int sift_descr_width;
    /** */
    int sift_descr_hist_bins;
    /** Sift length */
    int sift_dim;
    /** Magnitude threshold */
    float sift_descr_mag_thr;
    /** scaling ? */
    float sift_int_descr_fctr;
    /** Histrogram width */
    float hist_width;
    /** Histogram length */
    int hist_length;
    /** Radius */
    int radius;
    /** Patch top left corner - X */
    int patch_x;
    /** Patch top left corner - Y */
    int patch_y;
    /** Patch center X, sampling location */
    int patch_center_x;
    /** Patch center Y, sampling location */
    int patch_center_y;
    /** Patch size */
    int patch_size;
    /** Root sift */
    bool sift_root;
  };

  /**
   * @class GPUCache
   * @brief Cache for GPU ssift computation
   * @author Christophe Ecabert
   * @ingroup utils
   * @date  04/05/17
   */
  class GPUCache {
   public:
    /**
     * @name    GPUCache
     * @fn  GPUCache(void);
     * @brief   Constructor
     */
    GPUCache(void);

    /**
     * @name    GPUCache
     * @fn  GPUCache(const size_t& n_stream, const size_t& n_pts)
     * @param[in] n_stream  Number of stream in the cache
     * @param[in] n_pts     Number of points
     */
    GPUCache(const size_t& n_stream, const size_t& n_pts);

    /**
     * @name    ~GPUCache
     * @fn  ~GPUCache(void);
     * @brief   Destructor
     */
    ~GPUCache(void);

    /**
     * @name    Create
     * @fn      void Create(const size_t& n_stream, const size_t& n_pts)
     * @brief   Create cache if already init with proper dimension then do
     *          nothing
     * @param[in] n_stream  Number of stream in the cache
     * @param[in] n_pts     Number of points
     */
    void Create(const size_t& n_stream, const size_t& n_pts);

    /**
     * @name    WaitForCompletion
     * @fn  void WaitForCompletion(void);
     * @brief   Wait till all stream have complete their job
     */
    void WaitForCompletion(void);

    /** Stream on which to run extraction */
    std::vector<cv::cuda::Stream> streams_;
    /** Low pass filter */
    std::vector<LTS5::CUDA::SeparableFilter<float>> filters_;
    /** Intermediate filter buffer */
    std::vector<cv::cuda::GpuMat> buffers_;
    /** Extracted image patch */
    std::vector<cv::cuda::GpuMat> img_patches_32f_;
    /** Filtered image patch */
    std::vector<cv::cuda::GpuMat> filter_patches_;
    /** Kernel parameters */
    std::vector<GPUSSift::KernelParameter> params_;
    /** ROI */
    std::vector<cv::Rect> patch_;
    /** Number of streams */
    size_t n_stream_;
    /** Number of points */
    size_t n_pts_;
    /** Flag marking if filters have been initualized */
    bool filters_init_;
  };

  /**
   *  @name   ComputeDescriptor
   *  @fn static void ComputeDescriptor(const cv::cuda::GpuMat& image,
                               const std::vector<cv::KeyPoint>& key_points,
                               const SSift::SSiftParameters& sift_param,
                               const GPUCache* cache,
                               Matrix<float>* sift_features)
   *  @brief  Extract SSfit descriptor at location provided by key_points
   *  @param[in]  image           Input images
   *  @param[in]  key_points      Location of sift patches
   *  @param[in]  sift_param      Parameters for sift extraction
   *  @param[in,out] cache        Cache buffer for sift computation
   *  @param[out] sift_features   Extracted descriptor
   *  @return -1 if error, 0 otherwise
   */
  static int ComputeDescriptor(const cv::cuda::GpuMat& image,
                               const std::vector<cv::KeyPoint>& key_points,
                               const SSift::SSiftParameters& sift_param,
                               const GPUCache* cache,
                               Matrix<float>* sift_features);
};
}  // namespace CUDA
}  // namespace LTS5


#endif /* __CUDA_SSIFT__ */
