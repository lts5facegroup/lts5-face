/**
 *  @file   cuda/utils/tensor_workspace.hpp
 *  @brief  Helper class for tensor workspace
 *  @ingroup    utils
 *
 *  @author Christophe Ecabert
 *  @date   23/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __CUDA_TENSOR_WORKSPACE__
#define __CUDA_TENSOR_WORKSPACE__

#include <atomic>

#include "lts5/utils/library_export.hpp"
#include "lts5/cuda/utils/tensor.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/** Tensor reduction operation */
template<typename T>
struct TensorRedOp;
/** Tensor convolution operation */
template<typename T>
struct TensorConvOp;

/**
 * @class   TensorWorkspace
 * @brief   Helper class for tensor workspace
 * @author  Christophe Ecabert
 * @date    23/06/2017
 * @ingroup utils
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS TensorWorkspace {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  TensorWorkspace
   * @fn    TensorWorkspace(void)
   * @brief Constructor
   */
  TensorWorkspace(void);

  /**
   * @name  TensorWorkspace
   * @fn    TensorWorkspace(const size_t size)
   * @brief Constructor
   * @param[in] size    Workspace dimension
   */
  TensorWorkspace(const size_t size);

  /**
   * @name  TensorWorkspace
   * @fn    TensorWorkspace(const TensorWorkspace<T>& other)
   * @brief Copy Constructor
   * @param[in] other   Object to copy from
   */
  TensorWorkspace(const TensorWorkspace<T>& other);

  /**
   * @name  operator=
   * @fn    TensorWorkspace<T>& operator=(const TensorWorkspace<T>& rhs)
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  TensorWorkspace<T>& operator=(const TensorWorkspace<T>& rhs);

  /**
   * @name  ~TensorWorkspace
   * @fn    ~TensorWorkspace(void)
   * @brief Destructor
   */
  ~TensorWorkspace(void);

  /**
   * @name  Create
   * @fn    void Create(const size_t size)
   * @brief Create workspace of a given size
   * @param[in] size    Workspace size in bytes needed.
   */
  void Create(const size_t size);

#pragma mark -
#pragma mark Usage

  /**
   * @name
   * @fn    static void ReductionSize(const TensorRedOp<T>& red,
                                      const Tensor<T>& A,
                                      const Tensor<T>& B,
                                      size_t* size);
   * @brief Compute the workspace size needed to perform a given reduction
   *        beteween tensor A and B.
   * @param[in] red Reduction operation
   * @param[in] A   Tensor to reduce
   * @param[in] B   Resulting tensor
   * @param[out] size   Computed workspace dimension in bytes
   */
  static void ReductionSize(const TensorRedOp<T>& red,
                            const Tensor<T>& A,
                            const Tensor<T>& B,
                            size_t* size);

  /**
   * @name  ConvolutionSize
   * @fn    static void ConvolutionSize(const Tensor<T>& input,
                                        const Tensor<T>& output,
                                        TensorConvOp<T>* conv,
                                        size_t* size)
   * @brief Finalize convolution operation and compute workspace dimension
   *        required to perform operation
   * @param[in] input       Input data to be filtered
   * @param[in] output      Output filtered data
   * @param[in,out] conv    Convolution operation to perform
   * @param[out] size       Required workspace dimension
   */
  static void ConvolutionSize(const Tensor<T>& input,
                              const Tensor<T>& output,
                              TensorConvOp<T>* conv,
                              size_t* size);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  buffer
   * @fn    T* buffer(void) const
   * @brief Proviate access to workspace buffer
   * @return    buffer
   */
  T* buffer(void) const {
    return buffer_;
  }

  /**
   * @name  size
   * @fn    const size_t& size() const
   * @brief Provide workspace dimension in bytes
   */
  const size_t& size() const {
    return size_;
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   *  @name Retain
   *  @fn void Retain(void)
   *  @brief  Increase reference counter
   *  @see https://stackoverflow.com/questions/10268737/
   */
  void Retain(void);

  /**
   *  @name Release
   *  @fn void Release(void)
   *  @brief  Release object memory if reference counter reach zero
   *  @see https://stackoverflow.com/questions/10268737/
   */
  void Release(void);

  /** Buffer */
  T* buffer_;
  /** Size */
  size_t size_;
  /** Reference counter */
  std::atomic<unsigned>* ref_;
};

}  // namepsace CUDA
}  // namepsace LTS5

#endif //__CUDA_TENSOR_WORKSPACE__
