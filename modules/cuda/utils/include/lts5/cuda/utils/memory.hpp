/**
 *  @file   lts5/cuda/utils/memory.hpp
 *  @brief  Memory blob located on the device
 *  @ingroup utils
 *
 *  @date   2/11/20
 */

/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2011, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *  Author: Anatoly Baskeheev, Itseez Ltd, (myname.mysurname@mycompany.com)
 */

#ifndef __LTS5_CUDA_MEMORY__
#define __LTS5_CUDA_MEMORY__

#include "lts5/utils/sys/refcount.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#if defined(__CUDACC__)
#define __LTS5_HOST_DEVICE__ __host__ __device__ __forceinline__
#else
#define __LTS5_HOST_DEVICE__
#endif

/**
 * @struct  DevPtr
 * @brief   Light weight struct to pass `MemoryBlob` to device kernel function
 * @date    12/02/2020
 * @ingroup utils
 * @tparam T    Data type
 */
template<typename T>
struct DevPtr {
  /** Data type */
  using Type = T;
  /** Size in bytes of single elements */
  static constexpr size_t elem_size = sizeof(T);
  /** Pointer to device/host buffer */
  T* data;

  /**
   * @name  DevPtr
   * @brief Default constructor
   */
  __LTS5_HOST_DEVICE__ DevPtr() : data(nullptr) {}

  /**
   * @name  DevPtr
   * @brief Default constructor
   * @param[in] array Pointer to device/host array
   */
  __LTS5_HOST_DEVICE__ DevPtr(T* array) : data(array) {}

  /**
   * @name  ElemSize
   * @brief Provide the size in bytes of single element in the array
   * @return    Number of bytes for one element
   */
  __LTS5_HOST_DEVICE__ size_t ElemSize() const { return elem_size; }
};

template<typename T>
struct PtrSz : public DevPtr<T> {
  /** Number of element */
  size_t n_element;

  /**
   * @name  PtrSz
   * @brief Default constructor
   */
  __LTS5_HOST_DEVICE__ PtrSz() : n_element(0) {}

  /**
   * @name  PtrSz
   * @brief Default constructor
   * @param[in] array       Pointer to device/host array
   * @param[in] n_element   Number of element in the array
   */
  __LTS5_HOST_DEVICE__ PtrSz(T* array, size_t n_element) :
          DevPtr<T>(array),
          n_element(n_element) {}
};

/**
 * @class   MemoryBlob
 * @brief   Reference counter memory blob located on GPU
 * @author  Christophe Ecabert
 * @date    11/02/2020
 * @ingroup utils
 */
class MemoryBlob : public RefCounted {
 public:
#pragma mark -
#pragma mark Initialization
  /**
   * @name  MemoryBlob
   * @fn    MemoryBlob()
   * @brief Constructor
   */
  MemoryBlob();

  /**
   * @name  MemoryBlob
   * @fn    explicit MemoryBlob(const size_t& n_bytes)
   * @brief Constructor
   * @param[in] n_bytes Number of bytes to allocate on the device
   */
  explicit MemoryBlob(const size_t& n_bytes);

  /**
   * @name  ~MemoryBlob
   * @fn    ~MemoryBlob() override
   * @brief Destructor
   */
  ~MemoryBlob() override;

  /**
   * @name  MemoryBlob
   * @fn    MemoryBlob(const MemoryBlob& other)
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  MemoryBlob(const MemoryBlob& other);

  /**
   * @name  operator=
   * @fn    MemoryBlob& operator=(const MemoryBlob& rhs)
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return Newly assigned object
   */
  MemoryBlob& operator=(const MemoryBlob& rhs);

  /**
   * @name  Create
   * @fn    void Create(const size_t& n_bytes)
   * @brief Create a memory blob for a given size if needed.
   * @param[in] n_bytes Number of bytes to allocate
   */
  void Create(const size_t& n_bytes);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Upload
   * @fn    void Upload(const void* data, const size_t& n_bytes)
   * @brief Copy data from host to the device
   * @param[in] data    Host array to copy to device
   * @param[in] n_bytes Number of bytes to copy to the device
   */
  void Upload(const void* data, const size_t& n_bytes);

  /**
   * @name  Download
   * @fn    void Download(void* data) const
   * @brief Copy data from device to host
   * @param[in] data Pointer where to copy the data. Must have proper
   *                 dimensions to accept the whole memory blob
   */
  void Download(void* data) const;

  /**
   * @name  Ptr
   * @fn    T* Ptr()
   * @brief Return pointer to internal device memory blob.
   * @return    Casted pointer
   * @tparam T  Casted type (static)
   */
  template<typename T>
  T* Ptr();

  /**
   * @name  Ptr
   * @fn    const T* Ptr() const
   * @brief Return constant pointer to internal device memory blob.
   * @return    Casted pointer
   * @tparam T  Casted type (static)
   */
  template<typename T>
  const T* Ptr() const;

  /**
   * @name  operator PtrSz<U>
   * @brief Conversion to PtrSz for passing to kernel functions
   * @tparam U  Data type
   * @return    PtrSz struct
   */
  template<typename U> operator PtrSz<U>() const;

  /**
   * @name  Empty
   * @fn    bool Empty() const
   * @brief Indicate if buffer is allocated
   * @return  Returns true if unallocated otherwise false.
   */
  bool Empty() const;

  /**
   * @name  Size
   * @fn    std::size_t Size() const
   * @brief Give the size in bytes of the underlying memory blob
   * @return    Allocated size in bytes
   */
  std::size_t Size() const;

  /**
   * @name  Size
   * @fn    std::size_t Size() const
   * @brief Give the size of the buffer for a given data type
   * @return    Buffer size
   */
  template<typename T>
  std::size_t Size() const;

 private:
#pragma mark -
#pragma mark Private
  /** Device pointer */
  void* data_;
  /** Size */
  size_t n_bytes_;
};

#pragma mark -
#pragma mark Inlined methods

template<typename T>
inline T* MemoryBlob::Ptr() { return static_cast<T*>(data_); }

template<typename T>
inline const T* MemoryBlob::Ptr() const { return static_cast<const T*>(data_); }

template<typename T>
inline std::size_t MemoryBlob::Size() const {return n_bytes_ / sizeof(T); }

template <class U> inline MemoryBlob::operator PtrSz<U>() const {
  return PtrSz<U>((U*)Ptr<U>(), n_bytes_ / sizeof(U));
}


}  // namespace CUDA
}  // namespace LTS5

#endif  // __LTS5_CUDA_MEMORY__
