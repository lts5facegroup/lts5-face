/**
 *  @file   cuda/utils/tensor_operation.hpp
 *  @brief  Various operation on tensor
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   22/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __CUDA_TENSOR_OPERATION__
#define __CUDA_TENSOR_OPERATION__

#include "lts5/utils/library_export.hpp"
#include "lts5/cuda/utils/tensor.hpp"

/** Tensor Operation implementation */
struct cudnnOpTensorStruct;
/** Tensor reduction */
struct cudnnReduceTensorStruct;
/** Filter descriptor */
struct cudnnFilterStruct;
/** Convolution descriptor */
struct cudnnConvolutionStruct;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Types definition

/**
 * @enum TensorOpType
 * @brief Possible element-wise operation on tensor
 * @ingroup utils
 */
enum TensorOpType {
  /** Addition */
  kOpAdd,
  /** Multiplication */
  kOpMul,
  /** Square root*/
  kOpSqrt
};

/**
 * @enum TensorRedType
 * @brief Type of tensor reduction to perform
 * @ingroup utils
 */
enum TensorRedType {
  /** Addition */
  kRedAdd,
  /** Multiplication */
  kRedMul,
  /** Average */
  kRedAvg,
  /** L1 Norm */
  kRedL1Norm,
  /** L2 Norm */
  kRedL2Norm
};

/**
 * @enum    TensorConvType
 * @brief   Type of convolution
 * @ingroup utils
 */
enum TensorConvType {
  /** Convolution */
  kConvolution,
  /** Cross-correlation */
  kCrossCorrelation
};

/**
 * @struct ConvParameter
 * @brief   Settings for convolution
 * @ingroup utils
 */
struct LTS5_EXPORTS ConvParameter {
  /** Convolution type */
  TensorConvType type;
  /** Vertical zero-padding : number of rows of zeros implicitly concatenated
        onto the top and onto the bottom of input */
  int pad_v;
  /** Horizontal zero-padding : number of columns of zeros implicitly concatenated
        onto the left and onto the right of input */
  int pad_h;
  /** Vertical filter stride */
  int stride_v;
  /** Horizonzal filter stride */
  int stride_h;
  /** Filter vertical dilation, >= 1 */
  int dilation_v;
  /** Filter horizontal dilation, >= 1 */
  int dilation_h;
};

#pragma mark -
#pragma mark Tensor Operation

/**
 * @struct  TensorOp
 * @brief   Encapsulate cuDNN tensor element-wise operation such as Addition,
 *          multiplication
 * @author  Christophe Ecabert
 * @date    22/06/2017
 * @ingroup utils
 * @tparam  T   Data type
 */
template<typename T>
struct LTS5_EXPORTS TensorOp {

  /**
   * @name  TensorOp
   * @fn    TensorOp(const TensorOpType& type)
   * @brief   Constructor
   * @param[in] type Type of tensor operation
   */
  TensorOp(const TensorOpType& type);

  /**
   * @name    ~TensorOp
   * @fn  ~TensorOp(void)
   * @brief   Destructor
   */
  ~TensorOp(void);

  /** Operation to perform */
  cudnnOpTensorStruct* op;
};

#pragma mark -
#pragma mark Tensor Reduction Operation

/**
 * @struct  TensorRedOp
 * @brief   Encapsulate cuDNN tensor reduction operation such as Addition,
 * @author  Christophe Ecabert
 * @date    23/06/2017
 * @ingroup utils
 * @tparam T    Data type
 */
template<typename T>
struct LTS5_EXPORTS TensorRedOp {
  /**
   * @name  TensorRedOp
   * @fn    TensorRedOp(const TensorRedType& type)
   * @brief   Constructor
   * @param[in] type Type of tensor reduction operation
   */
  TensorRedOp(const TensorRedType& type);

  /**
   * @name    ~TensorRedOp
   * @fn  ~TensorRedOp(void)
   * @brief   Destructor
   */
  ~TensorRedOp(void);

  /** Operation to perform */
  cudnnReduceTensorStruct* red;
};

#pragma mark -
#pragma mark Tensor Convolution Operation

/**
 * @struct  TensorConvOp
 * @brief   Convolution operation on tensor
 * @author  Christophe Ecabert
 * @date    26/06/2017
 * @ingroup utils
 * @tparam T    Data type
 */
template<typename T>
struct LTS5_EXPORTS TensorConvOp {

  /**
   * @name  TensorConvOp
   * @fn    TensorConvOp(void)
   * @brief Constructor
   */
  TensorConvOp(void);

  /**
   * @name  TensorConvOp
   * @fn    TensorConvOp(const Tensor<T>* filter,
                         const ConvParameter& parameter)
   * @brief Constructor
   * @param[in] filter  Filter to use for convolution (reference to filter
   *                    will be hold)
   * @param[in] parameter   Convolution parameters
   */
  TensorConvOp(const Tensor<T>* filter, const ConvParameter& parameter);

  /**
   * @name  ~TensorConvOp
   * @fn    ~TensorConvOp(void)
   * @brief Destructor
   */
  ~TensorConvOp(void);

  /**
   * @name  Create
   * @fn    void Create(const Tensor<T>* filter, const ConvParameter& parameter)
   * @brief Create/Init convolution operation
   * @param[in] filter      Filter to use for convolution (reference to filter
   *                        will be hold)
   * @param[in] parameter   Convolution parameters
   */
  void Create(const Tensor<T>* filter, const ConvParameter& parameter);

  /**
   * @name  GetOutputDimension
   * @fn    void GetOutputDimension(const Tensor<T>& data,
                                    int* depths,
                                    int* rows,
                                    int* cols) const
   * @brief Compute the dimension of the ouput tensor holding filtered data.
   * @param[in] data    Input's data to be filtered
   * @param[out] depths Output's depth
   * @param[out] rows   Output's rows
   * @param[out] cols   Output's cols
   */
  void GetOutputDimension(const Tensor<T>& data,
                          int* depths,
                          int* rows,
                          int* cols) const;

  /** Filter descriptor */
  cudnnFilterStruct* filter_desc;
  /** Convolution descriptor */
  cudnnConvolutionStruct* conv_desc;
  /** Filter */
  const Tensor<T>* filter;
  /** Convolution algorithm */
  int algo;
};

}  // namepsace CUDA
}  // namepsace LTS5

#endif //__CUDA_TENSOR_OPERATION__
