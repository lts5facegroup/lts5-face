/**
 *  @file   cuda/utils/tensor.hpp
 *  @brief  Tensor 4D abstraction for cudnn framework
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   22/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __CUDA_TENSOR__
#define __CUDA_TENSOR__

#include <atomic>

#include "opencv2/core/core.hpp"
#include "opencv2/core/cuda.hpp"

#include "lts5/utils/library_export.hpp"

/** cuDNN tensor descriptor */
struct cudnnTensorStruct;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

template<typename T>
struct TensorDescriptor;

/**
 * @class   Tensor
 * @brief   Tensor 4D abstraction for cudnn framework
 * @author  Christophe Ecabert
 * @date    22/06/2017
 * @tparam T    Data type
 * @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS Tensor {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  Tensor
   * @fn    Tensor()
   * @brief Constructor
   */
  Tensor();

  /**
   * @name  Tensor
   * @fn    explicit Tensor(const cv::Mat& data)
   * @brief Constructor with data to upload
   * @param data
   */
  explicit Tensor(const cv::Mat& data);

  /**
   * @name  Tensor
   * @fn  Tensor(const int& row, const int& col,
                 const int& depth, T* buffer)
   * @brief Constructor for data with external buffer.
   *        IMPORTANT take ownership of the data
   * @param[in] row     Number of rows
   * @param[in] col     Number of columns
   * @param[in] depth   Depth
   * @param[in] buffer  Device memory buffer allocated by user
   */
  Tensor(const int& row, const int& col, const int& depth, T* buffer);

  /**
   * @name  Tensor
   * @fn    Tensor(const Tensor<T>& other)
   * @brief Copy constructor, no deep copy.
   * @param[in] other   Object to copyfrom
   */
  Tensor(const Tensor<T>& other);

  /**
   * @name  operator=
   * @fn    Tensor<T>& operator=(const Tensor<T>& rhs)
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  Tensor<T>& operator=(const Tensor<T>& rhs);

  /**
   * @name  ~Tensor
   * @fn    ~Tensor()
   * @brief Destructor
   */
  ~Tensor();

  /**
   * @name  Create
   * @fn  void Create(const int& rows, const int& cols, const int& depth)
   * @brief Create tensor of a given dimensions
   * @param[in] rows     Number of rows
   * @param[in] cols     Number of columns
   * @param[in] depth   Tensor's depth
   */
  void Create(const int& rows, const int& cols, const int& depth);

  /**
   * @name  Clone
   * @fn    Tensor<T> Clone(const cv::cuda::Stream& stream) const
   * @brief Deep copy
   * @param[in] stream  Stream on which to run computation
   * @return    Deep copy of the object
   */
  Tensor<T> Clone(const cv::cuda::Stream& stream) const;

  /**
   * @name  Clone
   * @fn    Tensor<T> Clone() const
   * @brief Deep copy
   * @return    Deep copy of the object
   */
  Tensor<T> Clone() const;

  /**
   * @name  Upload
   * @fn    void Upload(const cv::Mat& data);
   * @brief upload data to device memory
   * @param[in] data    Data to push into tensor
   */
  void Upload(const cv::Mat& data);

  /**
   * @name  Upload
   * @fn    void Upload(const cv::Mat& data,  const cv::cuda::Stream& stream);
   * @brief upload data to device memory
   * @param[in] data    Data to push into tensor
   * @param[in] stream  Stream on which to launch the work
   */
  void Upload(const cv::Mat& data, const cv::cuda::Stream& stream);

  /**
   * @name  Download
   * @fn  void Download(cv::Mat* tensor) const
   * @brief Copy tensor from Device to Host
   * @param[out]  tensor  Host tensor where to store data
   */
  void Download(cv::Mat* tensor) const;

  /**
   * @name  Download
   * @fn  void Download(const cv::cuda::Stream& stream, cv::Mat* tensor) const
   * @brief Copy tensor from Device to Host
   * @param[in] stream  Stream on which to run computation
   * @param[out]  tensor  Host tensor where to store data
   */
  void Download(const cv::cuda::Stream& stream, cv::Mat* tensor) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  rows
   * @fn  int rows() const
   * @brief Provide number of rows
   * @return  Number of rows in the tensor
   */
  int rows() const {
    return rows_;
  }

  /**
   * @name  cols
   * @fn  int cols() const
   * @brief Provide number of columns
   * @return  Number of columns in the tensor
   */
  int cols() const {
    return cols_;
  }

  /**
   * @name  depth
   * @fn  int depth() const
   * @brief Provide depth
   * @return  Depth of the tensor
   */
  int depth() const {
    return depth_;
  }

  /**
   * @name  data
   * @fn  T* data() const
   * @brief Provide device data pointer
   * @return  Device data pointer
   */
  T* data() const {
    return data_;
  }

  /**
   * @name  descriptor
   * @fn    cudnnTensorStruct* descriptor()
   * @brief Provide tensor's descriptor access
   * @return    Tensor's descriptor
   */
  cudnnTensorStruct* descriptor() {
    return desc_;
  }

  /**
   * @name  descriptor
   * @fn    cudnnTensorStruct* descriptor() const
   * @brief Provide tensor's descriptor access
   * @return    Tensor's descriptor
   */
  cudnnTensorStruct* descriptor() const {
    return desc_;
  }

  /**
   * @name  empty
   * @fn  bool empty() const
   * @brief Indicate if matrix is empty or not
   * @return  True if empty, false otherwise
   */
  bool empty() const {
    return (data_ == nullptr);
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   *  @name Retain
   *  @fn void Retain()
   *  @brief  Increase reference counter
   *  @see https://stackoverflow.com/questions/10268737/
   */
  void Retain();

  /**
   *  @name Release
   *  @fn void Release()
   *  @brief  Release object memory if reference counter reach zero
   *  @see https://stackoverflow.com/questions/10268737/
   */
  void Release();

  /** Number of rows */
  int rows_;
  /** Number of cols */
  int cols_;
  /** Depth */
  int depth_;
  /** Tensor descriptor */
  cudnnTensorStruct* desc_;
  /** data buffer */
  T* data_;
  /** Reference counter */
  std::atomic<unsigned>* ref_;

};
}  // namepsace CUDA
}  // namepsace LTS5

#endif //__CUDA_TENSOR__
