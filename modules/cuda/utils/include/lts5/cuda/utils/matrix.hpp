/**
 *  @file   cuda/utils/matrix.hpp
 *  @brief  Matrix interface for GPU computation
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date
 *  Copyright (c) 2016 Christophe Ecabert. All right reserved.
 */
#ifndef __LTS5_GPU_MATRIX__
#define __LTS5_GPU_MATRIX__

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 * @class   Matrix
 * @brief   Matrix interface for GPU computation
 * @author  Christophe Ecabert
 * @date    27/06/16
 * @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS Matrix {

 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  Matrix
   * @fn  Matrix(void)
   * @brief Default constructor
   */
  Matrix(void);

  /**
   * @name  Matrix
   * @fn  Matrix(const cv::Mat& matrix, const cv::cuda::Stream& stream)
   * @brief Constructor with direct device upload
   * @param[in] matrix  Host matrix to upload on device
   * @param[in] stream  Stream on which to run computation
   */
  Matrix(const cv::Mat& matrix, const cv::cuda::Stream& stream);

  /**
   * @name  Matrix
   * @fn  explicit Matrix(const cv::Mat& matrix)
   * @brief Constructor with direct device upload
   * @param[in] matrix  Host matrix to upload on device
   */
  explicit Matrix(const cv::Mat& matrix);

  /**
   * @name  Matrix
   * @fn  Matrix(const int row, const int col, T* buffer)
   * @brief Constructor for data with external buffer.
   *        IMPORTANT take ownership of the data
   * @param[in] row     Number of rows
   * @param[in] col     Number of columns
   * @param[in] buffer  Device memory buffer allocated by user
   */
  Matrix(const int row, const int col, T* buffer);

  /**
   * @name  Matrix
   * @fn  Matrix(const Matrix<T>& other)
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  Matrix(const Matrix<T>& other);

  /**
   * @name  operator=
   * @fn  Matrix<T>& operator=(const Matrix<T>& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return  New assigned object
   */
  Matrix<T>& operator=(const Matrix<T>& rhs);

  /**
   * @name  Clone
   * @fn  Matrix<T> Clone(const cv::cuda::Stream& stream)
   * @brief Deep copy
   * @param[in] stream  Stream on which to run computation
   * @return Copy of the current matrix
   */
  Matrix<T> Clone(const cv::cuda::Stream& stream);

  /**
   * @name  Clone
   * @fn  Matrix<T> Clone(const cv::cuda::Stream& stream) const
   * @brief Deep copy
   * @param[in] stream  Stream on which to run computation
   * @return Copy of the current matrix
   */
  Matrix<T> Clone(const cv::cuda::Stream& stream) const;

  /**
   * @name  ~Matrix
   * @fn  ~Matrix(void)
   * @brief Destructor
   */
  ~Matrix(void);

  /**
   * @name  Create
   * @fn  void Create(const int row, const int col)
   * @brief Create GPU matrix of a given size
   * @param[in] row Number of rows
   * @param[in] col Number of columns
   */
  void Create(const int row, const int col);

  /**
   * @name  Reshape
   * @fn  void Reshape(const int row, const int col)
   * @brief Reshape GPU matrix of a given size, col*row must be correct
   * @param[in] row Number of rows
   * @param[in] col Number of columns
   */
  void Reshape(const int row, const int col);

  /**
   * @name  Upload
   * @fn  void Upload(const cv::Mat& matrix, const cv::cuda::Stream& stream)
   * @brief Upload Host matrix into Device memory
   * @param[in] matrix  Host matrix to push onto Device
   * @param[in] stream  Stream on which to run computation
   */
  void Upload(const cv::Mat& matrix, const cv::cuda::Stream& stream);

  /**
   * @name  Upload
   * @fn  void Upload(const cv::Mat& matrix)
   * @brief Upload Host matrix into Device memory
   * @param[in] matrix  Matrix to upload
   */
  void Upload(const cv::Mat& matrix);

  /**
   * @name  Download
   * @fn  void Download(const cv::cuda::Stream& stream, cv::Mat* matrix) const
   * @brief Copy matrix from Device to Host
   * @param[in] stream  Stream on which to run computation
   * @param[out]  matrix  Host matrix where to store data
   */
  void Download(const cv::cuda::Stream& stream, cv::Mat* matrix) const;

  /**
   * @name  Download
   * @fn  void Download(cv::Mat* matrix) const
   * @brief Copy matrix from Device to Host
   * @param[out]  matrix  Host matrix where to store data
   */
  void Download(cv::Mat* matrix) const;

  /**
   * @name  SetTo
   * @fn    void SetTo(const T value, const cv::cuda::Stream& stream)
   * @brief Initialize matrix with a given \p value
   * @param[in] value   Value to set
   * @param[in] stream  Stream on which to run computation
   */
  void SetTo(const T value, const cv::cuda::Stream& stream);

  /**
   * @name  SetTo
   * @fn    void SetTo(const T value)
   * @brief Initialize matrix with a given \p value
   * @param[in] value   Value to set
   */
  void SetTo(const T value);

  /**
   * @name  ConvertTo
   * @fn    void ConvertTo(const cv::cuda::Stream& stream,
                           Matrix<D>* matrix) const
   * @brief Convert the matrix into another type
   * @tparam D  Type to convert to
   * @param[in] stream  Stream on which to run computation
   * @param[out] matrix Converted matrix
   */
  template<typename D>
  void ConvertTo(const cv::cuda::Stream& stream, Matrix<D>* matrix) const;

  /**
   * @name  ConvertTo
   * @fn    void ConvertTo(Matrix<D>* matrix) const
   * @brief Convert the matrix into another type
   * @tparam D  Type to convert to
   * @param[out] matrix Converted matrix
   */
  template<typename D>
  void ConvertTo(Matrix<D>* matrix) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  rows
   * @fn  int rows(void) const
   * @brief Provide number of rows
   * @return  Number of rows in the matrix
   */
  int rows(void) const {
    return rows_;
  }

  /**
   * @name  cols
   * @fn  int cols(void) const
   * @brief Provide number of columns
   * @return  Number of columns in the matrix
   */
  int cols(void) const {
    return cols_;
  }

  /**
   * @name  data
   * @fn  T* data(void) const
   * @brief Provide device data pointer
   * @return  Device data pointer
   */
  T* data(void) const {
    return data_;
  }

  /**
   * @name  empty
   * @fn  bool empty(void) const
   * @brief Indicate if matrix is empty or not
   * @return  True if empty, false otherwise
   */
  bool empty(void) const {
    return (data_ == nullptr);
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   *  @name Retain
   *  @fn void Retain(void)
   *  @brief  Increase reference counter
   */
  void Retain(void);

  /**
   *  @name Release
   *  @fn void Release(void)
   *  @brief  Release object memory if reference counter reach zero
   */
  void Release(void);

  /** Number of rows */
  int rows_;
  /** Number of columns */
  int cols_;
  /** Device data - ColoumnMajor layout */
  T* data_;
  /** Reference counter */
  unsigned* ref_;
};
} // namesapce CUDA
} // namesapce LTS5
#endif //__LTS5_GPU_MATRIX__
