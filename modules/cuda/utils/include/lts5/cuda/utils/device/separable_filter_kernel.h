/**
 *  @file   "cuda/utils/device/separable_filter_kernel.h"
 *  @brief  GPU Kernel code for image filtering
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   13/10/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_SEPARABLE_FILTER_KERNEL__
#define __LTS5_SEPARABLE_FILTER_KERNEL__

#include <cuda_runtime.h>

#include "opencv2/core/cuda.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 *  @namespace  device
 *  @brief      CUDA device code
 */
namespace device {

#pragma mark -
#pragma mark Row Filter

/**
 *  @namespace  RowFilter
 *  @brief      CUDA device code linked to row filter
 */
namespace RowFilter {

/**
 * @name    Call
 * @fn      void Call(const T* src,
                       const size_t spitch,
                       const size_t dpitch,
                       const size_t width,
                       D* dest)
 * @brief   Apply row kernel filter on a given image
 * @ingroup utils
 * @param[in] src   Input image
 * @param[in] spitch    Input's pitch
 * @param[in] dpitch    Destination's pitch
 * @param[in] width     Image width
 * @param[out] dest      Filtered image
 * @tparam KRADIUS  Filter radius
 * @tparam T    Source data tyoe
 * @tparam D    Destination data type
 */
template<int KRADIUS, typename T, typename D>
__global__ void Call(const T* src,
                     const size_t spitch,
                     const size_t dpitch,
                     const size_t width,
                     D* dest);

/**
 * @name    CallBatched
 * @fn      void CallBatched(const T** src,
                             const size_t batch_size,
                             const size_t spitch,
                             const size_t dpitch,
                             const size_t width,
                             D** dest)
 * @brief   Apply row kernel filter on a given image
 * @ingroup utils
 * @param[in] src   Input, array of image
 * @param[in] batch_size    Batch size
 * @param[in] spitch    Input's pitch
 * @param[in] dpitch    Destination's pitch
 * @param[in] width     Image width
 * @param[out] dest      Array of filtered image
 * @tparam KRADIUS  Filter radius
 * @tparam T    Source data tyoe
 * @tparam D    Destination data type
 */
template<int KRADIUS, typename T, typename D>
__global__ void CallBatched(const T** src,
                            const size_t batch_size,
                            const size_t spitch,
                            const size_t dpitch,
                            const size_t width,
                            D** dest);

} // namepsace RowFilter

#pragma mark -
#pragma mark Col Filter

/**
 *  @namespace  ColFilter
 *  @brief      CUDA device code linked to col filter
 */
namespace ColFilter {

/**
 * @name    Call
 * @fn      void Call(const T* src,
                       const size_t spitch,
                       const size_t dpitch,
                       const size_t height,
                       D* dest)
 * @brief   Apply column kernel filter on a given image
 * @ingroup utils
 * @param[in] src   Input image
 * @param[in] spitch    Input's pitch
 * @param[in] dpitch    Destination's pitch
 * @param[in] height     Image height
 * @param[out] dest      Filtered image
 * @tparam KRADIUS  Filter radius
 * @tparam T    Source data tyoe
 * @tparam D    Destination data type
 */
template<int KRADIUS, typename T, typename D>
__global__ void Call(const T* src,
                     const size_t spitch,
                     const size_t dpitch,
                     const size_t height,
                     D* dest);

/**
 * @name    CallBatched
 * @fn      void CallBatched(const T** src,
                             const size_t batch_size,
                             const size_t spitch,
                             const size_t dpitch,
                             const size_t height,
                       D** dest)
 * @brief   Apply column kernel filter on a given image
 * @ingroup utils
 * @param[in] src   Input, array of image
 * @param[in] batch_size    Batch size
 * @param[in] spitch    Input's pitch
 * @param[in] dpitch    Destination's pitch
 * @param[in] height     Image width
 * @param[out] dest      Array of filtered image
 * @tparam KRADIUS  Filter radius
 * @tparam T    Source data tyoe
 * @tparam D    Destination data type
 */
template<int KRADIUS, typename T, typename D>
__global__ void CallBatched(const T** src,
                            const size_t batch_size,
                            const size_t spitch,
                            const size_t dpitch,
                            const size_t height,
                            D** dest);

}  // namespace ColFilter
}  // namepsace device

#pragma mark -
#pragma mark Caller

namespace RowFilter {

/**
 * @name    SetKernel
 * @fn      void SetKernel(const T* k_data,
                           const int size,
                           const cv::cuda::Stream& stream)
 * @brief   Set filter kernel's values
 * @ingroup utils
 * @param[in] k_data    Kernel data
 * @param[in] size      Kernel size
 * @param[in] stream    Stream
 */
template<typename T>
void SetKernel(const T* k_data, const int size, const cv::cuda::Stream& stream);

/**
 * @name    LinearRowFilter
 * @fn      void LinearRowFilter(const cv::cuda::GpuMat& source,
                                 const int ksize,
                                 const cv::cuda::Stream& stream,
                                 cv::cuda::GpuMat* dest)
 * @brief   Interface to call linear filter
 * @ingroup utils
 * @param[in] source    Source image
 * @param[in] ksize     Kernel radius
 * @param[in] stream    Stream where to launch kernel
 * @param[out] dest     Filtered image
 */
template<typename T, typename D>
void LinearRowFilter(const cv::cuda::GpuMat& source,
                     const int ksize,
                     const cv::cuda::Stream& stream,
                     cv::cuda::GpuMat* dest);

/**
 * @name    LinearRowFilterBatched
 * @fn      void LinearRowFilterBatched(const cv::cuda::GpuMat* source,
                                        const int ksize,
                                        const size_t n_batch,
                                        const size_t batch_size,
                                        const cv::cuda::Stream& stream,
                                        cv::cuda::GpuMat* dest)
 * @brief   Interface to call linear filter
 * @ingroup utils
 * @param[in] source    Array of image to filer
 * @param[in] ksize     Kernel radius
 * @param[in] n_batch   Batch index
 * @param[in] batch_size    Size of the batch
 * @param[in] stream    Stream where to launch kernel
 * @param[out] dest     Filtered image
 */
template<typename T, typename D>
void LinearRowFilterBatched(const cv::cuda::GpuMat* source,
                            const int ksize,
                            const size_t n_batch,
                            const size_t batch_size,
                            const cv::cuda::Stream& stream,
                            cv::cuda::GpuMat* dest);


}  // namepsace RowFilter

namespace ColFilter {

/**
 * @name    SetKernel
 * @fn      void SetKernel(const T* k_data,
                           const int size,
                           const cv::gpu::Stream& stream)
 * @brief   Set filter kernel's values
 * @ingroup utils
 * @param[in] k_data    Kernel data
 * @param[in] size      Kernel size
 * @param[in] stream    Stream
 */
template<typename T>
void SetKernel(const T* k_data, const int size, const cv::cuda::Stream& stream);

/**
 * @name    LinearColFilter
 * @fn      void LinearColFilter(cv::cuda::GpuMat& source,
                                 const int ksize,
                                 const cv::cuda::Stream& stream,
                                 cv::cuda::GpuMat* dest)
 * @brief   Filter constructor
 * @ingroup utils
 * @param[in] source    Source image
 * @param[in] ksize     Kernel dimension
 * @param[in] stream    Stream where to launch kernel
 * @param[out] dest      Filtered image
 */
template<typename T, typename D>
void LinearColFilter(const cv::cuda::GpuMat& source,
                     const int ksize,
                     const cv::cuda::Stream& stream,
                     cv::cuda::GpuMat* dest);

/**
 * @name    LinearColFilterBatched
 * @fn      void LinearColFilterBatched(const cv::cuda::GpuMat* source,
                                        const int ksize,
                                        const size_t n_batch,
                                        const size_t batch_size,
                                        const cv::cuda::Stream& stream,
                                        cv::cuda::GpuMat* dest)
 * @brief   Interface to call linear filter
 * @ingroup utils
 * @param[in] source    Array of image to filer
 * @param[in] ksize     Kernel radius
 * @param[in] n_batch   Batch index
 * @param[in] batch_size    Size of the batch
 * @param[in] stream    Stream where to launch kernel
 * @param[out] dest     Filtered image
 */
template<typename T, typename D>
void LinearColFilterBatched(const cv::cuda::GpuMat* source,
                            const int ksize,
                            const size_t n_batch,
                            const size_t batch_size,
                            const cv::cuda::Stream& stream,
                            cv::cuda::GpuMat* dest);

}  // namepsace ColFilter
}  // namepsace CUDA
}  // namepsace LTS5

#endif //__LTS5_SEPARABLE_FILTER_KERNEL__
