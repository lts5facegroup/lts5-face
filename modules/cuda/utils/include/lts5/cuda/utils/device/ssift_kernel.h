/**
 *  @file   cuda/utils/device/ssift_kernel.h
 *  @brief  GPU Custom kernel for SSift computation
 *
 *  Created by Christophe Ecabert on 05/05/16.
 *  Copyright (c) 2016 Ecabert Christophe. All rights reserved.
 */

#ifndef __SSIFT_KERNEL__
#define __SSIFT_KERNEL__

#include <cuda.h>
#include <cuda_runtime.h>

#include "opencv2/core/cuda.hpp"

#include "lts5/utils/ssift.hpp"
#include "lts5/cuda/utils/ssift.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
*  @namespace  CUDA
*  @brief      CUDA dev space
*/
namespace CUDA {

/**
 * @name    SetValue
 * @fn  void SetValue(const float value, const int N,
                      const cv::cuda::Stream &stream, float* array)
 * @brief   Initialize an array to a given value
 * @param[in] value         Value to set
 * @param[in] N             Array dimension
 * @param[in] stream        Stream on which to launch the computation
 * @param[in,out] array     Array to initialize
 * @ingroup utils
 */
void SetValue(const float value,
              const int N,
              const cv::cuda::Stream &stream,
              float* array);

/**
 * @name    SetValue
 * @fn  void SetValue(const float value,
                       const int N,
                       const size_t n_batch,
                       const size_t batch_size,
                       const cv::cuda::Stream &stream,
                       float* array)
 * @brief   Initialize an array of array to a given value
 * @param[in] value         Value to set
 * @param[in] N             Array dimension
 * @param[in] n_batch       Current batch index
 * @param[in] batch_size    Batch size
 * @param[in] stream        Stream on which to launch the computation
 * @param[in,out] array     Array to initialize
 * @ingroup utils
 */
void SetValueBatched(const float value,
                     const int N,
                     const size_t n_batch,
                     const size_t batch_size,
                     const cv::cuda::Stream &stream,
                     float* array);

/**
 * @name    ImageRoiToGrayFloat
 * @fn  void ImageRoiToGrayFloat(const cv::cuda::GpuMat& image,
                     const cv::Rect& roi,
                     const cv::cuda::Stream &stream,
                     cv::cuda::GpuMat* imagef)
 * @brief   Convert a given image roi into grayscale float image
 * @param[in] image Image to convert
 * @param[in] roi   Region of interest
 * @param[in] stream    Stream on which to launch the computation
 * @param[in,out] imagef    Converted image
 * @ingroup utils
 */
void ImageRoiToGrayFloat(const cv::cuda::GpuMat& image,
                         const cv::Rect& roi,
                         const cv::cuda::Stream &stream,
                         cv::cuda::GpuMat* imagef);

/**
 * @name    ImageRoiToGrayFloatBatched
 * @fn  void ImageRoiToGrayFloatBatched(const cv::cuda::GpuMat& image,
                                        const cv::Rect* roi,
                                        const size_t n_batch,
                                        const size_t batch_size,
                                        const cv::cuda::Stream &stream,
                                        cv::cuda::GpuMat* imagef)
 * @brief   Convert a given image roi into grayscale float image
 * @param[in] image Images to convert
 * @param[in] roi   Array of region of interest
 * @param[in] n_batch Current batch index
 * @param[in] batch_size    Size of the batch
 * @param[in] stream    Stream on which to launch the computation
 * @param[in,out] imagef    Array of converted image
 * @ingroup utils
 */
void ImageRoiToGrayFloatBatched(const cv::cuda::GpuMat& image,
                                const cv::Rect* roi,
                                const size_t n_batch,
                                const size_t batch_size,
                                const cv::cuda::Stream &stream,
                                cv::cuda::GpuMat* imagef);

/**
 * @name    SiftFilter
 * @fn  void SiftFilter(const cv::cuda::GpuMat& filtered_image,
                     const GPUSSift::KernelParameter& parameter,
                     float* histogram)
 * @param[in] filtered_image    Patch filtered with gaussian kernel where to
 *                              compute sift
 * @param[in] parameter         SSift extraction parameters
 * @param[in] stream            On which stream to launch the kernel
 * @param[out] histogram        Computed histogram
 * @ingroup utils
 */
void SiftFilter(const cv::cuda::GpuMat &filtered_image,
                const GPUSSift::KernelParameter &parameter,
                const cv::cuda::Stream &stream,
                float *histogram);

/**
 * @name    SiftFilter
 * @fn  void SiftFilter(const cv::cuda::GpuMat* filtered_image,
                       const GPUSSift::KernelParameter* parameter,
                       const size_t n_batch,
                       const size_t batch_size,
                       const cv::cuda::Stream& stream,
                       float *histogram)
 * @param[in] filtered_image    Array of filtered patch with gaussian kernel
 *                              where to compute sift
 * @param[in] parameter     Array of SSift extraction parameters
 * @param[in] n_batch       Current batch index
 * @param[in] batch_size    Size of the batch
 * @param[in] stream        On which stream to launch the kernel
 * @param[out] histogram    Computed histogram
 * @ingroup utils
 */
void SiftFilterBatched(const cv::cuda::GpuMat* filtered_image,
                       const GPUSSift::KernelParameter* parameter,
                       const size_t n_batch,
                       const size_t batch_size,
                       const cv::cuda::Stream& stream,
                       float *histogram);

/**
 * @name    FinalizeHistogram
 * @fn  void FinalizeHistogram(const GPUSSift::KernelParameter& parameter,
                               const cv::cuda::Stream& stream,
                               float* histogram)
 * @brief Finalize histogram, threshold + root sift
 * @param[in] parameter     Sift parameters
 * @param[in] stream        Stream where to launch kernel
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
void FinalizeHistogram(const GPUSSift::KernelParameter& parameter,
                       const cv::cuda::Stream& stream,
                       float* histogram);

/**
 * @name    FinalizeHistogram
 * @fn  void FinalizeHistogram(const GPUSSift::KernelParameter* parameter,
                              const size_t n_batch,
                              const size_t batch_size,
                              const cv::cuda::Stream& stream,
                              float* histogram)
 * @brief Finalize histogram, threshold + root sift
 * @param[in] parameter     Array of SSift extraction parameters
 * @param[in] n_batch       Current batch index
 * @param[in] batch_size    Size of the batch
 * @param[in] stream        Stream where to launch kernel
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
void FinalizeHistogramBatched(const GPUSSift::KernelParameter* parameter,
                              const size_t n_batch,
                              const size_t batch_size,
                              const cv::cuda::Stream& stream,
                              float* histogram);

/**
 * @name    SiftRoot
 * @fn  void SiftRoot(const GPUSSift::KernelParameter& parameter,
                               const cv::cuda::Stream& stream,
                               float* histogram)
 * @brief Apply sift root normalization
 * @param[in] parameter     Sift parameters
 * @param[in] stream        Stream where to launch kernel
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
void SiftRoot(const GPUSSift::KernelParameter& parameter,
              const cv::cuda::Stream& stream,
              float* histogram);

/**
 * @name    SiftRootBatched
 * @fn  void SiftRootBatched(const GPUSSift::KernelParameter* parameter,
                             const size_t n_batch,
                             const size_t batch_size,
                             const cv::cuda::Stream& stream,
                             float* histogram)
 * @brief Apply sift root normalization
 * @param[in] parameter     Sift parameters
 * @param[in] n_batch       Current batch index
 * @param[in] batch_size    Size of the batch
 * @param[in] stream        Stream where to launch kernel
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
void SiftRootBatched(const GPUSSift::KernelParameter* parameter,
                     const size_t n_batch,
                     const size_t batch_size,
                     const cv::cuda::Stream& stream,
                     float* histogram);

/**
 *  @namespace  device
 *  @brief      CUDA Device code
 */
namespace device {

/**
 * @name    set_value
 * @fn  void set_value(const float value, const int N, float* array)
 * @brief   Initialize an array to a given value
 * @param[in] value         Value to set
 * @param[in] N             Array dimension
 * @param[in,out] array     Array to initialize
 */
__global__
void set_value(const float value, const int N, float* array);

/*
 * @name    set_value_batched
 * @fn  void set_value_batched(const float value, const int N, float** array)
 * @brief   Initialize an array of array to a given value
 * @param[in] value         Value to set
 * @param[in] N             Array dimension
 * @param[in,out] array     Arrays to initialize
 */
template<size_t B>
__global__
void set_value_batched(const float value, const int N, float** array);


/**
 * @name    color_to_gray
 * @fn  void color_to_gray(const float* img, const size_t pitch,
                            cv::gpu::PtrStepf gray);
 * @brief   Convert color image into float grayscale image
 * @param[in] img       Image color pointer
 * @param[in] pitch     Image pitch
 * @param[in] dpitch    Destination image pitch
 * @param[out] gray     Converted image
 */
__global__
void color_to_gray(const uchar* img,
                   const size_t pitch,
                   const size_t dpitch,
                   float* gray);

/**
 * @name    color_to_gray_batched
 * @fn  void color_to_gray_batched(const uchar** img,
                           const size_t pitch,
                           const size_t dpitch,
                           float** gray);
 * @brief   Convert a batch of color images into float images. Images must
 *          have all the same dimension
 * @tparam D Batch dimension
 * @param[in] img       Image color pointer
 * @param[in] pitch     Image pitch
 * @param[in] dpitch    Destination image pitch
 * @param[out] gray     Converted image
 */
template<size_t D>
__global__
void color_to_gray_batched(const uchar** img,
                           const size_t pitch,
                           const size_t dpitch,
                           float** gray);

/**
 * @name    gray_to_float
 * @fn  void gray_to_float(const uchar* img,
                           const size_t pitch,
                           const size_t dpitch,
                           float* gray);
 * @brief   Convert grayscale image into float image
 * @param[in] img       Image grayscale pointer
 * @param[in] pitch     Image pitch
 * @param[in] dpitch    Destination image pitch
 * @param[out] gray     Converted image
 */
__global__
void gray_to_float(const uchar* img,
                   const size_t pitch,
                   const size_t dpitch,
                   float* gray);

/**
 * @name    gray_to_float_batched
 * @fn  void gray_to_float_batched(const uchar** img,
                           const size_t pitch,
                           const size_t dpitch,
                           float** gray);
 * @brief   Convert a batch of grayscale images into float images. Images must
 *          have all the same dimension
 * @tparam D Batch dimension
 * @param[in] img       Image grayscale pointer
 * @param[in] pitch     Image pitch
 * @param[in] dpitch    Destination image pitch
 * @param[out] gray     Converted image
 */
template<size_t D>
__global__
void gray_to_float_batched(const uchar** img,
                           const size_t pitch,
                           const size_t dpitch,
                           float** gray);


/**
 * @name    sample_histogram
 * @fn  void sample_histogram(const float* image,
                              const size_t pitch,
                              const int width,
                              const int height,
                              const GPUSSift::KernelParameter parameters,
                              float* histogram)
 * @param[in] image         Patch filtered with gaussian kernel where to compute
 *                          sift
 * @param[in] pitch         Image pitch
 * @param[in] width         Image width
 * @param[in] height        Image height
 * @param[in] parameters    SSift extraction parameters
 * @param[out] histogram    Computed histogram
 * @ingroup utils
 */
__global__
void sample_histogram(const float* image,
                      const size_t pitch,
                      const int width,
                      const int height,
                      const GPUSSift::KernelParameter parameters,
                      float* histogram);

/**
 * @name    sample_histogram
 * @fn  void sample_histogram(const float* image,
                              const size_t pitch,
                              const int width,
                              const int height,
                              const GPUSSift::KernelParameter parameters,
                              float* histogram)
 * @param[in] image         Patch filtered with gaussian kernel where to compute
 *                          sift
 * @param[in] pitch         Image pitch
 * @param[in] width         Image width
 * @param[in] height        Image height
 * @param[in] parameters    SSift extraction parameters
 * @param[out] histogram    Computed histogram
 * @ingroup utils
 */
template<size_t D>
__global__
void sample_histogram_batched(const float** image,
                              const size_t pitch,
                              const int width,
                              const int height,
                              const GPUSSift::KernelParameter* parameters,
                              float** histogram);

/**
 * @name    finalize_histogram
 * @fn  void finalize_histogram(const GPUSSift::KernelParameter parameters,
                        float* histogram)
 * @brief
 * @param[in] parameters        Sift parameters
 * @param[in,out] histogram     Updated histogram
 * @ingroup utils
 */
__global__
void finalize_histogram(const GPUSSift::KernelParameter parameters,
                        float* histogram);

/**
 * @name    finalize_histogram
 * @fn  void finalize_histogram(const GPUSSift::KernelParameter* parameters,
                                float** histogram)
 * @brief
 * @param[in] parameters        Sift parameters
 * @param[in,out] histogram     Updated histogram
 * @ingroup utils
 */
template<size_t D>
__global__
void finalize_histogram_batched(const GPUSSift::KernelParameter* parameters,
                                float** histogram);

/**
 * @name    root_sift
 * @fn  void root_sift(const GPUSSift::KernelParameter parameters,
               float* histogram)
 * @brief   RootSift
    R. Arandjelovic, A. Zisserman, Three things everyone should know
    to improve object retrieval, CVPR'12
 * @param[in] parameters    Sift parameters
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
__global__
void root_sift(const GPUSSift::KernelParameter parameters,
               float* histogram);

/**
 * @name    root_sift
 * @fn  void root_sift(const GPUSSift::KernelParameter* parameters,
                       float** histogram)
 * @brief   RootSift
    R. Arandjelovic, A. Zisserman, Three things everyone should know
    to improve object retrieval, CVPR'12
 * @param[in] parameters    Array of Sift parameters
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
template<size_t D>
__global__
void root_sift_batched(const GPUSSift::KernelParameter* parameters,
                       float** histogram);

}  // device
} // namespace CUDA
} // namespace LTS5

#endif // __SSIFT_KERNEL__