/**
 *  @file   "cuda/utils/device/matrix_kernel.h"
 *  @brief  Matrix kernel function
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   07/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __CUDA_MATRIX_KERNEL__
#define __CUDA_MATRIX_KERNEL__

#include <cuda.h>
#include <cuda_runtime.h>

#include "opencv2/core/cuda.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 * @name    SetTo
 * @fn  void SetTo(const T value, const int N,
                   const cv::cuda::Stream& stream, T* array)
 * @brief   Initialize matrix with a given value
 * @tparam T    Data type
 * @param[in] value Value to set
 * @param[in] N     Array dimension
 * @param[in] stream    Stream on which to lauch computation
 * @param[in] array Buffer to fill
 */
template<typename T>
void SetTo(const T value, const int N, const cv::cuda::Stream& stream, T* array);

/**
 * @name    ConvertTo
 * @fn  void ConvertTo(const T* src, const int N,
                       const cv::cuda::Stream& stream, D* dest)
 * @brief   Convert a given array into a different type
 * @tparam T    Source type
 * @tparam D    Destination type
 * @param[in] src       Source array
 * @param[in] N         Array dimension
 * @param[in] stream    Stream on which to lauch computation
 * @param[out] dest     Converted array
 */
template<typename T, typename D>
void ConvertTo(const T* src, const int N,
               const cv::cuda::Stream& stream, D* dest);

/**
 *  @namespace  device
 *  @brief      CUDA Device code
 */
namespace device {

/**
 * @name    set_to
 * @fn  void set_to(const T value, const int N, T* array)
 * @brief   Initialize matrix buffer with a given value
 * @tparam T    Data type
 * @param[in] value Value to set
 * @param[in] N     Array dimension
 * @param[in] array Buffer to fill
 */
template<typename T>
__global__
void set_to(const T value, const int N, T* array);

/**
 * @name    convert_to
 * @fn  void void convert_to(const T* src, const int N, D* dest);
 * @brief   Convert a given array into a different type
 * @tparam T    Source type
 * @tparam D    Destination type
 * @param[in] src       Source array
 * @param[in] N         Array dimension
 * @param[out] dest     Converted array
 */
template<typename T, typename D>
__global__
void convert_to(const T* src, const int N, D* dest);

}  // namepsace device
}  // namepsace CUDA
}  // namepsace LTS5
#endif //__CUDA_MATRIX_KERNEL__
