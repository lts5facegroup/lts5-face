/**
 *  @file   cuda/utils/safe_call.hpp
 *  @brief  Cuda kernel safe call function helper
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   08/07/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_SAFE_CALL__
#define __LTS5_SAFE_CALL__

#include "cuda_runtime_api.h"
#include "cublas_v2.h"
#include "nvToolsExt.h"
#include "cuda_profiler_api.h"
#include "cudnn.h"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Error handling

/**
 * @name
 * @fn  void error(const char *error_string,
                   const char *file,
                   const int line,
                   const char *func)
 * @brief Print error
 * @ingroup utils
 * @param[in] error_string  Error message
 * @param[in] file          File that trigger the error
 * @param[in] line          Line where the error was throw
 * @param[in] func          Function that throw error
 */
void error(const char *error_string,
           const char *file,
           const int line,
           const char *func);

/**
 * @name    cublasGetErrorEnum
 * @fn  static const char* cublasGetErrorEnum(cublasStatus_t error)
 * @brief   Provide error code for a given cublas error code.
 * @ingroup utils
 * @param[in] error Code of the cublas error
 * @return  String formatted with corresponding error.
 */
static const char* cublasGetErrorEnum(cublasStatus_t error) {
  switch (error) {
    case CUBLAS_STATUS_SUCCESS:
      return "CUBLAS_STATUS_SUCCESS";

    case CUBLAS_STATUS_NOT_INITIALIZED:
      return "CUBLAS_STATUS_NOT_INITIALIZED";

    case CUBLAS_STATUS_ALLOC_FAILED:
      return "CUBLAS_STATUS_ALLOC_FAILED";

    case CUBLAS_STATUS_INVALID_VALUE:
      return "CUBLAS_STATUS_INVALID_VALUE";

    case CUBLAS_STATUS_ARCH_MISMATCH:
      return "CUBLAS_STATUS_ARCH_MISMATCH";

    case CUBLAS_STATUS_MAPPING_ERROR:
      return "CUBLAS_STATUS_MAPPING_ERROR";

    case CUBLAS_STATUS_EXECUTION_FAILED:
      return "CUBLAS_STATUS_EXECUTION_FAILED";

    case CUBLAS_STATUS_INTERNAL_ERROR:
      return "CUBLAS_STATUS_INTERNAL_ERROR";

    default:
      return "<unknown>";
  }
}

/**
 * @name  ___cudaSafeCall
 * @fn  static inline int ___cudaSafeCall(cudaError_t err,
                                          const char *file,
                                          const int line,
                                          const char *func = "")
 * @brief Call cuda kernel and check returned error code
 * @ingroup utils
 * @param[in] err   CUDA Error code
 * @param[in] file  File that trigger the error
 * @param[in] line  Line where the error was throw
 * @param[in] func  Function that throw error
 * @return -1 if error, 0 otherwise
 */
static inline int ___cudaSafeCall(cudaError_t err,
                                  const char *file,
                                  const int line,
                                  const char *func = "") {
  int e = 0;
  if (cudaSuccess != err) {
    e = -1;
    error(cudaGetErrorName(err), file, line, func);
  }
  return e;
}

/**
 * @name  ___cublasSafeCall
 * @fn  static inline int ___cublasSafeCall(cublasStatus_t err,
                                             const char *file,
                                             const int line,
                                             const char *func = "")
 * @brief Call cublas function and check returned error code
 * @ingroup utils
 * @param[in] err   cuBLAS Error code
 * @param[in] file  File that trigger the error
 * @param[in] line  Line where the error was throw
 * @param[in] func  Function that throw error
 * @return -1 if error, 0 otherwise
 */
static inline int ___cublasSafeCall(cublasStatus_t err,
                                    const char *file,
                                    const int line,
                                    const char *func = "") {
  int e = 0;
  if (CUBLAS_STATUS_SUCCESS != err) {
    e = -1;
    error(cublasGetErrorEnum(err), file, line, func);
  }
  return e;
}

/**
 * @name  ___cudnnSafeCall
 * @fn  static inline int ___cudnnSafeCall(cudnnStatus_t err,
                                            const char *file,
                                            const int line,
                                            const char *func = "")
 * @brief Call cudnn function and check returned error code
 * @ingroup utils
 * @param[in] err   cuDNN Error code
 * @param[in] file  File that trigger the error
 * @param[in] line  Line where the error was throw
 * @param[in] func  Function that throw error
 * @return -1 if error, 0 otherwise
 */
static inline int ___cudnnSafeCall(cudnnStatus_t err,
                                   const char *file,
                                   const int line,
                                   const char *func = "") {
  int e = 0;
  if (CUDNN_STATUS_SUCCESS != err) {
    e = -1;
    error(cudnnGetErrorString(err), file, line, func);
  }
  return e;
}

#if defined(__GNUC__)
#define cudaSafeCall(expr)  LTS5::CUDA::___cudaSafeCall(expr, __FILE__, __LINE__, __func__)
#define cublasSafeCall(expr)  LTS5::CUDA::___cublasSafeCall(expr, __FILE__, __LINE__, __func__)
#define cudnnSafeCall(expr)  LTS5::CUDA::___cudnnSafeCall(expr, __FILE__, __LINE__, __func__)
#else /* defined(__CUDACC__) || defined(__MSVC__) */
#define cudaSafeCall(expr)  LTS5::CUDA::___cudaSafeCall(expr, __FILE__, __LINE__)
#define cublasSafeCall(expr)  LTS5::CUDA::___cublasSafeCall(expr, __FILE__, __LINE__)
#define cudnnSafeCall(expr)  LTS5::CUDA::___cudnnSafeCall(expr, __FILE__, __LINE__)
#endif

#pragma mark -
#pragma mark Profiling

/**
 * @class   Trace
 * @brief   Define cuda trace for profiling session
 * @author  Christophe Ecabert
 * @date    10/06/2017
 * @ingroup utils
 * @see https://devblogs.nvidia.com/parallelforall/cuda-pro-tip-generate-custom-application-profile-timelines-nvtx/
 */
class LTS5_EXPORTS Trace {
 public:
  /** http://www.rapidtables.com/web/color/RGB_Color.htm */
  static constexpr uint32_t color[] = {0x00FF0000, // Red
                                       0x00FFA500, // Orange
                                       0x00FFFF00, // Yellow
                                       0x00ADFF2F, // Green yellow
                                       0x0090EE90, // Light green
                                       0x0020B2AA, // Light sea green
                                       0x0000FFFF, // Cyan
                                       0x007FFFD4, // Aqua Marine
                                       0x001E90FF, // Dodger blue
                                       0x000000FF, // Blue
                                       0x006A5ACD, // Slate blue
                                       0x00800080, // Purple
                                       0x00FF1493, // Deep pink
                                       0x00FFEBCD, // Blanche almond
                                       0x00D2691E, // Chocolate
                                       0x00FFE4E1, // Misty rose
                                       0x00B0C4DE, // Light steel blue
                                       0x00F0FFFF, // Azure
                                       0x00000000, // black
                                       0x00A9A9A9, // Dark gray
                                       0x00D3D3D3, // Light gray
                                       0x00F5F5F5}; // White smoke
  static const int n_color = sizeof(color)/sizeof(color[0]);

  /**
   * @name  Trace
   * @fn    explicit Trace(const char* name, const int idx)
   * @brief Construct a trace with a given name
   * @param[in] name    Trace's name
   * @param[in] idx     Color index
   */
  explicit Trace(const char* name, const int idx) {
    // Create event
    nvtxEventAttributes_t e;
    e.version = NVTX_VERSION;
    e.version = NVTX_VERSION;
    e.size = NVTX_EVENT_ATTRIB_STRUCT_SIZE;
    e.colorType = NVTX_COLOR_ARGB;
    e.color = this->color[idx % this->n_color];
    e.messageType = NVTX_MESSAGE_TYPE_ASCII;
    e.message.ascii = name;
    nvtxRangePushEx(&e);
  }

  /**
   * @name  ~Trace
   * @fn    ~Trace
   * @brief Destructor
   */
  ~Trace(void) {
    nvtxRangePop();
  }
};

#ifdef DEBUG
/** Helper function to initialize a trace */
#define LTS5_CUDA_TRACE(name, cid) LTS5::CUDA::Trace uniq_name_using_macros(name, cid)
/** Helper function to start cuda profiling */
#define LTS5_CUDA_START_PROFILE cudaSafeCall(cudaProfilerStart())
/** Helper function to stop cuda profiling */
#define LTS5_CUDA_STOP_PROFILE cudaSafeCall(cudaProfilerStop())
#else
/** Helper function to initialize a trace */
#define LTS5_CUDA_TRACE(name, cid);
/** Helper function to start cuda profiling */
#define LTS5_CUDA_START_PROFILE
/** Helper function to stop cuda profiling */
#define LTS5_CUDA_STOP_PROFILE
#endif
}  // namepsace CUDA
}  // namepsace LTS5

#endif //__LTS5_SAFE_CALL__
