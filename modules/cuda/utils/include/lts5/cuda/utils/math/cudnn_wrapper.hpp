/**
 *  @file   cuda/utils/math/cudnn_wrapper.hpp
 *  @brief  cuDNN abstraction layer
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   22/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __CUDNN_WRAPPER__
#define __CUDNN_WRAPPER__

#include <thread>
#include <mutex>
#include <unordered_map>

#include "opencv2/core/cuda.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/cuda/utils/tensor.hpp"
#include "lts5/cuda/utils/tensor_operation.hpp"
#include "lts5/cuda/utils/tensor_workspace.hpp"

/** Forward cudnn context */
struct cudnnContext;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 * @class   CudnnContext
 * @brief   Encapsulate cuDNN context into a singleton for each thread
 * @author  Christophe Ecabert
 * @date    06/06/2017
 * @ingroup utils
 */
class LTS5_EXPORTS CudnnContext {
 public:

  /**
   * @name  Instance
   * @fn    static CudnnContext& Instance(void)
   * @brief Provide single instance of CudnnContext
   * @return    Cublas context
   */
  static CudnnContext& Instance(void);

  /**
   * @name  ~CudnnContext
   * @fn    ~CudnnContext(void)
   * @brief Destructor
   */
  ~CudnnContext(void);

  /**
   * @name  Get
   * @fn    cudnnContext* Get(void) const
   * @brief Provide access to context state
   * @return    cublas raw context for this specific thread
   */
  cudnnContext* Get(void);

 private:
  /**
   * @name  CudnnContext
   * @fn    CudnnContext(void)
   * @brief Constructor
   */
  CudnnContext(void);

  /** cuBLAS Handle */
  std::unordered_map<std::thread::id, cudnnContext* > ctx_;
  /** Mutex for thread safety at insertion */
  std::mutex mutex_;
};

/**
 * @class   TensorAlgebra
 * @brief   cuDNN abstraction layer for basic operation on 3D Tensors
 * @author  Christophe Ecabert
 * @date    22/06/2017
 * @tparam T    Data type
 * @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS TensorAlgebra {
 public:

  /** Tensor type */
  using Tensor = typename LTS5::CUDA::Tensor<T>;

#pragma mark -
#pragma mark Tensor Algebra

  /**
   * @name  Operation
   * @fn    static int Operation(const TensorOp<T>& op,
                                 const Tensor& A, const T alpha1,
                                 const Tensor& B, const T alpha2,
                                 const T beta,
                                 Tensor* C)
   * @brief Perform a given operation on tensor :
            C = op(alpha1 * A, alpha2 * B) + beta * C
            Note tensor C must have proper dimension beforehand
   * @param op      Tensor operation to perform
   * @param A       Tensor A
   * @param alpha1  scaling coefficient
   * @param B       Tensor B
   * @param alpha2  scaling coefficient
   * @param beta    scaling coefficient
   * @param C       Tensor C holding result of a given operation
   * @return -1 if error, 0 otherwise
   */
  static int Operation(const TensorOp<T>& op,
                       const Tensor& A, const T alpha1,
                       const Tensor& B, const T alpha2,
                       const T beta,
                       Tensor* C);

  /**
   * @name  Operation
   * @fn    static int Operation(const TensorOp<T>& op,
                      const Tensor& A, const T alpha1,
                      const Tensor& B, const T alpha2,
                      const T beta,
                      const cv::cuda::Stream& stream,
                      Tensor* C)
   * @brief Perform a given operation on tensor :
            C = op(alpha1 * A, alpha2 * B) + beta * C
            Note tensor C must have proper dimension beforehand
   * @param[in] op      Tensor operation to perform
   * @param[in] A       Tensor A
   * @param[in] alpha1  scaling coefficient
   * @param[in] B       Tensor B
   * @param[in] alpha2  scaling coefficient
   * @param[in] beta    scaling coefficient
   * @param[in] stream  Stream on which to run computation
   * @param[in,out] C   Tensor C holding result of a given
   * @return -1 if error, 0 otherwise
   */
  static int Operation(const TensorOp<T>& op,
                       const Tensor& A, const T alpha1,
                       const Tensor& B, const T alpha2,
                       const T beta,
                       const cv::cuda::Stream& stream,
                       Tensor* C);

  /**
   * @name  Reduction
   * @fn    static int Reduction(const TensorRedOp<T>& red,
                                 const TensorWorkspace<T>& workspace,
                                 const Tensor& A, const T alpha,
                                 const T beta,
                                 Tensor* C)
   * @brief Perform tensor reduction :
   *        C = alpha * red_op(A) + beta * C
   * @param[in] red         Reduction operation
   * @param[in] workspace   Workspace with proper dimension
   * @param[in] A           Tensor A
   * @param[in] alpha       scaling factor
   * @param[in] beta        scaling factor
   * @param[out] C          Resulting tensor, NOTE: Dimension must be correct
   *                        beforehand
   * @return    -1 if error, 0 otherwise
   */
  static int Reduction(const TensorRedOp<T>& red,
                       const TensorWorkspace<T>& workspace,
                       const Tensor& A, const T alpha,
                       const T beta,
                       Tensor* C);

  /**
   * @name  Reduction
   * @fn    static int Reduction(const TensorRedOp<T>& red,
                                 const TensorWorkspace<T>& workspace,
                                 const Tensor& A, const T alpha,
                                 const T beta,
                                 const cv::cuda::Stream& stream,
                                 Tensor* C)
   * @brief Perform tensor reduction :
   *        C = alpha * red_op(A) + beta * C
   * @param[in] red         Reduction operation
   * @param[in] workspace   Workspace with proper dimension
   * @param[in] A           Tensor A
   * @param[in] alpha       scaling factor
   * @param[in] beta        scaling factor
   * @param[in] stream      Stream on which to run computation
   * @param[out] C          Resulting tensor, NOTE: Dimension must be correct
   *                        beforehand
   * @return    -1 if error, 0 otherwise
   */
  static int Reduction(const TensorRedOp<T>& red,
                       const TensorWorkspace<T>& workspace,
                       const Tensor& A, const T alpha,
                       const T beta,
                       const cv::cuda::Stream& stream,
                       Tensor* C);

  /**
   * @name  Convolution
   * @fn    static int Convolution(const TensorConvOp<T>& conv,
                                   const TensorWorkspace<T>& workspace,
                                   const Tensor& input, const T alpha,
                                   const T beta,
                                   Tensor* output)
   * @brief Perform convolution
   * @param[in] conv        Convolution operationo
   * @param[in] workspace   Workspace
   * @param[in] input       Input tensor to filter
   * @param[in] alpha       scaling factor
   * @param[in] beta        scaling factor
   * @param[out] output     Tensor holding filtered data
   * @return    -1 if error, 0 otherwise
   */
  static int Convolution(const TensorConvOp<T>& conv,
                         const TensorWorkspace<T>& workspace,
                         const Tensor& input, const T alpha,
                         const T beta,
                         Tensor* output);

  /**
   * @name  Convolution
   * @fn    static int Convolution(const TensorConvOp<T>& conv,
                                   const TensorWorkspace<T>& workspace,
                                   const Tensor& input, const T alpha,
                                   const T beta,
                                   const cv::cuda::Stream& stream,
                                   Tensor* output)
   * @brief Perform convolution
   * @param[in] conv        Convolution operationo
   * @param[in] workspace   Workspace
   * @param[in] input       Input tensor to filter
   * @param[in] alpha       scaling factor
   * @param[in] beta        scaling factor
   * @param[in] stream      Stream on which to launch computation
   * @param[out] output     Tensor holding filtered data
   * @return    -1 if error, 0 otherwise
   */
  static int Convolution(const TensorConvOp<T>& conv,
                         const TensorWorkspace<T>& workspace,
                         const Tensor& input, const T alpha,
                         const T beta,
                         const cv::cuda::Stream& stream,
                         Tensor* output);
};

}  // namepsace CUDA
}  // namepsace LTS5

#endif //__CUDNN_WRAPPER__
