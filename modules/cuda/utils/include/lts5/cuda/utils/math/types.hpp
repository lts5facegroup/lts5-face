/**
 *  @file   "cuda/utils/math/types.hpp"
 *  @brief  Math vector/matrix type
 *  @ingroup    utils
 *
 *  @author Christophe Ecabert
 *  @date   29/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_CUDA_MATH_TYPE__
#define __LTS5_CUDA_MATH_TYPE__

#include "cuda.h"
#include "cuda_runtime.h"
#include "cuda_runtime_api.h"

#include "lts5/cuda/utils/helper_math.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<typename T>
struct MathTypes;

template<>
struct MathTypes<int> {
  /** Base type */
  using Type = int;
  /** Base constant type */
  using ConstType = const int;
  /** Vector 2 type */
  using Vec2 = int2;
  /** Vector 3 type */
  using Vec3 = int3;
  /** Vector 4 type */
  using Vec4 = int4;
  /** Const Vector 2 type */
  using ConstVec2 = const int2;
  /** Const Vector 3 type */
  using ConstVec3 = const int3;
  /** Const Vector 4 type */
  using ConstVec4 = const int4;
};

template<>
struct MathTypes<unsigned int> {
  /** Base type */
  using Type = unsigned int;
  /** Base constant type */
  using ConstType = const unsigned int;
  /** Vector 2 type */
  using Vec2 = uint2;
  /** Vector 3 type */
  using Vec3 = uint3;
  /** Vector 4 type */
  using Vec4 = uint4;
  /** Const Vector 2 type */
  using ConstVec2 = const uint2;
  /** Const Vector 3 type */
  using ConstVec3 = const uint3;
  /** Const Vector 4 type */
  using ConstVec4 = const uint4;
};

template<>
struct MathTypes<char> {
  /** Base type */
  using Type = char;
  /** Base constant type */
  using ConstType = const char;
  /** Vector 2 type */
  using Vec2 = char2;
  /** Vector 3 type */
  using Vec3 = char3;
  /** Vector 4 type */
  using Vec4 = char4;
  /** Const Vector 2 type */
  using ConstVec2 = const char2;
  /** Const Vector 3 type */
  using ConstVec3 = const char3;
  /** Const Vector 4 type */
  using ConstVec4 = const char4;
};

template<>
struct MathTypes<unsigned char> {
  /** Base type */
  using Type = unsigned char;
  /** Base constant type */
  using ConstType = const unsigned char;
  /** Vector 2 type */
  using Vec2 = uchar2;
  /** Vector 3 type */
  using Vec3 = uchar3;
  /** Vector 4 type */
  using Vec4 = uchar4;
  /** Const Vector 2 type */
  using ConstVec2 = const uchar2;
  /** Const Vector 3 type */
  using ConstVec3 = const uchar3;
  /** Const Vector 4 type */
  using ConstVec4 = const uchar4;
};

template<>
struct MathTypes<short> {
  /** Base type */
  using Type = short;
  /** Base constant type */
  using ConstType = const short;
  /** Vector 2 type */
  using Vec2 = short2;
  /** Vector 3 type */
  using Vec3 = short3;
  /** Vector 4 type */
  using Vec4 = short4;
  /** Const Vector 2 type */
  using ConstVec2 = const short2;
  /** Const Vector 3 type */
  using ConstVec3 = const short3;
  /** Const Vector 4 type */
  using ConstVec4 = const short4;
};

template<>
struct MathTypes<unsigned short> {
  /** Base type */
  using Type = unsigned short;
  /** Base constant type */
  using ConstType = const unsigned short;
  /** Vector 2 type */
  using Vec2 = ushort2;
  /** Vector 3 type */
  using Vec3 = ushort3;
  /** Vector 4 type */
  using Vec4 = ushort4;
  /** Const Vector 2 type */
  using ConstVec2 = const ushort2;
  /** Const Vector 3 type */
  using ConstVec3 = const ushort3;
  /** Const Vector 4 type */
  using ConstVec4 = const ushort4;
};

template<>
struct MathTypes<float> {
  /** Base type */
  using Type = float;
  /** Base constant type */
  using ConstType = const float;
  /** Vector 2 type */
  using Vec2 = float2;
  /** Vector 3 type */
  using Vec3 = float3;
  /** Vector 4 type */
  using Vec4 = float4;
  /** Const Vector 2 type */
  using ConstVec2 = const float2;
  /** Const Vector 3 type */
  using ConstVec3 = const float3;
  /** Const Vector 4 type */
  using ConstVec4 = const float4;
};

template<>
struct MathTypes<double> {
  /** Base type */
  using Type = double;
  /** Base constant type */
  using ConstType = const double;
  /** Vector 2 type */
  using Vec2 = double2;
  /** Vector 3 type */
  using Vec3 = double3;
  /** Vector 4 type */
  using Vec4 = double4;
  /** Const Vector 2 type */
  using ConstVec2 = const double2;
  /** Const Vector 3 type */
  using ConstVec3 = const double3;
  /** Const Vector 4 type */
  using ConstVec4 = const double4;
};

// Vec2
template<typename T>
inline __host__ __device__ typename MathTypes<T>::Vec2 Pack(const T& a, const T& b);

// Vec3
template<typename T>
inline __host__ __device__ typename MathTypes<T>::Vec3 Pack3(const T& v);
template<typename T>
inline __host__ __device__ typename MathTypes<T>::Vec3 Pack(const T& a, const T& b, const T& c);

// Vec4
template<typename T>
inline __host__ __device__ typename MathTypes<T>::Vec4 Pack4(const T& a);
template<typename T>
inline __host__ __device__ typename MathTypes<T>::Vec4 Pack(const T& a, const T& b, const T& c, const T& d);






// Vec2
template<>
inline __host__ __device__ typename MathTypes<float>::Vec2 Pack(const float& a, const float& b) {
  return make_float2(a, b);
}
template<>
inline __host__ __device__ typename MathTypes<double>::Vec2 Pack(const double& a, const double& b) {
  return make_double2(a, b);
}

// Vec3
template<>
inline __host__ __device__ typename MathTypes<float>::Vec3 Pack3(const float& v) {
  return make_float3(v, v, v);
}
template<>
inline __host__ __device__ typename MathTypes<double>::Vec3 Pack3(const double& v) {
  return make_double3(v, v, v);
}

template<>
inline __host__ __device__ typename MathTypes<float>::Vec3 Pack(const float& a, const float& b, const float& c) {
  return make_float3(a, b, c);
}

template<>
inline __host__ __device__ typename MathTypes<double>::Vec3 Pack(const double& a, const double& b, const double& c) {
  return make_double3(a, b, c);
}

// Vec4
template<>
inline __host__ __device__ typename MathTypes<float>::Vec4 Pack4(const float& a) {
  return make_float4(a, a, a, a);
}
template<>
inline __host__ __device__ typename MathTypes<double>::Vec4 Pack4(const double& a) {
  return make_double4(a, a, a, a);
}

template<>
inline __host__ __device__ typename MathTypes<float>::Vec4 Pack(const float& a, const float& b, const float& c, const float& d) {
  return make_float4(a, b, c, d);
}

template<>
inline __host__ __device__ typename MathTypes<double>::Vec4 Pack(const double& a, const double& b, const double& c, const double& d) {
  return make_double4(a, b, c, d);
}


/**
 * @struct  Mat2x2
 * @brief   2x2 matrix representation for GPU device with data stored as:
 *          [a, b,
 *           c, d]
 * @author  Christophe Ecabert
 * @date    27/07/20
 * @tparam T Data type
 * @ingroup utils
 */
template<typename T>
struct Mat2x2 {
  /** Vector3 */
  using Vec2 = typename MathTypes<T>::Vec2;
  /** Entries */
  T a, b, c, d;

  /**
   * @name  Mat2x2
   * @brief Default constructor, empty matrix
   */
  __device__
  Mat2x2() : a(T(0.0)),
             b(T(0.0)),
             c(T(0.0)),
             d(T(0.0)) {}

  /**
   * @name  Mat2x2
   * @brief Constructor
   * @param[in] a M00
   * @param[in] b M01
   * @param[in] c M10
   * @param[in] d M11
   */
  __device__
  Mat2x2(const T& a, const T& b, const T& c, const T& d) : a(a),
                                                           b(b),
                                                           c(c),
                                                           d(d) {
  }

  /**
   * @name  Ones
   * @brief Create matrix filled with one
   * @return    matrix
   */
  __device__
  static Mat2x2 Ones() {
    return Mat2x2(T(1.0), T(1.0),
                  T(1.0), T(1.0));
  }

  /**
   * @name  Zeros
   * @brief Create matrix filled with zeros
   * @return    matrix
   */
  __device__
  static Mat2x2 Zeros() {
    return Mat2x2();
  }

  /**
   * @name  Identity
   * @brief Create an identity matrix
   * @return    matrix
   */
  __device__
  static Mat2x2 Identity() {
    return Mat2x2(T(1.0), T(0.0),
                  T(0.0), T(1.0));
  }

  /**
   * @name  operator+
   * @brief Matrix addition
   * @param[in] rhs Right matrix
   * @return Addition output
   */
  __device__
  Mat2x2 operator+(const Mat2x2& rhs) {
    return Mat2x2(a + rhs.a,
                  b + rhs.b,
                  c + rhs.c,
                  d + rhs.d);
  }

  /**
   * @name  operator-
   * @brief Matrix subtraction
   * @param[in] rhs Right matrix
   * @return Subtraction output
   */
  __device__
  Mat2x2 operator-(const Mat2x2& rhs) {
    return Mat2x2(a - rhs.a,
                  b - rhs.b,
                  c - rhs.c,
                  d - rhs.d);
  }

  /**
   * @name operator-
   * @brief Negation operator
   * @return    -matrix
   */
  __device__
  Mat2x2 operator-() {
    return Mat2x2(-a,
                  -b,
                  -c,
                  -d);
  }

  /**
   * @name  operator*
   * @brief Matrix multiplication
   * @param[in] rhs Right matrix
   * @return Multiplication output
   */
  __device__
  Mat2x2 operator*(const Mat2x2& rhs) {
    return Mat2x2((a * rhs.a) + (b * rhs.c),
                  (a * rhs.b) + (b * rhs.d),
                  (c * rhs.a) + (d * rhs.c),
                  (c * rhs.b) + (d * rhs.d));
  }

  /**
   * @name  operator*
   * @brief Matrix-vector multiplication
   * @param[in] rhs Right vector
   * @return Vector
   */
  __device__
  Vec2 operator*(const Vec2& rhs) const {
    return Pack((a * rhs.x) + (b * rhs.y),
                (c * rhs.x) + (d * rhs.y));
  }

  /**
   * @name  operator*
   * @brief Matrix-scalar multiplication
   * @param[in] rhs Right scalar
   * @return Matrix
   */
  __device__
  Mat2x2 operator*(const T& rhs) {
    return Mat2x2((a * rhs), (b * rhs),
                  (c * rhs), (d * rhs));
  }

  /**
   * @name Transpose
   * @brief Matrix transpose
   * @return    Transposed matrix
   */
  __device__
  Mat2x2 Transpose() {
    return Mat2x2(a, c,
                  b, d);
  }
};


/**
 * @struct  Mat3x3
 * @brief   3x3 matrix representation for GPU device with data stored as:
 *          [a, b, c,
 *           d, e, f,
 *           g, h, i]
 * @author  Christophe Ecabert
 * @date    17/07/19
 * @tparam T Data type
 * @ingroup utils
 */
template<typename T>
struct Mat3x3 {
  /** Vector3 */
  using Vec3 = typename MathTypes<T>::Vec3;
  /** Entries */
  T a, b, c, d, e, f, g, h, i;

  /**
   * @name  Mat3x3
   * @brief Default constructor, empty matrix
   */
  __device__
  Mat3x3() : a(T(0.0)),
             b(T(0.0)),
             c(T(0.0)),
             d(T(0.0)),
             e(T(0.0)),
             f(T(0.0)),
             g(T(0.0)),
             h(T(0.0)),
             i(T(0.0)) {}

  /**
   * @name  Mat3x3
   * @brief Constructor
   * @param[in] a M00
   * @param[in] b M01
   * @param[in] c M02
   * @param[in] d M10
   * @param[in] e M11
   * @param[in] f M12
   * @param[in] g M20
   * @param[in] h M21
   * @param[in] i M22
   */
  __device__
  Mat3x3(const T& a,
         const T& b,
         const T& c,
         const T& d,
         const T& e,
         const T& f,
         const T& g,
         const T& h,
         const T& i) : a(a),
                       b(b),
                       c(c),
                       d(d),
                       e(e),
                       f(f),
                       g(g),
                       h(h),
                       i(i) {
  }

  /**
   * @name  Ones
   * @brief Create matrix filled with one
   * @return    matrix
   */
  __device__
  static Mat3x3 Ones() {
    return Mat3x3(T(1.0), T(1.0), T(1.0),
                  T(1.0), T(1.0), T(1.0),
                  T(1.0), T(1.0), T(1.0));
  }

  /**
   * @name  Zeros
   * @brief Create matrix filled with zeros
   * @return    matrix
   */
  __device__
  static Mat3x3 Zeros() {
    return Mat3x3();
  }

  /**
   * @name  Identity
   * @brief Create an identity matrix
   * @return    matrix
   */
  __device__
  static Mat3x3 Identity() {
    return Mat3x3(T(1.0), T(0.0), T(0.0),
                  T(0.0), T(1.0), T(0.0),
                  T(0.0), T(0.0), T(1.0));
  }

  /**
   * @name  Skew
   * @brief Create an scew matrix from a given vector {x, y, z}.
   *        Skew = [0 -z y,
   *                z 0 -x,
   *                -y x 0]
   * @return    matrix
   */
  __device__
  static Mat3x3 Skew(const T& x, const T& y, const T& z) {
    return Mat3x3(T(0.0), -z, y,
                  z, T(0.0), -x,
                  -y, x, T(0.0));
  }

  /**
   * @name  operator+
   * @brief Matrix addition
   * @param[in] rhs Right matrix
   * @return Addition output
   */
  __device__
  Mat3x3 operator+(const Mat3x3& rhs) {
    return Mat3x3(a + rhs.a,
                  b + rhs.b,
                  c + rhs.c,
                  d + rhs.d,
                  e + rhs.e,
                  f + rhs.f,
                  g + rhs.g,
                  h + rhs.h,
                  i + rhs.i);
  }

  /**
   * @name  operator-
   * @brief Matrix subtraction
   * @param[in] rhs Right matrix
   * @return Subtraction output
   */
  __device__
  Mat3x3 operator-(const Mat3x3& rhs) {
    return Mat3x3(a - rhs.a,
                  b - rhs.b,
                  c - rhs.c,
                  d - rhs.d,
                  e - rhs.e,
                  f - rhs.f,
                  g - rhs.g,
                  h - rhs.h,
                  i - rhs.i);
  }

  /**
   * @name operator-
   * @brief Negation operator
   * @return    -matrix
   */
  __device__
  Mat3x3 operator-() {
    return Mat3x3(-a,
                  -b,
                  -c,
                  -d,
                  -e,
                  -f,
                  -g,
                  -h,
                  -i);
  }

  /**
   * @name  operator*
   * @brief Matrix multiplication
   * @param[in] rhs Right matrix
   * @return Multiplication output
   */
  __device__
  Mat3x3 operator*(const Mat3x3& rhs) {
    return Mat3x3((a * rhs.a) + (b * rhs.d) + (c * rhs.g),
                  (a * rhs.b) + (b * rhs.e) + (c * rhs.h),
                  (a * rhs.c) + (b * rhs.f) + (c * rhs.i),
                  (d * rhs.a) + (e * rhs.d) + (f * rhs.g),
                  (d * rhs.b) + (e * rhs.e) + (f * rhs.h),
                  (d * rhs.c) + (e * rhs.f) + (f * rhs.i),
                  (g * rhs.a) + (h * rhs.d) + (i * rhs.g),
                  (g * rhs.b) + (h * rhs.e) + (i * rhs.h),
                  (g * rhs.c) + (h * rhs.f) + (i * rhs.i));
  }

  /**
   * @name  operator*
   * @brief Matrix-vector multiplication
   * @param[in] rhs Right vector
   * @return Vector
   */
  __device__
  Vec3 operator*(const Vec3& rhs) const {
    return Pack((a * rhs.x) + (b * rhs.y) + (c * rhs.z),
                (d * rhs.x) + (e * rhs.y) + (f * rhs.z),
                (g * rhs.x) + (h * rhs.y) + (i * rhs.z));
  }

  /**
   * @name  operator*
   * @brief Matrix-scalar multiplication
   * @param[in] rhs Right scalar
   * @return Matrix
   */
  __device__
  Mat3x3 operator*(const T& rhs) {
    return Mat3x3((a * rhs), (b * rhs), (c * rhs),
                  (d * rhs), (e * rhs), (f * rhs),
                  (g * rhs), (h * rhs), (i * rhs));
  }

  /**
   * @name Transpose
   * @brief Matrix transpose
   * @return    Transposed matrix
   */
  __device__
  Mat3x3 Transpose() {
    return Mat3x3(a, d, g,
                  b, e, h,
                  c, f, i);
  }
};

/**
 * @struct  Mat4x4
 * @brief   4x4 matrix representation for GPU device with data stored as:
 *          [a, b, c, d,
 *           e, f, g, h,
 *           i, j, k, l,
 *           m, n, o, p]
 * @author  Christophe Ecabert
 * @date    17/07/2020
 * @tparam T Data type
 * @ingroup utils
 */
template<typename T>
struct Mat4x4 {
  /** Vector3 */
  using Vec4 = typename MathTypes<T>::Vec4;
  /** Entries */
  T a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p;

  /**
   * @name  Mat4x4
   * @brief Default constructor, empty matrix
   */
  __device__
  Mat4x4() : a(T(0.0)),
             b(T(0.0)),
             c(T(0.0)),
             d(T(0.0)),
             e(T(0.0)),
             f(T(0.0)),
             g(T(0.0)),
             h(T(0.0)),
             i(T(0.0)),
             j(T(0.0)),
             k(T(0.0)),
             l(T(0.0)),
             m(T(0.0)),
             n(T(0.0)),
             o(T(0.0)),
             p(T(0.0)) {}

  __device__
  ~Mat4x4() {}

  /**
   * @name  Mat4x4
   * @brief Constructor
   * @param[in] a M00
   * @param[in] b M01
   * @param[in] c M02
   * @param[in] d M03
   * @param[in] e M10
   * @param[in] f M11
   * @param[in] g M12
   * @param[in] h M13
   * @param[in] i M20
   * @param[in] j M21
   * @param[in] k M22
   * @param[in] l M23
   * @param[in] m M30
   * @param[in] n M31
   * @param[in] o M32
   * @param[in] p M33
   */
  __device__
  Mat4x4(const T& a,
         const T& b,
         const T& c,
         const T& d,
         const T& e,
         const T& f,
         const T& g,
         const T& h,
         const T& i,
         const T& j,
         const T& k,
         const T& l,
         const T& m,
         const T& n,
         const T& o,
         const T& p) : a(a),
                       b(b),
                       c(c),
                       d(d),
                       e(e),
                       f(f),
                       g(g),
                       h(h),
                       i(i),
                       j(j),
                       k(k),
                       l(l),
                       m(m),
                       n(n),
                       o(o),
                       p(p) {
  }

  /**
   * @name  Ones
   * @brief Create matrix filled with one
   * @return    matrix
   */
  __device__
  static Mat4x4 Ones() {
    return Mat4x4(T(1.0), T(1.0), T(1.0), T(1.0),
                  T(1.0), T(1.0), T(1.0), T(1.0),
                  T(1.0), T(1.0), T(1.0), T(1.0),
                  T(1.0), T(1.0), T(1.0), T(1.0));
  }

  /**
   * @name  Zeros
   * @brief Create matrix filled with zeros
   * @return    matrix
   */
  __device__
  static Mat4x4 Zeros() {
    return Mat4x4();
  }

  /**
   * @name  Identity
   * @brief Create an identity matrix
   * @return    matrix
   */
  __device__
  static Mat4x4 Identity() {
    return Mat4x4(T(1.0), T(0.0), T(0.0), T(0.0),
                  T(0.0), T(1.0), T(0.0), T(0.0),
                  T(0.0), T(0.0), T(1.0), T(0.0),
                  T(0.0), T(0.0), T(0.0), T(1.0));
  }

  /**
   * @name  operator+
   * @brief Matrix addition
   * @param[in] rhs Right matrix
   * @return Addition output
   */
  __device__
  Mat4x4 operator+(const Mat4x4& rhs) {
    return Mat4x4(a + rhs.a,
                  b + rhs.b,
                  c + rhs.c,
                  d + rhs.d,
                  e + rhs.e,
                  f + rhs.f,
                  g + rhs.g,
                  h + rhs.h,
                  i + rhs.i,
                  j + rhs.j,
                  k + rhs.k,
                  l + rhs.l,
                  m + rhs.m,
                  n + rhs.n,
                  o + rhs.o,
                  p + rhs.p);
  }

  /**
   * @name  operator-
   * @brief Matrix subtraction
   * @param[in] rhs Right matrix
   * @return Subtraction output
   */
  __device__
  Mat4x4 operator-(const Mat4x4& rhs) {
    return Mat4x4(a - rhs.a,
                  b - rhs.b,
                  c - rhs.c,
                  d - rhs.d,
                  e - rhs.e,
                  f - rhs.f,
                  g - rhs.g,
                  h - rhs.h,
                  i - rhs.i,
                  j - rhs.j,
                  k - rhs.k,
                  l - rhs.l,
                  m - rhs.m,
                  n - rhs.n,
                  o - rhs.o,
                  p - rhs.p);
  }

  /**
   * @name operator-
   * @brief Negation operator
   * @return    -matrix
   */
  __device__
  Mat4x4 operator-() {
    return Mat4x4(-a,
                  -b,
                  -c,
                  -d,
                  -e,
                  -f,
                  -g,
                  -h,
                  -i,
                  -j,
                  -k,
                  -l,
                  -m,
                  -n,
                  -o,
                  -p);
  }

  /**
   * @name  operator*
   * @brief Matrix multiplication
   * @param[in] rhs Right matrix
   * @return Multiplication output
   */
  __device__
  Mat4x4 operator*(const Mat4x4& rhs) {
    return Mat4x4((a * rhs.a) + (b * rhs.e) + (c * rhs.i) + (d * rhs.m),
                  (a * rhs.b) + (b * rhs.f) + (c * rhs.j) + (d * rhs.n),
                  (a * rhs.c) + (b * rhs.g) + (c * rhs.k) + (d * rhs.o),
                  (a * rhs.d) + (b * rhs.h) + (c * rhs.l) + (d * rhs.p),
                  (e * rhs.a) + (f * rhs.e) + (g * rhs.i) + (h * rhs.m),
                  (e * rhs.b) + (f * rhs.f) + (g * rhs.j) + (h * rhs.n),
                  (e * rhs.c) + (f * rhs.g) + (g * rhs.k) + (h * rhs.o),
                  (e * rhs.d) + (f * rhs.h) + (g * rhs.l) + (h * rhs.p),
                  (i * rhs.a) + (j * rhs.e) + (k * rhs.i) + (l * rhs.m),
                  (i * rhs.b) + (j * rhs.f) + (k * rhs.j) + (l * rhs.n),
                  (i * rhs.c) + (j * rhs.g) + (k * rhs.k) + (l * rhs.o),
                  (i * rhs.d) + (j * rhs.h) + (k * rhs.l) + (l * rhs.p),
                  (m * rhs.a) + (n * rhs.e) + (o * rhs.i) + (p * rhs.m),
                  (m * rhs.b) + (n * rhs.f) + (o * rhs.j) + (p * rhs.n),
                  (m * rhs.c) + (n * rhs.g) + (o * rhs.k) + (p * rhs.o),
                  (m * rhs.d) + (n * rhs.h) + (o * rhs.l) + (p * rhs.p));
  }

  /**
   * @name  operator*
   * @brief Matrix-vector multiplication
   * @param[in] rhs Right vector
   * @return Vector
   */
  __device__
  Vec4 operator*(const Vec4& rhs) const {
    return Pack((a * rhs.x) + (b * rhs.y) + (c * rhs.z) + (d * rhs.w),
                (e * rhs.x) + (f * rhs.y) + (g * rhs.z) + (h * rhs.w),
                (i * rhs.x) + (j * rhs.y) + (k * rhs.z) + (l * rhs.w),
                (m * rhs.x) + (n * rhs.y) + (o * rhs.z) + (p * rhs.w));
  }

  /**
   * @name  operator*
   * @brief Matrix-scalar multiplication
   * @param[in] rhs Right scalar
   * @return Matrix
   */
  __device__
  Mat4x4 operator*(const T& rhs) {
    return Mat4x4((a * rhs), (b * rhs), (c * rhs), (d * rhs),
                  (e * rhs), (f * rhs), (g * rhs), (h * rhs),
                  (i * rhs), (j * rhs), (k * rhs), (l * rhs),
                  (m * rhs), (n * rhs), (o * rhs), (p * rhs));
  }

  /**
   * @name Transpose
   * @brief Matrix transpose
   * @return    Transposed matrix
   */
  __device__
  Mat4x4 Transpose() {
    return Mat4x4(a, e, i, m,
                  b, f, j, n,
                  c, g, k, o,
                  d, h, l, p);
  }
};

/**
 * @struct  Mat2x3
 * @brief   2x3 matrix representation for GPU device with data stored as:
 *          [a, b, c,
 *           d, e, f]
 * @author  Christophe Ecabert
 * @date    17/07/2020
 * @tparam T Data type
 * @ingroup utils
 */
template<typename T>
struct Mat2x3 {
  /** Vector3 */
  using Vec2 = typename MathTypes<T>::Vec2;
  using Vec3 = typename MathTypes<T>::Vec3;
  /** Entries */
  T a, b, c, d, e, f;

  /**
   * @name  Mat2x3
   * @brief Default constructor, empty matrix
   */
  __device__
  Mat2x3() : a(T(0.0)),
             b(T(0.0)),
             c(T(0.0)),
             d(T(0.0)),
             e(T(0.0)),
             f(T(0.0)) {}

  /**
   * @name  Mat2x3
   * @brief Constructor
   * @param[in] a M00
   * @param[in] b M01
   * @param[in] c M02
   * @param[in] d M10
   * @param[in] e M11
   * @param[in] f M12
   */
  __device__
  Mat2x3(const T& a, const T& b, const T& c,
         const T& d, const T& e, const T& f) : a(a),
                                               b(b),
                                               c(c),
                                               d(d),
                                               e(e),
                                               f(f) {
  }

  /**
   * @name  Ones
   * @brief Create matrix filled with one
   * @return    matrix
   */
  __device__
  static Mat2x3 Ones() {
    return Mat2x3(T(1.0), T(1.0), T(1.0),
                  T(1.0), T(1.0), T(1.0));
  }

  /**
   * @name  Zeros
   * @brief Create matrix filled with zeros
   * @return    matrix
   */
  __device__
  static Mat2x3 Zeros() {
    return Mat2x3();
  }

  /**
   * @name  Identity
   * @brief Create an identity matrix
   * @return    matrix
   */
  __device__
  static Mat2x3 Identity() {
    return Mat2x3(T(1.0), T(0.0), T(0.0),
                  T(0.0), T(1.0), T(0.0));
  }

  /**
   * @name  operator+
   * @brief Matrix addition
   * @param[in] rhs Right matrix
   * @return Addition output
   */
  __device__
  Mat2x3 operator+(const Mat2x3& rhs) {
    return Mat2x3(a + rhs.a,
                  b + rhs.b,
                  c + rhs.c,
                  d + rhs.d,
                  e + rhs.e,
                  f + rhs.f);
  }

  /**
   * @name  operator-
   * @brief Matrix subtraction
   * @param[in] rhs Right matrix
   * @return Subtraction output
   */
  __device__
  Mat2x3 operator-(const Mat2x3& rhs) {
    return Mat2x3(a - rhs.a,
                  b - rhs.b,
                  c - rhs.c,
                  d - rhs.d,
                  e - rhs.e,
                  f - rhs.f);
  }

  /**
   * @name operator-
   * @brief Negation operator
   * @return    -matrix
   */
  __device__
  Mat2x3 operator-() {
    return Mat2x3(-a,
                  -b,
                  -c,
                  -d,
                  -e,
                  -f);
  }

  /**
   * @name  operator*
   * @brief Matrix multiplication
   * @param[in] rhs Right matrix
   * @return Multiplication output
   */
  __device__
  Mat2x3 operator*(const Mat3x3<T>& rhs) {
    return Mat2x3((a * rhs.a) + (b * rhs.d) + (c * rhs.g),
                  (a * rhs.b) + (b * rhs.e) + (c * rhs.h),
                  (a * rhs.c) + (b * rhs.f) + (c * rhs.i),
                  (d * rhs.a) + (e * rhs.d) + (f * rhs.g),
                  (d * rhs.b) + (e * rhs.e) + (f * rhs.h),
                  (d * rhs.c) + (e * rhs.f) + (f * rhs.i));
  }

  /**
   * @name  operator*
   * @brief Matrix-vector multiplication
   * @param[in] rhs Right vector
   * @return Vector
   */
  __device__
  Vec2 operator*(const Vec3& rhs) const {
    return Pack((a * rhs.x) + (b * rhs.y) + (c * rhs.z),
                (d * rhs.x) + (e * rhs.y) + (f * rhs.z));
  }

  /**
   * @name  operator*
   * @brief Matrix-scalar multiplication
   * @param[in] rhs Right scalar
   * @return Matrix
   */
  __device__
  Mat2x3 operator*(const T& rhs) {
    return Mat2x3((a * rhs), (b * rhs), (c * rhs),
                  (d * rhs), (e * rhs), (f * rhs));
  }
};

/**
 * @struct  Mat3x2
 * @brief   3x2 matrix representation for GPU device with data stored as:
 *          [a, b,
 *           c, d,
 *           e, f]
 * @author  Christophe Ecabert
 * @date    17/07/2020
 * @tparam T Data type
 * @ingroup utils
 */
template<typename T>
struct Mat3x2 {
  /** Vector3 */
  using Vec2 = typename MathTypes<T>::Vec2;
  using Vec3 = typename MathTypes<T>::Vec3;
  /** Entries */
  T a, b, c, d, e, f;

  /**
   * @name  Mat3x2
   * @brief Default constructor, empty matrix
   */
  __device__
  Mat3x2() : a(T(0.0)),
             b(T(0.0)),
             c(T(0.0)),
             d(T(0.0)),
             e(T(0.0)),
             f(T(0.0)) {}

  /**
   * @name  Mat3x2
   * @brief Constructor
   * @param[in] a M00
   * @param[in] b M01
   * @param[in] c M02
   * @param[in] d M10
   * @param[in] e M11
   * @param[in] f M12
   */
  __device__
  Mat3x2(const T& a, const T& b,
         const T& c, const T& d,
         const T& e, const T& f) : a(a),
                                   b(b),
                                   c(c),
                                   d(d),
                                   e(e),
                                   f(f) {
  }

  /**
   * @name  Ones
   * @brief Create matrix filled with one
   * @return    matrix
   */
  __device__
  static Mat3x2 Ones() {
    return Mat3x2(T(1.0), T(1.0),
                  T(1.0), T(1.0),
                  T(1.0), T(1.0));
  }

  /**
   * @name  Zeros
   * @brief Create matrix filled with zeros
   * @return    matrix
   */
  __device__
  static Mat3x2 Zeros() {
    return Mat3x2();
  }

  /**
   * @name  Identity
   * @brief Create an identity matrix
   * @return    matrix
   */
  __device__
  static Mat3x2 Identity() {
    return Mat3x2(T(1.0), T(0.0),
                  T(0.0), T(1.0),
                  T(0.0), T(0.0));
  }

  /**
   * @name  operator+
   * @brief Matrix addition
   * @param[in] rhs Right matrix
   * @return Addition output
   */
  __device__
  Mat3x2 operator+(const Mat3x2& rhs) {
    return Mat3x2(a + rhs.a,
                  b + rhs.b,
                  c + rhs.c,
                  d + rhs.d,
                  e + rhs.e,
                  f + rhs.f);
  }

  /**
   * @name  operator-
   * @brief Matrix subtraction
   * @param[in] rhs Right matrix
   * @return Subtraction output
   */
  __device__
  Mat3x2 operator-(const Mat3x2& rhs) {
    return Mat3x2(a - rhs.a,
                  b - rhs.b,
                  c - rhs.c,
                  d - rhs.d,
                  e - rhs.e,
                  f - rhs.f);
  }

  /**
   * @name operator-
   * @brief Negation operator
   * @return    -matrix
   */
  __device__
  Mat3x2 operator-() {
    return Mat3x2(-a,
                  -b,
                  -c,
                  -d,
                  -e,
                  -f);
  }

  /**
   * @name  operator*
   * @brief Matrix multiplication
   * @param[in] rhs Right matrix
   * @return Multiplication output
   */
  __device__
  Mat3x2 operator*(const Mat2x2<T>& rhs) {
    return Mat3x2((a * rhs.a) + (b * rhs.c),
                  (a * rhs.b) + (b * rhs.d),
                  (c * rhs.a) + (d * rhs.c),
                  (c * rhs.b) + (d * rhs.d),
                  (e * rhs.a) + (f * rhs.c),
                  (e * rhs.b) + (f * rhs.d));
  }

  /**
   * @name  operator*
   * @brief Matrix-vector multiplication
   * @param[in] rhs Right vector
   * @return Vector
   */
  __device__
  Vec3 operator*(const Vec2& rhs) const {
    return Pack((a * rhs.x) + (b * rhs.y),
                (c * rhs.x) + (d * rhs.y),
                (e * rhs.x) + (f * rhs.y));
  }

  /**
   * @name  operator*
   * @brief Matrix-scalar multiplication
   * @param[in] rhs Right scalar
   * @return Matrix
   */
  __device__
  Mat3x2 operator*(const T& rhs) {
    return Mat3x2((a * rhs), (b * rhs),
                  (c * rhs), (d * rhs),
                  (e * rhs), (f * rhs));
  }
};

/**
 * @struct  Mat4x3
 * @brief   4x3 matrix representation for GPU device with data stored as:
 *          [a, b, c,
 *           d, e, f,
 *           g, h, i,
 *           j, k, l]
 * @author  Christophe Ecabert
 * @date    17/07/2020
 * @tparam T Data type
 * @ingroup utils
 */
template<typename T>
struct Mat4x3 {
  /** Vector3 */
  using Vec3 = typename MathTypes<T>::Vec3;
  using Vec4 = typename MathTypes<T>::Vec4;
  /** Entries */
  T a, b, c, d, e, f, g, h, i, j, k, l;

  /**
   * @name  Mat4x3
   * @brief Default constructor, empty matrix
   */
  __device__
  Mat4x3() : a(T(0.0)),
             b(T(0.0)),
             c(T(0.0)),
             d(T(0.0)),
             e(T(0.0)),
             f(T(0.0)),
             g(T(0.0)),
             h(T(0.0)),
             i(T(0.0)),
             j(T(0.0)),
             k(T(0.0)),
             l(T(0.0)) {}

  /**
   * @name  Mat4x3
   * @brief Constructor
   * @param[in] a M00
   * @param[in] b M01
   * @param[in] c M02
   * @param[in] d M10
   * @param[in] e M11
   * @param[in] f M12
   * @param[in] g M20
   * @param[in] h M21
   * @param[in] i M22
   * @param[in] j M30
   * @param[in] k M31
   * @param[in] l M32
   */
  __device__
  Mat4x3(const T& a,
         const T& b,
         const T& c,
         const T& d,
         const T& e,
         const T& f,
         const T& g,
         const T& h,
         const T& i,
         const T& j,
         const T& k,
         const T& l) : a(a),
                       b(b),
                       c(c),
                       d(d),
                       e(e),
                       f(f),
                       g(g),
                       h(h),
                       i(i),
                       j(j),
                       k(k),
                       l(l) {
  }

  /**
   * @name  Ones
   * @brief Create matrix filled with one
   * @return    matrix
   */
  __device__
  static Mat4x3 Ones() {
    return Mat4x3(T(1.0), T(1.0), T(1.0),
                  T(1.0), T(1.0), T(1.0),
                  T(1.0), T(1.0), T(1.0),
                  T(1.0), T(1.0), T(1.0));
  }

  /**
   * @name  Zeros
   * @brief Create matrix filled with zeros
   * @return    matrix
   */
  __device__
  static Mat4x3 Zeros() {
    return Mat4x3();
  }

  /**
   * @name  Identity
   * @brief Create an identity matrix
   * @return    matrix
   */
  __device__
  static Mat4x3 Identity() {
    return Mat4x3(T(1.0), T(0.0), T(0.0),
                  T(0.0), T(1.0), T(0.0),
                  T(0.0), T(0.0), T(1.0),
                  T(0.0), T(0.0), T(0.0));
  }

  /**
   * @name  operator+
   * @brief Matrix addition
   * @param[in] rhs Right matrix
   * @return Addition output
   */
  __device__
  Mat4x3 operator+(const Mat4x3& rhs) {
    return Mat4x3(a + rhs.a,
                  b + rhs.b,
                  c + rhs.c,
                  d + rhs.d,
                  e + rhs.e,
                  f + rhs.f,
                  g + rhs.g,
                  h + rhs.h,
                  i + rhs.i,
                  j + rhs.j,
                  k + rhs.k,
                  l + rhs.l);
  }

  /**
   * @name  operator-
   * @brief Matrix subtraction
   * @param[in] rhs Right matrix
   * @return Subtraction output
   */
  __device__
  Mat4x3 operator-(const Mat4x3& rhs) {
    return Mat4x3(a - rhs.a,
                  b - rhs.b,
                  c - rhs.c,
                  d - rhs.d,
                  e - rhs.e,
                  f - rhs.f,
                  g - rhs.g,
                  h - rhs.h,
                  i - rhs.i,
                  j - rhs.j,
                  k - rhs.k,
                  l - rhs.l);
  }

  /**
   * @name operator-
   * @brief Negation operator
   * @return    -matrix
   */
  __device__
  Mat4x3 operator-() {
    return Mat4x3(-a,
                  -b,
                  -c,
                  -d,
                  -e,
                  -f,
                  -g,
                  -h,
                  -i,
                  -j,
                  -k,
                  -l);
  }

  /**
   * @name  operator*
   * @brief Matrix multiplication
   * @param[in] rhs Right matrix
   * @return Multiplication output
   */
  __device__
  Mat4x3 operator*(const Mat3x3<T>& rhs) {
    return Mat4x3((a * rhs.a) + (b * rhs.d) + (c * rhs.g),
                  (a * rhs.b) + (b * rhs.e) + (c * rhs.h),
                  (a * rhs.c) + (b * rhs.f) + (c * rhs.i),
                  (d * rhs.a) + (e * rhs.d) + (f * rhs.g),
                  (d * rhs.b) + (e * rhs.e) + (f * rhs.h),
                  (d * rhs.c) + (e * rhs.f) + (f * rhs.i),
                  (g * rhs.a) + (h * rhs.d) + (i * rhs.g),
                  (g * rhs.b) + (h * rhs.e) + (i * rhs.h),
                  (g * rhs.c) + (h * rhs.f) + (i * rhs.i),
                  (j * rhs.a) + (k * rhs.d) + (l * rhs.g),
                  (j * rhs.b) + (k * rhs.e) + (l * rhs.h),
                  (j * rhs.c) + (k * rhs.f) + (l * rhs.i));
  }

  /**
   * @name  operator*
   * @brief Matrix-vector multiplication
   * @param[in] rhs Right vector
   * @return Vector
   */
  __device__
  Vec4 operator*(const Vec3& rhs) const {
    return Pack((a * rhs.x) + (b * rhs.y) + (c * rhs.z),
                (d * rhs.x) + (e * rhs.y) + (f * rhs.z),
                (g * rhs.x) + (h * rhs.y) + (i * rhs.z),
                (j * rhs.x) + (k * rhs.y) + (l * rhs.z));
  }

  /**
   * @name  operator*
   * @brief Matrix-scalar multiplication
   * @param[in] rhs Right scalar
   * @return Matrix
   */
  __device__
  Mat4x3 operator*(const T& rhs) {
    return Mat4x3((a * rhs), (b * rhs), (c * rhs),
                  (d * rhs), (e * rhs), (f * rhs),
                  (g * rhs), (h * rhs), (i * rhs),
                  (j * rhs), (k * rhs), (l * rhs));
  }
};

/**
 * @struct  Mat3x4
 * @brief   3x4 matrix representation for GPU device with data stored as:
 *          [a, b, c, d,
 *           e, f, g, h,
 *           i, j, k, l]
 * @author  Christophe Ecabert
 * @date    17/07/2020
 * @tparam T Data type
 * @ingroup utils
 */
template<typename T>
struct Mat3x4 {
  /** Vector3 */
  using Vec3 = typename MathTypes<T>::Vec3;
  using Vec4 = typename MathTypes<T>::Vec4;
  /** Entries */
  T a, b, c, d, e, f, g, h, i, j, k, l;

  /**
   * @name  Mat3x4
   * @brief Default constructor, empty matrix
   */
  __device__
  Mat3x4() : a(T(0.0)),
             b(T(0.0)),
             c(T(0.0)),
             d(T(0.0)),
             e(T(0.0)),
             f(T(0.0)),
             g(T(0.0)),
             h(T(0.0)),
             i(T(0.0)),
             j(T(0.0)),
             k(T(0.0)),
             l(T(0.0)) {}

  /**
   * @name  Mat3x4
   * @brief Constructor
   * @param[in] a M00
   * @param[in] b M01
   * @param[in] c M02
   * @param[in] d M03
   * @param[in] e M10
   * @param[in] f M11
   * @param[in] g M12
   * @param[in] h M13
   * @param[in] i M20
   * @param[in] j M21
   * @param[in] k M22
   * @param[in] l M23
   */
  __device__
  Mat3x4(const T& a,
         const T& b,
         const T& c,
         const T& d,
         const T& e,
         const T& f,
         const T& g,
         const T& h,
         const T& i,
         const T& j,
         const T& k,
         const T& l) : a(a),
                       b(b),
                       c(c),
                       d(d),
                       e(e),
                       f(f),
                       g(g),
                       h(h),
                       i(i),
                       j(j),
                       k(k),
                       l(l) {
  }

  /**
   * @name  Ones
   * @brief Create matrix filled with one
   * @return    matrix
   */
  __device__
  static Mat3x4 Ones() {
    return Mat3x4(T(1.0), T(1.0), T(1.0), T(1.0),
                  T(1.0), T(1.0), T(1.0), T(1.0),
                  T(1.0), T(1.0), T(1.0), T(1.0));
  }

  /**
   * @name  Zeros
   * @brief Create matrix filled with zeros
   * @return    matrix
   */
  __device__
  static Mat3x4 Zeros() {
    return Mat3x4();
  }

  /**
   * @name  Identity
   * @brief Create an identity matrix
   * @return    matrix
   */
  __device__
  static Mat3x4 Identity() {
    return Mat3x4(T(1.0), T(0.0), T(0.0), T(0.0),
                  T(0.0), T(1.0), T(0.0), T(0.0),
                  T(0.0), T(0.0), T(1.0), T(0.0));
  }

  /**
   * @name  operator+
   * @brief Matrix addition
   * @param[in] rhs Right matrix
   * @return Addition output
   */
  __device__
  Mat3x4 operator+(const Mat3x4& rhs) {
    return Mat3x4(a + rhs.a,
                  b + rhs.b,
                  c + rhs.c,
                  d + rhs.d,
                  e + rhs.e,
                  f + rhs.f,
                  g + rhs.g,
                  h + rhs.h,
                  i + rhs.i,
                  j + rhs.j,
                  k + rhs.k,
                  l + rhs.l);
  }

  /**
   * @name  operator-
   * @brief Matrix subtraction
   * @param[in] rhs Right matrix
   * @return Subtraction output
   */
  __device__
  Mat3x4 operator-(const Mat3x4& rhs) {
    return Mat3x4(a - rhs.a,
                  b - rhs.b,
                  c - rhs.c,
                  d - rhs.d,
                  e - rhs.e,
                  f - rhs.f,
                  g - rhs.g,
                  h - rhs.h,
                  i - rhs.i,
                  j - rhs.j,
                  k - rhs.k,
                  l - rhs.l);
  }

  /**
   * @name operator-
   * @brief Negation operator
   * @return    -matrix
   */
  __device__
  Mat3x4 operator-() {
    return Mat3x4(-a,
                  -b,
                  -c,
                  -d,
                  -e,
                  -f,
                  -g,
                  -h,
                  -i,
                  -j,
                  -k,
                  -l);
  }

  /**
   * @name  operator*
   * @brief Matrix multiplication
   * @param[in] rhs Right matrix
   * @return Multiplication output
   */
  __device__
  Mat3x4 operator*(const Mat4x4<T>& rhs) {
    return Mat3x4((a * rhs.a) + (b * rhs.e) + (c * rhs.i) + (d * rhs.m),
                  (a * rhs.b) + (b * rhs.f) + (c * rhs.j) + (d * rhs.n),
                  (a * rhs.c) + (b * rhs.g) + (c * rhs.k) + (d * rhs.o),
                  (a * rhs.d) + (b * rhs.h) + (c * rhs.l) + (d * rhs.p),
                  (e * rhs.a) + (f * rhs.e) + (g * rhs.i) + (h * rhs.m),
                  (e * rhs.b) + (f * rhs.f) + (g * rhs.j) + (h * rhs.n),
                  (e * rhs.c) + (f * rhs.g) + (g * rhs.k) + (h * rhs.o),
                  (e * rhs.d) + (f * rhs.h) + (g * rhs.l) + (h * rhs.p),
                  (i * rhs.a) + (j * rhs.e) + (k * rhs.i) + (l * rhs.m),
                  (i * rhs.b) + (j * rhs.f) + (k * rhs.j) + (l * rhs.n),
                  (i * rhs.c) + (j * rhs.g) + (k * rhs.k) + (l * rhs.o),
                  (i * rhs.d) + (j * rhs.h) + (k * rhs.l) + (l * rhs.p));
  }

  /**
   * @name  operator*
   * @brief Matrix-vector multiplication
   * @param[in] rhs Right vector
   * @return Vector
   */
  __device__
  Vec3 operator*(const Vec4& rhs) const {
    return Pack((a * rhs.x) + (b * rhs.y) + (c * rhs.z) + (d * rhs.w),
                (e * rhs.x) + (f * rhs.y) + (g * rhs.z) + (h * rhs.w),
                (i * rhs.x) + (j * rhs.y) + (k * rhs.z) + (l * rhs.w));
  }

  /**
   * @name  operator*
   * @brief Matrix-scalar multiplication
   * @param[in] rhs Right scalar
   * @return Matrix
   */
  __device__
  Mat3x4 operator*(const T& rhs) {
    return Mat3x4((a * rhs), (b * rhs), (c * rhs), (d * rhs),
                  (e * rhs), (f * rhs), (g * rhs), (h * rhs),
                  (i * rhs), (j * rhs), (k * rhs), (l * rhs));
  }
};

/** Matrix product operator */

template<typename T>
__device__ __forceinline__
Mat3x3<T> operator*(const Mat3x2<T>& a, const Mat2x3<T>& b) {
  return Mat3x3<T>((a.a * b.a) + (a.b * b.d),
                   (a.a * b.b) + (a.b * b.e),
                   (a.a * b.c) + (a.b * b.f),
                   (a.c * b.a) + (a.d * b.d),
                   (a.c * b.b) + (a.d * b.e),
                   (a.c * b.c) + (a.d * b.f),
                   (a.e * b.a) + (a.f * b.d),
                   (a.e * b.b) + (a.f * b.e),
                   (a.e * b.c) + (a.f * b.f));
}

template<typename T>
__device__ __forceinline__
Mat3x4<T> operator*(const Mat3x3<T>& a, const Mat3x4<T>& b) {
  return Mat3x4<T>((a.a * b.a) + (a.b * b.e) + (a.c * b.i),
                   (a.a * b.b) + (a.b * b.f) + (a.c * b.j),
                   (a.a * b.c) + (a.b * b.g) + (a.c * b.k),
                   (a.a * b.d) + (a.b * b.h) + (a.c * b.l),
                   (a.d * b.a) + (a.e * b.e) + (a.f * b.i),
                   (a.d * b.b) + (a.e * b.f) + (a.f * b.j),
                   (a.d * b.c) + (a.e * b.g) + (a.f * b.k),
                   (a.d * b.d) + (a.e * b.h) + (a.f * b.l),
                   (a.g * b.a) + (a.h * b.e) + (a.i * b.i),
                   (a.g * b.b) + (a.h * b.f) + (a.i * b.j),
                   (a.g * b.c) + (a.h * b.g) + (a.i * b.k),
                   (a.g * b.d) + (a.h * b.h) + (a.i * b.l));
}

template<typename T>
__device__ __forceinline__
Mat3x3<T> operator*(const Mat3x4<T>& a, const Mat4x3<T>& b) {
  return Mat3x3<T>((a.a * b.a) + (a.b * b.d) + (a.c * b.g) + (a.d * b.j),
                   (a.a * b.b) + (a.b * b.e) + (a.c * b.h) + (a.d * b.k),
                   (a.a * b.c) + (a.b * b.f) + (a.c * b.i) + (a.d * b.l),
                   (a.e * b.a) + (a.f * b.d) + (a.g * b.g) + (a.h * b.j),
                   (a.e * b.b) + (a.f * b.e) + (a.g * b.h) + (a.h * b.k),
                   (a.e * b.c) + (a.f * b.f) + (a.g * b.i) + (a.h * b.l),
                   (a.i * b.a) + (a.j * b.d) + (a.k * b.g) + (a.l * b.j),
                   (a.i * b.b) + (a.j * b.e) + (a.k * b.h) + (a.l * b.k),
                   (a.i * b.c) + (a.j * b.f) + (a.k * b.i) + (a.l * b.l));
}

/** Non-square matrix transpose */
template<typename T>
__device__ __forceinline__
Mat4x3<T> Transpose(const Mat3x4<T>& a) {
  return Mat4x3<T>(a.a, a.e, a.i,
                   a.b, a.f, a.j,
                   a.c, a.g, a.k,
                   a.d, a.h, a.l);
}



/** Vec3 time scalar */
template<typename T>
inline __host__ __device__ typename MathTypes<T>::Vec3 operator*(typename MathTypes<T>::ConstVec3& a, const T& b) {
  return Pack(b * a.x, b * a.y, b * a.z);
}

}  // namespace LTS5

// Add missing function - double
inline __host__ __device__ double3 operator-(double3 a, double3 b) {
  return make_double3(a.x - b.x, a.y - b.y, a.z - b.z);
}
inline __host__ __device__ double3 operator-(double3 a) {
  return make_double3(-a.x, -a.y, -a.z);
}
inline __host__ __device__ double3 operator+(double3 a, double3 b) {
  return make_double3(a.x + b.x, a.y + b.y, a.z + b.z);
}
/*inline __host__ __device__ double3 operator*(double3 a, double3 b) {
  return make_double3(a.x * b.x, a.y * b.y, a.z * b.z);
}*/
inline __host__ __device__ double3 operator*(const double3& a, const double3& b) {
  return make_double3(a.x * b.x, a.y * b.y, a.z * b.z);
}
inline __host__ __device__ void operator*=(double3 &a, double3 b) {
  a.x *= b.x;
  a.y *= b.y;
  a.z *= b.z;
}
inline __host__ __device__ double3 operator*(double3 a, double b) {
  return make_double3(a.x * b, a.y * b, a.z * b);
}
inline __host__ __device__ double3 operator*(double b, double3 a) {
  return make_double3(b * a.x, b * a.y, b * a.z);
}
inline __host__ __device__ void operator*=(double3 &a, double b) {
  a.x *= b;
  a.y *= b;
  a.z *= b;
}
inline __host__ __device__ void operator+=(double3 &a, double3 b) {
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
}
inline __host__ __device__ double dot(double3 a, double3 b) {
  return a.x * b.x + a.y * b.y + a.z * b.z;
}
inline __host__ __device__ double3 cross(double3 a, double3 b) {
  return make_double3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}

inline __host__ __device__ double3 normalize(double3 v) {
  double invLen = 1.0 / sqrt(dot(v, v));
  return v * invLen;
}

inline __host__ __device__ float norm(float3 v) {
  return sqrt(dot(v, v));
}

inline __host__ __device__ double norm(double3 v) {
  return sqrt(dot(v, v));
}



#endif //__LTS5_CUDA_MATH_TYPE__
