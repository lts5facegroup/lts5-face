/**
 *  @file   cuda/utils/math/cudnn_utils.hpp
 *  @brief  Utility functions/classes for cuDNN wrapper (private header,
 *          should not be used)
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   26/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __CUDA_CUDNN_UTILS__
#define __CUDA_CUDNN_UTILS__

#include "lts5/utils/library_export.hpp"
#include "lts5/cuda/utils/tensor_operation.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Data type selector

/**
 * @struct DataType
 * @brief   Data type selector
 * @author Christophe Ecabert
 * @date 22/06/2017
 * @ingroup utils
 * @tparam T    data type to process
 */
template<typename T>
struct LTS5_EXPORTS DataType {

  /**
   * @name  DataType
   * @fn    DataType(void)
   * @brief Constructor
   */
  DataType(void) {}

  /** Data type */
  static cudnnDataType_t value;
};

#pragma mark -
#pragma mark Tensor operation selector

/**
 * @struct OpSelector
 * @brief   Tensor operation selector
 * @author Christophe Ecabert
 * @date 22/06/2017
 * @ingroup utils
 */
struct OpSelector {

  /**
   * @name Select
   * @fn    static cudnnOpTensorOp_t Select(const TensorOpType& type)
   * @brief Select tensor operation based on an operation type
   * @param[in] type    Type of tensor operation (Add, Mul, Sqrt)
   * @return    cuDNN Tensor operation type
   */
  static cudnnOpTensorOp_t Select(const TensorOpType& type);
};

#pragma mark -
#pragma mark Tensor reduction selector

/**
 * @struct RedSelector
 * @brief   Tensor reduction selector
 * @author Christophe Ecabert
 * @date 23/06/2017
 * @ingroup utils
 */
struct RedSelector {

  /**
   * @name Select
   * @fn    static cudnnOpTensorOp_t Select(const TensorRedType& type)
   * @brief Select tensor reduction based on an reduction type
   * @param[in] type    Type of tensor operation (Add, Mul, Sqrt, ...)
   * @return    cuDNN Tensor operation type
   */
  static cudnnReduceTensorOp_t Select(const TensorRedType & type);
};

#pragma mark -
#pragma mark Tensor convolution selector

/**
 * @struct  ConvSelector
 * @brief   Convolution type selector
 * @author  Christophe Ecabert
 * @date    26/06/2017
 * @ingroup utils
 */
struct ConvSelector {

  /**
   * @name  Select
   * @fn    static cudnnConvolutionMode_t Select(const TensorConvType& type)
   * @brief Convert convolution type to cudnn
   * @param[in] type    Type of convolution
   * @return    converted type for cuDNN
   */
  static cudnnConvolutionMode_t Select(const TensorConvType& type);
};

}  // namepsace CUDA
}  // namepsace LTS5

#endif //__CUDA_CUDNN_UTILS__
