/**
 *  @file   cuda/utils/math/cublas_wrapper.hpp
 *  @brief  Wrapper for cuBLAS API
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   06/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __CUBLAS_WRAPPER__
#define __CUBLAS_WRAPPER__

#include <unordered_map>
#include <mutex>
#include <thread>

#include "opencv2/core/cuda.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/cuda/utils/matrix.hpp"

/** Forward cublas context */
struct cublasContext;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 * @class   CublasContext
 * @brief   Encapsulate cuBLAS context into a singleton
 * @author  Christophe Ecabert
 * @date    06/06/2017
 * @ingroup utils
 */
class LTS5_EXPORTS CublasContext {
 public:

  /**
   * @name  Instance
   * @fn    static CublasContext& Instance(void)
   * @brief Provide single instance of CublasContext
   * @return    Cublas context
   */
  static CublasContext& Instance(void);

  /**
   * @name  ~CublasContext
   * @fn    ~CublasContext(void)
   * @brief Destructor
   */
  ~CublasContext(void);

  /**
   * @name  Get
   * @fn    cublasContext* Get(void) const
   * @brief Provide access to context state
   * @return    cublas raw context for this specific thread
   */
  cublasContext* Get(void);

 private:
  /**
   * @name  CublasContext
   * @fn    CublasContext(void)
   * @brief Constructor
   */
  CublasContext(void);

  /** cuBLAS Handle */
  std::unordered_map<std::thread::id, cublasContext* > ctx_;
  /** Mutex for thread safety at insertion */
  std::mutex mutex_;
};

/**
 * @class   LinearAlgebra
 * @brief   Wrapper for cublas API
 * @author  Christophe Ecabert
 * @date    06/06/17
 * @tparam T    Type of computation, float/double
 * @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS LinearAlgebra {
 public:

  /** Matrix type - generic */
  using Matrix = typename LTS5::CUDA::Matrix<T>;
  /** Matrix type - float */
  using Matrixf = typename LTS5::CUDA::Matrix<float>;
  /** Matrix type - double */
  using Matrixd = typename LTS5::CUDA::Matrix<double>;

  /**
   * @enum  TransposeType
   * @brief Type of transpose
   */
  enum TransposeType {
    /** No transpose */
    kNoTranspose = 0,
    /** Transpose */
    kTranspose = 1,
    /** Conjugate transposed */
    kConjTranspose = 2
  };

  /**
   *  @name   Dot
   *  @fn T Dot(const Matrix& a, const Matrix& b)
   *  @brief  Compute the dot product between two vectors
   *  @param[in]  a  Vector A
   *  @param[in]  b  Vector B
   *  @return Dot product
   */
  static T Dot(const Matrix& a, const Matrix& b);

  /**
   *  @name   Dot
   *  @fn T Dot(const Matrix& a, const Matrix& b, const cv::cuda::Stream& stream)
   *  @brief  Compute the dot product between two vectors
   *  @param[in] a       Vector A
   *  @param[in] b       Vector B
   *  @param[in] stream  Stream on which to run computation
   *  @return Dot product
   */
  static T Dot(const Matrix& a, const Matrix& b, const cv::cuda::Stream& stream);

  /**
   *  @name   Gemv
   *  @fn void Gemv(const Matrix& A,
                   const TransposeType op,
                   const T& alpha,
                   const Matrix& x,
                   const T& beta,
                   Matrix* y)
   *  @brief  Multiplies a matrix by a vector
   *          y = a * Ax + b * y
   *  @param[in]      A         Matrix A
   *  @param[in]      op        Indicate if A is transpose or not
   *  @param[in]      alpha     Scaling factor alpha
   *  @param[in]      x         Vector X
   *  @param[in]      beta      Scaling factor beta
   *  @param[in,out]  y         Output vector
   */
  static void Gemv(const Matrix& A,
                   const TransposeType op,
                   const T& alpha,
                   const Matrix& x,
                   const T& beta,
                   Matrix* y);

  /**
   *  @name   Gemv
   *  @fn void Gemv(const Matrix& A,
                   const TransposeType op,
                   const T& alpha,
                   const Matrix& x,
                   const T& beta,
                   const cv::cuda::Stream& stream,
                   Matrix* y)
   *  @brief  Multiplies a matrix by a vector
   *          y = a * Ax + b * y
   *  @param[in]      A         Matrix A
   *  @param[in]      op        Indicate if A is transpose or not
   *  @param[in]      alpha     Scaling factor alpha
   *  @param[in]      x         Vector X
   *  @param[in]      beta      Scaling factor beta
   *  @param[in]      stream    Stream on which to run computation
   *  @param[in,out]  y         Output vector
   */
  static void Gemv(const Matrix& A,
                   const TransposeType op,
                   const T& alpha,
                   const Matrix& x,
                   const T& beta,
                   const cv::cuda::Stream& stream,
                   Matrix* y);

  /**
   *  @name   Gemm
   *  @fn void Gemm(const Matrix& A,
                   const TransposeType op_a,
                   const T& alpha,
                   const Matrix& B,
                   const TransposeType op_b,
                   const T& beta,
                   Matrix* C)
   *  @brief  Compute the product between two matrices
   *          C = a * AB + b * C
   *  @param[in]      A     Matrix A
   *  @param[in]      op_a  Transpose flag indicator for A
   *  @param[in]      alpha Alpha coefficient
   *  @param[in]      B     Matrix B
   *  @param[in]      op_b  Transpose flag indicator for B
   *  @param[in]      beta      Beta coefficient
   *  @param[in,out]  C     Resulting matrix
   */
  static void Gemm(const Matrix& A,
                   const TransposeType op_a,
                   const T& alpha,
                   const Matrix& B,
                   const TransposeType op_b,
                   const T& beta,
                   Matrix* C);

  /**
   *  @name   Gemm
   *  @fn void Gemm(const Matrix& A,
                   const TransposeType op_a,
                   const T& alpha,
                   const Matrix& B,
                   const TransposeType op_b,
                   const T& beta,
                   const cv::cuda::Stream& stream,
                   Matrix* C)
   *  @brief  Compute the product between two matrices
   *          C = a * AB + b * C
   *  @param[in]      A         Matrix A
   *  @param[in]      op_a      Transpose flag indicator for A
   *  @param[in]      alpha     Alpha coefficient
   *  @param[in]      B         Matrix B
   *  @param[in]      op_b      Transpose flag indicator for B
   *  @param[in]      beta      Beta coefficient
   *  @param[in]      stream    Stream on which to run computation
   *  @param[in,out]  C         Resulting matrix
   */
  static void Gemm(const Matrix& A,
                   const TransposeType op_a,
                   const T& alpha,
                   const Matrix& B,
                   const TransposeType op_b,
                   const T& beta,
                   const cv::cuda::Stream& stream,
                   Matrix* C);



};
}  // namepsace CUDA
}  // namepsace LTS5
#endif //__CUBLAS_WRAPPER__
