/**
 *  @file   lts5/cuda/utils/math/device_math.hpp
 *  @brief  Basic math operation for device code
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   5/1/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_CUDA_DEVICE_MATH__
#define __LTS5_CUDA_DEVICE_MATH__

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 *  @struct Math
 *  @brief  Mathematical operation using cuda function (intrinsic when available)
 *  @author Christophe Ecabert
 *  @date   5/1/18
 *  @tparam T   Data type
 *  @ingroup utils
 */
template<typename T>
struct Math {};

template <>
struct Math<float> {

  /**
   * @name Cos
   * @fn __device__ static float Cos(const float& x)
   * @brief Compute cosine of x
   * @param[in] x   X
   * @return    cos(x)
   */
  __device__
  static float Cos(const float& x) { return __cosf(x);}

  /**
   * @name Sin
   * @fn __device__ static float Sin(const float& x)
   * @brief Compute sine of x
   * @param[in] x   X
   * @return    sin(x)
   */
  __device__
  static float Sin(const float& x) { return __sinf(x);}

  /**
   * @name Tan
   * @fn __device__ static float Tan(const float& x)
   * @brief Compute tangent of x
   * @param[in] x   X
   * @return    tan(x)
   */
  __device__
  static float Tan(const float& x) { return __tanf(x);}

  /**
   * @name Cosh
   * @fn __device__ static float Cosh(const float& x)
   * @brief Compute hyperbolic cosine of x
   * @param[in] x   X
   * @return    cosh(x)
   */
  __device__
  static float Cosh(const float& x) { return coshf(x);}

  /**
   * @name Sinh
   * @fn __device__ static float Sinh(const float& x)
   * @brief Compute hyperbolic sine of x
   * @param[in] x   X
   * @return    sinh(x)
   */
  __device__
  static float Sinh(const float& x) { return sinhf(x);}

  /**
   * @name Tanh
   * @fn __device__ static float Tanh(const float& x)
   * @brief Compute hyperbolic tangent of x
   * @param[in] x   X
   * @return    tanh(x)
   */
  __device__
  static float Tanh(const float& x) { return tanhf(x);}

  /**
   * @name Acos
   * @fn __device__ static float Acos(const float& x)
   * @brief Compute arc cosine of x
   * @param[in] x   X
   * @return    acos(x)
   */
  __device__
  static float Acos(const float& x) { return acosf(x);}

  /**
   * @name Asin
   * @fn __device__ static float Asin(const float& x)
   * @brief Compute arc sine of x
   * @param[in] x   X
   * @return    asin(x)
   */
  __device__
  static float Asin(const float& x) { return asinf(x);}

  /**
   * @name Atan
   * @fn __device__ static float atan(const float& x)
   * @brief Compute arc tangent of x
   * @param[in] x   X
   * @return    atan(x)
   */
  __device__
  static float atan(const float& x) { return atanf(x);}

  /**
   * @name Acosh
   * @fn __device__ static float Acosh(const float& x)
   * @brief Compute hyperbolic arc cosine of x
   * @param[in] x   X
   * @return    acosh(x)
   */
  __device__
  static float Acosh(const float& x) { return acoshf(x);}

  /**
   * @name Asinh
   * @fn __device__ static float Asinh(const float& x)
   * @brief Compute arc hyperbolic sine of x
   * @param[in] x   X
   * @return    asinh(x)
   */
  __device__
  static float Asinh(const float& x) { return asinhf(x);}

  /**
   * @name Atanh
   * @fn __device__ static float Atanh(const float& x)
   * @brief Compute arc hyperbolic tangent of x
   * @param[in] x   X
   * @return    atanh(x)
   */
  __device__
  static float Atanh(const float& x) { return atanhf(x);}

  /**
   * @name Exp
   * @fn __device__ static float Exp(const float& x)
   * @brief Compute exponential of x
   * @param[in] x   X
   * @return    e^(x)
   */
  __device__
  static float Exp(const float& x) { return __expf(x);}

  /**
   * @name Log
   * @fn __device__ static float Log(const float& x)
   * @brief Compute natural logarithm of x
   * @param[in] x   X
   * @return    ln(x)
   */
  __device__
  static float Log(const float& x) { return __logf(x);}

  /**
   * @name Ceil
   * @fn __device__ static float Ceil(const float& x)
   * @brief Compute the ceiling of x
   * @param[in] x   X
   * @return    ceil(x)
   */
  __device__
  static float Ceil(const float& x) { return ceilf(x);}

  /**
   * @name Floor
   * @fn __device__ static float Floor(const float& x)
   * @brief Compute the floor of x
   * @param[in] x   X
   * @return    floor(x)
   */
  __device__
  static float Floor(const float& x) { return floorf(x);}

  /**
   * @name Rounding
   * @fn __device__ static float Round(const float& x)
   * @brief Compute the round of x
   * @param[in] x   X
   * @return    round(x)
   */
  __device__
  static float Round(const float& x) { return roundf(x);}

  /**
   * @name Rounding
   * @fn __device__ static float Abs(const float& x)
   * @brief Compute the absolute value of x
   * @param[in] x   X
   * @return    abs(x)
   */
  __device__
  static float Abs(const float& x) { return fabsf(x);}

  /**
   * @name Min
   * @fn __device__ static float Min(const float& x, const float& y)
   * @brief Compute the min of x / y
   * @param[in] x   X
   * @param[in] y   Y
   * @return    min(x, y)
   */
  __device__
  static float Min(const float& x, const float& y) { return fminf(x, y);}

  /**
   * @name Max
   * @fn __device__ static float Max(const float& x, const float& y)
   * @brief Compute the max of x / y
   * @param[in] x   X
   * @param[in] y   Y
   * @return    max(x, y)
   */
  __device__
  static float Max(const float& x, const float& y) { return fmaxf(x, y);}

  /**
   * @name Sqrt
   * @fn __device__ static float Sqrt(const float& x)
   * @brief Compute the square root of X
   * @param[in] x   X
   * @return    sqrt(x)
   */
  __device__
  static float Sqrt(const float& x) { return sqrtf(x);}
};

template <>
struct Math<double> {
  /**
   * @name Cos
   * @fn __device__ static double Cos(const double& x)
   * @brief Compute cosine of x
   * @param[in] x   X
   * @return    cos(x)
   */
  __device__
  static double Cos(const double& x) { return cos(x);}

  /**
   * @name Sin
   * @fn __device__ static double Sin(const double& x)
   * @brief Compute sine of x
   * @param[in] x   X
   * @return    sin(x)
   */
  __device__
  static double Sin(const double& x) { return sin(x);}

  /**
   * @name Tan
   * @fn __device__ static double Tan(const double& x)
   * @brief Compute tangent of x
   * @param[in] x   X
   * @return    tan(x)
   */
  __device__
  static double Tan(const double& x) { return tan(x);}

  /**
   * @name Cosh
   * @fn __device__ static double Cosh(const double& x)
   * @brief Compute hyperbolic cosine of x
   * @param[in] x   X
   * @return    cosh(x)
   */
  __device__
  static double Cosh(const double& x) { return cosh(x);}

  /**
   * @name Sinh
   * @fn __device__ static double Sinh(const double& x)
   * @brief Compute hyperbolic sine of x
   * @param[in] x   X
   * @return    sinh(x)
   */
  __device__
  static double Sinh(const double& x) { return sinh(x);}

  /**
   * @name Tanh
   * @fn __device__ static double Tanh(const double& x)
   * @brief Compute hyperbolic tangent of x
   * @param[in] x   X
   * @return    tanh(x)
   */
  __device__
  static double Tanh(const double& x) { return tanh(x);}

  /**
   * @name Acos
   * @fn __device__ static double Acos(const double& x)
   * @brief Compute arc cosine of x
   * @param[in] x   X
   * @return    acos(x)
   */
  __device__
  static double Acos(const double& x) { return acos(x);}

  /**
   * @name Asin
   * @fn __device__ static double Asin(const double& x)
   * @brief Compute arc sine of x
   * @param[in] x   X
   * @return    asin(x)
   */
  __device__
  static double Asin(const double& x) { return asin(x);}

  /**
   * @name Atan
   * @fn __device__ static double atan(const double& x)
   * @brief Compute arc tangent of x
   * @param[in] x   X
   * @return    atan(x)
   */
  __device__
  static double atan(const double& x) { return atan(x);}

  /**
   * @name Acosh
   * @fn __device__ static double Acosh(const double& x)
   * @brief Compute hyperbolic arc cosine of x
   * @param[in] x   X
   * @return    acosh(x)
   */
  __device__
  static double Acosh(const double& x) { return acosh(x);}

  /**
   * @name Asinh
   * @fn __device__ static double Asinh(const double& x)
   * @brief Compute arc hyperbolic sine of x
   * @param[in] x   X
   * @return    asinh(x)
   */
  __device__
  static double Asinh(const double& x) { return asinh(x);}

  /**
   * @name Atanh
   * @fn __device__ static double Atanh(const double& x)
   * @brief Compute arc hyperbolic tangent of x
   * @param[in] x   X
   * @return    atanh(x)
   */
  __device__
  static double Atanh(const double& x) { return atanh(x);}

  /**
   * @name Exp
   * @fn __device__ static double Exp(const double& x)
   * @brief Compute exponential of x
   * @param[in] x   X
   * @return    e^(x)
   */
  __device__
  static double Exp(const double& x) { return exp(x);}

  /**
   * @name Log
   * @fn __device__ static double Log(const double& x)
   * @brief Compute natural logarithm of x
   * @param[in] x   X
   * @return    ln(x)
   */
  __device__
  static double Log(const double& x) { return log(x);}

  /**
   * @name Ceil
   * @fn __device__ static double Ceil(const double& x)
   * @brief Compute the ceiling of x
   * @param[in] x   X
   * @return    ceil(x)
   */
  __device__
  static double Ceil(const double& x) { return ceil(x);}

  /**
   * @name Floor
   * @fn __device__ static double Floor(const double& x)
   * @brief Compute the floor of x
   * @param[in] x   X
   * @return    floor(x)
   */
  __device__
  static double Floor(const double& x) { return floor(x);}

  /**
   * @name Rounding
   * @fn __device__ static double Round(const double& x)
   * @brief Compute the round of x
   * @param[in] x   X
   * @return    round(x)
   */
  __device__
  static double Round(const double& x) { return round(x);}

  /**
   * @name Rounding
   * @fn __device__ static double Abs(const double& x)
   * @brief Compute the absolute value of x
   * @param[in] x   X
   * @return    abs(x)
   */
  __device__
  static double Abs(const double& x) { return abs(x);}

  /**
   * @name Min
   * @fn __device__ static double Min(const double& x, const double& y)
   * @brief Compute the min of x / y
   * @param[in] x   X
   * @param[in] y   Y
   * @return    min(x, y)
   */
  __device__
  static double Min(const double& x, const double& y) { return min(x, y);}

  /**
   * @name Max
   * @fn __device__ static double Max(const double& x, const double& y)
   * @brief Compute the max of x / y
   * @param[in] x   X
   * @param[in] y   Y
   * @return    max(x, y)
   */
  __device__
  static double Max(const double& x, const double& y) { return max(x, y);}

  /**
   * @name Sqrt
   * @fn static float Sqrt(const float& x)
   * @brief Compute the square root of X
   * @param[in] x   X
   * @return    sqrt(x)
   */
  __device__
  static float Sqrt(const float& x) { return sqrt(x);}
};

}  // namespace CUDA
}  // namespace LTS5 {
#endif //__LTS5_CUDA_DEVICE_MATH__
