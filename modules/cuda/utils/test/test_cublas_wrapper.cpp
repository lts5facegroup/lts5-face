/**
 *  @file   test_cublas_wrapper.cpp
 *  @brief  Unit test for cuBLAS API
 *
 *  @author Christophe Ecabert
 *  @date   06/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "opencv2/core.hpp"

#include "lts5/cuda/utils/matrix.hpp"
#include "lts5/cuda/utils/math/cublas_wrapper.hpp"


/** List of type to test against */
typedef ::testing::Types<float, double> TypeList;

/**
 *  @class  TestLinearAlgebra
 *  @brief  Class for testing multiple type
 *  @author Christophe Ecabert
 *  @date   06/06/2017
 *  @see    https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md
 */
template<typename T>
class TestLinearAlgebra : public ::testing::Test {
 public:
  TestLinearAlgebra(void) {}
};

/** Intantiate test case */
TYPED_TEST_CASE(TestLinearAlgebra, TypeList);

/** Dot product */
TYPED_TEST(TestLinearAlgebra, Dot) {
  using Matrix = typename LTS5::CUDA::Matrix<TypeParam>;
  // Define ground truth
  cv::Mat A(50, 1, cv::DataType<TypeParam>::type);
  cv::Mat B(50, 1, cv::DataType<TypeParam>::type);
  cv::randn(A, TypeParam(0), TypeParam(2));
  cv::randn(B, TypeParam(0), TypeParam(2));
  TypeParam dot_prod = B.dot(A);

  // Compute on device
  Matrix dA(A), dB(B);
  TypeParam device_prod = LTS5::CUDA::LinearAlgebra<TypeParam>::Dot(dA, dB);

  // Compare
  if (sizeof(TypeParam) == 4) {
    EXPECT_FLOAT_EQ(dot_prod, device_prod);
  } else {
    EXPECT_DOUBLE_EQ(dot_prod, device_prod);
  }
}

/** Matrix vector */
TYPED_TEST(TestLinearAlgebra, MatrixVector) {
  using Matrix = typename LTS5::CUDA::Matrix<TypeParam>;
  using LA = typename LTS5::CUDA::LinearAlgebra<TypeParam>;
  using TType = typename LTS5::CUDA::LinearAlgebra<TypeParam>::TransposeType;
  // Ground truth
  cv::Mat A(20, 35, cv::DataType<TypeParam>::type);
  cv::Mat x1(35, 1, cv::DataType<TypeParam>::type);
  cv::Mat x2(20, 1, cv::DataType<TypeParam>::type);
  cv::randn(A, TypeParam(0), TypeParam(2));
  cv::randn(x1, TypeParam(0), TypeParam(2));
  cv::randn(x2, TypeParam(0), TypeParam(2));

  cv::Mat y1 = A * x1;
  cv::Mat y2 = A.t() * x2;

  // Compute on device : y = A * x1
  Matrix dA(A), dx1(x1), dx2(x2), dy;
  cv::Mat host_y;
  LA::Gemv(dA, TType::kTranspose, TypeParam(1.0), dx1, TypeParam(0), &dy);
  dy.Download(&host_y);
  // Compare
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-14;
  TypeParam diff = (TypeParam)cv::norm(host_y.t() - y1, cv::NORM_INF) / y2.total();
  EXPECT_LT(diff, thr);
  // Compute on device : y = At * x2
  LA::Gemv(dA, TType::kNoTranspose, TypeParam(1.0), dx2, TypeParam(0), &dy);
  dy.Download(&host_y);
  // Compare
  diff = (TypeParam)cv::norm(host_y.t() - y2, cv::NORM_INF) / y2.total();
  EXPECT_LT(diff, thr);
}

/** Matrix Matrix */
TYPED_TEST(TestLinearAlgebra, MatrixMatrix) {
  using Matrix = typename LTS5::CUDA::Matrix<TypeParam>;
  using LA = typename LTS5::CUDA::LinearAlgebra<TypeParam>;
  using TType = typename LTS5::CUDA::LinearAlgebra<TypeParam>::TransposeType;
  // Ground truth
  cv::Mat A1(20, 35, cv::DataType<TypeParam>::type);
  cv::Mat A2(35, 20, cv::DataType<TypeParam>::type);
  cv::Mat B1(35, 15, cv::DataType<TypeParam>::type);
  cv::Mat B2(15, 35, cv::DataType<TypeParam>::type);
  cv::randn(A1, TypeParam(0), TypeParam(2));
  cv::randn(A2, TypeParam(0), TypeParam(2));
  cv::randn(B1, TypeParam(0), TypeParam(2));
  cv::randn(B2, TypeParam(0), TypeParam(2));

  cv::Mat C1 = A1 * B1;
  cv::Mat C2 = A1 * B2.t();
  cv::Mat C3 = A2.t() * B1;
  cv::Mat C4 = A2.t() * B2.t();

  // Compute on device : C = A * B
  // ------------------------------------------------------
  Matrix dA1(A1), dA2(A2), dB1(B1), dB2(B2), dC;
  cv::Mat host_C;
  LA::Gemm(dA1, TType::kTranspose, TypeParam(1),
           dB1, TType::kTranspose, TypeParam(0),
           &dC);
  dC.Download(&host_C);
  // Compare
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-14;
  TypeParam diff = (TypeParam)cv::norm(host_C.t() - C1, cv::NORM_INF) / C1.total();
  EXPECT_LT(diff, thr);

  // Compute on device : C = A * Bt
  // ------------------------------------------------------
  LA::Gemm(dA1, TType::kTranspose, TypeParam(1),
           dB2, TType::kNoTranspose, TypeParam(0),
           &dC);
  dC.Download(&host_C);
  // Compare
  diff = (TypeParam)cv::norm(host_C.t() - C2, cv::NORM_INF) / C2.total();
  EXPECT_LT(diff, thr);

  // Compute on device : C = At * B
  // ------------------------------------------------------
  LA::Gemm(dA2, TType::kNoTranspose, TypeParam(1),
           dB1, TType::kTranspose, TypeParam(0),
           &dC);
  dC.Download(&host_C);
  // Compare
  diff = (TypeParam)cv::norm(host_C.t() - C3, cv::NORM_INF) / C3.total();
  EXPECT_LT(diff, thr);

  // Compute on device : C = At * Bt
  // ------------------------------------------------------
  LA::Gemm(dA2, TType::kNoTranspose, TypeParam(1),
           dB2, TType::kNoTranspose, TypeParam(0),
           &dC);
  dC.Download(&host_C);
  // Compare
  diff = (TypeParam)cv::norm(host_C.t() - C4, cv::NORM_INF) / C4.total();
  EXPECT_LT(diff, thr);

}
int main(int argc, const char * argv[]) {
  // Init google test
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  // Run test
  return  RUN_ALL_TESTS();
}