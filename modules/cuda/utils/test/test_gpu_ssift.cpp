/**
 *  @file   test_gpu_ssift.cpp
 *  @brief  Unit Test for SSIFT gpu
 *
 *  @author Christophe Ecabert
 *  @date   18/05/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <numeric>
#include <random>
#include <chrono>

#include <cuda_runtime.h>
#include <cuda_profiler_api.h>
#include <thread>


#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/cuda.hpp"
#include "opencv2/cudafilters.hpp"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/ssift.hpp"
#include "lts5/cuda/utils/ssift.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/separable_filter.hpp"
#include "lts5/utils/file_io.hpp"

static cv::Mat s_input_image;

/**
 *  @class  GPUSSiftTest
 *  @brief  Test unit for GPU SSift implementation
 *          Input/Output are CPU container (cv::Mat)
 *  @author Christophe Ecabert
 *  @date   18/05/16
 */
class GPUSSiftTest : public ::testing::TestWithParam<int> {
public:

  GPUSSiftTest(void) : n_pts_(32), key_points_(n_pts_, cv::KeyPoint()) {
    test_instance_ = GetParam();
  }

  void ComputeCPUSSift(void) {
    //sift_params_.sift_root = false;
    auto t0 = cv::getTickCount();
    LTS5::SSift::ComputeDescriptorOpt(s_input_image,
                                      key_points_,
                                      sift_params_,
                                      &cpu_sift_);
    auto t1 = cv::getTickCount();
    std::cout << "cpu : " << (t1-t0) * 1000.0 / cv::getTickFrequency() << " ms" << std::endl;
  }

  void ComputeGPUSSift(void) {
    // Upload
    cv::cuda::GpuMat input_image(s_input_image);
    LTS5::CUDA::Matrix<float> gpu_sift;
    LTS5::CUDA::GPUSSift::GPUCache cache(4, key_points_.size());
    // Init
    int  err = LTS5::CUDA::GPUSSift::ComputeDescriptor(input_image,
                                                       key_points_,
                                                       sift_params_,
                                                       &cache,
                                                       &gpu_sift);
    // Test
    auto t0 = cv::getTickCount();
    err = LTS5::CUDA::GPUSSift::ComputeDescriptor(input_image,
                                                  key_points_,
                                                  sift_params_,
                                                  &cache,
                                                  &gpu_sift);
    auto t1 = cv::getTickCount();
    std::cout << "gpu : " << (t1-t0) * 1000.0 / cv::getTickFrequency() << " ms" << std::endl;

    gpu_sift.Download(&gpu_sift_);
    ASSERT_TRUE(err == 0);
  }

  void ComputeParallelGPUSSift(cv::Mat* sift0, cv::Mat* sift1) {
    using Matrixf = LTS5::CUDA::Matrix<float>;
    using Cache = LTS5::CUDA::GPUSSift::GPUCache;

    Cache cache0(4, key_points_.size());
    Cache cache1(4, key_points_.size());

    auto fcn = [&](Matrixf* sift, double * t, Cache* cache)-> int {
      cv::cuda::GpuMat input_image(s_input_image);
      // Init
      int  err = LTS5::CUDA::GPUSSift::ComputeDescriptor(input_image,
                                                         key_points_,
                                                         sift_params_,
                                                         cache,
                                                         sift);
      // Test
      auto t0 = cv::getTickCount();
      err = LTS5::CUDA::GPUSSift::ComputeDescriptor(input_image,
                                                    key_points_,
                                                    sift_params_,
                                                    cache,
                                                    sift);
      auto t1 = cv::getTickCount();
      *t = (t1-t0) * 1000.0 / cv::getTickFrequency();
      return err;
    };

    // Start CPU computation
    this->ComputeCPUSSift();
    // Start GPU computation
    Matrixf sift_t0, sift_t1;
    double dt0, dt1;
    std::thread t0(fcn, &sift_t0, &dt0, &cache0);
    t0.join();
    std::thread t1(fcn, &sift_t1, &dt1, &cache1);
    //t0.join();
    t1.join();
    std::cout << "gpu (avg) : " << (dt0 + dt1)/2.0 << " ms" << std::endl;



    // Download
    sift_t0.Download(sift0);
    sift_t1.Download(sift1);
  }

  void Compare(int* n_diff, float* max_diff) {
    int sz = cpu_sift_.cols * cpu_sift_.rows;
    int n = 0;
    float max_d = 0.f;
    for (int i = 0; i < sz; ++i) {
      float diff = std::abs(cpu_sift_.at<float>(i) - gpu_sift_.at<float>(i));
      if(diff > 1e-4) {
        n += 1;
      }
      if (diff > max_d) {
        max_d = diff;
      }
    }
    *max_diff = max_d;
    *n_diff = n;
  }

  /** N sift pts */
  int n_pts_;
  /** Sample location */
  std::vector<cv::KeyPoint> key_points_;
  /** SSift param */
  LTS5::SSift::SSiftParameters sift_params_;
  /** CPU sift */
  cv::Mat cpu_sift_;
  /** GPU sift */
  cv::Mat gpu_sift_;
  /** Test instance */
  int test_instance_;

protected:

  void SetUp(void) {
    // code here will execute just before the test ensues

    // Generate random pts
    unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now()
                                          .time_since_epoch()
                                          .count());
    std::mt19937_64 engine(seed);
    std::uniform_int_distribution<int> dist_w(80, s_input_image.cols - 80);
    std::uniform_int_distribution<int> dist_h(80, s_input_image.rows - 80);
    for (int p = 0; p < n_pts_; ++p) {
      int u = dist_w(engine);
      int v = dist_h(engine);
      key_points_[p].pt.x = static_cast<float>(u);
      key_points_[p].pt.y = static_cast<float>(v);
      key_points_[p].size = 32;
    }
  }

  void TearDown(void) {
    using namespace std::chrono;
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
    if (this->HasFatalFailure()) {
      // Save into file for further investigation
      // Get data
      char buff[50];
      auto now = system_clock::to_time_t(system_clock::now());
      std::strftime(buff, sizeof(buff), "%F-%T", std::localtime(&now));
      std::string date(buff);
      // Save data
      LTS5::SaveMatToBin(date + "-cpu-sift.bin", cpu_sift_);
      LTS5::SaveMatToBin(date + "-gpu-sift.bin", gpu_sift_);
    }
  }
};

/** Parameters for filter testing <img dim, kernel size> */
using FilterParams = std::pair<int, int>;

/**
 * @class   LTS5Filter
 * @brief   Test unit for gpu filter engine
 * @author  Christophe Ecabert
 * @date    30/05/17
 */
class LTS5Filter : public ::testing::TestWithParam<FilterParams> {
 public:

  LTS5Filter(void) {
    // Recover dimension
    auto p = GetParam();
    dim_ = p.first;
    ksize_ = p.second;

    // Create kernel
    kernel_.create(ksize_, 1, CV_32FC1);
    kernel_.setTo(0.f);
    switch (ksize_) {
      case 1: kernel_.at<float>(0) = 1.f;
        break;

      case 3: {
        kernel_.at<float>(0) = 0.25f;
        kernel_.at<float>(1) = 0.5f;
        kernel_.at<float>(2) = 0.25f;
      } break;

      case 5: {
        kernel_.at<float>(0) = 0.1f;
        kernel_.at<float>(1) = 0.2f;
        kernel_.at<float>(2) = 0.4f;
        kernel_.at<float>(3) = 0.2f;
        kernel_.at<float>(4) = 0.1f;
      } break;

      default:
        std::cout << "Error wrong kernel size" << std::endl;
    }

    // Generate data
    int b = 5;
    cv::Rect roi = cv::Rect(b, b, dim_ - (2 * b), dim_ - (2 * b));
    data_.create(dim_, dim_, CV_32FC1);
    data_.setTo(0.f);
    cv::randn(data_(roi), 0.f, 2.f);
  }

 protected:

  /** Synthetic data generated */
  cv::Mat data_;
  /** Dimension */
  int dim_;
  /** kernel size */
  int ksize_;
  /** kernel */
  cv::Mat kernel_;
};

TEST(DataTransfer, DISABLED_GPUMatrix) {
  // Generate data
  cv::Mat data(4, 6, CV_32FC1);
  cv::randn(data, 0.f, 2.f);
  // push to gpu
  LTS5::CUDA::Matrix<float> gpu_data;
  gpu_data.Upload(data);
  // download
  cv::Mat download_data;
  gpu_data.Download(&download_data);

  // Compare matrix dimension
  EXPECT_TRUE(data.rows == download_data.rows);
  EXPECT_TRUE(data.cols == download_data.cols);

  // Compare matrix content
  std::vector<float> true_data(reinterpret_cast<float*>(data.data),
                               reinterpret_cast<float*>(data.data) + data.total());
  ASSERT_THAT(true_data,
              ::testing::ElementsAreArray(reinterpret_cast<float*>(download_data.data),
                                          download_data.total()));
}

TEST_P(LTS5Filter, DISABLED_Filter) {
  // LTS5 filter engine
  LTS5::CUDA::SeparableFilter<float> filter(kernel_.ptr<float>(),
                                            ksize_,
                                            cv::cuda::Stream());
  // LTS5 OCV engine as ground truth
  cv::Ptr<cv::cuda::Filter> filter_ocv;
  filter_ocv = cv::cuda::createSeparableLinearFilter(CV_32FC1,
                                                     CV_32FC1,
                                                     kernel_,
                                                     kernel_,
                                                     cv::Point(-1,-1),
                                                     cv::BORDER_CONSTANT,
                                                     cv::BORDER_CONSTANT);
  // uploaded
  cv::cuda::GpuMat data_device, dest_device;
  data_device.upload(data_);
  dest_device.create(dim_, dim_, CV_32FC1);
  dest_device.setTo(0.f);

  // LTS
  cv::Mat res_lts;
  filter.Apply(data_device, cv::cuda::Stream(), &dest_device);
  dest_device.download(res_lts);
  // Check dimension + type
  EXPECT_TRUE(res_lts.cols == dim_ &&
              res_lts.rows == dim_ &&
              res_lts.type() == CV_32FC1);
  // OCV
  cv::Mat res_ocv;
  filter_ocv->apply(data_device, dest_device);
  dest_device.download(res_ocv);
  double diff = cv::norm(res_lts - res_ocv);
  EXPECT_DOUBLE_EQ(diff, 0.0);
}

TEST_P(GPUSSiftTest, ValidationTest) {
  // Compute ground truth
  this->ComputeCPUSSift();
  // Compute GPU
  this->ComputeGPUSSift();
  // Compare
  float diff = cv::norm(cpu_sift_ - gpu_sift_, cv::NORM_INF) / cpu_sift_.total();
  int n_diff = 0;
  float delta = 0.f;
  this->Compare(&n_diff, &delta);
  std::cout << "#diff : " << n_diff << std::endl;
  std::cout << "diff : " << diff << std::endl;
  std::cout << "max delta : " << delta << std::endl;
  EXPECT_LT(n_diff, cpu_sift_.total() * 0.01);
  EXPECT_LT(diff, 1e-6);
}

TEST_P(GPUSSiftTest, /*DISABLED_*/ParallelValidationTest) {
  cv::Mat sift0, sift1;
  this->ComputeParallelGPUSSift(&sift0, &sift1);

  double diff0 = cv::norm(sift0 - this->cpu_sift_, cv::NORM_INF) / sift0.total();
  double diff1 = cv::norm(sift1 - this->cpu_sift_, cv::NORM_INF) / sift1.total();
  double err = cv::norm(sift0 - sift1, cv::NORM_INF) / sift1.total();

  std::cout << "diff0 : " << diff0 << std::endl;
  std::cout << "diff1 : " << diff1 << std::endl;

  int i = GetParam();


  /*LTS5::SaveMatToBin(std::to_string(i) + "-cpu-sift.bin", cpu_sift_);
  LTS5::SaveMatToBin(std::to_string(i) + "-gpu-sift0.bin", sift0);
  LTS5::SaveMatToBin(std::to_string(i) + "-gpu-sift1.bin", sift1);*/


  /*EXPECT_LT(diff0, 1e-6);
  EXPECT_LT(diff1, 1e-6);
  EXPECT_LT(err, 1e-6);*/
  EXPECT_FLOAT_EQ(diff1, diff0);
}

INSTANTIATE_TEST_CASE_P(GPU,
                        GPUSSiftTest,
                        ::testing::Range(0, 10));
INSTANTIATE_TEST_CASE_P(GPUFilter,
                        LTS5Filter,
                        ::testing::Values(FilterParams(64, 1),
                                          FilterParams(64, 3),
                                          FilterParams(64, 5),
                                          FilterParams(128, 1),
                                          FilterParams(128, 3),
                                          FilterParams(128, 5),
                                          FilterParams(192, 1),
                                          FilterParams(192, 3),
                                          FilterParams(192, 5),
                                          FilterParams(256, 1),
                                          FilterParams(256, 3),
                                          FilterParams(256, 5),
                                          FilterParams(320, 1),
                                          FilterParams(320, 3),
                                          FilterParams(320, 5)));

int main(int argc, const char * argv[]) {

  LTS5::CmdLineParser parser;
  parser.AddArgument("-i",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Image path");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Init google test
    ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
    // Get option
    std::string image_path;
    parser.HasArgument("-i", &image_path);
    // Load data
    s_input_image = cv::imread(image_path, cv::IMREAD_GRAYSCALE);
    if (!s_input_image.empty()) {
      // Run test
      err = ::RUN_ALL_TESTS();
    } else {
      err = -1;
      std::cout << "Unable to load image : " << image_path << std::endl;
    }
  } else {
    std::cout << "Unable to parse cmd line" << std::endl;
  }
  return err;
}
