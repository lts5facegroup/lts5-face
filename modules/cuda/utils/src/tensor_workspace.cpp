/**
 *  @file   tensor_workspace.cpp
 *  @brief  Helper class for tensor workspace
 *  @ingroup    utils
 *
 *  @author Christophe Ecabert
 *  @date   23/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/cuda/utils/tensor_workspace.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/math/cudnn_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Initialization

/*
 * @name  TensorWorkspace
 * @fn    TensorWorkspace(void)
 * @brief Constructor
 */
template<typename T>
TensorWorkspace<T>::TensorWorkspace(void) : buffer_(nullptr),
                                            size_(0),
                                            ref_(nullptr) {
  ref_ = new std::atomic<unsigned>(1);
}

/*
 * @name  TensorWorkspace
 * @fn    TensorWorkspace(const size_t size)
 * @brief Constructor
 * @param[in] size    Workspace dimension
 */
template<typename T>
TensorWorkspace<T>::TensorWorkspace(const size_t size) : buffer_(nullptr),
                                                         size_(0),
                                                         ref_(nullptr) {
  ref_ = new std::atomic<unsigned>(1);
  this->Create(size);
}

/*
 * @name  TensorWorkspace
 * @fn    TensorWorkspace(const TensorWorkspace<T>& other)
 * @brief Copy Constructor
 * @param[in] other   Object to copy from
 */
template<typename T>
TensorWorkspace<T>::TensorWorkspace(const TensorWorkspace<T>& other) :
        buffer_(other.buffer_),
        size_(other.size_),
        ref_(other.ref_) {
  this->Retain();
}

/*
 * @name  operator=
 * @fn    TensorWorkspace<T>& operator=(const TensorWorkspace<T>& rhs)
 * @brief Assignment operator
 * @param[in] rhs Object to assign from
 * @return    Newly assigned object
 */
template<typename T>
TensorWorkspace<T>& TensorWorkspace<T>::operator=(const TensorWorkspace<T>& rhs) {
  if (this != &rhs) {
    this->Release();
    buffer_ = rhs.buffer_;
    size_ = rhs.size_;
    ref_ = rhs.ref_;
    this->Retain();
  }
  return *this;
}

/*
 * @name  ~TensorWorkspace
 * @fn    ~TensorWorkspace(void)
 * @brief Destructor
 */
template<typename T>
TensorWorkspace<T>::~TensorWorkspace(void) {
  this->Release();
}

/*
 * @name  Create
 * @fn    void Create(const size_t size)
 * @brief Create workspace of a given size
 * @param[in] size    Workspace size in bytes needed.
 */
template<typename T>
void TensorWorkspace<T>::Create(const size_t size) {
  if (size != size_) {
    size_ = size;
    if (buffer_) {
      cudaSafeCall(cudaFree(buffer_));
    }
    cudaSafeCall(cudaMalloc(&buffer_, size_));
  }
}

#pragma mark -
#pragma mark Usage

/*
 * @name
 * @fn    static size_t ReductionSize(const TensorRedOp<T>& red,
                                      const Tensor<T>& A,
                                      const Tensor<T>& B);
 * @brief Compute the workspace size needed to perform a given reduction
 *        beteween tensor A and B.
 * @param red
 * @param A
 * @param B
 * @return
 */
template<typename T>
void TensorWorkspace<T>::ReductionSize(const TensorRedOp<T>& red,
                                       const Tensor<T>& A,
                                       const Tensor<T>& B,
                                       size_t* size) {
  auto* h = CudnnContext::Instance().Get();
  cudnnSafeCall(cudnnGetReductionWorkspaceSize(h,
                                               red.red,
                                               A.descriptor(),
                                               B.descriptor(),
                                               size));
}

/*
 * @name  ConvolutionSize
 * @fn    static void ConvolutionSize(const Tensor<T>& input,
                                      const Tensor<T>& output,
                                      TensorConvOp<T>* conv,
                                      size_t* size)
 * @brief Finalize convolution operation and compute workspace dimension
 *        required to perform operation
 * @param[in] input       Input data to be filtered
 * @param[in] output      Output filtered data
 * @param[in,out] conv    Convolution operation to perform
 * @param[out] size       Required workspace dimension
 */
template<typename T>
void TensorWorkspace<T>::ConvolutionSize(const Tensor<T>& input,
                                         const Tensor<T>& output,
                                         TensorConvOp<T>* conv,
                                         size_t* size) {
  auto* h = CudnnContext::Instance().Get();
  // Get convolution algorithm
  cudnnConvolutionFwdAlgo_t algo;
  cudnnSafeCall(cudnnGetConvolutionForwardAlgorithm(h,
                                                    input.descriptor(),
                                                    conv->filter_desc,
                                                    conv->conv_desc,
                                                    output.descriptor(),
                                                    CUDNN_CONVOLUTION_FWD_PREFER_FASTEST,
                                                    0,
                                                    &algo));
  conv->algo = static_cast<int>(algo);
  // Query for workspace
  cudnnSafeCall(cudnnGetConvolutionForwardWorkspaceSize(h,
                                                        input.descriptor(),
                                                        conv->filter_desc,
                                                        conv->conv_desc,
                                                        output.descriptor(),
                                                        algo,
                                                        size));
}

#pragma mark -
#pragma mark Private

/*
 *  @name Retain
 *  @fn void Retain(void)
 *  @brief  Increase reference counter
 *  @see https://stackoverflow.com/questions/10268737/
 */
template<typename T>
void TensorWorkspace<T>::Retain(void) {

}

/*
 *  @name Release
 *  @fn void Release(void)
 *  @brief  Release object memory if reference counter reach zero
 *  @see https://stackoverflow.com/questions/10268737/
 */
template<typename T>
void TensorWorkspace<T>::Release(void) {
  if (std::atomic_fetch_sub_explicit(ref_,
                                     1u,
                                     std::memory_order_release) == 1) {
    std::atomic_thread_fence(std::memory_order_acquire);
    if (buffer_) {
      cudaSafeCall(cudaFree(buffer_));
      buffer_ = nullptr;
    }
    if (ref_) {
      delete ref_;
      ref_ = nullptr;
    }
  }
}

#pragma mark -
#pragma mark Implicit declaration
/** TensorWorkspace - float */
template class TensorWorkspace<float>;
/** TensorWorkspace - double */
template class TensorWorkspace<double>;
/** TensorWorkspace - int32 */
template class TensorWorkspace<int32_t>;

}  // namepsace CUDA
}  // namepsace LTS5