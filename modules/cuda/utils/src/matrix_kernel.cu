/**
 *  @file   matrix_kernel.cu
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   07/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "opencv2/core/cuda_stream_accessor.hpp"

#include "lts5/cuda/utils/device/matrix_kernel.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 *  @namespace  device
 *  @brief      CUDA Device code
 */
namespace device {

/**
 * @name    set_to
 * @fn  void set_to(const T value)
 * @brief   Initialize matrix buffer with a given value
 * @tparam T    Data type
 * @param[in] value Value to set
 * @param[in] N     Array dimension
 * @param[in] array Buffer to fill
 */
template<>
__global__
void set_to(const float value, const int N, float* array) {
  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < N) {
    array[i] = value;
  }
}

template<>
__global__
void set_to(const double value, const int N, double* array) {
  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < N) {
    array[i] = value;
  }
}

/*
 * @name    convert_to
 * @fn  void void convert_to(const T* src, const int N, D* dest);
 * @brief   Convert a given array into a different type
 * @tparam T    Source type
 * @tparam D    Destination type
 * @param[in] src       Source array
 * @param[in] N         Array dimension
 * @param[out] dest     Converted array
 */
template<>
__global__
void convert_to(const float* src, const int N, double* dest) {
  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < N) {
    dest[i] = double(src[i]);
  }
};

template<>
__global__
void convert_to(const double* src, const int N, float* dest) {
  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < N) {
    dest[i] = float(src[i]);
  }
};
}  // namepsace device


/*
 * @name    ConvertTo
 * @fn  void ConvertTo(const T* src, const int N,
                       const cv::cuda::Stream& stream, D* dest)
 * @brief   Convert a given array into a different type
 * @tparam T    Source type
 * @tparam D    Destination type
 * @param[in] src       Source array
 * @param[in] N         Array dimension
 * @param[in] stream    Stream on which to lauch computation
 * @param[out] dest     Converted array
 */
template<>
void ConvertTo(const float* src, const int N,
               const cv::cuda::Stream& stream, double* dest) {
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Define dimension
  int thd = 128;
  int  bck = (N + 127) / 128;
  device::convert_to<<<bck, thd, 0, s>>>(src, N, dest);
}

template<>
void ConvertTo(const double* src, const int N,
               const cv::cuda::Stream& stream, float* dest) {
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Define dimension
  int thd = 128;
  int  bck = (N + 127) / 128;
  device::convert_to<<<bck, thd, 0, s>>>(src, N, dest);
}

/**
 * @name    SetTo
 * @fn  void SetTo(const T value, const int N,
                   const cv::cuda::Stream& stream, T* array)
 * @brief   Initialize matrix with a given value
 * @tparam T    Data type
 * @param[in] value Value to set
 * @param[in] N     Array dimension
 * @param[in] stream    Stream on which to lauch computation
 * @param[in] array Buffer to fill
 */
template<>
void SetTo(const float value,
           const int N,
           const cv::cuda::Stream& stream,
           float* array) {
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Define dimension
  int thd = 128;
  int  bck = (N + 127) / 128;
  device::set_to<<<bck, thd, 0, s>>>(value, N, array);
}

template<>
void SetTo(const double value,
           const int N,
           const cv::cuda::Stream& stream,
           double* array) {
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Define dimension
  int thd = 128;
  int  bck = (N + 127) / 128;
  device::set_to<<<bck, thd, 0, s>>>(value, N, array);
}

}  // namepsace CUDA
}  // namepsace LTS5