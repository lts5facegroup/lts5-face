/**
 *  @file   tensor_operation.cpp
 *  @brief  Various operation on tensor
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   22/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/cuda/utils/tensor_operation.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/math/cudnn_utils.hpp"
/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Tensor Operation

/*
 * @name  TensorOp
 * @fn    TensorOp(const TensorOpType& type)
 * @brief   Constructor
 * @param[in] type Type of tensor operation
 */
template<typename T>
TensorOp<T>::TensorOp(const TensorOpType& type) : op(nullptr) {
  cudnnSafeCall(cudnnCreateOpTensorDescriptor(&op));
  cudnnSafeCall(cudnnSetOpTensorDescriptor(op,
                                           OpSelector::Select(type),
                                           DataType<T>::value,
                                           CUDNN_PROPAGATE_NAN));
}

/*
 * @name    ~TensorOp
 * @fn  ~TensorOp(void)
 * @brief   Destructor
 */
template<typename T>
TensorOp<T>::~TensorOp(void) {
  if (op) {
    cudnnSafeCall(cudnnDestroyOpTensorDescriptor(op));
    op = nullptr;
  }
}



#pragma mark -
#pragma mark Tensor Reduction

/*
 * @name  TensorRedOp
 * @fn    TensorRedOp(const TensorRedType& type)
 * @brief   Constructor
 * @param[in] type Type of tensor reduction operation
 */
template<typename T>
TensorRedOp<T>::TensorRedOp(const TensorRedType& type) {
  cudnnSafeCall(cudnnCreateReduceTensorDescriptor(&red));
  cudnnSafeCall(cudnnSetReduceTensorDescriptor(red,
                                               RedSelector::Select(type),
                                               DataType<T>::value,
                                               CUDNN_PROPAGATE_NAN,
                                               CUDNN_REDUCE_TENSOR_NO_INDICES,
                                               CUDNN_32BIT_INDICES));

}

/*
 * @name    ~TensorRedOp
 * @fn  ~TensorRedOp(void)
 * @brief   Destructor
 */
template<typename T>
TensorRedOp<T>::~TensorRedOp(void) {
  if(red) {
    cudnnSafeCall(cudnnDestroyReduceTensorDescriptor(red));
    red = nullptr;
  }
}

#pragma mark -
#pragma mark Tensor Convolution Operation

/*
 * @name  TensorConvOp
 * @fn    TensorConvOp(void)
 * @brief Constructor
 */
template<typename T>
TensorConvOp<T>::TensorConvOp(void) : filter_desc(nullptr),
                                      conv_desc(nullptr) {
  // Init descriptor
  cudnnSafeCall(cudnnCreateFilterDescriptor(&filter_desc));
  cudnnSafeCall(cudnnCreateConvolutionDescriptor(&conv_desc));
}

/*
 * @name  TensorConvOp
 * @fn    TensorConvOp(const Tensor<T>* filter,
                       const ConvParameter& parameter)
 * @brief Constructor
 * @param[in] filter  Filter to use for convolution (reference to filter
 *                    will be hold)
 * @param[in] parameter   Convolution parameters
 */
template<typename T>
TensorConvOp<T>::TensorConvOp(const Tensor<T>* filter,
                              const ConvParameter& parameter) :
        filter_desc(nullptr),
        conv_desc(nullptr) {
  // Init descriptor
  cudnnSafeCall(cudnnCreateFilterDescriptor(&filter_desc));
  cudnnSafeCall(cudnnCreateConvolutionDescriptor(&conv_desc));
  // Configure descriptors
  this->Create(filter, parameter);
}

/*
 * @name  ~TensorConvOp
 * @fn    ~TensorConvOp(void)
 * @brief Destructor
 */
template<typename T>
TensorConvOp<T>::~TensorConvOp(void) {
  if (filter_desc) {
    cudnnSafeCall(cudnnDestroyFilterDescriptor(filter_desc));
    filter_desc = nullptr;
  }
  if (conv_desc) {
    cudnnSafeCall(cudnnDestroyConvolutionDescriptor(conv_desc));
    conv_desc = nullptr;
  }
}

/*
 * @name  Create
 * @fn    void Create(const Tensor<T>* filter, const ConvParameter& parameter)
 * @brief Create/Init convolution operation
 * @param[in] filter      Filter to use for convolution (reference to filter
 *                        will be hold)
 * @param[in] parameter   Convolution parameters
 */
template<typename T>
void TensorConvOp<T>::Create(const Tensor<T>* filter,
                             const ConvParameter& parameter) {
  // Configure filter
  cudnnSafeCall(cudnnSetFilter4dDescriptor(filter_desc,
                                           DataType<T>::value,
                                           CUDNN_TENSOR_NCHW,
                                           1,
                                           filter->depth(),
                                           filter->rows(),
                                           filter->cols()));
  this->filter = filter;
  // Convolution
  cudnnSafeCall(cudnnSetConvolution2dDescriptor(conv_desc,
                                                parameter.pad_v,
                                                parameter.pad_h,
                                                parameter.stride_v,
                                                parameter.stride_h,
                                                parameter.dilation_v,
                                                parameter.dilation_h,
                                                ConvSelector::Select(parameter.type),
                                                DataType<T>::value));
}

/*
 * @name  GetOutputDimension
 * @fn    void GetOutputDimension(const Tensor<T>& data,
                                  int* depths,
                                  int* rows,
                                  int* cols) const
 * @brief Compute the dimension of the ouput tensor holding filtered data.
 * @param[in] data    Input's data to be filtered
 * @param[out] depths Output's depth
 * @param[out] rows   Output's rows
 * @param[out] cols   Output's cols
 */
template<typename T>
void TensorConvOp<T>::GetOutputDimension(const Tensor<T>& data,
                                         int* depths,
                                         int* rows,
                                         int* cols) const {
  int n = 0;
  cudnnSafeCall(cudnnGetConvolution2dForwardOutputDim(conv_desc,
                                                      data.descriptor(),
                                                      filter_desc,
                                                      &n,
                                                      depths,
                                                      rows,
                                                      cols));
  assert(n == 1);
}

#pragma mark -
#pragma mark Explicit declaration

/** TensorOp - float */
template struct TensorOp<float>;
/** TensorOp - double */
template struct TensorOp<double>;
/** TensorOp - int32 */
template struct TensorOp<int32_t>;

/** TensorRedOp - float */
template struct TensorRedOp<float>;
/** TensorRedOp - double */
template struct TensorRedOp<double>;
/** TensorRedOp - int32 */
template struct TensorRedOp<int32_t>;

/** TensorConvOp - float */
template struct TensorConvOp<float>;
/** TensorConvOp - double */
template struct TensorConvOp<double>;
/** TensorConvOp - int32 */
template struct TensorConvOp<int32_t>;

}  // namepsace CUDA
}  // namepsace LTS5