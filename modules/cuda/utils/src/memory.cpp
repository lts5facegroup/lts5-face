/**
 *  @file   lts5/cuda/utils/memory.cpp
 *  @brief  Memory blob located on the device
 *  @ingroup utils
 *
 *  @date   2/11/20
 */

/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2011, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *  Author: Anatoly Baskeheev, Itseez Ltd, (myname.mysurname@mycompany.com)
 */

#include "cuda_runtime.h"

#include "lts5/cuda/utils/memory.hpp"
#include "lts5/cuda/utils/safe_call.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Initialization
/*
 * @name  MemoryBlob
 * @fn    MemoryBlob()
 * @brief Constructor
 */
MemoryBlob::MemoryBlob() : data_(nullptr), n_bytes_(0) {}

/**
 * @name  MemoryBlob
 * @fn    explicit MemoryBlob(const size_t& n_bytes)
 * @brief Constructor
 * @param[in] n_bytes Number of bytes to allocate on the device
 */
MemoryBlob::MemoryBlob(const size_t& n_bytes) : data_(nullptr), n_bytes_(0) {
  this->Create(n_bytes);
}

/*
 * @name  ~MemoryBlob
 * @fn    ~MemoryBlob()
 * @brief Destructor
 */
MemoryBlob::~MemoryBlob() {
  if (this->Unref()) {
    // Release data
    cudaSafeCall(cudaFree(data_));
  }
}

/*
 * @name  MemoryBlob
 * @fn    MemoryBlob(const MemoryBlob& other)
 * @brief Copy constructor
 * @param[in] other Object to copy from
 */
MemoryBlob::MemoryBlob(const MemoryBlob& other) : RefCounted::RefCounted(other),
                                                  data_(other.data_),
                                                  n_bytes_(other.n_bytes_) {

}

/*
 * @name  operator=
 * @fn    MemoryBlob& operator=(const MemoryBlob& rhs)
 * @brief Assignment operator
 * @param[in] rhs Object to assign from
 * @return Newly assigned object
 */
MemoryBlob& MemoryBlob::operator=(const MemoryBlob& rhs) {
  if (this != &rhs) {
    if (this->RefCountIsOne() && data_) {
      cudaSafeCall(cudaFree(data_));
    }
    RefCounted::operator=(rhs); // Handle ref count
    data_ = rhs.data_;
    n_bytes_ = rhs.n_bytes_;
  }
  return *this;
}

/*
 * @name  Create
 * @fn    void Create(const size_t& n_bytes)
 * @brief Create a memory blob for a given size if needed.
 * @param[in] n_bytes Number of bytes to allocate
 */
void MemoryBlob::Create(const size_t& n_bytes) {
  // Already proper dims ?
  if (n_bytes != n_bytes_) {
    if (n_bytes > 0) {
      if (this->Unref() && data_) {
        cudaSafeCall(cudaFree(data_));

      }
      this->Initialize(); // Create a new ref counter.
      cudaSafeCall(cudaMalloc(&data_, n_bytes)); // Allocate new buffer
      n_bytes_ = n_bytes;
    }
  }
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Upload
 * @fn    void Upload(const void* data, const size_t& n_bytes)
 * @brief Copy data from host to the device
 * @param[in] data    Host array to copy to device
 * @param[in] n_bytes Number of bytes to copy to the device
 */
void MemoryBlob::Upload(const void* data,
                        const size_t& n_bytes) {
  this->Create(n_bytes);  // Allocate buffer if not already done
  cudaSafeCall(cudaMemcpy(data_, data, n_bytes, cudaMemcpyHostToDevice));
  cudaSafeCall(cudaDeviceSynchronize());
}

/*
 * @name  Download
 * @fn    void Download(void* data) const
 * @brief Copy data from device to host
 * @param[in] data Pointer where to copy the data. Must have proper
 *                 dimensions to accept the whole memory blob
 */
void MemoryBlob::Download(void* data) const {
  cudaSafeCall(cudaMemcpy(data, data_, n_bytes_, cudaMemcpyDeviceToHost));
  cudaSafeCall(cudaDeviceSynchronize());
}

/**
 * @name  Empty
 * @fn    bool Empty() const
 * @brief Indicate if buffer is allocated
 * @return  Returns true if unallocated otherwise false.
 */
bool MemoryBlob::Empty() const {
  return data_ == nullptr;
}

/**
 * @name  Size
 * @fn    std::size_t Size() const
 * @brief Give the size in bytes of the underlying memory blob
 * @return    Allocated size in bytes
 */
std::size_t MemoryBlob::Size() const {
  return n_bytes_;
}


}  // namespace CUDA
}  // namespace LTS5
