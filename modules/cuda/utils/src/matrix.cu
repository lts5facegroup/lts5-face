/**
 *  @file   cuda/utils/matrix.cpp
 *  @brief  Matrix interface for GPU computation
 *
 *  @author Christophe Ecabert
 *  @date
 *  Copyright (c) 2016 Christophe Ecabert. All right reserved.
 */

#include <cuda_runtime.h>
#include <iostream>

#include "opencv2/core/cuda_stream_accessor.hpp"

#include "lts5/cuda/utils/matrix.hpp"
#include "lts5/cuda/utils/device/matrix_kernel.h"
#include "lts5/cuda/utils/safe_call.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Initialization

/*
 * @name  Matrix
 * @fn  Matrix()
 * @brief Default constructor
 */
template<typename T>
Matrix<T>::Matrix() : rows_(0),
                          cols_(0),
                          data_(nullptr),
                          ref_(nullptr) {
  ref_ = new unsigned;
  *ref_ = 1;
}

/*
 * @name  Matrix
 * @fn  Matrix(const cv::Mat& matrix, const cv::cuda::Stream& stream)
 * @brief Constructor with direct device upload
 * @param[in] matrix  Host matrix to upload on device
 * @param[in] stream  Stream on which to run computation
 */
template<typename T>
Matrix<T>::Matrix(const cv::Mat& matrix,
                  const cv::cuda::Stream& stream) : rows_(0),
                                                    cols_(0),
                                                    data_(nullptr),
                                                    ref_(nullptr) {
  ref_ = new unsigned;
  *ref_ = 1;
  this->Upload(matrix, stream);
}

/*
 * @name  Matrix
 * @fn  Matrix(const cv::Mat& matrix, const cv::cuda::Stream& stream)
 * @brief Constructor with direct device upload
 * @param[in] matrix  Host matrix to upload on device
 * @param[in] stream  Stream on which to run computation
 */
template<typename T>
Matrix<T>::Matrix(const cv::Mat& matrix) : rows_(0),
                                           cols_(0),
                                           data_(nullptr),
                                           ref_(nullptr) {
  ref_ = new unsigned;
  *ref_ = 1;
  this->Upload(matrix, cv::cuda::Stream::Null());
}

/*
 * @name  Matrix
 * @fn  Matrix(const int row, const int col, const T* buffer)
 * @brief Constructor for data with external buffer.
 *        IMPORTANT take ownership of the data
 * @param[in] row     Number of rows
 * @param[in] col     Number of columns
 * @param[in] buffer  Device memory buffer allocated by user
 */
template<typename T>
Matrix<T>::Matrix(const int row,
                  const int col,
                  T* buffer)  : rows_(row),
                                cols_(col),
                                data_(buffer),
                                ref_(nullptr) {
  ref_ = new unsigned;
  *ref_ = 1;
}

/*
 * @name  Matrix
 * @fn  Matrix(const Matrix<T>& other)
 * @brief Copy constructor
 * @param[in] other Object to copy from
 */
template<typename T>
Matrix<T>::Matrix(const Matrix<T>& other) : rows_(other.rows_),
                                            cols_(other.cols_),
                                            data_(other.data_),
                                            ref_(other.ref_) {
  this->Retain();
}

/*
 * @name  operator=
 * @fn  Matrix<T>& operator=(const Matrix<T>& rhs) = delete
 * @brief Assignment operator
 * @param[in] rhs Object to assign from
 * @return  New assigned object
 */
template<typename T>
Matrix<T>& Matrix<T>::operator=(const Matrix<T>& rhs) {
  if (this != &rhs) {
    this->Release();
    rows_ = rhs.rows_;
    cols_ = rhs.cols_;
    data_ = rhs.data_;
    ref_ = rhs.ref_;
    this->Retain();
  }
  return *this;
}

/*
 * @name  Clone
 * @fn  Matrix<T> Clone(const cv::cuda::Stream& stream)
 * @brief Deep copy
 * @param[in] stream  Stream on which to run computation
 * @return Copy of the current matrix
 */
template<typename T>
Matrix<T> Matrix<T>::Clone(const cv::cuda::Stream& stream) {
  Matrix<T> matrix;
  matrix.Create(this->rows_, this->cols_);
  if (data_ != nullptr) {
    cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                     0 :
                     cv::cuda::StreamAccessor::getStream(stream);
    // Copy data
    cudaError err;
    if (s == 0) {
      err = cudaMemcpy(reinterpret_cast<void *>(matrix.data()),
                       reinterpret_cast<const void *>(data_),
                       rows_ * cols_ * sizeof(T),
                       cudaMemcpyDeviceToDevice);
      cudaSafeCall(cudaDeviceSynchronize());
    } else {
      err = cudaMemcpyAsync(reinterpret_cast<void *>(matrix.data()),
                            reinterpret_cast<const void *>(data_),
                            rows_ * cols_ * sizeof(T),
                            cudaMemcpyDeviceToDevice,
                            s);
    }
    if (err) {
      // Something went wrong while copying
      matrix.Release();
      cudaSafeCall(err);
    }
  }
  return matrix;
}

/**
 * @name  Clone
 * @fn  Matrix<T> Clone(const cv::cuda::Stream& stream) const
 * @brief Deep copy
 * @param[in] stream  Stream on which to run computation
 * @return Copy of the current matrix
 */
template<typename T>
Matrix<T> Matrix<T>::Clone(const cv::cuda::Stream& stream) const {
  Matrix<T> matrix;
  matrix.Create(this->rows_, this->cols_);
  if (data_ != nullptr) {
    cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                     0 :
                     cv::cuda::StreamAccessor::getStream(stream);
    // Copy data
    cudaError err;
    if (s == 0) {
      err = cudaMemcpy(reinterpret_cast<void *>(matrix.data()),
                       reinterpret_cast<const void *>(data_),
                       rows_ * cols_ * sizeof(T),
                       cudaMemcpyDeviceToDevice);
      cudaSafeCall(cudaDeviceSynchronize());
    } else {
      err = cudaMemcpyAsync(reinterpret_cast<void *>(matrix.data()),
                            reinterpret_cast<const void *>(data_),
                            rows_ * cols_ * sizeof(T),
                            cudaMemcpyDeviceToDevice,
                            s);
    }
    if (err) {
      // Something went wrong while copying
      matrix.Release();
      cudaSafeCall(err);
    }
  }
  return matrix;
}

/*
 * @name  Matrix @fn  Matrixid)
 * @brief Destructor
 */
template<typename T>
Matrix<T>::~Matrix() {
  if (ref_) {
    this->Release();
  }
}

/*
 * @name  Create
 * @fn  void Create(const int row, const int col)
 * @brief Create GPU matrix of a given size
 * @param[in] row Number of rows
 * @param[in] col Number of columns
 */
template<typename T>
void Matrix<T>::Create(const int row, const int col) {
  if (row != rows_ || col != cols_) {
    if (data_) {
      cudaSafeCall(cudaFree(data_));
      data_ = nullptr;
    }
    rows_ = row;
    cols_ = col;
    cudaSafeCall(cudaMalloc(reinterpret_cast<void**>(&data_),
                            rows_ * cols_ * sizeof(T)));;
  }
}

/*
 * @name  Reshape
 * @fn  void Reshape(const int row, const int col)
 * @brief Reshape GPU matrix of a given size, col*row must be correct
 * @param[in] row Number of rows
 * @param[in] col Number of columns
 */
template<typename T>
void Matrix<T>::Reshape(const int row, const int col) {
  int n_sz = row * col;
  if (n_sz == rows_ * cols_) {
    rows_ = row;
    cols_ = col;
  } else {
    std::cout << "Can not reshape, dimension mismatch" << std::endl;
  }
}

/*
 * @name  Upload
 * @fn  void Upload(const cv::Mat& matrix, const cv::cuda::Stream& stream)
 * @brief Upload Host matrix into Device memory
 * @param[in] matrix  Host matrix to push onto Device
 * @param[in] stream  Stream on which to run computation
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
void Matrix<T>::Upload(const cv::Mat& matrix, const cv::cuda::Stream& stream) {
  // Already allocated ? (change memory layout at the same time)
  this->Create(matrix.cols, matrix.rows);
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Upload
  if (s == 0) {
    cudaSafeCall(cudaMemcpy(reinterpret_cast<void *>(data_),
                            reinterpret_cast<const void *>(matrix.data),
                            cols_ * rows_ * sizeof(T),
                            cudaMemcpyHostToDevice));
    cudaSafeCall(cudaDeviceSynchronize());
  } else {
    cudaSafeCall(cudaMemcpyAsync(reinterpret_cast<void *>(data_),
                                 reinterpret_cast<const void *>(matrix.data),
                                 cols_ * rows_ * sizeof(T),
                                 cudaMemcpyHostToDevice,
                                 s));
  }
}

/*
 * @name  Upload
 * @fn  void Upload(const cv::Mat& matrix)
 * @brief Upload Host matrix into Device memory
 * @param[in] matrix  Host matrix to push onto Device
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
void Matrix<T>::Upload(const cv::Mat& matrix) {
  this->Upload(matrix, cv::cuda::Stream::Null());
}

/*
 * @name  Download
 * @fn  void Download(const cv::cuda::Stream& stream, cv::Mat* matrix) const
 * @brief Copy matrix from Device to Host
 * @param[in] stream  Stream on which to run computation
 * @param[out]  matrix  Host matrix where to store data
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
void Matrix<T>::Download(const cv::cuda::Stream& stream,cv::Mat* matrix) const {
  if (data_) {
    // Get stream
    cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                     0 :
                     cv::cuda::StreamAccessor::getStream(stream);
    // Create output
    matrix->create(cols_, rows_, cv::DataType<T>::type);
    // Copy data
    if (s == 0) {
      cudaSafeCall(cudaMemcpy(reinterpret_cast<void *>(matrix->data),
                              reinterpret_cast<const void *>(data_),
                              rows_ * cols_ * sizeof(T),
                              cudaMemcpyDeviceToHost));
      cudaSafeCall(cudaDeviceSynchronize());
    } else {
      cudaSafeCall(cudaMemcpyAsync(reinterpret_cast<void *>(matrix->data),
                                   reinterpret_cast<const void *>(data_),
                                   rows_ * cols_ * sizeof(T),
                                   cudaMemcpyDeviceToHost,
                                   s));
    }
  }
}

/*
 * @name  Download
 * @fn  void Download(cv::Mat* matrix) const
 * @brief Copy matrix from Device to Host
 * @param[out]  matrix  Host matrix where to store data
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
void Matrix<T>::Download(cv::Mat* matrix) const {
  this->Download(cv::cuda::Stream::Null(), matrix);
}

/*
 * @name  SetTo
 * @fn    void SetTo(const T value, const cv::cuda::Stream& stream)
 * @brief Initialize matrix with a given \p value
 * @param[in] value   Value to set
 * @param[in] stream  Stream on which to run computation
 */
template<typename T>
void Matrix<T>::SetTo(const T value, const cv::cuda::Stream& stream) {
  if (this->data_) {
    CUDA::SetTo(value,
                this->rows_ * this->cols_,
                stream,
                this->data_);
  }
}

/*
 * @name  SetTo
 * @fn    void SetTo(const T value)
 * @brief Initialize matrix with a given \p value
 * @param[in] value   Value to set
 */
template<typename T>
void Matrix<T>::SetTo(const T value) {
  this->SetTo(value, cv::cuda::Stream::Null());
}

/*
 * @name  ConvertTo
 * @fn    void ConvertTo(const cv::cuda::Stream& stream,
                         Matrix<D>* matrix) const
 * @brief Convert the matrix into another type
 * @tparam D  Type to convert to
 * @param[in] stream  Stream on which to run computation
 * @param[out] matrix Converted matrix
 */
template<>
template<>
void Matrix<float>::ConvertTo(const cv::cuda::Stream& stream,
                              Matrix<double>* matrix) const {
  if (!this->empty()) {
    matrix->Create(this->rows_, this->cols_);
    // Convert kernel
    CUDA::ConvertTo(this->data_,
                    this->rows_ * this->cols_,
                    stream,
                    matrix->data());
  }
}

template<>
template<>
void Matrix<float>::ConvertTo(const cv::cuda::Stream& stream,
                              Matrix<float>* matrix) const {
  // Copy header, no data
  *matrix = *this;
}

template<>
template<>
void Matrix<double>::ConvertTo(const cv::cuda::Stream& stream,
                               Matrix<double>* matrix) const {
  // Copy header, no data
  *matrix = *this;
}

template<>
template<>
void Matrix<double>::ConvertTo(const cv::cuda::Stream& stream,
                               Matrix<float>* matrix) const {
  if (!this->empty()) {
    matrix->Create(this->rows_, this->cols_);
    // Convert kernel
    CUDA::ConvertTo(this->data_,
                    this->rows_ * this->cols_,
                    stream,
                    matrix->data());
  }
}

/*
 * @name  ConvertTo
 * @fn    void ConvertTo(Matrix<D>* matrix) const
 * @brief Convert the matrix into another type
 * @tparam D  Type to convert to
 * @param[out] matrix Converted matrix
 */
template<>
template<>
void Matrix<float>::ConvertTo(Matrix<float>* matrix) const {
  this->ConvertTo(cv::cuda::Stream::Null(), matrix);
}

template<>
template<>
void Matrix<float>::ConvertTo(Matrix<double>* matrix) const {
  this->ConvertTo(cv::cuda::Stream::Null(), matrix);
}

template<>
template<>
void Matrix<double>::ConvertTo(Matrix<float>* matrix) const {
  this->ConvertTo(cv::cuda::Stream::Null(), matrix);
}

template<>
template<>
void Matrix<double>::ConvertTo(Matrix<double>* matrix) const {
  this->ConvertTo(cv::cuda::Stream::Null(), matrix);
}

#pragma mark -
#pragma mark Private

/*
 *  @name Retain
 *  @fn void Retain()
 *  @brief  Increase reference counter
 */
template<typename T>
void Matrix<T>::Retain() {
  assert(ref_);
  *ref_ += 1;
}

/*
 *  @name Release
 *  @fn void Release()
 *  @brief  Release object memory if reference counter reach zero
 */
template<typename T>
void Matrix<T>::Release() {
  assert(ref_ && *ref_ > 0);
  *ref_ -= 1;
  if (*ref_ == 0) {
    if (data_) {
      cudaFree(data_);
      data_ = nullptr;
    }
    rows_ = 0;
    cols_ = 0;
    delete ref_;
    ref_ = nullptr;
  }
}

#pragma mark -
#pragma mark Declaration

/** Float GPU Matrix */
template class Matrix<float>;
/** Double GPU Matrix */
template class Matrix<double>;

} // namesapce CUDA
} // namesapce LTS5
