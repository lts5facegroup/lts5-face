/**
 *  @file   cudnn_utils.cpp
 *  @brief  Utility functions/classes for cuDNN wrapper
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   26/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <cstdint>

#include "cudnn.h"

#include "lts5/cuda/utils/math/cudnn_utils.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Tensor data type selection

template<typename T>
cudnnDataType_t DataType<T>::value;

template<>
DataType<float>::DataType() {
  value = CUDNN_DATA_FLOAT;
}

template<>
DataType<double>::DataType() {
  value = CUDNN_DATA_DOUBLE;
}

template<>
DataType<int32_t>::DataType() {
  value = CUDNN_DATA_INT32;
}

#pragma mark -
#pragma mark Tensor operation selector

/*
 * @name Select
 * @fn    static cudnnOpTensorOp_t Select(const TensorOpType& type)
 * @brief Select tensor operation based on an operation type
 * @param[in] type    Type of tensor operation (Add, Mul, Sqrt)
 * @return    cuDNN Tensor operation type
 */
cudnnOpTensorOp_t OpSelector::Select(const TensorOpType& type) {
  cudnnOpTensorOp_t tensorOp = CUDNN_OP_TENSOR_ADD;
  switch (type) {
    case TensorOpType::kOpAdd : tensorOp = CUDNN_OP_TENSOR_ADD;
      break;
    case TensorOpType::kOpMul : tensorOp = CUDNN_OP_TENSOR_MUL;
      break;
    case TensorOpType::kOpSqrt : tensorOp = CUDNN_OP_TENSOR_SQRT;
      break;
  }
  return tensorOp;
}

#pragma mark -
#pragma mark Tensor reduction selector

/*
 * @name Select
 * @fn    static cudnnOpTensorOp_t Select(const TensorRedType& type)
 * @brief Select tensor reduction based on an reduction type
 * @param[in] type    Type of tensor operation (Add, Mul, Sqrt, ...)
 * @return    cuDNN Tensor operation type
 */
cudnnReduceTensorOp_t RedSelector::Select(const TensorRedType & type) {
  cudnnReduceTensorOp_t tensorRed = CUDNN_REDUCE_TENSOR_ADD;
  switch (type) {
    case TensorRedType::kRedAdd : tensorRed = CUDNN_REDUCE_TENSOR_ADD;
      break;
    case TensorRedType::kRedMul : tensorRed = CUDNN_REDUCE_TENSOR_MUL;
      break;
    case TensorRedType::kRedAvg : tensorRed = CUDNN_REDUCE_TENSOR_AVG;
      break;
    case TensorRedType::kRedL1Norm : tensorRed = CUDNN_REDUCE_TENSOR_NORM1;
      break;
    case TensorRedType::kRedL2Norm : tensorRed = CUDNN_REDUCE_TENSOR_NORM2;
      break;
  }
  return tensorRed;
}

#pragma mark -
#pragma mark Tensor convolution selector

/*
 * @name  Select
 * @fn    static cudnnConvolutionMode_t Select(const TensorConvType& type)
 * @brief Convert convolution type to cudnn
 * @param[in] type    Type of convolution
 * @return    converted type for cuDNN
 */
cudnnConvolutionMode_t ConvSelector::Select(const TensorConvType& type) {
  cudnnConvolutionMode_t mode = CUDNN_CONVOLUTION;
  switch (type) {
    case kConvolution : mode = CUDNN_CONVOLUTION;
      break;
    case kCrossCorrelation : mode = CUDNN_CROSS_CORRELATION;
      break;
  }
  return mode;
}

}  // namepsace CUDA
}  // namepsace LTS5
