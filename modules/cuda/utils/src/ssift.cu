/**
 *  @file   ssift.cpp
 *  @brief  Compute SIFT descriptor on GPU
 *
 *  @author Christophe Ecabert
 *  @date   04/05/16
 *  Copyright (c) 2016 Ecabert Christophe. All rights reserved.
 */

#include <iostream>
#include <fstream>

#include "cuda_runtime.h"
#include "cuda.h"

#include "opencv2/core/cuda.hpp"
#include "opencv2/core/cuda_stream_accessor.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/utils/math/constant.hpp"
#include "lts5/utils/file_io.hpp"

#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/ssift.hpp"
#include "lts5/cuda/utils/device/ssift_kernel.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/*
 * @name    GPUCache
 * @fn  GPUCache(void);
 * @brief   Constructor
 */
GPUSSift::GPUCache::GPUCache(void) : n_stream_(0),
                                     n_pts_(0),
                                     filters_init_(false) {
};

/*
 * @name
 * @fn
 * @param[in] n_stream  Number of stream in the cache
 * @param[in] n_pts     Number of points
 */
GPUSSift::GPUCache::GPUCache(const size_t& n_stream,
                             const size_t& n_pts) : n_stream_(0),
                                                    n_pts_(0),
                                                    filters_init_(false) {
  this->Create(n_stream, n_pts);
}

/*
 * @name    ~GPUCache
 * @fn  ~GPUCache(void);
 * @brief   Destructor
 */
GPUSSift::GPUCache::~GPUCache(void) {
  /*for (size_t i = 0; i < n_stream_; ++i) {
    cudaSafeCall(cudaStreamDestroy(streams_[i]));
  }*/
}

/*
 * @name    Create
 * @fn      void Create(const size_t& n_stream, const size_t& n_pts)
 * @brief   Create cache if already init with proper dimension then do
 *          nothing
 * @param[in] n_stream  Number of stream in the cache
 * @param[in] n_pts     Number of points
 */
void GPUSSift::GPUCache::Create(const size_t& n_stream,
                                const size_t& n_pts) {
  n_stream_ = n_stream;
  n_pts_ = n_pts;
  streams_.resize(n_stream_, cv::cuda::Stream());
  filters_.resize(n_stream_);
  buffers_.resize(n_pts_);
  img_patches_32f_.resize(n_pts_);
  filter_patches_.resize(n_pts_);
  params_.resize(n_pts_);
  patch_.resize(n_pts_);
  filters_init_ = false;
}

/*
 * @name    WaitForCompletion
 * @fn  void WaitForCompletion(void);
 * @brief   Wait till all stream have complete their job
 */
void GPUSSift::GPUCache::WaitForCompletion(void) {
  cudaSafeCall(cudaStreamSynchronize(0));
}

/*
 *  @name   ComputeDescriptor
 *  @fn static void ComputeDescriptor(const cv::cuda::GpuMat& image,
                             const std::vector<cv::KeyPoint>& key_points,
                             const SSift::SSiftParameters& sift_param,
                             const GPUCache* cache,
                             Matrix<float>* sift_features
 *  @brief  Extract SSfit descriptor at location provided by key_points
 *  @param[in]  image           Input images
 *  @param[in]  key_points      Location of sift patches
 *  @param[in]  sift_param      Parameters for sift extraction
 *  @param[in]  cache           Cache buffer for sift computation, can be null.
 *  @param[out] sift_features   Extracted descriptor
 *  @return -1 if error, 0 otherwise
 */
int GPUSSift::ComputeDescriptor(const cv::cuda::GpuMat& image,
                                const std::vector<cv::KeyPoint>& key_points,
                                const SSift::SSiftParameters& sift_param,
                                const GPUCache* cache,
                                Matrix<float>* sift_features) {
  LTS5_CUDA_TRACE("SSIFT", 7);
  GPUCache* c = const_cast<GPUCache*>(cache);
  // Init output, col major
  sift_features->Create(sift_param.sift_dim,
                        static_cast<int>(key_points.size()));

  // Filter
  float sigma = std::sqrt(std::max(sift_param.sift_sigma *
                                   sift_param.sift_sigma -
                                   sift_param.sift_init_sigma *
                                   sift_param.sift_init_sigma, 0.01f));
  int ksz = cvRound(sigma* 4 * 2 + 1) | 1;
  if (!c->filters_init_) {
    cv::Mat kernel = cv::getGaussianKernel(ksz, sigma, CV_32FC1);
    for (size_t f = 0; f < c->n_stream_; ++f) {
      c->filters_[f].SetKernel(kernel.ptr<float>(), ksz, c->streams_[0]);
    }
    c->filters_init_ = true;
  }


  // Loop
  for (size_t p = 0; p < key_points.size(); ++p) {
    // Init, compute dimension, scale, ...
    // ---------------------------------------------------------------
    c->params_[p].bin_per_rad = sift_param.sift_descr_hist_bins / 360.0f;
    c->params_[p].exp_scale = -1.f / (sift_param.sift_descr_width *
                                      sift_param.sift_descr_width *
                                      0.5f);
    c->params_[p].hist_length = ((sift_param.sift_descr_width + 2) *
                                 (sift_param.sift_descr_width + 2) *
                                 (sift_param.sift_descr_hist_bins + 2));
    c->params_[p].hist_width = (sift_param.sift_descr_scl_fctr *
                                key_points[p].size *
                                sift_param.sift_size_scl * 0.5f);
    c->params_[p].radius = cvRound(c->params_[p].hist_width *
                                   LTS5::Constants<float>::Sqrt2 *
                                   (sift_param.sift_descr_width + 1) *
                                   0.5f);
    //Define patch size according to filter size, patch sisze must be
    // a multiple of 64
    int psz = ksz + 2 * (c->params_[p].radius + 1) - 1;
    c->params_[p].patch_size = 64 * ((psz + 63) / 64);
    cv::Rect &patch = c->patch_[p];
    patch.width = c->params_[p].patch_size;
    patch.height = c->params_[p].patch_size;
    patch.x = static_cast<int>(key_points[p].pt.x) - patch.width / 2;
    patch.y = static_cast<int>(key_points[p].pt.y) - patch.height / 2;
    //Check for boundaries
    patch.x = patch.x < 0 ? 0 : patch.x;
    patch.x = patch.x + patch.width > image.cols ?
              image.cols - patch.width - 1 :
              patch.x;
    patch.y = patch.y < 0 ? 0 : patch.y;
    patch.y = patch.y + patch.height > image.rows ?
              image.rows - patch.height - 1 :
              patch.y;
    // Update parameters
    c->params_[p].patch_x = patch.x;
    c->params_[p].patch_y = patch.y;
    c->params_[p].patch_center_x = static_cast<int>(key_points[p].pt.x) -
                                   patch.x;
    c->params_[p].patch_center_y = static_cast<int>(key_points[p].pt.y) -
                                   patch.y;
    // Copy parameters needed
    c->params_[p].sift_descr_width = sift_param.sift_descr_width;
    c->params_[p].sift_descr_hist_bins = sift_param.sift_descr_hist_bins;
    c->params_[p].sift_dim = sift_param.sift_dim;
    c->params_[p].sift_descr_mag_thr = sift_param.sift_descr_mag_thr;
    c->params_[p].sift_int_descr_fctr = sift_param.sift_int_descr_fctr;
    c->params_[p].sift_root = sift_param.sift_root;
  }

  size_t step = key_points.size() / c->n_stream_;
  for (size_t p = 0, nbatch=0; p < key_points.size(); p += step, ++nbatch) {
    // Compute descriptor
    // ---------------------------------------------------------------
    //Extract image patch + convert to float type
    size_t bsize = p < (key_points.size() - step) ?
                   step :
                   key_points.size() - p;
    ImageRoiToGrayFloatBatched(image,
                               &c->patch_[p],
                               nbatch,
                               bsize,
                               c->streams_[(p/step) % c->n_stream_],
                               &c->img_patches_32f_[p]);
  }

  // Filter by row
  for (size_t p = 0, nbatch = 0; p < key_points.size(); p += step, ++nbatch) {
    //Filter the image
    size_t idx = (p / step) % c->n_stream_;
    size_t bsize = p < (key_points.size() - step) ?
                   step :
                   key_points.size() - p;
    c->filters_[idx].ApplyRowBatched(&c->img_patches_32f_[p],
                                     nbatch,
                                     bsize,
                                     c->streams_[idx],
                                     &c->buffers_[p]);
  }
  // Filter by column
  for (size_t p = 0, nbatch = 0; p < key_points.size(); p += step, ++nbatch) {
    //Filter the image
    size_t idx = (p / step) % c->n_stream_;
    size_t bsize = p < (key_points.size() - step) ?
                   step :
                   key_points.size() - p;
    c->filters_[idx].ApplyColumnBatched(&c->buffers_[p],
                                        nbatch,
                                        bsize,
                                        c->streams_[idx],
                                        &c->filter_patches_[p]);
  }

  // Initialize histgram
  for (size_t p = 0, nbatch = 0; p < key_points.size(); p += step, ++nbatch) {
    // Sample patch and build histogram
    float *hist = &(sift_features->data()[p * sift_param.sift_dim]);
    size_t idx = (p / step) % c->n_stream_;
    size_t bsize = p < (key_points.size() - step) ?
                   step :
                   key_points.size() - p;
    SetValueBatched(0.f,
                    sift_param.sift_dim,
                    nbatch,
                    bsize,
                    c->streams_[idx],
                    hist);
  }
  // Sample patch
  for (size_t p = 0, nbatch = 0; p < key_points.size(); p += step, ++nbatch) {
    // Sample patch and build histogram
    float *hist = &(sift_features->data()[p * sift_param.sift_dim]);
    size_t idx = (p / step) % c->n_stream_;
    size_t bsize = p < (key_points.size() - step) ?
                   step :
                   key_points.size() - p;
    SiftFilterBatched(&c->filter_patches_[p],
                      &c->params_[p],
                      nbatch,
                      bsize,
                      c->streams_[idx],
                      hist);
  }
  // Finalize histogram
  for (size_t p = 0, nbatch = 0; p < key_points.size(); p += step, ++nbatch) {
    float *hist = &(sift_features->data()[p * sift_param.sift_dim]);
    size_t idx = (p / step) % c->n_stream_;
    size_t bsize = p < (key_points.size() - step) ?
                   step :
                   key_points.size() - p;
    FinalizeHistogramBatched(&c->params_[p],
                             nbatch,
                             bsize,
                             c->streams_[idx],
                             hist);
  }



  if (sift_param.sift_root) {
    // Sift root normalization
    for (size_t p = 0, nbatch = 0; p < key_points.size(); p += step, ++nbatch) {
      float *hist = &(sift_features->data()[p * sift_param.sift_dim]);
      size_t idx = (p / step) % c->n_stream_;
      size_t bsize = p < (key_points.size() - step) ?
                     step :
                     key_points.size() - p;
      SiftRootBatched(&c->params_[p],
                      nbatch,
                      bsize,
                      c->streams_[idx],
                      hist);
    }

  }
  //cudaSafeCall(cudaStreamSynchronize(0));
  c->WaitForCompletion();
  return cudaSafeCall(cudaGetLastError());
}
}  // namespace CUDA
}  // namespace LTS5
