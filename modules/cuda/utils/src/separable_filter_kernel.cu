/**
 *  @file   separable_filter_kernel.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   13/10/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "opencv2/core/cuda.hpp"
#include "opencv2/core/cuda_stream_accessor.hpp"

#include "lts5/utils/threaded_ressource_manager.hpp"
#include "lts5/cuda/utils/device/separable_filter_kernel.h"
#include "lts5/cuda/utils/safe_call.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {


namespace Filter {

/**
 * @class   FilterBuffer
 * @brief   Class holding buffers for batched filtering computation
 * @author  Christophe Ecabert
 * @date    16/06/2017
 * @ingroup utils
 */
template<typename T>
class FilterBuffer {
 public:

  /**
   * @name  FilterBuffer
   * @fn    FilterBuffer(const size_t size)
   * @brief Construct a batch buffer of a given \p size
   * @param[in] size    Number of element in the buffer
   */
  FilterBuffer(const size_t size) : size_(size * sizeof(T)),
                                    host_(nullptr),
                                    device_(nullptr) {
    // Alloc host pinned memory
    cudaSafeCall(cudaMallocHost(&host_, size_));
    // Alloc device
    cudaSafeCall(cudaMalloc(&device_, size_));
  }

  /**
   * @name  ~FilterBuffer
   * @fn    ~FilterBuffer(void)
   * @brief Destructor
   */
  ~FilterBuffer(void) {
    if (device_) {
      cudaSafeCall(cudaFree(device_));
      device_ = nullptr;
    }
    if (host_) {
      cudaSafeCall(cudaFreeHost(host_));
      host_ = nullptr;
    }
  }

  /**
   * @name  Upload
   * @fn    void Upload(const size_t batch_size,
                        const cudaStream_t& stream)
   * @brief Copy host buffer to device memory
   * @param[in] batch_size  Batch size
   * @param[in] stream  Stream on which to launch transfer
   */
  void Upload(const size_t batch_size,
              const cudaStream_t& stream) {
    cudaSafeCall(cudaMemcpyAsync(&device_[start_],
                                 &host_[start_],
                                 batch_size * sizeof(T),
                                 cudaMemcpyHostToDevice,
                                 stream));
    start_ += batch_size;
  }

  /** Buffer size in byte */
  size_t size_;
  /** Starting index */
  size_t start_;
  /** Host buffer with pinned memory */
  T* host_;
  /** Device device buffer*/
  T* device_;
};

/**
 * @class   FilterManager
 * @brief   Class holding all buffers for batched ssift computation
 * @author  Christophe Ecabert
 * @date    16/06/2017
 * @ingroup utils
 */
template<size_t D>
class FilterManager {
 public:

  /**
   * @name  FilterManager
   * @fn    FilterManager(void)
   * @brief Constructor
   */
  FilterManager(void) : input_(D), intermediate_(D), output_(D) {
  }

  /**
   * @name  ~FilterManager
   * @fn    ~FilterManager(void)
   * @brief Destructor
   */
  ~FilterManager(void) {
  }

  /**
   * @name  Upload
   * @fn    void Upload(const size_t batch_size,
                        const cudaStream_t& stream)
   * @brief Copy host buffer to device buffer
   * @param[in] batch_size  Batch size
   * @param[in] is_row  Indicate if it is needed to push only data related to
   *                    row filter
   * @param[in] stream  Stream on which to run the transfer
   */
  void Upload(const size_t batch_size,
              const bool is_row,
              const cudaStream_t& stream) {
    if (is_row) {
      input_.Upload(batch_size, stream);
      intermediate_.Upload(batch_size, stream);
    } else {
      output_.Upload(batch_size, stream);
      intermediate_.Upload(batch_size, stream);
    }
  }

  /** Input image */
  FilterBuffer<float*> input_;
  /** Intermediate */
  FilterBuffer<float*> intermediate_;
  /** Output */
  FilterBuffer<float*> output_;

};

/** Default buffer */
using DefaultBuffer = FilterManager<68>;
/** Default Manager */
using DefaultFilterManager = LTS5::ThreadedRessourceManager<DefaultBuffer>;


}  //namespace Filter






/**
 *  @namespace  device
 *  @brief      CUDA device code
 */
namespace device {

#pragma mark -
#pragma mark device

/**
 *  @namespace  RowFilter
 *  @brief      CUDA device code linked to row filter
 */
namespace RowFilter {

/** Maximum kernel size */
const int MAX_KERNEL_SIZE = 16;
/** Kernel data */
__constant__ float kernel[MAX_KERNEL_SIZE];

const int ROWS_BLOCKDIM_X = 16;
const int ROWS_BLOCKDIM_Y = 8;
const int ROWS_RESULT_STEPS = 4;
const int ROWS_HALO_STEPS = 1;

/*
 * @name    Call
 * @fn      void Call(const T* src,
                       const size_t spitch,
                       const size_t dpitch,
                       const size_t width,
                       D* dest)
 * @brief   Apply row kernel filter on a given image
 * @ingroup utils
 * @param[in] src   Input image
 * @param[in] spitch    Input's pitch
 * @param[in] dpitch    Destination's pitch
 * @param[in] width     Image width
 * @param[out] dest      Filtered image
 * @tparam KRADIUS  Filter radius
 * @tparam T    Source data tyoe
 * @tparam D    Destination data type
 */
template<int KRADIUS, typename T, typename D>
__global__ void Call(const T *src,
                     const size_t spitch,
                     const size_t dpitch,
                     const size_t width,
                     D *dest) {
  // Shared memory buffer [4 x 80]
  __shared__ T buffer[ROWS_BLOCKDIM_Y][(ROWS_RESULT_STEPS + 2 * ROWS_HALO_STEPS) * ROWS_BLOCKDIM_X];
  // Define data pointer
  //Offset to the left halo edge
  const int baseX = (blockIdx.x * ROWS_RESULT_STEPS - ROWS_HALO_STEPS) * ROWS_BLOCKDIM_X + threadIdx.x;
  const int baseY = blockIdx.y * ROWS_BLOCKDIM_Y + threadIdx.y;
  const T* d_src = src;
  d_src += baseY * spitch + baseX;
  D* d_dest = dest;
  d_dest += baseY * dpitch + baseX;

  //Load main data
#pragma unroll
  for (int i = ROWS_HALO_STEPS;
       i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS;
       ++i) {
    buffer[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = d_src[i * ROWS_BLOCKDIM_X];
  }
  //Load left halo
#pragma unroll
  for (int i = 0; i < ROWS_HALO_STEPS; ++i) {
    buffer[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = (baseX >= -i * ROWS_BLOCKDIM_X) ?
                                                             d_src[i * ROWS_BLOCKDIM_X] :
                                                             0;
  }
  //Load right halo
#pragma unroll
  for (int i = ROWS_HALO_STEPS + ROWS_RESULT_STEPS;
       i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS + ROWS_HALO_STEPS;
       ++i) {
    buffer[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = (width - baseX > i * ROWS_BLOCKDIM_X) ?
                                                             d_src[i * ROWS_BLOCKDIM_X] :
                                                             0;
  }
  //Compute and store results
  __syncthreads();
#pragma unroll
  for (int i = ROWS_HALO_STEPS; i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS; i++) {
    T sum = 0;
#pragma unroll
    for (int j = -KRADIUS; j <= KRADIUS; j++) {
      sum += device::RowFilter::kernel[KRADIUS - j] * buffer[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X + j];
    }
    d_dest[i * ROWS_BLOCKDIM_X] = sum;
  }
}

/*
 * @name    Call
 * @fn      void Call(const T** src,
                       const size_t batch_size,
                       const size_t spitch,
                       const size_t dpitch,
                       const size_t width,
                       D** dest)
 * @brief   Apply row kernel filter on a given image
 * @ingroup utils
 * @param[in] src   Input, array of image
 * @param[in] batch_size    Batch size
 * @param[in] spitch    Input's pitch
 * @param[in] dpitch    Destination's pitch
 * @param[in] width     Image width
 * @param[out] dest      Array of filtered image
 * @tparam KRADIUS  Filter radius
 * @tparam T    Source data tyoe
 * @tparam D    Destination data type
 */
template<int KRADIUS, typename T, typename D>
__global__ void CallBatched(const T** src,
                            const size_t batch_size,
                            const size_t spitch,
                            const size_t dpitch,
                            const size_t width,
                            D** dest) {
  // Shared memory buffer [4 x 80]
  __shared__ T buffer[ROWS_BLOCKDIM_Y][(ROWS_RESULT_STEPS + 2 * ROWS_HALO_STEPS) * ROWS_BLOCKDIM_X];

  //Offset to the left halo edge
  const int baseX = (blockIdx.x * ROWS_RESULT_STEPS - ROWS_HALO_STEPS) * ROWS_BLOCKDIM_X + threadIdx.x;
  const int baseY = blockIdx.y * ROWS_BLOCKDIM_Y + threadIdx.y;

  // Loop over all batch
  for (size_t b = 0; b < batch_size; ++b) {
    // Define data pointer
    const T* d_src = src[b] + baseY * spitch + baseX;
    D* d_dest = dest[b] + baseY * dpitch + baseX;

    //Load main data
#pragma unroll
    for (int i = ROWS_HALO_STEPS;
         i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS;
         ++i) {
      buffer[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = d_src[i * ROWS_BLOCKDIM_X];
    }
    //Load left halo
#pragma unroll
    for (int i = 0; i < ROWS_HALO_STEPS; ++i) {
      buffer[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = (baseX >= -i * ROWS_BLOCKDIM_X) ?
                                                               d_src[i * ROWS_BLOCKDIM_X] :
                                                               0;
    }
    //Load right halo
#pragma unroll
    for (int i = ROWS_HALO_STEPS + ROWS_RESULT_STEPS;
         i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS + ROWS_HALO_STEPS;
         ++i) {
      buffer[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X] = (width - baseX > i * ROWS_BLOCKDIM_X) ?
                                                               d_src[i * ROWS_BLOCKDIM_X] :
                                                               0;
    }
    //Compute and store results
    __syncthreads();
#pragma unroll
    for (int i = ROWS_HALO_STEPS; i < ROWS_HALO_STEPS + ROWS_RESULT_STEPS; i++) {
      T sum = 0;
#pragma unroll
      for (int j = -KRADIUS; j <= KRADIUS; j++) {
        sum += device::RowFilter::kernel[KRADIUS - j] * buffer[threadIdx.y][threadIdx.x + i * ROWS_BLOCKDIM_X + j];
      }
      d_dest[i * ROWS_BLOCKDIM_X] = sum;
    }
  }
}
}  // namepsace RowFilter

/**
 *  @namespace  ColFilter
 *  @brief      CUDA device code linked to row filter
 */
namespace ColFilter {

/** Maximum kernel size */
const int MAX_KERNEL_SIZE = 16;
/** Kernel data */
__constant__ float kernel[MAX_KERNEL_SIZE];

const int COLUMNS_BLOCKDIM_X = 8;
const int COLUMNS_BLOCKDIM_Y = 16;
const int COLUMNS_RESULT_STEPS = 4;
const int COLUMNS_HALO_STEPS = 1;

/*
 * @name    Call
 * @fn      void Call(const T* src,
                       const size_t spitch,
                       const size_t dpitch,
                       const size_t height,
                       D* dest)
 * @brief   Apply column kernel filter on a given image
 * @ingroup utils
 * @param[in] src   Input image
 * @param[in] spitch    Input's pitch
 * @param[in] dpitch    Destination's pitch
 * @param[in] height     Image height
 * @param[out] dest      Filtered image
 * @tparam KRADIUS  Filter radius
 * @tparam T    Source data tyoe
 * @tparam D    Destination data type
 */
template<int KRADIUS, typename T, typename D>
__global__
void Call(const T* src,
          const size_t spitch,
          const size_t dpitch,
          const size_t height,
          D* dest) {
  // Buffer [8 x 81]
  __shared__ T s_data[COLUMNS_BLOCKDIM_X][(COLUMNS_RESULT_STEPS + 2 * COLUMNS_HALO_STEPS) * COLUMNS_BLOCKDIM_Y + 1];
  //Offset to the upper halo edge
  const int baseX = blockIdx.x * COLUMNS_BLOCKDIM_X + threadIdx.x;
  const int baseY = (blockIdx.y * COLUMNS_RESULT_STEPS - COLUMNS_HALO_STEPS) * COLUMNS_BLOCKDIM_Y + threadIdx.y;
  const T* d_src = src + (baseY * spitch) + baseX;
  D* d_dest = dest + (baseY * dpitch) + baseX;

  //Main data
  #pragma unroll
  for (int i = COLUMNS_HALO_STEPS;
       i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS;
       ++i) {
    s_data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y] = d_src[i * COLUMNS_BLOCKDIM_Y * spitch];
  }
  //Upper halo
  #pragma unroll
  for (int i = 0; i < COLUMNS_HALO_STEPS; i++) {
    s_data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y] = (baseY >= -i * COLUMNS_BLOCKDIM_Y) ?
                                                                d_src[i * COLUMNS_BLOCKDIM_Y * spitch] :
                                                                0;
  }
  //Lower halo
  #pragma unroll
  for (int i = COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS;
       i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS + COLUMNS_HALO_STEPS;
       ++i) {
    s_data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y]= (height - baseY > i * COLUMNS_BLOCKDIM_Y) ?
                                                               d_src[i * COLUMNS_BLOCKDIM_Y * spitch] :
                                                               0;
  }
  //Compute and store results
  __syncthreads();
  #pragma unroll
  for (int i = COLUMNS_HALO_STEPS;
       i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS;
       ++i) {
    T sum = 0;
    #pragma unroll
    for (int j = -KRADIUS; j <= KRADIUS; j++) {
      sum += kernel[KRADIUS - j] * s_data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y + j];
    }
    d_dest[i * COLUMNS_BLOCKDIM_Y * dpitch] = sum;
  }
};

/*
 * @name    CallBatched
 * @fn      void CallBatched(const T** src,
                             const size_t batch_size,
                             const size_t spitch,
                             const size_t dpitch,
                             const size_t height,
                       D** dest)
 * @brief   Apply column kernel filter on a given image
 * @ingroup utils
 * @param[in] src   Input, array of image
 * @param[in] batch_size    Batch size
 * @param[in] spitch    Input's pitch
 * @param[in] dpitch    Destination's pitch
 * @param[in] height     Image width
 * @param[out] dest      Array of filtered image
 * @tparam KRADIUS  Filter radius
 * @tparam T    Source data tyoe
 * @tparam D    Destination data type
 */
template<int KRADIUS, typename T, typename D>
__global__ void CallBatched(const T** src,
                            const size_t batch_size,
                            const size_t spitch,
                            const size_t dpitch,
                            const size_t height,
                            D** dest) {
  // Buffer [8 x 81]
  __shared__ T s_data[COLUMNS_BLOCKDIM_X][(COLUMNS_RESULT_STEPS + 2 * COLUMNS_HALO_STEPS) * COLUMNS_BLOCKDIM_Y + 1];

  // Loop over batch
  for (size_t b = 0; b < batch_size; ++b) {
    //Offset to the upper halo edge
    const int baseX = blockIdx.x * COLUMNS_BLOCKDIM_X + threadIdx.x;
    const int baseY = (blockIdx.y * COLUMNS_RESULT_STEPS - COLUMNS_HALO_STEPS) * COLUMNS_BLOCKDIM_Y + threadIdx.y;
    const T* d_src = src[b];
    d_src += baseY * spitch + baseX;
    D* d_dest = dest[b];
    d_dest += baseY * dpitch + baseX;

    //Main data
#pragma unroll
    for (int i = COLUMNS_HALO_STEPS;
         i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS;
         ++i) {
      s_data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y] = d_src[i * COLUMNS_BLOCKDIM_Y * spitch];
    }
    //Upper halo
#pragma unroll
    for (int i = 0; i < COLUMNS_HALO_STEPS; i++) {
      s_data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y] = (baseY >= -i * COLUMNS_BLOCKDIM_Y) ?
                                                                  d_src[i * COLUMNS_BLOCKDIM_Y * spitch] :
                                                                  0;
    }
    //Lower halo
#pragma unroll
    for (int i = COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS;
         i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS + COLUMNS_HALO_STEPS;
         ++i) {
      s_data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y]= (height - baseY > i * COLUMNS_BLOCKDIM_Y) ?
                                                                 d_src[i * COLUMNS_BLOCKDIM_Y * spitch] :
                                                                 0;
    }
    //Compute and store results
    __syncthreads();
#pragma unroll
    for (int i = COLUMNS_HALO_STEPS;
         i < COLUMNS_HALO_STEPS + COLUMNS_RESULT_STEPS;
         ++i) {
      T sum = 0;
#pragma unroll
      for (int j = -KRADIUS; j <= KRADIUS; j++) {
        sum += kernel[KRADIUS - j] * s_data[threadIdx.x][threadIdx.y + i * COLUMNS_BLOCKDIM_Y + j];
      }
      d_dest[i * COLUMNS_BLOCKDIM_Y * dpitch] = sum;
    }
  }
}

}  // namepsace ColFilter
}  // namespace device

#pragma mark -
#pragma mark Caller

namespace RowFilter {

template<typename T>
void SetKernel(const T* k_data, const int size, const cv::cuda::Stream& stream) {
  assert(size < device::RowFilter::MAX_KERNEL_SIZE);
  cudaStream_t cstream = cv::cuda::StreamAccessor::getStream(stream);
  cudaSafeCall(cudaMemcpyToSymbolAsync((const void*)&device::RowFilter::kernel[0],
                                       (const void*)k_data,
                                       size * sizeof(T),
                                       0,
                                       cudaMemcpyHostToDevice,
                                       cstream));
}

/**
 * @name    LinearRowFilter
 * @fn      void LinearRowFilter(const cv::cuda::GpuMat& source,
                                 const int ksize,
                                 const cv::cuda::Stream& stream,
                                 cv::cuda::GpuMat* dest)
 * @brief   Interface to call linear filter
 * @ingroup utils
 * @param[in] source    Source image
 * @param[in] ksize     Kernel radius
 * @param[in] stream    Stream where to launch kernel
 * @param[out] dest     Filtered image
 */
template<typename T, typename D>
void LinearRowFilter(const cv::cuda::GpuMat& source,
                     const int ksize,
                     const cv::cuda::Stream& stream,
                     cv::cuda::GpuMat* dest) {
  assert(device::RowFilter::ROWS_BLOCKDIM_X * device::RowFilter::ROWS_HALO_STEPS >= ksize);
  assert(source.cols % (device::RowFilter::ROWS_RESULT_STEPS * device::RowFilter::ROWS_BLOCKDIM_X) == 0);
  assert(source.rows % device::RowFilter::ROWS_BLOCKDIM_Y == 0);
  cudaStream_t cstream = cv::cuda::StreamAccessor::getStream(stream);
  /** Row filtering function signature */
  typedef void (*row_t)(const T* src, const size_t spitch, const size_t dpitch,
                        const size_t width, D* d);
  /** Define function lookup table for all supported filter size */
  static const row_t filters[] {device::RowFilter::Call<0, T, D>,
                                device::RowFilter::Call<1, T, D>,
                                device::RowFilter::Call<2, T, D>,
                                device::RowFilter::Call<3, T, D>,
                                device::RowFilter::Call<4, T, D>,
                                device::RowFilter::Call<5, T, D>,
                                device::RowFilter::Call<6, T, D>,
                                device::RowFilter::Call<7, T, D>};
  // Define dimension
  dim3 bck(source.cols / (device::RowFilter::ROWS_RESULT_STEPS * device::RowFilter::ROWS_BLOCKDIM_X),
           source.rows / device::RowFilter::ROWS_BLOCKDIM_Y);
  dim3 thd(device::RowFilter::ROWS_BLOCKDIM_X,
           device::RowFilter::ROWS_BLOCKDIM_Y);
  // Get filter
  const row_t f = filters[ksize];
  if (f) {
    const T* in = reinterpret_cast<T*>(source.data);
    D* out = reinterpret_cast<D*>(dest->data);
    f<<<bck,thd, 0, cstream>>>(in, source.step1(), dest->step1(), size_t(source.cols), out);
  } else {
    LTS5::CUDA::error("Unsupported dimension for RowFilter",
                      __FILE__,
                      __LINE__,
                      "");
  }
}

/*
 * @name    LinearRowFilterBatched
 * @fn      void LinearRowFilterBatched(const cv::cuda::GpuMat* source,
                                        const int ksize,
                                        const size_t n_batch,
                                        const size_t batch_size,
                                        const cv::cuda::Stream& stream,
                                        cv::cuda::GpuMat* dest)
 * @brief   Interface to call linear filter
 * @ingroup utils
 * @param[in] source    Array of image to filer
 * @param[in] ksize     Kernel radius
 * @param[in] n_batch   Batch index
 * @param[in] stream    Stream where to launch kernel
 * @param[out] dest     Filtered image
 */
template<typename T, typename D>
void LinearRowFilterBatched(const cv::cuda::GpuMat* source,
                            const int ksize,
                            const size_t n_batch,
                            const size_t batch_size,
                            const cv::cuda::Stream& stream,
                            cv::cuda::GpuMat* dest) {
  assert(device::RowFilter::ROWS_BLOCKDIM_X * device::RowFilter::ROWS_HALO_STEPS >= ksize);
  assert(source->cols % (device::RowFilter::ROWS_RESULT_STEPS * device::RowFilter::ROWS_BLOCKDIM_X) == 0);
  assert(source->rows % device::RowFilter::ROWS_BLOCKDIM_Y == 0);
  cudaStream_t cstream = cv::cuda::StreamAccessor::getStream(stream);
  /** Row filtering function signature */
  typedef void (*row_t)(const T** src, const size_t bsize, const size_t spitch,
                        const size_t dpitch, const size_t width, D** d);
  /** Define function lookup table for all supported filter size */
  static const row_t filters[] {device::RowFilter::CallBatched<0, T, D>,
                                device::RowFilter::CallBatched<1, T, D>,
                                device::RowFilter::CallBatched<2, T, D>,
                                device::RowFilter::CallBatched<3, T, D>,
                                device::RowFilter::CallBatched<4, T, D>,
                                device::RowFilter::CallBatched<5, T, D>,
                                device::RowFilter::CallBatched<6, T, D>,
                                device::RowFilter::CallBatched<7, T, D>};
  // Get manager
  auto* man = Filter::DefaultFilterManager::Instance().Get();
  man->input_.start_ = n_batch == 0 ? 0 : man->input_.start_;
  man->intermediate_.start_ = n_batch == 0 ? 0 : man->intermediate_.start_;
  size_t start = man->input_.start_;
  for (int b = 0; b < batch_size; ++b) {
    size_t i = start + b;
    man->input_.host_[i] = reinterpret_cast<float*>(source[b].data);
    dest[b].create(source->rows, source->cols, CV_32FC1);
    man->intermediate_.host_[i] = reinterpret_cast<float*>(dest[b].data);
  }
  // Copy pointer to device memory
  man->Upload(batch_size,true ,cstream);
  // Define dimension
  dim3 bck(source->cols / (device::RowFilter::ROWS_RESULT_STEPS * device::RowFilter::ROWS_BLOCKDIM_X),
           source->rows / device::RowFilter::ROWS_BLOCKDIM_Y);
  dim3 thd(device::RowFilter::ROWS_BLOCKDIM_X,
           device::RowFilter::ROWS_BLOCKDIM_Y);
  // Get filter
  const row_t f = filters[ksize];
  if (f) {
    const T** in = (const float**)&(man->input_.device_[start]);
    D** out = &(man->intermediate_.device_[start]);
    f<<<bck,thd, 0, cstream>>>(in, batch_size,
            source->step1(),
            dest->step1(),
            size_t(source->cols), out);
  } else {
    LTS5::CUDA::error("Unsupported dimension for RowFilter",
                      __FILE__,
                      __LINE__,
                      "");
  }
}

}  // namepsace RowFilter

namespace ColFilter {

template<typename T>
void SetKernel(const T* k_data, const int size, const cv::cuda::Stream& stream) {
  assert(size < device::ColFilter::MAX_KERNEL_SIZE);
  cudaStream_t cstream = cv::cuda::StreamAccessor::getStream(stream);
  cudaSafeCall(cudaMemcpyToSymbolAsync((const void*)&device::ColFilter::kernel[0],
                                       (const void*)k_data,
                                       size * sizeof(T),
                                       0,
                                       cudaMemcpyHostToDevice,
                                       cstream));
}

/*
 * @name    LinearColFilter
 * @fn      void LinearColFilter(cv::cuda::GpuMat& source,
                                 const int ksize,
                                 const cv::cuda::Stream& stream,
                                 cv::cuda::GpuMat* dest)
 * @brief   Filter constructor
 * @ingroup utils
 * @param[in] source    Source image
 * @param[in] ksize     Kernel dimension
 * @param[in] stream    Stream where to launch kernel
 * @param[out] dest      Filtered image
 */
template<typename T, typename D>
void LinearColFilter(const cv::cuda::GpuMat& source,
                     const int ksize,
                     const cv::cuda::Stream& stream,
                     cv::cuda::GpuMat* dest) {
  assert(device::ColFilter::COLUMNS_BLOCKDIM_Y * device::ColFilter::COLUMNS_HALO_STEPS >= ksize);
  assert(source.cols % device::ColFilter::COLUMNS_BLOCKDIM_X == 0);
  assert(source.rows % (device::ColFilter::COLUMNS_RESULT_STEPS * device::ColFilter::COLUMNS_BLOCKDIM_Y) == 0);
  cudaStream_t cstream = cv::cuda::StreamAccessor::getStream(stream);
  /** Column filtering function signature */
  typedef void (*col_t)(const T* src, const size_t spitch, const size_t dpitch,
                        const size_t height, D* d);
  /** Define function lookup table for all supported filter size */
  static const col_t filters[] {device::ColFilter::Call<0, T, D>,
                                device::ColFilter::Call<1, T, D>,
                                device::ColFilter::Call<2, T, D>,
                                device::ColFilter::Call<3, T, D>,
                                device::ColFilter::Call<4, T, D>,
                                device::ColFilter::Call<5, T, D>,
                                device::ColFilter::Call<6, T, D>,
                                device::ColFilter::Call<7, T, D>};
  // Get dimension
  const dim3 bck(source.cols / device::ColFilter::COLUMNS_BLOCKDIM_X,
                 source.rows / (device::ColFilter::COLUMNS_RESULT_STEPS * device::ColFilter::COLUMNS_BLOCKDIM_Y));
  const dim3 thd(device::ColFilter::COLUMNS_BLOCKDIM_X,
                 device::ColFilter::COLUMNS_BLOCKDIM_Y);
  // Get filter
  const col_t f = filters[ksize];
  if (f) {
    const T* in = reinterpret_cast<const T*>(source.data);
    D* out = reinterpret_cast<D*>(dest->data);
    f<<<bck, thd, 0, cstream>>>(in, source.step1(), dest->step1(), size_t(source.rows), out);
  } else {
    LTS5::CUDA::error("Unsupported dimension for ColFilter",
                      __FILE__,
                      __LINE__,
                      "");
  }
}


/**
 * @name    LinearColFilterBatched
 * @fn      void LinearColFilterBatched(const cv::cuda::GpuMat* source,
                                        const int ksize,
                                        const size_t n_batch,
                                        const size_t batch_size,
                                        const cv::cuda::Stream& stream,
                                        cv::cuda::GpuMat* dest)
 * @brief   Interface to call linear filter
 * @ingroup utils
 * @param[in] source    Array of image to filer
 * @param[in] ksize     Kernel radius
 * @param[in] n_batch   Batch index
 * @param[in] stream    Stream where to launch kernel
 * @param[out] dest     Filtered image
 */
template<typename T, typename D>
void LinearColFilterBatched(const cv::cuda::GpuMat* source,
                            const int ksize,
                            const size_t n_batch,
                            const size_t batch_size,
                            const cv::cuda::Stream& stream,
                            cv::cuda::GpuMat* dest) {
  assert(device::ColFilter::COLUMNS_BLOCKDIM_Y * device::ColFilter::COLUMNS_HALO_STEPS >= ksize);
  assert(source->cols % device::ColFilter::COLUMNS_BLOCKDIM_X == 0);
  assert(source->rows % (device::ColFilter::COLUMNS_RESULT_STEPS * device::ColFilter::COLUMNS_BLOCKDIM_Y) == 0);
  cudaStream_t cstream = cv::cuda::StreamAccessor::getStream(stream);
  /** Column filtering function signature */
  typedef void (*col_t)(const T** src, const size_t bsize, const size_t spitch,
                        const size_t dpitch, const size_t height, D** d);
  /** Define function lookup table for all supported filter size */
  static const col_t filters[] {device::ColFilter::CallBatched<0, T, D>,
                                device::ColFilter::CallBatched<1, T, D>,
                                device::ColFilter::CallBatched<2, T, D>,
                                device::ColFilter::CallBatched<3, T, D>,
                                device::ColFilter::CallBatched<4, T, D>,
                                device::ColFilter::CallBatched<5, T, D>,
                                device::ColFilter::CallBatched<6, T, D>,
                                device::ColFilter::CallBatched<7, T, D>};
  // Get manager
  auto* man = Filter::DefaultFilterManager::Instance().Get();
  man->intermediate_.start_ = n_batch == 0 ? 0 : man->intermediate_.start_;
  man->output_.start_ = n_batch == 0 ? 0 : man->output_.start_;
  size_t start = man->output_.start_;
  for (int b = 0; b < batch_size; ++b) {
    size_t i = start + b;
    man->intermediate_.host_[i] = reinterpret_cast<float*>(source[b].data);
    dest[b].create(source->rows, source->cols, CV_32FC1);
    man->output_.host_[i] = reinterpret_cast<float*>(dest[b].data);
  }
  // Copy pointer to device memory
  man->Upload(batch_size, false ,cstream);
  // Get dimension
  const dim3 bck(source->cols / device::ColFilter::COLUMNS_BLOCKDIM_X,
                 source->rows / (device::ColFilter::COLUMNS_RESULT_STEPS * device::ColFilter::COLUMNS_BLOCKDIM_Y));
  const dim3 thd(device::ColFilter::COLUMNS_BLOCKDIM_X,
                 device::ColFilter::COLUMNS_BLOCKDIM_Y);
  // Get filter
  const col_t f = filters[ksize];
  if (f) {
    const T** in = (const T**)&(man->intermediate_.device_[start]);
    D** out = &(man->output_.device_[start]);
    f<<<bck, thd, 0, cstream>>>(in,
            batch_size,
            source->step1(),
            dest->step1(),
            size_t(source->rows),
            out);
  } else {
    LTS5::CUDA::error("Unsupported dimension for ColFilter",
                      __FILE__,
                      __LINE__,
                      "");
  }
}

}  // namepsace ColFilter

#pragma mark -
#pragma mark Declaration

/** Upload kernel for row filter - Float */
template void RowFilter::SetKernel(const float* k_data,
                                   const int size,
                                   const cv::cuda::Stream& stream);
/** Upload kernel for column filter  - Float */
template void ColFilter::SetKernel(const float* k_data,
                                   const int size,
                                   const cv::cuda::Stream& stream);

/** Row Filter processing : Float */
template void RowFilter::LinearRowFilter<float, float>(const cv::cuda::GpuMat& source,
                                                       const int ksize,
                                                       const cv::cuda::Stream& stream,
                                                       cv::cuda::GpuMat* dest);

/** Col Filter processing : Float */
template void ColFilter::LinearColFilter<float, float>(const cv::cuda::GpuMat& source,
                                                       const int ksize,
                                                       const cv::cuda::Stream& stream,
                                                       cv::cuda::GpuMat* dest);

/** Batched Row filter - Float */
template
void RowFilter::LinearRowFilterBatched<float, float>(const cv::cuda::GpuMat* source,
                                                     const int ksize,
                                                     const size_t n_batch,
                                                     const size_t batch_size,
                                                     const cv::cuda::Stream& stream,
                                                     cv::cuda::GpuMat* dest);

/** Batched Col filter - Float */
template
void ColFilter::LinearColFilterBatched<float, float>(const cv::cuda::GpuMat* source,
                                                     const int ksize,
                                                     const size_t n_batch,
                                                     const size_t batch_size,
                                                     const cv::cuda::Stream& stream,
                                                     cv::cuda::GpuMat* dest);


}  // namepsace CUDA
}  // namepsace LTS5
