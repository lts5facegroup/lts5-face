/**
 *  @file   cublas_wrapper.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   06/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <cublas_v2.h>
#include "opencv2/core/cuda_stream_accessor.hpp"

#include "lts5/cuda/utils/math/cublas_wrapper.hpp"
#include "lts5/cuda/utils/safe_call.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/*
 * @name  Instance
 * @fn    static CublasContext& Instance(void)
 * @brief Provide single instance of CublasContext
 * @return    Cublas context
 */
CublasContext& CublasContext::Instance(void) {
  static CublasContext ctx;
  return ctx;
}

/*
 * @name  ~CublasContext
 * @fn    ~CublasContext(void)
 * @brief Destructor
 */
CublasContext::~CublasContext(void) {
  // Release every handles
  for (auto& h : ctx_) {
    cublasSafeCall(cublasDestroy(h.second));
  }

}

/*
 * @name  CublasContext
 * @fn    CublasContext(void)
 * @brief Constructor
 */
CublasContext::CublasContext(void) {
}

/*
 * @name  Get
 * @fn    cublasContext* Get(void) const
 * @brief Provide access to context state
 * @return    cublas raw context for this specific thread
 */
cublasContext* CublasContext::Get(void) {
  // check if thread has already a handle
  auto id = std::this_thread::get_id();
  cublasContext* handle = nullptr;
  auto p = ctx_.find(id);
  if (p == ctx_.end()) {
    // Create new handle
    cublasSafeCall(cublasCreate(&handle));
    // Ensure single thread push to the map
    std::lock_guard<std::mutex> lock(this->mutex_);
    ctx_.insert({id, handle});
  } else {
    // Known handle
    handle = p->second;
  }
  return handle;
}

/*
 *  @name   Dot
 *  @fn T Dot(const Matrix& a, const Matrix& b, const cv::cuda::Stream& stream)
 *  @brief  Compute the dot product between two vectors
 *  @param[in] a       Vector A
 *  @param[in] b       Vector B
 *  @param[in] stream  Stream on which to run computation
 *  @return Dot product
 */
template<typename T>
T LinearAlgebra<T>::Dot(const Matrix& a,
                        const Matrix& b,
                        const cv::cuda::Stream& stream) {
  return LinearAlgebra<T>::Dot(a, b);
}

template<>
float LinearAlgebra<float>::Dot(const Matrixf& a,
                                const Matrixf& b,
                                const cv::cuda::Stream& stream) {
  assert((a.rows() == b.rows()) && (a.cols() == b.cols()));
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Get handle
  auto* ctx = CublasContext::Instance().Get();
  cublasSafeCall(cublasSetStream(ctx, s));
  // Compute dot product
  const int N = std::max(a.rows(), a.cols());
  float dot = std::numeric_limits<float>::quiet_NaN();
  cublasSafeCall(cublasSdot(ctx, N, a.data(), 1, b.data(),1 ,&dot));
  return dot;
}

template<>
double LinearAlgebra<double>::Dot(const Matrixd& a,
                                const Matrixd& b,
                                const cv::cuda::Stream& stream) {
  assert((a.rows() == b.rows()) && (a.cols() == b.cols()));
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Get handle
  auto* ctx = CublasContext::Instance().Get();
  cublasSafeCall(cublasSetStream(ctx, s));
  // Compute dot product
  const int N = std::max(a.rows(), a.cols());
  double dot = std::numeric_limits<double>::quiet_NaN();
  cublasSafeCall(cublasDdot(ctx, N, a.data(), 1, b.data(),1 ,&dot));
  return dot;
}

/*
 *  @name   Dot
 *  @fn T Dot(const Matrix& a, const Matrix& b)
 *  @brief  Compute the dot product between two vectors
 *  @param[in]  a  Vector A
 *  @param[in]  b  Vector B
 *  @return Dot product
 */
template<typename T>
T LinearAlgebra<T>::Dot(const Matrix& a, const Matrix& b) {
  // stub
  return -1;
}

template<>
float LinearAlgebra<float>::Dot(const Matrixf& a, const Matrixf& b) {
  return LinearAlgebra<float>::Dot(a, b, cv::cuda::Stream::Null());
}

template<>
double LinearAlgebra<double>::Dot(const Matrixd& a, const Matrixd& b) {
  return LinearAlgebra<double>::Dot(a, b, cv::cuda::Stream::Null());
}

/**
 *  @name   Gemv
 *  @fn void Gemv(const Matrix& A,
                 const TransposeType op,
                 const T& alpha,
                 const Matrix& x,
                 const T& beta,
                 const cv::cuda::Stream& stream,
                 Matrix* y)
 *  @brief  Multiplies a matrix by a vector
 *          y = a * Ax + b * y
 *  @param[in]      A         Matrix A
 *  @param[in]      trans_a   Indicate if A is transpose or not
 *  @param[in]      alpha     Scaling factor alpha
 *  @param[in]      x         Vector X
 *  @param[in]      beta      Scaling factor beta
 *  @param[in]      stream    Stream on which to run computation
 *  @param[in,out]  y         Output vector
 */
template<typename T>
void LinearAlgebra<T>::Gemv(const Matrix& A,
                            const TransposeType op,
                            const T& alpha,
                            const Matrix& x,
                            const T& beta,
                            const cv::cuda::Stream& stream,
                            Matrix* y) {
  // stub
}

template<>
void LinearAlgebra<float>::Gemv(const Matrixf& A,
                            const TransposeType op,
                            const float& alpha,
                            const Matrixf& x,
                            const float& beta,
                            const cv::cuda::Stream& stream,
                            Matrixf* y) {
  //Take care for output
  if(op == TransposeType::kNoTranspose) {
    assert(A.cols() == std::max(x.cols(), x.rows()));
    y->Create(A.rows(), 1);
  } else {
    assert(A.rows() == std::max(x.cols(), x.rows()));
    y->Create(A.cols(), 1);
  }
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Get handle
  auto* ctx = CublasContext::Instance().Get();
  cublasSafeCall(cublasSetStream(ctx, s));
  cublasSafeCall(cublasSgemv_v2(ctx,
                                (cublasOperation_t)op,
                                A.rows(),
                                A.cols(),
                                &alpha,
                                A.data(),
                                A.rows(),
                                x.data(),
                                1,
                                &beta,
                                y->data(),
                                1));
}

template<>
void LinearAlgebra<double>::Gemv(const Matrixd& A,
                                const TransposeType op,
                                const double& alpha,
                                const Matrixd& x,
                                const double& beta,
                                const cv::cuda::Stream& stream,
                                Matrixd* y) {
  //Take care for output
  if(op == TransposeType::kNoTranspose) {
    assert(A.cols() == std::max(x.cols(), x.rows()));
    y->Create(A.rows(), 1);
  } else {
    assert(A.rows() == std::max(x.cols(), x.rows()));
    y->Create(A.cols(), 1);
  }
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Get handle
  auto* ctx = CublasContext::Instance().Get();
  cublasSafeCall(cublasSetStream(ctx, s));
  cublasSafeCall(cublasDgemv_v2(ctx,
                                (cublasOperation_t)op,
                                A.rows(),
                                A.cols(),
                                &alpha,
                                A.data(),
                                A.rows(),
                                x.data(),
                                1,
                                &beta,
                                y->data(),
                                1));
}

/*
 *  @name   Gemv
 *  @fn void Gemv(const Matrix& A,
                 const TransposeType trans_a,
                 const T& alpha,
                 const Matrix& x,
                 const T& beta,
                 Matrix* y)
 *  @brief  Multiplies a matrix by a vector
 *          y = a * Ax + b * y
 *  @param[in]      A         Matrix A
 *  @param[in]      trans_a   Indicate if A is transpose or not
 *  @param[in]      alpha     Scaling factor alpha
 *  @param[in]      x         Vector X
 *  @param[in]      beta      Scaling factor beta
 *  @param[in,out]  y         Output vector
 */
template<typename T>
void LinearAlgebra<T>::Gemv(const Matrix& A,
                            const TransposeType op,
                            const T& alpha,
                            const Matrix& x,
                            const T& beta,
                            Matrix* y) {
  //stub
}

template<>
void LinearAlgebra<float>::Gemv(const Matrixf& A,
                                const TransposeType op,
                                const float& alpha,
                                const Matrixf& x,
                                const float& beta,
                                Matrixf* y) {
  LinearAlgebra<float>::Gemv(A, op, alpha, x, beta, cv::cuda::Stream::Null(), y);
}

template<>
void LinearAlgebra<double>::Gemv(const Matrixd& A,
                                 const TransposeType op,
                                 const double& alpha,
                                 const Matrixd& x,
                                 const double& beta,
                                 Matrixd* y) {
  LinearAlgebra<double>::Gemv(A, op, alpha, x, beta, cv::cuda::Stream::Null(), y);
}

/**
 *  @name   Gemm
 *  @fn void Gemm(const Matrix& A,
                 const TransposeType op_a,
                 const T& alpha,
                 const Matrix& B,
                 const TransposeType op_b,
                 const T& beta,
                 const cv::cuda::Stream& stream,
                 Matrix* C)
 *  @brief  Compute the product between two matrices
 *          C = a * AB + b * C
 *  @param[in]      A         Matrix A
 *  @param[in]      op_a      Transpose flag indicator for A
 *  @param[in]      alpha     Alpha coefficient
 *  @param[in]      B         Matrix B
 *  @param[in]      op_b      Transpose flag indicator for B
 *  @param[in]      beta      Beta coefficient
 *  @param[in]      stream    Stream on which to run computation
 *  @param[in,out]  C         Resulting matrix
 */
template<typename T>
void LinearAlgebra<T>::Gemm(const Matrix& A,
                            const TransposeType op_a,
                            const T& alpha,
                            const Matrix& B,
                            const TransposeType op_b,
                            const T& beta,
                            const cv::cuda::Stream& stream,
                            Matrix* C) {
  // stub
}

template<>
void LinearAlgebra<float>::Gemm(const Matrixf& A,
                                const TransposeType op_a,
                                const float& alpha,
                                const Matrixf& B,
                                const TransposeType op_b,
                                const float& beta,
                                const cv::cuda::Stream& stream,
                                Matrixf* C) {
  //Define output matrix size
  const int o_row = op_a == TransposeType::kNoTranspose ?
                    A.rows() :
                    A.cols();
  const int o_col = op_b == TransposeType::kNoTranspose ?
                    B.cols() :
                    B.rows();
  const int K = op_a == TransposeType::kNoTranspose ?
                A.cols() :
                A.rows();
  C->Create(o_row,o_col);
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Get handle
  auto* ctx = CublasContext::Instance().Get();
  cublasSafeCall(cublasSetStream(ctx, s));
  cublasSafeCall(cublasSgemm(ctx,
                             (cublasOperation_t)op_a,
                             (cublasOperation_t)op_b,
                             o_row,
                             o_col,
                             K,
                             &alpha,
                             A.data(),
                             A.rows(),
                             B.data(),
                             B.rows(),
                             &beta,
                             C->data(),
                             C->rows()));
}

template<>
void LinearAlgebra<double>::Gemm(const Matrixd& A,
                                 const TransposeType op_a,
                                 const double& alpha,
                                 const Matrixd& B,
                                 const TransposeType op_b,
                                 const double& beta,
                                 const cv::cuda::Stream& stream,
                                 Matrixd* C) {
  //Define output matrix size
  const int o_row = op_a == TransposeType::kNoTranspose ?
                    A.rows() :
                    A.cols();
  const int o_col = op_b == TransposeType::kNoTranspose ?
                    B.cols() :
                    B.rows();
  const int K = op_a == TransposeType::kNoTranspose ?
                A.cols() :
                A.rows();
  C->Create(o_row,o_col);
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Get handle
  auto* ctx = CublasContext::Instance().Get();
  cublasSafeCall(cublasSetStream(ctx, s));
  cublasSafeCall(cublasDgemm(ctx,
                             (cublasOperation_t)op_a,
                             (cublasOperation_t)op_b,
                             o_row,
                             o_col,
                             K,
                             &alpha,
                             A.data(),
                             A.rows(),
                             B.data(),
                             B.rows(),
                             &beta,
                             C->data(),
                             C->rows()));
}

/*
 *  @name   Gemm
 *  @fn void Gemm(const Matrix& A,
                 const TransposeType op_a,
                 const T& alpha,
                 const Matrix& B,
                 const TransposeType op_b,
                 const T& beta,
                 Matrix* C)
 *  @brief  Compute the product between two matrices
 *          C = a * AB + b * C
 *  @param[in]      A     Matrix A
 *  @param[in]      op_a  Transpose flag indicator for A
 *  @param[in]      alpha Alpha coefficient
 *  @param[in]      B     Matrix B
 *  @param[in]      op_b  Transpose flag indicator for B
 *  @param[in]      beta      Beta coefficient
 *  @param[in,out]  C     Resulting matrix
 */
template<typename T>
void LinearAlgebra<T>::Gemm(const Matrix& A,
                            const TransposeType op_a,
                            const T& alpha,
                            const Matrix& B,
                            const TransposeType op_b,
                            const T& beta,
                            Matrix* C) {
  // stub
}

template<>
void LinearAlgebra<float>::Gemm(const Matrixf& A,
                            const TransposeType op_a,
                            const float& alpha,
                            const Matrixf& B,
                            const TransposeType op_b,
                            const float& beta,
                            Matrixf* C) {
  LinearAlgebra<float>::Gemm(A, op_a, alpha,
                             B, op_b, beta,
                             cv::cuda::Stream::Null(),
                             C);
}

template<>
void LinearAlgebra<double>::Gemm(const Matrixd& A,
                                const TransposeType op_a,
                                const double& alpha,
                                const Matrixd& B,
                                const TransposeType op_b,
                                const double& beta,
                                Matrixd* C) {
  LinearAlgebra<double>::Gemm(A, op_a, alpha,
                              B, op_b, beta,
                              cv::cuda::Stream::Null(),
                              C);
}


}  // namepsace CUDA
}  // namepsace LTS5

