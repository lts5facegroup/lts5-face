/**
 *  @file   tensor.cpp
 *  @brief  Tensor 4D abstraction for cudnn framework
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   22/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "opencv2/core/cuda_stream_accessor.hpp"

#include "lts5/cuda/utils/tensor.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/math/cudnn_utils.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Initialization

/*
 * @name  Tensor
 * @fn    Tensor()
 * @brief Constructor
 */
template<typename T>
Tensor<T>::Tensor() : rows_(0),
                      cols_(0),
                      depth_(0),
                      desc_(nullptr),
                      data_(nullptr),
                      ref_(nullptr) {
  // Init ref counter
  ref_ = new std::atomic<unsigned>(1);
  // Init descriptor
  cudnnSafeCall(cudnnCreateTensorDescriptor(&desc_));
}

/*
 * @name  Tensor
 * @fn    explicit Tensor(const cv::Mat& data)
 * @brief Constructor with data to upload
 * @param data
 */
template<typename T>
Tensor<T>::Tensor(const cv::Mat& data) : rows_(0),
                                         cols_(0),
                                         depth_(0),
                                         desc_(nullptr),
                                         data_(nullptr),
                                         ref_(nullptr) {
  // Init ref counter
  ref_ = new std::atomic<unsigned>(1);
  // Init descriptor
  cudnnSafeCall(cudnnCreateTensorDescriptor(&desc_));
  // Upload
  this->Upload(data, cv::cuda::Stream::Null());
}

/*
 * @name  Tensor
 * @fn  Tensor(const int& row, const int& col,
               const int& depth, T* buffer)
 * @brief Constructor for data with external buffer.
 *        IMPORTANT take ownership of the data
 * @param[in] row     Number of rows
 * @param[in] col     Number of columns
 * @param[in] depth   Depth
 * @param[in] buffer  Device memory buffer allocated by user
 */
template<typename T>
Tensor<T>::Tensor(const int& row,
                  const int& col,
                  const int& depth,
                  T* buffer) : rows_(0),
                               cols_(0),
                               depth_(0),
                               desc_(nullptr),
                               data_(buffer),
                               ref_(nullptr) {
  // Init ref counter
  ref_ = new std::atomic<unsigned>(1);
  // Init descriptor
  cudnnSafeCall(cudnnCreateTensorDescriptor(&desc_));
  // Create
  this->Create(row, col, depth);
}

/*
 * @name  Tensor
 * @fn    Tensor(const Tensor<T>& other)
 * @brief Copy constructor, no deep copy.
 * @param[in] other   Object to copyfrom
 */
template<typename T>
Tensor<T>::Tensor(const Tensor<T>& other) : rows_(other.rows_),
                                            cols_(other.cols_),
                                            depth_(other.depth_),
                                            desc_(other.desc_),
                                            data_(other.data_),
                                            ref_(other.ref_) {
  this->Retain();
}

/**
 * @name  operator=
 * @fn    Tensor<T>& operator=(const Tensor<T>& rhs)
 * @brief Assignment operator
 * @param[in] rhs Object to assign from
 * @return    Newly assigned object
 */
template<typename T>
Tensor<T>& Tensor<T>::operator=(const Tensor<T>& rhs) {
  if (this != &rhs) {
    this->Release();
    rows_ = rhs.rows_;
    cols_ = rhs.cols_;
    depth_ = rhs.depth_;
    desc_ = rhs.desc_;
    data_ = rhs.data_;
    ref_ = rhs.ref_;
    this->Retain();
  }
  return *this;
}

/*
 * @name  ~Tensor
 * @fn    ~Tensor()
 * @brief Destructor
 */
template<typename T>
Tensor<T>::~Tensor() {
  this->Release();
}

/*
 * @name  Create
 * @fn  void Create(const int& rows, const int& cols, const int& depth)
 * @brief Create tensor of a given dimensions
 * @param[in] row     Number of rows
 * @param[in] col     Number of columns
 * @param[in] depth   Tensor's depth
 */
template<typename T>
void Tensor<T>::Create(const int& rows, const int& cols, const int& depth) {
  if (rows_ != rows || cols != cols_ || depth != depth_) {
    // Release buffer already allocated
    if (data_) {
      cudaSafeCall(cudaFree(data_));
      data_ = nullptr;
    }
    rows_ = rows;
    cols_ = cols;
    depth_ = depth;
    cudaSafeCall(cudaMalloc(&data_, rows_ * cols_ * depth_ * sizeof(T)));
    // Update descriptor
    cudnnSafeCall(cudnnSetTensor4dDescriptor(desc_,
                                             CUDNN_TENSOR_NCHW,
                                             DataType<T>::value,
                                             1,
                                             depth_,
                                             rows_,
                                             cols_));
  }
}

/*
 * @name  Clone
 * @fn    Tensor<T> Clone(const cv::cuda::Stream& stream) const
 * @brief Deep copy
 * @param[in] stream  Stream on which to run computation
 * @return    Deep copy of the object
 */
template<typename T>
Tensor<T> Tensor<T>::Clone(const cv::cuda::Stream& stream) const {
  Tensor<T> tensor;
  tensor.Create(this->rows_, this->cols_, this->depth_);
  if (data_ != nullptr) {
    cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                     nullptr :
                     cv::cuda::StreamAccessor::getStream(stream);
    // Copy data
    cudaError err;
    if (s == nullptr) {
      err = cudaMemcpy(reinterpret_cast<void *>(tensor.data()),
                       reinterpret_cast<const void *>(data_),
                       rows_ * cols_ * depth_ * sizeof(T),
                       cudaMemcpyDeviceToDevice);
      cudaSafeCall(cudaDeviceSynchronize());
    } else {
      err = cudaMemcpyAsync(reinterpret_cast<void *>(tensor.data()),
                            reinterpret_cast<const void *>(data_),
                            rows_ * cols_ * depth_ * sizeof(T),
                            cudaMemcpyDeviceToDevice,
                            s);
    }
    if (err) {
      // Something went wrong while copying
      tensor.Release();
      cudaSafeCall(err);
    }
  }
  return tensor;
}

/*
 * @name  Clone
 * @fn    Tensor<T> Clone() const
 * @brief Deep copy
 * @return    Deep copy of the object
 */
template<typename T>
Tensor<T> Tensor<T>::Clone() const {
  return this->Clone(cv::cuda::Stream::Null());
}

/*
 * @name  Upload
 * @fn    void Upload(const cv::Mat& data);
 * @brief upload data to device memory
 * @param[in] data    Data to push into tensor
 */
template<typename T>
void Tensor<T>::Upload(const cv::Mat& data) {
  this->Upload(data, cv::cuda::Stream::Null());
}

/*
 * @name  Upload
 * @fn    void Upload(const cv::Mat& data,  const cv::cuda::Stream& stream);
 * @brief upload data to device memory
 * @param[in] data    Data to push into tensor
 * @param[in] stream  Stream on which to launch the work
 */
template<typename T>
void Tensor<T>::Upload(const cv::Mat& data, const cv::cuda::Stream& stream) {
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   nullptr :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Create tensor
  int row = data.size[0];
  int col = data.size[1];
  int depth = data.size[2];
  this->Create(row, col, depth);
  // Copy data
  if (s == nullptr) {
    cudaSafeCall(cudaMemcpy(data_,
                            data.data, row * col * depth * sizeof(T),
                            cudaMemcpyHostToDevice));
    cudaSafeCall(cudaDeviceSynchronize());
  } else {
    cudaSafeCall(cudaMemcpyAsync(data_,
                                 data.data, row * col * depth * sizeof(T),
                                 cudaMemcpyHostToDevice,
                                 s));
  }
}

/*
 * @name  Download
 * @fn  void Download(cv::Mat* tensor) const
 * @brief Copy tensor from Device to Host
 * @param[out]  tensor  Host tensor where to store data
 */
template<typename T>
void Tensor<T>::Download(cv::Mat* tensor) const {
  this->Download(cv::cuda::Stream::Null(), tensor);
}

/*
 * @name  Download
 * @fn  void Download(const cv::cuda::Stream& stream, cv::Mat* tensor) const
 * @brief Copy tensor from Device to Host
 * @param[in] stream  Stream on which to run computation
 * @param[out]  tensor  Host tensor where to store data
 */
template<typename T>
void Tensor<T>::Download(const cv::cuda::Stream& stream, cv::Mat* tensor) const {
  if (data_) {
    // Get stream
    cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                     nullptr:
                     cv::cuda::StreamAccessor::getStream(stream);
    // Init output
    if (depth_ == 1) {
      tensor->create(rows_, cols_, cv::DataType<T>::type);
    } else {
      int dim[3] = {rows_, cols_, depth_};
      tensor->create(3, dim, cv::DataType<T>::type);
    }
    // Copy device to host
    if (s == nullptr) {
      cudaSafeCall(cudaMemcpy(tensor->data,
                              data_,
                              rows_ * cols_ * depth_ * sizeof(T),
                              cudaMemcpyDeviceToHost));
      cudaSafeCall(cudaDeviceSynchronize());
    } else {
      cudaSafeCall(cudaMemcpyAsync(tensor->data,
                                   data_,
                                   rows_ * cols_ * depth_ * sizeof(T),
                                   cudaMemcpyDeviceToHost,
                                   s));
    }
  }
}

#pragma mark -
#pragma mark Private

/*
 *  @name Retain
 *  @fn void Retain()
 *  @brief  Increase reference counter
 *  @see https://stackoverflow.com/questions/10268737/
 */
template<typename T>
void Tensor<T>::Retain() {
  std::atomic_fetch_add_explicit(ref_, 1u, std::memory_order_relaxed);
}

/*
 *  @name Release
 *  @fn void Release()
 *  @brief  Release object memory if reference counter reach zero
 *  @see https://stackoverflow.com/questions/10268737/
 */
template<typename T>
void Tensor<T>::Release() {
  if (std::atomic_fetch_sub_explicit(ref_,
                                     1u,
                                     std::memory_order_release) == 1) {
    std::atomic_thread_fence(std::memory_order_acquire);
    // Clean up
    if (data_) {
      cudaSafeCall(cudaFree(data_));
      data_ = nullptr;
    }
    // clear descriptor
    if (desc_) {
      cudnnSafeCall(cudnnDestroyTensorDescriptor(desc_));
      desc_ = nullptr;
    }
    if (ref_) {
      delete ref_;
      ref_ = nullptr;
    }
    // clear dimensions
    rows_ = 0;
    cols_ = 0;
    depth_ = 0;
  }
}

#pragma mark -
#pragma mark Explicit instantiation

/** Tensor - Float */
template class Tensor<float>;
/** Tensor - Double */
template class Tensor<double>;
/** Tensor - int32 */
template class Tensor<int32_t>;

}  // namepsace CUDA
}  // namepsace LTS5