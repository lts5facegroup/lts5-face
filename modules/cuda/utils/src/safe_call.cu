/**
 *  @file   safe_call.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   08/07/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <vector>

#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {


#pragma mark -
#pragma mark Error Handling

/*
 * @name
 * @fn  void error(const char *error_string,
                   const char *file,
                   const int line,
                   const char *func)
 * @brief Print error
 * @param[in] error_string  Error message
 * @param[in] file          File that trigger the error
 * @param[in] line          Line where the error was throw
 * @param[in] func          Function that throw error
 */
void error(const char *error_string, const char *file, const int line, const char *func) {
  Logger::Instance().Log(Logger::Level::kError,
                         file,
                         line,
                         Logger::LogData<LTS5::Logger::None>() << error_string);
  //std::cout << "LTS5 error: " << error_string << "\t" << file << ":" << line << std::endl;
}


#pragma mark -
#pragma mark Profiling

/** Array of color code */
const constexpr uint32_t Trace::color[];
/** Array dimension */
const int Trace::n_color;

}  // namepsace CUDA
}  // namepsace LTS5
