/**
 *  @file   separable_filter.cu
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   13/10/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <cuda_runtime.h>

#include "opencv2/core/cuda_stream_accessor.hpp"

#include "lts5/cuda/utils/separable_filter.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/device/separable_filter_kernel.h"

/**
*  @namespace  LTS5
*  @brief      LTS5 laboratory dev space
*/
namespace LTS5 {
/**
*  @namespace  CUDA
*  @brief      CUDA dev space
*/
namespace CUDA {

#pragma mark -
#pragma mark Initialization

/*
 * @name  SeparableFilter
 * @fn    SeparableFilter(void)
 * @brief Constructor
 */
template<typename T>
SeparableFilter<T>::SeparableFilter(void) {}

/*
 * @name  SeparableFilter
 * @fn    SeparableFilter(const T* kernel,
                           const int size,
                           const cv::cuda::Stream& stream)
 * @brief Constructor
 * @param[in] kernel  Kerne data
 * @param[in] size    Kernel size
 * @param[in] stream  Stream to run on
 */
template<typename T>
SeparableFilter<T>::SeparableFilter(const T* kernel,
                                    const int size,
                                    const cv::cuda::Stream& stream) {
  this->SetKernel(kernel, size, stream);
}

/**
 * @name  SeparableFilter
 * @fn    SeparableFilter(const SeparableFilter& other)
 * @brief Copy constructor
 * @param[in] other Object to copy from
 */
template<typename T>
SeparableFilter<T>::SeparableFilter(const SeparableFilter& other) {
  kernel_size_ = other.kernel_size_;
  kernel_radius_ = other.kernel_radius_;
  buffer_ = other.buffer_.clone();
}

/**
 * @name  operator=
 * @fn    SeparableFilter& operator=(const SeparableFilter& rhs)
 * @brief Assignment operator
 * @param[in] rhs Object to assign from
 * @return    Newly assigned object
 */
template<typename T>
SeparableFilter<T>& SeparableFilter<T>::operator=(const SeparableFilter<T>& rhs) {
  if (this != &rhs) {
    kernel_size_ = rhs.kernel_size_;
    kernel_radius_ = rhs.kernel_radius_;
    buffer_ = rhs.buffer_.clone();
  }
  return *this;
}

/*
 * @name  ~SeparableFilter
 * @fn    ~SeparableFilter(void)
 * @brief Destructor
 */
template<typename T>
SeparableFilter<T>::~SeparableFilter(void) {

}

#pragma mark -
#pragma mark Usage

/**
 * @name  SetKernel
 * @fn    void SetKernel(const T* kernel,
                         const int size,
                         const cv::gpu::Stream& stream,)
 * @brief Load kernel into GPU (symbol constant memory)
 * @param[in] kernel  Kerne data
 * @param[in] size    Kernel size
 * @param[in] stream  Cuda stream to use to run operation
 */
template<typename T>
void SeparableFilter<T>::SetKernel(const T *kernel,
                                   const int size,
                                   const cv::cuda::Stream& stream) {
  // Init
  kernel_size_ = size;
  kernel_radius_ = (kernel_size_- 1)/2;
  // load row filter data
  RowFilter::SetKernel(kernel, size, stream);
  // load column filter data
  ColFilter::SetKernel(kernel, size, stream);
}

/*
 * @name  Apply
 * @fn  template<typename T>
void SeparableFilter<T>::Apply(const cv::cuda::GpuMat& source,
                               const cv::cuda::Stream& stream,
                               cv::cuda::GpuMat* dest)
 * @brief Apply filter
 * @param[in] source  Image to filter (Dimension must be a multiple of 64)
 * @param[in] stream  Stream on where to launch the computation
 * @param[out] dest    Filtered image
 */
template<typename T>
void SeparableFilter<T>::Apply(const cv::cuda::GpuMat& source,
                               const cv::cuda::Stream& stream,
                               cv::cuda::GpuMat* dest) {
  // Apply filter
  this->ApplyRow(source, stream, &buffer_);
  this->ApplyColumn(buffer_, stream, dest);
}

/*
 * @name  Apply
 * @fn    void Apply(const cv::cuda::GpuMat& source,
                     const cv::cuda::Stream& stream,
                     cv::cuda::GpuMat* buffer,
                     cv::cuda::GpuMat* dest)
 * @brief Apply filter
 * @param[in] source  Image to filter (Dimension must be a multiple of 64)
 * @param[in] stream  Stream on where to launch the computation
 * @param[in] buffer  Temporary buffer where to store partially filtered image
 * @param[out] dest    Filtered image
 */
template<typename T>
void SeparableFilter<T>::Apply(const cv::cuda::GpuMat& source,
                               const cv::cuda::Stream& stream,
                               cv::cuda::GpuMat* buffer,
                               cv::cuda::GpuMat* dest) {
  // Apply filter
  this->ApplyRow(source, stream, buffer);
  this->ApplyColumn(*buffer, stream, dest);
}

/*
 * @name  ApplyRow
 * @fn
 * @brief Apply filter on the row
 * @param[in] source  Image to filter (Dimension must be a multiple of 64)
 * @param[in] stream  Stream on where to launch the computation
 * @param[out] dest    Filtered image
 */
template<typename T>
void SeparableFilter<T>::ApplyRow(const cv::cuda::GpuMat& source,
                                  const cv::cuda::Stream& stream,
                                  cv::cuda::GpuMat* dest) {
  // Apply filter
  dest->create(source.rows, source.cols, source.type());
  RowFilter::LinearRowFilter<T,T>(source,
                                  kernel_radius_,
                                  stream,
                                  dest);
}

/*
 * @name  ApplyRowBatched
 * @fn    void ApplyRowBatched(const cv::cuda::GpuMat* sources,
                               const size_t n_batch,
                               const size_t batch_size,
                               const cv::cuda::Stream& stream,
                               cv::cuda::GpuMat* dest)
 * @brief Apply row filter on every images
 * @param[in] sources     Array of images to filter
 * @param[in] n_batch     Current batch index
 * @param[in] batch_size  Batch size, number of image to filter
 * @param[in] stream      Stream on which to run computation
 * @param[in,out] dest    Array of filtered images
 */
template<typename T>
void SeparableFilter<T>::ApplyRowBatched(const cv::cuda::GpuMat* sources,
                                         const size_t n_batch,
                                         const size_t batch_size,
                                         const cv::cuda::Stream& stream,
                                         cv::cuda::GpuMat* dest) {
  RowFilter::LinearRowFilterBatched<T,T>(sources,
                                         kernel_radius_,
                                         n_batch,
                                         batch_size,
                                         stream, dest);
}

/*
 * @name  ApplyColumn
 * @fn
 * @brief Apply filter on the column
 * @param[in] source  Image to filter (Dimension must be a multiple of 64)
 * @param[in] stream  Stream on where to launch the computation
 * @param[out] dest    Filtered image
 */
template<typename T>
void SeparableFilter<T>::ApplyColumn(const cv::cuda::GpuMat& source,
                                     const cv::cuda::Stream& stream,
                                     cv::cuda::GpuMat* dest) {
  // Apply filter
  dest->create(source.rows, source.cols, source.type());
  ColFilter::LinearColFilter<T,T>(source,
                                  kernel_radius_,
                                  stream,
                                  dest);
}

/*
 * @name  ApplyColumnBatched
 * @fn    void ApplyColumnBatched(const cv::cuda::GpuMat* sources,
                               const size_t n_batch,
                               const size_t batch_size,
                               const cv::cuda::Stream& stream,
                               cv::cuda::GpuMat* dest)
 * @brief Apply row filter on every images
 * @param[in] sources     Array of images to filter
 * @param[in] n_batch     Current batch index
 * @param[in] batch_size  Batch size, number of image to filter
 * @param[in] stream      Stream on which to run computation
 * @param[in,out] dest    Array of filtered images
 */
template<typename T>
void SeparableFilter<T>::ApplyColumnBatched(const cv::cuda::GpuMat* sources,
                                            const size_t n_batch,
                                            const size_t batch_size,
                                            const cv::cuda::Stream& stream,
                                            cv::cuda::GpuMat* dest) {
  ColFilter::LinearColFilterBatched<T,T>(sources,
                                         kernel_radius_,
                                         n_batch,
                                         batch_size,
                                         stream,
                                         dest);
}

#pragma mark -
#pragma mark Declaration

/** Filter type=float */
template class SeparableFilter<float>;


}  // namepsace CUDA
}  // namepsace LTS5

