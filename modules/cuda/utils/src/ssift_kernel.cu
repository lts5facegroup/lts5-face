/**
 *  @file   ssift_kernel.cu
 *  @brief  GPU Custom kernel for SSift computation
 *
 *  Created by Christophe Ecabert on 05/05/16.
 *  Copyright (c) 2016 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <stdlib.h>
#include <cfloat>
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "opencv2/core.hpp"
#include "opencv2/core/cuda.hpp"
#include "opencv2/core/cuda_stream_accessor.hpp"

#include "lts5/utils/ssift.hpp"
#include "lts5/utils/threaded_ressource_manager.hpp"
#include "lts5/cuda/utils/device/ssift_kernel.h"
#include "lts5/cuda/utils/safe_call.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
*  @namespace  CUDA
*  @brief      CUDA dev space
*/
namespace CUDA {

/** Atan2 partial coefficient 1 */
static float __constant__ d_atan2_p1 = 0.9997878412794807f*(float)(180/CV_PI);
/** Atan2 partial coefficient 3 */
static float __constant__ d_atan2_p3 = -0.3258083974640975f*(float)(180/CV_PI);
/** Atan2 partial coefficient 5 */
static float __constant__ d_atan2_p5 = 0.1555786518463281f*(float)(180/CV_PI);
/** Atan2 partial coefficient 7 */
static float __constant__ d_atan2_p7 = -0.04432655554792128f*(float)(180/CV_PI);

/**
 * @namespace Ssift
 * @brief Space for Ssift helper class / function
 */
namespace SSIFT {

/**
 * @class   BatchBuffer
 * @brief   Class holding buffers for batched ssift computation
 * @author  Christophe Ecabert
 * @date    16/06/2017
 * @ingroup utils
 */
template<typename T>
class BatchBuffer {
 public:

  /**
   * @name  BatchBuffer
   * @fn    BatchBuffer(const size_t size)
   * @brief Construct a batch buffer of a given \p size
   * @param[in] size    Number of element in the buffer
   */
  BatchBuffer(const size_t size) : size_(size * sizeof(T)),
                                   host_(nullptr),
                                   device_(nullptr) {
    // Alloc host pinned memory
    cudaSafeCall(cudaMallocHost(&host_, size_));
    // Alloc device
    cudaSafeCall(cudaMalloc(&device_, size_));
  }

  /**
   * @name  ~BatchBuffer
   * @fn    ~BatchBuffer(void)
   * @brief Destructor
   */
  ~BatchBuffer(void) {
    if (device_) {
      cudaSafeCall(cudaFree(device_));
      device_ = nullptr;
    }
    if (host_) {
      cudaSafeCall(cudaFreeHost(host_));
      host_ = nullptr;
    }
  }

  /**
   * @name  Upload
   * @fn    void Upload(const size_t batch_size,
                        const cv::cuda::Stream& stream)
   * @brief Copy host buffer to device memory
   * @param[in] batch_size  Batch size
   * @param[in] stream  Stream on which to launch transfer
   */
  void Upload(const size_t batch_size,
              const cv::cuda::Stream& stream) {
    cudaStream_t cs = cv::cuda::StreamAccessor::getStream(stream);
    cudaSafeCall(cudaMemcpyAsync(&device_[start_],
                                 &host_[start_],
                                 batch_size * sizeof(T),
                                 cudaMemcpyHostToDevice,
                                 cs));
    start_ += batch_size;
  }

  /** Buffer size in byte */
  size_t size_;
  /** Starting index */
  size_t start_;
  /** Host buffer with pinned memory */
  T* host_;
  /** Device device buffer*/
  T* device_;
};

/**
 * @class   DescriptorBuffer
 * @brief   Class holding all buffers for batched ssift computation
 * @author  Christophe Ecabert
 * @date    16/06/2017
 * @ingroup utils
 */
template<size_t D>
class DescriptorBuffer {
 public:

  /**
   * @name  DescriptorBuffer
   * @fn    DescriptorBuffer(void)
   * @brief Constructor
   */
  DescriptorBuffer(void) : image_patch_(D),
                           gray_patch_(D),
                           set_(D),
                           f_image_(D),
                           kernel_parameter_(D),
                           histogram_(D) {
  }

  /**
   * @name  ~DescriptorBuffer
   * @fn    ~DescriptorBuffer(void)
   * @brief Destructor
   */
  ~DescriptorBuffer(void) {
  }

  /**
   * @name  UploadConversion
   * @fn    void UploadConversion(const size_t batch_size,
                                  const cv::cuda::Stream& stream)
   * @brief Copy host buffer to device buffer for conversion step
   * @param[in] batch_size  Batch size
   * @param[in] stream  Stream on which to run the transfer
   */
  void UploadConversion(const size_t batch_size,
                        const cv::cuda::Stream& stream) {
    image_patch_.Upload(batch_size, stream);
    gray_patch_.Upload(batch_size, stream);
  }

  /**
   * @name  UploadSet
   * @fn    void UploadSet(const size_t batch_size,
                          const cv::cuda::Stream& stream)
   * @brief Copy host buffer to device buffer for initialization step
   *        (set_value)
   * @param[in] batch_size  Batch size
   * @param[in] stream  Stream on which to run the transfer
   */
  void UploadSet(const size_t batch_size,
                 const cv::cuda::Stream& stream) {
    set_.Upload(batch_size, stream);
  }

  /**
   * @name  UploadSiftFilter
   * @fn    void UploadSiftFilter(const size_t batch_size,
                                    const cv::cuda::Stream& stream)
   * @brief Copy host buffer to device buffer for initialization step
   *        (set_value)
   * @param[in] batch_size  Batch size
   * @param[in] stream  Stream on which to run the transfer
   */
  void UploadSiftFilter(const size_t batch_size,
                 const cv::cuda::Stream& stream) {
    f_image_.Upload(batch_size, stream);
    kernel_parameter_.Upload(batch_size, stream);
    histogram_.Upload(batch_size, stream);
  }


  /** Input image patches */
  BatchBuffer<uchar*> image_patch_;
  /** Grayscale patches */
  BatchBuffer<float*> gray_patch_;
  /** Set buffer */
  BatchBuffer<float*> set_;
  /** Filtered image */
  BatchBuffer<float*> f_image_;
  /** Sift kernel parameters */
  BatchBuffer<GPUSSift::KernelParameter> kernel_parameter_;
  /** histogram */
  BatchBuffer<float*> histogram_;
};

/** Default buffer */
using DefaultDescriptorBuffer = DescriptorBuffer<68>;
/** Default Manager */
using DefaultRessourceManager = LTS5::ThreadedRessourceManager<DefaultDescriptorBuffer>;


/** Function pointer to gray/color_to_float function */
typedef void (*img2f_t)(const uchar **img,
                        const size_t pitch,
                        const size_t dpitch,
                        float **gray);

/** Function selection based on batch dimension */
static const img2f_t g2f[] = { 0,
                               device::gray_to_float_batched<1>,
                               device::gray_to_float_batched<2>,
                               device::gray_to_float_batched<3>,
                               device::gray_to_float_batched<4>,
                               device::gray_to_float_batched<5>,
                               device::gray_to_float_batched<6>,
                               device::gray_to_float_batched<7>,
                               device::gray_to_float_batched<8>,
                               device::gray_to_float_batched<9>,
                               device::gray_to_float_batched<10>,
                               device::gray_to_float_batched<11>,
                               device::gray_to_float_batched<12>,
                               device::gray_to_float_batched<13>,
                               device::gray_to_float_batched<14>,
                               device::gray_to_float_batched<15>,
                               device::gray_to_float_batched<16>,
                               device::gray_to_float_batched<17>,
                               device::gray_to_float_batched<18>,
                               device::gray_to_float_batched<19>,
                               device::gray_to_float_batched<20>,
                               device::gray_to_float_batched<21>,
                               device::gray_to_float_batched<22>,
                               device::gray_to_float_batched<23>,
                               device::gray_to_float_batched<24>,
                               device::gray_to_float_batched<25>,
                               device::gray_to_float_batched<26>,
                               device::gray_to_float_batched<27>,
                               device::gray_to_float_batched<28>,
                               device::gray_to_float_batched<29>,
                               device::gray_to_float_batched<30>,
                               device::gray_to_float_batched<31>,
                               device::gray_to_float_batched<32>};

/** Function selection based on batch dimension */
static const img2f_t c2f[] = { 0,
                               device::color_to_gray_batched<1>,
                               device::color_to_gray_batched<2>,
                               device::color_to_gray_batched<3>,
                               device::color_to_gray_batched<4>,
                               device::color_to_gray_batched<5>,
                               device::color_to_gray_batched<6>,
                               device::color_to_gray_batched<7>,
                               device::color_to_gray_batched<8>,
                               device::color_to_gray_batched<9>,
                               device::color_to_gray_batched<10>,
                               device::color_to_gray_batched<11>,
                               device::color_to_gray_batched<12>,
                               device::color_to_gray_batched<13>,
                               device::color_to_gray_batched<14>,
                               device::color_to_gray_batched<15>,
                               device::color_to_gray_batched<16>,
                               device::color_to_gray_batched<17>,
                               device::color_to_gray_batched<18>,
                               device::color_to_gray_batched<19>,
                               device::color_to_gray_batched<20>,
                               device::color_to_gray_batched<21>,
                               device::color_to_gray_batched<22>,
                               device::color_to_gray_batched<23>,
                               device::color_to_gray_batched<24>,
                               device::color_to_gray_batched<25>,
                               device::color_to_gray_batched<26>,
                               device::color_to_gray_batched<27>,
                               device::color_to_gray_batched<28>,
                               device::color_to_gray_batched<29>,
                               device::color_to_gray_batched<30>,
                               device::color_to_gray_batched<31>,
                               device::color_to_gray_batched<32>};

/** Function pointer to set_value_batched function */
typedef void (*s2v_t)(const float value, const int N, float** array);
/** Function lookup table for batched operation */
static const s2v_t s2v[] {0,
                          device::set_value_batched<1>,
                          device::set_value_batched<2>,
                          device::set_value_batched<3>,
                          device::set_value_batched<4>,
                          device::set_value_batched<5>,
                          device::set_value_batched<6>,
                          device::set_value_batched<7>,
                          device::set_value_batched<8>,
                          device::set_value_batched<9>,
                          device::set_value_batched<10>,
                          device::set_value_batched<11>,
                          device::set_value_batched<12>,
                          device::set_value_batched<13>,
                          device::set_value_batched<14>,
                          device::set_value_batched<15>,
                          device::set_value_batched<16>,
                          device::set_value_batched<17>,
                          device::set_value_batched<18>,
                          device::set_value_batched<19>,
                          device::set_value_batched<20>,
                          device::set_value_batched<21>,
                          device::set_value_batched<22>,
                          device::set_value_batched<23>,
                          device::set_value_batched<24>,
                          device::set_value_batched<25>,
                          device::set_value_batched<26>,
                          device::set_value_batched<27>,
                          device::set_value_batched<28>,
                          device::set_value_batched<29>,
                          device::set_value_batched<30>,
                          device::set_value_batched<31>,
                          device::set_value_batched<32>};

/** Function pointer to set_value_batched function */
typedef void (*sift_t)(const float** image,
                       const size_t pitch,
                       const int width,
                       const int height,
                       const GPUSSift::KernelParameter* parameters,
                       float** histogram);
/** Function selection based on batch dimension */
static const sift_t sift_filters[] = {0,
                                      device::sample_histogram_batched<1>,
                                      device::sample_histogram_batched<2>,
                                      device::sample_histogram_batched<3>,
                                      device::sample_histogram_batched<4>,
                                      device::sample_histogram_batched<5>,
                                      device::sample_histogram_batched<6>,
                                      device::sample_histogram_batched<7>,
                                      device::sample_histogram_batched<8>,
                                      device::sample_histogram_batched<9>,
                                      device::sample_histogram_batched<10>,
                                      device::sample_histogram_batched<11>,
                                      device::sample_histogram_batched<12>,
                                      device::sample_histogram_batched<13>,
                                      device::sample_histogram_batched<14>,
                                      device::sample_histogram_batched<15>,
                                      device::sample_histogram_batched<16>,
                                      device::sample_histogram_batched<17>,
                                      device::sample_histogram_batched<18>,
                                      device::sample_histogram_batched<19>,
                                      device::sample_histogram_batched<20>,
                                      device::sample_histogram_batched<21>,
                                      device::sample_histogram_batched<22>,
                                      device::sample_histogram_batched<23>,
                                      device::sample_histogram_batched<24>,
                                      device::sample_histogram_batched<25>,
                                      device::sample_histogram_batched<26>,
                                      device::sample_histogram_batched<27>,
                                      device::sample_histogram_batched<28>,
                                      device::sample_histogram_batched<29>,
                                      device::sample_histogram_batched<30>,
                                      device::sample_histogram_batched<31>,
                                      device::sample_histogram_batched<32>};

/** Function pointer to set_value_batched function */
typedef void (*fin_sift_t)(const GPUSSift::KernelParameter* parameters,
                           float** histogram);
/** Function selection based on batch dimension */
static const fin_sift_t fin_sift[] = {0,
                                      device::finalize_histogram_batched<1>,
                                      device::finalize_histogram_batched<2>,
                                      device::finalize_histogram_batched<3>,
                                      device::finalize_histogram_batched<4>,
                                      device::finalize_histogram_batched<5>,
                                      device::finalize_histogram_batched<6>,
                                      device::finalize_histogram_batched<7>,
                                      device::finalize_histogram_batched<8>,
                                      device::finalize_histogram_batched<9>,
                                      device::finalize_histogram_batched<10>,
                                      device::finalize_histogram_batched<11>,
                                      device::finalize_histogram_batched<12>,
                                      device::finalize_histogram_batched<13>,
                                      device::finalize_histogram_batched<14>,
                                      device::finalize_histogram_batched<15>,
                                      device::finalize_histogram_batched<16>,
                                      device::finalize_histogram_batched<17>,
                                      device::finalize_histogram_batched<18>,
                                      device::finalize_histogram_batched<19>,
                                      device::finalize_histogram_batched<20>,
                                      device::finalize_histogram_batched<21>,
                                      device::finalize_histogram_batched<22>,
                                      device::finalize_histogram_batched<23>,
                                      device::finalize_histogram_batched<24>,
                                      device::finalize_histogram_batched<25>,
                                      device::finalize_histogram_batched<26>,
                                      device::finalize_histogram_batched<27>,
                                      device::finalize_histogram_batched<28>,
                                      device::finalize_histogram_batched<29>,
                                      device::finalize_histogram_batched<30>,
                                      device::finalize_histogram_batched<31>,
                                      device::finalize_histogram_batched<32>};

/** Function pointer to set_value_batched function */
typedef void (*r_sift_t)(const GPUSSift::KernelParameter* parameters,
                         float** histogram);
/** Function selection based on batch dimension */
static const r_sift_t root_sift[] = {0,
                                     device::root_sift_batched<1>,
                                     device::root_sift_batched<2>,
                                     device::root_sift_batched<3>,
                                     device::root_sift_batched<4>,
                                     device::root_sift_batched<5>,
                                     device::root_sift_batched<6>,
                                     device::root_sift_batched<7>,
                                     device::root_sift_batched<8>,
                                     device::root_sift_batched<9>,
                                     device::root_sift_batched<10>,
                                     device::root_sift_batched<11>,
                                     device::root_sift_batched<12>,
                                     device::root_sift_batched<13>,
                                     device::root_sift_batched<14>,
                                     device::root_sift_batched<15>,
                                     device::root_sift_batched<16>,
                                     device::root_sift_batched<17>,
                                     device::root_sift_batched<18>,
                                     device::root_sift_batched<19>,
                                     device::root_sift_batched<20>,
                                     device::root_sift_batched<21>,
                                     device::root_sift_batched<22>,
                                     device::root_sift_batched<23>,
                                     device::root_sift_batched<24>,
                                     device::root_sift_batched<25>,
                                     device::root_sift_batched<26>,
                                     device::root_sift_batched<27>,
                                     device::root_sift_batched<28>,
                                     device::root_sift_batched<29>,
                                     device::root_sift_batched<30>,
                                     device::root_sift_batched<31>,
                                     device::root_sift_batched<32>};

}
/*
 * @name    SetValue
 * @fn  void SetValue(const float value, const int N,
                      const cv::cuda::Stream &stream, float* array)
 * @brief   Initialize an array to a given value
 * @param[in] value         Value to set
 * @param[in] N             Array dimension
 * @param[in] stream        Stream on which to launch the computation
 * @param[in,out] array     Array to initialize
 */
void SetValue(const float value,
              const int N,
              const cv::cuda::Stream &stream,
              float* array) {
  // Compute dimension
  int thd = 128;
  int bck = (N + 127) / 128;
  cudaStream_t cs = cv::cuda::StreamAccessor::getStream(stream);
  device::set_value<<<bck, thd, 0, cs>>>(value, N, array);
}

/*
 * @name    SetValue
 * @fn  void SetValue(const float value,
                       const int N,
                       const size_t n_batch,
                       const size_t batch_size,
                       const cv::cuda::Stream &stream,
                       float** array)
 * @brief   Initialize an array of array to a given value
 * @param[in] value         Value to set
 * @param[in] N             Array dimension
 * @param[in] n_batch       Current batch index
 * @param[in] batch_size    Batch size
 * @param[in] stream        Stream on which to launch the computation
 * @param[in,out] array     Array to initialize
 * @ingroup utils
 */
void SetValueBatched(const float value,
                     const int N,
                     const size_t n_batch,
                     const size_t batch_size,
                     const cv::cuda::Stream &stream,
                     float* array) {
  auto* man = SSIFT::DefaultRessourceManager::Instance().Get();
  // Loop over batch size
  man->set_.start_ = n_batch == 0 ? 0 : man->set_.start_;
  size_t start = man->set_.start_;
  for (int b = 0; b < batch_size; ++b) {
    size_t i = man->set_.start_ + b;
    man->set_.host_[i] = &(array[b * N]);
  }
  // Copy pointer to device memory
  man->UploadSet(batch_size, stream);
  // Compute dimension
  int thd = 128;
  int bck = (N + 127) / 128;
  // Launch kernel
  const SSIFT::s2v_t f = SSIFT::s2v[batch_size];
  if (f) {
    cudaStream_t cs = cv::cuda::StreamAccessor::getStream(stream);
    float** data = &(man->set_.device_[start]);
    f<<<bck, thd, 0, cs>>>(value, N, data);
  } else {
    std::cout << "Error, invalid function pointer" << std::endl;
  }
}

/*
 * @name    ImageRoiToGrayFloat
 * @fn  void ImageRoiToGrayFloat(const cv::cuda::GpuMat& image,
                     const cv::Rect& roi,
                     const cv::cuda::Stream &stream,
                     cv::cuda::GpuMat* imagef)
 * @brief   Convert a given image roi into grayscale float image
 * @param[in] image Image to convert
 * @param[in] roi   Region of interest
 * @param[in] stream    Stream on which to launch the computation
 * @param[in,out] imagef    Converted image
 * @ingroup utils
 */
void ImageRoiToGrayFloat(const cv::cuda::GpuMat& image,
                         const cv::Rect& roi,
                         const cv::cuda::Stream &stream,
                         cv::cuda::GpuMat* imagef) {
  // Init output
  imagef->create(roi.height, roi.width, CV_32FC1);
  // Define dimension
  dim3 thd(32, 32);
  dim3 bck(((uint)roi.height + 31) / 32, ((uint)roi.width + 31) / 32);
  float* dest = reinterpret_cast<float*>(imagef->data);
  cudaStream_t cs = cv::cuda::StreamAccessor::getStream(stream);
  if (image.channels() != 1) {
    const uchar* img = image.data + roi.y * image.step + (3*roi.x);
    device::color_to_gray<<<bck, thd, 0, cs>>>(img,
            image.step,
            imagef->step1(),
            dest);
  } else {
    const uchar* img = image.data + roi.y * image.step + roi.x;
    device::gray_to_float<<<bck, thd, 0, cs>>>(img,
            image.step,
            imagef->step1(),
            dest);
  }
}

/*
 * @name    ImageRoiToGrayFloatBatched
 * @fn  void ImageRoiToGrayFloatBatched(const cv::cuda::GpuMat& image,
                                        const cv::Rect* roi,
                                        const size_t n_batch,
                                        const size_t batch_size,
                                        const cv::cuda::Stream &stream,
                                        cv::cuda::GpuMat** imagef)
 * @brief   Convert a given image roi into grayscale float image
 * @param[in] image Array of images to convert
 * @param[in] roi   Array of region of interest
 * @param[in] n_batch Current batch index
 * @param[in] batch_size    Size of the batch
 * @param[in] stream    Stream on which to launch the computation
 * @param[in,out] imagef    Array of converted image
 * @ingroup utils
 */
void ImageRoiToGrayFloatBatched(const cv::cuda::GpuMat& image,
                                const cv::Rect* roi,
                                const size_t n_batch,
                                const size_t batch_size,
                                const cv::cuda::Stream &stream,
                                cv::cuda::GpuMat* imagef) {
  auto* man = SSIFT::DefaultRessourceManager::Instance().Get();
  // Loop over batch size

  man->image_patch_.start_ = n_batch == 0 ? 0 : man->image_patch_.start_;
  man->gray_patch_.start_ = n_batch == 0 ? 0 : man->gray_patch_.start_;
  size_t start = man->image_patch_.start_;
  for (int b = 0; b < batch_size; ++b) {
    size_t i = man->image_patch_.start_ + b;
    man->image_patch_.host_[i] = image.data + roi[b].y * image.step;
    man->image_patch_.host_[i] += image.channels() != 1 ? 3*roi[b].x : roi[b].x;
    imagef[b].create(roi[b].height, roi[b].width, CV_32FC1);
    man->gray_patch_.host_[i] = reinterpret_cast<float*>(imagef[b].data);
  }
  // Copy pointer to device memory
  man->UploadConversion(batch_size, stream);
  // Define dimension
  dim3 thd(32, 32);
  dim3 bck(((uint)roi->height + 31) / 32, ((uint)roi->width + 31) / 32);

  const SSIFT::img2f_t converter = image.channels() != 1 ?
                                   SSIFT::c2f[batch_size] :
                                   SSIFT::g2f[batch_size];
  if (converter) {
    cudaStream_t cs = cv::cuda::StreamAccessor::getStream(stream);
    converter<<<bck, thd, 0, cs>>>((const uchar**)&(man->image_patch_.device_[start]),
            image.step,
            imagef->step1(),
            &(man->gray_patch_.device_[start]));
  } else {
    std::cout << "Error, invalid function pointer" << std::endl;
  }
}

/*
 * @name    SiftFilter
 * @fn  void SiftFilter(const cv::cuda::GpuMat& filtered_image,
                     const GPUSSift::KernelParameter& parameter,
                     float* histogram)
 * @param[in] filtered_image    Patch filtered with gaussian kernel where to
 *                              compute sift
 * @param[in] parameter         SSift extraction parameters
 * @param[in] stream            On which stream to launch the kernel
 * @param[out] histogram        Computed histogram
 * @ingroup utils
 */
void SiftFilter(const cv::cuda::GpuMat &filtered_image,
                const GPUSSift::KernelParameter &parameter,
                const cv::cuda::Stream &stream,
                float *histogram) {
  // Compute #block + #thread per block
  unsigned int w = static_cast<uint>((parameter.radius * 2) + 1);
  dim3 thd(16, 16);
  dim3 bck((w + 15) / 16, (w + 15) / 16);
  // shared mem
  int mem = parameter.hist_length * sizeof(float);
  // Run
  const float* in = reinterpret_cast<const float*>(filtered_image.data);
  cudaStream_t cs = cv::cuda::StreamAccessor::getStream(stream);
  device::sample_histogram<<<bck, thd, mem, cs>>>(in,
          filtered_image.step1(),
          filtered_image.cols,
          filtered_image.rows,
          parameter,
          histogram);
}

/*
 * @name    SiftFilter
 * @fn  void SiftFilter(const cv::cuda::GpuMat* filtered_image,
                       const GPUSSift::KernelParameter* parameter,
                       const size_t n_batch,
                       const size_t batch_size,
                       const cv::cuda::Stream& stream,
                       float *histogram)
 * @param[in] filtered_image    Array of filtered patch with gaussian kernel
 *                              where to compute sift
 * @param[in] parameter     Array of SSift extraction parameters
 * @param[in] n_batch       Current batch index
 * @param[in] batch_size    Size of the batch
 * @param[in] stream        On which stream to launch the kernel
 * @param[out] histogram    Computed histogram
 * @ingroup utils
 */
void SiftFilterBatched(const cv::cuda::GpuMat* filtered_image,
                       const GPUSSift::KernelParameter* parameter,
                       const size_t n_batch,
                       const size_t batch_size,
                       const cv::cuda::Stream& stream,
                       float *histogram) {
  auto* man = SSIFT::DefaultRessourceManager::Instance().Get();
  // Loop over batch size
  man->f_image_.start_ = n_batch == 0 ? 0 : man->f_image_.start_;
  man->kernel_parameter_.start_ = n_batch == 0 ? 0 : man->kernel_parameter_.start_;
  man->histogram_.start_ = n_batch == 0 ? 0 : man->histogram_.start_;
  size_t start = man->f_image_.start_;
  for (size_t b = 0; b < batch_size; ++b) {
    size_t i = start + b;
    man->f_image_.host_[i] = reinterpret_cast<float*>(filtered_image[b].data);
    man->kernel_parameter_.host_[i] = parameter[b];
    man->histogram_.host_[i] = &(histogram[b * parameter->sift_dim]);
  }
  // Copy pointer to device memory
  man->UploadSiftFilter(batch_size, stream);
  // Compute #block + #thread per block
  unsigned int w = static_cast<uint>((parameter->radius * 2) + 1);
  dim3 thd(16, 16);
  dim3 bck((w + 15) / 16, (w + 15) / 16);
  // shared mem
  int mem = parameter->hist_length * sizeof(float);
  // Run
  const SSIFT::sift_t f = SSIFT::sift_filters[batch_size];
  if (f) {
    const float** in = (const float**)&(man->f_image_.device_[start]);
    const auto* kernel_p = &(man->kernel_parameter_.device_[start]);
    float** out = &(man->histogram_.device_[start]);
    cudaStream_t cs = cv::cuda::StreamAccessor::getStream(stream);
    f<<<bck, thd, mem, cs>>>(in,
            filtered_image->step1(),
            filtered_image->cols,
            filtered_image->rows,
            kernel_p,
            out);
  } else {
    std::cout << "Error, invalid function pointer" << std::endl;
  }
}

/*
 * @name    FinalizeHistogram
 * @fn  void FinalizeHistogram(const GPUSSift::KernelParameter& parameter,
                               const cv::cuda::Stream& stream,
                               float* histogram)
 * @brief Finalize histogram, threshold + root sift
 * @param[in] parameter     Sift parameters
 * @param[in] stream        Stream where to launch kernel
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
void FinalizeHistogram(const GPUSSift::KernelParameter& parameter,
                       const cv::cuda::Stream& stream,
                       float* histogram) {
  // Sanity check
  if (parameter.sift_dim != 128) {
    LTS5::CUDA::error("Unsported sift dimension", __FILE__, __LINE__,"");
  } else {
    // shared mem
    int nthread = parameter.sift_dim / 2;
    int nblock = (parameter.sift_dim + ((2 * nthread) - 1)) / (2 * nthread);
    int mem = (nthread + parameter.sift_dim) * sizeof(float);
    assert(nblock == 1);
    cudaStream_t cs = cv::cuda::StreamAccessor::getStream(stream);
    // Run kernel
    device::finalize_histogram<<<nblock, nthread, mem, cs>>>(parameter,
            histogram);
  }
}

/*
 * @name    FinalizeHistogram
 * @fn  void FinalizeHistogram(const GPUSSift::KernelParameter* parameter,
                              const size_t n_batch,
                              const size_t batch_size,
                              const cv::cuda::Stream& stream,
                              float* histogram)
 * @brief Finalize histogram, threshold + root sift
 * @param[in] parameter     Array of SSift extraction parameters
 * @param[in] n_batch       Current batch index
 * @param[in] batch_size    Size of the batch
 * @param[in] stream        Stream where to launch kernel
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
void FinalizeHistogramBatched(const GPUSSift::KernelParameter* parameter,
                              const size_t n_batch,
                              const size_t batch_size,
                              const cv::cuda::Stream& stream,
                              float* histogram) {
  auto* man = SSIFT::DefaultRessourceManager::Instance().Get();
  // Loop over batch size,
  man->kernel_parameter_.start_ = n_batch == 0 ? 0 : man->kernel_parameter_.start_;
  man->histogram_.start_ = n_batch == 0 ? 0 : man->histogram_.start_;
  size_t start = man->kernel_parameter_.start_;
  // Copy pointer to device memory
  // Already on device at previous iteration -> do not need to update, only
  // update start position
  man->kernel_parameter_.start_ += batch_size;
  man->histogram_.start_ += batch_size;
  // shared mem
  int thd = parameter->sift_dim / 2;
  int bck = (parameter->sift_dim + ((2 * thd) - 1)) / (2 * thd);
  int mem = (thd + parameter->sift_dim) * sizeof(float);
  assert(bck == 1);
  // Run kernel
  const SSIFT::fin_sift_t f = SSIFT::fin_sift[batch_size];
  if (f) {
    const auto* kernel_p = &(man->kernel_parameter_.device_[start]);
    float** out = &(man->histogram_.device_[start]);
    cudaStream_t cs = cv::cuda::StreamAccessor::getStream(stream);
    f<<<bck, thd, mem, cs>>>(kernel_p, out);
  } else {
    std::cout << "Error, invalid function pointer" << std::endl;
  }
}

/*
 * @name    SiftRoot
 * @fn  void SiftRoot(const GPUSSift::KernelParameter& parameter,
                               const cv::cuda::Stream& stream,
                               float* histogram)
 * @brief Apply sift root normalization
 * @param[in] parameter     Sift parameters
 * @param[in] stream        Stream where to launch kernel
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
void SiftRoot(const GPUSSift::KernelParameter& parameter,
              const cv::cuda::Stream& stream,
              float* histogram) {
  // Sanity check
  if (parameter.sift_dim != 128) {
    LTS5::CUDA::error("Unsported sift dimension", __FILE__, __LINE__,"");
  } else {
    // shared mem
    int thd = parameter.sift_dim / 2;
    int bck = (parameter.sift_dim + ((2 * thd) - 1)) / (2 * thd);
    int mem = (thd + parameter.sift_dim) * sizeof(float);
    assert(bck == 1);
    cudaStream_t cs = cv::cuda::StreamAccessor::getStream(stream);
    // Run kernel
    device::root_sift<<<bck, thd, mem, cs>>>(parameter, histogram);
  }
}

/*
 * @name    SiftRootBatched
 * @fn  void SiftRootBatched(const GPUSSift::KernelParameter* parameter,
                             const size_t n_batch,
                             const size_t batch_size,
                             const cv::cuda::Stream& stream,
                             float* histogram)
 * @brief Apply sift root normalization
 * @param[in] parameter     Sift parameters
 * @param[in] n_batch       Current batch index
 * @param[in] batch_size    Size of the batch
 * @param[in] stream        Stream where to launch kernel
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
void SiftRootBatched(const GPUSSift::KernelParameter* parameter,
                     const size_t n_batch,
                     const size_t batch_size,
                     const cv::cuda::Stream& stream,
                     float* histogram) {
  auto* man = SSIFT::DefaultRessourceManager::Instance().Get();
  // Loop over batch size,
  man->kernel_parameter_.start_ = n_batch == 0 ? 0 : man->kernel_parameter_.start_;
  man->histogram_.start_ = n_batch == 0 ? 0 : man->histogram_.start_;
  size_t start = man->kernel_parameter_.start_;
  // Copy pointer to device memory
  // Already on device at previous iteration -> do not need to update, only
  // update start position
  man->kernel_parameter_.start_ += batch_size;
  man->histogram_.start_ += batch_size;
  // shared mem
  int thd = parameter->sift_dim / 2;
  int bck = (parameter->sift_dim + ((2 * thd) - 1)) / (2 * thd);
  int mem = (thd + parameter->sift_dim) * sizeof(float);
  assert(bck == 1);
  // Run kernel
  const SSIFT::r_sift_t f = SSIFT::root_sift[batch_size];
  if (f) {
    const auto* kernel_p = &(man->kernel_parameter_.device_[start]);
    float** out = &(man->histogram_.device_[start]);
    cudaStream_t cs = cv::cuda::StreamAccessor::getStream(stream);
    f<<<bck, thd, mem, cs>>>(kernel_p, out);
  } else {
    std::cout << "Error, invalid function pointer" << std::endl;
  }
}

/**
 *  @namespace  device
 *  @brief      CUDA Device code
 */
namespace device {

/*
 * @name    set_value
 * @fn  void set_value(const float value, const int N, float* array)
 * @brief   Initialize an array to a given value
 * @param[in] value         Value to set
 * @param[in] N             Array dimension
 * @param[in,out] array     Array to initialize
 */
__global__
void set_value(const float value, const int N, float* array) {
  unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
  if (x < N) {
    array[x] = value;
  }
}

/*
 * @name    set_value_batched
 * @fn  void set_value_batched(const float value, const int N, float** array)
 * @brief   Initialize an array of array to a given value
 * @param[in] value         Value to set
 * @param[in] N             Array dimension
 * @param[in,out] array     Arrays to initialize
 */
template<size_t B>
__global__
void set_value_batched(const float value, const int N, float** array) {
  unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
  if (x < N) {
    #pragma unroll
    for (size_t b = 0; b < B; ++b) {
      array[b][x] = value;
    }
  }
}

/*
 * @name    color_to_gray
 * @fn  void color_to_gray(const float* img, const size_t pitch,
                            cv::gpu::PtrStepf gray);
 * @brief   Convert color image into float grayscale image
 * @param[in] img       Image color pointer
 * @param[in] pitch     Image pitch
 * @param[in] dpitch    Destination image pitch
 * @param[out] gray     Converted image
 */
__global__
void color_to_gray(const uchar* img,
                   const size_t pitch,
                   const size_t dpitch,
                   float* gray) {
  // Taken from opencv sources
#define CV_DESCALE(x, n) (((x) + (1 << ((n)-1))) >> (n))
  // Compute position
  unsigned int c = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int r = blockIdx.y * blockDim.y + threadIdx.y;
  // Read input
  const uchar* base = &img[r * pitch + 3*c];
  uint blue = *base++;
  uint green = *base++;
  uint red = *base;
  // Convert
  uint p = (1868 * blue) + (9617 * green) + (4899 * red);
  gray[r * dpitch + c] = float(CV_DESCALE(p, 14));
  // Clean up macro
#undef CV_DESCALE
}

/*
 * @name    color_to_gray_batched
 * @fn  void color_to_gray_batched(const uchar** img,
                           const size_t pitch,
                           const size_t dpitch,
                           float** gray);
 * @brief   Convert a batch of color images into float images. Images must
 *          have all the same dimension
 * @tparam D Batch dimension
 * @param[in] img       Image color pointer
 * @param[in] pitch     Image pitch
 * @param[in] dpitch    Destination image pitch
 * @param[out] gray     Converted image
 */
template<size_t D>
__global__
void color_to_gray_batched(const uchar** img,
                           const size_t pitch,
                           const size_t dpitch,
                           float** gray) {
  // Taken from opencv sources
#define CV_DESCALE(x, n) (((x) + (1 << ((n)-1))) >> (n))
  // Compute position
  unsigned int c = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int r = blockIdx.y * blockDim.y + threadIdx.y;

#pragma unroll
  for (size_t b = 0; b < D; ++b) {
    // Read input
    const uchar* base = &img[b][r * pitch + 3*c];
    uint blue = *base++;
    uint green = *base++;
    uint red = *base;
    // Convert
    uint p = (1868 * blue) + (9617 * green) + (4899 * red);
    gray[b][r * dpitch + c] = float(CV_DESCALE(p, 14));
  }
  // Clean up macro
#undef CV_DESCALE
}

/*
 * @name    gray_to_float
 * @fn  void gray_to_float(const uchar* img,
                           const size_t pitch,
                           const size_t dpitch,
                           float* gray);
 * @brief   Convert grayscale image into float image
 * @param[in] img       Image color pointer
 * @param[in] pitch     Image pitch
 * @param[in] dpitch    Destination image pitch
 * @param[out] gray     Converted image
 */
__global__
void gray_to_float(const uchar* img,
                   const size_t pitch,
                   const size_t dpitch,
                   float* gray) {
  // Compute position
  unsigned int c = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int r = blockIdx.y * blockDim.y + threadIdx.y;
  // Convert
  gray[r * dpitch + c] = float(img[r * pitch + c]);
}

/*
 * @name    gray_to_float_batched
 * @fn  void gray_to_float_batched(const uchar** img,
                           const size_t pitch,
                           const size_t dpitch,
                           float** gray);
 * @brief   Convert a batch of grayscale images into float images. Images must
 *          have all the same dimension
 * @tparam D Batch dimension
 * @param[in] img       Image grayscale pointer
 * @param[in] pitch     Image pitch
 * @param[in] dpitch    Destination image pitch
 * @param[out] gray     Converted image
 */
template<size_t D>
__global__
void gray_to_float_batched(const uchar** img,
                           const size_t pitch,
                           const size_t dpitch,
                           float** gray) {
  // Compute position
  unsigned int c = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int r = blockIdx.y * blockDim.y + threadIdx.y;
  // Do work
  #pragma unroll
  for (size_t b = 0; b < D; ++b) {
    // Convert
    gray[b][r * dpitch + c] = float(img[b][r * pitch + c]);
  }
}


/**
 * @name    sample_histogram
 * @fn  void sample_histogram(const float* image,
                              const size_t pitch,
                              const int width,
                              const int height,
                              const GPUSSift::KernelParameter parameters,
                              float* histogram)
 * @param[in] image         Patch filtered with gaussian kernel where to compute
 *                          sift
 * @param[in] pitch         Image pitch
 * @param[in] width         Image width
 * @param[in] height        Image height
 * @param[in] parameter     SSift extraction parameters
 * @param[out] histogram    Computed histogram
 * @ingroup utils
 */
__global__
void sample_histogram(const float* image,
                      const size_t pitch,
                      const int width,
                      const int height,
                      const GPUSSift::KernelParameter parameters,
                      float* histogram) {
  // Local histogram + init
  extern __shared__ float shist[];
  int step = (parameters.hist_length + (blockDim.x * blockDim.y) - 1) /
            (blockDim.x * blockDim.y);
  int tidx = threadIdx.y * blockDim.x + threadIdx.x;
  for (int i = step * tidx; i < step * (tidx+1); ++i) {
    if (i < parameters.hist_length) {
      shist[i] = 0.f;
    }
  }
  // Wait init is complete
  __syncthreads();

  // Do something if in range
  volatile const int w = (parameters.radius * 2) + 1;
  volatile const int x = blockIdx.x * blockDim.x + threadIdx.x;
  volatile const int y = blockIdx.y * blockDim.y + threadIdx.y;
  // Inside sampling grid ?
  if (x < w && y < w) {
    // Define index by row/col
    int i = -parameters.radius + y;
    int j = -parameters.radius + x;
    int r = parameters.patch_center_y + i;
    int c = parameters.patch_center_x + j;
    if (r > 0 && r < (height - 1) && c > 0 && c < (width - 1)) {
      float c_rot = static_cast<float>(j) / parameters.hist_width;
      float r_rot = static_cast<float>(i) / parameters.hist_width;
      float rbin = r_rot + parameters.sift_descr_width / 2 - 0.5f;
      float cbin = c_rot + parameters.sift_descr_width / 2 - 0.5f;
      if (rbin > -1 && rbin < parameters.sift_descr_width && cbin > -1 &&
          cbin < parameters.sift_descr_width) {
        // Compute "Gradient"
        // --------------------------------------------------------------------
        const float dx = image[r * pitch + c + 1] -
                         image[r * pitch + c - 1];
        const float dy = image[(r - 1) * pitch + c] -
                         image[(r + 1) * pitch + c];
        // Atan2, taken from opencv sources
        float ax = std::abs(dx);
        float ay = std::abs(dy);
        float ang, c1, c2;
        if (ax >= ay) {
          c1 = ay/(ax + (float)DBL_EPSILON);
          c2 = c1*c1;
          ang = (((d_atan2_p7*c2 + d_atan2_p5)*c2 + d_atan2_p3)*c2 + d_atan2_p1)*c1;
        } else {
          c1 = ax/(ay + (float)DBL_EPSILON);
          c2 = c1*c1;
          ang = 90.f - (((d_atan2_p7*c2 + d_atan2_p5)*c2 + d_atan2_p3)*c2 + d_atan2_p1)*c1;
        }
        if( dx < 0 )
          ang = 180.f - ang;
        if( dy < 0 )
          ang = 360.f - ang;

        // Magnitude
        float mag = sqrtf(dx * dx + dy * dy);
        // Compute Exponent
        float exp = (c_rot * c_rot + r_rot * r_rot) * parameters.exp_scale;
        exp = expf(exp);

        // Build histo gram
        // --------------------------------------------------------------------
        float obin = ang * parameters.bin_per_rad;
        mag = mag * exp;
        int r0 = static_cast<int>(floorf(rbin));
        int c0 = static_cast<int>(floorf(cbin));
        int o0 = static_cast<int>(floorf(obin));
        rbin -= r0;
        cbin -= c0;
        obin -= o0;
        if (o0 < 0) {
          o0 += parameters.sift_descr_hist_bins;
        }
        if (o0 >= parameters.sift_descr_hist_bins) {
          o0 -= parameters.sift_descr_hist_bins;
        }

        // Histogram update using tri-linear interpolation
        float v_r1 = mag * rbin, v_r0 = mag - v_r1;
        float v_rc11 = v_r1 * cbin, v_rc10 = v_r1 - v_rc11;
        float v_rc01 = v_r0 * cbin, v_rc00 = v_r0 - v_rc01;
        float v_rco111 = v_rc11 * obin, v_rco110 = v_rc11 - v_rco111;
        float v_rco101 = v_rc10 * obin, v_rco100 = v_rc10 - v_rco101;
        float v_rco011 = v_rc01 * obin, v_rco010 = v_rc01 - v_rco011;
        float v_rco001 = v_rc00 * obin, v_rco000 = v_rc00 - v_rco001;
        // Compute hist bin's index
        int idx = ((r0 + 1) * (parameters.sift_descr_width + 2) + c0 + 1) *
                  (parameters.sift_descr_hist_bins + 2) + o0;
        // Accumulate value
        atomicAdd(&(shist[idx]), v_rco000);
        atomicAdd(&(shist[idx+1]), v_rco001);
        atomicAdd(&(shist[idx + (parameters.sift_descr_hist_bins + 2)]), v_rco010);
        atomicAdd(&(shist[idx + (parameters.sift_descr_hist_bins + 3)]), v_rco011);
        atomicAdd(&(shist[idx + (parameters.sift_descr_width + 2) *
                             (parameters.sift_descr_hist_bins + 2)]),
                  v_rco100);
        atomicAdd(&(shist[idx + (parameters.sift_descr_width + 2) *
                             (parameters.sift_descr_hist_bins + 2) + 1]), v_rco101);
        atomicAdd(&(shist[idx + (parameters.sift_descr_width + 3) *
                             (parameters.sift_descr_hist_bins + 2)]), v_rco110);
        atomicAdd(&(shist[idx + (parameters.sift_descr_width + 3) *
                             (parameters.sift_descr_hist_bins + 2) + 1]), v_rco111);
      }
    }
  }
  __syncthreads();
  // Finalize histogram, since the orientation histograms are circular
  if (threadIdx.x < parameters.sift_descr_width &&
      threadIdx.y < parameters.sift_descr_width) {
    // Compute index
    int idx = ((threadIdx.x + 1) * (parameters.sift_descr_width + 2) +
               (threadIdx.y + 1)) * (parameters.sift_descr_hist_bins + 2);
    atomicAdd(&(shist[idx]),
              shist[idx + parameters.sift_descr_hist_bins]);
    atomicAdd(&(shist[idx + 1]),
              shist[idx + parameters.sift_descr_hist_bins + 1]);
    // Copy to dest
    for (int k = 0; k < parameters.sift_descr_hist_bins; k++) {
      atomicAdd(&(histogram[(threadIdx.x * parameters.sift_descr_width + threadIdx.y) *
                        parameters.sift_descr_hist_bins + k]),
                shist[idx + k]);
    }
  }
}

/*
 * @name    sample_histogram
 * @fn  void sample_histogram(const float* image,
                              const size_t pitch,
                              const int width,
                              const int height,
                              const GPUSSift::KernelParameter parameters,
                              float* histogram)
 * @param[in] image         Patch filtered with gaussian kernel where to compute
 *                          sift
 * @param[in] pitch         Image pitch
 * @param[in] width         Image width
 * @param[in] height        Image height
 * @param[in] parameter     SSift extraction parameters
 * @param[out] histogram    Computed histogram
 * @ingroup utils
 */
template<size_t D>
__global__
void sample_histogram_batched(const float** image,
                              const size_t pitch,
                              const int width,
                              const int height,
                              const GPUSSift::KernelParameter* parameters,
                              float** histogram) {
  // Local histogram + init
  extern __shared__ float shist[];

  for (size_t b = 0; b < D; ++b) {
    GPUSSift::KernelParameter param = parameters[b];
    int step = (param.hist_length + (blockDim.x * blockDim.y) - 1) /
               (blockDim.x * blockDim.y);
    int tidx = threadIdx.y * blockDim.x + threadIdx.x;
    for (int i = step * tidx; i < step * (tidx + 1); ++i) {
      if (i < param.hist_length) {
        shist[i] = 0.f;
      }
    }
    // Wait init is complete
    __syncthreads();

    // Do something if in range
    volatile const int w = (param.radius * 2) + 1;
    volatile const int x = blockIdx.x * blockDim.x + threadIdx.x;
    volatile const int y = blockIdx.y * blockDim.y + threadIdx.y;
    // Inside sampling grid ?
    if (x < w && y < w) {
      // Define index by row/col
      int i = -param.radius + y;
      int j = -param.radius + x;
      int r = param.patch_center_y + i;
      int c = param.patch_center_x + j;
      if (r > 0 && r < (height - 1) && c > 0 && c < (width - 1)) {
        float c_rot = static_cast<float>(j) / param.hist_width;
        float r_rot = static_cast<float>(i) / param.hist_width;
        float rbin = r_rot + param.sift_descr_width / 2 - 0.5f;
        float cbin = c_rot + param.sift_descr_width / 2 - 0.5f;
        if (rbin > -1 && rbin < param.sift_descr_width && cbin > -1 &&
            cbin < param.sift_descr_width) {
          // Compute "Gradient"
          // --------------------------------------------------------------------
          const float dx = image[b][r * pitch + c + 1] -
                           image[b][r * pitch + c - 1];
          const float dy = image[b][(r - 1) * pitch + c] -
                           image[b][(r + 1) * pitch + c];
          // Atan2, taken from opencv sources
          float ax = std::abs(dx);
          float ay = std::abs(dy);
          float ang, c1, c2;
          if (ax >= ay) {
            c1 = ay / (ax + (float) DBL_EPSILON);
            c2 = c1 * c1;
            ang = (((d_atan2_p7 * c2 + d_atan2_p5) * c2 + d_atan2_p3) * c2 +
                   d_atan2_p1) * c1;
          } else {
            c1 = ax / (ay + (float) DBL_EPSILON);
            c2 = c1 * c1;
            ang = 90.f -
                  (((d_atan2_p7 * c2 + d_atan2_p5) * c2 + d_atan2_p3) * c2 +
                   d_atan2_p1) * c1;
          }
          if (dx < 0)
            ang = 180.f - ang;
          if (dy < 0)
            ang = 360.f - ang;

          // Magnitude
          float mag = sqrtf(dx * dx + dy * dy);
          // Compute Exponent
          float exp = (c_rot * c_rot + r_rot * r_rot) * param.exp_scale;
          exp = expf(exp);

          // Build histo gram
          // --------------------------------------------------------------------
          float obin = ang * param.bin_per_rad;
          mag = mag * exp;
          int r0 = static_cast<int>(floorf(rbin));
          int c0 = static_cast<int>(floorf(cbin));
          int o0 = static_cast<int>(floorf(obin));
          rbin -= r0;
          cbin -= c0;
          obin -= o0;
          if (o0 < 0) {
            o0 += param.sift_descr_hist_bins;
          }
          if (o0 >= param.sift_descr_hist_bins) {
            o0 -= param.sift_descr_hist_bins;
          }

          // Histogram update using tri-linear interpolation
          float v_r1 = mag * rbin, v_r0 = mag - v_r1;
          float v_rc11 = v_r1 * cbin, v_rc10 = v_r1 - v_rc11;
          float v_rc01 = v_r0 * cbin, v_rc00 = v_r0 - v_rc01;
          float v_rco111 = v_rc11 * obin, v_rco110 = v_rc11 - v_rco111;
          float v_rco101 = v_rc10 * obin, v_rco100 = v_rc10 - v_rco101;
          float v_rco011 = v_rc01 * obin, v_rco010 = v_rc01 - v_rco011;
          float v_rco001 = v_rc00 * obin, v_rco000 = v_rc00 - v_rco001;
          // Compute hist bin's index
          int idx = ((r0 + 1) * (param.sift_descr_width + 2) + c0 + 1) *
                    (param.sift_descr_hist_bins + 2) + o0;
          // Accumulate value
          atomicAdd(&(shist[idx]), v_rco000);
          atomicAdd(&(shist[idx + 1]), v_rco001);
          atomicAdd(&(shist[idx + (param.sift_descr_hist_bins + 2)]),
                    v_rco010);
          atomicAdd(&(shist[idx + (param.sift_descr_hist_bins + 3)]),
                    v_rco011);
          atomicAdd(&(shist[idx + (param.sift_descr_width + 2) *
                                  (param.sift_descr_hist_bins + 2)]),
                    v_rco100);
          atomicAdd(&(shist[idx + (param.sift_descr_width + 2) *
                                  (param.sift_descr_hist_bins + 2) + 1]),
                    v_rco101);
          atomicAdd(&(shist[idx + (param.sift_descr_width + 3) *
                                  (param.sift_descr_hist_bins + 2)]),
                    v_rco110);
          atomicAdd(&(shist[idx + (param.sift_descr_width + 3) *
                                  (param.sift_descr_hist_bins + 2) + 1]),
                    v_rco111);
        }
      }
    }
    __syncthreads();
    // Finalize histogram, since the orientation histograms are circular
    if (threadIdx.x < param.sift_descr_width &&
        threadIdx.y < param.sift_descr_width) {
      // Compute index
      int idx = ((threadIdx.x + 1) * (param.sift_descr_width + 2) +
                 (threadIdx.y + 1)) * (param.sift_descr_hist_bins + 2);
      atomicAdd(&(shist[idx]),
                shist[idx + param.sift_descr_hist_bins]);
      atomicAdd(&(shist[idx + 1]),
                shist[idx + param.sift_descr_hist_bins + 1]);
      // Copy to dest
      float* d = histogram[b];
      for (int k = 0; k < param.sift_descr_hist_bins; k++) {
        atomicAdd(&(d[(threadIdx.x * param.sift_descr_width + threadIdx.y) *
                      param.sift_descr_hist_bins + k]),
                  shist[idx + k]);
      }
    }
    __syncthreads();
  }
}

/*
 * @name    finalize_histogram
 * @fn  void finalize_histogram(const GPUSSift::KernelParameter parameters,
                        float* histogram)
 * @brief
 * @param[in] parameters        Sift parameters
 * @param[in,out] histogram     Updated histogram
 * @ingroup utils
 */
__global__
void finalize_histogram(const GPUSSift::KernelParameter parameters,
                        float* histogram) {
  // Reduction, compute squared norm
  // --------------------------------------------------------------------------
  extern __shared__ float buffer[];
  float* shist = buffer;
  float* sdata = &buffer[parameters.sift_dim];

  // perform first level of reduction,
  // reading from global memory, writing to shared memory
  unsigned int tid = threadIdx.x;
  unsigned int i = blockIdx.x*(blockDim.x * 2) + threadIdx.x;

  shist[i] = (i < parameters.sift_dim) ? histogram[i] : 0.f;
  float mySum = (i < parameters.sift_dim) ? shist[i] * shist[i] : 0.f;
  if (i + blockDim.x < parameters.sift_dim) {
    shist[i + blockDim.x] = histogram[i + blockDim.x];
    mySum += (shist[i + blockDim.x] * shist[i + blockDim.x]);
  }
  sdata[tid] = mySum;
  __syncthreads();
  // do reduction in shared mem
  for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
    if (tid < s) {
      sdata[tid] = mySum = mySum + sdata[tid + s];
    }
    __syncthreads();
  }
  // Got squared norm, compute thresh + reduction again (update norm)
  // --------------------------------------------------------------------------
  float thr = sqrtf(sdata[0]) * parameters.sift_descr_mag_thr;
  // update histo + reduction
  float v = (i < parameters.sift_dim) ? fminf(shist[i], thr) : 0.f;
  shist[i] = v;
  mySum =  v * v;
  if (i + blockDim.x < parameters.sift_dim) {
    v = fminf(shist[i + blockDim.x], thr);
    shist[i + blockDim.x] = v;
    mySum += (v * v);
  }
  sdata[tid] = mySum;
  __syncthreads();
  // do reduction in shared mem
  for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
    if (tid < s) {
      sdata[tid] = mySum = mySum + sdata[tid + s];
    }
    __syncthreads();
  }
  // Got squared norm, compute scaling factor + saturate cast
  // --------------------------------------------------------------------------
  mySum = parameters.sift_int_descr_fctr / fmaxf(sqrtf(sdata[0]), FLT_EPSILON);
  if (i < parameters.sift_dim) {
    uint res = 0;
    asm("cvt.rni.sat.u8.f32 %0, %1;" : "=r"(res) : "f"(shist[i] * mySum));
    histogram[i] = res;
  }
  if (i + blockDim.x < parameters.sift_dim) {
    uint res = 0;
    asm("cvt.rni.sat.u8.f32 %0, %1;" : "=r"(res) : "f"(shist[i + blockDim.x] * mySum));
    histogram[i + blockDim.x] = res;
  }
}

/*
 * @name    finalize_histogram
 * @fn  void finalize_histogram(const GPUSSift::KernelParameter* parameters,
                                float** histogram)
 * @brief
 * @param[in] parameters        Sift parameters
 * @param[in,out] histogram     Updated histogram
 * @ingroup utils
 */
template<size_t D>
__global__
void finalize_histogram_batched(const GPUSSift::KernelParameter* parameters,
                                float** histogram) {
  // Reduction, compute squared norm
  // --------------------------------------------------------------------------
  extern __shared__ float buffer[];
  float* shist = buffer;
  float* sdata = &buffer[parameters->sift_dim];

  for (size_t b = 0;  b < D; ++b) {
    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*(blockDim.x * 2) + threadIdx.x;
    GPUSSift::KernelParameter param = parameters[b];

    shist[i] = (i < param.sift_dim) ? histogram[b][i] : 0.f;
    float mySum = (i < param.sift_dim) ? shist[i] * shist[i] : 0.f;
    if (i + blockDim.x < param.sift_dim) {
      shist[i + blockDim.x] = histogram[b][i + blockDim.x];
      mySum += (shist[i + blockDim.x] * shist[i + blockDim.x]);
    }
    sdata[tid] = mySum;
    __syncthreads();
    // do reduction in shared mem
    for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
      if (tid < s) {
        sdata[tid] = mySum = mySum + sdata[tid + s];
      }
      __syncthreads();
    }
    // Got squared norm, compute thresh + reduction again (update norm)
    // --------------------------------------------------------------------------
    float thr = sqrtf(sdata[0]) * param.sift_descr_mag_thr;
    // update histo + reduction
    float v = (i < param.sift_dim) ? fminf(shist[i], thr) : 0.f;
    shist[i] = v;
    mySum =  v * v;
    if (i + blockDim.x < param.sift_dim) {
      v = fminf(shist[i + blockDim.x], thr);
      shist[i + blockDim.x] = v;
      mySum += (v * v);
    }
    sdata[tid] = mySum;
    __syncthreads();
    // do reduction in shared mem
    for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
      if (tid < s) {
        sdata[tid] = mySum = mySum + sdata[tid + s];
      }
      __syncthreads();
    }
    // Got squared norm, compute scaling factor + saturate cast
    // --------------------------------------------------------------------------
    mySum = param.sift_int_descr_fctr / fmaxf(sqrtf(sdata[0]), FLT_EPSILON);
    if (i < param.sift_dim) {
      uint res = 0;
      asm("cvt.rni.sat.u8.f32 %0, %1;" : "=r"(res) : "f"(shist[i] * mySum));
      histogram[b][i] = res;
    }
    if (i + blockDim.x < param.sift_dim) {
      uint res = 0;
      asm("cvt.rni.sat.u8.f32 %0, %1;" : "=r"(res) : "f"(shist[i + blockDim.x] * mySum));
      histogram[b][i + blockDim.x] = res;
    }
    __syncthreads();
  }
}

/*
 * @name    root_sift
 * @fn  void root_sift(const GPUSSift::KernelParameter parameters,
               float* histogram)
 * @brief   RootSift
    R. Arandjelovic, A. Zisserman, Three things everyone should know
    to improve object retrieval, CVPR'12
 * @param[in] parameters    Sift parameters
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
__global__
void root_sift(const GPUSSift::KernelParameter parameters,
               float* histogram) {
  // Reduction, compute squared norm
  // --------------------------------------------------------------------------
  extern __shared__ float buffer[];
  float* shist = buffer;
  float* sdata = &buffer[parameters.sift_dim];

  // perform first level of reduction,
  // reading from global memory, writing to shared memory
  unsigned int tid = threadIdx.x;
  unsigned int i = blockIdx.x*(blockDim.x * 2) + threadIdx.x;

  shist[i] = (i < parameters.sift_dim) ? histogram[i] : 0.f;
  float mySum = (i < parameters.sift_dim) ? shist[i] : 0.f;
  if (i + blockDim.x < parameters.sift_dim) {
    shist[i + blockDim.x] = histogram[i + blockDim.x];
    mySum += shist[i + blockDim.x];
  }
  sdata[tid] = mySum;
  __syncthreads();
  // do reduction in shared mem
  for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
    if (tid < s) {
      sdata[tid] = mySum = mySum + sdata[tid + s];
    }
    __syncthreads();
  }
  // Copy to dest
  float s = 1.f / fmaxf(sdata[0], FLT_EPSILON);
  if (i < parameters.sift_dim) {
    float v = sqrtf(shist[i] * s);
    histogram[i] = v;
  }
  if (i + blockDim.x < parameters.sift_dim) {
    float v = sqrtf(shist[i + blockDim.x] * s);
    histogram[i + blockDim.x] = v;
  }
}

/*
 * @name    root_sift
 * @fn  void root_sift(const GPUSSift::KernelParameter* parameters,
                       float** histogram)
 * @brief   RootSift
    R. Arandjelovic, A. Zisserman, Three things everyone should know
    to improve object retrieval, CVPR'12
 * @param[in] parameters    Array of Sift parameters
 * @param[in,out] histogram Updated histogram
 * @ingroup utils
 */
template<size_t D>
__global__
void root_sift_batched(const GPUSSift::KernelParameter* parameters,
                       float** histogram) {
  // Reduction, compute squared norm
  // --------------------------------------------------------------------------
  extern __shared__ float buffer[];
  float* shist = buffer;
  float* sdata = &buffer[parameters->sift_dim];

  for (size_t b = 0; b < D; ++b) {
    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*(blockDim.x * 2) + threadIdx.x;
    GPUSSift::KernelParameter param = parameters[b];

    shist[i] = (i < param.sift_dim) ? histogram[b][i] : 0.f;
    float mySum = (i < param.sift_dim) ? shist[i] : 0.f;
    if (i + blockDim.x < param.sift_dim) {
      shist[i + blockDim.x] = histogram[b][i + blockDim.x];
      mySum += shist[i + blockDim.x];
    }
    sdata[tid] = mySum;
    __syncthreads();
    // do reduction in shared mem
    for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1) {
      if (tid < s) {
        sdata[tid] = mySum = mySum + sdata[tid + s];
      }
      __syncthreads();
    }
    // Copy to dest
    float s = 1.f / fmaxf(sdata[0], FLT_EPSILON);
    if (i < param.sift_dim) {
      float v = sqrtf(shist[i] * s);
      histogram[b][i] = v;
    }
    if (i + blockDim.x < param.sift_dim) {
      float v = sqrtf(shist[i + blockDim.x] * s);
      histogram[b][i + blockDim.x] = v;
    }
    __syncthreads();
  }
}


}  // namepsace device
} // namespace CUDA
} // namespace LTS5
