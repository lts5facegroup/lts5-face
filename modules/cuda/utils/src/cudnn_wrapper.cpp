/**
 *  @file   cudnn_wrapper.cpp
 *  @brief  cuDNN abstraction layer
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   22/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "opencv2/core/cuda_stream_accessor.hpp"

#include "lts5/cuda/utils/math/cudnn_wrapper.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/tensor_operation.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark cuDNN Context

/*
 * @name  Instance
 * @fn    static CudnnContext& Instance(void)
 * @brief Provide single instance of CudnnContext
 * @return    Cublas context
 */
CudnnContext& CudnnContext::Instance(void) {
  static CudnnContext ctx;
  return ctx;
}

/*
 * @name  ~CudnnContext
 * @fn    ~CudnnContext(void)
 * @brief Destructor
 */
CudnnContext::~CudnnContext(void) {
  // Release every handles
  for (auto& h : ctx_) {
    cudnnSafeCall(cudnnDestroy(h.second));
  }
}

/*
 * @name  CudnnContext
 * @fn    CudnnContext(void)
 * @brief Constructor
 */
CudnnContext::CudnnContext(void) {
}

/*
 * @name  Get
 * @fn    cudnnContext* Get(void) const
 * @brief Provide access to context state
 * @return    cublas raw context for this specific thread
 */
cudnnContext* CudnnContext::Get(void) {
  // check if thread has already a handle
  auto id = std::this_thread::get_id();
  cudnnContext* handle = nullptr;
  auto p = ctx_.find(id);
  if (p == ctx_.end()) {
    // Create new handle
    cudnnSafeCall(cudnnCreate(&handle));
    // Ensure single thread push to the map
    std::lock_guard<std::mutex> lock(this->mutex_);
    ctx_.insert({id, handle});
  } else {
    // Known handle
    handle = p->second;
  }
  return handle;
}

#pragma mark -
#pragma mark Tensor Algebra

/*
 * @name  Operation
 * @fn    static int Operation(const TensorOp& op,
                    const Tensor& A, const T alpha1,
                    const Tensor& B, const T alpha2,
                    const T beta,
                    Tensor* C)
 * @brief Perform a given operation on tensor :
          C = op(alpha1 * A, alpha2 * B) + beta * C
          Note tensor C must have proper dimension beforehand
 * @param op      Tensor operation to perform
 * @param A       Tensor A
 * @param alpha1  scaling coefficient
 * @param B       Tensor B
 * @param alpha2  scaling coefficient
 * @param beta    scaling coefficient
 * @param C       Tensor C holding result of a given \p
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TensorAlgebra<T>::Operation(const TensorOp<T>& op,
                               const Tensor& A, const T alpha1,
                               const Tensor& B, const T alpha2,
                               const T beta,
                               Tensor* C) {
  return TensorAlgebra<T>::Operation(op,
                                     A, alpha1,
                                     B, alpha2,
                                     beta,
                                     cv::cuda::Stream::Null(),
                                     C);
}


/*
 * @name  Operation
 * @fn    static int Operation(const TensorOp& op,
                    const Tensor& A, const T alpha1,
                    const Tensor& B, const T alpha2,
                    const T beta,
                    const cv::cuda::Stream& stream,
                    Tensor* C)
 * @brief Perform a given operation on tensor :
          C = op(alpha1 * A, alpha2 * B) + beta * C
          Note tensor C must have proper dimension beforehand
 * @param[in] op      Tensor operation to perform
 * @param[in] A       Tensor A
 * @param[in] alpha1  scaling coefficient
 * @param[in] B       Tensor B
 * @param[in] alpha2  scaling coefficient
 * @param[in] beta    scaling coefficient
 * @param[in] stream  Stream on which to run computation
 * @param[in,out] C   Tensor C holding result of a given \p
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TensorAlgebra<T>::Operation(const TensorOp<T>& op,
                                const Tensor& A, const T alpha1,
                                const Tensor& B, const T alpha2,
                                const T beta,
                                const cv::cuda::Stream& stream,
                                Tensor* C) {
  int err = 0;
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Get handle
  auto h = CudnnContext::Instance().Get();
  cudnnSafeCall(cudnnSetStream(h, s));
  // Perform op
  auto e = cudnnOpTensor(h,
                         op.op,
                         &alpha1, A.descriptor(), A.data(),
                         &alpha2, B.descriptor(), B.data(),
                         &beta, C->descriptor(), C->data());
  if (e != CUDNN_STATUS_SUCCESS) {
    cudnnSafeCall(e);
    err = -1;
  }
  return err;
}

/*
 * @name  Reduction
 * @fn    const Reduction(TensorRedOp<T>& red,
                          const TensorWorkspace<T>& workspace,
                          const Tensor& A, const T alpha,
                          const T beta,
                          Tensor* C
 * @brief Perform tensor reduction :
 *        C = alpha * red_op(A) + beta * C
 * @param[in] red         Reduction operation
 * @param[in] workspace   Workspace with proper dimension
 * @param[in] A           Tensor A
 * @param[in] alpha       scaling factor
 * @param[in] beta        scaling factor
 * @param[out] C          Resulting tensor, NOTE: Dimension must be correct
 *                        beforehand
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TensorAlgebra<T>::Reduction(const TensorRedOp<T>& red,
                                   const TensorWorkspace<T>& workspace,
                                   const Tensor& A, const T alpha,
                                   const T beta,
                                   Tensor* C) {
  return TensorAlgebra<T>::Reduction(red,
                                     workspace,
                                     A,
                                     alpha,
                                     beta,
                                     cv::cuda::Stream::Null(),
                                     C);
}

/*
 * @name  Reduction
 * @fn    const Reduction(TensorRedOp<T>& red,
                          const TensorWorkspace<T>& workspace,
                          const Tensor& A, const T alpha,
                          const T beta,
                          const cv::cuda::Stream& stream,
                          Tensor* C
 * @brief Perform tensor reduction :
 *        C = alpha * red_op(A) + beta * C
 * @param[in] red         Reduction operation
 * @param[in] workspace   Workspace with proper dimension
 * @param[in] A           Tensor A
 * @param[in] alpha       scaling factor
 * @param[in] beta        scaling factor
 * @param[in] stream      Stream on which to run computation
 * @param[out] C          Resulting tensor, NOTE: Dimension must be correct
 *                        beforehand
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TensorAlgebra<T>::Reduction(const TensorRedOp<T>& red,
                                const TensorWorkspace<T>& workspace,
                                const Tensor& A, const T alpha,
                                const T beta,
                                const cv::cuda::Stream& stream,
                                Tensor* C) {
  int err = 0;
  // Get handle
  auto* h = CudnnContext::Instance().Get();
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  cudnnSafeCall(cudnnSetStream(h, s));
  // Process
  auto e = cudnnReduceTensor(h,
                             red.red,
                             nullptr,
                             0,
                             workspace.buffer(),
                             workspace.size(),
                             &alpha,
                             A.descriptor(),
                             A.data(),
                             &beta,
                             C->descriptor(),
                             C->data());
  if (e != CUDNN_STATUS_SUCCESS) {
    err = -1;
    cudnnSafeCall(e);
  }
  return err;
}

/*
 * @name  Convolution
 * @fn    static int Convolution(const TensorConvOp<T>& conv,
                                 const TensorWorkspace<T>& workspace,
                                 const Tensor& input, const T alpha,
                                 const T beta,
                                 Tensor* output)
 * @brief Perform convolution
 * @param[in] conv        Convolution operationo
 * @param[in] workspace   Workspace
 * @param[in] input       Input tensor to filter
 * @param[in] alpha       scaling factor
 * @param[in] beta        scaling factor
 * @param[out] output     Tensor holding filtered data
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TensorAlgebra<T>::Convolution(const TensorConvOp<T>& conv,
                                  const TensorWorkspace<T>& workspace,
                                  const Tensor& input, const T alpha,
                                  const T beta,
                                  Tensor* output) {
  return TensorAlgebra<T>::Convolution(conv,
                                       workspace,
                                       input,
                                       alpha,
                                       beta,
                                       cv::cuda::Stream::Null(),
                                       output);
}

/*
 * @name  Convolution
 * @fn    static int Convolution(const TensorConvOp<T>& conv,
                                 const TensorWorkspace<T>& workspace,
                                 const Tensor& input, const T alpha,
                                 const T beta,
                                 const cv::cuda::Stream& stream,
                                 Tensor* output)
 * @brief Perform convolution
 * @param[in] conv        Convolution operationo
 * @param[in] workspace   Workspace
 * @param[in] input       Input tensor to filter
 * @param[in] alpha       scaling factor
 * @param[in] beta        scaling factor
 * @param[in] stream      Stream on which to launch computation
 * @param[out] output     Tensor holding filtered data
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TensorAlgebra<T>::Convolution(const TensorConvOp<T>& conv,
                                  const TensorWorkspace<T>& workspace,
                                  const Tensor& input, const T alpha,
                                  const T beta,
                                  const cv::cuda::Stream& stream,
                                  Tensor* output) {
  int err = 0;
  // Get handle
  auto* h = CudnnContext::Instance().Get();
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  cudnnSafeCall(cudnnSetStream(h, s));
  // Convolve
  auto e = cudnnConvolutionForward(h,
                                   &alpha,
                                   input.descriptor(),
                                   input.data(),
                                   conv.filter_desc,
                                   conv.filter->data(),
                                   conv.conv_desc,
                                   (cudnnConvolutionFwdAlgo_t)conv.algo,
                                   workspace.buffer(),
                                   workspace.size(),
                                   &beta,
                                   output->descriptor(),
                                   output->data());
  if (e != CUDNN_STATUS_SUCCESS) {
    cudnnSafeCall(e);
    err = -1;
  }
  return err;
}

#pragma mark -
#pragma mark Declaration

/** Tensor algebra - float */
template class TensorAlgebra<float>;

/** Tensor algebra - double */
template class TensorAlgebra<double>;

}  // namepsace CUDA
}  // namepsace LTS5
