set(SUBSYS_NAME cuda_face_tracker)
set(SUBSYS_DESC "LTS5 CUDA Face Tracker library")
# Define subsystem path
SET(SUBSYS_PATH cuda/face_tracker)
#Set internal/cuda library dependencies
set(SUBSYS_DEPS utils face_tracker face_detector classifier cuda_utils)

set(build TRUE)
LTS5_SUBSYS_OPTION(build "${SUBSYS_NAME}" "${SUBSYS_DESC}" ON)
#Add dependencies as well as external dependencies
SET(inc_deps)
LTS5_SUBSYS_DEPEND(build "${SUBSYS_NAME}" inc_deps DEPS ${SUBSYS_DEPS} EXT_DEPS opencv)
if (build)
  set(incs
      include/lts5/${SUBSYS_PATH}/const_tracker_assessment.hpp
      include/lts5/${SUBSYS_PATH}/mstream_sdm_tracker.hpp
      include/lts5/${SUBSYS_PATH}/sdm_tracker.hpp
      include/lts5/${SUBSYS_PATH}/svm_tracker_assessment.hpp
      )
  set(kernel_incs
      include/lts5/${SUBSYS_PATH}/device/sdm_kernel.h
      )
  set(srcs
      src/const_tracker_assessment.cpp
      src/mstream_sdm_tracker.cpp
      src/sdm_tracker.cpp
      src/svm_tracker_assessment.cpp
      )
  set(kernel_srcs
      src/sdm_kernel.cu
      )

  #LIBRARY MODULE
  set(LIB_NAME "lts5_${SUBSYS_NAME}")
  LTS5_ADD_CUDA_LIBRARY("${LIB_NAME}" "${SUBSYS_NAME}" ${srcs} ${kernel_srcs} ${incs} ${kernel_incs})
  TARGET_INCLUDE_DIRECTORIES(${LIB_NAME}
          PUBLIC
            $<INSTALL_INTERFACE:include>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
          PRIVATE
            ${CMAKE_CURRENT_SOURCE_DIR}/include
            ${inc_deps}
            ${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES}
            ${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES}/../samples/common/inc
            $<TARGET_PROPERTY:lts5_utils,INCLUDE_DIRECTORIES>)
  TARGET_LINK_LIBRARIES("${LIB_NAME}"
          PUBLIC
            ${CUDA_EXTRA_LIBRARIES}
            ${OpenCV_LIBS}
            lts5_utils
            lts5_cuda_utils)

  # Add test target, .... for testing purpose
  IF(OPT_BUILD_EXAMPLE)
    LTS5_ADD_EXAMPLE(lts5_gpu_sdm_tracker
            FILES
            test/test_gpu_sdm_tracker.cpp
            LINK_WITH
              lts5_utils lts5_cuda_utils lts5_cuda_face_tracker
            INC_FOLDER
              ${LTS5_SOURCE_DIR}/modules/face_tracker/include
              ${LTS5_SOURCE_DIR}/modules/face_detector/include
              ${LTS5_SOURCE_DIR}/modules/classifier/include)
  ENDIF(OPT_BUILD_EXAMPLE)

  # Install include files
  LTS5_ADD_INCLUDES("${SUBSYS_NAME}" "${SUBSYS_PATH}" ${incs})
endif()

