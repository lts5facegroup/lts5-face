/**
 *  @file   mstream_sdm_tracker.cpp
 *  @brief  SDM Face tracker for multiple image stream
 *          One tracking model for multiple input
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   12/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "opencv2/core/cuda_stream_accessor.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"

#include "lts5/cuda/face_tracker/mstream_sdm_tracker.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/math/cublas_wrapper.hpp"
#include "lts5/cuda/face_tracker/device/sdm_kernel.h"
#include "lts5/cuda/face_tracker/const_tracker_assessment.hpp"
#include "lts5/cuda/face_tracker/svm_tracker_assessment.hpp"

#include "lts5/utils/shape_transforms.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/string_util.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   MStreamSdmTracker
 *  @fn MStreamSdmTracker(const int n_stream)
 *  @brief  Constructor
 *  @param[in]  n_stream  Number of image to track
 */
MStreamSdmTracker::MStreamSdmTracker(const int n_stream) {
  n_stream_ = n_stream;
  size_t n = static_cast<size_t>(n_stream_);
  working_img_.resize(n);
  downsample_img_.resize(n);
  stream_.resize(n);
  sift_features_32f_.resize(n);
  sift_features_.resize(n);
  sift_features_wbiais_.resize(n);
  cache_.resize(n);
  previous_shape_.resize(n);
  sdm_model_.resize(n);
  face_location_.resize(n);
  current_shape_buffer_.resize(n);
  face_region_.resize(n);
  initial_shape_.resize(n);
  is_tracking_.resize(n);
}


/*
 *  @name   ~MStreamSdmTracker
 *  @fn ~MStreamSdmTracker(void)
 *  @brief  Destructor
 */
MStreamSdmTracker::~MStreamSdmTracker(void) {
  // Clean up
  for (size_t i = 0; i < tracker_assessment_.size(); ++i) {
    delete tracker_assessment_[i];
    tracker_assessment_[i] = nullptr;
  }
}

/*
 *  @name   Load
 *  @fn     int Load(const std::string& model_filename);
 *  @brief  Load the SDM model
 *  @param[in]  model_filename      Path to the model
 *  @return -1 if errors, 0 otherwise
 */
int MStreamSdmTracker::Load(const std::string& model_filename) {
  // Binary model ?
  int error = -1;
  const bool is_binary = model_filename.find(".bin") != std::string::npos;
  std::ios_base::openmode flag = (is_binary ?
                                  std::ios_base::in|std::ios_base::binary :
                                  std::ios_base::in);
  std::ifstream input_stream(model_filename.c_str(), flag);
  if (input_stream.is_open()) {
    int status = LTS5::ScanStreamForObject(input_stream,
                                           HeaderObjectType::kSdm);
    if (status == 0) {
      error = this->Load(input_stream, is_binary);
    }
  }
  return error;
}

/*
 *  @name   Load
 *  @fn     int Load(std::ifstream& model_stream, const bool& is_binary)
 *  @brief  Load the SDM model
 *  @param[in]  model_stream      Stream to the model
 *  @param[in]  is_binary         Indicate if stream is open as binary
 *  @return -1 if errors, 0 otherwise
 */
int MStreamSdmTracker::Load(std::istream& model_stream, const bool& is_binary) {
  // Check if it is a binary stream or not
  int err = -1;
  if (is_binary) {
    // Reader header
    err = this->tracker_parameters_.ReadParameterHeader(model_stream);
    this->tracker_parameters_.starting_pass = 1;
    // Bin
    err |= LTS5::ReadMatFromBin(model_stream, &sdm_meanshape_);
    // Define number of tracked points
    num_points_ = std::max(sdm_meanshape_.rows,sdm_meanshape_.cols) / 2;
    // Init feature extraction tree, aka keypoints
    sift_extraction_location_.resize(n_stream_,
                                     std::vector<cv::KeyPoint>(num_points_));
    // Load sdm
    sdm_model_.clear();
    sdm_model_.reserve(tracker_parameters_.number_of_stage);
    for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
      cv::Mat stage_model;
      err &= LTS5::ReadMatFromBin(model_stream, &stage_model);
      sdm_model_.push_back(Matrixd(stage_model));
      if (err != 0)
        break;
    }
    if (!err) {

     for (int i = 0; i < n_stream_; ++i) {
        // Init cache
        cache_[i].Create(4, static_cast<size_t>(num_points_));
        // Init sift container
        int n_elem = num_points_ * sift_desciptor_params_.sift_dim + 1;
        double* buffer = nullptr;
        cudaSafeCall(cudaMalloc(reinterpret_cast<void**>(&buffer),
                                static_cast<size_t>(n_elem * sizeof(double))));
        this->sift_features_[i] = Matrixd(sift_desciptor_params_.sift_dim,
                                          num_points_,
                                          buffer );
        this->sift_features_wbiais_[i] = Matrixd(1, n_elem, buffer);
        this->sift_features_wbiais_[i].SetTo(1.0);
      }


      // Tracker assessment present ?
      int status = ScanStreamForObject(model_stream,
                                       HeaderObjectType::kSvmTrackerAssessment);
      if (!status) {
        // found one, load it
        auto pos = model_stream.tellg();


        tracker_assessment_.resize(n_stream_, nullptr);
        for (int i = 0; i < n_stream_; ++i) {
          // Set stream at correct location
          model_stream.seekg(pos);
          // init assessment
          if (tracker_assessment_[i]) {
            delete tracker_assessment_[i];
          }
          tracker_assessment_[i] = new CUDA::SvmTrackerAssessment(sdm_meanshape_,
                                                                  sift_desciptor_params_,
                                                                  tracker_parameters_.face_region_width,
                                                                  tracker_parameters_.sift_size,
                                                                  &cache_[i],
                                                                  stream_[i]);
          err |= tracker_assessment_[i]->Load(model_stream, is_binary);
        }
      } else {
        // No use default
        tracker_assessment_.resize(n_stream_, nullptr);
        for (int i = 0; i < n_stream_; ++i) {
          if (tracker_assessment_[i]) {
            delete tracker_assessment_[i];
          }
          tracker_assessment_[i] = new LTS5::CUDA::ConstTrackerAssessment(true);
        }

      }
    }
  }
  return err;
}

/*
 *  @name   Load
 *  @fn     int Load(const std::string& model_filename,
            const std::string& face_detector_config)
 *  @brief  Load the SDM model and Viola Jones face detector
 *  @param[in]  model_filename          Path to the model
 *  @param[in]  face_detector_config    Configuration file for
 *                                      Viola & Jones face detector
 *  @return -1 if errors, 0 otherwise
 */
int MStreamSdmTracker::Load(const std::string& model_filename,
                            const std::string& face_detector_config) {
  // Load SDM
  int error = this->Load(model_filename);
  // Init Viola-Jones detector
  face_detector_ = cv::cuda::CascadeClassifier::create(face_detector_config);
  error |= !face_detector_->empty() ? error : -1;
  return error;
}

/*
 *  @name   Load
 *  @fn     int Load(std::istream& model_stream,
                     const bool& is_binary,
                     const std::string& face_detector_config)
 *  @brief  Load the SDM model and Viola Jones face detector
 *  @param[in]  model_stream          Stream to the model
 *  @param[in]  is_binary             Indicate if stream is open as binary
 *  @param[in]  face_detector_config  Configuration file for Viola & Jones
 *                                    face detector
 *  @return -1 if errors, 0 otherwise
 */
int MStreamSdmTracker::Load(std::istream& model_stream,
                            const bool& is_binary,
                            const std::string& face_detector_config) {
  int error = -1;
  // Load SDM
  if (model_stream.good()) {
    error = this->Load(model_stream, is_binary);
  }
  // Init Viola-Jones detector
  face_detector_ = cv::cuda::CascadeClassifier::create(face_detector_config);
  error |= !face_detector_.empty() ? error : -1;
  return error;
}

/*
 *  @name   Save
 *  @fn int Save(const std::string& model_name)
 *  @brief  Save the trained model in file.
 *  @param  [in]    model_name      Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int MStreamSdmTracker::Save(const std::string& model_name) {
  int err = -1;
  // Open stream
  std::string dir, file, ext, filename;
  LTS5::ExtractDirectory(model_name, &dir, &file, &ext);
  filename = dir + file + ".bin";
  std::ofstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    // Write object
    err = this->Write(stream);
  }
  return err;
}

/*
 *  @name   Write
 *  @fn int Write(std::ostream& out_stream) const
 *  @brief  Write the object into a binary stream
 *  @param[in]  out_stream  Binary stream to files
 *  @return -1 if error, 0 otherwise.
 */
int MStreamSdmTracker::Write(std::ostream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Ok
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kSdm);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type),
                     sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size),
                     sizeof(obj_size));
    // Parameters header
    error = tracker_parameters_.WriteParameterHeader(out_stream);
    // Mean shape
    error |= LTS5::WriteMatToBin(out_stream, sdm_meanshape_);
    // Regressor
    for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
      cv::Mat m;
      sdm_model_[i].Download(&m);
      error |= LTS5::WriteMatToBin(out_stream, m);
    }
    // Quality assessment
    error |= tracker_assessment_[0]->Write(out_stream);
    // Done sanity check,
    error |= out_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name   ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the memory used by object
 *  @return Size in bytes
 */
int MStreamSdmTracker::ComputeObjectSize(void) const {
  int size = 0;
  // Configuration header
  size = tracker_parameters_.ComputeHeaderSize();
  // Meanshape
  size += 3 * sizeof(int);
  size += sdm_meanshape_.total() * sdm_meanshape_.elemSize();
  // Regressor
  for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
    // Matrix header
    size += 3 * sizeof(int);
    // Model
    cv::Mat m;
    sdm_model_[i].Download(&m);
    size += m.total() * m.elemSize();
  }
  return size;
}


#pragma mark -
#pragma mark Tracking

/*
 *  @name   Detect
 *  @fn int Detect(const cv::cuda::GpuMat& image, Matrixd* shape)
 *  @brief  Dectection method, given a still image, provides a set
 *          of landmarks.
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::Detect(const cv::cuda::GpuMat& image, Matrixd* shape) {
  return this->Detect(image, 0, shape);
}

/*
 *  @name   Detect
 *  @fn int Detect(const std::vector<cv::cuda::GpuMat>&images,
                   std::vector<Matrixd>* shapes)
 *  @brief  Dectection method, given a multiples image, provides a set of
 *          landmarks for each image. This must be override by subclass
 *  @param[in]  images   List of input images
 *  @param[out] shapes   List of set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::Detect(const std::vector<cv::cuda::GpuMat>& images,
                              std::vector<Matrixd>* shapes) {
  assert(images.size() == n_stream_);
  shapes->resize(static_cast<size_t>(n_stream_));
  // Loop over all image stream
  int err = 0;
  for (int i = 0; i < n_stream_; ++i) {
    err |= this->Detect(images[i], i, &((*shapes)[i]));
  }
  return err;
}

/*
 *  @name   Detect
 *  @fn int Detect(const cv::cuda::GpuMat& image,
                   const cv::Rect& face_region,
                   Matrixd* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks.
 *  @param[in]  image           Input image
 *  @param[in]  face_region     Face boundingg box
 *  @param[out] shape           Set of landmarks
 *  @return     >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::Detect(const cv::cuda::GpuMat& image,
                              const cv::Rect& face_region,
                              Matrixd* shape) {
  return this->Detect(image, face_region, 0, shape);
}

/*
 *  @name   Detect
 *  @fn int Detect(const std::vector<cv::cuda::GpuMat>& images,
                   const std::vector<cv::Rect>& face_regions,
                   std::vector<Matrixd>* shapes)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks. This must be override by subclass
 *  @param[in]  images           List of input image
 *  @param[in]  face_regions     List of face boundingg box
 *  @param[out] shapes           List of set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::Detect(const std::vector<cv::cuda::GpuMat>& images,
                              const std::vector<cv::Rect>& face_regions,
                              std::vector<Matrixd>* shapes) {
  assert((images.size() == n_stream_) && face_regions.size() == n_stream_);
  shapes->resize(static_cast<std::size_t >(n_stream_));
  // Loop over all image stream
  int err = 0;
  for (int i = 0; i < n_stream_; ++i) {
    err |= this->Detect(images[i], face_regions[i], i, &((*shapes)[i]));
  }
  return err;
}

/*
 *  @name   Track
 *  @fn int Track(const cv::cuda::GpuMat& image, Matrixd* shape)
 *  @brief  Tracking method, given an input image
 *  @param[in]      image   Input image
 *  @param[in,out]  shape   Set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::Track(const cv::cuda::GpuMat& image, Matrixd* shape) {
  return this->TrackStream(image, 0, shape);
}

/*
 *  @name   Track
 *  @fn int Track(const std::vector<cv::Mat>& images,
                  std::vector<Matrixd>* shapes)
 *  @brief  Tracking method, given an input image (from a sequence), provides
 *          a set of landmarks. This must be override by subclass
 *  @param[in]  images   List of input image
 *  @param[out] shapes   List of set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::Track(const std::vector<cv::cuda::GpuMat>& images,
                             std::vector<Matrixd>* shapes) {
  assert(images.size() == n_stream_);
  shapes->resize(static_cast<size_t>(n_stream_));
  LTS5_CUDA_TRACE("Process",4);
#if defined(__APPLE__)
  __block int err = 0;
  dispatch_apply(n_stream_,
                 dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                           (unsigned long)NULL), ^(size_t i) {
#else
  int err = 0;
  for (int i = 0; i < n_stream_; ++i) {
#endif
    err |= this->TrackStream(images[i], i, &(shapes->at(i)));
  }
#if defined(__APPLE__)
  );
#endif
  return err;
}


#pragma mark -
#pragma mark Training

/**
 *  @name   Train
 *  @fn void Train(const std::string& folder)
 *  @brief  SDM training method
 *  @param[in]  folder  Location of images/annotations
 */
void MStreamSdmTracker::Train(const std::string& folder) {

}

#pragma mark -
#pragma mark Private

/*
 *  @name   Detect
 *  @fn int Detect(const cv::cuda::GpuMat& image,const int n_stream, Matrixd* shape)
 *  @brief  Dectection method, given a still image, provides a set
 *          of landmarks.
 *  @param[in]  image     Input image
 *  @param[in]  n_stream  Current stream index
 *  @param[out] shape     Set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::Detect(const cv::cuda::GpuMat& image,
                              const int n_stream,
                              Matrixd * shape) {
  int err = 0;
  // Take reference to vector
  auto& working_img = working_img_[n_stream];
  auto& downsample_img = downsample_img_[n_stream];
  auto& stream = stream_[n_stream];
  auto& face_location = face_location_[n_stream];
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cuda::cvtColor(image, working_img, cv::COLOR_BGR2GRAY, 0, stream);
  } else {
    working_img = image;
  }
  // Downsample image to speedup computation
  float aspect_ratio = (float)working_img.rows / (float)working_img.cols;
  int img_height = static_cast<int>(320.f * aspect_ratio);
  cv::cuda::resize(working_img,
                   downsample_img,
                   cv::Size(320, img_height), 0, 0,
                   cv::INTER_LINEAR,
                   stream);
  float scale = (static_cast<float>(working_img.cols) /
                 static_cast<float>(downsample_img.cols));
  // Scan image for faces
  stream.waitForCompletion();
  int n_face = 0;
  face_detector_->setScaleFactor(1.1);
  face_detector_->setMinNeighbors(2);
  face_detector_->setMinObjectSize(cv::Size((200.0 / scale),
                                            (200.0 / scale)));
  face_detector_->detectMultiScale(downsample_img, face_location);
  n_face = face_location.rows;
  if (n_face > 0) {
    auto& face_region = face_region_[n_stream];
    cv::Mat host_loc;
    face_location.colRange(0, n_face).download(host_loc);
    cv::Rect* rect = host_loc.ptr<cv::Rect>();
    // Sort by size
    std::sort(rect,
              rect + n_face,
              LTS5::SdmTracker::SortRectBySize());
    face_region = *rect;
    // Convert back to original image coordinate
    int x = static_cast<int>(face_region.x * scale);
    int y = static_cast<int>(face_region.y * scale);
    int w = static_cast<int>(face_region.width * scale);
    int h = static_cast<int>(face_region.height * scale);
    face_region.x = x;
    face_region.y = y;
    face_region.width = w;
    face_region.height = h;
    // Can track landmarks, create inital shape. If size is Ok does nothing
    initial_shape_[n_stream].create(num_points_ * 2, 1, CV_64F);
    previous_shape_[n_stream].Create(num_points_ * 2, 1);
    // Compute initial shape
    this->ComputeInitialShape(sdm_meanshape_,
                              face_region,
                              &initial_shape_[n_stream]);
    //  Face alignment
    if (tracker_parameters_.align_with_rotation) {
      *shape = this->AlignFaceWithScaleAndRotation(working_img, n_stream);
    } else {
      *shape = this->AlignFaceWithScale(working_img, n_stream);
    }
    // Save shape as previous
    previous_shape_[n_stream] = shape->Clone(stream);
  } else {
    err |= (0x01<<n_stream);
  }
  return err;
}

/*
 *  @name   Detect
 *  @fn int Detect(const cv::cuda::GpuMat& image,
                   const cv::Rect& face_region,
                   const int n_stream,
                   Matrixd* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks.
 *  @param[in]  image           Input image
 *  @param[in]  face_region     Face boundingg box
 *  @param[in]  n_stream        Current stream index
 *  @param[out] shape           Set of landmarks
 *  @return     >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::Detect(const cv::cuda::GpuMat& image,
                              const cv::Rect& face_region,
                              const int n_stream,
                              Matrixd* shape) {
  // Take reference to vector
  auto& working_img = working_img_[n_stream];
  auto& stream = stream_[n_stream];
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cuda::cvtColor(image, working_img, cv::COLOR_BGR2GRAY, 0, stream);
  } else {
    working_img = image;
  }
  // Can track landmarks, create inital shape. If size is Ok does nothing
  initial_shape_[n_stream].create(num_points_ * 2, 1, CV_64F);
  previous_shape_[n_stream].Create(num_points_ * 2, 1);
  // Align init shape with bbox
  face_region_[n_stream] = face_region;
  this->ComputeInitialShape(sdm_meanshape_,
                            face_region,
                            &initial_shape_[n_stream]);
  //  Face alignment
  if (tracker_parameters_.align_with_rotation) {
    *shape = this->AlignFaceWithScaleAndRotation(working_img, n_stream);
  } else {
    *shape = this->AlignFaceWithScale(working_img, n_stream);
  }
  // Save shape as previous
  previous_shape_[n_stream] = shape->Clone(stream);
  return 0;
}

/*
 *  @name   Track
 *  @fn int Track(const cv::cuda::GpuMat& image, Matrixd* shape)
 *  @brief  Tracking method, given an input image
 *  @param[in]      image   Input image
 *  @param[in,out]  shape   Set of landmarks
 *  @return >0 if error (bit indicate stream), 0 otherwise
 */
int MStreamSdmTracker::TrackStream(const cv::cuda::GpuMat& image,
                             const int n_stream,
                             Matrixd* shape) {
  LTS5_CUDA_TRACE("Track(...)", 4);
  int err = -1;
  // Color ?
  auto& working_img = working_img_[n_stream];
  auto& stream = stream_[n_stream];
  auto& previous_shape  = previous_shape_[n_stream];
  auto& initial_shape = initial_shape_[n_stream];
  if (image.channels() != 1) {
    cv::cuda::cvtColor(image,
                       working_img,
                       cv::COLOR_BGR2GRAY,
                       0,
                       stream_[n_stream]);
  } else {
    working_img = image;
  }
  // Have we already detect something ?
  if (!is_tracking_[n_stream]) {
    // Look for face
    err = this->Detect(working_img, n_stream, shape);
    is_tracking_[n_stream] = true;
  } else {
    // Tracking
    previous_shape = *shape;
    this->AlignMeanshapeToCurrent(previous_shape, n_stream, &initial_shape);
    *shape = this->AlignFaceWithScaleAndRotation(working_img, n_stream);
    err = 0;
  }
  // Still tracking ?
  double score = tracker_assessment_[n_stream]->Assess(working_img, *shape);
  is_tracking_[n_stream] = score > 0.0;
  if (!is_tracking_[n_stream]) {
    *shape = Matrixd();
    std::cout << "Reset stream : " << n_stream;
    std::cout << " score : " << tracker_assessment_[n_stream]->get_score() <<std::endl;
  } else {
    // Save shape
    previous_shape = shape->Clone(stream);
  }
  return err;
}

/*
 *  @name   ComputeInitialShape
 *  @fn void ComputeInitialShape(const cv::Mat& meanshape,
                                 const cv::Rect& bounding_box,
                                 cv::Mat* initial_shape)
 *  @brief  Aligned the meanshape inside the bounding box
 *  @param[in]  meanshape       Model meanshape
 *  @param[in]  bounding_box    Region of interest
 *  @param[out] initial_shape   Initial shape
 */
void MStreamSdmTracker::ComputeInitialShape(const cv::Mat& meanshape,
                                            const cv::Rect& bounding_box,
                                            cv::Mat* initial_shape) {
  // Init
  initial_shape->create(meanshape.rows, meanshape.cols, meanshape.type());
  // Compute shape
  const double* src = reinterpret_cast<const double*>(meanshape.data);
  double* dest = reinterpret_cast<double*>(initial_shape->data);
  for (int p = 0; p < num_points_; ++p, ++src, ++dest) {
    dest[0] = bounding_box.x + src[0] * bounding_box.width;
    dest[num_points_] = bounding_box.y + src[num_points_] * bounding_box.width;
  }
}

/*
 *  @name   AlignFaceWithScale
 *  @fn Matrixd AlignFaceWithScale(const cv::cuda::GpuMat& image, const int n_stream)
 *  @brief  Face alignement with SDM model
 *  @param[in]  image     Input image
 *  @param[in]  n_stream  Current stream
 *  @return Return the new shape
 */
MStreamSdmTracker::Matrixd
MStreamSdmTracker::AlignFaceWithScale(const cv::cuda::GpuMat& image,
                                      const int n_stream) {
  std::cout << "Not implemented" << std::endl;
  return Matrixd();
}

/*
 *  @name   AlignFaceWithScaleAndRotation
 *  @fn Matrixd AlignFaceWithScaleAndRotation(const cv::cuda::GpuMat& image,
                                              const int n_stream)
 *  @brief  Face alignement with SDM model
 *  @param[in]  image     Input image
 *  @param[in]  n_stream  Current stream
 *  @return Return the new shape
 */
MStreamSdmTracker::Matrixd
MStreamSdmTracker::AlignFaceWithScaleAndRotation(const cv::cuda::GpuMat& image,
                                                 const int n_stream) {
  using LA = CUDA::LinearAlgebra<double>;
  using TType = CUDA::LinearAlgebra<double>::TransposeType;
  // Take references
  Matrixd shape_update;
  cv::cuda::GpuMat normalized_image;
  cv::Mat reference_shape, trsfrm, inv_trsfrm;
  const int number_pass = this->tracker_parameters_.number_of_pass;
  const int starting_pass = tracker_parameters_.starting_pass;
  cv::cuda::Stream& stream = stream_[n_stream];
  cv::Mat normalized_shape, prev_normalized_shape;
  // Loop over all stage
  auto model_it = sdm_model_.cbegin();
  for (int stage = 0;
       stage < tracker_parameters_.number_of_stage;
       ++stage, ++model_it) {
    // Define reference shape
    int edge = tracker_parameters_.face_region_width;
    if (tracker_parameters_.coarse_to_fine_search &&
        stage < tracker_parameters_.number_of_coarse_stages) {
      edge /= 2;
    }
    for (int pass = starting_pass; pass < number_pass; pass++) {
      //  Normalize face
      // ----------------------------------------------
      if (stage == 0) {
        shape_update.Upload(initial_shape_[n_stream], stream);
        normalized_shape = initial_shape_[n_stream].clone();
      } else {
        shape_update.Download(stream, &normalized_shape);
        stream.waitForCompletion();
      }
      reference_shape = sdm_meanshape_ * edge + edge/2;
      ComputeSimilarityTransform(reference_shape, normalized_shape, &trsfrm);
      //Define inverse
      cv::invertAffineTransform(trsfrm, inv_trsfrm);
      cv::cuda::warpAffine(image,
                           normalized_image,
                           inv_trsfrm,
                           cv::Size(edge * 2, edge * 2),
                           cv::INTER_LINEAR,
                           cv::BORDER_REPLICATE,
                           cv::Scalar(),
                           stream);
      //normalized shape
      CUDA::TransformShape(ShapeTransform<double>(inv_trsfrm),
                           stream,
                           &shape_update);
      LTS5::TransformShape(inv_trsfrm, &normalized_shape);
      // Sift extraction + update key points
      // ----------------------------------------------
      double x = 0.0, y = 0.0;
      for (int p = 0; p < sift_extraction_location_[n_stream].size(); ++p) {
        x = normalized_shape.at<double>(p);
        y = normalized_shape.at<double>(p + num_points_);
        auto& kp = sift_extraction_location_[n_stream][p];
        kp.pt.x = cvRound(x);
        kp.pt.y = cvRound(y);
        kp.size = tracker_parameters_.sift_size;
      }
      stream.waitForCompletion();
      GPUSSift::ComputeDescriptor(normalized_image,
                                  sift_extraction_location_[n_stream],
                                  sift_desciptor_params_,
                                  &cache_[n_stream],
                                  &sift_features_32f_[n_stream]);
      sift_features_32f_[n_stream].ConvertTo(stream,
                                             &sift_features_[n_stream]);
      // Shape update
      // ----------------------------------------------
      LA::Gemv(*model_it,
               TType::kNoTranspose,
               1.0,
               sift_features_wbiais_[n_stream],
               1.0,
               stream,
               &shape_update);

      if (pass == 0) {
        throw std::runtime_error("Multipass not supported yet !");
        /*// Get new shape
        cv::Mat temp_shape, new_shape, trsfrm_t;
        shape_update.Download(stream, &new_shape);
        stream.waitForCompletion();
        // Normalize
        temp_shape = LTS5::TransformShape(trsfrm, new_shape);


        ComputeSimilarityTransform(prev_normalized_shape, temp_shape, &trsfrm_t);

        std::cout << trsfrm << std::endl;
        std::cout << trsfrm_t << std::endl;

        normalized_shape = LTS5::TransformShape(trsfrm_t, prev_normalized_shape);
        //shape_update[n_stream].Upload(n_shape, stream);*/


      } else {
        // Back-project shape to original coordinates
        CUDA::TransformShape(CUDA::ShapeTransform<double>(trsfrm),
                             stream_[n_stream],
                             &shape_update);
      }
    }
  }
  stream.waitForCompletion();
  return shape_update;
}

/*
 *  @name AlignMeanshapeToCurrent
 *  @fn cv::Mat AlignMeanshapeToCurrent(const Matrixd& current_shape,
                                       const int n_stream,
                                       cv::Mat* aligned_shape)
 *  @brief  Align the meanshape on the shape recovered at previous
 *          frame
 *  @param[in]  current_shape   Shape revovered at previous frame
 *  @param[in]  n_stream  Current stream
 *  @param[in,out] aligned_shape  Transformed meanshape
 *  @return Aligned shape
 */
void MStreamSdmTracker::AlignMeanshapeToCurrent(const Matrixd& current_shape,
                                                const int n_stream,
                                                cv::Mat* aligned_shape) {
  aligned_shape->create(sdm_meanshape_.rows,
                        sdm_meanshape_.cols,
                        sdm_meanshape_.type());
  cv::Mat trsfrm;
  current_shape.Download(stream_[n_stream], &current_shape_buffer_[n_stream]);
  stream_[n_stream].waitForCompletion();
  // Compute similarity
  LTS5::ComputeSimilarityTransform(sdm_meanshape_,
                                   current_shape_buffer_[n_stream],
                                   &trsfrm);
  // Apply transformation
  const double* src = reinterpret_cast<const double*>(sdm_meanshape_.data);
  const double* t_ptr = reinterpret_cast<const double*>(trsfrm.data);
  double* dest = reinterpret_cast<double*>(aligned_shape->data);
  for (int p = 0; p < num_points_; ++p, ++src, ++dest) {
    const double x = src[0];
    const double y = src[num_points_];
    dest[0] = t_ptr[0] * x + t_ptr[1] * y + t_ptr[2];
    dest[num_points_] = t_ptr[3] * x + t_ptr[4] * y + t_ptr[5];
  }
}


}  // namepsace CUDA
}  // namepsace LTS5