/**
 *  @file   svm_tracker_assessment.cpp
 *  @brief  face tracker assessment based on svm classifier
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   06/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <fstream>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/cuda_stream_accessor.hpp"
#include "opencv2/cudawarping.hpp"

#include "lts5/cuda/face_tracker/svm_tracker_assessment.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/cuda/utils/math/cublas_wrapper.hpp"
#include "lts5/cuda/utils/safe_call.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   SvmTrackerAssessment
 *  @fn SvmTrackerAssessment(const cv::Mat& meanshape,
                     const LTS5::SSift::SSiftParameters& sift_parameters,
                     const int edge,
                     const int sift_size)
 *  @brief  Constructor
 *  @param[in] meanshape  Meanshape
 *  @param[in] sift_parameters    SSIFT extraction parameters
 *  @param[in] edge               Edge size
 *  @param[in] sift_size          Size windows dimension
 *  @param[in] stream             Stream on which to run extraction
 */
SvmTrackerAssessment::SvmTrackerAssessment(const cv::Mat& meanshape,
                                           const LTS5::SSift::SSiftParameters& sift_parameters,
                                           const int edge,
                                           const int sift_size,
                                           const GPUSSift::GPUCache* cache,
                                           const cv::cuda::Stream& stream) {
  // Set parameters
  sift_parameters_ = sift_parameters;
  sift_size_ = sift_size;
  // Compute reference shape
  edge_ = edge;
  reference_shape_ = meanshape * edge + edge/2.0;
  // single stream
  // 49 points only inner shape
  cache_ = cache;
  stream_ = stream;
}

/*
 *  @name   ~SvmTrackerAssessment
 *  @fn ~SvmTrackerAssessment(void)
 *  @brief  Destructor
 */
SvmTrackerAssessment::~SvmTrackerAssessment(void) {
}

/*
 *  @name   Load
 *  @brief  Load the assessment model from file
 *  @fn int Load(const std::string& model_name)
 *  @param[in]  model_name      Path to the model
 *  @return -1 if errors, 0 otherwise
 */
int SvmTrackerAssessment::Load(const std::string& model_name) {
  int err = -1;
  std::ifstream input_stream(model_name.c_str(),
                             std::ios_base::in|std::ios_base::binary);
  // Open ?
  if (input_stream.is_open()) {
    int status = ScanStreamForObject(input_stream,
                                     HeaderObjectType::kSvmTrackerAssessment);
    if (status == 0) {
      err = this->Load(input_stream, true);
    }
  }
  return err;
}

/*
 *  @name   Load
 *  @fn int Load(std::istream& input_stream, const bool& is_binary)
 *  @brief  Load the assessment model from file stream
 *  @param[in]  input_stream  File stream for reading the model
 *  @param[in]  is_binary     Indicate if stream is open has binary
 *  @return -1 if errors, 0 otherwise
 */
int SvmTrackerAssessment::Load(std::istream& input_stream,
                               const bool& is_binary) {
  int err = -1;
  if (input_stream.good()) {
    cv::Mat svm_w, data;
    // Read svm model
    err = LTS5::ReadMatFromBin(input_stream, &data);
    svm_w = data.colRange(0, data.cols - 1).clone();
    svm_.Upload(svm_w);
    bias_ = data.at<double>(data.cols - 1);
    // Load bias
    input_stream.read(reinterpret_cast<char*>(&score_threshold_),
                      sizeof(score_threshold_));
    // Sanity check
    err |= input_stream.good() ? 0 : -1;
  }
  return err;
}

/*
 *  @name   Write
 *  @fn int Write(std::ofstream& out_stream) const
 *  @brief  Copy the object into a binary stream
 *  @param[in]  out_stream  Output binary stream
 *  @return -1 if error, 0 otherwise
 */
int SvmTrackerAssessment::Write(std::ostream& out_stream) const {
  int err = -1;
  if (out_stream.good()) {
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kSvmTrackerAssessment);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type),
                     sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size),
                     sizeof(obj_size));
    // svm
    cv::Mat svm_w;
    svm_.Download(&svm_w);
    err = LTS5::WriteMatToBin(out_stream, svm_w);
    // threshold
    out_stream.write(reinterpret_cast<const char*>(&score_threshold_),
                     sizeof(score_threshold_));
    // Done, sanity check
    err |= out_stream.good() ? 0 : -1;
  }
  return err;
}

/*
 *  @name ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the object size in bytes
 *  @return Object memory size
 */
int SvmTrackerAssessment::ComputeObjectSize(void) const {
  int sz = 3 * sizeof(int);
  cv::Mat svm_w;
  svm_.Download(&svm_w);
  sz += svm_w.total() * svm_w.elemSize();
  // Threshold
  sz += sizeof(score_threshold_);
  return sz;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Assess
 *  @fn double Assess(const cv::cuda::GpuMat& image,
                      const Matrix<double>& shape)
 *  @brief  Assessment method, given the current shape and image
 *  @param[in]  image   Input image
 *  @param[in]  shape     Current shape tracked by the face tracker
 *  @return   scalar score, the larger the better
 */
double SvmTrackerAssessment::Assess(const cv::cuda::GpuMat& image,
                                    const Matrix<double>& shape) {
  using LA = LTS5::CUDA::LinearAlgebra<double>;
  LTS5_CUDA_TRACE("Svm::Assess(...)", 2);
  // Init score
  double score = -std::numeric_limits<double>::max();
  this->score_ = score;
  if (!image.empty() && !shape.empty()) {
    // Extract feature
    this->ExtractFeature(image, shape);
    // Assess
    this->score_ = LA::Dot(this->features_, svm_, stream_);
    stream_.waitForCompletion();
    this->score_ += bias_;
    // Score
    score = this->score_ > score_threshold_ ? 1.0 : -1.0;
  }
  return score;
}

#pragma mark -
#pragma mark Private

/*
 * @name  ExtractFeature
 * @fn    void ExtractFeature(const cv::cuda::GpuMat& image,
                              const Matrix<double>& shape)
 * @brief Extract sift feature at specific location
 * @param[in] image   Input image
 * @param[in] shape   Current position of tracked landmarks
 */
void SvmTrackerAssessment::ExtractFeature(const cv::cuda::GpuMat& image,
                                          const Matrix<double>& shape) {
  // Normalize shape + image
  // ----------------------------------------------------------------------
  // Compute similarity transform
  cv::Mat host_shape, host_norm_shape, trsfrm;
  shape.Download(stream_, &host_shape);
  stream_.waitForCompletion();
  LTS5::ComputeSimilarityTransform(host_shape, reference_shape_, &trsfrm);
  // Warp
  cv::cuda::warpAffine(image,
                       normalized_image_,
                       trsfrm,
                       cv::Size(edge_ * 2, edge_ * 2),
                       cv::INTER_LINEAR,
                       cv::BORDER_REPLICATE, cv::Scalar(),
                       stream_);
  //normalized shape
  host_norm_shape = LTS5::TransformShape(trsfrm, host_shape);
  // Extract features
  // ----------------------------------------------------------------------
  int num_points = std::max(host_shape.rows, host_shape.cols)/2;
  static std::vector<cv::KeyPoint> sift_location;
  sift_location.resize(49);
  int k = 0;
  double x = 0.0, y = 0.0;
  for (int p = 0; p < num_points; ++p) {
    // only inner face landmarks and without 60 and 64 (inside lip cornner)
    if (p < 17 || p == 60 || p == 64) {
      continue;
    }
    x = host_norm_shape.at<double>(p);
    y = host_norm_shape.at<double>(p + num_points);
    auto& loc = sift_location[k++];
    loc.pt.x = cvRound(x);
    loc.pt.y = cvRound(y);
    loc.size = sift_size_;
  }
  stream_.waitForCompletion();
  // Extract ssift
  LTS5::CUDA::GPUSSift::ComputeDescriptor(normalized_image_,
                                          sift_location,
                                          sift_parameters_,
                                          cache_,
                                          &features_32f_);
  features_32f_.Reshape(features_32f_.rows() * features_32f_.cols(), 1);
  // Convert to double
  // ----------------------------------------------------------------------
  features_32f_.ConvertTo(stream_, &this->features_);
}
}  // namepsace CUDA
}  // namepsace LTS5

