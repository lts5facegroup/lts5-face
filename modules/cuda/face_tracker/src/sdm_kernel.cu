/**
 *  @file   sdm_kernel.cu
 *  @brief  CUDA kernel for SDM face tracker
 *
 *  @author Christophe Ecabert
 *  @date   09/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "opencv2/core/cuda_stream_accessor.hpp"

#include "lts5/cuda/face_tracker/device/sdm_kernel.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Type definition

/*
 * @name  ShapeTransform
 * @fn    ShapeTransform(void)
 * @brief Constructor
 */
template<typename T>
ShapeTransform<T>::ShapeTransform(void) : a(0), b(0), tx(0), ty(0) {}

/*
 * @name  ShapeTransform
 * @fn    ShapeTransform(const cv::Mat& transform)
 * @brief Constructor
 * @param[in] transform   Affine transform
 */
template<typename T>
ShapeTransform<T>::ShapeTransform(const cv::Mat& transform) {
  a = transform.at<T>(0);
  tx = transform.at<T>(2);
  b = transform.at<T>(3);
  ty = transform.at<T>(5);
}

#pragma mark -
#pragma mark Device code

/**
 *  @namespace  device
 *  @brief      CUDA device code dev space
 */
namespace device {

/*
 * @name    transform_shape
 * @fn      void TransformShape(const ShapeTransform<T> transform,
                                const int N,
                                T* shape)
 * @brief   Apply affine transformation on a given \p shape. Transformation
 *          happens inplace.
 * @tparam T    Data type
 * @param[in] transform Affine transform
 * @param[in] N         Number of points in the shape
 * @param[in,out] shape Shape to transform
 */
template<>
__global__
void transform_shape(const ShapeTransform<float> transform,
                     const int N,
                     float* shape) {
  int i = blockDim.x * blockIdx.x + threadIdx.x;
  if (i < N) {
    // read value
    float x = shape[2 * i];
    float y = shape[2 * i + 1];
    // Compute transform
    shape[2 * i] = (transform.a * x) - (transform.b * y) + transform.tx;
    shape[2 * i + 1] = (transform.b * x) + (transform.a * y) + transform.ty;
  }
}
template<>
__global__
void transform_shape(const ShapeTransform<double> transform,
                     const int N,
                     double* shape) {
  int i = blockDim.x * blockIdx.x + threadIdx.x;
  if (i < N) {
    // read value
    double x = shape[2 * i];
    double y = shape[2 * i + 1];
    // Compute transform
    shape[2 * i] = (transform.a * x) - (transform.b * y) + transform.tx;
    shape[2 * i + 1] = (transform.b * x) + (transform.a * y) + transform.ty;
  }
}

}  // namepsace device

#pragma mark -
#pragma mark Kernel caller

/*
 * @name    TransformShape
 * @fn      void TransformShape(const ShapeTransform<T>& transform,
                                const cv::cuda::Stream& stream,
                                Matrix* shape)
 * @brief   Apply affine transformation on a given \p shape. Transformation
 *          happens inplace.
 * @tparam T    Data type
 * @param[in] transform Affine transform
 * @param[in] stream    Stream on which to run computation
 * @param[in,out] shape Shape to transform
 */
template<>
void TransformShape(const ShapeTransform<float>& transform,
                    const cv::cuda::Stream& stream,
                    Matrix<float>* shape) {
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Define dimension
  int N = std::max(shape->rows(), shape->cols()) / 2;
  int thd = 32;
  int  bck = (N + 31) / 32;
  // Call kernel
  device::transform_shape<<<bck, thd, 0, s>>>(transform,
          N,
          shape->data());
}

template<>
void TransformShape(const ShapeTransform<double>& transform,
                    const cv::cuda::Stream& stream,
                    Matrix<double>* shape) {
  // Get stream
  cudaStream_t s = stream == cv::cuda::Stream::Null() ?
                   0 :
                   cv::cuda::StreamAccessor::getStream(stream);
  // Define dimension
  int N = std::max(shape->rows(), shape->cols()) / 2;
  int thd = 32;
  int  bck = (N + 31) / 32;
  // Call kernel
  device::transform_shape<<<bck, thd, 0, s>>>(transform,
          N,
          shape->data());
}

#pragma mark -
#pragma mark Explicit instanciation

/** Shape Transform - float */
template
struct ShapeTransform<float>;
/** Shape Transform - double */
template
struct ShapeTransform<double>;

}  // namepsace CUDA
}  // namepsace LTS5
