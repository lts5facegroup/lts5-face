/**
 *  @file   const_tracker_assessment.cpp
 *  @brief  constant face tracker assessment
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   06/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <fstream>

#include "lts5/cuda/face_tracker/const_tracker_assessment.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/cuda/utils/math/cublas_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Initialization
/*
 *  @name   ConstTrackerAssessment
 *  @brief  Constructor
 *  @param[in] detect Flag to indicate dectection or tracking
 */
ConstTrackerAssessment::ConstTrackerAssessment(const bool detect) {
  detect_ = detect;
}

/*
 *  @name   ~ConstTrackerAssessment
 *  @brief  Destructor
 */
ConstTrackerAssessment::~ConstTrackerAssessment(void) {
}

/*
 *  @name   Load
 *  @brief  Load the assessment model from file
 *  @param[in]  model_filename      Path to the model
 *  @return -1 if errors, 0 otherwise
 */
int ConstTrackerAssessment::Load(const std::string& model_name) {
  int error = -1;
  const bool is_binary = model_name.find(".bin") != std::string::npos;
  std::ios_base::openmode flag = (is_binary ?
                                  std::ios_base::in|std::ios_base::binary :
                                  std::ios_base::in);
  std::ifstream input_stream(model_name.c_str(), flag);
  // Open ?
  if (input_stream.is_open()) {
    int status = LTS5::ScanStreamForObject(input_stream,
                                           HeaderObjectType::kConstTrackerAssessment);
    if (status == 0) {
      error = this->Load(input_stream, is_binary);
    }
  }
  return error;
}

/*
 *  @name   Load
 *  @brief  Load the assessment model from file stream
 *  @param[in]  input_stream  File stream for reading the model
 *  @param[in]  is_binary     Indicate if stream is open has binary
 *  @return -1 if errors, 0 otherwise
 */
int ConstTrackerAssessment::Load(std::istream& input_stream,
                                 const bool& is_binary) {
  int error = -1;
  if (input_stream.good()) {
    int state = -1;
    input_stream.read(reinterpret_cast<char*>(&state), sizeof(state));
    detect_ = (state != 0);
    error = state == -1 ? -1 : 0;
  }
  return error;
}

/*
 *  @name   Write
 *  @brief  Copy the object into a binary stream
 *  @param[in]  out_stream  Output binary stream
 *  @return -1 if error, 0 otherwise
 */
int ConstTrackerAssessment::Write(std::ostream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Header
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kConstTrackerAssessment);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type), sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size), sizeof(obj_size));
    int state = detect_ ? 1 : 0;
    out_stream.write(reinterpret_cast<const char*>(&state), sizeof(state));
    // Done sanity check
    error = out_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name ComputeObjectSize
 *  @brief  Compute the object size in bytes
 *  @return Object memory size
 */
int ConstTrackerAssessment::ComputeObjectSize(void) const {
  return sizeof(int);
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name set_score_threshold
 *  @fn virtual void set_score_threshold(const double threshold)
 *  @brief  Set up a new decision threshold
 *  @param[in]  threshold   New decision threshold
 */
void ConstTrackerAssessment::set_score_threshold(const double threshold) {
  std::cout << "Constant assessment does not have decision threshold !" << std::endl;
}

/*
 *  @name get_score_threshold
 *  @fn virtual double get_score_threshold(void) = 0
 *  @brief  Get the current decision threshold
 *  @return  NaN
 */
double ConstTrackerAssessment::get_score_threshold(void) const {
  return std::numeric_limits<double>::quiet_NaN();
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Assess
 *  @fn double Assess(const cv::cuda::GpuMat& image,
                      const Matrix<double>& shape)
 *  @brief  Assessment method, given the current shape and image
 *  @param[in]  image   Input image
 *  @param[in]  shape     Current shape tracked by the face tracker
 *  @return   scalar score, the larger the better
 */
double ConstTrackerAssessment::Assess(const cv::cuda::GpuMat& image,
                                      const Matrix<double>& shape) {
  double score = -std::numeric_limits<double>::max();
  // Ok ?
  if (!image.empty() && !shape.empty()) {
    if (detect_) {
      score = -1.0;
    } else {
      score = 1.0;
    }
  }

  return score;
}
}  // namepsace CUDA
}  // namepsace LTS5

