/**
*  @file   cuda/face_tracker/sdm_tracker_kernel.cu
*  @brief  GPU Sdm-tracker kernel
*
*  @author Christophe Ecabert
*  @date   27/06/2016
*  Copyright (c) 2016 Christophe Ecabert. All right reserved.
*/

#include <device_launch_parameters.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/extrema.h>
#include <thrust/execution_policy.h>
#include <cublas_v2.h>
#include <assert.h>

#include "opencv2/gpu/gpu.hpp"

#include "lts5/cuda/utils/matrix.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/face_tracker/device/sdm_tracker_kernel.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {
  
#pragma mark -
#pragma mark Device kernel

/**
 *  @namespace  device
 *  @brief      CUDA Device code development space
 */
namespace device {

/*
*  @name  init_shape_kernel
*  @fn void init_shape_kernel(const double* meanshape_ptr,
                              const int N,
                              const int x,
                              const int y,
                              const int width,
                              const int height,
                              double* shape)
*  @brief  Shape initaliuation kernel
*  @param[in]  meanshape_ptr Pointer to meanshape
*  @param[in]  N             Number of points to transfrom
*  @param[in]  x             bbox X position
*  @param[in]  y             bbox Y position
*  @param[in]  width         bbox width
*  @param[in]  heigh         bbox height
*  @param[out] shape         Transform shape
*/
__global__
void init_shape_kernel(const double* meanshape_ptr,
                       const int N,
                       const int x,
                       const int y,
                       const int width,
                       const int height,
                       double* shape) {
  // Get index
  int idx = threadIdx.x;
  if (idx >= 0 && idx < N) {
    double pos = (idx < N/2) ? (double)x : (double)y;
    double scale = (idx <N/2) ? (double)width : (double)height;
    shape[idx] = pos + (meanshape_ptr[idx] * scale);
  }
}

/*
 *  @name compute_similarity_transform
 *  @fn void prepare_similarity_probem(const double* src_ptr,
                                       const int N,
                                       const double* dest_ptr,
                                       float* problem);
 *  @brief  Compute transformation between to shapes
 *  @param[in]  src_ptr   Pointer to source shape
 *  @param[in]  N         Shape size
 *  @param[in]  dest_ptr  Pointer to destination shape
 *  @param[out] problem   Similarity problem to solve, SHOULD be already
 *                        allocated
 */
__global__
void prepare_similarity_probem(const double* src_ptr,
                               const int N,
                               const double* dest_ptr,
                               float* problem) {
  // Get current index
  const int idx = threadIdx.x;
  if (idx >= 0 && idx < N) {
    __shared__ float sa[20];
    if (idx == 0) {
      for (int i = 0; i < 20; ++i) {
        sa[i] = 0.f;
      }
    }
    __syncthreads();
    // Fill
    float a_x = static_cast<float>(src_ptr[idx]);
    float a_y = static_cast<float>(src_ptr[idx + N]);
    float b_x = static_cast<float>(dest_ptr[idx]);
    float b_y = static_cast<float>(dest_ptr[idx + N]);
    //Can optimized due to symetrie
    // A col 1
    atomicAdd(&sa[0], ((a_x * a_x) + (a_y * a_y)));
    atomicAdd(&sa[2], a_x);
    atomicAdd(&sa[3], a_y);
    // A col 2
    atomicAdd(&sa[5], ((a_x * a_x) + (a_y * a_y)));
    atomicAdd(&sa[6], -a_y);
    atomicAdd(&sa[7], a_x);
    // A col 3
    atomicAdd(&sa[8], a_x);
    atomicAdd(&sa[9], -a_y);
    atomicAdd(&sa[10], 1.f);
    // A col 4
    atomicAdd(&sa[12], a_y);
    atomicAdd(&sa[13], a_x);
    atomicAdd(&sa[15], 1.f);

    // B
    atomicAdd(&sa[16], ((a_x * b_x) + (a_y * b_y)));
    atomicAdd(&sa[17], ((a_x * b_y) - (a_y * b_x)));
    atomicAdd(&sa[18], b_x);
    atomicAdd(&sa[19], b_y);

    __syncthreads();
    // Copy to output
    if (idx == 0) {
      memcpy(reinterpret_cast<void*>(problem),
             reinterpret_cast<const void*>(&sa[0]),
             20 * sizeof(float));
    }
  }
}

template<typename T>
__global__
void prepare_similarity_probem_v2(const T* src_ptr,
                                  const int N,
                                  const T* dest_ptr,
                                  T* problem) {
  unsigned int idx = threadIdx.x;
  if (idx < 20) {
    // Init
    problem[idx] = T(0.0);
    // Fill

    switch (idx) {
      // Element 0,5
      case 5:
      case 0: {
        for (int i = 0; i < N; ++i) {
          problem[idx] +=(src_ptr[i]*src_ptr[i]) + (src_ptr[i+N]*src_ptr[i+N]);
        }
      }
        break;
      // Element 1,4,11,14
      case 1:
      case 4:
      case 11:
      case 14: {
        //Nothing to do
      }
        break;
      // Element 2,7,8,13
      case 2:
      case 7:
      case 8:
      case 13: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += src_ptr[i];
        }
      }
        break;
      // Element 3,12
      case 3:
      case 12: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += src_ptr[i+N];
        }
      }
        break;
      // Element 6,9
      case 6:
      case 9: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += -src_ptr[i+N];
        }
      }
        break;
      // Element 10,15
      case 10:
      case 15: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += T(1.0);
        }
      }
        break;
      // ELement 16
      case 16: {
        for (int i = 0; i < N; ++i) {
          problem[idx] +=(src_ptr[i]*dest_ptr[i]) + (src_ptr[i+N]*dest_ptr[i+N]);
        }
      }
        break;
      // ELement 17
      case 17: {
        for (int i = 0; i < N; ++i) {
          problem[idx] +=(src_ptr[i]*dest_ptr[i+N]) - (src_ptr[i+N]*dest_ptr[i]);
        }
      }
        break;
      // ELement 18
      case 18: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += dest_ptr[i];
        }
      }
        break;
      // ELement 19
      case 19: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += dest_ptr[i+N];
        }
      }
        break;
      default:
        break;
    }
  }
}

template<>
__global__
void prepare_similarity_probem_v2(const float* src_ptr,
                                  const int N,
                                  const float* dest_ptr,
                                  float* problem) {
  unsigned int idx = threadIdx.x;
  if (idx < 20) {
    // Init
    problem[idx] = 0.f;
    // Fill

    switch (idx) {
      // Element 0,5
      case 5:
      case 0: {
        for (int i = 0; i < N; ++i) {
          problem[idx] +=(src_ptr[i]*src_ptr[i]) + (src_ptr[i+N]*src_ptr[i+N]);
        }
      }
        break;
      // Element 1,4,11,14
      case 1:
      case 4:
      case 11:
      case 14: {
        //Nothing to do
      }
        break;
      // Element 2,7,8,13
      case 2:
      case 7:
      case 8:
      case 13: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += src_ptr[i];
        }
      }
        break;
      // Element 3,12
      case 3:
      case 12: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += src_ptr[i+N];
        }
      }
        break;
      // Element 6,9
      case 6:
      case 9: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += -src_ptr[i+N];
        }
      }
        break;
      // Element 10,15
      case 10:
      case 15: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += 1.f;
        }
      }
        break;
      // ELement 16
      case 16: {
        for (int i = 0; i < N; ++i) {
          problem[idx] +=(src_ptr[i]*dest_ptr[i]) + (src_ptr[i+N]*dest_ptr[i+N]);
        }
      }
        break;
      // ELement 17
      case 17: {
        for (int i = 0; i < N; ++i) {
          problem[idx] +=(src_ptr[i]*dest_ptr[i+N]) - (src_ptr[i+N]*dest_ptr[i]);
        }
      }
        break;
      // ELement 18
      case 18: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += dest_ptr[i];
        }
      }
        break;
      // ELement 19
      case 19: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += dest_ptr[i+N];
        }
      }
        break;
      default:
        break;
    }
  }
}

template<>
__global__
void prepare_similarity_probem_v2(const double* src_ptr,
                                  const int N,
                                  const double* dest_ptr,
                                  double* problem) {
  unsigned int idx = threadIdx.x;
  if (idx < 20) {
    // Init
    problem[idx] = 0.0;
    // Fill

    switch (idx) {
      // Element 0,5
      case 5:
      case 0: {
        for (int i = 0; i < N; ++i) {
          problem[idx] +=(src_ptr[i]*src_ptr[i]) + (src_ptr[i+N]*src_ptr[i+N]);
        }
      }
        break;
      // Element 1,4,11,14
      case 1:
      case 4:
      case 11:
      case 14: {
        //Nothing to do
      }
        break;
      // Element 2,7,8,13
      case 2:
      case 7:
      case 8:
      case 13: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += src_ptr[i];
        }
      }
        break;
      // Element 3,12
      case 3:
      case 12: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += src_ptr[i+N];
        }
      }
        break;
      // Element 6,9
      case 6:
      case 9: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += -src_ptr[i+N];
        }
      }
        break;
      // Element 10,15
      case 10:
      case 15: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += 1.0;
        }
      }
        break;
      // ELement 16
      case 16: {
        for (int i = 0; i < N; ++i) {
          problem[idx] +=(src_ptr[i]*dest_ptr[i]) + (src_ptr[i+N]*dest_ptr[i+N]);
        }
      }
        break;
      // ELement 17
      case 17: {
        for (int i = 0; i < N; ++i) {
          problem[idx] +=(src_ptr[i]*dest_ptr[i+N]) - (src_ptr[i+N]*dest_ptr[i]);
        }
      }
        break;
      // ELement 18
      case 18: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += dest_ptr[i];
        }
      }
        break;
      // ELement 19
      case 19: {
        for (int i = 0; i < N; ++i) {
          problem[idx] += dest_ptr[i+N];
        }
      }
        break;
      default:
        break;
    }
  }
}

/**
 * @name  transform_shape
 * @fn  void transform_shape(const T* shape,
                             const int N,
                             const T* transform,
                             T* t_shape);
 * @brief Apply rigid transformation on a given \p shape and put the result
 *        into \p t_shape.
 * @param[in]   shape       Shape to apply transformation on  (device)
 * @param[in]   N           Shape size
 * @param[in]   transform   Transformation to apply (device)
 * @param[out]  t_shape     Transformed shape (devive)
 */
template<typename T>
__global__
void transform_shape(const T* shape,
                     const int N,
                     const T* transform,
                     T* t_shape) {
  const int idx = threadIdx.x;
  if (idx >= 0 && idx < N) {
    const T x = shape[idx];
    const T y = shape[idx + N];
    const T a = transform[0];
    const T b = transform[1];
    const T tx = transform[2];
    const T ty = transform[3];
    // Transform
    t_shape[idx] = (a * x) - (b * y) + tx;
    t_shape[idx + N] = (b * x) + (a * y) + ty;
  }
}

template<>
__global__
void transform_shape(const float* shape,
                     const int N,
                     const float* transform,
                     float* t_shape) {
  const int idx = threadIdx.x;
  if (idx >= 0 && idx < N) {
    const float x = shape[idx];
    const float y = shape[idx + N];
    const float a = transform[0];
    const float b = transform[1];
    const float tx = transform[2];
    const float ty = transform[3];
    // Transform
    t_shape[idx] = (a * x) - (b * y) + tx;
    t_shape[idx + N] = (b * x) + (a * y) + ty;
  }
}

template<>
__global__
void transform_shape(const double* shape,
                     const int N,
                     const double* transform,
                     double* t_shape) {
  const int idx = threadIdx.x;
  if (idx >= 0 && idx < N) {
    const double x = shape[idx];
    const double y = shape[idx + N];
    const double a = transform[0];
    const double b = transform[1];
    const double tx = transform[2];
    const double ty = transform[3];
    // Transform
    t_shape[idx] = (a * x) - (b * y) + tx;
    t_shape[idx + N] = (b * x) + (a * y) + ty;
  }
}

/*
 * @name  scale_shape
 * @fn  void scale_shape(const T* shape, const int N, const T scale, const T dx, const T dy, T* t_shape);
 * @brief Scale a given shape
 * @param[in] shape     shape to scale
 * @param[in] N         Number of points in the shape
 * @param[in] scale     Scale factore
 * @param[in] dx        Delta - X
 * @param[in] dy        Delta - Y
 * @param[in] t_shape   Transformed shape
 */
template<typename T>
__global__
void scale_shape(const T* shape,
                 const int N,
                 const T scale,
                 const T dx,
                 const T dy,
                 T* t_shape) {
  const int idx = threadIdx.x;
  if (idx >= 0 && idx < N) {
    const T x = shape[idx];
    const T y = shape[idx + N];
    t_shape[idx] = (x - dx) * scale + dx;
    t_shape[idx + N] = (y - dy) * scale + dy;
  }
}

template<>
__global__
void scale_shape(const float* shape,
                 const int N,
                 const float scale,
                 const float dx,
                 const float dy,
                 float* t_shape) {
  const int idx = threadIdx.x;
  if (idx >= 0 && idx < N) {
    const float x = shape[idx];
    const float y = shape[idx + N];
    t_shape[idx] = (x - dx) * scale + dx;
    t_shape[idx + N] = (y - dy) * scale + dy;
  }
}

template<>
__global__
void scale_shape(const double* shape,
                 const int N,
                 const double scale,
                 const double dx,
                 const double dy,
                 double* t_shape) {
  const int idx = threadIdx.x;
  if (idx >= 0 && idx < N) {
    const double x = shape[idx];
    const double y = shape[idx + N];
    t_shape[idx] = (x - dx) * scale + dx;
    t_shape[idx + N] = (y - dy) * scale + dy;
  }
}

/*
 * @name  convert_type
 * @fn  void convert_type(const T* input, const int N, U* output);
 * @brief Convert one array to another type
 * @param[in]   input   Array to convert
 * @param[in]   N       Number of element to convert
 * @param[out]  output  Converted data
 */
template<typename T, typename U>
__global__
void convert_type(const T* input, const int N, U* output) {
  // Thread idx
  const int idx = blockIdx.x * blockDim.x + threadIdx.x;
  if (idx >= 0 && idx < N) {
    output[idx] = static_cast<U>(input[idx]);
  }
}

/*
 * @name  set_element_value
 * @fn  void set_element_value(T* array, const int idx, const T value)
 * @brief Set ONE element in a device \p array.
 * @param[in] array   Array where to set one element
 * @param[in] idx     Index to set
 * @param[in] value   Value to set
 */
template<typename T>
__global__
void set_element_value(T* array, const int idx, const T value) {
  array[idx] = value;
}

/*
 * @name  compute_shape_cog
 * @fn  void compute_shape_cog(const T* shape, const int N, T* cog);
 * @brief Compute shape's center of gravity on device
 * @param[in]   shape
 * @param[in]   N
 * @param[out]  cog
 */
template<typename T>
__global__
void compute_shape_cog(const T* shape, const int N, T* cog) {
  extern __shared__ __align__(sizeof(T)) unsigned char s_data[];
  T* sdata = reinterpret_cast<T *>(s_data);
  // each thread loads one element from global to shared mem
  unsigned int idx = threadIdx.x;
  if (idx < N) {
    sdata[idx] = shape[idx];
    sdata[idx + N] = shape[idx + N];
    __syncthreads();
    // Accumulate in position 0
    for (int i = 1; i < N; ++i) {
      sdata[0] += sdata[i];
      sdata[N] += sdata[i + N];
    }
    // Write the result into global mem
    if (idx == 0) {
      cog[0] = sdata[0] / N;
      cog[1] = sdata[N] / N;
    }
  }
}

template<>
__global__
void compute_shape_cog(const float* shape, const int N, float* cog) {
  extern __shared__ __align__(sizeof(float)) unsigned char s_data[];
  float* sdata = reinterpret_cast<float *>(s_data);
  // each thread loads one element from global to shared mem
  unsigned int idx = threadIdx.x;
  if (idx < N) {
    sdata[idx] = shape[idx];
    sdata[idx + N] = shape[idx + N];
    __syncthreads();
    // Accumulate in position 0
    for (int i = 1; i < N; ++i) {
      sdata[0] += sdata[i];
      sdata[N] += sdata[i + N];
    }
    // Write the result into global mem
    if (idx == 0) {
      cog[0] = sdata[0] / N;
      cog[1] = sdata[N] / N;
    }
  }
}

template<>
__global__
void compute_shape_cog(const double* shape, const int N, double* cog) {
  extern __shared__ __align__(sizeof(double)) unsigned char s_data[];
  double* sdata = reinterpret_cast<double *>(s_data);
  // each thread loads one element from global to shared mem
  unsigned int idx = threadIdx.x;
  if (idx < N) {
    sdata[idx] = shape[idx];
    sdata[idx + N] = shape[idx + N];
    __syncthreads();
    // Accumulate in position 0
    for (int i = 1; i < N; ++i) {
      sdata[0] += sdata[i];
      sdata[N] += sdata[i + N];
    }
    // Write the result into global mem
    if (idx == 0) {
      cog[0] = sdata[0] / N;
      cog[1] = sdata[N] / N;
    }
  }
}

#pragma mark -
#pragma mark Kernel declaration

/** Formulate Similarity problem - Float
template
__global__
void prepare_similarity_probem_v2(const float * src_ptr,
                                  const int N,
                                  const float* dest_ptr,
                                  float* problem);*/
/** Formulate Similarity problem - Double
template
__global__
void prepare_similarity_probem_v2(const double * src_ptr,
                                  const int N,
                                  const double* dest_ptr,
                                  double* problem);*/

/** Shape transform - Float 
template
__global__
void transform_shape(const float* shape,
                     const int N,
                     const float* transform,
                     float* t_shape);*/

/** Shape transform - Double 
template
__global__
void transform_shape(const double* shape,
                     const int N,
                     const double* transform,
                     double* t_shape);*/

/** Scale shape - Float 
template
__global__
void scale_shape(const float* shape,
                 const int N,
                 const float scale,
                 const float dx,
                 const float dy,
                 float* t_shape);*/

/** Scale shape - Float 
template
__global__
void scale_shape(const double* shape,
                 const int N,
                 const double scale,
                 const double dx,
                 const double dy,
                 double* t_shape);*/

/** Shape CoG - Float 
template
__global__
void compute_shape_cog(const float* shape, const int N, float* cog);*/
/** Shape CoG - Double
template
__global__
void compute_shape_cog(const double* shape, const int N, double* cog);*/

}  // namesapce device

#pragma mark -
#pragma mark Kernel caller

/*
 *  @name       PickFaceRegion
 *  @fn         void PickFaceRegion(const cv::gpu::GpuMat& object, cv::Rect* face_region)
 *  @brief      Select proper face region by size
 *  @param[in]  object      Result of face detection
 *  @param[in]  n           Number of detection
 *  @param[out] face_region Selected face region
 */
void PickFaceRegion(const cv::gpu::GpuMat& object,
                    const int n,
                    cv::Rect* face_region) {
  // Sanity check
  assert(object.isContinuous());
  // Sort
  GPURect<int>* d_rect = (GPURect<int>*)object.ptr<GPURect<int> >(0);
  thrust::sort(thrust::device, d_rect, d_rect + n, AreaGreaterThan<int>());
  // Push back into host memory
  cudaMemcpy(reinterpret_cast<void*>(&face_region->x),
             reinterpret_cast<const void*>(&d_rect[0].x_),
             sizeof(GPURect<int>),
             cudaMemcpyDeviceToHost);
}

/*
 *  @name       PickClosestFaceRegion
 *  @fn         void PickClosestFaceRegion(const cv::gpu::GpuMat& object, const int n,
 const float cog_x, const float cog_y,
 cv::Rect* region);
 *  @brief      Select proper face region by picking the closest to CoG
 *  @param[in]  object      Result of face detection
 *  @param[in]  n           Number of detection
 *  @param[in]  cog_x       Center of Gravity, X component
 *  @param[in]  cog_y       Center of Gravity, Y component
 *  @param[out] face_region Selected face region
 */
void PickClosestFaceRegion(const cv::gpu::GpuMat& object,
                           const int n,
                           const float cog_x,
                           const float cog_y,
                           cv::Rect* region) {
  // Sanity check
  assert(object.isContinuous());
  // Pick closest
  GPURect<int>* d_rect = (GPURect<int>*)object.ptr<GPURect<int> >(0);
  GPURect<int>* it = thrust::min_element(thrust::device,
                                         d_rect,
                                         d_rect + n,
                                         CloserTo<int, float>(cog_x, cog_y));
  // Push back result
  cudaMemcpy(reinterpret_cast<void*>(&region->x),
             reinterpret_cast<const void*>(&(it->x_)),
             sizeof(GPURect<int>),
             cudaMemcpyDeviceToHost);
}

/**
 *  @name ComputeInitialShape
 *  @fn void ComputeInitialShapeKernel(const GPUMatrix<double>& meanshape,
                                       const cv::Rect& bbox,
                                       GPUMatrix<double>* init_shape);
 *  @brief  Place meanshape inside bounding box
 *  @param[in]  meanshape   Meanshape
 *  @param[in]  bbox        Region of interest
 *  @param[out] init_shape  New shape
 */
void ComputeInitialShapeKernel(const GPUMatrix<double>& meanshape,
                               const cv::Rect& bbox,
                               GPUMatrix<double>* init_shape) {
  // Init output
  init_shape->Create(meanshape.rows(), meanshape.cols());
  // Transform
  const int n = std::max(meanshape.rows(), meanshape.cols());
  device::init_shape_kernel<<<1, n>>>(meanshape.data(),
                                      n,
                                      bbox.x,
                                      bbox.y,
                                      bbox.width,
                                      bbox.height,
                                      init_shape->data());
}

/*
 * @name  ComputeSimilarityTransform
 * @fn  void ComputeSimilarityTransform(const cublasHandle_t& handle,
                                        const GPUMatrix<double>& src,
                                        const GPUMatrix<double>& dest,
                                        RTransform<float>* transform)
 * @brief Compute similarity transform between two shapes
 * @param[in]     handle      CUBLAS context
 * @param[in]     src         Source
 * @param[in]     dest        Target
 * @param[out]    transform   Transformation
 */
void ComputeSimilarityTransform(const cublasHandle_t& handle,
                                const GPUMatrix<double>& src,
                                const GPUMatrix<double>& dest,
                                RTransform<float>* transform) {
  // Compute similarity
  const int N = std::max(src.rows(), src.cols()) / 2;
  // Allocate 20 float (problem)
  float* problem;
  cudaMalloc(reinterpret_cast<void**>(&problem), 20 * sizeof(problem[0]));
  device::prepare_similarity_probem<<<1, N>>>(src.data(),
                                              N,
                                              dest.data(),
                                              problem);
  // Solve
  cublasStatus_t status;
  int info;
  float* A[2] = {&problem[0], &problem[16]};
  float** devA;
  cudaMalloc(reinterpret_cast<void**>(&devA), 2 * sizeof(float*));
  cudaMemcpy(devA, A, 2 * sizeof(float*), cudaMemcpyHostToDevice);
  status = cublasSgelsBatched(handle,       /* cublas handle */
                              CUBLAS_OP_N,  /* No transpose */
                              4,            /* m */
                              4,            /* n */
                              1,            /* nrhs */
                              (float**)&devA[0],        /* A */
                              4,            /* lda */
                              (float**)&devA[1],        /* C */
                              4,            /* ldc */
                              &info,        /* info */
                              nullptr,      /* not used */
                              1);           /* Batch size */
  if (status == CUBLAS_STATUS_SUCCESS) {
    cudaError_t error = cudaMemcpy(reinterpret_cast<void*>(transform->data),
                                   reinterpret_cast<const void*>(&(problem[16])),
                                   4 * sizeof(float),
                                   cudaMemcpyDeviceToDevice);
  } else {
    std::cout << "CUBLAS ERROR" << std::endl;
  }
  cudaFree(problem);
  cudaFree(devA);
}

void ComputeSimilarityTransform_v2(const cublasHandle_t& handle,
                                   const GPUMatrix<double>& src,
                                   const GPUMatrix<double>& dest,
                                   RTransform<double>* transform) {
  // Compute similarity
  const int N = std::max(src.rows(), src.cols()) / 2;
  // Allocate 20 float (problem)
  double* problem;
  cudaMalloc(reinterpret_cast<void**>(&problem), 20 * sizeof(problem[0]));
  device::prepare_similarity_probem_v2<<<1, 20>>>(src.data(),
                                                  N,
                                                  dest.data(),
                                                  problem);

  double prob[20];
  cudaMemcpy(reinterpret_cast<void*>(&prob[0]),
             reinterpret_cast<const void*>(problem),
             20*sizeof(double),
             cudaMemcpyDeviceToHost);
  // Solve
  cublasStatus_t status;
  int info;
  double* A[2] = {&problem[0], &problem[16]};
  double** devA;
  cudaMalloc(reinterpret_cast<void**>(&devA), 2 * sizeof(double*));
  cudaMemcpy(devA, A, 2 * sizeof(double*), cudaMemcpyHostToDevice);
  status = cublasDgelsBatched(handle,       /* cublas handle */
                              CUBLAS_OP_N,  /* No transpose */
                              4,            /* m */
                              4,            /* n */
                              1,            /* nrhs */
                              &devA[0],     /* A */
                              4,            /* lda */
                              &devA[1],     /* C */
                              4,            /* ldc */
                              &info,        /* info */
                              nullptr,      /* not used */
                              1);           /* Batch size */
  if (status == CUBLAS_STATUS_SUCCESS) {
    cudaError_t error = cudaMemcpy(reinterpret_cast<void*>(transform->data),
                                   reinterpret_cast<const void*>(&(problem[16])),
                                   4 * sizeof(double),
                                   cudaMemcpyDeviceToDevice);
  } else {
    std::cout << "CUBLAS ERROR" << std::endl;
  }
  cudaFree(problem);
  cudaFree(devA);
}

/*
 *  @name   NormalizeScaleRotation
 *  @fn void NormalizeScaleRotation(const cublasHandle_t& handle,,
                                     const GPUMatrix<double>& ref_shape,
                                     const int edge,
                                     const cv::gpu::GpuMat& image,
                                     cv::gpu::GpuMat* norm_image,
                                     GPUMatrix<double>* norm_shape,
                                     GPUMatrix<double>* transform)
 *  @brief  Normalize shape + image regarding the scale and rotation
 *  @param[in]  handle        Handle to cublas library
 *  @param[in]  ref_shape     Shape to align
 *  @param[in]  edge          Edge dimension
 *  @param[in]  image         Input image
 *  @param[out] norm_image    Normalized image used for features extraction
 *  @param[out] norm_shape    Normalized shape
 *  @param[out] inverse_transform   Transformation applied for normalization
 */
void NormalizeScaleRotation(const cublasHandle_t& handle,
                            const GPUMatrix<double>& ref_shape,
                            const int edge,
                            const cv::gpu::GpuMat& image,
                            cv::gpu::GpuMat* norm_image,
                            GPUMatrix<double>* norm_shape,
                            RTransform<double>* transform) {
  // Compute similarity
  ComputeSimilarityTransform_v2(handle, ref_shape, *norm_shape, transform);
  // Compute inverse
  cv::Mat t_mat, inverse_t;
  transform->CreateMatrix<double>(&t_mat);
  cv::invertAffineTransform(t_mat, inverse_t);
  // Warp
  if (image.data) {
    cv::gpu::warpAffine(image,
                        *norm_image,
                        inverse_t,
                        cv::Size(edge * 2, edge * 2),
                        cv::INTER_LINEAR,
                        cv::BORDER_REPLICATE);
  }
  //normalized shape
  RTransform<double> inv_transform;
  inv_transform.UploadMatrix<double>(inverse_t);
  const int N = std::max(norm_shape->rows(), norm_shape->cols()) / 2;
  device::transform_shape<<<1, N>>>(norm_shape->data(),
                                    N,
                                    inv_transform.data,
                                    norm_shape->data());
}

/**
 *  @name   NormalizeScale
 *  @fn void NormalizeScale(const cublasHandle_t& handle,
                            const GPUMatrix<double>& ref_shape,
                            const int edge,
                            const cv::gpu::GpuMat& image,
                            cv::gpu::GpuMat* norm_image,
                            GPUMatrix<double>* norm_shape,
                            RTransform<double>* transform)
 *  @brief  Normalize shape + image regarding the scale
 *  @param[in]  handle             Handle to cublas library
 *  @param[in]  ref_shape          Shape to align
 *  @param[in]  edge               Edge dimension
 *  @param[in]  image              Input image
 *  @param[out] norm_image         Normalized image used for features extraction
 *  @param[out] norm_shape         Normalized shape
 *  @param[out] inverse_transform  Transformation applied for normalization
 */
void NormalizeScale(const cublasHandle_t& handle,
                    const GPUMatrix<double>& ref_shape,
                    const int edge,
                    const cv::gpu::GpuMat& image,
                    cv::gpu::GpuMat* norm_image,
                    GPUMatrix<double>* norm_shape,
                    RTransform<double>* inverse_transform) {
  // Compute similarity transform
  RTransform<double> transform;
  ComputeSimilarityTransform_v2(handle, ref_shape, *norm_shape, &transform);
  cv::Mat t_mat, inv_t;
  transform.CreateMatrix<double>(&t_mat);
  const double* ptr = reinterpret_cast<const double*>(t_mat.data);
  double a = ptr[0];
  double b = ptr[1];
  double scale = std::sqrt((a * a) + (b * b));
  double dx = edge - image.cols * scale / 2;
  double dy = edge - image.rows * scale / 2;
  cv::Mat t = (cv::Mat_<double>(2, 3) << scale, 0, dx, 0, scale, dy);
  transform.UploadMatrix<double>(t);
  //Get inverse transform
  cv::invertAffineTransform(t, inv_t);
  inverse_transform->UploadMatrix<double>(inv_t);
  // Warp
  if (image.data) {
    cv::warpAffine(image, *norm_image,
                   t,
                   cv::Size(edge * 2, edge * 2),
                   cv::INTER_LINEAR,
                   cv::BORDER_REPLICATE);
  }
  // Normalize shape
  const int N = std::max(norm_shape->rows(), norm_shape->cols()) / 2;
  device::transform_shape<<<1, N>>>(norm_shape->data(),
                                    N,
                                    transform.data,
                                    norm_shape->data());
}

/*
 * @name  transform_shape
 * @fn  void transform_shape(const GPUMatrix<T>& shape,
                              const RTransform<T>& transform,
                              GPUMatrix<T>* t_shape);
 * @brief Apply rigid transformation on a given \p shape and put the result
 *        into \p t_shape.
 * @param[in]   shape       Shape to apply transformation on  (device)
 * @param[in]   N           Shape size
 * @param[in]   transform   Transformation to apply (device)
 * @param[out]  t_shape     Transformed shape (devive)
 */
template<typename T>
void TransformShape(const GPUMatrix<T>& shape,
                    const RTransform<T>& transform,
                    GPUMatrix<T>* t_shape) {
  const int N = std::max(shape.rows(), shape.cols()) / 2;
  device::transform_shape<<<1, N>>>(shape.data(),
                                    N,
                                    transform.data,
                                    t_shape->data());
}

/*
 * @name  ScaleShape
 * @fn  void ScaleShape(const GPUMatrix<T>& shape,
                        const T scale,
                        const T dx,
                        const T dy,
                        GPUMatrix<T>* t_shape);
 * @brief Scale a given shape
 * @param[in] shape     shape to scale
 * @param[in] scale     Scale factore
 * @param[in] dx        Delta - X
 * @param[in] dy        Delta - Y
 * @param[in] t_shape   Transformed shape
 */
template<typename T>
void ScaleShape(const GPUMatrix<T>& shape,
                const T scale,
                const T dx,
                const T dy,
                GPUMatrix<T>* t_shape) {
  const int N = std::max(shape.rows(),shape.cols()) / 2;
  device::scale_shape<<<1, N>>>(shape.data(), N, scale, dx, dy, t_shape->data());
}

/*
 * @name  ConvertType
 * @fn  void ConvertType(const T* input, const int N, U* output);
 * @brief Convert one array to another type
 * @param[in]   input   Array to convert
 * @param[in]   N       Number of element to convert
 * @param[out]  output  Converted data
 */
template<typename T, typename U>
void ConvertType(const T* input, const int N, U* output) {
  int nb_block = N + 63 / 64;
  device::convert_type<<<nb_block, 64>>>(input, N, output);
};

/*
 * @name  ComputeCoG
 * @fn  void ComputeCoG(const GPUMatrix<T>& shape, T* cog_x, T* cog_y);
 * @brief Compute shape center of gravity
 * @param[in]   shape   Shape to compute center of gravity
 * @param[out]  cog     Center of gravity - {x,y}
 */
template<typename T>
void ComputeCoG(const GPUMatrix<T>& shape, T* cog) {
  const int N = std::max(shape.rows(), shape.cols()) / 2;
  T* d_cog;
  cudaSafeCall(cudaMalloc(reinterpret_cast<void**>(&d_cog), 2 * sizeof(T)));
  device::compute_shape_cog<<<1, N, 2 * N * sizeof(T)>>>(shape.data(), N, d_cog);
  cudaSafeCall(cudaMemcpy(reinterpret_cast<void*>(cog),
                          reinterpret_cast<const void*>(d_cog),
                          2 * sizeof(T),
                          cudaMemcpyDeviceToHost));
  cudaSafeCall(cudaFree(d_cog));
}

/*
 * @name  SetElementValue
 * @fn  void SetElementValue(T* array, const int idx, const T value)
 * @brief Set ONE element in a device \p array.
 * @param[in] array   Array where to set one element
 * @param[in] idx     Index to set
 * @param[in] value   Value to set
 */
template<typename T>
void SetElementValue(T* array, const int idx, const T value) {
  device::set_element_value<<<1, 1>>>(array, idx, value);
}

#pragma mark -
#pragma mark Declaration

/** Double - Float conversion */
template void ConvertType(const double* input, const int N, float* output);
/** Float - Double conversion */
template void ConvertType(const float* input, const int N, double* output);

/** Set Element - Float */
template void SetElementValue(float* array, const int idx, const float value);
/** Set Element - Double */
template void SetElementValue(double* array, const int idx, const double value);

/* TransformShape - Float */
template void TransformShape(const GPUMatrix<float>& shape,
                             const RTransform<float>& transform,
                             GPUMatrix<float>* t_shape);
/* TransformShape - Double */
template void TransformShape(const GPUMatrix<double>& shape,
                             const RTransform<double>& transform,
                             GPUMatrix<double>* t_shape);

/** Scale - Float */
template void ScaleShape(const GPUMatrix<float>& shape,
                         const float scale,
                         const float dx,
                         const float dy,
                         GPUMatrix<float>* t_shape);
/** Scale - Double */
template void ScaleShape(const GPUMatrix<double>& shape,
                         const double scale,
                         const double dx,
                         const double dy,
                         GPUMatrix<double>* t_shape);

/** Shape Cog - Float */
template void ComputeCoG(const GPUMatrix<float>& shape,
                         float* cog);
/** Shape Cog - Double */
template void ComputeCoG(const GPUMatrix<double>& shape,
                         double* cog);


}  // namespace CUDA
}  // namespace LTS5






