/**
 *  @file   sdm_tracker.cpp
 *  @brief  GPU implementation of SDM face tracker
 *
 *  @author Christophe Ecabert
 *  @date   07/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <fstream>

#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/core/cuda_stream_accessor.hpp"

#include "lts5/cuda/face_tracker/sdm_tracker.hpp"
#include "lts5/cuda/face_tracker/svm_tracker_assessment.hpp"
#include "lts5/cuda/face_tracker/const_tracker_assessment.hpp"
#include "lts5/cuda/utils/math/cublas_wrapper.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/face_tracker/device/sdm_kernel.h"

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/utils/shape_transforms.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   SdmTracker
 *  @fn SdmTracker(void)
 *  @brief  Constructor
 */
SdmTracker::SdmTracker(void) : shape_center_gravity_(-1.f, -1.f),
                               tracking_checker_(nullptr),
                               is_tracking_(false) {

}

/*
 *  @name   ~SdmTracker
 *  @fn ~SdmTracker(void)
 *  @brief  Destructor
 */
SdmTracker::~SdmTracker(void) {
  // Clean up
  if (tracking_checker_) {
    delete tracking_checker_;
    tracking_checker_ = nullptr;
  }
}

/*
 *  @name   Load
 *  @fn     int Load(const std::string& model_filename);
 *  @brief  Load the SDM model
 *  @param[in]  model_filename      Path to the model
 *  @return -1 if errors, 0 otherwise
 */
int SdmTracker::Load(const std::string& model_filename) {
  int err = -1;
  const bool is_binary = model_filename.find(".bin") != std::string::npos;
  std::ios_base::openmode flag = (is_binary ?
                                  std::ios_base::in|std::ios_base::binary :
                                  std::ios_base::in);
  std::ifstream in_stream(model_filename.c_str(), flag);
  if (in_stream.is_open() && is_binary) {
    int status = LTS5::ScanStreamForObject(in_stream,
                                           HeaderObjectType::kSdm);
    if (status == 0) {
      err = this->Load(in_stream, is_binary);
    }
  }
  return err;
}

/*
 *  @name   Load
 *  @fn     int Load(std::istream& model_stream, const bool& is_binary)
 *  @brief  Load the SDM model
 *  @param[in]  model_stream      Stream to the model
 *  @param[in]  is_binary         Indicate if stream is open as binary
 *  @return -1 if errors, 0 otherwise
 */
int SdmTracker::Load(std::istream& model_stream, const bool& is_binary) {
  // Check if it is a binary stream or not
  int err = -1;
  if (is_binary) {
    // Reader header
    err = this->tracker_parameters_.ReadParameterHeader(model_stream);
    // Bin
    err |= LTS5::ReadMatFromBin(model_stream, &sdm_meanshape_);
    // Define number of tracked points
    num_points_ = std::max(sdm_meanshape_.rows,sdm_meanshape_.cols) / 2;
    // Init feature extraction tree, aka keypoints
    sift_extraction_location_.clear();
    sift_extraction_location_.reserve(static_cast<size_t>(num_points_));
    for (int p = 0; p < num_points_; ++p) {
      sift_extraction_location_.push_back(cv::KeyPoint());
    }
    // Load sdm
    sdm_model_.clear();
    sdm_model_.reserve(tracker_parameters_.number_of_stage);
    for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
      cv::Mat stage_model;
      err &= LTS5::ReadMatFromBin(model_stream, &stage_model);
      sdm_model_.push_back(Matrixd(stage_model));
      if (err != 0)
        break;
    }
    if (!err) {
      // Init cache
      cache_.Create(4, static_cast<size_t>(num_points_));
      // Init sift container
      int n_elem = num_points_ * sift_desciptor_params_.sift_dim + 1;
      double* buffer = nullptr;
      cudaSafeCall(cudaMalloc(reinterpret_cast<void**>(&buffer),
                              static_cast<size_t>(n_elem * sizeof(double))));
      this->sift_features_ = Matrixd(sift_desciptor_params_.sift_dim,
                                     num_points_,
                                     buffer );
      this->sift_features_wbiais_ = Matrixd(1, n_elem, buffer);
      this->sift_features_wbiais_.SetTo(1.0);

      // Tracker assessment present ?
      int status = ScanStreamForObject(model_stream,
                                       HeaderObjectType::kSvmTrackerAssessment);
      if (!status) {
        // found one, load it
        tracking_checker_ = new CUDA::SvmTrackerAssessment(sdm_meanshape_,
                                                           sift_desciptor_params_,
                                                           tracker_parameters_.face_region_width,
                                                           tracker_parameters_.sift_size,
                                                           &cache_,
                                                           stream_);
        err = tracking_checker_->Load(model_stream, is_binary);
      } else {
        // No use default
        tracking_checker_ = new LTS5::CUDA::ConstTrackerAssessment(true);
      }
    }
  }
  return err;
}

/*
 *  @name   Load
 *  @fn     int Load(const std::string& model_filename,
            const std::string& face_detector_config)
 *  @brief  Load the SDM model and Viola Jones face detector
 *  @param[in]  model_filename          Path to the model
 *  @param[in]  face_detector_config    Configuration file for
 *                                      Viola & Jones face detector
 *  @return -1 if errors, 0 otherwise
 */
int SdmTracker::Load(const std::string& model_filename,
                     const std::string& face_detector_config) {
  // Load SDM
  int err= this->Load(model_filename);
  // Init Viola-Jones detector
  face_detector_ = cv::cuda::CascadeClassifier::create(face_detector_config);
  // Sanity check
  err = !face_detector_->empty() ? err : -1;
  return err;
}

/*
 *  @name   Load
 *  @fn     int Load(std::istream& model_stream,
                     const bool& is_binary,
                     const std::string& face_detector_config)
 *  @brief  Load the SDM model and Viola Jones face detector
 *  @param[in]  model_stream          Stream to the model
 *  @param[in]  is_binary             Indicate if stream is open as binary
 *  @param[in]  face_detector_config  Configuration file for Viola & Jones
 *                                    face detector
 *  @return -1 if errors, 0 otherwise
 */
int SdmTracker::Load(std::istream& model_stream,
                     const bool& is_binary,
                     const std::string& face_detector_config) {
  int err = -1;
  // Load SDM
  if (model_stream.good()) {
    err = this->Load(model_stream, is_binary);
  }
  // Init Viola-Jones detector
  face_detector_ = cv::cuda::CascadeClassifier::create(face_detector_config);
  // Sanity check
  err |= !face_detector_->empty() ? err : -1;
  return err;
}

/*
 *  @name   Save
 *  @fn int Save(const std::string& model_name)
 *  @brief  Save the trained model in file.
 *  @param  [in]    model_name      Name of the file
 *  @return -1 in case of error, otherwise 0
 */
int SdmTracker::Save(const std::string& model_name) {
  // Check output
  std::string file, dir, ext;
  LTS5::ExtractDirectory(model_name, &dir, &file, &ext);
  std::string name = dir + file + ".bin";
  // open stream + write
  std::ofstream stream(name.c_str(), std::ios_base::binary);
  return this->Write(stream);
}

/*
 *  @name   Write
 *  @fn int Write(std::ostream& out_stream) const
 *  @brief  Write the object into a binary stream
 *  @param[in]  out_stream  Binary stream to files
 *  @return -1 if error, 0 otherwise.
 */
int SdmTracker::Write(std::ostream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Ok
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kSdm);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type), sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size), sizeof(obj_size));
    // Parameters header
    error = tracker_parameters_.WriteParameterHeader(out_stream);
    // Mean shape
    error |= LTS5::WriteMatToBin(out_stream, sdm_meanshape_);
    // Regressor
    for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
      cv::Mat stage;
      sdm_model_[i].Download(&stage);
      error |= LTS5::WriteMatToBin(out_stream, stage);
    }
    // Quality assessment
    error |= tracking_checker_->Write(out_stream);
    // Done sanity check,
    error |= out_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name   ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the memory used by object
 *  @return Size in bytes
 */
int SdmTracker::ComputeObjectSize(void) const {
  int size = 0;
  // Configuration header
  size = tracker_parameters_.ComputeHeaderSize();
  // Meanshape
  size += 3 * sizeof(int);
  size += sdm_meanshape_.total() * sdm_meanshape_.elemSize();
  // Regressor
  for (int i = 0; i < tracker_parameters_.number_of_stage; ++i) {
    // Matrix header
    size += 3 * sizeof(int);
    cv::Mat stage;
    sdm_model_[i].Download(&stage);
    size += stage.total() * stage.elemSize();
  }
  return size;
}

#pragma mark -
#pragma mark Tracking

/*
 *  @name   Detect
 *  @fn int Detect(const cv::cuda::GpuMat& image, Matrix<double>* shape)
 *  @brief  Dectection method, given a still image, provides a set
 *          of landmarks.
 *  @param[in]  image   Input image
 *  @param[out] shape   Set of landmarks
 *  @return -1 if error, 0 otherwise
 */
int SdmTracker::Detect(const cv::cuda::GpuMat& image, Matrix<double>* shape) {
  int err = -1;
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cuda::cvtColor(image, working_img_, cv::COLOR_BGR2GRAY, 0, stream_);
  } else {
    working_img_ = image;
  }
  // Downsample image to speedup computation
  float aspect_ratio = (float)working_img_.rows / (float)working_img_.cols;
  int img_height = static_cast<int>(320.f * aspect_ratio);
  static cv::cuda::GpuMat downsample_input;
  cv::cuda::resize(working_img_,
                   downsample_input,
                   cv::Size(320, img_height), 0, 0,
                   cv::INTER_LINEAR, stream_);
  float scale = static_cast<float>(working_img_.cols) / static_cast<float>(downsample_input.cols);
  // Scan image for faces
  stream_.waitForCompletion();
  static cv::cuda::GpuMat face_location;
  face_detector_->setScaleFactor(1.1);
  face_detector_->setMinNeighbors(2);
  face_detector_->setMinObjectSize(cv::Size((200.0 / scale),
                                            (200.0 / scale)));
  face_detector_->detectMultiScale(downsample_input, face_location);
  int n_face = face_location.rows;
  if (n_face > 0) {
    cv::Mat host_loc;
    face_location.colRange(0, n_face).download(host_loc);
    cv::Rect* rect = host_loc.ptr<cv::Rect>();
    // Sort by size
    std::sort(rect,
              rect + n_face,
              LTS5::SdmTracker::SortRectBySize());
    face_region_ = *rect;
    // Convert back to original image coordinate
    int x = static_cast<int>(face_region_.x * scale);
    int y = static_cast<int>(face_region_.y * scale);
    int w = static_cast<int>(face_region_.width * scale);
    int h = static_cast<int>(face_region_.height * scale);
    face_region_.x = x;
    face_region_.y = y;
    face_region_.width = w;
    face_region_.height = h;
    // Can track landmarks, create inital shape. If size is Ok does nothing
    initial_shape_.create(num_points_ * 2, 1, CV_64F);
    previous_shape_.Create(num_points_ * 2, 1);
    // Compute initial shape
    this->ComputeInitialShape(sdm_meanshape_, face_region_, &initial_shape_);
    //  Face alignment
    if (tracker_parameters_.align_with_rotation) {
      *shape = this->AlignFaceWithScaleAndRotation(working_img_);
    } else {
      *shape = this->AlignFaceWithScale(working_img_);
    }
    // Save shape as previous
    //previous_shape_ = shape->Clone();
    err = 0;
  }
  return err;
}

/*
 *  @name   Detect
 *  @fn int Detect(const cv::cuda::GpuMat& image,
                   const cv::Rect& face_region,
                   Matrix<double>* shape)
 *  @brief  Dectection method, given a still image, provides a set of
 *          landmarks.
 *  @param[in]  image           Input image
 *  @param[in]  face_region     Face boundingg box
 *  @param[out] shape           Set of landmarks
 *  @return     -1 if error, 0 otherwise
 */
int SdmTracker::Detect(const cv::cuda::GpuMat& image,
                       const cv::Rect& face_region,
                       Matrix<double>* shape) {
  int err = -1;
  // Is it already a gray scale images ? then assign to working image
  if (image.channels() != 1) {
    cv::cuda::cvtColor(image, working_img_, cv::COLOR_BGR2GRAY, 0, stream_);
  } else {
    working_img_ = image;
  }
  // Can track landmarks, create inital shape. If size is Ok does nothing
  initial_shape_.create(num_points_ * 2, 1, CV_64F);
  previous_shape_.Create(num_points_ * 2, 1);
  // Compute initial shape
  face_region_ = face_region;
  this->ComputeInitialShape(sdm_meanshape_, face_region_, &initial_shape_);
  //  Face alignment
  if (tracker_parameters_.align_with_rotation) {
    *shape = this->AlignFaceWithScaleAndRotation(working_img_);
  } else {
    *shape = this->AlignFaceWithScale(working_img_);
  }
  err = 0;
  return err;
}

/*
 *  @name   Track
 *  @fn int Track(const cv::cuda::GpuMat& image, Matrix<double>* shape)
 *  @brief  Tracking method, given an input image
 *  @param[in]      image   Input image
 *  @param[in,out]  shape   Set of landmarks
 *  @return         -1 if error, 0 otherwise
 */
int SdmTracker::Track(const cv::cuda::GpuMat& image,
                      Matrix<double>* shape) {
  LTS5_CUDA_TRACE("SdmTracker::Track()", 0);
  int err = -1;
  // Color ?
  if (image.channels() != 1) {
    cv::cuda::cvtColor(image,
                       working_img_,
                       cv::COLOR_BGR2GRAY,
                       0,
                       stream_);
  } else {
    working_img_ = image;
  }
  // Have we already detect something ?
  if (!is_tracking_) {
    // Look for face
    err = this->Detect(working_img_, shape);
    is_tracking_ = true;
  } else {
    // Tracking
    previous_shape_ = *shape;
    initial_shape_ = this->AlignMeanshapeToCurrent(previous_shape_);
    stream_.waitForCompletion();
    *shape = this->AlignFaceWithScaleAndRotation(working_img_);
    err = 0;
  }
  // Still tracking ?
  is_tracking_ = tracking_checker_->Assess(working_img_, *shape) > 0.0;
  if (!is_tracking_) {
    *shape = Matrixd();
  } else {
    // Save shape
    previous_shape_ = shape->Clone(cv::cuda::Stream::Null());
  }
  return err;
}

/*
 *  @name   Track
 *  @fn int Track(const cv::cuda::GpuMat& image,
                  const cv::Rect& face_region,
                  Matrix<double>* shape)
 *  @brief  Tracking method, given an input image
 *  @param[in]  image       Input image
 *  @param[in]  face_region Location of the head used to initialize
 *                                    tracking;
 *  @param[in,out] shape       Set of landmarks
 *  @return    -1 if error, 0 otherwise
 */
int SdmTracker::Track(const cv::cuda::GpuMat& image,
                      const cv::Rect& face_region,
                      Matrix<double>* shape) {
  int err = -1;
  // Convert to grayscale first
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_, cv::COLOR_BGR2GRAY);
  } else {
    working_img_ = image;
  }
  // Have we already detect something ?
  if (!is_tracking_) {
    // Call for detection
    err = this->Detect(working_img_, face_region, shape);
    is_tracking_ = true;
  } else {
    // Tracking
    previous_shape_ = *shape;
    initial_shape_ = this->AlignMeanshapeToCurrent(previous_shape_);
    *shape = this->AlignFaceWithScaleAndRotation(working_img_);
    err = 0;
  }
  // Still tracking ?
  is_tracking_ = tracking_checker_->Assess(working_img_, *shape) > 0.0;
  if (!is_tracking_) {
    *shape = Matrixd();
  } else {
    // Save shape
    previous_shape_ = shape->Clone(cv::cuda::Stream::Null());
  }
  return err;
}

#pragma mark -
#pragma mark Training

/*
 *  @name   Train
 *  @fn void Train(const std::string& folder)
 *  @brief  SDM training method
 *  @param[in]  folder  Location of images/annotations
 */
void SdmTracker::Train(const std::string& folder) {
}

#pragma mark -
#pragma mark Private

/*
 *  @name AlignMeanshapeToCurrent
 *  @fn cv::Mat AlignMeanshapeToCurrent(const Matrixd& current_shape)
 *  @brief  Align the meanshape on the shape recovered at previous
 *          frame
 *  @param[in]  current_shape   Shape revovered at previous frame
 *  @return Aligned shape
 */
cv::Mat SdmTracker::AlignMeanshapeToCurrent(const Matrixd& current_shape) {
  // Download
  static cv::Mat shape, trsfrm;
  current_shape.Download(&shape);
  // Compute similarity
  LTS5::ComputeSimilarityTransform(sdm_meanshape_, shape, &trsfrm);
  // Apply transformation
  return LTS5::TransformShape(trsfrm, sdm_meanshape_);
}

/*
 *  @name   AlignFaceWithScaleAndRotation
 *  @fn Matrixd AlignFaceWithScaleAndRotation(const cv::cuda::GpuMat& image)
 *  @brief  Face alignement with SDM model
 *  @param[in]  image   Input image
 *  @return Return the new shape
 */
Matrix<double> SdmTracker::AlignFaceWithScaleAndRotation(const cv::cuda::GpuMat& image){
  using LA = CUDA::LinearAlgebra<double>;
  using TType = CUDA::LinearAlgebra<double>::TransposeType;

  // Take references
  Matrixd shape_update;
  cv::cuda::GpuMat normalized_image;
  cv::Mat reference_shape, trsfrm, inv_trsfrm;
  const int number_pass = this->tracker_parameters_.number_of_pass;
  const int starting_pass = tracker_parameters_.starting_pass;
  cv::Mat normalized_shape;

  // Loop over all stage
  auto model_it = sdm_model_.cbegin();
  for (int stage = 0;
       stage < tracker_parameters_.number_of_stage;
       ++stage, ++model_it) {
    // Define reference shape
    int edge = tracker_parameters_.face_region_width;
    if (tracker_parameters_.coarse_to_fine_search &&
        stage < tracker_parameters_.number_of_coarse_stages) {
      edge /= 2;
    }
    for (int pass = starting_pass; pass < number_pass; pass++) {
      //  Normalize face
      // ----------------------------------------------
      cv::Mat normalized_shape;
      if (stage == 0) {
        shape_update.Upload(initial_shape_, stream_);
        normalized_shape = initial_shape_.clone();
      } else {
        shape_update.Download(stream_, &normalized_shape);
        stream_.waitForCompletion();
      }
      reference_shape = sdm_meanshape_ * edge + edge/2;
      ComputeSimilarityTransform(reference_shape, normalized_shape, &trsfrm);
      //Define inverse
      cv::invertAffineTransform(trsfrm, inv_trsfrm);
      cv::cuda::warpAffine(image,
                           normalized_image,
                           inv_trsfrm,
                           cv::Size(edge * 2, edge * 2),
                           cv::INTER_LINEAR,
                           cv::BORDER_REPLICATE,
                           cv::Scalar(),
                           stream_);
      //normalized shape
      CUDA::TransformShape(ShapeTransform<double>(inv_trsfrm),
                           stream_,
                           &shape_update);
      LTS5::TransformShape(inv_trsfrm, &normalized_shape);
      // Sift extraction + update key points
      // ----------------------------------------------
      double x = 0.0, y = 0.0;
      for (int p = 0; p < sift_extraction_location_.size(); ++p) {
        x = normalized_shape.at<double>(p);
        y = normalized_shape.at<double>(p + num_points_);
        auto& kp = sift_extraction_location_[p];
        kp.pt.x = cvRound(x);
        kp.pt.y = cvRound(y);
        kp.size = tracker_parameters_.sift_size;
      }
      stream_.waitForCompletion();
      GPUSSift::ComputeDescriptor(normalized_image,
                                  sift_extraction_location_,
                                  sift_desciptor_params_,
                                  &cache_,
                                  &sift_features_32f_);
      sift_features_32f_.ConvertTo(stream_, &sift_features_);
      // Shape update
      // ----------------------------------------------
      LA::Gemv(*model_it,
               TType::kNoTranspose,
               1.0,
               sift_features_wbiais_,
               1.0,
               stream_,
               &shape_update);

      if (pass == 0) {
        throw std::runtime_error("Multipass not supported yet !");
      } else {
        // Back-project shape to original coordinates
        CUDA::TransformShape(CUDA::ShapeTransform<double>(trsfrm),
                             stream_,
                             &shape_update);
      }
    }
  }
  stream_.waitForCompletion();
  return shape_update;
}

/*
 *  @name   AlignFaceWithScale
 *  @fn cv::Mat AlignFaceWithScale(const cv::cuda::GpuMat& image)
 *  @brief  Face alignement with SDM model
 *  @param[in]  image   Input image
 *  @return Return the new shape
 */
Matrix<double> SdmTracker::AlignFaceWithScale(const cv::cuda::GpuMat& image) {
  std::cout << "Not implemented" << std::endl;
  return Matrixd();
}

/*
 *  @name   ComputeInitialShape
 *  @fn void ComputeInitialShape(const cv::Mat& meanshape,
                                 const cv::Rect& bounding_box,
                                 cv::Mat* initial_shape)
 *  @brief  Aligned the meanshape inside the bounding box
 *  @param[in]  meanshape       Model meanshape
 *  @param[in]  bounding_box    Region of interest
 *  @param[out] initial_shape   Initial shape
 */
void SdmTracker::ComputeInitialShape(const cv::Mat& meanshape,
                                     const cv::Rect& bounding_box,
                                     cv::Mat* initial_shape) {
  // Init
  initial_shape->create(meanshape.rows, meanshape.cols, meanshape.type());
  // Compute shape
  const double* src = reinterpret_cast<const double*>(meanshape.data);
  double* dest = reinterpret_cast<double*>(initial_shape->data);
  for (int p = 0; p < num_points_; ++p, ++src, ++dest) {
    dest[0] = bounding_box.x + src[0] * bounding_box.width;
    dest[num_points_] = bounding_box.y + src[num_points_] * bounding_box.width;
  }
}



}  // namepsace CUDA
}  // namepsace LTS5