/**
 *  @file   test_gpu_sdm_tracker.cpp
 *  @brief  GPU Test unit for SDM
 *
 *  @author Christophe Ecabert
 *  @date   27/06/2016
 *  Copyright (c) 2016 Christophe Ecabert. All right reserved.
 */

#include <iostream>

#include "opencv2/core/cuda.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/utils/logger.hpp"

#include "lts5/cuda/face_tracker/mstream_sdm_tracker.hpp"

int main(int argc, const char * argv[]) {
  using MStreamSdmTracker = LTS5::CUDA::MStreamSdmTracker;
  using Matrixd = LTS5::CUDA::Matrix<double>;

  LTS5::CmdLineParser parser;
  parser.AddArgument("-v",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Video path");
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Tracker path");
  parser.AddArgument("-f",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face detector path");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Get option
    std::string video_path;
    std::string tracker_path;
    std::string f_detect_path;
    parser.HasArgument("-v", &video_path);
    parser.HasArgument("-t", &tracker_path);
    parser.HasArgument("-f", &f_detect_path);
    // Treat input data
    std::vector<std::string> path;
    LTS5::SplitString(video_path,";",&path);

    // Open video
    std::vector<cv::VideoCapture*> reader;
    for (int i = 0; i < path.size(); ++i) {
      // Try to open
      auto* cap = new cv::VideoCapture(path[i]);
      if (!cap->isOpened()) {
        cap->release();
        delete cap;
      } else {
        reader.push_back(cap);
      }
    }
    // Load tracker
    auto* tracker = new MStreamSdmTracker(path.size());
    err = tracker->Load(tracker_path, f_detect_path);
    auto p = tracker->get_tracker_parameters();
    tracker->set_score_threshold(-2.5);
    if (!err) {
      int n_view = static_cast<int>(path.size());
      // Create window
      std::vector<std::string> win_name;
      for (int i = 0; i < n_view; ++i) {
        win_name.push_back("Stream " + std::to_string(i));
        cv::namedWindow(win_name[i]);
      }
      // Process
      bool process = true;
      std::vector<cv::Mat> image(n_view);
      std::vector<cv::cuda::GpuMat> device_image(n_view);
      std::vector<Matrixd> shapes;
      std::vector<cv::Mat> host_shapes;

      while (process) {
        // Load image
        for (int i = 0; i < n_view; ++i) {
          reader[i]->read(image[i]);
          device_image[i].upload(image[i]);
        }
        // Track
        auto t0 = cv::getTickCount();
        tracker->Track(device_image, &shapes);
        auto t1 = cv::getTickCount();
        std::cout << "Time : " << (t1 - t0) * 1000.0 / cv::getTickFrequency() << std::endl;
        // Show
        for (int i = 0; i < n_view; ++i) {
          if (!shapes[i].empty()) {
            static cv::Mat data;
            const int n_pts = shapes[i].rows() / 2;
            shapes[i].Download(&data);
            for (int j = 0; j < n_pts; ++j) {
              cv::circle(image[i],
                         cv::Point(data.at<double>(j),
                                   data.at<double>(j + n_pts)),
                         2,
                         CV_RGB(0, 255, 0),
                         -1);
            }
          }
          cv::imshow(win_name[i], image[i]);
        }

        // Leave
        char key = static_cast<char>(cv::waitKey(5));
        if (key == 'q') {
          process = false;
        }
      }
    } else {
      LTS5_LOG_ERROR("Unable to load face tracker");
    }
    // Clean up
    delete tracker;
    tracker = nullptr;
    for (size_t v = 0; v < reader.size(); ++v) {
      reader[v]->release();
      delete reader[v];
    }
  } else {
    LTS5_LOG_ERROR("Unable to parse cmd line");
    parser.PrintHelp();
  }
  return err;
}
