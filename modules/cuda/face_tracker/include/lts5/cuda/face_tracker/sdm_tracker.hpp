/**
 *  @file   cuda/face_tracker/sdm_tracker.hpp
 *  @brief  GPU implementation of SDM face tracker
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   07/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __CUDA_SDM_TRACKER__
#define __CUDA_SDM_TRACKER__


#include "opencv2/core/cuda.hpp"
#include "opencv2/cudaobjdetect.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_tracker/base_face_tracker.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"

#include "lts5/cuda/utils/matrix.hpp"
#include "lts5/cuda/utils/ssift.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 *  @class  SdmTracker
 *  @brief  SDM-facetracker implementation
 *  @author Christophe Ecabert
 *  @date   07/06/2017
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS SdmTracker :
        public BaseFaceTracker<cv::cuda::GpuMat, Matrix<double>> {
 public:

#pragma mark -
#pragma mark Initialization
  /**
   *  @name   SdmTracker
   *  @fn SdmTracker(void)
   *  @brief  Constructor
   */
  SdmTracker(void);

  /**
   *  @name   ~SdmTracker
   *  @fn ~SdmTracker(void)
   *  @brief  Destructor
   */
  ~SdmTracker(void);

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& model_filename);
   *  @brief  Load the SDM model
   *  @param[in]  model_filename      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename);

  /**
   *  @name   Load
   *  @fn     int Load(std::istream& model_stream, const bool& is_binary)
   *  @brief  Load the SDM model
   *  @param[in]  model_stream      Stream to the model
   *  @param[in]  is_binary         Indicate if stream is open as binary
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& model_stream, const bool& is_binary);

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& model_filename,
              const std::string& face_detector_config)
   *  @brief  Load the SDM model and Viola Jones face detector
   *  @param[in]  model_filename          Path to the model
   *  @param[in]  face_detector_config    Configuration file for
   *                                      Viola & Jones face detector
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename,
           const std::string& face_detector_config);

  /**
   *  @name   Load
   *  @fn     int Load(std::istream& model_stream,
                       const bool& is_binary,
                       const std::string& face_detector_config)
   *  @brief  Load the SDM model and Viola Jones face detector
   *  @param[in]  model_stream          Stream to the model
   *  @param[in]  is_binary             Indicate if stream is open as binary
   *  @param[in]  face_detector_config  Configuration file for Viola & Jones
   *                                    face detector
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& model_stream,
           const bool& is_binary,
           const std::string& face_detector_config);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& model_name)
   *  @brief  Save the trained model in file.
   *  @param  [in]    model_name      Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  int Save(const std::string& model_name);

  /**
   *  @name   Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Write the object into a binary stream
   *  @param[in]  out_stream  Binary stream to files
   *  @return -1 if error, 0 otherwise.
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name   ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the memory used by object
   *  @return Size in bytes
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Tracking

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::cuda::GpuMat& image, Matrix<double>* shape)
   *  @brief  Dectection method, given a still image, provides a set
   *          of landmarks.
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int Detect(const cv::cuda::GpuMat& image, Matrix<double>* shape);

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::cuda::GpuMat& image,
                     const cv::Rect& face_region,
                     Matrix<double>* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face boundingg box
   *  @param[out] shape           Set of landmarks
   *  @return     -1 if error, 0 otherwise
   */
  int Detect(const cv::cuda::GpuMat& image,
             const cv::Rect& face_region,
             Matrix<double>* shape);

  /**
   *  @name   Track
   *  @fn int Track(const cv::cuda::GpuMat& image, Matrix<double>* shape)
   *  @brief  Tracking method, given an input image
   *  @param[in]      image   Input image
   *  @param[in,out]  shape   Set of landmarks
   *  @return         -1 if error, 0 otherwise
   */
  int Track(const cv::cuda::GpuMat& image, Matrix<double>* shape);

  /**
   *  @name   Track
   *  @fn int Track(const cv::cuda::GpuMat& image,
                    const cv::Rect& face_region,
                    Matrix<double>* shape)
   *  @brief  Tracking method, given an input image
   *  @param[in]  image       Input image
   *  @param[in]  face_region Location of the head used to initialize
   *                                    tracking;
   *  @param[in,out] shape       Set of landmarks
   *  @return    -1 if error, 0 otherwise
   */
  int Track(const cv::cuda::GpuMat& image,
            const cv::Rect& face_region,
            Matrix<double>* shape);

#pragma mark -
#pragma mark Training

  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder)
   *  @brief  SDM training method
   *  @param[in]  folder  Location of images/annotations
   */
  void Train(const std::string& folder);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   get_tracker_parameters
   *  @fn SdmTrackerParameters get_tracker_parameters(void) const
   *  @brief  Provide the actual configuration of the tracker
   *  @return Struct with tracker parameters
   */
  LTS5::SdmTracker::SdmTrackerParameters get_tracker_parameters(void) const {
    return tracker_parameters_;
  }

  /**
 *  @name   set_tracker_parameters
 *  @fn void set_tracker_parameters(const LTS5::SdmTracker::SdmTrackerParameters& tracker_params)
 *  @brief  Set face tracker configuration
 *  @param[in]  tracker_params  Configuration
 */
  void set_tracker_parameters(const LTS5::SdmTracker::SdmTrackerParameters& tracker_params) {
    tracker_parameters_ = tracker_params;
  }

#pragma mark -
#pragma mark Private
 private:

  /** Matrix - double type */
  using Matrixd = Matrix<double>;
  /** Matrix - double type */
  using Matrixf = Matrix<float>;

  /**
   *  @name   ComputeInitialShape
   *  @fn void ComputeInitialShape(const cv::Mat& meanshape,
                                   const cv::Rect& bounding_box,
                                   cv::Mat* initial_shape)
   *  @brief  Aligned the meanshape inside the bounding box
   *  @param[in]  meanshape       Model meanshape
   *  @param[in]  bounding_box    Region of interest
   *  @param[out] initial_shape   Initial shape
   */
  void ComputeInitialShape(const cv::Mat& meanshape,
                           const cv::Rect& bounding_box,
                           cv::Mat* initial_shape);

  /**
   *  @name   AlignFaceWithScale
   *  @fn cv::Mat AlignFaceWithScale(const cv::cuda::GpuMat& image)
   *  @brief  Face alignement with SDM model
   *  @param[in]  image   Input image
   *  @return Return the new shape
   */
  Matrixd AlignFaceWithScale(const cv::cuda::GpuMat& image);

  /**
   *  @name   AlignFaceWithScaleAndRotation
   *  @fn Matrixd AlignFaceWithScaleAndRotation(const cv::cuda::GpuMat& image)
   *  @brief  Face alignement with SDM model
   *  @param[in]  image   Input image
   *  @return Return the new shape
   */
  Matrixd AlignFaceWithScaleAndRotation(const cv::cuda::GpuMat& image);

  /**
   *  @name AlignMeanshapeToCurrent
   *  @fn cv::Mat AlignMeanshapeToCurrent(const Matrixd& current_shape)
   *  @brief  Align the meanshape on the shape recovered at previous
   *          frame
   *  @param[in]  current_shape   Shape revovered at previous frame
   *  @return Aligned shape
   */
  cv::Mat AlignMeanshapeToCurrent(const Matrixd& current_shape);

  /** Initial shape used to start alignment */
  cv::Mat initial_shape_;
  /** Shape recovered at previous iteration, used for tracking */
  Matrixd previous_shape_;
  /** SSIFT cache */
  GPUSSift::GPUCache cache_;
  /** SDM-facetracker model */
  std::vector<Matrixd> sdm_model_;
  /** Viola-Jones face detector */
  cv::Ptr<cv::cuda::CascadeClassifier> face_detector_;
  /** Stream to run computation */
  cv::cuda::Stream stream_;
  /** Region selected for the alignment */
  cv::Rect face_region_;
  /** Shape center of gravity */
  cv::Point2f shape_center_gravity_;
  /** Grayscale images where face-tracker is applied */
  cv::cuda::GpuMat working_img_;
  /** SIFT features - float */
  Matrixf sift_features_32f_;
  /** SIFT features */
  Matrixd sift_features_;
  /** SIFT features including biais term */
  Matrixd sift_features_wbiais_;
  /** Position where SIFT features will be extracted */
  std::vector<cv::KeyPoint> sift_extraction_location_;
  /** Sift descriptor parameters */
  SSift::SSiftParameters sift_desciptor_params_;
  /** Tracking quality assessment */
  LTS5::BaseFaceTrackerAssessment<cv::cuda::GpuMat, Matrixd>* tracking_checker_;
  /** Tracking indicator flag */
  bool is_tracking_;
  /** Tracker parameters */
  LTS5::SdmTracker::SdmTrackerParameters tracker_parameters_;
  /** Number of points tracked by the model */
  int num_points_;
  /** Model meanshape */
  cv::Mat sdm_meanshape_;


};
}  // namepsace CUDA
}  // namepsace LTS5

#endif //__CUDA_SDM_TRACKER__
