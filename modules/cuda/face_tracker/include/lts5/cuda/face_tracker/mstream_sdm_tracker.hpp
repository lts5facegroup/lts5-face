/**
 *  @file   cuda/face_tracker/mstream_sdm_tracker.hpp
 *  @brief  SDM Face tracker for multiple image stream
 *          One tracking model for multiple input
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   12/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __CUDA_MSTREAM_SDM_TRACKER__
#define __CUDA_MSTREAM_SDM_TRACKER__

#include <mutex>

#include "opencv2/core/cuda.hpp"
#include "opencv2/cudaobjdetect.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_tracker/mstream_face_tracker.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"

#include "lts5/cuda/utils/matrix.hpp"
#include "lts5/cuda/utils/ssift.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 *  @class  MStreamSdmTracker
 *  @brief  SDM-facetracker implementation
 *  @author Christophe Ecabert
 *  @date   10/06/17
 *  @see    https://courses.cs.washington.edu/courses/cse590v/13au/intraface.pdf
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS MStreamSdmTracker :
        public MStreamFaceTracker<cv::cuda::GpuMat, Matrix<double>> {
 public:

#pragma mark -
#pragma mark Initialization

  /** Matrix - float */
  using Matrixf = LTS5::CUDA::Matrix<float>;
  /** Matrix - double */
  using Matrixd = LTS5::CUDA::Matrix<double>;

#pragma mark -
#pragma mark Initialization

/**
   *  @name   MStreamSdmTracker
   *  @fn MStreamSdmTracker(const int n_stream)
   *  @brief  Constructor
   *  @param[in]  n_stream  Number of image to track
   */
  explicit MStreamSdmTracker(const int n_stream);

  /**
   *  @name   MStreamSdmTracker
   *  @fn MStreamSdmTracker(const MStreamSdmTracker& other) = delete
   *  @brief  Copy Constructor disable
   *  @param[in]  other   Object to copy from
   */
  MStreamSdmTracker(const MStreamSdmTracker& other) = delete;

  /**
   *  @name   operator=
   *  @fn MStreamSdmTracker& operator=(const MStreamSdmTracker& rhs) = delete
   *  @brief  Assignment operator
   *  @param[in]  rhs   Object to assign from
   *  @return Assigned face tracker
   */
  MStreamSdmTracker& operator=(const MStreamSdmTracker& rhs) = delete;

  /**
   *  @name   ~MStreamSdmTracker
   *  @fn ~MStreamSdmTracker(void)
   *  @brief  Destructor
   */
  ~MStreamSdmTracker(void);

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& model_filename);
   *  @brief  Load the SDM model
   *  @param[in]  model_filename      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename);

  /**
   *  @name   Load
   *  @fn     int Load(std::istream& model_stream, const bool& is_binary)
   *  @brief  Load the SDM model
   *  @param[in]  model_stream      Stream to the model
   *  @param[in]  is_binary         Indicate if stream is open as binary
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& model_stream, const bool& is_binary);

  /**
   *  @name   Load
   *  @fn     int Load(const std::string& model_filename,
              const std::string& face_detector_config)
   *  @brief  Load the SDM model and Viola Jones face detector
   *  @param[in]  model_filename          Path to the model
   *  @param[in]  face_detector_config    Configuration file for
   *                                      Viola & Jones face detector
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_filename,
           const std::string& face_detector_config);

  /**
   *  @name   Load
   *  @fn     int Load(std::istream& model_stream,
                       const bool& is_binary,
                       const std::string& face_detector_config)
   *  @brief  Load the SDM model and Viola Jones face detector
   *  @param[in]  model_stream          Stream to the model
   *  @param[in]  is_binary             Indicate if stream is open as binary
   *  @param[in]  face_detector_config  Configuration file for Viola & Jones
   *                                    face detector
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& model_stream,
           const bool& is_binary,
           const std::string& face_detector_config);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& model_name)
   *  @brief  Save the trained model in file.
   *  @param  [in]    model_name      Name of the file
   *  @return -1 in case of error, otherwise 0
   */
  int Save(const std::string& model_name);

  /**
   *  @name   Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Write the object into a binary stream
   *  @param[in]  out_stream  Binary stream to files
   *  @return -1 if error, 0 otherwise.
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name   ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the memory used by object
   *  @return Size in bytes
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Tracking

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::cuda::GpuMat& image, Matrixd* shape)
   *  @brief  Dectection method, given a still image, provides a set
   *          of landmarks.
   *  @param[in]  image   Input image
   *  @param[out] shape   Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::cuda::GpuMat& image, Matrixd* shape);

  /**
   *  @name   Detect
   *  @fn int Detect(const std::vector<cv::cuda::GpuMat>&images,
                     std::vector<Matrixd>* shapes)
   *  @brief  Dectection method, given a multiples image, provides a set of
   *          landmarks for each image. This must be override by subclass
   *  @param[in]  images   List of input images
   *  @param[out] shapes   List of set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const std::vector<cv::cuda::GpuMat>& images,
             std::vector<Matrixd>* shapes);

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::cuda::GpuMat& image,
                     const cv::Rect& face_region,
                     Matrixd* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face boundingg box
   *  @param[out] shape           Set of landmarks
   *  @return     >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::cuda::GpuMat& image,
             const cv::Rect& face_region,
             Matrixd* shape);

  /**
   *  @name   Detect
   *  @fn int Detect(const std::vector<cv::cuda::GpuMat>& images,
                     const std::vector<cv::Rect>& face_regions,
                     std::vector<Matrixd>* shapes)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks. This must be override by subclass
   *  @param[in]  images           List of input image
   *  @param[in]  face_regions     List of face boundingg box
   *  @param[out] shapes           List of set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const std::vector<cv::cuda::GpuMat>& images,
             const std::vector<cv::Rect>& face_regions,
             std::vector<Matrixd>* shapes);

  /**
   *  @name   Track
   *  @fn int Track(const cv::cuda::GpuMat& image, Matrixd* shape)
   *  @brief  Tracking method, given an input image
   *  @param[in]      image   Input image
   *  @param[in,out]  shape   Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Track(const cv::cuda::GpuMat& image, Matrixd* shape);

  /**
   *  @name   Track
   *  @fn int Track(const std::vector<cv::cuda::GpuMat>& images,
                    std::vector<Matrixd>* shapes)
   *  @brief  Tracking method, given an input image (from a sequence), provides
   *          a set of landmarks. This must be override by subclass
   *  @param[in]  images   List of input image
   *  @param[out] shapes   List of set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Track(const std::vector<cv::cuda::GpuMat>& images,
            std::vector<Matrixd>* shapes);

#pragma mark -
#pragma mark Training

  /**
   *  @name   Train
   *  @fn void Train(const std::string& folder)
   *  @brief  SDM training method
   *  @param[in]  folder  Location of images/annotations
   */
  void Train(const std::string& folder);

#pragma mark -
#pragma mark Accessors

/**
   *  @name   get_tracker_parameters
   *  @fn SdmTracker::SdmTrackerParameters get_tracker_parameters(void) const
   *  @brief  Provide the actual configuration of the tracker
   *  @return Struct with tracker parameters
   */
  SdmTracker::SdmTrackerParameters get_tracker_parameters(void) const {
    return tracker_parameters_;
  }

  /**
   *  @name get_score_threshold
   *  @fn double get_score_threshold(void) const
   *  @brief  Get the actual decision threshold
   *  @return Decision threshold
   */
  double get_score_threshold(void) const {
    return tracker_assessment_[0]->get_score_threshold();
  }

  /**
   *  @name   is_tracking
   *  @fn bool is_tracking(const int n_stream) const
   *  @brief  Give tracking status
   *  @param[in]  n_stream  Current stream
   *  @return true if tracking false otherwise
   */
  bool is_tracking(const int n_stream) const {
    return is_tracking_[n_stream];
  }

  /**
   *  @name   set_tracker_parameters
   *  @fn void set_tracker_parameters(const SdmTracker::SdmTrackerParameters& tracker_params)
   *  @brief  Set face tracker configuration
   *  @param[in]  tracker_params  Configuration
   */
  void set_tracker_parameters(const SdmTracker::SdmTrackerParameters& tracker_params) {
    tracker_parameters_ = tracker_params;
  }

  /**
   *  @name set_score_threshold
   *  @fn void set_score_threshold(const double threshold)
   *  @brief  Set new decision threshold
   *  @param[in]  threshold   Decision threshold
   */
  void set_score_threshold(const double threshold) {
    for (int i = 0; i < n_stream_; ++i) {
      tracker_assessment_[i]->set_score_threshold(threshold);
    }
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::cuda::GpuMat& image,const int n_stream, Matrixd* shape)
   *  @brief  Dectection method, given a still image, provides a set
   *          of landmarks.
   *  @param[in]  image     Input image
   *  @param[in]  n_stream  Current stream index
   *  @param[out] shape     Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::cuda::GpuMat& image, const int n_stream, Matrixd * shape);

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::cuda::GpuMat& image,
                     const cv::Rect& face_region,
                     const int n_stream,
                     Matrixd* shape)
   *  @brief  Dectection method, given a still image, provides a set of
   *          landmarks.
   *  @param[in]  image           Input image
   *  @param[in]  face_region     Face boundingg box
   *  @param[in]  n_stream        Current stream index
   *  @param[out] shape           Set of landmarks
   *  @return     >0 if error (bit indicate stream), 0 otherwise
   */
  int Detect(const cv::cuda::GpuMat& image,
             const cv::Rect& face_region,
             const int n_stream,
             Matrixd* shape);

  /**
   *  @name   Track
   *  @fn int Track(const cv::cuda::GpuMat& image, Matrixd* shape)
   *  @brief  Tracking method, given an input image
   *  @param[in]      image   Input image
   *  @param[in,out]  shape   Set of landmarks
   *  @return >0 if error (bit indicate stream), 0 otherwise
   */
  int TrackStream(const cv::cuda::GpuMat& image, const int n_stream, Matrixd* shape);

  /**
   *  @name   ComputeInitialShape
   *  @fn void ComputeInitialShape(const cv::Mat& meanshape,
                                   const cv::Rect& bounding_box,
                                   cv::Mat* initial_shape)
   *  @brief  Aligned the meanshape inside the bounding box
   *  @param[in]  meanshape       Model meanshape
   *  @param[in]  bounding_box    Region of interest
   *  @param[out] initial_shape   Initial shape
   */
  void ComputeInitialShape(const cv::Mat& meanshape,
                           const cv::Rect& bounding_box,
                           cv::Mat* initial_shape);

  /**
   *  @name   AlignFaceWithScale
   *  @fn Matrixd AlignFaceWithScale(const cv::cuda::GpuMat& image, const int n_stream)
   *  @brief  Face alignement with SDM model
   *  @param[in]  image     Input image
   *  @param[in]  n_stream  Current stream
   *  @return Return the new shape
   */
  Matrixd AlignFaceWithScale(const cv::cuda::GpuMat& image, const int n_stream);

  /**
   *  @name   AlignFaceWithScaleAndRotation
   *  @fn Matrixd AlignFaceWithScaleAndRotation(const cv::cuda::GpuMat& image,
                                                const int n_stream)
   *  @brief  Face alignement with SDM model
   *  @param[in]  image     Input image
   *  @param[in]  n_stream  Current stream
   *  @return Return the new shape
   */
  Matrixd AlignFaceWithScaleAndRotation(const cv::cuda::GpuMat& image,
                                        const int n_stream);

  /**
   *  @name AlignMeanshapeToCurrent
   *  @fn cv::Mat AlignMeanshapeToCurrent(const Matrixd& current_shape,
                                         const int n_stream,
                                         cv::Mat* aligned_shape)
   *  @brief  Align the meanshape on the shape recovered at previous
   *          frame
   *  @param[in]  current_shape   Shape revovered at previous frame
   *  @param[in]  n_stream  Current stream
   *  @param[in,out] aligned_shape  Transformed meanshape
   */
  void AlignMeanshapeToCurrent(const Matrixd& current_shape,
                               const int n_stream,
                               cv::Mat* aligned_shape);


  /** Grayscale images where face-tracker is applied */
  std::vector<cv::cuda::GpuMat> working_img_;
  /** Low resolution grayscale images */
  std::vector<cv::cuda::GpuMat> downsample_img_;
  /** Stream to run computation */
  std::vector<cv::cuda::Stream> stream_;
  /** SIFT features */
  std::vector<Matrixf> sift_features_32f_;
  /** SIFT features */
  std::vector<Matrixd> sift_features_;
  /** SIFT features including biais term */
  std::vector<Matrixd> sift_features_wbiais_;
  /** SSIFT cache */
  std::vector<GPUSSift::GPUCache> cache_;
  /** Shape recovered at previous iteration, used for tracking */
  std::vector<Matrixd> previous_shape_;
  /** SDM-facetracker model */
  std::vector<Matrixd> sdm_model_;
  /** Viola-Jones face detector */
  cv::Ptr<cv::cuda::CascadeClassifier> face_detector_;
  /** Detected face on GPU */
  std::vector<cv::cuda::GpuMat> face_location_;
  /** Tracking quality assessment */
  std::vector<LTS5::BaseFaceTrackerAssessment<cv::cuda::GpuMat,Matrixd>*> tracker_assessment_;
  /** Initial shape used to start alignment */
  std::vector<cv::Mat> initial_shape_;
  /** Buffer to avoid reallocation */
  std::vector<cv::Mat> current_shape_buffer_;
  /** Position where SIFT features will be extracted */
  std::vector<std::vector<cv::KeyPoint>> sift_extraction_location_;
  /** Region selected for the alignment */
  std::vector<cv::Rect> face_region_;
  /** Model meanshape */
  cv::Mat sdm_meanshape_;
  /** Sift descriptor parameters */
  SSift::SSiftParameters sift_desciptor_params_;
  /** Tracker parameters */
  SdmTracker::SdmTrackerParameters tracker_parameters_;
  /** Number of points tracked by the model */
  int num_points_;
  /** Number of tracked stream */
  int n_stream_;
  /** Tracking indicator flag */
  std::vector<bool> is_tracking_;
};

}  // namepsace CUDA
}  // namepsace LTS5

#endif //__CUDA_MSTREAM_SDM_TRACKER__
