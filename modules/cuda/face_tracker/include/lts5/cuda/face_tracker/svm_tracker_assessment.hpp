/**
 *  @file   cuda/face_tracker/svm_tracker_assessment.hpp
 *  @brief  face tracker assessment based on svm classifier
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   06/06/17
*  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __CUDA_SVM_TRACKER_ASSESSMENT__
#define __CUDA_SVM_TRACKER_ASSESSMENT__

#include <string>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/ssift.hpp"
#include "lts5/face_tracker/face_tracker_assessment.hpp"

#include "lts5/cuda/utils/matrix.hpp"
#include "lts5/cuda/utils/ssift.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/** Base class */
using GPUBaseTrackerAssessment = BaseFaceTrackerAssessment<cv::cuda::GpuMat,
        Matrix<double>>;

/**
 *  @class  SvmTrackerAssessment
 *  @brief  Face tracker quality assessment
 *  @author Christophe Ecabert
 *  @date   06/06/17
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS SvmTrackerAssessment : public GPUBaseTrackerAssessment {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   SvmTrackerAssessment
   *  @fn SvmTrackerAssessment(const cv::Mat& meanshape,
                       const LTS5::SSift::SSiftParameters& sift_parameters,
                       const int edge,
                       const int sift_size,
                       const GPUSSift::GPUCache* cache,
                       const cv::cuda::Stream& stream)
   *  @brief  Constructor
   *  @param[in] meanshape  Meanshape
   *  @param[in] sift_parameters    SSIFT extraction parameters
   *  @param[in] edge               Edge size
   *  @param[in] sift_size          Size windows dimension
   *  @param[in] cache              GPU Cache for ssift computation
   *  @param[in] stream             Stream on which to run extraction
   */
  SvmTrackerAssessment(const cv::Mat& meanshape,
                       const LTS5::SSift::SSiftParameters& sift_parameters,
                       const int edge,
                       const int sift_size,
                       const GPUSSift::GPUCache* cache,
                       const cv::cuda::Stream& stream);

  /**
   *  @name   ~SvmTrackerAssessment
   *  @fn ~SvmTrackerAssessment(void)
   *  @brief  Destructor
   */
  ~SvmTrackerAssessment(void);

  /**
   *  @name   Load
   *  @brief  Load the assessment model from file
   *  @fn int Load(const std::string& model_name)
   *  @param[in]  model_name      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_name);

  /**
   *  @name   Load
   *  @fn int Load(std::istream& input_stream, const bool& is_binary)
   *  @brief  Load the assessment model from file stream
   *  @param[in]  input_stream  File stream for reading the model
   *  @param[in]  is_binary     Indicate if stream is open has binary
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& input_stream, const bool& is_binary);

  /**
   *  @name   Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Copy the object into a binary stream
   *  @param[in]  out_stream  Output binary stream
   *  @return -1 if error, 0 otherwise
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the object size in bytes
   *  @return Object memory size
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Assess
   *  @fn double Assess(const cv::cuda::GpuMat& image,
                        const Matrix<double>& shape)
   *  @brief  Assessment method, given the current shape and image
   *  @param[in]  image   Input image
   *  @param[in]  shape     Current shape tracked by the face tracker
   *  @return   scalar score, the larger the better
   */
  double Assess(const cv::cuda::GpuMat& image, const Matrix<double>& shape);

#pragma mark -
#pragma mark Accessor

  /**
   *  @name   set_score_threshold
   *  @fn void set_score_threshold(const double threshold)
   *  @brief  Set classification threshold
   *  @param[in]  threshold Classification threshold
   */
  void set_score_threshold(const double threshold) {
    score_threshold_ = threshold;
  }

  /**
   *  @name get_score_threshold
   *  @fn virtual double get_score_threshold(void) const = 0
   *  @brief  Get the current decision threshold
   *  @return  Current decision threshold
   */
  double get_score_threshold(void) const {
    return score_threshold_;
  }

#pragma mark -
#pragma mark Private
 private :

  /**
   * @name  ExtractFeature
   * @fn    void ExtractFeature(const cv::cuda::GpuMat& image,
                                const Matrix<double>& shape)
   * @brief Extract sift feature at specific location
   * @param[in] image   Input image
   * @param[in] shape   Current position of tracked landmarks
   */
  void ExtractFeature(const cv::cuda::GpuMat& image,
                      const Matrix<double>& shape);

  /** Ssift cache */
  const LTS5::CUDA::GPUSSift::GPUCache* cache_;
  /** Stream */
  cv::cuda::Stream stream_;
  /** Normalized image */
  cv::cuda::GpuMat normalized_image_;
  /**  linear svm model for face tracker assessment */
  Matrix<double> svm_;
  /** SVM bias */
  double bias_;
  /** Decision threshold */
  double score_threshold_ = -0.3;
  /** Reference shape */
  cv::Mat reference_shape_;
  /** Image size */
  int edge_;
  /** SSift size */
  int sift_size_;
  /** SSift configuration */
  SSift::SSiftParameters sift_parameters_;
  /** Features */
  Matrix<float> features_32f_;
  /** Features */
  Matrix<double> features_;
};

} // namesapce CUDA
}  // namespace LTS5

#endif /* __CUDA_SVM_TRACKER_ASSESSMENT__ */
