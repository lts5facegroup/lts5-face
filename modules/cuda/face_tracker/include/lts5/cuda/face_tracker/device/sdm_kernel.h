/**
 *  @file   cuda/face_tracker/device/sdm_kernel.h
 *  @brief
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   09/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __CUDA_SDM_KERNEL__
#define __CUDA_SDM_KERNEL__

#include <cuda_runtime.h>

#include "opencv2/core/core.hpp"
#include "opencv2/core/cuda.hpp"

#include "lts5/utils/library_export.hpp"

#include "lts5/cuda/utils/matrix.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Type definition

/**
 * @name    ShapeTransform
 * @brief   Lightweight affine transform representation
 * @ingroup face_tracker
 */
template<typename T>
struct LTS5_EXPORTS ShapeTransform {

  /**
   * @name  ShapeTransform
   * @fn    ShapeTransform(void)
   * @brief Constructor
   */
  ShapeTransform(void);

  /**
   * @name  ShapeTransform
   * @fn    ShapeTransform(const cv::Mat& transform)
   * @brief Constructor
   * @param[in] transform   Affine transform
   */
  ShapeTransform(const cv::Mat& transform);

  /** scaled cosine */
  T a;
  /** scaled sine */
  T b;
  /** Tx */
  T tx;
  /** Ty */
  T ty;
};

#pragma mark -
#pragma mark Shape transform

/**
 * @name    TransformShape
 * @fn      void TransformShape(const ShapeTransform<T>& transform,
                                const cv::cuda::Stream& stream,
                                GPUMatrix* shape)
 * @brief   Apply affine transformation on a given \p shape. Transformation
 *          happens inplace.
 * @tparam T    Data type
 * @param[in] transform Affine transform
 * @param[in] stream    Stream on which to run computation
 * @param[in,out] shape Shape to transform
 */
template<typename T>
void TransformShape(const ShapeTransform<T>& transform,
                    const cv::cuda::Stream& stream,
                    Matrix<T>* shape);


/**
 *  @namespace  device
 *  @brief      CUDA device code dev space
 */
namespace device {

/**
 * @name    transform_shape
 * @fn      void TransformShape(const ShapeTransform<T> transform,
                                const int N,
                                T* shape)
 * @brief   Apply affine transformation on a given \p shape. Transformation
 *          happens inplace.
 * @tparam T    Data type
 * @param[in] transform Affine transform
 * @param[in] N         Number of points in the shape
 * @param[in,out] shape Shape to transform
 */
template<typename T>
__global__
void transform_shape(const ShapeTransform<T> transform, const int N, T* shape);

}  // namepsace device
}  // namepsace CUDA
}  // namepsace LTS5

#endif //__CUDA_SDM_KERNEL__
