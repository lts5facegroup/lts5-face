/**
 *  @file   cuda/face_tracker/const_tracker_assessment.hpp
 *  @brief  constant face tracker assessment
 *  @ingroup face_tracker
 *
 *  @author Christophe Ecabert
 *  @date   07/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __CUDA_CONST_TRACKER_ASSESSMENT__
#define __CUDA_CONST_TRACKER_ASSESSMENT__

#include "opencv2/core/cuda.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_tracker/face_tracker_assessment.hpp"
#include "lts5/cuda/utils/matrix.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

using GPUTrackerAssessment = BaseFaceTrackerAssessment<cv::cuda::GpuMat,
        CUDA::Matrix<double>>;

/**
 *  @class  ConstTrackerAssessment
 *  @brief  Face tracker quality assessment
 *  @author Christophe Ecabert
 *  @date   07/06/2017
 *  @ingroup face_tracker
 */
class LTS5_EXPORTS ConstTrackerAssessment : public GPUTrackerAssessment {
 public :

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   ConstTrackerAssessment
   *  @fn explicit ConstTrackerAssessment(const bool detect)
   *  @brief  Constructor
   *  @param[in] detect Flag to indicate dectection or tracking
   */
  explicit ConstTrackerAssessment(const bool detect);

  /**
   *  @name   ~ConstTrackerAssessment
   *  @fn ~ConstTrackerAssessment(void)
   *  @brief  Destructor
   */
  ~ConstTrackerAssessment(void);

  /**
   *  @name   Load
   *  @fn int Load(const std::string& model_name)
   *  @brief  Load the assessment model from file
   *  @param[in]  model_name      Path to the model
   *  @return -1 if errors, 0 otherwise
   */
  int Load(const std::string& model_name);

  /**
   *  @name   Load
   *  @fn int Load(std::istream& input_stream, const bool& is_binary)
   *  @brief  Load the assessment model from file stream
   *  @param[in]  input_stream  File stream for reading the model
   *  @param[in]  is_binary     Indicate if stream is open has binary
   *  @return -1 if errors, 0 otherwise
   */
  int Load(std::istream& input_stream, const bool& is_binary);

  /**
   *  @name   Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Copy the object into a binary stream
   *  @param[in]  out_stream  Output binary stream
   *  @return -1 if error, 0 otherwise
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the object size in bytes
   *  @return Object memory size
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Assess
   *  @fn double Assess(const cv::cuda::GpuMat& image,
                        const Matrix<double>& shape)
   *  @brief  Assessment method, given the current shape and image
   *  @param[in]  image   Input image
   *  @param[in]  shape     Current shape tracked by the face tracker
   *  @return   scalar score, the larger the better
   */
  double Assess(const cv::cuda::GpuMat& image, const Matrix<double>& shape);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name set_score_threshold
   *  @fn virtual void set_score_threshold(const double threshold)
   *  @brief  Set up a new decision threshold
   *  @param[in]  threshold   New decision threshold
   */
  void set_score_threshold(const double threshold);

  /**
   *  @name get_score_threshold
   *  @fn double get_score_threshold(void) const
   *  @brief  Get the current decision threshold
   *  @return  NaN, decision not used in constant assessment.
   */
  double get_score_threshold(void) const;

 private :
  /** State indicating whether or not it will indicate to reset tracker */
  bool detect_;
};

}  // namepsace CUDA
}  // namepsace LTS5
#endif //__CUDA_CONST_TRACKER_ASSESSMENT__
