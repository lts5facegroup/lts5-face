/**
 *  @file   lts5/cuda/geometry/octree.hpp
 *  @brief  OCTree gpu
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   2/11/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_CUDA_OCTREE__
#define __LTS5_CUDA_OCTREE__

#include <limits>

#include "lts5/utils/library_export.hpp"
#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/cuda/utils/memory.hpp"
#include "lts5/geometry/mesh.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {


/**
 * @struct Stack
 * @brief Fixed size stack
 * @ingroup geometry
 * @tparam  T Data type
 * @tparam  SIZE  Total number of element to be stored at most (i.e. capacity)
 */
template<typename T, size_t SIZE>
struct Stack {
  /** Fixed size data buffer, stack is not infinite */
  T buffer[SIZE];
  /** Number of element in the buffer */
  int head;

  /**
   * @name  Stack
   * @fn    Stack()
   * @brief Constructor
   */
  __LTS5_HOST_DEVICE__ Stack() : head(-1) {};

  /**
   * @name  Empty
   * @fn    bool Empty() const
   * @brief Indicate if stack is empty or not
   * @return    true if empty, false otherwise
   */
  __LTS5_HOST_DEVICE__
  bool Empty() const {
    return head == -1;
  }

  /**
   * @name  Clear
   * @fn    void Clear()
   * @brief Clear stack. However the underlying object are not deleted right
   *        away.
   */
  __LTS5_HOST_DEVICE__
  void Clear() {
    head = -1;
  }

  /**
   * @name  Size
   * @fn    int Size() const
   * @brief Give the current size of the stack (i.e. number of element in the
   *        buffer)
   * @return    Number of elements present in the stack
   */
  __LTS5_HOST_DEVICE__
  int Size() const {
    return head + 1;
  }

  /**
   * @name  Top
   * @fn    T& Top()
   * @brief Get the element on top of the stack
   * @return    reference to top element
   */
  __LTS5_HOST_DEVICE__
  T& Top() {
    return buffer[head];
  }

  /**
   * @name  Top
   * @fn    const T& Top() const
   * @brief Get the element on top of the stack
   * @return    const reference to top element
   */
  __LTS5_HOST_DEVICE__
  const T& Top() const {
    return buffer[head];
  }

  /**
   * @name  Pop
   * @fn    void Pop()
   * @brief Remove element from the top of the stack
   */
  __LTS5_HOST_DEVICE__
  void Pop() {
    head = head >= 0 ? head - 1 : head;
  }

  /**
   * @name  Push
   * @fn    void Push(const T& value)
   * @brief Add a new element on top of the stack. No boundary check is
   *        performed
   * @param[in] value Value to add
   */
  __LTS5_HOST_DEVICE__
  void Push(const T& value) {
    buffer[++head] = value;
    if (head >= SIZE) {
      printf("Stack overflow!\n");
    }
  }
};


/**
 * @class OCTree
 * @brief OCTree on gpu
 * @author Christophe Ecabert
 * @date  2/11/20
 * @ingroup geometry
 * @tparam T Data type
 */
template<typename T>
class OCTree {
 public:

  /**
   * @name  OCTree
   * @fn    OCTree()
   * @brief Constructor
   */
  OCTree() = default;

  /**
   * @name  Build
   * @fn    void Build(const Mesh<T, Vector3<T>>& mesh)
   * @brief Build OCTree for a given mesh
   * @param[in] mesh Mesh object to build tree for.
   */
  void Build(const Mesh<T, Vector3<T>>& mesh);

  /**
   * @name  IntersectWithSegment
   * @fn int IntersectWithSegment(const MemoryBlob& p,
                           const MemoryBlob& q,
                           const bool& cull_face,
                           const T& t_max,
                           MemoryBlob* t)
   * @brief Search for intersection between a given segment `PQ` and the tree.
   *        All segment share the SAME ending position.
   * @param[in] p       Array if starting position of segment
   * @param[in] q       Ending position of the segments
   * @param[in] cull_face If true, does not check back facing triangles
   * @param[in] t_max   Maximum segment length [0, 1.0]
   * @param[out] t     Array of intersection value. If no intersection happens
   *                    set it to -1
   * @return -1 if error, 0 otherwise
   */
  int IntersectWithSegment(const MemoryBlob& p,
                           const MemoryBlob& q,
                           const bool& cull_face,
                           const T& t_max,
                           MemoryBlob* t);

  /**
   * @name  IntersectWithSegmentOnSurface
   * @fn int IntersectWithSegmentOnSurface(const MemoryBlob& p,
                                    const int& vertex_index,
                                    const bool& cull_face,
                                    const T& t_max,
                                    MemoryBlob* t)
   * @brief Search for intersection between a given segment `PQ` and the tree.
   *        All segment share the SAME ending position and is located ON the
   *        underlying mesh surface.
   * @param[in] p   Array if starting position of segment
   * @param[in] vertex_index    Index of the vertex to use as ending point
   * @param[in] cull_face   If true, does not check back facing triangles
   * @param[in] t_max   Maximum segment length [0, 1.0]
   * @param[out] t  Array of intersection value. If no intersection happens
   *                    set it to -1
   * @return -1 if error, 0 otherwise
   */
  int IntersectWithSegmentOnSurface(const MemoryBlob& p,
                                    const int& vertex_index,
                                    const bool& cull_face,
                                    const T& t_max,
                                    MemoryBlob* t);

  /**
   * @name  CountIntersectOnSurface
   * @fn int CountIntersectOnSurface(const MemoryBlob& p,
                              const int& vertex_index,
                              const bool& cull_face,
                              const T& t_max,
                              MemoryBlob* t)
   * @brief Search for intersection between a given segment `PQ` and the tree.
   *        All segment share the SAME ending position and is located ON the
   *        underlying mesh surface. Output the total number of intersection
   * @param[in] p   Array if starting position of segment
   * @param[in] vertex_index    Index of the vertex to use as ending point
   * @param[in] cull_face   If true, does not check back facing triangles
   * @param[in] t_max   Maximum segment length [0, 1.0]
   * @param[out] t  Array where to store the count
   * @return -1 if error, 0 otherwise
   */
  int CountIntersectOnSurface(const MemoryBlob& p,
                              const int& vertex_index,
                              const bool& cull_face,
                              const T& t_max,
                              MemoryBlob* t);

  /**
   * @name  get_vertex
   * @fn    const MemoryBlob& get_vertex() const
   * @brief Access device buffer holding vertex
   * @return    Device buffer
   */
  const MemoryBlob& get_vertex() const {
    return vertex_;
  }

 protected:

  /**
   * @name  CountIntersectForAmbientOcclusion
   * @fn  int CountIntersectForAmbientOcclusion(const MemoryBlob& transform,
                                        const int& n_sample,
                                        const T& radius,
                                        const bool& cull_face,
                                        const T& t_max,
                                        MemoryBlob* t)
   * @brief Count number of intersection at a given location
   * @param[in] transform Transformation
   * @param[in] n_sample  Number of rays to shoot
   * @param[in] radius  Half-sphere radius
   * @param[in] cull_face If True cull backfacing triangle
   * @param[in] t_max
   * @param[out] t  Intersection position
   * @return  Number of interstection
   */
  int CountIntersectForAmbientOcclusion(const MemoryBlob& transform,
                                        const int& n_sample,
                                        const T& radius,
                                        const bool& cull_face,
                                        const T& t_max,
                                        MemoryBlob* t);

  /** Vertices */
  MemoryBlob vertex_;
  /** Number of vertices */
  int n_vertex_;

 private:
  /** Node */
  MemoryBlob nodes_;
  /** Triangles */
  MemoryBlob tri_;
  /** Object map */
  MemoryBlob index_map_;
  /** Random state for AO generation */
  MemoryBlob random_state_;
};

}  // namespace CUDA
}  // namespace LTS5

#endif  // __LTS5_CUDA_OCTREE__
