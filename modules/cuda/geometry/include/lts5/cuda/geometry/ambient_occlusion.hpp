/**
 *  @file   lts5/cuda/geometry/ambient_occlusion.hpp
 *  @brief  Ambient occlusion GPU implementation
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   2/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_CUDA_AMBIENT_OCCLUSION__
#define __LTS5_CUDA_AMBIENT_OCCLUSION__

#include <string>

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/cuda/geometry/octree.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 * class  AmbientOcclusionGenerator
 * @brief Estimate ambiant occlusion for a given mesh
 * @author  Christophe Ecabert
 * @date  2/12/20
 * @ingroup geometry
 * @tparam  T Data type
 */
template<typename T>
class LTS5_EXPORTS AmbientOcclusionGenerator : public OCTree<T> {
 public:
  /** Mesh Type */
  using Mesh_t = Mesh<T, Vector3<T>>;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  AmbientOcclusionGenerator
   * @fn    AmbientOcclusionGenerator(Mesh_t* mesh)
   * @brief Constructor
   * @param[in] mesh    Mesh
   */
  explicit AmbientOcclusionGenerator(Mesh_t* mesh);

  /**
   * @name  AmbientOcclusionGenerator
   * @fn AmbientOcclusionGenerator(const AmbientOcclusionGenerator& other) =
   *  delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  AmbientOcclusionGenerator(const AmbientOcclusionGenerator& other) = delete;

  /**
   * @name  operator=
   * @fn AmbientOcclusionGenerator& operator=(const AmbientOcclusionGenerator&
        rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  AmbientOcclusionGenerator&
  operator=(const AmbientOcclusionGenerator& rhs) = delete;

  /**
   * @name  ~AmbientOcclusionGenerator
   * @fn    ~AmbientOcclusionGenerator() = default
   * @brief Destructor
   */
  ~AmbientOcclusionGenerator() = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Generate
   * @fn    void Generate(const int& n_sample)
   * @brief Generate Ambient Occlusion using Monte Carlo sampling
   * @param[in] n_sample Number of sample to use to estimate AO.
   */
  void Generate(const int& n_sample);

  /**
   * @name  Save
   * @fn    int Save(const std::string& path)
   * @brief Save ambient occlusion into a file
   * @param[in] path Location where to dump the data (i.e. AO)
   * @return -1 if error, 0 otherwise
   */
  int Save(const std::string& path);

#pragma mark -
#pragma mark Accessor

  /**
   * @name  get_ao
   * @fn    const cv::Mat& get_ao() const
   * @brief Return estimated Ambient Occlusions
   * @return    AOs
   */
  const cv::Mat& get_ao() const {
    return ao_;
  }

#pragma mark -
#pragma mark Private

 private:
  /** Normal - Device */
  MemoryBlob dNormal_;
  /** Query points - Half sphere - Device */
  MemoryBlob dHalfSphere_;
  /** Per vertex transform */
  MemoryBlob dTransform_;
  /** Ambient occlusion - Device */
  MemoryBlob dAo_;
  /** Ambient occlusion */
  cv::Mat ao_;
  /** Half-sphere radius */
  T radius_;
};

}  // namespace CUDA
}  // namespace LTS5

#endif  // __LTS5_CUDA_AMBIENT_OCCLUSION__
