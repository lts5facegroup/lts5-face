/**
 *  @file   ambient_occlusion.cu
 *  @brief  Ambient occlusion GPU implementation
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   2/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <random>
#include <chrono>
#include <thread>
#include <unordered_map>

#include "cuda_profiler_api.h"

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/cuda/utils/math/types.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/geometry/ambient_occlusion.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

template<typename T>
using DVec3 = typename MathTypes<T>::Vec3;

template<typename T>
__device__ __forceinline__
Mat3x3<T> BuildRMat(const DVec3<T>& axis, const T& angle) {
  Mat3x3<T> rmat = Mat3x3<T>::Identity();
  if (angle > std::numeric_limits<T>::epsilon()) {
    // Convert to angles
    auto ct = std::cos(angle);
    auto st = std::sin(angle);
    auto c1 = T(1.0) - ct;
    auto i_ang = angle ? 1.0 / angle : 0.0;
    auto vx = axis.x * i_ang;
    auto vy = axis.y * i_ang;
    auto vz = axis.z * i_ang;
    // Define part of Rmat
    rmat.a = ct + (c1 * vx * vx);
    rmat.b = (c1 * vx * vy) - (st * vz);
    rmat.c = (c1 * vx * vz) + (st * vy);
    rmat.d = (c1 * vy * vx) + (st * vz);
    rmat.e = ct + (c1 * vy * vy);
    rmat.f = (c1 * vy * vz) - (st * vx);
    rmat.g = (c1 * vz * vx) - (st * vy);
    rmat.h = (c1 * vz * vy) + (st * vx);
    rmat.i = ct + (c1 * vz * vz);
  }
  return rmat;
}

/**
 * @name    RotateSamplingPts
 * @brief   Rotate sampling points to be aligned with tangent plane at a given
 *          location
 * @param[in] p_ptr         Array of sampling points to rotate
 * @param[in] v_ptr         Array of vertex
 * @param[in] n_ptr         Array of normal's corresponding to vertex
 * @param[in] vertex_idx    Vertex index to consider
 * @param[in] n_pts         Total number of sampling points
 * @param[out] pt_ptr   Transformed sampling points
 * @tparam T    Data type
 */
template<typename T>
__global__
void RotateSamplingPts(const DVec3<T>* p_ptr,
                       const DVec3<T>* v_ptr,
                       const DVec3<T>* n_ptr,
                       T radius,
                       int vertex_idx,
                       int n_pts,
                       DVec3<T>* pt_ptr) {
  int ix = (blockDim.x * blockIdx.x) + threadIdx.x;
  // Estimate rotation matrix
  __shared__ Mat3x3<T> rmat;
  const auto& v = v_ptr[vertex_idx];
  const auto& n = n_ptr[vertex_idx];
  if (threadIdx.x == 0) {
    // Initialize rotation matrix
    // -> Axis
    DVec3<T> axis = cross(Pack(T(0.0), T(0.0), T(1.0)), n);
    axis = normalize(axis);
    // -> Angle
    T ddot = dot(Pack(T(0.0), T(0.0), T(1.0)), n);
    ddot = ddot > T(1.0) ? T(1.0) : ddot;
    ddot = ddot < T(-1.0) ? T(-1.0) : ddot;
    T ang = std::acos(ddot);
    // Build rotation matrix
    rmat = BuildRMat(axis, ang);
  }
  __syncthreads();

  if (ix >= n_pts) return;

  // Apply rotation
  DVec3<T> pt = (rmat * p_ptr[ix]) * radius;
  pt_ptr[ix] = v + pt;
}

template<typename T>
__global__
void ComputeTransform(const DVec3<T>* n_ptr,
                      int n_normal,
                      Mat3x3<T>* trsfrm_ptr) {
  int ix = (blockDim.x * blockIdx.x) + threadIdx.x;
  if (ix >= n_normal) return;

  // compute transform
  const auto& n = n_ptr[ix];
  // -> Axis
  DVec3<T> axis = cross(Pack(T(0.0), T(0.0), T(1.0)), n);
  axis = normalize(axis);
  // -> Angle
  T ddot = dot(Pack(T(0.0), T(0.0), T(1.0)), n);
  ddot = ddot > T(1.0) ? T(1.0) : ddot;
  ddot = ddot < T(-1.0) ? T(-1.0) : ddot;
  T ang = std::acos(ddot);
  // Build rotation matrix
  Mat3x3<T> rmat = Mat3x3<T>::Identity();
  if (ang > std::numeric_limits<T>::epsilon()) {
    // Convert to angles
    auto ct = std::cos(ang);
    auto st = std::sin(ang);
    auto c1 = T(1.0) - ct;
//    auto i_ang = ang ? T(1.0) / ang : T(0.0);
    auto vx = axis.x;// * i_ang;
    auto vy = axis.y;// * i_ang;
    auto vz = axis.z;// * i_ang;
    // Define part of Rmat
    rmat.a = ct + (c1 * vx * vx);
    rmat.b = (c1 * vx * vy) - (st * vz);
    rmat.c = (c1 * vx * vz) + (st * vy);
    rmat.d = (c1 * vy * vx) + (st * vz);
    rmat.e = ct + (c1 * vy * vy);
    rmat.f = (c1 * vy * vz) - (st * vx);
    rmat.g = (c1 * vz * vx) - (st * vy);
    rmat.h = (c1 * vz * vy) + (st * vx);
    rmat.i = ct + (c1 * vz * vz);
  }
  trsfrm_ptr[ix] = rmat;
}

/**
 * @name  FinalizeAo
 * @brief finalize ambient occlusion computation
 * @param[in,out] ao_ptr    Array storing the number of intersection
 * @param[in] size          Array size
 * @param[in] n_sample      Number samples (ray in half-sphere) used to
 *                          integrate
 * @tparam T    Data type
 */
template<typename T>
__global__
void FinalizeAo(T* ao_ptr, int size, T n_sample) {
  int ix = (blockDim.x * blockIdx.x) + threadIdx.x;
  if (ix >= size) return;

  // Finalize:
  // AO = 1.0 - #intersection / #sample
  // No intersection => AO = 1.0
  // Full intersection => AO = 0.0
  ao_ptr[ix] = T(1.0) - ao_ptr[ix] / n_sample;
}


}  // namespace device

using namespace std::chrono;

/**
 * @class SphericalSampler
 * @brief Uniform sampler over half-sphere
 * @date  11/13/19
 * @ingroup   geometry
 */
template<typename T>
class HalfSphereSampler {
 public:
  /** Vec3 */
  using Vec3 = LTS5::Vector3<T>;

  /**
   * @name    HalfSphereSampler
   * @fn      HalfSphereSampler()
   * @brief   Constructor
   */
  HalfSphereSampler() : gen_(system_clock::now().time_since_epoch().count()),
                        dist_(T(0.0), T(1.0)) {
  }

  /**
   * @name    operator()
   * @fn  Vec3 operator()()
   * @brief Sample 3D pts randomly on a half sphere.
   * @return  Sampled points
   */
  Vec3 operator()() {
    Vec3 pt;
    pt.x_ = dist_(gen_);
    pt.y_ = dist_(gen_);
    pt.z_ = dist_(gen_);
    // normalize
    pt.Normalize();
    pt.z_ = std::abs(pt.z_);
    return pt;
  }

 private:
  /** Generator */
  std::mt19937 gen_;
  /** Distribution */
  std::normal_distribution<T> dist_;
};

#pragma mark -
#pragma mark Initialization

/**
 * @name  AmbientOcclusionGenerator
 * @fn    AmbientOcclusionGenerator(Mesh* mesh)
 * @brief Constructor
 * @param[in] mesh    Mesh
 */
template<typename T>
AmbientOcclusionGenerator<T>::
AmbientOcclusionGenerator(Mesh_t* mesh) {
  // Ensure normals are computed
  mesh->ComputeVertexNormal();
  // Compute sampling radius
  const auto& bbox = mesh->bbox();
  auto diff = bbox.max_ - bbox.min_;
  this->radius_ = std::max(diff.x_, std::max(diff.y_, diff.z_));
  // Build octree
  OCTree<T>::Build(*mesh);
  // Init AO buffer
  this->ao_.create(this->n_vertex_, 1,cv::DataType<T>::type);
  // Init device buffer
  dNormal_.Upload(mesh->get_normal().data(),
                  this->n_vertex_ * sizeof(Vector3<T>));
  dAo_.Create(this->n_vertex_ * sizeof(T));
}

/*
 * @name  Generate
 * @fn    void Generate(const int& n_sample)
 * @brief Generate Ambient Occlusion using Monte Carlo sampling
 * @param[in] n_sample Number of sample to use to estimate AO.
 */
template<typename T>
void AmbientOcclusionGenerator<T>::Generate(const int& n_sample) {
  using HVec3 = Vector3<T>;
  using DVec3 = typename MathTypes<T>::Vec3;

//  cudaSafeCall(cudaProfilerStart());

//  // Sample half-sphere once ... see if it works
//  HalfSphereSampler<T> sampler;
//  dHalfSphere_.Create(n_sample * sizeof(HVec3));
//  std::vector<HVec3> samples;
//  for (int i = 0; i < n_sample; ++i) {
//    samples.push_back(sampler());
//  }
//  dHalfSphere_.Upload(samples.data(), samples.size() * sizeof(HVec3));

  // Build array of transforms
  dTransform_.Create(this->n_vertex_ * sizeof(Mat3x3<T>));
  {
    // Raw ptr
    const auto* n_ptr = dNormal_.Ptr<DVec3>();
    auto* trsfrm_ptr = dTransform_.Ptr<Mat3x3<T>>();
    // Dims
    dim3 thd(1024);
    dim3 bck((this->n_vertex_ + 1023) / 1024);
    device::ComputeTransform<T><<<bck, thd>>>(n_ptr,
            this->n_vertex_,
            trsfrm_ptr);
  }

  // Compute intersection
  this->CountIntersectForAmbientOcclusion(dTransform_,
                                          n_sample,
                                          this->radius_,
                                          false,
                                          T(0.99),
                                          &dAo_);

  // Finalize AO
  {
    // Raw ptr
    T* ao_ptr = dAo_.Ptr<T>();
    // Dims
    dim3 thd(1024);
    dim3 bck((this->n_vertex_ + 1023) / 1024);
    device::FinalizeAo<T><<<bck, thd>>>(ao_ptr, this->n_vertex_, n_sample);
  }
  // Download AO
  dAo_.Download(reinterpret_cast<T*>(ao_.data));
}

/*
 * @name  Save
 * @fn    int Save(const std::string& path)
 * @brief Save ambient occlusion into a file
 * @param[in] path Location where to dump the data (i.e. AO)
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int AmbientOcclusionGenerator<T>::Save(const std::string& path) {
  return SaveMatToBin(path, this->ao_);
}


template class AmbientOcclusionGenerator<float>;
#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
template class AmbientOcclusionGenerator<double>;
#endif


}  // namespace CUDA
}  // namespace LTS5