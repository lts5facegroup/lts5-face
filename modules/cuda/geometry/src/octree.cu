/**
 *  @file   octree.cpp
 *  @brief  OCTree gpu
 *  @ingroup geometry
 *
 *  @author Christophe Ecabert
 *  @date   2/11/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <vector>
#include <stack>
#include <unordered_map>
#include <chrono>

#include "curand_kernel.h"


#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/shared_memory.hpp"
#include "lts5/cuda/geometry/octree.hpp"

#include "lts5/geometry/octree.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

template<typename T>
using Vec3 = typename MathTypes<T>::Vec3;

#pragma mark -
#pragma mark OCTree node

/**
 * @struct  Node
 * @brief   Node for OCTree, compatible with device (i.e. no internal pointer)
 * @author  Christophe Ecabert
 * @date    03/03/16
 * @ingroup geometry
 */
template< typename T>
struct LTS5_EXPORTS Node {
  using Vec3 = typename MathTypes<T>::Vec3;

  /** Node level */
  int level;
  /** Bounding volume - min */
  Vec3 min;
  /** Bounding volume - max */
  Vec3 max;
  /** First index in the map */
  size_t first;
  /** Last index in the map */
  size_t last;
  /** Child node */
  int child[8];

#pragma mark -
#pragma mark Initialization

  /**
   * @name  Node
   * @fn    Node()
   * @brief Constructor
   */
  Node() : min(Pack3(0.0)),
           max(Pack3(0.0)),
           first(0),
           last(0),
           level(-1),
           child{-1} {}

  /**
   * @name  Node
   * @fn    Node(const std::size_t first,
                       const std::size_t last,
                       const AABB<T>& bbox,
                       const int level)
   * @brief Constructor
   * @param[in] first     Entry index in the map
   * @param[in] last      Exit index in the map
   * @param[in] min       Bounding volume of the node, min
   * @param[in] max       Bounding volume of the node, max
   * @param[in] level     Node tree level (i.e. depth)
   */
  Node(size_t first,
       size_t last,
       const Vec3& min,
       const Vec3& max,
       int level) : level(level),
                    min(min),
                    max(max),
                    first(first),
                    last(last),
                    child{-1} {
  }

  /**
   * @name  IsLeaf
   * @fn      bool IsLeaf() const
   * @brief   Check if this node is a leaf
   * @return  Return true if it is a leaf, false otherwise
   */
  __LTS5_HOST_DEVICE__
  bool IsLeaf() const {
    for (int i : child) {
      if (i != -1) {
        return false;
      }
    }
    return true;
  }

  /**
   * @name  Size
   * @fn    std::size_t Size() const
   * @brief Compute the size of this node, meaning the number of objects inside
   * @return  Number of objectes inside the node
   */
  __LTS5_HOST_DEVICE__
  size_t Size() const {
    if (first == std::numeric_limits<size_t>::max()
        || last == std::numeric_limits<size_t>::max()) {
      return 0;
    } else {
      return (last - first + 1);
    }
  }
};

#pragma mark -
#pragma mark Device code

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

unsigned char __device__ node_lut[8][3] = {
        // Exit plane : YZ, XZ, XY
        {4, 2, 1},  // Node 0
        {5, 3, 8},  // Node 1
        {6, 8, 3},  // Node 2
        {7, 8, 8},  // Node 3
        {8, 6, 5},  // Node 4
        {8, 7, 8},  // Node 5
        {8, 8, 7},  // Node 6
        {8, 8, 8},  // Node 7
};

template<typename T>
struct RayTraversal {
  using uchar_t = unsigned char;
  /** Entry point */
  Vec3<T> entry;
  /** Exit point */
  Vec3<T> exit;
  /** Node index */
  int node_idx;
  /** Flag */
  uchar_t flag;

  /**
   * @name  RayTraversal
   * @fn  RayTraversal()
   * @brief Constructor
   */
  __device__
  RayTraversal() : entry(Pack3(T(0.0))),
                   exit(Pack3(T(0.0))),
                   node_idx(-1),
                   flag(0) {}

  /**
   * @name  GetFirstNode
   * @fn  static uchar GetFirstNode(const Vector3<T>& entry,
   *                                const Vector3<T>& mid)
   * @brief Determine which node is hit by the given ray.
   * @param[in]   entry   Entry point parametric value (t value)
   * @param[in]   mid     Middle point parametric value (t value)
   */
  __device__
  static uchar_t GetFirstNode(const Vec3<T>& entry, const Vec3<T>& mid) {
    // Find entry plane by looking at max(entry_x, entry_y, entry_z).
    uchar_t node = 0;
    if (entry.x > entry.y) {
      if (entry.x > entry.z) {
        // Max X -> plane YZ
        node |= mid.y < entry.x ? uchar(0x2) : uchar(0x0);
        node |= mid.z < entry.x ? uchar(0x1) : uchar(0x0);
        return node;
      }
    } else if (entry.y > entry.z) {
      // Max Y -> plane XZ
      node |= mid.x < entry.y ? uchar(0x4) : uchar(0x0);
      node |= mid.z < entry.y ? uchar(0x1) : uchar(0x0);
      return node;
    }
    // Max Z -> plane XY
    node |= mid.x < entry.z ? uchar(0x4) : uchar(0x0);
    node |= mid.y < entry.z ? uchar(0x2) : uchar(0x0);
    return node;
  }

  /**
   * @name  GetNextNode
   * @fn  static uchar GetNextNode(const uchar node_idx,const Vector3<T>& exit)
   * @brief Determine the next node hit from a given node and t value.
   * @param[in]   node_idx  Current node index [0-7]
   * @param[in]   exit    Exit point
   */
  __device__
  static uchar_t GetNextNode(const uchar_t& node_idx,
                             const Vec3<T> exit) {
    // Exit plane
    // YZ = 0
    // XZ = 1
    // XY = 2
    // Define exit plane, min tx val
    T t_min;
    uchar_t exit_plane;
    if (exit.x < exit.y) {
      // Min = tx
      t_min = exit.x;
      exit_plane = 0;
    } else {
      // Min = ty
      t_min = exit.y;
      exit_plane = 1;
    }
    // check for z
    if (exit.z < t_min) {
      exit_plane = 2;
    }
    // Query LUT
    return node_lut[node_idx][exit_plane];
  }
};

template<typename T>
__device__ __forceinline__
RayTraversal<T> InitRayTraversal(const Node<T>& root,
                                 Vec3<T>& dir,
                                 Vec3<T>& org) {
  Vec3<T> center = (root.min + root.max) * T(0.5);
  // Define symmetric problem if dir has negative component
  RayTraversal<T> helper;
  if (dir.x < T(0.0)) {
    // mirror ray around cube center - x axis
    org.x = (T(2.0) * center.x) - org.x;
    dir.x = -dir.x;
    helper.flag |= 0x4;
  }
  if (dir.y < T(0.0)) {
    // mirror ray around cube center - y axis
    org.y = (T(2.0) * center.y) - org.y;
    dir.y = -dir.y;
    helper.flag |= 0x2;
  }
  if (dir.z < T(0.0)) {
    // mirror ray around cube center - z axis
    org.z = (T(2.0) * center.z) - org.z;
    dir.z = -dir.z;
    helper.flag |= 0x1;
  }
  // Compute t value, handle parallel ray to main axis
  if (dir.x == T(0.0)) {
    helper.entry.x = ((root.min.x -org.x) < T(0.0) ?
                      -std::numeric_limits<T>::infinity() :
                      std::numeric_limits<T>::infinity());
    helper.exit.x = ((root.max.x -org.x) < T(0.0) ?
                     -std::numeric_limits<T>::infinity() :
                     std::numeric_limits<T>::infinity());
  } else {
    helper.entry.x = (root.min.x -org.x) / dir.x;
    helper.exit.x =  (root.max.x -org.x) / dir.x;
  }
  if (dir.y == T(0.0)) {
    helper.entry.y = ((root.min.y -org.y) < T(0.0) ?
                      -std::numeric_limits<T>::infinity() :
                      std::numeric_limits<T>::infinity());
    helper.exit.y = ((root.max.y -org.y) < T(0.0) ?
                     -std::numeric_limits<T>::infinity() :
                     std::numeric_limits<T>::infinity());
  } else {
    helper.entry.y = (root.min.y - org.y) / dir.y;
    helper.exit.y = (root.max.y - org.y) / dir.y;
  }
  if (dir.z == T(0.0)) {
    helper.entry.z = ((root.min.z - org.z) < T(0.0) ?
                      -std::numeric_limits<T>::infinity() :
                      std::numeric_limits<T>::infinity());
    helper.exit.z = ((root.max.z - org.z) < T(0.0) ?
                     -std::numeric_limits<T>::infinity() :
                     std::numeric_limits<T>::infinity());
  } else {
    helper.entry.z = (root.min.z -org.z) / dir.z;
    helper.exit.z = (root.max.z -org.z) / dir.z;
  }
  return helper;
}

template<typename T, int SIZE>
__device__ __forceinline__
void ProcessChildren(const Node<T>& node,
                     const Vec3<T>& org,
                     const Vec3<T>& dir,
                     const RayTraversal<T>& traversal,
                     Stack<RayTraversal<T>, SIZE>* stack) {
  // Compute middle parametric value, handle parallel ray
  Vec3<T> center = (node.min - node.max) * T(0.5);
  Vec3<T> mid;
  if (dir.x == T(0.0)) {
    mid.x = (org.x < center.x) ?
            std::numeric_limits<T>::infinity() :
            -std::numeric_limits<T>::infinity();
  } else {
    mid.x = (traversal.entry.x + traversal.exit.x) * T(0.5);
  }
  if (dir.y == T(0.0)) {
    mid.y = (org.y < center.y) ?
            std::numeric_limits<T>::infinity() :
            -std::numeric_limits<T>::infinity();
  } else {
    mid.y = (traversal.entry.y + traversal.exit.y) * T(0.5);
  }
  if (dir.z == T(0.0)) {
    mid.z = (org.z < center.z) ?
            std::numeric_limits<T>::infinity() :
            -std::numeric_limits<T>::infinity();
  } else {
    mid.z = (traversal.entry.z + traversal.exit.z) * T(0.5);
  }
  // Get first node hit
  unsigned char node_idx = RayTraversal<T>::GetFirstNode(traversal.entry,
                                                         mid);

  // Loop over children
  RayTraversal<T> next;
  do {
    switch (node_idx) {
      // child[0]
      case 0: {
        next.entry = traversal.entry;
        next.exit = mid;
        next.node_idx = node.child[traversal.flag];
        next.flag = traversal.flag;
        stack->Push(next);
        node_idx = RayTraversal<T>::GetNextNode(node_idx, next.exit);
      }
        break;
        // child[1]
      case 1: {
        next.entry.x = traversal.entry.x;
        next.entry.y = traversal.entry.y;
        next.entry.z = mid.z;
        next.exit.x = mid.x;
        next.exit.y = mid.y;
        next.exit.z = traversal.exit.z;
        next.node_idx = node.child[traversal.flag^0x01];
        next.flag = traversal.flag;
        stack->Push(next);
        node_idx = RayTraversal<T>::GetNextNode(node_idx, next.exit);
      }
        break;
        // child[2]
      case 2: {
        next.entry.x = traversal.entry.x;
        next.entry.y = mid.y;
        next.entry.z = traversal.entry.z;
        next.exit.x = mid.x;
        next.exit.y = traversal.exit.y;
        next.exit.z = mid.z;
        next.node_idx = node.child[traversal.flag^2];
        next.flag = traversal.flag;
        stack->Push(next);
        node_idx = RayTraversal<T>::GetNextNode(node_idx, next.exit);
      }
        break;
        // child[3]
      case 3: {
        next.entry.x = traversal.entry.x;
        next.entry.y = mid.y;
        next.entry.z = mid.z;
        next.exit.x = mid.x;
        next.exit.y = traversal.exit.y;
        next.exit.z = traversal.exit.z;
        next.node_idx = node.child[traversal.flag^0x03];
        next.flag = traversal.flag;
        stack->Push(next);
        node_idx = RayTraversal<T>::GetNextNode(node_idx, next.exit);
      }
        break;
        // child[4]
      case 4: {
        next.entry.x = mid.x;
        next.entry.y = traversal.entry.y;
        next.entry.z = traversal.entry.z;
        next.exit.x = traversal.exit.x;
        next.exit.y = mid.y;
        next.exit.z = mid.z;
        next.node_idx = node.child[traversal.flag^0x04];
        next.flag = traversal.flag;
        stack->Push(next);
        node_idx = RayTraversal<T>::GetNextNode(node_idx, next.exit);
      }
        break;
        // child[5]
      case 5: {
        next.entry.x = mid.x;
        next.entry.y = traversal.entry.y;
        next.entry.z = mid.z;
        next.exit.x = traversal.exit.x;
        next.exit.y = mid.y;
        next.exit.z = traversal.exit.z;
        next.node_idx = node.child[traversal.flag^0x05];
        next.flag = traversal.flag;
        stack->Push(next);
        node_idx = RayTraversal<T>::GetNextNode(node_idx, next.exit);
      }
        break;
        // child[6]
      case 6: {
        next.entry.x = mid.x;
        next.entry.y = mid.y;
        next.entry.z = traversal.entry.z;
        next.exit.x = traversal.exit.x;
        next.exit.y = traversal.exit.y;
        next.exit.z = mid.z;
        next.node_idx = node.child[traversal.flag^0x06];
        next.flag = traversal.flag;
        stack->Push(next);
        node_idx = RayTraversal<T>::GetNextNode(node_idx, next.exit);
      }
        break;
        // child[7]
      case 7: {
        next.entry.x = mid.x;
        next.entry.y = mid.y;
        next.entry.z = mid.z;
        next.exit.x = traversal.exit.x;
        next.exit.y = traversal.exit.y;
        next.exit.z = traversal.exit.z;
        next.node_idx = node.child[traversal.flag^0x07];
        next.flag = traversal.flag;
        stack->Push(next);
        node_idx = 8;
      }
        break;

      default:
        printf("Should not reach this point!\n");
    }
  } while (node_idx < 8);
}


template<typename T>
__device__
bool IntersectTriSegment(const Vec3<T>& p,
                         const Vec3<T>& q,
                         const Vec3<T>& A,
                         const Vec3<T>& B,
                         const Vec3<T>& C,
                         const bool& cull_face,
                         const T& tol,
                         T* t) {
  // Local vertex
  Vec3<T> pl = p;
  Vec3<T> ql = q;
  // Define edges
  const Vec3<T> AB = B - A;
  const Vec3<T> AC = C - A;
  Vec3<T> QP = pl - ql;
  // Compute triangle normal. Can be precalculated or cached if
  // intersecting multiple segments against the same triangle
  const Vec3<T> n = cross(AB, AC);
  // Compute denominator d. If d <= 0, segment is parallel to or points
  // away from triangle, so exit early
  T d = dot(QP, n);
  bool revert = false;
  if (d < T(0.0) && !cull_face) {
    // Revert segment to make it face triangle
    // std::swap(pl, ql);
    Vec3<T> tmp = pl;
    pl = ql;
    ql = tmp;
    QP = pl - ql;
    d *= T(-1.0);
    revert = true;
  }
  if (d <= T(0.0)) { return false; }
  // Compute intersection t value of pq with plane of triangle. A ray
  // intersects iff 0 <= t. Segment intersects iff 0 <= t <= 1. Delay
  // dividing by d until intersection has been found to pierce triangle
  const Vec3<T> AP = pl - A;
  T tt = dot(AP, n);
  if (tt < T(0.0)) { return false; }
  if (tt > d) { return false;}  // For segment; exclude this code line for a ray
  // Compute barycentric coordinate components and test if within bounds
  const Vec3<T> E = cross(QP, AP);
  T v = dot(AC, E);
  if (v < -tol || (v - tol) > d) { return false; }
  T w = -dot(AB, E);
  if (w < -tol || (v + w - tol) > d) { return false; }
  *t = revert ? T(1.0) - tt/d : tt/d;
  // Reach intersection
  return true;
}


/**
 * @name    InterSegmentKernel
 * @brief   Check intersection with segment and tree
 * @param[in] p_ptr         Array of starting position
 * @param[in] q_ptr         Array of ending position
 * @param[in] tree_ptr      Array of octree node
 * @param[in] n_query       Number of query position
 * @param[in] cull_face     Whether or not to cull back facing triangle
 * @param[in] t_max         Maximum segment length to consider
 * @param[in] t_ptr         Point of intersection along the segment [0, 1]
 * @tparam T    Data type
 * @tparam SIZE Stack size
 */
template<typename T, int SIZE>
__global__
void InterSegmentKernel(const Vec3<T>* p_ptr,
                        const Vec3<T>* q_ptr,
                        const Node<T>* tree_ptr,
                        const Vec3<T>* v_ptr,
                        const Vec3<int>* tri_ptr,
                        const int* tri_map_ptr,
                        int n_query,
                        int end_p,
                        bool cull_face,
                        T t_max,
                        T* t_ptr) {
  // Get index
  int ix = (blockDim.x * blockIdx.x) + threadIdx.x;
  if (ix >= n_query) return;

  t_ptr[ix] = T(-1.0);
  // Put origin at bbox min
  const auto& root = tree_ptr[0];
  const auto& q = q_ptr[end_p];
  const auto& p = p_ptr[ix];
  // Init search, if need apply flip in order to have positive ray direction.
  Vec3<T> org = p;
  Vec3<T> ray_dir = normalize(q - org);
  RayTraversal<T> helper = InitRayTraversal<T>(root, ray_dir, org);
  // Check if ray intersect with root node
  T max_entry = max(helper.entry.x, max(helper.entry.y, helper.entry.z));
  T min_exit = max(helper.exit.x, max(helper.exit.y, helper.exit.z));
  if (max_entry < min_exit) {
    // Process tree
    helper.node_idx = 0;  // Root node
    Stack<RayTraversal<T>, SIZE> stack;
    stack.Push(helper);
    while (!stack.Empty()) {
      RayTraversal<T> traversal = stack.Top();
      stack.Pop();
      // No Children present or invalid exit point ?
      if (traversal.node_idx == -1 ||
          traversal.exit.x < T(0.0) ||
          traversal.exit.y < T(0.0) ||
          traversal.exit.z < T(0.0)) {
        continue;
      }
      // Reach leaf ?
      const auto& node = tree_ptr[traversal.node_idx];
      if (node.IsLeaf()) {
        // Check if intersect with triangles
        const auto tol = T(2.0) * std::numeric_limits<T>::epsilon();
        T t_inter = T(-1.0);
        for (size_t i = node.first; i <= node.last; ++i) {
          const auto& tri = tri_ptr[tri_map_ptr[i]];
          const auto& A = v_ptr[tri.x];
          const auto& B = v_ptr[tri.y];
          const auto& C = v_ptr[tri.z];
          bool inter = IntersectTriSegment<T>(p, q,
                                              A, B, C,
                                              cull_face,
                                              tol,
                                              &t_inter);
          if (inter && (t_inter < t_max)) {
            stack.Clear();
            break;
          }
        }
        t_ptr[ix] = t_inter;
        continue;
      }
      // Process childrens
      ProcessChildren<T, SIZE>(node, org, ray_dir, traversal, &stack);
    }
  }
}

template<typename T, int SIZE>
__global__
void CountInterSegmentOnSurfaceKernel(const Vec3<T>* p_ptr,
                                      const Vec3<T>* q_ptr,
                                      const Node<T>* tree_ptr,
                                      const Vec3<T>* v_ptr,
                                      const Vec3<int>* tri_ptr,
                                      const int* tri_map_ptr,
                                      int n_query,
                                      int end_p,
                                      bool cull_face,
                                      T t_max,
                                      T* t_ptr) {

  // Get index
  SharedMemory<T> shared_cnt;
  T* inter_cnt = shared_cnt.getPointer();
  int ix = (blockDim.x * blockIdx.x) + threadIdx.x;
  inter_cnt[threadIdx.x] = T(0.0);
  if (ix >= n_query) return;

  // Put origin at bbox min
  const auto& root = tree_ptr[0];
  const auto& q = q_ptr[end_p];
  const auto& p = p_ptr[ix];
  // Init search, if need apply flip in order to have positive ray direction.
  Vec3<T> org = p;
  Vec3<T> ray_dir = normalize(q - org);
  RayTraversal<T> helper = InitRayTraversal<T>(root, ray_dir, org);
  // Check if ray intersect with root node
  T max_entry = max(helper.entry.x, max(helper.entry.y, helper.entry.z));
  T min_exit = max(helper.exit.x, max(helper.exit.y, helper.exit.z));
  if (max_entry < min_exit) {
    // Process tree
    helper.node_idx = 0;  // Root node
    Stack<RayTraversal<T>, SIZE> stack;
    stack.Push(helper);
    while (!stack.Empty()) {
      RayTraversal<T> traversal = stack.Top();
      stack.Pop();
      // No Children present or invalid exit point ?
      if (traversal.node_idx == -1 ||
          traversal.exit.x < T(0.0) ||
          traversal.exit.y < T(0.0) ||
          traversal.exit.z < T(0.0)) {
        continue;
      }
      // Reach leaf ?
      const auto& node = tree_ptr[traversal.node_idx];
      if (node.IsLeaf()) {
        // Check if intersect with triangles
        const auto tol = T(2.0) * std::numeric_limits<T>::epsilon();
        T t_inter = T(-1.0);
        for (size_t i = node.first; i <= node.last; ++i) {
          const auto& tri = tri_ptr[tri_map_ptr[i]];
          const auto& A = v_ptr[tri.x];
          const auto& B = v_ptr[tri.y];
          const auto& C = v_ptr[tri.z];
          bool inter = IntersectTriSegment<T>(p, q,
                                              A, B, C,
                                              cull_face,
                                              tol,
                                              &t_inter);
          if (inter && (t_inter < t_max)) {
            inter_cnt[threadIdx.x] = T(1.0);
            stack.Clear();
            break;
          }
        }
        continue;
      }
      // Process childrens
      ProcessChildren<T, SIZE>(node, org, ray_dir, traversal, &stack);
    }
  }

  __syncthreads();

  // Do reduction in shared memory
  for (unsigned int s = blockDim.x / 2; s > 0; s >>=1) {
    if (threadIdx.x < s) {
      inter_cnt[threadIdx.x] += inter_cnt[threadIdx.x + s];
    }
    __syncthreads();
  }

  if (threadIdx.x == 0) {
    atomicAdd(&t_ptr[0], inter_cnt[0]);
  }
}

template<typename T, int SIZE>
__global__
void CountInterSegmentOnSurfaceKernel(const Vec3<T>* p_ptr,
                                      const Node<T>* tree_ptr,
                                      const Vec3<T>* v_ptr,
                                      const Vec3<int>* tri_ptr,
                                      const int* tri_map_ptr,
                                      int n_query,
                                      int n_vertex,
                                      bool cull_face,
                                      T t_max,
                                      T* t_ptr) {

  // Get index
  SharedMemory<T> shared_cnt;
  T* inter_cnt = shared_cnt.getPointer();
  int tidx = threadIdx.x;
  int tidy = threadIdx.y;
  int bdx = blockDim.x;
  int ix = (bdx * blockIdx.x) + tidx;
  int iy = (blockDim.y * blockIdx.y) + tidy;

  inter_cnt[(tidy * bdx) + tidx] = T(0.0);
  if (ix >= n_query || iy >= n_vertex) return;

  // Put origin at bbox min
  const auto& root = tree_ptr[0];
  const auto& q = v_ptr[iy];
  const auto& p = p_ptr[ix];

  // Init search, if need apply flip in order to have positive ray direction.
  Vec3<T> org = p;
  Vec3<T> ray_dir = normalize(q - org);
  RayTraversal<T> helper = InitRayTraversal<T>(root, ray_dir, org);
  // Check if ray intersect with root node
  T max_entry = max(helper.entry.x, max(helper.entry.y, helper.entry.z));
  T min_exit = max(helper.exit.x, max(helper.exit.y, helper.exit.z));
  if (max_entry < min_exit) {
    // Process tree
    helper.node_idx = 0;  // Root node
    Stack<RayTraversal<T>, SIZE> stack;
    stack.Push(helper);
    while (!stack.Empty()) {
      RayTraversal<T> traversal = stack.Top();
      stack.Pop();
      // No Children present or invalid exit point ?
      if (traversal.node_idx == -1 ||
          traversal.exit.x < T(0.0) ||
          traversal.exit.y < T(0.0) ||
          traversal.exit.z < T(0.0)) {
        continue;
      }
      // Reach leaf ?
      const auto& node = tree_ptr[traversal.node_idx];
      if (node.IsLeaf()) {
        // Check if intersect with triangles
        const auto tol = T(2.0) * std::numeric_limits<T>::epsilon();
        T t_inter = T(-1.0);
        for (size_t i = node.first; i <= node.last; ++i) {
          const auto& tri = tri_ptr[tri_map_ptr[i]];
          const auto& A = v_ptr[tri.x];
          const auto& B = v_ptr[tri.y];
          const auto& C = v_ptr[tri.z];
          bool inter = IntersectTriSegment<T>(p, q,
                                              A, B, C,
                                              cull_face,
                                              tol,
                                              &t_inter);
          if (inter && (t_inter < t_max)) {
            inter_cnt[(tidy * bdx) + tidx] = T(1.0);
            stack.Clear();
            break;
          }
        }
        continue;
      }
      // Process childrens
      ProcessChildren<T, SIZE>(node, org, ray_dir, traversal, &stack);
    }
  }
  // Wait till all threads of the block have filled the shared memory
  __syncthreads();

  // Do reduction in shared memory
  for (unsigned int s = blockDim.x / 2; s > 0; s >>=1) {
    if (tidx < s) {
      inter_cnt[(tidy * bdx) + tidx] += inter_cnt[(tidy * bdx) + tidx + s];
    }
    __syncthreads();
  }

  // Add everything into global memory
  if (tidx == 0) {
    atomicAdd(&t_ptr[iy], inter_cnt[tidy * bdx]);
  }
}

template<typename T>
__device__
Vec3<T> SampleHalfSphere(curandState& state);

template<>
Vec3<float> SampleHalfSphere<float>(curandState& state) {
  Vec3<float> v;
  v.x = curand_normal(&state);
  v.y = curand_normal(&state);
  v.z = curand_normal(&state);
  v = normalize(v);
  v.z = std::abs(v.z);
  return v;
}
template<>
Vec3<double> SampleHalfSphere<double>(curandState& state) {
  Vec3<double> v;
  v.x = curand_normal_double(&state);
  v.y = curand_normal_double(&state);
  v.z = curand_normal_double(&state);
  v = normalize(v);
  v.z = std::abs(v.z);
  return v;
}


template<typename T, int SIZE, int By>
__global__
void CountInterOnSurface(const Mat3x3<T>* trsfrm_ptr,
                         const Node<T>* tree_ptr,
                         const Vec3<T>* v_ptr,
                         const Vec3<int>* tri_ptr,
                         const int* tri_map_ptr,
                         curandState* prnd_state,
                         T radius,
                         int n_query,
                         int n_vertex,
                         bool cull_face,
                         T t_max,
                         T* t_ptr) {
  // Get index
  __shared__ T inter_cnt[By * By];
  __shared__ Mat3x3<T> trsfrm[By];
  int tidx = threadIdx.x;
  int tidy = threadIdx.y;
  int bdx = blockDim.x;
  int ix = (bdx * blockIdx.x) + tidx;
  int iy = (blockDim.y * blockIdx.y) + tidy;

  inter_cnt[(tidy * bdx) + tidx] = T(0.0);

  // Cache transform
  if (tidx == 0) {
    trsfrm[tidy] = trsfrm_ptr[iy];
  }
  __syncthreads();

  if (ix >= n_query || iy >= n_vertex) return;
  // Put origin at bbox min
  const auto& root = tree_ptr[0];
  auto q = v_ptr[iy];

  // Copy state to local memory for efficiency
  curandState state = prnd_state[(tidy * By) + tidx];
  auto p = SampleHalfSphere<T>(state);
  // Copy state back to global memory
  prnd_state[(tidy * By) + tidx] = state;

  // Apply transform
  p = q + ((trsfrm[tidy] * p) * radius);

  // Init search, if need apply flip in order to have positive ray direction.
  Vec3<T> org = p;
  Vec3<T> ray_dir = normalize(q - org);
  RayTraversal<T> helper = InitRayTraversal<T>(root, ray_dir, org);
  // Check if ray intersect with root node
  T max_entry = max(helper.entry.x, max(helper.entry.y, helper.entry.z));
  T min_exit = max(helper.exit.x, max(helper.exit.y, helper.exit.z));
  if (max_entry < min_exit) {
    // Process tree
    helper.node_idx = 0;  // Root node
    Stack<RayTraversal<T>, SIZE> stack;
    stack.Push(helper);
    while (!stack.Empty()) {
      RayTraversal<T> traversal = stack.Top();
      stack.Pop();
      // No Children present or invalid exit point ?
      if (traversal.node_idx == -1 ||
          traversal.exit.x < T(0.0) ||
          traversal.exit.y < T(0.0) ||
          traversal.exit.z < T(0.0)) {
        continue;
      }
      // Reach leaf ?
      const auto& node = tree_ptr[traversal.node_idx];
      if (node.IsLeaf()) {
        // Check if intersect with triangles
        const auto tol = T(2.0) * std::numeric_limits<T>::epsilon();
        T t_inter = T(-1.0);
        for (size_t i = node.first; i <= node.last; ++i) {
          const auto& tri = tri_ptr[tri_map_ptr[i]];
          const auto& A = v_ptr[tri.x];
          const auto& B = v_ptr[tri.y];
          const auto& C = v_ptr[tri.z];
          bool inter = IntersectTriSegment<T>(p, q,
                                              A, B, C,
                                              cull_face,
                                              tol,
                                              &t_inter);
          if (inter && (t_inter < t_max)) {
            inter_cnt[(tidy * bdx) + tidx] = T(1.0);
            stack.Clear();
            break;
          }
        }
        continue;
      }
      // Process childrens
      ProcessChildren<T, SIZE>(node, org, ray_dir, traversal, &stack);
    }
  }
  // Wait till all threads of the block have filled the shared memory
  __syncthreads();

  // Do reduction in shared memory
  for (unsigned int s = blockDim.x / 2; s > 0; s >>=1) {
    if (tidx < s) {
      inter_cnt[(tidy * bdx) + tidx] += inter_cnt[(tidy * bdx) + tidx + s];
    }
    __syncthreads();
  }

  // Add everything into global memory
  if (tidx == 0) {
    atomicAdd(&t_ptr[iy], inter_cnt[tidy * bdx]);
  }
}

/**
 * @name    SetValue
 * @brief Set all element of the array to a given value
 * @param[in,out] array Array to set value
 * @param[in] size  Array dimension
 * @param[in] value Value to set
 * @tparam T    Data type
 */
template<typename T>
__global__
void SetValue(T* array, int size, T value) {
  int ix = (blockDim.x * blockIdx.x) + threadIdx.x;
  if (ix < size) {
    array[ix] = value;
  }
}

/**
 * @name
 * @brief Initialize random state for random number generator
 * @param[in,out] state Array of state
 * @param[in] seed      Generator seed
 * @param[in] n_state   Number of state in the array
 */
__global__
void InitializeRandomState(curandState* state,
                           unsigned long long seed,
                           int n_state) {
  int ix = (blockDim.x * blockIdx.x) + threadIdx.x;
  if (ix >= n_state) return;
  // Init
  curand_init(seed, ix, 0, &state[ix]);
}



}  // namespace device



#pragma mark -
#pragma mark OCTree

template<typename T>
void OCTree<T>::Build(const Mesh<T, Vector3<T>>& mesh) {
  using HTree = LTS5::OCTree<T>;
  using HNode = LTS5::OCTreeNode<T>;
  using DNode = Node<T>;
  // Create regular OCTree on host
  HTree hTree;
  hTree.Insert(mesh, Mesh<T, Vector3<T>>::kTriangle);
  hTree.Build();
  n_vertex_ = mesh.get_vertex().size();
  // Go through the whole tree, linearize nodes into one block of memory
  std::vector<const HNode*> hNodes;
  std::stack<const HNode*> queue;
  std::unordered_map<const HNode*, size_t> hNodeIdxMap;
  queue.push(hTree.get_root_node());
  while (!queue.empty()) {
    // Get element on top
    const auto* n = queue.top();
    queue.pop();
    hNodes.push_back(n);
    hNodeIdxMap.emplace(n, hNodes.size() - 1);
    // Goes through children
    for (auto* c : n->child_) {
      if (c != nullptr) {
        queue.push(c);
      }
    }
  }
  // Convert to device childs (i.e. without pointer reference
  std::vector<DNode> dNodes;
  for (const auto* hn : hNodes) {
    // Create device friendly node -> not reference
    DNode node(hn->first_,
               hn->last_,
               Pack(hn->bbox_.min_.x_, hn->bbox_.min_.y_, hn->bbox_.min_.z_),
               Pack(hn->bbox_.max_.x_, hn->bbox_.max_.y_, hn->bbox_.max_.z_),
               hn->level_);
    // Add children
    for (int i = 0; i < 8; ++i) {
      if (hn->child_[i] != nullptr) {
        auto it = hNodeIdxMap.find(hn->child_[i]);
        node.child[i] = it->second;
      } else {
        node.child[i] = -1;
      }
    }
    // Add to list
    dNodes.push_back(node);
  }
  // Push data to device
  using Vertex = typename Mesh<T, Vector3<T>>::Vertex;
  using Triangle = typename Mesh<T, Vector3<T>>::Triangle;
  const auto& vertex = mesh.get_vertex();
  const auto& tri = mesh.get_triangle();
  nodes_.Upload(dNodes.data(), dNodes.size() * sizeof(DNode));
  vertex_.Upload(vertex.data(), vertex.size() * sizeof(Vertex));
  tri_.Upload(tri.data(), tri.size() * sizeof(Triangle));
  // Index map
  const auto& map = hTree.get_index_map();
  std::vector<int> idx;
  for (const auto& m : map) {
    idx.push_back(static_cast<int>(m));
  }
  index_map_.Upload(idx.data(), idx.size() * sizeof(int));
}

template<typename T>
int OCTree<T>::IntersectWithSegment(const MemoryBlob& p,
                                    const MemoryBlob& q,
                                    const bool& cull_face,
                                    const T& t_max,
                                    MemoryBlob* t) {
  using HVec3 = Vector3<T>;

  // Process all queries in parallel
  auto n_queries = static_cast<int>(p.Size<HVec3>());
  // Setup output
  t->Create(n_queries * sizeof(T));

  // Access pointer
  const auto* p_ptr = p.Ptr<Vec3<T>>();
  const auto* q_ptr = q.Ptr<Vec3<T>>();
  const auto* n_ptr = nodes_.Ptr<Node<T>>();
  const auto* v_ptr = vertex_.Ptr<Vec3<T>>();
  const auto* tri_ptr = tri_.Ptr<Vec3<int>>();
  const auto* tri_map_ptr = index_map_.Ptr<int>();
  auto* t_ptr = t->Ptr<T>();
  // Launch kernel
  dim3 thd(128);
  dim3 bck((n_queries + 127) / 128);
  device::InterSegmentKernel<T, 16><<<bck, thd>>>(p_ptr,
          q_ptr,
          n_ptr,
          v_ptr,
          tri_ptr,
          tri_map_ptr,
          n_queries,
          0,
          cull_face,
          t_max,
          t_ptr);
  return cudaGetLastError() == cudaSuccess ? 0 : -1;
}

/*
 * @name  IntersectWithSegmentOnSurface
 * @brief Search for intersection between a given segment `PQ` and the tree.
 *        All segment share the SAME ending position and is located ON the
 *        underlying mesh surface.
 * @param[in] p   Array if starting position of segment
 * @param[in] vertex_index    Index of the vertex to use as ending point
 * @param[in] cull_face   If true, does not check back facing triangles
 * @param[in] t_max   Maximum segment length [0, 1.0]
 * @param[out] t  Array of intersection value. If no intersection happens
 *                    set it to -1
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int OCTree<T>::IntersectWithSegmentOnSurface(const MemoryBlob& p,
                                             const int& vertex_index,
                                             const bool& cull_face,
                                             const T& t_max,
                                             MemoryBlob* t) {
  using HVec3 = Vector3<T>;

  // Process all queries in parallel
  auto n_queries = static_cast<int>(p.Size<HVec3>());
  // Setup output
  t->Create(n_queries * sizeof(T));
  // Access pointer
  const auto* p_ptr = p.Ptr<Vec3<T>>();
  const auto* n_ptr = nodes_.Ptr<Node<T>>();
  const auto* v_ptr = vertex_.Ptr<Vec3<T>>();
  const auto* tri_ptr = tri_.Ptr<Vec3<int>>();
  const auto* tri_map_ptr = index_map_.Ptr<int>();
  auto* t_ptr = t->Ptr<T>();
  // Launch kernel
  dim3 thd(128);
  dim3 bck((n_queries + 127) / 128);
  device::InterSegmentKernel<T, 16><<<bck, thd>>>(p_ptr,
          v_ptr,
          n_ptr,
          v_ptr,
          tri_ptr,
          tri_map_ptr,
          n_queries,
          vertex_index,
          cull_face,
          t_max,
          t_ptr);
  return cudaGetLastError() == cudaSuccess ? 0 : -1;
}

/*
 * @name  CountIntersectOnSurface
 * @brief Search for intersection between a given segment `PQ` and the tree.
 *        All segment share the SAME ending position and is located ON the
 *        underlying mesh surface. Output the total number of intersection
 * @param[in] p   Array if starting position of segment
 * @param[in] vertex_index    Index of the vertex to use as ending point
 * @param[in] cull_face   If true, does not check back facing triangles
 * @param[in] t_max   Maximum segment length [0, 1.0]
 * @param[out] t  Pointer to where to place the intersection count.
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int OCTree<T>::CountIntersectOnSurface(const MemoryBlob& p,
                                       const int& vertex_index,
                                       const bool& cull_face,
                                       const T& t_max,
                                       MemoryBlob* t) {
  using HVec3 = Vector3<T>;

  // Process all queries in parallel
  // Can do it for one vertex on surface or for all off them.
  auto n_queries = static_cast<int>(p.Size<HVec3>());
  auto n_vertex = vertex_index < 0 ? n_vertex_ : 1;
  t->Create(n_vertex * sizeof(T));
  // Access pointer
  const auto* p_ptr = p.Ptr<Vec3<T>>();
  const auto* n_ptr = nodes_.Ptr<Node<T>>();
  const auto* v_ptr = vertex_.Ptr<Vec3<T>>();
  const auto* tri_ptr = tri_.Ptr<Vec3<int>>();
  const auto* tri_map_ptr = index_map_.Ptr<int>();
  auto* t_ptr = t->Ptr<T>();

  {
    // Initialize destination container
    dim3 thd(1024);
    dim3 bck((n_vertex + 1023) / 1024);
    device::SetValue<T><<<bck, thd>>>(t_ptr, n_vertex, T(0.0));
  }

  // Launch kernel
  if (n_vertex == 1) {
    dim3 thd(512);
    dim3 bck((n_queries + 511) / 512);
    int mem = thd.x * sizeof(T);

    device::CountInterSegmentOnSurfaceKernel<T, 32><<<bck, thd, mem>>>(p_ptr,
            v_ptr,
            n_ptr,
            v_ptr,
            tri_ptr,
            tri_map_ptr,
            n_queries,
            vertex_index,
            cull_face,
            t_max,
            t_ptr);
  } else {
    dim3 thd(16, 16);   //256 thread
    dim3 bck((n_queries + 15) / 16, (n_vertex + 15) / 16);
    int mem = 16 * 16 * sizeof(T);
    device::CountInterSegmentOnSurfaceKernel<T, 32><<<bck, thd, mem>>>(p_ptr,
            n_ptr,
            v_ptr,
            tri_ptr,
            tri_map_ptr,
            n_queries,
            n_vertex,
            cull_face,
            t_max,
            t_ptr);
  }
  auto err = cudaGetLastError();
  cudaSafeCall(err);
  return err == cudaSuccess ? 0 : -1;
}

template<typename T>
int OCTree<T>::CountIntersectForAmbientOcclusion(const MemoryBlob& transform,
                                                 const int& n_sample,
                                                 const T& radius,
                                                 const bool& cull_face,
                                                 const T& t_max,
                                                 MemoryBlob* t) {
  using HVec3 = Vector3<T>;
  t->Create(n_vertex_ * sizeof(T));

  // Access pointer
  const auto* trsfrm_ptr = transform.Ptr<Mat3x3<T>>();
  const auto* n_ptr = nodes_.Ptr<Node<T>>();
  const auto* v_ptr = vertex_.Ptr<Vec3<T>>();
  const auto* tri_ptr = tri_.Ptr<Vec3<int>>();
  const auto* tri_map_ptr = index_map_.Ptr<int>();
  auto* t_ptr = t->Ptr<T>();

  {
    // Initialize destination container
    dim3 thd(1024);
    dim3 bck((n_vertex_ + 1023) / 1024);
    device::SetValue<T><<<bck, thd>>>(t_ptr, n_vertex_, T(0.0));
  }

  // Initialize random state
  // 256 = 16 * 16 (i.e. block dim in next kernel)
  random_state_.Create(256 * sizeof(curandState));
  auto* prnd_ptr = random_state_.Ptr<curandState>();
  auto seed = std::chrono::system_clock::now().time_since_epoch().count();
  device::InitializeRandomState<<<1, 256>>>(prnd_ptr, seed, 256);

  {
    dim3 thd(16, 16);   //256 thread
    dim3 bck((n_sample + 15) / 16, (n_vertex_ + 15) / 16);
    int mem = 16 * 16 * sizeof(T);
    // <T, 32, 16> == Data type, stack size, block dim y
    device::CountInterOnSurface<T, 32, 16><<<bck, thd, mem>>>(trsfrm_ptr,
            n_ptr,
            v_ptr,
            tri_ptr,
            tri_map_ptr,
            prnd_ptr,
            radius,
            n_sample,
            n_vertex_,
            cull_face,
            t_max,
            t_ptr);
  }
  auto err = cudaGetLastError();
  cudaSafeCall(err);
  return err == cudaSuccess ? 0 : -1;
}


template class OCTree<float>;
#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
template class OCTree<double>;
#endif


}  // namespace CUDA
}  // namespace LTS5