/**
 *  @file   lts5_cuda_octree.cpp
 *  @brief  
 *  @ingroup 
 *
 *  @author Christophe Ecabert
 *  @date   2/11/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <chrono>
#include <random>
using namespace std::chrono;

#include <iostream>
#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/cuda/geometry/octree.hpp"
#include "lts5/geometry/octree.hpp"

int main(int argc, const char * argv[]) {
  using namespace LTS5::CUDA;
  using T = float;

  LTS5::CmdLineParser parser;
  parser.AddArgument("--mesh", LTS5::CmdLineParser::kNeeded, "Mesh location");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    std::string path;
    parser.HasArgument("--mesh", &path);

    // Load mesh
    LTS5::Mesh<T, LTS5::Vector3<T>> mesh(path);
    const auto& vertex = mesh.get_vertex();
    const auto nose_tip = vertex[8321];
    bool cull_face = false;

    // Generate random query points inside mesh bounding box
    std::vector<LTS5::Vector3<T>> pHost;
    auto bbox = mesh.bbox();
    auto width = (bbox.max_ - bbox.min_) * T(0.5);
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<T> dis(T(-1.0), T(1.0));
//    int n_sample = 5000;
    int n_sample = 5000;
    pHost.emplace_back(T(100.0), T(100.0), T(100.0));
    for (int i = 0; i < n_sample; ++i) {
      T vx = bbox.center_.x_ + dis(gen) * width.x_;
      T vy = bbox.center_.y_ + dis(gen) * width.y_;
      T vz = bbox.center_.z_ + dis(gen) * width.z_;
      pHost.emplace_back(vx, vy, vz);
    }

    std::vector<T> inter_gpu(n_sample);
    std::vector<T> gpu_cnt;
    {
      // GPU
      OCTree<T> tree;
      tree.Build(mesh);
      auto n_vertex = mesh.get_vertex().size();
      gpu_cnt.resize(n_vertex);

      MemoryBlob p, q, t, t_cnt;
      t.Create(n_sample * sizeof(T));
      p.Upload(pHost.data(), pHost.size() * sizeof(LTS5::Vector3<T>));
      t_cnt.Create(n_vertex * sizeof(T));

      auto start_gpu = high_resolution_clock::now();
      tree.IntersectWithSegmentOnSurface(p, 8321, cull_face, T(0.99), &t);
      auto stop_gpu = high_resolution_clock::now();
      auto dt_gpu = duration_cast<milliseconds>(stop_gpu - start_gpu);
      std::cout << "Time gpu: " << dt_gpu.count() << "ms" << std::endl;
      t.Download(inter_gpu.data());

      auto start_gpu_cnt = high_resolution_clock::now();
      tree.CountIntersectOnSurface(p, -1, cull_face, T(0.99), &t_cnt);
      auto stop_gpu_cnt = high_resolution_clock::now();
      auto dt_gpu_cnt = duration_cast<milliseconds>(stop_gpu_cnt - start_gpu_cnt);
      std::cout << "Time gpu cnt: " << dt_gpu_cnt.count() << "ms" << std::endl;
      t_cnt.Download(gpu_cnt.data());
    }

    std::vector<T> inter_cpu;
    inter_cpu.reserve(n_sample);
    T cpu_cnt = T(0.0);
    {
      // CPU
      LTS5::OCTree<T> hTree;
      hTree.Insert(mesh, LTS5::Mesh<T, LTS5::Vector3<T>>::kTriangle);
      hTree.Build();
      auto start_cpu = high_resolution_clock::now();
      for (const auto& pt : pHost) {
        T t_inter = T(0.0);
        bool is_inter = hTree.IntersectWithSegment(mesh,
                                                   pt,
                                                   nose_tip,
                                                   cull_face,
                                                   T(0.99),
                                                   &t_inter);
        t_inter = is_inter ? t_inter : T(-1.0);
        cpu_cnt = is_inter ? cpu_cnt + T(1.0) : cpu_cnt;
        inter_cpu.push_back(t_inter);
      }
      auto stop_cpu = high_resolution_clock::now();
      auto dt_cpu = duration_cast<milliseconds>(stop_cpu - start_cpu);
      std::cout << "Time cpu: " << dt_cpu.count() << "ms" << std::endl;
    }

    LTS5_LOG_INFO("Gpu cnt: " << gpu_cnt[8321] << ", cpu cnt: " << cpu_cnt);

    // Compare
    for (int i = 0; i < n_sample; ++i) {
      T cpu_value = inter_cpu[i];
      T gpu_value = inter_gpu[i];
      if (std::abs(cpu_value - gpu_value) > 1e-2) {
        LTS5_LOG_WARNING("Result diverges: "<< cpu_value << " vs " << gpu_value);
      }
    }
  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
}

