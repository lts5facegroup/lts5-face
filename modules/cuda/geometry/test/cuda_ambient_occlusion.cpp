/**
 *  @file   cuda_ambient_occlusion.cpp
 *  @brief  
 *  @ingroup 
 *
 *  @author Christophe Ecabert
 *  @date   2/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <string>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/colormap.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/utils/sys/file_system.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/cuda/geometry/ambient_occlusion.hpp"

int main(int argc, const char * argv[]) {
  using namespace LTS5;

  using T = float;
  using Mesh_t = Mesh<T, Vector3<T>>;
  using Color = typename Mesh_t::Color;
  using AOGenerator = CUDA::AmbientOcclusionGenerator<T>;
  using CMap = ColorMap<T>;

  CmdLineParser parser;
  parser.AddArgument("--input_folder",
                     CmdLineParser::kNeeded,
                     "Folder where mesh are stored");
  parser.AddArgument("--output",
                     CmdLineParser::kNeeded,
                     "Folder where to place computed ao");
  parser.AddArgument("--n_sample",
                     CmdLineParser::kOptional,
                     "Number of sample to use during integration");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    std::string input_folder;
    std::string output;
    std::string n_sample_str;
    parser.HasArgument("--input_folder", &input_folder);
    parser.HasArgument("--output", &output);
    parser.HasArgument("--n_sample", &n_sample_str);

    int n_sample = n_sample_str.empty() ? 5000 : std::stoi(n_sample_str);

    // Scan meshes
    std::vector<std::string> meshes;
    auto* fs = GetDefaultFileSystem();
    fs->ListDir(input_folder, &meshes);
    std::sort(meshes.begin(), meshes.end());

    // Output exists ?
    if (!fs->FileExist(output).Good()) {
      fs->CreateDir(output);
    }

    // Iterate over all meshes
    int cnt = 0;
    for (const auto& path : meshes) {
      // Define name
      std::string folder, filename, ext, fname;
      Path::SplitComponent(path, &folder, &filename, &ext);
      fname = Path::Join(output, filename + ".ao");

      if (cnt % 1000 == 0) {
        LTS5_LOG_INFO("Compute AO for: " << filename << "." << ext);
      }

      // Exists ?
      if (!fs->FileExist(fname).Good()) {
        // No process
        // Load mesh + compute AOs
        Mesh_t mesh(path);
        AOGenerator generator(&mesh);
        generator.Generate(n_sample);
        generator.Save(fname);
      }
      cnt += 1;
    }


//    const auto& ao = generator.get_ao();
//    CMap cm(CMap::kGrayscale, T(0.0), T(1.0));
//    std::vector<Color> colors;
//    for (int i = 0; i < ao.rows; ++i) {
//      Color c;
//      cm.PickColor(ao.at<T>(i), &c);
//      colors.push_back(c);
//    }
//    mesh.set_vertex_color(colors);
//    // Dump mesh + ao
//    std::string folder, file, ext;
//    ExtractDirectory(path, &folder, &file, &ext);
//    file += "_ao_gpu";
//    ext = ".ply";
//    mesh.Save(Path::Join(folder, file + ext));

  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
}