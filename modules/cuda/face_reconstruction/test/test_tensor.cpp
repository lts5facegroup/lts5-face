/**
 *  @file   test_tensor.cpp
 *  @brief  cuDNN test sample
 *
 *  @author Christophe Ecabert
 *  @date   21/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <vector>
#include <algorithm>


#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/tensor.hpp"
#include "lts5/cuda/utils/math/cudnn_wrapper.hpp"
#include "lts5/cuda/utils/tensor_workspace.hpp"
#include "lts5/cuda/face_reconstruction/bilinear_face_model.hpp"


int main(int argc, const char * argv[]) {

  using T = float;
  using Tensorf = LTS5::CUDA::Tensor<T>;
  using TA = LTS5::CUDA::TensorAlgebra<T>;
  using TensorWorkspace = LTS5::CUDA::TensorWorkspace<T>;
  using TensorOpType = LTS5::CUDA::TensorOpType;
  using TensorRedType = LTS5::CUDA::TensorRedType;
  using TensorOp = LTS5::CUDA::TensorOp<T>;
  using TensorRedOp = LTS5::CUDA::TensorRedOp<T>;

  Tensorf tA, tB1, tB2, tAB, tC1, tC1B2, tC;
  TensorOp op(TensorOpType::kOpMul);
  TensorRedOp red(TensorRedType::kRedAdd);
  TensorWorkspace tw;

  // Dimension for A
  const size_t row_a = 7;
  const size_t col_a = 6;
  const size_t depth_a = 5;

  const size_t row_b1 = 1;
  const size_t col_b1 = 1;
  const size_t depth_b1 = 5;

  const size_t row_b2 = 1;
  const size_t col_b2 = 6;
  const size_t depth_b2 = 1;

  const size_t row_c1 = 7;
  const size_t col_c1 = 6;
  const size_t depth_c1 = 1;

  const size_t row_c = 7;
  const size_t col_c = 1;
  const size_t depth_c = 1;

  LTS5::CUDA::BilinearFaceModel model;
  int e = model.Load("BilinearFaceModel/v1.3/BilinearFaceModel_ocv.raw");



  // ---------------------------------------------------------------------
  // Host
  // ---------------------------------------------------------------------

  // Init tensor A's data
  std::vector<T> h_a(row_a * col_a * depth_a);
  std::iota(h_a.begin(), h_a.end(), T(0));
  int dimA[] = {row_a, col_a, depth_a};
  cv::Mat hA(3, dimA, CV_32FC1, (void*)h_a.data());

  // Init tensor B's data
  std::vector<T> h_b1(row_b1 * col_b1 * depth_b1);
  std::iota(h_b1.begin(), h_b1.end(), T(1));
  int dimB1[] = {row_b1, col_b1, depth_b1};
  cv::Mat hB1(3, dimB1, CV_32FC1, (void*)h_b1.data());

  int dimB2[] = {row_b2, col_b2, depth_b2};

  cv::Mat hB2(3, dimB2, CV_32FC1,h_a.data());


  // Init tensor C's data
  std::vector<T> h_c1{24.0, 27.0, 30.0, 33.0, 36.0, 39.0,
                      42.0, 45.0, 48.0, 51.0, 54.0, 57.0};




  // ---------------------------------------------------------------------
  // Device Container
  // ---------------------------------------------------------------------

  tA.Upload(hA);
  tB1.Upload(hB1);
  tB2.Upload(hB2);
  tAB.Create(row_a, col_a, depth_a);
  tC1.Create(row_c1, col_c1, depth_c1);
  tC1B2.Create(row_c1, col_c1, depth_c1);
  tC.Create(row_c, col_c, depth_c);

  // Query for workspace dimension
  size_t workspace_size = 0, sz = 0;
  TensorWorkspace::ReductionSize(red, tAB, tC1, &sz);
  workspace_size = std::max(workspace_size, sz);
  TensorWorkspace::ReductionSize(red, tC1B2, tC, &sz);
  workspace_size = std::max(workspace_size, sz);

  // Create workspace
  tw.Create(workspace_size);


  // ---------------------------------------------------------------------
  // Tensor operation
  // ---------------------------------------------------------------------
  T alpha(1.0);
  T beta(0.0);
  int err = TA::Operation(op, tA, alpha, tB1, alpha, beta, &tAB);
  if (err) {
    std::cout << "Unable to perform A * B1" << std::endl;
  }

  err = TA::Reduction(red, tw, tAB, alpha, beta, &tC1);
  if (err) {
    std::cout << "Unable to perform reduction" << std::endl;
  }

  err = TA::Operation(op, tC1, alpha, tB2, alpha, beta, &tC1B2);
  if (err) {
    std::cout << "Unable to perform C1 * B2" << std::endl;
  }

  err = TA::Reduction(red, tw, tC1B2, alpha, beta, &tC);
  if (err) {
    std::cout << "Unable to perform reduction" << std::endl;
  }




  cv::Mat tensor;
  tC.Download(&tensor);

  std::cout << tensor << std::endl;

  tensor.at<T>(0, 0, 0);

}
