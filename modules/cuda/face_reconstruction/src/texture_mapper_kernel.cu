/**
 *  @file   texture_mapper_kernel.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   03/05/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <cstdio>
#include <cfloat>
#include <iostream>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>

#include "opencv2/core/cuda_stream_accessor.hpp"

#include "lts5/cuda/face_reconstruction/device/texture_mapper_kernel.h"
#include "lts5/cuda/utils/safe_call.hpp"

//#define USE_CACHE
#define USE_TEXTURE


#ifdef USE_TEXTURE
using tex_t = texture<uchar, 2,  cudaReadModeElementType>;
// Texture reference for 2D float texture
tex_t tex0, tex1, tex2;

template<typename T, size_t D>
T* TexSelector(void) {
  return nullptr;
};

template<>
tex_t* TexSelector<tex_t, 0>(void) {
  return &tex0;
}
template<>
tex_t* TexSelector<tex_t, 1>(void) {
  return &tex1;
}
template<>
tex_t* TexSelector<tex_t, 2>(void) {
  return &tex2;
}

/** Selector pointer */
typedef tex_t* (*selector_t)(void);
/** Selector lookup table */
const selector_t tex_selector[] = {TexSelector<tex_t, 0>,
                                   TexSelector<tex_t, 1>,
                                   TexSelector<tex_t, 2>};

#endif

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {



/** Pointer to wrapping function */
typedef void (*caller_ptr)(const cv::cuda::PtrStepSzb* img_array,
                           const cv::cuda::PtrStepf triangle_map,
                           const float2* image_pts,
                           const float2* uv_pts,
                           const cv::cuda::PtrStepb vertex_view_visibility,
                           const cv::cuda::PtrStepf vertex_view_weights,
                           const cv::cuda::PtrStepi vertex_weight_selection,
                           const float w_u,
                           const dim3 tex_size,
                           uchar* tex_ptr);

typedef void (*caller_tex_ptr)(const dim3 img_size,
                               const cv::cuda::PtrStepf triangle_map,
                               const float2* image_pts,
                               const float2* uv_pts,
                               const cv::cuda::PtrStepb vertex_view_visibility,
                               const cv::cuda::PtrStepf vertex_view_weights,
                               const cv::cuda::PtrStepi vertex_weight_selection,
                               const float w_u,
                               const dim3 tex_size,
                               uchar* tex_ptr);

/*
 * @name    WarpView
 * @brief   Helper function for Warpping function
 * @ingroup face_reconstruction
 * @param[in] img_array                 Array of input images
 * @param[in] n_view                    Number of input images
 * @param[in] triangle_map              Triangle equivalent for every pixel in
 *                                      the texture map
 * @param[in] image_pts                 2D coordinate on the input image
 * @param[in] uv_pts                    Texture coordinate
 * @param[in] vertex_view_visibility    Vertex visibility for each view
 * @param[in] vertex_view_weights       Vertex weights for each view
 * @param[in] vertex_weight_selection   List of vertex to consider for a given
 *                                      triangle
 * @param[in] w_u                       RBF interpolation factor
 * @param[in] tex_size                  Texture map dimension (w, h, n_pts)
 * @param[in,out] tex_ptr               Generated texture map
 */
void WarpView(const cv::cuda::PtrStepSzb* img_array,
              const int n_view,
              const cv::cuda::GpuMat& triangle_map,
              const float2* image_pts,
              const float2* uv_pts,
              const cv::cuda::GpuMat& vertex_view_visibility,
              const cv::cuda::GpuMat& vertex_view_weights,
              const cv::cuda::GpuMat& vertex_weight_selection,
              const float w_u,
              const dim3 tex_size,
              uchar* tex_ptr) {
  // function array with different size
  static const caller_ptr wrap_fcn[6] {
    0,
    device::warp_view_kernel<1, 826>,
    device::warp_view_kernel<2, 826>,
    device::warp_view_kernel<3, 826>,
    0,
    0
  };
  // Define function
  // Define grid + block dimension
  dim3 thd(16, 16);
  dim3 bck((tex_size.x + 15) / 16,
           (tex_size.y + 15) / 16);
  // Get wrapper function
  const caller_ptr wrapping_fcn = wrap_fcn[n_view];
  if (wrapping_fcn) {
    wrapping_fcn<<<bck, thd>>>(img_array,
            triangle_map,
            image_pts,
            uv_pts,
            vertex_view_visibility,
            vertex_view_weights,
            vertex_weight_selection,
            w_u,
            tex_size,
            tex_ptr);
  } else {
    LTS5::CUDA::error("Number of view not supported", __FILE__, __LINE__, "");
  }
}

/*
 * @name    WarpView
 * @brief   Helper function for Warpping function
 * @ingroup face_reconstruction
 * @param[in] images                    Array of input images
 * @param[in] triangle_map              Triangle equivalent for every pixel in
 *                                      the texture map
 * @param[in] image_pts                 2D coordinate on the input image
 * @param[in] n_image_pts               number of 2D point on each input
 * @param[in] vertex_view_visibility    Vertex visibility for each view
 * @param[in] vertex_view_weights       Vertex weights for each view
 * @param[in] vertex_weight_selection   List of vertex to consider for a given
 *                                      triangle
 * @param[in] w_u                       RBF interpolation factor
 * @param[in] texture_size              Texture map dimension
 * @param[in,out] texture_ptr           Generated texture map
 */
void WarpView(const std::vector<cv::cuda::GpuMat>& images,
              const cv::cuda::GpuMat& triangle_map,
              const float2* image_pts,
              const float2* uv_pts,
              const cv::cuda::GpuMat& vertex_view_visibility,
              const cv::cuda::GpuMat& vertex_view_weights,
              const cv::cuda::GpuMat& vertex_weight_selection,
              const float w_u,
              const dim3 texture_size,
              uchar* texture_ptr) {
  // function array with different size
  static const caller_tex_ptr wrap_fcn[6] {0,
                                           device::warp_view_kernel_tex<1, 826>,
                                           device::warp_view_kernel_tex<2, 826>,
                                           device::warp_view_kernel_tex<3, 826>,
                                           0,
                                           0};
  // Bind texture
  // Texture Channel Description
  cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<uchar>();
  for (int i = 0; i < images.size(); ++i) {
    // Set mutable properties, if needed
    auto* tex = tex_selector[i]();
    tex->normalized = false;
    tex->addressMode[0] = cudaAddressModeClamp;  // if my indexing is out of bounds: automatically use a valid indexing (0 if negative index, last if too great index)
    tex->addressMode[1] = cudaAddressModeClamp;
    // Bind texture to pitch mem
    size_t offset;
    cudaSafeCall(cudaBindTexture2D(&offset,
                                   tex,
                                   images[i].data,
                                   &channelDesc,
                                   images[i].cols,
                                   images[i].rows,
                                   images[i].step));
  }
  // Define dimension
  dim3 img_dim(images[0].cols, images[0].rows);
  // Define grid + block dimension
  dim3 thd(16, 16);
  dim3 bck((texture_size.x + 15) / 16,
           (texture_size.y + 15) / 16);
  // Get wrapper function
  const caller_tex_ptr wrapping_fcn = wrap_fcn[images.size()];
  if (wrapping_fcn) {
    wrapping_fcn<<<bck, thd>>>(img_dim,
            triangle_map,
            image_pts,
            uv_pts,
            vertex_view_visibility,
            vertex_view_weights,
            vertex_weight_selection,
            w_u,
            texture_size,
            texture_ptr);
  } else {
    LTS5::CUDA::error("Number of view not supported", __FILE__, __LINE__, "");
  }

  for (int i = 0; i < images.size(); ++i) {
    // Unbind
    cudaSafeCall(cudaUnbindTexture(tex_selector[i]()));
  }

}

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {

/*
 * @name    warp_view_kernel
 * @brief   Image warping kernel
 * @ingroup face_reconstruction
 * @param[in] img_array                 Array of input images
 * @param[in] triangle_map              Triangle equivalent for every pixel in
 *                                      the texture map
 * @param[in] image_pts                 2D coordinate on the input image
 * @param[in] uv_pts                    2D Coordinate in the texture space
 * @param[in] vertex_view_visibility    Vertex visibility for each view
 * @param[in] vertex_view_weights       Vertex weights for each view
 * @param[in] vertex_weight_selection   List of vertex to consider for a given
 *                                      triangle
 * @param[in] w_u                       RBF interpolation factor
 * @param[in] tex_size              Texture map dimension (w, h, n_pts)
 * @param[in,out] tex_ptr           Generated texture map
 */
template <int NVIEW, int NVERTEX>
__global__
void warp_view_kernel(const cv::cuda::PtrStepSzb* img_array,
                      const cv::cuda::PtrStepf triangle_map,
                      const float2* image_pts,
                      const float2* uv_pts,
                      const cv::cuda::PtrStepb vertex_view_visibility,
                      const cv::cuda::PtrStepf vertex_view_weights,
                      const cv::cuda::PtrStepi vertex_weight_selection,
                      const float w_u,
                      const dim3 tex_size,
                      uchar* tex_ptr) {
  // Query position
  volatile unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
  volatile unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
  volatile unsigned int th_x = threadIdx.x * 2;
  volatile unsigned int th_y = threadIdx.y;

  __shared__ float4 trimap[16][2*16];
  const float4* tri_r_ptr = reinterpret_cast<const float4*>(triangle_map.ptr(y));
  trimap[th_y][th_x] = tri_r_ptr[2 * x];
  trimap[th_y][th_x + 1] = tri_r_ptr[2 * x + 1];

  // Pixel inside triangle ?
  if (trimap[th_y][th_x].x >= 0.f) {
    // Loop over each view
    const volatile int t_idx = static_cast<int>(trimap[th_y][th_x].x);
    float volatile sum_wip = FLT_EPSILON;
    float volatile pixel = 0.f;

    for (int v = 0; v < NVIEW; ++v) {
      // Check visibility
      int tx = static_cast<int>(trimap[th_y][th_x+1].x);
      int ty = static_cast<int>(trimap[th_y][th_x+1].y);
      int tz = static_cast<int>(trimap[th_y][th_x+1].z);

      int visi = (vertex_view_visibility(tx, v + 1) +
                  vertex_view_visibility(ty, v + 1) +
                  vertex_view_visibility(tz, v + 1));
      if (visi > 0) {
        // Visible define sampling location with barycentric coordinate
        int base = v * tex_size.z;
        int idx = base + tx;
        float p0x = image_pts[idx].x;
        float p0y = image_pts[idx].y;
        idx = base + ty;
        float p1x = image_pts[idx].x;
        float p1y = image_pts[idx].y;
        idx = base + tz;
        float p2x = image_pts[idx].x;
        float p2y = image_pts[idx].y;
        const float px = (p0x * trimap[th_y][th_x].y) +
                         (p1x * trimap[th_y][th_x].z) +
                         (p2x * trimap[th_y][th_x].w);
        const float py = (p0y * trimap[th_y][th_x].y) +
                         (p1y * trimap[th_y][th_x].z) +
                         (p2y * trimap[th_y][th_x].w);

        // Check boundaries
        if (px < 0.f || py < 0.f ||
            px > img_array[v].cols || py > img_array[v].rows) {
        } else {
          // Interpolate
          int X = static_cast<int>(px);
          int Y = static_cast<int>(py);
          // Get delta
          float dx = px - X;
          float dy = py - Y;
          float dx1 = 1.f - dx;
          float dy1 = 1.f - dy;
          // Interpolate
          float pixel_value = (dx1 * dy1 * img_array[v](Y, X)) +
                              (dx * dy1 * img_array[v](Y, X + 1)) +
                              (dx1 * dy * img_array[v](Y + 1, X)) +
                              (dx * dy * img_array[v](Y + 1, X + 1));
          // Per-pixel weight
          float w_ip = 0.f;
          const volatile int n_sel = vertex_weight_selection(t_idx, 0);
          for (int k = 0; k < n_sel; ++k) {
            int sel = vertex_weight_selection(t_idx, k + 1);
            float v_visi = vertex_view_visibility(sel, 0);
            float p_dx = x - uv_pts[sel].x;
            float p_dy = y - uv_pts[sel].y;
            float e = w_u * ((p_dx * p_dx) + (p_dy * p_dy));
            w_ip += (v_visi * __expf(-e)) * vertex_view_weights(sel, v);
          }
          //pixel_w[v] = w_ip;
          pixel += w_ip * pixel_value;
          sum_wip += w_ip;
        }
      }
    }
    pixel /= sum_wip;
    // Clamp
    uint res = 0;
    asm("cvt.rni.sat.u8.f32 %0, %1;" : "=r"(res) : "f"(pixel));
    tex_ptr[y * tex_size.x + x] = uchar(res);
  } else {
    tex_ptr[y * tex_size.x + x] = uchar(0);
  }
}

/*
 * @name    warp_view_kernel_tex
 * @brief   Image warping kernel using 2D texture as image input
 * @ingroup face_reconstruction
 * @param[in] img_size                  Image dimension
 * @param[in] triangle_map              Triangle equivalent for every pixel in
 *                                      the texture map
 * @param[in] image_pts                 2D coordinate on the input image
 * @param[in] uv_pts                    2D Coordinate in the texture space
 * @param[in] vertex_view_visibility    Vertex visibility for each view
 * @param[in] vertex_view_weights       Vertex weights for each view
 * @param[in] vertex_weight_selection   List of vertex to consider for a given
 *                                      triangle
 * @param[in] w_u                       RBF interpolation factor
 * @param[in] tex_size                  Texture map dimension (w, h, n_pts)
 * @param[in,out] tex_ptr               Generated texture map
 */
template<int NVIEW, int NVERTEX>
__global__
void warp_view_kernel_tex(const dim3 img_size,
                          const cv::cuda::PtrStepf triangle_map,
                          const float2* image_pts,
                          const float2* uv_pts,
                          const cv::cuda::PtrStepb vertex_view_visibility,
                          const cv::cuda::PtrStepf vertex_view_weights,
                          const cv::cuda::PtrStepi vertex_weight_selection,
                          const float w_u,
                          const dim3 tex_size,
                          uchar* tex_ptr) {
  // Query position
  volatile int x = blockIdx.x * blockDim.x + threadIdx.x;
  volatile int y = blockIdx.y * blockDim.y + threadIdx.y;
  volatile unsigned int th_x = threadIdx.x * 2;
  volatile unsigned int th_y = threadIdx.y;

  __shared__ float4 trimap[16][2*16];
  const float4* tri_r_ptr = reinterpret_cast<const float4*>(triangle_map.ptr(y));
  trimap[th_y][th_x] = tri_r_ptr[2 * x];
  trimap[th_y][th_x + 1] = tri_r_ptr[2 * x + 1];

  // Pixel inside triangle ?
  if (trimap[th_y][th_x].x >= 0.f) {
    // Loop over each view
    float volatile sum_wip = FLT_EPSILON;
    float volatile pixel = 0.f;
    #pragma unroll
    for (int v = 0; v < NVIEW; ++v) {
      // Check visibility
      const int tx = static_cast<int>(trimap[th_y][th_x+1].x);
      const int ty = static_cast<int>(trimap[th_y][th_x+1].y);
      const int tz = static_cast<int>(trimap[th_y][th_x+1].z);
      const int visi = (vertex_view_visibility(tx, v + 1) +
                        vertex_view_visibility(ty, v + 1) +
                        vertex_view_visibility(tz, v + 1));
      if (visi > 0) {
        // Visible define sampling location with barycentric coordinate
        int base = v * tex_size.z;
        int idx = base + tx;
        float p0x = image_pts[idx].x;
        float p0y = image_pts[idx].y;
        idx = base + ty;
        float p1x = image_pts[idx].x;
        float p1y = image_pts[idx].y;
        idx = base + tz;
        float p2x = image_pts[idx].x;
        float p2y = image_pts[idx].y;
        const float px = (p0x * trimap[th_y][th_x].y) +
                         (p1x * trimap[th_y][th_x].z) +
                         (p2x * trimap[th_y][th_x].w);
        const float py = (p0y * trimap[th_y][th_x].y) +
                         (p1y * trimap[th_y][th_x].z) +
                         (p2y * trimap[th_y][th_x].w);
        // Check boundaries
        if (px > 0.f && py > 0.f &&
            px < img_size.x && py < img_size.y) {
          // Sample texture
          float pixel_value = 0.f;
          if (v == 0) {
            pixel_value = tex2D(tex0, px + 0.5f, py + 0.5f);
          } else if (v==1) {
            pixel_value = tex2D(tex1, px + 0.5f, py + 0.5f);
          } else {
            pixel_value = tex2D(tex2, px + 0.5f, py + 0.5f);
          }
          // Per-pixel weight
          float w_ip = 0.f;
          const int t_idx = static_cast<int>(trimap[th_y][th_x].x);
          const int n_sel = vertex_weight_selection(t_idx, 0);
          for (int k = 0; k < n_sel; ++k) {
            int sel = vertex_weight_selection(t_idx, k + 1);
            float v_visi = vertex_view_visibility(sel, 0);
            float p_dx = x - uv_pts[sel].x;
            float p_dy = y - uv_pts[sel].y;
            float e = w_u * ((p_dx * p_dx) + (p_dy * p_dy));
            w_ip += (v_visi * __expf(-e)) * vertex_view_weights(sel, v);
          }
          //pixel_w[v] = w_ip;
          pixel += w_ip * pixel_value;
          sum_wip += w_ip;
        }
      }
    }
    pixel /= sum_wip;
    // Clamp
    uint res = 0;
    asm("cvt.rni.sat.u8.f32 %0, %1;" : "=r"(res) : "f"(pixel));
    tex_ptr[y * tex_size.x + x] = uchar(res);
  } else {
    tex_ptr[y * tex_size.x + x] = uchar(0);
  }
}
}  // namepsace device
}  // namepsace CUDA
}  // namepsace LTS5
