/**
 *  @file   bilinear_face_model.cpp
 *  @brief  Bilinear face model
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   23/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <fstream>
#include <cudnn.h>
#include <cublas_v2.h>

#include "lts5/utils/logger.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/cuda/face_reconstruction/bilinear_face_model.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/utils/math/cublas_wrapper.hpp"
#include "lts5/cuda/utils/tensor_operation.hpp"

#include "lts5/utils/file_io.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   BilinearFaceModel
 *  @fn BilinearFaceModel(void)
 *  @brief  Create a bilinear face model object.
 */
BilinearFaceModel::BilinearFaceModel(void)/* : op_(TensorOpType::kOpMul),
                                             red_(TensorRedType::kRedAdd)*/ {

}

/*
 *  @name   BilinearFaceModel
 *  @fn BilinearFaceModel(const std::string& filename)
 *  @brief  Create a bilinear face model object.
 *  @param[in]  filename  File containing the data to load into the core
 *                        tensor.
 */
BilinearFaceModel::BilinearFaceModel(const std::string& filename) {
  this->Load(filename);
}

/*
 *  @name   Base destructor
 *  @fn ~BilinearFaceModel()
 *  @brief  Destructor
 */
BilinearFaceModel::~BilinearFaceModel() {
}

/*
 *  @name   Load
 *  @fn void Load(const std::string& filename)
 *  @brief  Load face model from config file
 *  @param[in]  filename Model configuration file
 *  @return -1 if error, 0 otherwise
 */
int BilinearFaceModel::Load(const std::string& filename) {
  int err = -1;
  std::string dir, file, ext;
  LTS5::ExtractDirectory(filename, &dir, &file, &ext);
  if (ext == "raw") {
    std::ifstream stream(filename.c_str(),
                         std::ios_base::in | std::ios_base::binary);
    if (stream.is_open()) {
      stream.read(reinterpret_cast<char *>(&model_dims_[0]),
                  3 * sizeof(model_dims_[0]));
      rank_vertices_ = model_dims_[0];
      rank_identity_ = model_dims_[1];
      rank_expression_ = model_dims_[2];
      // Init model matrix
      int length = rank_vertices_ * rank_identity_ * rank_expression_;
      cv::Mat core(3, model_dims_, CV_32FC1);
      // Read model
      stream.read(reinterpret_cast<char *>(core.data), length * sizeof(float));
      // Convert to proper format
      int i = 0;
      std::vector<float> buffer(length);
      for (int depth = 0; depth < rank_expression_; ++depth) {
        for (int row = 0; row < rank_vertices_; ++row) {
          for (int col = 0; col < rank_identity_; ++col) {
            buffer[i] = core.at<float>(row, col, depth);
            ++i;
          }
        }
      }
      cv::Mat core_temp(3, model_dims_, CV_32FC1, buffer.data());
      err = stream.good() ? 0 : -1;
      if (!err) {
        // Initialize tensors
        core_.Upload(core_temp);
        expr_.Create(1, 1, rank_expression_);
        id_.Create(1, rank_identity_, 1);
        core_expr_.Create(rank_vertices_, rank_identity_, 1);
        core_id_expr_.Create(rank_vertices_, 1, 1);
        // Init convolution
        ConvParameter parameter;
        parameter.type = TensorConvType::kConvolution;
        parameter.pad_h = 0;
        parameter.pad_v = 0;
        parameter.stride_h = 1;
        parameter.stride_v = 1;
        parameter.dilation_h = 1;
        parameter.dilation_v = 1;
        conv_.Create(&expr_, parameter);
        // Init workspace
        size_t workspace_sz = 0;
        TensorWorkspace::ConvolutionSize(core_,
                                         core_expr_,
                                         &conv_,
                                         &workspace_sz);
        workspace_.Create(workspace_sz);
        err = 0;
      } else {
        LTS5_LOG_ERROR("Can not read model : " << filename);
      }
    }
  }
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Generate
 * @fn    int Generate(const cv::Mat& weight_id,
                        const cv::Mat& weight_expr,
                        cv::Mat* shape)
 * @brief Generate 3d face shape from a pair if given id/exp coefficient.
 * @param[in] weight_id   Identity weight
 * @param[in] weight_expr Expression weight
 * @param[out] shape      Generated 3D shape
 * @return -1 if error, 0 otherwise
 */
int BilinearFaceModel::Generate(const cv::Mat& weight_id,
                                const cv::Mat& weight_expr,
                                cv::Mat* shape) {
  LTS5_CUDA_TRACE("GenerateSurface", 10);
  LTS5_CUDA_START_PROFILE;
  int err = 0;
  using TA = TensorAlgebra<float>;
  // Create matrix header with proper dimension
  int dim_id[] = {id_.rows(), id_.cols(), id_.depth()};
  int dim_exp[] = {expr_.rows(), expr_.cols(), expr_.depth()};
  cv::Mat id(3, dim_id, CV_32FC1, weight_id.data);
  cv::Mat exp(3, dim_exp, CV_32FC1, weight_expr.data);
  // Upload
  id_.Upload(id);
  expr_.Upload(exp);
  // 1x1 Convolution by expression coefficient
  float alpha = 1.f;
  float beta = 0.f;
  TA::Convolution(conv_, workspace_, core_, alpha, beta, &core_expr_);
  // Generate surface by multiplying by ID coefficient.
  auto* chandle = CublasContext::Instance().Get();
  cublasSafeCall(cublasSgemv_v2(chandle,
                                CUBLAS_OP_T,
                                core_expr_.cols(),
                                core_expr_.rows(),
                                &alpha,
                                core_expr_.data(),
                                core_expr_.cols(),
                                id_.data(),
                                1,
                                &beta,
                                core_id_expr_.data(),
                                1));
  core_id_expr_.Download(shape);
  LTS5_CUDA_STOP_PROFILE;
  return err;
}


}  // namepsace CUDA
}  // namepsace LTS5
