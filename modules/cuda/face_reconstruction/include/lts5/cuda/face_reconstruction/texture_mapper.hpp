/**
 *  @file   "cuda/face_reconstruction/texture_mapper.hpp"
 *  @brief  CUDA Implementation of TextureMapper
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   06/04/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __CUDA_TEXTURE_MAPPER__
#define __CUDA_TEXTURE_MAPPER__

#include <string>
#include <atomic>

#include "opencv2/core/core.hpp"
#include "opencv2/core/cuda.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/ogl/texture.hpp"

#include "lts5/face_reconstruction/texture_mapper.hpp"
#include "lts5/face_reconstruction/base_stream.hpp"

/** Forward cuda graphic ressources */
struct cudaGraphicsResource;
/** Forward float2 types */
struct float2;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 *  @class  TextureMapper
 *  @brief  CUDA Implementation of TextureMapper
 *  @author Christophe Ecabert
 *  @date   06/04/2017
 *  @ingroup face_reconstruction
 */
class LTS5_EXPORTS TextureMapper {
 public :

#pragma mark -
#pragma mark Type definition

  /** Mapper configuration type */
  using TextureMapperConfig = LTS5::TextureMapper::TextureMapperConfig;
  /** Type of mapping */
  using MappingType = LTS5::TextureMapper::MappingType;
#pragma mark -
#pragma mark Constructor/Destructor

  /**
   *  @name   TextureMapper
   *  @fn TextureMapper(void)
   *  @brief  Constructor
   */
  TextureMapper(void);

  /**
   *  @name   TextureMapper
   *  @fn explicit TextureMapper(const TextureMapperConfig& config)
   *  @brief  Constructor
   *  @param[in]  config  Mapper configuration
   */
  explicit TextureMapper(const TextureMapperConfig& config);

  /**
   *  @name   TextureMapper
   *  @fn TextureMapper(const TextureMapper& other) = delete
   *  @brief  Copy Constructor
   *  @param[in]  other  Object to copy from
   */
  TextureMapper(const TextureMapper& other) = delete;

  /**
   * @name  operator=
   * @fn    TextureMapper& operator=(const TextureMapper& rhs) = delete
   * @brief Assignment operator
   * @param rhs Object to assign from
   * @return    Newly assigned object
   */
  TextureMapper& operator=(const TextureMapper& rhs) = delete;

  /**
   *  @name   ~TextureMapper
   *  @fn ~TextureMapper(void)
   *  @brief  Destructor
   */
  ~TextureMapper(void);

  /**
   *  @name Load
   *  @fn int Load(const std::string& file)
   *  @brief  Load mapper configuration
   *  @param[in] file File holding configuration
   */
  int Load(const std::string& file);

  /**
   *  @name Initialize
   *  @fn void Initialize(LTS5::Mesh<float>* face_mesh)
   *  @brief  Initialize texture mapper module
   *  @param[in,out]  face_mesh Mesh used to map texture on
   */
  void Initialize(LTS5::Mesh<float>* face_mesh);

  /**
   * @name  RegisterOpenGL
   * @fn    int RegisterOpenGL(const OGLTexture* texture)
   * @brief Link cuda and opengl together
   * @param[in] texture OpenGL Texture to link to.
   * @return    -1 if error, 0 otherwise
   */
  int RegisterOpenGL(const OGLTexture* texture);

#pragma mark -
#pragma mark Process

  /**
   *  @name Process
   *  @fn void Process(const BaseStream::Buffer& images,
                       const std::vector<cv::Mat>& projection_mat,
                       const std::vector<cv::Mat>& landmarks,
                       LTS5::Mesh<float>* face_mesh)
   *  @brief  Map texture for a given set of images
   *  @param[in]  images          Set of images used to map texture
   *  @param[in]  projection_mat  Projection matrix for each views
   *  @param[in]  landmarks       List of landmarks detected by the face tracker
   *  @param[in,out]  face_mesh   Reconstructed mesh
   *  @return -1 if error, 0 otherwise
   */
  int Process(const BaseStream::Buffer& images,
              const std::vector<cv::Mat>& projection_mat,
              const std::vector<cv::Mat>& landmarks,
              LTS5::Mesh<float>* face_mesh);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name get_mapper_configuration
   *  @fn TextureMapperConfig& get_mapper_configuration(void)
   *  @brief  Provide mapper configuration
   *  @return Mapper configuration
   */
  TextureMapperConfig& get_mapper_configuration(void) {
    return mapper_config_;
  }

  /**
   *  @name set_mapper_configuration
   *  @fn void set_mapper_configuration(const TextureMapperConfig& config)
   *  @brief  Set a new configuration
   *  @param[in]  config  Configuration to set
   */
  void set_mapper_configuration(const TextureMapperConfig& config) {
    mapper_config_ = config;
  }

#pragma mark -
#pragma mark Private
 private :

  /** Vertex type */
  using Vertex = Mesh<float>::Vertex;
  /** Normal type */
  using Normal = Mesh<float>::Normal;
  /** Edge type */
  using Edge = Mesh<float>::Edge;
  /** Texture coordinate type */
  using TCoord = Mesh<float>::TCoord;
  /** Triangle type */
  using Triangle = Mesh<float>::Triangle;
  /** 2D Vector */
  using Vec2 = Vector2<float>;
  /** 3D Vector */
  using Vec3 = Vector3<float>;
  /** Camera-Normal angle treshold */
  //constexpr static float angle_thr = 0.35f;
  constexpr static float angle_thr = 0.35f;
  /** Frontal Normal angle treshold */
  constexpr static float front_angle_thr = 0.1f;

  /**
   * @struct    TexMapCorrespondance
   * @brief     Correspondance for one vertex, low->high res
   *            texture map triangulation
   */
  struct TexMapCorrespondance {
    /** High resolution triangle index */
    int tri_index;
    /** Barycentric coordinate - u */
    float u;
    /** Barycentric coordinate - v */
    float v;
    /** Barycentric coordinate - w */
    float w;
  };

  /**
   *  @name ProjectionMat2RotationMat
   *  @fn void ProjectionMat2RotationMat(const cv::Mat& proj_mat, cv::Mat* rot_mat)
   *  @brief  Extract rotation from projection matrix
   *  @param[in]  proj_mat  Projection matrix
   *  @param[out] rot_mat   Rotation matrix
   */
  void ProjectionMat2RotationMat(const cv::Mat& proj_mat, cv::Mat* rot_mat);

  /**
   *  @name SetupControlPoints
   *  @fn void SetupControlPoints(cv::cuda::Stream& stream,
                             const LTS5::Mesh<float>* mesh,
                             const std::vector<cv::Mat>& proj_mat)
   *  @brief  Setup control points onto image plane, either from face tracker
   *          detection or by projecting 3d pts
   *  @param[in]  stream    Stream to used to push data
   *  @param[in]  mesh      Where control points are stored
   *  @param[in]  proj_mat  List of projection matrix
   */
  void SetupControlPoints(cv::cuda::Stream& stream,
                          const LTS5::Mesh<float>* mesh,
                          const std::vector<cv::Mat>& proj_mat);

  /**
   * @name  ComputeTriangleMap
   * @fn    void ComputeTriangleMap(void)
   * @brief Compute a map where each pixel has the triangle number that belong
   *        to it in the texture map
   */
  void ComputeTriangleMap(void);

  /**
   * @name  DownsampleTextureMap
   * @fn void DownsampleTextureMap(const LTS5::Mesh<float>* mesh);
   * @brief Uniformly downsample the texture map and compute triangle close
   *        neighbors
   * @param[in] mesh    Mesh holding texture coordinate.
   */
  void DownsampleTextureMap(const LTS5::Mesh<float>* mesh);

  /**
   * @name  ComputeVertexVisibilityAndWeight
   * @fn    void ComputeVertexVisibilityAndWeight(cv::cuda::Stream& stream,
                                        const LTS5::Mesh<float>* mesh,
                                        const std::vector<cv::Mat>& proj_mat)
   * @brief In one pass compute the following coefficients :
   *        vertex visibility by view
   *        vertex visibility in the frontal view
   *        vertex weight for a given view
   * @param[in] stream        Cuda stream to run the computation on
   * @param[in] mesh          Reconstructed mesh
   * @param[in] proj_mat      Projection matrix for each views
   */
  void ComputeVertexVisibilityAndWeight(cv::cuda::Stream& stream,
                                        const LTS5::Mesh<float>* mesh,
                                        const std::vector<cv::Mat>& proj_mat);

  /**
   *  @name GenerateTextureMap
   *  @fn int GenerateTextureMap(const LTS5::Mesh<float>* mesh,
                            const std::vector<cv::cuda::GpuMat>& images,
                            const std::vector<cv::Mat>& proj_mat,
                            const std::vector<cv::Mat>& landmarks)
   *  @brief  Generate texture map
   *  @param[in]  mesh      Mesh from where to compute texture map
   *  @param[in]  images    Input images
   *  @param[in]  proj_mat  Projection matrix
   *  @param[in]  landmarks List of landmarks
   *  @return -1 if error, 0 otherwise
   */
  int GenerateTextureMap(const LTS5::Mesh<float>* mesh,
                            const std::vector<cv::cuda::GpuMat>& images,
                            const std::vector<cv::Mat>& proj_mat,
                            const std::vector<cv::Mat>& landmarks);

  /** Device source point - unroll for each view */
  float2* device_image_pts_;
  /** Device UV point */
  float2* device_uv_pts_;
  /** Device Image array */
  cv::cuda::PtrStepSzb* device_images_array_;
  /** Vertex visibility for frontal image + each view */
  cv::cuda::GpuMat device_vertex_view_visibility_;
  /** Vertex weight by view */
  cv::cuda::GpuMat device_vertex_view_weight_;
  /** Selection of neighboring weight vertex */
  cv::cuda::GpuMat device_vertex_weight_selection_;
  /** Triangle map */
  cv::cuda::GpuMat device_triangle_map_;
  /** Cuda - OpenGL ressources (texture map in opengl memory) */
  cudaGraphicsResource* graphic_resource_;
  /** Interpolated vertex */
  std::vector<Vec3> sampled_vertex_;
  /** Src points */
  std::vector<std::vector<Vec2>> image_pts_;
  /** Texture coordinate */
  std::vector<TCoord> tex_coordinate_;
  /** Target points, scaled texture coordinate */
  std::vector<TCoord> target_pts_;
  /** Target points, scaled texture coordinate for low resolution mesh */
  std::vector<TCoord> low_res_target_pts_;
  /** Texture map bounding box */
  AABB<float> tex_bbox_;
  /** Low resolution triangulation */
  std::vector<Triangle> low_res_triangulation_;
  /** Connectivity */
  std::vector<std::vector<int>> low_res_connectivity_;
  /** Low resultion triangulation to high resolution correspondance */
  std::vector<TexMapCorrespondance> interpolation_map_;
  /** Triangulation */
  std::vector<Triangle> triangle_list_;
  /** Extra triangle for closing eye + mouth */
  std::vector<Triangle> extra_triangle_list_;
  /** Indicate if module is initialized */
  bool  is_initialized_;
  /** Mapper configuration */
  TextureMapperConfig mapper_config_;
  /** Index of the control points for image warping */
  std::vector<int> control_pts_index_;
};

}  // namepsace CUDA
}  // namepsace LTS5
#endif //__CUDA_TEXTURE_MAPPER__
