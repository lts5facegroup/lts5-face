/**
 *  @file   "device/texture_mapper_kernel.h"
 *  @brief  Definition of CUDA kernel for texture mapping
 *  @ingroup face_reconstruction
 *
 *  @author Christophe Ecabert
 *  @date   03/05/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_TEXTURE_MAPPER_KERNEL__
#define __LTS5_TEXTURE_MAPPER_KERNEL__

#include <cuda_runtime.h>

#include "opencv2/core/cuda.hpp"
#include "opencv2/core/cuda_types.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  CUDA
 *  @brief      CUDA dev space
 */
namespace CUDA {

/**
 * @name    WarpView
 * @brief   Helper function for Warpping function
 * @ingroup face_reconstruction
 * @param[in] img_array                 Array of input images
 * @param[in] n_view                    Number of input images
 * @param[in] triangle_map              Triangle equivalent for every pixel in
 *                                      the texture map
 * @param[in] image_pts                 2D coordinate on the input image
 * @param[in] uv_pts                    Texture coordinate
 * @param[in] vertex_view_visibility    Vertex visibility for each view
 * @param[in] vertex_view_weights       Vertex weights for each view
 * @param[in] vertex_weight_selection   List of vertex to consider for a given
 *                                      triangle
 * @param[in] w_u                       RBF interpolation factor
 * @param[in] tex_size                  Texture map dimension (w, h, n_pts)
 * @param[in,out] tex_ptr               Generated texture map
 */
void WarpView(const cv::cuda::PtrStepSzb* img_array,
              const int n_view,
              const cv::cuda::GpuMat& triangle_map,
              const float2* image_pts,
              const float2* uv_pts,
              const cv::cuda::GpuMat& vertex_view_visibility,
              const cv::cuda::GpuMat& vertex_view_weights,
              const cv::cuda::GpuMat& vertex_weight_selection,
              const float w_u,
              const dim3 tex_size,
              uchar* tex_ptr);

/**
 * @name    WarpView
 * @brief   Helper function for Warpping function
 * @ingroup face_reconstruction
 * @param[in] images                    Array of input images
 * @param[in] triangle_map              Triangle equivalent for every pixel in
 *                                      the texture map
 * @param[in] image_pts                 2D coordinate on the input image
 * @param[in] uv_pts                    Texture coordinate
 * @param[in] vertex_view_visibility    Vertex visibility for each view
 * @param[in] vertex_view_weights       Vertex weights for each view
 * @param[in] vertex_weight_selection   List of vertex to consider for a given
 *                                      triangle
 * @param[in] w_u                       RBF interpolation factor
 * @param[in] tex                       Texture map dimension (w, h, n_pts)
* @param[in,out] tex_ptr                Generated texture map
 */
void WarpView(const std::vector<cv::cuda::GpuMat>& images,
              const cv::cuda::GpuMat& triangle_map,
              const float2* image_pts,
              const float2* uv_pts,
              const cv::cuda::GpuMat& vertex_view_visibility,
              const cv::cuda::GpuMat& vertex_view_weights,
              const cv::cuda::GpuMat& vertex_weight_selection,
              const float w_u,
              const dim3 tex_size,
              uchar* tex_ptr);

#pragma mark -
#pragma mark Device caller

/**
 *  @namespace  device
 *  @brief      CUDA device code space
 */
namespace device {


/**
 * @name    warp_view_kernel
 * @brief   Image warping kernel
 * @ingroup face_reconstruction
 * @param[in] img_array                 Array of input images
 * @param[in] triangle_map              Triangle equivalent for every pixel in
 *                                      the texture map
 * @param[in] image_pts                 2D coordinate on the input image
 * @param[in] uv_pts                    2D Coordinate in the texture space
 * @param[in] vertex_view_visibility    Vertex visibility for each view
 * @param[in] vertex_view_weights       Vertex weights for each view
 * @param[in] vertex_weight_selection   List of vertex to consider for a given
 *                                      triangle
 * @param[in] w_u                       RBF interpolation factor
 * @param[in] tex_size              Texture map dimension (w, h, n_pts)
 * @param[in,out] tex_ptr           Generated texture map
 */
template<int NVIEW, int NVERTEX>
__global__
void warp_view_kernel(const cv::cuda::PtrStepSzb* img_array,
                      const cv::cuda::PtrStepf triangle_map,
                      const float2* image_pts,
                      const float2* uv_pts,
                      const cv::cuda::PtrStepb vertex_view_visibility,
                      const cv::cuda::PtrStepf vertex_view_weights,
                      const cv::cuda::PtrStepi vertex_weight_selection,
                      const float w_u,
                      const dim3 tex_size,
                      uchar* tex_ptr);

/**
 * @name    warp_view_kernel_tex
 * @brief   Image warping kernel using 2D texture as image input
 * @ingroup face_reconstruction
 * @param[in] img_size                  Image dimension
 * @param[in] triangle_map              Triangle equivalent for every pixel in
 *                                      the texture map
 * @param[in] image_pts                 2D coordinate on the input image
 * @param[in] uv_pts                    2D Coordinate in the texture space
 * @param[in] vertex_view_visibility    Vertex visibility for each view
 * @param[in] vertex_view_weights       Vertex weights for each view
 * @param[in] vertex_weight_selection   List of vertex to consider for a given
 *                                      triangle
 * @param[in] w_u                       RBF interpolation factor
 * @param[in] tex_size                  Texture map dimension (w, h, n_pts)
 * @param[in,out] tex_ptr               Generated texture map
 */
template<int NVIEW, int NVERTEX>
__global__
void warp_view_kernel_tex(const dim3 img_size,
                          const cv::cuda::PtrStepf triangle_map,
                          const float2* image_pts,
                          const float2* uv_pts,
                          const cv::cuda::PtrStepb vertex_view_visibility,
                          const cv::cuda::PtrStepf vertex_view_weights,
                          const cv::cuda::PtrStepi vertex_weight_selection,
                          const float w_u,
                          const dim3 tex_size,
                          uchar* tex_ptr);


}  // namepsace device
}  // namepsace CUDA
}  // namepsace LTS5

#endif //__LTS5_TEXTURE_MAPPER_KERNEL__
