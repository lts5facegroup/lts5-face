#!/bin/bash
set -e

if [ "$#" -ne 1 ]; then
	echo "$#"
	echo "Usage:"
	echo "$0 <Folder>"
	echo "Where:"
	echo "  Folder:	Location where to place the data"	
else
	FOLDER="$1"
	IMAGE_VALIDATION_FILE_URL="http://www.image-net.org/challenges/LSVRC/2012/nnoupb/ILSVRC2012_img_val.tar"
	IMAGE_VALIDATION_FILE="${FOLDER}/ILSVRC2012_img_val.tar"
	IMAGE_VALIDATION_FOLDER="${FOLDER}/imagenet_validation"
	IMAGE_VALIDATION_MD5="29b22e2961454d5413ddabcf34fc5622"
	# IMAGE_TEST_FILE_URL="http://www.image-net.org/challenges/LSVRC/2010/download/non-pub/ILSVRC2010_images_test.tar"
	# IMAGE_TEST_FILE="${FOLDER}/ILSVRC2010_images_test.tar"
	# IMAGE_TEST_FOLDER="${FOLDER}/imagenet_testset"
	# IMAGE_PATCH_FILE_URL="http://www.image-net.org/challenges/LSVRC/2010/download/non-pub/patch_images.tar"
	# IMAGE_PATCH_FILE="${FOLDER}/patch_images.tar"
	# IMAGE_PATCH_FOLDER="${FOLDER}/imagenet_patch"
	# GROUND_TRUTH_TEST_FILE_URL="http://www.image-net.org/challenges/LSVRC/2010/ILSVRC2010_test_ground_truth.txt"
	# GROUND_TRUTH_TEST_FILE="${FOLDER}/ILSVRC2010_test_ground_truth.txt"
	# DEVKIT_FILE_URL="http://image-net.org/challenges/LSVRC/2010/download/ILSVRC2010_devkit-1.0.tar.gz"
	DEVKIT_FILE_URL="http://www.image-net.org/challenges/LSVRC/2012/nnoupb/ILSVRC2012_devkit_t12.tar.gz"
	DEVKIT_FILE="${FOLDER}/ILSVRC2012_devkit_t12.tar.gz"
	DEVKIT_FOLDER="${FOLDER}/devkit"

	md5check() {
		# https://stackoverflow.com/a/45502352/4546884
		md5_to_test=$1
		md5_from_file=$(md5sum "$2" | cut -d " " -f1)
		md5_results="Input: $md5_to_test\nFile:  $md5_from_file"
		if [[ $md5_to_test != $md5_from_file ]]; then
		  echo -e "\e[91mFAILURE\e[39m: MD5 checksum does not match"
		  return -1
		fi
	}

	# https://unix.stackexchange.com/a/216475
	open_sem(){
		mkfifo pipe-$$
		exec 3<>pipe-$$
		rm pipe-$$
		local i=$1
		for((;i>0;i--)); do
			printf %s 000 >&3
		done
	}
	run_with_lock(){
		local x
		read -u 3 -n 3 x && ((0==x)) || exit $x
		(
		"$@" 
		printf '%.3d' $? >&3
		)&
	}

	# -----------------------------------------------------------------
	# Download data required for testing 
	# -----------------------------------------------------------------
	mkdir -p ${FOLDER}
	# Download imagenet testset images
	if [ ! -f ${IMAGE_VALIDATION_FILE} ]; then
		echo "Downlading imagenet valdiation set images..."
		# Download file
		wget ${IMAGE_VALIDATION_FILE_URL} -P ${FOLDER}
		# Check MD5 checksum
		md5check ${IMAGE_VALIDATION_MD5} ${IMAGE_VALIDATION_FILE}
		echo "Done..."
	fi

	# # Download patch
	# if [ ! -f ${IMAGE_PATCH_FILE} ]; then
	# 	echo "Downlading imagenet test set patches..."
	# 	# Download file
	# 	wget ${IMAGE_PATCH_FILE_URL} -P ${FOLDER}
	# 	echo "Done..."
	# fi

	# # Download devkit
	# if [ ! -f ${DEVKIT_FILE} ]; then
	# 	echo "Downlading imagenet devkit..."
	# 	# Download file
	# 	wget ${DEVKIT_FILE_URL} -P ${FOLDER}
	# 	echo "Done..."
	# fi

	# # Download ground truth
	# if [ ! -f ${GROUND_TRUTH_TEST_FILE} ]; then
	# 	echo "Downlading imagenet test set labels..."
	# 	# Download file
	# 	wget ${GROUND_TRUTH_TEST_FILE_URL} -P ${FOLDER}
	# 	echo "Done..."
	# fi

	# -----------------------------------------------------------------
	# Extracting compressed data
	# -----------------------------------------------------------------

	if [ ! -d ${IMAGE_VALIDATION_FOLDER} ]; then
		echo "Uncompress imagenet validation set..."
		mkdir -p ${IMAGE_VALIDATION_FOLDER}
		tar -xf ${IMAGE_VALIDATION_FILE} -C ${IMAGE_VALIDATION_FOLDER}
		echo "Done..."
	fi

	# if [ ! -d ${IMAGE_PATCH_FOLDER} ]; then
	# 	echo "Uncompress imagenet patches..."
	# 	mkdir -p ${IMAGE_PATCH_FOLDER}
	# 	tar -xf ${IMAGE_PATCH_FILE} -C ${IMAGE_PATCH_FOLDER} --strip-components=1
	# 	# Apply patch
	# 	cp ${IMAGE_PATCH_FOLDER}/test/* ${IMAGE_TEST_FOLDER}
	# 	echo "Done..."
	# fi

	# if [ ! -d ${DEVKIT_FOLDER} ]; then
	# 	echo "Uncompress imagenet devkit..."
	# 	mkdir -p ${DEVKIT_FOLDER}
	# 	tar -xzf ${DEVKIT_FILE} -C ${DEVKIT_FOLDER} --strip-components=1
	# 	echo "Done..."
	# fi

	# -----------------------------------------------------------------
	# Convert grayscale images into RGB using imagemagick
	# -----------------------------------------------------------------
	convert_image_to_sRGB() {
		if [ ! -z "$(identify -verbose "$1" | grep 'Colormap entries:')" ]; then
			# Image as a palette, convert it to sRGB (remove palette)
			echo "Convert image: ${k}"
			convert $1 -colorspace sRGB -type truecolor $1
		fi
	}


	echo "Convert images using color palette into sRGB format..."
	IMAGES=$(ls ${IMAGE_VALIDATION_FOLDER})
	N=8	# 8 process at max
	open_sem $N
	for k in ${IMAGES}; do
		run_with_lock convert_image_to_sRGB "${IMAGE_VALIDATION_FOLDER}/${k}"
	done
	echo "Done"
fi










