#!/bin/bash

# Check if model is given
if [ "$#" -ne 2 ]; then
    echo "Usage"
    echo "$0 <model weights (*.npy)> <mean value (imagenet_mean.binaryproto)>"
    exit
fi

# convert *binaryproto into npy file. Using caffe framework
# https://github.com/BVLC/caffe/issues/290#issuecomment-62846228
PROTO_TO_NPY="import caffe
import numpy as np
import sys
from skimage.transform import resize

def crop_center(img, cropx, cropy):
	y,x, c = img.shape
	startx = x//2-(cropx//2)
	starty = y//2-(cropy//2)    
	return img[starty:starty+cropy, startx:startx+cropx, :]

blob = caffe.proto.caffe_pb2.BlobProto()
data = open('$2', 'rb' ).read()
blob.ParseFromString(data)
arr = np.array( caffe.io.blobproto_to_array(blob))[0]
arr = arr.astype(np.float32)
arr = np.transpose(arr, [1, 2, 0])
# crop to 227x227x3
arr = crop_center(arr, 227, 227)
# Load weights
w = np.load('$1')
w_dict = w.item()
w_dict[b'transform'.decode('utf-8')] = {'weights': arr}
w.itemset(w_dict)
# Save model
np.save('$1', w)"


# Run into docker
docker run --volume $(pwd):/workspace -ti bvlc/caffe:cpu python -c "${PROTO_TO_NPY}"
