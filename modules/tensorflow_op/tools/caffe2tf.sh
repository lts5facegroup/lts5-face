#!/bin/bash

# Check if model is given
if [ "$#" -ne 1 ]; then
    echo "Wrong number of parameters"
    echo "$0 <model_name>"
    exit
fi

BASE=`pwd`

# Build converter
if [[ "$(docker images -q cecabert/caffe2tf:1.0 2> /dev/null)" == "" ]]; then
  # Build
  echo "Build converter ..."
  docker build --file Dockerfile_caffe2tf --tag cecabert/caffe2tf:1.0 .
fi

# Convert
echo "Start conversion ..."
CMD="./caffe2tf-converter/convert.py  \
	   --caffemodel $1.caffemodel \
     --data-output-path $1.npy \
     --code-output-path $1.py \
	   $1_deploy.prototxt"
docker run --volume $(pwd):/workspace -ti cecabert/caffe2tf:1.0 ${CMD}

