#!/bin/bash

if [ "$#" -ne 2 ]; then
	echo "$#"
	echo "Usage:"
	echo "$0 <Model Folder> <ImageNet Folder>"
	echo "Where:"
	echo "  <Model Folder>:		Location where models are stored"
	echo "  <ImageNet Folder>:	Location ImageNet dataset is stored"
else
	echo ""
	echo "*** Benchmark ****"
	echo "* AlexNet"
	echo "***"
	python ../script/imagenet_benchmark.py --name AlexNet --model $1/bvlc_alexnet.npy --folder $2

	echo ""
	echo "*** Benchmark ****"
	echo "* ResNet-50"
	echo "***"
	python ../script/imagenet_benchmark.py --name ResNet50 --model $1/bvlc_resnet_50.npy --folder $2

	echo ""
	echo "*** Benchmark ****"
	echo "* ResNet-101"
	echo "***\n"
	python ../script/imagenet_benchmark.py --name ResNet101 --model $1/bvlc_resnet_101.npy --folder $2

	echo ""
	echo "*** Benchmark ****"
	echo "* ResNet-152"
	echo "***"
	python ../script/imagenet_benchmark.py --name ResNet152 --model $1/bvlc_resnet_152.npy --folder $2

	echo ""
	echo "*** Benchmark ****"
	echo "* Vgg-16"
	echo "***"
	python ../script/imagenet_benchmark.py --name Vgg16 --model $1/vgg16.npy --folder $2

	echo ""
	echo "*** Benchmark ****"
	echo "* Vgg-19"
	echo "***"
	python ../script/imagenet_benchmark.py --name Vgg19 --model $1/vgg19.npy --folder $2
fi	