# coding: utf-8
# See: https://stackoverflow.com/questions/45199643
# To be run with docker image: cecabert/caffe_ssd2tf
# Env var: PYTHONPATH=/caffe/python

import caffe
from os.path import basename, splitext
import numpy as np

prototxt = 'sfd_deploy.prototxt'
caffemodel = 'sfd.caffemodel'
npyname = splitext(basename(caffemodel))[0]

# Load model
net = caffe.Net(prototxt, caffemodel, caffe.TEST) # read the net + weights

# Iterate over every layer stored in the network
weights = {}
for i, layer in enumerate(net.layers):
  # Select only `Convolution` + `InnerProduct` layers
  name = net._layer_names[i]
  if layer.type == 'Convolution':
    params = {}
    #  Conv: (c_o, c_i, h, w) -> (h, w, c_i, c_o)
    for k, blob in enumerate(layer.blobs):
      blob_type = 'weights' if k == 0 else 'biases'
      w = blob.data
      if k == 0:
        w = w.transpose(2, 3, 1, 0)
      # Add to collection
      params[blob_type] = w
    weights[name] = params

  elif layer.type == 'InnerProduct':
    #  Inner: (c_o, c_i) -> (c_i, c_o)
    print('Skip: %s (%s)' % (name, layer.type))
  elif layer.type == 'Normalize':
    assert len(layer.blobs) == 1, 'Normalization layer must have single blob'
    weights[name] = {'alpha' : layer.blobs[0].data}
  else:
    print('Skip: %s (%s)' % (name, layer.type))

# Dump to numpy
with open(npyname + '.npy', 'wb') as f_out:
  np.save(f_out, weights)
