# coding=utf-8
from os.path import basename, exists, join, splitext
from argparse import ArgumentParser
import torch
import torch.nn as nn
import torch.nn.functional as F
import onnx
from onnx_tf.backend import prepare


class ResidualBlock(nn.Module):
  def __init__(self, in_channel, out_channel):
    super(ResidualBlock, self).__init__()
    # nbn1/nbn2/.../nbn5 abn1/abn2/.../abn5
    self.bn = nn.BatchNorm2d(in_channel)
    # nconv1/nconv2/.../nconv5 aconv1/aconv2/.../aconv5
    self.conv = nn.Conv2d(in_channel, out_channel, kernel_size=3, stride=1, padding=1)
    # nbn1r/nbn2r/.../nbn5r abn1r/abn2r/.../abn5r
    self.bnr = nn.BatchNorm2d(out_channel)
    # nconv1r/nconv2r/.../nconv5r aconv1r/aconv2r/.../anconv5r
    self.convr = nn.Conv2d(out_channel, out_channel, kernel_size=3, stride=1, padding=1)

  def forward(self, x):
    out = self.conv(F.relu(self.bn(x)))
    out = self.convr(F.relu(self.bnr(out)))
    out += x
    return out


class SfSNet(nn.Module):  # SfSNet = PS-Net in SfSNet_deploy.prototxt
  def __init__(self):
    # C64
    super(SfSNet, self).__init__()
    # TODO 初始化器 xavier
    self.conv1 = nn.Conv2d(3, 64, 7, 1, 3)
    self.bn1 = nn.BatchNorm2d(64)
    # C128
    self.conv2 = nn.Conv2d(64, 128, 3, 1, 1)
    self.bn2 = nn.BatchNorm2d(128)
    # C128 S2
    self.conv3 = nn.Conv2d(128, 128, 3, 2, 1)
    # ------------RESNET for normals------------
    # RES1
    self.n_res1 = ResidualBlock(128, 128)
    # RES2
    self.n_res2 = ResidualBlock(128, 128)
    # RES3
    self.n_res3 = ResidualBlock(128, 128)
    # RES4
    self.n_res4 = ResidualBlock(128, 128)
    # RES5
    self.n_res5 = ResidualBlock(128, 128)
    # nbn6r
    self.nbn6r = nn.BatchNorm2d(128)
    # CD128
    # TODO 初始化器 bilinear
    self.nup6 = nn.ConvTranspose2d(128, 128, 4, 2, 1, groups=128, bias=False)
    # nconv6
    self.nconv6 = nn.Conv2d(128, 128, 1, 1, 0)
    # nbn6
    self.nbn6 = nn.BatchNorm2d(128)
    # CD 64
    self.nconv7 = nn.Conv2d(128, 64, 3, 1, 1)
    # nbn7
    self.nbn7 = nn.BatchNorm2d(64)
    # C*3
    self.Nconv0 = nn.Conv2d(64, 3, 1, 1, 0)

    # --------------------Albedo---------------
    # RES1
    self.a_res1 = ResidualBlock(128, 128)
    # RES2
    self.a_res2 = ResidualBlock(128, 128)
    # RES3
    self.a_res3 = ResidualBlock(128, 128)
    # RES4
    self.a_res4 = ResidualBlock(128, 128)
    # RES5
    self.a_res5 = ResidualBlock(128, 128)
    # abn6r
    self.abn6r = nn.BatchNorm2d(128)
    # CD128
    self.aup6 = nn.ConvTranspose2d(128, 128, 4, 2, 1, groups=128, bias=False)
    # nconv6
    self.aconv6 = nn.Conv2d(128, 128, 1, 1, 0)
    # nbn6
    self.abn6 = nn.BatchNorm2d(128)
    # CD 64
    self.aconv7 = nn.Conv2d(128, 64, 3, 1, 1)
    # nbn7
    self.abn7 = nn.BatchNorm2d(64)
    # C*3
    self.Aconv0 = nn.Conv2d(64, 3, 1, 1, 0)

    # ---------------Light------------------
    # lconv1
    self.lconv1 = nn.Conv2d(384, 128, 1, 1, 0)
    # lbn1
    self.lbn1 = nn.BatchNorm2d(128)
    # lpool2r
    self.lpool2r = nn.AvgPool2d(64)
    # fc_light
    self.fc_light = nn.Linear(128, 27)

  def forward(self, inputs):
    # C64
    x = F.relu(self.bn1(self.conv1(inputs)))
    # C128
    x = F.relu(self.bn2(self.conv2(x)))
    # C128 S2
    conv3 = self.conv3(x)
    # ------------RESNET for normals------------
    # RES1
    x = self.n_res1(conv3)
    # RES2
    x = self.n_res2(x)
    # RES3
    x = self.n_res3(x)
    # RES4
    x = self.n_res4(x)
    # RES5
    nsum5 = self.n_res5(x)
    # nbn6r
    nrelu6r = F.relu(self.nbn6r(nsum5))
    # CD128
    x = self.nup6(nrelu6r)
    # nconv6/nbn6/nrelu6
    x = F.relu(self.nbn6(self.nconv6(x)))
    # nconv7/nbn7/nrelu7
    x = F.relu(self.nbn7(self.nconv7(x)))
    # nconv0
    normal = self.Nconv0(x)
    # --------------------Albedo---------------
    # RES1
    x = self.a_res1(conv3)
    # RES2
    x = self.a_res2(x)
    # RES3
    x = self.a_res3(x)
    # RES4
    x = self.a_res4(x)
    # RES5
    asum5 = self.a_res5(x)
    # nbn6r
    arelu6r = F.relu(self.abn6r(asum5))
    # CD128
    x = self.aup6(arelu6r)
    # nconv6/nbn6/nrelu6
    x = F.relu(self.abn6(self.aconv6(x)))
    # nconv7/nbn7/nrelu7
    x = F.relu(self.abn7(self.aconv7(x)))
    # nconv0
    albedo = self.Aconv0(x)
    # ---------------Light------------------
    # lconcat1, shape(1 256 64 64)
    x = torch.cat((nrelu6r, arelu6r), 1)
    # lconcat2, shape(1 384 64 64)
    x = torch.cat([x, conv3], 1)
    # lconv1/lbn1/lrelu1 shape(1 128 64 64)
    x = F.relu(self.lbn1(self.lconv1(x)))
    # lpool2r, shape(1 128 1 1)
    x = self.lpool2r(x)
    x = x.view(-1, 128)
    # fc_light
    light = self.fc_light(x)

    return normal, albedo, light


def convert_model(filename):

  # Onnx model representation
  onnx_model_name = '/opt/{}.onnx'.format(splitext(basename(filename))[0])
  if not exists(onnx_model_name):
    # Convert pytorch model to onnx, load the trained model from file
    model = SfSNet()
    map_location = (lambda storage, loc: storage.cuda()) if torch.cuda.is_available() else torch.device('cpu')
    model_state_dict = torch.load(join('opt', filename), map_location=map_location)
    model.load_state_dict(model_state_dict)
    model.eval()

    # Export the trained model to ONNX
    print('*** Export to ONNX representation ***')
    dummy_input = torch.randn(1, 3, 128, 128)
    torch.onnx.export(model,
                      dummy_input,
                      onnx_model_name,
                      do_constant_folding=True,
                      dynamic_axes={'image': {0: 'batch'},
                                    'normals': {0: 'batch'},
                                    'albedo': {0: 'batch'},
                                    'light': {0: 'batch'}},
                      input_names=['image'],
                      output_names=['normals', 'albedo', 'light'],
                      opset_version=10)

  print('*** Export to Tensorflow representation ***')
  # Export tensorflow
  model = onnx.load(onnx_model_name)

  # Check the model
  onnx.checker.check_model(model)
  print('The model is checked!')

  # Import the ONNX model to Tensorflow
  tf_rep = prepare(model)
  # Input/output nodes to the model
  print('inputs:', tf_rep.inputs)
  print('outputs:', tf_rep.outputs)

  #  Save graph to tensorflow .pb
  tf_rep.export_graph(onnx_model_name.replace('.onnx', '.pb'))


if __name__ == '__main__':
  p = ArgumentParser()
  p.add_argument('--filename',
                 type=str,
                 required=True,
                 help='Path to the pytorch model estimating normals from rgb'
                      ' image (i.e. `rgb2normal_consistency.pth`)')
  args = p.parse_args()
  convert_model(args.filename)
