# SfSNet: Learning Shape, Reflectance and Illuminance of Faces ‘in the wild’

Source: [project page](SfSNet: Learning Shape, Reflectance and Illuminance of Faces 'in the wild')

Conversion from PyTorch model into Tensorflow (*original model is in caffe*). The [repo](https://github.com/Mannix1994/SfSNet-Pytorch) has converted to Pytorch, pretrain model was taken there.

