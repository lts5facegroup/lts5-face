#!/bin/bash
set -e

# Convert ResNet-50
echo "Check ResNet-50 ..."
if [ ! -f ./caffe2tf-model/ResNet-50-model.npy ]; then
	echo "Start conversion"
	./caffe2tf.sh ./caffe2tf-model/ResNet-50-model
fi

# Convert ResNet-101
echo "Check ResNet-101 ..."
if [ ! -f ./caffe2tf-model/ResNet-101-model.npy ]; then
	echo "Start conversion"
	./caffe2tf.sh ./caffe2tf-model/ResNet-101-model
fi

# Convert ResNet-152
echo "Check ResNet-152 ..."
if [ ! -f ./caffe2tf-model/ResNet-152-model.npy ]; then
	echo "Start conversion"
	./caffe2tf.sh ./caffe2tf-model/ResNet-152-model
fi

# Convert Vgg16
echo "Check Vgg16 ..."
if [ ! -f ./caffe2tf-model/vgg16.npy ]; then
	echo "Start conversion"
	./caffe2tf.sh ./caffe2tf-model/vgg16
fi

# Convert Vgg19
echo "Check Vgg19 ..."
if [ ! -f ./caffe2tf-model/vgg19.npy ]; then
	echo "Start conversion"
	./caffe2tf.sh ./caffe2tf-model/vgg19
fi

# Convert VggFace
echo "Check VggFace ..."
if [ ! -f ./caffe2tf-model/vgg-face.npy ]; then
	echo "Start conversion"
	./caffe2tf.sh ./caffe2tf-model/vgg-face
fi

# Done
echo "Done!"