# coding=utf-8
"""
Unit Test for DynamicLandmarkOp
"""
import os
import unittest
import numpy as np
import tensorflow as tf
from tensorflow.python.ops.gradient_checker_v2 import max_error

__author__ = 'Christophe Ecabert'


class TestDynamicLandmarkOp(unittest.TestCase):
  """ Series of Unit-Test for DynamicLandmarkOp """

  def test_import_op(self):
    """
    Check if custom op package can be loaded
    """
    try:
      imported = True
      from lts5.tensorflow_op import dynamic_landmark_op as proj_op
    except ImportError as e:
      imported = False
    # Check
    self.assertTrue(imported)

  def test_backward_op(self):
    """
    Check if gradient with respect to rotation parameters is correctly
    computed
    """
    from lts5.tensorflow_op import surface_normal_op as n_op
    from lts5.tensorflow_op import dynamic_landmark_op as proj_op

    # Inputs
    f = 525.0
    width = 15
    height = 15

    # Single triangle
    z0 = -(1.75 * f)
    z1 = -(1.85 * f)
    z2 = -(2.05 * f)
    v = [-10, -7.5, z2,
         -2, -7.5, z1,
         0, -7.5, z0,
         2, -7.5, z1,
         10, -7.5, z2,
         10, 10, z2,
         2, 10, z1,
         0, 10, z0,
         -2, 10, z1,
         -10, 10, z2]
    vertex = np.asarray([v,
                         v], np.float32)
    vertex = tf.convert_to_tensor(vertex)
    tri = np.asarray([[0, 8, 9],
                      [0, 1, 8],
                      [1, 7, 8],
                      [1, 2, 7],
                      [2, 6, 7],
                      [2, 3, 6],
                      [3, 5, 6],
                      [3, 4, 5]], np.int32)
    tri = tf.convert_to_tensor(tri)
    # 0, 2, 4, 5, 9
    indices = np.asarray([[1, 0, 0, 0],
                          [1, 2, 0, 0],
                          [3, 2, 3, 4],
                          [1, 5, 0, 0],
                          [1, 9, 0, 0]], np.int32)
    indices = tf.convert_to_tensor(indices)


    normal, _ = n_op(vertex=vertex, triangle=tri)
    v = tf.reshape(vertex, (2, -1, 3))
    n = tf.reshape(normal, (2, -1, 3))

    # Forward
    # @tf.function
    def _proj_op(vertex):
      lms, idx = proj_op(vertex=vertex,
                         normal=n,
                         focal=f,
                         height=height,
                         width=width,
                         indices=indices,
                         top_left=True)
      return lms

    # Derivative of the image with respect to color input
    theo, num = tf.test.compute_gradient(_proj_op, [v], delta=1e-2)
    # Compare structure
    dlms_dv_e = max_error(theo[0], num[0])
    self.assertLess(dlms_dv_e, 1e-4)


if __name__ == '__main__':
  # Run test
  unittest.main()
