# coding=utf-8
"""
Unit Test for SurfaceNormalOp
"""
import os
import unittest
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'  # Set C++ logging level to warning
import tensorflow as tf
from lts5.utils import RodriguesFloat as Rodrigues

__author__ = 'Christophe Ecabert'


class TestSurfaceNormalOp(unittest.TestCase):
  """ Series of Unit-Test for SurfaceNormalOp """

  def test_import_op(self):
    """
    Check if custom op package can be loaded
    """
    try:
      imported = True
      from lts5.tensorflow_op import surface_normal_op as surf_normal_op
    except ImportError as e:
      imported = False
    # Check
    self.assertTrue(imported)

  def test_inputs(self):
    """ Check input validity """
    from lts5.tensorflow_op import surface_normal_op as surf_normal_op

    def _run_test():
      # Wrong rank - vertex
      tri = np.asarray([[0, 1, 3]], dtype=np.int32)
      vertex = np.asarray([0.0, 0.0, 0.0,
                           0.0, 0.0, 1.0,
                           1.0, 0.0, 1.0], dtype=np.float32)
      self.assertRaises(tf.errors.InvalidArgumentError,
                        surf_normal_op,
                        vertex=vertex,
                        triangle=tri)
      # Vertex is not a multiple of 3
      vertex = np.asarray([[0.0, 0.0, 0.0,
                            0.0, 0.0, 1.0,
                            1.0, 0.0, 1.0, 0.5]], dtype=np.float32)
      self.assertRaises(tf.errors.InvalidArgumentError,
                        surf_normal_op,
                        vertex=vertex,
                        triangle=tri)
      # Triangle is not rank 2
      tri = np.asarray([0, 1, 3], dtype=np.int32)
      vertex = np.asarray([[0.0, 0.0, 0.0,
                            0.0, 0.0, 1.0,
                            1.0, 0.0, 1.0]], dtype=np.float32)
      self.assertRaises(tf.errors.InvalidArgumentError,
                        surf_normal_op,
                        vertex=vertex,
                        triangle=tri)
    # https://stackoverflow.com/a/50144255
    with tf.Session() as sess:
      # Run test in eager
      sess.run(tf.py_function(_run_test, [], []))

  def test_forward_op(self):
    """
    Check if forward op can properly generates rotation matrix
    """
    from lts5.tensorflow_op import surface_normal_op as surf_normal_op

    def _run_test():
      def _generate_surface(axis, angle):
        """
        Generate a square of 4 vertices laying on a plane. Apply a given rotation
        on it.
        :param axis:  Rotation axis
        :param angle: Rotation angle
        :return:  tuple(vertex, triangle)
        """
        v = np.asarray([[-1.0, 0.0, -1.0],
                        [-1.0, 0.0, 1.0],
                        [1.0, 0.0, 1.0],
                        [1.0, 0.0, -1.0]], dtype=np.float32)
        tri = np.asarray([[0, 1, 3],
                          [1, 2, 3]], dtype=np.int32)
        # Build ground truth at the same time
        n = np.asarray([[0.0, 1.0, 0.0],
                        [0.0, 1.0, 0.0],
                        [0.0, 1.0, 0.0],
                        [0.0, 1.0, 0.0]], dtype=np.float32)

        # Define rotation matrix
        rmat = Rodrigues(axis=axis, angle=angle).to_rotation_matrix()[:3, :3]
        vt = v @ rmat.T
        nt = n @ rmat.T
        return vt.reshape(1, -1), nt.reshape(1, -1), tri,

      # No rotation
      vertex, true_normal, tri = _generate_surface(axis=[1.0, 0.0, 0.0],
                                                   angle=0.0)
      normals, scaling = surf_normal_op(vertex=vertex, triangle=tri)
      self.assertTrue(np.allclose(normals, true_normal))

      # 45° around X axis
      vertex, true_normal, tri = _generate_surface(axis=[1.0, 0.0, 0.0],
                                                   angle=np.deg2rad(45.0))
      normals, scaling = surf_normal_op(vertex=vertex, triangle=tri)
      self.assertTrue(np.allclose(normals, true_normal))

      # 30° around Y axis
      vertex, true_normal, tri = _generate_surface(axis=[0.0, 1.0, 0.0],
                                                   angle=np.deg2rad(30.0))
      normals, scaling = surf_normal_op(vertex=vertex, triangle=tri)
      self.assertTrue(np.allclose(normals, true_normal))

      # -90° around Z axis
      vertex, true_normal, tri = _generate_surface(axis=[0.0, 0.0, 1.0],
                                                   angle=np.deg2rad(-90.0))
      normals, scaling = surf_normal_op(vertex=vertex, triangle=tri)
      self.assertTrue(np.allclose(normals, true_normal))
    # https://stackoverflow.com/a/50144255
    with tf.Session() as sess:
      # Run test in eager
      sess.run(tf.py_function(_run_test, [], []))

  def test_backward_op(self):
    """
    Check if gradient with respect to rotation parameters is correctly
    computed
    """
    from lts5.tensorflow_op import surface_normal_op as surf_normal_op

    v = np.asarray([[-1.0, 1.0, -1.0,
                     -1.0, 1.0, 1.0,
                     1.0, -1.0, 1.0,
                     1.0, -1.0, -1.0]],
                   dtype=np.float32)
    tri = np.asarray([[0, 1, 2],
                      [0, 2, 3]], dtype=np.int32)

    v_ph = tf.placeholder(shape=v.shape, dtype=tf.float32)
    tri_ph = tf.placeholder(shape=tri.shape, dtype=tf.int32)
    n, scaling = surf_normal_op(vertex=v_ph, triangle=tri)

    sess = tf.Session()
    with sess.as_default():
      # Init
      sess.run(tf.global_variables_initializer())
      # dColor_d_reflectance
      p = {v_ph: v,
           tri_ph: tri}

      dn_dv_e = tf.test.compute_gradient_error(y=n,
                                               y_shape=n.shape.as_list(),
                                               x=v_ph,
                                               x_shape=v_ph.shape.as_list(),
                                               x_init_value=v,
                                               extra_feed_dict=p,
                                               delta=5e-3)
      self.assertLessEqual(dn_dv_e, 1e-4)


if __name__ == '__main__':
  # Run test
  unittest.main()
