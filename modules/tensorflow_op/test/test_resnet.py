#!/usr/bin/env python
""" Sample showing how to create an ResNet instance and use it to perform
image classification """
import numpy as np
import tensorflow as tf
import cv2
import lts5.utils as utils
from lts5.tensorflow_op.network.resnet import ResNet, ResNetSize, ResNetVersion
from lts5.tensorflow_op.network.network import DataFormat
from lts5.tensorflow_op.data.label import get_imagenet_text_labels
__author__ = 'Christophe Ecabert'

# Sanity check
utils.check_py_version(3, 0)
logger = utils.init_logger()
# Define the path to resnet model
tf.app.flags.DEFINE_string('model',
                           'bvlc_resnet_50.npy',
                           'Path to the *npy file storing ResNet parameters')
tf.app.flags.DEFINE_string('image', 'laska.png', 'Image path')
# Flags
FLAGS = tf.app.flags.FLAGS
# mean of imagenet dataset in BGR
resnet_mean = np.array([103.939, 116.779, 123.68], dtype=np.float32)


def main(_):
  """
  Main function
  """
  # Load image, remove mean, convert to BGR
  image = (cv2.imread(FLAGS.image)[:, :, :3]).astype(np.float32)

  # Download ImageNet label
  labels = get_imagenet_text_labels()

  # Create network
  with tf.device('gpu:0'):
    # Initializer
    init = tf.global_variables_initializer()

    # Input
    x = tf.placeholder(tf.float32, shape=[1, 224, 224, 3], name='Input')

    # Model
    net = ResNet(ResNetSize.Size50,
                 ResNetVersion.v1,
                 DataFormat.NHWC,
                 1000)
    softmax = net.inference(x, 'ResNet')

  # Create computational session
  with tf.Session() as sess:
    # Initialize all variables
    sess.run(init)

    # Graph writer
    writer = tf.summary.FileWriter('output-resnet', sess.graph)

    # Init network
    net.restore(session=sess, path=FLAGS.model)

    # Convert image to float32 and resize to (224x224)
    img = cv2.resize(image, (224, 224))

    # Subtract the ImageNet mean
    img -= resnet_mean

    # Reshape as needed to feed into model
    img = img.reshape((1, 224, 224, 3))

    # Run the session and calculate the class probability
    #op = net.op('conv1')
    probs = sess.run(softmax, feed_dict={x: img})

    # Get the class name of the class with the highest probability
    max_idx = np.argmax(probs)
    label = labels[max_idx]

    # Dump result
    logger.info('Class: {}, probability: {:.2f} '
                .format(label, probs[0, max_idx]))
    # Done
    writer.close()


if __name__ == '__main__':
  tf.app.run()
