# coding=utf-8
"""
Unit Test for MorphableModelOp
"""
import sys
import unittest
import argparse
import numpy as np
import tensorflow as tf
__author__ = 'Christophe Ecabert'


model_path = ''
mesh_path = ''


class TestMorphableModelOp(unittest.TestCase):
  """ Series of Unit-Test for MorphableModelOp """

  def test_import_op(self):
    """
    Check if custom op package can be loaded
    """
    try:
      imported = True
      from lts5.tensorflow_op import morphable_model_op as mm_op
    except ImportError as e:
      imported = False
    # Check
    self.assertTrue(imported)

  # def test_load_model(self):
  #   """
  #   Check if op can load model
  #   """
  #   from lts5.tensorflow_op import morphable_model_op as mm_op
  # 
  #   # Call function
  #   self.assertRaises(tf.errors.InvalidArgumentError,
  #                     mm_op,
  #                     path='',
  #                     w_shp=np.zeros((1, 120), dtype=np.float32),
  #                     w_tex=np.zeros((1, 80), dtype=np.float32))

  # def test_inputs(self):
  #   """
  #   Check input validity
  #   """
  #   import tensorflow as tf
  #   tf.enable_eager_execution()
  #   from lts5.tensorflow_op import morphable_model_op as mm_op
  #
  #   # Wrong shape dims
  #   self.assertRaises(tf.errors.InvalidArgumentError,
  #                     mm_op,
  #                     path=model_path,
  #                     w_shp=np.zeros((1, 100), dtype=np.float32),
  #                     w_tex=np.zeros((1, 80), dtype=np.float32))
  #   # Wrong tex dims
  #   self.assertRaises(tf.errors.InvalidArgumentError,
  #                     mm_op,
  #                     path=model_path,
  #                     w_shp=np.zeros((1, 120), dtype=np.float32),
  #                     w_tex=np.zeros((1, 60), dtype=np.float32))
  #   # Inconsistent batch size
  #   self.assertRaises(tf.errors.InvalidArgumentError,
  #                     mm_op,
  #                     path=model_path,
  #                     w_shp=np.zeros((2, 120), dtype=np.float32),
  #                     w_tex=np.zeros((1, 80), dtype=np.float32))
  #
  # def test_forward_op(self):
  #   """
  #   Check if forward op can properly generates surface/color/normal
  #   """
  #   from lts5.geometry import MeshFloat as Mesh
  #   from lts5.tensorflow_op import morphable_model_op as mm_op
  #
  #   # generate surface meanshape + meantex
  #   w_shp = np.zeros((1, 120), dtype=np.float32)
  #   w_tex = np.zeros((1, 80), dtype=np.float32)
  #   vertex, color, tri = mm_op(path=model_path,
  #                              w_shp=w_shp,
  #                              w_tex=w_tex)
  #   vertex = vertex.numpy().reshape(-1, 3)
  #   #normal = normal.numpy().reshape(-1, 3)
  #   color = color.numpy().reshape(-1, 3)
  #   tri = tri.numpy()
  #   # Load "ground truth"
  #   m = Mesh()
  #   m.load(filename=mesh_path)
  #   m.compute_vertex_normal()
  #   # Check
  #   self.assertTrue(np.allclose(vertex, m.vertex))
  #   #self.assertTrue(np.allclose(normal, m.normal, atol=1.1e-3))
  #   self.assertTrue(np.allclose(color, m.vertex_color, atol=5e-3))
  #   self.assertTrue(np.allclose(tri, m.tri))

  def test_backward_op(self):
    """
    Check if gradient with respect to shape/texture parameters is correctly
    computed
    """
    import tensorflow as tf
    from lts5.tensorflow_op import morphable_model_op as mm_op
    # Model dimensions
    ns = 120
    nt = 80
    # Input
    w_shp = tf.placeholder(shape=(1, ns), dtype=tf.float32)
    w_tex = tf.placeholder(shape=(1, nt), dtype=tf.float32)
    vertex, color, tri = mm_op(path=model_path, w_shp=w_shp, w_tex=w_tex)

    sess = tf.Session()
    with sess.as_default():
      # Init
      sess.run(tf.global_variables_initializer())
      # dVertex_d_ws
      p = {w_shp: np.zeros((1, ns), dtype=np.float32),
           w_tex: np.zeros((1, nt), dtype=np.float32)}
      dV_dws_e = tf.test.compute_gradient_error(y=vertex,
                                                y_shape=vertex.shape.as_list(),
                                                x=w_shp,
                                                x_shape=w_shp.shape.as_list(),
                                                delta=1e-6,
                                                extra_feed_dict=p)

      dC_dwt_e = tf.test.compute_gradient_error(y=color,
                                                y_shape=color.shape.as_list(),
                                                x=w_tex,
                                                x_shape=w_tex.shape.as_list(),
                                                delta=1e-6,
                                                extra_feed_dict=p)


      a = 0



if __name__ == '__main__':
  # parse input
  parser = argparse.ArgumentParser(add_help=False)

  # Model
  parser.add_argument('--model',
                      type=str,
                      required=True,
                      help='Path to the morphable model file')
  # Reference mesh
  parser.add_argument('--mesh',
                      type=str,
                      required=True,
                      help='Path to the basel face model mesh (*.ply)')

  # Parse
  args, extra_args = parser.parse_known_args()
  model_path = args.model
  mesh_path = args.mesh

  # Run test
  unittest.main(argv=sys.argv[:1] + extra_args)
