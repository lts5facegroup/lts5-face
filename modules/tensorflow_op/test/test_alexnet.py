#!/usr/bin/env python
""" Sample showing how to create an AlexNet instance and use it to perform
image classification """
import numpy as np
import tensorflow as tf
from imageio import imread
import lts5.utils as utils
from lts5.tensorflow_op import AlexNet
from lts5.tensorflow_op.data.label import get_imagenet_text_labels
__author__ = 'Christophe Ecabert'

# Sanity check
utils.check_py_version(3, 0)
logger = utils.init_logger()
# Define the path to alexnet model
tf.app.flags.DEFINE_string('model',
                           'bvlc_alexnet.npy',
                           'Path to the *npy file storing AlexNet parameters')
tf.app.flags.DEFINE_string('image', 'laska.png', 'Image path')
# Flags
FLAGS = tf.app.flags.FLAGS
# mean of imagenet dataset in BGR
imagenet_mean = np.array([103.939, 116.779, 123.68], dtype=np.float32)


def main(_):
  """
  Main function
  """

  # Load image
  img = imread(FLAGS.image).astype(np.float32)
  # Download ImageNet label
  labels = get_imagenet_text_labels()

  # Model
  net = AlexNet(1000, 1.0)
  # Input container + preprocessing
  ph_img = tf.placeholder(dtype=tf.float32, shape=img.shape, name='Input')
  # Define pre-processing
  in_layer = tf.expand_dims(net.input_fn(ph_img, None)[0], axis=0)

  # Create network
  with tf.device('gpu:0'):
    # Forward
    softmax = net.inference(in_layer, 'AlexNet')

  # Create computational session
  with tf.Session() as sess:
    # Initialize all variables
    sess.run(tf.global_variables_initializer())
    # Init network
    net.restore(session=sess, path=FLAGS.model)

    # Run the session and calculate the class probability
    probs = sess.run(softmax, feed_dict={ph_img: img})

    # Get the class name of the class with the highest probability
    max_idx = np.argmax(probs)
    label = labels[max_idx]

    # Dump result
    logger.info('Class: {}, probability: {:.2f} '
                .format(label, probs[0, max_idx]))


if __name__ == '__main__':
  tf.app.run()
