# coding=utf-8
"""
Unit Test for ShLightingOp
"""
import unittest
import numpy as np
import tensorflow as tf


__author__ = 'Christophe Ecabert'


class TestShLightingOp(unittest.TestCase):
  """ Series of Unit-Test for ShLightingOp """

  def test_import_op(self):
    """
    Check if custom op package can be loaded
    """
    try:
      imported = True
      from lts5.tensorflow_op import sh_lighting_op as sh_op
    except ImportError:
      imported = False
    # Check
    self.assertTrue(imported)

  def test_inputs(self):
    """ Check input validity """
    from lts5.tensorflow_op import sh_lighting_op as sh_op

    def _run_test():
      # Wrong rank
      # Reflectance
      self.assertRaises(tf.errors.InvalidArgumentError,
                        sh_op,
                        reflectance=np.asarray([1.0, 0.0, 0.0],
                                               np.float32),
                        normal=np.asarray([[1.0, 0.0, 0.0]],
                                          np.float32),
                        w_illu=np.asarray([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],
                                          np.float32))
      # Normal
      self.assertRaises(tf.errors.InvalidArgumentError,
                        sh_op,
                        reflectance=np.asarray([[1.0, 0.0, 0.0]],
                                               np.float32),
                        normal=np.asarray([1.0, 0.0, 0.0],
                                          np.float32),
                        w_illu=np.asarray([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],
                                          np.float32))
      # Willu
      self.assertRaises(tf.errors.InvalidArgumentError,
                        sh_op,
                        reflectance=np.asarray([[1.0, 0.0, 0.0]],
                                               np.float32),
                        normal=np.asarray([[1.0, 0.0, 0.0]],
                                          np.float32),
                        w_illu=np.asarray([1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                           1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                           1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                          np.float32))

      # Wrong dims
      # Reflectance
      self.assertRaises(tf.errors.InvalidArgumentError,
                        sh_op,
                        reflectance=np.asarray([[1.0, 0.0, 0.0, 0.0]],
                                               np.float32),
                        normal=np.asarray([[1.0, 0.0, 0.0]],
                                          np.float32),
                        w_illu=np.asarray([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],
                                          np.float32))
      # Normal
      self.assertRaises(tf.errors.InvalidArgumentError,
                        sh_op,
                        reflectance=np.asarray([[1.0, 0.0, 0.0]],
                                               np.float32),
                        normal=np.asarray([[1.0, 0.0, 0.0, 0.0]],
                                          np.float32),
                        w_illu=np.asarray([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],
                                          np.float32))
      # Willu
      self.assertRaises(tf.errors.InvalidArgumentError,
                        sh_op,
                        reflectance=np.asarray([[1.0, 0.0, 0.0]],
                                               np.float32),
                        normal=np.asarray([[1.0, 0.0, 0.0]],
                                          np.float32),
                        w_illu=np.asarray([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],
                                          np.float32))
      # Wrong dims - miss match reflectance normal
      self.assertRaises(tf.errors.InvalidArgumentError,
                        sh_op,
                        reflectance=np.asarray([[1.0, 0.0, 0.0, 0.0, 1.0, 0.0]],
                                               np.float32),
                        normal=np.asarray([[1.0, 0.0, 0.0]],
                                          np.float32),
                        w_illu=np.asarray([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],
                                          np.float32))
      # Wrong dims - miss match normal-w_illu
      self.assertRaises(tf.errors.InvalidArgumentError,
                        sh_op,
                        reflectance=np.asarray([[1.0, 0.0, 0.0]],
                                               np.float32),
                        normal=np.asarray([[1.0, 0.0, 0.0, 1.0, 0.0, 0.0]],
                                          np.float32),
                        w_illu=np.asarray([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],
                                          np.float32))
    # # https://stackoverflow.com/a/50144255
    # with tf.Session() as sess:
    #   # Run test in eager
    #   sess.run(tf.py_function(_run_test, [], []))
    _run_test()

  def test_forward_op(self):
    """
    Check if forward op can properly generates rotation matrix
    """
    from lts5.tensorflow_op import sh_lighting_op as sh_op

    def _run_test():
      def _compute_color_with_light(r, n, wi):
        n = n.reshape(-1, 3)
        r = r.reshape(-1, 3)
        wi = wi.reshape(-1, 9)
        color = []
        for ni, ri in zip(n, r):
          nx = ni[0]
          ny = ni[1]
          nz = ni[2]
          nxy = nx * ny
          nxz = nx * nz
          nyz = ny * nz
          nx2_ny2 = (nx ** 2.0) - (ny ** 2.0)
          nz2_1 = 3.0 * (nz ** 2.0) - 1.0
          basis = np.asarray([[1.0, nx, ny, nz, nxy, nxz, nyz, nx2_ny2, nz2_1]],
                             np.float32)
          sh_coef = basis @ wi.T
          # Color
          c = ri * sh_coef
          color.append(c)
        return np.vstack(color)

      # Single entry
      r = np.asarray([[0.7, 0.1, 0.2, 0.3, 0.7, 1.0, 0.4, 0.6, 0.7]], np.float32)
      n = np.asarray([[1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]], np.float32)
      wi = np.asarray([[1.0, 0.1, 0.1, 0.4, 0.0, 0.0, 0.0, 0.0, 0.0,
                        1.0, 0.2, 0.4, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0,
                        1.0, 0.3, 0.2, 0.7, 0.0, 0.0, 0.0, 0.0, 0.0]],
                      np.float32)
      color = sh_op(reflectance=r, normal=n, w_illu=wi)
      gt_color = _compute_color_with_light(r=r, n=n, wi=wi).reshape(1, -1)
      self.assertTrue(np.allclose(color, gt_color))

      # Two entries
      r = np.asarray([[0.7, 0.1, 0.2, 0.3, 0.7, 1.0, 0.4, 0.6, 0.7],
                      [0.1, 0.7, 0.4, 0.5, 0.1, 0.8, 0.2, 0.3, 0.1]], np.float32)
      n = np.asarray([[1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0],
                      [0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]], np.float32)
      wi = np.asarray([[1.0, 0.1, 0.1, 0.4, 0.0, 0.0, 0.0, 0.0, 0.0,
                        1.0, 0.2, 0.4, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0,
                        1.0, 0.3, 0.2, 0.7, 0.0, 0.0, 0.0, 0.0, 0.0],
                       [0.7, 0.4, 0.2, 0.4, 0.0, 0.0, 0.0, 0.0, 0.0,
                        0.7, 0.2, 0.7, 0.2, 0.0, 0.0, 0.0, 0.0, 0.0,
                        0.7, 0.15, 0.4, 0.7, 0.0, 0.0, 0.0, 0.0, 0.0]],
                      np.float32)
      color = sh_op(reflectance=r, normal=n, w_illu=wi)
      gt_color = np.vstack([_compute_color_with_light(r=ri, n=ni, wi=wii).reshape(1, -1)
                            for ri, ni, wii in zip(r, n, wi)])
      self.assertTrue(np.allclose(color, gt_color))

    # # https://stackoverflow.com/a/50144255
    # with tf.Session() as sess:
    #   # Run test in eager
    #   sess.run(tf.py_function(_run_test, [], []))
    _run_test()

  def test_backward_op(self):
    """
    Check if gradient with respect to rotation parameters is correctly
    computed
    """
    from lts5.tensorflow_op import sh_lighting_op as sh_op

    a = 1.0 / (3.0 ** 0.5)
    # Two entries
    r = np.asarray([[0.3, 0.6, 0.1, 0.3, 0.7, 1.0, 0.4, 0.6, 0.7],
                    [0.1, 0.7, 0.4, 0.5, 0.1, 0.8, 0.2, 0.3, 0.1]], np.float32)
    n = np.asarray([[a, a, a, -a, -a, a, a, a, -a],
                    [-a, -a, -a, a, -a, -a, -a, a, -a]], np.float32)
    wi = np.asarray([[1.0, 0.1, 0.1, 0.4, 0.1, 0.2, 0.3, 0.4, 0.5,
                      1.0, 0.2, 0.4, 0.2, 0.5, 0.4, 0.3, 0.2, 0.1,
                      1.0, 0.3, 0.2, 0.7, 0.08, 0.1, 0.05, 0.4, 0.6],
                     [0.7, 0.4, 0.2, 0.4, 0.05, 0.01, 0.02, 0.03, 0.01,
                      0.7, 0.2, 0.7, 0.2, 0.03, 0.04, 0.05, 0.06, 0.07,
                      0.7, 0.15, 0.4, 0.1, 0.07, 0.09, 0.1, 0.8, 0.1]],
                    np.float32)

    @tf.function
    def _sh_light(reflectance, normal, params):
      return sh_op(reflectance=reflectance,
                   normal=normal,
                   w_illu=params)

    def _compute_error(theoretical, numerical):
      if not isinstance(theoretical, tuple):
        theoretical = [theoretical]
        numerical = [numerical]
      error = 0
      for j_t, j_n in zip(theoretical, numerical):
        if j_t.size or j_n.size:  # Handle zero size tensors correctly
          error = np.maximum(error, np.fabs(j_t - j_n).max())
      return error

    # dCol_dr
    theo, num = tf.test.compute_gradient(_sh_light, [r, n, wi])
    dCol_dr_e = _compute_error(theo[0], num[0])
    self.assertLessEqual(dCol_dr_e, 1e-4)
    # dCol_dn
    dCol_dn_e = _compute_error(theo[1], num[1])
    self.assertLessEqual(dCol_dn_e, 1e-4)
    # dCol_dwi
    dCol_dwi_e = _compute_error(theo[2], num[2])
    self.assertLessEqual(dCol_dwi_e, 1e-4)


if __name__ == '__main__':
  # Run test
  unittest.main()
