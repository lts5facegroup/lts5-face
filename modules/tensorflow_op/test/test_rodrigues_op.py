# coding=utf-8
"""
Unit Test for RodriguesOp
"""
import unittest
import numpy as np
import tensorflow as tf
from lts5.utils import RodriguesFloat as Rodrigues

__author__ = 'Christophe Ecabert'


class TestRodriguesOp(unittest.TestCase):
  """ Series of Unit-Test for RodriguesOp """

  def test_import_op(self):
    """
    Check if custom op package can be loaded
    """
    try:
      imported = True
      from lts5.tensorflow_op import rodrigues_op as r_op
    except ImportError as e:
      imported = False
    # Check
    self.assertTrue(imported)

  def test_inputs(self):
    """ Check input validity """
    from lts5.tensorflow_op import rodrigues_op as r_op

    def _run_test():
      # Wrong rank
      rvec = np.asarray([0.0, 0.0, 0.0], dtype=np.float32)
      self.assertRaises(tf.errors.InvalidArgumentError,
                        r_op,
                        rvec=rvec)
      # Wrong number of parameters
      rvec = np.asarray([[0.0, 0.0, 0.0, 0.0]], dtype=np.float32)
      self.assertRaises(tf.errors.InvalidArgumentError,
                        r_op,
                        rvec=rvec)

    # https://stackoverflow.com/a/50144255
    with tf.Session() as sess:
      # Run test in eager
      sess.run(tf.py_function(_run_test, [], []))

  def test_forward_op(self):
    """
    Check if forward op can properly generates rotation matrix
    """
    from lts5.tensorflow_op import rodrigues_op as r_op

    def _run_test():
      def _generate_rvec(n=1):
        rvecs = []
        rod = []
        for k in range(n):
          # Axis
          axis = np.random.uniform(-1.0, 1.0, size=3)
          axis /= np.linalg.norm(axis)
          # Angle
          ang = np.deg2rad(np.random.uniform(-180, 180))
          # Rodrigues
          r = Rodrigues(axis=axis, angle=ang)
          rod.append(r)
          rvecs.append(np.asarray([r.x, r.y, r.z], dtype=np.float32))
        return np.stack(rvecs), rod

      # Single matrix
      rvec, rodri = _generate_rvec(n=1)
      rmat = r_op(rvec=rvec).numpy()
      gt_rmat = np.expand_dims(rodri[0].to_rotation_matrix()[:3, :3],
                               axis=0)
      self.assertTrue(np.allclose(rmat,
                                  gt_rmat,
                                  atol=1e-6),
                                  msg='R={}\n\n GT={}'.format(rmat, gt_rmat))

      # Multiple matrix
      rvec, rodri = _generate_rvec(n=5)
      rmat = r_op(rvec=rvec).numpy()
      gt_rmat = np.stack([r.to_rotation_matrix()[:3, :3] for r in rodri])
      self.assertTrue(np.allclose(rmat,
                                  gt_rmat,
                                  atol=1e-6),
                                  msg='R={}\n\n GT={}'.format(rmat, gt_rmat))

    # https://stackoverflow.com/a/50144255
    with tf.Session() as sess:
      # Run test in eager
      sess.run(tf.py_function(_run_test, [], []))

  def test_backward_op(self):
    """
    Check if gradient with respect to rotation parameters is correctly
    computed
    """
    from lts5.tensorflow_op import rodrigues_op as r_op

    # Input
    rvec = tf.placeholder(shape=(2, 3), dtype=tf.float32)
    rmat = r_op(rvec=rvec, use_identity=False)

    sess = tf.Session()
    with sess.as_default():
      # Init
      sess.run(tf.global_variables_initializer())
      # dVertex_d_ws
      rvec_init = np.asarray([[0.0, 0.0, 0.0],
                             [0.1, 0.2, 0.3]], dtype=np.float32)
      p = {rvec: rvec_init}
      dR_dq_e = tf.test.compute_gradient_error(y=rmat,
                                               y_shape=rmat.shape.as_list(),
                                               x=rvec,
                                               x_init_value=rvec_init,
                                               x_shape=rvec.shape.as_list(),
                                               extra_feed_dict=p)
      self.assertLessEqual(dR_dq_e, 1e-4)


if __name__ == '__main__':
  # Run test
  unittest.main()
