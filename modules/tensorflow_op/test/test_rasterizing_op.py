# coding=utf-8
"""
Unit Test for ShLightingOp
"""
import unittest
import numpy as np
import tensorflow as tf


__author__ = 'Christophe Ecabert'


class TestRasterizingOp(unittest.TestCase):
  """ Series of Unit-Test for RasterizingOp """

  def test_import_op(self):
    """
    Check if custom op package can be loaded
    """
    try:
      imported = True
      from lts5.tensorflow_op import rasterizing_op as raster_op
    except ImportError:
      imported = False
    # Check
    self.assertTrue(imported)

  def test_backward_op(self):
    """
    Check if gradient with respect to rotation parameters is correctly
    computed
    """
    from lts5.tensorflow_op import rasterizing_op as raster_op
    from lts5.tensorflow_op.layers import RigidTransform

    vertex = [[-50.0, 50.0, 50.0],
              [-50.0, -50.0, 50.0],
              [50.0, 50.0, 50.0],
              [50.0, -50.0, 50.0],
              [-50.0, 50.0, -50.0],
              [-50.0, -50.0, -50.0],
              [50.0, 50.0, -50.0],
              [50.0, -50.0, -50.0]]

    tri = [[1, 3, 2],
           [0, 1, 2],
           [4, 6, 5],
           [6, 7, 5],
           [2, 3, 6],
           [3, 7, 6],
           [0, 4, 5],
           [0, 5, 1]]
    tri = tf.convert_to_tensor(tri, dtype=tf.int32)
    vertex = tf.convert_to_tensor(vertex, dtype=tf.float32)
    vertex = tf.reshape(vertex, (1, -1))

    rt = RigidTransform()
    axis = np.asarray([[0.0, 1.0, 0.0]], dtype=np.float32)
    axis /= np.linalg.norm(axis)
    ang = np.deg2rad(-40.0)
    rvec = tf.convert_to_tensor(axis * ang)
    tvec = tf.convert_to_tensor([[0.0, 0.0, -500.0]])
    vertex = rt([vertex, rvec, tvec])

    # Rasterizer parameters
    h = 16
    w = 16
    focal = 37.5
    near = 1e2
    far = 2e3

    @tf.function
    def _rasterize(vertex):
      rasterized = raster_op(width=w,
                             height=h,
                             near=near,
                             far=far,
                             focal=focal,
                             vertex=vertex,
                             triangle=tri)
      # Convert output
      # tri_index = tf.cast(rasterized[..., 0] - 1.0, tf.int32)
      # bcoords = rasterized[..., 1:]
      # mask = tf.cast(tri_index >= 0, tf.int32)
      # return tri_index, bcoords, mask
      return rasterized

    def _compute_error(theoretical, numerical):
      if not isinstance(theoretical, tuple):
        theoretical = [theoretical]
        numerical = [numerical]
      error = 0
      for j_t, j_n in zip(theoretical, numerical):
        if j_t.size or j_n.size:  # Handle zero size tensors correctly
          error = np.maximum(error, np.fabs(j_t - j_n).max())
      return error

    # dCol_dr
    theo, num = tf.test.compute_gradient(_rasterize, [vertex], delta=1e-4)

    a = 0


    dCol_dr_e = _compute_error(theo[0], num[0])
    self.assertLessEqual(dCol_dr_e, 1e-4)
    # dCol_dn
    dCol_dn_e = _compute_error(theo[1], num[1])
    self.assertLessEqual(dCol_dn_e, 1e-4)
    # dCol_dwi
    dCol_dwi_e = _compute_error(theo[2], num[2])
    self.assertLessEqual(dCol_dwi_e, 1e-4)


if __name__ == '__main__':
  # Run test
  unittest.main()
