# coding=utf-8
"""
Unit Test for RenderingOp
"""
import os
import unittest
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'  # Set C++ logging level to warning
import tensorflow as tf

__author__ = 'Christophe Ecabert'


class TestRenderingOpOp(unittest.TestCase):
  """ Series of Unit-Test for RenderingOpOp """

  def test_import_op(self):
    """
    Check if custom op package can be loaded
    """
    try:
      imported = True
      from lts5.tensorflow_op import rendering_op as r_op
    except ImportError as e:
      imported = False
    # Check
    self.assertTrue(imported)

  # def test_inputs(self):
  #   """ Check input validity """
  #   from lts5.tensorflow_op import rendering_op as r_op
  #
  #   def _run_test():
  #     pass
  #
  #   # https://stackoverflow.com/a/50144255
  #   with tf.Session() as sess:
  #     # Run test in eager
  #     sess.run(tf.py_function(_run_test, [], []))
  #
  # def test_forward_op(self):
  #   """
  #   Check if forward op can properly generates rotation matrix
  #   """
  #   from lts5.tensorflow_op import rendering_op as r_op
  #   pass

  def test_backward_op(self):
    """
    Check if gradient with respect to rotation parameters is correctly
    computed
    """
    from lts5.tensorflow_op import rendering_op as r_op

    # Inputs
    f = 525.0
    width = 15
    height = 15
    near = 1e0
    far = 1e3

    # Single triangle
    z = -(1.75 * f)
    background = np.ones((2, height, width, 3), np.float32) * 0.75
    vertex = np.asarray([[-10, -10, z, 10, -10, z, 0, 10, z, 10, 10, z],
                         [-10, -10, z, 10, -10, z, 0, 10, z, 10, 10, z]], np.float32)
    normal = np.asarray([[0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1],
                         [0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1]], np.float32)
    color = np.asarray([[0.8, 0, 0, 0, 0.8, 0, 0, 0, 0.8, 0, 0, 0.8],
                        [0.8, 0, 0, 0, 0.8, 0, 0, 0, 0.8, 0, 0, 0.8]], np.float32)
    tri = np.asarray([[0, 1, 2],
                      [2, 1, 3]], np.int32)

    # Input placeholders
    bg_ph = tf.placeholder(shape=background.shape, dtype=tf.float32)
    v_ph = tf.placeholder(shape=vertex.shape, dtype=tf.float32)
    n_ph = tf.placeholder(shape=normal.shape, dtype=tf.float32)
    c_ph = tf.placeholder(shape=color.shape, dtype=tf.float32)
    tri_ph = tf.placeholder(shape=tri.shape, dtype=tf.int32)
    # Forward
    image = r_op(vertex=v_ph,
                 normal=n_ph,
                 color=c_ph,
                 background=bg_ph,
                 triangle=tri_ph,
                 width=width,
                 height=height,
                 near=near,
                 far=far,
                 focal=f,
                 visible=False,
                 training=True)

    sess = tf.Session()
    with sess.as_default():
      # Init
      sess.run(tf.global_variables_initializer())
      # dColor_d_reflectance
      p = {bg_ph: background,
           v_ph: vertex,
           n_ph: normal,
           c_ph: color,
           tri_ph: tri}
      # Derivative of the image with respect to color input
      j_t, j_n = tf.test.compute_gradient(y=image[0],
                                          y_shape=image[0].shape.as_list(),
                                          x=c_ph,
                                          x_shape=c_ph.shape.as_list(),
                                          x_init_value=color,
                                          extra_feed_dict=p)
      # Compare structure
      same_struct = np.all(((j_t != 0.0) ^ (j_n != 0.0)) == False)
      self.assertTrue(same_struct)
      # Compare value, for some reason numerical gradient is not always computed
      # correctly. It has a difference of 2x.
      dImg_dCol_e = np.maximum(0.0, np.fabs(j_t - j_n).max())
      self.assertTrue((np.abs(j_n).max() / 2.0) > dImg_dCol_e)

      # # Derivative of the image with respect to vertex
      # j_t, j_n = tf.test.compute_gradient(y=image[0],
      #                                     y_shape=image[0].shape.as_list(),
      #                                     x=v_ph,
      #                                     x_shape=v_ph.shape.as_list(),
      #                                     x_init_value=vertex,
      #                                     extra_feed_dict=p,
      #                                     delta=1e0)
      # # Compare structure
      # same_struct = np.all(((j_t != 0.0) ^ (j_n != 0.0)) == False)
      # self.assertTrue(same_struct)
      # # # Compare value, for some reason numerical gradient is not always computed
      # # # correctly. It has a difference of 2x.
      # # dImg_dVert_e = np.maximum(0.0, np.fabs(j_t - j_n).max())
      # # self.assertTrue((np.abs(j_n).max() / 2.0) > dImg_dVert_e)


if __name__ == '__main__':
  # Run test
  unittest.main()
