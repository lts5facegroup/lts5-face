/**
 *  @file   shared_op.hpp
 *  @brief  Toy example to test CPU/GPU op
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   05/12/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_SHARED_OP__
#define __LTS5_SHARED_OP__

#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

#define EIGEN_USE_THREADS
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @struct  ExampleFunctor
 * @brief   Helper function to select automatically the device on which to
 *          launch the computation (CPU, GPU)
 * @author  Christophe Ecabert
 * @date    7/12/2017
 * @ingroup tensorflow_op
 * @tparam Device   Device on which to run the computation
 * @tparam T        Data type
 */
template <typename Device, typename T>
struct ExampleFunctor {
  /**
   * @name  operator()
   * @fn void operator()(const Device& d, int size, const T* in, T* out)
   * @brief Do the computation
   * @param[in] d   Device object
   * @param[in] size    Input size
   * @param[in] in      Input data
   * @param[out] out    Output data (computed from \p in)
   */
  void operator()(const Device& d, int size, const T* in, T* out) {};
};

#ifdef GOOGLE_CUDA
/**
 * @struct  ExampleFunctor
 * @brief   Helper function to select automatically the device on which to
 *          launch the computation (CPU, GPU)
 *          GPU Selection specialization
 * @author  Christophe Ecabert
 * @date    7/12/2017
 * @ingroup tensorflow_op
 * @tparam T Data type
 */
template <typename T>
struct ExampleFunctor<Eigen::GpuDevice, T> {
  /**
   * @name  operator()
   * @fn void operator()(const Eigen::GpuDevice& d, int size, const T* in, T* out)
   * @brief Do the computation
   * @param[in] d   Device object
   * @param[in] size    Input size
   * @param[in] in      Input data
   * @param[out] out    Output data (computed from \p in)
   */
  void operator()(const Eigen::GpuDevice& d, int size, const T* in, T* out);
};
#endif


}  // namepsace LTS5

/**
 * @class   SharedOp
 * @brief   Custom Op definition
 * @author  Christophe Ecabert
 * @date    7/12/2017
 * @ingroup tensorflow_op
 * @tparam Device   Device on which to run the computation
 * @tparam T        Data type
 */
template <typename Device, typename T>
class SharedOp : public tensorflow::OpKernel {
 public:

  /**
   * @name  SharedOp
   * @fn    explicit SharedOp(tensorflow::OpKernelConstruction* context)
   * @brief Constructor
   * @param[in] context Op kernel context
   */
  explicit SharedOp(tensorflow::OpKernelConstruction* context);

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* context) override
   * @brief Do the actual computation of the Op
   * @param[in] context Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* context) override;

 private:
  /** Storage */
  tensorflow::PersistentTensor storage_;

  Eigen::Tensor<T, 1, Eigen::RowMajor, Eigen::DenseIndex> buffer_;
};


#endif //__LTS5_SHARED_OP__
