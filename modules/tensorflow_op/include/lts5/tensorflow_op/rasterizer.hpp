/**
 *  @file   lts5/tensorflow_op/rasterizer.hpp
 *  @brief  OpenGL-bassed rasterizer
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/14/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TENSORFLOW_OP_RASTERIZER__
#define __LTS5_TENSORFLOW_OP_RASTERIZER__

#include <string>

#include "tensorflow/core/framework/tensor.h"

#include "lts5/tensorflow_op/mm_parameter.hpp"
#include "lts5/tensorflow_op/mm_renderer_utils.hpp"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   Rasterizer
 * @brief   Rasterize 3D surface using OpenGL
 * @author  Christophe Ecabert
 * @date    14/07/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class Rasterizer {
 public:
  /** Rasterization parameters */
  using Parameters = RasterizerParameters<T>;

  #pragma mark -
  #pragma mark Initialization

  /**
   * @name  Rasterizer
   * @fn    Rasterizer()
   * @brief Constructor
   */
  Rasterizer();

  /**
   * @name  Rasterizer
   * @fn    Rasterizer(const Rasterizer& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  Rasterizer(const Rasterizer& other) = delete;

  /**
   * @name operator=
   * @fn Rasterizer& operator=(const Rasterizer& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  Rasterizer& operator=(const Rasterizer& rhs) = delete;

  /**
   * @name  ~Rasterizer
   * @fn    ~Rasterizer();
   * @brief Destructor
   */
  ~Rasterizer();

  /**
   * @name  CreateContext
   * @fn    int CreateContext(const Parameters& params)
   * @brief Create opengl context and build shading program
   * @param[in] params  Rasterizer parameters. NOTE: If params.focal is negative
   *    assume vertex are already in clip space and therefore does not apply
   *    projection matrix
   * @return    -1 if error, 0 otherwise
   */
  int CreateContext(const Parameters& params);

#pragma mark -
#pragma mark Usage


  /**
   * @name  Render
   * @fn    int Render(tf::OpKernelContext* ctx,
             const tf::Tensor& vertex,
             const tf::Tensor& triangle,
             tf::Tensor* image)
   * @brief Render the vertex into a given tensor
   * @param[in] ctx Op's context
   * @param[in] vertex  Vertex to render, batched (i.e. in camera of clip space)
   * @param[in] triangle  Surface triangulation
   * @param[out] image Tensor where to place the data, must be allocated
   *                    correctly beforehand
   * @return -1 if error, 0 otherwise
   */
  int Render(tf::OpKernelContext* ctx,
             const tf::Tensor& vertex,
             const tf::Tensor& triangle,
             tf::Tensor* image);


#pragma mark -
#pragma mark Private

 private:
  /** OpenGL Mapper */
  using OGLMapper = Functor::MapOGLResource<Device, T, T>;
  /** OpenGL Triangle Mapper */
  using OGLFaceMapper = Functor::MapOGLResource<Device, T, unsigned int>;
  /** Downloader type */
  using DLImage = Functor::DownloadImage<Device, T>;
  /** Shader type */
  using Shader = OGLProgram;
  /** Renderer type */
  using Renderer = OGLOffscreenRenderer<T, LTS5::Vector3<T>>;
  /** OpenGL Buffer information */
  using BufferInfo = typename Renderer::BufferInfo;

  /**
   * @name  ConfigureOpenGlBuffer
   * @fn    int ConfigureOpenGlBuffer(tf::OpKernelContext* ctx,
                                      const tf::Tensor& vertex,
                                      const tf::Tensor& triangle)
   * @brief Configure OpenGL buffers for vertex / triangles
   * @param[in] ctx         Op-s context
   * @param[in] vertex      Vertex, [Batch, 3N]
   * @param[in] triangle    Triangles, [T, 3]
   * @return /1 if error, 0 otherwise
   */
  int ConfigureOpenGlBuffer(tf::OpKernelContext* ctx,
                            const tf::Tensor& vertex,
                            const tf::Tensor& triangle);



  /** Offscreen renderer */
  Renderer renderer_;
  /** Rendering program */
  Shader shader_;
  /** Vertex mapper */
  OGLMapper* v_map_;
  /** Face mapper (i.e. Primitive) */
  OGLFaceMapper* f_map_;
  /** Downloader */
  DLImage* dl_img_;
  /** OpenGL buffer information */
  BufferInfo ogl_info_;
};

}  // namespace LTS5
#endif  // __LTS5_TENSORFLOW_OP_RASTERIZER__
