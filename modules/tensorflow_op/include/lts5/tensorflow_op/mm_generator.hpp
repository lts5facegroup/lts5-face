/**
 *  @file   "lts5/tensorflow_op/mm_generator.hpp"
 *  @brief  Tensorflow Op for rendering Morphable Model instance. The input is
 *          the model's coefficients and the output is the generated image
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   04/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_MM_GENERATOR__
#define __LTS5_TF_MM_GENERATOR__

#include "lts5/tensorflow_op/mm_wrapper.hpp"
#include "lts5/tensorflow_op/mm_renderer.hpp"
#include "lts5/tensorflow_op/mm_storage.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   MMGeneratorInitializer
 * @brief   Device specific shape/tex generator initialization. Copy mean shape
 *          tex
 * @author  Christophe Ecabert
 * @date    30/01/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class MMGeneratorInitializer {
 public:
  /** Tensor type */
  using Tensor = tensorflow::Tensor;

  /**
   * @name  operator()
   * @brief Copy a given tensor in all rows of the output tensor. Used to copy
   *        mean shape/tex
   * @param[in] ctx         Kernel context
   * @param[in] value       Vector to copy on each row
   * @param[out] output     Where to copy the data
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const Tensor& value,
                 Tensor* output);
};

/**
 * @class   MMGeneratorNormalizer
 * @brief   Device specific shape/tex generator normalizer. Remove center
 *          gravity of a given shape
 * @author  Christophe Ecabert
 * @date    30/01/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class MMGeneratorNormalizer {
 public:
  /** Tensor type */
  using Tensor = tensorflow::Tensor;

  /**
   * @name  operator()
   * @brief Remove center of gravity from a given shape
   * @param[in] ctx         Kernel context
   * @param[in] cog         Center of gravity
   * @param[in,out] shape   Shape from which to remove cog
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const std::vector<T>& cog,
                 Tensor* shape);
};

/**
 * @class   MMFinalizer
 * @brief   Device specific Finalize implementation
 * @author  Christophe Ecabert
 * @date    04/01/2018
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class MMFinalizer {
 public:

  /** Model type */
  using Model = MorphableModelContainer<T>;
  /** Tensor type */
  using Tensor = tensorflow::Tensor;
  /** Storage type */
  using Storage = PersistentStorage<T>;

  /**
   * @name  operator()
   * @fn    int operator()(tensorflow::OpKernelContext* ctx,
                           const tensorflow::int64& img_idx,
                           const Model* model,
                           const Tensor& parameters,
                           const Tensor& shape,
                           const Tensor& texture,
                           Storage* storage,
                           T* shape_ogl_buffer,
                           T* normal_ogl_buffer,
                           T* tex_ogl_buffer)
   * @brief Finalize MM instance (i.e. illumination, copy to OGL)
   * @param[in] ctx                 Kernel context
   * @param[in] img_idx             Index of the image being finalized
   * @param[in] parameters          Model's parameters
   * @param[in] shape               Shape instance
   * @param[in] texture             Texture instance
   * @param[in] storage             Persistant storage from manager
   * @param[out] shape_ogl_buffer   Pointer to OpenGL buffer holding vertex
   * @param[out] normal_ogl_buffer  Pointer to OpenGL buffer holding normal
   * @param[out] tex_ogl_buffer     Pointer to OpenGL buffer holding color
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const tensorflow::int64& img_idx,
                 const Tensor& parameters,
                 const Tensor& shape,
                 const Tensor& texture,
                 Storage* storage,
                 T* shape_ogl_buffer,
                 T* normal_ogl_buffer,
                 T* tex_ogl_buffer);
};

/**
 * @class   NormalGenerator
 * @brief   Compute surface's normals
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct NormalGenerator {

  /** Storage type */
  using Storage = PersistentStorage<T>;

  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in] ctx         Op's context
   * @param[in,out] storage Morphable Model shared storage
   */
  void operator()(tf::OpKernelContext* ctx, Storage* storage);

  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in,out] ctx Op's context
   * @param[in] vertex  Surface's vertcies
   * @param[in] conn    Connectivity
   * @param[out] normal Surface's normals computed
   */
  void operator()(tf::OpKernelContext* ctx,
                  const tf::Tensor* vertex,
                  const tf::Tensor* conn,
                  tf::Tensor* normal);
};

/**
 * @class   TriangleGenerator
 * @brief   Copy triangulation to output
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T Data type
 */
template<typename Device, typename T>
struct TriangleGenerator {

  /**
   * @name operator()
   * @brief Copy triangulation to output
   * @param[in,out] ctx
   * @param[in] triangulation
   * @param[out] tri
   */
  void operator()(tf::OpKernelContext* ctx,
                  const tf::Tensor* triangulation,
                  tf::Tensor* tri);
};

}  // namespace Functor

#pragma mark -
#pragma mark Generator

/**
 * @class   MMGenerator
 * @brief   Generate an instance of a Morphable Model.
 * @author  Christophe Ecabert
 * @date    7/12/2017
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class MMGenerator {
 public:

#pragma mark -
#pragma mark Type Definitions

  /** Model type */
  using Model = MorphableModelContainerV2<T>;
  /** Storage type */
  using Storage = PersistentStorage<T>;
  /** Tensor type */
  using Tensor = tensorflow::Tensor;



#pragma mark -
#pragma mark Initialization

  /**
   * @name  MMGenerator
   * @fn    MMGenerator() = default;
   * @brief Constructor
   */
  MMGenerator() = default;

  /**
   * @name  MMGenerator
   * @fn    MMGenerator(const MMGenerator& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  MMGenerator(const MMGenerator& other) = delete;

  /**
   * @name  operator=
   * @fn    MMGenerator& operator=(const MMGenerator& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  MMGenerator& operator=(const MMGenerator& rhs) = delete;

  /**
   * @name  MMGenerator
   * @fn    MMGenerator(MMGenerator&& other) = delete
   * @brief Move constructor
   * @param[in] other Object to move from
   */
  MMGenerator(MMGenerator&& other) = delete;

  /**
   * @name  operator=
   * @fn    MMGenerator& operator=(MMGenerator&& rhs) = delete
   * @brief Move-Assignment operator
   * @param[in] rhs Object to move-assign from
   * @return    Newly moved-assign object
   */
  MMGenerator& operator=(MMGenerator&& rhs) = delete;

  /**
   * @name  ~MMGenerator
   * @fn    ~MMGenerator();
   * @brief Destructor
   */
  ~MMGenerator() = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Generate
   * @fn    void Generate(tensorflow::OpKernelContext* ctx,
                          const Tensor& parameters,
                          Storage* storage)
   * @brief Generate an instance of a Morphable model
   * @param[in] ctx     Op context
   * @param[in] parameters  Input parameters
   * @param[in] storage Persistant storage
   */
  void Generate(tensorflow::OpKernelContext* ctx,
                const Tensor& parameters,
                Storage* storage);

  /**
   * @name  Generate
   * @brief Generate an instance of a morphable model
   * @param[in,out] ctx     Op's context
   * @param[in] w_shape     Shape parameter
   * @param[in] w_tex       Texture parameters
   * @param[in] model       Morphable model instance
   * @param[out] vertex     Generated vertex
   * @param[out] color      Generated color
   * @param[out] triangle   Surface's triangulation
   */
  void Generate(tf::OpKernelContext* ctx,
                const tf::Tensor* w_shape,
                const tf::Tensor* w_tex,
                Model* model,
                tf::Tensor* vertex,
                tf::Tensor* color,
                tf::Tensor* triangle);
};
}  // namepsace LTS5
#endif //__LTS5_TF_MM_GENERATOR__
