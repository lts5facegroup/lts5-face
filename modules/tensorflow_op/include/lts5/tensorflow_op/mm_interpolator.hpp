/**
 *  @file   lts5/tensorflow_op/mm_interpolator.hpp
 *  @brief  Morphable model Interpolator class. Compute pixel's barycentric
 *          coordinates on the image
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   5/3/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_MM_INTERPOLATOR__
#define __LTS5_MM_INTERPOLATOR__

#include "lts5/tensorflow_op/mm_renderer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   MMInterpShader
 * @brief   Shading program for interpolation
 * @author  Christophe Ecabert
 * @date    03/05/18
 * @ingroup tensforflow_op
 * @tparam Device on which the computation is done
 * @tparam T    Data type
 */
template<typename Device, typename T>
class MMInterpShader;

/**
 * @class   MMInterpShader
 * @brief   Shading program for interpolation - CPU Specialization
 * @author  Christophe Ecabert
 * @date    03/05/18
 * @ingroup tensorflow_op
 * @tparam T    Data type
 */
template<typename T>
class MMInterpShader<CPUDevice, T> {
 public:
  /**
   * @name  MMInterpShader
   * @fn    MMInterpShader(const MMRendererParameters<T>& p,
                           const bool& with_rigid)
   * @brief Constructor
   * @param[in] p Rendering parameters
   * @param[in] with_rigid  Flag to add the rigid transform in the shader
   */
  MMInterpShader(const MMRendererParameters<T>& p,
                 const bool& with_rigid);

  /**
   * @name  ~MMInterpShader
   * @fn    ~MMInterpShader() override = default
   * @brief Destructor
   */
  ~MMInterpShader();

  /**
   * @name  UpdateModelTransform
   * @fn    int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                                     const tensorflow::Tensor& parameter,
                                     const tensorflow::int64& idx)
   * @brief Update model transformation matrix from camera parameters
   * @param[in] ctx Op context
   * @param[in] parameter Morphable model parameters
   * @param[in] idx Index where camera parameters start
   * @return    -1 if error, 0 otherwise
   */
  int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                           const tensorflow::Tensor& parameter,
                           const tensorflow::int64& idx);

  /**
   * @name  get_shader
   * @fn    const OGLProgram& get_shader() const
   * @brief Give shading program
   * @return    Shader
   */
  const OGLProgram& get_shader() const {
    return *shader_;
  }

  /**
   * @name  get_shader
   * @fn    OGLProgram& get_shader()
   * @brief Give shading program
   * @return    Shader
   */
  OGLProgram& get_shader() {
    return *shader_;
  }

 protected:
  /** Shader */
  OGLProgram* shader_;
  /** UBO */
  UniformBufferObject* ubo_;
  /** Focal length */
  T focal_;
  /** Flag */
  bool has_rigid_;
};

/**
 * @class   MMInterpolator
 * @brief   Morphable model Interpolator class. Compute pixel's barycentric
 *          coordinates on the image
 * @author  Christophe Ecabert
 * @date    03/05/18
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class MMInterpolator {
 public:

#pragma mark -
#pragma mark Type definition

  /** Tensor type */
  using Tensor = tensorflow::Tensor;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  MMInterpolator
   * @fn    MMInterpolator()
   * @brief Constructor
   */
  MMInterpolator();

  /**
   * @name  ~MMInterpolator
   * @fn    ~MMInterpolator()
   * @brief Destructor
   */
  ~MMInterpolator();

  /**
   * @name  Init
   * @fn    int Init(const Tensor& triangle,
   *                 const MMRendererParameters<T>& params)
   * @brief Initialise the interpolator
   * @param[in] triangle    Mesh triangulation
   * @param[in] params      Rendering parameters
   * @return -1 if error, 0 otherwise
   */
  int Init(const Tensor& triangle,
           const MMRendererParameters<T>& params);

#pragma mark -
#pragma mark Process

  /**
   * @name  ActivateContext
   * @brief Enable OpenGL context
   * @fn    void ActivateContext() const
   */
  void ActivateContext() const;

  /**
   * @name  DisableContext
   * @brief Disable OpenGL context
   * @fn    void DisableContext() const
   */
  void DisableContext() const;

  /**
   * @name  ComputeBarycentric
   * @fn    int ComputeBarycentric(tf::OpKernelContext* ctx,
                                   const Tensor& vertex,
                                   const Tensor& triangle,
                                   const size_t& n_element,
                                   Tensor* bcoords)
   * @brief Compute barycentric interpolant
   * @param[in] ctx Op's context
   * @param[in] vertex Vertex array (batch)
   * @param[in] triangle Shared triangulation
   * @param[in] n_element   Number of primitive element to be rendered. If set
   *                        to numeric_limits<size_t>::max() will render all of
   *                        them otherwise only the specified number
   * @param[out] bcoords Barycentric coordinate
   */
  int ComputeBarycentric(tf::OpKernelContext* ctx,
                         const Tensor& vertex,
                         const Tensor& triangle,
                         const size_t& n_element,
                         Tensor* bcoords);


#pragma mark -
#pragma mark Private

 private:
  /** Renderer type */
  using Renderer = MMRendererV2<Device, T>;
  /** OpenGL Mapper */
  using OGLMapper = Functor::MapOGLResource<Device, T, T>;


  /** Renderer */
  Renderer* renderer_;
  /** Shader */
  MMInterpShader<Device, T>* shader_;
  /** Vertex mapper */
  OGLMapper* v_map_;
  /** Normal mapper */
  OGLMapper* n_map_;
  /** Camera index */
  tensorflow::int64 cam_idx_;
  /** Number of triangle */
  size_t n_tri_;
  /** Initialised flag */
  bool is_init_;
};

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   MMShuffleOglBuffer
 * @brief   Reorganise mesh from a shared topology to a splitted one
 * @author  Christophe Ecabert
 * @date    03/05/18
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class MMShuffleOglBuffer {
 public:
  /** Tensor type */
  using Tensor = tensorflow::Tensor;

  /**
   * @name  operator()
   * @fn    int operator()(tensorflow::OpKernelContext* ctx, const Tensor* buffer,
                 const Tensor* triangle, T* ogl_buffer)
   * @brief Reorganise a buffer from a shared topology to a splitted one based
   *        on a triangulation
   * @param[in] ctx Op context
   * @param[in] buffer  Buffer to reorganise
   * @param[in] triangle    List of triangle
   * @param[out] ogl_buffer OpenGL buffer where to dump the output
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const Tensor* buffer,
                 const Tensor* triangle,
                 T* ogl_buffer);
};

}  // namespace Functor

}  // namespace LTS5
#endif // __LTS5_MM_INTERPOLATOR__
