/**
 *  @file   surface_normal.hpp
 *  @brief  Compute surface's normals of a given "mesh"
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/5/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_SURFACE_NORMAL__
#define __LTS5_TF_SURFACE_NORMAL__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   NormalGeneratorV2
 * @brief   Compute surface's normals
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct NormalGeneratorV2 {
  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in,out] ctx Op's context
   * @param[in] vertex  Surface's vertcies
   * @param[in] conn    Connectivity
   * @param[out] normal Surface's normals computed
   * @param[out] scaling Normal's length before normalization to unit length
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* vertex,
                 const tf::Tensor* conn,
                 tf::Tensor* normal,
                 tf::Tensor* scaling);
};

}  // namespace Functor
}  // namespace LTS5

/**
 * @class   SurfaceNormalKernel
 * @brief   Kernel implementing the computation of surface's normal
 * @author  Christophe Ecabert
 * @date    05/07/19
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class SurfaceNormalKernel : public tf::OpKernel {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  SurfaceNormalKernel
   * @fn    explicit SurfaceNormalKernel(tensorflow::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit SurfaceNormalKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~SurfaceNormalKernel
   * @fn    ~SurfaceNormalKernel() override
   * @brief Destructor
   */
  ~SurfaceNormalKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(SurfaceNormalKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** Connectivity */
  tf::PersistentTensor conn_;
};

#endif  // __LTS5_TF_SURFACE_NORMAL__
