/**
 *  @file   lts5/tensorflow_op/mm_renderer_utils.hpp
 *  @brief  Utility function for mm_renderer
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   5/2/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_MM_RENDERER_UTILS__
#define __LTS5_TF_MM_RENDERER_UTILS__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/utils/library_export.hpp"
#include "lts5/ogl/offscreen_renderer.hpp"
#include "lts5/ogl/uniform_buffer_object.hpp"
#include "lts5/tensorflow_op/mm_parameter.hpp"
//#include "lts5/tensorflow_op/mm_storage.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @enum    PixelType
 * @brief   Pixel classification
 */
enum PixelType {
  /** Exterior */
  kExterior = 0x00,
  /** Interior */
  kInterior = 0x01,
  /** Boundary */
  kBoundary = 0x02,
};


/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/** Fragment shader selector for rendering MModel instance */

/**
 * @struct  FSRenderSelector
 * @brief   Shader selector for a given device known at compilation. Must be
 *          specialized for specific device.
 * @author  Christophe Ecabert
 * @date    8/9/18
 * @tparam D    Device on which the computation is run
 */
template<typename D>
struct FSRenderSelector;

/**
 * @class  MapOGLResource
 * @brief   Map a specific OpenGL Ressource to a pointer accessible for a given
 *          device
 * @author  Christophe Ecabert
 * @date    15/01/2018
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam RType    Rendering type
 * @tparam MType    Mapping type
 */
template<typename Device, typename RType, typename MType>
class MapOGLResource {
 public:
  /** Renderer type */
  using Renderer = OGLOffscreenRenderer<RType, Vector3<RType>>;
  /** Buffer Type */
  using BufferIdx = typename Renderer::BufferIdx;

  /**
   * @name  MapOGLResource
   * @fn    MapOGLResource()
   * @brief Constructor
   */
  MapOGLResource();

  /**
   * @name  Init
   * @fn void Init(const BufferIdx& resource, const Renderer* renderer)
   * @brief Initialize mapper
   * @param[in] resource    Resource index to map
   * @param[in] renderer    Renderer
   */
  void Init(const BufferIdx& resource, const Renderer* renderer);

  /**
   * @name  ~MapOGLResource
   * @fn    ~MapOGLResource()
   * @brief Destructor
   */
  ~MapOGLResource();

  /**
   * @name  Map
   * @fn    MType* Map(tensorflow::OpKernelContext* ctx)
   * @brief Map opengl resource to a pointer usable for this device
   * @param[in] ctx Op context
   * @return    Pointer to the mapped resource if success, nullptr otherwise
   */
  MType* Map(tensorflow::OpKernelContext* ctx);

  /**
   * @name  Unmap
   * @fn    void Unmap(tensorflow::OpKernelContext* ctx)
   * @brief Unmap the underlying resources
   * @param[in] ctx Op context
   */
  void Unmap(tensorflow::OpKernelContext* ctx);

 private:
  /** Resource */
  BufferIdx resource_;
  /** Renderer */
  const Renderer* renderer_;
};

/**
 * @class   DownloadImage
 * @brief   Retrieve image from an OpenGL renderer a place it into a tensor
 * @author  Christophe Ecabert
 * @date    25/01/18
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class DownloadImage {
 public:
  /** Renderer type */
  using Renderer = OGLOffscreenRenderer<T, Vector3<T>>;

  /**
   * @name  DownloadImage
   * @fn    explicit DownloadImage()
   * @brief Constructor
   */
  DownloadImage();

  /**
   * @name  ~DownloadImage
   * @fn    ~DownloadImage()
   * @brief Destrutcor
   */
  ~DownloadImage();

  /**
   * @name  Init
   * @fn    void Init(const Renderer* renderer)
   * @brief Initialize downloader object
   * @param[in] renderer Render object to download from
   */
  void Init(const Renderer* renderer);

  /**
   * @name  operator()
   * @fn    int operator()(tf::OpKernelContext* ctx, tf::Tensor* image)
   * @brief Copy image to the tensor
   * @param[in] ctx Op context
   * @param[out] image  Image where to copy the data
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx, tf::Tensor* image);

  /**
   * @name  operator()
   * @fn    int operator()(tf::OpKernelContext* ctx,
                           const tf::Tensor& background,
                           tf::Tensor* image)
   * @brief Transfer image from OpenGL buffer to cuda array and sitch background
   *        at the same time.
   * @param[in] ctx
   * @param[in] background
   * @param[out] image
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& background,
                 tf::Tensor* image);
};

/**
 * @class   DownloadBoundaryImage
 * @brief   Retrieve edge image from an OpenGL renderer a place it into a tensor
 * @author  Christophe Ecabert
 * @date    15/04/19
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class DownloadBoundaryImage {
 public:
  /** Renderer type */
  using Renderer = OGLOffscreenRenderer<T, Vector3<T>>;

  /**
   * @name  DownloadBoundaryImage
   * @fn    explicit DownloadBoundaryImage()
   * @brief Constructor
   */
  DownloadBoundaryImage();

  /**
   * @name  ~DownloadBoundaryImage
   * @fn    ~DownloadBoundaryImage()
   * @brief Destrutcor
   */
  ~DownloadBoundaryImage();

  /**
   * @name  Init
   * @fn    void Init(const Renderer* renderer)
   * @brief Initialize downloader object
   * @param[in] renderer Render object to download from
   */
  void Init(const Renderer* renderer);

  /**
   * @name  operator()
   * @fn    int operator()(tensorflow::OpKernelContext* ctx,
                           tensorflow::Tensor* boundaries)
   * @brief Copy image to the tensor
   * @param[in] ctx Op context
   * @param[out] boundaries  Image where to copy the data
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx, tensorflow::Tensor* image);
};

/**
 * @class   UpdateOglPrimitive
 * @brief   Update VAO buffer (vertex, normal, color)
 * @author  Christophe Ecabert
 * @date    15/04/18
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class UpdateOglBuffer {
 public:
  /** Ogl Mapper */
  using Mapper = MapOGLResource<Device, T, T>;

  /**
   * @name  operator()
   * @brief Update opengl buffers (VAOs)
   * @param[in] ctx Op's context
   * @param[in] idx Surface's index that need to be push to opengl buffers
   * @param[in] storage Morphable Model shared storage
   * @param[in] vertex_map  Vertex mapper to OpenGL VAO's buffer
   * @param[in] normal_map  Normal mapper to OpenGL VAO's buffer
   * @param[in] vertex_color_map    Vertex Color mapper to OpenGL VAO's buffer
   * @return -1 if error, 0 otherwise
   */
  /*int operator()(tensorflow::OpKernelContext* ctx,
                 const size_t& idx,
                 Storage* storage,
                 Mapper* vertex_map,
                 Mapper* normal_map,
                 Mapper* vertex_color_map);*/
  /**
   * @name  operator()
   * @brief Update opengl buffers (VAOs)
   * @param[in] ctx Op's context
   * @param[in] vertex  Vertex array (slice)
   * @param[in] normal  Normal array (slice)
   * @param[in] color   Color array (slice)
   * @param[in] vertex_map  Vertex mapper to OpenGL VAO's buffer
   * @param[in] normal_map  Normal mapper to OpenGL VAO's buffer
   * @param[in] vertex_color_map    Vertex Color mapper to OpenGL VAO's buffer
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& normal,
                 const tf::Tensor& color,
                 Mapper* vertex_map,
                 Mapper* normal_map,
                 Mapper* vertex_color_map);
};

/**
 * @class   UpdateOglPrimitive
 * @brief   Update VBO's element index from a given array
 * @author  Christophe Ecabert
 * @date    15/04/18
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class UpdateOglEdgeIndex {
 public:
  /** Ogl Mapper */
  using Mapper = MapOGLResource<Device, T, unsigned int>;

  /**
   * @name  operator()
   * @param[in] ctx         Op's context
   * @param[in] edge_list   List of edges to push to OpenGL
   * @param[in] offset      Offset from where to start in the index array
   * @param[in] mapper      Mapper to OpenGL's VBO
   * @return    Number of element pushed to the index array
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const tensorflow::Tensor& edge_list,
                 const size_t& offset,
                 Mapper& mapper);
};

/**
 * @class   UploadTriangle
 * @brief   Update VAO with triangles
 * @author  Christophe Ecabert
 * @date    15/07/2020
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class UploadTriangle {
 public:
  /** Ogl Face Mapper */
  using Mapper = MapOGLResource<Device, T, unsigned int>;

  /**
   * @name  operator()
   * @param[in] ctx         Op's context
   * @param[in] triangle    Triangles to upload
   * @param[in] mapper      Mapper to OpenGL's VBO
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& triangle,
                 Mapper& mapper);
};

/**
 * @class   UploadAttribute
 * @brief   Update VAO with a given attributes (vertex, normal, color, ...)
 * @author  Christophe Ecabert
 * @date    15/07/2020
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class UploadAttribute {
 public:
  /** Ogl Face Mapper */
  using Mapper = MapOGLResource<Device, T, T>;

  /**
   * @name  operator()
   * @param[in] ctx         Op's context
   * @param[in] attribute   Attributes to upload
   * @param[in] mapper      Mapper to OpenGL's VBO
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& attributes,
                 Mapper& mapper);
};
}  // namespace Functor

/**
 * @name  CreateEdgeShader
 * @fn    static OGLProgram* CreateEdgeRenderingShader()
 * @brief Create GLSL programm to render Edge silhouette
 * @return    GLSL Program
 */
OGLProgram* CreateEdgeRenderingShader();

/**
 * @name  CreateEdgeShader
 * @fn    static OGLProgram* CreateEdgeRenderingShader()
 * @brief Create GLSL programm to render Edge silhouette
 * @return    GLSL Program
 */
template<typename T>
OGLProgram* CreateEdgeRenderingShader(const MMRendererParameters<T>& params);

/**
 * @class   MMRenderingShader
 * @brief   Shading program for image rendering
 * @author  Christophe Ecabert
 * @date    03/05/18
 * @ingroup tensorflow
 */
template<typename Device, typename T>
class MMRenderingShader;


/**
 * @class   MMRenderingShader
 * @brief   Shading program for image rendering - CPU Specialization
 * @author  Christophe Ecabert
 * @date    03/05/18
 * @ingroup tensorflow_op
 */
template<typename T>
class MMRenderingShader<CPUDevice, T> {
 public:
  /**
   * @name  MMRenderingShader
   * @fn    MMRenderingShader(const MMRendererParameters<T>& p)
   * @brief Constructor
   * @param[in] p   Rendering parameters
   */
  explicit MMRenderingShader(const MMRendererParameters<T>& p);

  /**
   * @name  ~MMRenderingShader
   * @fn    ~MMRenderingShader() override
   * @brief Destructor
   */
  ~MMRenderingShader();

  /**
   * @name  UpdateModelTransform
   * @fn    int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                                     const tensorflow::Tensor& parameter,
                                     const tensorflow::int64& i_idx,
                                     const tensorflow::int64& ct_idx,
                                     const tensorflow::int64& cam_idx)
   * @brief Update model transformation matrix from camera parameters
   * @param[in] ctx Op context
   * @param[in] parameter Morphable model parameters
   * @param[in] i_idx   Index where illumination parameters start
   * @param[in] ct_idx Index where color transformation parameters start
   * @param[in] cam_idx Index where camera parameters start
   * @return    -1 if error, 0 otherwise
   */
  int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                           const tensorflow::Tensor& parameter,
                           const tensorflow::int64& i_idx,
                           const tensorflow::int64& ct_idx,
                           const tensorflow::int64& cam_idx);

  /**
   * @name  get_shader
   * @fn    const OGLProgram& get_shader() const
   * @brief Give shading program
   * @return    Shader
   */
  const OGLProgram& get_shader() const {
    return *shader_;
  }

  /**
   * @name  get_shader
   * @fn    OGLProgram& get_shader()
   * @brief Give shading program
   * @return    Shader
   */
  OGLProgram& get_shader() {
    return *shader_;
  }

 private:
  /** Vertex Shader */
  static constexpr const char *vs_str =
          "#version 330\n"
          "layout (location = 0) in vec3 vertex;\n"
          "layout (location = 1) in vec3 normal;\n"
          "layout (location = 2) in vec3 color;\n"
          "layout (std140) uniform Transform {\n"
          "  mat4 model;\n"
          "  mat4 projection;\n"
          "};\n"
          "out vec3 normal0;\n"
          "out vec3 color0;\n"
          "void main() {\n"
          "  gl_Position = projection * model * vec4(vertex, 1.f);\n"
          "  color0 = color;\n"
          "  normal0 = mat3(model) * normal;\n"
          "}";
  /** Uniform Buffer Object */
  UniformBufferObject* ubo_;
  /** Shading program */
  OGLProgram* shader_;
  /** Focal length */
  T focal_;
};

/**
 * @class   ImageShader
 * @brief   Shading program for image rendering
 * @author  Christophe Ecabert
 * @date    11/07/19
 * @ingroup tensorflow_op
 */
template<typename T>
class LTS5_EXPORTS ImageShader {
 public:
  /** Parameters */
  using Parameters = MMRendererParameters<T>;
#pragma mark -
#pragma mark Initialization

  /**
   * @name  ImageShader
   * @fn    ImageShader(const Parameters& p)
   * @brief Constructor
   * @param[in] p   Rendering parameters
   */
  explicit ImageShader(const Parameters& p);

  /**
   * @name  ImageShader
   * @fn    ImageShader(const ImageShader& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  ImageShader(const ImageShader& other) = delete;

  /**
   * @name  operator=
   * @fn    ImageShader& operator=(const ImageShader& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  ImageShader& operator=(const ImageShader& rhs) = delete;

  /**
   * @name  ~ImageShader
   * @fn    ~ImageShader() override
   * @brief Destructor
   */
  ~ImageShader();

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_shader
   * @fn    const OGLProgram& get_shader() const
   * @brief Give shading program
   * @return    Shader
   */
  const OGLProgram& get_shader() const {
    return *shader_;
  }

  /**
   * @name  get_shader
   * @fn    OGLProgram& get_shader()
   * @brief Give shading program
   * @return    Shader
   */
  OGLProgram& get_shader() {
    return *shader_;
  }

#pragma mark -
#pragma mark Accessors
 private:
  /** Shader */
  OGLProgram* shader_;
};
}  // namespace LTS5
#endif  // __LTS5_TF_MM_RENDERER_UTILS__
