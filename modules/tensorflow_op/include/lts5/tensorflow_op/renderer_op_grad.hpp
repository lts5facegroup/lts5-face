/**
 *  @file   renderer_op_grad.hpp
 *  @brief  Gradient fir the rendering operation
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   22/7/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_RENDERER_OP_GRAD__
#define __LTS5_TF_RENDERER_OP_GRAD__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"

#include "lts5/tensorflow_op/mm_renderer.hpp"
#include "lts5/tensorflow_op/mm_interpolator.hpp"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   RenderingGradientHelper
 * @brief   Compute renderer gradient with respecto to vertex/color
 * @author  Christophe Ecabert
 * @date    22/07/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct RenderingGradientHelper {
  /**
   * @name  operator()
   * @brief Compute renderer gradient
   * @param[in] ctx             Op's context
   * @param[in] g_image         Back propagated gradient
   * @param[in] vertex          Input vertex
   * @param[in] color           Input color
   * @param[in] triangle        Surface's triangulation
   * @param[in] bcoord          Barycentric coordinates
   * @param[in] spatial_grad    Image spatial gradient
   * @param[in] focal           Focal length
   * @param[out] grad_vertex    Grad dImage_dVertex
   * @param[out] grad_color     Grad dImage_dColor
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_image,
                 const tf::Tensor* vertex,
                 const tf::Tensor* color,
                 const tf::Tensor* triangle,
                 const tf::Tensor* bcoord,
                 const tf::Tensor* spatial_grad,
                 const T& focal,
                 tf::Tensor* grad_vertex,
                 tf::Tensor* grad_color);
};


}  // namespace Functor
}  // namespace LTS5

/**
 * @class   RenderingGradKernel
 * @brief   Kernel implementing spherical harmonics lighting
 * @author  Christophe Ecabert
 * @date    10/07/19
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class RenderingGradKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  RenderingGradKernel
   * @fn    explicit RenderingGradKernel(tensorflow::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit RenderingGradKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~RenderingGradKernel
   * @fn    ~RenderingGradKernel() override
   * @brief Destructor
   */
  ~RenderingGradKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(RenderingGradKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** Width */
  int width_;
  /** Height */
  int height_;
  /** Focal */
  T focal_;
};


#endif  // __LTS5_TF_RENDERER_OP_GRAD__
