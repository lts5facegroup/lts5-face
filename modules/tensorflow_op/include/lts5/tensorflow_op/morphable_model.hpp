/**
 *  @file   lts5/tensorflow_op/morphable_model.hpp
 *  @brief  Morphable Model wrapper for TensorFlow
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/2/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_MORPHABLE_MODEL__
#define __LTS5_TF_MORPHABLE_MODEL__

#include <string>

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/tensorflow_op/mm_generator.hpp"

/**
 * @class   MorphableModelKernel
 * @brief   Kernel generating surface/normal/reflectance/triangle from a given
 *          set of parameters w_shp & w_tex.
 *          Use statistical model (PCA) for shape + texture
 * @author  Christophe Ecabert
 * @date    7/2/19
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class MorphableModelKernel : public tensorflow::OpKernel {
 public:
  /**
   * @name  MorphableModelKernel
   * @fn    explicit MorphableModelKernel(tensorflow::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit MorphableModelKernel(tensorflow::OpKernelConstruction* ctx);

  /**
   * @name  ~MorphableModelKernel
   * @fn    ~MorphableModelKernel() override
   * @brief Destructor
   */
  ~MorphableModelKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(MorphableModelKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private
 private:
  /** Generator type */
  using Generator = LTS5::MMGenerator<Device, T>;

  /** Mutex */
  tensorflow::mutex mutex_;
  /** Morphable Model's path */
  std::string model_path_;
  /** Generator */
  Generator* gen_;
};

#endif  // __LTS5_TF_MORPHABLE_MODEL__
