/**
 *  @file   lts5/tensorflow_op/tensor_utils.hpp
 *  @brief  Utility function for Matrix
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   20/12/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_TF_MATRIX_UTILS__
#define __LTS5_TF_MATRIX_UTILS__

#include "opencv2/core.hpp"

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      Helper function
 */
namespace Functor {

#pragma mark -
#pragma mark Type Definition

/**
 * @enum  DenseOpType
 * @brief List of possible action to apply on tensor
 * @ingroup tensorflow_op
 */
enum class DenseOpType : uchar {
  /** Addition */
  kAdd,
  /** Subtraction */
  kSub,
  /** Copy device to device */
  kD2DCopy,
  /** Copy host to device */
  kH2DCopy,
  /** Copy device to host */
  kD2HCopy
};

#pragma mark -
#pragma mark Declaration

/**
 * @struct  DenseOp
 * @brief   Apply operation on tensor
 * @tparam Device   Device on which to run the conversion
 * @tparam T        Data type
 * @tparam OP       Operation type
 * @ingroup tensorflow_op
 * @note Take from  tensorflow/tensorflow/core/kernels/dense_update_ops.h
 */
template <typename Device, typename T, DenseOpType OP>
struct DenseOp;

/**
 * @struct  ConvertMatrix
 * @brief   Convert tensorflow::Tensor into an Matrix (RowMajor)
 * @author  Christophe Ecabert
 * @date    20/12/17
 * @tparam Device   Device on which to run the conversion
 * @tparam T        Data type
 * @ingroup tensorflow_op
 */
template <typename Device, typename T>
struct ConvertMatrix;

// ---------------------------------------------------------------

#pragma mark -
#pragma mark Implementation + Specialiazation

/** CPU Device Abstraction */
using CPUDevice = Eigen::ThreadPoolDevice;

#pragma mark Matrix Convertion

/**
 * @struct  ConvertMatrix
 * @brief   Convert tensorflow::Tensor into an Matrix (RowMajor).
 *          CPU Specialization
 * @author  Christophe Ecabert
 * @date    20/12/17
 * @tparam T        Data type
 * @ingroup tensorflow_op
 */
template <typename T>
struct ConvertMatrix<CPUDevice, T> {

  /** Tensorflow tensor */
  using Tensor = tensorflow::Tensor;
  /** Tensorflow mapped as const matrix */
  using TFConstMatrix = typename tensorflow::TTypes<T>::ConstMatrix;
  /** Tensorflow mapped as matrix */
  using TFMatrix = typename tensorflow::TTypes<T>::Matrix;
  /** Tensorflow mapped as const flat buffer */
  using TFConstFlat = typename tensorflow::TTypes<T>::ConstFlat;
  /** Tensorflow mapped as const unaligned flat buffer */
  using TFConstUnFlat = typename tensorflow::TTypes<T>::UnalignedConstFlat;
  /** Tensorflow mapped as flat buffer */
  using TFFlat = typename tensorflow::TTypes<T>::Flat;
  /** Tensorflow mapped as unaligned flat buffer */
  using TFUnFlat = typename tensorflow::TTypes<T>::UnalignedFlat;
  /** Eigen Map */
  using EigenMap = Eigen::Map<const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>;

  /**
   * @name    ToEigen
   * @brief   Convert tensorflow::Tensor into an Eigen Matrix
   * @param tensor    Tensor to convert
   * @return  Eigen Matrix
   */
  static EigenMap ToEigen(const Tensor& tensor) {
    auto matrix = tensor.matrix<T>();
    return Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>::Map(
            matrix.data(), matrix.dimension(0), matrix.dimension(1));
  }

  /**
   * @name    ToOpenCV
   * @brief   Convert tensorflow::Tensor into an OpenCV Matrix
   * @param tensor    Tensor to convert
   * @return  OpenCV Matrix
   */
  static cv::Mat ToOpenCV(const Tensor& tensor) {
    return cv::Mat(tensor.dim_size(0),
                   tensor.dim_size(1),
                   cv::DataType<T>::type,
                   (void*)tensor.template unaligned_flat<T>().data());
  }

  /**
   * @name    ToOpenCV
   * @brief   Convert tensorflow::Tensor into an OpenCV Matrix
   * @param matrix    Matrix to convert
   * @return  OpenCV Matrix
   */
  static cv::Mat ToOpenCV(TFConstMatrix& matrix) {
    return cv::Mat((int)matrix.dimension(0),
                   (int)matrix.dimension(1),
                   cv::DataType<T>::type,
                   (void*)matrix.data());
  }

  /**
   * @name    ToOpenCV
   * @brief   Convert tensorflow::Tensor into an OpenCV Matrix
   * @param matrix    Matrix to convert
   * @return  OpenCV Matrix
   */
  static cv::Mat ToOpenCV(TFMatrix& matrix) {
    return cv::Mat((int)matrix.dimension(0),
                   (int)matrix.dimension(1),
                   cv::DataType<T>::type,
                   (void*)matrix.data());
  }

  /**
   * @name    ToOpenCV
   * @brief   Convert a constant aligned flatten Tensor into an OpenCV Matrix
   * @param[in] array    Array to convert
   * @param[in] rows    Number of rows of the converted matrix (ocv)
   * @param[in] cols    Number of cols of the converted matrix (ocv)
   * @return  OpenCV Matrix
   */
  static cv::Mat ToOpenCV(TFConstFlat& array,
                          const int& rows,
                          const int& cols) {
    DCHECK_EQ(rows * cols, array.size());
    return cv::Mat(rows,
                   cols,
                   cv::DataType<T>::type,
                   (void*)array.data());
  }

  /**
   * @name    ToOpenCV
   * @brief   Convert an algined flatten tensor into an OpenCV Matrix
   * @param[in] array    Array to convert
   * @param[in] rows    Number of rows of the converted matrix (ocv)
   * @param[in] cols    Number of cols of the converted matrix (ocv)
   * @return  OpenCV Matrix
   */
  static cv::Mat ToOpenCV(TFFlat& array,
                          const int& rows,
                          const int& cols) {
    DCHECK_EQ(rows * cols, array.size());
    return cv::Mat(rows,
                   cols,
                   cv::DataType<T>::type,
                   (void*)array.data());
  }

  /**
   * @name    ToOpenCV
   * @brief   Convert a constant unaligned flatten Tensor into an OpenCV Matrix
   * @param[in] array    Array to convert
   * @param[in] rows    Number of rows of the converted matrix (ocv)
   * @param[in] cols    Number of cols of the converted matrix (ocv)
   * @return  OpenCV Matrix
   */
  static cv::Mat ToOpenCV(TFConstUnFlat& array,
                          const int& rows,
                          const int& cols) {
    DCHECK_EQ(rows * cols, array.size());
    return cv::Mat(rows,
                   cols,
                   cv::DataType<T>::type,
                   (void*)array.data());
  }

  /**
   * @name    ToOpenCV
   * @brief   Convert an unaligned flatten tensor into an OpenCV Matrix
   * @param[in] array    Array to convert
   * @param[in] rows    Number of rows of the converted matrix (ocv)
   * @param[in] cols    Number of cols of the converted matrix (ocv)
   * @return  OpenCV Matrix
   */
  static cv::Mat ToOpenCV(TFUnFlat& array,
                          const int& rows,
                          const int& cols) {
    DCHECK_EQ(rows * cols, array.size());
    return cv::Mat(rows,
                   cols,
                   cv::DataType<T>::type,
                   (void*)array.data());
  }
};

#pragma mark Dense Operation

template <typename T>
struct DenseOp<CPUDevice, T, DenseOpType::kAdd> {

  /**
   * @name  operator()
   * @brief Add a tensor to another one
   * @param[in] d           Device
   * @param[in] to_add      Tensor to add
   * @param[in,out] params  Tensor with the added value (+=)
   */
  void operator()(const CPUDevice& d,
                  typename tensorflow::TTypes<T>::ConstFlat to_add,
                  typename tensorflow::TTypes<T>::Flat params) {
    params.device(d) += to_add;
  }

  /**
   * @name  operator()
   * @brief Add a tensor to another one - Unaligned
   * @param[in] d           Device
   * @param[in] to_add      Tensor to add
   * @param[in,out] params  Tensor with the added value (+=)
   */
  void operator()(const CPUDevice& d,
                  typename tensorflow::TTypes<T>::UnalignedConstFlat to_add,
                  typename tensorflow::TTypes<T>::UnalignedFlat params) {
    params.device(d) += to_add;
  }
};

template <typename T>
struct DenseOp<CPUDevice, T, DenseOpType::kSub> {

  /**
   * @name  operator()
   * @brief Subtract a tensor to another one
   * @param[in] d           Device
   * @param[in] to_sub      Tensor to subtract
   * @param[in,out] params  Tensor with the subtracted value (-=)
   */
  void operator()(const CPUDevice& d,
                  typename tensorflow::TTypes<T>::ConstFlat to_sub,
                  typename tensorflow::TTypes<T>::Flat params) {
    params.device(d) -= to_sub;
  }

  /**
   * @name  operator()
   * @brief Subtract a tensor to another one - Unaligned
   * @param[in] d           Device
   * @param[in] to_sub      Tensor to subtract
   * @param[in,out] params  Tensor with the subtracted value (-=)
   */
  void operator()(const CPUDevice& d,
                  typename tensorflow::TTypes<T>::UnalignedConstFlat to_sub,
                  typename tensorflow::TTypes<T>::UnalignedFlat params) {
    params.device(d) -= to_sub;
  }
};

template <typename T>
struct DenseOp<CPUDevice, T, DenseOpType::kD2DCopy> {

  /**
   * @name  operator()
   * @brief Copy a tensor to another one (device to device)
   * @param[in] d           Device
   * @param[in] to_copy     Tensor to copy (device)
   * @param[in,out] params  Tensor with the copied value (device)
   */
  void operator()(const CPUDevice& d,
                  typename tensorflow::TTypes<T>::ConstFlat to_copy,
                  typename tensorflow::TTypes<T>::Flat params) {
    params.device(d) = to_copy;
  }

  /**
   * @name  operator()
   * @brief Copy a tensor to another one (device to device) - Unaligned
   * @param[in] d           Device
   * @param[in] to_copy     Tensor to copy (device)
   * @param[in,out] params  Tensor with the copied value (device)
   */
  void operator()(const CPUDevice& d,
                  typename tensorflow::TTypes<T>::UnalignedConstFlat to_copy,
                  typename tensorflow::TTypes<T>::UnalignedFlat params) {
    params.device(d) = to_copy;
  }
};

template <typename T>
struct DenseOp<CPUDevice, T, DenseOpType::kH2DCopy> {

  /**
   * @name  operator()
   * @brief Copy a tensor to another one from host to device (only copy when
   *        used with CPU device)
   * @param[in] d           Device
   * @param[in] to_copy     Tensor to copy (host)
   * @param[in,out] params  Tensor with the copied value (device)
   */
  void operator()(const CPUDevice& d,
                  typename tensorflow::TTypes<T>::ConstFlat to_copy,
                  typename tensorflow::TTypes<T>::Flat params) {
    params.device(d) = to_copy;
  }

  /**
   * @name  operator()
   * @brief Copy a tensor to another one from host to device (only copy when
   *        used with CPU device) - Unaligned
   * @param[in] d           Device
   * @param[in] to_copy     Tensor to copy (host)
   * @param[in,out] params  Tensor with the copied value (device)
   */
  void operator()(const CPUDevice& d,
                  typename tensorflow::TTypes<T>::UnalignedConstFlat to_copy,
                  typename tensorflow::TTypes<T>::UnalignedFlat params) {
    params.device(d) = to_copy;
  }
};

template <typename T>
struct DenseOp<CPUDevice, T, DenseOpType::kD2HCopy> {

  /**
   * @name  operator()
   * @brief Copy a tensor to another one from device to host (only copy when
   *        used with CPU device)
   * @param[in] d           Device
   * @param[in] to_copy     Tensor to copy (device)
   * @param[in,out] params  Tensor with the copied value (host)
   */
  void operator()(const CPUDevice& d,
                  typename tensorflow::TTypes<T>::ConstFlat to_copy,
                  typename tensorflow::TTypes<T>::Flat params) {
    params.device(d) = to_copy;
  }

  /**
   * @name  operator()
   * @brief Copy a tensor to another one from device to host (only copy when
   *        used with CPU device) - Unaligned
   * @param[in] d           Device
   * @param[in] to_copy     Tensor to copy (device)
   * @param[in,out] params  Tensor with the copied value (host)
   */
  void operator()(const CPUDevice& d,
                  typename tensorflow::TTypes<T>::UnalignedConstFlat to_copy,
                  typename tensorflow::TTypes<T>::UnalignedFlat params) {
    params.device(d) = to_copy;
  }
};

}  // namepsace Functor
}  // namepsace LTS5
#endif //__LTS5_TF_MATRIX_UTILS__
