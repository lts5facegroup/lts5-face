/**
 *  @file   mm_mask.hpp
 *  @brief  Mask generator for Morphable model
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   10/4/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_MM_MASK__
#define __LTS5_MM_MASK__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/tensorflow_op/mm_storage.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @struct  MMMaskGenerator
 * @brief   Generate standard deviation mask
 * @author  Christophe Ecabert
 * @date    04.10.18
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct MMMaskGenerator {

  /** Tensor type */
  using Tensor = tensorflow::Tensor;
  /** Context type */
  using OpKernelContext = tensorflow::OpKernelContext;
  /** Storage type */
  using Storage = PersistentStorage<T>;

  /**
   * @name operator()
   * @brief Compute texture standard deviation mask
   * @param[in] ctx Op context
   * @param[in] param Morphable model's parameters
   * @param[in] storage Persistent storage
   * @param[in] n_std   How much std to take into accound while generating the
   *                    mask
   * @param[in] mask    Generated mask with min/max texture value explainable by
   *                    the model
   */
  void operator()(OpKernelContext* ctx,
                  const Tensor& bcoord,
                  Tensor* mask);
};

}  // namespace Functor
}  // namespace LTS5
#endif  // __LTS5_MM_MASK__
