/**
 *  @file   sh_lighting.hpp
 *  @brief  Spherical Harmonics Lighting
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   10/7/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_SH_LIGHTING__
#define __LTS5_TF_SH_LIGHTING__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   SHLightingHelper
 * @brief   Compute rotation matrix using rodrigues formula
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct SHLightingHelper {
  /**
   * @name  operator()
   * @brief Compute lighting approximation
   * @param[in] ctx     Op's context
   * @param[in] aldebo  Reflectance without lighting
   * @param[in] normal  Surface's normals
   * @param[in] w_illu  Illumination parameters (SH weights)
   * @param[out] color  Color with illumination applied on it
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* aldebo,
                 const tf::Tensor* normal,
                 const tf::Tensor* w_illu,
                 tf::Tensor* color);
};

}  // namespace Functor
}  // namespace LTS5

/**
 * @class   SHLightingKernel
 * @brief   Kernel implementing spherical harmonics lighting
 * @author  Christophe Ecabert
 * @date    10/07/19
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class SHLightingKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  SHLightingKernel
   * @fn    explicit SHLightingKernel(tensorflow::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit SHLightingKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~SHLightingKernel
   * @fn    ~SHLightingKernel() override
   * @brief Destructor
   */
  ~SHLightingKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(SHLightingKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** SH Bands */
  int n_band_;
};

#endif  // __LTS5_TF_RODRIGUES__
