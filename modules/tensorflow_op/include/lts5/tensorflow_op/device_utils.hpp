/**
 *  @file   "lts5/tensorflow_op/device_utils.hpp"
 *  @brief  Helper tools for handling various device
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   03/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_DEVICE_UTILS__
#define __LTS5_TF_DEVICE_UTILS__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      Helper function
 */
namespace Functor {

/**
 * @struct  DeviceHelper
 * @brief   Utility class for managing various device
 * @author  Christophe Ecabert
 * @date    03.01.18
 * @ingroup tensorflow_op
 * @tparam Device   Device on which to run the conversion
 */
template<typename Device>
struct DeviceHelper {

  /**
   * @name  Synchronize
   * @fn    static void Synchronize(const Device& d)
   * @param[in] d   Device to synchronize
   * @brief Synchronize the device
   */
  static void Synchronize(const Device& d);

  /**
   * @name  Ok
   * @fn    static bool Ok(const Device& d)
   * @brief Check if everything's godd for a given device
   * @param[in] d   Device to check
   * @return    True if Ok, False otherwise
   */
  static bool Ok(const Device& d);
};

// ---------------------------------------------------------------

#pragma mark -
#pragma mark Implementation + Specialiazation

/** CPU Device Abstraction */
using CPUDevice = Eigen::ThreadPoolDevice;

template<>
struct DeviceHelper<CPUDevice> {

  /*
   * @name  Synchronize
   * @fn    static void Synchronize(const Device& d)
   * @param[in] d   Device to synchronize
   * @brief Synchronize the device
   */
  static void Synchronize(const CPUDevice& d) {
    // Nothing to do here
  }

  /*
   * @name  Ok
   * @fn    static bool Ok(const CPUDevice& d)
   * @brief Check if everything's godd for a given device
   * @param[in] d   Device to check
   * @return    True if Ok, False otherwise
   */
  static bool Ok(const CPUDevice& d) {
    return true;
  }
};


}  // namepsace Functor
}  // namepsace LTS5
#endif //__LTS5_TF_DEVICE_UTILS__
