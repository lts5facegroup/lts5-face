/**
 *  @file   lts5/tensorflow_op/utils/shape.hpp
 *  @brief  Utility functions to check tensor's shape dimensions
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/8/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_TENSORSHAPE_UTILS__
#define __LTS5_TF_TENSORSHAPE_UTILS__

#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/lib/core/errors.h"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   ShapeUtils
 * @brief   Utility function to check tensor dimensions
 * @author  Christophe Ecabert
 * @date    08/07/19
 * @ingroup tensorflow_op
 */
class ShapeUtils {
 public:

  /**
   * @name  CheckRank
   * @brief Check `t` matches expected rank
   * @param[in] ctx     Op's context
   * @param[in] t       Tensor to validate
   * @param[in] rank    Expected rank
   */
  static void CheckRank(tf::OpKernelContext* ctx,
                        const tf::Tensor& t,
                        const tf::int64& rank);

  /**
   * @name  CheckType
   * @brief Check `t` matches expected type
   * @param[in] ctx     Op's context
   * @param[in] t       Tensor to validate
   * @param[in] dtype   Expected data type
   */
  static void CheckType(tf::OpKernelContext* ctx,
                        const tf::Tensor& t,
                        const tf::DataType& dtype);

  /**
   * @name  CheckRankAndType
   * @brief Check `t` matches expected type
   * @param[in] ctx     Op's context
   * @param[in] t       Tensor to validate
   * @param[in] rank    Expected rank
   * @param[in] dtype   Expected data type
   */
  static void CheckRankAndType(tf::OpKernelContext* ctx,
                               const tf::Tensor& t,
                               const tf::int64& rank,
                               const tf::DataType& dtype);

  /**
   * @name  CheckShape
   * @brief Check `t` matches expected dim values/rank/type
   * @param[in] ctx     Op's context
   * @param[in] t       Tensor to validate
   * @param[in] rank    Expected rank
   * @param[in] dtype   Expected data type
   * @param[in] dims    List of expected dimensions, `-1` means unknown
   */
  static void CheckShape(tf::OpKernelContext* ctx,
                         const tf::Tensor& t,
                         const tf::int64& rank,
                         const tf::DataType& dtype,
                         const std::vector<tf::int64>& dims);
};
}  // namespace LTS5
#endif  // __LTS5_TF_TENSORSHAPE_UTILS__
