/**
 *  @file   bbox.hpp
 *  @brief  Utility function for bbox handling. Taken from Caffe SSD git
 *  @ingroup    tensorflow_op
 *  @see    https://github.com/weiliu89/caffe/blob/ssd/include/caffe/util/bbox_util.hpp
 *
 *  @author Christophe Ecabert
 *  @date   8/13/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_BBOX_UTILS__
#define __LTS5_TF_BBOX_UTILS__

#include <unordered_map>
#include <vector>

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {



#pragma mark -
#pragma mark Type definitions

/**
 * @struct  NormalizedBBox
 * @brief   Normalized bounding box [0, 1] w.r.t. the input image size.
 * @author  Christophe Ecabert
 * @date    13/08/19
 * @ingroup tensorflow_op
 * @tparam T    Data type
 */
template<typename T>
struct NormalizedBBox {

  /**
   * @name  NormalizedBBox
   * @brief Constructor
   * @fn    NormalizedBBox()
   */
  NormalizedBBox() : xmin(0.0),
                     ymin(0.0),
                     xmax(1.0),
                     ymax(1.0),
                     score(-1.0),
                     size(-1.0),
                     label(-1),
                     difficult(false) {
  }

  /**
   * @name  ComputeSize
   * @brief Compute bbox size (i.e. area)
   * @param[in] normalized  Indicate if bbox is normalized or not
   * @return Area
   */
  T ComputeSize(const bool& normalized) const {
    if (this->xmax < this->xmin || this->ymax < this->ymin) {
      // If bbox is invalid (e.g. xmax < xmin or ymax < ymin), return 0.
      return T(0.0);
    } else {
      T width = this->xmax - this->xmin;
      T height = this->ymax - this->ymin;
      if (normalized) {
        return width * height;
      } else {
        // If bbox is not within range [0, 1].
        return (width + 1) * (height + 1);
      }
    }
  }

  /**
   * @name  Intersect
   * @brief Compute the intersection between `this` and `other` bboxes
   * @param[in] other   Second bounding box
   * @return    Intersaecion box
   */
  NormalizedBBox<T> Intersect(const NormalizedBBox<T>& other) const {
    NormalizedBBox<T> intersect_bbox;
    if (other.xmin > this->xmax || other.xmax < this->xmin ||
        other.ymin > this->ymax || other.ymax < this->ymin) {
      // Return [0, 0, 0, 0] if there is no intersection.
      intersect_bbox.xmin = T(0.0);
      intersect_bbox.ymin = T(0.0);
      intersect_bbox.xmax = T(0.0);
      intersect_bbox.ymax = T(0.0);
    } else {
      intersect_bbox.xmin = std::max(this->xmin, other.xmin);
      intersect_bbox.ymin = std::max(this->ymin, other.ymin);
      intersect_bbox.xmax = std::min(this->xmax, other.xmax);
      intersect_bbox.ymax = std::min(this->ymax, other.ymax);
    }
    return intersect_bbox;
  }

  /**
   * @name  Clip
   * @brief Clip bounding box in range [0, 1.]
   */
  void Clip() {
    this->xmin = std::max(std::min(this->xmin, T(1.0)), T(0.0));
    this->ymin = std::max(std::min(this->ymin, T(1.0)), T(0.0));
    this->xmax = std::max(std::min(this->xmax, T(1.0)), T(0.0));
    this->ymax = std::max(std::min(this->ymax, T(1.0)), T(0.0));
    this->size = this->ComputeSize(true);
  }

  /** Xmin */
  T xmin;
  /** Ymin */
  T ymin;
  /** Xmax */
  T xmax;
  /** Ymax */
  T ymax;
  /** Box score, predicted by network */
  T score;
  /** Size */
  T size;
  /** Box label */
  int label;
  /** Indicator */
  bool difficult;
};

/**
 * @struct  ScoreIndex
 * @brief   Holds score/index pair
 * @tparam T    Data type
 */
template<typename T>
struct ScoreIndex {
  /** Score entry */
  T score;
  /** Index entry */
  int index;
};

template<typename T>
struct ScoreIndices {
  /** Score entry */
  T score;
  /** label */
  int label;
  /** Index  */
  int index;
};

/**
 * @enum    PriorBoxType
 * @brief   Encode/decode type.
 */
enum PriorBoxType {
  kCorner = 1,
  kCenterSize = 2,
  kCornerSize = 3
};

/** Labeled bouding box */
template<typename T>
using LabelBBox = std::unordered_map<int, std::vector<NormalizedBBox<T>>>;
/** Confidence score */
template<typename T>
using ConfScore = std::unordered_map<int, std::vector<T>>;
/** Priors variances */
template<typename T>
using Vars = std::vector<T>;

#pragma mark -
#pragma mark Functions

/**
 * @name    GetMaxScoreIndex
 * @brief   Get max scores with corresponding indices.
 * @param[in] scores        A set of scores.
 * @param[in] threshold     Only consider scores higher than the threshold.
 * @param[in] top_k         If -1, keep all; otherwise, keep at most top_k.
 * @param[out] score_index  Store the sorted (score, index) pair.
 * @tparam T    Data type
 */
template<typename T>
void GetMaxScoreIndex(const std::vector<T>& scores,
                      const T& threshold,
                      const int& top_k,
                      std::vector<ScoreIndex<T>>* score_index);

/**
 * @name    JaccardOverlap
 * @brief   Compute the jaccard (intersection over union IoU) overlap between
 *          two bboxes.
 * @param[in] bbox1         BBox 1
 * @param[in] bbox2         BBox 2
 * @param[in] normalized    If true, indicates that bbox are normalized.
 * @tparam T    Data type
 * @return  IoU
 */
template<typename T>
T JaccardOverlap(const NormalizedBBox<T>& bbox1,
                 const NormalizedBBox<T>& bbox2,
                 const bool& normalized);

/**
 * @name    GetLocPredictions
 * @brief   Get location predictions from loc_data.
 * @param[in] loc_data  Array of predicted location:
 *                      num x num_preds_per_class * num_loc_classes * 4
 * @param[in] num       The number of images in the batch
 * @param[in] num_preds_per_class   Number of predictions per class
 * @param[in] num_loc_classes   number of location classes. It is 1 if
 *                              `share_location` is true; and is equal to number
 *                              of classes needed to predict otherwise.
 * @param[in] share_location    if true, all classes share the same location
 *                              prediction.
 * @param[out] loc_preds        stores the location prediction, where each item
 *                              contains location prediction for an image.
 * @tparam T    Data type
 */
template<typename T>
void GetLocPredictions(const T* loc_data,
                       const int& num,
                       const int& num_preds_per_class,
                       const int& num_loc_classes,
                       const bool& share_location,
                       std::vector<LabelBBox<T>>* loc_preds);

/**
 * @name    GetConfidenceScores
 * @brief   Get confidence predictions from conf_data.
 * @param[in] conf_data          num x num_preds_per_class * num_classes array
 * @param[in] num                   The number of images.
 * @param[in] num_preds_per_class   Number of predictions per class.
 * @param[in] num_classes           Number of classes.
 * @param[out] conf_scores          stores the confidence prediction, where
 *                                  each item contains confidence prediction for
 *                                  an image.
 * @tparam T    Data type
 */
template <typename T>
void GetConfidenceScores(const T* conf_data,
                         const int& num,
                         const int& num_preds_per_class,
                         const int& num_classes,
                         std::vector<ConfScore<T>>* conf_scores);

/**
 * @name    GetPriorBBoxes
 * @brief   Get prior bounding boxes from prior_data.
 * @param[in] prior_data    2 x num_priors x 4 array of priors (i.e. anchors)
 * @param[in] num_priors    Number of priors.
 * @param[out] prior_bboxes Stores all the prior bboxes in the format of
 *                          NormalizedBBox.
 * @param[out] prior_variances  Stores all the variances needed by prior bboxes.
 * @tparam T    Data type
 */
template <typename T>
void GetPriorBBoxes(const T* prior_data,
                    const int& num_priors,
                    std::vector<NormalizedBBox<T>>* prior_bboxes,
                    std::vector<Vars<T>>* prior_variances);

/**
 * @name    DecodeBBox
 * @brief   Decode a bbox according to a prior bbox.
 * @param[in] prior_bbox        Box prior
 * @param[in] prior_variance    Box variance
 * @param[in] code_type         Decoding type
 * @param[in] variance_encoded_in_target
 * @param[in] clip_bbox
 * @param[in] bbox          Box to decode
 * @param[out] decode_bbox  Decoded box
 * @tparam T    Data type
 */
template<typename T>
void DecodeBBox(const NormalizedBBox<T>& prior_bbox,
                const Vars<T>& prior_variance,
                const PriorBoxType& code_type,
                const bool& variance_encoded_in_target,
                const bool& clip_bbox,
                const NormalizedBBox<T>& bbox,
                NormalizedBBox<T>* decode_bbox);

/**
 * @name    DecodeBBoxes
 * @brief   Decode a set of bboxes according to a set of prior bboxes.
 * @param prior_bboxes      Boxes prior
 * @param prior_variances   Boxes variance
 * @param code_type         Decoding type
 * @param variance_encoded_in_target
 * @param clip_bbox
 * @param bboxes            Boxes to decode
 * @param decode_bboxes     Decoded boxes
 * @tparam T    Data type
 */
template<typename T>
void DecodeBBoxes(const std::vector<NormalizedBBox<T>>& prior_bboxes,
                  const std::vector<Vars<T>>& prior_variances,
                  const PriorBoxType& code_type,
                  const bool& variance_encoded_in_target,
                  const bool& clip_bbox,
                  const std::vector<NormalizedBBox<T>>& bboxes,
                  std::vector<NormalizedBBox<T>>* decode_bboxes);

/**
 * @name    DecodeBBoxesAll
 * @brief   Decode all bboxes in a batch.
 * @param[in] all_loc_pred  stores the location prediction, where each item
 *                          contains location prediction for an image.
 * @param[in] prior_bboxes
 * @param[in] prior_variances   Stores all the variances needed by prior bboxes.
 * @param[in] num
 * @param[in] share_location
 * @param[in] num_loc_classes
 * @param[in] background_label_id
 * @param[in] code_type
 * @param[in] variance_encoded_in_target
 * @param[in] clip
 * @param[out] all_decode_bboxes
 * @tparam T    Data type
 */
template<typename T>
void DecodeBBoxesAll(const std::vector<LabelBBox<T>>& all_loc_pred,
                     const std::vector<NormalizedBBox<T>>& prior_bboxes,
                     const std::vector<Vars<T>>& prior_variances,
                     const int& num,
                     const bool& share_location,
                     const int& num_loc_classes,
                     const int& background_label_id,
                     const PriorBoxType& code_type,
                     const bool& variance_encoded_in_target,
                     const bool& clip,
                     std::vector<LabelBBox<T>>* all_decode_bboxes);

/**
 * @name    ApplyNMSFast
 * @brief   Do non maximum suppression given bboxes and scores. Inspired by
 *          Piotr Dollar's NMS implementation in EdgeBox.
 *          https://goo.gl/jV3JYS
 * @param[in] bboxes            A set of bounding boxes.
 * @param[in] scores            A set of corresponding confidences.
 * @param[in] score_threshold   A threshold used to filter detection results.
 * @param[in] nms_threshold     A threshold used in non maximum suppression.
 * @param[in] eta               Adaptation rate for nms threshold
 *                              (see Piotr's paper).
 * @param[in] top_k             If not -1, keep at most top_k picked indices.
 * @param[out] indices          the kept indices of bboxes after nms.
 * @tparam T    Data type
 */
template<typename T>
void ApplyNMSFast(const std::vector<NormalizedBBox<T>>& bboxes,
                  const std::vector<T>& scores,
                  const T& score_threshold,
                  const T& nms_threshold,
                  const T& eta,
                  const int& top_k,
                  std::vector<int>* indices);

}  // namespace LTS5
#endif  // __LTS5_TF_BBOX_UTILS__
