/**
 *  @file   renderer_op.hpp
 *  @brief  Rendering operation using standard OpenGL pipeline
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/10/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_RENDERER_OP__
#define __LTS5_TF_RENDERER_OP__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"

#include "lts5/tensorflow_op/mm_renderer.hpp"
#include "lts5/tensorflow_op/mm_interpolator.hpp"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

}  // namespace Functor
}  // namespace LTS5

/**
 * @class   RenderingKernel
 * @brief   Kernel implementing spherical harmonics lighting
 * @author  Christophe Ecabert
 * @date    10/07/19
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class RenderingKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  RenderingKernel
   * @fn    explicit RenderingKernel(tensorflow::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit RenderingKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~RenderingKernel
   * @fn    ~RenderingKernel() override
   * @brief Destructor
   */
  ~RenderingKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(RenderingKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Renderer parameters */
  using RendererParameters = LTS5::MMRendererParameters<T>;
  /** Renderer */
  using Renderer = LTS5::MMRendererV2<Device, T>;
  /** Shader */
  using Shader = LTS5::ImageShader<T>;
  /** Interpolator */
  using Interpolator = LTS5::MMInterpolator<Device, T>;

  /** Mutex */
  tf::mutex mutex_;
  /** Renderer */
  Renderer* renderer_;
  /** Interpolator */
  Interpolator* interp_;
  /** Shader */
  Shader* shader_;
  /** Edge Shader */
  LTS5::OGLProgram* edge_shader_;
  /** Rendering attributes */
  RendererParameters r_params_;
  /** Training flag */
  bool train_;
  /** Initialization flag */
  bool initialized_;
};


#endif  // __LTS5_TF_RENDERER_OP__
