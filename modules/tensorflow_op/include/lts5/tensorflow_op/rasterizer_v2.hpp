/**
 *  @file   rasterizer_v2.hpp
 *  @brief  OpenGL-bassed rasterizer
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   11/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TENSORFLOW_OP_RASTERIZER_V2__
#define __LTS5_TENSORFLOW_OP_RASTERIZER_V2__

#include "tensorflow/core/framework/tensor.h"

#include "lts5/ogl/context_lightweight.hpp"
#include "lts5/ogl/vertex_array_object.hpp"
#include "lts5/ogl/frame_buffer_object.hpp"
#include "lts5/ogl/program.hpp"
#include "lts5/ogl/command.hpp"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLResources
 * @brief   Interface with OpenGL buffer to transfer data
 * @author  Christophe Ecabert
 * @date    12/11/2020
 * @tparam Device   Device on which to run the computation
 * @tparam T    Data type
 */
template<typename Device, typename T>
class OGLResources {
 public:
  /** Type of buffer */
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;

  /**
   * @name  OGLResources
   * @fn    OGLResources()
   * @brief Constructor
   */
  OGLResources();

  /**
   * @name  ~OGLResources
   * @fn    ~OGLResources()
   * @brief Destructor
   */
  ~OGLResources();

  /**
   * @name  Register
   * @brief Register an existing OpenGL buffer
   * @param[in] buffer Buffer index to register
   * @param[in] type    Type of buffer to regiuster
   * @return    -1 if error, 0 otherwise
   */
  int Register(const unsigned int& buffer, const BufferType& type);

  /**
   * @name  UnRegister
   * @brief Unregister the underlying resources
   */
  void UnRegister();

  /**
   * @name  Upload
   * @brief Upload data to the buffer
   * @param[in] ctx     Op's context
   * @param[in] data    Data to transfer
   * @return -1 if error, 0 otherwise
   */
  int Upload(tf::OpKernelContext* ctx, const tf::Tensor& data);
};

/**
 * @class   OGLImage
 * @brief   Interface with OpenGL texture to transfer data
 * @author  Christophe Ecabert
 * @date    12/11/2020
 * @tparam Device   Device on which to run the computation
 * @tparam T    Data type
 */
template<typename Device, typename T>
class OGLImage {
 public:
  /**
   * @name  OGLImage
   * @fn    OGLImage()
   * @brief Constructor
   */
  OGLImage();

  /**
   * @name  Register
   * @brief Register an existing OpenGL buffer
   * @param[in] buffer Buffer index to register
   * @return    -1 if error, 0 otherwise
   */
  int Register(const unsigned int& buffer);

  /**
   * @name  UnRegister
   * @brief Unregister the underlying resources
   */
  void UnRegister();

  /**
   * @name  Upload
   * @brief Upload data to the buffer
   * @param[in] ctx     Op's context
   * @param[in] data    Tensor where to transfer the data
   * @return -1 if error, 0 otherwise
   */
  int Download(tf::OpKernelContext* ctx, tf::Tensor* data);
};

/**
 * @class   RasterizerV2
 * @brief   Rasterize batch of 3D surface using OpenGL
 * @author  Christophe Ecabert
 * @date    12/11/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class RasterizerV2 {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  RasterizerV2
   * @fn    RasterizerV2()
   * @brief Constructor
   */
  RasterizerV2();

  /**
   * @name  RasterizerV2
   * @fn    RasterizerV2(const RasterizerV2& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  RasterizerV2(const RasterizerV2& other) = delete;

  /**
   * @name operator=
   * @fn RasterizerV2& operator=(const RasterizerV2& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  RasterizerV2& operator=(const RasterizerV2& rhs) = delete;

  /**
   * @name  ~RasterizerV2
   * @fn    ~RasterizerV2();
   * @brief Destructor
   */
  ~RasterizerV2();

  /**
   * @name  InitOpenGL
   * @fn    int InitOpenGL()
   * @brief Initialize all opengl component
   * @return    -1 if error, 0 otherwise
   */
  int InitOpenGL();

#pragma mark -
#pragma mark Usage

  /**
   * @name  ActiveContext
   * @fn    void ActiveContext() const
   * @brief Enable opengl context
   */
  void ActiveContext() const;

  /**
   * @name  ReleaseContext
   * @fn    void ReleaseContext() const
   * @brief Release opengl context
   */
  void ReleaseContext() const;

  /**
   * @name  ResizeBuffers
   * @brief Resize internal opengl buffers to fit the given data
   * @param[in] ctx Op's context
   * @param[in] vertex  3D Tensor storing vertices in clip space, [B, N, 4]
   * @param[in] triangle  2D Tensor with shared triangulation [T, 3]
   * @param[in] width   Image width
   * @param[in] height  Image height
   * @return    -1 if error, 0 otherwise
   */
  int ResizeBuffers(tf::OpKernelContext* ctx,
                    const tf::Tensor& vertex,
                    const tf::Tensor& triangle,
                    const int& width,
                    const int& height);

  /**
   * @name
   * @brief Render barycentric coordinates
   * @param[in] ctx
   * @param[in] vertex
   * @param[in] triangle
   * @param[out] bcoords
   * @return -1 if error, 0 otherwise
   */
  int Render(tf::OpKernelContext* ctx,
             const tf::Tensor& vertex,
             const tf::Tensor& triangle,
             tf::Tensor* bcoords);

#pragma mark -
#pragma mark Private
 private:
  /** Shader type */
  using Shader = OGLProgram;

  /**
   *  @enum BufferIdx
   *  @brief  Position of the attributes in the buffer array
   */
  enum BufferIdx {
    /** Vertex buffer index */
    kVertex = 0,
    /** Triangle buffer index */
    kTriangle = 1
  };

  /** Context */
  OGLContextLightweight ctx_;
  /** VAO */
  OGLVertexArrayObject<T> vao_;
  /** FBO */
  FrameBufferObject fbo_;
  /** Shader */
  Shader shader_;
  /** Vertex resource */
  OGLResources<Device, T>* ogl_vertex_buffer_;
  /** Triangle resource */
  OGLResources<Device, int>* ogl_tri_buffer_;
  /** Image resource */
  OGLImage<Device, T>* ogl_img_buffer_;
  /** Rendering command */
  std::vector<OGLDrawCommand> cmd_;
  /** Size of opengl buffer for vertex */
  int ogl_vertex_size_;
  /** Size of opengl buffer for triangle */
  int ogl_tri_size_;
  /** Image height */
  int height_;
  /** Image width */
  int width_;
  /** Image depth */
  int depth_;
  /** Number of triangle */
  int n_tri_;

};

}  // namespace LTS5
#endif  // __LTS5_TENSORFLOW_OP_RASTERIZER_V2__
