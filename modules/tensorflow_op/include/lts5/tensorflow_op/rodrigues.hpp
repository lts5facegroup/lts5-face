/**
 *  @file   lts5/tensorflow_op/rodrigues.hpp
 *  @brief  Rotation parametrization using Rodrigues formula.
 *  @see    https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/8/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_RODRIGUES__
#define __LTS5_TF_RODRIGUES__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   RodriguesHelper
 * @brief   Compute rotation matrix using rodrigues formula
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct RodriguesHelper {
  /**
   * @name  operator()
   * @brief Compute rotation matrix using Rodrigues formula
   * @param[in] ctx     Op's context
   * @param[in] rvec    Rotation vector
   * @param[out] rmat   Rotation matrix
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* rvec,
                 tf::Tensor* rmat);
};

}  // namespace Functor
}  // namespace LTS5

/**
 * @class   RodriguesKernel
 * @brief   Kernel implementing rodrigues rotation matrix conversion
 * @author  Christophe Ecabert
 * @date    08/07/19
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class RodriguesKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  RodriguesKernel
   * @fn    explicit RodriguesKernel(tensorflow::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit RodriguesKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~RodriguesKernel
   * @fn    ~RodriguesKernel() override
   * @brief Destructor
   */
  ~RodriguesKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(RodriguesKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
};

#endif  // __LTS5_TF_RODRIGUES__
