/**
 *  @file   lts5/tensorflow_op/mm_wrapper.hpp
 *  @brief  Morphable model abstraction for tensorflow framework
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   03/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_MM_WRAPPER__
#define __LTS5_TF_MM_WRAPPER__

#include <iostream>
#include <vector>
#include <string>

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/framework/resource_mgr.h"

#include "lts5/utils/math/vector.hpp"
#include "lts5/tensorflow_op/file_io.hpp"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   MorphableModelContainer
 * @brief   Morphable model abstraction
 * @author  Christophe Ecabert
 * @date    03.01.18
 * @tparam T    Data type
 * @ingroup tensorflow_op
 */
template<typename T>
class MorphableModelContainer {
 public:
#pragma mark -
#pragma mark Type definition
  /** Eigen Matrix type */
  using EMatrix = typename IO<T>::EMatrix;
  /** Matrix - int */
  using EIntMatrix = typename IO<T>::EIntMatrix;
  /** Triangle type */
  using Triangle = Vector3<int>;
  /** Vpe */
  using Vpe = Vector2<int>;
  /** Fpe */
  using Fpe = Vector2<int>;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  MorphableModelContainer
   * @fn    MorphableModelContainer()
   * @brief Constructor
   */
  MorphableModelContainer();

  /**
   * @name  ~MorphableModelContainer
   * @fn    virtual ~MorphableModelContainer() = default
   * @brief Destructor
   */
  virtual ~MorphableModelContainer() = default;

  /**
   * @name  Load
   * @fn    int Load(const std::string& filename)
   * @brief Load a model into memory
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream)
   * @brief Load a model into memory
   * @param[in] stream    Binary stream from which to load the model
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_meanshape_buffer
   * @fn    const EMatrix& get_meanshape_buffer() const
   * @brief Provide shape meanshape
   * @return    Meanshape
   */
  const EMatrix& get_meanshape_buffer() const {
    return meanshape_buffer_;
  }

  /**
   * @name  get_shape_variation_buffer
   * @fn    const EMatrix& get_shape_variation_buffer() const
   * @brief Provide shape variation
   * @return    Shape's eigenvector
   */
  const EMatrix& get_shape_variation_buffer() const {
    return shape_basis_buffer_;
  }

  /**
   * @name  get_meantexture_buffer
   * @fn    const EMatrix& get_meantexture_buffer() const
   * @brief Provide texture meanshape
   * @return    Meantexture
   */
  const EMatrix& get_meantexture_buffer() const {
    return meantexture_buffer_;
  }

  /**
   * @name  get_texture_variation_buffer
   * @fn    const EMatrix& get_texture_variation_buffer() const
   * @brief Provide texture variation
   * @return    Texture's eigenvector
   */
  const EMatrix& get_texture_variation_buffer() const {
    return tex_basis_buffer_;
  }

  /**
   * @name  get_texture_std_buffer
   * @fn    const EMatrix& get_texture_std_buffer() const
   * @brief Provide per-vertex texture standard deviation
   * @return    Texture's standard deviation
   */
  const EMatrix& get_texture_std_buffer() const {
    return tex_std_buffer_;
  }

  /**
   * @name  get_connectivity_buffer
   * @fn    const EIntMatrix& get_connectivity_buffer() const
   * @brief Vertex connectivity
   * @return   List of edges for each vertex
   */
  const EIntMatrix& get_connectivity_buffer() const {
    return conn_;
  }

  /**
   * @name  get_n_vertex
   * @fn    size_t get_n_vertex() const
   * @brief Provide the shape dimension
   * @return Number of vertex in the shape model
   */
  size_t get_n_vertex() const {
    return n_vertex_;
  }

  /**
   * @name  get_n_texel
   * @fn    size_t get_n_texel() const
   * @brief Provide the texture dimension
   * @return Number of texel in the color model
   */
  size_t get_n_texel() const {
    return n_texel_;
  }

  /**
   * @name  get_n_channels
   * @fn    size_t get_n_channels() const
   * @brief Provide the number of channels in the texture model
   * @return Number of texture channels
   */
  size_t get_n_channels() const {
    return n_tex_channel_;
  }

  /**
   * @name  get_n_triangle
   * @fn    size_t get_n_triangle() const
   * @brief Provide how many triangles are in the shape model tessalation
   * @return Number of triangles in the surface tessalation
   */
  size_t get_n_triangle() const {
    return n_tri_;
  }

  /**
   * @name  get_n_edge
   * @fn    size_t get_n_edge() const
   * @brief Indicate how many edges the model contains
   * @return    Number of edges
   */
  size_t get_n_edge() const {
    return vpe_.size();
  }

  /**
   * @name  get_triangle
   * @fn    const std::vector<Triangle>& get_triangle() const
   * @brief Provide the triangle list
   * @return    List of triangle
   */
  const std::vector<Triangle>& get_triangle() const {
    return tri_;
  }

  /**
   * @name get_vertex_per_edge
   * @fn    const std::vector<Vpe>& get_vertex_per_edge() const
   * @brief Give the list of vertex index for all edges
   * @return    Vertex index for edges
   */
  const std::vector<Vpe>& get_vertex_per_edge() const {
    return vpe_;
  }

  /**
   * @name get_face_per_edge
   * @fn    const std::vector<Fpe>& get_face_per_edge() const
   * @brief Give the list of face index for all edges
   * @return    Face index for edges
   */
  const std::vector<Fpe>& get_face_per_edge() const {
    return fpe_;
  }

  /**
   * @name  get_n_shape_parameter
   * @fn    size_t get_n_shape_parameter() const
   * @brief Provide the number of parameters for the shape
   * @return    Shape's parameter dimension
   */
  size_t get_n_shape_parameter() const {
    return n_shape_parameter_;
  }

  /**
   * @name  get_n_tex_parameter
   * @fn    size_t get_n_tex_parameter() const
   * @brief Provide the number of parameters for the texture
   * @return    Texture's parameter dimension
   */
  size_t get_n_tex_parameter() const {
    return n_tex_parameter_;
  }

  /**
   * @name  get_n_illu_parameter
   * @fn    size_t get_n_illu_parameter() const
   * @brief Provide the number of parameters for the illumination model
   * @return    Illumination's parameter dimension
   */
  size_t get_n_illu_parameter() const {
    return n_tex_channel_ * 9;
  }

  /**
   * @name  get_n_color_trsfrm_parameter
   * @fn    size_t get_n_color_trsfrm_parameter() const
   * @brief Provide the number of parameters for the affine color transformation
   *        model
   * @return    Color transformation's parameter dimension
   */
  size_t get_n_color_trsfrm_parameter() const {
    return n_color_trsrm_parameter_;
  }

  /**
   * @name  get_n_parameter
   * @fn    size_t get_n_parameter() const
   * @brief Indicate the dimension of model parameters (shape + texture + illu
   *        + color transform)
   * @return    \f$ n_{s} + n_{t} + n_{i} \f$
   */
  size_t get_n_parameter() const {
    // Color transform: [gain * 3, offset * 3, contrast]
    return (n_shape_parameter_ +
            n_tex_parameter_ +
            (n_tex_channel_ * 9) +
            n_color_trsrm_parameter_);
  }

  /**
   * @name  get_shape_dimensions
   * @fn    const std::vector<int>& get_shape_dimensions() const
   * @brief Return list of shape dimensions
   * @return    Size of each shape model dimensions
   */
  const std::vector<int>& get_shape_dimensions() const {
    return this->dim_;
  }

  /**
   * @name  get_cog
   * @fn    const std::vector<T>& get_cog() const
   * @brief Return model's center of gravitiy
   * @return    Cog (dx, dy, dz)
   */
  const std::vector<T>& get_cog() const {
    return cog_;
  }

#pragma mark -
#pragma mark Private

 private:
  /**
   * @name  LoadShapeModel
   * @fn    int LoadShapeModel(std::istream& stream)
   * @brief Load shape model
   * @param[in] stream  Binary stream from which model need to be loaded
   * @return -1 if error, 0 otherwise
   */
  int LoadShapeModel(std::istream& stream);

  /**
   * @name  LoadTextureModel
   * @fn    int LoadTextureModel(std::istream& stream)
   * @brief Load texture model
   * @param[in] stream  Binary stream from which model need to be loaded
   * @return -1 if error, 0 otherwise
   */
  int LoadTextureModel(std::istream& stream);

  /**
   * @name  LoadStandardDeviationModel
   * @fn    int LoadStandardDeviationModel(std::istream& stream)
   * @brief Load per-vertex variance model if any present. Assume model is not
   *        present if stream reach the end (stream.good() == false).
   * @param[in] stream  Binary stream from which model need to be loaded
   * @return -1 if error, 0 otherwise. If model is not present no error will be
   *         emitted
   */
  int LoadStandardDeviationModel(std::istream &stream);

  /**
   * @name  BuildConnectivity
   * @fn    void BuildConnectivity()
   * @brief Build connectivity map
   */
  void BuildConnectivity();

 protected:
  /** Meanshape Raw Buffer */
  EMatrix meanshape_buffer_;
  /** Shape Eigenvectors (scaled by eigenvalues) */
  EMatrix shape_basis_buffer_;
  /** Meantexture Raw Buffer */
  EMatrix meantexture_buffer_;
  /** Texture Eigenvectors (scaled by eigenvalues) */
  EMatrix tex_basis_buffer_;
  /** Appearance per-vertex standard deviation  */
  EMatrix tex_std_buffer_;
  /** Connectivity */
  EIntMatrix conn_;

 private:
  /** Cog */
  std::vector<T> cog_;
  /** Number of vertex */
  size_t n_vertex_;
  /** Number of texel */
  size_t n_texel_;
  /** Number of triangle */
  size_t n_tri_;
  /** Triangle list */
  std::vector<Triangle> tri_;
  /** Vpe - Vertex per edges */
  std::vector<Vpe> vpe_;
  /** Fpe - Faces per edges */
  std::vector<Fpe> fpe_;
  /** Shape channels */
  size_t n_shape_channel_;
  /** Texutre chanel */
  size_t n_tex_channel_;
  /** Shape dimensions */
  std::vector<int> dim_;
  /** Number of shape parameters */
  size_t n_shape_parameter_;
  /** Number of texture parameters */
  size_t n_tex_parameter_;
  /** Number of color transform parameters */
  size_t n_color_trsrm_parameter_;
};


/**
 * @class   MorphableModelContainerV2
 * @brief   Morphable model abstraction including GPU storage
 * @author  Christophe Ecabert
 * @date    03.07.19
 * @tparam T    Data type
 * @ingroup tensorflow_op
 */
template<typename T>
class MorphableModelContainerV2 : public MorphableModelContainer<T> {
 public:
  /** Context */
  using Context = tensorflow::OpKernelContext;
  /** Persistent Tensor */
  using PTensor = tensorflow::PersistentTensor;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  MorphableModelContainerV2
   * @fn    MorphableModelContainerV2() = default
   * @brief Constructor
   */
  MorphableModelContainerV2() = default;

  /**
   * @name  ~MorphableModelContainerV2
   * @fn    ~MorphableModelContainerV2() = default
   * @brief Destructor
   */
  ~MorphableModelContainerV2() override = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Upload
   * @fn    void Upload(Context* ctx)
   * @brief Push data to device
   * @param[in,out] ctx Context
   * @tparam Device Device on which the computation is run
   */
  template<typename Device>
  void Upload(Context* ctx);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_mean_shape
   * @fn    const tf::Tensor* get_mean_shape(Context* ctx)
   * @brief Access mean shape on device
   * @param[in,out] ctx Op's context
   * @return    Tensor
   */
  const tf::Tensor* get_mean_shape(Context* ctx) {
    return this->d_mean_shape_.AccessTensor(ctx);
  }

  /**
   * @name  get_var_shape
   * @fn    const tf::Tensor* get_var_shape(Context* ctx)
   * @brief Access shape variance on device
   * @param[in,out] ctx Op's context
   * @return    Tensor
   */
  const tf::Tensor* get_var_shape(Context* ctx) {
    return this->d_var_shape_.AccessTensor(ctx);
  }

  /**
   * @name  get_mean_tex
   * @fn    const tf::Tensor* get_mean_tex(Context* ctx)
   * @brief Access mean texture on device
   * @param[in,out] ctx Op's context
   * @return    Tensor
   */
  const tf::Tensor* get_mean_tex(Context* ctx) {
    return this->d_mean_tex_.AccessTensor(ctx);
  }

  /**
   * @name  get_var_tex
   * @fn    const tf::Tensor* get_var_tex(Context* ctx)
   * @brief Access texture variance on device
   * @param[in,out] ctx Op's context
   * @return    Tensor
   */
  const tf::Tensor* get_var_tex(Context* ctx) {
    return this->d_var_tex_.AccessTensor(ctx);
  }

  /**
   * @name  get_triangle
   * @fn    const tf::Tensor* get_triangle(Context* ctx)
   * @brief Access triangulation on device
   * @param[in,out] ctx Op's context
   * @return    Tensor
   */
  const tf::Tensor* get_triangle(Context* ctx) {
    return this->d_tri_.AccessTensor(ctx);
  }

  /**
   * @name  get_connectivity
   * @fn    const tf::Tensor* get_connectivity(Context* ctx)
   * @brief Access connectivity on device
   * @param[in,out] ctx Op's context
   * @return    Tensor
   */
  const tf::Tensor* get_connectivity(Context* ctx) {
    return this->d_conn_.AccessTensor(ctx);
  }

  /**
   * @name  get_fpe
   * @fn    const tf::Tensor* get_fpe(Context* ctx)
   * @brief Access face-to-edge map on device
   * @param[in,out] ctx Op's context
   * @return    Tensor
   */
  const tf::Tensor* get_fpe(Context* ctx) {
    return this->d_fpe_.AccessTensor(ctx);
  }

  /**
   * @name  get_vpe
   * @fn    const tf::Tensor* get_vpe(Context* ctx)
   * @brief Access vertex-to-edge map on device
   * @param[in,out] ctx Op's context
   * @return    Tensor
   */
  const tf::Tensor* get_vpe(Context* ctx) {
    return this->d_vpe_.AccessTensor(ctx);
  }

#pragma mark -
#pragma mark Prive

 private:
  /** Device meanshape */
  PTensor d_mean_shape_;
  /** Device shape variance */
  PTensor d_var_shape_;
  /** Device mean tex */
  PTensor d_mean_tex_;
  /** Device tex variance */
  PTensor d_var_tex_;
  /** Device triangle */
  PTensor d_tri_;
  /** Device connectivity */
  PTensor d_conn_;
  /** Device - edge-to-face map */
  PTensor d_fpe_;
  /** Device - vertex-to-face map */
  PTensor d_vpe_;
};

/**
 * @class   SharedMorphableModelContainerV2
 * @brief   Morphable model abstraction including GPU storage shared accros ops.
 *          The class is therefore refcounted!
 * @author  Christophe Ecabert
 * @date    03.07.19
 * @tparam T    Data type
 * @ingroup tensorflow_op
 */
template<typename T>
class SharedMorphableModelContainerV2 : public MorphableModelContainerV2<T>,
                                      public tf::ResourceBase {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  SharedMorphableModelContainerV2
   * @fn    SharedMorphableModelContainerV2() = default
   * @brief Constructor
   */
  SharedMorphableModelContainerV2() = default;

  /**
   * @name  ~SharedMorphableModelContainerV2
   * @fn    ~SharedMorphableModelContainerV2() = default
   * @brief Destructor
   */
  ~SharedMorphableModelContainerV2() override = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name    DebugString
   * @fn  std::string DebugString() const override
   * @brief Provide debug information about this class
   * @return  "SharedMorphableModelContainerV2"
   */
  std::string DebugString() const override {
    return "SharedMorphableModelContainerV2";
  }
};
}  // namespace LTS5
#endif  // __LTS5_TF_MM_WRAPPER__
