/**
 *  @file   lts5/tensorflow_op/mm_renderer.hpp
 *  @brief  Transform an instance of a Morphable Model into an image
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   15/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_MM_RENDERER__
#define __LTS5_TF_MM_RENDERER__

#include "tensorflow/core/framework/tensor.h"

#include "lts5/ogl/offscreen_renderer.hpp"
#include "lts5/tensorflow_op/mm_renderer_utils.hpp"
#include "lts5/tensorflow_op/mm_parameter.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   MMRendererV2
 * @brief   Transform an instance of a Morphable Model into an image
 * @author  Christophe Ecabert
 * @date    15/01/2018
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class MMRendererV2 {
 public:
  /** Rendering parameters */
  using Parameters = MMRendererParameters<T>;
  /** Shader type */
  using Shader = OGLProgram;
  /** Renderer type */
  using Renderer = OGLOffscreenRenderer<T, LTS5::Vector3<T>>;
  /** OGL Buffer information */
  using BufferInfo = typename Renderer::BufferInfo;
  /** Capability */
  using Capability = typename Renderer::Capability;
  /** DepthFunc */
  using DepthFunc = typename Renderer::DepthFunc;
  /** PolyMode */
  using PolyMode = typename Renderer::PolyMode;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  MMRendererV2
   * @fn    MMRendererV2()
   * @brief Constructor
   */
  MMRendererV2();

  /**
   * @name  MMRendererV2
   * @fn    MMRendererV2(const MMRendererV2& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  MMRendererV2(const MMRendererV2& other) = delete;

  /**
   * @name  operator=
   * @fn    MMRendererV2& operator=(const MMRendererV2& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  MMRendererV2& operator=(const MMRendererV2& rhs) = delete;

  /**
   * @name  MMRendererV2
   * @fn    MMRendererV2(MMRendererV2&& other) = delete
   * @brief Move constructor
   * @param[in] other Object to move from
   */
  MMRendererV2(MMRendererV2&& other) = delete;

  /**
   * @name  operator=
   * @fn    MMRendererV2& operator=(MMRendererV2&& rhs) = delete
   * @brief Move-Assignment operator
   * @param[in] rhs Object to move-assign from
   * @return    Newly moved-assign object
   */
  MMRendererV2& operator=(MMRendererV2&& rhs) = delete;

  /**
   * @name  ~MMRendererV2
   * @fn    ~MMRendererV2();
   * @brief Destructor
   */
  ~MMRendererV2();

  /**
   * @name  Init
   * @fn    int Init(const Parameters& params)
   * @brief Initialize renderer
   * @param[in] params  Renderer parameters
   * @return    -1 if error, 0 otherwise
   */
  int Init(const Parameters& params);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Enable
   * @fn    void Enable(const Capability& cap) const
   * @brief Enable an OpenGL capability
   * @param[in] cap OpenGL's capability to enable
   */
  void Enable(const Capability& cap) const {
    renderer_->Enable(cap);
  }

  /**
   * @name  Disable
   * @fn    void Disable(const Capability& cap) const
   * @brief Disable an OpenGL capability
   * @param[in] cap OpenGL's capability to disable
   */
  void Disable(const Capability& cap) const {
    renderer_->Disable(cap);
  }

  /**
   * @name  DepthFunction
   * @fn    void DepthFunction(const DepthFunc& func) const
   * @brief Define which criteria is used to performed depth test when enabled
   * @param[in] func Depth test criteria
   */
  void DepthFunction(const DepthFunc& func) const {
    renderer_->DepthFunction(func);
  }

  /**
   * @name PolygonMode
   * @fn void PolygonMode(const PolyMode& mode, const T& factor,
                          const T& units) const
   * @brief Select a polygon rasterization mode and set the scale and units
   *        used to calculate depth values
   * @param[in] mode    Specifies how polygons will be rasterized
   * @param[in] factor  Specifies a scale factor that is used to create a
   *                    variable depth offset for each polygon.
   *                    DEFAULT value is 0.
   * @param[in] units   Is multiplied by an implementation-specific value to
   *                    create a constant depth offset. DEFAULT value is 0.
   */
  void PolygonMode(const PolyMode& mode,
                   const T& factor,
                   const T& units) const {
    renderer_->PolygonMode(mode, factor, units);
  }

  /**
   * @name PolygonMode
   * @fn void PolygonMode(const PolyMode& mode) const
   * @brief Select a polygon rasterization mode
   * @param[in] mode    Specifies how polygons will be rasterized
   */
  void PolygonMode(const PolyMode& mode) const {
    renderer_->PolygonMode(mode);
  }

  /**
   * @name  PolygonOffset
   * @fn    void PolygonOffset(const T& factor, const T& units) const
   * @brief Set polygon offset
   * @param factor  Specifies a scale factor that is used to create a
   *                variable depth offset for each polygon.
   *                DEFAULT value is 0.
   * @param units   Is multiplied by an implementation-specific value to
   *                create a constant depth offset. DEFAULT value is 0.
   */
  void PolygonOffset(const T& factor, const T& units) const {
    renderer_->PolygonOffset(factor, units);
  }

  /**
   * @name  ActivateContext
   * @brief Enable OpenGL context
   * @fn    void ActivateContext() const
   */
  void ActivateContext() const;

  /**
   * @name  DisableContext
   * @brief Disable OpenGL context
   * @fn    void DisableContext() const
   */
  void DisableContext() const;

  /**
   * @name  Update
   * @brief Update OpenGL vertex/normal/color and triangle buffers if needed.
   *        On first call, compute edge maps as well and push triangulation into
   *        OpenGL buffer.
   * @param[in] ctx Op's context
   * @param[in] vertex  Vertex array
   * @param[in] normal  Normal array
   * @param[in] color   Color array
   * @param[in] triangle    Triangulation
   * @param[in] shader  Rendering shader
   * @return -1 if error, 0 otherwise
   */
  int Update(tf::OpKernelContext* ctx,
             const tf::Tensor* vertex,
             const tf::Tensor* normal,
             const tf::Tensor* color,
             const tf::Tensor* triangle,
             const OGLProgram& shader);

  /**
   * @name  Update
   * @brief Update OpenGL buffers and triangle buffers based on provided info.
   *        Assume elements are triangles and do not perform edge maps.
   * @param[in] infos   Buffer infos used for initialization
   * @param[in] shader  Rendering shader
   * @return    -1 if error, 0 otherwise
   */
  int Update(const BufferInfo& infos,
             const OGLProgram& shader);

  /**
   * @name  Render
   * @brief Generate an image from a given instance of the model
   * @param[in] ctx Op context
   * @param[in] vertex      Vertex array (slice)
   * @param[in] normal      Normal array (slice)
   * @param[in] color       Color array (slice)
   * @param[in] clear       Indicate if rendering buffer needs to be cleared
   * @param[in] n_element   Number of primitive element to be rendered. If set
   *                        to numeric_limits<size_t>::max() will render all of
*                           them otherwise only the specified number
   * @param[out] image      Generated image
   */
  void Render(tensorflow::OpKernelContext* ctx,
              const tf::Tensor& vertex,
              const tf::Tensor& normal,
              const tf::Tensor& color,
              const bool& clear,
              const size_t& n_element,
              tensorflow::Tensor* image);

  /**
   * @name  Render
   * @brief Generate an image from a given instance of the model
   * @param[in] ctx Op context
   * @param[in] vertex      Vertex array (slice)
   * @param[in] normal      Normal array (slice)
   * @param[in] color       Color array (slice)
   * @param[in] color       Background array (slice)
   * @param[in] clear       Indicate if rendering buffer needs to be cleared
   * @param[in] n_element   Number of primitive element to be rendered. If set
   *                        to numeric_limits<size_t>::max() will render all of
*                           them otherwise only the specified number
   * @param[out] image      Generated image
   */
  void Render(tensorflow::OpKernelContext* ctx,
              const tf::Tensor& vertex,
              const tf::Tensor& normal,
              const tf::Tensor& color,
              const tf::Tensor& background,
              const bool& clear,
              const size_t& n_element,
              tensorflow::Tensor* image);

  /**
   * @name  Render
   * @brief Render an image with the currently binded VBOs.
   * @param[in] ctx Op context
   * @param[in] n_element   Number of element (i.e. triangle) to render. If set
 *                        to -1, render them all else only the specified number
   * @param[in] offset  Offset in bytes to take into the element buffer array
   * @param[in] clear   If `True` will clear the rendering buffer
   * @param[in] image   Generate image (i.e. one slice at a time)
   */
  void Render(tf::OpKernelContext* ctx,
              const size_t& n_element,
              const size_t& offset,
              const bool& clear,
              tf::Tensor* image);

  /**
   * @name  RenderEdges
   * @fn    void RenderEdges(tensorflow::OpKernelContext* ctx,
                             const tf::Tensor& vertex,
                             const tf::Tensor& normal,
                             const tf::Tensor& triangle,
                             const bool& clear,
                             tensorflow::Tensor* edges)
   * @brief Render silhouette edges
   * @param[in] ctx     Op context
   * @param[in] vertex      Vertex array (slice)
   * @param[in] normal      Normal array (slice)
   * @param[in] triangle    Triangulation array
   * @param[in] clear       Whether or not to clear the currently binded FBO
   * @param[in] n_element   Number of face to draw
   * @param[out] edges  Image with edges on top of it
   */
  void RenderEdges(tensorflow::OpKernelContext* ctx,
                   const tf::Tensor& vertex,
                   const tf::Tensor& normal,
                   const tf::Tensor& triangle,
                   const bool& clear,
                   const size_t& n_element,
                   tensorflow::Tensor* edges);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  set_program
   * @fn    void set_program(OGLProgram* program)
   * @brief Change glsl program used for rendering
   * @param[in] program GLSL rendering program
   */
  void set_program(OGLProgram* program) {
    renderer_->set_program(program);
  }

  /**
   * @name  set_clear_color
   * @brief Define background color
   * @param[in] r   Value for red channel
   * @param[in] g   Value for green channel
   * @param[in] b   Value for blue channel
   * @param[in] a   Value for alpha channel
   */
  void set_clear_color(const T& r, const T& g, const T& b, const T& a) {
    renderer_->set_clear_color(r, g, b, a);
  }

  /**
   * @name  get_renderer
   * @fn    const Renderer* get_renderer() const
   * @brief Provide render
   * @return    Renderer instance
   */
  const Renderer* get_renderer() const {
    return renderer_;
  }


#pragma mark -
#pragma mark Private
 private:
  /** OpenGL Mapper */
  using OGLMapper = Functor::MapOGLResource<Device, T, T>;
  /** Downloader type */
  using DLImage = Functor::DownloadImage<Device, T>;
  /** Downloader type */
  using DLBndImage = Functor::DownloadBoundaryImage<Device, T>;
  /** OpenGL Mapper */
  using OGLElementMapper = Functor::MapOGLResource<Device, T, unsigned int>;


  /** Vertex-per-edge map */
  tf::PersistentTensor vpe_;
  /** Face-per-edge map */
  tf::PersistentTensor fpe_;
  /** Renderer */
  Renderer* renderer_;
  /** Vertex mapper */
  OGLMapper* v_map_;
  /** Normal mapper */
  OGLMapper* n_map_;
  /** Vertex color */
  OGLMapper* vc_map_;
  /** Face mapper (i.e. Primitive) */
  OGLElementMapper* f_map_;
  /** Downloader */
  DLImage* dl_img_;
  /** Edge downloader */
  DLBndImage* dl_bnd_img_;
  /** Current vertex array size */
  tf::int64 n_vertex_;
  /** Current normal array size */
  tf::int64 n_normal_;
  /** Current color array size */
  tf::int64 n_color_;
  /** Current triangle array size */
  tf::int64 n_tri_;
  /** Current edge array size */
  tf::int64 n_edge_;
};

}  // namespace LTS5
#endif  // __LTS5_TF_MM_RENDERER__
