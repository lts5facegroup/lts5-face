/**
 *  @file   landmark_projector.hpp
 *  @brief  Project a subset of vertices into image plane. If multiple
 *          candidates are provided, it will pick the most tangent to the
 *          surface
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   8/14/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_LANDMARK_PROJECTOR__
#define __LTS5_LANDMARK_PROJECTOR__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   LandmarkGenerator
 * @brief   Project 3D points to image plane using pinhole camera model
 * @author  Christophe Ecabert
 * @date    16/08/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct LandmarkGenerator {
  /**
   * @name  operator()
   * @brief Project landmarks to image plane
   * @param[in,out] ctx     Op's context
   * @param[in] vertex      Surface's vertices
   * @param[in] vertex      Surface's normal
   * @param[in] indices     Vertex indices to project
   * @param[in] focal       Focal length
   * @param[in] height      Image height
   * @param[in] width       Image width
   * @param[in] top_left    If `True` convert to top left coordinate system
   * @param[out] landmark   Surface's normals computed
   * @param[out] picked     Vertex index selected for projection
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* vertex,
                 const tf::Tensor* normal,
                 const tf::Tensor* indices,
                 const T& focal,
                 const T& height,
                 const T& width,
                 const bool& top_left,
                 tf::Tensor* landmark,
                 tf::Tensor* picked);
};

/**
 * @class   LandmarkGeneratorV2
 * @brief   Project 3D points to image plane using pinhole camera model
 * @author  Christophe Ecabert
 * @date    16/08/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct LandmarkGeneratorV2 {
  /**
   * @name  operator()
   * @brief Project landmarks to image plane
   * @param[in,out] ctx     Op's context
   * @param[in] vertex      Surface's vertices
   * @param[in] vertex      Surface's normal
   * @param[in] indices     Vertex indices to project
   * @param[in] rvec        Rotation vector
   * @param[in] focal       Focal length
   * @param[in] height      Image height
   * @param[in] width       Image width
   * @param[in] thresh      Head rotation threshold for frontal detection
   * @param[in] top_left    If `True` convert to top left coordinate system
   * @param[out] landmark   Surface's normals computed
   * @param[out] picked     Vertex index selected for projection
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* vertex,
                 const tf::Tensor* normal,
                 const tf::Tensor* indices,
                 const tf::Tensor* rvec,
                 const T& focal,
                 const T& height,
                 const T& width,
                 const T& thresh,
                 const bool& top_left,
                 tf::Tensor* landmark,
                 tf::Tensor* picked);
};


}  // namespace Functor
}  // namespace LTS5

template<typename Device, typename T>
class DynamicLandmarkKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
 * @name  DynamicLandmarkKernel
 * @fn    explicit DynamicLandmarkKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] ctx Op kernel context
 */
  explicit DynamicLandmarkKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~DynamicLandmarkKernel
   * @fn    ~DynamicLandmarkKernel() override
   * @brief Destructor
   */
  ~DynamicLandmarkKernel() override = default;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(DynamicLandmarkKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** Focal length */
  T focal_;
  /** Image width */
  T width_;
  /** Image height */
  T height_;
  /** Top left flag */
  bool top_left_;
};

template<typename Device, typename T>
class DynamicLandmarkKernelV2 : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
 * @name  DynamicLandmarkKernelV2
 * @fn    explicit DynamicLandmarkKernelV2(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] ctx Op kernel context
 */
  explicit DynamicLandmarkKernelV2(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~DynamicLandmarkKernelV2
   * @fn    ~DynamicLandmarkKernelV2() override
   * @brief Destructor
   */
  ~DynamicLandmarkKernelV2() override = default;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(DynamicLandmarkKernelV2);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** Focal length */
  T focal_;
  /** Image width */
  T width_;
  /** Image height */
  T height_;
  /** Orientation threshold in radian */
  T thresh_;
  /** Top left flag */
  bool top_left_;
};

#endif  // __LTS5_LANDMARK_PROJECTOR__
