/**
 *  @file   rasterizer_op_grad.hpp
 *  @brief  Implement custom gradient for RasterizerOp
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/27/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TENSORFLOW_OP_RASTERIZER_OP_GRAD__
#define __LTS5_TENSORFLOW_OP_RASTERIZER_OP_GRAD__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"

#include "lts5/tensorflow_op/mm_parameter.hpp"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   RasterizerGradientHelper
 * @brief   Rasterizer gradient
 * @author  Christophe Ecabert
 * @date    27/07/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct RasterizerGradientHelper {

  /** Hyper parameters */
  using Params = RasterizerParameters<T>;

  /**
   * @name operator()
   * @brief Compute rasterizer gradient
   * @param[in] ctx Ops context
   * @param[in] vertex  Surface vertex
   * @param[in] triangle  Surface triangles
   * @param[in] image  Rasterized image (0: tri idx, 1: bc1, 2: bc2, 3: Mask)
   * @param[in] g_bcoords Back-propagated gradient
   * @param[out] grad Computed gradient
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& triangle,
                 const tf::Tensor& image,
                 const tf::Tensor& g_bcoords,
                 tf::Tensor* grad);
};

/**
 * @struct   EyeToClipGradientHelper
 * @brief   EyeToCLip gradient
 * @author  Christophe Ecabert
 * @date    30/09/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct EyeToClipGradientHelper {

  /**
   * @name  operator()
   * #@brief    Compute projection derivative
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex to project, [B, N, 3]
   * @param[in] focal       Focal length to use, [B, 1]
   * @param[in] grad_in     Back-propagated gradient, [B, N, 4]
   * @param[in] near        Near plane
   * @param[in] far         Far plane
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] grad_vertex Gradient with respect to vertex
   * @param[in] grad_focal  Gradient with respect to focal
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& focal,
                 const tf::Tensor& grad_in,
                 const T& near,
                 const T& far,
                 const T& width,
                 const T& height,
                 tf::Tensor* grad_vertex,
                 tf::Tensor* grad_focal);
};

/**
 * @struct   ClipToScreenGradientHelper
 * @brief   Clip to screen gradient
 * @author  Christophe Ecabert
 * @date    01/10/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct ClipToScreenGradientHelper {

  /**
   * @name  operator()
   * #@brief    Compute projection derivative
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex to project, [B, N, 4]
   * @param[in] grad_in     Back-propagated gradient, [B, N, 2]
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] top_left    True if origin is located at top left corner,
   *    otherwise it is bottom left
   * @param[in] grad_vertex Gradient with respect to vertex
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& grad_in,
                 const T& width,
                 const T& height,
                 const bool& top_left,
                 tf::Tensor* grad_vertex);
};

}  // namespace Functor
}  // namespace LTS5

/**
 * @class   EyeToClipGradKernel
 * @brief   Kernel implementing gradient of the conversion from `eye` space
 *          (i.e. camera) into `clip` space like in OpenGL pipeline
 * @author  Christophe Ecabert
 * @date    30/09/2020
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class EyeToClipGradKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  EyeToClipGradKernel
   * @fn    explicit EyeToClipGradKernel(tf::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit EyeToClipGradKernel(tf::OpKernelConstruction *ctx);

  /**
   * @name  ~EyeToClipGradKernel
   * @fn    ~EyeToClipGradKernel() override
   * @brief Destructor
   */
  ~EyeToClipGradKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(EyeToClipGradKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tf::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tf::OpKernelContext *ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** Near plane */
  T near_;
  /** Far plane */
  T far_;
  /** Image width */
  int width_;
  /** Image height */
  int height_;
};

/**
 * @class   ClipToScreenGradKernel
 * @brief   Kernel implementing gradient of the conversion from `clip` space
 *          into `screen` space like in OpenGL pipeline
 * @author  Christophe Ecabert
 * @date    01/10/2020
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class ClipToScreenGradKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  ClipToScreenGradKernel
   * @fn    explicit ClipToScreenGradKernel(tf::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit ClipToScreenGradKernel(tf::OpKernelConstruction *ctx);

  /**
   * @name  ~ClipToScreenGradKernel
   * @fn    ~ClipToScreenGradKernel() override
   * @brief Destructor
   */
  ~ClipToScreenGradKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(ClipToScreenGradKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tf::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tf::OpKernelContext *ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** Image width */
  int width_;
  /** Image height */
  int height_;
  /** Origin indicator, Top left or bottom left */
  bool top_left_;
};


/**
 * @class   RasterizerGradKernel
 * @brief   Kernel implementing rasterization gradient
 * @author  Christophe Ecabert
 * @date    27/07/2020
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class RasterizerGradKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  RasterizerGradKernel
   * @fn    explicit RasterizerGradKernel(tf::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit RasterizerGradKernel(tf::OpKernelConstruction *ctx);

  /**
   * @name  ~RasterizerGradKernel
   * @fn    ~RasterizerGradKernel() override
   * @brief Destructor
   */
  ~RasterizerGradKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(RasterizerGradKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tf::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tf::OpKernelContext *ctx) override;

 private:
  /** Mutex */
  tf::mutex mutex_;
};

#endif  // __LTS5_TENSORFLOW_OP_RASTERIZER_OP_GRAD__
