/**
 *  @file   detection_output.hpp
 *  @brief  Generate the detection output based on location and confidence
 *          predictions by doing non maximum suppression.
 *          Intended for use with MultiBox detection method.
 *          NOTE: does not implement Backwards operation.
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   8/14/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_DETECTION_OUTPUT__
#define __LTS5_DETECTION_OUTPUT__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/tensorflow_op/utils/bbox.hpp"

namespace tf = tensorflow;

/**
 * @class   DetectionOutputKernel
 * @brief   Generate the detection output based on location and confidence
 *          predictions by doing non maximum suppression.
 * @author  Christophe Ecabert
 * @date    14/08/19
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class DetectionOutputKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  DetectionOutputKernel
   * @fn    explicit DetectionOutputKernel(tf::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit DetectionOutputKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~DetectionOutputKernel
   * @fn    ~DetectionOutputKernel() override
   * @brief Destructor
   */
  ~DetectionOutputKernel() override = default;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(DetectionOutputKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;

  /** Number of classes to be predicted */
  int num_classes_;
  /** If true, bounding box are shared among different classes. */
  bool share_location_;

  int num_loc_classes_;
  /** Label for background */
  int background_label_id_;
  /** Type of encoding/decoding */
  LTS5::PriorBoxType code_type_;
  /** Indicator for variance encoding */
  bool variance_encoded_in_target_;
  /** Maximum number of box per image */
  int keep_top_k_;
  /** Box confidence threshold */
  T confidence_threshold_;


  /** Number of priors */
  int num_priors_;

  /** Threshold for non-maximum suppression */
  T nms_threshold_;
  /** Maximum number of box to keep in NMS */
  int nms_top_k_;
  /** Eta */
  T nms_eta_;
};



#endif  // __LTS5_DETECTION_OUTPUT__
