/**
 *  @file   landmark_projector_grad.hpp
 *  @brief  Landmarks projection gradient
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   8/14/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_LANDMARK_PROJECTOR_GRAD__
#define __LTS5_LANDMARK_PROJECTOR_GRAD__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   LandmarkGradientHelper
 * @brief   Landmark generator gradient
 * @author  Christophe Ecabert
 * @date    16/08/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct LandmarkGradientHelper {
  /**
   * @name  operator()
   * @brief Project landmarks to image plane
   * @param[in,out] ctx     Op's context
   * @param[in] g_landmark  Back-propagated gradient
   * @param[in] vertex      Surface's vertex
   * @param[in] picked      Vertex indices to selected for projection
   * @param[in] focal       Focal length
   * @param[in] top_left    If `True` convert to top left coordinate system
   * @param[out] grad       Op's gradient
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_landmark,
                 const tf::Tensor* vertex,
                 const tf::Tensor* picked,
                 const T& focal,
                 const bool& top_left,
                 tf::Tensor* grad);
};


}  // namespace Functor
}  // namespace LTS5

template<typename Device, typename T>
class DynamicLandmarkGradKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
 * @name  DynamicLandmarkGradKernel
 * @fn    explicit DynamicLandmarkGradKernel(tf::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] ctx Op kernel context
 */
  explicit DynamicLandmarkGradKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~DynamicLandmarkGradKernel
   * @fn    ~DynamicLandmarkGradKernel() override
   * @brief Destructor
   */
  ~DynamicLandmarkGradKernel() override = default;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(DynamicLandmarkGradKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;

  /** Focal length */
  T focal_;
  /** Mid point - x */
  T cx_;
  /** Mid point - y */
  T cy_;
  /** Image width */
  T width_;
  /** Image height */
  T height_;
  /** Top left flag */
  bool top_left_;
};

#endif  // __LTS5_LANDMARK_PROJECTOR_GRAD__
