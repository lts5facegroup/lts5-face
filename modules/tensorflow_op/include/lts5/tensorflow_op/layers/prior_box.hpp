/**
 *  @file   prior_box.hpp
 *  @brief  Tensorflow implementation of `PriorBox` layer from caffe SSD.
 *  @ingroup    tensorflow_op
 *  @see    https://github.com/weiliu89/caffe/blob/ssd/include/caffe/layers/prior_box_layer.hpp
 *
 *  @author Christophe Ecabert
 *  @date   8/12/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_PRIOR_BOX__
#define __LTS5_TF_PRIOR_BOX__

#include <vector>

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

namespace tf = tensorflow;

/**
 * @class   PriorBoxKernel
 * @brief   Tensorflow implementation of `PriorBox` layer from caffe SSD.
 * @date    12/08/19
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
class PriorBoxKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  PriorBoxKernel
   * @fn    explicit PriorBoxKernel(tensorflow::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit PriorBoxKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~PriorBoxKernel
   * @fn    ~PriorBoxKernel() override
   * @brief Destructor
   */
  ~PriorBoxKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(PriorBoxKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** Thread pool */
  tf::thread::ThreadPool* pool_;
  /** Minimum box size */
  std::vector<T> min_sizes_;
  /** Maximum box size */
  std::vector<T> max_sizes_;
  /** Box aspect ratio */
  std::vector<T> aspect_ratios_;
  /**  If true, will flip each aspect ratio. */
  bool flip_;
  /** Total number of priors */
  int num_priors_;
  /** If true, will clip the prior so that it is within [0, 1] */
  bool clip_;
  /** Variance for adjusting the prior bboxes */
  std::vector<T> variance_;
  /** Image width */
  int img_w_;
  /** Image height */
  int img_h_;
  /** Feature width */
  int feat_w_;
  /** Feature height */
  int feat_h_;
  /** Step */
  T step_;
  /** Offset to the top left corner of each cell. */
  T offset_;
};

#endif  // __LTS5_TF_PRIOR_BOX__
