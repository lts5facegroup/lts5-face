/**
 *  @file   lts5/tensorflow_op/layers/rasterizer_op.hpp
 *  @brief  Tensorflow interface for rasterizer op
 *  @ingroup  tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/14/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_TENSORFLOW_RASTERIZER_OP__
#define __LTS5_TENSORFLOW_RASTERIZER_OP__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"

#include "lts5/tensorflow_op/mm_parameter.hpp"
#include "lts5/tensorflow_op/rasterizer.hpp"
#include "lts5/tensorflow_op/rasterizer_v2.hpp"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   EyeToClipHelper
 * @brief   Compute perspective projection similar to OpenGL pipeline.
 * @author  Christophe Ecabert
 * @date    30/09/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct EyeToClipHelper {

  /**
   * @name  operator()
   * #@brief    Project vertex from `eye` space into `clip` space
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex to project, [B, N, 3]
   * @param[in] focal       Focal length to use, [B, 1]
   * @param[in] near        Near plane
   * @param[in] far         Far plane
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] v_clip      Projected vertex in clip space
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& focal,
                 const T& near,
                 const T& far,
                 const T& width,
                 const T& height,
                 tf::Tensor* v_clip);
};

/**
 * @class   ClipToScreenHelper
 * @brief   Transform vertex in `clip` space into `screen` space
 * @author  Christophe Ecabert
 * @date    01/10/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct ClipToScreenHelper {

  /**
   * @name  operator()
   * @brief Transform vertex in `clip` into `screen` space.
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex in clip space
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] top_left    True if origin is located at top left corner,
   *    otherwise it is bottom left
   * @param[out] v_screen   Vertex in screen space
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const T& width,
                 const T& height,
                 const bool& top_left,
                 tf::Tensor* v_screen);
};
}  // namespace Functor
}  // namespace LTS5


/**
 * @class   EyeToClipKernel
 * @brief   Kernel implementing conversion from `eye` space (i.e. camera) into
 *          `clip` space like in OpenGL pipeline
 * @author  Christophe Ecabert
 * @date    30/09/2020
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class EyeToClipKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  EyeToClipKernel
   * @fn    explicit EyeToClipKernel(tensorflow::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit EyeToClipKernel(tf::OpKernelConstruction *ctx);

  /**
   * @name  ~EyeToClipKernel
   * @fn    ~EyeToClipKernel() override
   * @brief Destructor
   */
  ~EyeToClipKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(EyeToClipKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tf::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tf::OpKernelContext *ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** Near plane */
  T near_;
  /** Far plane */
  T far_;
  /** Image width */
  int width_;
  /** Image height */
  int height_;
};

/**
 * @class   ClipToScreenKernel
 * @brief   Kernel implementing conversion from `clip` space into `screen`
 *  space like in OpenGL pipeline
 * @author  Christophe Ecabert
 * @date    01/10/2020
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class ClipToScreenKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  ClipToScreenKernel
   * @fn    explicit ClipToScreenKernel(tf::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit ClipToScreenKernel(tf::OpKernelConstruction *ctx);

  /**
   * @name  ~ClipToScreenKernel
   * @fn    ~ClipToScreenKernel() override
   * @brief Destructor
   */
  ~ClipToScreenKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(ClipToScreenKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tf::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tf::OpKernelContext *ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** Image width */
  int width_;
  /** Image height */
  int height_;
  /** Origin indicator, Top left or bottom left */
  bool top_left_;
};

/**
 * @class   RasterizerKernel
 * @brief   Kernel implementing rasterization based on OpenGL
 * @author  Christophe Ecabert
 * @date    14/07/2020
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class RasterizerKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  RasterizerKernel
   * @fn    explicit RasterizerKernel(tensorflow::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit RasterizerKernel(tf::OpKernelConstruction *ctx);

  /**
   * @name  ~RasterizerKernel
   * @fn    ~RasterizerKernel() override
   * @brief Destructor
   */
  ~RasterizerKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(RasterizerKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext *ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Parameters */
  using Params = LTS5::RasterizerParameters<T>;
  /** Rasterizer */
  using Rasterizer = LTS5::Rasterizer<Device, T>;

  /** Mutex */
  tf::mutex mutex_;
  /** Rasterization parameters */
  Params params_;
  /** Rasterizer */
  Rasterizer rasterizer_;
};

/**
 * @class   RasterizerV2Kernel
 * @brief   Kernel implementing rasterization based on OpenGL
 * @author  Christophe Ecabert
 * @date    12/11/2020
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class RasterizerV2Kernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  RasterizerV2Kernel
   * @fn    explicit RasterizerV2Kernel(tf::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit RasterizerV2Kernel(tf::OpKernelConstruction *ctx);

  /**
   * @name  ~RasterizerV2Kernel
   * @fn    ~RasterizerV2Kernel() override
   * @brief Destructor
   */
  ~RasterizerV2Kernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(RasterizerV2Kernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tf::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tf::OpKernelContext *ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Rasterizer */
  using Rasterizer = LTS5::RasterizerV2<Device, T>;

  /** Mutex */
  tf::mutex mutex_;
  /** Rasterizer */
  Rasterizer rasterizer_;
  /** Image width */
  int width_;
  /** Image height */
  int height_;
  /** OpenGL is initialized */
  bool gl_init_;
  /** Fixed triangles */
  bool fixed_tri_;
};
#endif  // __LTS5_TENSORFLOW_RASTERIZER_OP__
