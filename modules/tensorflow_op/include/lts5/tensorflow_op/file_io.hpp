/**
 *  @file   "lts5/tensorflow_op/file_io.hpp"
 *  @brief  Load data into tensorflow framework
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   03/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_FILE_IO__
#define __LTS5_TF_FILE_IO__

#include <iostream>

#define EIGEN_USE_THREADS

#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_types.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


/**
 * @class   IO
 * @brief   Data loader
 * @author  Christophe Ecabert
 * @date    03.01.18
 * @ingroup tensorflow_op
 * @tparam T    Data type
 */
template<typename T>
class IO {
 public:

  /** Tensor type */
  using EMatrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic,
          Eigen::RowMajor>;
  /** Matrix of int */
  using EIntMatrix = Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic,
          Eigen::RowMajor>;

  /**
   * @name  ReadTensor
   * @fn    static int ReadTensor(std::istream& stream, EMatrix* tensor)
   * @brief Load data into a given tensor
   * @param[in] stream  binary stream from which to load the data
   * @param[out] tensor Loaded tensor
   * @return  -1 if error, 0 otherwise
   */
  static int ReadTensor(std::istream& stream, EMatrix* tensor);

};

}  // namepsace LTS5
#endif //__LTS5_TF_FILE_IO__
