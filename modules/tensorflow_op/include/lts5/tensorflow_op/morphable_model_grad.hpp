/**
 *  @file   morphable_model_grad.hpp
 *  @brief  Morphable Model wrapper for TensorFlow (Gradient)
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/4/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_MORPHABLE_MODEL_GRAD__
#define __LTS5_TF_MORPHABLE_MODEL_GRAD__

#include <string>

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {


/**
 * @struct  TextureGradientHelper
 * @brief   Functor helping at the computation of the operation grad
 * @author  Christophe Ecabert
 * @date    04/07/19
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct TextureGradientHelper {
  /**
   * @name  operator()
   * @brief Compute derivative of the color with respect to the texture
   * parameters
   * @param[in,out] ctx Op's context
   * @param[in] w_tex   Texture's parameters
   * @param[in] g_color Back-propagated gradient
   * @param[in] grad    Newly computed gradient include appearance derivative
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* w_tex,
                 const tf::Tensor* g_color,
                 tf::Tensor* grad);
};

/**
 * @struct  ShapeGradientHelper
 * @brief   Functor helping at the computation of the operation grad
 * @author  Christophe Ecabert
 * @date    04/07/19
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct ShapeGradientHelper {
  /**
   * @name  operator()
   * @brief Compute derivate of the shape with respect to the shape parameters
   * @param[in,out] ctx Op's context
   * @param[in] w_shp       Shape's parameters
   * @param[in] g_vertex    Back-propagated gradient
   * @param[in] grad        Newly computed gradient include shape derivative
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* w_shp,
                 const tf::Tensor* g_vertex,
                 tf::Tensor* grad);
};

}  // namespace Functor
}  // namespace LTS5

/**
 * @class   MorphableModelGradKernel
 * @brief   Derivative of the `MorphableModelKernel` operation
 * @author  Christophe Ecabert
 * @date    7/2/19
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class MorphableModelGradKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  MorphableModelGradKernel
   * @fn    explicit MorphableModelGradKernel(tensorflow::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit MorphableModelGradKernel(tensorflow::OpKernelConstruction* ctx);

  /**
   * @name  ~MorphableModelGradKernel
   * @fn    ~MorphableModelGradKernel() override
   * @brief Destructor
   */
  ~MorphableModelGradKernel() override = default;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(MorphableModelGradKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tensorflow::mutex mutex_;
  /** Morphable Model's path */
  std::string model_path_;
};

#endif  // __LTS5_TF_MORPHABLE_MODEL_GRAD__
