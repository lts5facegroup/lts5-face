/**
 *  @file   lts5/tensorflow_op/rodrigues_grad.hpp
 *  @brief  Rodrigues formula gradient
 *  @see    https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   15/7/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_RODRIGUES_GRAD__
#define __LTS5_TF_RODRIGUES_GRAD__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   RodriguesGradientHelper
 * @brief   Compute rotation matrix gradient
 * @author  Christophe Ecabert
 * @date    15/07/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct RodriguesGradientHelper {
  /**
   * @name  operator()
   * @brief Compute rotation matrix using Rodrigues formula
   * @param[in] ctx             Op's context
   * @param[in] rvec            Rotation vector
   * @param[in] g_rmat          Back-propagated gradient
   * @param[in] use_identity    Indicate if only identiy matrix should be
   *                            considered (i.e. additive increment).
   * @param[out] grad           Rotation matrix gradient []
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* rvec,
                 const tf::Tensor* g_rmat,
                 tf::Tensor* grad);
};

}  // namespace Functor
}  // namespace LTS5

/**
 * @class   RodriguesGradKernel
 * @brief   Kernel implementing Rodrigues formula's gradient
 * @author  Christophe Ecabert
 * @date    15/07/19
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class RodriguesGradKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  RodriguesGradKernel
   * @fn    explicit RodriguesGradKernel(tf::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit RodriguesGradKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~RodriguesGradKernel
   * @fn    ~RodriguesGradKernel() override
   * @brief Destructor
   */
  ~RodriguesGradKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(RodriguesGradKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
};

#endif  // __LTS5_TF_RODRIGUES_GRAD__
