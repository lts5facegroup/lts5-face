/**
 *  @file   sh_lighting_grad.hpp
 *  @brief  Spherical Harmonics Lighting Gradient
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   15/7/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_SH_LIGHTING_GRAD__
#define __LTS5_TF_SH_LIGHTING_GRAD__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   SHLightingGradientHelper
 * @brief   Compute SH lighting gradient
 * @author  Christophe Ecabert
 * @date    15/07/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct SHLightingGradientHelper {
  /**
   * @name  operator()
   * @brief Compute SH lighting gradient
   * @param[in] ctx     Op's context
   * @param[in] g_color Back-propagated gradient
   * @param[in] aldebo  Reflectance without lighting
   * @param[in] normal  Surface's normals
   * @param[in] w_illu  Illumination parameters (SH weights)
   * @param[out] grad_color   Chained gradient with respect to color
   * @param[out] grad_normal  Chained gradient with respect to surface's normal
   * @param[out] grad_w       Chained gradient with respect to w_illu
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_color,
                 const tf::Tensor* aldebo,
                 const tf::Tensor* normal,
                 const tf::Tensor* w_illu,
                 tf::Tensor* grad_color,
                 tf::Tensor* grad_normal,
                 tf::Tensor* grad_w);
};

}  // namespace Functor
}  // namespace LTS5

/**
 * @class   SHLightingGradKernel
 * @brief   Kernel implementing spherical harmonics lighting
 * @author  Christophe Ecabert
 * @date    10/07/19
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class SHLightingGradKernel : public tf::OpKernel {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  SHLightingGradKernel
   * @fn    explicit SHLightingGradKernel(tf::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit SHLightingGradKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~SHLightingGradKernel
   * @fn    ~SHLightingGradKernel() override
   * @brief Destructor
   */
  ~SHLightingGradKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(SHLightingGradKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tf::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tf::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** SH Bands */
  int n_band_;
};

#endif  // __LTS5_TF_SH_LIGHTING_GRAD__
