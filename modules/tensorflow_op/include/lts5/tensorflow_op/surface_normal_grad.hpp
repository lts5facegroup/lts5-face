/**
 *  @file   surface_normal_grad.hpp
 *  @brief  Compute surface's normals gradient of a given "mesh"
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/5/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TF_SURFACE_NORMAL_GRAD__
#define __LTS5_TF_SURFACE_NORMAL_GRAD__

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   NormalGradientHelper
 * @brief   Compute surface's normals gradient
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename Device, typename T>
struct NormalGradientHelper {
  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in,out] ctx Op's context
   * @param[in] g_normal  Back propagated gradient
   * @param[in] vertex  Surface's vertices
   * @param[in] conn    Connectivity
   * @param[in] normal  Surface's normals
   * @param[in] scaling Surface's normals scaling (norm before normalization)
   * @param[out] grad   Surface's normals gradient
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_normal,
                 const tf::Tensor* vertex,
                 const tf::Tensor* conn,
                 const tf::Tensor* normal,
                 const tf::Tensor* scaling,
                 tf::Tensor* grad);
};

}  // namespace Functor
}  // namespace LTS5

/**
 * @class   SurfaceNormalGradKernel
 * @brief   Kernel implementing the computation of surface's normal gradient
 * @author  Christophe Ecabert
 * @date    05/07/19
 * @ingroup tensorflow_op
 * @tparam Device Device on which the computation is run
 * @tparam T      Data type
 */
template<typename Device, typename T>
class SurfaceNormalGradKernel : public tf::OpKernel {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  SurfaceNormalGradKernel
   * @fn    explicit SurfaceNormalGradKernel(tf::OpKernelConstruction* ctx)
   * @brief Constructor
   * @param[in] ctx Op kernel context
   */
  explicit SurfaceNormalGradKernel(tf::OpKernelConstruction* ctx);

  /**
   * @name  ~SurfaceNormalGradKernel
   * @fn    ~SurfaceNormalGradKernel() override
   * @brief Destructor
   */
  ~SurfaceNormalGradKernel() override;

  /* Disable copy and assignment */
  TF_DISALLOW_COPY_AND_ASSIGN(SurfaceNormalGradKernel);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Compute
   * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
   * @brief Do the actual computation of the Op
   * @param[in] ctx Op kernel context
   */
  void Compute(tensorflow::OpKernelContext* ctx) override;

#pragma mark -
#pragma mark Private

 private:
  /** Mutex */
  tf::mutex mutex_;
  /** Connectivity */
  tf::PersistentTensor conn_;
};

#endif  // __LTS5_TF_SURFACE_NORMAL__
