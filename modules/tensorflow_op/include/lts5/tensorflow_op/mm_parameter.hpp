/**
 *  @file   mm_parameter.hpp
 *  @brief  Parameter holder for various object used by Morphable model fitting
 *          process
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   9/14/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_MM_PARAMETER__
#define __LTS5_MM_PARAMETER__

#include <cstddef>

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


/**
 * @struct    MMRendererParameters
 * @brief     Renderer parameters
 * @author    Christophe Ecabert
 * @date      03/05/18
 * @ingroup   tensorflow_op
 * @tparam T  Data type
 */
template<typename T>
struct MMRendererParameters {
  /** Width */
  int width;
  /** Height */
  int height;
  /** Focal length */
  T focal;
  /** Near plane */
  T near;
  /** Far plane */
  T far;
  /** Visibility flag */
  bool visible;
  /** Number of face to render */
  size_t n_face;
  /** Multisampling factor */
  int multisampling;
};

/**
 * @struct    RasterizerParameters
 * @brief     Rasterization parameters
 * @author    Christophe Ecabert
 * @date      14/07/2020
 * @ingroup   tensorflow_op
 * @tparam T  Data type
 */
template<typename T>
struct RasterizerParameters {
  /** Width */
  int width;
  /** Height */
  int height;
  /** Focal length */
  T focal;
  /** Near plane */
  T near;
  /** Far plane */
  T far;
  /** Visibility flag */
  bool visible;
};

/**
 * @struct    MMHyperParameters
 * @brief     Hyperparameters for morphable model fitting
 * @author    Christophe Ecabert
 * @date      14/09/18
 * @ingroup   tensorflow_op
 * @tparam T  Data type
 */
template<typename T>
struct MMHyperParameters {
  /** Gradient scaling - identity component */
  T grad_scale_ids;
  /** Gradient scaling - expression component */
  T grad_scale_exp;
  /** Gradient scaling - texture component */
  T grad_scale_tex;
  /** Gradient scaling - position component (XY) */
  T grad_scale_pos_xy;
  /** Gradient scaling - position component (Z) */
  T grad_scale_pos_z;
  /** Gradient scaling - rotation component - Qx */
  T grad_scale_rot_qx;
  /** Gradient scaling - rotation component - Qy */
  T grad_scale_rot_qy;
  /** Gradient scaling - rotation component - Qz */
  T grad_scale_rot_qz;
  /** Gradient scaling - illumination component */
  T grad_scale_ill;
  /** Gradient scaling - color transform component */
  T grad_scale_ctr;
};

}  // namespace LTS5
#endif  // __LTS5_MM_PARAMETER__
