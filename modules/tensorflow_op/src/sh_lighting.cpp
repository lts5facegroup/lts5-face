/**
 *  @file   sh_lighting.cpp
 *  @brief  Spherical Harmonics Lighting
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/7/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "tensorflow/core/framework/shape_inference.h"

#include "lts5/tensorflow_op/sh_lighting.hpp"
#include "lts5/tensorflow_op/utils/shape.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   SHLightingHelper
 * @brief   Compute rotation matrix using rodrigues formula
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam T        Data type
 */
template<typename T>
struct SHLightingHelper<CPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute lighting approximation
   * @param[in] ctx     Op's context
   * @param[in] aldebo  Reflectance without lighting
   * @param[in] normal  Surface's normals
   * @param[in] w_illu  Illumination parameters (SH weights)
   * @param[out] color  Color with illumination applied on it
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* aldebo,
                 const tf::Tensor* normal,
                 const tf::Tensor* w_illu,
                 tf::Tensor* color) {
    return -1;
  }
};
}  // namespace Functor
}  // namespace LTS5

#pragma mark -
#pragma mark Initialization

/*
 * @name  SHLightingKernel
 * @fn    explicit SHLightingKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
SHLightingKernel<Device, T>::
SHLightingKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx),
                                                  n_band_(3) {
  //OP_REQUIRES_OK(ctx, ctx->GetAttr("n_band", &this->n_band_));
}

/*
 * @name  ~SHLightingKernel
 * @fn    ~SHLightingKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
SHLightingKernel<Device, T>::~SHLightingKernel() {}

#pragma mark -
#pragma mark Usage

/**
 * @name GetNumberParameter
 * @brief Compute the number of parameters for a given number of band
 * @param[in] n_band Number of spherical harmonics bands
 * @return  Number of parameters needed
 */
static int GetNumberParameter(const int& n_band) {
  switch (n_band - 1) {
    case 0: return 1;
    case 1: return 4;
    case 2: return 9;
    case 3: return 16;
    case 4: return 25;
    default: return -1;
  }
}

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void SHLightingKernel<Device, T>::Compute(tensorflow::OpKernelContext* ctx) {
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);

  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* reflectance = nullptr;
  const tf::Tensor* normal = nullptr;
  const tf::Tensor* w_illu = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("reflectance", &reflectance));
  OP_REQUIRES_OK(ctx, ctx->input("normal", &normal));
  OP_REQUIRES_OK(ctx, ctx->input("w_illu", &w_illu));

  // Sanity check
  // Reflectance
  LTS5::ShapeUtils::CheckRankAndType(ctx,
                                     *reflectance,
                                     2,
                                     tf::DataTypeToEnum<T>::v());
  OP_REQUIRES(ctx,
              reflectance->dim_size(1) % 3 == 0,
              tf::errors::InvalidArgument("Reflectance dims ",
                                          reflectance->dim_size(1),
                                          " % 3 != 0, must be flattened"));
  // Normal
  LTS5::ShapeUtils::CheckRankAndType(ctx,
                                     *normal,
                                     2,
                                     tf::DataTypeToEnum<T>::v());
  OP_REQUIRES(ctx,
              normal->dim_size(1) % 3 == 0,
              tf::errors::InvalidArgument("Normal dims ",
                                          normal->dim_size(1),
                                          " % 3 != 0, must be flattened"));
  // Reflectance + normal must have same dims
  OP_REQUIRES(ctx,
              reflectance->shape() == normal->shape(),
              tf::errors::InvalidArgument("reflectance and normal dimensions do not match"));
  // Illumination parameter
  int np = GetNumberParameter(this->n_band_);
  LTS5::ShapeUtils::CheckShape(ctx,
                               *w_illu,
                               2,
                               tf::DataTypeToEnum<T>::v(), {-1, 3 * np});
  // Batch size
  OP_REQUIRES(ctx,
              w_illu->dim_size(0) == reflectance->dim_size(0),
              tf::errors::InvalidArgument("Batch size does not match ",
                                          w_illu->dim_size(0),
                                          " != ",
                                          reflectance->dim_size(0)));
  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* color;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("color", reflectance->shape(), &color));
  int err = LTS5::Functor::SHLightingHelper<Device, T>()(ctx,
                                                         reflectance,
                                                         normal,
                                                         w_illu,
                                                         color);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in forward kernel"));
  }
}


#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;

REGISTER_OP("ShLightingOp")
        .Input("reflectance: T")
        .Input("normal: T")
        .Input("w_illu: T")
        .Output("color: T")
        .Attr("T: {float}")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          std::vector<ShapeHandle> r_shp;
          auto s = ctx->input("reflectance", &r_shp);
          s = ctx->set_output("color", {r_shp});
          return s;
        })
        .Doc(R"doc(Compute Spherical Harmonics Lighting
reflectance: Color without illumination, [Batch, 3N].
normal: Surface's normals, [Batch, 3N].
w_illu: Illumination parameters, [Batch, K].
color: Color with illumination approximation, [Batch, 3N].)doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("ShLightingOp")                      \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      SHLightingKernel<CPUDevice, Type>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(Type)                                                 \
  REGISTER_KERNEL_BUILDER(                                                 \
      Name("ShLightingOp")                                                 \
      .Device(tf::DEVICE_GPU)                                              \
      .TypeConstraint<Type>("T"),                                          \
      SHLightingKernel<GPUDevice, Type>);
REGISTER_GPU(float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA
