/**
 *  @file   rasterizer_v2.cpp
 *  @brief  
 *  @ingroup 
 *
 *  @author Christophe Ecabert
 *  @date   11/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/tensorflow_op/rasterizer_v2.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/ogl/command.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

static int ROUND_UP_BITS(uint32_t x, uint32_t y) {
  // Round x up so that it has at most y bits of mantissa.
  if (x < (1u << y))
    return x;
  uint32_t m = 0;
  while (x & ~m)
    m = (m << 1) | 1u;
  m >>= y;
  if (!(x & m))
    return x;
  return (x | m) + 1u;
}

#pragma mark -
#pragma mark Shader Code

// Vertex shader with input position directly from clip space (i.e. already
// multiplied by projection matrix)
static const char* v_clip_str =
        "#version 330\n"
        "#extension GL_ARB_shader_draw_parameters : enable\n"
        "layout (location = 0) in vec4 position;\n"
        "out int v_layer;\n"
        "out int v_offset;\n"
        "void main() {\n"
        "  gl_Position = position;\n"
        "  v_layer = gl_DrawIDARB;\n"
        "  v_offset = gl_BaseInstanceARB; // Sneak in TriID offset here.\n"
        "}";

// Geometry shader
static const char* g_str =
        "#version 330\n"
        "layout (triangles) in;\n"
        "layout (triangle_strip, max_vertices = 3) out;\n"
        "in int v_layer[];\n"
        "in int v_offset[];\n"
        "out vec3 bcoords;\n"
        "void main() {\n"
        "  int layer_id = v_layer[0];\n"
        "  int prim_id = gl_PrimitiveIDIn + v_offset[0];\n"
        "  for (int i = 0; i < 3; ++i) {\n"
        "    gl_Layer = layer_id;\n"
        "    gl_PrimitiveID = prim_id;\n"
        "    gl_Position = gl_in[i].gl_Position;\n"
        "    bcoords = vec3(i==0 ? 1.f : 0.f,\n"
        "                   i==1 ? 1.f : 0.f,\n"
        "                   i==2 ? 1.f : 0.f);\n"
        "    EmitVertex();\n"
        "  }\n"
        "  EndPrimitive();\n"
        "}";

// Fragment shader
static const char* f_str =
        "#version 330\n"
        "in vec3 bcoords;\n"
        "in int gl_PrimitiveID;\n"
        "layout(location = 0) out vec4 fragColor;\n"
        "void main() {\n"
        "  fragColor = vec4(float(gl_PrimitiveID + 1), bcoords.xyz);\n"
        "}";

#pragma mark -
#pragma mark OpenGL Data Transfer

/**
 * @class   OGLResources
 * @brief   Interface with OpenGL buffer to transfer data, CPU specialization
 * @author  Christophe Ecabert
 * @date    12/11/2020
 * @tparam Device   Device on which to run the computation
 * @tparam T    Data type
 */
template<typename T>
class OGLResources<CPUDevice, T> {
 public:
  /** Type of buffer */
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;

  /**
   * @name  OGLResources
   * @fn    OGLResources()
   * @brief Constructor
   */
  OGLResources() : resource_(0), type_(BufferType::kAttribute) {}

  /**
   * @name  ~OGLResources
   * @fn    ~OGLResources()
   * @brief Destructor
   */
  ~OGLResources() {}

  /**
   * @name  Register
   * @brief Register an existing OpenGL buffer
   * @param[in] buffer Buffer index to register
   * @param[in] type    Type of buffer to regiuster
   * @return    -1 if error, 0 otherwise
   */
  int Register(const unsigned int& buffer, const BufferType& type) {
    resource_ = buffer;
    type_ = type;
    return 0;
  }

  /**
   * @name  UnRegister
   * @brief Unregister the underlying resources
   */
  void UnRegister() {
    resource_ = 0;
  }

  /**
   * @name  Upload
   * @brief Upload data to the buffer
   * @param[in] ctx     Op's context
   * @param[in] data    Data to transfer
   * @return -1 if error, 0 otherwise
   */
  int Upload(tf::OpKernelContext* ctx, const tf::Tensor& data) {
    // See: OGLVertexArrayObject<T>::Map(...)
    // 1) Bind buffer with glBindBuffer
    // 2) Map to memory with `glMapBuffer`
    // 3) Transfer data
    // 4) Unmap with `glUnmapBuffer`
    // 5) Unbind
    return -1;
  }

 private:
  /** Resource */
  unsigned int resource_;
  /** Type */
  BufferType type_;
};

/**
 * @class   OGLImage
 * @brief   Interface with OpenGL texture to transfer data
 * @author  Christophe Ecabert
 * @date    12/11/2020
 * @tparam Device   Device on which to run the computation
 * @tparam T    Data type
 */
template<typename T>
class OGLImage<CPUDevice, T> {
 public:
  /**
   * @name  OGLImage
   * @fn    OGLImage()
   * @brief Constructor
   */
  OGLImage() : buffer_(0) {}

  /**
   * @name  ~OGLImage
   * @fn    ~OGLImage()
   * @brief Destructor
   */
  ~OGLImage() {}

  /**
   * @name  Register
   * @brief Register an existing OpenGL buffer
   * @param[in] buffer Buffer index to register
   * @return    -1 if error, 0 otherwise
   */
  int Register(const unsigned int& buffer) {
    return -1;
  }

  /**
   * @name  UnRegister
   * @brief Unregister the underlying resources
   */
  void UnRegister() {
    buffer_ = 0;
  }

  /**
   * @name  Upload
   * @brief Upload data to the buffer
   * @param[in] ctx     Op's context
   * @param[in] data    Tensor where to transfer the data
   * @return -1 if error, 0 otherwise
   */
  int Download(tf::OpKernelContext* ctx, tf::Tensor* data) {
    return -1;
  }

 private:
  /** Buffer */
  unsigned int buffer_;
};



#pragma mark -
#pragma mark Initialization

/*
 * @name  RasterizerV2
 * @fn    RasterizerV2()
 * @brief Constructor
 */
template<typename Device, typename T>
RasterizerV2<Device, T>::RasterizerV2() : ctx_(true),
                                          ogl_vertex_buffer_(nullptr),
                                          ogl_tri_buffer_(nullptr),
                                          ogl_img_buffer_(nullptr),
                                          cmd_(0),
                                          ogl_vertex_size_(0),
                                          ogl_tri_size_(0),
                                          height_(0),
                                          width_(0),
                                          depth_(0),
                                          n_tri_(0) {
  // Create VAO + Shaders
  //  this->InitOpenGL();
  ogl_vertex_buffer_ = new OGLResources<Device, T>();
  ogl_tri_buffer_ = new OGLResources<Device, int>();
  ogl_img_buffer_ = new OGLImage<Device, T>();
  // Disable context, it was explicitly turn on at construction.
  ctx_.DisableContext();
}

/*
 * @name  ~RasterizerV2
 * @fn    ~RasterizerV2();
 * @brief Destructor
 */
template<typename Device, typename T>
RasterizerV2<Device, T>::~RasterizerV2() {
  ctx_.ActiveContext();
  delete ogl_vertex_buffer_;
  delete ogl_tri_buffer_;
  delete ogl_img_buffer_;
}

/*
 * @name  InitOpenGL
 * @fn    int InitOpenGL()
 * @brief Initialize all opengl component
 * @return    -1 if error, 0 otherwise
 */
template<typename Device, typename T>
int RasterizerV2<Device, T>::InitOpenGL() {
  using BuffType = typename OGLVertexArrayObject<T>::BufferType;
  using ShaderType = typename OGLShader::ShaderType;
  using AttTyp = OGLProgram::AttributeType;
  using Cst = OGLCommand::Constant;

  // Enable context
  this->ActiveContext();
  // Create single VAO for vertex + tri
  int err = vao_.Create(2);
  vao_.Bind();
  vao_.BindBuffer(BufferIdx::kVertex, BuffType::kAttribute);
  err |= vao_.SetAttributePointer(BufferIdx::kVertex, 4);  // Vertex
  vao_.Unbind();
  // Create shader
  shader_.AddShader(OGLShader(v_clip_str, ShaderType::kVertex),
                    {{AttTyp::kPoint, 0, "position"}});
  shader_.AddShader(OGLShader(g_str, ShaderType::kGeometry));
  shader_.AddShader(OGLShader(f_str, ShaderType::kFragment));
  shader_.Build();
  // Enable some feature
  OGLCommand::GlEnable(Cst::kGLCullFace);
  OGLCommand::GlEnable(Cst::kGLDepthTest);
  OGLCommand::GlCullFace(Cst::kGLBack);
  OGLCommand::GlFrontFace(Cst::kGLCcw);
  // Release context
  this->ReleaseContext();
  return err;
}

#pragma mark -
#pragma mark Usage

/**
 * @name  ActiveContext
 * @fn    void ActiveContext() const
 * @brief Enable opengl context
 */
template<typename Device, typename T>
void RasterizerV2<Device, T>::ActiveContext() const {
  ctx_.ActiveContext();
}

/**
 * @name  ReleaseContext
 * @fn    void ReleaseContext() const
 * @brief Release opengl context
 */
template<typename Device, typename T>
void RasterizerV2<Device, T>::ReleaseContext() const {
  ctx_.DisableContext();
}

/*
 * @name  ResizeBuffers
 * @brief Resize internal opengl buffers to fit the given data
 * @param[in] ctx Op's context
 * @param[in] vertex  3D Tensor storing vertices in clip space, [B, N, 4]
 * @param[in] triangle  2D Tensor with shared triangulation [T, 3]
 * @param[in] width   Image width
 * @param[in] height  Image height
 * @return    -1 if error, 0 otherwise
 */
template<typename Device, typename T>
int RasterizerV2<Device, T>::ResizeBuffers(tf::OpKernelContext* ctx,
                                           const tf::Tensor& vertex,
                                           const tf::Tensor& triangle,
                                           const int& width,
                                           const int& height) {
  using VAOBuffType = typename OGLVertexArrayObject<T>::BufferType;
  using VAOIntBuffType = typename OGLVertexArrayObject<int>::BufferType;
  using FBOBuffType = typename FrameBufferObject::BufferType;
  int err = 0;
  // -------------- VERTEX RESIZE ----------
  int depth = vertex.dim_size(0);
  int vertex_sz = depth * vertex.dim_size(1) * 4;
  if (vertex_sz > ogl_vertex_size_) {
    // Increase size
    ogl_vertex_size_ = (vertex_sz > 64) ? ROUND_UP_BITS(vertex_sz, 2) : 64;
    LTS5_LOG_DEBUG("Increasing vertex buffer size to "
                           << ogl_vertex_size_ << " elements");
    vao_.Bind();
    vao_.BindBuffer(BufferIdx::kVertex, VAOBuffType::kAttribute);
    err |= vao_.AddData(ogl_vertex_size_ * sizeof(T),  // Size in bytes
                        nullptr,                         // No data
                        VAOBuffType::kAttribute);        // Vertex
    vao_.Unbind();
    // Register new cuda resource
    err |= ogl_vertex_buffer_->Register(vao_.get_ogl_buffer(BufferIdx::kVertex),
                                        VAOBuffType::kAttribute);

  }
  // -------------- TRIANGLE RESIZE ----------
  n_tri_ = triangle.dim_size(0);
  int tri_sz = n_tri_ * 3;
  if (tri_sz > ogl_tri_size_) {
    // Increase size
    ogl_tri_size_ = (tri_sz > 64) ? ROUND_UP_BITS(tri_sz, 2) : 64;
    LTS5_LOG_DEBUG("Increasing triangle buffer size to "
                           << ogl_tri_size_ << " elements");
    vao_.Bind();
    vao_.BindBuffer(BufferIdx::kTriangle, VAOBuffType::kElement);
    err |= vao_.AddData(ogl_tri_size_ * sizeof(int),  // Size in bytes
                        nullptr,                        // No data
                        VAOBuffType::kElement);         // Triangle
    vao_.Unbind();
    // Register new cuda resource
    err |= ogl_tri_buffer_->Register(vao_.get_ogl_buffer(BufferIdx::kTriangle),
                                     VAOIntBuffType::kElement);
  }
  // Done with VAO
  // -------------- FBO RESIZE ----------
  if (depth != depth_ || width != width_ || height != height_) {
    // Increase size
    width_  = width;
    height_ = height;
    depth_  = depth;
    err |= fbo_.Create(height_, width_, depth_, FBOBuffType::kRGBAf);
    // Register all GL buffers into Cuda.
    auto idx = fbo_.GetOglBuffer(FBOBuffType::kRGBAf);
    err |= ogl_img_buffer_->Register(idx);
  }
  // -------------- COMMANDS RESIZE ----------
  if (depth > cmd_.size()) {
    int new_sz = (depth > 64) ? ROUND_UP_BITS(depth, 1) : 64;
    LTS5_LOG_DEBUG("Increasing command array size to " << new_sz << " elements");
    cmd_.resize(new_sz);
  }
  return err;
}

/*
 * @name
 * @brief Render barycentric coordinates
 * @param[in] ctx
 * @param[in] vertex
 * @param[in] triangle
 * @param[out] bcoords
 * @return -1 if error, 0 otherwise
 */
template<typename Device, typename T>
int RasterizerV2<Device, T>::Render(tf::OpKernelContext* ctx,
                                    const tf::Tensor& vertex,
                                    const tf::Tensor& triangle,
                                    tf::Tensor* bcoords) {
  using Cst = OGLCommand::Constant;
  // Push vertex + triangle if any
  int err = ogl_vertex_buffer_->Upload(ctx, vertex);
  if (triangle.NumElements()) {
    LTS5_LOG_DEBUG("Upload triangles");
    err |= ogl_tri_buffer_->Upload(ctx, triangle);
  }
  // Render the meshes.
  vao_.Bind();
  fbo_.BindForWriting();
  shader_.Use();
  // Set viewport, clear color and depth/stencil buffers.
  err |= OGLCommand::GlViewport(0, 0, width_, height_);
//  err |= fbo_.Clear(height_, width_, depth_);
  err |= OGLCommand::GlClear(Cst::kGLColorBufferBit |
                             Cst::kGLDepthBufferBit);
  if (depth_ == 1) {
    // Draw
    err |= OGLCommand::GlDrawElements(Cst::kGLTriangles,
                                      n_tri_ * 3,
                                      Cst::kGLUnsignedInt,
                                      nullptr);
  } else {
    // Fill in range array to instantiate the same triangles for each output
    // layer. Triangle IDs starts at zero (i.e., one) for each layer, so they
    // correspond to the first dimension in addressing the triangle array.
    int vtxPerInstance = vertex.dim_size(1);  // vertex == [B, N, 4]
    for (int i = 0; i < depth_; ++i) {
      auto& cmd = cmd_[i];
      cmd.first_index    = 0;
      cmd.count         = n_tri_ * 3;
      cmd.base_vertex    = vtxPerInstance * i;
      cmd.base_instance  = 0;
      cmd.instance_count = 1;
    }
    // Draw
    err |= OGLCommand::GlMultiDrawElementsIndirect(Cst::kGLTriangles,
                                                   Cst::kGLUnsignedInt,
                                                   cmd_.data(),
                                                   depth_,
                                                   sizeof(OGLDrawCommand));

  }
  // Copy image back
  err |= ogl_img_buffer_->Download(ctx, bcoords);
  // Unbind
  vao_.Unbind();
  fbo_.Unbind();
  shader_.StopUsing();
  // Done
  return err;
}



#pragma mark -
#pragma mark Explicit Instantiation

/** Float - CPU */
template class RasterizerV2<CPUDevice, float>;
/** Double - CPU */
template class RasterizerV2<CPUDevice, double>;

#ifdef GOOGLE_CUDA
/** Float - GPU */
template class RasterizerV2<GPUDevice, float>;
/** Double - GPU */
template class RasterizerV2<GPUDevice, double>;
#endif

}  // namespace LTS5