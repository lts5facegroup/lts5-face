/**
 *  @file   lts5/tensorflow_op/file_io.cpp
 *  @brief  Load data into tensorflow framework
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   03/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include "opencv2/core.hpp"

#include "lts5/tensorflow_op/file_io.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


#pragma mark -
#pragma mark Loader

/*
 * @name  ReadTensor
 * @fn    static int ReadTensor(std::istream& stream, EMatrix* tensor)
 * @brief Load data into a given tensor
 * @param[in] stream  binary stream from which to load the data
 * @param[out] tensor Loaded tensor
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
int IO<T>::ReadTensor(std::istream& stream, EMatrix* tensor) {
  int err = -1;
  if (stream.good()) {
    // Read dimensions + type
    int type, cols, rows;
    stream.read(reinterpret_cast<char*>(&type), sizeof(type));
    stream.read(reinterpret_cast<char*>(&rows), sizeof(rows));
    stream.read(reinterpret_cast<char*>(&cols), sizeof(cols));
    // Check type match
    if (type == cv::DataType<T>::type) {
      // Ok, can allocate buffer + read
      tensor->resize(rows, cols);
      // Read
      stream.read(reinterpret_cast<char*>(tensor->data()),
                  sizeof(T) * rows * cols);
      err = stream.good() ? 0 : -1;
    }
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}


#pragma mark -
#pragma mark Explicit Instantiation

/** 32bits ints */
template class IO<int>;
/** Float */
template class IO<float>;
/** Double */
template class IO<double>;

}  // namepsace LTS5