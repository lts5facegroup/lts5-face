/**
 *  @file   mm_renderer.cpp
 *  @brief  Transform an instance of a Morphable Model into an image
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   15/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <vector>
#include <algorithm>

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/tensorflow_op/mm_renderer.hpp"
#include "lts5/tensorflow_op/mm_renderer_utils.hpp"
#include "lts5/tensorflow_op/tensor_utils.hpp"
#include "lts5/tensorflow_op/mm_image_processor.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/quaternion.hpp"

#ifdef GOOGLE_CUDA
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
#endif

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Renderer

template<typename Device, typename T>
struct OGLBufferTypeSelector;

template<typename T>
struct OGLBufferTypeSelector<CPUDevice, T> {
  using Type = typename OGLOffscreenRenderer<T, Vector3<T>>::ImageType;
  static constexpr Type type = Type::kRGBf;
};
template<typename T>
constexpr typename OGLBufferTypeSelector<CPUDevice, T>::Type OGLBufferTypeSelector<CPUDevice, T>::type;


template<typename T>
struct OGLBufferTypeSelector<GPUDevice, T> {
  using Type = typename OGLOffscreenRenderer<T, Vector3<T>>::ImageType;
  static constexpr Type type = Type::kRGBAf;
};
template<typename T>
constexpr typename OGLBufferTypeSelector<GPUDevice, T>::Type OGLBufferTypeSelector<GPUDevice, T>::type;


#pragma mark -
#pragma mark MMRendererV2

/*
 * @name  MMRendererV2
 * @fn    MMRendererV2()
 * @brief Constructor
 */
template<typename Device, typename T>
MMRendererV2<Device, T>::MMRendererV2() : renderer_(new Renderer()),
                                          v_map_(new OGLMapper()),
                                          n_map_(new OGLMapper()),
                                          vc_map_(new OGLMapper()),
                                          f_map_(new OGLElementMapper()),
                                          dl_img_(new DLImage()),
                                          dl_bnd_img_(new DLBndImage()),
                                          n_vertex_(0),
                                          n_normal_(0),
                                          n_color_(0),
                                          n_tri_(0),
                                          n_edge_(0) {
}

/**
 * @name  ~MMRendererV2
 * @fn    ~MMRendererV2();
 * @brief Destructor
 */
template<typename Device, typename T>
MMRendererV2<Device, T>::~MMRendererV2() {
  if (renderer_) {
    renderer_->ActiveContext();
  }
  delete v_map_;
  delete n_map_;
  delete vc_map_;
  delete f_map_;
  delete dl_img_;
  delete dl_bnd_img_;
  delete renderer_;
}

/**
 * @name  Init
 * @fn    int Init(const Parameters& params)
 * @brief Initialize renderer
 * @param[in] params  Renderer parameter
 * @return    -1 if error, 0 otherwise
 */
template<typename Device, typename T>
int MMRendererV2<Device, T>::Init(const Parameters& params) {
  using ImgType = typename Renderer::ImageType;
  // Init renderer
  int err = renderer_->Init(params.width,
                            params.height,
                            params.multisampling,
                            params.visible,
                            ImgType::kRGBAf);
  dl_img_->Init(renderer_);
  dl_bnd_img_->Init(renderer_);
  return err;
}

/*
 * @name  ActivateContext
 * @brief Enable OpenGL context
 * @fn    void ActivateContext() const
 */
template<typename Device, typename T>
void MMRendererV2<Device, T>::ActivateContext() const {
  if (renderer_) {
    renderer_->ActiveContext();
  }
}

/*
 * @name  DisableContext
 * @brief Disable OpenGL context
 * @fn    void DisableContext() const
 */
template<typename Device, typename T>
void MMRendererV2<Device, T>::DisableContext() const {
  if (renderer_) {
    renderer_->DisableContext();
  }
}

/*
 * @name  Update
 * @brief Update OpenGL vertex/normal/color and triangle buffers if needed.
 *        On first call, compute edge maps as well and push triangulation into
 *        OpenGL buffer.
 * @param[in] ctx Op's context
 * @param[in] vertex  Vertex array
 * @param[in] normal  Normal array
 * @param[in] color   Color array
 * @param[in] triangle    Triangulation
 * @param[in] shader  Rendering shader
 * @return -1 if error, 0 otherwise
 */
template<typename Device, typename T>
int MMRendererV2<Device, T>::Update(tf::OpKernelContext* ctx,
                                    const tf::Tensor* vertex,
                                    const tf::Tensor* normal,
                                    const tf::Tensor* color,
                                    const tf::Tensor* triangle,
                                    const OGLProgram& shader) {
  using Vertex = typename Renderer::Mesh::Vertex;
  using Normal = typename Renderer::Mesh::Normal;
  using Color = typename Renderer::Mesh::Color;
  using Triangle = typename Renderer::Mesh::Triangle;
  using BufferIdx = typename Renderer::BufferIdx;
  // Get dimensions
  int err = 0;
  auto n_vertex = vertex->dim_size(1) / 3;
  auto n_normal = normal->dim_size(1) / 3;
  auto n_color = color->dim_size(1) / 3;
  auto n_tri = triangle->dim_size(0);
  if ((n_vertex != n_vertex_) ||
      (n_normal != n_normal_) ||
      (n_color != n_color_) ||
      (n_tri != n_tri_)) {
    // Buffer needs to be updated
    BufferInfo infos;
    infos.dim_vertex_ = n_vertex * sizeof(Vertex);
    infos.dim_normal_ = n_normal * sizeof(Normal);
    infos.dim_color_ = n_color * sizeof(Color);
    infos.n_elements_ = n_tri * 3;
    // Special case for triangle -> VPE
    std::vector<int> elem;
    if (n_tri != n_tri_) {
      using TMapI = tf::TTypes<int>::Tensor;
      using CTMapI = tf::TTypes<int>::ConstTensor;
      using CopyFuncD2H = Functor::DenseOp<Device, int, Functor::DenseOpType::kD2HCopy>;
      using CopyFuncH2D = Functor::DenseOp<Device, int, Functor::DenseOpType::kH2DCopy>;
      using Mesh_t = typename Renderer::Mesh;
      using Fpe_t = typename Mesh_t::Fpe;
      using Vpe_t = typename Mesh_t::Vpe;
      // Download triangulation
      std::vector<Triangle> tri(n_tri);
      auto& device = ctx->eigen_device<Device>();
      {
        auto in = triangle->flat<int>();
        auto out = TMapI((int *) tri.data(), tri.size() * 3);
        CopyFuncD2H()(device, in, out);
      }
      // Build VPE
      Mesh_t m;
      std::vector<Fpe_t> fpe;
      std::vector<Vpe_t> vpe;
      // Need to known how many vertices are in the mesh in order to compute
      // properly the VPE map. Therefore set dummy vertex to `m`.
      m.set_triangle(tri);
      m.set_vertex(std::vector<Vertex>(n_vertex));
      m.ComputeEdgeProperties(&vpe, &fpe);
      // Concatenate triangles + edges
      auto* tri_ptr = reinterpret_cast<const int*>(tri.data());
      auto* edge_ptr = reinterpret_cast<const int*>(vpe.data());
      elem.insert(elem.end(), tri_ptr, tri_ptr + (n_tri * 3));
      elem.insert(elem.end(), edge_ptr, edge_ptr + (vpe.size() * 2));
      infos.data_elements_ = elem.data();
      infos.n_elements_ += vpe.size() * 2;
      n_edge_ = vpe.size();

      // Push vpe/fpe into persistent storage
      tf::Tensor* buffer;
      tf::AllocatorAttributes attr;
      attr.set_gpu_compatible(true);
      auto s = ctx->allocate_persistent(tf::DataTypeToEnum<int>::value,
                                        {n_edge_, 2},
                                        &vpe_,
                                        &buffer,
                                        attr);
      if (s.ok()) {
        // Fill data - VPE
        auto map = CTMapI(edge_ptr, n_edge_ * 2);
        CopyFuncH2D()(device, map, buffer->flat<int>());
      }
      s = ctx->allocate_persistent(tf::DataTypeToEnum<int>::value,
                                   {(tf::int64)fpe.size(), 2},
                                   &fpe_,
                                   &buffer,
                                   attr);
      if (s.ok()) {
        // Fill data - VPE
        auto map = CTMapI(reinterpret_cast<const int*>(fpe.data()),
                          fpe.size() * 2);
        CopyFuncH2D()(device, map, buffer->flat<int>());
      }
      if (!s.ok()) {
        ctx->CtxFailureWithWarning(__FILE__, __LINE__, s);
      }
    }
    // Update buffer
    renderer_->AddProgram(shader);
    err = renderer_->BindToOpenGL(infos);
    // Init mapper
    v_map_->Init(BufferIdx::kVertex, renderer_);
    n_map_->Init(BufferIdx::kNormal, renderer_);
    vc_map_->Init(BufferIdx::kVertexColor, renderer_);
    f_map_->Init(BufferIdx::kTriangle, renderer_);
    // Update values
    n_vertex_ = n_vertex;
    n_normal_ = n_normal;
    n_color_ = n_color;
    n_tri_ = n_tri;
  }
  return err;
}

/*
 * @name  Update
 * @brief Update OpenGL buffers and triangle buffers based on provided info.
 * @param[in] ctx     Op's context
 * @param[in] infos   Buffer infos used for initialization
 * @param[in] shader  Rendering shader
 * @return    -1 if error, 0 otherwise
 */
template<typename Device, typename T>
int MMRendererV2<Device, T>::Update(const BufferInfo& infos,
                                    const OGLProgram& shader) {
  using Vertex = typename Renderer::Mesh::Vertex;
  using Normal = typename Renderer::Mesh::Normal;
  using Color = typename Renderer::Mesh::Color;
  using BufferIdx = typename Renderer::BufferIdx;
  // Update buffer
  renderer_->AddProgram(shader);
  int err = renderer_->BindToOpenGL(infos);
  // Init mapper
  v_map_->Init(BufferIdx::kVertex, renderer_);
  n_map_->Init(BufferIdx::kNormal, renderer_);
  vc_map_->Init(BufferIdx::kVertexColor, renderer_);
  f_map_->Init(BufferIdx::kTriangle, renderer_);
  // Update values
  n_vertex_ = infos.dim_vertex_ / sizeof(Vertex);
  n_normal_ = infos.dim_normal_ / sizeof(Normal);
  n_color_ = infos.dim_color_ / sizeof(Color);
  n_tri_ = infos.n_elements_ / 3; // Assume only triangle
  n_edge_ = 0;
  return err;
}

/*
 * @name  Render
 * @brief Generate an image from a given instance of the model
 * @param[in] ctx Op context
 * @param[in] vertex      Vertex array (slice)
 * @param[in] normal      Normal array (slice)
 * @param[in] color       Color array (slice)
 * @param[in] clear       Indicate if rendering buffer needs to be cleared
 * @param[in] n_element   Number of primitive element to be rendered. If set
 *                        to numeric_limits<size_t>::max() will render all of
 *                        them otherwise only the specified number
 * @param[out] image      Generated image
 */
template<typename Device, typename T>
void MMRendererV2<Device, T>::Render(tf::OpKernelContext* ctx,
                                     const tf::Tensor& vertex,
                                     const tf::Tensor& normal,
                                     const tf::Tensor& color,
                                     const bool& clear,
                                     const size_t& n_element,
                                     tf::Tensor* image) {
  using PType = typename Renderer::PrimitiveType;
  // Push to opengl
  // -------------------------------------------------------------------
  int e = Functor::UpdateOglBuffer<Device, T>()(ctx,
                                                vertex,
                                                normal,
                                                color,
                                                v_map_,
                                                n_map_,
                                                vc_map_);
  // Generate instance
  // -------------------------------------------------------------------
  renderer_->need_to_clear(clear);
  auto nt = n_element == std::numeric_limits<size_t>::max() ?
            static_cast<size_t>(n_tri_) :
            n_element;
  renderer_->Render(PType::kOglTriangle, nt * 3, 0);
  // Copy back to tensor
  // -------------------------------------------------------------------
  e |= dl_img_->operator()(ctx, image);
  if (e != 0) {
    LTS5_LOG_ERROR("Can't upload/download data/image to/from OpenGL buffers");
  }
}

/*
 * @name  Render
 * @brief Generate an image from a given instance of the model
 * @param[in] ctx Op context
 * @param[in] vertex      Vertex array (slice)
 * @param[in] normal      Normal array (slice)
 * @param[in] color       Color array (slice)
 * @param[in] color       Background array (slice)
 * @param[in] clear       Indicate if rendering buffer needs to be cleared
 * @param[in] n_element   Number of primitive element to be rendered. If set
 *                        to numeric_limits<size_t>::max() will render all of
 *                        them otherwise only the specified number
 * @param[out] image      Generated image
 */
template<typename Device, typename T>
void MMRendererV2<Device, T>::Render(tensorflow::OpKernelContext* ctx,
                                     const tf::Tensor& vertex,
                                     const tf::Tensor& normal,
                                     const tf::Tensor& color,
                                     const tf::Tensor& background,
                                     const bool& clear,
                                     const size_t& n_element,
                                     tensorflow::Tensor* image) {
  using PType = typename Renderer::PrimitiveType;
  // Push to opengl
  // -------------------------------------------------------------------
  int e = Functor::UpdateOglBuffer<Device, T>()(ctx,
                                                vertex,
                                                normal,
                                                color,
                                                v_map_,
                                                n_map_,
                                                vc_map_);
  // Generate instance
  // -------------------------------------------------------------------
  renderer_->need_to_clear(clear);
  auto nt = n_element == std::numeric_limits<size_t>::max() ?
            static_cast<size_t>(n_tri_) :
            n_element;
  renderer_->Render(PType::kOglTriangle, nt * 3, 0);
  // Copy back to tensor
  // -------------------------------------------------------------------
  e |= dl_img_->operator()(ctx, background, image);
  if (e != 0) {
    LTS5_LOG_ERROR("Can't upload/download data/image to/from OpenGL buffers");
  }
}

/*
 * @name  Render
 * @brief Render an image with the currently binded VBOs.
 * @param[in] ctx Op context
 * @param[in] n_element   Number of element (i.e. triangle) to render. If set
*                        to -1, render them all else only the specified number
 * @param[in] offset  Offset in bytes to take into the element buffer array
 * @param[in] clear   If `True` will clear the rendering buffer
 * @param[in] image   Generate image (i.e. one slice at a time)
 */
template<typename Device, typename T>
void MMRendererV2<Device, T>::Render(tf::OpKernelContext* ctx,
                                     const size_t& n_element,
                                     const size_t& offset,
                                     const bool& clear,
                                     tf::Tensor* image) {
  // Generate instance
  // -------------------------------------------------------------------
  using PType = typename Renderer::PrimitiveType;
  renderer_->need_to_clear(clear);
  auto nt = n_element == std::numeric_limits<size_t>::max() ?
            static_cast<size_t>(n_tri_) :
            n_element;
  renderer_->Render(PType::kOglTriangle, nt * 3, 0);
  // Copy back to tensor
  // -------------------------------------------------------------------
  int e = dl_img_->operator()(ctx, image);
  if (e != 0) {
    LTS5_LOG_ERROR("Can't upload/download data/image to/from OpenGL buffers");
  }
}

/*
 * @name  RenderEdges
 * @fn    void RenderEdges(tensorflow::OpKernelContext* ctx,
                           const tf::Tensor& vertex,
                           const tf::Tensor& normal,
                           const tf::Tensor& triangle,
                           const bool& clear,
                           tensorflow::Tensor* edges)
 * @brief Render silhouette edges
 * @param[in] ctx     Op context
 * @param[in] vertex      Vertex array (slice)
 * @param[in] normal      Normal array (slice)
 * @param[in] triangle    Triangulation array
 * @param[in] clear   Whether or not to clear the currently binded FBO
 * @param[out] edges  Image with edges on top of it
 */
template<typename Device, typename T>
void MMRendererV2<Device, T>::RenderEdges(tf::OpKernelContext* ctx,
                                          const tf::Tensor& vertex,
                                          const tf::Tensor& normal,
                                          const tf::Tensor& triangle,
                                          const bool& clear,
                                          const size_t& n_element,
                                          tf::Tensor* edges) {
  using ImProcessor = MMImageProcessor<Device, T>;
  using PType = typename Renderer::PrimitiveType;

  // Access FPE, VPE
  tf::Tensor* vpe = vpe_.AccessTensor(ctx);
  tf::Tensor* fpe = fpe_.AccessTensor(ctx);

  // Compute sildhouette indexes
  tf::Tensor edges_idx;
  auto nt = n_element == std::numeric_limits<size_t>::max() ?
            -1 :
            static_cast<int>(n_element);
  ImProcessor::ComputeSilhouetteEdges(ctx,
                                      vertex,
                                      triangle,
                                      *fpe,
                                      *vpe,
                                      nt,
                                      &edges_idx);
  auto n_tri = triangle.dim_size(0);
  // Update edge list
  int n_edge = Functor::UpdateOglEdgeIndex<Device, T>()(ctx,
                                                        edges_idx,
                                                        n_tri * 3,
                                                        *f_map_);
  if (n_edge > 0) {
    // Render
    renderer_->need_to_clear(clear);
    renderer_->Render(PType::kOglEdge, n_edge * 2, n_tri * 3);
    // Download
    int e = dl_bnd_img_->operator()(ctx, edges);
    if (e != 0) {
      LTS5_LOG_ERROR("Can not download boundaries from the device");
    }
  }
}

#pragma mark -
#pragma mark Explicit Instantiation

/** Float - CPU */
template class MMRendererV2<CPUDevice, float>;
/** Double - CPU */
template class MMRendererV2<CPUDevice, double>;

#ifdef GOOGLE_CUDA
/** Float - GPU */
template class MMRendererV2<GPUDevice, float>;
/** Double - GPU */
template class MMRendererV2<GPUDevice, double>;
#endif


}  // namepsace LTS5
