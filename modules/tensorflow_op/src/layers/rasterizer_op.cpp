/**
 *  @file   rasterizer_op.cpp
 *  @brief  Tensorflow interface for rasterizer op
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/14/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "tensorflow/core/framework/shape_inference.h"

#include "lts5/tensorflow_op/layers/rasterizer_op.hpp"
#include "lts5/tensorflow_op/utils/shape.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Helper

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   EyeToClipHelper
 * @brief   Compute perspective projection similar to OpenGL pipeline.
 * @author  Christophe Ecabert
 * @date    30/09/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct EyeToClipHelper<CPUDevice, T> {

  /**
   * @name  operator()
   * #@brief    Project vertex from `eye` space into `clip` space
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex to project, [B, N, 3]
   * @param[in] focal       Focal length to use, [B, 1]
   * @param[in] near        Near plane
   * @param[in] far         Far plane
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] v_clip      Projected vertex in clip space
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& focal,
                 const T& near,
                 const T& far,
                 const T& width,
                 const T& height,
                 tf::Tensor* v_clip) {
    return -1;
  }
};

/**
 * @class   ClipToScreenHelper
 * @brief   Transform vertex in `clip` space into `screen` space
 * @author  Christophe Ecabert
 * @date    01/10/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct ClipToScreenHelper<CPUDevice, T> {

  /**
   * @name  operator()
   * @brief Transform vertex in `clip` into `screen` space.
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex in clip space
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] top_left    True if origin is located at top left corner,
   *    otherwise it is bottom left
   * @param[out] v_screen   Vertex in screen space
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const T& width,
                 const T& height,
                 const bool& top_left,
                 tf::Tensor* v_screen) {
    return -1;
  }
};
}  // namespace Functor
}  // namespace LTS5

#pragma mark -
#pragma mark Initialization

/*
 * @name  EyeToClipKernel
 * @fn    explicit EyeToClipKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
EyeToClipKernel<Device, T>::
EyeToClipKernel(tf::OpKernelConstruction *ctx) : OpKernel(ctx) {
  // Query Hyper parameters
  // -----------------------------------
  OP_REQUIRES_OK(ctx, ctx->GetAttr("near", &near_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("far", &far_));
  // See https://gamedev.stackexchange.com/questions/87329
  // for more info about Z-Buffer precision
  const T ratio = far_ / near_;
  OP_REQUIRES(ctx,
              ratio <= T(100000.0),
              tf::errors::InvalidArgument("Ratio far/near is to high, lead to "
                                          "imprecision in Z-Buffer"));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("width", &width_));
  OP_REQUIRES(ctx,
              (width_ > 0) &&
              (width_ < tf::kint32max),
              tf::errors::InvalidArgument("width has wrong dimensions"));
  OP_REQUIRES_OK(ctx,
                 ctx->GetAttr("height",
                              &height_));
  OP_REQUIRES(ctx,
              (height_ > 0) &&
              (height_ < tf::kint32max),
              tf::errors::InvalidArgument("height has wrong dimensions"));
}

/*
 * @name  ~EyeToClipKernel
 * @fn    ~EyeToClipKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
EyeToClipKernel<Device, T>::~EyeToClipKernel() {}

/*
 * @name  ClipToScreenKernel
 * @fn    explicit ClipToScreenKernel(tf::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
ClipToScreenKernel<Device, T>::
ClipToScreenKernel(tf::OpKernelConstruction *ctx) : OpKernel(ctx) {
  // Query attributes
  // -----------------------------------
  OP_REQUIRES_OK(ctx, ctx->GetAttr("width", &width_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("height", &height_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("top_left", &top_left_));
}

/*
 * @name  ~ClipToScreenKernel
 * @fn    ~ClipToScreenKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
ClipToScreenKernel<Device, T>::~ClipToScreenKernel() {};

/*
 * @name  RasterizerKernel
 * @fn    explicit RasterizerKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
RasterizerKernel<Device, T>::
RasterizerKernel(tf::OpKernelConstruction *ctx)  : OpKernel(ctx) {
  // Query Hyper parameters
  // -----------------------------------

  // Init params with default value since it is not used
  params_.near = T(1e-1);
  params_.far = T(1e2);
  params_.focal = T(-1.0);  // Set it negative to enable rendering from clip
  // space
  OP_REQUIRES_OK(ctx, ctx->GetAttr("width", &params_.width));
  OP_REQUIRES(ctx,
              (params_.width > 0) &&
              (params_.width < tf::kint32max),
              tf::errors::InvalidArgument("width has wrong dimensions"));
  OP_REQUIRES_OK(ctx,
                 ctx->GetAttr("height",
                              &params_.height));
  OP_REQUIRES(ctx,
              (params_.height > 0) &&
              (params_.height < tf::kint32max),
              tf::errors::InvalidArgument("height has wrong dimensions"));
  OP_REQUIRES_OK(ctx,
                 ctx->GetAttr("visible",
                              &params_.visible));
  // Create rasterizer
  int err = rasterizer_.CreateContext(params_);
  OP_REQUIRES(ctx,
          err == 0,
          tf::errors::Internal("Error while initializing rasterizer"));
}

/*
 * @name  ~RasterizerKernel
 * @fn    ~RasterizerKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
RasterizerKernel<Device, T>::~RasterizerKernel() {}

/*
 * @name  RasterizerV2Kernel
 * @fn    explicit RasterizerV2Kernel(tf::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
RasterizerV2Kernel<Device, T>::
RasterizerV2Kernel(tf::OpKernelConstruction *ctx) : OpKernel(ctx) {
  // Get attributes
  OP_REQUIRES_OK(ctx, ctx->GetAttr("width", &width_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("height", &height_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("fixed_triangle", &fixed_tri_));
  gl_init_ = false;
}

/**
 * @name  ~RasterizerKernel
 * @fn    ~RasterizerKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
RasterizerV2Kernel<Device, T>::~RasterizerV2Kernel() {}

#pragma mark -
#pragma mark Usage

#define MULTIPLE_OF_THREE(x, dim, mult)                  \
  OP_REQUIRES(ctx,                                       \
  x->dim_size(dim) % mult == 0,                          \
  tf::errors::InvalidArgument("Wrong "#x" dims: ",       \
                              x->dim_size(dim),          \
                              "%"#mult" != 0"))

/*
* @name  Compute
* @fn    void Compute(tf::OpKernelContext* ctx) override
* @brief Do the actual computation of the Op
* @param[in] ctx Op kernel context
*/
template<typename Device, typename T>
void EyeToClipKernel<Device, T>::Compute(tf::OpKernelContext *ctx) {
  using Shp = LTS5::ShapeUtils;
  using Eye2ClipCHelper = LTS5::Functor::EyeToClipHelper<Device, T>;

  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);
  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* focal = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("focal", &focal));
  Shp::CheckShape(ctx,
                  *vertex,
                  3,
                  tf::DataTypeToEnum<T>::v(),
                  {-1, -1, 3});
  Shp::CheckShape(ctx,
                  *focal,
                  2,
                  tf::DataTypeToEnum<T>::v(),
                  {-1, 1});

  // OUTPUTS
  // -------------------------------------------------------
  // Projected vertices have shape: [Batch, N, 4]
  tf::Tensor* v_clip = nullptr;
  auto n_img = vertex->dim_size(0);
  auto n_vertex = vertex->dim_size(1);
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("vertex_clip",
                                      {n_img, n_vertex, 4},
                                      &v_clip));
  // COMPUTE
  // -------------------------------------------------------
  int err = Eye2ClipCHelper()(ctx,
                              *vertex,
                              *focal,
                              near_,
                              far_,
                              width_,
                              height_,
                              v_clip);
  OP_REQUIRES(ctx,
              err == 0,
              tf::errors::Internal("Error while doing projection into"
                                   " `clip` space"));
}

/**
 * @name  Compute
 * @fn    void Compute(tf::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
void ClipToScreenKernel<Device, T>::Compute(tf::OpKernelContext *ctx) {
  using Shp = LTS5::ShapeUtils;
  using Clip2ScrHelper = LTS5::Functor::ClipToScreenHelper<Device, T>;

  // Method should be thread safe, add lock_guard to guarantee it.
  tf::mutex_lock lock(this->mutex_);

  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* vertex = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  Shp::CheckShape(ctx,
                  *vertex,
                  3,
                  tf::DataTypeToEnum<T>::v(),
                  {-1, -1, 4});

  // OUTPUTS
  // -------------------------------------------------------
  // Screen vertices have shape: [Batch, N, 2]
  tf::Tensor* v_screen = nullptr;
  auto n_img = vertex->dim_size(0);
  auto n_vertex = vertex->dim_size(1);
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("vertex_screen",
                                      {n_img, n_vertex, 2},
                                      &v_screen));

  // COMPUTE
  // -------------------------------------------------------
  int err = Clip2ScrHelper()(ctx,
                             *vertex,
                             width_,
                             height_,
                             top_left_,
                             v_screen);
  OP_REQUIRES(ctx,
              err == 0,
              tf::errors::Internal("Error while doing conversion "
                                   "from `clip` space to `screen` space"));
}

/**
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
void RasterizerKernel<Device, T>::Compute(tf::OpKernelContext *ctx) {
  using Shp = LTS5::ShapeUtils;

  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);
  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* triangle = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("triangle", &triangle));
  Shp::CheckRankAndType(ctx,
                        *vertex,
                        3,
                        tf::DataTypeToEnum<T>::v());
  Shp::CheckShape(ctx,
                  *triangle,
                  2,
                  tf::DataTypeToEnum<int>::v(),
                  {-1, 3});

  auto vertex_sz = vertex->dim_size(2);
  if (params_.focal > T(0.0)) {
    // Vertex are in camera space, therefore must be of size 3 (i.e. x, y, z)
    OP_REQUIRES(ctx,
                vertex_sz == 3,
                tf::errors::InvalidArgument("Vertex in `camera` space "
                                            "must have last dimension equal to "
                                            "3. Got ",
                                            vertex_sz,
                                            "!= 3 !"));
  } else {
    // Vertex are in clip space, therefore must be of size 4 (i.e. x, y, z, w)
    OP_REQUIRES(ctx,
                vertex_sz == 4,
                tf::errors::InvalidArgument("Focal length is negative,"
                                            " therefore it is assumed that "
                                            "vertex are in `clip` space and must"
                                            " have last dimension equal to 4. "
                                            "Got ",
                        vertex_sz,
                        "!= 4 !"));
  }


  // OUTPUTS
  // -------------------------------------------------------
  // Image has 4 channels: Tri idx, barycentric0, barycentric1, barycentric2
  tf::Tensor* image = nullptr;
  auto n_img = vertex->dim_size(0);
  auto h = static_cast<tf::int64>(params_.height);
  auto w = static_cast<tf::int64>(params_.width);
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("image", {n_img, h, w, 4}, &image));

  // COMPUTE
  // -------------------------------------------------------
  int err = rasterizer_.Render(ctx, *vertex, *triangle, image);
  OP_REQUIRES(ctx,
              err == 0,
              tf::errors::Internal("Error while doing rasterization"));
}


/*
 * @name  Compute
 * @fn    void Compute(tf::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
void RasterizerV2Kernel<Device, T>::Compute(tf::OpKernelContext *ctx) {
  using Shp = LTS5::ShapeUtils;

  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);

  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* triangle = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("triangle", &triangle));
  Shp::CheckShape(ctx,
                  *vertex,
                  3,
                  tf::DataTypeToEnum<T>::v(), {-1, -1, 4});
  Shp::CheckShape(ctx,
                  *triangle,
                  2,
                  tf::DataTypeToEnum<int>::v(),
                  {-1, 3});
  // OUTPUTS
  // -------------------------------------------------------
  // Image has 4 channels: Tri idx, barycentric0, barycentric1, barycentric2
  tf::Tensor* image = nullptr;
  auto n_img = vertex->dim_size(0);
  auto h = static_cast<tf::int64>(height_);
  auto w = static_cast<tf::int64>(width_);
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("image", {n_img, h, w, 4}, &image));

  // COMPUTE
  // -------------------------------------------------------
  bool is_init = gl_init_;
  if (!gl_init_) {
    int err = rasterizer_.InitOpenGL();
    OP_REQUIRES(ctx,
                err == 0,
                tf::errors::Internal("Error while initializing OpenGL"));
    gl_init_ = true;
  }
  rasterizer_.ActiveContext();
  // Setup buffers if needed (lazy init)
  int err = rasterizer_.ResizeBuffers(ctx, *vertex, *triangle, width_, height_);
  OP_REQUIRES(ctx,
              err == 0,
              tf::errors::Internal("Error while doing resizing buffers"));
  // Do rasterization
  tf::Tensor tri = (is_init && fixed_tri_) ? tf::Tensor() : *triangle;
  err |= rasterizer_.Render(ctx, *vertex, tri, image);
  rasterizer_.ReleaseContext();
  OP_REQUIRES(ctx,
              err == 0,
              tf::errors::Internal("Error while doing rasterization"));

}

#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;
using DimensionHandle = tf::shape_inference::DimensionHandle;

REGISTER_OP("EyeToClipOp")
        .Attr("T: {float}")
        .Attr("width: int")
        .Attr("height: int")
        .Attr("near: float = 1e0")
        .Attr("far: float = 1e2")
        .Input("vertex: T")
        .Input("focal: T")
        .Output("vertex_clip: T")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          std::vector<ShapeHandle> v_shp;
          auto s = ctx->input("vertex", &v_shp);
          auto bsize = ctx->Dim(v_shp[0], 0);
          auto n_vertex = ctx->Dim(v_shp[0], 1);
          auto v_clip_shp = ctx->MakeShape({bsize, n_vertex, 4});
          ctx->set_output(0, v_clip_shp);
          return s;
        })
        .Doc(R"doc(Project vertex from `eye` space (i.e. camera) into `clip`
 space like OpenGL pipeline does.
vertex: Geometry of the object to project, [Batch, N, 3].
focal: Focal length to use to project, [Batch, 1].
width: Width of the image.
height Height of the image.
near: Near plane.
far: Far plane.)doc");

REGISTER_OP("ClipToScreenOp")
        .Attr("T: {float}")
        .Attr("width: int")
        .Attr("height: int")
        .Attr("top_left: bool = true")
        .Input("vertex: T")
        .Output("vertex_screen: T")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          std::vector<ShapeHandle> v_shp;
          auto s = ctx->input("vertex", &v_shp);
          auto bsize = ctx->Dim(v_shp[0], 0);
          auto n_vertex = ctx->Dim(v_shp[0], 1);
          auto vc_shp = ctx->MakeShape({bsize, n_vertex, 2});
          ctx->set_output(0, vc_shp);
          return s;
        })
        .Doc(R"doc(Transfrom vertex in `clip` space into `screen` space
following the OpenGL pipeline.
vertex: Vertex in `clip` space to convert, [Batch, N, 4]
width: Image width
height: Image height)doc");

REGISTER_OP("RasterizingOp")
        .Attr("T: {float}")
        .Attr("width: int")
        .Attr("height: int")
        .Attr("visible: bool = false")
        .Input("vertex: T")
        .Input("triangle: int32")
        .Output("image: T")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          int w, h;
          std::vector<ShapeHandle> v_shp;
          auto s = ctx->input("vertex", &v_shp);
          s = ctx->GetAttr("height", &h);
          s = ctx->GetAttr("width", &w);
          auto bsize = ctx->Dim(v_shp[0], 0);
          auto im_shp = ctx->MakeShape({bsize, h, w, 4});
          ctx->set_output(0, im_shp);
          return s;
        })
        .Doc(R"doc(Rasterize a given surface (vertex + triangle) using OpenGL
 backend.
vertex: Geometry of the object to render [Batch, N, 4].
triangle: Surface triangulation [T, 3] (same for each object).
image: Rasterized image [Batch, H, W, 4]. Channels property are:
 0: Index of the triangle covering that pixel location.
 1: First barycentric coordinate at pixel location.
 2: Second barycentric coordinate at pixel location.
 3: Third barycentric coordinate at pixel location.
width: Width of the rendered image.
height Height of the rendered image.
visible: Flag to show/hide the OpenGL window used to render.)doc");

REGISTER_OP("RasterizingV2Op")
        .Attr("T: {float}")
        .Attr("width: int")
        .Attr("height: int")
        .Attr("fixed_triangle: bool = false")
        .Input("vertex: T")
        .Input("triangle: int32")
        .Output("image: T")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          int w, h;
          std::vector<ShapeHandle> v_shp;
          auto s = ctx->input("vertex", &v_shp);
          s = ctx->GetAttr("height", &h);
          s = ctx->GetAttr("width", &w);
          auto bsize = ctx->Dim(v_shp[0], 0);
          auto im_shp = ctx->MakeShape({bsize, h, w, 4});
          ctx->set_output(0, im_shp);
          return s;
        })
        .Doc(R"doc(Rasterize a given surface (vertex + triangle) using OpenGL
 backend.
vertex: Geometry of the object to render [Batch, N, 4].
triangle: Surface triangulation [T, 3] (same for each object).
image: Rasterized image [Batch, H, W, 4]. Channels property are:
 0: Index of the triangle covering that pixel location.
 1: First barycentric coordinate at pixel location.
 2: Second barycentric coordinate at pixel location.
 3: Third barycentric coordinate at pixel location.
width: Width of the rendered image.
height: Height of the rendered image.
fixed_triangle: If `True` indicates triangles are shared across all rendering
 pass, therefore can be uploaded in OpenGL buffer only once.)doc");

#define REGISTER_CPU(OpName, Class, Type)       \
REGISTER_KERNEL_BUILDER(                        \
      Name(OpName)                              \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      Class<CPUDevice, Type>)
REGISTER_CPU("RasterizingOp", RasterizerKernel, float);
REGISTER_CPU("RasterizingV2Op", RasterizerV2Kernel, float);
REGISTER_CPU("EyeToClipOp", EyeToClipKernel, float);
REGISTER_CPU("ClipToScreenOp", ClipToScreenKernel, float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(OpName, Class, Type)       \
REGISTER_KERNEL_BUILDER(                        \
      Name(OpName)                              \
      .Device(tf::DEVICE_GPU)                   \
      .TypeConstraint<Type>("T"),               \
      Class<GPUDevice, Type>)
REGISTER_GPU("RasterizingOp", RasterizerKernel, float);
REGISTER_GPU("RasterizingV2Op", RasterizerV2Kernel, float);
REGISTER_GPU("EyeToClipOp", EyeToClipKernel, float);
REGISTER_GPU("ClipToScreenOp", ClipToScreenKernel, float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA