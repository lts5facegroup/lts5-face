/**
 *  @file   landmark_projector.cpp
 *  @brief  Project a subset of vertices into image plane. If multiple
 *          candidates are provided, it will pick the most tangent to the
 *          surface
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   8/14/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>
#include <lts5/utils/math/constant.hpp>

#include "tensorflow/core/framework/shape_inference.h"

#include "lts5/tensorflow_op/layers/landmark_projector.hpp"
#include "lts5/tensorflow_op/utils/shape.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   LandmarkGenerator
 * @brief   Project 3D points to image plane using pinhole camera model
 * @author  Christophe Ecabert
 * @date    16/08/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct LandmarkGenerator<CPUDevice, T> {
  /**
   * @name  operator()
   * @brief Project landmarks to image plane
   * @param[in,out] ctx     Op's context
   * @param[in] vertex      Surface's vertices
   * @param[in] vertex      Surface's normal
   * @param[in] indices     Vertex indices to project
   * @param[in] focal       Focal length
   * @param[in] height      Image height
   * @param[in] width       Image width
   * @param[in] top_left    If `True` convert to top left coordinate system
   * @param[out] landmark   Surface's normals computed
   * @param[out] picked     Vertex index selected for projection
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* vertex,
                 const tf::Tensor* normal,
                 const tf::Tensor* indices,
                 const T& focal,
                 const T& height,
                 const T& width,
                 const bool& top_left,
                 tf::Tensor* landmark,
                 tf::Tensor* picked) {
    return -1;
  }
};

/**
 * @class   LandmarkGeneratorV2
 * @brief   Project 3D points to image plane using pinhole camera model
 * @author  Christophe Ecabert
 * @date    16/08/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct LandmarkGeneratorV2<CPUDevice, T> {
  /**
   * @name  operator()
   * @brief Project landmarks to image plane
   * @param[in,out] ctx     Op's context
   * @param[in] vertex      Surface's vertices
   * @param[in] vertex      Surface's normal
   * @param[in] indices     Vertex indices to project
   * @param[in] rvec        Rotation vector
   * @param[in] focal       Focal length
   * @param[in] height      Image height
   * @param[in] width       Image width
   * @param[in] thresh      Head rotation threshold for frontal detection
   * @param[in] top_left    If `True` convert to top left coordinate system
   * @param[out] landmark   Surface's normals computed
   * @param[out] picked     Vertex index selected for projection
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* vertex,
                 const tf::Tensor* normal,
                 const tf::Tensor* indices,
                 const tf::Tensor* rvec,
                 const T& focal,
                 const T& height,
                 const T& width,
                 const T& thresh,
                 const bool& top_left,
                 tf::Tensor* landmark,
                 tf::Tensor* picked) {
    return -1;
  }
};


}  // namespace Functor
}  // namespace LTS5


#pragma mark -
#pragma mark Landmarks

/*
 * @name  DynamicLandmarkKernel
 * @fn    explicit DynamicLandmarkKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
DynamicLandmarkKernel<Device, T>::
DynamicLandmarkKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx) {
  int value;
  OP_REQUIRES_OK(ctx, ctx->GetAttr("focal", &this->focal_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("width", &value));
  this->width_ = static_cast<T>(value);
  OP_REQUIRES_OK(ctx, ctx->GetAttr("height", &value));
  this->height_ = static_cast<T>(value);
  OP_REQUIRES_OK(ctx, ctx->GetAttr("top_left", &this->top_left_));
}

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void DynamicLandmarkKernel<Device, T>::Compute(tf::OpKernelContext* ctx) {
  using Shape = LTS5::ShapeUtils;
  using Projector = LTS5::Functor::LandmarkGenerator<Device, T>;
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);
  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* normal = nullptr;
  const tf::Tensor* indices = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("normal", &normal));
  OP_REQUIRES_OK(ctx, ctx->input("indices", &indices));

  //Sanity check
  Shape::CheckShape(ctx,
                    *vertex,
                    3,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, -1, 3});
  Shape::CheckShape(ctx,
                    *normal,
                    3,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, -1, 3});
  Shape::CheckRankAndType(ctx,
                          *indices,
                          2,
                          tf::DataTypeToEnum<int>::v());
  OP_REQUIRES(ctx,
              vertex->shape() == normal->shape(),
              tf::errors::InvalidArgument("`vertex` and `normal` must have same shape: ",
                                          vertex->shape(),
                                          " != ",
                                          normal->shape()));
  // OUTPUTS
  // -------------------------------------------------------
  auto bsize = vertex->dim_size(0);
  auto n_idx = indices->dim_size(0);
  tf::Tensor* landmarks = nullptr;
  tf::Tensor* picked = nullptr;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("landmark",
                                      {bsize, n_idx, 2},
                                      &landmarks));
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("pick",
                                      {bsize, n_idx},
                                      &picked));
  // COMPUTE
  // -------------------------------------------------------
  // Project landmarks
  int err = Projector()(ctx,
                        vertex,
                        normal,
                        indices,
                        this->focal_,
                        this->height_,
                        this->width_,
                        this->top_left_,
                        landmarks,
                        picked);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in forward kernel"));
  }
}

#pragma mark -
#pragma mark Landmarks V2

/*
 * @name  DynamicLandmarkKernelV2
 * @fn    explicit DynamicLandmarkKernelV2(tf::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
DynamicLandmarkKernelV2<Device, T>::
DynamicLandmarkKernelV2(tf::OpKernelConstruction* ctx) : OpKernel(ctx) {
  int value;
  OP_REQUIRES_OK(ctx, ctx->GetAttr("focal", &this->focal_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("width", &value));
  this->width_ = static_cast<T>(value);
  OP_REQUIRES_OK(ctx, ctx->GetAttr("height", &value));
  this->height_ = static_cast<T>(value);
  T thr;
  OP_REQUIRES_OK(ctx, ctx->GetAttr("thresh", &thr));
  this->thresh_ = thr * LTS5::Constants<T>::Deg2Rad;
  OP_REQUIRES_OK(ctx, ctx->GetAttr("top_left", &this->top_left_));
}

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void DynamicLandmarkKernelV2<Device, T>::Compute(tf::OpKernelContext* ctx) {
  using Shape = LTS5::ShapeUtils;
  using ProjectorV2 = LTS5::Functor::LandmarkGeneratorV2<Device, T>;
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);
  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* normal = nullptr;
  const tf::Tensor* indices = nullptr;
  const tf::Tensor* rvec = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("normal", &normal));
  OP_REQUIRES_OK(ctx, ctx->input("indices", &indices));
  OP_REQUIRES_OK(ctx, ctx->input("rvec", &rvec));

  //Sanity check
  Shape::CheckShape(ctx,
                    *vertex,
                    3,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, -1, 3});
  Shape::CheckShape(ctx,
                    *normal,
                    3,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, -1, 3});
  Shape::CheckRankAndType(ctx,
                          *indices,
                          2,
                          tf::DataTypeToEnum<int>::v());
  Shape::CheckShape(ctx,
                    *rvec,
                    3,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, 1, 3});
  OP_REQUIRES(ctx,
              vertex->shape() == normal->shape(),
              tf::errors::InvalidArgument("`vertex` and `normal` must have same shape: ",
                                          vertex->shape(),
                                          " != ",
                                          normal->shape()));

  OP_REQUIRES(ctx,
              vertex->dim_size(0) == rvec->dim_size(0),
              tf::errors::InvalidArgument("`vertex` and `rvec` batch size must be the same: ",
                                          vertex->dim_size(0),
                                          " != ",
                                          rvec->dim_size(0)));
  // OUTPUTS
  // -------------------------------------------------------
  auto bsize = vertex->dim_size(0);
  auto n_idx = indices->dim_size(0);
  tf::Tensor* landmarks = nullptr;
  tf::Tensor* picked = nullptr;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("landmark",
                                      {bsize, n_idx, 2},
                                      &landmarks));
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("pick",
                                      {bsize, n_idx},
                                      &picked));
  // COMPUTE
  // -------------------------------------------------------
  // Project landmarks
  int err = ProjectorV2()(ctx,
                          vertex,
                          normal,
                          indices,
                          rvec,
                          this->focal_,
                          this->height_,
                          this->width_,
                          this->thresh_,
                          this->top_left_,
                          landmarks,
                          picked);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in forward kernel"));
  }
}


#pragma mark -
#pragma mark Registration With Tensorflow
using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;
using DimensionHandle = tf::shape_inference::DimensionHandle;

REGISTER_OP("DynamicLandmarkOp")
        .Input("vertex: T")
        .Input("normal: T")
        .Input("indices: int32")
        .Output("landmark: T")
        .Output("pick: int32")
        .Attr("T: {float}")
        .Attr("focal: float = 525.0")
        .Attr("width: int")
        .Attr("height: int")
        .Attr("top_left: bool = true")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          // Get index size
          std::vector<ShapeHandle> idx_shp;
          auto s = ctx->input("indices", &idx_shp);
          auto n_idx = ctx->Dim(idx_shp[0], 0);
          // Get batch size
          std::vector<ShapeHandle> v_shp;
          s = ctx->input("vertex", &v_shp);
          auto bsize = ctx->Dim(v_shp[0], 0);
          auto shp = ctx->MakeShape({bsize,
                                     n_idx,
                                     2});
          s = ctx->set_output("landmark", {shp});
          auto pick_shp = ctx->MakeShape({bsize, n_idx});
          s = ctx->set_output("pick", {pick_shp});
          return s;
        })
        .Doc(R"doc(
Project 3D vertices into 2D image space

For each landmark to project several vertex candidate can be provided. If so
the vertex which is the most perpendicular to the optical axis will be selected.

vertex: 3D Point cloud [Batch, -1, 3]
normal: Surface's normals, must have the same dimensions as `vertex`
  [Batch, -1, 3]
landmark: Projected landmarks, [Batch, K, 2]
pick: Selected vertices for projection, [Batch, K]
indices: Index of the landmarks to be projected. Each row represents candidates
  for the ith landmarks. First element must be number of candidate.
  [N, ....]
focal: Camera focal length
width: Image's width
height: Image's height
top_left: If `True` convert to top left coordinate system. If `False` the origin
  is located at the bottom left (i.e. Right hand system)
)doc");

REGISTER_OP("DynamicLandmarkOpV2")
        .Input("vertex: T")
        .Input("normal: T")
        .Input("indices: int32")
        .Input("rvec: T")
        .Output("landmark: T")
        .Output("pick: int32")
        .Attr("T: {float}")
        .Attr("focal: float = 525.0")
        .Attr("width: int")
        .Attr("height: int")
        .Attr("thresh: float = 5.0")
        .Attr("top_left: bool = true")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          // Get index size
          std::vector<ShapeHandle> idx_shp;
          auto s = ctx->input("indices", &idx_shp);
          auto n_idx = ctx->Dim(idx_shp[0], 0);
          // Get batch size
          std::vector<ShapeHandle> v_shp;
          s = ctx->input("vertex", &v_shp);
          auto bsize = ctx->Dim(v_shp[0], 0);
          auto shp = ctx->MakeShape({bsize,
                                     n_idx,
                                     2});
          s = ctx->set_output("landmark", {shp});
          auto pick_shp = ctx->MakeShape({bsize, n_idx});
          s = ctx->set_output("pick", {pick_shp});
          return s;
        })
        .Doc(R"doc(
Project 3D vertices into 2D image space

For each landmark to project several vertex candidate can be provided. If so
the vertex which is the most perpendicular to the optical axis will be selected.

vertex: 3D Point cloud [Batch, -1, 3]
normal: Surface's normals, must have the same dimensions as `vertex`
  [Batch, -1, 3]
landmark: Projected landmarks, [Batch, K, 2]
pick: Selected vertices for projection, [Batch, K]
indices: Index of the landmarks to be projected. Each row represents candidates
  for the ith landmarks. First element must be number of candidate.
  [N, ....]
rvec: Rotation applied to `vertex`. This is used to define wheter the object is
  in frontal position or not, [B, 1, 3]
focal: Camera focal length
width: Image's width
height: Image's height
thresh: Head orientation threshold to consider frontal pose. Angle is given in
  degrees
top_left: If `True` convert to top left coordinate system. If `False` the origin
  is located at the bottom left (i.e. Right hand system)
)doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("DynamicLandmarkOp")                 \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      DynamicLandmarkKernel<CPUDevice, Type>)
#define REGISTER_CPU_V2(Type)                   \
REGISTER_KERNEL_BUILDER(                        \
      Name("DynamicLandmarkOpV2")               \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      DynamicLandmarkKernelV2<CPUDevice, Type>)
REGISTER_CPU(float);
REGISTER_CPU_V2(float);
// Remove definition
#undef REGISTER_CPU
#undef REGISTER_CPU_V2

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(Type)                       \
  REGISTER_KERNEL_BUILDER(                       \
      Name("DynamicLandmarkOp")                  \
      .Device(tf::DEVICE_GPU)                    \
      .TypeConstraint<Type>("T"),                \
      DynamicLandmarkKernel<GPUDevice, Type>);
#define REGISTER_GPU_V2(Type)                    \
  REGISTER_KERNEL_BUILDER(                       \
      Name("DynamicLandmarkOpV2")                \
      .Device(tf::DEVICE_GPU)                    \
      .TypeConstraint<Type>("T"),                \
      DynamicLandmarkKernelV2<GPUDevice, Type>);
REGISTER_GPU(float);
REGISTER_GPU_V2(float);

// Remove definition
#undef REGISTER_GPU
#undef REGISTER_GPU_V2

#endif  // GOOGLE_CUDA