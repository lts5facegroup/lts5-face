/**
 *  @file   landmark_projector_grad.cpp
 *  @brief  Landmark projection gradient
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   8/14/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "tensorflow/core/framework/shape_inference.h"

#include "lts5/tensorflow_op/layers/landmark_projector_grad.hpp"
#include "lts5/tensorflow_op/utils/shape.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   LandmarkGradientHelper
 * @brief   Projection gradient helper
 * @author  Christophe Ecabert
 * @date    16/08/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct LandmarkGradientHelper<CPUDevice, T> {
  /**
   * @name  operator()
   * @brief Project landmarks to image plane
   * @param[in,out] ctx     Op's context
   * @param[in] g_landmark  Back-propagated gradient
   * @param[in] vertex      Surface's vertex
   * @param[in] picked      Vertex indices to selected for projection
   * @param[in] focal       Focal length
   * @param[in] height      Image height
   * @param[in] width       Image width
   * @param[in] top_left    If `True` convert to top left coordinate system
   * @param[out] grad       Op's gradient
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_landmark,
                 const tf::Tensor* vertex,
                 const tf::Tensor* picked,
                 const T& focal,
                 const bool& top_left,
                 tf::Tensor* grad) {
    return -1;
  }
};


}  // namespace Functor
}  // namespace LTS5


#pragma mark -
#pragma mark Initialization

/*
 * @name  DynamicLandmarkGradKernel
 * @fn    explicit DynamicLandmarkGradKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
DynamicLandmarkGradKernel<Device, T>::
DynamicLandmarkGradKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx) {
  int value;
  OP_REQUIRES_OK(ctx, ctx->GetAttr("focal", &this->focal_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("width", &value));
  this->width_ = static_cast<T>(value);
  OP_REQUIRES_OK(ctx, ctx->GetAttr("height", &value));
  this->height_ = static_cast<T>(value);
  OP_REQUIRES_OK(ctx, ctx->GetAttr("top_left", &this->top_left_));
  // Compute midpoint
  this->cx_ = this->width_ / T(2.0);
  this->cy_ = this->height_ / T(2.0);
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void DynamicLandmarkGradKernel<Device, T>::Compute(tf::OpKernelContext* ctx) {
  using Shape = LTS5::ShapeUtils;
  using ProjectorGrad = LTS5::Functor::LandmarkGradientHelper<Device, T>;
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);
  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* g_landmark = nullptr;
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* normal = nullptr;
  const tf::Tensor* indices = nullptr;
  const tf::Tensor* landmark = nullptr;
  const tf::Tensor* pick = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("g_landmark", &g_landmark));
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("normal", &normal));
  OP_REQUIRES_OK(ctx, ctx->input("indices", &indices));
  OP_REQUIRES_OK(ctx, ctx->input("landmark", &landmark));
  OP_REQUIRES_OK(ctx, ctx->input("pick", &pick));

  //Sanity check
  Shape::CheckShape(ctx,
                    *vertex,
                    3,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, -1, 3});
  Shape::CheckShape(ctx,
                    *normal,
                    3,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, -1, 3});
  Shape::CheckRankAndType(ctx,
                          *indices,
                          2,
                          tf::DataTypeToEnum<int>::v());
  OP_REQUIRES(ctx,
              vertex->shape() == normal->shape(),
              tf::errors::InvalidArgument("`vertex` and `normal` must have same shape: ",
                                          vertex->shape(),
                                          " != ",
                                          normal->shape()));


  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* grad_landmark = nullptr;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("grad_landmark",
                                      vertex->shape(),
                                      &grad_landmark));


  // COMPUTE
  // -------------------------------------------------------
  // Project landmarks
  int err = ProjectorGrad()(ctx,
          g_landmark,
          vertex,
          pick,
          this->focal_,
          this->top_left_,
          grad_landmark);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in backward kernel"));
  }
}


#pragma mark -
#pragma mark Registration With Tensorflow
using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;
using DimensionHandle = tf::shape_inference::DimensionHandle;

REGISTER_OP("DynamicLandmarkOpGrad")
        .Input("g_landmark: T")
        .Input("vertex: T")
        .Input("normal: T")
        .Input("indices: int32")
        .Input("landmark: T")
        .Input("pick: int32")
        .Output("grad_landmark: T")
        .Attr("T: {float}")
        .Attr("focal: float = 525.0")
        .Attr("width: int")
        .Attr("height: int")
        .Attr("top_left: bool = true")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          // Get input dims
          std::vector<ShapeHandle> v_shp;
          auto s = ctx->input("vertex", &v_shp);
          s = ctx->set_output("grad_landmark", v_shp);
          return s;
        })
        .Doc(R"doc(
Compute DynamicLandmarkOp's gradient

g_landmark: Back-propagated gradient up to this Op.
vertex: 3D Point cloud [Batch, -1, 3]
normal: Surface's normals, must have the same dimensions as `vertex`
  [Batch, -1, 3]
indices: Index of the landmarks to be projected. Each row represents candidates
  for the ith landmarks. First element must be number of candidate.
  [N, ....]
landmark: Projected landmarks, [Batch, K, 2]
pick: Selected vertices for projection, [Batch, K]
focal: Camera focal length
width: Image's width
height: Image's height
top_left: If `True` convert to top left coordinate system. If `False` the origin
  is located at the bottom left (i.e. Right hand system)
)doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("DynamicLandmarkOpGrad")             \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      DynamicLandmarkGradKernel<CPUDevice, Type>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(Type)                       \
  REGISTER_KERNEL_BUILDER(                       \
      Name("DynamicLandmarkOpGrad")              \
      .Device(tf::DEVICE_GPU)                    \
      .TypeConstraint<Type>("T"),                \
      DynamicLandmarkGradKernel<GPUDevice, Type>);
REGISTER_GPU(float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA