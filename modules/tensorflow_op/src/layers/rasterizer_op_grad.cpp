/**
 *  @file   rasterizer_op_grad.cpp
 *  @brief  Implement custom gradient for RasterizerOp
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/27/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include "tensorflow/core/framework/shape_inference.h"

#include "lts5/tensorflow_op/layers/rasterizer_op_grad.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/*
 * @class   RasterizerGradientHelper
 * @brief   Rasterizer gradient
 * @author  Christophe Ecabert
 * @date    27/07/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct RasterizerGradientHelper<CPUDevice, T> {

  /** Hyper parameters */
  using Params = RasterizerParameters<T>;

  /**
   * @name operator()
   * @brief Compute rasterizer gradient
   * @param[in] ctx Ops context
   * @param[in] vertex  Surface vertex
   * @param[in] triangle  Surface triangles
   * @param[in] image  Rasterized image (0: tri idx, 1: bc1, 2: bc2, 3: Mask)
   * @param[in] g_bcoords Back-propagated gradient
   * @param[out] grad Computed gradient
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& triangle,
                 const tf::Tensor& image,
                 const tf::Tensor& g_bcoords,
                 tf::Tensor* grad) {
    return -1;
  }
};

/**
 * @struct   EyeToClipGradientHelper
 * @brief   EyeToCLip gradient
 * @author  Christophe Ecabert
 * @date    30/09/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct EyeToClipGradientHelper<CPUDevice, T> {

  /**
   * @name  operator()
   * #@brief    Compute projection derivative
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex to project, [B, N, 3]
   * @param[in] focal       Focal length to use, [B, 1]
   * @param[in] grad_in     Back-propagated gradient, [B, N, 4]
   * @param[in] near        Near plane
   * @param[in] far         Far plane
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] grad_vertex Gradient with respect to vertex
   * @param[in] grad_focal  Gradient with respect to focal
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& focal,
                 const tf::Tensor& grad_in,
                 const T& near,
                 const T& far,
                 const T& width,
                 const T& height,
                 tf::Tensor* grad_vertex,
                 tf::Tensor* grad_focal) {
    return -1;
  }
};

/**
 * @struct   ClipToScreenGradientHelper
 * @brief   Clip to screen gradient
 * @author  Christophe Ecabert
 * @date    01/10/2020
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct ClipToScreenGradientHelper<CPUDevice, T> {

  /**
   * @name  operator()
   * #@brief    Compute projection derivative
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex to project, [B, N, 4]
   * @param[in] grad_in     Back-propagated gradient, [B, N, 2]
   * @param[in] width       Image width
   * @param[in] height      Image height
   * @param[in] top_left    True if origin is located at top left corner,
   *    otherwise it is bottom left
   * @param[in] grad_vertex Gradient with respect to vertex
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& grad_in,
                 const T& width,
                 const T& height,
                 const bool& top_left,
                 tf::Tensor* grad_vertex) {
    return -1;
  }
};

}  // namespace Functor
}  // namespace LTS5

#pragma mark -
#pragma mark Initialization

/*
 * @name  EyeToClipGradKernel
 * @fn    explicit EyeToClipGradKernel(tf::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
EyeToClipGradKernel<Device, T>::
EyeToClipGradKernel(tf::OpKernelConstruction *ctx) : OpKernel(ctx) {
  OP_REQUIRES_OK(ctx, ctx->GetAttr("near", &near_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("far", &far_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("width", &width_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("height", &height_));
}

/*
 * @name  ~EyeToClipGradKernel
 * @fn    ~EyeToClipGradKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
EyeToClipGradKernel<Device, T>::~EyeToClipGradKernel() {}

/*
 * @name  ClipToScreenGradKernel
 * @fn    explicit ClipToScreenGradKernel(tf::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
ClipToScreenGradKernel<Device, T>::
ClipToScreenGradKernel(tf::OpKernelConstruction *ctx) : OpKernel(ctx) {
  OP_REQUIRES_OK(ctx, ctx->GetAttr("width", &width_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("height", &height_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("top_left", &top_left_));
}

/*
 * @name  ~ClipToScreenGradKernel
 * @fn    ~ClipToScreenGradKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
ClipToScreenGradKernel<Device, T>::~ClipToScreenGradKernel() {}

/*
 * @name  RasterizerGradKernel
 * @fn    explicit RasterizerGradKernel(tf::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
RasterizerGradKernel<Device, T>::
RasterizerGradKernel(tf::OpKernelConstruction *ctx) : OpKernel(ctx) {}

/*
 * @name  ~RasterizerGradKernel
 * @fn    ~RasterizerGradKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
RasterizerGradKernel<Device, T>::~RasterizerGradKernel() {}

#pragma mark -
#pragma mark Usage

/*
 * @name  Compute
 * @fn    void Compute(tf::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
void EyeToClipGradKernel<Device, T>::Compute(tf::OpKernelContext *ctx) {
  using Eye2ClipGrad = LTS5::Functor::EyeToClipGradientHelper<Device, T>;

  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);
  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* focal = nullptr;
  const tf::Tensor* grad_in = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("focal", &focal));
  OP_REQUIRES_OK(ctx, ctx->input("grad", &grad_in));

  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* grad_vertex = nullptr;
  tf::Tensor* grad_focal = nullptr;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("grad_vertex",
                                      vertex->shape(),
                                      &grad_vertex));
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("grad_focal",
                                      focal->shape(),
                                      &grad_focal));
  // COMPUTE
  // -------------------------------------------------------
  int err = Eye2ClipGrad()(ctx,
                           *vertex,
                           *focal,
                           *grad_in,
                           near_,
                           far_,
                           width_,
                           height_,
                           grad_vertex,
                           grad_focal);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in backward kernel"));
  }
}

/*
 * @name  Compute
 * @fn    void Compute(tf::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
void ClipToScreenGradKernel<Device, T>::Compute(tf::OpKernelContext *ctx) {
  using Clip2ScrGrad = LTS5::Functor::ClipToScreenGradientHelper<Device, T>;

  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);
  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* grad_in = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("grad", &grad_in));

  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* grad_vertex = nullptr;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("grad_vertex",
                                      vertex->shape(),
                                      &grad_vertex));
  // COMPUTE
  // -------------------------------------------------------
  int err = Clip2ScrGrad()(ctx,
                           *vertex,
                           *grad_in,
                           width_,
                           height_,
                           top_left_,
                           grad_vertex);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in backward kernel"));
  }
}

/**
 * @name  Compute
 * @fn    void Compute(tf::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] ctx Op kernel context
 */
template<typename Device, typename T>
void RasterizerGradKernel<Device, T>::Compute(tf::OpKernelContext *ctx) {
  using RasterGrad = LTS5::Functor::RasterizerGradientHelper<Device, T>;

  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);

  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* image = nullptr;
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* triangle = nullptr;
  const tf::Tensor* g_image = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("image", &image));
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("triangle", &triangle));
  OP_REQUIRES_OK(ctx, ctx->input("g_image", &g_image));


  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* grad_bcoord = nullptr;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("grad_barycentric",
                                      vertex->shape(),
                                      &grad_bcoord));

  // COMPUTE
  // -------------------------------------------------------
  int err = RasterGrad()(ctx,
                         *vertex,
                         *triangle,
                         *image,
                         *g_image,
                         grad_bcoord);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in backward kernel"));
  }
}

#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;
using DimensionHandle = tf::shape_inference::DimensionHandle;

REGISTER_OP("EyeToClipOpGrad")
        .Attr("T: {float}")
        .Attr("width: int")
        .Attr("height: int")
        .Attr("near: float = 1e0")
        .Attr("far: float = 1e2")
        .Input("vertex: T")
        .Input("focal: T")
        .Input("grad: T")
        .Output("grad_vertex: T")
        .Output("grad_focal: T")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          std::vector<ShapeHandle> v_shp, f_shp;
          auto s = ctx->input("vertex", &v_shp);
          s = ctx->input("focal", &f_shp);
          s = ctx->set_output("grad_vertex", v_shp);
          s = ctx->set_output("grad_focal", f_shp);
          return s;
        })
        .Doc(R"doc(Project vertex from `eye` space (i.e. camera) into `clip`
 space like OpenGL pipeline does.
vertex: Geometry of the object to project, [Batch, N, 3].
focal: Focal length to use to project, [Batch, 1].
grad: Back-propagated gradient, [Batch, N, 4]
width: Width of the image.
height Height of the image.
near: Near plane.
far: Far plane.
grad_vertex: Gradient of vertex in `clip` space with respect to vertex in `eye`
 space.
grad_focal: Gradient of vertex in `clip` space with respect to the focal length.
)doc");

REGISTER_OP("ClipToScreenOpGrad")
        .Attr("T: {float}")
        .Attr("width: int")
        .Attr("height: int")
        .Attr("top_left: bool = true")
        .Input("vertex: T")
        .Input("grad: T")
        .Output("grad_vertex: T")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          std::vector<ShapeHandle> v_shp, f_shp;
          auto s = ctx->input("vertex", &v_shp);
          s = ctx->set_output("grad_vertex", v_shp);
          return s;
        })
        .Doc(R"doc(Transform vertex from `clip` space into `screen` space like
 OpenGL pipeline does.
vertex: Geometry in clip space, [Batch, N, 4].
grad: Back-propagated gradient, [Batch, N, 2]
width: Width of the image.
height Height of the image.
grad_vertex: Gradient of vertex.)doc");

REGISTER_OP("RasterizingOpGrad")
        .Attr("T: {float}")
        .Input("g_image: T")
        .Input("image: T")
        .Input("vertex: T")
        .Input("triangle: int32")
        .Output("grad_barycentric: T")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          // Get input dims
          std::vector<ShapeHandle> v_shp;
          auto s = ctx->input("vertex", &v_shp);
          s = ctx->set_output("grad_barycentric", v_shp);
          return s;
        }).Doc(R"doc(Gradient of the rasterization of a given surface (vertex +
 triangle) using OpenGL backend.
g_image: Backpropagated gradient up to this op (i.e. Derivative of loss with
 respect to barycentric coordinate.
image: Rasterized image with following channels ordering:
 1. Triangle index (starting at 1)
 2. First barycentric coordinate
 3. Second barycentric coordinate
 4. Third barycentric coordinate
vertex: Geometry in the `clip` space of the object to render [Batch, N, 4].
triangle: Surface triangulation [T, 3] (same for each object).
grad_barycentric: Gradient of barycentric coordinates with respect to vertex in
 the `clip` space [Batch, N, 3].
)doc");

#define REGISTER_CPU(OpName, Class, Type)       \
REGISTER_KERNEL_BUILDER(                        \
      Name(OpName)                              \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      Class<CPUDevice, Type>)
REGISTER_CPU("RasterizingOpGrad", RasterizerGradKernel, float);
REGISTER_CPU("EyeToClipOpGrad", EyeToClipGradKernel, float);
REGISTER_CPU("ClipToScreenOpGrad", ClipToScreenGradKernel, float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(OpName, Class, Type)       \
  REGISTER_KERNEL_BUILDER(                      \
      Name(OpName)                              \
      .Device(tf::DEVICE_GPU)                   \
      .TypeConstraint<Type>("T"),               \
      Class<GPUDevice, Type>);
REGISTER_GPU("RasterizingOpGrad", RasterizerGradKernel, float);
REGISTER_GPU("EyeToClipOpGrad", EyeToClipGradKernel, float);
REGISTER_GPU("ClipToScreenOpGrad", ClipToScreenGradKernel, float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA
