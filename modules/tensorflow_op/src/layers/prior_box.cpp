/**
 *  @file   prior_box.cpp
 *  @brief  
 *  @ingroup 
 *
 *  @author Christophe Ecabert
 *  @date   8/12/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/lib/core/threadpool.h"

#include "lts5/tensorflow_op/layers/prior_box.hpp"

#include "lts5/utils/logger.hpp"


using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

}  // namespace Functor
}  // namespace LTS5

#pragma mark -
#pragma mark Initialization

template<typename T, typename CTX>
struct PriorBoxParam {

  /**
   * @name  PriorBoxParam
   * @brief Constructor
   */
  PriorBoxParam() : min_sizes_(),
                    max_sizes_(),
                    aspect_ratios_(),
                    flip_(true),
                    num_priors_(-1),
                    clip_(false),
                    variance_(),
                    step_(0.0),
                    offset_(0.5) {
  }

  /**
   * @name  ParseFromContext
   * @brief Fill parameters from attributes
   * @param ctx Context
   */
  tf::Status ParseFromContext(CTX* ctx) {
    // Min sizes
    std::vector<T> min_size;
    auto s = ctx->GetAttr("min_size", &min_size);
    if (s.ok()) {
      for (const auto &sz : min_size) {
        CHECK_GT(sz, 0);
        this->min_sizes_.push_back(sz);
      }
    }
    // Flip
    s = ctx->GetAttr("flip", &this->flip_);
    // Aspect ratio
    std::vector<T> a_ratio;
    s = ctx->GetAttr("aspect_ratio", &a_ratio);
    this->aspect_ratios_.push_back(1.0);
    if (s.ok()) {
      for (const auto &ar : a_ratio) {
        // Already inside ?
        bool already_exist = false;
        for (const auto &ar_in : this->aspect_ratios_) {
          if (std::abs(ar - ar_in) < 1e-6) {
            already_exist = true;
            break;
          }
        }
        if (!already_exist) {
          this->aspect_ratios_.push_back(ar);
          if (this->flip_) {
            aspect_ratios_.push_back(1.0 / ar);
          }
        }
      }
    }
    this->num_priors_ = this->aspect_ratios_.size() * this->min_sizes_.size();

    // Max size
    std::vector<T> max_size;
    s = ctx->GetAttr("max_size", &max_size);
    if (s.ok()) {
      if (!max_size.empty()) {
        CHECK_EQ(min_size.size(), max_size.size());
        for (int k = 0; k < max_size.size(); ++k) {
          this->max_sizes_.push_back(max_size[k]);
          CHECK_GT(max_size[k], min_size[k])
            << "max_size must be greater than min_size";
          this->num_priors_ += 1;
        }
      }
    }

    // clip
    s = ctx->GetAttr("clip", &this->clip_);
    // Variance
    std::vector<T> var;
    s = ctx->GetAttr("variance", &var);
    if (s.ok()) {
      if (var.size() > 1) {
        // Must and only provide 4 variance.
        CHECK_EQ(var.size(), 4);
        for (const auto &v : var) {
          CHECK_GT(v, 0);
          this->variance_.push_back(v);
        }
      } else if (var.size() == 1) {
        CHECK_GT(var[0], 0);
        this->variance_ = std::vector<T>(4, var[0]);
      } else {
        // Set default to 0.1.
        this->variance_ = std::vector<T>(4, T(0.1));
      }
    }
    // Step
    s = ctx->GetAttr("step", &this->step_);
    // Offset
    s = ctx->GetAttr("offset", &this->offset_);
    return s;
  }

  /** Minimum box size */
  std::vector<T> min_sizes_;
  /** Maximum box size */
  std::vector<T> max_sizes_;
  /** Box aspect ratio */
  std::vector<T> aspect_ratios_;
  /**  If true, will flip each aspect ratio. */
  bool flip_;
  /** Total number of priors */
  int num_priors_;
  /** If true, will clip the prior so that it is within [0, 1] */
  bool clip_;
  /** Variance for adjusting the prior bboxes */
  std::vector<T> variance_;
  /** Step */
  T step_;
  /** Offset to the top left corner of each cell. */
  T offset_;
};

/*
 * @name  PriorBoxKernel
 * @fn    explicit PriorBoxKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
PriorBoxKernel<Device, T>::
PriorBoxKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx),
                                                pool_(nullptr) {
  // Get parameters
  PriorBoxParam<T, tf::OpKernelConstruction> params;
  OP_REQUIRES_OK(ctx, params.ParseFromContext(ctx));

  this->min_sizes_ = params.min_sizes_;
  this->max_sizes_ = params.max_sizes_;
  this->aspect_ratios_ = params.aspect_ratios_;
  this->flip_ = params.flip_;
  this->num_priors_ = params.num_priors_;
  this->clip_ = params.clip_;
  this->variance_ = params.variance_;
  this->step_ = params.step_;
  this->offset_ = params.offset_;
  // Create thread pool
  this->pool_ = new tf::thread::ThreadPool(ctx->env(),
                                           "PriorBoxWorker",
                                           1);

}

/*
 * @name  ~PriorBoxKernel
 * @fn    ~PriorBoxKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
PriorBoxKernel<Device, T>::
~PriorBoxKernel() {
  if (pool_) {
    delete pool_;
  }
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void PriorBoxKernel<Device, T>::Compute(tensorflow::OpKernelContext* ctx) {
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);


  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* image;
  const tf::Tensor* features;

  OP_REQUIRES_OK(ctx,
                 ctx->input("image", &image));  // [Batch, H, W, C]
  OP_REQUIRES_OK(ctx,
                 ctx->input("features", &features)); // [Kf, Hf, Wf, Cf]

  // Image size
  this->img_h_ = image->dim_size(1);
  this->img_w_ = image->dim_size(2);
  CHECK_GT(this->img_h_, 0);
  CHECK_GT(this->img_w_, 0);
  // Feature size
  this->feat_h_ = features->dim_size(1);
  this->feat_w_ = features->dim_size(2);
  CHECK_GT(this->feat_h_, 0);
  CHECK_GT(this->feat_w_, 0);


  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* priors;
  int nbox = this->feat_h_ * this->feat_w_ * this->num_priors_;

  // priors[0, :, :] = anchors
  // priors[1, :, :] = variance
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("priors",
                                      {2, nbox, 4},
                                      &priors));

  // COMPUTE
  // -------------------------------------------------------
  T* data = priors->flat<T>().data();
  tf::int64 prior_offset = this->num_priors_ * 4;
  auto generate_prior = [&](tf::int64 from, tf::int64 to) -> void {
    // Goes through all lines [from, to]
    for (tf::int64 h = from; h < to; ++h) {
      // Goes through all columns [0, feat_width[
      for (tf::int64 w = 0; w < this->feat_w_; ++w) {
        // Priors
        // ------------------------------------------------------------------
        T center_x = (static_cast<T>(w) + offset_) * this->step_;
        T center_y = (static_cast<T>(h) + offset_) * this->step_;
        T box_width, box_height;
        // Goes through all min sizes
        for (int s = 0; s < min_sizes_.size(); ++s) {
          T min_sz = static_cast<T>(min_sizes_[s]);
          // first prior: aspect_ratio = 1, size = min_size
          box_width = box_height = min_sz;
          int idx = ((h * this->feat_w_) + w) * prior_offset;
          int local_idx = 0;
          // xmin, ymin, xmax, ymax
          T xmin = (center_x - box_width / 2.) / this->img_w_;
          T ymin = (center_y - box_height / 2.) / this->img_h_;
          T xmax = (center_x + box_width / 2.) / this->img_w_;
          T ymax = (center_y + box_height / 2.) / this->img_h_;

          xmin = this->clip_ ? std::min(std::max(xmin, T(0.0)), T(1.0)) : xmin;
          ymin = this->clip_ ? std::min(std::max(ymin, T(0.0)), T(1.0)) : ymin;
          xmax = this->clip_ ? std::min(std::max(xmax, T(0.0)), T(1.0)) : xmax;
          ymax = this->clip_ ? std::min(std::max(ymax, T(0.0)), T(1.0)) : ymax;

          T* d_ptr = &data[idx];
          d_ptr[local_idx++] = xmin;
          d_ptr[local_idx++] = ymin;
          d_ptr[local_idx++] = xmax;
          d_ptr[local_idx++] = ymax;

          if (!max_sizes_.empty()) {
            CHECK_EQ(min_sizes_.size(), max_sizes_.size());
            T max_sz = static_cast<T>(max_sizes_[s]);

            // second prior: aspect_ratio = 1, size = sqrt(min_size * max_size)
            box_width = box_height = std::sqrt(min_sz * max_sz);
            // xmin, ymin, xmax, ymax
            xmin = (center_x - box_width / 2.) / this->img_w_;
            ymin = (center_y - box_height / 2.) / this->img_h_;
            xmax = (center_x + box_width / 2.) / this->img_w_;
            ymax = (center_y + box_height / 2.) / this->img_h_;

            xmin = this->clip_ ? std::min(std::max(xmin, T(0.0)), T(1.0)) : xmin;
            ymin = this->clip_ ? std::min(std::max(ymin, T(0.0)), T(1.0)) : ymin;
            xmax = this->clip_ ? std::min(std::max(xmax, T(0.0)), T(1.0)) : xmax;
            ymax = this->clip_ ? std::min(std::max(ymax, T(0.0)), T(1.0)) : ymax;

            d_ptr[local_idx++] = xmin;
            d_ptr[local_idx++] = ymin;
            d_ptr[local_idx++] = xmax;
            d_ptr[local_idx++] = ymax;
          }

          // rest of priors
          for (int r = 1; r < aspect_ratios_.size(); ++r) {
            T ar = aspect_ratios_[r];
            box_width = min_sz * std::sqrt(ar);
            box_height = min_sz / std::sqrt(ar);

            // xmin, ymin, xmax, ymax
            xmin = (center_x - box_width / 2.) / this->img_w_;
            ymin = (center_y - box_height / 2.) / this->img_h_;
            xmax = (center_x + box_width / 2.) / this->img_w_;
            ymax = (center_y + box_height / 2.) / this->img_h_;

            xmin = this->clip_ ? std::min(std::max(xmin, T(0.0)), T(1.0)) : xmin;
            ymin = this->clip_ ? std::min(std::max(ymin, T(0.0)), T(1.0)) : ymin;
            xmax = this->clip_ ? std::min(std::max(xmax, T(0.0)), T(1.0)) : xmax;
            ymax = this->clip_ ? std::min(std::max(ymax, T(0.0)), T(1.0)) : ymax;

            d_ptr[local_idx++] = xmin;
            d_ptr[local_idx++] = ymin;
            d_ptr[local_idx++] = xmax;
            d_ptr[local_idx++] = ymax;
          }
        }

        // Variances
        // ------------------------------------------------------------------
        int idx = this->feat_h_ * this->feat_w_ * prior_offset;
        idx += ((h * this->feat_w_) + w) * prior_offset;
        T* d_ptr = &data[idx];
        for (int k = 0; k < 4; ++k) {
          d_ptr[k] = this->variance_[k];
        }
      }
    }
  };

  // Run shards
  tf::int64 cost_per_unit = this->feat_w_ * prior_offset;
  tf::int64 total = this->feat_h_;
  pool_->ParallelFor(total, cost_per_unit, generate_prior);
}

#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;
using DimensionHandle = tf::shape_inference::DimensionHandle;

REGISTER_OP("PriorBoxOp")
        .Input("image: float")
        .Input("features: float")
        .Output("priors: float")
        .Attr("min_size: list(float)")
        .Attr("aspect_ratio: list(float) = []")
        .Attr("variance: list(float) = [0.1, 0.1, 0.2, 0.2]")
        .Attr("max_size: list(float) = []")
        .Attr("flip: bool = True")
        .Attr("clip: bool = False")
        .Attr("step: float = 0.0")
        .Attr("offset: float = 0.5")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          // Get feature maps dims
          std::vector<ShapeHandle> feat_shp;
          auto s = ctx->input("features", &feat_shp);
          auto feat_h = ctx->Dim(feat_shp[0], 1);
          auto feat_w = ctx->Dim(feat_shp[0], 2);
          DimensionHandle prod;
          s = ctx->Multiply(feat_h, feat_w, &prod);
          // Get parameters
          PriorBoxParam<float, InferenceContext> params;
          s = params.ParseFromContext(ctx);
          if (s.ok()) {
            // int nbox = feat_h * feat_w * params.num_priors_;
            DimensionHandle nbox;
            s = ctx->Multiply(prod, params.num_priors_, &nbox);
            auto shp = ctx->MakeShape({2, nbox, 4});
            s = ctx->set_output("priors", {shp});
          }
          return s;
        })
        .Doc(R"doc(Generate bounding box priors

image: Input image [Batch, H, W, C]
features: Feature maps [K, Hf, Wf, Cf]
priors: Generated bounding box priors [2, #Priors, 4]. Slices have the
  following meaning:
  [0, ...] = anchors, [1, ...] = variance
min_size: Minimum box size (in pixels), can be multiple
aspect_ratio: Various of aspect ratios. Duplicate ratios will be ignored. If
  none is provided, we use default ratio 1.
variance: Variance for adjusting the prior bboxes
max_size: Maximum box size in pixels. can be ignored or same as the # of
  min_size
flip: If true, will flip each aspect ratio (i.e. r => 1/r).
clip: If true, will clip the prior so that it is within [0, 1]
step: Step size.
offset: Offset to the top left corner of each cell.)doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("PriorBoxOp")                        \
      .Device(tf::DEVICE_CPU),                  \
      PriorBoxKernel<CPUDevice, Type>)
REGISTER_CPU(float);

// Remove definition
#undef REGISTER_CPU
