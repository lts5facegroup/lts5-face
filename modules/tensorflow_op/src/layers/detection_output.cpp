/**
 *  @file   detection_output.cpp
 *  @brief  Generate the detection output based on location and confidence
 *          predictions by doing non maximum suppression.
 *          Intended for use with MultiBox detection method.
 *          NOTE: does not implement Backwards operation.
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   8/14/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "tensorflow/core/framework/shape_inference.h"

#include "lts5/tensorflow_op/layers/detection_output.hpp"
#include "lts5/tensorflow_op/utils/shape.hpp"

#include "lts5/utils/logger.hpp"


using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;


#pragma mark -
#pragma mark Initialization

/*
 * @name  DetectionOutputKernel
 * @fn    explicit DetectionOutputKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
DetectionOutputKernel<Device, T>::
DetectionOutputKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx) {

  OP_REQUIRES_OK(ctx, ctx->GetAttr("num_classes",
                                   &this->num_classes_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("share_location",
                                   &this->share_location_));
  this->num_loc_classes_ = this->share_location_ ?
                           1 :
                           this->num_classes_;
  OP_REQUIRES_OK(ctx, ctx->GetAttr("background_label_id",
                                   &this->background_label_id_));
  int type = -1;
  OP_REQUIRES_OK(ctx, ctx->GetAttr("code_type", &type));
  CHECK_GT(type, 0) << "code_type must be {1, 2, 3}";
  CHECK_LT(type, 4) << "code_type must be {1, 2, 3}";
  this->code_type_ = static_cast<LTS5::PriorBoxType>(type);

  OP_REQUIRES_OK(ctx, ctx->GetAttr("variance_encoded_in_target",
                                   &this->variance_encoded_in_target_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("keep_top_k",
                                   &this->keep_top_k_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("confidence_threshold",
                                   &this->confidence_threshold_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("nms_threshold",
                                   &this->nms_threshold_));
  CHECK_GT(this->nms_threshold_, T(0.0)) << "nms_threshold must be positive";
  OP_REQUIRES_OK(ctx, ctx->GetAttr("nms_top_k",
                                   &this->nms_top_k_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("nms_eta",
                                   &this->nms_eta_));
  CHECK_GT(this->nms_eta_, 0.0);
  CHECK_LE(this->nms_eta_, 1.0);
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void DetectionOutputKernel<Device, T>::Compute(tf::OpKernelContext* ctx) {
  using Shape = LTS5::ShapeUtils;
  using Indices = std::unordered_map<int, std::vector<int>>;
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);
  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* location = nullptr;
  const tf::Tensor* confidence = nullptr; // Boxes confidence
  const tf::Tensor* priors = nullptr;     // Anchors + variances
  OP_REQUIRES_OK(ctx, ctx->input("location", &location));
  OP_REQUIRES_OK(ctx, ctx->input("confidence", &confidence));
  OP_REQUIRES_OK(ctx, ctx->input("priors", &priors));

  //Sanity check
  Shape::CheckShape(ctx,
                    *location,
                    3,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, -1, 4});
  Shape::CheckShape(ctx,
                    *confidence,
                    3,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, -1, num_classes_});
  Shape::CheckShape(ctx,
                    *priors,
                    3,
                    tf::DataTypeToEnum<T>::v(),
                    {2, -1, 4});

  num_priors_ = priors->dim_size(1);
  CHECK_EQ(num_priors_ * num_loc_classes_, location->dim_size(1))
    << "Number of priors must match number of location predictions.";
  CHECK_EQ(num_priors_, confidence->dim_size(1))
    << "Number of priors must match number of confidence predictions.";

  // COMPUTE
  // -------------------------------------------------------
  const int num = static_cast<int>(location->dim_size(0));
  const T* loc_data = location->flat<T>().data();
  const T* conf_data = confidence->flat<T>().data();
  const T* prior_data = priors->flat<T>().data();


  //LTS5_LOG_INFO("Conf shape: " << confidence->shape().DebugString());
  //LTS5_LOG_INFO("  Num priors: " << num_priors_);
  //LTS5_LOG_INFO("  Num classes: " << num_classes_);
  //LTS5_LOG_INFO("  Num loc classes: " << num_loc_classes_);

  // Retrieve all location predictions.
  std::vector<LTS5::LabelBBox<T>> all_loc_preds;
  LTS5::GetLocPredictions<T>(loc_data,
                             num,
                             num_priors_,
                             num_loc_classes_,
                             share_location_,
                             &all_loc_preds);

  // Retrieve all confidences.
  std::vector<LTS5::ConfScore<T>> all_conf_scores;
  LTS5::GetConfidenceScores<T>(conf_data,
                               num,
                               num_priors_,
                               num_classes_,
                               &all_conf_scores);

  // Retrieve all prior bboxes. It is same within a batch since we assume all
  // images in a batch are of same dimension.
  std::vector<LTS5::NormalizedBBox<T>> prior_bboxes;
  std::vector<LTS5::Vars<T>> prior_variances;
  LTS5::GetPriorBBoxes<T>(prior_data,
                          num_priors_,
                          &prior_bboxes,
                          &prior_variances);

  // Decode all loc predictions to bboxes.
  std::vector<LTS5::LabelBBox<T>> all_decode_bboxes;
  const bool clip_bbox = false;
  LTS5::DecodeBBoxesAll<T>(all_loc_preds,
                           prior_bboxes,
                           prior_variances,
                           num,
                           share_location_,
                           num_loc_classes_,
                           background_label_id_,
                           code_type_,
                           variance_encoded_in_target_,
                           clip_bbox,
                           &all_decode_bboxes);


  int num_kept = 0;
  std::vector<Indices> all_indices;
  // Goes through all decoded bounding box
  auto cmp = [](const LTS5::ScoreIndices<T>& p1,
                const LTS5::ScoreIndices<T>& p2) -> bool {
    return p1.score > p2.score;
  };
  for (int i = 0; i < num; ++i) {
    const auto& decode_bboxes = all_decode_bboxes[i];
    const auto& conf_scores = all_conf_scores[i];
    Indices indices;  // Label, indices
    int num_det = 0;
    for (int c = 0; c < num_classes_; ++c) {
      if (c == background_label_id_) {
        // Ignore background class.
        continue;
      }

      // Search confidence for class 'c'
      auto conf_it = conf_scores.find(c);
      if (conf_it == conf_scores.end()) {
        // Something bad happened if there are no predictions for current label.
        LOG(FATAL) << "Could not find confidence predictions for label " << c;
      }

      // Get all score for class 'c'
      const auto& scores = conf_it->second;
      int label = share_location_ ? -1 : c;
      // Search box for this label
      auto bboxes_it = decode_bboxes.find(label);
      if (bboxes_it == decode_bboxes.end()) {
        // Something bad happened if there are no predictions for current label.
        LOG(FATAL) << "Could not find location predictions for label " << label;
        continue;
      }
      const auto& bboxes = bboxes_it->second;
      LTS5::ApplyNMSFast<T>(bboxes,
                            scores,
                            confidence_threshold_,
                            nms_threshold_,
                            nms_eta_,
                            nms_top_k_,
                            &(indices[c]));
      num_det += indices[c].size();
    }

    if (keep_top_k_ > -1 && num_det > keep_top_k_) {
      std::vector<LTS5::ScoreIndices<T>> score_index_pairs;
      for (auto it = indices.begin(); it != indices.end(); ++it) {
        int label = it->first;
        const auto& label_indices = it->second;

        auto conf_scores_it = conf_scores.find(label);
        if (conf_scores_it == conf_scores.end()) {
          // Something bad happened for current label.
          LOG(FATAL) << "Could not find location predictions for " << label;
          continue;
        }
        const std::vector<T>& scores = conf_scores_it->second;
        for (int idx: label_indices) {
          CHECK_LT(idx, scores.size());
          score_index_pairs.push_back({scores[idx], label, idx});
        }
      }
      // Keep top k results per image.
      std::sort(score_index_pairs.begin(), score_index_pairs.end(), cmp);
      score_index_pairs.resize(keep_top_k_);

      // Store the new indices.
      Indices new_indices;
      /*for (int j = 0; j < score_index_pairs.size(); ++j) {
        int label = score_index_pairs[j].second.first;
        int idx = score_index_pairs[j].second.second;
        new_indices[label].push_back(idx);
      }*/
      for(const auto& idx_pair : score_index_pairs) {
        new_indices[idx_pair.label].push_back(idx_pair.index);
      }
      all_indices.push_back(new_indices);
      num_kept += keep_top_k_;
    } else {
      all_indices.push_back(indices);
      num_kept += num_det;
    }
  }
  // OUTPUTS
  // -------------------------------------------------------
  // Define output once detection decoding as been performed since we don't
  // know in advance how many boxes will be present
  tf::Tensor* out_detection = nullptr;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("output",
                                      {num_kept, 7},
                                      &out_detection));
  // Copy data
  T* top_data = out_detection->flat<T>().data();
  int count = 0;
  // Loop over each images
  for (int i = 0; i < num; ++i) {
    const auto& conf_scores = all_conf_scores[i];
    const auto& decode_bboxes = all_decode_bboxes[i];
    for (auto it = all_indices[i].begin(); it != all_indices[i].end(); ++it) {

      int label = it->first;
      auto conf_scores_it = conf_scores.find(label);
      if (conf_scores_it == conf_scores.end()) {
        // Something bad happened if there are no predictions for current label.
        LOG(FATAL) << "Could not find confidence predictions for " << label;
        continue;
      }
      const auto& scores = conf_scores_it->second;
      int loc_label = share_location_ ? -1 : label;
      auto decode_bboxes_it = decode_bboxes.find(loc_label);
      if (decode_bboxes_it == decode_bboxes.end()) {
        // Something bad happened if there are no predictions for current label.
        LOG(FATAL) << "Could not find location predictions for " << loc_label;
        continue;
      }
      const auto& bboxes = decode_bboxes_it->second;
      const auto& indices = it->second;

      for (int idx : indices) {
        top_data[count * 7] = i;
        top_data[count * 7 + 1] = label;
        top_data[count * 7 + 2] = scores[idx];
        const auto& bbox = bboxes[idx];
        top_data[count * 7 + 3] = bbox.xmin;
        top_data[count * 7 + 4] = bbox.ymin;
        top_data[count * 7 + 5] = bbox.xmax;
        top_data[count * 7 + 6] = bbox.ymax;
        ++count;
      }
    }
  }
}


#pragma mark -
#pragma mark Registration With Tensorflow
using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;
using DimensionHandle = tf::shape_inference::DimensionHandle;

REGISTER_OP("DetectionOutputOp")
        .Input("location: T")
        .Input("confidence: T")
        .Input("priors: T")
        .Output("output: T")
        .Attr("T: {float}")
        .Attr("num_classes: int")
        .Attr("share_location: bool = true")
        .Attr("background_label_id: int = 0")
        .Attr("code_type: int = 1")
        .Attr("variance_encoded_in_target: bool = false")
        .Attr("keep_top_k: int = -1")
        .Attr("confidence_threshold: float = -1.0")
        .Attr("nms_threshold: float = 0.3")
        .Attr("nms_top_k: int = -1")
        .Attr("nms_eta: float = 1.0")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          auto shp = ctx->MakeShape({InferenceContext::kUnknownDim, 7});
          ctx->set_output("output", {shp});
          return tf::Status::OK();
        })
        .Doc(R"doc(
Generate the detection output.

This op generate detection output based on location and confidence predictions
by doing non maximum suppression.

location: Locatiion predicted [Batch, -1, 4]
confidence: Confidence [Batch, -1, #classes]
priors: Box priors [2, -1, 4]
output: Detection output [N, 7] where last dimensions has the following
  informations: [image_id, label, confidence, xmin, ymin, xmax, ymax]
num_classes: Number of classes predicted
share_location: If true, bounding box are shared among different classes.
background_label_id: Background label id. If there is no background class, set
  it as -1.
code_type: Type of coding method for bbox.
variance_encoded_in_target: If true, variance is encoded in target; otherwise
  we need to adjust the predicted offset accordingly.
keep_top_k: Number of total bboxes to be kept per image after nms step.
  -1 means keeping all bboxes after nms step.
confidence_threshold: Only consider detections whose confidences are larger than
  a threshold. If -1.0, consider all boxes.
nms_threshold: Threshold to be used in nms.
nms_top_k: Maximum number of results to be kept. -1 keep them all
nms_eta: Adaptive nms
)doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("DetectionOutputOp")                 \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      DetectionOutputKernel<CPUDevice, Type>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU