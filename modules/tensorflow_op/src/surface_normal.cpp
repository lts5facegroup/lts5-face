/**
 *  @file   surface_normal.cpp
 *  @brief  Compute surface's normals of a given "mesh"
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/5/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>
#include <algorithm>

#include "lts5/tensorflow_op/surface_normal.hpp"

#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/lib/core/errors.h"

#include "lts5/tensorflow_op/utils/shape.hpp"
#include "lts5/tensorflow_op/tensor_utils.hpp"
#include "lts5/tensorflow_op/file_io.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/geometry/mesh.hpp"

#ifdef GOOGLE_CUDA
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
#endif

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/*
 * @class   NormalGeneratorV2
 * @brief   Compute surface's normals
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct NormalGeneratorV2<CPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in,out] ctx Op's context
   * @param[in] vertex  Surface's vertcies
   * @param[in] conn    Connectivity
   * @param[out] normal Surface's normals computed
   * @param[out] scaling Normal's length before normalization to unit length
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* vertex,
                 const tf::Tensor* conn,
                 tf::Tensor* normal,
                 tf::Tensor* scaling) {
    return -1;
  }
};

}  // namespace Functor
}  // namespace LTS5



#pragma mark -
#pragma mark Initialization

/*
 * @name  SurfaceNormalKernel
 * @fn    explicit SurfaceNormalKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
SurfaceNormalKernel<Device, T>::
SurfaceNormalKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx) {}

/*
 * @name  ~SurfaceNormalKernel
 * @fn    ~SurfaceNormalKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
SurfaceNormalKernel<Device, T>::~SurfaceNormalKernel() {}

#pragma mark -
#pragma mark Usage

/**
 * @name
 * @brief Build connectivity map for a given triangulation.
 * @param[in,out] ctx          Op's context
 * @param[in]     vertex       Vertex array [Batch x 3N]
 * @param[in]     triangle     Triangulation [Tx3]
 * @param[out]    connectivity Connectivity map [N x max(2x#Edge)]
 */
template<typename Device, typename T>
static void BuildConnectivity(tf::OpKernelContext* ctx,
                              const tf::Tensor* vertex,
                              const tf::Tensor* triangle,
                              tf::PersistentTensor* connectivity) {

  using EIntMatrix = typename LTS5::IO<T>::EIntMatrix;
  using TensorMap = typename tensorflow::TTypes<int>::Tensor;
  using CTensorMap = typename tensorflow::TTypes<int>::ConstTensor;
  using CopyFuncD2H = LTS5::Functor::DenseOp<Device, int, LTS5::Functor::DenseOpType::kD2HCopy>;
  using CopyFuncH2D = LTS5::Functor::DenseOp<Device, int, LTS5::Functor::DenseOpType::kH2DCopy>;
  using CVecI = const std::vector<int>;
  using Mesh = LTS5::Mesh<T, LTS5::Vector3<T>>;
  using Triangle = typename Mesh::Triangle;
  using Vertex = typename Mesh::Vertex;
  // Move triangulation to host memory
  const auto& device = ctx->eigen_device<Device>();
  std::vector<Triangle> htri(triangle->dim_size(0));
  {
    auto in = triangle->flat<int>();
    auto out = TensorMap((int*)htri.data(), htri.size() * 3);
    CopyFuncD2H()(device, in, out);
  }
  int n_vertex = static_cast<int>(vertex->dim_size(1));
  // Compute connectivity
  Mesh m;
  m.set_triangle(htri);
  m.set_vertex(std::vector<Vertex>(n_vertex));
  m.BuildConnectivity();
  // Find largest neighborhood
  const auto& conn = m.get_connectivity();
  auto it_conn = std::max_element(conn.begin(),
                                  conn.end(),
                                  [](CVecI& a, CVecI& b) -> bool {
                                    return a.size() < b.size();
                                  });
  size_t max_conn = it_conn->size();
  // Store it into Eigen matrix
  EIntMatrix econn = EIntMatrix::Zero(conn.size(), max_conn + 1);
  for (size_t v = 0; v < n_vertex; ++v) {
    // Fill connectivity for each vertex
    const auto& edges = conn[v];
    const size_t sz = edges.size();
    econn(v, 0) = static_cast<int>(sz);
    // Add element
    for (size_t i = 0; i < sz; ++i) {
      econn(v, i + 1) = edges[i];
    }
  }
  // Init persistent storage + copy
  tf::Tensor* buffer;
  tf::AllocatorAttributes attr;
  attr.set_gpu_compatible(true);
  auto s = ctx->allocate_persistent(tf::DataTypeToEnum<int>::value,
                                    {econn.rows(), econn.cols()},
                                    connectivity,
                                    &buffer,
                                    attr);
  if (s.ok()) {
    auto map = CTensorMap(econn.data(), econn.rows() * econn.cols());
    CopyFuncH2D()(device, map, buffer->flat<int>());
  } else {
    ctx->CtxFailureWithWarning(__FILE__, __LINE__, s);
  }
}

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void SurfaceNormalKernel<Device, T>::Compute(tf::OpKernelContext* ctx) {
  using NormalGen = LTS5::Functor::NormalGeneratorV2<Device, T>;
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);

  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* triangle = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("triangle", &triangle));

  // Sanity check
  LTS5::ShapeUtils::CheckRankAndType(ctx,
                                     *vertex,
                                     3,
                                     tf::DataTypeToEnum<T>::v());
  OP_REQUIRES(ctx,
              vertex->dim_size(2) == 3,
              tf::errors::InvalidArgument("Vertex dims: ",
                                          vertex->dim_size(2),
                                          " != 3"));
  LTS5::ShapeUtils::CheckShape(ctx,
                               *triangle,
                               2,
                               tf::DataTypeToEnum<int>::v(),
                               {-1, 3});

  // Connectivity already build ?
  if (this->conn_.NumElements() == 0) {
    LTS5_LOG_DEBUG("Build connectivity");
    BuildConnectivity<Device, T>(ctx, vertex, triangle, &this->conn_);
  }
  // OUTPUTS
  // -------------------------------------------------------
  auto bsize = vertex->dim_size(0);
  auto N = vertex->dim_size(1);
  tf::Tensor* normal;
  tf::Tensor* scaling;
  OP_REQUIRES_OK(ctx, ctx->allocate_output("normal",
                                           vertex->shape(),
                                           &normal));
  OP_REQUIRES_OK(ctx, ctx->allocate_output("scaling",
                                           {bsize, N},
                                           &scaling));
  const auto* conn = this->conn_.AccessTensor(ctx);
  int err = NormalGen()(ctx, vertex, conn, normal, scaling);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in forward kernel"));
  }
}

#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;
using DimensionHandle = tf::shape_inference::DimensionHandle;

REGISTER_OP("SurfaceNormalOp")
        .Input("vertex: T")
        .Input("triangle: int32")
        .Output("normal: T")
        .Output("scaling: T")
        .Attr("T: {float}")
        .SetIsStateful()  // Store connectivity internally !!!
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          // Normals have the same size as vertex array!
          std::vector<ShapeHandle> input_shp;
          auto s = ctx->input("vertex", &input_shp);
          s = ctx->set_output("normal", input_shp);

          auto scaling_shp = ctx->MakeShape({ctx->Dim(input_shp[0], 0),
                                             ctx->Dim(input_shp[0], 1)});
          s = ctx->set_output("scaling", {scaling_shp});
          return s;
        })
        .Doc(R"doc(Compute surface's normals at each vertex
vertex: Stacked vertex position [Batch, N, 3]
triangle: Triangulation of the surface shared for all meshes
normal: Computed stacked normals [Batch, N, 3]
scaling: Norm of the normal before normalization to unit length [Batch, N])doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("SurfaceNormalOp")                   \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      SurfaceNormalKernel<CPUDevice, Type>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(Type)                                                 \
  REGISTER_KERNEL_BUILDER(                                                 \
      Name("SurfaceNormalOp")                                              \
      .Device(tf::DEVICE_GPU)                                              \
      .TypeConstraint<Type>("T"),                                          \
      SurfaceNormalKernel<GPUDevice, Type>);
REGISTER_GPU(float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA
