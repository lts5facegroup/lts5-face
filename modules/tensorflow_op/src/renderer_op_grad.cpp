/**
 *  @file   renderer_op_grad.cpp
 *  @brief  Gradient fir the rendering operation
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   22/7/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "tensorflow/core/framework/shape_inference.h"

#include "lts5/tensorflow_op/renderer_op_grad.hpp"
#include "lts5/tensorflow_op/utils/shape.hpp"
#include "lts5/tensorflow_op/mm_image_processor.hpp"


#include "lts5/utils/file_io.hpp"
#include "lts5/tensorflow_op/tensor_utils.hpp"
#include "lts5/tensorflow_op/device_utils.hpp"

#ifdef GOOGLE_CUDA
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
#include "lts5/cuda/tensorflow_op/device_utils.hpp"
#endif


using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   RenderingGradientHelper
 * @brief   Compute renderer gradient with respecto to vertex/color
 * @author  Christophe Ecabert
 * @date    22/07/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct RenderingGradientHelper<CPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute renderer gradient
   * @param[in] ctx             Op's context
   * @param[in] g_image         Back propagated gradient
   * @param[in] vertex          Input vertex
   * @param[in] color           Input color
   * @param[in] triangle        Surface's triangulation
   * @param[in] bcoord          Barycentric coordinates
   * @param[in] spatial_grad    Image spatial gradient
   * @param[in] focal           Focal length
   * @param[out] grad_vertex    Grad dImage_dVertex
   * @param[out] grad_color     Grad dImage_dColor
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_image,
                 const tf::Tensor* vertex,
                 const tf::Tensor* color,
                 const tf::Tensor* triangle,
                 const tf::Tensor* bcoord,
                 const tf::Tensor* spatial_grad,
                 const T& focal,
                 tf::Tensor* grad_vertex,
                 tf::Tensor* grad_color) {
    return -1;
  }
};

}  // namespace Functor
}  // namespace LTS5

#pragma mark -
#pragma mark Initialization

/*
 * @name  RenderingGradKernel
 * @fn    explicit RenderingGradKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
RenderingGradKernel<Device, T>::
RenderingGradKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx) {
  // Query Attr
  // -----------------------------------
  OP_REQUIRES_OK(ctx, ctx->GetAttr("height", &height_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("width", &width_));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("focal", &focal_));
}

/*
 * @name  ~RenderingGradKernel
 * @fn    ~RenderingGradKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
RenderingGradKernel<Device, T>::~RenderingGradKernel() {}

#pragma mark -
#pragma mark Usage

#define MULTIPLE_OF_THREE(x, dim, mult)                  \
  OP_REQUIRES(ctx,                                       \
  x->dim_size(dim) % mult == 0,                          \
  tf::errors::InvalidArgument("Wrong "#x" dims: ",  \
                              x->dim_size(dim),          \
                              "%"#mult" != 0"))

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void RenderingGradKernel<Device, T>::Compute(tensorflow::OpKernelContext* ctx) {
  using Shape = LTS5::ShapeUtils;
  using GradHelper = LTS5::Functor::RenderingGradientHelper<Device, T>;
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);
  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* g_image = nullptr;
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* normal = nullptr;
  const tf::Tensor* color = nullptr;
  const tf::Tensor* background = nullptr;
  const tf::Tensor* triangle = nullptr;
  tf::OpInputList images; // [Image, bcoord, grad]
  OP_REQUIRES_OK(ctx, ctx->input("g_image", &g_image));
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("normal", &normal));
  OP_REQUIRES_OK(ctx, ctx->input("color", &color));
  OP_REQUIRES_OK(ctx, ctx->input("background", &background));
  OP_REQUIRES_OK(ctx, ctx->input("triangle", &triangle));
  OP_REQUIRES_OK(ctx, ctx->input_list("image", &images));

  // Sanity check
  OP_REQUIRES(ctx,
              images.size() == 3,
              tf::errors::InvalidArgument("Gradient can ony be computed during training"));
  Shape::CheckShape(ctx,
                    *g_image,
                    4,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, height_, width_, 3});
  Shape::CheckRankAndType(ctx, *vertex, 2, tf::DataTypeToEnum<T>::v());
  Shape::CheckRankAndType(ctx, *normal, 2, tf::DataTypeToEnum<T>::v());
  Shape::CheckRankAndType(ctx, *color, 2, tf::DataTypeToEnum<T>::v());
  Shape::CheckShape(ctx,
                    *background,
                    4,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, height_, width_, 3});
  Shape::CheckShape(ctx,
                    *triangle,
                    2,
                    tf::DataTypeToEnum<int>::v(),
                    {-1, 3});
  bool same_bsize = ((vertex->dim_size(0) == normal->dim_size(0)) &&
                     (vertex->dim_size(0) == color->dim_size(0)) &&
                     (vertex->dim_size(0) == background->dim_size(0)));
  OP_REQUIRES(ctx,
              same_bsize,
              tf::errors::InvalidArgument("Inputs haven't same batch size"));
  // Attributes must be a multiple of 3
  MULTIPLE_OF_THREE(vertex, 1, 3);
  MULTIPLE_OF_THREE(normal, 1, 3);
  MULTIPLE_OF_THREE(color, 1, 3);
  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* grad_vertex = nullptr;
  tf::Tensor* grad_color = nullptr;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("grad_vertex",
                                      vertex->shape(),
                                      &grad_vertex));
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("grad_color",
                                      color->shape(),
                                      &grad_color));
  // COMPUTE
  // -------------------------------------------------------
  int err = GradHelper()(ctx,
                         g_image,
                         vertex,
                         color,
                         triangle,
                         &images[1],
                         &images[2],
                         focal_,
                         grad_vertex,
                         grad_color);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in grad kernel"));
  }
}

#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;
using DimensionHandle = tf::shape_inference::DimensionHandle;

REGISTER_OP("RenderingOpGrad")
        .Input("g_image: T")
        .Input("vertex: T")
        .Input("normal: T")
        .Input("color: T")
        .Input("background: T")
        .Input("triangle: int32")
        .Input("image: n_out * T")
        .Output("grad_vertex: T")
        .Output("grad_color: T")
        .Attr("T: {float}")
        .Attr("width: int")
        .Attr("height: int")
        .Attr("focal: float")
        .Attr("n_out: int = 4")
        .SetIsStateful()
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          std::vector<ShapeHandle> v_shp;
          std::vector<ShapeHandle> c_shp;
          // Query input dims
          auto s = ctx->input("vertex", &v_shp);
          s = ctx->input("color", &c_shp);
          // Set output dims
          s = ctx->set_output("grad_vertex", v_shp);
          s = ctx->set_output("grad_color", c_shp);
          return s;
        })
        .Doc(R"doc(Render a given scene
vertex: Geometry of the object to render [Batch, 3N]
normal: Object's normals [Batch, 3N]
color: Vertex colors [Batch, 3N]
background: Image background to render (only used in training)
triangle: Surface triangulation (same for each object)
image: List of rendered images [Batch, H, W, 3]. (Image, Mask, Barycentric)
grad_vertex: Grad: dImage_dVertex
grad_color: Grad: dImage_dColor
)doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("RenderingOpGrad")                   \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      RenderingGradKernel<CPUDevice, Type>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(Type)                                                 \
  REGISTER_KERNEL_BUILDER(                                                 \
      Name("RenderingOpGrad")                                              \
      .Device(tf::DEVICE_GPU)                                              \
      .TypeConstraint<Type>("T"),                                          \
      RenderingGradKernel<GPUDevice, Type>);
REGISTER_GPU(float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA

