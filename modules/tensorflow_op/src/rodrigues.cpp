/**
 *  @file   rodrigues.cpp
 *  @brief  Rotation parametrization using Rodrigues formula.
 *  @see    https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/8/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "tensorflow/core/framework/shape_inference.h"

#include "lts5/tensorflow_op/rodrigues.hpp"
#include "lts5/tensorflow_op/utils/shape.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/*
 * @class   RodriguesHelper
 * @brief   Compute rotation matrix using rodrigues formula
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct RodriguesHelper<CPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute rotation matrix using Rodrigues formula
   * @param[in] ctx     Op's context
   * @param[in] rvec    Rotation vector
   * @param[out] rmat   Rotation matrix
   */
  int operator()(tf::OpKernelContext *ctx,
                 const tf::Tensor *rvec,
                 tf::Tensor *rmat) {
    return -1;
  }
};
}  // namespace Functor
}  // namespace LTS5

#pragma mark -
#pragma mark Initialization

/*
 * @name  RodriguesKernel
 * @fn    explicit RodriguesKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
RodriguesKernel<Device, T>::
RodriguesKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx) {}

/*
 * @name  ~RodriguesKernel
 * @fn    ~RodriguesKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
RodriguesKernel<Device, T>::~RodriguesKernel() {}

#pragma mark -
#pragma mark Usage

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void RodriguesKernel<Device, T>::Compute(tensorflow::OpKernelContext* ctx) {
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);

  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* rvec = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("rvec", &rvec));

  // Sanity check
  LTS5::ShapeUtils::CheckShape(ctx,
                               *rvec,
                               3,
                               tf::DataTypeToEnum<T>::v(),
                               {-1, 1, 3});
  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* rmat;
  OP_REQUIRES_OK(ctx, ctx->allocate_output("rotation_matrix",
                                           {rvec->dim_size(0), 3, 3},
                                           &rmat));
  // build rotation matrices
  int err = LTS5::Functor::RodriguesHelper<Device, T>()(ctx, rvec, rmat);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in forward kernel"));
  }
}


#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;

REGISTER_OP("RodriguesOp")
        .Input("rvec: T")
        .Output("rotation_matrix: T")
        .Attr("T: {float}")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          std::vector<ShapeHandle> rvec_shp;
          auto s = ctx->input("rvec", &rvec_shp);
          auto shp = ctx->MakeShape({ctx->Dim(rvec_shp[0], 0),
                                     3,
                                     3});
          s = ctx->set_output("rotation_matrix", {shp});
          return s;
        })
        .Doc(R"doc(Compute rotation matrix)doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("RodriguesOp")                       \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      RodriguesKernel<CPUDevice, Type>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(Type)                                                 \
  REGISTER_KERNEL_BUILDER(                                                 \
      Name("RodriguesOp")                                                  \
      .Device(tf::DEVICE_GPU)                                              \
      .TypeConstraint<Type>("T"),                                          \
      RodriguesKernel<GPUDevice, Type>);
REGISTER_GPU(float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA
