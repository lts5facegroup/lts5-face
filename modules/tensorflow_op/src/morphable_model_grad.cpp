/**
 *  @file   morphable_model_grad.cpp
 *  @brief  Morphable Model wrapper for TensorFlow (Gradient)
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/4/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "lts5/tensorflow_op/morphable_model_grad.hpp"
#include "lts5/tensorflow_op/mm_wrapper.hpp"
#include "lts5/tensorflow_op/utils/shape.hpp"

namespace tf = tensorflow;

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @struct  TextureGradientHelper
 * @brief   Functor helping at the computation of the operation grad
 * @author  Christophe Ecabert
 * @date    04/07/19
 * @ingroup tensorflow_op
 * @tparam T        Data type
 */
template<typename T>
struct TextureGradientHelper<CPUDevice, T> {
  /**
   * @name
   * @brief Compute derivate of the color with respect to the texture parameters
   * @param[in,out] ctx Op's context
   * @param[in] w_tex   Texture's parameters
   * @param[in] g_color Backpropagated gradient
   * @param[in] grad    Newly computed gradient include appearance derivative
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* w_tex,
                 const tf::Tensor* g_color,
                 tf::Tensor* grad) {
    return -1;
  }
};

/**
 * @struct  ShapeGradientHelper
 * @brief   Functor helping at the computation of the operation grad
 * @author  Christophe Ecabert
 * @date    04/07/19
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct ShapeGradientHelper<CPUDevice, T> {
  /*
   * @name  operator()
   * @brief Compute derivate of the shape with respect to the shape parameters
   * @param[in,out] ctx Op's context
   * @param[in] w_shp       Shape's parameters
   * @param[in] g_vertex    Back-propagated gradient
   * @param[in] grad        Newly computed gradient include shape derivative
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* w_shp,
                 const tf::Tensor* g_vertex,
                 tf::Tensor* grad) {
    return -1;
  }
};

}  // namespace Functor
}  // namespace LTS5

#pragma mark -
#pragma mark Sanity Check

/*
 * @name    ValidateInputParameters
 * @brief Check if input parameters are valid (rank, n_param, type)
 * @param[in,out] ctx   Op's context
 * @param[in] t         Tensor to check
 * @param[in] dims      Required number of parameters (dim==1)
 * @tparam T Data type
 */
template<typename T>
static void ValidateInputParameters(tf::OpKernelContext* ctx,
                                    const tf::Tensor* t,
                                    const tf::int64& dims) {
  const auto& shp = t->shape();
  OP_REQUIRES(ctx,
              tf::TensorShapeUtils::IsMatrix(shp),
              tf::errors::InvalidArgument("rank(T) != 2"));
  OP_REQUIRES(ctx,
              shp.dim_size(1) == dims,
              tf::errors::InvalidArgument("Wrong number of parameters: ",
                                          dims,
                                          "!=",
                                          shp.dim_size(1)));
  OP_REQUIRES(ctx,
              t->dtype() == tf::DataTypeToEnum<T>::value,
              tf::errors::InvalidArgument("Parameter's type is not supported"));
}

#pragma mark -
#pragma mark Op implementation

/*
 * @name  MorphableModelGradKernel
 * @fn    explicit MorphableModelGradKernel(tf::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
MorphableModelGradKernel<Device,T>::
MorphableModelGradKernel(tensorflow::OpKernelConstruction* ctx) :
        OpKernel(ctx),
        model_path_("") {
  // Get model path
  // -----------------------------------
  OP_REQUIRES_OK(ctx, ctx->GetAttr("path", &model_path_));
  OP_REQUIRES(ctx,
              !model_path_.empty(),
              tf::errors::InvalidArgument("Specify a valid model path"));
}

#pragma mark -
#pragma mark Usage

/**
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void MorphableModelGradKernel<Device,T>::Compute(tf::OpKernelContext* ctx) {
  /** Model type */
  using Model = LTS5::SharedMorphableModelContainerV2<T>;
  /** TextureGrad */
  using TextureGrad = LTS5::Functor::TextureGradientHelper<Device, T>;
  /** ShapeGrad */
  using ShapeGrad = LTS5::Functor::ShapeGradientHelper<Device, T>;

  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);

  // Shared morphable model
  // -------------------------------------------------------
  auto creator = [&](Model** model) ->tf::Status {
    *model = new Model();
    // Load model
    int err = (*model)->Load(model_path_);
    if (err) {
      return tf::Status(tf::error::Code::INVALID_ARGUMENT,
                        "Can not load model: " + model_path_);
    }
    return tf::Status::OK();
  };

  Model* model = nullptr;
  auto* rm = ctx->resource_manager();
  auto s = rm->LookupOrCreate<Model>(rm->default_container(),
                                     "SharedMorphableModel",
                                     &model,
                                     creator);
  ctx->SetStatus(s);
  tf::core::ScopedUnref scoped_ref(model);  // Automatic ref handling
  if (s.ok()) {
    // INPUTS
    // -------------------------------------------------------
    const tf::Tensor* g_vertex;
    const tf::Tensor* g_color;
    const tf::Tensor* w_shp;
    const tf::Tensor* w_tex;
    const tf::Tensor* vertex;
    const tf::Tensor* color;
    const tf::Tensor* triangle;
    ctx->input("g_vertex", &g_vertex);
    ctx->input("g_color", &g_color);
    ctx->input("w_shp", &w_shp);
    ctx->input("w_tex", &w_tex);
    ctx->input("vertex", &vertex);
    ctx->input("color", &color);
    ctx->input("triangle", &triangle);

    // Sanity check inputs
    ValidateInputParameters<T>(ctx, w_shp, model->get_n_shape_parameter());
    ValidateInputParameters<T>(ctx, w_tex, model->get_n_tex_parameter());
    ValidateInputParameters<T>(ctx, vertex, model->get_n_vertex() * 3);
    ValidateInputParameters<T>(ctx, color, model->get_n_texel() * 3);
    LTS5::ShapeUtils::CheckShape(ctx,
                                 *triangle,
                                 2,
                                 tf::DataTypeToEnum<int>::v(),
                                 {(tf::int64)model->get_n_triangle(), 3});
    // Sanity check gradients
    ValidateInputParameters<T>(ctx, g_vertex, model->get_n_vertex() * 3);
    ValidateInputParameters<T>(ctx, g_color, model->get_n_texel() * 3);


    // OUTPUTS
    // -------------------------------------------------------
    tf::Tensor* grad_w_shp;
    tf::Tensor* grad_w_tex;
    auto bsize = w_shp->dim_size(0);
    auto n_shp_param = static_cast<tf::int64>(model->get_n_shape_parameter());
    auto n_tex_param = static_cast<tf::int64>(model->get_n_tex_parameter());
    OP_REQUIRES_OK(ctx, ctx->allocate_output("grad_w_shp",
                                             {bsize, n_shp_param},
                                             &grad_w_shp));
    OP_REQUIRES_OK(ctx, ctx->allocate_output("grad_w_tex",
                                             {bsize, n_tex_param},
                                             &grad_w_tex));
    // COMPUTE
    // -------------------------------------------------------
    model->template Upload<Device>(ctx);

    // Compute grad here
    int err = ShapeGrad()(ctx, w_shp, g_vertex, grad_w_shp); // Shape grad
    err |= TextureGrad()(ctx, w_tex, g_color, grad_w_tex); // Appearance grad
    if (err) {
      ctx->CtxFailureWithWarning(__FILE__,
                                 __LINE__,
                                 tf::errors::Internal("Failure in grad kernels"));
    }
  }
}



#pragma mark -
#pragma mark Registration With Tensorflow

REGISTER_OP("MorphableModelOpGrad")
        .Input("g_vertex: T")
        .Input("g_color: T")
        .Input("w_shp: T")
        .Input("w_tex: T")
        .Input("vertex: T")
        .Input("color: T")
        .Input("triangle: int32")
        .Output("grad_w_shp: T")
        .Output("grad_w_tex: T")
        .Attr("T: {float}")
        .Attr("path: string")
        .SetIsStateful()
        .Doc(R"doc(MorphableModelOp's gradient)doc");

#define REGISTER_CPU(T)                                  \
REGISTER_KERNEL_BUILDER(                                 \
      Name("MorphableModelOpGrad")                       \
      .Device(tf::DEVICE_CPU).TypeConstraint<T>("T"),    \
      MorphableModelGradKernel<CPUDevice, T>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(T)                                   \
  REGISTER_KERNEL_BUILDER(                                \
      Name("MorphableModelOpGrad")                        \
      .Device(tf::DEVICE_GPU).TypeConstraint<T>("T"),     \
      MorphableModelGradKernel<GPUDevice, T>);
REGISTER_GPU(float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA