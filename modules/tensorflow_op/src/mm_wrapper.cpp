/**
 *  @file   mm_wrapper.cpp
 *  @brief  Morphable model abstraction
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   03/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <fstream>
#include <string>
#include <vector>

#include "lts5/tensorflow_op/mm_wrapper.hpp"
#include "lts5/tensorflow_op/file_io.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/geometry/mesh.hpp"

#include "lts5/tensorflow_op/tensor_utils.hpp"

#ifdef GOOGLE_CUDA
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
using GPUDevice = Eigen::GpuDevice;
#endif

using CPUDevice = Eigen::ThreadPoolDevice;
namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  MorphableModelContainer
 * @fn    MorphableModelContainer()
 * @brief Constructor
 */
template<typename T>
MorphableModelContainer<T>::MorphableModelContainer() :
        n_color_trsrm_parameter_(7) {
}

/*
 * @name  Load
 * @fn    int Load(const std::string& filename)
 * @brief Load a model into memory
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MorphableModelContainer<T>::Load(const std::string &filename) {
  int err = -1;
  std::ifstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Load(stream);
    stream.close();
  }
  return err;
}

/*
 * @name  Load
 * @fn    int Load(std::istream& stream)
 * @brief Load a model into memory
 * @param[in] stream    Binary stream from which to load the model
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MorphableModelContainer<T>::Load(std::istream& stream) {
  using ObjType = HeaderObjectType;
  int err = -1;
  if (stream.good()) {
    // Search for color model
    int s0 = ScanStreamForObject(stream, ObjType::kColorMorphableModel);
    int s1 = ScanStreamForObject(stream, ObjType::kMorphableModel);
    if (s0 == 0 && s1 == 0) {
      // Load a ColorMorphableModel instance
      // Read shape/texture type
      int type = -1;
      stream.read(reinterpret_cast<char*>(&type), sizeof(type));
      stream.read(reinterpret_cast<char*>(&type), sizeof(type));
      // Load shape model
      err = this->LoadShapeModel(stream);
      // Load texture model
      err |= this->LoadTextureModel(stream);
      // Load per-vertex standard deviation if any
      err |= this->LoadStandardDeviationModel(stream);
      // Sanity check
      err |= stream.good() ? 0 : -1;
    }
    // Build connectivity
    this->BuildConnectivity();
  }
  return err;
}


#pragma mark -
#pragma mark Private

/*
 * @name  LoadShapeModel
 * @fn    int LoadShapeModel(std::istream& stream)
 * @brief Load shape model
 * @param[in] stream  Binary stream from which model need to be loaded
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int MorphableModelContainer<T>::LoadShapeModel(std::istream& stream) {
  using Mesh_t = Mesh<T, Vector3<T>>;
  using Vertex = typename Mesh_t::Vertex;
  int err = -1;
  if (stream.good()) {
    int s0 = ScanStreamForObject(stream, HeaderObjectType::kMultiShapeModel);
    int s1 = ScanStreamForObject(stream, HeaderObjectType::kPCAModel);
    if (s0 == 0 && s1 == 0) {
      // Mean + Var + Prior
      // ----------------------------------------------------
      EMatrix var, prior;
      err = IO<T>::ReadTensor(stream, &meanshape_buffer_);
      err |= IO<T>::ReadTensor(stream, &var);
      err |= IO<T>::ReadTensor(stream, &prior);
      shape_basis_buffer_ = var * prior.asDiagonal();
      n_shape_parameter_ = size_t(shape_basis_buffer_.cols());
      // Channels (Not used)
      // ----------------------------------------------------
      int dummy;
      stream.read(reinterpret_cast<char*>(&dummy), sizeof(dummy));
      // Triangles
      // ----------------------------------------------------
      stream.read(reinterpret_cast<char*>(&n_tri_), sizeof(n_tri_));
      if (n_tri_ > 0) {
        tri_.resize(n_tri_);
        stream.read(reinterpret_cast<char*>(tri_.data()),
                    n_tri_ * sizeof(Vector3<int>));
      }
      // Init var
      // ----------------------------------------------------
      n_shape_channel_ = size_t(dummy);
      n_vertex_ = size_t(meanshape_buffer_.rows()) / n_shape_channel_;

      // Landmarks (Not used)
      // ----------------------------------------------------
      stream.read(reinterpret_cast<char*>(&dummy), sizeof(dummy));
      std::vector<int> landmarks(static_cast<size_t>(dummy), 0);
      stream.read(reinterpret_cast<char*>(landmarks.data()),
                  dummy * sizeof(landmarks[0]));
      // Shape dimensions
      // ----------------------------------------------------
      int ndim = 0;
      stream.read(reinterpret_cast<char*>(&ndim), sizeof(ndim));
      this->dim_.resize(size_t(ndim));
      stream.read(reinterpret_cast<char*>(dim_.data()), ndim * sizeof(dim_[0]));
      // Cog
      // ----------------------------------------------------
      int n = 0;
      stream.read(reinterpret_cast<char*>(&n), sizeof(n));
      this->cog_.resize(n);
      stream.read(reinterpret_cast<char*>(this->cog_.data()), n * sizeof(T));
      // Edges
      // ----------------------------------------------------
      auto* v_ptr = reinterpret_cast<const Vertex*>(meanshape_buffer_.data());
      auto v = std::vector<Vertex>(v_ptr, v_ptr + n_vertex_);
      Mesh_t m;
      m.set_vertex(v);
      m.set_triangle(tri_);
      m.ComputeEdgeProperties(&vpe_, &fpe_);
      // Sanity check
      err |= stream.good() ? 0 : -1;
    }
  }
  return err;
}

/*
 * @name  LoadTextureModel
 * @fn    int LoadTextureModel(std::istream& stream)
 * @brief Load texture model
 * @param[in] stream  Binary stream from which model need to be loaded
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int MorphableModelContainer<T>::LoadTextureModel(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    int s0 = ScanStreamForObject(stream, HeaderObjectType::kColorModel);
    int s1 = ScanStreamForObject(stream, HeaderObjectType::kPCAModel);
    if (s0 == 0 && s1 == 0) {
      EMatrix var, prior;
      err = IO<T>::ReadTensor(stream, &meantexture_buffer_);
      err |= IO<T>::ReadTensor(stream, &var);
      err |= IO<T>::ReadTensor(stream, &prior);
      tex_basis_buffer_ = var * prior.asDiagonal();
      n_tex_parameter_ = size_t(tex_basis_buffer_.cols());
      // Channels
      // ----------------------------------------------------
      int dummy;
      stream.read(reinterpret_cast<char*>(&dummy), sizeof(dummy));
      n_tex_channel_ = static_cast<size_t>(dummy);
      n_texel_ = size_t(meantexture_buffer_.rows()) / n_tex_channel_;
      // Sanity check
      err |= stream.good() ? 0 : -1;
    }
  }
  return err;
}

/*
 * @name  LoadStandardDeviationModel
 * @fn    int LoadStandardDeviationModel(std::istream& stream)
 * @brief Load per-vertex variance model if any present. Assume model is not
 *        present if stream reach the end (stream.good() == false).
 * @param[in] stream  Binary stream from which model need to be loaded
 * @return -1 if error, 0 otherwise. If model is not present no error will be
 *         emitted
 */
template<typename T>
int MorphableModelContainer<T>::LoadStandardDeviationModel(std::istream &stream) {
  int err = 0;
  if (stream.good()) {
    // reach the end of file ?
    if (stream.peek() != std::istream::traits_type::eof()) {
      err |= IO<T>::ReadTensor(stream, &tex_std_buffer_);
      err |= stream.good() ? 0 : -1;
    } else {
      // Peek raised eofbit therefore reset it in order to not perturb stream
      // check of the caller. This mehtod is allowed to fail at the moment.
      stream.clear();
    }
  }
  return err;
}

/*
 * @name  BuildConnectivity
 * @fn    void BuildConnectivity()
 * @brief Build connectivity map
 */
template<typename T>
void MorphableModelContainer<T>::BuildConnectivity() {
  // Type
  using VecI = std::vector<int>;
  // Compute connectivity
  if (n_vertex_ != conn_.rows()) {
    std::vector<VecI> conn(n_vertex_, VecI(0));
    size_t max_conn = 0;
    for (const auto& tri : tri_) {
      const int* tidx = &(tri.x_);
      // Loop over all edges
      for (size_t e = 0; e < 3; ++e) {
        int mid = tidx[e];
        int e1 = tidx[(e + 1) % 3];
        int e2 = tidx[(e + 2) % 3];
        conn[mid].push_back(e1);
        conn[mid].push_back(e2);
        size_t sz = conn[mid].size();
        // Store maximum vertex degree
        if (sz > max_conn) {
          max_conn = sz;
        }
      }
    }
    // Store it into Eigen matrix
    conn_.resize(n_vertex_, max_conn + 1);
    conn_.setZero();
    for (size_t v = 0; v < n_vertex_; ++v) {
      // Fill connectivity for each vertex
      const auto& edges = conn[v];
      const size_t sz = edges.size();
      conn_(v, 0) = static_cast<int>(sz);
      // Add element
      for (size_t i = 0; i < sz; ++i) {
        conn_(v, i + 1) = edges[i];
      }
    }
  }
}

#pragma mark -
#pragma mark MorphableModelContainerV2

/*
 * @name  Upload
 * @fn    void Upload(tensorflow::OpKernelConstruction* ctx)
 * @brief Push data to device
 * @param[in,out] ctx
 */
template<typename T>
template<typename Device>
void MorphableModelContainerV2<T>::Upload(Context* ctx) {
  using TensorMap = typename tf::TTypes<T>::ConstTensor;
  using TensorMapInt = typename tf::TTypes<int>::ConstTensor;

  const auto &device = ctx->eigen_device<Device>();
  Functor::DenseOp<Device, T, Functor::DenseOpType::kH2DCopy> copy;
  Functor::DenseOp<Device, int, Functor::DenseOpType::kH2DCopy> copy_int;

  // Infos
  // ------------------------------------------------------------------
  auto vertex_dim = static_cast<tf::int64>(this->get_n_vertex()) * 3;
  auto n_texel = static_cast<tf::int64>(this->get_n_texel());
  auto texel_dim = static_cast<tf::int64>(this->get_n_channels()) * n_texel;
  auto nv = static_cast<tf::int64>(this->get_n_shape_parameter());
  auto nt = static_cast<tf::int64>(this->get_n_tex_parameter());
  auto n_tri = static_cast<tf::int64>(this->get_n_triangle());
  auto n_edge = static_cast<tf::int64>(this->get_n_edge());
  tf::AllocatorAttributes attr;
  attr.set_gpu_compatible(true);

  // Instantiate if needed
  // ------------------------------------------------------------------
  tf::Tensor* buffer;
  auto s = tf::Status::OK();
  // Mean shape
  // ------------------------------------------------------------------
  if (!this->d_mean_shape_.NumElements()) {
    s = ctx->allocate_persistent(tf::DataTypeToEnum<T>::value,
                                 {vertex_dim, 1},
                                 &this->d_mean_shape_,
                                 &buffer,
                                 attr);
    if (s.ok()) {
      // Fill data - meanshape
      const auto &ms = this->get_meanshape_buffer();
      auto map = TensorMap(ms.data(), ms.rows() * ms.cols());
      copy(device, map, buffer->flat<T>());
    }
  }
  // Mean texture
  // ------------------------------------------------------------------
  if (!this->d_mean_tex_.NumElements()) {
    s = ctx->allocate_persistent(tf::DataTypeToEnum<T>::value,
                                 {texel_dim, 1},
                                 &this->d_mean_tex_,
                                 &buffer,
                                 attr);
    if (s.ok()) {
      // Fill data - meanshape
      const auto &mt = this->get_meantexture_buffer();
      auto map = TensorMap(mt.data(), mt.rows() * mt.cols());
      copy(device, map, buffer->flat<T>());
    }
  }
  // Var shape
  // ------------------------------------------------------------------
  if (!this->d_var_shape_.NumElements()) {
    s = ctx->allocate_persistent(tf::DataTypeToEnum<T>::value,
                                 {vertex_dim, nv},
                                 &this->d_var_shape_,
                                 &buffer,
                                 attr);
    if (s.ok()) {
      // Fill data - meanshape
      const auto &vars = this->get_shape_variation_buffer();
      auto map = TensorMap(vars.data(), vars.rows() * vars.cols());
      copy(device, map, buffer->flat<T>());
    }
  }
  // Var tex
  // ------------------------------------------------------------------
  if (!this->d_var_tex_.NumElements()) {
    s = ctx->allocate_persistent(tf::DataTypeToEnum<T>::value,
                                 {texel_dim, nt},
                                 &this->d_var_tex_,
                                 &buffer,
                                 attr);
    if (s.ok()) {
      // Fill data - meanshape
      const auto &vart = this->get_texture_variation_buffer();
      auto map = TensorMap(vart.data(), vart.rows() * vart.cols());
      copy(device, map, buffer->flat<T>());
    }
  }
  // Tri
  // ------------------------------------------------------------------
  if (!this->d_tri_.NumElements()) {
    s = ctx->allocate_persistent(tf::DataTypeToEnum<int>::value,
                                 {n_tri, 3},
                                 &this->d_tri_,
                                 &buffer,
                                 attr);
    if (s.ok()) {
      // Fill data - meanshape
      const auto &tri = MorphableModelContainer<T>::get_triangle();
      auto map = TensorMapInt(reinterpret_cast<const int *>(tri.data()),
                              tri.size() * 3);
      copy_int(device, map, buffer->flat<int>());
    }
  }
  // Edge-to-face Map (FPE)
  // ------------------------------------------------------------------
  if (!this->d_fpe_.NumElements()) {
    s = ctx->allocate_persistent(tf::DataTypeToEnum<int>::value,
                                 {n_edge, 2},
                                 &this->d_fpe_,
                                 &buffer,
                                 attr);
    if (s.ok()) {
      // Fill data - edge list
      const auto &fpe = this->get_face_per_edge();
      auto map = TensorMapInt(reinterpret_cast<const int*>(fpe.data()),
                              fpe.size() * 2);
      copy_int(device, map, buffer->flat<int>());
    }
  }
  // Edge-to-vertex Map (VPE)
  // ------------------------------------------------------------------
  if (!this->d_vpe_.NumElements()) {
    s = ctx->allocate_persistent(tf::DataTypeToEnum<int>::value,
                                 {n_edge, 2},
                                 &this->d_vpe_,
                                 &buffer,
                                 attr);
    if (s.ok()) {
      // Fill data - edge list
      const auto &vpe = this->get_vertex_per_edge();
      auto map = TensorMapInt(reinterpret_cast<const int*>(vpe.data()),
                              vpe.size() * 2);
      copy_int(device, map, buffer->flat<int>());
    }
  }
  // Connectivity
  // ------------------------------------------------------------------
  if (!this->d_conn_.NumElements()) {
    const auto& conn = this->get_connectivity_buffer();
    s = ctx->allocate_persistent(tf::DataTypeToEnum<int>::value,
                                 {conn.rows(), conn.cols()},
                                 &this->d_conn_,
                                 &buffer,
                                 attr);
    if (s.ok()) {
      // Fill data - var texture
      auto map = TensorMapInt(conn.data(), conn.rows() * conn.cols());
      copy_int(device, map, buffer->flat<int>());
    }
  }
  // set status
  if (!s.ok()) {
    ctx->CtxFailureWithWarning(__FILE__, __LINE__, s);
  }
}


#pragma mark -
#pragma mark Explicit Instantiation

/** Float */
template class MorphableModelContainer<float>;
template class MorphableModelContainerV2<float>;
template void MorphableModelContainerV2<float>::Upload<CPUDevice>(Context*);
template class SharedMorphableModelContainerV2<float>;
/** Float */
template class MorphableModelContainer<double>;
template class MorphableModelContainerV2<double>;
template void MorphableModelContainerV2<double>::Upload<CPUDevice>(Context*);
template class SharedMorphableModelContainerV2<double>;

#ifdef GOOGLE_CUDA
template void MorphableModelContainerV2<float>::Upload<GPUDevice>(Context*);
template void MorphableModelContainerV2<double>::Upload<GPUDevice>(Context*);
#endif

}  // namespace LTS5
