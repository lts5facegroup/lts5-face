/**
 *  @file   rasterizer.cpp
 *  @brief  OpenGL bassed rasterizer
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/14/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <string>

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/tensorflow_op/rasterizer.hpp"

#ifdef GOOGLE_CUDA
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
#endif

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

// Vertex shader
static const char* v_str =  "#version 330\n"
                            "layout (location = 0) in vec3 position;\n"
                            "uniform mat4 camera;\n"
                            "void main() {\n"
                            " gl_Position = camera * vec4(position, 1);\n"
                            "}";
// Vertex shader with input position directly from clip space (i.e. already
// multiplied by projection matrix)
static const char* v_clip_str =  "#version 330\n"
                                 "layout (location = 0) in vec4 position;\n"
                                 "void main() {\n"
                                 " gl_Position = position;\n"
                                 "}";
// Geometry shader
static const char* g_str = "#version 330\n"
                           "layout (triangles) in;\n"
                           "layout (triangle_strip, max_vertices = 3) out;\n"
                           "in int gl_PrimitiveIDIn;\n"
                           "out vec3 bcoords;\n"
                           "out float tri_idx;\n"
                           "void main() {\n"
                           "  for (int i = 0; i < 3; ++i) {\n"
                           "    gl_Position = gl_in[i].gl_Position;\n"
                           "    bcoords = vec3(i==0 ? 1.f : 0.f,\n"
                           "                   i==1 ? 1.f : 0.f,\n"
                           "                   i==2 ? 1.f : 0.f);\n"
                           "    tri_idx = gl_PrimitiveIDIn;\n"
                           "    EmitVertex();\n"
                           "  }\n"
                           "  EndPrimitive();\n"
                           "}";
// Fragment shader
static const char* f_str = "#version 330\n"
                           "in vec3 bcoords;\n"
                           "in float tri_idx;\n"
                           "out vec4 fragColor;\n"
                           "void main() {\n"
                           "  fragColor = vec4(round(tri_idx + 1.0), bcoords.xyz);\n"
                           "}";

#pragma mark -
#pragma mark Initialization

/*
 * @name  Rasterizer
 * @fn    Rasterizer()
 * @brief Constructor
 */
template<typename Device, typename T>
Rasterizer<Device, T>::Rasterizer() : v_map_(new OGLMapper()),
                                      f_map_(new OGLFaceMapper()),
                                      dl_img_(new DLImage()) {
}

/*
 * @name  ~Rasterizer
 * @fn    ~Rasterizer();
 * @brief Destructor
 */
template<typename Device, typename T>
Rasterizer<Device, T>::~Rasterizer() {
  renderer_.ActiveContext();
  delete v_map_;
  delete f_map_;
  delete dl_img_;
  renderer_.DisableContext();
}

/*
 * @name  CreateContext
 * @fn    int CreateContext(const Parameters& params)
 * @brief Create opengl context and build shading program
 * @param[in] params  Rasterizer parameters. NOTE: If params.focal is negative
 *    assume vertex are already in clip space and therefore does not apply
 *    projection matrix
 * @return    -1 if error, 0 otherwise
 */
template<typename Device, typename T>
int Rasterizer<Device, T>::CreateContext(const Parameters& params) {
  using ImgType = typename Renderer::ImageType;
  using ShaderType = typename OGLShader::ShaderType;
  using AttTyp = OGLProgram::AttributeType;
  using Mat4 = Matrix4x4<T>;
  // Init renderer
  int err = renderer_.Init(params.width,
                           params.height,
                           0,
                           params.visible,
                           ImgType::kRGBAf);
  dl_img_->Init(&renderer_);
  // Initialize shader
  renderer_.ActiveContext();
  if (params.focal > T(0.0)) {
    shader_.AddShader(OGLShader(v_str,
                                ShaderType::kVertex),
                      {{AttTyp::kPoint,
                               0,
                               "position"}});
  } else {
    shader_.AddShader(OGLShader(v_clip_str,
                                ShaderType::kVertex),
                      {{AttTyp::kPoint,
                               0,
                               "position"}});
  }

  shader_.AddShader(OGLShader(g_str,
                              ShaderType::kGeometry));
  shader_.AddShader(OGLShader(f_str,
                              ShaderType::kFragment));
  shader_.Build();
  // Add shader to renderer
  renderer_.set_program(&shader_);
  if (params.focal > T(0.0)) {
    // Add projection matrix since it has to be initialized once.
    // http://kgeorge.github.io/2014/03/08/calculating-opengl-perspective-matrix-from-opencv-intrinsic-matrix
    Mat4 P;  // Projection matrix
    T cx = static_cast<T>(params.width) / T(2.0);
    T cy = static_cast<T>(params.height) / T(2.0);
    P(0, 0) = params.focal / cx;
    P(1, 1) = params.focal / cy;
    P(2, 2) = -(params.far + params.near) / (params.far - params.near);
    P(2, 3) = -(T(2.0) * params.far * params.near) / (params.far - params.near);
    P(3, 2) = T(-1.0);
    // Push to shader
    shader_.Use();
    shader_.SetUniform("camera", P);
    shader_.StopUsing();
  }
  // Done
  renderer_.DisableContext();
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Render
 * @fn    int Render(tf::OpKernelContext* ctx, const tf::Tensor& vertex,
 *                    tf::Tensor* image)
 * @brief Render the vertex into a given tensor
 * @param[in] ctx Op's context
 * @param[in] vertex  Vertex to render, batched
 * @param[in] triangle  Surface triangulation
 * @param[out] image Tensor where to place the data, must be allocated
 *                    correctly beforehand
 * @return -1 if error, 0 otherwise
 */
template<typename Device, typename T>
int Rasterizer<Device, T>::Render(tf::OpKernelContext* ctx,
                                  const tf::Tensor& vertex,
                                  const tf::Tensor& triangle,
                                  tf::Tensor* image) {
  renderer_.ActiveContext();
  // Create opengl buffer of proper dimensions
  int err = this->ConfigureOpenGlBuffer(ctx, vertex, triangle);
  if (!err) {
    // Goes over every slices
    tf::int64 bsize = vertex.dim_size(0);
    for (tf::int64 k = 0; k < bsize; ++k) {
      auto v_slice = vertex.Slice(k, k + 1);
      auto im_slice = image->Slice(k, k + 1);
      // Upload vertex / triangle
      // -------------------------------------------------------------------
      err |= Functor::UploadAttribute<Device, T>()(ctx, v_slice, *v_map_);
      // Render
      // -------------------------------------------------------------------
      renderer_.Render();
      // Download
      // -------------------------------------------------------------------
      err |= dl_img_->operator()(ctx, &im_slice);
    }
  }
  renderer_.DisableContext();
  return err;
}

#pragma mark -
#pragma mark Private

template<typename Device, typename T>
int Rasterizer<Device, T>::ConfigureOpenGlBuffer(tf::OpKernelContext* ctx,
                                                 const tf::Tensor& vertex,
                                                 const tf::Tensor& triangle) {
  using BufferIdx = typename OGLBaseRenderer<T, Vector3<T>>::BufferIdx;

  int err = 0;
  // Setup VAO for given input
  // Define storage space required for the data
  auto n_vertex = vertex.dim_size(1);   // vertex == [Batch, N, 3/4]
  auto vertex_sz = vertex.dim_size(2);
  auto n_tri = triangle.dim_size(0);    // triangle == [T, 3]
  BufferInfo info;
  info.dim_vertex_ = vertex_sz * n_vertex * sizeof(T);
  info.size_vertex_ = vertex_sz;
  info.n_elements_ = n_tri * 3;
  // Does it matched current setup ?
  if (info.dim_vertex_ != ogl_info_.dim_vertex_ ||
      info.n_elements_ != ogl_info_.n_elements_) {
    // Create OpenGL buffer + Initialize mapper
    err = renderer_.BindToOpenGL(info);
    v_map_->Init(BufferIdx::kVertex, &renderer_);
    f_map_->Init(BufferIdx::kTriangle, &renderer_);
    // Copy triangle into Buffer at same time
    err |= Functor::UploadTriangle<Device, T>()(ctx, triangle, *f_map_);
    // Update buffer info
    ogl_info_ = info;
  }
  return err;
}

#pragma mark -
#pragma mark Explicit Instantiation

/** Float - CPU */
template class Rasterizer<CPUDevice, float>;
/** Double - CPU */
template class Rasterizer<CPUDevice, double>;

#ifdef GOOGLE_CUDA
/** Float - GPU */
template class Rasterizer<GPUDevice, float>;
/** Double - GPU */
template class Rasterizer<GPUDevice, double>;
#endif
}  // namespace LTS5