/**
 *  @file   morphable_model.cpp
 *  @brief  Morphable Model wrapper for TensorFlow
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/2/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/lib/core/errors.h"

#include "lts5/tensorflow_op/morphable_model.hpp"
#include "lts5/tensorflow_op/mm_wrapper.hpp"

#ifdef GOOGLE_CUDA
// Include cuda functor here
#endif

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Op Definition

namespace tf = tensorflow;

/**
 * @name    ValidateInputParameters
 * @brief Check if input parameters are valid (rank, n_param, type)
 * @param[in,out] ctx   Op's context
 * @param[in] t         Tensor to check
 * @param[in] dims      Required number of parameters (dim==1)
 * @tparam T Data type
 */
template<typename T>
static void ValidateInputParameters(tf::OpKernelContext* ctx,
                                    const tf::Tensor* t,
                                    const tf::int64& dims) {
  const auto& shp = t->shape();
  OP_REQUIRES(ctx,
              tf::TensorShapeUtils::IsMatrix(shp),
              tf::errors::InvalidArgument("rank(T) != 2"));
  OP_REQUIRES(ctx,
              shp.dim_size(1) == dims,
              tf::errors::InvalidArgument("Wrong number of parameters: ",
                                          dims,
                                          "!=",
                                          shp.dim_size(1)));
  OP_REQUIRES(ctx,
              t->dtype() == tf::DataTypeToEnum<T>::value,
              tf::errors::InvalidArgument("Parameter's type is not supported"));
}


/*
 * @name  MorphableModelKernel
 * @fn    explicit MorphableModelKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
MorphableModelKernel<Device, T>::
MorphableModelKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx),
                                                      model_path_(""),
                                                      gen_(nullptr) {
  // Get model path
  // -----------------------------------
  OP_REQUIRES_OK(ctx, ctx->GetAttr("path", &model_path_));
  OP_REQUIRES(ctx,
              !model_path_.empty(),
              tf::errors::InvalidArgument("Specify a valid model path"));
  // Create Generator
  // -----------------------------------
  gen_ = new LTS5::MMGenerator<Device, T>();
}

/*
 * @name  ~MMDecoderOp
 * @fn    ~MMDecoderOp() override
 * @brief Destructor
 */
template<typename Device, typename T>
MorphableModelKernel<Device, T>::~MorphableModelKernel() {
  delete gen_;
}

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void MorphableModelKernel<Device, T>::Compute(tf::OpKernelContext* ctx) {

  /** Model type */
  using Model = LTS5::SharedMorphableModelContainerV2<T>;
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);


  // Shared morphable model
  // -------------------------------------------------------
  auto creator = [&](Model** model) ->tf::Status {
    *model = new Model();
    // Load model
    int err = (*model)->Load(model_path_);
    if (err) {
      return tf::Status(tf::error::Code::INVALID_ARGUMENT,
                        "Can not load model: " + model_path_);
    }
    return tf::Status::OK();
  };

  Model* model = nullptr;
  auto* rm = ctx->resource_manager();
  auto s = rm->LookupOrCreate<Model>(rm->default_container(),
                                     "SharedMorphableModel",
                                     &model,
                                     creator);
  ctx->SetStatus(s);
  tf::core::ScopedUnref scoped_ref(model);  // Automatic ref handling
  if (s.ok()) {
    // INPUTS
    // -------------------------------------------------------
    const tf::Tensor* w_shp;
    const tf::Tensor* w_tex;
    OP_REQUIRES_OK(ctx, ctx->input("w_shp", &w_shp));
    OP_REQUIRES_OK(ctx, ctx->input("w_tex", &w_tex));

    // Sanity check dims
    auto bsize = w_shp->dim_size(0);
    ValidateInputParameters<T>(ctx, w_shp, model->get_n_shape_parameter());
    ValidateInputParameters<T>(ctx, w_tex, model->get_n_tex_parameter());
    OP_REQUIRES(ctx,
                bsize == w_tex->dim_size(0),
                tf::errors::InvalidArgument("Parameter set inconsistent: ",
                                            bsize,
                                            "!=",
                                            w_tex->dim_size(0)));
    // OUTPUTS
    // -------------------------------------------------------
    tf::Tensor* vertex;
    tf::Tensor* color;
    tf::Tensor* triangle;

    auto n_vertex = static_cast<tf::int64>(model->get_n_vertex());
    auto n_texel = static_cast<tf::int64>(model->get_n_texel());
    auto n_tri = static_cast<tf::int64>(model->get_n_triangle());
    OP_REQUIRES_OK(ctx, ctx->allocate_output("vertex",
                                             {bsize, 3 * n_vertex},
                                             &vertex));
    OP_REQUIRES_OK(ctx, ctx->allocate_output("color",
                                             {bsize, 3 * n_texel},
                                             &color));
    OP_REQUIRES_OK(ctx, ctx->allocate_output("triangle",
                                             {n_tri, 3},
                                             &triangle));
    // COMPUTE
    // -------------------------------------------------------
    model->template Upload<Device>(ctx);
    gen_->Generate(ctx, w_shp, w_tex, model, vertex, color, triangle);
  }
}

#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;
REGISTER_OP("MorphableModelOp")
        .Input("w_shp: T")
        .Input("w_tex: T")
        .Output("vertex: T")
        .Output("color: T")
        .Output("triangle: int32")
        .Attr("T: {float}")
        .Attr("path: string")
        .SetIsStateful()
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          // Query attributes
          std::string path;
          auto s = ctx->GetAttr("path", &path);
          if (s.ok()) {
            // arbitrarily pick float model, since no way to template function
            auto m = LTS5::MorphableModelContainer<float>();
            int err = m.Load(path);
            if (!err) {
              auto n_vertex = m.get_n_vertex() * 3;
              auto n_texel = m.get_n_texel() * 3;
              auto n_tri = m.get_n_triangle();

              { // vertex
                std::vector<ShapeHandle> input_shp;
                s = ctx->input("w_shp", &input_shp);
                auto v_shape = ctx->MakeShape({ctx->Dim(input_shp[0], 0),
                                               n_vertex});
                s = ctx->set_output("vertex", {v_shape});
              }
              { // Color
                std::vector<ShapeHandle> input_shp;
                s = ctx->input("w_tex", &input_shp);
                auto t_shape = ctx->MakeShape({ctx->Dim(input_shp[0], 0),
                                               n_texel});
                s = ctx->set_output("color", {t_shape});
              }
              { // Triangle
                auto shp = ctx->MakeShape({n_tri, 3});
                s = ctx->set_output("triangle", {shp});
              }
            } else {
              s = tf::Status(tf::error::Code::INVALID_ARGUMENT,
                             "Can not load model");
            }
          }
          return s;
        })
        .Doc(R"doc(Generate an instance of a morphable model.
w_shp: Shape parameter
w_tex: Texture parameter
vertex: Generated surface (Set of vertex)
color: Generated color attributes
triangle: Triangulation of the surface, common for each instance
path: Location where the morphable model is stored)doc");



#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("MorphableModelOp")                  \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      MorphableModelKernel<CPUDevice, Type>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(Type)                                                 \
  REGISTER_KERNEL_BUILDER(                                                 \
      Name("MorphableModelOp")                                             \
      .Device(tf::DEVICE_GPU)                                              \
      .TypeConstraint<Type>("T"),                                          \
      MorphableModelKernel<GPUDevice, Type>);
REGISTER_GPU(float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA
