/**
 *  @file   shared_op.cpp
 *  @brief
 *  @ingroup
 *
 *  @author Christophe Ecabert
 *  @date   05/12/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <algorithm>

#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"

#include "lts5/tensorflow_op/shared_op.hpp"
#include "lts5/tensorflow_op/linear_algebra_functor.hpp"
#include "lts5/tensorflow_op/tensor_utils.hpp"
#include "lts5/tensorflow_op/device_utils.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#ifdef GOOGLE_CUDA
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
#endif

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @struct  ExampleFunctor
 * @brief   Helper function to select automatically the device on which to
 *          launch the computation (CPU, GPU)
 *          CPU Selection specialization
 * @author  Christophe Ecabert
 * @date    7/12/2017
 * @ingroup tensorflow_op
 * @tparam T Data type
 */
template <typename T>
struct ExampleFunctor<CPUDevice, T> {
  void operator()(const CPUDevice& d, int size, const T* in, T* out) {
    for (int i = 0; i < size; ++i) {
      out[i] = 2 * in[i];
    }
  }
};

template struct ExampleFunctor<CPUDevice, tensorflow::int32>;
template struct ExampleFunctor<CPUDevice, float>;
template struct ExampleFunctor<CPUDevice, double>;
}  // namepsace LTS5


/*
 * @name  SharedOp
 * @fn    explicit SharedOp(tensorflow::OpKernelConstruction* context)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template <typename Device, typename T>
SharedOp<Device, T>::SharedOp(tensorflow::OpKernelConstruction* context) :
        OpKernel(context) {

  std::cout << "CTO R" << std::endl;


  buffer_ = Eigen::Tensor<T, 1, Eigen::RowMajor, Eigen::DenseIndex>(2);
  buffer_(0) = T(100.0);
  buffer_(1) = T(200.0);

  std::cout << "CTOR Complete" << std::endl;
}

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* context) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template <typename Device, typename T>
void SharedOp<Device, T>::Compute(tensorflow::OpKernelContext* context) {

  // Get device
  const auto& device = context->eigen_device<Device>();
  // Grab the input tensor
  const tensorflow::Tensor* input_tensor = nullptr;
  const tensorflow::Tensor* input_vector = nullptr;

  OP_REQUIRES_OK(context, context->input("matrix", &input_tensor));
  OP_REQUIRES_OK(context, context->input("vector", &input_vector));


  // Create an output tensor
  tensorflow::Tensor* output_tensor = nullptr;
  tensorflow::int64 nrow = input_tensor->dim_size(0);
  tensorflow::int64 ncol = input_vector->dim_size(1);
  OP_REQUIRES_OK(context, context->allocate_output(0,
                                                   {nrow, ncol},
                                                   &output_tensor));
  // Sanity check for dimensions.
  OP_REQUIRES(context,
              input_tensor->NumElements() <= tensorflow::kint32max,
              tensorflow::errors::InvalidArgument("Too many elements in tensor"));





  static bool storage_init = false;
  if (!storage_init) {
    using TensorMap = typename tensorflow::TTypes<T>::ConstTensor;

    std::cout << "Init storage" << std::endl;

    // Allocate
    tensorflow::AllocatorAttributes attr;
    attr.set_gpu_compatible(true);
    OP_REQUIRES_OK(context,
                   context->allocate_persistent(tensorflow::DataTypeToEnum<T>::value,
                                                {nrow, ncol},
                                                &storage_,
                                                nullptr,
                                                attr));
    // Copy data into it
    tensorflow::Tensor* storage = storage_.AccessTensor(context);
    auto map = TensorMap(buffer_.data(), buffer_.dimensions());

    LTS5::Functor::DenseOp<Device, T, LTS5::Functor::DenseOpType::kH2DCopy> copy;
    copy(device,
         map,
         storage->flat<T>());
    LTS5::Functor::DeviceHelper<Device>::Synchronize(device);
    storage_init = true;
  }




  // Copy storage to output
  std::cout << "copy to output" << std::endl;
  LTS5::Functor::DenseOp<Device, T, LTS5::Functor::DenseOpType::kD2DCopy> copy;
  const tensorflow::Tensor* pers_storage = storage_.AccessTensor(context);
  copy(device, pers_storage->flat<T>(), output_tensor->flat<T>());

  LTS5::Functor::DeviceHelper<Device>::Synchronize(device);









  // Launch computation
  std::cout << "Co  mpute" << std::endl;




  std::cout << "GEMM" << std::endl;
  LTS5::Functor::LinearAlgebra<Device, T>::Gemm(context,
                                                *input_tensor,
                                                false,
                                                T(1.0),
                                                *input_vector,
                                                false,
                                                T(1.0),
                                                output_tensor);
};

#pragma mark -
#pragma mark Registration With Tensorflow

// Register the custom Op
// -------------------------------------------------------------
REGISTER_OP("SharedOp")
        .Attr("T: {float, double}")
        .Input("matrix: T")
        .Input("vector: T")
        .Output("out: T");

// Register the CPU kernels.
// -------------------------------------------------------------
#define REGISTER_CPU(T)                                          \
  REGISTER_KERNEL_BUILDER(                                       \
      Name("SharedOp").Device(tensorflow::DEVICE_CPU).TypeConstraint<T>("T"), \
      SharedOp<CPUDevice, T>);
REGISTER_CPU(float);
REGISTER_CPU(double);

#undef REGISTER_CPU

// Register the GPU kernels.
// -------------------------------------------------------------
#ifdef GOOGLE_CUDA
#define REGISTER_GPU(T)                                               \
  /*extern template class LTS5::Functor::LinearAlgebra<GPUDevice, T>; */\
  /*extern template struct LTS5::Functor::DenseOp<GPUDevice, T, LTS5::Functor::DenseOpType::kD2DCopy>; */\
  /*extern template struct LTS5::Functor::DenseOp<GPUDevice, T, LTS5::Functor::DenseOpType::kH2DCopy>; */\
  /*extern template struct LTS5::Functor::DeviceHelper<GPUDevice>;    */\
  REGISTER_KERNEL_BUILDER(Name("SharedOp")                          \
                          .Device(tensorflow::DEVICE_GPU)           \
                          .TypeConstraint<T>("T"),                  \
                          SharedOp<GPUDevice, T>);
REGISTER_GPU(float);
REGISTER_GPU(double);

#undef REGISTER_GPU
#endif  // GOOGLE_CUDA
