/**
 *  @file   rodrigues.cpp
 *  @brief  Rotation parametrization using Rodrigues formula.
 *  @see    https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/8/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "tensorflow/core/framework/shape_inference.h"

#include "lts5/tensorflow_op/rodrigues_grad.hpp"
#include "lts5/tensorflow_op/utils/shape.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   RodriguesGradientHelper
 * @brief   Compute rotation matrix gradient
 * @author  Christophe Ecabert
 * @date    15/07/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct RodriguesGradientHelper<CPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute rotation matrix using Rodrigues formula
   * @param[in] ctx             Op's context
   * @param[in] rvec            Rotation vector
   * @param[in] g_rmat          Back-propagated gradient
   * @param[out] grad           Rotation matrix gradient []
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* rvec,
                 const tf::Tensor* g_rmat,
                 tf::Tensor* grad) {
    return -1;
  }
};
}  // namespace Functor
}  // namespace LTS5

#pragma mark -
#pragma mark Initialization

/*
 * @name  RodriguesGradKernel
 * @fn    explicit RodriguesGradKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
RodriguesGradKernel<Device, T>::
RodriguesGradKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx) {}

/*
 * @name  ~RodriguesGradKernel
 * @fn    ~RodriguesGradKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
RodriguesGradKernel<Device, T>::~RodriguesGradKernel() {}

#pragma mark -
#pragma mark Usage

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void RodriguesGradKernel<Device, T>::Compute(tf::OpKernelContext* ctx) {
  using GradHelper = LTS5::Functor::RodriguesGradientHelper<Device, T>;

  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);
  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* g_rmat = nullptr;
  const tf::Tensor* rvec = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("g_rmat", &g_rmat));
  OP_REQUIRES_OK(ctx, ctx->input("rvec", &rvec));
  // Sanity check
  LTS5::ShapeUtils::CheckShape(ctx,
                               *rvec,
                               3,
                               tf::DataTypeToEnum<T>::v(),
                               {-1, 1, 3});
  LTS5::ShapeUtils::CheckShape(ctx,
                               *g_rmat,
                               3,
                               tf::DataTypeToEnum<T>::v(),
                               {-1, 3, 3});
  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* grad;
  OP_REQUIRES_OK(ctx, ctx->allocate_output("grad", rvec->shape(), &grad));
  // COMPUTE
  // -------------------------------------------------------
  // build rotation matrices
  int err = GradHelper()(ctx, rvec, g_rmat, grad);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in grad kernel"));
  }
}


#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;

REGISTER_OP("RodriguesOpGrad")
        .Input("g_rmat: T")
        .Input("rvec: T")
        .Output("grad: T")
        .Attr("T: {float}")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          std::vector<ShapeHandle> rvec_shp;
          auto s = ctx->input("rvec", &rvec_shp);
          s = ctx->set_output("grad", rvec_shp);
          return s;
        })
        .Doc(R"doc(Compute rotation matrix gradient)doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("RodriguesOpGrad")                   \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      RodriguesGradKernel<CPUDevice, Type>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(Type)                                                 \
  REGISTER_KERNEL_BUILDER(                                                 \
      Name("RodriguesOpGrad")                                              \
      .Device(tf::DEVICE_GPU)                                              \
      .TypeConstraint<Type>("T"),                                          \
      RodriguesGradKernel<GPUDevice, Type>);
REGISTER_GPU(float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA
