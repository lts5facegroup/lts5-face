/**
 *  @file   mm_mask.cpp
 *  @brief  Mask generator for Morphable model
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   10/4/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/tensorflow_op/mm_mask.hpp"
#include "lts5/utils/logger.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;
namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

#pragma mark -
#pragma mark Mask Generator

/*
 * @struct  MMMaskGenerator
 * @brief   Generate standard deviation mask
 * @author  Christophe Ecabert
 * @date    04.10.18
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct MMMaskGenerator<CPUDevice, T> {

  /** Tensor type */
  using Tensor = tensorflow::Tensor;
  /** Context type */
  using OpKernelContext = tensorflow::OpKernelContext;
  /** Storage type */
  using Storage = PersistentStorage<T>;

  /**
   * @name operator()
   * @brief Compute texture standard deviation mask
   * @param[in] ctx Op context
   * @param[in] param Morphable model's parameters
   * @param[in] storage Persistent storage
   * @param[in] n_std   How much std to take into accound while generating the
   *                    mask
   * @param[in] mask    Generated mask with min/max texture value explainable by
   *                    the model
   */
  void operator()(OpKernelContext* ctx,
                  const Tensor& bcoord,
                  Tensor* mask) {

    LTS5_LOG_INFO("Run cpu mask generator");
    // Do stuff here


  }
};

#pragma mark -
#pragma mark Explicit Instantiation

/** Float */
template struct MMMaskGenerator<CPUDevice, float>;
/** Double */
template struct MMMaskGenerator<CPUDevice, double>;

}  // namespace Functor
}  // namespace LTS5
