/**
 *  @file   mm_interpolator.cpp
 *  @brief  Morphable model Interpolator class. Compute pixel's barycentric
 *          coordinates on the image
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   5/3/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <numeric>
#include <vector>

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/tensorflow_op/mm_interpolator.hpp"
#include "lts5/tensorflow_op/mm_renderer_utils.hpp"

#include "lts5/utils/process_error.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/utils/math/matrix.hpp"
#include "lts5/utils/math/quaternion.hpp"

#include "lts5/utils/file_io.hpp"

#ifdef GOOGLE_CUDA
#include "lts5/cuda/utils/safe_call.hpp"
#include "lts5/cuda/tensorflow_op/mm_interpolator.hpp"

#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
#endif

using CPUDevice = Eigen::ThreadPoolDevice;
namespace tf = tensorflow;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Shader

/** Vertex Shader */
static constexpr const char* vs_str_rigid =
        "#version 430 core\n"
        "layout (location = 0) in vec3 vertex;\n"
        "layout (location = 1) in vec3 normal;\n"
        "layout (location = 2) in vec2 tcoord;\n"
        "layout (std140) uniform Transform {\n"
        "  mat4 model;\n"
        "  mat4 projection;\n"
        "};\n"
        "out VS_OUT {\n"
        "  vec3 normal0;\n"
        "  flat vec2 tcoord0;\n"
        "} vs_out;\n"
        "void main() {\n"
        "  gl_Position = projection * model * vec4(vertex, 1.f);\n"
        "  vs_out.normal0 = normal;\n"
        "  vs_out.tcoord0 = tcoord;\n"
        "}";
static constexpr const char* vs_str =
        "#version 430 core\n"
        "layout (location = 0) in vec3 vertex;\n"
        "layout (location = 1) in vec3 normal;\n"
        "layout (location = 2) in vec2 tcoord;\n"
        "uniform mat4 projection;\n"
        "out VS_OUT {\n"
        "  vec3 normal0;\n"
        "  flat vec2 tcoord0;\n"
        "} vs_out;\n"
        "void main() {\n"
        "  gl_Position = projection * vec4(vertex, 1.f);\n"
        "  vs_out.normal0 = normal;\n"
        "  vs_out.tcoord0 = tcoord;\n"
        "}";

/** Geometry shader */
static constexpr const char* gs_str =
        "#version 430 core\n"
        "layout (triangles) in;\n"
        "layout (triangle_strip, max_vertices = 3) out;\n"
        "in VS_OUT {\n"
        "  vec3 normal0;\n"
        "  flat vec2 tcoord0;\n"
        "} gs_in[];\n"
        "out vec3 bary0;\n"
        "out vec3 normal1;\n"
        "flat out vec2 tcoord1;\n"
        "void main() {\n"
        "  for (int i = 0; i < 3; ++i) {\n"
        "    gl_Position = gl_in[i].gl_Position;\n"
        "    normal1 = gs_in[i].normal0;\n"
        "    tcoord1 = gs_in[i].tcoord0;\n"
        "    bary0 = vec3(0.f);\n"
        "    bary0[i] = 1.f;\n"
        "    EmitVertex();\n"
        "  }\n"
        "  EndPrimitive();\n"
        "}";

/** Fragment shader - CPU */
static constexpr const char* fs_str =
        "#version 430 core\n"
        "in vec3 bary0;\n"
        "in vec3 normal1;\n"
        "flat in vec2 tcoord1;\n"
        "out vec3 fragColor;\n"
        "void main() {\n"
        "  fragColor = vec3(tcoord1.x, bary0.x, bary0.y);\n"
        "}";


/*
 * @class   MMInterpShader
 * @brief   Shading program for interpolation - CPU Specialization
 * @author  Christophe Ecabert
 * @date    03/05/18
 * @ingroup tensforflow_op
 * @tparam T    Data type
 */
template<typename T>
MMInterpShader<CPUDevice, T>::MMInterpShader(const MMRendererParameters<T>& p,
                                             const bool& with_rigid) :
        shader_(nullptr),
        ubo_(nullptr),
        focal_(p.focal), has_rigid_(with_rigid) {
  using Mat4 = Matrix4x4<T>;
  try {
    std::vector<OGLShader> shaders;
    const char* vs = has_rigid_ ? vs_str_rigid : vs_str;
    shaders.emplace_back(vs, OGLShader::ShaderType::kVertex);
    shaders.emplace_back(gs_str, OGLShader::ShaderType::kGeometry);
    shaders.emplace_back(fs_str, OGLShader::ShaderType::kFragment);
    // Define attributes
    using Attributes = OGLProgram::Attributes;
    using Types = OGLProgram::AttributeType;
    std::vector<Attributes> attributes;
    attributes.emplace_back(Types::kPoint, 0, "vertex");
    attributes.emplace_back(Types::kNormal, 1, "normal");
    attributes.emplace_back(Types::kTexCoord, 2, "tcoord");
    shader_ = new OGLProgram(shaders, attributes);
  } catch (const LTS5::ProcessError &e) {
    std::string msg = "OpenGL failed to initialize shader: ";
    msg += std::string(e.what());
    throw LTS5::ProcessError(ProcessError::ProcessErrorEnum::kErr,
                             msg,
                             FUNC_NAME);
  }

  if (has_rigid_) {
    // Define ubo binding point
    int err = this->shader_->SetUniformBlockBinding("Transform", 0);
    if (err) {
      LTS5_LOG_ERROR("Can not set UBO binding point");
    }
    // Create Uniform buffer
    ubo_ = new UniformBufferObject();
    err |= ubo_->Create({{0, 2 * 16 * sizeof(T)}});  // 2x[4x4] matrix of T
    if (!err) {
      // Add projection matrix since it has to be initialized once.
      // http://kgeorge.github.io/2014/03/08/calculating-opengl-perspective-matrix-from-opencv-intrinsic-matrix
      Mat4 P;  // Projection matrix
      T cx = static_cast<T>(p.width) / T(2.0);
      T cy = static_cast<T>(p.height) / T(2.0);
      P(0, 0) = p.focal / cx;
      P(1, 1) = p.focal / cy;
      P(2, 2) = -(p.far + p.near) / (p.far - p.near);
      P(2, 3) = -(T(2.0) * p.far * p.near) / (p.far - p.near);
      P(3, 2) = T(-1.0);
      auto Pt = P.Transpose();
      err = ubo_->AddData(0, 16 * sizeof(T), 16 * sizeof(T), Pt.Data());
      if (err) {
        LTS5_LOG_ERROR("Can not initialize projection matrix");
      }
    } else {
      LTS5_LOG_ERROR("Can not initialize UBO");
    }
  } else {
    Mat4 P;  // Projection matrix
    T cx = static_cast<T>(p.width) / T(2.0);
    T cy = static_cast<T>(p.height) / T(2.0);
    P(0, 0) = p.focal / cx;
    P(1, 1) = p.focal / cy;
    P(2, 2) = -(p.far + p.near) / (p.far - p.near);
    P(2, 3) = -(T(2.0) * p.far * p.near) / (p.far - p.near);
    P(3, 2) = T(-1.0);
    shader_->Use();
    shader_->SetUniform("projection", P);
    shader_->StopUsing();
  }
}

/*
 * @name  ~MMInterpShader
 * @fn    ~MMInterpShader() override = default
 * @brief Destructor
 */
template<typename T>
MMInterpShader<CPUDevice, T>::~MMInterpShader() {
  delete ubo_;
  delete shader_;
}

/*
 * @name  UpdateModelTransform
 * @fn    int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                                   const tensorflow::Tensor& parameter,
                                   const tensorflow::int64& idx)
 * @brief Update model transformation matrix from camera parameters
 * @param[in] ctx Op context
 * @param[in] parameter Morphable model parameters
 * @param[in] idx Index where camera parameters start
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MMInterpShader<CPUDevice, T>::
UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                     const tensorflow::Tensor& parameter,
                     const tensorflow::int64& idx) {
  if (has_rigid_) {
    using Mat4 = Matrix4x4<T>;
    using Quat = Quaternion<T>;
    // Access camera parameters
    const auto *p_ptr = parameter.unaligned_flat<T>().data();
    p_ptr = &p_ptr[idx];
    // Construct transformation matrix
    Mat4 M;
    Quat q;
    q.FromEuler(p_ptr[0], p_ptr[1], p_ptr[2]);
    q.ToRotationMatrix(&M);
    M(0, 3) = p_ptr[3] * focal_;
    M(1, 3) = p_ptr[4] * focal_;
    M(2, 3) = p_ptr[5] * focal_;
    auto Mt = M.Transpose();  // OGL is column major :(
    // Update
    return ubo_->AddData(0, 0, 16 * sizeof(T), Mt.Data());
  }
  return -1;
}


#pragma mark -
#pragma mark Interpolator

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   MMShuffleOglBuffer
 * @brief   Reorganise mesh from a shared topology to a splitted one
 * @author  Christophe Ecabert
 * @date    03/05/18
 * @ingroup tensorflow_op
 * @tparam T        Data type
 */
template<typename T>
class MMShuffleOglBuffer<CPUDevice, T>{
 public:

  /** Tensor type */
  using Tensor = tensorflow::Tensor;

  /**
   * @name  operator()
   * @fn    int operator()(tensorflow::OpKernelContext* ctx, const Tensor* buffer,
                 const Tensor* triangle, T* ogl_buffer)
   * @brief Reorganise a buffer from a shared topology to a splitted one based
   *        on a triangulation
   * @param[in] ctx Op context
   * @param[in] buffer
   * @param[in] triangle
   * @param[out] ogl_buffer
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const Tensor* buffer,
                 const Tensor* triangle,
                 T* ogl_buffer) {
    using Vec3i = Vector3<int>;
    using Vec3 = Vector3<T>;
    // Access data
    const auto* tri_ptr = reinterpret_cast<const Vec3i*>(triangle->flat<int>().data());
    const auto* src_ptr = reinterpret_cast<const Vec3*>(buffer->unaligned_flat<T>().data());
    auto* dst_ptr = reinterpret_cast<Vec3*>(ogl_buffer);
    const auto n_tri = static_cast<size_t>(triangle->dim_size(0));
    // Loop over all triangle
    Parallel::For(n_tri,
                  [&](const size_t& t) {
                    auto tri = tri_ptr[t];
                    dst_ptr[3 * t] = src_ptr[tri.x_];
                    dst_ptr[(3 * t) + 1] = src_ptr[tri.y_];
                    dst_ptr[(3 * t) + 2] = src_ptr[tri.z_];
                  });
    // Done
    return 0;
  }
};



}  // namespace Functor

/*
 * @name  MMInterpolator
 * @fn    MMInterpolator()
 * @brief Constructor
 */
template<typename Device, typename T>
MMInterpolator<Device, T>::MMInterpolator() : renderer_(nullptr),
                                              shader_(nullptr),
                                              v_map_(new OGLMapper()),
                                              n_map_(new OGLMapper()),
                                              is_init_(false) {
}

/*
 * @name  ~MMInterpolator
 * @fn    ~MMInterpolator()
 * @brief Destructor
 */
template<typename Device, typename T>
MMInterpolator<Device, T>::~MMInterpolator() {
  if (renderer_) {
    renderer_->ActivateContext();
  }
  delete shader_;
  delete v_map_;
  delete n_map_;
  delete renderer_;
}

/*
 * @name  Init
 * @fn    int Init(const Tensor& triangle,
 *                 const MMRendererParameters<T>& params)
 * @brief Initialise the interpolator
 * @param[in] triangle    Mesh triangulation
 * @param[in] params      Rendering parameters
 * @return -1 if error, 0 otherwise
 */
template<typename Device, typename T>
int MMInterpolator<Device, T>::Init(const Tensor& triangle,
                                    const MMRendererParameters<T>& params) {
  int err = 0;
  if (!is_init_) {
    using BufferInfo = typename Renderer::BufferInfo;
    using Vertex = typename Renderer::Renderer::Mesh::Vertex;
    using Normal = typename Renderer::Renderer::Mesh::Normal;
    using Color = typename Renderer::Renderer::Mesh::Color;
    using TCoord = typename Renderer::Renderer::Mesh::TCoord;
    using Triangle = typename Renderer::Renderer::Mesh::Triangle;

    // Define opengl buffer dimension + init with fixed data
    // ------------------------------------------------------------------

    n_tri_ = triangle.dim_size(0);
    BufferInfo infos;
    infos.dim_vertex_ = 3 * n_tri_ * sizeof(Vertex);
    infos.dim_normal_ = 3 * n_tri_ * sizeof(Normal);
    infos.dim_color_ = 3 * n_tri_ * sizeof(Color);
    infos.dim_tex_coord_ = 3 * n_tri_ * sizeof(TCoord);
    infos.n_elements_ = 3 * n_tri_ ;

    // Define new triangulation
    std::vector<Triangle> tri(n_tri_);
    auto* tri_ptr = reinterpret_cast<int*>(tri.data());
    std::iota(tri_ptr, tri_ptr + (3 * n_tri_), 0);
    // Fill tcoord with triangle indexes
    std::vector<TCoord> tcoords(3 * n_tri_);
    for (size_t i = 0; i < n_tri_; ++i) {
      auto tc = TCoord(i + 1, i + 1);  // Triangle index start with 1!!!
      tcoords[3 * i] = tc;
      tcoords[(3 * i) + 1] = tc;
      tcoords[(3 * i) + 2] = tc;
    }
    infos.data_elements_ = reinterpret_cast<const int*>(tri.data());
    infos.data_tex_coord_ = tcoords.data();

    // Create interpolator
    // ------------------------------------------------------------------
    if (!renderer_) {
      renderer_ = new Renderer();
    }
    renderer_->set_clear_color(T(0.0), T(0.0), T(0.0), T(0.0));
    // Interpolator is not multisampled otherwise the triangle index will be
    // wrong due to interpolation
    auto copy_params = params;
    copy_params.multisampling = 0;
    err = renderer_->Init(copy_params);
    // Create shader + bind + mapper
    if (!shader_) {
      shader_ = new MMInterpShader<Device, T>(params, false);
    }
    err |= renderer_->Update(infos, shader_->get_shader());
    // Init mapper
    v_map_->Init(OGLMapper::BufferIdx::kVertex, renderer_->get_renderer());
    n_map_->Init(OGLMapper::BufferIdx::kNormal, renderer_->get_renderer());
    renderer_->DisableContext();
    // Check if properly init
    is_init_ = (err == 0);

  }
  return err;
}

#pragma mark -
#pragma mark Process

/*
 * @name  ActivateContext
 * @brief Enable OpenGL context
 * @fn    void ActivateContext() const
 */
template<typename Device, typename T>
void MMInterpolator<Device, T>::ActivateContext() const {
  renderer_->ActivateContext();
}

/*
 * @name  DisableContext
 * @brief Disable OpenGL context
 * @fn    void DisableContext() const
 */
template<typename Device, typename T>
void MMInterpolator<Device, T>::DisableContext() const {
  renderer_->DisableContext();
}

/*
 * @name  ComputeBarycentric
 * @fn    int ComputeBarycentric(tensorflow::OpKernelContext* ctx,
                                 const Tensor& parameters,
                                 Storage* storage,
                                 Tensor* bcoord)
 * @brief Compute barycentric interpolant
 * @param[in] ctx Op's context
 * @param[in] parameters Model's parameters
 * @param[in] storage Morphable model storage
 * @param[out] bcoord Barycentric coordinate
 */
/*template<typename Device, typename T>
int MMInterpolator<Device, T>::ComputeBarycentric(tensorflow::OpKernelContext* ctx,
                                                  const Tensor& parameters,
                                                  Storage* storage,
                                                  Tensor* bcoord) {
  int err = 0;
  auto* shp = storage->shape.AccessTensor(ctx);
  auto* triangle = storage->triangle.AccessTensor(ctx);
  auto n_img = parameters.dim_size(0);
  // Loop over the whole batch
  for (auto k = 0; k < n_img; ++k) {
    auto shp_slice = shp->Slice(k, k + 1);
    auto param_slice = parameters.Slice(k, k + 1);
    auto bcoord_slice = bcoord->Slice(k, k + 1);
    // Update uniforms
    err |= shader_->UpdateModelTransform(ctx, param_slice, cam_idx_);
    if (err) {
      LTS5_LOG_ERROR("Can not update model matrix");
    }
    // Mapped ptr + shuffle vertices
    T* v_ptr = v_map_->Map(ctx);
    err = Functor::MMShuffleOglBuffer<Device, T>()(ctx,
                                                   &shp_slice,
                                                   triangle,
                                                   v_ptr);
    // Unmap
    v_map_->Unmap(ctx);
    // Render image
    renderer_->Render(ctx, n_tri_ * 3, 0, true, nullptr, &bcoord_slice);
  }
  // Done
  return err;
}*/

/*
 * @name  ComputeBarycentric
 * @fn    int ComputeBarycentric(tf::OpKernelContext* ctx,
                                 const Tensor& vertex,
                                 const Tensor& triangle,
                                 const size_t& n_element,
                                 Tensor* bcoords)
 * @brief Compute barycentric interpolant
 * @param[in] ctx Op's context
 * @param[in] vertex Vertex array (batch)
 * @param[in] triangle Shared triangulation
 * @param[in] n_element   Number of primitive element to be rendered. If set
 *                        to numeric_limits<size_t>::max() will render all of
 *                        them otherwise only the specified number
 * @param[out] bcoord Barycentric coordinate
 */
template<typename Device, typename T>
int MMInterpolator<Device, T>::ComputeBarycentric(tf::OpKernelContext* ctx,
                                                  const Tensor& vertex,
                                                  const Tensor& triangle,
                                                  const size_t& n_element,
                                                  Tensor* bcoords) {
  int err = 0;
  auto n_img = vertex.dim_size(0);
  // Loop over the whole batch
  for (auto k = 0; k < n_img; ++k) {
    auto shp_slice = vertex.Slice(k, k + 1);
    auto bcoord_slice = bcoords->Slice(k, k + 1);
    // Mapped ptr + shuffle vertices
    T* v_ptr = v_map_->Map(ctx);
    err = Functor::MMShuffleOglBuffer<Device, T>()(ctx,
                                                   &shp_slice,
                                                   &triangle,
                                                   v_ptr);
    // Unmap
    v_map_->Unmap(ctx);
    // Render image
    renderer_->Render(ctx, n_element, 0, true, &bcoord_slice);
  }
  // Done
  return err;
}

#pragma mark -
#pragma mark Explicit Instantiation

/** Float - CPU */
template class MMInterpShader<CPUDevice, float>;
/** Double - CPU */
template class MMInterpShader<CPUDevice, double>;

/** Float - CPU */
template class MMInterpolator<CPUDevice, float>;
/** Double - CPU */
template class MMInterpolator<CPUDevice, double>;

#ifdef GOOGLE_CUDA
/** Float - GPU */
template class MMInterpolator<GPUDevice, float>;
/** Double - GPU */
template class MMInterpolator<GPUDevice, double>;
#endif

}  // namespace LTS5 {
