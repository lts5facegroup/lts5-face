/**
 *  @file   bbox.cpp
 *  @brief  Utility function for bbox handling. Taken from Caffe SSD git
 *  @ingroup    tensorflow_op
 *  @see    https://github.com/weiliu89/caffe/blob/ssd/include/caffe/util/bbox_util.hpp
 *
 *  @author Christophe Ecabert
 *  @date   8/13/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <algorithm>

#include "tensorflow/core/framework/op.h"

#include "lts5/tensorflow_op/utils/bbox.hpp"

#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Functions

/**
 * @name    GetMaxScoreIndex
 * @brief   Get max scores with corresponding indices.
 * @param[in] scores        A set of scores.
 * @param[in] threshold     Only consider scores higher than the threshold.
 * @param[in] top_k         If -1, keep all; otherwise, keep at most top_k.
 * @param[out] score_index  Store the sorted (score, index) pair.
 * @tparam T    Data type
 */
template<typename T>
void GetMaxScoreIndex(const std::vector<T>& scores,
                      const T& threshold,
                      const int& top_k,
                      std::vector<ScoreIndex<T>>* score_index) {
  score_index->clear();
  // Generate index score pairs.
  for (int i = 0; i < scores.size(); ++i) {
    if (scores[i] > threshold) {
      score_index->push_back({scores[i], i});
    }
  }
  // Sort the score pair according to the scores in descending order
  auto cmp = [](const ScoreIndex<T>& p1, const ScoreIndex<T>& p2) -> bool {
    return p1.score > p2.score;
  };
  std::stable_sort(score_index->begin(), score_index->end(), cmp);
  // Keep top_k scores if needed.
  if (top_k > -1 && top_k < score_index->size()) {
    score_index->resize(top_k);
  }
}

/**
 * @name    JaccardOverlap
 * @brief   Compute the jaccard (intersection over union IoU) overlap between
 *          two bboxes.
 * @param[in] bbox1         BBox 1
 * @param[in] bbox2         BBox 2
 * @param[in] normalized    If true, indicates that bbox are normalized.
 * @tparam T    Data type
 * @return  IoU
 */
template<typename T>
T JaccardOverlap(const NormalizedBBox<T>& bbox1,
                 const NormalizedBBox<T>& bbox2,
                 const bool& normalized) {
  NormalizedBBox<T> intersect_bbox = bbox1.Intersect(bbox2);
  float intersect_width, intersect_height;
  if (normalized) {
    intersect_width = intersect_bbox.xmax - intersect_bbox.xmin;
    intersect_height = intersect_bbox.ymax - intersect_bbox.ymin;
  } else {
    intersect_width = intersect_bbox.xmax - intersect_bbox.xmin + 1;
    intersect_height = intersect_bbox.ymax - intersect_bbox.ymin + 1;
  }
  if (intersect_width > 0 && intersect_height > 0) {
    float intersect_size = intersect_width * intersect_height;



    T bbox1_size = bbox1.ComputeSize(normalized);
    T bbox2_size = bbox2.ComputeSize(normalized);
    return intersect_size / (bbox1_size + bbox2_size - intersect_size);
  } else {
    return 0.;
  }
}

/*
 * @name    GetLocPredictions
 * @brief   Get location predictions from loc_data.
 * @param[in] loc_data  Array of predicted location:
 *                      num x num_preds_per_class * num_loc_classes * 4
 * @param[in] num       The number of images in the batch
 * @param[in] num_preds_per_class   Number of predictions per class
 * @param[in] num_loc_classes   number of location classes. It is 1 if
 *                              `share_location` is true; and is equal to number
 *                              of classes needed to predict otherwise.
 * @param[in] share_location    if true, all classes share the same location
 *                              prediction.
 * @param[out] loc_preds        stores the location prediction, where each item
 *                              contains location prediction for an image.
 * @tparam T    Data type
 */
template<typename T>
void GetLocPredictions(const T* loc_data,
                       const int& num,
                       const int& num_preds_per_class,
                       const int& num_loc_classes,
                       const bool& share_location,
                       std::vector<LabelBBox<T>>* loc_preds) {
  loc_preds->clear();
  if (share_location) {
    CHECK_EQ(num_loc_classes, 1);
  }
  loc_preds->resize(num);
  // Iterate over all entries
  for (int i = 0; i < num; ++i) {
    auto& label_bbox = loc_preds->at(i);
    for (int p = 0; p < num_preds_per_class; ++p) {
      int start_idx = p * num_loc_classes * 4;
      for (int c = 0; c < num_loc_classes; ++c) {
        int label = share_location ? -1 : c;
        if (label_bbox.find(label) == label_bbox.end()) {
          label_bbox[label].resize(num_preds_per_class);
        }
        label_bbox[label][p].xmin = loc_data[start_idx + c * 4];
        label_bbox[label][p].ymin = loc_data[start_idx + c * 4 + 1];
        label_bbox[label][p].xmax = loc_data[start_idx + c * 4 + 2];
        label_bbox[label][p].ymax = loc_data[start_idx + c * 4 + 3];
      }
    }
    loc_data += num_preds_per_class * num_loc_classes * 4;
  }
}

/*
 * @name    GetConfidenceScores
 * @brief   Get confidence predictions from conf_data.
 * @param[in] conf_data          num x num_preds_per_class * num_classes array
 * @param[in] num                   The number of images.
 * @param[in] num_preds_per_class   Number of predictions per class.
 * @param[in] num_classes           Number of classes.
 * @param[out] conf_scores          stores the confidence prediction, where
 *                                  each item contains confidence prediction for
 *                                  an image.
 * @tparam T    Data type
 */
template <typename T>
void GetConfidenceScores(const T* conf_data,
                         const int& num,
                         const int& num_preds_per_class,
                         const int& num_classes,
                         std::vector<ConfScore<T>>* conf_scores) {
  conf_scores->clear();
  conf_scores->resize(num);
  for (int i = 0; i < num; ++i) {
    auto& label_scores = conf_scores->at(i);
    for (int p = 0; p < num_preds_per_class; ++p) {
      int start_idx = p * num_classes;
      for (int c = 0; c < num_classes; ++c) {
        label_scores[c].push_back(conf_data[start_idx + c]);

        //std::cout << start_idx + c << ", class: " << c << ", conf " << conf_data[start_idx + c] << std::endl;
      }
    }
    conf_data += num_preds_per_class * num_classes;
  }
}

/*
 * @name    GetPriorBBoxes
 * @brief   Get prior bounding boxes from prior_data.
 * @param[in] prior_data    2 x num_priors x 4 array of priors (i.e. anchors)
 * @param[in] num_priors    Number of priors.
 * @param[out] prior_bboxes Stores all the prior bboxes in the format of
 *                          NormalizedBBox.
 * @param[out] prior_variances  Stores all the variances needed by prior bboxes.
 * @tparam T    Data type
 */
template <typename T>
void GetPriorBBoxes(const T* prior_data,
                    const int& num_priors,
                    std::vector<NormalizedBBox<T>>* prior_bboxes,
                    std::vector<Vars<T>>* prior_variances) {
  prior_bboxes->clear();
  prior_variances->clear();
  // BBoxes
  for (int i = 0; i < num_priors; ++i) {
    int start_idx = i * 4;
    NormalizedBBox<T> bbox;
    bbox.xmin = prior_data[start_idx];
    bbox.ymin = prior_data[start_idx + 1];
    bbox.xmax = prior_data[start_idx + 2];
    bbox.ymax = prior_data[start_idx + 3];
    bbox.size = bbox.ComputeSize(true);
    prior_bboxes->push_back(bbox);
  }
  // BBox variances
  for (int i = 0; i < num_priors; ++i) {
    int start_idx = (num_priors + i) * 4;
    std::vector<T> var;
    for (int j = 0; j < 4; ++j) {
      var.push_back(prior_data[start_idx + j]);
    }
    prior_variances->push_back(var);
  }
}

/*
 * @name    DecodeBBox
 * @brief   Decode a bbox according to a prior bbox.
 * @param[in] prior_bbox        Box prior
 * @param[in] prior_variance    Box variance
 * @param[in] code_type         Decoding type
 * @param[in] variance_encoded_in_target
 * @param[in] clip_bbox
 * @param[in] bbox          Box to decode
 * @param[out] decode_bbox  Decoded box
 * @tparam T    Data type
 */
template<typename T>
void DecodeBBox(const NormalizedBBox<T>& prior_bbox,
                const Vars<T>& prior_variance,
                const PriorBoxType& code_type,
                const bool& variance_encoded_in_target,
                const bool& clip_bbox,
                const NormalizedBBox<T>& bbox,
                NormalizedBBox<T>* decode_bbox) {
  if (code_type == PriorBoxType::kCorner) {
    if (variance_encoded_in_target) {
      // variance is encoded in target, we simply need to add the offset
      // predictions.
      decode_bbox->xmin = prior_bbox.xmin + bbox.xmin;
      decode_bbox->ymin = prior_bbox.ymin + bbox.ymin;
      decode_bbox->xmax = prior_bbox.xmax + bbox.xmax;
      decode_bbox->ymax = prior_bbox.ymax + bbox.ymax;
    } else {
      // variance is encoded in bbox, we need to scale the offset accordingly.
      decode_bbox->xmin = prior_bbox.xmin + (prior_variance[0] * bbox.xmin);
      decode_bbox->ymin = prior_bbox.ymin + (prior_variance[1] * bbox.ymin);
      decode_bbox->xmax = prior_bbox.xmax + (prior_variance[2] * bbox.xmax);
      decode_bbox->ymax = prior_bbox.ymax + (prior_variance[3] * bbox.ymax);
    }
  } else if (code_type == PriorBoxType::kCenterSize) {
    T prior_width = prior_bbox.xmax - prior_bbox.xmin;
    T prior_height = prior_bbox.ymax - prior_bbox.ymin;
    CHECK_GT(prior_width, 0);
    CHECK_GT(prior_height, 0);
    T prior_center_x = (prior_bbox.xmin + prior_bbox.xmax) / T(2.0);
    T prior_center_y = (prior_bbox.ymin + prior_bbox.ymax) / T(2.0);

    T decode_bbox_center_x, decode_bbox_center_y;
    T decode_bbox_width, decode_bbox_height;
    if (variance_encoded_in_target) {
      // variance is encoded in target, we simply need to retore the offset
      // predictions.
      decode_bbox_center_x = bbox.xmin * prior_width + prior_center_x;
      decode_bbox_center_y = bbox.ymin * prior_height + prior_center_y;
      decode_bbox_width = std::exp(bbox.xmax) * prior_width;
      decode_bbox_height = std::exp(bbox.ymax) * prior_height;
    } else {
      // variance is encoded in bbox, we need to scale the offset accordingly.
      decode_bbox_center_x = prior_variance[0] * bbox.xmin * prior_width + prior_center_x;
      decode_bbox_center_y = prior_variance[1] * bbox.ymin * prior_height + prior_center_y;
      decode_bbox_width = std::exp(prior_variance[2] * bbox.xmax) * prior_width;
      decode_bbox_height = std::exp(prior_variance[3] * bbox.ymax) * prior_height;
    }
    decode_bbox->xmin = decode_bbox_center_x - decode_bbox_width / T(2.0);
    decode_bbox->ymin = decode_bbox_center_y - decode_bbox_height / T(2.0);
    decode_bbox->xmax = decode_bbox_center_x + decode_bbox_width / T(2.0);
    decode_bbox->ymax = decode_bbox_center_y + decode_bbox_height / T(2.0);
  } else if (code_type == PriorBoxType::kCornerSize) {
    T prior_width = prior_bbox.xmax - prior_bbox.xmin;
    T prior_height = prior_bbox.ymax - prior_bbox.ymin;
    CHECK_GT(prior_width, 0);
    CHECK_GT(prior_height, 0);
    if (variance_encoded_in_target) {
      // variance is encoded in target, we simply need to add the offset
      // predictions.
      decode_bbox->xmin = prior_bbox.xmin + bbox.xmin * prior_width;
      decode_bbox->ymin = prior_bbox.ymin + bbox.ymin * prior_height;
      decode_bbox->xmax = prior_bbox.xmax + bbox.xmax * prior_width;
      decode_bbox->ymax = prior_bbox.ymax + bbox.ymax * prior_height;
    } else {
      // variance is encoded in bbox, we need to scale the offset accordingly.
      decode_bbox->xmin = prior_bbox.xmin + prior_variance[0] * bbox.xmin * prior_width;
      decode_bbox->ymin = prior_bbox.ymin + prior_variance[1] * bbox.ymin * prior_height;
      decode_bbox->xmax = prior_bbox.xmax + prior_variance[2] * bbox.xmax * prior_width;
      decode_bbox->ymax = prior_bbox.ymax + prior_variance[3] * bbox.ymax * prior_height;
    }
  } else {
    LOG(FATAL) << "Unknown PriorBoxType.";
  }
  decode_bbox->size = decode_bbox->ComputeSize(true);
  if (clip_bbox) {
    decode_bbox->Clip();
  }
}

/*
 * @name    DecodeBBoxes
 * @brief   Decode a set of bboxes according to a set of prior bboxes.
 * @param prior_bboxes      Boxes prior
 * @param prior_variances   Boxes variance
 * @param code_type         Decoding type
 * @param variance_encoded_in_target
 * @param clip_bbox
 * @param bboxes            Boxes to decode
 * @param decode_bboxes     Decoded boxes
 * @tparam T    Data type
 */
template<typename T>
void DecodeBBoxes(const std::vector<NormalizedBBox<T>>& prior_bboxes,
                  const std::vector<Vars<T>>& prior_variances,
                  const PriorBoxType& code_type,
                  const bool& variance_encoded_in_target,
                  const bool& clip_bbox,
                  const std::vector<NormalizedBBox<T>>& bboxes,
                  std::vector<NormalizedBBox<T>>* decode_bboxes) {
  CHECK_EQ(prior_bboxes.size(), prior_variances.size());
  CHECK_EQ(prior_bboxes.size(), bboxes.size());
  int num_bboxes = prior_bboxes.size();
  if (num_bboxes >= 1) {
    CHECK_EQ(prior_variances[0].size(), 4);
  }
  decode_bboxes->clear();
  for (int i = 0; i < num_bboxes; ++i) {
    NormalizedBBox<T> decode_bbox;
    DecodeBBox<T>(prior_bboxes[i],
                  prior_variances[i],
                  code_type,
                  variance_encoded_in_target,
                  clip_bbox,
                  bboxes[i],
                  &decode_bbox);
    decode_bboxes->push_back(decode_bbox);
  }
}

/*
 * @name    DecodeBBoxesAll
 * @brief   Decode all bboxes in a batch.
 * @param[in] all_loc_pred  stores the location prediction, where each item
 *                          contains location prediction for an image.
 * @param[in] prior_bboxes
 * @param[in] prior_variances   Stores all the variances needed by prior bboxes.
 * @param[in] num
 * @param[in] share_location
 * @param[in] num_loc_classes
 * @param[in] background_label_id
 * @param[in] code_type
 * @param[in] variance_encoded_in_target
 * @param[in] clip
 * @param[out] all_decode_bboxes
 * @tparam T    Data type
 */
template<typename T>
void DecodeBBoxesAll(const std::vector<LabelBBox<T>>& all_loc_pred,
                     const std::vector<NormalizedBBox<T>>& prior_bboxes,
                     const std::vector<Vars<T>>& prior_variances,
                     const int& num,
                     const bool& share_location,
                     const int& num_loc_classes,
                     const int& background_label_id,
                     const PriorBoxType& code_type,
                     const bool& variance_encoded_in_target,
                     const bool& clip,
                     std::vector<LabelBBox<T>>* all_decode_bboxes) {
  CHECK_EQ(all_loc_pred.size(), num);
  all_decode_bboxes->clear();
  all_decode_bboxes->resize(num);

  for (int i = 0; i < num; ++i) {
    // Decode predictions into bboxes.
    auto& decode_bboxes = all_decode_bboxes->at(i);
    for (int c = 0; c < num_loc_classes; ++c) {
      int label = share_location ? -1 : c;
      if (label == background_label_id) {
        // Ignore background class.
        continue;
      }
      if (all_loc_pred[i].find(label) == all_loc_pred[i].end()) {
        // Something bad happened if there are no predictions for current label.
        LOG(FATAL) << "Could not find location predictions for label " << label;
      }
      const auto& label_loc_preds = all_loc_pred[i].find(label)->second;
      DecodeBBoxes<T>(prior_bboxes,
                      prior_variances,
                      code_type,
                      variance_encoded_in_target,
                      clip,
                      label_loc_preds,
                      &(decode_bboxes[label]));
    }
  }
}

/*
 * @name    ApplyNMSFast
 * @brief   Do non maximum suppression given bboxes and scores. Inspired by
 *          Piotr Dollar's NMS implementation in EdgeBox.
 *          https://goo.gl/jV3JYS
 * @param[in] bboxes            A set of bounding boxes.
 * @param[in] scores            A set of corresponding confidences.
 * @param[in] score_threshold   A threshold used to filter detection results.
 * @param[in] nms_threshold     A threshold used in non maximum suppression.
 * @param[in] eta               Adaptation rate for nms threshold
 *                              (see Piotr's paper).
 * @param[in] top_k             If not -1, keep at most top_k picked indices.
 * @param[out] indices          the kept indices of bboxes after nms.
 * @tparam T    Data type
 */
template<typename T>
void ApplyNMSFast(const std::vector<NormalizedBBox<T>>& bboxes,
                  const std::vector<T>& scores,
                  const T& score_threshold,
                  const T& nms_threshold,
                  const T& eta,
                  const int& top_k,
                  std::vector<int>* indices) {
  // Sanity check.
  CHECK_EQ(bboxes.size(), scores.size())
    << "bboxes and scores have different size.";



  /*LTS5_LOG_DEBUG("*** nms params ***");
  LTS5_LOG_DEBUG("  thresh: " << nms_threshold);
  LTS5_LOG_DEBUG("  top_k : " << top_k);
  LTS5_LOG_DEBUG("eta : " << eta);*/

  // Get top_k scores (with corresponding indices).
  std::vector<ScoreIndex<T>> score_index_vec;
  GetMaxScoreIndex<T>(scores,
                      score_threshold,
                      top_k,
                      &score_index_vec);
  // Do nms.
  T adaptive_threshold = nms_threshold;
  indices->clear();
  while (score_index_vec.size() != 0) {
    const int idx = score_index_vec.front().index;
    bool keep = true;
    for (int k = 0; k < indices->size(); ++k) {
      if (keep) {
        const int kept_idx = indices->at(k);
        T overlap = JaccardOverlap<T>(bboxes[idx], bboxes[kept_idx], true);
        keep = overlap <= adaptive_threshold;

        //LTS5_LOG_DEBUG(k << " overlap: " << overlap);

      } else {
        break;
      }
    }
    if (keep) {
      indices->push_back(idx);
    }
    score_index_vec.erase(score_index_vec.begin());
    if (keep && eta < 1 && adaptive_threshold > 0.5) {
      adaptive_threshold *= eta;
    }
    //LTS5_LOG_DEBUG("Adaptive thresh: " << adaptive_threshold);
  }
}



#pragma mark -
#pragma mark Explicit instantiations

template
void GetMaxScoreIndex(const std::vector<float>&, const float&, const int&,
                      std::vector<ScoreIndex<float>>*);
template
float JaccardOverlap(const NormalizedBBox<float>&,
                     const NormalizedBBox<float>&,
                     const bool&);
template
double JaccardOverlap(const NormalizedBBox<double>&,
                      const NormalizedBBox<double>&,
                      const bool&);
template
void GetMaxScoreIndex(const std::vector<double>&, const double&, const int&,
                      std::vector<ScoreIndex<double>>*);
template
void GetLocPredictions(const float*, const int&, const int&, const int&,
                       const bool&, std::vector<LabelBBox<float>>*);
template
void GetLocPredictions(const double*, const int&, const int&, const int&,
                       const bool&, std::vector<LabelBBox<double>>*);
template
void GetConfidenceScores(const float*, const int&, const int&, const int&,
                         std::vector<ConfScore<float>>*);
template
void GetConfidenceScores(const double*, const int&, const int&, const int&,
                         std::vector<ConfScore<double>>*);
template
void GetPriorBBoxes(const float*, const int&,
                    std::vector<NormalizedBBox<float>>*,
                    std::vector<Vars<float>>*);
template
void GetPriorBBoxes(const double*, const int&,
                    std::vector<NormalizedBBox<double>>*,
                    std::vector<Vars<double>>*);
template
void DecodeBBoxesAll(const std::vector<LabelBBox<float>>&,
                     const std::vector<NormalizedBBox<float>>&,
                     const std::vector<Vars<float>>&,
                     const int&,
                     const bool&,
                     const int&,
                     const int&,
                     const PriorBoxType&,
                     const bool&,
                     const bool&,
                     std::vector<LabelBBox<float>>* all_decode_bboxes);
template
void DecodeBBoxesAll(const std::vector<LabelBBox<double>>&,
                     const std::vector<NormalizedBBox<double>>&,
                     const std::vector<Vars<double>>&,
                     const int&,
                     const bool&,
                     const int&,
                     const int&,
                     const PriorBoxType&,
                     const bool&,
                     const bool&,
                     std::vector<LabelBBox<double>>* all_decode_bboxes);
template
void ApplyNMSFast(const std::vector<NormalizedBBox<float>>&,
                  const std::vector<float>&,
                  const float&,
                  const float&,
                  const float&,
                  const int&,
                  std::vector<int>*);
template
void ApplyNMSFast(const std::vector<NormalizedBBox<double>>&,
                  const std::vector<double>&,
                  const double&,
                  const double&,
                  const double&,
                  const int&,
                  std::vector<int>*);

}  // namespace LTS5