/**
 *  @file   tensorshape_utils.cpp
 *  @brief  Utility functions to check tensor's shape dimensions
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/8/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include "lts5/tensorflow_op/utils/shape.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name  CheckRank
 * @brief Check `t` matches expected rank
 * @param[in] ctx     Op's context
 * @param[in] t       Tensor to validate
 * @param[in] rank    Expected rank
 */
void ShapeUtils::CheckRank(tf::OpKernelContext* ctx,
                           const tf::Tensor& t,
                           const tf::int64& rank) {
  OP_REQUIRES(ctx,
              t.dims() == rank,
              tf::errors::InvalidArgument("rank(T) != ", rank));
}

/*
 * @name  CheckType
 * @brief Check `t` matches expected type
 * @param[in] ctx     Op's context
 * @param[in] t       Tensor to validate
 * @param[in] dtype   Expected data type
 */
void ShapeUtils::CheckType(tf::OpKernelContext* ctx,
                           const tf::Tensor& t,
                           const tf::DataType& dtype) {
  OP_REQUIRES(ctx,
              t.dtype() == dtype,
              tf::errors::InvalidArgument("Invalid type"));
}

/*
 * @name  CheckRankAndType
 * @brief Check `t` matches expected type
 * @param[in] ctx     Op's context
 * @param[in] t       Tensor to validate
 * @param[in] rank    Expected rank
 * @param[in] dtype   Expected data type
 */
void ShapeUtils::CheckRankAndType(tf::OpKernelContext* ctx,
                                  const tf::Tensor& t,
                                  const tf::int64& rank,
                                  const tf::DataType& dtype) {
  OP_REQUIRES(ctx,
              t.dims() == rank,
              tf::errors::InvalidArgument("rank(T) != ", rank));
  OP_REQUIRES(ctx,
              t.dtype() == dtype,
              tf::errors::InvalidArgument("Invalid type"));
}

/*
 * @name  CheckShape
 * @brief Check dimensions matches expected value/rank/type for a given tensor
 * @param[in] ctx     Op's context
 * @param[in] t       Tensor to validate
 * @param[in] rank    Expected rank
 * @param[in] dtype   Expected data type
 * @param[in] dims    List of expected dimensions, `-1` means unknown
 */
void ShapeUtils::CheckShape(tf::OpKernelContext *ctx,
                            const tf::Tensor& t,
                            const tf::int64 &rank,
                            const tf::DataType& dtype,
                            const std::vector<tf::int64> &dims) {
  const auto& shp = t.shape();
  // Check rank
  OP_REQUIRES(ctx,
              shp.dims() == rank,
              tf::errors::InvalidArgument("rank(T) != ", rank));
  // Check dims
  for (size_t i = 0; i < dims.size(); ++i) {
    OP_REQUIRES(ctx,
                (shp.dim_size(i) == dims[i]) || (dims[i] == -1),
                tf::errors::InvalidArgument(i, "th dimension doesn't match: ",
                                            dims[i],
                                            "!=",
                                            shp.dim_size(i)));
  }
  // Check type
  OP_REQUIRES(ctx,
              t.dtype() == dtype,
              tf::errors::InvalidArgument("Invalid type"));
}

}  // namespace LTS5