/**
 *  @file   surface_normal.cpp
 *  @brief  Compute surface's normals of a given "mesh"
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/5/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>
#include <algorithm>

#include "lts5/tensorflow_op/surface_normal_grad.hpp"

#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/lib/core/errors.h"

#include "lts5/tensorflow_op/utils/shape.hpp"
#include "lts5/tensorflow_op/tensor_utils.hpp"
#include "lts5/tensorflow_op/file_io.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/geometry/mesh.hpp"

#ifdef GOOGLE_CUDA
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
#endif

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/*
 * @class   NormalGradientHelper
 * @brief   Compute surface's normals gradient
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct NormalGradientHelper<CPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in,out] ctx Op's context
   * @param[in] g_normal  Back propagated gradient
   * @param[in] vertex  Surface's vertices
   * @param[in] conn    Connectivity
   * @param[in] normal  Surface's normals
   * @param[in] scaling Surface's normals scaling (norm before normalization)
   * @param[out] grad   Surface's normals gradient
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_normal,
                 const tf::Tensor* vertex,
                 const tf::Tensor* conn,
                 const tf::Tensor* normal,
                 const tf::Tensor* scaling,
                 tf::Tensor* grad) {
    return -1;
  }
};

}  // namespace Functor
}  // namespace LTS5



#pragma mark -
#pragma mark Initialization

/**
 * @name
 * @brief Build connectivity map for a given triangulation.
 * @param[in,out] ctx          Op's context
 * @param[in]     vertex       Vertex [Batch x 3N]
 * @param[in]     triangle     Triangulation [Tx3]
 * @param[out]    connectivity Connectivity map [N x max(2x#Edge)]
 */
template<typename Device, typename T>
static void BuildConnectivity(tf::OpKernelContext* ctx,
                              const tf::Tensor* vertex,
                              const tf::Tensor* triangle,
                              tf::PersistentTensor* connectivity) {

  using EIntMatrix = typename LTS5::IO<T>::EIntMatrix;
  using TensorMap = typename tensorflow::TTypes<int>::Tensor;
  using CTensorMap = typename tensorflow::TTypes<int>::ConstTensor;
  using CopyFuncD2H = LTS5::Functor::DenseOp<Device, int, LTS5::Functor::DenseOpType::kD2HCopy>;
  using CopyFuncH2D = LTS5::Functor::DenseOp<Device, int, LTS5::Functor::DenseOpType::kH2DCopy>;
  using CVecI = const std::vector<int>;
  using Mesh = LTS5::Mesh<T, LTS5::Vector3<T>>;
  using Triangle = typename Mesh::Triangle;
  using Vertex = typename Mesh::Vertex;
  // Move triangulation to host memory
  const auto& device = ctx->eigen_device<Device>();
  std::vector<Triangle> htri(triangle->dim_size(0));
  {
    auto in = triangle->flat<int>();
    auto out = TensorMap((int*)htri.data(), htri.size() * 3);
    CopyFuncD2H()(device, in, out);
  }
  int n_vertex = static_cast<int>(vertex->dim_size(1));
  // Compute connectivity
  Mesh m;
  m.set_triangle(htri);
  m.set_vertex(std::vector<Vertex>(n_vertex));
  m.BuildConnectivity();
  // Find largest neighborhood
  const auto& conn = m.get_connectivity();
  auto it_conn = std::max_element(conn.begin(),
                                  conn.end(),
                                  [](CVecI& a, CVecI& b) -> bool {
                                    return a.size() < b.size();
                                  });
  size_t max_conn = it_conn->size();
  // Store it into Eigen matrix
  EIntMatrix econn = EIntMatrix::Zero(conn.size(), max_conn + 1);
  for (size_t v = 0; v < n_vertex; ++v) {
    // Fill connectivity for each vertex
    const auto& edges = conn[v];
    const size_t sz = edges.size();
    econn(v, 0) = static_cast<int>(sz);
    // Add element
    for (size_t i = 0; i < sz; ++i) {
      econn(v, i + 1) = edges[i];
    }
  }
  // Init persistent storage + copy
  tf::Tensor* buffer;
  tf::AllocatorAttributes attr;
  attr.set_gpu_compatible(true);
  auto s = ctx->allocate_persistent(tf::DataTypeToEnum<int>::value,
                                    {econn.rows(), econn.cols()},
                                    connectivity,
                                    &buffer,
                                    attr);
  if (s.ok()) {
    auto map = CTensorMap(econn.data(), econn.rows() * econn.cols());
    CopyFuncH2D()(device, map, buffer->flat<int>());




  } else {
    ctx->CtxFailureWithWarning(__FILE__, __LINE__, s);
  }
}

/*
 * @name  SurfaceNormalGradKernel
 * @fn    explicit SurfaceNormalGradKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
SurfaceNormalGradKernel<Device, T>::
SurfaceNormalGradKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx) {}

/*
 * @name  ~SurfaceNormalGradKernel
 * @fn    ~SurfaceNormalGradKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
SurfaceNormalGradKernel<Device, T>::~SurfaceNormalGradKernel() {}

#pragma mark -
#pragma mark Usage

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void SurfaceNormalGradKernel<Device, T>::Compute(tf::OpKernelContext* ctx) {
  using GradientHelper = LTS5::Functor::NormalGradientHelper<Device, T>;
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);

  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* g_normal = nullptr;
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* triangle = nullptr;
  const tf::Tensor* normal = nullptr;
  const tf::Tensor* scaling = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("g_normal", &g_normal));
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("triangle", &triangle));
  OP_REQUIRES_OK(ctx, ctx->input("normal", &normal));
  OP_REQUIRES_OK(ctx, ctx->input("scaling", &scaling));

  // Sanity check
  LTS5::ShapeUtils::CheckShape(ctx,
                               *vertex,
                               3,
                               tf::DataTypeToEnum<T>::v(),
                               {-1, -1, 3});
  LTS5::ShapeUtils::CheckShape(ctx,
                               *triangle,
                               2,
                               tf::DataTypeToEnum<int>::v(),
                               {-1, 3});

  // Connectivity already build ?
  if (this->conn_.NumElements() == 0) {
    LTS5_LOG_DEBUG("Build connectivity");
    BuildConnectivity<Device, T>(ctx, vertex, triangle, &this->conn_);
  }

  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* grad;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("grad",
                                      vertex->shape(),
                                      &grad));
  const auto* conn = this->conn_.AccessTensor(ctx);
  int err  = GradientHelper()(ctx,
                              g_normal,
                              vertex,
                              conn,
                              normal,
                              scaling,
                              grad);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in grad kernel"));
  }
}

#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;

REGISTER_OP("SurfaceNormalOpGrad")
        .Input("g_normal: T")
        .Input("vertex: T")
        .Input("triangle: int32")
        .Input("normal: T")
        .Input("scaling: T")
        .Output("grad: T")
        .Attr("T: {float}")
        .SetIsStateful()
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          std::vector<ShapeHandle> input_shp;
          auto s = ctx->input("vertex", &input_shp);
          s = ctx->set_output("grad", input_shp);
          return s;
        })
        .Doc(R"doc(Compute surface's normals gradient at each vertex
g_normal: Back-propagated gradient up to this Op.
vertex: Stacked vertex position [Batch, N, 3].
triangle: Triangulation of the surface shared for all meshes [K, 3]
normal: Computed stacked normals [Batch, N, 3]
scaling: Norm of the normal before normalization to unit length [Batch, N]
grad: Gradient of normal with respect to vertex [Batch, 3xN])doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("SurfaceNormalOpGrad")               \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      SurfaceNormalGradKernel<CPUDevice, Type>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(Type)                                                 \
  REGISTER_KERNEL_BUILDER(                                                 \
      Name("SurfaceNormalOpGrad")                                          \
      .Device(tf::DEVICE_GPU)                                              \
      .TypeConstraint<Type>("T"),                                          \
      SurfaceNormalGradKernel<GPUDevice, Type>);
REGISTER_GPU(float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA
