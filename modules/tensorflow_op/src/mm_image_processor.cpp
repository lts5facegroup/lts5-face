/**
 *  @file   mm_image_processor.cpp
 *  @brief  Utility class to compute various data in the image space
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   4/9/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include "lts5/tensorflow_op/mm_image_processor.hpp"
#include "lts5/utils/logger.hpp"


using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @struct  MMImageProcessor
 * @brief   Utility class to compute various data in the image space.
 *          Specialisation for CPU device
 * @author  Christophe Ecabert
 * @date    09.04.19
 * @ingroup tensorflow_op
 * @tparam T        Data type
 */
template<typename T>
class MMImageProcessor<CPUDevice, T> {
 public:
  /** Tensor type */
  using Tensor = tensorflow::Tensor;
  /** Context type */
  using OpKernelContext = tensorflow::OpKernelContext;

  /**
   * @name    ComputeSilhouetteEdges
   * @brief   Define the type of pixel for a rendered image
   *          {Exterior, Interior, Border}
   * @param[in] ctx   Operation context
   * @param[in] bcoords   Barycentric coordinates
   * @param[in] params    Model's parameters
   * @param[in] storage   Persistent storage
   * @param[out] boundaries    Computed boundaries
   * @return -1 if error, 0 otherwise
   */
  static int ComputeSilhouetteEdges(OpKernelContext* ctx,
                                    const Tensor& bcoords,
                                    Tensor* boundaries) {
    LTS5_LOG_ERROR("Not implemented");
    return -1;
  }


  /**
   * @name  ComputeSilhouetteEdges
   * @brief Compute vertex indexes located on the contour of the surface
   * @param[in] ctx         Op's context
   * @param[in] vertex      Vertex array slice
   * @param[in] normal      Normal array slice
   * @param[in] triangle    Triangles
   * @param[in] fpe         Face-per-edge map
   * @param[in] vpe         Vertex-per-edge map
   * @param[in] n_face      Number of face to check, if all set -1
   * @param[out] silhouette_edge_idx    Silhouette indexes
   * @return -1 if error, 0 otherwise
   */
  static int ComputeSilhouetteEdges(OpKernelContext* ctx,
                                    const Tensor& vertex,
                                    const Tensor& triangle,
                                    const Tensor& fpe,
                                    const Tensor& vpe,
                                    const int& n_face,
                                    Tensor* silhouette_edge_idx) {
    LTS5_LOG_ERROR("Not implemented");
    return -1;
  }

  /**
   * @name  CorrectBoundaryMap
   * @brief Correct rendered edge map. This is needed since rendered boundary
   *        map are one pixel off.
   * @param[in] ctx         Op's context
   * @param[in] image_edge  Rendered edges
   * @param[out] boundaries Corrected boundary map
   * @return -1 if error, 0 otherwise
   */
  static int CorrectBoundaryMap(OpKernelContext* ctx,
                                const Tensor& image_edge,
                                Tensor* boundaries) {
    LTS5_LOG_ERROR("Not implemented");
    return -1;
  }

  /**
   * @name ComputeSpatialGradient
   * @brief Compute image spatial gradient in both direction {x,y}
   * @param[in] ctx   Operation context
   * @param[in] img_synth Color images from which to compute gradient (Synth)
   * @param[in] img_label True images from which to compute gradient, used for
   *                      border in order to have proper derivative (influenced
   *                      by background)
   * @param[in] boundaries  Object boundaries map
   * @param[out] grad Gradient in X/Y direction
   * @return -1 if error, 0 otherwise
   */
  static int ComputeSpatialGradient(OpKernelContext* ctx,
                                    const Tensor& img_synth,
                                    const Tensor& img_label,
                                    const Tensor& boundaries,
                                    Tensor* grad) {
    LTS5_LOG_ERROR("Not implemented");
    return -1;
  }

  /**
   * @name ComputeObjectMask
   * @brief Compute binary mask where the object lies
   * @param[in] ctx     Op context
   * @param[in] bcoord  Barycentric coordinates (4D Tensor)
   * @param[in] mask    Generated mask with min/max texture value explainable by
   *                    the model
   * @return -1 if error, 0 otherwise
   */
  static int ComputeObjectMask(OpKernelContext* ctx,
                               const Tensor& bcoord,
                               Tensor* mask) {
    LTS5_LOG_ERROR("Not implemented");
    return -1;
  }
};

#pragma mark -
#pragma mark Explicit Instantiation

/** Image processor - CPU */
template class MMImageProcessor<CPUDevice, float>;
/** Image processor - CPU */
template class MMImageProcessor<CPUDevice, double>;

}  // namespace LTS5
