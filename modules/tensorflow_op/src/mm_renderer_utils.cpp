/**
 *  @file   mm_renderer_utils.cpp
 *  @brief  Utility function for mm_renderer
 *  @ingroup tensforflow_op
 *
 *  @author Christophe Ecabert
 *  @date   5/2/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <vector>
#include <string>

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/tensor_types.h"

#include "lts5/tensorflow_op/mm_renderer_utils.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/matrix.hpp"
#include "lts5/utils/math/quaternion.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @struct  FSRenderSelector
 * @brief   Shader selector for a given device known at compilation.
 *          CPU Specialization
 * @author  Christophe Ecabert
 * @date    8/9/18
 * @tparam D    Device on which the computation is run
 */
template<>
struct FSRenderSelector<CPUDevice> {
  static constexpr const char* str =
          "#version 330\n"
          "in vec3 normal0;\n"
          "in vec3 color0;\n"
          "layout (std140) uniform ColorTransform {\n"
          "  mat4 M;\n"
          "  vec4 off;\n"
          "  vec4 sh_coef[9];\n"
          "};\n"
          "out vec3 fragColor;\n"
          "vec3 ComputeShCoef(in vec3 n, in vec4 sh_coef[9]) {\n"
          "  vec3 sh;\n"
          "  float i_coef[9];\n"
          "  i_coef[0] = 1.f;\n"
          "  i_coef[1] = n.x;\n"
          "  i_coef[2] = n.y;\n"
          "  i_coef[3] = n.z;\n"
          "  i_coef[4] = n.x * n.y;\n"
          "  i_coef[5] = n.x * n.z;\n"
          "  i_coef[6] = n.y * n.z;\n"
          "  i_coef[7] = (n.x * n.x) - (n.y * n.y);\n"
          "  i_coef[8] = (3.f * n.z * n.z) - 1.f;\n"
          "  for (int k = 0; k < 9; ++k) {\n"
          "    sh += i_coef[k] * sh_coef[k].xyz;\n"
          "  }\n"
          "  return sh;\n"
          "}\n"
          "void main() {\n"
          "  vec3 sh = ComputeShCoef(normal0, sh_coef);\n"
          "  vec4 c0 = vec4(sh * color0, 1.f);\n"
          "  vec4 c_f = (M * c0) + off;\n"
          "  fragColor = clamp(c_f.rgb, 0.f, 1.f);\n"
          "}";
};



/**
 * @class   MapOGLResource
 * @brief   Map a specific OpenGL Ressource to a pointer accessible for a given
 *          device
 *          CPU - Specialization
 * @author  Christophe Ecabert
 * @date    15/01/2018
 * @tparam T        Data type
 */
template<typename RType, typename MType>
class MapOGLResource<CPUDevice, RType, MType> {
 public:

  /** Renderer type */
  using Renderer = LTS5::OGLOffscreenRenderer<RType, LTS5::Vector3<RType>>;
  /** Buffer Type */
  using BufferIdx = typename Renderer::BufferIdx;

  /*
   * @name  MapOGLResource
   * @fn    MapOGLResource()
   * @brief Constructor
   */
  MapOGLResource() : resource_(BufferIdx::kVertex),
                     renderer_(nullptr) {
  }

  /*
   * @name  Init
   * @fn void Init(const BufferIdx& resource, const Renderer* renderer)
   * @brief Initialize mapper
   * @param[in] resource    Resource index to map
   * @param[in] renderer    Renderer
   */
  void Init(const BufferIdx& resource, const Renderer* renderer) {
    resource_ = resource;
    renderer_ = renderer;
  }

  /*
   * @name  ~MapOGLResource
   * @fn    ~MapOGLResource()
   * @brief Destructor
   */
  ~MapOGLResource() {}

  /**
   * @name  Map()
   * @fn    T* Map()
   * @brief Map opengl resource to a pointer usable for this device
   * @param[in] ctx Op context
   * @return    Pointer to the mapped resource if success, nullptr otherwise
   */
  MType* Map(tensorflow::OpKernelContext* ctx) {
    if (renderer_) {
      return reinterpret_cast<MType*>(renderer_->Map(resource_));
    } else {
      return nullptr;
    }
  }

  /**
   * @name  Unmap
   * @fn    void Unmap(tensorflow::OpKernelContext* ctx)
   * @param[in] ctx Op context
   * @brief Unmap the underlying resources
   */
  void Unmap(tensorflow::OpKernelContext* ctx) {
    if (renderer_) {
      renderer_->Unmap(resource_);
    }
  }

 private:
  /** Resource */
  BufferIdx resource_;
  /** Renderer */
  const Renderer* renderer_;
};

/*
 * @class   DownloadImage
 * @brief   Retrieve image from an OpenGL renderer a place it into a tensor
 * @author  Christophe Ecabert
 * @date    25/01/18
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class DownloadImage<CPUDevice, T> {
 public:
  /** Renderer type */
  using Renderer = LTS5::OGLOffscreenRenderer<T, LTS5::Vector3<T>>;

  /**
   * @name  DownloadImage
   * @fn    explicit DownloadImage()
   * @brief Constructor
   * @param[in] renderer Render object to download from
   */
  DownloadImage() : renderer_(nullptr) {}

  /**
   * @name  ~DownloadImage
   * @fn    ~DownloadImage()
   * @brief Destrutcor
   */
  ~DownloadImage() {}

  /*
   * @name  Init
   * @fn    void Init(const Renderer* renderer)
   * @brief Initialize downloader object
   * @param[in] renderer Render object to download from
   */
  void Init(const Renderer* renderer) {
    renderer_ = renderer;
  }

  /*
   * @name  operator()
   * @fn    int operator()(tensorflow::OpKernelContext* ctx, tensorflow::Tensor* image)
   * @brief Copy image to the tensor
   * @param[in] ctx Op context
   * @param[out] image  Image where to copy the data
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx, tensorflow::Tensor* image) {
    T* buffer = image->unaligned_flat<T>().data();
    return renderer_->GetImage(buffer);
  }

  /*
   * @name  operator()
   * @fn    int operator()(tf::OpKernelContext* ctx,
                           const tf::Tensor& background,
                           tf::Tensor* image)
   * @brief Transfer image from OpenGL buffer to cuda array and sitch background
   *        at the same time.
   * @param[in] ctx
   * @param[in] background
   * @param[out] image
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& background,
                 tf::Tensor* image) {
    return -1;
  }

 private:
  /** Renderer */
  const Renderer* renderer_;
};

/**
 * @class   DownloadBoundaryImage
 * @brief   Retrieve edge image from an OpenGL renderer a place it into a tensor
 * @author  Christophe Ecabert
 * @date    15/04/19
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class DownloadBoundaryImage<CPUDevice, T>{
 public:
  /** Renderer type */
  using Renderer = LTS5::OGLOffscreenRenderer<T, LTS5::Vector3<T>>;

  /**
   * @name  DownloadBoundaryImage
   * @fn    explicit DownloadImage()
   * @brief Constructor
   */
  DownloadBoundaryImage() : renderer_(nullptr) {}

  /**
   * @name  ~DownloadBoundaryImage
   * @fn    ~DownloadBoundaryImage()
   * @brief Destrutcor
   */
  ~DownloadBoundaryImage() {}

  /**
   * @name  Init
   * @fn    void Init(const Renderer* renderer)
   * @brief Initialize downloader object
   * @param[in] renderer Render object to download from
   */
  void Init(const Renderer* renderer) {
    renderer_ = renderer;
  }

  /**
   * @name  operator()
   * @fn    int operator()(tensorflow::OpKernelContext* ctx,
                           tensorflow::Tensor* boundaries)
   * @brief Copy image to the tensor
   * @param[in] ctx Op context
   * @param[out] boundaries  Image where to copy the data
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 tensorflow::Tensor* boundaries) {
    T* buffer = boundaries->unaligned_flat<T>().data();
    return renderer_->GetImage(buffer);
  }

 private:
  /** Renderer */
  const Renderer* renderer_;
};

/*
 * @class   UpdateOglPrimitive
 * @brief   Update VAO buffer (vertex, normal, color)
 * @author  Christophe Ecabert
 * @date    15/04/18
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class UpdateOglBuffer<CPUDevice, T> {
 public:
  /** Ogl Mapper */
  using Mapper = MapOGLResource<CPUDevice, T, T>;

  /*
   * @name  operator()
   * @brief Update opengl buffers (VAOs)
   * @param[in] ctx Op's context
   * @param[in] idx Surface's index that need to be push to opengl buffers
   * @param[in] storage Morphable Model shared storage
   * @param[in] vertex_map  Vertex mapper to OpenGL VAO's buffer
   * @param[in] normal_map  Normal mapper to OpenGL VAO's buffer
   * @param[in] vertex_color_map    Vertex Color mapper to OpenGL VAO's buffer
   * @return -1 if error, 0 otherwise
   */
  /*int operator()(tensorflow::OpKernelContext* ctx,
                 const size_t& idx,
                 Storage* storage,
                 Mapper* vertex_map,
                 Mapper* normal_map,
                 Mapper* vertex__color_map) {
    return -1;
  }*/

  /*
   * @name  operator()
   * @brief Update opengl buffers (VAOs)
   * @param[in] ctx Op's context
   * @param[in] vertex  Vertex array (slice)
   * @param[in] normal  Normal array (slice)
   * @param[in] color   Color array (slice)
   * @param[in] vertex_map  Vertex mapper to OpenGL VAO's buffer
   * @param[in] normal_map  Normal mapper to OpenGL VAO's buffer
   * @param[in] vertex_color_map    Vertex Color mapper to OpenGL VAO's buffer
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& vertex,
                 const tf::Tensor& normal,
                 const tf::Tensor& color,
                 Mapper* vertex_map,
                 Mapper* normal_map,
                 Mapper* vertex_color_map) {
    return -1;
  }
};

/**
 * @class   UpdateOglPrimitive
 * @brief   Update VBO's element index from a given array
 * @author  Christophe Ecabert
 * @date    15/04/18
 * @tparam T        Data type
 */
template<typename T>
class UpdateOglEdgeIndex<CPUDevice, T> {
 public:
  /** Ogl Mapper */
  using Mapper = MapOGLResource<CPUDevice, T, unsigned int>;
  /**
   * @name  operator()
   * @param[in] ctx         Op's context
   * @param[in] edge_list   List of edges to push to OpenGL
   * @param[in] offset      Offset from where to start in the index array
   * @param[in] mapper      Mapper to OpenGL's VBO
   * @return    Number of element pushed to the index array
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const tensorflow::Tensor& edge_list,
                 const size_t& offset,
                 Mapper& mapper) {
    return -1;
  }
};

/**
 * @class   UploadTriangle
 * @brief   Update VAO with triangles
 * @author  Christophe Ecabert
 * @date    15/07/2020
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class UploadTriangle<CPUDevice, T> {
 public:
  /** Ogl Face Mapper */
  using Mapper = MapOGLResource<CPUDevice, T, unsigned int>;

  /**
   * @name  operator()
   * @param[in] ctx         Op's context
   * @param[in] edge_list   Triangles to upload
   * @param[in] mapper      Mapper to OpenGL's VBO
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& triangle,
                 Mapper& mapper) {
    return -1;
  }
};

/**
 * @class   UploadAttribute
 * @brief   Update VAO with a given attributes (vertex, normal, color, ...)
 * @author  Christophe Ecabert
 * @date    15/07/2020
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class UploadAttribute<CPUDevice, T> {
 public:
  /** Ogl Face Mapper */
  using Mapper = MapOGLResource<CPUDevice, T, T>;

  /**
   * @name  operator()
   * @param[in] ctx         Op's context
   * @param[in] attribute   Attributes to upload
   * @param[in] mapper      Mapper to OpenGL's VBO
   * @return    -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor& attributes,
                 Mapper& mapper) {
    return -1;
  }
};

}  // namespace Functor

#pragma mark -
#pragma mark Edge Rendering

static constexpr const char* edge_vs_str = "#version 330\n"
                     "layout (location = 0) in vec3 vertex;\n"
                     "layout (location = 1) in vec3 normal;\n"
                     "layout (std140) uniform Transform {\n"
                     "  mat4 model;\n"
                     "  mat4 projection;\n"
                     "};\n"
                     "out vec3 normal0;\n"
                     "void main() {\n"
                     "  gl_Position = projection * model * vec4(vertex, 1.f);\n"
                     "  normal0 = normal;\n"
                     "}";

static constexpr const char* edge_fs_str = "#version 330\n"
                     "in vec3 normal0;\n"
                     "out vec4 fragColor;\n"
                     "void main() {\n"
                     "  fragColor = vec4(0.f, 1.f, 0.f, 0.5f);\n"
                     "}";

/*
 * @name  CreateEdgeShader
 * @fn    static OGLProgram* CreateEdgeRenderingShader()
 * @brief Create GLSL programm to render Edge silhouette
 * @return    GLSL Program
 */
OGLProgram* CreateEdgeRenderingShader() {
  std::vector<OGLShader> shaders;
  shaders.emplace_back(OGLShader(edge_vs_str,
                                 OGLShader::ShaderType::kVertex));
  shaders.emplace_back(OGLShader(edge_fs_str,
                                 OGLShader::ShaderType::kFragment));
  // Define attributes
  using Attributes = OGLProgram::Attributes;
  using Types = OGLProgram::AttributeType;
  std::vector<Attributes> attributes;
  attributes.emplace_back(Attributes(Types::kPoint, 0, "vertex"));
  attributes.emplace_back(Attributes(Types::kNormal, 1, "normal"));
  // Create shader
  auto* p = new OGLProgram(shaders, attributes);
  // Bind to UBO
  int err = p->SetUniformBlockBinding("Transform", 0);
  if (err) {
    LTS5_LOG_ERROR("Can't setup UBO");
  }
  return p;
}

/*
 * @name  CreateEdgeShader
 * @fn    static OGLProgram* CreateEdgeRenderingShader()
 * @brief Create GLSL programm to render Edge silhouette
 * @return    GLSL Program
 */
template<typename T>
OGLProgram* CreateEdgeRenderingShader(const MMRendererParameters<T>& params) {
  using Mat4 = Matrix4x4<T>;

  static constexpr const char* e_vs_str =
          "#version 330\n"
          "layout (location = 0) in vec3 vertex;\n"
          "layout (location = 1) in vec3 normal;\n"
          "uniform mat4 projection_p;\n"
          "out vec3 normal0;\n"
          "void main() {\n"
          "  gl_Position = projection_p * vec4(vertex, 1.f);\n"
          "  normal0 = normal;\n"
          "}";
  static constexpr const char* e_fs_str =
          "#version 330\n"
          "in vec3 normal0;\n"
          "out vec4 fragColor;\n"
          "void main() {\n"
          "  fragColor = vec4(0.f, 1.f, 0.f, 0.5f);\n"
          "}";


  std::vector<OGLShader> shaders;
  shaders.emplace_back(OGLShader(e_vs_str,
                                 OGLShader::ShaderType::kVertex));
  shaders.emplace_back(OGLShader(e_fs_str,
                                 OGLShader::ShaderType::kFragment));
  // Define attributes
  using Attributes = OGLProgram::Attributes;
  using Types = OGLProgram::AttributeType;
  std::vector<Attributes> attributes;
  attributes.emplace_back(Attributes(Types::kPoint, 0, "vertex"));
  attributes.emplace_back(Attributes(Types::kNormal, 1, "normal"));
  // Create shader
  auto* p = new OGLProgram(shaders, attributes);
  // Add projection matrix since it has to be initialized once.
  // http://kgeorge.github.io/2014/03/08/calculating-opengl-perspective-matrix-from-opencv-intrinsic-matrix
  Mat4 P;  // Projection matrix
  T cx = static_cast<T>(params.width) / T(2.0);
  T cy = static_cast<T>(params.height) / T(2.0);
  P(0, 0) = params.focal / cx;
  P(1, 1) = params.focal / cy;
  P(2, 2) = -(params.far + params.near) / (params.far - params.near);
  P(2, 3) = -(T(2.0) * params.far * params.near) / (params.far - params.near);
  P(3, 2) = T(-1.0);
  // Push to shader
  p->Use();
  p->SetUniform("projection_p", P);
  p->StopUsing();
  return p;
}

#pragma mark -
#pragma mark Forward Rendering Shader

/*
 * @name  MMRenderingShader
 * @fn    MMRenderingShader(const MMRendererParameters<T>& p)
 * @brief Constructor
 * @param[in] p   Rendering parameters
 */
template<typename T>
MMRenderingShader<CPUDevice, T>::
MMRenderingShader(const MMRendererParameters<T>& p) : ubo_(nullptr),
                                                      shader_(nullptr),
                                                      focal_(p.focal) {
  using Mat4 = Matrix4x4<T>;
  try {
    std::vector<OGLShader> shaders;
    shaders.emplace_back(OGLShader(vs_str,
                                   OGLShader::ShaderType::kVertex));
    shaders.emplace_back(OGLShader(Functor::FSRenderSelector<CPUDevice>::str,
                                   OGLShader::ShaderType::kFragment));
    // Define attributes
    using Attributes = OGLProgram::Attributes;
    using Types = OGLProgram::AttributeType;
    std::vector<Attributes> attributes;
    attributes.emplace_back(Attributes(Types::kPoint, 0, "vertex"));
    attributes.emplace_back(Attributes(Types::kNormal, 1, "normal"));
    attributes.emplace_back(Attributes(Types::kColor, 2, "color"));
    this->shader_ = new OGLProgram(shaders, attributes);
  } catch (const LTS5::ProcessError &e) {
    std::string msg = "OpenGL failed to initialize shader: ";
    msg += std::string(e.what());
    throw LTS5::ProcessError(ProcessError::ProcessErrorEnum::kErr,
                             msg,
                             FUNC_NAME);
  }
  // Define binding point
  int err = this->shader_->SetUniformBlockBinding("Transform", 0);
  err |= this->shader_->SetUniformBlockBinding("ColorTransform", 1);
  if (err) {
    LTS5_LOG_ERROR("Can not set UBO binding point");
  }
  // Create Uniform buffer
  ubo_ = new UniformBufferObject();
  // Bind0: 2x[4x4] matrix of T
  // Bind0: 1x[4x4] matrix of T + vec4 of T + 9 * vec4 of T
  err |= ubo_->Create({{0, 2 * 16 * sizeof(T)},
                       {1, (16 + (10 * 4)) * sizeof(T)}});
  if (!err) {
    // Add projection matrix since it has to be initialized once.
    // http://kgeorge.github.io/2014/03/08/calculating-opengl-perspective-matrix-from-opencv-intrinsic-matrix
    Mat4 P;  // Projection matrix
    T cx = static_cast<T>(p.width) / T(2.0);
    T cy = static_cast<T>(p.height) / T(2.0);
    P(0, 0) = p.focal / cx;
    P(1, 1) = p.focal / cy;
    P(2, 2) = -(p.far + p.near) / (p.far - p.near);
    P(2, 3) = -(T(2.0) * p.far * p.near) / (p.far - p.near);
    P(3, 2) = T(-1.0);
    auto Pt = P.Transpose();
    err = ubo_->AddData(0, 16*sizeof(T), 16*sizeof(T), Pt.Data());
    if (err) {
      LTS5_LOG_ERROR("Can not initialize projection matrix");
    }
  } else {
    LTS5_LOG_ERROR("Can not initialize UBO");
  }
}

/*
 * @name  ~MMRenderingShader
 * @fn    ~MMRenderingShader() override
 * @brief Destructor
 */
template<typename T>
MMRenderingShader<CPUDevice, T>::~MMRenderingShader() {
  delete ubo_;
  delete shader_;
}

/*
 * @name  UpdateModelTransform
 * @fn    int UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                                   const tensorflow::Tensor& parameter,
                                   const tensorflow::int64& ct_idx,
                                   const tensorflow::int64& cam_idx)
 * @brief Update model transformation matrix from camera parameters
 * @param[in] ctx Op context
 * @param[in] parameter Morphable model parameters
 * @param[in] ct_idx Index where color transformation parameters start
 * @param[in] cam_idx Index where camera parameters start
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MMRenderingShader<CPUDevice, T>::
  UpdateModelTransform(tensorflow::OpKernelContext* ctx,
                       const tensorflow::Tensor& parameter,
                       const tensorflow::int64& i_idx,
                       const tensorflow::int64& ct_idx,
                       const tensorflow::int64& cam_idx) {
  using Mat4 = Matrix4x4<T>;
  using Vec4 = Vector4<T>;
  using Quat = Quaternion<T>;
  // Access camera parameters
  const auto* p_ptr = parameter.unaligned_flat<T>().data();
  const auto* i_ptr = &p_ptr[i_idx];
  const auto* ct_ptr = &p_ptr[ct_idx];
  const auto* cam_ptr = &p_ptr[cam_idx];
  // Construct model matrix
  // -------------------------------------------
  Mat4 M;
  Quat q;
  q.FromEuler(cam_ptr[0], cam_ptr[1], cam_ptr[2]);
  q.ToRotationMatrix(&M);
  M(0, 3) = cam_ptr[3] /* focal_*/;
  M(1, 3) = cam_ptr[4] /* focal_*/;
  M(2, 3) = cam_ptr[5] /* focal_*/;
  auto Mt = M.Transpose();  // OGL is column major :(
  // Update
  int err = ubo_->AddData(0, 0, 16 * sizeof(T), Mt.Data());
  // Construct color transformation matrix
  // -------------------------------------------
  Mat4 ColorM;
  const T& gr = ct_ptr[0];
  const T& gg = ct_ptr[1];
  const T& gb = ct_ptr[2];
  const T& c = ct_ptr[6];
  const T luminance_r = ((T(1.0) - c) * T(0.3));
  const T luminance_g = ((T(1.0) - c) * T(0.59));
  const T luminance_b = ((T(1.0) - c) * T(0.11));
  // Linear transform - first row
  ColorM[0] = gr * (c + luminance_r);
  ColorM[1] = gr * luminance_g;
  ColorM[2] = gr * luminance_b;
  ColorM[3] = T(0.0);
  // Linear transform - second row
  ColorM[4] = gg * luminance_r;
  ColorM[5] = gg * (c + luminance_g);
  ColorM[6] = gg * luminance_b;
  ColorM[7] = T(0.0);
  // Linear transform - third row
  ColorM[8] = gb * luminance_r;
  ColorM[9] = gb * luminance_g;
  ColorM[10] = gb * (c + luminance_b);
  ColorM[11] = T(0.0);
  // Linear transform (Offset) - forth row
  ColorM[12] = ct_ptr[3];
  ColorM[13] = ct_ptr[4];
  ColorM[14] = ct_ptr[5];
  ColorM[15] = T(1.0);
  auto ColorMt = ColorM.Transpose();  // OGL is column major :(
  // SH parameters
  std::vector<Vec4> sh_param;
  for (size_t k = 0; k < 9; ++k) {
    sh_param.emplace_back(i_ptr[k], i_ptr[9 + k], i_ptr[18 + k], T(0.0));
  }
  // Push to gpu
  err |= ubo_->AddData(1,
                       0,
                       16 * sizeof(T),
                       ColorMt.Data());  // linear transform + offset
  err |= ubo_->AddData(1,
                       16 * sizeof(T),
                       36 * sizeof(T) ,
                       sh_param.data());  // Sh parameters
  return err;
}

#pragma mark -
  #pragma mark ImageShader

/** Vertex Shader */
static constexpr const char* vs_str =
        "#version 330\n"
        "layout (location = 0) in vec3 vertex;\n"
        "layout (location = 1) in vec3 normal;\n"
        "layout (location = 2) in vec3 color;\n"
        "uniform mat4 projection;\n"
        "out vec3 normal0;\n"
        "out vec3 color0;\n"
        "void main() {\n"
        "  gl_Position = projection * vec4(vertex, 1.f);\n"
        "  color0 = color;\n"
        "  normal0 = normal;\n"
        "}";

/** Fragment shader */
static constexpr const char* fs_str =
        "#version 330\n"
        "in vec3 normal0;\n"
        "in vec3 color0;\n"
        "out vec4 fragColor;\n"
        "void main() {\n"
        "  fragColor = clamp(vec4(color0, 1.f), 0.f, 1.f);\n"
        "}";

/*
 * @name  ImageShader
 * @fn    ImageShader(const Parameters& p)
 * @brief Constructor
 * @param[in] p   Rendering parameters
 */
template<typename T>
ImageShader<T>::ImageShader(const Parameters& p) : shader_(nullptr) {
  using Mat4 = Matrix4x4<T>;
  try {
    std::vector<OGLShader> shaders;
    shaders.emplace_back(OGLShader(vs_str,
                                   OGLShader::ShaderType::kVertex));
    shaders.emplace_back(OGLShader(fs_str,
                                   OGLShader::ShaderType::kFragment));
    // Define attributes
    using Attributes = OGLProgram::Attributes;
    using Types = OGLProgram::AttributeType;
    std::vector<Attributes> attributes;
    attributes.emplace_back(Attributes(Types::kPoint, 0, "vertex"));
    attributes.emplace_back(Attributes(Types::kNormal, 1, "normal"));
    attributes.emplace_back(Attributes(Types::kColor, 2, "color"));
    this->shader_ = new OGLProgram(shaders, attributes);
  } catch (const LTS5::ProcessError &e) {
    std::string msg = "OpenGL failed to initialize shader: ";
    msg += std::string(e.what());
    throw LTS5::ProcessError(ProcessError::ProcessErrorEnum::kErr,
                             msg,
                             FUNC_NAME);
  }
  // Add projection matrix since it has to be initialized once.
  // http://kgeorge.github.io/2014/03/08/calculating-opengl-perspective-matrix-from-opencv-intrinsic-matrix
  Mat4 P;  // Projection matrix
  T cx = static_cast<T>(p.width) / T(2.0);
  T cy = static_cast<T>(p.height) / T(2.0);
  P(0, 0) = p.focal / cx;
  P(1, 1) = p.focal / cy;
  P(2, 2) = -(p.far + p.near) / (p.far - p.near);
  P(2, 3) = -(T(2.0) * p.far * p.near) / (p.far - p.near);
  P(3, 2) = T(-1.0);
  // Push to shader
  shader_->Use();
  shader_->SetUniform("projection", P);
  shader_->StopUsing();
}

/*
 * @name  ~ImageShader
 * @fn    ~ImageShader() override
 * @brief Destructor
 */
template<typename T>
ImageShader<T>::~ImageShader() {
  if (shader_) {
    delete shader_;
  }
}



#pragma mark -
#pragma mark Explicit Instantiation

template
OGLProgram* CreateEdgeRenderingShader<float>(const MMRendererParameters<float>&);
template
OGLProgram* CreateEdgeRenderingShader<double>(const MMRendererParameters<double>&);

/** Float - CPU - OpenGL Mapper */
template class Functor::MapOGLResource<CPUDevice, float, float>;
/** Double - CPU - OpenGL Mapper */
template class Functor::MapOGLResource<CPUDevice, double, double>;
template class Functor::MapOGLResource<CPUDevice, float, unsigned int>;
template class Functor::MapOGLResource<CPUDevice, double, unsigned int>;

/** Float - CPU - Download */
template class Functor::DownloadImage<CPUDevice, float>;
/** Double - CPU - Download */
template class Functor::DownloadImage<CPUDevice, double>;

/** Float - CPU - Download */
template class Functor::DownloadBoundaryImage<CPUDevice, float>;
/** Double - CPU - Download */
template class Functor::DownloadBoundaryImage<CPUDevice, double>;

/** Float - UpdateOglBuffer */
template class Functor::UpdateOglBuffer<CPUDevice, float>;
/** Float - UpdateOglBuffer */
template class Functor::UpdateOglBuffer<CPUDevice, double>;

/** Float - UpdateOglEdgeIndex */
template class Functor::UpdateOglEdgeIndex<CPUDevice, float>;
/** Double - UpdateOglEdgeIndex */
template class Functor::UpdateOglEdgeIndex<CPUDevice, double>;

/** Float - UploadTriangle */
template class Functor::UploadTriangle<CPUDevice, float>;
/** Double - UpdateOglEdgeIndex */
template class Functor::UploadTriangle<CPUDevice, double>;

/** Float - UploadAttribute */
template class Functor::UploadAttribute<CPUDevice, float>;
/** Double - UpdateOglEdgeIndex */
template class Functor::UploadAttribute<CPUDevice, double>;

/** Float - Forward Rendering Shader */
template class MMRenderingShader<CPUDevice, float>;
/** Double - Forward Rendering Shader */
template class MMRenderingShader<CPUDevice, double>;

/** Float - ImageShader */
template class ImageShader<float>;
/** Double - ImageShader */
template class ImageShader<double>;


}  // namespace LTS5
