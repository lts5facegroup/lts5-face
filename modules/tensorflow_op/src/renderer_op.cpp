/**
 *  @file   renderer_op.cpp
 *  @brief  Rendering operation using standard OpenGL pipeline
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/10/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "tensorflow/core/framework/shape_inference.h"

#include "lts5/tensorflow_op/renderer_op.hpp"
#include "lts5/tensorflow_op/utils/shape.hpp"
#include "lts5/tensorflow_op/mm_image_processor.hpp"


#include "lts5/utils/file_io.hpp"
#include "lts5/tensorflow_op/tensor_utils.hpp"
#include "lts5/tensorflow_op/device_utils.hpp"

#ifdef GOOGLE_CUDA
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
#include "lts5/cuda/tensorflow_op/device_utils.hpp"
#endif


using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

}  // namespace Functor
}  // namespace LTS5

#pragma mark -
#pragma mark Initialization

/*
 * @name  RenderingKernel
 * @fn    explicit RenderingKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
RenderingKernel<Device, T>::
RenderingKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx),
                                                 renderer_(new Renderer()),
                                                 interp_(nullptr),
                                                 train_(false),
                                                 initialized_(false) {
  // Query Hyper parameters
  // -----------------------------------
  OP_REQUIRES_OK(ctx, ctx->GetAttr("near", &r_params_.near));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("far", &r_params_.far));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("focal", &r_params_.focal));
  // See https://gamedev.stackexchange.com/questions/87329
  // for more info about Z-Buffer precision
  const T ratio = r_params_.far / r_params_.near;
  OP_REQUIRES(ctx,
              ratio <= T(100000.0),
              tf::errors::InvalidArgument("Ratio far/near is to high, lead to "
                                          "imprecision in Z-Buffer"));
  OP_REQUIRES_OK(ctx, ctx->GetAttr("width", &r_params_.width));
  OP_REQUIRES(ctx,
              (r_params_.width > 0) &&
              (r_params_.width < tf::kint32max),
              tf::errors::InvalidArgument("width has wrong dimensions"));
  OP_REQUIRES_OK(ctx,
                 ctx->GetAttr("height",
                                  &r_params_.height));
  OP_REQUIRES(ctx,
              (r_params_.height > 0) &&
              (r_params_.height < tf::kint32max),
              tf::errors::InvalidArgument("height has wrong dimensions"));
  OP_REQUIRES_OK(ctx,
                 ctx->GetAttr("visible",
                                  &r_params_.visible));
  int n_face;
  OP_REQUIRES_OK(ctx,
                 ctx->GetAttr("n_face",
                              &n_face));
  r_params_.n_face = n_face == -1 ?
                     std::numeric_limits<size_t>::max() :
                     static_cast<size_t>(n_face);
  // Multisampling
  OP_REQUIRES_OK(ctx,
                 ctx->GetAttr("multisampling", &r_params_.multisampling));
  // Training flag
  OP_REQUIRES_OK(ctx, ctx->GetAttr("training", &this->train_));

  // Init renderer
  OP_REQUIRES(ctx,
              this->renderer_->Init(this->r_params_) == 0,
              tf::errors::Internal("Error while initializing renderer"));
  renderer_->set_clear_color(T(0.0), T(0.0), T(0.0), T(0.0));
  // Create shader
  shader_ = new Shader(this->r_params_);
  edge_shader_ = LTS5::CreateEdgeRenderingShader(this->r_params_);
  renderer_->DisableContext();
  // Create interpolator if needed
  // -----------------------------------
  if (train_) {
    interp_ = new Interpolator();
  }
}

/*
 * @name  ~RenderingKernel
 * @fn    ~RenderingKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
RenderingKernel<Device, T>::~RenderingKernel() {
  LTS5_LOG_DEBUG("DTOR Rendering Kernel");
  if (renderer_) {
    renderer_->ActivateContext();
  }
  delete shader_;
  delete edge_shader_;
  delete renderer_;
  delete interp_;
}

#pragma mark -
#pragma mark Usage

#define MULTIPLE_OF_THREE(x, dim, mult)                  \
  OP_REQUIRES(ctx,                                       \
  x->dim_size(dim) % mult == 0,                          \
  tf::errors::InvalidArgument("Wrong "#x" dims: ",  \
                              x->dim_size(dim),          \
                              "%"#mult" != 0"))

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void RenderingKernel<Device, T>::Compute(tensorflow::OpKernelContext* ctx) {
  using Capability = typename Renderer::Capability;
  using DepthFunc = typename Renderer::DepthFunc;
  using PolyMode = typename Renderer::PolyMode;
  using ImProcessor = LTS5::MMImageProcessor<Device, T>;
  using Shape = LTS5::ShapeUtils;
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);
  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* vertex = nullptr;
  const tf::Tensor* normal = nullptr;
  const tf::Tensor* color = nullptr;
  const tf::Tensor* background = nullptr;
  const tf::Tensor* triangle = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("vertex", &vertex));
  OP_REQUIRES_OK(ctx, ctx->input("normal", &normal));
  OP_REQUIRES_OK(ctx, ctx->input("color", &color));
  OP_REQUIRES_OK(ctx, ctx->input("background", &background));
  OP_REQUIRES_OK(ctx, ctx->input("triangle", &triangle));
  // Sanity check
  Shape::CheckRankAndType(ctx, *vertex, 2, tf::DataTypeToEnum<T>::v());
  Shape::CheckRankAndType(ctx, *normal, 2, tf::DataTypeToEnum<T>::v());
  Shape::CheckRankAndType(ctx, *color, 2, tf::DataTypeToEnum<T>::v());
  Shape::CheckShape(ctx,
                    *background,
                    4,
                    tf::DataTypeToEnum<T>::v(),
                    {-1, r_params_.height, r_params_.width, 3});
  Shape::CheckShape(ctx,
                    *triangle,
                    2,
                    tf::DataTypeToEnum<int>::v(),
                    {-1, 3});
  bool same_bsize = ((vertex->dim_size(0) == normal->dim_size(0)) &&
                     (vertex->dim_size(0) == color->dim_size(0)) &&
                     (vertex->dim_size(0) == background->dim_size(0)));
  OP_REQUIRES(ctx,
              same_bsize,
              tf::errors::InvalidArgument("Inputs haven't same batch size"));
  // Attributes must be a multiple of 3
  MULTIPLE_OF_THREE(vertex, 1, 3);
  MULTIPLE_OF_THREE(normal, 1, 3);
  MULTIPLE_OF_THREE(color, 1, 3);
  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* out_image = nullptr;
  tf::Tensor* out_bcoord = nullptr;
  tf::Tensor* out_spatial_grad = nullptr;
  auto n_img = vertex->dim_size(0);
  auto h = static_cast<tf::int64>(r_params_.height);
  auto w = static_cast<tf::int64>(r_params_.width);
  tf::OpOutputList outputs;
  // Output image has 4 channels, where the last one is a binary indicator
  // for the 3d object position in the image plane
  OP_REQUIRES_OK(ctx, ctx->output_list("image", &outputs));
  OP_REQUIRES_OK(ctx,
                 outputs.allocate(0, {n_img, h, w, 4}, &out_image));
  if (train_) {
    // Barycentric coordinate
    OP_REQUIRES_OK(ctx,
                   outputs.allocate(1, {n_img, h, w, 4}, &out_bcoord));
    // Image spatial gradient similar to OpenDR
    OP_REQUIRES_OK(ctx,
                   outputs.allocate(2, {2*n_img, h, w, 3}, &out_spatial_grad));
  }
  // COMPUTE
  // -------------------------------------------------------
  tf::Tensor edges;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_temp(tf::DataTypeToEnum<tf::uint8>::value,
                                    {n_img, h, w},
                                    &edges));
  renderer_->ActivateContext();
  // Update triangulation here, build VPE-FPE here as well ?
  renderer_->Update(ctx,
                    vertex,
                    normal,
                    color,
                    triangle,
                    shader_->get_shader());
  // Rendering loop for each image in the batch
  for (auto k = 0; k < n_img; ++k) {
    // Get slices
    auto vertex_slice = vertex->Slice(k, k + 1);
    auto normal_slice = normal->Slice(k, k + 1);
    auto color_slice = color->Slice(k, k + 1);
    auto bg_slice = background->Slice(k, k + 1);
    auto img_slice = out_image->Slice(k, k + 1);
    // Setup glsl program + option
    renderer_->set_program(&shader_->get_shader());
    // Render
    renderer_->Enable(Capability::kPolygonOffsetFill);
    renderer_->PolygonOffset(1.0, 5.0);
    renderer_->Render(ctx,
                      vertex_slice,
                      normal_slice,
                      color_slice,
                      bg_slice,
                      true,
                      r_params_.n_face,
                      &img_slice);
    renderer_->Disable(Capability::kPolygonOffsetFill);
    // In training compute the silhouette as well
    if (train_) {
      auto idx = (2 * k);
      auto g_slice = out_spatial_grad->Slice(idx, idx + 2);
      auto edge_slice = edges.Slice(k, k + 1);
      // Switch glsl program + update option
      renderer_->DepthFunction(DepthFunc::kLessOrEqual);
      renderer_->PolygonMode(PolyMode::kLine);
      renderer_->set_program(edge_shader_);
      renderer_->RenderEdges(ctx,
                             vertex_slice,
                             normal_slice,
                             *triangle,
                             false,
                             r_params_.n_face,
                             &edge_slice);
      renderer_->PolygonMode(PolyMode::kFill);
      renderer_->DepthFunction(DepthFunc::kLess);

      // Compute image level spatial gradient
      ImProcessor::ComputeSpatialGradient(ctx,
                                          img_slice,
                                          bg_slice,
                                          edge_slice,
                                          &g_slice);
    }
  }
  // Disable OpenGL
  renderer_->DisableContext();

  if (train_) {
    // lazy init interp
    int err = interp_->Init(*triangle, r_params_);
    OP_REQUIRES(ctx,
                err == 0,
                tf::errors::InvalidArgument("Can not initialize "
                                            "interpolator"));
    interp_->ActivateContext();
    interp_->ComputeBarycentric(ctx,
                                *vertex,
                                *triangle,
                                r_params_.n_face,
                                out_bcoord);
    interp_->DisableContext();
  }
}

#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;
using DimensionHandle = tf::shape_inference::DimensionHandle;

REGISTER_OP("RenderingOp")
        .Input("vertex: T")
        .Input("normal: T")
        .Input("color: T")
        .Input("background: T")
        .Input("triangle: int32")
        .Output("image: n_out * T")
        .Attr("T: {float}")
        .Attr("width: int")
        .Attr("height: int")
        .Attr("near: float = 1e0")
        .Attr("far: float = 1e2")
        .Attr("focal: float = 525.0")
        .Attr("visible: bool = false")
        .Attr("n_face: int = -1")
        .Attr("training: bool = false")
        .Attr("multisampling: int = 4")
        .Attr("n_out: int >= 1 = 1")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          bool train;
          int w, h;
          std::vector<ShapeHandle> shp;
          std::vector<ShapeHandle> v_shp;
          auto s = ctx->input("vertex", &v_shp);
          s = ctx->GetAttr("height", &h);
          s = ctx->GetAttr("width", &w);
          s = ctx->GetAttr("training", &train);
          auto bsize = ctx->Dim(v_shp[0], 0);
          auto im_shp = ctx->MakeShape({bsize,
                                        h,
                                        w,
                                        4});
          shp.push_back(im_shp);
          if (train) {
            // Add barycentric image
            auto bc_shp = ctx->MakeShape({bsize,
                                          h,
                                          w,
                                          4});
            shp.push_back(bc_shp);
            // Add spatial gradient
            DimensionHandle twice_bsize;
            ctx->Multiply(bsize, 2, &twice_bsize);
            auto spa_g_shp = ctx->MakeShape({twice_bsize,
                                             h,
                                             w,
                                             3});
            shp.push_back(spa_g_shp);
          }
          s = ctx->set_output("image", shp);
          return s;
        })
        .Doc(R"doc(Render a given scene
vertex: Geometry of the object to render [Batch, 3N]
normal: Object's normals [Batch, 3N]
color: Vertex colors [Batch, 3N]
background: Image background to render (only used in training)
triangle: Surface triangulation (same for each object)
image: List of rendered images [Batch, H, W, 4]. The alpha channel is used as
  binary mask to indicate where the 3d object is located(Image, Barycentric,
  SPGrad)
width: Width of the rendered image.
height Height of the rendered image.
near: Near plane.
far: Far plane.
focal: Focal length to use to build projection matrix.
visible: Flag to show/hide the OpenGL window used to render.
n_face: Number of face to render, if set to -1 render all the primitives.
training: If `True` indicates training phase and output barycentric coordinates
  and image spatial gradient needed during gradient computation.
multisampling: Multisampling factor to use when rendering the image. 0 disable
  it.
n_out: Number of output tensors genereted by the op. Either `1` or `3` during
  training.
)doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("RenderingOp")                       \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      RenderingKernel<CPUDevice, Type>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(Type)                                                 \
  REGISTER_KERNEL_BUILDER(                                                 \
      Name("RenderingOp")                                                  \
      .Device(tf::DEVICE_GPU)                                              \
      .TypeConstraint<Type>("T"),                                          \
      RenderingKernel<GPUDevice, Type>);
REGISTER_GPU(float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA

