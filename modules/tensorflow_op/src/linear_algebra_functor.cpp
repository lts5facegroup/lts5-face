/**
 *  @file   linear_algebra_Functor.cpp
 *  @brief
 *  @ingroup
 *
 *  @author Christophe Ecabert
 *  @date   18/12/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/tensorflow_op/linear_algebra_functor.hpp"
#include "lts5/tensorflow_op/tensor_utils.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"

#include "lts5/utils/file_io.hpp"


using CPUDevice = Eigen::ThreadPoolDevice;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      Helper function
 */
namespace Functor {

#pragma mark -
#pragma mark Partial Specialization

template<typename T>
class LinearAlgebra<CPUDevice, T> {
 public:

  using Context = tensorflow::OpKernelContext;
  using Tensor = tensorflow::Tensor;
  using Transpose = typename LTS5::LinearAlgebra<T>::TransposeType;
  using TFConstMatrix = typename tensorflow::TTypes<T>::ConstMatrix;
  using TFConstFlat = typename tensorflow::TTypes<T>::ConstFlat;
  using TFConstUnFlat = typename tensorflow::TTypes<T>::UnalignedConstFlat;
  using TFFlat = typename tensorflow::TTypes<T>::Flat;
  using TFUnFlat = typename tensorflow::TTypes<T>::UnalignedFlat;

  static void Gemv(Context* ctx,
                   const Tensor& A,
                   const bool& trans,
                   const T& alpha,
                   const Tensor& x,
                   const T& beta,
                   Tensor* y);

  static void Gemv(Context* ctx,
                   TFConstMatrix& A,
                   const bool& trans,
                   const T& alpha,
                   TFConstFlat& x,
                   const T& beta,
                   TFFlat* y);

  static void Gemv(Context* ctx,
                   TFConstMatrix& A,
                   const bool& trans,
                   const T& alpha,
                   TFConstUnFlat& x,
                   const T& beta,
                   TFUnFlat* y);

  static void Gemm(Context* ctx,
                   const Tensor& A,
                   const bool& trans_a,
                   const T& alpha,
                   const Tensor& B,
                   const bool& trans_b,
                   const T& beta,
                   Tensor* C);

  static void Gemm(Context* ctx,
                   const T* a_ptr,
                   const uint64_t& a_row,
                   const uint64_t& a_col,
                   const uint64_t& a_stride,
                   const bool& trans_a,
                   const T& alpha,
                   const T* b_ptr,
                   const uint64_t& b_row,
                   const uint64_t& b_col,
                   const uint64_t& b_stride,
                   const bool& trans_b,
                   const T& beta,
                   Tensor* C);
};

#pragma mark -
#pragma mark Implementation

template<typename T>
void LinearAlgebra<CPUDevice, T>::Gemv(Context *ctx,
                                       const Tensor &A,
                                       const bool &trans,
                                       const T &alpha,
                                       const Tensor &x,
                                       const T &beta,
                                       Tensor *y) {
  // Convert tensor to opencv matrix
  cv::Mat AMat = ConvertMatrix<CPUDevice, T>::ToOpenCV(A);
  Transpose ta = trans ? Transpose::kTranspose : Transpose::kNoTranspose;
  cv::Mat XVec = ConvertMatrix<CPUDevice, T>::ToOpenCV(x);
  cv::Mat YVec = ConvertMatrix<CPUDevice, T>::ToOpenCV(*y);
  // Call blas function
  LTS5::LinearAlgebra<T>::Gemv(AMat,
                               ta,
                               alpha,
                               XVec,
                               beta,
                               &YVec);
}

template<typename T>
void LinearAlgebra<CPUDevice, T>::Gemv(Context* ctx,
                                       TFConstMatrix& A,
                                       const bool& trans,
                                       const T& alpha,
                                       TFConstFlat& x,
                                       const T& beta,
                                       TFFlat* y) {
  // Convert tensor to opencv matrix
  cv::Mat AMat = ConvertMatrix<CPUDevice, T>::ToOpenCV(A);
  Transpose ta = trans ? Transpose::kTranspose : Transpose::kNoTranspose;
  int x_rows = trans ? AMat.rows : AMat.cols;
  int x_cols = 1;
  int y_rows = trans ? AMat.cols : AMat.rows;
  int y_cols = 1;
  cv::Mat XVec = ConvertMatrix<CPUDevice, T>::ToOpenCV(x, x_rows, x_cols);
  cv::Mat YVec = ConvertMatrix<CPUDevice, T>::ToOpenCV(*y, y_rows, y_cols);
  // Call blas function
  LTS5::LinearAlgebra<T>::Gemv(AMat,
                               ta,
                               alpha,
                               XVec,
                               beta,
                               &YVec);
}

template<typename T>
void LinearAlgebra<CPUDevice, T>::Gemv(Context* ctx,
                                       TFConstMatrix& A,
                                       const bool& trans,
                                       const T& alpha,
                                       TFConstUnFlat& x,
                                       const T& beta,
                                       TFUnFlat* y) {
  // Convert tensor to opencv matrix
  cv::Mat AMat = ConvertMatrix<CPUDevice, T>::ToOpenCV(A);
  Transpose ta = trans ? Transpose::kTranspose : Transpose::kNoTranspose;
  int x_rows = trans ? AMat.rows : AMat.cols;
  int x_cols = 1;
  int y_rows = trans ? AMat.cols : AMat.rows;
  int y_cols = 1;
  cv::Mat XVec = ConvertMatrix<CPUDevice, T>::ToOpenCV(x, x_rows, x_cols);
  cv::Mat YVec = ConvertMatrix<CPUDevice, T>::ToOpenCV(*y, y_rows, y_cols);
  // Call blas function
  LTS5::LinearAlgebra<T>::Gemv(AMat,
                               ta,
                               alpha,
                               XVec,
                               beta,
                               &YVec);
}

template<typename T>
void LinearAlgebra<CPUDevice, T>::Gemm(Context* ctx,
                                       const Tensor& A,
                                       const bool& trans_a,
                                       const T& alpha,
                                       const Tensor& B,
                                       const bool& trans_b,
                                       const T& beta,
                                       Tensor* C) {
  // Convert tensor to opencv matrix
  cv::Mat AMat = ConvertMatrix<CPUDevice, T>::ToOpenCV(A);
  Transpose ta = trans_a ? Transpose::kTranspose : Transpose::kNoTranspose;
  Transpose tb = trans_b ? Transpose::kTranspose : Transpose::kNoTranspose;
  cv::Mat BMat = ConvertMatrix<CPUDevice, T>::ToOpenCV(B);
  cv::Mat CMat = ConvertMatrix<CPUDevice, T>::ToOpenCV(*C);
  // Call blas function
  LTS5::LinearAlgebra<T>::Gemm(AMat,
                               ta,
                               alpha,
                               BMat,
                               tb,
                               beta,
                               &CMat);
}

template<typename T>
void LinearAlgebra<CPUDevice, T>::Gemm(Context* ctx,
                                       const T* a_ptr,
                                       const uint64_t& a_row,
                                       const uint64_t& a_col,
                                       const uint64_t& a_stride,
                                       const bool& trans_a,
                                       const T& alpha,
                                       const T* b_ptr,
                                       const uint64_t& b_row,
                                       const uint64_t& b_col,
                                       const uint64_t& b_stride,
                                       const bool& trans_b,
                                       const T& beta,
                                       Tensor* C) {
  throw std::runtime_error("Method not yet implemented");
}



#pragma mark -
#pragma mark Explicit Instantiation

/** Linear Algebra - Float */
template class LinearAlgebra<CPUDevice, float>;
/** Linear Algebra - Float */
template class LinearAlgebra<CPUDevice, double>;

}  // namepsace Functor
}  // namepsace LTS5
