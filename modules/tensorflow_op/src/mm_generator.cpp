/**
 *  @file   "mm_generator.cpp"
 *  @brief  Tensorflow Op for rendering Morphable Model instance. The input is
 *          the model's coefficients and the output is the generated image
 *  @ingroup    tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   04/01/2018
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <algorithm>
#include <vector>

#define EIGEN_USE_THREADS
#include "tensorflow/core/framework/op_kernel.h"

#include "lts5/tensorflow_op/mm_generator.hpp"
#include "lts5/tensorflow_op/device_utils.hpp"
#include "lts5/tensorflow_op/linear_algebra_functor.hpp"
#include "lts5/utils/math/quaternion.hpp"
#include "lts5/utils/sys/parallel.hpp"

#ifdef GOOGLE_CUDA
#include "lts5/cuda/tensorflow_op/tensor_utils.hpp"
#include "lts5/cuda/utils/safe_call.hpp"
#endif


using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;
namespace tf = tensorflow;


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/*
 * @class   MMGeneratorInitializer
 * @brief   Device specific shape/tex generator initialization. Copy mean shape
 *          tex
 * @author  Christophe Ecabert
 * @date    30/01/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
class MMGeneratorInitializer<CPUDevice, T> {
 public:
  /** Tensor type */
  using Tensor = tensorflow::Tensor;

  /**
   * @name  operator()
   * @brief Copy a given tensor in all rows of the output tensor. Used to copy
   *        mean shape/tex
   * @param[in] ctx         Kernel context
   * @param[in] value       Vector to copy on each row
   * @param[out] output     Where to copy the data
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const Tensor& value,
                 Tensor* output) {
    return -1;
  }
};

/**
 * @class   MMGeneratorNormalizer
 * @brief   Device specific shape/tex generator normalizer. Remove center
 *          gravity of a given shape
 * @author  Christophe Ecabert
 * @date    30/01/2019
 * @ingroup tensorflow_op
 * @tparam T        Data type
 */
template<typename T>
class MMGeneratorNormalizer<CPUDevice, T> {
 public:
  /** Tensor type */
  using Tensor = tensorflow::Tensor;

  /**
   * @name  operator()
   * @brief Remove center of gravity from a given shape
   * @param[in] ctx         Kernel context
   * @param[in] cog         Center of gravity
   * @param[in/out] shape   Shape from which to remove cog
   * @return -1 if error, 0 otherwise
   */
  int operator()(tensorflow::OpKernelContext* ctx,
                 const std::vector<T>& cog,
                 Tensor* shape) {
    return -1;
  }
};

/*
 * @class   MMFinalizer
 * @brief   CPU Finalize implementation
 * @author  Christophe Ecabert
 * @date    04/01/2018
 * @ingroup tensorflow_op
 * @tparam T        Data type
 */
template<typename T>
class MMFinalizer<CPUDevice, T> {
 public:
  /** Tensor type */
  using Tensor = tf::Tensor;
  /** Storage type */
  using Storage = PersistentStorage<T>;

  /*
   * @name  operator()
   * @fn    int operator()(tensorflow::OpKernelContext* ctx,
                           const tensorflow::int64& img_idx,
                           const Tensor& parameters,
                           const Tensor& shape,
                           const Tensor& texture,
                           Storage* storage,
                           T* shape_ogl_buffer,
                           T* normal_ogl_buffer,
                           T* tex_ogl_buffer)
   * @brief Finalize MM instance (i.e. illumination, copy to OGL)
   * @param[in] ctx                 Kernel context
   * @param[in] img_idx             Index of the image being finalized
   * @param[in] model               Morphable model
   * @param[in] parameters          Model's parameters
   * @param[in] shape               Shape instance
   * @param[in] texture             Texture instance
   * @param[in] storage             Persistant storage from manager
   * @param[out] shape_ogl_buffer   Pointer to OpenGL buffer holding vertex
   * @param[out] normal_ogl_buffer  Pointer to OpenGL buffer holding normal
   * @param[out] tex_ogl_buffer     Pointer to OpenGL buffer holding color
   * @return -1 if error, 0 otherwise
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::int64& img_idx,
                 const Tensor& parameters,
                 const Tensor& shape,
                 const Tensor& texture,
                 Storage* storage,
                 T* shape_ogl_buffer,
                 T* normal_ogl_buffer,
                 T* tex_ogl_buffer) {
    using Vertex = Vector3<T>;
    using Vec3 = Vector3<T>;
    using Quat = Quaternion<T>;
    using Mat3 = Matrix3x3<T>;
    // Dimensions
    const auto* model = storage->model;
    const auto ni_start = (tf::int64)(model->get_n_shape_parameter() +
                                      model->get_n_tex_parameter());
    const auto ni = (tf::int64)model->get_n_illu_parameter();
    const auto n_vertex = model->get_n_vertex();
    const auto n_texel = model->get_n_texel();
    const auto n_param = parameters.dim_size(1);
    // Get raw pointer
    const auto* shp_ptr = shape.flat<T>().data();
    const auto* tex_ptr = texture.flat<T>().data();
    const auto* ip_ptr = parameters.flat<T>().data();
    ip_ptr = &ip_ptr[(img_idx * n_param) + ni_start];
    // Retrieve connectivity
    const auto* conn = storage->conn.AccessTensor(ctx);
    // Retrieve normals
    auto* normal = storage->normal.AccessTensor(ctx);
    auto* nrm_ptr = normal->template flat<T>().data();
    // Get input pointer
    const auto* v_ptr = reinterpret_cast<const Vertex*>(&shp_ptr[img_idx *
                                                                 n_vertex *
                                                                 3]);
    const auto c_mat = conn->template matrix<int>();
    const auto* t_ptr = reinterpret_cast<const Vec3*>(&tex_ptr[img_idx *
                                                               n_texel *
                                                               3]);
    // Output pointer
    auto* n_ptr = reinterpret_cast<Vec3*>(&nrm_ptr[img_idx *
                                                   normal->dim_size(1)]);
    auto* ogl_v_ptr = reinterpret_cast<Vertex*>(shape_ogl_buffer);
    auto* ogl_n_ptr = reinterpret_cast<Vec3*>(normal_ogl_buffer);
    auto* ogl_t_ptr = reinterpret_cast<Vec3*>(tex_ogl_buffer);
    // Model pose to transform normal
    const auto nc_start = ni_start + ni;
    const T* cam_p = &(parameters.unaligned_flat<T>().data()[nc_start]);
    Quat q;
    Mat3 rmat;
    q.FromEuler(cam_p[0], cam_p[1], cam_p[2]);
    q.ToRotationMatrix(&rmat);
    // Loop over all vertex
    Parallel::For(n_vertex,
                  [&](const size_t& vi){
      int n_edge = c_mat(vi, 0);
      // Copy vertex to OpenGL
      const Vertex& A = v_ptr[vi];
      ogl_v_ptr[vi] = A;
      // Compute normal
      Vec3 wn;
      for (size_t j = 0; j < n_edge; j += 2) {
        const Vertex& B = v_ptr[c_mat(vi, j + 1)];
        const Vertex& C = v_ptr[c_mat(vi, j + 2)];
        // Define edges AB, AC
        Vec3 AB = B - A;
        Vec3 AC = C - A;
        // Compute surface's normal (triangle ABC)
        Vec3 n = AB ^ AC;
        n.Normalize();
        // Stack each face contribution and weight with angle
        AB.Normalize();
        AC.Normalize();
        T dot = AB * AC;
        dot = dot > T(1.0) ? T(1.0) : dot;
        dot = dot < T(-1.0) ? T(-1.0) : dot;
        const T angle = std::acos(dot);
        wn += (n * angle);
      }
      // normalize and set
      wn.Normalize();
      ogl_n_ptr[vi] = wn;
      n_ptr[vi] = wn;
      // Illumination is added in the fragment shader, therefore juste dump
      // reflectance into ogl buffer
      ogl_t_ptr[vi] = t_ptr[vi];
    });
    return 0;
  }
};

/*
 * @class   NormalGenerator
 * @brief   Compute surface's normals
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct NormalGenerator<CPUDevice, T> {
  /** Storage type */
  using Storage = PersistentStorage<T>;

  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in] ctx         Op's context
   * @param[in,out] storage Morphable Model shared storage
   */
  void operator()(tensorflow::OpKernelContext* ctx, Storage* storage) {}

  /**
   * @name  operator()
   * @brief Compute surface's normal
   * @param[in,out] ctx Op's context
   * @param[in] vertex  Surface's vertcies
   * @param[in] conn    Connectivity
   * @param[out] normal Surface's normals computed
   */
  void operator()(tf::OpKernelContext* ctx,
                  const tf::Tensor* vertex,
                  const tf::Tensor* conn,
                  tf::Tensor* normal) {}
};

/**
 * @class   TriangleGenerator
 * @brief   Copy triangulation to output
 * @author  Christophe Ecabert
 * @date    15/04/2019
 * @ingroup tensorflow_op
 * @tparam T Data type
 */
template<typename T>
struct TriangleGenerator<CPUDevice, T> {
  /**
   * @name operator()
   * @brief Copy triangulation to output
   * @param[in,out] ctx
   * @param[in] triangulation
   * @param[out] tri
   */
  void operator()(tf::OpKernelContext* ctx,
                  const tf::Tensor* triangulation,
                  tf::Tensor* tri) {
  }
};

}  // namespace Functor

#pragma mark -
#pragma mark Generator

/*
 * @name  Generate
 * @fn    void Generate(tf::OpKernelContext* ctx,
                        const tf::Tensor& parameters,
                        Storage* storage,
                        tf::Tensor* shape,
                        tf::Tensor* tex)
 * @brief Generate an instance of a Morphable model
 * @param[in] ctx     Op context
 * @param[in] parameters  Input parameters
 * @param[in] storage Persistant storage
 */
template<typename Device, typename T>
void MMGenerator<Device, T>::Generate(tf::OpKernelContext* ctx,
                                      const tf::Tensor& parameters,
                                      Storage* storage) {
  using LA = Functor::LinearAlgebra<Device, T>;
  // Parameters
  auto ns = (tf::uint64)storage->model->get_n_shape_parameter();  // #Shp param
  auto nt = (tf::uint64)storage->model->get_n_tex_parameter();    // #Tex param
  auto n_img = (tf::uint64)parameters.dim_size(0);                // #Img
  auto np = (tf::uint64)parameters.dim_size(1);                   // #Params
  auto nv = (tf::uint64)storage->model->get_n_vertex() * 3;       // #Vertex
  // Access tensors
  auto* shape = storage->shape.AccessTensor(ctx);
  auto* tex = storage->texture.AccessTensor(ctx);
  const auto* mean_shp = storage->meanshape.AccessTensor(ctx);
  const auto* mean_tex = storage->meantex.AccessTensor(ctx);
  const auto* var_shp = storage->varshape.AccessTensor(ctx);
  const auto* var_tex = storage->vartex.AccessTensor(ctx);
  // Raw ptr on parameters +
  const auto* param_ptr = parameters.flat<T>().data();
  const auto* shp_param_ptr = param_ptr;
  const auto* tex_param_ptr = &param_ptr[ns];
  const auto* var_shp_ptr = var_shp->template flat<T>().data();
  const auto* var_tex_ptr = var_tex->template flat<T>().data();
  // Copy Mean to output
  Functor::MMGeneratorInitializer<Device, T> initializer;
  initializer(ctx, *mean_shp, shape);
  initializer(ctx, *mean_tex, tex);
  // Add shape variation + remove cog
  Functor::MMGeneratorNormalizer<Device, T> normalizer;
  LA::Gemm(ctx,
           shp_param_ptr, n_img, ns, np, false, T(1.0),
           var_shp_ptr, nv, ns, ns, true, T(1.0), shape);
  normalizer(ctx, storage->model->get_cog(), shape);
  // Add texture variation
  LA::Gemm(ctx,
           tex_param_ptr, n_img, nt, np, false, T(1.0),
           var_tex_ptr, nv, nt, nt, true, T(1.0), tex);
  // Compute normals
  Functor::NormalGenerator<Device, T>()(ctx, storage);
}

/*
 * @name  Generate
 * @brief Generate an instance of a morphable model
 * @param[in,out] ctx     Op's context
 * @param[in] w_shape     Shape parameter
 * @param[in] w_tex       Texture parameters
 * @param[in] model       Morphable model instance
 * @param[out] vertex     Generated vertex
 * @param[out] normal     Generated normal
 * @param[out] color      Generated color
 * @param[out] triangle   Surface's triangulation
 */
template<typename Device, typename T>
void MMGenerator<Device, T>::Generate(tf::OpKernelContext* ctx,
                                      const tf::Tensor* w_shape,
                                      const tf::Tensor* w_tex,
                                      Model* model,
                                      tf::Tensor* vertex,
                                      tf::Tensor* color,
                                      tf::Tensor* triangle) {
  using LA = Functor::LinearAlgebra<Device, T>;
  // Access tensors
  const auto* mean_shp = model->get_mean_shape(ctx);
  const auto* mean_tex = model->get_mean_tex(ctx);
  const auto* var_shp = model->get_var_shape(ctx);
  const auto* var_tex = model->get_var_tex(ctx);
  // Copy Mean to output
  Functor::MMGeneratorInitializer<Device, T> initializer;
  initializer(ctx, *mean_shp, vertex);
  initializer(ctx, *mean_tex, color);
  // Add shape variation + remove cog
  Functor::MMGeneratorNormalizer<Device, T> normalizer;
  // vertex = mean_shape + (w_shape @ var_shp.T)
  LA::Gemm(ctx, *w_shape, false, T(1.0), *var_shp, true, T(1.0), vertex);
  normalizer(ctx, model->get_cog(), vertex);
  // Add texture variation
  // color = mean_tex + (w_tex @ var_tex.T)
  LA::Gemm(ctx, *w_tex, false, T(1.0), *var_tex, true, T(1.0), color);
  // Copy triangles
  Functor::TriangleGenerator<Device, T>()(ctx,
                                          model->get_triangle(ctx),
                                          triangle);
}

#pragma mark -
#pragma mark Explicit Instantiation

/** Float - CPU */
template class MMGenerator<CPUDevice, float>;
/** Double - CPU */
template class MMGenerator<CPUDevice, double>;

#ifdef GOOGLE_CUDA

/** Float - GPU */
template class MMGenerator<GPUDevice, float>;
/** Double - GPU */
template class MMGenerator<GPUDevice, double>;
#endif

}  // namespace LTS5
