/**
 *  @file   sh_lighting_grad.cpp
 *  @brief  Spherical Harmonics Lighting Gradient
 *  @ingroup tensorflow_op
 *
 *  @author Christophe Ecabert
 *  @date   7/7/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "tensorflow/core/framework/shape_inference.h"

#include "lts5/tensorflow_op/sh_lighting_grad.hpp"
#include "lts5/tensorflow_op/utils/shape.hpp"

using CPUDevice = Eigen::ThreadPoolDevice;
using GPUDevice = Eigen::GpuDevice;

#pragma mark -
#pragma mark Functor

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Functor
 *  @brief      LTS5 Functor dev space
 */
namespace Functor {

/**
 * @class   SHLightingGradientHelper
 * @brief   Compute SH lighting gradient
 * @author  Christophe Ecabert
 * @date    15/07/2019
 * @ingroup tensorflow_op
 * @tparam Device   Device on which the computation is run
 * @tparam T        Data type
 */
template<typename T>
struct SHLightingGradientHelper<CPUDevice, T> {
  /**
   * @name  operator()
   * @brief Compute SH lighting gradient
   * @param[in] ctx     Op's context
   * @param[in] g_color Back-propagated gradient
   * @param[in] aldebo  Reflectance without lighting
   * @param[in] normal  Surface's normals
   * @param[in] w_illu  Illumination parameters (SH weights)
   * @param[out] grad_color   Chained gradient with respect to color
   * @param[out] grad_normal  Chained gradient with respect to surface's normal
   * @param[out] grad_w       Chained gradient with respect to w_illu
   */
  int operator()(tf::OpKernelContext* ctx,
                 const tf::Tensor* g_color,
                 const tf::Tensor* aldebo,
                 const tf::Tensor* normal,
                 const tf::Tensor* w_illu,
                 tf::Tensor* grad_color,
                 tf::Tensor* grad_normal,
                 tf::Tensor* grad_w) {
    return -1;
  }
};
}  // namespace Functor
}  // namespace LTS5

#pragma mark -
#pragma mark Initialization

/*
 * @name  SHLightingGradKernel
 * @fn    explicit SHLightingGradKernel(tensorflow::OpKernelConstruction* ctx)
 * @brief Constructor
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
SHLightingGradKernel<Device, T>::
SHLightingGradKernel(tf::OpKernelConstruction* ctx) : OpKernel(ctx),
                                                      n_band_(3) {
}

/*
 * @name  ~SHLightingGradKernel
 * @fn    ~SHLightingGradKernel() override
 * @brief Destructor
 */
template<typename Device, typename T>
SHLightingGradKernel<Device, T>::~SHLightingGradKernel() {}

#pragma mark -
#pragma mark Usage

/**
 * @name GetNumberParameter
 * @brief Compute the number of parameters for a given number of band
 * @param[in] n_band Number of spherical harmonics bands
 * @return  Number of parameters needed
 */
static int GetNumberParameter(const int& n_band) {
  switch (n_band - 1) {
    case 0: return 1;
    case 1: return 4;
    case 2: return 9;
    case 3: return 16;
    case 4: return 25;
    default: return -1;
  }
}

#define MULTIPLE_OF_THREE(x, dim, mult)                  \
  OP_REQUIRES(ctx,                                       \
  x->dim_size(dim) % mult == 0,                          \
  tf::errors::InvalidArgument("Wrong "#x" dims: ",  \
                              x->dim_size(dim),          \
                              "%"#mult" != 0"))

/*
 * @name  Compute
 * @fn    void Compute(tensorflow::OpKernelContext* ctx) override
 * @brief Do the actual computation of the Op
 * @param[in] context Op kernel context
 */
template<typename Device, typename T>
void SHLightingGradKernel<Device, T>::Compute(tensorflow::OpKernelContext* ctx) {
  using GradientHelper = LTS5::Functor::SHLightingGradientHelper<Device, T>;
  // Method should be thread safe, add lock_guard to guarante it.
  tf::mutex_lock lock(this->mutex_);

  // INPUTS
  // -------------------------------------------------------
  const tf::Tensor* g_color = nullptr;
  const tf::Tensor* reflectance = nullptr;
  const tf::Tensor* normal = nullptr;
  const tf::Tensor* w_illu = nullptr;
  OP_REQUIRES_OK(ctx, ctx->input("g_color", &g_color));
  OP_REQUIRES_OK(ctx, ctx->input("reflectance", &reflectance));
  OP_REQUIRES_OK(ctx, ctx->input("normal", &normal));
  OP_REQUIRES_OK(ctx, ctx->input("w_illu", &w_illu));

  // Sanity check
  LTS5::ShapeUtils::CheckRankAndType(ctx,
                                     *reflectance,
                                     2,
                                     tf::DataTypeToEnum<T>::v());
  LTS5::ShapeUtils::CheckRankAndType(ctx,
                                     *normal,
                                     2,
                                     tf::DataTypeToEnum<T>::v());
  LTS5::ShapeUtils::CheckShape(ctx,
                                     *w_illu,
                                     2,
                                     tf::DataTypeToEnum<T>::v(),
                                     {-1, 3 * GetNumberParameter(3)});
  MULTIPLE_OF_THREE(reflectance, 1, 3);
  MULTIPLE_OF_THREE(normal, 1, 3);
  MULTIPLE_OF_THREE(w_illu, 1, 3);

  // OUTPUTS
  // -------------------------------------------------------
  tf::Tensor* grad_color;
  tf::Tensor* grad_normal;
  tf::Tensor* grad_w;
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("grad_color",
                                      reflectance->shape(),
                                      &grad_color));
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("grad_normal",
                                      normal->shape(),
                                      &grad_normal));
  OP_REQUIRES_OK(ctx,
                 ctx->allocate_output("grad_w",
                                      w_illu->shape(),
                                      &grad_w));
  // Compute
  // -------------------------------------------------------
  int err = GradientHelper()(ctx,
                             g_color,
                             reflectance,
                             normal,
                             w_illu,
                             grad_color,
                             grad_normal,
                             grad_w);
  if (err) {
    ctx->CtxFailureWithWarning(__FILE__,
                               __LINE__,
                               tf::errors::Internal("Failure in grad kernel"));
  }
}


#pragma mark -
#pragma mark Registration With Tensorflow

using InferenceContext = tf::shape_inference::InferenceContext;
using ShapeHandle = tf::shape_inference::ShapeHandle;

REGISTER_OP("ShLightingOpGrad")
        .Input("g_color: T")
        .Input("reflectance: T")
        .Input("normal: T")
        .Input("w_illu: T")
        .Output("grad_color: T")
        .Output("grad_normal: T")
        .Output("grad_w: T")
        .Attr("T: {float}")
        .SetShapeFn([](InferenceContext* ctx) -> tf::Status {
          std::vector<ShapeHandle> r_shp;
          std::vector<ShapeHandle> n_shp;
          std::vector<ShapeHandle> w_shp;
          auto s = ctx->input("reflectance", &r_shp);
          s = ctx->input("normal", &n_shp);
          s = ctx->input("w_illu", &w_shp);
          s = ctx->set_output("grad_color", {r_shp});
          s = ctx->set_output("grad_normal", {n_shp});
          s = ctx->set_output("grad_w", {w_shp});
          return s;
        })
        .Doc(R"doc(Compute Spherical Harmonics Lighting Gradient)doc");

#define REGISTER_CPU(Type)                      \
REGISTER_KERNEL_BUILDER(                        \
      Name("ShLightingOpGrad")                  \
      .Device(tf::DEVICE_CPU)                   \
      .TypeConstraint<Type>("T"),               \
      SHLightingGradKernel<CPUDevice, Type>)
REGISTER_CPU(float);
// Remove definition
#undef REGISTER_CPU

#ifdef GOOGLE_CUDA
#define REGISTER_GPU(Type)                                                 \
  REGISTER_KERNEL_BUILDER(                                                 \
      Name("ShLightingOpGrad")                                             \
      .Device(tf::DEVICE_GPU)                                              \
      .TypeConstraint<Type>("T"),                                          \
      SHLightingGradKernel<GPUDevice, Type>);
REGISTER_GPU(float);

// Remove definition
#undef REGISTER_GPU

#endif  // GOOGLE_CUDA
