# coding=utf-8
""" Utility function for Tensorflow-Keras compile step """
import tensorflow as tf
from tensorflow.python.keras.engine.compile_utils import LossesContainer as \
  LossesContainerBase
from tensorflow.python.keras.engine.compile_utils import match_dtype_and_rank
from tensorflow.python.keras.engine.compile_utils import apply_mask
from tensorflow.python.distribute import distribution_strategy_context as ds_context
from tensorflow.python.keras.utils import losses_utils
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import math_ops


class LossesContainer(LossesContainerBase):
  """ Adaptation of compile_utils.LossesContainer class for model outputting
  nested struct """

  def __init__(self,
               losses,
               loss_weights=None,
               output_names=None,
               flat_output=True):
    """
    Constructor
    :param losses:  String (name of objective function), objective function or
            `tf.keras.losses.Loss` instance. See `tf.keras.losses`.
            An objective function is any callable with the signature
            `loss = fn(y_true, y_pred)`, where
            y_true = ground truth values with shape = `[batch_size, d0, .. dN]`,
            except sparse loss functions such as sparse categorical crossentropy
            where shape = `[batch_size, d0, .. dN-1]`.
            y_pred = predicted values with shape = `[batch_size, d0, .. dN]`.
            It returns a weighted loss float tensor.
            If a custom `Loss` instance is used and reduction is set to NONE,
            return value has the shape [batch_size, d0, .. dN-1] ie. per-sample
            or per-timestep loss values; otherwise, it is a scalar.
            If the model has multiple outputs, you can use a different loss on
            each output by passing a dictionary or a list of losses. The loss
            value that will be minimized by the model will then be the sum of
            all individual losses.
    :param flat_output: If `True` flatten the output of the network before
      applying losses objects, otherwise keep the structure as it is.
    :param loss_weights: Optional list or dictionary specifying scalar
            coefficients (Python floats) to weight the loss contributions
            of different model outputs.
            The loss value that will be minimized by the model
            will then be the *weighted sum* of all individual losses,
            weighted by the `loss_weights` coefficients.
            If a list, it is expected to have a 1:1 mapping
            to the model's outputs. If a dict, it is expected to map
            output names (strings) to scalar coefficients.
    :param output_names: List of name for the network's output.
    """
    super(LossesContainer, self).__init__(losses, loss_weights, output_names)
    self.flat_output = flat_output

  def __call__(self,
               y_true,
               y_pred,
               sample_weight=None,
               regularization_losses=None):
    """Computes the overall loss.

    Arguments:
      y_true: An arbitrary structure of Tensors representing the ground truth.
      y_pred: An arbitrary structure of Tensors representing a Model's outputs.
      sample_weight: An arbitrary structure of Tensors representing the
        per-sample loss weights. If one Tensor is passed, it is used for all
        losses. If multiple Tensors are passed, the structure should match
        `y_pred`.
      regularization_losses: Additional losses to be added to the total loss.

    Returns:
      Tuple of `(total_loss, per_output_loss_list)`
    """
    y_true = self._conform_to_outputs(y_pred, y_true)
    sample_weight = self._conform_to_outputs(y_pred, sample_weight)

    if not self._built:
      self._build(y_pred)

    # Flatten output
    if self.flat_output:
      y_pred = tf.nest.flatten(y_pred)
      y_true = tf.nest.flatten(y_true)
      sample_weight = tf.nest.flatten(sample_weight)
    else:
      # If sample_weight is None will populate it with the same structure as
      # `y_pred` and we don't wan't that. Therefore make it as list of single
      # element (Could be none or a Tensor).
      sample_weight = [s if not isinstance(s, (tuple, list)) else s[0]
                       for s in sample_weight]

    # Compute losses + metrics
    loss_values = []  # Used for gradient calculation.
    loss_metric_values = []  # Used for loss metric calculation.
    batch_dim = None
    zip_args = (y_true, y_pred, sample_weight, self._losses, self._loss_weights,
                self._per_output_metrics)
    for y_t, y_p, sw, loss_obj, loss_weight, metric_obj in zip(*zip_args):
      if y_t is None or loss_obj is None:  # Ok to have no loss for an output.
        continue

      if not isinstance(y_p, (list, tuple)):
        # Might have output that are not tensor but list or tuple of tensors
        y_t, y_p, sw = match_dtype_and_rank(y_t, y_p, sw)
      sw = apply_mask(y_p, sw)

      loss_value = loss_obj(y_t, y_p, sample_weight=sw)

      loss_metric_value = loss_value
      # Correct for the `Mean` loss metrics counting each replica as a batch.
      if loss_obj.reduction == losses_utils.ReductionV2.SUM:
        loss_metric_value *= ds_context.get_strategy().num_replicas_in_sync

      if batch_dim is None:
        batch_dim = array_ops.shape(y_t)[0]
      if metric_obj is not None:
        metric_obj.update_state(loss_metric_value, sample_weight=batch_dim)

      if loss_weight is not None:
        loss_value *= loss_weight
        loss_metric_value *= loss_weight

      if (loss_obj.reduction == losses_utils.ReductionV2.SUM_OVER_BATCH_SIZE or
          loss_obj.reduction == losses_utils.ReductionV2.AUTO):
        loss_value = losses_utils.scale_loss_for_distribution(loss_value)

      loss_values.append(loss_value)
      loss_metric_values.append(loss_metric_value)

    if regularization_losses:
      regularization_losses = losses_utils.cast_losses_to_common_dtype(
        regularization_losses)
      reg_loss = math_ops.add_n(regularization_losses)
      loss_metric_values.append(reg_loss)
      loss_values.append(losses_utils.scale_loss_for_distribution(reg_loss))

    if loss_values:
      loss_metric_values = losses_utils.cast_losses_to_common_dtype(
        loss_metric_values)
      total_loss_metric_value = math_ops.add_n(loss_metric_values)
      self._loss_metric.update_state(
        total_loss_metric_value, sample_weight=batch_dim)

      loss_values = losses_utils.cast_losses_to_common_dtype(loss_values)
      total_loss = math_ops.add_n(loss_values)
      return total_loss
    else:
      # Ok for a model to have no compiled loss.
      return array_ops.zeros(shape=())
