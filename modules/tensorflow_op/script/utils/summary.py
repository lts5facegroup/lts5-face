# coding=utf-8
"""
Summary Writer Manager

See:
  - https://www.tensorflow.org/tensorboard/migrate
"""
from os.path import join as _join
from os.path import exists as _exists
from os import makedirs as _makedirs
import tensorflow as _tf
from tensorflow.keras.backend import learning_phase

__author__ = 'Christophe Ecabert'


class SummaryFileWriter:
  """
  Dump summary into files

  See: https://www.tensorflow.org/tensorboard/migrate
  """

  def __init__(self, logdir, writer_name, log_freqency=100):
    """
    Constructor
    :param logdir:        Location where to dump the summaries
    :param writer_name:   Name given to the summary logger
    :param log_freqency:  Logging frequency (i.e. number of steps between logs)
    """
    # Define output
    self._logdir = logdir
    if not _exists(logdir):
      _makedirs(logdir)
    # Writer
    self._wname = writer_name
    p = _join(self._logdir, self._wname)
    self._writer = _tf.summary.create_file_writer(p)
    # Frequency
    self._log_freq = _tf.convert_to_tensor(log_freqency, dtype=_tf.int64)

  def scalar(self, name, data, step, description=None):
    """
    Record scalar value
    :param name:  A name for this summary. The summary tag used for TensorBoard
                  will be this name prefixed by any active name scopes.
    :param data:  A real numeric scalar value, convertible to a float32 Tensor.
    :param step:  Explicit int64-castable monotonic step value for this summary
    :param description: Optional long-form description for this summary, as a
    """
    # Dump
    with self._writer.as_default():
      with _tf.summary.record_if((step % self._log_freq) == 0):
        _tf.summary.scalar(data=data,
                           name=name,
                           step=step,
                           description=description)
        self._writer.flush()

  def histogram(self, name, data, step, buckets=None, description=None):
    """
    Record histogram value
    :param name: A name for this summary. The summary tag used for TensorBoard
                  will be this name prefixed by any active name scopes.
    :param data:  A Tensor of any shape. Must be castable to float64.
    :param step:  Explicit int64-castable monotonic step value for this summary.
    :param buckets: Optional positive int. The output will have this many
                    buckets, except in two edge cases.
    :param description:  Optional long-form description for this summary, as a
                         constant str. Markdown is supported. Defaults to empty.
    """
    # Dump
    with self._writer.as_default():
      with _tf.summary.record_if((step % self._log_freq) == 0):
        _tf.summary.histogram(data=data,
                              name=name,
                              step=step,
                              buckets=buckets,
                              description=description)
        self._writer.flush()

  def image(self, name, data, step, max_outputs=3, description=None):
    """
    Record image value
    :param name:   A name for this summary. The summary tag used for TensorBoard
                    will be this name prefixed by any active name scopes.
    :param data:  A Tensor representing pixel data with shape [k, h, w, c],
                  where k is the number of images, h and w are the height and
                  width of the images, and c is the number of channels, which
                  should be 1, 2, 3, or 4
    :param step:  Explicit int64-castable monotonic step value for this summary.
    :param max_outputs: Optional int or rank-0 integer Tensor. At most this many
                        images will be emitted at each step
    :param description: Optional long-form description for this summary, as a
                        constant str. Markdown is supported. Defaults to empty.
    """
    # Hack
    # See: https://github.com/tensorflow/tensorflow/issues/28007
    with _tf.device('cpu:0'):
      with self._writer.as_default():
        with _tf.summary.record_if((step % self._log_freq) == 0):
          _tf.summary.image(data=data,
                            name=name,
                            step=step,
                            max_outputs=max_outputs,
                            description=description)
          self._writer.flush()

  def text(self, name, data, step, description=None):
    """
    Record text value
    :param name:  A name for this summary. The summary tag used for TensorBoard
                  will be this name prefixed by any active name scopes.
    :param data:  A UTF-8 string tensor value.
    :param step:  Explicit int64-castable monotonic step value for this summary.
    :param description: Optional long-form description for this summary, as a
                        constant str. Markdown is supported. Defaults to empty.
    """
    with self._writer.as_default():
      with _tf.summary.record_if((step % self._log_freq) == 0):
        _tf.summary.text(data=data,
                         name=name,
                         step=step,
                         description=description)
        self._writer.flush()

  def audio(self, name, data, sample_rate, step, max_outputs=3, encoding=None,
            description=None):
    """
    Record audio value
    :param name:  A name for this summary. The summary tag used for TensorBoard
                  will be this name prefixed by any active name scopes.
    :param data:  A Tensor representing audio data with shape [k, t, c], where
                  k is the number of audio clips, t is the number of frames,
                   and c is the number of channels. Elements should be
                   floating-point values in [-1.0, 1.0]
    :param sample_rate: An int or rank-0 int32 Tensor that represents the sample
                        rate, in Hz. Must be positive.
    :param step:  Explicit int64-castable monotonic step value for this summary.
    :param max_outputs: Optional int or rank-0 integer Tensor. At most this many
                        audio clips will be emitted at each step.
    :param encoding:  Optional constant str for the desired encoding. Only "wav"
                      is currently supported, but this is not guaranteed to
                      remain the default, so if you want "wav" in particular,
                      set this explicitly.
    :param description: Optional long-form description for this summary, as a
                        constant str. Markdown is supported. Defaults to empty.
    """
    with self._writer.as_default():
      with _tf.summary.record_if((step % self._log_freq) == 0):
        _tf.summary.audio(data=data,
                          name=name,
                          step=step,
                          sample_rate=sample_rate,
                          max_outputs=max_outputs,
                          encoding=encoding,
                          description=description)
        self._writer.flush()


def _write_summary(summary_fn, writer, step, log_freq, *args, **kwargs):
  """
  Record a summary for the training phase
  :param summary_fn:  Type of summary
  :param writer:      SummaryWriter instance to use to dump data
  :param step:        Step
  :param log_freq:    Logging frequency
  :param args:        args
  :param kwargs:      keyword arguments
  """
  with writer.as_default():
    with _tf.summary.record_if((step % log_freq) == 0):
      summary_fn(*args, step=step, **kwargs)


class SummaryFileWriterV2:
  """
  Dump summary into files

  See: https://www.tensorflow.org/tensorboard/migrate
  """

  def __init__(self, logdir, writer_name, log_freqency=100):
    """
    Constructor
    :param logdir:        Location where to dump the summaries
    :param writer_name:   Name given to the summary logger
    :param log_freqency:  Logging frequency (i.e. number of steps between logs)
    """
    # Define output
    self._logdir = logdir
    if not _exists(logdir):
      _makedirs(logdir)
    # Writer
    self._wname = writer_name
    p = _join(self._logdir, self._wname)
    self._writer = _tf.summary.create_file_writer(p)
    p = _join(self._logdir, 'val_' + self._wname)
    self._val_writer = _tf.summary.create_file_writer(p)
    # Frequency
    self._log_freq = log_freqency

  def _train_summary(self, summary_fn, step, *args, **kwargs):
    """
    Record a summary for the training phase
    :param summary_fn:  Type of summary
    :param step:        Step
    :param args:        args
    :param kwargs:      keyword arguments
    """
    with self._writer.as_default():
      with _tf.summary.record_if((step % self._log_freq) == 0):
        summary_fn(*args, step=step, **kwargs)

  def _test_summary(self, summary_fn, step, *args, **kwargs):
    """
    Record a summary for the testing phase
    :param summary_fn:  Type of summary
    :param step:        Step
    :param args:        args
      :param kwargs:      keyword arguments
      """
    with self._val_writer.as_default():
      with _tf.summary.record_if((step % self._log_freq) == 0):
        summary_fn(*args, step=step, **kwargs)

  def scalar(self, name, data, step, training=None):
    """
    Record scalar value
    :param name:  A name for this summary. The summary tag used for TensorBoard
                  will be this name prefixed by any active name scopes.
    :param data:  A real numeric scalar value, convertible to a float32 Tensor.
    :param step:  Explicit int64-castable monotonic step value for this summary
    """
    if training is None:
      training = learning_phase()
    # Select proper writer
    if training:
      _write_summary(_tf.summary.scalar,
                     self._writer,
                     step,
                     self._log_freq,
                     data=data,
                     name=name)
    else:
      _write_summary(_tf.summary.scalar,
                     self._val_writer,
                     step,
                     self._log_freq,
                     data=data,
                     name=name)

  def histogram(self, name, data, step, training=None):
    """
    Record histogram value
    :param name: A name for this summary. The summary tag used for TensorBoard
                  will be this name prefixed by any active name scopes.
    :param data:  A Tensor of any shape. Must be castable to float64.
    :param step:  Explicit int64-castable monotonic step value for this summary.
    """
    if training is None:
      training = learning_phase()

    if training:
      _write_summary(_tf.summary.histogram,
                     self._writer,
                     step=step,
                     log_freq=self._log_freq,
                     data=data,
                     name=name)
    else:
      _write_summary(_tf.summary.histogram,
                     self._val_writer,
                     step=step,
                     log_freq=self._log_freq,
                     data=data,
                     name=name)

  def image(self, name, data, step, max_outputs=3, training=None):
    """
    Record image value
    :param name:   A name for this summary. The summary tag used for TensorBoard
                    will be this name prefixed by any active name scopes.
    :param data:  A Tensor representing pixel data with shape [k, h, w, c],
                  where k is the number of images, h and w are the height and
                  width of the images, and c is the number of channels, which
                  should be 1, 2, 3, or 4
    :param step:  Explicit int64-castable monotonic step value for this summary.
    :param max_outputs: Optional int or rank-0 integer Tensor. At most this many
                        images will be emitted at each step
    """
    if training is None:
      training = learning_phase()

    if training:
      _write_summary(_tf.summary.image,
                     self._writer,
                     step=step,
                     log_freq=self._log_freq,
                     data=data,
                     max_outputs=max_outputs,
                     name=name)
    else:
      _write_summary(_tf.summary.image,
                     self._val_writer,
                     step=step,
                     log_freq=self._log_freq,
                     data=data,
                     max_outputs=max_outputs,
                     name=name)

  def text(self, name, data, step, training=None):
    """
    Record text value
    :param name:  A name for this summary. The summary tag used for TensorBoard
                  will be this name prefixed by any active name scopes.
    :param data:  A UTF-8 string tensor value.
    :param step:  Explicit int64-castable monotonic step value for this summary.
    """
    if training is None:
      training = learning_phase()

    if training:
      _write_summary(_tf.summary.text,
                     self._writer,
                     step=step,
                     log_freq=self._log_freq,
                     data=data,
                     name=name)
    else:
      _write_summary(_tf.summary.text,
                     self._val_writer,
                     step=step,
                     log_freq=self._log_freq,
                     data=data,
                     name=name)

  def audio(self,
            name,
            data,
            sample_rate,
            step,
            max_outputs=3,
            training=None):
    """
    Record audio value
    :param name:  A name for this summary. The summary tag used for TensorBoard
                  will be this name prefixed by any active name scopes.
    :param data:  A Tensor representing audio data with shape [k, t, c], where
                  k is the number of audio clips, t is the number of frames,
                   and c is the number of channels. Elements should be
                   floating-point values in [-1.0, 1.0]
    :param sample_rate: An int or rank-0 int32 Tensor that represents the sample
                        rate, in Hz. Must be positive.
    :param step:  Explicit int64-castable monotonic step value for this summary.
    :param max_outputs: Optional int or rank-0 integer Tensor. At most this many
                        audio clips will be emitted at each step.
    """
    if training is None:
      training = learning_phase()

    if training:
      _write_summary(_tf.summary.audio,
                     self._writer,
                     step=step,
                     log_freq=self._log_freq,
                     data=data,
                     sample_rate=sample_rate,
                     max_outputs=max_outputs,
                     name=name)
    else:
      _write_summary(_tf.summary.audio,
                     self._val_writer,
                     step=step,
                     log_freq=self._log_freq,
                     data=data,
                     sample_rate=sample_rate,
                     max_outputs=max_outputs,
                     name=name)


class SummaryWriterManager:
  """
  Summary writer manager
  """

  def __init__(self, log_dir, log_freqency=100, use_v2=False):
    """
    Constructor
    :param log_dir: Root director where summary files will be dropped
    :param log_freqency:  Logging frequency (i.e. number of steps between logs)
    :param use_v2:  Use version 2 writer
    """
    self._log_dir = log_dir
    self._log_freq = log_freqency
    self._writers = {}
    self._ctor = SummaryFileWriterV2 if use_v2 else SummaryFileWriter

  def get_writer(self, writer_name):
    """
    Get an instance of `tf.summary.SummaryWriter` for the given subdirectory
    under the logdir
    :param writer_name:  The name of the directory for which to create or
                         retrieve a writer
    :param phase:        Network phase,  str `train`, `validation` or `test`
    :return: `tf.summary.SummaryWriter`
    """
    if writer_name not in self._writers:
      writer = self._ctor(logdir=self._log_dir,
                          writer_name=writer_name,
                          log_freqency=self._log_freq)
      self._writers[writer_name] = writer
    return self._writers[writer_name]

  @property
  def logdir(self):
    """ Root folder where logs will be dumped """
    return self._log_dir
