# coding=utf-8
"""
Utility function for data conversion to TFRecords.

See:
  https://www.tensorflow.org/tutorials/load_data/tf_records
"""
from os.path import join as _join
import tensorflow as _tf
from tensorflow.core.example.feature_pb2 import Feature
from tensorflow.core.example.feature_pb2 import BytesList, FloatList, Int64List
from lts5.utils.tools import init_logger

__author__ = 'Christophe Ecabert'

logger = init_logger()


def to_feature(value):
  """
  Convert a given `value` to a tf.train.Feature protocol buffer.
  :param value: Value to be converted
  :return:  tf.train.Feature instance
  """
  # Convert to list if needed
  if not isinstance(value, list):
    value = [value]

  # Convert to features
  if all(isinstance(x, (str, bytes)) for x in value):
    # String/bytes object
    return Feature(bytes_list=BytesList(value=value))
  elif all(isinstance(x, float) for x in value):
    # Float object
    return Feature(float_list=FloatList(value=value))
  elif all(isinstance(x, (bool, int)) for x in value):
    # bool / enum / int / uint object
    return Feature(int64_list=Int64List(value=value))
  else:
    raise TypeError('Unsupported type: {}'.format(type(value[0])))


def convert_csv_to_tfrecords(samples,
                             parse_fn,
                             shard_size,
                             folder,
                             type='train'):
  """
  Convert a list of CSV entries into a tfrecords files
  :param samples:     List of CSV entries to be exports (i.e. line)
  :param parse_fn:    Parsing function to use to create tf.train.Example object
                      prototype must be `parse_fn(line)`
  :param shard_size:  Maximum number of samples per tfrecord shard
  :param folder:      Location where tfrecords will be dumped
  :param type:        Partition: 'train', 'validation', 'test'
  """
  n_shard = (len(samples) // shard_size) + 1
  for k in range(n_shard):
    start = k * shard_size
    stop = (k + 1) * shard_size
    stop = stop if stop < len(samples) else len(samples)
    # Create tfrecords writer for the given shards
    wname = _join(folder,
                  'records',
                  '{}_{:03d}.tfrecords'.format(type.lower(), k))
    logger.info('Process %s',
                'records/{}_{:02d}.tfrecords'.format(type.lower(), k))
    with _tf.io.TFRecordWriter(path=wname) as writer:
      # Iterate over samples
      for s in samples[start:stop]:
        # Create an tf.train.Example from a given line
        example = parse_fn(s)
        # Dump it
        writer.write(example.SerializeToString())
