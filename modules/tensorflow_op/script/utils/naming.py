# coding=utf-8
""" Utility function to create unique names """
import re
from collections import defaultdict

# Define some usefull variable
first_cap_re = re.compile('(.)([A-Z][a-z]+)')
all_cap_re = re.compile('([a-z0-9])([A-Z])')

NAME_UID_MAP = defaultdict(int)


def to_camel_case(value):
  """
  Convert a given string into camel case format
  :param value: String to convert to camel case
  :return:  Camel case formatted string
  """
  # See: https://stackoverflow.com/a/1176023
  s1 = first_cap_re.sub(r'\1_\2', value)
  return all_cap_re.sub(r'\1_\2', s1).lower()


def get_default_name_uid_map():
  """
  Provide unique id map
  :return:  Dict of unique id maps
  """
  return NAME_UID_MAP


def reset_default_name_uid_map():
  """
  Clear unique id naming map
  """
  NAME_UID_MAP.clear()


def create_unique_name(name):
  """
  Create a unique name
  :param name:  Name to make unique
  :return:  Unique string name
  """
  name_uid_map = get_default_name_uid_map()
  name_key = name
  name_uid_map[name_key] += 1  # Increase counter for this key
  return name + '_' + str(name_uid_map[name_key])


