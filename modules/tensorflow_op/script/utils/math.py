# coding=utf-8
""" Math utility function """
import tensorflow as tf


def _nonzero_sign(x):
  """Returns the sign of x with sign(0) defined as 1 instead of 0."""
  one = tf.ones_like(x)
  return tf.where(tf.greater_equal(x, 0.0), one, -one)


def _safe_signed_div(x, y, eps=1e-10):
  """ Calculates x/y safely """
  return x / (y + _nonzero_sign(y) * eps)


def planes_from_perspective_matrix(matrix):
  """
  Extract `near` and `far` plane from a given set of perspective matrix
  :param matrix:  Perspective matrix [A1, ..., An, 4, 4]
  :return:  Tuple of tensors: `near` and `far` plane both [A1, ..., An, 1]
  """
  matrix = tf.convert_to_tensor(matrix)
  a = matrix[..., 2, 2:3]
  b = matrix[..., 2, 3:4]
  near = b / (a - 1.0)
  far = b / (a + 1.0)
  return near, far


def build_perspective_matrix(focal, cx, cy, near, far):
  """
  Build opengl perspective matrix from focal length and image center points
  :param focal: Tensor, focal length [A1, ..., An, 1]
  :param cx:  Tensor, center point x [A1, ..., An, 1]
  :param cy:  Tensor, center point y [A1, ..., An, 1]
  :param near: Tensor, Near plane [A1, ..., An, 1]
  :param far:   Tensor, Far plane [A1, ..., An, 1]
  :return:  Tensor, perspective matrix [A1, ..., An, 4, 4]
  """
  # Convert to tensor
  focal = tf.convert_to_tensor(focal)
  cx = tf.convert_to_tensor(cx)
  cy = tf.convert_to_tensor(cy)
  near = tf.convert_to_tensor(near)
  far = tf.convert_to_tensor(far)
  # Build perspective matrix
  zero = tf.zeros_like(focal)
  one = tf.ones_like(focal)
  a = -(far + near) / (far - near)
  b = -(2.0 * far * near) / (far - near)
  mat = tf.concat([focal / cx, zero, zero, zero,
                   zero, focal / cy, zero, zero,
                   zero, zero, a, b,
                   zero, zero, -one, zero], axis=-1)
  shp = tf.shape(mat)
  output_shp = tf.concat((shp[:-1], (4, 4)), axis=-1)
  return tf.reshape(mat, shape=output_shp)


def eye_to_clip(point_eye_space,
                perspective_matrix):
  """
  Transform points from eye space (i.e. camera) into clip space
  see: http://www.songho.ca/opengl/gl_transform.html
  :param point_eye_space:     Array of points to transform [A1, ..., An, 3]
  :param perspective_matrix:  Perspective projection matrix [A1, ..., An, 4, 4]
  :return:  Transformed points in clip space, [A1, ..., An, 4]
  """
  with tf.name_scope('eye_to_clip'):
    # Convert points to homogenous coordinates
    point_eye_space = tf.convert_to_tensor(point_eye_space)
    batch_shape = tf.shape(input=point_eye_space)[:-1]
    one = tf.ones(shape=tf.concat((batch_shape, (1,)), axis=-1),
                  dtype=point_eye_space.dtype)
    point_eye_space = tf.concat((point_eye_space, one), axis=-1)
    point_eye_space = tf.expand_dims(point_eye_space, axis=-1)
    point_clip_space = tf.matmul(perspective_matrix, point_eye_space)
    return tf.squeeze(point_clip_space, axis=-1)


def clip_to_ndc(point_clip_space):
  """
  Convert point from clip space to ndc space
  see: http://www.songho.ca/opengl/gl_transform.html
  :param point_clip_space: Array of points to transform [A1, ..., An, 4]
  :return: Transformed points in NDC space, [A1, ..., An, 3]
  """
  with tf.name_scope('clip_to_ndc'):
    point_clip_space = tf.convert_to_tensor(point_clip_space)
    w = tf.expand_dims(point_clip_space[..., -1], -1)
    return point_clip_space[..., :3] / w


def ndc_to_screen(point_ndc_space,
                  dims,
                  near,
                  far,
                  top_left=True):
  """
  Convert NDC space into screen space
  :param point_ndc_space: Point to transform, [A1, ..., An, 3]
  :param dims:  Screen dimensions (i.e. width, height), [A1, ..., An, 2]
  :param near:  Near plane, [A1, ..., An, 1]
  :param far:   Far plane, [A1, ..., An, 1]
  :param top_left: If `True` convert bottom-left coordinate system into top-left
                    origin coordinate system.
  :return:
  """
  with tf.name_scope('ndc_to_screen'):
    dims = tf.convert_to_tensor(dims)
    near = tf.convert_to_tensor(near)
    far = tf.convert_to_tensor(far)
    # Linearly scale to screen dimensions
    bias = tf.concat((dims, (near + far)), axis=-1) / 2.0
    if top_left:
      dims = tf.concat((dims[..., 0:1], -dims[..., -1:]), axis=-1)
    scale = tf.concat((dims, far - near), axis=-1) / 2.0
    return scale * point_ndc_space + bias


def eye_to_screen(point_eye_space,
                  perspective_matrix,
                  screen_dims,
                  top_left=True):
  """
  Transform point from eye space (i.e. camera) into screen space.
  :param point_eye_space:     Point to transform, [A1, ..., An,  3]
  :param perspective_matrix:  Perspective projection matrix, [A1, ..., An, 4, 4]
  :param screen_dims:         Screen dimensions in pixels (i.e. width + height),
                              [A1, ..., An, 2]
  :param top_left:  If `True` indicate to transform to top-left corner origin.
  :return:  A tuple of tensors or dimensions [A1, ..., An, 3] and
            [A1, ..., An, 1]. The first element holds the transformed points in
            screen space and the second element holds the `w` component of the
            point in the clip space.
  """
  # Eye -> clip
  point_clip = eye_to_clip(point_eye_space, perspective_matrix)
  # Clip -> NDC
  point_ndc = clip_to_ndc(point_clip)
  # NDC -> Screen
  near, far = planes_from_perspective_matrix(perspective_matrix)
  point_screen = ndc_to_screen(point_ndc, screen_dims, near, far, top_left)
  return point_screen, point_clip[..., -1:]


def compute_barycentric_coordinates(tri_vertex, pixel_position):
  """
  Computes the barycentric coordinates of pixels for 2D triangles.
  :param tri_vertex: A tensor of shape `[A1, ..., An, 3, 2]`, where the last two
      dimensions represents the `x` and `y` coordinates for each vertex of a
      2D triangle.
  :param pixel_position: A tensor of shape `[A1, ..., An, 2]`, where `N`
      represents the number of pixels, and the last dimension represents the `x`
      and `y` coordinates of each pixel.
  :return: barycentric_coordinates: A float tensor of shape `[A1, ..., An, 3]`,
      representing the barycentric coordinates.
    valid: A boolean tensor of shape `[A1, ..., An, N], which is `True` where
      pixels are inside the triangle, and `False` otherwise.
  """
  # Ensure tensor
  tri_vert = tf.convert_to_tensor(value=tri_vertex)
  pixels = tf.convert_to_tensor(value=pixel_position)
  # Extract components
  v1, v2, v3 = tf.unstack(tf.expand_dims(tri_vert, axis=-3), axis=-2)
  v1x, v1y = tf.unstack(v1, axis=-1)
  v2x, v2y = tf.unstack(v2, axis=-1)
  v3x, v3y = tf.unstack(v3, axis=-1)
  px, py = tf.unstack(pixels, axis=-1)

  v1x_minus_v3x = v1x - v3x
  v3x_minus_v2x = v3x - v2x
  v3y_minus_v1y = v3y - v1y
  v2y_minus_v3y = v2y - v3y
  px_minus_v3x = px - v3x
  py_minus_v3y = py - v3y

  determinant = v2y_minus_v3y * v1x_minus_v3x - v3x_minus_v2x * v3y_minus_v1y
  bcoord1 = v2y_minus_v3y * px_minus_v3x + v3x_minus_v2x * py_minus_v3y
  bcoord1 = _safe_signed_div(bcoord1, determinant)
  bcoord2 = v3y_minus_v1y * px_minus_v3x + v1x_minus_v3x * py_minus_v3y
  bcoord2 = _safe_signed_div(bcoord2, determinant)
  bcoord3 = 1.0 - (bcoord1 + bcoord2)
  # Fuse
  bcoords = tf.stack((bcoord1, bcoord2, bcoord3), axis=-1)
  valid = tf.logical_and(tf.logical_and(bcoord1 >= 0.0, bcoord2 >= 0.0),
                         bcoord3 >= 0.0)
  return bcoords, valid


def perspective_corrected_barycentrics(triangle_vertices_eye_space,
                                       pixel_position,
                                       perspective_matrix,
                                       screen_dims,
                                       top_left=True):
  """
  Compute perspective corrected barycentric coordinates
  :param triangle_vertices_eye_space:  A tensor of shape `[A1, ..., An, 3, 3]`,
      where the last dimension represents the vertices of a triangle in eye
      space (i.e. camera space).
  :param pixel_position: A tensor of shape `[A1, ..., An, 2]`, where the last
      dimension stores the position (in pixels) where the interpolation is
      requested.
  :param perspective_matrix: Projection matrices to transform points from eye
      to clip coordinates, [A1, ..., An, 4, 4]
  :param screen_dims: A tensor of shape `[A1, ..., An, 2]`, where the last
      dimension is expressed in pixels and captures the width and the height
      (in pixels) of the screen.
  :param top_left: If `True` indicate to transform to top-left corner origin.
  :return: A tensor of shape `[A1, ..., An, 3]`, containing perspective correct
      barycentric coordinates.
  """
  tri_vert_eye_space = tf.convert_to_tensor(triangle_vertices_eye_space)
  pix_pos = tf.convert_to_tensor(pixel_position)
  # Project to screen space
  vertices_screen, vertices_w = eye_to_screen(tri_vert_eye_space,
                                              perspective_matrix,
                                              screen_dims,
                                              top_left)
  vertices_w = tf.squeeze(vertices_w, axis=-1)

  # Compute uncorrected barycentric coordinates
  pix_pos = tf.expand_dims(pix_pos, axis=-2)
  bcoords, bc_valid = compute_barycentric_coordinates(vertices_screen[..., :2],
                                                      pix_pos)
  # Correct barycentric coordinates
  bcoords = tf.squeeze(bcoords, axis=-2)
  coeffs = bcoords / vertices_w
  return tf.linalg.normalize(coeffs, ord=1, axis=-1)[0]
