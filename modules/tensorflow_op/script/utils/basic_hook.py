""" Implementation of various basic hook compatible with `tf.Estimator` API """
from time import clock
import io
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg
from itertools import product
__author__ = 'Christophe Ecabert'


class RestoreNpyModelHook(tf.estimator.SessionRunHook):
  """ Restore a pretrained model from numpy *.npy file. Usually the outcome
  from a caffe2tensorflow conversion

  See:
   - https://github.com/tensorflow/tensorflow/issues/14713#issuecomment-345658784
  """

  def __init__(self, init_fn):
    """
    Constructor
    :param init_fn: Initialization function with the following signature:
                    `init_fn(session)`
    """
    self._init_fn = init_fn

  def after_create_session(self, session, coord=None):
    """ Restore model if first iteration, aka global_step == 0 """
    if session.run(tf.train.get_or_create_global_step()) == 0:
      # Init network
      tf.logging.info('Restoring pretrained network')
      self._init_fn(session)


class TimerHook(tf.estimator.SessionRunHook):
  """ Hook definition for timing estimator execution """

  def __init__(self, logger, every_n_iter=100):
    """
    Constructor
    :param logger: Instance following the logging API. Must implement standard
                   logging methods (i.e. info(), debug(), ...)
    """
    if every_n_iter is None or every_n_iter <= 0:
      raise ValueError('invalid every_n_iter=%s.' % every_n_iter)
    self._logger = logger
    self._start = 0.0
    self._end = 0.0
    self._step = 0
    self._every_n_iter = every_n_iter

  def before_run(self, run_context):
    """
    Called before each call to run().

    Start measuring execution time at this point.
    :param run_context: A `SessionRunContext` object.
    :return:  None or a `SessionRunArgs` object.
    """
    if self._step == 0:
      self._start = clock()

  def after_run(self, run_context, run_values):
    """
    Called after each call to run().

    Compute the execution time for one call to `run()` method.
    :param run_context: A `SessionRunContext` object.
    :param run_values:  A SessionRunValues object.
    """
    self._step += 1
    if self._step == self._every_n_iter:
      self._end = clock()
      self._step = 0
      dt = self._end - self._start
      dt /= float(self._every_n_iter)
      msg = 'The Estimator run took: %.2f secondes in average'
      self._logger.info(msg, dt)


class ConfusionMatrixHook(tf.estimator.SessionRunHook):
  """
  Hook definition for generation of confusion matrix figure compatible with
  estimators

  Link:
   https://github.com/tensorflow/tensorboard/issues/227#issuecomment-397857872
  """

  def __init__(self,
               labels,
               tensor_name,
               model_dir,
               name,
               figsize=None,
               dpi=None,
               cmap='Oranges'):
    """
    Constructor
    :param labels:      List of classes in the confustion matrix
    :param tensor_name: Name of the tensor storing the confusion matrix
    :param model_dir:   Location where the model is stored
    :param name:        Summary name
    :param figsize:     Tuple with dimension of the final figure
    :param dpi:         Dot per inch
    :param cmap:        Color map
    """
    self._labels = labels
    self._tensor_name = tensor_name
    self._writer = tf.summary.FileWriterCache.get(model_dir)
    self._name = name
    self._figsize = figsize
    self._dpi = dpi
    self._cmap = cmap

  def end(self, session):
    """
    Called at the end of session
    :param session:  Session used to run the model
    """
    # Get tensor + global step
    g = tf.get_default_graph()
    cm = g.get_tensor_by_name(self._tensor_name).eval(session=session)
    gstep = tf.train.get_global_step().eval(session=session)
    # Create cm figure
    fig = self._plot_confusion_matrix(cm=cm,
                                      labels=self._labels,
                                      title='Confusion Matrix',
                                      cmap=self._cmap)
    summary = self._figure_to_summary(fig)
    # Add summary
    self._writer.add_summary(summary, gstep)

  def _plot_confusion_matrix(self, cm, labels, title, cmap='Oranges'):
    """
    Plot confusion matrix for Tensorboad

    See:
      - https://stackoverflow.com/a/48030258/4546884
    :param cm:      Confusion matrix
    :param labels:  List of string labels
    :param title:   Plot's title
    :return:
    """
    # Normalize
    cmf = cm.astype(np.float32)
    cmf /= cmf.sum(axis=-1).reshape(-1, 1)
    # Plot
    fig = plt.figure(figsize=self._figsize,
                     dpi=self._dpi,
                     facecolor='w',
                     edgecolor='k')
    ax = fig.add_subplot(1, 1, 1)
    ax.imshow(cmf, cmap=cmap)

    # Axes
    tick_marks = np.arange(len(labels))
    ax.set_title(title)
    # X axis
    ax.set_xlabel('Predicted')  # fontsize=7
    ax.set_xticks(tick_marks)
    ax.set_xticklabels(labels, rotation=-90, ha='center')  # fontsize=4
    ax.xaxis.set_label_position('bottom')
    ax.xaxis.tick_bottom()
    # Y axis
    ax.set_ylabel('True Label')  # fontsize=7
    ax.set_yticks(tick_marks)
    ax.set_yticklabels(labels, va='center')  # fontsize=4
    ax.yaxis.set_label_position('left')
    ax.yaxis.tick_left()
    # Draw number
    n_classes = len(labels)
    for i, j in product(range(n_classes), range(n_classes)):
      cm_str = '{:0.2f}'.format(cmf[i, j])
      ax.text(j, i, cm_str if cmf[i, j] > 0.0 else '.',
              horizontalalignment="center", verticalalignment='center',
              color="black")
    fig.set_tight_layout(True)
    return fig

  def _figure_to_summary(self, fig):
    """
    Converts a matplotlib figure ``fig`` into a TensorFlow Summary object
    that can be directly fed into ``Summary.FileWriter``.
    :param fig: A ``matplotlib.figure.Figure`` object.
    :return: A TensorFlow ``Summary`` protobuf object containing the plot image
             as a image summary.
    """
    # attach a new canvas if not exists
    if fig.canvas is None:
      FigureCanvasAgg(fig)

    fig.canvas.draw()
    w, h = fig.canvas.get_width_height()

    # get PNG data from the figure
    png_buffer = io.BytesIO()
    fig.canvas.print_png(png_buffer)
    png_encoded = png_buffer.getvalue()
    png_buffer.close()

    # Create summary
    summary_image = tf.Summary.Image(height=h, width=w, colorspace=4,  # RGB-A
                                     encoded_image_string=png_encoded)
    summary = tf.Summary(value=[tf.Summary.Value(tag=self._name,
                                                 image=summary_image)])
    # Close figure
    plt.close(fig)
    return summary
