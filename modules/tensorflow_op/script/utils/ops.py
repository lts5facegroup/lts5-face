# coding=utf-8
"""
Custom implementation of basic algebra operations
"""
import tensorflow as tf

@tf.custom_gradient
def stable_norm(tensor,
                ord='euclidean',
                axis=None,
                keepdims=None,
                name=None,
                epsilon=1e-7):
  """
  This function can compute several different vector norms (the 1-norm, the
  Euclidean or 2-norm, the inf-norm, and in general the p-norm for p > 0) and
  matrix norms (Frobenius, 1-norm, 2-norm and inf-norm).
  :param tensor:    Tensor of types float32, float64, complex64, complex128
  :param ord:       Order of the norm
  :param axis:      If axis is a Python integer, the input is considered a batch
                    of vectors, and axis determines the axis in tensor over
                    which to compute vector norms
  :param keepdims:  If True, the axis indicated in axis are kept with size 1.
                    Otherwise, the dimensions in axis are removed from the
                    output shape.
  :param name:      The name of the op.
  :param epsilon:   A small constant for numerical stability in gradient
  :return:          A Tensor of the same type as tensor, containing the vector
                    or matrix norms
  See: https://github.com/tensorflow/tensorflow/issues/12071
  """
  # Standard norm
  y = tf.norm(tensor, ord=ord, axis=axis, keepdims=keepdims, name=name)

  def grad(dy):
    return dy * (tensor / (y + epsilon))

  return y, grad
