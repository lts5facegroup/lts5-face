# coding:utf-8
"""
Learning Rate finder

See:
  - https://medium.com/octavian-ai/how-to-use-the-learning-rate-finder-in-tensorflow-126210de9489
  - https://towardsdatascience.com/speeding-up-neural-net-training-with-lr-finder-c3b401a116d0
"""
import tensorflow as _tf
import tensorflow.keras.backend as K
from tensorflow.keras.callbacks import Callback

__author__ = 'Christophe Ecabert'


class LearningRateFinder(Callback):
  """ Learning Rate Finder """

  def __init__(self,
               model,
               start_lr: float,
               end_lr: float,
               steps: int):
    """
    Constructor
    :param model:     Keras model for which LR needs to be estimated
    :param start_lr:  Starting learning rate
    :param end_lr:    Stopping learning rate
    :param steps:     Number of steps to take between `start_lr` and `stop_lr`
    """
    super(LearningRateFinder, self).__init__()
    # Model
    self.model = model
    # Learning rate multiplier
    self._start_lr = start_lr
    self._n_steps = steps
    self._lr_mult = (end_lr / start_lr) ** (1.0 / float(steps))
    # Storage
    self._losses = []
    self._lrs = []
    self._best_loss = 1e15
    self._original_lr = None

  def find_lr(self, x=None, y=None, **kwargs):
    """
    Find "optimum" learning rate.
    :param x: Input data. It could be:
                - A Numpy array (or array-like), or a list of arrays (in case
                  the model has multiple inputs).
                - A TensorFlow tensor, or a list of tensors (in case the model
                  has multiple inputs).
                - A dict mapping input names to the corresponding array/tensors,
                  if the model has named inputs.
                - A tf.data dataset. Should return a tuple of either (inputs,
                  targets) or (inputs, targets, sample_weights).
    :param y:   Target data, not needed if using `tf.data.Dataset`
    :param kwargs:  Extra keyword arguments
    """
    # Run test
    self.model.fit(x=x,
                   y=y,
                   callbacks=[self],
                   steps_per_epoch=self._n_steps,
                   epochs=1,
                   **kwargs)

  def on_train_begin(self, logs=None):
    """
    Called at the beginning of training.
    :param logs:  dict. Currently no data is passed to this argument for this
                  method but that may change in the future.
    """
    # Save model
    self.model.save_weights('tmp')
    # Remember the original learning rate
    self._original_lr = K.get_value(self.model.optimizer.lr)
    # Set the initial learning rate
    K.set_value(self.model.optimizer.lr, self._start_lr)

    pass

  def on_train_end(self, logs=None):
    """
    Called at the end of training.
    :param logs: dict. Currently no data is passed to this argument for this
                 method but that may change in the future.
    """
    # Restore model
    self.model.load_weights('tmp')
    # Restore the original learning rate
    K.set_value(self.model.optimizer.lr, self._original_lr)

  def on_train_batch_end(self, batch, logs=None):
    """
    Called at the end of a training batch in `fit` methods.
    :param batch: integer, index of batch within the current epoch.
    :param logs:  dict. Metric results for this batch.
    """

    # Log the learning rate
    lr = K.get_value(self.model.optimizer.lr)
    self._lrs.append(lr)
    # Log the loss
    loss = logs['loss']
    self._losses.append(loss)

    # Check whether the loss got too large or NaN
    if (batch == self._n_steps or
        (batch > 16 and (_tf.math.is_nan(loss) or
                         loss > self._best_loss * 10))):
      self.model.stop_training = True
      print("Stop Training at %s, loss = %.3f" % (batch, loss))
      return

    if loss < self._best_loss:
      self._best_loss = loss

    # Increase the learning rate for the next batch
    lr *= self._lr_mult
    K.set_value(self.model.optimizer.lr, lr)

  def plot(self, n_skip_beginning=5, n_skip_end=5, sma=10):
    """
    Generate plot
    :param n_skip_beginning:
    :param n_skip_end:
    :param sma:
    :return:
    """
    import matplotlib.pyplot as plt

    def _moving_average(x, w):
      # https://stackoverflow.com/questions/14313510/how-to-calculate-moving-average-using-numpy
      import numpy as _np
      return _np.convolve(_np.array(x), _np.ones(w), 'same') / w

    losses = self._losses
    losses = _moving_average(losses, sma)
    losses = losses[n_skip_beginning:-n_skip_end]

    fig = plt.figure(figsize=(12, 6))
    ax = fig.add_subplot(111)
    ax.plot(self._lrs[n_skip_beginning:-n_skip_end], losses)
    ax.set_ylabel('Loss')
    ax.set_xlabel('Learning Rate (log scale)')
    ax.set_xscale('log')
    ax.set_title('LR vs Loss Graph')


