""" Utility functions to work with tensorflow event file (i.e. `tf.summary`) """
import numpy as np
import tensorflow as tf
__author__ = 'Christophe Ecabert'


def parse(filename, tag, last=False):
  """
  Parse a given event file and look for specific `tag`.

  :param filename:  Path to the event file
  :param tag:       Tag to seek
  :param last:      Flag indicating if only the most recent event is needed or
                    the whole list.
  :return:  Single event or list of events.
  """
  events = []
  for e in tf.train.summary_iterator(filename):
    for v in e.summary.value:
      if tag in v.tag:
        events.append((e.step, v))
  events.sort(key=lambda x: x[0])
  return events[-1] if last else events


def convert_histogram(hist, nbin=30):
  """
  Convert tensorflow summary histogram into numpy array for easy display or
  analysis

  See: https://github.com/tensorflow/tensorflow/blob/17c47804b86e340203d451125a721310033710f1/tensorflow/tensorboard/components/tf_backend/backend.ts#L400

  :param hist:  Histogram summary object to convert
  :param nbin:  Number of bins to generate (resampling)
  :return:  (x, dx, y)
  """
  # Define min/max value of the histogram
  if hist.min == hist.max:
    hmax = hist.min * 1.1 + 1.0
    hmin = hist.min / 1.1 - 1.0
  else:
    hmin = hist.min
    hmax = hist.max
  # Bin properties
  bin_w = (hmax - hmin) / nbin
  bucket_left = hmin
  bucket_pos = 0
  # Resample
  x = []
  dx = []
  y = []
  for bin_left in np.arange(hmin, hmax, bin_w):
    bin_right = bin_left + bin_w
    # Take the count of each existing bucket, multiply it by the proportion of
    # overlap with the new bin, then sum and store as the count for the new bin.
    # If no overlap, will add to zero, if 100 % overlap, will include the full
    # count into new bin.
    bin_y = 0
    while bucket_pos < len(hist.bucket_limit):
      # Clip the right edge because right-most edge can be infinite-sized.
      bucket_right = min(hmax, hist.bucket_limit[bucket_pos])
      intersect = min(bucket_right, bin_right) - max(bucket_left, bin_left)
      if intersect > 0.0:
        count = (intersect / (bucket_right - bucket_left)) * hist.bucket[
          bucket_pos]
        bin_y += count
      # If bucket_right is bigger than bin_right, than this bin is finished and
      # there is data for the next bin, so don't increment bucket_pos.
      if bucket_right > bin_right:
        break
      bucket_left = max(hmin, bucket_right)
      bucket_pos += 1
    x.append(bin_left)
    dx.append(bin_w)
    y.append(bin_y)
  return x, dx, y
