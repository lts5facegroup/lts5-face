# coding=utf-8
"""
Limit tensorflow GPU memory usage

See:
  - https://www.tensorflow.org/guide/gpu
"""
import tensorflow as _tf
from lts5.utils import init_logger

logger = init_logger()

__gpus = None


def initialize_device_memory(device_idx=0):
  global __gpus

  if __gpus is None:
    # Get devices if any
    __gpus = _tf.config.experimental.list_physical_devices('GPU')
    if __gpus:
      # Currently, memory growth needs to be the same across GPUs
      try:
        for gpu in __gpus:
          _tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = _tf.config.experimental.list_logical_devices('GPU')
        logger.info('%d Physical GPUs, %d Logical GPU',
                    len(__gpus),
                    len(logical_gpus))
      except RuntimeError as e:
        # Visible devices must be set before GPUs have been initialized
        print(e)
