#coding=utf-8
""" Structure like dictionary """

__author__ = 'Christophe Ecabert'


class Params(dict):
  """
  Convenience class that behaves exactly like dict(), but allows accessing
  the keys and values using the attribute syntax, i.e., "mydict.key = value".

  See:
    https://github.com/tkarras/progressive_growing_of_gans/blob/master/config.py#L12
  """

  def __init__(self, *args, **kwargs):
    kw = {}
    for k, v in kwargs.items():
      if isinstance(v, dict):
        kw[k] = Params(v)
      else:
        kw[k] = v
    super(Params, self).__init__(*args, **kw)

  def __getattr__(self, name):
    """
    Get attribute (i.e. mydict.name)
    :param name:  Attribute's name
    :return:    Attribute's value
    """
    if name.startswith('__'):
      return super(Params, self).__getattr__(name)
    return self[name]

  def __setattr__(self, name, value):
    """
    Set attribute's value.
    :param name:  Attribute's name to be set
    :param value: Value to set
    """
    self[name] = value

  def __delattr__(self, name):
    """
    Delete an attribute
    :param name:  Attribute's name to be deleted
    """
    del self[name]