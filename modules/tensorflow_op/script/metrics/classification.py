# coding=utf-8
""" Tensorflow keras metrics for classification """
from itertools import product
from numpy import arange, zeros
import tensorflow as _tf
from tensorflow.keras.metrics import Metric
from lts5.tensorflow_op.metrics import ImagePair


__author__ = 'Christophe Ecabert'


class MeanIoU(_tf.keras.metrics.MeanIoU):
  """ Subclass tf.keras.metrics.MeanIou to add the option to preprocess
  optionaly  `y_true` and `y_pred` before computing metric"""

  def __init__(self,
               num_classes,
               name='MeanIoU',
               y_true_fn=None,
               y_pred_fn=None,
               dtype=None):
    """
    Constructor
    :param num_classes: The possible number of labels the prediction task can
      have. This value must be provided, since a confusion matrix of
      dimension = [num_classes, num_classes] will be allocated.
    :param name: (Optional) string name of the metric instance.
    :param y_true_fn: Callable function applying custom transformation before
                      storing it. Prototype must be `xt = y_true_fn(x)`
    :param y_pred_fn: Callable function applying custom transformation before
                      storing it. Prototype must be `xt = y_pred_fn(x)`
    :param dtype:   (Optional) data type of the metric result.
    """
    super(MeanIoU, self).__init__(num_classes=num_classes,
                                  name=name,
                                  dtype=dtype)
    self.y_true_fn = _tf.identity if y_true_fn is None else y_true_fn
    self.y_pred_fn = _tf.identity if y_pred_fn is None else y_pred_fn

  def update_state(self, y_true, y_pred, **kwargs):
    """
    Save `y_true` and `y_pred`
    :param y_true:  Ground truth value
    :param y_pred:  Predicted value
    :param kwargs:  Extra keyword arguments
    """
    y_true = self.y_true_fn(y_true)
    y_pred = self.y_pred_fn(y_pred)
    return super(MeanIoU, self).update_state(y_true, y_pred)

  def get_config(self):
    """
    :return: Metric's config
    """
    cfg = super(MeanIoU, self).get_config()
    cfg['y_true_fn'] = self.y_true_fn
    cfg['y_pred_fn'] = self.y_pred_fn
    return cfg


class ConfusionMatrix(Metric):
  """ Confusion matrix """

  def __init__(self,
               num_classes,
               name='ConfusionMatrix',
               **kwargs):
    """
    Constructor
    :param num_classes: Number of classes
    :param name:        Metric's name
    :param kwargs:      Extra keyword arguments
    """
    super(ConfusionMatrix, self).__init__(name=name, **kwargs)
    self.n_class = num_classes
    self.cm = self.add_weight(shape=(self.n_class, self.n_class),
                              initializer='zeros',
                              dtype=_tf.int64,
                              name='cm')

  def update_state(self, y_true, y_pred, **kwargs):
    """
    Compute confusion matrix. Labels and prediction are assumed to be
    probabilities, therefore it will apply threshold / argmax internally.
    :param y_true:  Labels
    :param y_pred:  Predicted value
    """
    # Convert into integer value
    if self.n_class == 2:
      labels = _tf.cast(y_true > 0.5, _tf.int32)
      preds = _tf.cast(y_pred > 0.5, _tf.int32)
    else:
      labels = _tf.cast(y_true, _tf.int32)
      preds = _tf.argmax(y_pred, axis=-1, output_type=_tf.int32)
      preds = _tf.expand_dims(preds, -1)
    # Flatten inputs
    labels = _tf.reshape(labels, [-1])
    preds = _tf.reshape(preds, [-1])
    # Compute CM
    cm = _tf.math.confusion_matrix(labels=labels,
                                   predictions=preds,
                                   num_classes=self.n_class,
                                   dtype=self.cm.dtype)
    return self.cm.assign_add(cm)

  def reset_states(self):
    """
    Resets all of the metric state variables.
    This function is called between epochs/steps, when a metric is evaluated
    during training.
    """
    values = [(v, zeros(v.shape)) for v in self.variables]
    _tf.keras.backend.batch_set_value(values)

  def result(self):
    """
    Finalize confusion matrix
    :return:  Confusion matrix
    """
    n = _tf.reduce_sum(self.cm, axis=1, keepdims=True)
    n = _tf.cast(n, _tf.float32)
    return _tf.math.divide_no_nan(_tf.cast(self.cm, _tf.float32), n)

  def get_config(self):
    """
    :return: Metric's config
    """
    cfg = super(ConfusionMatrix, self).get_config()
    cfg['num_classes'] = self.n_class
    return cfg


class SegmentationMask(ImagePair):
  """ Generate segmentation mask from tensor pairs """

  def __init__(self,
               num_classes,
               y_true_fn=None,
               cmap=None,
               name='SegmentationMask',
               **kwargs):
    """
    Constructor
    :param num_classes: Number of classes being segmented
    :param y_true_fn: Callable function applying custom transformation before
                      storing it. Prototype must be `xt = y_true_fn(x)`
    :param name:    Metric's name
    :param kwargs:  Extra keyword arguments
    :keyword max_outputs: Maximum number of image to save from 4D Tensor
    :keyword cmap:  Colormap to use when rendering in tensorboard
    """

    def _y_pred_fn(x):
      # Generate mask
      if num_classes == 2:  # Binary => threshold
        mask = _tf.cast(x > 0.5, _tf.int32)
      else:  # Multi-class argmax
        mask = _tf.argmax(x, axis=-1, output_type=_tf.int32)
        mask = _tf.expand_dims(mask, -1)  # [Batch, H, W, 1]
      return mask

    super(SegmentationMask, self).__init__(y_true_fn=y_true_fn,
                                           y_pred_fn=_y_pred_fn,
                                           dtype=_tf.int32,
                                           name=name,
                                           **kwargs)
    self.n_classes = num_classes

  def get_config(self):
    """
    :return: Metric's config
    """
    cfg = {'num_classes': self.n_classes,
           'y_true_fn': self.y_true_fn,
           'name': self.name}
    return cfg

  def update_state(self, y_true, y_pred, **kwargs):
    """
    Save `y_true` and `y_pred`
    :param y_true:  Ground truth value
    :param y_pred:  Predicted value
    :param kwargs:  Extra keyword arguments
    """
    y_true = _tf.cast(y_true, self.dtype)
    return super(SegmentationMask, self).update_state(y_true=y_true,
                                                      y_pred=y_pred,
                                                      **kwargs)
