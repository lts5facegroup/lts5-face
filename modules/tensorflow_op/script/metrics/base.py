# coding=utf-8
""" Shared class for custom tf.keras.metrics.Metric classes """
import numpy as _np
import tensorflow as _tf
from tensorflow.keras.metrics import Metric
from tensorflow.python.keras import backend as K
__author__ = 'Christophe Ecabert'

_tf_to_np_dtype = {_tf.float32: _np.float32,
                   _tf.float64: _np.float64,
                   _tf.int8: _np.int8,
                   _tf.uint8: _np.uint8,
                   _tf.int16: _np.int16,
                   _tf.uint16: _np.uint16,
                   _tf.int32: _np.int32,
                   _tf.uint32: _np.uint32,
                   _tf.int64: _np.int64,
                   _tf.uint64: _np.uint64}


class ValueTensor(Metric):
  """ Persistent storage for a pair tensor """

  def __init__(self,
               dtype,
               name,
               max_outputs=3,
               **kwargs):
    """
    Constructor
    :param dtype:   Data type of the `y_true` tensor to be stored
    :param name:    Metric's name
    :param max_outputs: Maximum number of tensor to store
    :param kwargs:  Extra keyword arguments
    """
    super(ValueTensor, self).__init__(name=name, dtype=dtype, **kwargs)
    self.max_outputs = max_outputs
    self.y_true = None
    self.y_pred = None
    self._built = False

  def _build(self, shape):
    """
    Initialize this metric for a given shape
    :param shape:
    """
    shp = (self.max_outputs, ) + tuple(shape[1:])
    self.y_true = self.add_weight(shape=shp,
                                  aggregation=_tf.VariableAggregation.ONLY_FIRST_REPLICA,
                                  name='y_true',
                                  initializer='zeros',
                                  dtype=self.dtype)
    self.y_pred = self.add_weight(shape=shp,
                                  aggregation=_tf.VariableAggregation.ONLY_FIRST_REPLICA,
                                  name='y_pred',
                                  initializer='zeros',
                                  dtype=self.dtype)
    self._built = True

  def update_state(self, y_true, y_pred, **kwargs):
    """
    Save `y_true` and `y_pred`
    :param y_true:  Ground truth value
    :param y_pred:  Predicted value
    :param kwargs:  Extra keyword arguments
    """
    # lazy init
    if not self._built:
      self._build(y_pred.shape)
    # Assign new values
    update_ytrue_op = self.y_true.assign(y_true[:self.max_outputs, ...])
    with _tf.control_dependencies([update_ytrue_op]):
      return self.y_pred.assign(y_pred[:self.max_outputs, ...])

  def reset_states(self):
    """
    Resets all of the metric state variables.
    This function is called between epochs/steps, when a metric is evaluated
    during training.
    """
    values = [(v, _np.zeros(v.shape,
                            _tf_to_np_dtype[v.dtype])) for v in self.variables]
    _tf.keras.backend.batch_set_value(values)

  def result(self):
    """
    Return stored values
    :return:  y_true, y_pred
    """
    return self.y_true, self.y_pred

  def get_config(self):
    """
    :return: Metric's config
    """
    cfg = super(ValueTensor, self).get_config()
    cfg['name'] = self.name
    cfg['max_outputs'] = self.max_outputs
    return cfg


class ImagePair(ValueTensor):
  """
  Store image pair of form of `Ground truth / Predicted` usefull for
  monitoring image generation process
  """

  def __init__(self,
               dtype,
               y_true_fn=None,
               y_pred_fn=None,
               cmap=None,
               name='ImagePair',
               **kwargs):
    """
    Constructor
    :param dtype: Data type of the tensor to be stored
    :param y_true_fn: Callable function applying custom transformation before
                      storing it. Prototype must be `xt = y_true_fn(x)`
    :param y_pred_fn: Callable function applying custom transformation before
                      storing it. Prototype must be `xt = y_pred_fn(x)`
    :param cmap:    Colormap to use when rendering it in tensorboard
    :param name:    Image pair's name
    :param kwargs:  Extra keyword arguments
    :keyword max_outputs: Maximum number of image to save from 4D Tensor
    """
    super(ImagePair, self).__init__(dtype=dtype,
                                    name=name,
                                    **kwargs)
    self.cmap = cmap
    self.y_true_fn = _tf.identity if y_true_fn is None else y_true_fn
    self.y_pred_fn = _tf.identity if y_pred_fn is None else y_pred_fn

  def update_state(self, y_true, y_pred, **kwargs):
    """
    Save `y_true` and `y_pred`
    :param y_true:  Ground truth value
    :param y_pred:  Predicted value
    :param kwargs:  Extra keyword arguments
    """
    y_true = self.y_true_fn(y_true)
    y_pred = self.y_pred_fn(y_pred)
    return super(ImagePair, self).update_state(y_true, y_pred)

  def get_config(self):
    """
    :return: Metric's config
    """
    cfg = super(ImagePair, self).get_config()
    cfg['y_true_fn'] = self.y_true_fn
    cfg['y_pred_fn'] = self.y_pred_fn
    cfg['name'] = self.name
    cfg['cmap'] = self.cmap
    return cfg


class DirectionalMeanTensor(Metric):
  """
  Computes the directional (i.e. axis) mean of the given tensors.
  """

  def __init__(self, name='directional_mean_tensor', dtype=None, **kwargs):
    """
    Constructor
    :param name:    Metric's name
    :param dtype:   Data type
    :param kwargs:  Extra keyword arguments
    """
    super(DirectionalMeanTensor, self).__init__(name=name,
                                                dtype=dtype,
                                                **kwargs)
    self._axis = -1
    self._total = None  # Accumulated values
    self._count = None  # Number of value accumulated
    self._built = False

  def update_state(self, values, axis=-1, sample_weights=None):
    """
    Accumulate statistics for the given direction
    :param values:  Values to update the mean for
    :param axis:    Direction in which the mean is computed
    :param sample_weights:  Sample weight, not used
    """
    # Set proper type
    values = _tf.cast(values, self._dtype)
    # Built ?
    if not self._built:
      self._build(values.shape, axis)

    # Accumulate
    num_values = _tf.cast(values.shape[self._axis], self._dtype)
    values = _tf.reduce_sum(values, axis=self._axis)
    total = self._total.assign_add(values)
    with _tf.control_dependencies([total]):
      return self._count.assign_add(num_values)

  def _build(self, shape, axis):
    """
    Build metric
    :param shape: Input shape
    :param axis:  Direction in which the metric is evaluated
    """
    self._axis = axis
    shp = [v for k, v in enumerate(shape) if k != axis]
    self._total = self.add_weight('total',
                                  shape=shp,
                                  dtype=self._dtype,
                                  initializer='zeros')
    self._count = self.add_weight('count',
                                  dtype=self._dtype,
                                  initializer='zeros')
    # with _tf.init_scope():
    #   if not _tf.executing_eagerly():
    #     K._initialize_variables(K._get_session())  # pylint: disable=protected-access
    self._built = True

  def result(self):
    """ Compute actual mean value """
    return _tf.math.divide_no_nan(self._total, self._count)

  def reset_states(self):
    if self._built:
      K.batch_set_value([(v, _np.zeros(v.shape)) for v in self.variables])


class Covariance(Metric):
  """ Unbiased covariance metric for rank2 tensors. Assumes samples are stored
    by ROW!
  See:
    - https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online
    - https://github.com/tensorflow/gan/blob/a8699e6983abcb6625daed940ca63724ee3db553/tensorflow_gan/python/eval/eval_utils.py#L238
  """

  def __init__(self,
               name='covariance',
               sample_by_row=True,
               dtype=None,
               **kwargs):
    """
    Constructor
    :param name:    Metric's name
    :param dtype:   Data type
    :param kwargs:  Extra keyword arguments
    """
    super(Covariance, self).__init__(name=name, dtype=dtype, **kwargs)
    self._s_by_row = sample_by_row
    self._n_feature = None
    self._mean_x = None
    self._mean_y = None
    self._cov = None
    self._count = None
    self._built = False

  def _build(self, shape):
    """
    Initialize metric for a given shape
    :param shape:  Input shape
    """
    # Create new state variable
    self._n_feature = shape[1] if self._s_by_row else shape[0]
    shp = [1, self._n_feature] if self._s_by_row else [self._n_feature, 1]
    self._mean_x = self.add_weight('mean_x',
                                   shape=shp,
                                   dtype=self._dtype,
                                   initializer='zeros')
    self._mean_y = self.add_weight('mean_y',
                                   shape=shp,
                                   dtype=self._dtype,
                                   initializer='zeros')
    self._cov = self.add_weight('cov',
                                shape=[self._n_feature, self._n_feature],
                                dtype=self._dtype,
                                initializer='zeros')
    self._count = self.add_weight('count',
                                  dtype=self._dtype,
                                  initializer='zeros')
    # with _tf.init_scope():
    #   if not _tf.executing_eagerly():
    #     K._initialize_variables(K._get_session())  # pylint: disable=protected-access
    self._built = True

  def update_state(self, x, y=None, sample_weight=None):
    """
    Update covariance matrix
    :param x: A 2D numeric `Tensor` holding samples.
    :param y: Optional Tensor with same dtype and shape as `x`. Default value:
              `None` (y is effectively set to x).
    :param sample_weight: Not used at the moment
    :return:  Covariance matrix
    """
    # Cast input into proper type
    x = _tf.cast(x, self._dtype)
    if y is not None:
      y = _tf.cast(y, self._dtype)
    # Is y provided ?
    if y is None:
      y = x
    # Sanity check
    x.shape.assert_has_rank(2)
    y.shape.assert_has_rank(2)
    # Metric already initialized ?
    if not self._built:
      self._build(x.shape)
    # compute stuff
    batch_axis = 0 if self._s_by_row else 1
    axis = 0 if self._s_by_row else 1
    num_values = _tf.cast(_tf.shape(x)[batch_axis], dtype=self._dtype)
    dx = _tf.reduce_mean(x, axis=axis, keepdims=True) - self._mean_x
    dy = _tf.reduce_mean(y, axis=axis, keepdims=True) - self._mean_y

    # Update mean_y
    # mean_y += m / (n + m) * dy
    mean_y = self._mean_y.assign_add((num_values / (self._count + num_values)) * dy)

    # Ensure `num_values`, `dx`, `dy` and `mean_y` are updated before changing
    # other variables
    with _tf.control_dependencies([mean_y]):
      # Update cov
      a = x - self._mean_x  # x - mean_x (old)
      b = y - mean_y  # y - mean_y (new)
      tp_a = self._s_by_row
      tp_b = not self._s_by_row
      Cn = _tf.matmul(a=a, b=b, transpose_a=tp_a, transpose_b=tp_b)
      cov = self._cov.assign_add(Cn)
    with _tf.control_dependencies([cov]):
      mean_x = self._mean_x.assign_add((num_values / (self._count + num_values)) * dx)
    with _tf.control_dependencies([mean_y, mean_x]):
      return self._count.assign_add(num_values)

  def reset_states(self):
    if self._built:
      K.batch_set_value([(v, _np.zeros(v.shape)) for v in self.variables])

  def result(self):
    """ Compute actual covariance matrix """
    return _tf.math.divide_no_nan(self._cov,
                                  _tf.maximum(self._count - 1.0, 0.0))
