# coding=utf-8
"""
Implementation of various tensorflow geometric-based metrics

Link:
  - https://www.tensorflow.org/api_docs/python/tf/metrics
"""
import tensorflow as _tf
from tensorflow.keras.metrics import Mean
from tensorflow.python.ops.metrics import metric_variable
from tensorflow.python.ops import state_ops
from lts5.tensorflow_op.utils.shape import check_static

__author__ = 'Christophe Ecabert'


def mean_landmark_error(landmark,
                        ground_truth,
                        normalizer=None,
                        name=None):
  """
  Compute accumulated landmarks error (or normalized error) for a given set of
   points
  :param landmark:      Detected landmarks [Batch, K, 2]
  :param ground_truth:  True landmarks [Batch, K, 2]
  :param normalizer:    Normalization instance (i.e. iod normalizer)
  :param name:          Op's name
  :return:  Metric ops
  """
  with _tf.variable_scope(name,
                          'landmark_error',
                          (landmark, ground_truth)):
    # Sanity check
    check_static(landmark, has_rank=3)
    check_static(ground_truth, has_rank=3)
    # Create metric value
    total = metric_variable([], _tf.float32, name='total')
    count = metric_variable([], _tf.float32, name='count')

    # compute l2 norm
    err = _tf.norm(landmark - ground_truth, ord=2, axis=-1)  # [Batch, K]
    err = _tf.reduce_mean(err, axis=-1)  # [Batch, ]
    if normalizer is not None:
      scale = normalizer(landmark)
      err /= scale
    l_lms = _tf.reduce_mean(err)  # Scalar

    # Define update
    update_total_op = state_ops.assign_add(total, l_lms)
    with _tf.control_dependencies([l_lms]):
      update_count_op = state_ops.assign_add(count, 1)

    mean_ph = _tf.div_no_nan(total, count, name='value')
    update_op = _tf.div_no_nan(update_total_op,
                               _tf.maximum(update_count_op, 0),
                               name='update_op')
    return mean_ph, update_op


class MeanLandmarkError(Mean):
  """ Mean L2 error between facial landmarks """

  def __init__(self,
               normalizer=None,
               name='mean_landmark_error',
               **kwargs):
    """
    Constructor
    :param normalizer: Callable return normalization factor for a set of
                        landmarks
    :param name:    Metric's name
    :param kwargs:  Extra keyword arguments
    """
    super(MeanLandmarkError, self).__init__(name=name, **kwargs)
    self.normalizer = normalizer

  def update_state(self, y_true, y_pred, sample_weight=None):
    """
    Accumulates metric statistics.
    :param y_true: The ground truth values.
    :param y_pred: The predicted values.
    :param sample_weight: Optional weighting of each example. Defaults to 1. Can be a
      `Tensor` whose rank is either 0, or the same rank as `y_true`, and must
      be broadcastable to `y_true`.
    :return: Update op.
    """
    # Sanity check
    bsize = _tf.shape(y_true)[0]
    gt = _tf.reshape(y_true, shape=[bsize, -1, 2])   # [Batch, K, 2]
    lms = _tf.reshape(y_pred, shape=[bsize, -1, 2])   # [Batch, K, 2]

    # compute l2 norm
    err = _tf.norm(lms - gt, ord=2, axis=-1)  # [Batch, K]
    l_lms = _tf.reduce_mean(err, axis=-1)  # [Batch, ]
    if self.normalizer is not None:
      scale = self.normalizer(lms)
      l_lms /= scale
    return super(MeanLandmarkError, self).update_state(l_lms, sample_weight)


class MeanLandmarkPairError(Mean):
  """ Mean L2 error between pairs of facial landmarks """

  def __init__(self, pairs, name='landmark_pair_error', **kwargs):
    """
    Constructor
    :param pairs: List of pair index
    :param name:  Metric's name
    :param kwargs:  Extra keyword arguments
    """
    super(MeanLandmarkPairError, self).__init__(name=name, **kwargs)
    self.p = _tf.convert_to_tensor(pairs)

  def update_state(self, y_true, y_pred, sample_weight=None):
    """
    Compute metric for a given batch
    :param y_true: Ground truth
    :param y_pred:  Predicted landmarks
    :param sample_weight: Weight if any
    :return:  Update op
    """

    # Reshape to [Batch, K, 2]
    bs = _tf.shape(y_pred)[0]
    true_lms = _tf.reshape(y_true, shape=[bs, -1, 2])
    pred_lms = _tf.reshape(y_pred, shape=[bs, -1, 2])
    # Compute distance between landmarks
    e0_true = _tf.gather(true_lms, self.p[:, 0], axis=1)  # [Batch, M, 2]
    e1_true = _tf.gather(true_lms, self.p[:, 1], axis=1)  # [Batch, M, 2]
    d_true = e1_true - e0_true
    e0_pred = _tf.gather(pred_lms, self.p[:, 0], axis=1)  # [Batch, M, 2]
    e1_pred = _tf.gather(pred_lms, self.p[:, 1], axis=1)  # [Batch, M, 2]
    d_pred = e1_pred - e0_pred
    # Error
    l_dist = _tf.norm(d_pred - d_true, axis=-1)
    l_dist = _tf.reduce_mean(l_dist, axis=-1)   # [Batch,]
    return super(MeanLandmarkPairError, self).update_state(l_dist,
                                                           sample_weight)
