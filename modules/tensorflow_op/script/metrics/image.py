# coding=utf-8
"""
Implementation of various tensorflow metrics for RGB images

Link:
  - https://www.tensorflow.org/api_docs/python/tf/metrics
"""
import tensorflow as _tf
from tensorflow.keras.metrics import Metric, Mean, MeanTensor
from lts5.tensorflow_op.network.inception import InceptionV3
from lts5.tensorflow_op.metrics.base import Covariance, DirectionalMeanTensor
from lts5.tensorflow_op.image.util import smart_resize
__author__ = 'Christophe Ecabert'


class MeanPhotometricError(Mean):
  """ Mean photometric error metrics, masks are stored in alpha channels """

  def __init__(self, name='MeanPhotometricError', **kwargs):
    """
    Constructor
    :param name:    Metric's name
    :param kwargs:  Extra keyword arguments
    """
    super(MeanPhotometricError, self).__init__(name=name, **kwargs)

  def update_state(self, y_true, y_pred, sample_weight=None):
    """
    Accumulates metric statistics.
    :param y_true: The ground truth values.
    :param y_pred: The predicted values.
    :param sample_weight: Optional weighting of each example. Defaults to 1. Can
      be a `Tensor` whose rank is either 0, or the same rank as `y_true`, and
      must be broadcastable to `y_true`.
    :return: Update op.
    """
    # Compute metric -> L2,1 norm on pixel intensity A. Tewari, 2017

    mm_mask = _tf.expand_dims(y_pred[..., -1], -1)   # Mask stored in alpha
    y_pred = y_pred[..., :3]
    pskin = _tf.expand_dims(y_true[..., -1], -1)     # Seg mask == alpha channel
    y_true = y_true[..., :3]

    mask = mm_mask * pskin
    n_elem = _tf.reduce_sum(mask, axis=[1, 2]) + 1e-8
    n_elem = _tf.squeeze(n_elem, -1)  # Remove dangling dims to avoid broadcast
    # Pixel l2 error
    px_err = (y_true - y_pred) * mask
    px_nrm = _tf.reduce_sum(px_err ** 2.0, axis=-1) ** 0.5  # [B, H, W]
    # Sum everything, L1 norm
    l_photo = _tf.reduce_sum(px_nrm, axis=[1, 2]) / n_elem   # [Batch,]
    return super(MeanPhotometricError, self).update_state(l_photo,
                                                          sample_weight)


class MeanPhotometricSimilarity(Mean):
  """
  Mean photometric cosine similarity between image (i.e. ground truth) and
  and rendered instance (i.e. reconstructed surface)
  """

  def __init__(self, mask, name='MeanPhotometricSimilarity', **kwargs):
    """
    Constructor
    :param mask:  Optionnal binary mask indicating where to compute the metric
    :param name:  Metric's name
    :param kwargs:  Extra keyword arguments for metrics
    """
    super(MeanPhotometricSimilarity, self).__init__(name=name, **kwargs)
    self._mask = mask

  def update_state(self, y_true, y_pred, sample_weight=None):
    """
    Update metric state with new measurement
    :param y_true:  Ground truth
    :param y_pred:  Predicted value
    :param sample_weight: Optional weighting of each example. Defaults to 1. Can
      be a `Tensor` whose rank is either 0, or the same rank as `y_true`, and
      must be broadcastable to `y_true`.
    :return: Update op.
    """
    bsize = _tf.shape(y_true)[0]
    yt = _tf.reshape(y_true, [bsize, -1])
    yp = _tf.reshape(y_pred, [bsize, -1])
    if self._mask is not None:
      m = _tf.reshape(self._mask, [bsize, -1])
      yt *= m
      yp *= m
    yt_norm = _tf.norm(yt, axis=-1)         # [Batch, ]
    yp_norm = _tf.norm(yp, axis=-1)
    sim = _tf.reduce_sum(yt * yp, axis=-1)  # [Batch, ]
    sim /= (yt_norm * yp_norm)              # [Batch, ]
    return super(MeanPhotometricSimilarity, self).update_state(sim,
                                                               sample_weight)


def _symmetric_matrix_square_root(mat, eps=1e-10):
  """
  Compute square root of a symmetric matrix.
  Note that this is different from an elementwise square root. We want to
  compute M' where M' = sqrt(mat) such that M' * M' = mat.
  Also note that this method **only** works for symmetric matrices.

  :param mat: Matrix to take the square root of.
  :param eps: Small epsilon such that any element less than eps will not be
              square rooted to guard against numerical instability.
  :return: Matrix square root of mat.
  """
  # Unlike numpy, tensorflow's return order is (s, u, v)
  s, u, v = _tf.linalg.svd(mat)
  # sqrt is unstable around 0, just use 0 in such case
  si = _tf.where(_tf.less(s, eps), s, _tf.sqrt(s))
  # Note that the v returned by Tensorflow is v = V
  # (when referencing the equation A = U S V^T)
  # This is unlike Numpy which returns v = V^T
  return _tf.matmul(_tf.matmul(u, _tf.linalg.tensor_diag(si)),
                    v,
                    transpose_b=True)


def _trace_sqrt_product(sigma, sigma_v):
  """
  Let sigma = A A so A = sqrt(sigma), and sigma_v = B B.
  We want to find trace(sqrt(sigma sigma_v)) = trace(sqrt(A A B B))
  Note the following properties:

  (i) forall M1, M2: eigenvalues(M1 M2) = eigenvalues(M2 M1)
     => eigenvalues(A A B B) = eigenvalues (A B B A)

  (ii) if M1 = sqrt(M2), then eigenvalues(M1) = sqrt(eigenvalues(M2))
     => eigenvalues(sqrt(sigma sigma_v)) = sqrt(eigenvalues(A B B A))

  (iii) forall M: trace(M) = sum(eigenvalues(M))
     => trace(sqrt(sigma sigma_v)) = sum(eigenvalues(sqrt(sigma sigma_v)))
                                   = sum(sqrt(eigenvalues(A B B A)))
                                   = sum(eigenvalues(sqrt(A B B A)))
                                   = trace(sqrt(A B B A))
                                   = trace(sqrt(A sigma_v A))

  A = sqrt(sigma). Both sigma and A sigma_v A are symmetric, so we **can**
  use the _symmetric_matrix_square_root function to find the roots of these
  matrices.
  :param sigma:     A square, symmetric, real, positive semi-definite covariance
                    matrix
  :param sigma_v:   Same as sigma
  :return:     The trace of the positive square root of sigma*sigma_v
  """

  # Note sqrt_sigma is called "A" in the proof above
  sqrt_sigma = _symmetric_matrix_square_root(sigma)
  # This is sqrt(A sigma_v A) above
  sqrt_a_sigmav_a = _tf.matmul(sqrt_sigma, _tf.matmul(sigma_v, sqrt_sigma))
  return _tf.linalg.trace(_symmetric_matrix_square_root(sqrt_a_sigmav_a))


class FrechetInceptionDistance(Metric):
  """
  Implementation of the Frechet Inception Distance.
  See:
    - https://arxiv.org/abs/1706.08500
    - https://github.com/tensorflow/gan
  """

  def __init__(self, name='fid_metric', dtype=None, **kwargs):
    """
    Constructor
    :param name:    Metric's name
    :param dtype:   data type
    :param kwargs:  Extra keyword arguments
    """
    super(FrechetInceptionDistance, self).__init__(name=name,
                                                   dtype=dtype,
                                                   **kwargs)
    # Instantiate Inception v3 network + load pre-trained network
    self._fid = InceptionV3.InceptionV3(include_top=False,
                                        pooling='avg')  # -> 2048
    self._fid.trainable = False   # Freeze network
    self._fid.load_weights('imagenet')
    # Activation's mean
    self.m = DirectionalMeanTensor(name='mean_activation',
                                   dtype=self._dtype)
    self.m_w = DirectionalMeanTensor(name='mean_w_activation',
                                     dtype=self._dtype)
    # Activation's covariance
    self.sigma = Covariance(name='sigma_activation',
                            sample_by_row=True,
                            dtype=self._dtype)
    self.sigma_w = Covariance(name='sigma_w_activation',
                              sample_by_row=True,
                              dtype=self._dtype)

  def _img_preprocessing(self, x):
    """
    Prepare a batch of image for inception network
    :param x: 4D Tensor, batch of image
    :return:  Pre-processed image
    """
    # Rescale
    x = smart_resize(x, size=[299, 299]) # Bilinear interpolation
    # To bgr
    x = _tf.reverse(x, axis=[-1])
    return x

  def update_state(self, y_true, y_pred, sample_weight=None):
    """
    Update Frechet Inception Distance score
    :param y_true:  Image from true distribution
    :param y_pred:  Generated image
    :param sample_weight: Not used
    :return:  FID Score
    """

    # Image preparation -> rescale + convert to bgr
    img_true = self._img_preprocessing(y_true)
    img_pred = self._img_preprocessing(y_pred)
    # Compute activation
    act_true = self._fid(img_true)
    act_pred = self._fid(img_pred)
    # Compute mean and covariance matrices of activations.
    with _tf.control_dependencies([act_true, act_pred]):
      updt1_op = self.m.update_state(act_true, axis=0)
      updt2_op = self.m_w.update_state(act_pred, axis=0)
      updt3_op = self.sigma.update_state(act_true, sample_weight=sample_weight)
      with _tf.control_dependencies([updt1_op, updt2_op, updt3_op]):
        return self.sigma_w.update_state(act_pred, sample_weight=sample_weight)

  def reset_states(self):
    """ Reset metrics """
    self.m.reset_states()
    self.m_w.reset_states()
    self.sigma.reset_states()
    self.sigma_w.reset_states()

  def result(self):
    """
    Compute FID metric
    https://github.com/tensorflow/gan/blob/a8699e6983abcb6625daed940ca63724ee3db553/tensorflow_gan/python/eval/classifier_metrics.py#L727
    """
    # Find the Tr(sqrt(sigma sigma_w)) component of FID
    sigma = self.sigma.result()
    sigma_w = self.sigma_w.result()
    sqrt_trace_component = _trace_sqrt_product(sigma, sigma_w)
    # Compute the two components of FID.
    # Covariance. Here, note that trace(A + B) = trace(A) + trace(B)
    trace = _tf.linalg.trace(sigma + sigma_w) - 2.0 * sqrt_trace_component
    # Next the distance between means.
    m = self.m.result()
    m_w = self.m_w.result()
    # Equivalent to L2 but more stable.
    mean = _tf.reduce_sum(_tf.math.squared_difference(m, m_w))
    fid = trace + mean
    return fid

