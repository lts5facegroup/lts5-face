# coding=utf-8
"""
Inherit from tf.keras.callbacks.TensorBoard and add new functionality:
- Confusion matrix
- Image
- Segmentation mask
"""
from io import BytesIO
from itertools import product
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg
from numpy import arange, stack
import tensorflow as _tf
from tensorflow.python.eager import context
from tensorflow.python.ops import summary_ops_v2
from tensorflow.keras.callbacks import TensorBoard
from lts5.tensorflow_op.metrics.classification import ConfusionMatrix as CMMetric
from lts5.tensorflow_op.metrics.classification import SegmentationMask as SegMask
from lts5.tensorflow_op.metrics.base import ImagePair as IMPair
from lts5.tensorflow_op.network.network import Network

__author__ = 'Christophe Ecabert'


def _get_metric_list(model):
  """
  Given a model (i.e. tf.keras.Model or lts5.tensorflow_op.network.Network),
  this method return a list of metrics attached to it
  :param model: Model instance
  :return:  List of metrics
  """
  if isinstance(model, Network):
    # LTS5 keras.Model subclass
    metrics = model.get_metrics()
    if isinstance(metrics, dict):
      metrics = _tf.nest.flatten(metrics.values())
      # Add other metrics add with Keras
      metrics.extend(model.metrics)
    return _tf.nest.flatten(metrics)
  else:
    # Keras model
    return model.metrics


def _plot_confusion_matrix(cm,
                           title='Confusion Matrix',
                           labels=None,
                           cmap=None,
                           figsize=None,
                           dpi=None):
  """
  Plot confusion matrix for Tensorboad

  See:
    - https://stackoverflow.com/a/48030258/4546884
  :param cm:      Confusion matrix
  :param title:   Plot's title
  :param labels:  List of name to display, if none generate default one.
  :param cmap:    Colormap style
  :param figsize: Figure dimensions
  :param dpi:     Dot per inch
  :return:  figure
  """
  # Plot
  fig = plt.figure(figsize=figsize,
                   dpi=dpi,
                   facecolor='w',
                   edgecolor='k')
  ax = fig.add_subplot(1, 1, 1)
  ax.imshow(cm, cmap=cmap)
  # Title
  ax.set_title(title)
  # Labels
  if labels is None:
    labels = [str(k) for k in range(cm.shape[0])]
  # Axes
  tick_marks = arange(len(labels))
  # X axis
  ax.set_xlabel('Predicted')  # fontsize=7
  ax.set_xticks(tick_marks)
  ax.set_xticklabels(labels, rotation=-90, ha='center')  # fontsize=4
  ax.xaxis.set_label_position('bottom')
  ax.xaxis.tick_bottom()
  # Y axis
  ax.set_ylabel('True Label')  # fontsize=7
  ax.set_yticks(tick_marks)
  ax.set_yticklabels(labels, va='center')  # fontsize=4
  ax.yaxis.set_label_position('left')
  ax.yaxis.tick_left()
  # Draw number
  n_class = len(labels)
  font_sz = 'small' if n_class < 10 else 'xx-small'
  for i, j in product(range(n_class), range(n_class)):
    cm_str = '{:0.2f}'.format(cm[i, j])
    ax.text(j, i, cm_str if cm[i, j] > 0.0 else '.',
            horizontalalignment="center",
            verticalalignment='center',
            fontsize=font_sz,
            color="black")
  fig.set_tight_layout(True)
  return fig


def _plot_image(image, cmap=None):
  """
  Convert a Tensor into a `matplotlib.figure.Figure` object
  See: https://stackoverflow.com/a/37812313
  :param image: Tensor to convert
  :param cmap:  Color map to use
  :return:  Figure
  """
  fig = plt.figure(figsize=(1, 1), dpi=image.shape[0])
  ax = plt.Axes(fig, [0.05, 0.05, 0.95, 0.95])
  fig.add_axes(ax)
  if image.shape[-1] == 1:
    image = image[..., 0]
  ax.imshow(image, cmap=cmap)
  ax.set_axis_off()
  return fig


def _figure_to_tensor(fig):
  """
  Converts a matplotlib figure ``fig`` into a TensorFlow Tensor
  that can be directly fed into ``Summary.FileWriter``.
  :param fig: A ``matplotlib.figure.Figure`` object.
  :return: A Tensor
  """
  # attach a new canvas if not exists
  if fig.canvas is None:
    FigureCanvasAgg(fig)
  fig.canvas.draw()
  w, h = fig.canvas.get_width_height()
  # get PNG data from the figure
  png_buffer = BytesIO()
  fig.canvas.print_png(png_buffer)
  png_encoded = png_buffer.getvalue()
  png_buffer.close()
  plt.close(fig)
  # Create image
  image = _tf.image.decode_png(png_encoded, channels=4)
  image = _tf.expand_dims(image, 0)
  return image


def _write_confusion_matrix(name,
                            cm,
                            step,
                            labels,
                            cmap,
                            figsize,
                            dpi):
  """
  Dump confusion matrix into a summary
  :param cm:        Confusion matrix
  :param name:      Record name
  :param step:      Current step of the summary
  :param labels:    Confusion matrix labels
  :param cmap:      Colormap scheme to use
  :param figsize:   Figure size
  :param dpi:       Dot per inch
  """
  fig = _plot_confusion_matrix(cm,
                               'Confusion Matrix',
                               labels,
                               cmap,
                               figsize,
                               dpi)
  img = _figure_to_tensor(fig)
  summary_ops_v2.image(name, img, step=step, max_images=1)


def _write_image(name, image, step, max_outputs, cmap=None):
  """
  Write an image into a summary
  :param name:    Record's name
  :param image:   Image to write (i.e. 5D tensor: y_true + y_pred)
  :param step:    Current step
  :param max_outputs: Number maximum of image
  :param cmap:  Colormap
  """
  images = []
  for k in range(image.shape[1]):
    fig = _plot_image(image[0, k, ...], cmap=cmap)
    gt = _figure_to_tensor(fig)
    fig = _plot_image(image[1, k, ...], cmap=cmap)
    pred = _figure_to_tensor(fig)
    fused = _tf.concat([gt, pred], axis=2)
    images.append(fused)
  summary_ops_v2.image(name,
                       _tf.concat(images, 0),
                       step=step,
                       max_images=max_outputs)


def _select_cmap(name: str, metric_names: list, metric_cmaps: list):
  """
  Select colormap based on summary name formatted as <prefix>_<metric_name>
  where <prefix> in ('epoch', 'batch')
  :param name:          Summary name
  :param metric_names:  List of metrics name to loop into
  :param metric_cmaps:  List of metrics cmap, same size as metric_names
  :return: Corresponding colormap or None if no match was found
  """
  cmap = None,
  for m_name, m_cmap in zip(metric_names, metric_cmaps):
    if m_name in name:
      cmap = m_cmap
      break
  return cmap


class AugmentedTensorBoard(TensorBoard):
  """
  Enable visualizations for TensorBoard.
  TensorBoard is a visualization tool provided with TensorFlow.
  This callback logs events for TensorBoard, including:
  * Metrics summary plots
  * Training graph visualization
  * Activation histograms
  * Sampled profiling
  * Confusion matrix

  If you have installed TensorFlow with pip, you should be able
  to launch TensorBoard from the command line:

  ```sh
  tensorboard --logdir=path_to_your_logs
  ```

  You can find more information about TensorBoard
  [here](https://www.tensorflow.org/get_started/summaries_and_tensorboard).

  Arguments:
      log_dir: the path of the directory where to save the log files to be
        parsed by TensorBoard.
      histogram_freq: frequency (in epochs) at which to compute activation and
        weight histograms for the layers of the model. If set to 0, histograms
        won't be computed. Validation data (or split) must be specified for
        histogram visualizations.
      write_graph: whether to visualize the graph in TensorBoard. The log file
        can become quite large when write_graph is set to True.
      write_images: whether to write model weights to visualize as image in
        TensorBoard.
      update_freq: `'batch'` or `'epoch'` or integer. When using `'batch'`,
        writes the losses and metrics to TensorBoard after each batch. The same
        applies for `'epoch'`. If using an integer, let's say `1000`, the
        callback will write the metrics and losses to TensorBoard every 1000
        samples. Note that writing too frequently to TensorBoard can slow down
        your training.
      profile_batch: Profile the batch to sample compute characteristics. By
        default, it will profile the second batch. Set profile_batch=0 to
        disable profiling. Must run in TensorFlow eager mode.
      embeddings_freq: frequency (in epochs) at which embedding layers will
        be visualized. If set to 0, embeddings won't be visualized.
      embeddings_metadata: a dictionary which maps layer name to a file name in
        which metadata for this embedding layer is saved. See the
        [details](
          https://www.tensorflow.org/how_tos/embedding_viz/#metadata_optional)
        about metadata files format. In case if the same metadata file is
        used for all embedding layers, string can be passed.
      labels: List of class labels used while generating the confusion matrix
      figsize: Size of the generated images, tuple ints
      dpi:  Number of points per inch
      cmap: Colormap style used while computing Confusion matrix
      max_img_outputs: Maximum number of image generated

  Raises:
      ValueError: If histogram_freq is set and no validation data is provided.
  """

  def __init__(self,
               log_dir='logs',
               histogram_freq=0,
               write_graph=True,
               write_images=False,
               update_freq='epoch',
               profile_batch=2,
               embeddings_freq=0,
               embeddings_metadata=None,
               labels=None,
               figsize=None,
               dpi=None,
               cmap='Oranges',
               max_img_outputs=3,
               **kwargs):
    """ Constructor """
    super(AugmentedTensorBoard, self).__init__(log_dir=log_dir,
                                               histogram_freq=histogram_freq,
                                               write_graph=write_graph,
                                               write_images=write_images,
                                               update_freq=update_freq,
                                               profile_batch=profile_batch,
                                               embeddings_freq=embeddings_freq,
                                               embeddings_metadata=embeddings_metadata,
                                               **kwargs)
    # Figure
    self._figsize = figsize
    self._dpi = dpi
    self._cmap = cmap
    self._max_img_outputs = max_img_outputs
    self._metrics_are_set = False
    # Confusion matrix
    self._labels = labels
    self._cm_metric_name = []   # Confusion matrix name$
    # Segmentation mask
    self._seg_mask_name = []
    self._seg_mask_cmap = []
    # Image pair
    self._im_pair_name = []
    self._im_pair_cmap = []
    # Whether or not to override batch counter
    self._override_batch_counter = True

  def _set_custom_metrics(self):
    """
    Check if custom metrics have been added to the model
    """
    if not self._metrics_are_set:
      metrics = _get_metric_list(self.model)
      if len(metrics):
        self._metrics_are_set = True
        for metric in metrics:
          if isinstance(metric, CMMetric):
            self._cm_metric_name.append(metric.name)
          if isinstance(metric, SegMask):
            self._seg_mask_name.append(metric.name)
            self._seg_mask_cmap.append(metric.cmap)
          if isinstance(metric, IMPair):
            self._im_pair_name.append(metric.name)
            self._im_pair_cmap.append(metric.cmap)

  def _init_batch_steps(self):
    """Create the total batch counters."""
    if self._override_batch_counter or not hasattr(self, '_total_batches_seen'):
      super(AugmentedTensorBoard, self)._init_batch_steps()

  def set_model(self, model):
    """
    Set reference to model. check if MeanIoU is part of the metrics.
    :param model: Model
    """
    super(AugmentedTensorBoard, self).set_model(model)
    self._set_custom_metrics()

  def override_batch_init(self, flag):
    """
    Enable / Disable the override of batch counter.
    :param flag:  If `True` override batch counter when training start,
                  otherwise keep it as it is.
    """
    self._override_batch_counter = flag

  def on_train_batch_end(self, batch, logs=None):
    """
    Invoked at the end of each batch during training
    :param batch:
    :param logs:
    :return:
    """
    # Register custom metrics here since they might not be visible early due to
    # lazy initialization / compilation (in self.set_model(...) function for
    # instance)
    self._set_custom_metrics()
    # Call base function
    super(AugmentedTensorBoard, self).on_train_batch_end(batch, logs)

  def _log_metrics(self, logs, prefix, step):
    """
    Writes metrics out as custom scalar summaries.

    :param logs: Dict. Keys are scalar summary names, values are NumPy scalars.
    :param prefix: String. The prefix to apply to the scalar summary names.
    :param step: Int. The global step to use for TensorBoard.
    """
    # Confusion matrix
    logs = self._log_confusion_matrix(logs, prefix, step)
    # Segmentation mask
    logs = self._log_segmentation_mask(logs, prefix, step)
    # Segmentation mask
    logs = self._log_image_pair(logs, prefix, step)
    # Call base method
    super(AugmentedTensorBoard, self)._log_metrics(logs, prefix, step)

  def _write_log_for(self, metric_names, logs, prefix, step, write_fn):
    """
    Extract and dump logs into summaries
    :param metric_names:  List of names to dump
    :param logs:          Dictionary of metrics
    :param prefix:        Prefix to append before logging name (epoch/batch)
    :param step:          Current step
    :param write_fn:      Callable function in form of
                          `write_fn(name, value, step)`
    """
    # Parse logs to extract information of interest -> given by names
    logs_by_writer = {self._train_run_name: [],
                      self._validation_run_name: []}
    validation_prefix = 'val_'
    for name, value in logs.items():
      if any([x in name for x in metric_names]):
        # Found our confusion matrix results
        if name.startswith(validation_prefix):
          # Validation results
          name = name[len(validation_prefix):]
          w_name = self._validation_run_name
        else:
          # Train
          w_name = self._train_run_name
        name = prefix + name  # assign batch or epoch prefix
        logs_by_writer[w_name].append((name, value))
    for n in metric_names:
      # Dump to summary
      with context.eager_mode():
        with summary_ops_v2.always_record_summaries():
          for writer_name in logs_by_writer:
            these_logs = logs_by_writer[writer_name]
            if not these_logs:
              # Don't create a "validation" events file if we don't
              # actually have any validation data.
              continue
            writer = self._get_writer(writer_name)
            with writer.as_default():
              for (name, value) in these_logs:
                write_fn(name, value, step)
            writer.flush()
      # Remove from original logs to avoid double logging
      to_rmv = [x for x in logs.keys() if n in x]
      for v in to_rmv:
        logs.pop(v)
    return logs

  def _log_confusion_matrix(self, logs, prefix, step):
    """
    Log confusion matrix
    :param logs:    Dict holding metric's results
    :param prefix:  Summaries prefix
    :param step:    Current step
    """
    def _write_cm(name, value, step):
      _write_confusion_matrix(name,
                              value,
                              step,
                              self._labels,
                              self._cmap,
                              self._figsize,
                              self._dpi)
    # Record entry
    return self._write_log_for(metric_names=self._cm_metric_name,
                               logs=logs,
                               prefix=prefix,
                               step=step,
                               write_fn=_write_cm)

  def _log_segmentation_mask(self, logs, prefix, step):
    """
    Log segmentation mask
    :param logs:    Dict holding metric's results
    :param prefix:  Summaries prefix
    :param step:    Current step
    """
    def _write_mask(name, value, step):
      cmap = _select_cmap(name, self._seg_mask_name, self._seg_mask_cmap)
      _write_image(name,
                   value,
                   step,
                   self._max_img_outputs,
                   cmap=cmap)
    # Record entry
    return self._write_log_for(metric_names=self._seg_mask_name,
                               logs=logs,
                               prefix=prefix,
                               step=step,
                               write_fn=_write_mask)

  def _log_image_pair(self, logs, prefix, step):
    """
    Log ImagePair metrics
    :param logs:    Dict holding metric's results
    :param prefix:  Summaries prefix
    :param step:    Current step
    """
    def _write_img(name, value, step):
      cmap = _select_cmap(name, self._im_pair_name, self._im_pair_cmap)
      _write_image(name,
                   value,
                   step,
                   self._max_img_outputs,
                   cmap=cmap)
    # Record entry
    return self._write_log_for(metric_names=self._im_pair_name,
                               logs=logs,
                               prefix=prefix,
                               step=step,
                               write_fn=_write_img)

