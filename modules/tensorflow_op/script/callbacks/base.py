# coding=utf-8
""" Basic keras callbacks """
from os.path import exists as _exists
from os import makedirs as _mkdir
from tensorflow.keras.callbacks import Callback

__author__ = 'Christophe Ecabert'


class PeriodicModelInference(Callback):
  """ Run model inference periodically during training phase """

  def __init__(self,
               input_fn,
               output_fn,
               logdir='inference',
               step='begin'):
    """
    Constructor

    The callback follow this steps:
      inputs = input_fn()
      outputs = model(inputs)
      output_fn(outputs, n_epoch)

    :param input_fn:  Callable function in form of `input_fn()` that return
                      the data needed by the model to perform evaluations.
    :param output_fn: Callable function in form of `output_fn(outputs, epoch,
                      logdir)` that handle model's output.
    :param logdir:  Location where to place the generate output
    :param step:  When to perform evaluation. Possible value are `begin` or
                  `end` of epoch
    """
    super(PeriodicModelInference, self).__init__()
    self.input_fn = input_fn
    self.output_fn = output_fn
    self.logdir = logdir
    if not _exists(self.logdir):
      _mkdir(self.logdir)
    self.step = step
    if self.step not in ['begin', 'end']:
      raise ValueError('`step` parameter should be either `begin` or `end` '
                       'but not {}!'.format(self.step))

  def on_epoch_begin(self, epoch, logs=None):
    """
    Called at the start of an epoch.
    :param epoch: integer, index of epoch.
    :param logs: dict. Currently no data is passed to this argument for this
                 method but that may change in the future.
    """

    if self.step == 'begin':
      self._run_inference(epoch)

  def on_epoch_end(self, epoch, logs=None):
    """
    Called at the end of an epoch.
    :param epoch:  integer, index of epoch.
    :param logs:  dict, metric results for this training epoch, and for the
                  validation epoch if validation is performed. Validation result
                  keys are prefixed with val_
    """
    if self.step == 'end':
      self._run_inference(epoch)

  def _run_inference(self, epoch):
    """
    Run model inference at a given epoch
    :param epoch:   integer, index of epoch.
    """
    inputs = self.input_fn()
    # We don't use model.predict() method directly since it is assumed to be run
    # on a single batch.
    # https://www.tensorflow.org/api_docs/python/tf/keras/Model#predict
    outputs = self.model.predict(inputs)
    self.output_fn(outputs, epoch, self.logdir)
