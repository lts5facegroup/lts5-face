# coding=utf-8
""" Beholder plugin """
from os.path import join as _join
from os.path import getmtime as _getmtime
from time import time
import tensorflow as tf
from tensorflow.keras.callbacks import Callback
import tensorboard.plugins.beholder.shared_config as cfg
import tensorboard.plugins.beholder.file_system_tools as fs_tools
from tensorboard.plugins.beholder import im_util
from tensorboard.plugins.beholder.visualizer import Visualizer


class Beholder(Callback):

  def __init__(self, logdir):
    """
    Constructor
    :param logdir:  Location where to place `Beholder` data/config
    """
    super(Beholder, self).__init__()
    # Log dir
    self.logir = _join(logdir, 'plugins', cfg.PLUGIN_NAME)
    # Config
    self.last_image_shape = []
    self.last_update_time = time.time()
    self.config_last_modified_time = -1
    self.previous_config = dict(cfg.DEFAULT_CONFIG)

    # Plugin config
    if not tf.io.gfile.exists(_join(self.logir, cfg.CONFIG_FILENAME)):
      tf.io.gfile.makedirs(self.logir)
      fs_tools.write_pickle(cfg.DEFAULT_CONFIG,
                            _join(self.logir, cfg.CONFIG_FILENAME))
    # Visualizer
    self.visualizer = Visualizer(self.PLUGIN_LOGDIR)

  def _get_config(self):
    """
    Read cached config file from disk or create a new one.
    :return:  Configuration
    """
    fname = _join(self.logir, cfg.CONFIG_FILENAME)
    modified_time = _getmtime(fname)

    if modified_time != self.config_last_modified_time:
      config = fs_tools.read_pickle(fname, default=self.previous_config)
      self.previous_config = config
    else:
      config = self.previous_config

    self.config_last_modified_time = modified_time
    return config

  def _get_final_image(self, config, arrays=None, frame=None):
    """
    Generate image
    :param config:  Configuration
    :param arrays:  Arrays
    :param frame:   Frames
    :return:  Weight frame
    """
    if config['values'] == 'frames':
      if frame is None:
        final_image = im_util.get_image_relative_to_script('frame-missing.png')
      else:
        frame = frame() if callable(frame) else frame
        final_image = im_util.scale_image_for_display(frame)



  def on_epoch_end(self, epoch, logs=None):
    super(Beholder, self).on_epoch_end(epoch, logs)
    self.beholder.update(session=self.session)
