# coding=utf-8
""" Scheduling Callbacks """
from math import pi
from math import cos
from math import floor
from tensorflow.keras.callbacks import Callback
import tensorflow.keras.backend as K

__author__ = 'Christophe Ecabert'


class PlateauScheduler:
  """ Generate learning rate with plateau for a given set of boundaries"""

  def __init__(self,
               lr: list,
               boundaries: list):
    """
    Constructor
    :param lr: List of learning rate that specifies the values for the intervals
     defined by `boundaries`.
    :param boundaries: A list of values with strictly increasing entries.
    """
    if not (isinstance(lr, (list, tuple)) and
            isinstance(boundaries, (list, tuple))):
      raise ValueError('`lr` and `boundaries` must be a list/tuple')
    if len(boundaries) + 1 != len(lr):
      raise ValueError('The length of `boundaries` should be 1 less than the '
                       'length of `lr`')
    self.lr = lr
    self.bnd = boundaries

  def __call__(self, epoch):
    """
    Compute learning rate for a given epoch
    :param epoch: Epoch number
    :return:  Learning rate
    """
    # Piece wise schedule
    for bnd, lr in zip(self.bnd, self.lr):
      if epoch < bnd:
        return lr
    # reach the last "piece"
    return self.lr[-1]


class StepLearningRateSchedule(Callback):
  """ Step learning rate scheduler callback """

  def __init__(self, lr, boundaries):
    """
    Constructor
    :param lr: List of learning rate that specifies the values for the intervals
     defined by `boundaries`.
    :param boundaries: A list of values with strictly increasing entries.
    """
    super(StepLearningRateSchedule, self).__init__()
    self.sched = PlateauScheduler(lr, boundaries)
    self.base_lr = lr[0]
    self.wd = None

  # calculate and set learning rate at the start of the epoch
  def on_epoch_begin(self, epoch, logs=None):
    """
    Calculate and set learning rate at the beginning of new epoch
    :param epoch: Epoch number
    :param logs:  Logs
    """
    lr = self.sched(epoch)
    factor = lr / self.base_lr
    # set learning rate
    K.set_value(self.model.optimizer.lr, lr)
    # Weight decay at optimizer level need to be taken care of
    if hasattr(self.model.optimizer, 'weight_decay'):
      if self.wd is None:
        # Save base weight_decay factor
        self.wd = K.get_value(self.model.optimizer.weight_decay)
      # Update weight_decay factor
      new_wd = self.wd * factor
      K.set_value(self.model.optimizer.weight_decay, new_wd)

  def on_epoch_end(self, epoch, logs=None):
    """ Log learning rate """
    logs = logs or {}
    logs['lr'] = K.get_value(self.model.optimizer.lr)
    if hasattr(self.model.optimizer, 'weight_decay'):
      logs['weight_decay'] = K.get_value(self.model.optimizer.weight_decay)


class CosineAnnealingLearningRateSchedule(Callback):
  """
  Cosine annealing learning rate scheduler callback
  See:
  - https://machinelearningmastery.com/snapshot-ensemble-deep-learning-neural-network/
  - https://arxiv.org/pdf/1704.00109.pdf
  """

  def __init__(self,
               n_epochs,
               n_cycles,
               lr,
               boundaries=None):
    """
    Constructor
    :param n_epochs:    Total number of epoch to train for
    :param n_cycles:    Number of cycle to perform over the whole training phase
    :param lr:          float: Base learning rate
                        list float: List of learning rate that specifies the
                        values for the intervals defined by `boundaries`.
    :param boundaries:  A list of values with strictly increasing entries. Each
                        element must be a multiple multiple of
                        floor(n_epoch, n_cycle)
    """
    super(CosineAnnealingLearningRateSchedule, self).__init__()
    self.n_epochs = n_epochs
    self.n_cycles = n_cycles
    self.lr = lr
    self.wd = None    # weight_decay
    self.plateau = boundaries
    if isinstance(boundaries, list):
      # Ensure each bnd are multiple of floor(n_epoch, n_cycle)
      epochs_per_cycle = floor(self.n_epochs / self.n_cycles)
      for bnd in boundaries:
        if bnd % epochs_per_cycle != 0:
          raise ValueError('Each element in `boundaries` must be multiple of'
                           ' floor(`n_epoch`, `n_cycle`). {} does not satisfy '
                           'this constraint'.format(bnd))
      # Plateau
      self.plateau = PlateauScheduler(self.lr, boundaries)

  def _base_learning_rate(self, epoch):
    if self.plateau:
      # Piece wise schedule
      return self.plateau(epoch)
    else:
      # Constant all the time
      return self.lr

  def _cosine_annealing(self, epoch):
    """
    Compute cosine annealing factor
    :param epoch: Current epoch
    :return:      scaling factor
    """
    epochs_per_cycle = floor(self.n_epochs / self.n_cycles)
    cos_inner = (pi * (epoch % epochs_per_cycle)) / epochs_per_cycle
    return 0.5 * (cos(cos_inner) + 1.0)

  # calculate and set learning rate at the start of the epoch
  def on_epoch_begin(self, epoch, logs=None):
    """
    Calculate and set learning rate at the beginning of new epoch
    :param epoch:
    :param logs:
    :return:
    """
    # calculate learning rate
    base_lr = self._base_learning_rate(epoch)
    factor = self._cosine_annealing(epoch)
    lr = base_lr * factor
    # set learning rate
    K.set_value(self.model.optimizer.lr, lr)
    # Weight decay at optimizer level need to be taken care of
    #   step = tf.Variable(0, trainable=False)
    #   schedule = tf.optimizers.schedules.PiecewiseConstantDecay(
    #      [10000, 15000], [1e-0, 1e-1, 1e-2])
    #   # lr and wd can be a function or a tensor
    #   lr = 1e-1 * schedule(step)
    #   wd = lambda: 1e-4 * schedule(step)
    #   optimizer = tfa.optimizers.AdamW(learning_rate=lr, weight_decay=wd)
    if hasattr(self.model.optimizer, 'weight_decay'):
      if self.wd is None:
        # Save base weight_decay factor
        self.wd = K.get_value(self.model.optimizer.weight_decay)
      # Update weight_decay factor
      new_wd = self.wd * factor
      K.set_value(self.model.optimizer.weight_decay, new_wd)

  def on_epoch_end(self, epoch, logs=None):
    """ Log learning rate """
    logs = logs or {}
    logs['lr'] = K.get_value(self.model.optimizer.lr)
    if hasattr(self.model.optimizer, 'weight_decay'):
      logs['weight_decay'] = K.get_value(self.model.optimizer.weight_decay)
