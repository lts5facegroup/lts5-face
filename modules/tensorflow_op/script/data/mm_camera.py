# coding=utf-8
""" Camera parameters abstraction structure """
import numpy as np
from lts5.utils.pyutils import RodriguesFloat as Rodrigues
__author__ = 'Christophe Ecabert'


class MMCamera(object):
  """ Camera abstraction for morphable model generation (decoder)

  """

  def __init__(self, angle, axis, position):
    """
    Constructor

    :param angle:   Rotation angle along a given axis in degree
    :param axis:    Rotation axis
    :param position:    Camera position in the space (x,y,z)
    """
    r = Rodrigues(axis=axis, angle=np.deg2rad(angle))
    self._params = np.zeros((1, 6), dtype=np.float32)
    self._params[0, 0] = r.x
    self._params[0, 1] = r.y
    self._params[0, 2] = r.z
    self._params[0, 3] = position[0]
    self._params[0, 4] = position[1]
    self._params[0, 5] = position[2]

  @classmethod
  def from_image_size(cls, width, height):
    """
    Construct the camera from the image dimensions

    :param width:   Image width
    :param height:  Image height
    :return:    Camera object
    """
    dmax = max(width, height)
    return cls(angle=0.0,
               axis=(0.0, 1.0, 0.0),
               position=(0.0, 0.0, dmax * 1.5))

  @classmethod
  def from_image_and_orientation(cls, width, height, angle, axis):
    """
    Construct the camera from the image dimensions and the orientation

    :param width:   Image width
    :param height:  Image height
    :param angle:   Rotation angle
    :param axis:    Rotation axis
    :return:    Camera object
    """
    dmax = max(width, height)
    return cls(angle=angle,
               axis=axis,
               position=(0.0, 0.0, dmax * 1.5))

  @classmethod
  def from_attitude(cls, angle, axis, pos):
    """
    Construct the camera from the orientation/position

    :param angle:   Rotation angle
    :param axis:    Rotation axis
    :param pos:     Object position
    :return:    Camera object
    """
    return cls(angle=angle,
               axis=axis,
               position=pos)

  @property
  def params(self):
    """
    Give a readonly collection of parameters of the camera
    :return:    Camera parameters ordered by:
                [gamma, theta, phi, pos_x, pos_y, pos_z, focal]
    """
    return self._params

  @property
  def n_params(self):
    """
    Indicate the total number of parameters of the camera
    :return: 6
    """
    return self._params.shape[1]
