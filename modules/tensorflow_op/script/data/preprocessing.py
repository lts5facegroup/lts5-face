# coding=utf-8
""" Implement various pre-processing step that can be used with tf.dataset API
"""
import tensorflow as tf


def _smallest_size_at_least(height, width, smallest_side):
  """
  Computes new shape with the smallest side equal to `smallest_side` while
  preserving the original aspect ratio.

  :param height:  an int32 scalar tensor indicating the current height.
  :param width:   an int32 scalar tensor indicating the current width.
  :param smallest_side: A python integer or scalar `Tensor` indicating the size
                        of the smallest side after resize.

  :returns: An int32 scalar tensor indicating the new height and an int32 scalar
            tensor indicating the new width.
  """
  smallest_side = tf.convert_to_tensor(smallest_side, dtype=tf.int32)

  height = tf.cast(height, dtype=tf.float32)
  width = tf.cast(width, dtype=tf.float32)
  smallest_side = tf.cast(smallest_side, dtype=tf.float32)

  scale = tf.cond(tf.greater(height, width),
                  lambda: smallest_side / width,
                  lambda: smallest_side / height)
  new_height = tf.cast(tf.math.rint(height * scale), dtype=tf.int32)
  new_width = tf.cast(tf.math.rint(width * scale), dtype=tf.int32)
  return new_height, new_width


def _largest_size_at_most(height, width, largest_side):
  """
  Computes new shape with the smallest side equal to `smallest_side` while
  preserving the original aspect ratio.

  :param height:        an int32 scalar tensor indicating the current height.
  :param width:         an int32 scalar tensor indicating the current width.
  :param largest_side:  A python integer or scalar `Tensor` indicating the size
                        of the largest side after resize.
  :return: An int32 scalar tensor indicating the new height and an int32 scalar
           tensor indicating the new width.
  """
  largest_side = tf.convert_to_tensor(largest_side, dtype=tf.int32)
  height = tf.cast(height, dtype=tf.float32)
  width = tf.cast(width, dtype=tf.float32)
  largest_side = tf.cast(largest_side, dtype=tf.float32)
  # compute scaling factor
  scale = tf.cond(tf.greater(height, width),
                  lambda: largest_side / height,
                  lambda: largest_side / width)
  new_height = tf.cast(tf.math.rint(height * scale), dtype=tf.int32)
  new_width = tf.cast(tf.math.rint(width * scale), dtype=tf.int32)
  return new_height, new_width


def _largest_size_at_most_v2(height, width, largest_side):
  """
  Computes new shape with the smallest side equal to `smallest_side` while
  preserving the original aspect ratio.

  :param height:        an int32 scalar tensor indicating the current height.
  :param width:         an int32 scalar tensor indicating the current width.
  :param largest_side:  A python integer or scalar `Tensor` indicating the size
                        of the largest side after resize.
  :return: An int32 scalar tensor indicating the new height and an int32 scalar
           tensor indicating the new width.
  """
  largest_side = tf.cast(largest_side, dtype=tf.int32)
  height = tf.cast(height, dtype=tf.float32)
  width = tf.cast(width, dtype=tf.float32)
  largest_side = tf.cast(largest_side, dtype=tf.float32)
  if height > width:
    scale = largest_side / height
  else:
    scale = largest_side / width
  new_height = tf.cast(tf.math.rint(height * scale), dtype=tf.int32)
  new_width = tf.cast(tf.math.rint(width * scale), dtype=tf.int32)
  return new_height, new_width


__side_selector = {'min': _smallest_size_at_least,
                   'max': _largest_size_at_most}


def aspect_preserving_resize(image, side, side_type='Min'):
  """
  Resize an image preserving the aspect ratio.
  :param image:     Image
  :param side:      Image minimum/maximum side dimensions
  :param side_type: `Min` or `Max`
  :returns:  Resized image
  """
  shape = tf.shape(image)
  im_rank = image.get_shape().rank
  if im_rank == 3:
    idx = 0
  elif im_rank == 4:
    idx = 1
  else:
    raise ValueError('Image must be either 3/4 rank!')
  h = shape[idx]
  w = shape[idx + 1]
  # Pick size function
  # comp_type = side_type.lower()
  # if comp_type == 'min':
  #   _size_fn = _smallest_size_at_least
  # elif comp_type == 'max':
  #   _size_fn = _largest_size_at_most
  # else:
  #   raise ValueError('Unsupported type: {}'.format(side_type))
  new_h, new_w = h, w #_largest_size_at_most_v2(h, w, side)
  resized_img = tf.image.resize(images=image,
                                size=[new_h, new_w],
                                method=tf.image.ResizeMethod.BILINEAR)
  return resized_img


def remove_mean(image, mean):
  """
  Remove mean value for the overall image
  :param image: Image
  :param mean:  Mean value to remove for each pixels
  :return:  Processed image
  """
  img = tf.subtract(image, tf.convert_to_tensor(mean, dtype=tf.float32))
  return img


def rgb_to_bgr(image):
  """
  Convert an RGB image into its cBGR counter part
  :param image: Image
  :return:  Converted image
  """
  r, g, b = tf.unstack(image, axis=-1)
  bgr = tf.stack([b, g, r], axis=-1)
  return bgr


def crop(image, off_h, off_w, crop_h, crop_w):
  """
  Crops the given image using the provided offsets and sizes.

  Note that the method doesn't assume we know the input image size but it does
  assume we know the input image rank.

  :param image: An image of shape [height, width, channels].
  :param off_h: A scalar tensor indicating the height offset.
  :param off_w: A scalar tensor indicating the width offset.
  :param crop_h:  The height of the cropped image
  :param crop_w:  The width of the cropped image
  :return:  The cropped (and resized) image
  """
  shape = tf.shape(image)
  shp = shape.get_shape().as_list()
  r_assert = tf.Assert(tf.equal(tf.rank(image), 3),
                       ['Rank of image must be equal to 3.'])
  with tf.control_dependencies([r_assert]):
    cropped_shape = tf.stack([crop_h, crop_w, shp[-1]])
  offsets = tf.to_int32(tf.stack([off_h, off_w, 0]))
  # Use tf.slice instead of crop_to_bounding box as it accepts tensors to
  # define the crop size.
  sz_assert = tf.Assert(tf.logical_and(tf.greater_equal(shape[0], crop_h),
                                       tf.greater_equal(shape[1], crop_w)),
                        ['Crop size greater than the image size.'])
  with tf.control_dependencies([sz_assert]):
    roi = tf.slice(image, offsets, cropped_shape)
    img = tf.reshape(roi, cropped_shape)
  return img


def central_crop(image, crop_h, crop_w):
  """
  Performs central crop of the given image
  https://github.com/tensorflow/models/blob/master/research/slim/preprocessing/vgg_preprocessing.py
  :param image: Image to be cropped
  :param crop_h:  Crop dimensions (Height)
  :param crop_w:  Crop dimensions (Width)
  :return:  Cropped image
  """
  # Define bbox
  shape = tf.shape(image)
  img_h = shape[0]
  img_w = shape[1]
  off_h = (img_h - crop_h) / 2
  off_w = (img_w - crop_w) / 2
  return crop(image, off_h, off_w, crop_h, crop_w)



