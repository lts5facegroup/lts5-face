# coding=utf-8
"""
Interface abstraction for dataset / data augmentation handling. This abstraction
 layer encapsulate the concept of `tf.data.Dataset`.

Link:
- https://www.tensorflow.org/api_docs/python/tf/data/Dataset#top_of_page
"""
from abc import ABC
from abc import abstractmethod
import tensorflow as tf
__author__ = 'Christophe Ecabert'


class DataProvider(ABC):
  """
  Abstraction layer around `tf.data.Dataset` class for convenience.
  """

  def __init__(self):
    """ Constructor, should be overloaded by uesr. """
    self._batch_size = None

  @property
  def _source(self):
    return self.__source

  @_source.setter
  def _source(self, src):
    assert isinstance(src, tf.data.Dataset)
    self.__source = src

  def output_classes(self):
    """
    Returns the class of each component of an element of this dataset.

    :return: A nested structure of Python type objects corresponding to each
             component of an element of this dataset.
    """
    return None if self._source is None else self._source.output_classes

  def output_shapes(self):
    """
    Returns the shape of each component of an element of this dataset.

    :return: A nested structure of tf.TensorShape objects corresponding to
             each component of an element of this dataset.
    """
    return None if self._source is None else self._source.output_shapes

  def output_types(self):
    """
    Returns the type of each component of an element of this dataset.

    :return: A nested structure of tf.DType objects corresponding to each
             component of an element of this dataset.
    """
    return None if self._source is None else self._source.output_types

  @abstractmethod
  def decode(self):
    """
    Function parsing input dataset and generate tensors (i.e. image + labels)
    :return: A `DataProvider`
    """
    pass

  @abstractmethod
  def preprocess(self):
    """
    Apply all the pre-processing / augmentation steps on the dataset
    :return: A `DataProvider`
    """
    pass

  def batch(self, batch_size, drop_remainder):
    """
    Combines consecutive elements of this dataset into batches.

    :param batch_size:   The number of consecutive elements of this dataset
                         to combine in a single batch.
    :param drop_remainder:  Indicate if remainding part of the set need to be
                            dropped. If dataset size is not a multiple of the
                            batch size
    :return: A `DataProvider`
    """
    self._batch_size = batch_size
    self._source = self._source.batch(batch_size=batch_size,
                                      drop_remainder=drop_remainder)
    return self

  def cache(self, filename=''):
    """
    Caches the elements in this dataset.

    :param filename: The name of a directory on the filesystem to use for
                     caching tensors
    :return: A `DataProvider`
    """
    self._source = self._source.cache(filename=filename)
    return self

  def map(self, map_func, n_parallel_call):
    """
    Maps `map_func` across this dataset.

    :param map_func:  A function mapping a nested structure of tensors to
                      another nested structure of tensors
    :param n_parallel_call: Number elements to process in parallel, should
                            be equal to the number of core of the machine.
    :return: A `DataProvider`
    """
    self._source = self._source.map(map_func, n_parallel_call)
    return self

  def prefetch(self, buffer_size):
    """
    Creates a `DataProvider` that prefetches elements from this dataset.

    :param buffer_size:  The maximum number of elements that will be
                         buffered when prefetching.
    :return: A `DataProvider`
    """
    self._source = self._source.prefetch(buffer_size=buffer_size)
    return self

  def repeat(self, count=-1):
    """
    Repeats this dataset `count` times.

    :param count: The number of times the dataset should be repeated.
    :return: A `DataProvider`
    """
    self._source = self._source.repeat(count=count)
    return self

  def shard(self, n_shards, index):
    """
    Creates a Dataset that includes only 1/n_shards of this dataset.
    NOTE:
      Be sure to shard before you use any randomizing operator (such as shuffle)
    :param n_shards:  The number of shards operating in parallel.
    :param index: The worker index
    :return: A `DataProvider`
    """
    self._source = self._source.shard(num_shards=n_shards, index=index)
    return self

  def shuffle(self, buffer_size, seed=None, reshuffle_each_iteration=None):
    """
    Randomly shuffles the elements of this dataset.

    :param buffer_size: Number of elements from this dataset from which the
                        new dataset will sample
    :param seed: Seed that will be used to create the distribution
    :param reshuffle_each_iteration: true indicates that the dataset should
                                     be pseudorandomly reshuffled each time
                                     it is iterated over
    :return: A `DataProvider`
    """
    self._source = self._source.shuffle(buffer_size,
                                        seed,
                                        reshuffle_each_iteration)
    return self

  def one_shot_iterator(self):
    """
    Return a `tf.data.Dataset` iterator
    :return:    Iterator
    """
    return self._source#.make_one_shot_iterator()

  @property
  def batch_size(self):
      return self._batch_size

