# coding=utf-8
""" Utility function for image processing """
import tensorflow as tf
from tensorflow.python.framework import tensor_util


def smart_resize(images,
                 size,
                 method=tf.image.ResizeMethod.BILINEAR,
                 preserve_aspect_ratio=False,
                 antialias=False,
                 name=None):
  """
  Resize `images` to `size` using the specified `method`. If `images` shape
    match the target `size` no operation will be performed.

  :param images: 4-D Tensor of shape `[batch, height, width, channels]` or
    3-D Tensor of shape `[height, width, channels]`.
  :param size: A 1-D int32 Tensor of 2 elements: `new_height, new_width`.  The
    new size for the images.
  :param method: An `tf.image.ResizeMethod`, or string equivalent.  Defaults to
    `bilinear`.
  :param preserve_aspect_ratio: Whether to preserve the aspect ratio. If this is
    set, then `images` will be resized to a size that fits in `size` while
    preserving the aspect ratio of the original image. Scales up the image if
    `size` is bigger than the current size of the `image`. Defaults to False.
  :param antialias: Whether to use an anti-aliasing filter when downsampling an
    image.
  :param name:
  :return: A name for this operation (optional).
  """
  images = tf.convert_to_tensor(images, name='images')
  size = tf.convert_to_tensor(size, tf.int32, name='size')
  # Does shape match
  if images.shape.rank == 4:
    height, width = images.shape[1:3]
  elif images.shape.rank == 3:
    height, width = images.shape[0:3]
  else:
    raise ValueError('\'images\' must have either 3 or 4 dimensions.')
  # Get image size
  size_const_as_shape = tensor_util.constant_value_as_shape(size)
  new_height_const = size_const_as_shape.dims[0].value
  new_width_const = size_const_as_shape.dims[1].value
  if all([x is not None
          for x in [new_height_const, new_width_const, width, height]]) and (
      width == new_width_const and height == new_height_const):
    return images
  return tf.image.resize(images,
                         size,
                         method,
                         preserve_aspect_ratio,
                         antialias,
                         name)


