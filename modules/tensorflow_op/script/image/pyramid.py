# coding=utf-8
""" Gaussian pyramid """
import numpy as np
import tensorflow as tf
__author__ = 'Christophe Ecabert'


def _get_kernel(ksize, sigma):
  """
  Create 1D gaussian kernel. Based on OpenCV
  :param ksize: Filter size
  :param sigma: Gaussian standard deviation
  :return:  Kernel
  """
  sigmaX = sigma if sigma > 0.0 else ((ksize - 1) * 0.5 - 1) * 0.3 + 0.8
  scale2X = -0.5 / (sigmaX ** 2.0)
  k = np.arange(ksize).reshape(-1, 1)
  k = k - ((ksize - 1.0) * 0.5)
  k = np.exp((k ** 2.0) * scale2X)
  k /= k.sum()
  return k


def _gaussian_filter(images, ksize, sigma, stride):
  """
  Apply gaussian filter on a given image
  :param images:  Images to filter
  :param ksize:   Kernel size
  :param sigma:   Gaussian standard deviation
  :param stride:  Kernel stride
  :return:  Filtered images
  """
  # Gaussian kernel of dims [ksize x ksize]
  k = _get_kernel(ksize=ksize, sigma=sigma)
  k = k @ k.T
  k = tf.constant(k, tf.float32, shape=[ksize, ksize, 1, 1])
  # Duplicate kernel based on the number of channels in images
  # Filter must have dims: [ksize, ksize, in_channels, channel_multiplier]
  if tf.executing_eagerly():
    n_channel = images.get_shape().as_list()[-1]
    kernel = tf.concat([k] * n_channel, axis=2)
  else:
    n_channel = tf.shape(images)[-1]
    kernel = tf.tile(k, [1, 1, n_channel, 1])
  # Apply convolution, input needs to be rank4, expand first dims if not ok.
  in_rank = tf.rank(images)
  if in_rank == 3:
    images = tf.expand_dims(images, 0)
  output = tf.nn.depthwise_conv2d(images,
                                kernel,
                                strides=[1, stride, stride, 1],
                                padding='VALID')
  if in_rank == 3:
    output = tf.squeeze(output, 0)
  return output


def gaussian_pyramid(images, max_level, ksize=5, sigma=1.0):
  """
  Create a Gaussian pyramid
  :param images:     Image at level 0
  :param max_level:  Number of level
  :param ksize:      Kernel size
  :param sigma:      Gaussian standard deviation
  :return:  List of images, one entry for each level
  """
  im_pyr = []
  im_gauss = images
  for level in range(max_level):
    im_gauss = im_gauss if level == 0 else _gaussian_filter(im_gauss,
                                                            ksize=ksize,
                                                            sigma=sigma,
                                                            stride=2)
    im_pyr.append(im_gauss)
  return im_pyr
