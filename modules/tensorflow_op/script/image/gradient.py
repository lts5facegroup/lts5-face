# coding=utf-8
""" Image gradient """
import tensorflow as _tf
__author__ = 'Christophe Ecabert'


def spatial_gradient(images, name=None):
  """
  Calculate and return the spatial gradient for one or more images.

  The gradient is computed as finite difference.

  :param images:  4-D Tensor of shape `[batch, height, width, channels]` or
                  3-D Tensor of shape `[height, width, channels]`.
  :param name:    A name for the operation (optional).
  :return:  Magnitude of the gradient
  """
  with _tf.name_scope(name, 'spatial_gradient'):
    ndims = images.get_shape().ndims
    if ndims == 3:
      # The input is a single image with shape [height, width, channels].

      dx = (images[:, 1:, :] - images[:, :-1, :])
      dy = (images[1:, :, :] - images[:-1, :, :])
      grad = ((dx[:-1, :, :] ** 2.0) +
              (dy[:, :-1, :] ** 2.0) + 1e-8) ** 0.5
    elif ndims == 4:
      # The input is a batch of images with shape:
      # [batch, height, width, channels].
      dx = (images[:, :, 1:, :] - images[:, :, :-1, :])  # [N, H, W-1, C]
      dy = (images[:, 1:, :, :] - images[:, :-1, :, :])  # [N, H-1, W, C]
      grad = ((dx[:, :-1, :, :] ** 2.0) +
              (dy[:, :, :-1, :] ** 2.0) + 1e-8) ** 0.5
    else:
      raise ValueError('\'images\' must be either 3 or 4-dimensional.')
  return grad
