# coding=utf-8
"""
Implement loss aggregation. Given a list of tf.keras.losses.Loss object,
aggregate them together and log them into tensorboard. Logging is optional
"""
import tensorflow as tf
from tensorflow.keras.losses import Loss
from tensorflow.keras.backend import learning_phase
__author__ = 'Christophe Ecabert'


class LossAggregator(Loss):
  """
  Combine multiple `keras.losses.Loss` instance into a single callable object
   aggregating them together. Each component can be optionally logged into
   tensorboard summary.
  """

  def __init__(self,
               losses,
               display_names=None,
               manager=None,
               name='LossAggregator',
               **kwargs):
    """
    Constructor
    :param losses:  List, tuple of `keras.losses.Loss` instance to aggregate
    :param display_names: List of names to display in tensorboard, must have the
      same size as `losses`. If `None` will default to `loss.name`
    :param manager: Instance of `SummaryWriterManager` to use to record each
      losses into tensorboard (optional).
    :param name:  Loss name
    :param kwargs:  Extra keyword arguments
    :keyword reduction: Type of reduction
    """
    super(LossAggregator, self).__init__(name=name, **kwargs)
    # Sanity check on losses
    if not isinstance(losses, (list, tuple)):
      raise ValueError('`losses` must be a list/tuple')
    if not all([isinstance(loss, Loss) for loss in losses]):
      raise ValueError('Not all entry in `losses` are `keras.losses.Loss '
                       'instance.')
    self.losses = losses
    if display_names is None:
      self.display_names = [loss.name for loss in losses]
    else:
      self.display_names = display_names
      if len(losses) != len(display_names):
        raise ValueError('Missing entry in `display_names`, got {} != {}'
                         ''.format(len(display_names), len(losses)))
    # Does it needs logging ?
    self._logging_fn = None
    self._w_loss = None
    if manager is not None:
      # Writer
      self._w_loss = manager.get_writer('loss')
      # Log function
      def _log_fn(inputs, step, training):
        for value, name in zip(inputs, self.display_names):
          self._w_loss.scalar(name=name,
                              data=value,
                              step=step,
                              training=training)
      # Step counter
      self._step = tf.Variable(initial_value=0,
                               name='{}_step'.format(self.name),
                               dtype=tf.int64,
                               trainable=False)
      self._val_step = tf.Variable(initial_value=0,
                                   name='{}_val_step'.format(self.name),
                                   dtype=tf.int64,
                                   trainable=False)
      # Set log function
      self._logging_fn = _log_fn

  def call(self, y_true, y_pred):
    """
    Aggregate each loss
    :param y_true:  Tensor, ground truth
    :param y_pred:  Tensor, predicted values
    :return:  Loss
    """
    # Compute losses
    loss_values = [loss(y_true, y_pred) for loss in self.losses]
    # Log ?
    loss_values = self._log_value_if_needed(loss_values)
    # Sum all
    return tf.add_n(loss_values)

  def _log_value_if_needed(self, loss_values):
    """
    Log each entries if logging is enable
    :param loss_values: List of loss value to record
    :return: List of logged values
    """
    with tf.name_scope('logging') as scope:
      if self._logging_fn is not None:
        training = learning_phase()
        if training:
          self._logging_fn(loss_values, self._step, training)
          self._step.assign_add(1)
        else:
          self._logging_fn(loss_values, self._val_step, training)
          self._val_step.assign_add(1)
    return loss_values
