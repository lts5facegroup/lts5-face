# coding=utf-8
"""
Geometry based `tf.keras.losses` classes
"""
import tensorflow as _tf
from tensorflow.keras.losses import Loss
from lts5.tensorflow_op.probability import IsotropicGaussianLandmarkEvaluator
from lts5.tensorflow_op.probability import IsotropicLaplacianLandmarkEvaluator

__author__ = 'Christophe Ecabert'


class InterOccularDistance:
  """ Compute inter-occular distance for normalization """

  def __init__(self, n_points=68):
    """
    Constructor
    :param n_points:  Number of landmarks. Supported values are:
                      [11:
                       16: idiap
                       20: BioID
                       29: LFPW29
                       49: CMU68_inner_shape
                       58: CVHCI
                       66: CMU66
                       68: CMU68
                       76: MUCT]
    """
    supported = (11, 16, 20, 29, 49, 58, 66, 68, 76)
    if n_points not in supported:
      raise ValueError('#Points not supported {} not in {}'.format(n_points,
                                                                   supported))
    indices = {11: (0, 1, 2, 3),
               16: (2, 3, 4, 5),
               20: (9, 10, 11, 12),
               29: (8, 10, 11, 9),
               49: (19, 22, 25, 28),
               58: (21, 25, 17, 13),
               66: (36, 39, 42, 45),
               68: (36, 39, 42, 45),
               76: (27, 29, 34, 32)}
    self._idx = indices.get(n_points)

  def __call__(self, landmarks):
    """
    Compute intra-occular distance
    :param landmarks: Set of detected landmarks [Batch, K, 2]
    :return:  IoD
    """
    # Get eye's centers
    l_eye = (landmarks[:, self._idx[0], :] + landmarks[:, self._idx[1], :])
    l_eye /= 2.0
    r_eye = (landmarks[:, self._idx[2], :] + landmarks[:, self._idx[3], :])
    r_eye /= 2.0
    # Compute distance
    iod = _tf.norm(l_eye - r_eye, ord=2, axis=-1)  # [Batch, ]
    return iod


class LandmarkLogLikelihood(Loss):
  """ Landmarks loss function """

  def __init__(self,
               sigma,
               weight=1.0,
               sample_weights=None,
               type='l2',
               normalized=True,
               name='landmark_loss', **kwargs):
    """
    Constructor
    :param sigma:   Residual error standard deviation
    :param weight:  Likelihood contribution in the total loss
    :param sample_weights: Weights for individual samples
    :param type:  Distribution type: 'l1' or 'l2'
    :param normalized: If `True` normalize the distribution, otherwise biased
    :param name:    Loss name
    :param kwargs:  Extra keyword arguments
    """
    super(LandmarkLogLikelihood, self).__init__(name=name, **kwargs)
    self._sdev = sigma
    self._w_lms = weight
    self._type = type
    self._sample_weights = sample_weights
    if self._sample_weights is not None:
      self._sample_weights = _tf.convert_to_tensor(self._sample_weights)
    if self._type not in ('l1', 'l2'):
      raise ValueError('Unsupported type: {}! ({})'.format(self._type,
                                                           ('l1', 'l2')))
    if self._type == 'l2':
      self._eval = IsotropicGaussianLandmarkEvaluator(sdev=self._sdev,
                                                      ndim=2,
                                                      normalized=normalized)
    else:
      self._eval = IsotropicLaplacianLandmarkEvaluator(sdev=self._sdev,
                                                       ndim=2,
                                                       normalized=normalized)

  def call(self, y_true, y_pred):
    """
    Compute landmark's likelihood
    :param y_true:  Ground truth
    :param y_pred:  Predicted landmarks
    :return:  Negative log-likelihood
    """
    # Reshape to [Batch, K, 2]
    bs = _tf.shape(y_pred)[0]
    lms = _tf.reshape(y_true, shape=[bs, -1, 2])
    mm_lms = _tf.reshape(y_pred, shape=[bs, -1, 2])
    # Compute error
    l_lms = self._eval.log_value(first=mm_lms,
                                 second=lms,
                                 weights=self._sample_weights)  # [Batch, K]
    l_lms = _tf.reduce_mean(l_lms, axis=-1)             # [Batch,]
    # neg_ll = -self._w_lms * _tf.reduce_mean(l_lms)    # Scalar
    neg_ll = -self._w_lms * l_lms                       # Batch
    return _tf.debugging.check_numerics(neg_ll, 'Landmark loss is not valid')


class LandmarkLoss(Loss):
  """ Landmarks loss function """

  def __init__(self,
               weight,
               type='l2',
               sample_weights=None,
               name='landmark_loss',
               **kwargs):
    """
    Constructor
    :param weight:  Error contribution in the total loss
    :param sample_weights: Weights for individual samples
    :param type:  Error type: 'l1' or 'l2'
    :param name:    Loss name
    :param kwargs:  Extra keyword arguments
    """
    super(LandmarkLoss, self).__init__(name=name, **kwargs)
    self._w_lms = weight
    self._sample_weights = sample_weights
    if self._sample_weights is not None:
      self._sample_weights = _tf.convert_to_tensor(self._sample_weights)
    if type not in ('l1', 'l2'):
      raise ValueError('Unsupported type: {}! ({})'.format(type,
                                                           ('l1', 'l2')))
    self._type = 2 if type == 'l2' else 1

  def call(self, y_true, y_pred):
    """
    Compute landmark's likelihood
    :param y_true:  Ground truth
    :param y_pred:  Predicted landmarks
    :return:  L2 or L1 loss
    """
    # Reshape to [Batch, K, 2]
    bs = _tf.shape(y_pred)[0]
    lms = _tf.reshape(y_true, shape=[bs, -1, 2])
    mm_lms = _tf.reshape(y_pred, shape=[bs, -1, 2])
    # Compute error
    l_lms = _tf.norm(lms - mm_lms + 1e-7, ord=self._type, axis=-1)  # [Batch, K]
    if self._sample_weights is not None:
      l_lms *= self._sample_weights
    l_lms = _tf.reduce_mean(l_lms, axis=-1)                   # [Batch,]
    return self._w_lms * l_lms


class LandmarksPairLogLikelihood(Loss):
  """ Negative loglikelihood for landmark's pair distance """

  def __init__(self,
               pairs,
               sigma,
               weight=1.0,
               sample_weights=None,
               type='l2',
               normalized=True,
               name='landmark_pair_loss', **kwargs):
    """
    Constructor
    :param pairs:   List of pair indexes to compute distance for
    :param sigma:   Residual error standard deviation
    :param weight:  Likelihood contribution in the total loss
    :param sample_weights: Weights for individual samples
    :param type:  Distribution type: 'l1' or 'l2'
    :param normalized: If `True` normalize the distribution, otherwise biased
    :param name:    Loss name
    :param kwargs:  Extra keyword arguments
    """
    super(LandmarksPairLogLikelihood, self).__init__(name=name, **kwargs)
    self._sdev = sigma
    self._w_lms = weight
    self._type = type
    if not isinstance(pairs, list) or len(pairs[0]) != 2:
      raise ValueError('`pairs` must be a list of pair of indexes')
    self._pairs = _tf.convert_to_tensor(pairs)
    self._sample_weights = sample_weights
    if self._sample_weights is not None:
      self._sample_weights = _tf.convert_to_tensor(self._sample_weights)
    if self._type not in ('l1', 'l2'):
      raise ValueError('Unsupported type: {}! ({})'.format(self._type,
                                                           ('l1', 'l2')))
    if self._type == 'l2':
      self._eval = IsotropicGaussianLandmarkEvaluator(sdev=self._sdev,
                                                      ndim=2,
                                                      normalized=normalized)
    else:
      self._eval = IsotropicLaplacianLandmarkEvaluator(sdev=self._sdev,
                                                       ndim=2,
                                                       normalized=normalized)

  def call(self, y_true, y_pred):
    """
    Compute landmark's likelihood
    :param y_true:  Ground truth
    :param y_pred:  Predicted landmarks
    :return:  Negative log-likelihood
    """
    # Reshape to [Batch, K, 2]
    bs = _tf.shape(y_pred)[0]
    lms = _tf.reshape(y_true, shape=[bs, -1, 2])
    mm_lms = _tf.reshape(y_pred, shape=[bs, -1, 2])
    # Compute distance between landmarks
    e0_true = _tf.gather(lms, self._pairs[:, 0], axis=1)  # [Batch, M]
    e1_true = _tf.gather(lms, self._pairs[:, 1], axis=1)  # [Batch, M]
    d_true = e1_true - e0_true
    e0_pred = _tf.gather(mm_lms, self._pairs[:, 0], axis=1)  # [Batch, M]
    e1_pred = _tf.gather(mm_lms, self._pairs[:, 1], axis=1)  # [Batch, M]
    d_pred = e1_pred - e0_pred
    # Likelihood
    l_dist = self._eval.log_value(first=d_pred,
                                  second=d_true,
                                  weights=self._sample_weights)  # [Batch, M]
    l_dist = _tf.reduce_mean(l_dist, axis=-1)             # [Batch,]
    # neg_ll = -self._w_lms * _tf.reduce_mean(l_lms)      # Scalar
    neg_ll = -self._w_lms * l_dist                        # Batch
    return _tf.debugging.check_numerics(neg_ll, 'Landmark loss is not valid')


class LandmarksPairLoss(Loss):
  """ L2 or L1 norm for landmark's pair distance """

  def __init__(self,
               pairs,
               weight,
               sample_weights=None,
               type='l2',
               name='landmark_pair_loss',
               **kwargs):
    """
    Constructor
    :param pairs:   List of pair indexes to compute distance for
    :param weight:  Loss contribution in the total loss
    :param sample_weights: Weights for individual samples
    :param type:  Distribution type: 'l1' or 'l2'
    :param name:    Loss name
    :param kwargs:  Extra keyword arguments
    """
    super(LandmarksPairLoss, self).__init__(name=name, **kwargs)
    self._w_lms = weight

    if not isinstance(pairs, list) or len(pairs[0]) != 2:
      raise ValueError('`pairs` must be a list of pair of indexes')
    self._pairs = _tf.convert_to_tensor(pairs)
    self._sample_weights = sample_weights
    if self._sample_weights is not None:
      self._sample_weights = _tf.convert_to_tensor(self._sample_weights)
    if type not in ('l1', 'l2'):
      raise ValueError('Unsupported type: {}! ({})'.format(type,
                                                           ('l1', 'l2')))
    self._type = 2 if type == 'l2' else 1

  def call(self, y_true, y_pred):
    """
    Compute landmark's error
    :param y_true:  Ground truth
    :param y_pred:  Predicted landmarks
    :return:  Negative log-likelihood
    """
    # Reshape to [Batch, K, 2]
    bs = _tf.shape(y_pred)[0]
    lms = _tf.reshape(y_true, shape=[bs, -1, 2])
    mm_lms = _tf.reshape(y_pred, shape=[bs, -1, 2])
    # Compute distance between landmarks
    e0_true = _tf.gather(lms, self._pairs[:, 0], axis=1)  # [Batch, M, 2]
    e1_true = _tf.gather(lms, self._pairs[:, 1], axis=1)  # [Batch, M, 2]
    d_true = e1_true - e0_true
    e0_pred = _tf.gather(mm_lms, self._pairs[:, 0], axis=1)  # [Batch, M, 2]
    e1_pred = _tf.gather(mm_lms, self._pairs[:, 1], axis=1)  # [Batch, M, 2]
    d_pred = e1_pred - e0_pred
    # Likelihood
    l_dist = _tf.norm(d_true - d_pred + 1e-7, ord=self._type, axis=-1)
    if self._sample_weights is not None:
      l_dist *= self._sample_weights
    l_dist = _tf.reduce_mean(l_dist, axis=-1)             # [Batch,]
    return self._w_lms * l_dist
