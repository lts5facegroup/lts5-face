# coding=utf-8
""" Tensorflow keras loss function for classification

See:
  - https://github.com/umbertogriffo/focal-loss-keras
"""
import tensorflow as _tf
import tensorflow.keras.losses as kl
import tensorflow.keras.backend as K

__author__ = 'Christophe Ecabert'


class BinaryFocalLoss(kl.Loss):
  """
  Implementation of Focal Loss from the paper in multi-class classification
    Formula:
        loss = -alpha*((1-p)^gamma)*log(p)
  """

  def __init__(self,
               gamma=2.0,
               alpha=0.25,
               from_logits=False,
               name='BinaryFocalLoss',
               **kwargs):
    """
    Constructor
    :param gamma: Focusing parameter for modulating factor (1-p)
    :param alpha: The same as weighting factor in balanced cross entropy
    :param from_logits: If `True` indicates value from logits
    :param name:  Scope name
    :param kwargs:  Extra keyword arguments
    :keyword reduction: Type of reduction to apply
    """
    super(BinaryFocalLoss, self).__init__(name=name, **kwargs)
    self.alpha = alpha
    self.gamma = gamma
    self.from_logits = from_logits

  def call(self, y_true, y_pred):
    """
    Compute cost
    :param y_true:  True value
    :param y_pred:  Predicted class
    :return:  Cost for each sample in the batch
    """

    # Ref: https://github.com/tensorflow/addons/blob/v0.6.0/tensorflow_addons/losses/focal_loss.py
    eps = _tf.keras.backend.epsilon()
    y_pred = _tf.convert_to_tensor(y_pred)
    y_true = _tf.cast(y_true, y_pred.dtype)
    # Get the binary cross_entropy
    bce = _tf.keras.losses.binary_crossentropy(y_true,
                                               y_pred,
                                               from_logits=self.from_logits)
    bce = _tf.expand_dims(bce, -1)
    # If logits are provided then convert the predictions into probabilities
    if self.from_logits:
      y_pred = _tf.sigmoid(y_pred)
    else:
      y_pred = _tf.clip_by_value(y_pred, eps, 1.0 - eps)

    p_t = (y_true * y_pred) + ((1 - y_true) * (1 - y_pred))
    alpha_factor = 1
    modulating_factor = 1
    if self.alpha:
      alpha = _tf.convert_to_tensor(self.alpha, dtype=y_pred.dtype)
      alpha_factor = y_true * alpha + ((1.0 - alpha) * (1.0 - y_true))
    if self.gamma:
      gamma = _tf.convert_to_tensor(self.gamma, dtype=y_pred.dtype)
      modulating_factor = _tf.math.pow((1 - p_t), gamma)
    # compute the final loss and return
    loss = alpha_factor * modulating_factor * bce
    return loss


class BinaryCrossentropy(kl.Loss):
  """
  BinaryCrossentropy with support to `ignore_index` similar to caffe/pytorch
  """

  def __init__(self,
               ignore_index,
               from_logits=False,
               name='BinaryCrossentropy',
               **kwargs):
    """
    Constructor
    :param ignore_index:  Integer, index of the label to ignore
    :param from_logits:   Whether to interpret y_pred as a tensor of logit
                          values. By default, we assume that y_pred contains
                          probabilities (i.e., values in [0, 1]). Note: Using
                          from_logits=True may be more numerically stable.
    :param name:          Loss name
    :param kwargs:        Extra keyword arguments
    :keyword reduction:   Type of reduction to apply.
    """
    super(BinaryCrossentropy, self).__init__(name=name, **kwargs)
    self._ignore_idx = ignore_index
    self._from_logits = from_logits

  def call(self, y_true, y_pred):
    """
    Compute binary crossentropy
    :param y_true:  Ground truth values
    :param y_pred:  The predicted values
    :return:  Loss value averaged over last dimensions
    """
    y_pred = _tf.convert_to_tensor(y_pred)
    y_true = _tf.cast(y_true, y_pred.dtype)
    # Build weights
    labels = _tf.cast(y_true, _tf.int32)
    mask = _tf.cast(_tf.not_equal(labels, self._ignore_idx), _tf.float32)
    # Masked binary_cross entropy
    bce = K.binary_crossentropy(y_true,
                                y_pred,
                                from_logits=self._from_logits)
    bce = mask * bce
    # Average over last dim
    n_entry = _tf.reduce_sum(mask, axis=-1)
    bce = _tf.reduce_sum(bce, axis=-1) / n_entry
    return bce


class CategoricalFocalLoss(kl.Loss):
  """
  Implementation of Focal Loss from the paper in multi-class classification

    Formula:
        loss = -alpha*((1-p)^gamma)*log(p)
  """

  def __init__(self,
               gamma=2.0,
               alpha=0.25,
               name='CategoricalFocalLoss',
               **kwargs):
    """
    Constructor
    :param gamma:
    :param alpha:
    :param name:
    :param kwargs:
    """
    super(CategoricalFocalLoss, self).__init__(name=name, **kwargs)
    self.gamma = gamma
    self.alpha = alpha

  def call(self, y_true, y_pred):
    """
    Compute categorical focal loss
    :param y_true:  Label
    :param y_pred:  Prediction
    :return:  Loss
    """

    # Define epsilon so that the backpropagation will not result in NaN
    # for 0 divisor case
    eps = _tf.keras.backend.epsilon()
    # Clip the prediction value
    y_pred = _tf.clip_by_value(y_pred, eps, 1.0 - eps)
    # Calculate cross entropy
    cross_entropy = -y_true * _tf.math.log(y_pred)
    # Calculate weight that consists of  modulating factor and weighting factor
    weight = self.alpha * _tf.math.pow((1.0 - y_pred), self.gamma)
    # Calculate focal loss
    loss = weight * cross_entropy
    return loss


class SparseCategoricalCrossentropy(kl.Loss):
  """
  Sparse categorical crossentropy with support to `ignore_index` similar to
  caffe/pytorch
  """

  def __init__(self,
               ignore_index,
               from_logits=False,
               name='SparseCategoricalCrossentropy',
               **kwargs):
    """
    Constructor
    :param ignore_index:  Integer, index of the label to ignore
    :param from_logits:   Whether to interpret y_pred as a tensor of logit
                          values. By default, we assume that y_pred contains
                          probabilities (i.e., values in [0, 1]). Note: Using
                          from_logits=True may be more numerically stable.
    :param name:          Loss name
    :param kwargs:        Extra keyword arguments
    :keyword reduction:   Type of reduction to apply.
    """
    super(SparseCategoricalCrossentropy, self).__init__(name=name, **kwargs)
    self._ignore_idx = ignore_index
    self._from_logits = from_logits

  def call(self, y_true, y_pred):
    """
    Compute sparse categorical crossentropy loss
    :param y_true:  Label, integer
    :param y_pred:  Prediction
    :return:  Loss
    """
    y_pred = _tf.convert_to_tensor(y_pred)
    y_true = _tf.convert_to_tensor(y_true)
    # Build weights
    mask = _tf.cast(_tf.not_equal(y_true, self._ignore_idx),
                    _tf.float32)
    mask = _tf.squeeze(mask, axis=-1)
    # Masked categorical crossentropy
    cce = K.sparse_categorical_crossentropy(y_true,
                                            y_pred,
                                            from_logits=self._from_logits,
                                            axis=-1)
    cce = mask * cce
    return cce


class OnlineHardExampleMining(kl.Loss):
  """
  Apply online hard example mining using top-k samples inside the current batch
  """

  def __init__(self,
               loss_fn,
               threshold,
               min_sample,
               from_logits=False,
               name='OnlineHardExampleMining'):
    """
    Constructor
    :param loss_fn:     Loss function instance to apply (i.e. keras.Loss)
    :param threshold:   Score threshold to identify hard examples
    :param min_sample:  Minimum number of samples to select to compute the loss
    :param from_logits: If `True` indicates the loss is computed from logits
    :param name:  Loss name
    """
    super(OnlineHardExampleMining, self).__init__(name=name,
                                                  reduction=kl.Reduction.NONE)
    self.loss_fn = loss_fn
    self.loss_fn.reduction = kl.Reduction.NONE
    self.thresh = -_tf.math.log(threshold) if from_logits else threshold
    self.thresh = _tf.convert_to_tensor(self.thresh)
    self.min_sample = _tf.convert_to_tensor(min_sample)

  @_tf.function
  def call(self, y_true, y_pred):
    """
    Compute loss
    :param y_true:  True labels
    :param y_pred:  Predicted labels
    :return:  Loss, scalar
    """
    # Compute loss
    cost = self.loss_fn(y_true, y_pred)
    cost = _tf.reshape(cost, [-1])
    # Sort cost
    cost = _tf.sort(cost, direction='DESCENDING')
    idx = _tf.minimum(self.min_sample, cost.shape[0] - 1)
    if cost[idx] > self.thresh:
      cost = cost[cost > self.thresh]
    else:
      cost = cost[:idx]
    return _tf.reduce_mean(cost)
