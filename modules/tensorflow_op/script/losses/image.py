# coding=utf-8
"""
Image related keras loss
"""
import tensorflow as _tf
from tensorflow.keras.losses import Loss
from lts5.tensorflow_op.image.pyramid import gaussian_pyramid
from lts5.tensorflow_op.layers.image import GaussianPyramid
from lts5.tensorflow_op.probability import IsotropicGaussianPixelEvaluator
from lts5.tensorflow_op.probability import IsotropicLaplacianPixelEvaluator
from lts5.tensorflow_op.probability import HistogramPixelEvaluator
from lts5.tensorflow_op.layers.image import SpatialGradient
from lts5.tensorflow_op.network.vgg import VGG
from lts5.tensorflow_op.network.facenet import FaceNet

__author__ = 'Christophe Ecabert'


class PhotometricLogLikelihood(Loss):
  """ Photometric likelihood loss """

  def __init__(self,
               synth_mask,
               pskin,
               background_prior=None,
               weight=1.0,
               n_level=2,
               sigma=1.0,
               type='l2',
               normalized=True,
               name='photometric_likelihood',
               **kwargs):
    """
    Constructor
    :param synth_mask:  Synthetic image mask
    :param pskin:       Skin segmentation mask
    :param background_prior:  Background prior location (i.e. file)
    :param weight:      Loss weight
    :param n_level:     Number of levels in the image pyramid
    :param sigma:       Residual error standard deviation
    :param type:        Distribution type: 'l1' or 'l2'
    :param normalized:  If `True` normalize the distribution, otherwise biased
    :param name:        Op's name
    :param kwargs:  Extra keyword arguments
    """
    super(PhotometricLogLikelihood, self).__init__(name=name, **kwargs)
    self._sdev = sigma
    self._n_level = n_level
    self._type = type
    self._bg_prior_file = background_prior
    self._w_photo = weight
    self._synth_mask = synth_mask
    self._pskin = pskin
    self._normalized = normalized
    if self._type not in ('l1', 'l2'):
      raise ValueError('Unsupported type: {}! ({})'.format(self._type,
                                                           ('l1', 'l2')))
    # Foreground evaluator
    self._fg_eval = []
    for lvl in range(self._n_level):
      sdev = self._sdev / (2.0 ** lvl)
      if self._type == 'l2':
        self._fg_eval.append(IsotropicGaussianPixelEvaluator(sdev=sdev,
                                                             normalized=normalized))
      else:
        self._fg_eval.append(IsotropicLaplacianPixelEvaluator(sdev=sdev,
                                                              normalized=normalized))
    # Background evaluator
    self._bg_eval = None
    if self._bg_prior_file is not None:
      self._bg_eval = HistogramPixelEvaluator.from_file(self._bg_prior_file,
                                                        normalized)

  def call(self, y_true, y_pred):
    """
    Compute likelihood
    :param y_true:  Ground truth
    :param y_pred:  Predicted value
    :return:  Negative log-likelihood
    """

    y_pred = _tf.debugging.check_numerics(y_pred, 'Reconstruction is not valid')

    # Build three levels pyramid
    pyr = [gaussian_pyramid(images=y_pred,
                            max_level=self._n_level,
                            ksize=5,
                            sigma=1.6),
           gaussian_pyramid(images=self._synth_mask,
                            max_level=self._n_level,
                            ksize=5,
                            sigma=1.6),
           gaussian_pyramid(images=y_true,
                            max_level=self._n_level,
                            ksize=5,
                            sigma=1.6),
           gaussian_pyramid(images=self._pskin,
                            max_level=self._n_level,
                            ksize=5,
                            sigma=1.6)]
    l_photo = 0
    for lvl, element in enumerate(zip(*pyr)):
      im = element[0]
      msk = element[1]
      lbl = element[2]
      psk = element[3]
      # Build mask
      m = msk[..., 0] * psk[..., 0]
      m_elem = _tf.reduce_sum(m, axis=[1, 2]) + 1e-8
      # Foreground cost
      fg = self._fg_eval[lvl].log_value(first=im, second=lbl)
      l_fg = fg * m
      l_lvl = l_fg
      # Compute background cost
      if self._bg_eval is not None:
        bg = self._bg_eval.log_value(im)
        l_bg = bg * m
        l_lvl -= l_bg
      # Combine likelihood
      l_photo += (_tf.reduce_sum(l_lvl, axis=[1, 2])) / m_elem
    # neg_ll = -self._w_photo * _tf.reduce_mean(l_photo)
    neg_ll = -self._w_photo * l_photo
    return _tf.debugging.check_numerics(neg_ll, 'Photometric loss is not valid')


class PhotometricLogLikelihoodV2(Loss):
  """ Photometric likelihood loss """

  def __init__(self,
               background_prior,
               n_level,
               weight=1.0,
               sigma=1.0,
               type='l2',
               normalized=True,
               name='photometric_likelihood',
               **kwargs):
    """
    Constructor
    :param background_prior:  Background prior location (i.e. file)
    :param n_level:     Number of level in the gaussian pyramid
    :param weight:      Loss weight
    :param sigma:       Residual error standard deviation at level 0
    :param type:        Distribution type: 'l1' or 'l2'
    :param normalized:  If `True` normalize the distribution, otherwise biased
    :param name:        Op's name
    :param kwargs:  Extra keyword arguments
    """
    super(PhotometricLogLikelihoodV2, self).__init__(name=name, **kwargs)
    self._sdev = sigma
    self._n_level = n_level
    self._type = type
    self._bg_prior_file = background_prior
    self._w_photo = weight
    self._normalized = normalized
    if self._type not in ('l1', 'l2'):
      raise ValueError('Unsupported type: {}! ({})'.format(self._type,
                                                           ('l1', 'l2')))
    # Gaussian pyramid
    self._pyr_down = GaussianPyramid(self._n_level)
    # Create N Foreground evaluator
    fg_eval_ctor = (IsotropicGaussianPixelEvaluator if self._type == 'l2' else
                    IsotropicLaplacianPixelEvaluator)
    for level in range(self._n_level):
      sdev = self._sdev / (2.0 ** level)
      eval = fg_eval_ctor(sdev=sdev, normalized=normalized)
      setattr(self, '_fg_eval_{}'.format(level), eval)
    # Background evaluator
    self._bg_eval = None
    if self._bg_prior_file is not None:
      self._bg_eval = HistogramPixelEvaluator.from_file(self._bg_prior_file,
                                                        normalized)

  def call(self, y_true, y_pred):
    """
    Compute likelihood
    :param y_true:  Ground truth
    :param y_pred:  Predicted value
    :return:  Negative log-likelihood
    """

    # Go through gaussian pyramid
    pyr = [self._pyr_down(y_pred),
           self._pyr_down(y_true)]
    l_photo = 0.0
    for lvl, values in enumerate(zip(*pyr)):
      lvl_ypred = values[0]
      lvl_ytrue = values[1]
      # Binary mask stored in alpha channel
      lvl_mask = lvl_ypred[..., -1]
      lvl_ypred = lvl_ypred[..., :3]
      # Segmentation mask in the alpha channel
      lvl_pskin = lvl_ytrue[..., -1]
      lvl_ytrue = lvl_ytrue[..., :3]
      # Number of element
      mask = lvl_mask * lvl_pskin
      m_elem = _tf.reduce_sum(mask, axis=[1, 2]) + 1e-8
      # Foreground cost
      fg_eval = getattr(self, '_fg_eval_{}'.format(lvl))
      lvl = fg_eval.log_value(first=lvl_ypred, second=lvl_ytrue)
      if self._bg_eval is not None:
        # Compute background cost
        bg = self._bg_eval.log_value(lvl_ypred)
        lvl -= bg
      # Combine likelihood
      l_photo_ratio = lvl * mask
      l_photo += _tf.reduce_sum(l_photo_ratio, axis=[1, 2]) / m_elem
    # neg_ll = -self._w_photo * _tf.reduce_mean(l_photo)
    neg_ll = -self._w_photo * l_photo   # Batch
    return neg_ll


class PhotometricLoss(Loss):
  """ Multi-level Photometric L2,1 loss """

  def __init__(self,
               n_level,
               weight=1.0,
               name='photometric_loss',
               **kwargs):
    """
    Constructor
    :param n_level:     Number of level in the gaussian pyramid
    :param weight:      Loss weight
    :param name:        Op's name
    :param kwargs:  Extra keyword arguments
    """
    super(PhotometricLoss, self).__init__(name=name, **kwargs)
    # Gaussian pyramid
    self._n_level = n_level
    self._pyr_down = GaussianPyramid(self._n_level)
    # Losss weight
    self._w_photo = weight

  def call(self, y_true, y_pred):
    """
    Compute L2,1 error
    :param y_true:  Ground truth
    :param y_pred:  Predicted value
    :return:  Loss
    """
    # Go through gaussian pyramid
    pyr = [self._pyr_down(y_pred), self._pyr_down(y_true)]
    l_photo = 0.0
    for lvl, values in enumerate(zip(*pyr)):
      lvl_ypred = values[0]
      lvl_ytrue = values[1]
      # Binary mask stored in alpha channel
      lvl_mask = lvl_ypred[..., -1]
      lvl_ypred = lvl_ypred[..., :3]
      # Segmentation mask in the alpha channel
      lvl_pskin = lvl_ytrue[..., -1]
      lvl_ytrue = lvl_ytrue[..., :3]
      # Number of element
      mask = lvl_mask * lvl_pskin
      m_elem = _tf.reduce_sum(mask, axis=[1, 2]) + 1e-8

      # Compute l2 norm over channel
      lvl_diff = lvl_ytrue - lvl_ypred + 1e-7                 # [Batch, H, W, 3]
      l_lvl = _tf.norm(lvl_diff, ord=2, axis=-1)              # [Batch, H, W]
      l_lvl = l_lvl * mask
      l_photo += _tf.reduce_sum(l_lvl, axis=[1, 2]) / m_elem  # [Batch,]
    return self._w_photo * l_photo


class ImageGradientLikelihood(Loss):
  """
  Compute negative log likelihood of spatial derivative in Y/X direction between
   two images
  """

  def __init__(self,
               n_level,
               sigma=1.0,
               weight=1.0,
               type='l2',
               normalized=True,
               name='image_gradient_likelihood',
               **kwargs):
    """
    Constructor
    :param n_level: Number of level in the gaussian pyramid
    :param weight:  Loss weight
    :param sigma:   Residual standard deviation
    :param type:    Distribution type: 'l1' or 'l2'
    :param normalized:  If `True` normalize the distribution, otherwise biased
    :param name:    Loss's name (i.e. scope)
    :param kwargs:  Extra keyword arguments
    """
    super(ImageGradientLikelihood, self).__init__(name=name,
                                                  **kwargs)
    # Define parameters
    self._sdev = sigma
    self._n_level = n_level
    self._w_grad = weight
    self._type = type
    if self._type not in ('l1', 'l2'):
      raise ValueError('Unsupported type: {}! ({})'.format(self._type,
                                                           ('l1', 'l2')))
    # Gaussian pyramid
    self._pyr_down = GaussianPyramid(self._n_level)
    # Create N evaluator
    eval_ctor = (IsotropicGaussianPixelEvaluator if self._type == 'l2' else
                 IsotropicLaplacianPixelEvaluator)
    for level in range(self._n_level):
      sdev = self._sdev / (2.0 ** level)
      eval = eval_ctor(sdev=sdev, normalized=normalized)
      setattr(self, '_grad_eval_{}'.format(level), eval)
    # Compute spatial gradient
    self._spatial_grad = SpatialGradient()

  def call(self, y_true, y_pred):
    """
    Compute likelihood
    https://arxiv.org/pdf/1511.05440.pdf
    :param y_true:  Ground truth
    :param y_pred:  Predicted value
    :return:  Negative log-likelihood
    """

    pyr = [self._pyr_down(y_pred), self._pyr_down(y_true)]
    l_grad = 0.0

    for lvl, values in enumerate(zip(*pyr)):
      # Extract data
      lvl_ypred = values[0][..., :3]
      lvl_ytrue = values[1][..., :3]

      # Compute spatial gradient for both images
      g_true = _tf.abs(self._spatial_grad(lvl_ytrue))
      g_pred = _tf.abs(self._spatial_grad(lvl_ypred))
      # evaluate likelihood for both component
      gy_true = g_true[..., 0]
      gx_true = g_true[..., 1]
      gy_pred = g_pred[..., 0]
      gx_pred = g_pred[..., 1]
      # Evaluator
      grad_eval = getattr(self, '_grad_eval_{}'.format(lvl))
      l_gx = grad_eval.log_value(first=gx_pred,
                                 second=gx_true)  # [Batch, H, W]
      l_gy = grad_eval.log_value(first=gy_pred,
                                 second=gy_true)
      # Aggregate all likelihood
      l_gx = _tf.reduce_mean(l_gx, axis=[1, 2])          # [Batch, ]
      l_gy = _tf.reduce_mean(l_gy, axis=[1, 2])
      # Add contribution of both
      l_grad += (l_gx + l_gy)
    neg_ll = -self._w_grad * l_grad                     # [Batch, ]
    return neg_ll


class ImageGradientDifferenceLoss(Loss):
  """
  Compute negative log likelihood of spatial derivative in Y/X direction between
   two images

  See: https://arxiv.org/pdf/1511.05440.pdf
  """

  def __init__(self,
               alpha,
               weight=1.0,
               name='image_gradient_difference_loss',
               **kwargs):
    """
    Constructor
    :param alpha:   integer, Norm exponent value. Must be >= 1.
    :param weight:  Loss weight
    :param name:    Loss's name (i.e. scope)
    :param kwargs:  Extra keyword arguments
    """
    super(ImageGradientDifferenceLoss, self).__init__(name=name, **kwargs)

    self._w_grad = weight
    if not isinstance(alpha, int):
      raise ValueError('Power exponent `alpha` must be an integer')
    if alpha < 1:
      raise ValueError('Power exponent `alpha` must be >= 1')
    self._alpha = alpha
    # Compute spatial gradient
    self._spatial_grad = SpatialGradient()

  def call(self, y_true, y_pred):
    """
    Compute likelihood
    https://arxiv.org/pdf/1511.05440.pdf, page4
    :param y_true:  Ground truth
    :param y_pred:  Predicted value
    :return:  Loss
    """
    # Extract masks
    ytrue = y_true[..., :3]
    pskin = y_true[..., 3]
    ypred = y_pred[..., :3]
    mask = y_pred[..., 3]

    # Define mask
    m = pskin * mask
    m_elem = _tf.reduce_sum(mask, axis=[1, 2]) + 1e-8

    # Compute spatial gradient for both images
    g_true = _tf.abs(self._spatial_grad(ytrue))
    g_pred = _tf.abs(self._spatial_grad(ypred))
    # evaluate likelihood for both component
    gy_true = g_true[..., 0]
    gx_true = g_true[..., 1]
    gy_pred = g_pred[..., 0]
    gx_pred = g_pred[..., 1]
    gx_diff = _tf.norm(gx_true - gx_pred + 1e-7, self._alpha, axis=-1)
    gy_diff = _tf.norm(gy_true - gy_pred + 1e-7, self._alpha, axis=-1)
    g_diff = (gx_diff + gy_diff) * m
    l_gdl = _tf.reduce_sum(g_diff, axis=[1, 2]) / m_elem
    return self._w_grad * l_gdl


_name_to_network = {'vgg16': VGG.VGG16,
                    'vgg19': VGG.VGG19,
                    'vggface': VGG.VGGFace,
                    'facenet': FaceNet}


def _dist_fn(ytrue, ypred, weight):
  return _tf.reduce_mean((ytrue - ypred) ** 2.0, axis=[1, 2, 3]) * weight


class PerceptualLoss(_tf.keras.losses.Loss):
  """ Compute distance between network feature maps """

  def __init__(self,
               input_dims,
               weight,
               path,
               net_type='vgg16',
               feature_maps=('Predictor/relu7',),
               dist_fn=None,
               y_true_fn=None,
               y_pred_fn=None,
               name='net_feature_distance',
               **kwargs):
    """
    Constructor
    :param input_dims: Tuple, input dimensions without batch axis
    :param weight:    float, list. Loss contribution to the overall cost. Can
                      have dedicated weight for separated activation map. Must
                      have the same length as `feature_maps`.
    :param path:      Path to the VGG model to use
    :param net_type:  Type of vgg network. Valid values are 'vgg16', 'vgg19',
                      'vggface'
    :param feature_maps:  List of feature maps to compute distance for
    :param dist_fn:   Distance fn, dist_fn(ytrue, ypred, weight) and return
                      error per samples. By default use mean square error (L2)
    :param y_true_fn: Callable function to call on `y_true` before loss
    :param y_pred_fn: Callable function to call on `y_pred` before loss
    :param name:      Loss name
    :param kwargs:    Extra keyword arguments
    """
    super(PerceptualLoss, self).__init__(name=name, **kwargs)

    # Network type
    self._net_type = net_type.lower()
    if self._net_type not in _name_to_network.keys():
      raise RuntimeError('Unknown network type: {}, valid types are: {}'
                         .format(self._net_type,
                                 list(_name_to_network.keys())))
    # Parameters
    self._input_dims = input_dims
    self._path = path
    self._feat_maps = feature_maps
    # Distance function to use for similarity measurement
    self._dist_fn = dist_fn if dist_fn is not None else _dist_fn
    # Function to apply for each inputs
    self._y_true_fn = y_true_fn or _tf.identity
    self._y_pred_fn = y_pred_fn or _tf.identity
    # Distance weight
    self._w_feat = None
    if isinstance(weight, float):
      self._w_feat = [weight] * len(feature_maps)
    else:
      if len(weight) != len(feature_maps):
        raise ValueError('`weight` must have the same length as `feature_maps`')
      self._w_feat = weight
    # Create network model
    self._model, self._preproc_fn = self._initialize_model()

  def _initialize_model(self):
    """
    Create keras model with proper input / outputs
    :return:  Model
    """
    ctor_fn = _name_to_network.get(self._net_type)
    # Define inputs + create network
    if 'vgg' in self._net_type:
      input_image = _tf.keras.layers.Input(shape=self._input_dims)
      with_top = any([('Predictor' in v) for v in self._feat_maps])
      base_model = ctor_fn(input_tensor=input_image, include_top=with_top)
    else:
      # FaceNet -> Fixed input
      input_image = _tf.keras.layers.Input(shape=(160, 160, 3))
      base_model = ctor_fn(input_tensor=input_image)
    # Seek feature maps
    outputs = []
    for fmap in self._feat_maps:
      try:
        outputs.append(base_model.get_layer(fmap).output)
      except ValueError as e:
        msg = '{}\nPossible candidates ' \
              'are: {}'.format(e.args, [l.name for l in base_model.layers])
        raise ValueError(msg)
    # Combined model
    model = _tf.keras.Model(input_image, outputs, trainable=False)
    return model, base_model.inputs_config.preprocessing_fn

  def call(self, y_true, y_pred):
    """
    Compute
    :param y_true:  Real image, RGB formatted
    :param y_pred:  Predicted image, RGB formatted
    :return:  Cost
    """

    # Pre-processing
    yt = self._preproc_fn(self._y_true_fn(y_true))
    yp = self._preproc_fn(self._y_pred_fn(y_pred))
    # Extract features
    feat_true = self._model(yt, training=False)
    feat_pred = self._model(yp, training=False)
    if not isinstance(feat_true, list):
      feat_true = [feat_true]
      feat_pred = [feat_pred]
    # Compute cost
    bsize = _tf.shape(y_pred)[0]
    cost = _tf.zeros(shape=(bsize,), dtype=y_pred.dtype)
    for w, f_true, f_pred in zip(self._w_feat, feat_true, feat_pred):
      cost += self._dist_fn(f_true, f_pred, w)
    return cost
