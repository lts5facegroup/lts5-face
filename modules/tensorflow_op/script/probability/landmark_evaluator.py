# coding=utf-8
"""
Collection of various landmarks distributions evaluator

See:
  https://github.com/unibas-gravis/scalismo-faces
"""
from math import pi
import tensorflow as _tf
from lts5.tensorflow_op.utils.shape import check_static
from lts5.tensorflow_op.probability.evaluator import PairEvaluator

__author__ = 'Christophe Ecabert'


class IsotropicGaussianLandmarkEvaluator(PairEvaluator):
  """ Isotropic gaussian likelihood for landmarks """

  def __init__(self, sdev:float , ndim:int = 2, normalized=True):
    """
    Constructor
    :param sdev:  Residual's standard deviation
    :param ndim:  Landmarks dimension, usually 2 for image
    :param normalized: If `True` normalize the distribution, otherwise biased
    """
    self._ndim = ndim
    self._sdev = _tf.convert_to_tensor(sdev, _tf.float32)
    self._norm = 0.0
    if normalized:
      self._norm = -(ndim * 0.5) * (_tf.math.log(2.0 * pi) + _tf.math.log(sdev))
    self._norm = _tf.convert_to_tensor(self._norm, dtype=_tf.float32)

  def log_value(self, first, second, weights=None):
    """
    Log likelihood of the residual for given batch of landmarks
    :param first:   First term    [Batch, N, Ndim]
    :param second:  Second term   [Batch, N, Ndim]
    :param weights: Optional weighting matrix [N,]
    :return:  Log-likelihood  [Batch,]
    """
    with _tf.name_scope(self.__class__.__name__) as scope:
      # rank==3 && t.shape[-1] == self._ndim
      check_static(first,
                   has_rank=3,
                   has_dim_equals=(-1, self._ndim),
                   tensor_name='first')
      check_static(second,
                   has_rank=3,
                   has_dim_equals=(-1, self._ndim),
                   tensor_name='second')
      d = _tf.reduce_sum((first - second) ** 2.0, axis=-1)  # [Batch, N]
      l_lms = self._norm - ((0.5 * d) / (self._sdev * self._sdev))
      if weights is not None:
        check_static(weights,
                     has_rank=1,
                     tensor_name='weights')
        l_lms = weights * l_lms
      return l_lms  # [Batch, N]


class IsotropicLaplacianLandmarkEvaluator(PairEvaluator):
  """ Isotropic Laplacian likelihood for landmarks """

  def __init__(self, sdev:float , ndim:int = 2, normalized=True):
    """
    Constructor
    :param sdev:  Residual's standard deviation
    :param ndim:  Landmarks dimension, usually 2 for image
    :param normalized: If `True` normalize the distribution, otherwise biased
    """
    self._ndim = ndim
    self._sdev = _tf.convert_to_tensor(sdev, dtype=_tf.float32)
    self._norm = 0.0
    if normalized:
      self._norm = _tf.math.log(self._sdev) - _tf.math.log(2.0)

  def log_value(self, first, second, weights=None):
    """
    Log likelihood of the residual for given batch of landmarks
    :param first:   First term    [Batch, N, Ndim]
    :param second:  Second term   [Batch, N, Ndim]
    :param weights: Optional weighting matrix [N,]
    :return:  Log-likelihood  [Batch,]
    """
    with _tf.name_scope(self.__class__.__name__) as scope:
      # rank==3 && t.shape[-1] == self._ndim
      check_static(first,
                   has_rank=3,
                   has_dim_equals=(-1, self._ndim),
                   tensor_name='first')
      check_static(second,
                   has_rank=3,
                   has_dim_equals=(-1, self._ndim),
                   tensor_name='second')

      # |mu - x| / b
      l1_nrm = _tf.reduce_sum(_tf.abs(first - second), axis=-1)  # [Batch, N]
      l_lms = self._norm - (l1_nrm / self._sdev)
      if weights is not None:
        check_static(weights,
                     has_rank=1,
                     tensor_name='weights')
        l_lms = weights * l_lms
      return l_lms  # [Batch, N]
