# coding=utf-8
"""
Collection of various pixel color distributions

See:
  https://github.com/unibas-gravis/scalismo-faces
"""
from math import pi
from os.path import splitext
import numpy as np
import tensorflow as _tf
from imageio import imread
from lts5.utils import search_folder
from lts5.tensorflow_op.probability.evaluator import DistributionEvaluator
from lts5.tensorflow_op.probability.evaluator import PairEvaluator
__author__ = 'Christophe Ecabert'

_EPSILON = 1e-8


class ConstantPixelEvaluator(DistributionEvaluator):
  """ Constant value of evaluation, useful as a background likelihood """

  def __init__(self, value):
    """
    Constructor
    :param value: Constant value of the evaluator
    """
    self._value = _tf.convert_to_tensor(value, dtype=_tf.float32)

  def log_value(self, sample):
    with _tf.name_scope(self.__class__.__name__) as scope:
      return self._value


class IsotropicGaussianPixelEvaluator(PairEvaluator):
  """ Gaussian color model, isotropic, 3D (RGB) """

  def __init__(self, sdev, normalized=True):
    """
    Constructor
    :param sdev:  Standard deviation
    :param normalized:  If `True` normalize the distribution, otherwise biased
    """
    self._sdev = _tf.convert_to_tensor(sdev, dtype=_tf.float32)
    self._norm = 0.0
    if normalized:
      self._norm = -(3.0 / 2.0) * (_tf.math.log(2.0 * pi) + _tf.math.log(sdev))
    self._norm = _tf.convert_to_tensor(self._norm, dtype=_tf.float32)

  def log_value(self, first, second, weights=None):
    with _tf.name_scope(self.__class__.__name__) as scope:
      # d2a = _tf.norm(first - second + _EPSILON, ord=2, axis=-1)
      d2b = _tf.reduce_sum((first - second) ** 2.0, axis=-1)
      value = self._norm - (0.5 * d2b / (self._sdev * self._sdev))
      return _tf.debugging.check_numerics(value,
                                          '{} is not valid'.format(scope))


class TruncatedGaussianPixelEvaluator(PairEvaluator):
  """
  Truncated Gaussian color distribution to respect value range of [0, 1] per
  channel
  """

  def __init__(self, sdev):
    """
    Constructor
    :param sdev:  Standard deviation
    """
    self._sdev = _tf.convert_to_tensor(sdev, dtype=_tf.float32)
    self._var = self._sdev * self._sdev
    self._norm = 3.0 * (0.5 * _tf.math.log(2.0 * pi) + 0.5 * _tf.math.log(self._var))

  def log_value(self, first, second, weights=None):
    with _tf.name_scope(self.__class__.__name__) as scope:
      def _fnorm(chan):
        a = _tf.erf((1.0 - chan) / (_tf.sqrt(2.0) * self._sdev))
        b = _tf.erf(-chan / (_tf.sqrt(2.0) * self._sdev))
        return _tf.reduce_sum(_tf.math.log(a - b), axis=-1)

      d2 = _tf.norm(first - second + _EPSILON, ord=2, axis=-1)
      fnorm = 3.0 * _tf.math.log(0.5) + _fnorm(first)
      value = (-d2 / (2.0 * self._var)) - self._norm - fnorm
      return _tf.debugging.check_numerics(value,
                                          '{} is not valid'.format(scope))


class HistogramPixelEvaluator(DistributionEvaluator):
  """ Probability density function for Color histogram distribution """

  def __init__(self, histogram, bins_per_channel, normalized=True):
    """
    Constructor
    :param histogram: Normalized histogram (float)
    :param bins_per_channel:  Number of bin per channel
    :param normalized:  If `True` normalize the distribution, otherwise biased
    """
    self._hist = _tf.convert_to_tensor(histogram, dtype=_tf.float32)
    self._bin_per_channel = _tf.convert_to_tensor(bins_per_channel,
                                                  dtype=_tf.int32)
    # Convert to log likelihood
    # log(b) = log(h) + 3*log(n_bins)
    self._log_hist = _tf.math.log(self._hist)
    if normalized:
      self._log_hist += (3.0 * _tf.math.log(_tf.cast(bins_per_channel,
                                                     dtype=_tf.float32)))

  @classmethod
  def from_file(cls, filename, normalized=True):
    """
    Construct distribution from file
    :param filename:  Path the the model
    :param normalized:  If `True` normalize the distribution, otherwise biased
    :return: HistogramPixelEvaluator object initialized
    """
    # Load numpy array
    d = np.load(filename, allow_pickle=True).item()
    # Create hist
    return cls(histogram=d['hist'],
               bins_per_channel=d['bins'],
               normalized=normalized)

  @classmethod
  def from_image(cls, image, bins_per_channel=25):
    """
    Construct distribution from an image
    :param image: Image (ndarray, normalized [0, 1])
    :param bins_per_channel:  Number of bins per color channel
    :return:  Construct distribution from file
    """

    def _rgb_to_idx(img, n_bins):
      bidx = np.fmin(np.round((img * n_bins) - 0.5),
                     n_bins - 1).astype(np.int32)
      bidx[..., 0] *= (n_bins ** 2)
      bidx[..., 1] *= n_bins
      bidx = np.sum(bidx, axis=-1)
      return bidx.flatten()

    hist = np.zeros(bins_per_channel ** 3, dtype=np.float32)
    # Convert rgb triplets into indexes
    idx = _rgb_to_idx(img=image, n_bins=bins_per_channel)
    uidx, count = np.unique(idx, return_counts=True)
    hist[uidx] += count
    # Add small value to avoid -Inf when np.log is computed
    hist += np.finfo(np.float64).eps
    # Normalize
    hist /= np.sum(hist)
    # Create object
    return cls(histogram=hist, bins_per_channel=bins_per_channel)

  @staticmethod
  def build_from_data(folder, bins_per_channel=25, ext=None, ofile=None):
    """
    Build histogram from images stored into a given folder. Select only the one
    with proper extension (default: jpg)
    :param folder:  Location where images are stored
    :param bins_per_channel:  Number of bins per color channel
    :param ext:   List of extensions to pick, default .jpg
    :param ofile: Output filename, used to save the histogram build. If ofile is
                  None, no data will be saved
    :return:  Build histogram (histogram, bin_per_channel)
    """

    def _rgb_to_idx(img, n_bins):
      bidx = np.fmin(np.round((img * n_bins) - 0.5),
                     n_bins - 1).astype(np.int32)
      bidx[..., 0] *= (n_bins ** 2)
      bidx[..., 1] *= n_bins
      bidx = np.sum(bidx, axis=-1)
      return bidx.flatten()

    def _check_ext(filename):
      path, file_ext = splitext(filename)
      if file_ext != '.npy':
        return path + '.npy'
      return filename

    # Define extension list
    ext = ['.jpg'] if ext is None else ext
    # Scan for images
    img_path = search_folder(folder=folder, ext=ext, relative=False)
    hist = np.zeros(bins_per_channel ** 3, dtype=np.float32)
    for im_path in img_path:
      # Load
      im = imread(im_path).astype(np.float32) / 255.0
      # Convert rgb triplets into indexes
      idx = _rgb_to_idx(img=im, n_bins=bins_per_channel)
      uidx, count = np.unique(idx, return_counts=True)
      hist[uidx] += count
    # Add small value to avoid -Inf when np.log is computed
    hist += np.finfo(np.float64).eps
    # Normalize
    hist /= np.sum(hist)
    # save it
    if ofile is not None:
      d = {'hist': hist,
           'bins': bins_per_channel}
      np.save(_check_ext(ofile), d)
    return hist, bins_per_channel

  def log_value(self, sample):
    with _tf.name_scope(self.__class__.__name__) as scope:
      def _rgb_to_idx(value, nbins):
        nb = _tf.cast(nbins, dtype=_tf.float32)
        v = _tf.reshape(value, [-1, 3])
        bidx = _tf.round((v * nb) - 0.5)
        bidx = _tf.math.minimum(bidx, nb - 1.0)
        bidx = _tf.cast(bidx, _tf.int32)
        # Add each contribution
        idx_r = bidx[:, 0] * (nbins * nbins)
        idx_g = bidx[:, 1] * nbins
        idx_b = bidx[:, 2]
        bidx = _tf.stack([idx_r, idx_g, idx_b], axis=-1)
        bidx = _tf.reduce_sum(bidx, axis=-1)
        return _tf.reshape(bidx, [-1])

      # Get histogram index
      idx = _rgb_to_idx(value=sample, nbins=self._bin_per_channel)
      # Get log probability
      logp = _tf.gather(self._log_hist, idx)
      # Reshape
      shp = _tf.shape(sample)
      value = _tf.reshape(logp, shp[:-1])
      return _tf.debugging.check_numerics(value,
                                          '{} is not valid'.format(scope))


class IsotropicLaplacianPixelEvaluator(PairEvaluator):
  """
  Laplacian color model, isotropic, 3D (RGB)

  L(mu, b) = (1 / 2b) * exp( -|mu - x| / b)

  """

  def __init__(self, sdev, normalized=True):
    """
    Constructor
    :param sdev:  Standard deviation
    :param normalized:  If `True` normalize the distribution, otherwise biased
    """
    self._sdev = _tf.convert_to_tensor(sdev, dtype=_tf.float32)
    self._norm = 0.0
    if normalized:
      self._norm = _tf.math.log(self._sdev) - _tf.math.log(2.0)
    self._norm = _tf.convert_to_tensor(self._norm, dtype=_tf.float32)

  def log_value(self, first, second, weights=None):
    with _tf.name_scope(self.__class__.__name__) as scope:
      # |mu - x| / b
      l1_nrm = _tf.reduce_sum(_tf.abs(first - second), axis=-1)
      value = self._norm - (l1_nrm / self._sdev)
      return _tf.debugging.check_numerics(value,
                                          '{} is not valid'.format(scope))
