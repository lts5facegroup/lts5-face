# coding=utf-8
"""
Color histogram seen as a probabilistic function
"""
from os.path import splitext
import numpy as np
import tensorflow as tf
from tensorflow.python.framework import ops
from tensorflow.python.framework import constant_op
from tensorflow.python.framework import tensor_shape
from tensorflow.python.framework.ops import control_dependencies as ctrl_deps
from tensorflow.python.ops import check_ops
import tensorflow_probability as tfp
from tensorflow_probability.python.distributions import NOT_REPARAMETERIZED
from lts5.utils import search_folder
from imageio import imread
__author__ = 'Christophe Ecabert'


class RGBHistogram(tfp.distributions.Distribution):
  """
  Probability density function for Color histogram distribution
  """

  def __init__(self,
               histogram,
               bin_per_channel,
               validate_args=False,
               allow_nan_stats=True,
               name="RGBHistrogram"):
    """
    Construct a color distribition based on a given normalised histogram.

    :param histogram:  Normalized histogram (float)
    :param bin_per_channel: Number of bin per channel
    :param validate_args: Python `bool`, default `False`. When `True`
        distribution parameters are checked for validity despite possibly
        degrading runtime performance. When `False` invalid inputs may silently
        render incorrect outputs.
    :param allow_nan_stats: Python `bool`, default `True`. When `True`,
        statistics (e.g., mean, mode, variance) use the value "`NaN`" to
        indicate the result is undefined. When `False`, an exception is raised
        if one or more of the statistic's batch members are undefined.
    :param name: Python `str` name prefixed to Ops created by this class.
    """
    # Init distribution parameters
    with ops.name_scope(name, values=[histogram, bin_per_channel]) as name:
      msg = 'Histogram dimensions and number of bins do not match. \
      len(h) == n_bins ** 3'
      with ctrl_deps([check_ops.assert_equal(histogram.shape[0],
                                             bin_per_channel ** 3,
                                             message=msg)] if
                     validate_args else []):
        self._hist = tf.convert_to_tensor(histogram, dtype=tf.float32)
        self._bin_per_channel = tf.convert_to_tensor(bin_per_channel,
                                                     dtype=tf.int32)
    # Init distribution
    super(RGBHistogram, self).__init__(
        dtype=self._hist.dtype,
        reparameterization_type=NOT_REPARAMETERIZED,
        validate_args=validate_args,
        allow_nan_stats=allow_nan_stats,
        parameters=dict(locals()),
        graph_parents=[self._hist, self._bin_per_channel],
        name=name)

  @classmethod
  def from_file(cls,
                filename,
                validate_args=False,
                allow_nan_stats=True,
                name='RGBHistogram'):
    """
    Construct a RGBHistogram from a given file
    :param filename:  Path to the histogram model
    :param validate_args: Python `bool`, default `False`. When `True`
        distribution parameters are checked for validity despite possibly
        degrading runtime performance. When `False` invalid inputs may silently
        render incorrect outputs.
    :param allow_nan_stats: allow_nan_stats: Python `bool`, default `True`.
        When `True`, statistics (e.g., mean, mode, variance) use the value
        "`NaN`" to indicate the result is undefined. When `False`, an exception
        is raised if one or more of the statistic's batch members are undefined.
    :param name: name: Python `str` name prefixed to Ops created by this class.
    :return:  RGBHistogram instance
    """
    # Check extensions


    # Load numpy array
    d = np.load(filename).item()
    # Create hist
    return cls(histogram=d['hist'],
               bin_per_channel=d['bins'],
               validate_args=validate_args,
               allow_nan_stats=allow_nan_stats,
               name=name)

  @staticmethod
  def build_from_data(folder,
                      bins_per_channel=25,
                      ext=None,
                      ofile=None):
    """
    Build histogram from images stored into a given folder. Select only the one
    with proper extension (default: jpg)
    :param folder:  Location where images are stored
    :param bins_per_channel:  Number of bins per color channel
    :param ext:   List of extensions to pick, default .jpg
    :param ofile: Output filename, used to save the histogram build. If ofile is
                  None, no data will be saved
    :return:  Build histogram (histogram, bin_per_channel)
    """

    def _rgb_to_idx(img, n_bins):
      bidx = np.fmin(np.round((img * n_bins) - 0.5), n_bins - 1).astype(np.int32)
      bidx[:, :, 0] *= (n_bins ** 2)
      bidx[:, :, 1] *= n_bins
      bidx = np.sum(bidx, axis=-1)
      return bidx.flatten()

    def _check_ext(filename):
      path, file_ext = splitext(filename)
      if file_ext != '.npy':
        return path + '.npy'
      return filename

    # Define extension list
    ext = ['.jpg'] if ext is None else ext
    # Scan for images
    img_path = search_folder(folder=folder, ext=ext, relative=False)
    histo = np.zeros(bins_per_channel ** 3, dtype=np.float32)
    for im_path in img_path:
      # Load
      im = imread(im_path).astype(np.float32) / 255.0
      # Convert rgb tripets into indexes
      idx = _rgb_to_idx(img=im, n_bins=bins_per_channel)
      uidx, count = np.unique(idx, return_counts=True)
      histo[uidx] += count
    # Add small value to avoid -Inf when np.log is computed
    histo += np.finfo(np.float64).eps
    # Normalize
    histo /= np.sum(histo)
    # Convert to log likelihood
    # log(b) = log(h) + 3*log(n_bins)
    # histo = np.log(histo) + (3.0 * np.log(bins_per_channel))
    # save it
    if ofile is not None:
      d = {'hist': histo,
           'bins': bins_per_channel}
      np.save(_check_ext(ofile), d)
    return histo, bins_per_channel

  @property
  def hist(self):
    """ Distribution's histogram """
    return self._hist

  @property
  def bins_per_channel(self):
    """ Distribution's bins per channel """
    return self._bin_per_channel

  def _event_shape(self):
    # Shape of a single sample from a single batch as a `TensorShape`
    return tensor_shape.vector(3)

  def _event_shape_tensor(self):
    # Shape of a single sample from a single batch as a `TensorShape`
    return constant_op.constant(3, dtype=tf.int32)

  def _batch_shape(self):
    # Shape of a single sample from a single event index as a `TensorShape`
    return tensor_shape.scalar()

  def _batch_shape_tensor(self):
    # Shape of a single sample from a single event index as a `TensorShape`
    return constant_op.constant([], dtype=tf.int32)

  def _prob(self, value):
    """
    Gives the probability to have a given pixel value
    :param value: Batch of RGB tuple to get the probability of
    :return:  Probabilities
    """
    return tf.exp(self._log_prob(value))

  def _log_prob(self, value):
    """
    Give the log probability of a given pixel value
    :param value:  Batch of RGB tuple to get the log probability of
    :return:  Log probabilites
    """
    # Get histogram index
    idx = self._rgb_to_idx(value=value)
    # Get log probability
    logp = tf.gather(self._hist, idx)
    # Reshape
    return tf.reshape(logp, value.get_shape()[:-1])

  def _rgb_to_idx(self, value):
    """
    Convert RGB tuples into their corresponding histogram index
    :param value: RGB tuples, images
    :return:  Indexes
    """
    nbin = tf.to_float(self._bin_per_channel)
    v = tf.reshape(value, [-1, 3])
    idx = tf.round(v * nbin) - 0.5
    idx = tf.math.minimum(idx, nbin - 1.0)
    idx = tf.cast(idx, tf.int32)
    # Add each contribution
    idx_r = idx[:, 0] * (self._bin_per_channel * self._bin_per_channel)
    idx_g = idx[:, 1] * self._bin_per_channel
    idx_b = idx[:, 2]
    idx = tf.stack([idx_r, idx_g, idx_b], axis=1)
    idx = tf.reduce_sum(idx, axis=1)
    return tf.reshape(idx, [-1])
