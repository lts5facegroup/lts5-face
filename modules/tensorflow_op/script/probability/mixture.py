# coding=utf-8
"""
Gaussian Mixture model based on tensorflow probability and:
- https://www.tensorflow.org/probability/examples/Fitting_DPMM_Using_pSGLD
"""
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.keras.optimizers.schedules import PolynomialDecay
from lts5.tensorflow_op.optimizers import StochasticGradientLangevinDynamics as\
  SGLD

__author__ = 'Christophe Ecabert'
tfd = tfp.distributions


class GaussianMixtureModel(tf.keras.Model):
  """ Gaussian mixture model """

  def __init__(self,
               n_components,
               feature_dims,
               mu_range=(-1.0, 1.0),
               name='GaussianMixtureModel',
               **kwargs):
    """
    Construtor
    :param n_components:  Number of gaussian in the mixture
    :param feature_dims:  Size of feature space
    :param mu_range:  Gaussian mean value range, format: (min, max)
    :param name:  Model's name
    :param kwargs: Extra keyword arguments
    """
    super(GaussianMixtureModel, self).__init__(name=name, **kwargs)

    # Number of component in the mixture
    # k in 1, ..., K
    self.n_components = n_components
    self.feat_dims = feature_dims

    # Unconstrained mixing probability
    mix_init = tf.ones([self.n_components], self.dtype) / self.n_components
    self._mix_probs_uncst = self.add_weight(shape=(self.n_components,),
                                           initializer=lambda x, dtype: mix_init,
                                           name='mix_probs_uncst')
    # Gaussian means
    loc_init = tf.random.uniform(minval=mu_range[0],
                                 maxval=mu_range[1],
                                 shape=[self.n_components, self.feat_dims],
                                 dtype=self.dtype)
    self._locs = self.add_weight(shape=(self.n_components, self.feat_dims),
                                initializer=lambda x, dtype: loc_init,
                                name='locs')
    # Gaussian std
    scale_init = tf.ones([self.n_components, self.feat_dims],
                         dtype=self.dtype)
    self._scales_uncst = self.add_weight(shape=(self.n_components,
                                               self.feat_dims),
                                        initializer=lambda x, dtype: scale_init,
                                        name='scales_uncst')
    # Alpha
    alpha_init = tf.ones([1], dtype=self.dtype)
    self.alpha_unsct = self.add_weight(shape=(1,),
                                       initializer=lambda x, dtype: alpha_init,
                                       name='alpha_uncst')

    # Prior distributions of the training variables
    # Mean
    loc_dist = tfd.Normal(loc=tf.zeros((self.n_components, self.feat_dims),
                                       dtype=self.dtype),
                          scale=tf.ones((self.n_components, self.feat_dims),
                                        dtype=self.dtype))
    self._rv_locs = tfd.Independent(loc_dist,
                                    reinterpreted_batch_ndims=1,
                                    name='rv_loc')
    # Standard deviation
    scales_dist = tfd.InverseGamma(concentration=tf.ones((self.n_components,
                                                          self.feat_dims),
                                                         dtype=self.dtype),
                                   scale=tf.ones((self.n_components,
                                                  self.feat_dims),
                                                 dtype=self.dtype))
    self._rv_scales = tfd.Independent(scales_dist,
                                      reinterpreted_batch_ndims=1,
                                      name='rv_scales')

    self._rv_alpha = tfd.InverseGamma(concentration=tf.ones([1],
                                                            dtype=self.dtype),
                                     scale=tf.ones([1], self.dtype),
                                     name='rv_alpha')

  @property
  def mix_probs(self):
    """
    Mixture probability of each gaussian distribution
    :return: Tensor
    """
    return tf.math.softmax(self._mix_probs_uncst)

  @property
  def means(self):
    """
    Mean value of each gaussian distribution
    :return:  Tensor
    """
    return self._locs

  @property
  def scales(self):
    """
    Scale value of each gaussian distribution
    :return:  Tensor
    """
    return tf.math.softplus(self._scales_uncst)

  @property
  def alpha(self):
    """
    Alpha constrained
    :return:  Tensor
    """
    return tf.math.softplus(self.alpha_unsct)

  def call(self, x):
    """
    Forward
    :param x: Tenor, #Observation x #Features, samples
    :return:  Tuple: `x` likelihood, prior likelihood
    """
    # Setup prior
    mix_con = (tf.ones(self.n_components, self.dtype) * self.alpha) / \
              self.n_components
    rv_mix = tfd.Dirichlet(concentration=mix_con, name='rv_mix')
    # Setup mixture
    density = tfd.MixtureSameFamily(
      mixture_distribution=tfd.Categorical(probs=self.mix_probs),
      components_distribution=tfd.MultivariateNormalDiag(self.means,
                                                         self.scales))
    # Compute the mean log likelihood
    data_ll = density.log_prob(x)
    # Prior likelihood
    prior_ll = tf.concat([self._rv_locs.log_prob(self.means),
                          self._rv_scales.log_prob(self.scales),
                          self._rv_alpha.log_prob(self.alpha),
                          rv_mix.log_prob(self.mix_probs)[..., tf.newaxis]
                          ], axis=-1)
    prior_ll = tf.reduce_sum(prior_ll, axis=-1)
    # Done
    return data_ll, prior_ll

  def unnormalized_posterior(self, x):
    """
    Un-normalized posterior
    :param x: Observation
    :return:  Posterior log-likelihood
    """
    x = tf.expand_dims(x, axis=1)
    un_posterior_dist = tfd.MultivariateNormalDiag(loc=self.means,
                                                   scale_diag=self.scales)
    return un_posterior_dist.log_prob(x) + \
           tf.math.log(self.mix_probs[tf.newaxis, ...])

  def posterior(self, x):
    un_posterior = self.unnormalized_posterior(x)
    return un_posterior - tf.reduce_logsumexp(un_posterior,
                                              axis=-1)[..., tf.newaxis]

  @staticmethod
  def train(data,
            n_components,
            batch_size,
            learning_rate=1e-6,
            decay_steps=1e4,
            burnin=1500,
            n_steps=50000,
            verbose=False):
    """
    Train mixture model on data
    :param data:  Observations [N, K]
    :param n_components: int, number of gaussian in the mixture
    :param learning_rate: float, Starting learning rate
    :param decay_steps: int, Learning rate decay steps of `PolynomialDecay`
      scheduler
    :param burnin: int, The number of iterations to collect gradient statistics
      to update the pre-conditioner before starting to draw noisy samples.
    :param n_steps: int, Number of optimization steps to take
    :param verbose: bool, If `True` print loss at each steps
    """
    # Setup dataset
    dset = tf.data.Dataset.from_tensor_slices(data) \
      .shuffle(100 * batch_size) \
      .repeat() \
      .batch(batch_size)

    # Setup mixture
    num_samples, feat_dims = data.shape
    mu_range = (data.min(axis=0), data.max(axis=0))
    model = GaussianMixtureModel(n_components=n_components,
                                 feature_dims=feat_dims,
                                 mu_range=mu_range)

    # Optimizer
    lr = PolynomialDecay(initial_learning_rate=learning_rate,
                         decay_steps=decay_steps,
                         end_learning_rate=learning_rate * 1e-4)
    optimizer = SGLD(learning_rate=lr,
                     preconditioner_decay_rate=0.99,
                     burnin=burnin,
                     data_size=num_samples)

    # Optimization step
    @tf.function
    def _train_step(x):
      with tf.GradientTape() as tape:
        tape.watch(model.trainable_variables)
        data_ll, prior_ll = model(x)
        loss = -(prior_ll / num_samples + tf.reduce_mean(data_ll))
      tvars = model.trainable_variables
      gradients = tape.gradient(loss, tvars)
      optimizer.apply_gradients(zip(gradients, tvars))
      return loss

    # Optimize
    for k, obs in enumerate(dset):
      if k == n_steps:
        break
      loss = _train_step(obs).numpy()
      if verbose and (k % 100) == 0:
        print('{:04d} Loss: {:.3f}'.format(k, loss))
    # Done, return model
    return model