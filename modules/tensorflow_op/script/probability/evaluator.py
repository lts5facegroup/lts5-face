# coding=utf-8
"""
Distribution evaluator definition for pixels, background, model prior, ...

See:
  - https://github.com/unibas-gravis/scalismo
"""
__author__ = 'Christophe Ecabert'


class DistributionEvaluator:
  """ A probability or density """

  def log_value(self, sample):
    """
    Estimate log probability/density of sample
    :param sample:  Value at which to estimate the distribution
    :return:  Log of the distribution value
    """
    msg = '{} doesn\'t implement log_value()'.format(self.__class__.__name__)
    raise NotImplementedError(msg)


class PairEvaluator:
  """
  Probability density based on pairs, e.g. Gaussian centered on
  first evaluated at second
  """

  def log_value(self, first, second, weights=None):
    """
    Log probability/density value for a pair of sample
    :param first:   First element
    :param second:  Second element
    :param weights: Sampling weighting, optional
    :return:  Log probability / density
    """
    msg = '{} doesn\'t implement log_value()'.format(self.__class__.__name__)
    raise NotImplementedError(msg)
