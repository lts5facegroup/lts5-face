# coding=utf-8
"""
Collection of prior distributions for statistical model

See:
  https://github.com/unibas-gravis/scalismo-faces
"""
from math import pi
import numpy as np
import tensorflow as tf
from lts5.tensorflow_op.probability.evaluator import DistributionEvaluator
__author__ = 'Christophe Ecabert'

_EPSILON = 1e-8


class UniformPrior(DistributionEvaluator):
  """ Multivariate uniform distribution prior """

  def __init__(self, a, b, dtype=tf.float32):
    """
    Contruct uniform distribution prior bounded by: [a, b]
    :param a:     Lower bound
    :param b:     Upper bound
    :param ndim:  Number of dimensions
    :param dtype: Data type
    """
    self._lower = tf.convert_to_tensor(a, dtype=dtype)
    self._upper = tf.convert_to_tensor(b, dtype=dtype)
    self._theta = self._upper - self._lower
    self._value = -tf.math.log(self._theta)

  def log_value(self, sample):
    """
    Compute the log of the probability density function for given samples
    following a uniform distribution
    :param sample:  Value for which to compute the log of the density function.
                    Stored in the last dimension
    :return:  Likelihood
    """
    with tf.name_scope(self.__class__.__name__) as scope:
      cond = tf.logical_and(sample >= self._lower, sample <= self._upper)
      sample = tf.where(cond, self._value, tf.math.log(1e-8))
      return tf.reduce_sum(sample, axis=-1)


class GaussianPrior(DistributionEvaluator):
  """
  Multivariate gaussian distributed prior with diagonal covariance matrix
  """

  def __init__(self, mean, sdev, ndim, normalized=True, dtype=tf.float32):
    """
    Initialize multivariate gaussian distribution
    :param mean:  Mean value
    :param sdev:  Standard deviation
    :param ndim:  Number of dimensions
    :param normalized:  If `True` normalize the distribution, otherwise biased
    :param dtype: Data type, default float32
    """
    self._mean = tf.convert_to_tensor(mean, dtype=dtype)
    self._sdev = tf.convert_to_tensor(sdev, dtype=dtype)
    self._ndim = ndim
    self._norm = 0.0
    if normalized:
      self._norm = -0.5 * ndim * (tf.math.log(2.0 * pi) + 2.0 * tf.math.log(sdev))
    self._norm = tf.convert_to_tensor(self._norm, dtype=dtype)

  def log_value(self, sample):
    """
    Compute the log of the probability density function for given samples
    :param sample:  Value for which to compute the log of the density function.
                    Stored in the last dimension
    :return:  Output
    """
    with tf.name_scope(self.__class__.__name__) as scope:
      xn = tf.square(sample - self._mean)
      xn = tf.reduce_sum(xn, axis=-1)
      xn = xn / (self._sdev * self._sdev)
      return self._norm - (0.5 * xn)


class GaussianPlateauPrior(DistributionEvaluator):
  """
  Multivariate gaussian prior with plateau at +/- b with diagonal covariance
  matrix

  See: Editing Faces in Videos, Amberg et al, 2011
  """

  def __init__(self, mean, sdev, b, ndim, normalized=True, dtype=tf.float32):
    """
    Constructor
    :param mean:  Mean value
    :param sdev:  Standard deviation
    :param b:     Width of the plateau
    :param ndim:  Number of dimensions
    :param normalized:  If `True` normalize the distribution, otherwise biased
    :param dtype: Data type, default float32
    """
    self._mean = tf.convert_to_tensor(mean, dtype=dtype)
    self._sdev = tf.convert_to_tensor(sdev, dtype=dtype)
    self._ndim = ndim
    self._b = tf.convert_to_tensor(b, dtype=dtype)
    # Compute normalization factor
    self._norm = 0.0
    if normalized:
      a = (0.5 * self._ndim * (tf.math.log(2.0 * pi) +
                               2.0 * tf.math.log(self._sdev)))
      b = self._ndim * tf.math.log(2.0 * self._b)
      log_a = tf.maximum(a, b)
      log_b = tf.minimum(a, b)
      self._norm = -(log_a + tf.math.log1p(tf.exp(log_b - log_a)))
    self._norm = tf.convert_to_tensor(self._norm, dtype=dtype)

  def log_value(self, sample):
    """
    Compute the log of the probability density function for given samples
    :param sample:  Value for which to compute the log of the density function.
                    Stored in the last dimension
    :return:  Output
    """
    with tf.name_scope(self.__class__.__name__) as scope:
      xn = tf.maximum(tf.abs(sample - self._mean) - self._b, 0.0) ** 2.0
      xn = xn / (self._sdev * self._sdev)
      xn = tf.reduce_sum(xn, axis=-1)
      return self._norm - (0.5 * xn)


class MultivariateGaussianPrior(DistributionEvaluator):
  """
  Multivariate gaussian distributed prior with full covariance matrix
  """

  def __init__(self, mu, cov, normalized=True, dtype=tf.float32):
    """
    Constructor
    :param mu:  Mean vector, 1D
    :param cov: Covariance matrix, 2D
    :param normalized:  If `True` add normalization factor, otherwise skip it.
    :param dtype: Data type
    """
    self._mu = tf.convert_to_tensor(mu, dtype=dtype)
    self._cov = tf.convert_to_tensor(cov, dtype=dtype)
    self._norm = 0.0
    # Precompute inverse of covariance matrix
    self._icov = tf.linalg.inv(self._cov)
    if normalized:
      ndim = cov.shape[0]
      self._norm = ((ndim * tf.math.log(2.0 * pi)) +
                    (tf.linalg.logdet(self._cov)))
    self._norm = tf.convert_to_tensor(self._norm, dtype=dtype)

  @classmethod
  def from_file(cls, path, normalized=True, dtype=tf.float32):
    """
    Create multivariate gaussian prior from file
    :param path:  Path the numpy file storing prior
    :param normalized:  If `True` add normalization factor, otherwise skip it.
    :param dtype: Data type
    :return:  Prior object
    """
    # Load dictionary
    data = np.load(path, allow_pickle=True).item()
    return cls(mu=data['mean'],
               cov=data['cov'],
               normalized=normalized,
               dtype=dtype)

  def log_value(self, sample):
    """
    Compute the log of the probability density function for given samples
    :param sample:  Value for which to compute the log of the density function.
                    Stored in the last dimension
    :return:  Output
    """
    x = tf.expand_dims(sample - self._mu, axis=-1)
    dist = tf.matmul(x, tf.matmul(self._icov, x), transpose_a=True)
    dist = tf.squeeze(dist, axis=-1)
    return -0.5 * (self._norm + dist)
