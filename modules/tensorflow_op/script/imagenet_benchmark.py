# coding=utf-8
"""
Framework to validate neural network on imagenet test dataset ILSVRC-2010

See:
  https://github.com/mila-udem/fuel/blob/master/fuel/converters/ilsvrc2010.py
  To convert labels into proper 0based label.
"""
import argparse
import os
from multiprocessing import cpu_count
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'  # Set C++ logging level to warning
import tensorflow as tf
from lts5.tensorflow_op.network.resnet import *
from lts5.tensorflow_op.network.vgg import *
from lts5.tensorflow_op.network.network import *
from lts5.tensorflow_op.data.label import get_imagenet_text_labels
from lts5.tensorflow_op.data.label import get_ilsrvc_2012_validation_label
from lts5.utils import check_py_version
from lts5.utils import init_logger
from lts5.utils import search_folder

# Santiy check + init
check_py_version(3, 0)
logger = init_logger()


net_cls = get_network_classes()
net_lut = {'AlexNet': lambda: net_cls['AlexNet'](n_classes=1000,
                                                 rate=0.0),
           'ResNet50': lambda: net_cls['ResNet'](ResNetSize.SIZE_50,
                                                 ResNetVersion.v1,
                                                 DataFormat.NHWC,
                                                 1000),
           'ResNet101': lambda: net_cls['ResNet'](ResNetSize.SIZE_101,
                                                  ResNetVersion.v1,
                                                  DataFormat.NHWC,
                                                  1000),
           'ResNet152': lambda: net_cls['ResNet'](ResNetSize.SIZE_152,
                                                  ResNetVersion.v1,
                                                  DataFormat.NHWC,
                                                  1000),
           'Vgg16': lambda: net_cls['VggNet'](VggSize.SIZE_16,
                                              1000),
           'Vgg19': lambda: net_cls['VggNet'](VggSize.SIZE_19,
                                              1000)}


def network_constructor(args, lut):
  """
  Create network with default parameters for ImageNet benchmark from a given
  name
  :param args:  Parse argument
  :param lut:  Network lookup table
  :return:  Network constructor
  """
  ctor = lut.get(args.name, None)
  if ctor is None:
    raise ValueError('Unsupported network type: {}'.format(args.name))
  return ctor


def create_label_file(args):
  """
  Create label file if not already done
  :param args:  Parsed argument from command line
  :return: Number total of images
  """
  label_f = os.path.join(args.folder, 'labels.txt')
  if not os.path.exists(label_f):
    logger.info('Create label file')
    # Scan for images
    img_folder = os.path.join(args.folder, 'imagenet_validation')
    images = search_folder(img_folder, ['.JPEG'], relative=False)
    images.sort()
    # Load annotation
    gt = get_ilsrvc_2012_validation_label()
    text_labels = get_imagenet_text_labels()
    with open(label_f, 'w') as label_writer:
      for img, lbl in zip(images, gt):
        lbl_name = text_labels[lbl]
        lbl_name = lbl_name.replace(',', ' -')
        msg = '{}, {}, {}\n'.format(img, lbl, lbl_name)
        label_writer.write(msg)
    return len(images)
  else:
    count = 0
    with open(label_f, 'r') as f:
      for line in f:
        count += 1
    return count


def _img_decode_fcn(line):
  """ Decode an image from a given line (path) """
  # Decode the line into its fields
  fields = tf.decode_csv(records=line, record_defaults=[[''], [''], ['']])
  # Image
  img = tf.read_file(fields[0])
  img = tf.image.decode_jpeg(img)
  img.set_shape([None, None, None])
  # Label
  label = tf.string_to_number(fields[1], tf.int32)
  return img, label


def create_data_loader(args, input_fn, batch_size):
  """
  Create data loader with `tf.dataset` API
  :param args:  Parsed argument from command line
  :param input_fn:  Function applying preprocessing
  :param batch_size:  Batch size
  :return:  Dataset object iterator
  """
  logger.info('Create data loader object')
  fname = os.path.join(args.folder, 'labels.txt')
  n_cpu = cpu_count()
  loader = tf.data.TextLineDataset(filenames=fname)
  # Load image into memory
  loader = loader.map(_img_decode_fcn, n_cpu)
  # Pre-processing
  loader = loader.map(input_fn, n_cpu)
  # Define loader properties
  loader = loader.batch(batch_size).repeat(1).prefetch(batch_size*5)
  # Done, return iterator
  return loader.make_one_shot_iterator()


def run_benchmark(args, n_image):
  """
  Run network benchmark
  :param args:  Parsed arguments
  :param n_image:  Total number of images
  """

  # Network
  logger.info('Create Network')
  net = network_constructor(args=args, lut=net_lut)()

  # Create image producer
  logger.info('Create Image Producer')
  b_size = 25
  it = create_data_loader(args=args, input_fn=net.input_fn, batch_size=b_size)
  images, labels = it.get_next()

  # Define forward + metrics
  with tf.device('gpu'):
    prob = net.inference(x=images)
  # Top K can not run on GPU at the moment
  top_k_op = tf.nn.in_top_k(prob, labels, 5)

  # The number of images processed, The number of correctly classified images
  count = 0
  correct = 0
  # Run benchmark
  logger.info('Run evaluation')
  with tf.Session() as sess:
    # Initialize all variables
    sess.run(tf.global_variables_initializer())
    # Load network
    net.restore(session=sess, path=args.model)
    # Iterate over and classify mini-batches
    process = True
    while process:
      try:
        # Run top k metric
        p = sess.run(top_k_op)
        correct += np.sum(p)
        count += b_size
        if count % 10000 == 0:
          cur_accuracy = float(correct) * 100 / count
          logger.info('{:>6}/{:<6} {:>6.2f}%%'.format(count, n_image, cur_accuracy))

      except tf.errors.OutOfRangeError as e:
        process = False
    # Done
    acc = float(correct) * 100.0 / float(count)
    logger.info('Top 5 Accuracy: {:.3f}%%'.format(acc))
    logger.info('Top 5 Error rate: {:.3f}%%'.format(100.0 - acc))


if __name__ == '__main__':
  # Parser
  ag = argparse.ArgumentParser(description='ImageNet Benchmark')
  ag.add_argument('-n',
                  '--name',
                  type=str,
                  help='Network name (i.e. AlexNet, VGG16, ...)')
  ag.add_argument('-m',
                  '--model',
                  type=str,
                  help='Path to the model file *.npy')
  ag.add_argument('-f',
                  '--folder',
                  type=str,
                  help='Location where test data are stored')
  pargs = ag.parse_args()

  # Create label files if not already done
  n_image = create_label_file(args=pargs)
  # Benchmark
  run_benchmark(pargs, n_image)
