# coding=utf-8
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow_probability.python import distributions as tfd
from tensorflow_probability.python.math import diag_jacobian
from tensorflow.python.framework import test_util
from lts5.tensorflow_op.optimizers import StochasticGradientLangevinDynamics


@test_util.run_all_in_graph_and_eager_modes
class StochasticGradientLangevinDynamicsOptimizerTest(tf.test.TestCase):

  def testBasic(self):
    for dtype in [tf.half, tf.float32, tf.float64]:
      with self.cached_session():
        var0 = tf.Variable([1.1, 2.1], dtype=dtype)
        var1 = tf.Variable([3., 4.], dtype=dtype)
        grads0 = tf.constant([0.1, 0.1], dtype=dtype)
        grads1 = tf.constant([0.01, 0.01], dtype=dtype)
        decay_rate = 0.53
        sgd_optimizer = StochasticGradientLangevinDynamics(
          3.,
          preconditioner_decay_rate=decay_rate)
        if not tf.executing_eagerly():
          sgd_op = sgd_optimizer.apply_gradients(zip([grads0, grads1],
                                                     [var0, var1]))

        self.evaluate(tf.compat.v1.global_variables_initializer())
        # Fetch params to validate initial values
        self.assertAllCloseAccordingToType([1.1, 2.1], self.evaluate(var0))
        self.assertAllCloseAccordingToType([3., 4.], self.evaluate(var1))
        # Run 1 step of sgd
        if not tf.executing_eagerly():
          self.evaluate(sgd_op)
        else:
          sgd_optimizer.apply_gradients(zip([grads0, grads1], [var0, var1]))

        # Validate updated params
        grads_scaled = (0.5 * 0.1 /
                        np.sqrt(
                          decay_rate + (1. - decay_rate) * 0.1 ** 2 + 1e-8))
        # Note that `tfp.math.diag_jacobian(xs=var, ys=grad)` returns zero
        # tensor
        self.assertAllCloseAccordingToType(
          [1.1 - 3. * grads_scaled, 2.1 - 3. * grads_scaled],
          self.evaluate(var0))
        grads_scaled = (0.5 * 0.01 / np.sqrt(
          decay_rate + (1. - decay_rate) * 0.01 ** 2 + 1e-8))
        self.assertAllCloseAccordingToType(
          [3. - 3. * grads_scaled, 4. - 3. * grads_scaled],
          self.evaluate(var1))
        self.assertAllCloseAccordingToType(
          1, self.evaluate(sgd_optimizer.iterations))

  def testBasicMultiInstance(self):
    for dtype in [tf.half, tf.float32, tf.float64]:
      with self.cached_session():
        var0 = tf.Variable([1.1, 2.1], dtype=dtype)
        var1 = tf.Variable([3., 4.], dtype=dtype)
        grads0 = tf.constant([0.1, 0.1], dtype=dtype)
        grads1 = tf.constant([0.01, 0.01], dtype=dtype)
        vara = tf.Variable([1.1, 2.1], dtype=dtype)
        varb = tf.Variable([3., 4.], dtype=dtype)
        gradsa = tf.constant([0.1, 0.1], dtype=dtype)
        gradsb = tf.constant([0.01, 0.01], dtype=dtype)
        decay_rate = 0.5
        sgd_optimizer = StochasticGradientLangevinDynamics(
          3., preconditioner_decay_rate=decay_rate)
        sgd_optimizer2 = StochasticGradientLangevinDynamics(
          3., preconditioner_decay_rate=decay_rate)
        if not tf.executing_eagerly():
          sgd_op = sgd_optimizer.apply_gradients(
            zip([grads0, grads1], [var0, var1]))
          sgd_op2 = sgd_optimizer2.apply_gradients(
            zip([gradsa, gradsb], [vara, varb]))
        self.evaluate(tf.compat.v1.global_variables_initializer())
        # Fetch params to validate initial values
        self.assertAllCloseAccordingToType([1.1, 2.1], self.evaluate(var0))
        self.assertAllCloseAccordingToType([3., 4.], self.evaluate(var1))
        self.assertAllCloseAccordingToType([1.1, 2.1], self.evaluate(vara))
        self.assertAllCloseAccordingToType([3., 4.], self.evaluate(varb))

        # Run 1 step of sgd
        if not tf.executing_eagerly():
          self.evaluate(sgd_op)
          self.evaluate(sgd_op2)
        else:
          sgd_optimizer.apply_gradients(zip([grads0, grads1], [var0, var1]))
          sgd_optimizer2.apply_gradients(zip([gradsa, gradsb], [vara, varb]))

        # Validate updated params
        grads_scaled = (0.5 * 0.1 /
                        np.sqrt(
                          decay_rate + (1. - decay_rate) * 0.1 ** 2 + 1e-8))
        self.assertAllCloseAccordingToType(
          [1.1 - 3. * grads_scaled, 2.1 - 3. * grads_scaled],
          self.evaluate(var0))
        self.assertAllCloseAccordingToType(
          [1.1 - 3. * grads_scaled, 2.1 - 3. * grads_scaled],
          self.evaluate(vara))

        grads_scaled = (0.5 * 0.01 / np.sqrt(
          decay_rate + (1 - decay_rate) * 0.01 ** 2 + 1e-8))
        self.assertAllCloseAccordingToType(
          [3. - 3. * grads_scaled, 4. - 3. * grads_scaled],
          self.evaluate(var1))
        self.assertAllCloseAccordingToType(
          [3. - 3. * grads_scaled, 4. - 3. * grads_scaled],
          self.evaluate(varb))
        self.assertAllCloseAccordingToType(
          1, self.evaluate(sgd_optimizer.iterations))
        self.assertAllCloseAccordingToType(
          1, self.evaluate(sgd_optimizer2.iterations))

  def testTensorLearningRate(self):
    for dtype in [tf.half, tf.float32, tf.float64]:
      with self.cached_session():
        var0 = tf.Variable([1.1, 2.1], dtype=dtype)
        var1 = tf.Variable([3., 4.], dtype=dtype)
        grads0 = tf.constant([0.1, 0.1], dtype=dtype)
        grads1 = tf.constant([0.01, 0.01], dtype=dtype)
        lrate = tf.constant(3.0)
        decay_rate = 0.5
        opt = StochasticGradientLangevinDynamics(
          lrate,
          preconditioner_decay_rate=tf.constant(decay_rate))
        if not tf.executing_eagerly():
          sgd_op = opt.apply_gradients(zip([grads0, grads1], [var0, var1]))
        self.evaluate(tf.compat.v1.global_variables_initializer())
        # Fetch params to validate initial values
        self.assertAllCloseAccordingToType([1.1, 2.1], self.evaluate(var0))
        self.assertAllCloseAccordingToType([3., 4.], self.evaluate(var1))
        # Run 1 step of sgd
        if not tf.executing_eagerly():
          self.evaluate(sgd_op)
        else:
          opt.apply_gradients(zip([grads0, grads1], [var0, var1]))
        # Validate updated params
        grads_scaled = (0.5 * 0.1 /
                        np.sqrt(
                          decay_rate + (1. - decay_rate) * 0.1 ** 2 + 1e-8))
        # Note that `tfp.math.diag_jacobian(xs=var, ys=grad)` returns zero
        # tensor
        self.assertAllCloseAccordingToType(
          [1.1 - 3. * grads_scaled, 2.1 - 3. * grads_scaled],
          self.evaluate(var0))
        grads_scaled = (0.5 * 0.01 / np.sqrt(
          decay_rate + (1. - decay_rate) * 0.01 ** 2 + 1e-8))
        self.assertAllCloseAccordingToType(
          [3. - 3. * grads_scaled, 4. - 3. * grads_scaled],
          self.evaluate(var1))

  def testGradWrtRef(self):
    for dtype in [tf.half, tf.float32, tf.float64]:
      with self.cached_session():
        opt = StochasticGradientLangevinDynamics(3.0)
        values = [1., 3.]
        vars_ = [tf.Variable([v], dtype=dtype) for v in values]
        loss = lambda: vars_[0] + vars_[1]  # pylint: disable=cell-var-from-loop
        grads_and_vars = opt._compute_gradients(loss, vars_)
        self.evaluate(tf.compat.v1.global_variables_initializer())
        for grad, _ in grads_and_vars:
          self.assertAllCloseAccordingToType([1.], self.evaluate(grad))

  def testBurnin(self):
    for burnin_dtype in [tf.int8, tf.int16, tf.int32, tf.int64]:
      with self.cached_session():
        var0 = tf.Variable([1.1, 2.1], dtype=tf.float32)
        grads0 = tf.constant([0.1, 0.1], dtype=tf.float32)
        decay_rate = 0.53
        sgd_optimizer = StochasticGradientLangevinDynamics(
          3.,
          preconditioner_decay_rate=decay_rate,
          burnin=tf.constant(10, dtype=burnin_dtype))
        if not tf.executing_eagerly():
          sgd_op = sgd_optimizer.apply_gradients([(grads0, var0)])

        self.evaluate(tf.compat.v1.global_variables_initializer())
        # Validate that iterations is initialized to 0.
        self.assertAllCloseAccordingToType(
          0, self.evaluate(sgd_optimizer.iterations))
        # Run 1 step of sgd
        if not tf.executing_eagerly():
          self.evaluate(sgd_op)
        else:
          sgd_optimizer.apply_gradients([(grads0, var0)])
        # Validate that iterations is incremented.
        self.assertAllCloseAccordingToType(
          1, self.evaluate(sgd_optimizer.iterations))

  def testWithGlobalStep(self):
    for dtype in [tf.float32, tf.float64]:
      with self.cached_session():
        step = tf.Variable(0, dtype=tf.int64)

        var0 = tf.Variable([1.1, 2.1], dtype=dtype)
        var1 = tf.Variable([3., 4.], dtype=dtype)
        grads0 = tf.constant([0.1, 0.1], dtype=dtype)
        grads1 = tf.constant([0.01, 0.01], dtype=dtype)
        decay_rate = 0.1

        sgd_opt = StochasticGradientLangevinDynamics(
          3., preconditioner_decay_rate=decay_rate)
        sgd_opt.iterations = step
        if not tf.executing_eagerly():
          sgd_op = sgd_opt.apply_gradients(zip([grads0, grads1], [var0, var1]))

        self.evaluate(tf.compat.v1.global_variables_initializer())

        # Fetch params to validate initial values
        self.assertAllCloseAccordingToType([1.1, 2.1], self.evaluate(var0))
        self.assertAllCloseAccordingToType([3., 4.], self.evaluate(var1))
        # Run 1 step of sgd
        if not tf.executing_eagerly():
          self.evaluate(sgd_op)
        else:
          sgd_opt.apply_gradients(zip([grads0, grads1], [var0, var1]))

        # Validate updated params and step
        grads_scaled = (0.5 * 0.1 /
                        np.sqrt(
                          decay_rate + (1. - decay_rate) * 0.1 ** 2 + 1e-8))
        # Note that `tfp.math.diag_jacobian(xs=var, ys=grad)` returns zero
        # tensor
        self.assertAllCloseAccordingToType(
          [1.1 - 3. * grads_scaled, 2.1 - 3. * grads_scaled],
          self.evaluate(var0))
        grads_scaled = (0.5 * 0.01 / np.sqrt(
          decay_rate + (1. - decay_rate) * 0.01 ** 2 + 1e-8))
        self.assertAllCloseAccordingToType(
          [3. - 3. * grads_scaled, 4. - 3. * grads_scaled],
          self.evaluate(var1))
        self.assertAllCloseAccordingToType(1, self.evaluate(step))

  @test_util.run_gpu_only
  def testSparseBasic(self):
    for dtype in [tf.float32, tf.float64]:
      with self.cached_session():
        var0 = tf.Variable([[1.1], [2.1]], dtype=dtype)
        var1 = tf.Variable([[3.], [4.]], dtype=dtype)
        grads0 = tf.IndexedSlices(
          tf.constant([0.1], shape=[1, 1], dtype=dtype),
          tf.constant([0]), tf.constant([2, 1]))
        grads1 = tf.IndexedSlices(
          tf.constant([0.01], shape=[1, 1], dtype=dtype),
          tf.constant([1]), tf.constant([2, 1]))
        decay_rate = 0.9
        sgd = StochasticGradientLangevinDynamics(
          3., preconditioner_decay_rate=decay_rate)
        if not tf.executing_eagerly():
          sgd_op = sgd.apply_gradients(zip([grads0, grads1], [var0, var1]))
        self.evaluate(tf.compat.v1.global_variables_initializer())
        # Fetch params to validate initial values
        self.assertAllCloseAccordingToType([[1.1], [2.1]], self.evaluate(var0))
        self.assertAllCloseAccordingToType([[3.], [4.]], self.evaluate(var1))
        # Run 1 step of sgd
        if not tf.executing_eagerly():
          self.evaluate(sgd_op)
        else:
          sgd.apply_gradients(zip([grads0, grads1], [var0, var1]))
        # Validate updated params
        grads_scaled = (0.5 * 0.1 /
                        np.sqrt(
                          decay_rate + (1. - decay_rate) * 0.1 ** 2 + 1e-8))
        # Note that `tfp.math.diag_jacobian(xs=var, ys=grad)` returns zero
        # tensor
        self.assertAllCloseAccordingToType([[1.1 - 3. * grads_scaled], [2.1]],
                                           self.evaluate(var0))
        grads_scaled = (0.5 * 0.01 / np.sqrt(
          decay_rate + (1. - decay_rate) * 0.01 ** 2 + 1e-8))
        self.assertAllCloseAccordingToType(
          [[3. - 3. * 0], [4. - 3. * grads_scaled]], self.evaluate(var1))

  def testPreconditionerComputedCorrectly(self):
    """Test that SGLD step is computed correctly for a 3D Gaussian energy."""
    with self.cached_session():
      dtype = np.float32
      # Target function is the energy function of normal distribution
      true_mean = dtype([0, 0, 0])
      true_cov = dtype([[1, 0.25, 0.25], [0.25, 1, 0.25], [0.25, 0.25, 1]])
      # Target distribution is defined through the Cholesky decomposition
      chol = tf.linalg.cholesky(true_cov)
      target = tfd.MultivariateNormalTriL(loc=true_mean, scale_tril=chol)
      var_1 = tf.Variable(name='var_1', initial_value=[1., 1.])
      var_2 = tf.Variable(name='var_2', initial_value=[1.])

      var = [var_1, var_2]

      # Set up the learning rate and the optimizer
      learning_rate = .5
      optimizer_kernel = StochasticGradientLangevinDynamics(
        learning_rate=learning_rate, burnin=1)

      # Target function
      def target_fn(x, y):
        # Stack the input tensors together
        z = tf.concat([x, y], axis=-1) - true_mean
        return -target.log_prob(z)

      if not tf.executing_eagerly():
        grads = tf.gradients(ys=target_fn(*var), xs=var)
      else:
        with tf.GradientTape() as tape:
          tape.watch(var)
          loss = target_fn(*var)
        grads = tape.gradient(loss, var)

      # Update value of `var` with one iteration of the SGLD (without the
      # normal perturbation, since `burnin > 0`)
      if not tf.executing_eagerly():
        step = optimizer_kernel.apply_gradients(zip(grads, var))

      # True theoretical value of `var` after one iteration
      decay_tensor = optimizer_kernel._get_hyper('preconditioner_decay_rate', var[0].dtype)
      diagonal_bias = tf.cast(optimizer_kernel.diagonal_bias, var[0].dtype)
      learning_rate = optimizer_kernel._get_hyper('learning_rate', var[0].dtype)
      velocity = [(decay_tensor * tf.ones_like(v)
                   + (1 - decay_tensor) * tf.square(g))
                  for v, g in zip(var, grads)]
      preconditioner = [tf.math.rsqrt(vel + diagonal_bias) for vel in velocity]
      # # Compute second order gradients
      # _, grad_grads = diag_jacobian(
      #   xs=var,
      #   ys=grads)
      # Compute gradient of the preconditioner (compute the gradient manually)
      # preconditioner_grads = [-(g * (1. - decay_tensor) * p ** 3.)
      #                         for g, p in zip(grads, preconditioner)]

      # True theoretical value of `var` after one iteration
      var_true = [v - learning_rate * 0.5 * (p * g)
                  for v, p, g in zip(var, preconditioner, grads)]
      self.evaluate(tf.compat.v1.global_variables_initializer())
      var_true_ = self.evaluate(var_true)
      if not tf.executing_eagerly():
        self.evaluate(step)
      else:
        optimizer_kernel.apply_gradients(zip(grads, var))
      var_ = self.evaluate(var)  # new `var` after one SGLD step
      self.assertAllClose(var_true_,
                          var_, atol=0.001, rtol=0.001)

  def testDiffusionBehavesCorrectly(self):
    """Test that for the SGLD finds minimum of the 3D Gaussian energy."""
    with self.cached_session():
      # Set up random seed for the optimizer
      tf.random.set_seed(42)
      dtype = np.float32
      true_mean = dtype([0, 0, 0])
      true_cov = dtype([[1, 0.25, 0.25], [0.25, 1, 0.25], [0.25, 0.25, 1]])
      # Loss is defined through the Cholesky decomposition
      chol = tf.linalg.cholesky(true_cov)
      var_1 = tf.Variable(name='var_1', initial_value=[1., 1.])
      var_2 = tf.Variable(name='var_2', initial_value=[1.])

      # Loss function
      def loss_fn():
        var = tf.concat([var_1, var_2], axis=-1)
        loss_part = tf.linalg.cholesky_solve(chol, var[..., tf.newaxis])
        return tf.linalg.matvec(loss_part, var, transpose_a=True)

      # Set up the learning rate with a polynomial decay
      starter_learning_rate = .3
      end_learning_rate = 1e-4
      decay_steps = 1e4
      lr = tf.keras.optimizers.schedules.PolynomialDecay(starter_learning_rate,
                                                         decay_steps,
                                                         end_learning_rate)

      # Set up the optimizer
      optimizer_kernel = StochasticGradientLangevinDynamics(
        learning_rate=lr, preconditioner_decay_rate=0.99)
      if not tf.executing_eagerly():
        optimizer = optimizer_kernel.minimize(loss_fn,
                                              var_list=[var_1, var_2])
      else:
        @tf.function
        def _optimizer_fn():
          with tf.GradientTape() as tape:
            tape.watch([var_1, var_2])
            loss = loss_fn()
          grads = tape.gradient(loss, [var_1, var_2])
          optimizer_kernel.apply_gradients(zip(grads, [var_1, var_2]))

      # Number of training steps
      training_steps = 5000
      # Record the steps as and treat them as samples
      samples = [np.zeros([training_steps, 2]), np.zeros([training_steps, 1])]
      self.evaluate(tf.compat.v1.global_variables_initializer())
      for step in range(training_steps):
        if not tf.executing_eagerly():
          self.evaluate(optimizer)
        else:
          _optimizer_fn()

        sample = [self.evaluate(var_1), self.evaluate(var_2)]
        samples[0][step, :] = sample[0]
        samples[1][step, :] = sample[1]

    samples_ = np.concatenate(samples, axis=-1)
    sample_mean = np.mean(samples_, 0)
    self.assertAllClose(sample_mean, true_mean, atol=0.15, rtol=0.1)

if __name__ == '__main__':
  tf.test.main()
