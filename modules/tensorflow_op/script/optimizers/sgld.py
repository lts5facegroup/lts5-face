# coding=utf-8
""" Tensorflow keras optimizer adaptation """
import tensorflow as tf
from tensorflow.keras.optimizers import Optimizer
from tensorflow.python.training.training_ops import resource_apply_gradient_descent

__author__ = 'Christophe Ecabert'
tfd = tf.debugging


class StochasticGradientLangevinDynamics(Optimizer):
  """An optimizer module for stochastic gradient Langevin dynamics.
  This implements the preconditioned Stochastic Gradient Langevin Dynamics
  optimizer [(Li et al., 2016)][1]. The optimization variable is regarded as a
  sample from the posterior under Stochastic Gradient Langevin Dynamics with
  noise rescaled in each dimension according to [RMSProp](
  http://www.cs.toronto.edu/~tijmen/csc321/slides/lecture_slides_lec6.pdf).
  Note: If a prior is included in the loss, it should be scaled by
  `1/data_size`, where `data_size` is the number of points in the data set.
  I.e., it should be divided by the `data_size` term described below.

  Note:
    - For TF2.0 follow: https://github.com/tensorflow/probability/issues/602
    - https://pysgmcmc.readthedocs.io/en/pytorch/_modules/pysgmcmc/optimizers/sgld.html
  """

  def __init__(self,
               learning_rate=1e-4,
               data_size=1,
               burnin=25,
               preconditioner_decay_rate=0.95,
               diagonal_bias=1e-8,
               name='StochasticGradientLangevinDynamics',
               **kwargs):
    """
    Constructor
    :param learning_rate: A `Tensor`, floating point value, or a schedule that
      is a `tf.keras.optimizers.schedules.LearningRateSchedule`, or a callable
      that takes no arguments and returns the actual value to use, The
      learning rate. Defaults to 0.0001.
    :param data_size: Scalar `int`-like `Tensor`. The effective number of
      points in the data set. Assumes that the loss is taken as the mean over a
      minibatch. Otherwise if the sum was taken, divide this number by the
      batch size. If a prior is included in the loss function, it should be
      normalized by `data_size`. Default value: `1`.
    :param burnin: Scalar `int`-like `Tensor`. The number of iterations to
      collect gradient statistics to update the preconditioner before starting
      to draw noisy samples. (Default: `25`)
    :param preconditioner_decay_rate: Scalar `float`-like `Tensor`, or a
      callable that takes no arguments and returns the actual value to use. The
      exponential decay rate of the rescaling of the preconditioner (RMSprop).
      (This is "alpha" in Li et al. (2016)). Should be smaller than but nearly
      `1` to approximate sampling from the posterior. (Default: `0.95`)
    :param diagonal_bias: Scalar `float`-like `Tensor`. Term added to the
      diagonal of the preconditioner to prevent the preconditioner from
      degenerating. (Default: `1e-8`)
    :param name: Optional name for the operations created when applying
      gradients. Defaults to `"StochasticGradientLangevinDynamics"`.
    :param kwargs:  Extra keyword arguments
    """

    super(StochasticGradientLangevinDynamics, self).__init__(name=name,
                                                             **kwargs)
    # Setup hyper parameters
    self._set_hyper('learning_rate', learning_rate)
    self._set_hyper('preconditioner_decay_rate', preconditioner_decay_rate)

    # Fixed parameters
    self.data_size = tf.convert_to_tensor(data_size, name='data_size')
    self.burnin = tf.convert_to_tensor(burnin,
                                       name='burnin',
                                       dtype_hint=tf.int64)
    self.diagonal_bias = tf.convert_to_tensor(diagonal_bias,
                                              name='diagonal_bias')
    # Sanity check
    # tfd.assert_greater(self.data_size,
    #                    0,
    #                    message='`data_size` must be greater than zero')
    # tfd.assert_non_negative(self.burnin,
    #                         message='`burnin` must be non-negative')
    # tfd.assert_integer(self.burnin,
    #                    message='`burnin` must be an integer')
    # tfd.assert_non_negative(self.diagonal_bias,
    #                         message='`diagonal_bias` must be non-negative')


  def _create_slots(self, var_list):
    # Create auxiliary variables
    for var in var_list:
      self.add_slot(var, 'rms', 'ones')

  def _prepare_local(self, var_device, var_dtype, apply_state):
    # Get dynamic variables
    super(StochasticGradientLangevinDynamics, self)._prepare_local(var_device,
                                                                   var_dtype,
                                                                   apply_state)
    # Learning rate automatically decayed if needed in base function
    decay_rate = tf.identity(self._get_hyper('preconditioner_decay_rate',
                                             var_dtype))
    # tfd.assert_positive(decay_rate,
    #                     message='`preconditioner_decay_rate` must be positive')
    # Update remaining values
    apply_state[(var_device, var_dtype)].update(
      dict(
        decay_rate=decay_rate,
        data_size=tf.cast(self.data_size, var_dtype),
        burnin=self.burnin,
        diagonal_bias=tf.cast(self.diagonal_bias, var_dtype)
      ))

  def _resource_apply_dense(self, grad, var, apply_state):
    """ Update for dense grad """
    var_device, var_dtype = var.device, var.dtype.base_dtype
    coefficients = ((apply_state or {}).get((var_device, var_dtype))
                    or self._fallback_apply_state(var_device, var_dtype))

    # Get aux variable
    rms = self.get_slot(var, 'rms')
    # Update
    new_grad = self._apply_noisy_update(rms, grad, var, coefficients)
    return resource_apply_gradient_descent(var.handle,
                                           tf.cast(coefficients['lr_t'],
                                                   var.dtype.base_dtype),
                                           new_grad,
                                           use_locking=self._use_locking)

  def _resource_apply_sparse(self, grad, var, indices, apply_state):
    """ Update for space grad """
    if tf.executing_eagerly():
      raise RuntimeError('SGLD optimizer does not work with sparse gradient in '
                         'eager mode, there is something wrong with how'
                         ' `_resource_scatter_update` works, further '
                         'investigation is needed (are unit test correct or is'
                         ' it something else ?)')
    var_device, var_dtype = var.device, var.dtype.base_dtype
    coefficients = ((apply_state or {}).get((var_device, var_dtype))
                    or self._fallback_apply_state(var_device, var_dtype))
    # Get aux variable
    rms = self.get_slot(var, 'rms')
    # Update
    new_grad = self._apply_noisy_update(rms, grad, var, coefficients, indices)
    update = -new_grad * tf.cast(coefficients['lr_t'], var.dtype.base_dtype)
    return self._resource_scatter_add(var, indices, update)

  def _apply_noisy_update(self, mom, grad, var, coeff, indices=None):
    """
    Compute and apply the gradient update following preconditioned Langevin
     dynamics.
    :param mom: Momentum
    :param grad:  Gradient
    :param var: Variable
    :param coeff: dict of internal hyper parameters
    :param indices: Indices for sparse update
    :return:  Update value
    """

    stddev = tf.where(tf.squeeze(self.iterations > tf.cast(coeff['burnin'],
                                                           tf.int64)),
                      tf.cast(tf.math.rsqrt(coeff['lr_t']), grad.dtype),
                      tf.zeros([], grad.dtype))
    # Keep an exponentially weighted moving average of squared gradients.
    # Not thread safe
    decay_tensor = tf.cast(coeff['decay_rate'], grad.dtype)
    new_mom = decay_tensor * mom + (1. - decay_tensor) * tf.square(grad)
    precond = tf.math.rsqrt(new_mom + tf.cast(coeff['diagonal_bias'],
                                                     grad.dtype))

    # Skip computation of `preconditioner_grads` since it is not compatible
    # with eager mode. See:
    # https://github.com/tensorflow/probability/issues/602#issuecomment-651495721

    # Mean
    mean = 0.5 * (precond * grad * tf.cast(coeff['data_size'], grad.dtype))
    stddev *= tf.sqrt(precond)
    result_shape = tf.broadcast_dynamic_shape( tf.shape(mean),
                                               tf.shape(stddev))
    # Update momentum
    if indices is None:
      update_ops = mom.assign(new_mom)
    else:
      update_ops = self._resource_scatter_update(mom, indices, new_mom)

    # Ensure everything is updated before running gradient adaptation
    with tf.control_dependencies([update_ops]):
      return tf.random.normal(shape=result_shape,
                              mean=mean,
                              stddev=stddev,
                              dtype=grad.dtype)

  def get_config(self):
    cfg = super(StochasticGradientLangevinDynamics, self).get_config()
    cfg.update({
      'learning_rate': self._serialize_hyperparameter('learning_rate'),
      'preconditioner_decay_rate': \
        self._serialize_hyperparameter('preconditioner_decay_rate'),
      'data_size': self.data_size,
      'burnin': self.burnin,
      'diagonal_bias': self.diagonal_bias
    })
    return cfg
