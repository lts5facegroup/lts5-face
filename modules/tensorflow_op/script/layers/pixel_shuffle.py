# coding=utf-8
"""
Tensorflow interface for up-sampling layer based on pixel shuffling.
"""
from tensorflow.keras.layers import Layer
import tensorflow as _tf

__author__ = 'Christophe Ecabert'


class PixelShuffling(Layer):
  """ Tensor up-sampling based on "pixel-shuffling" """

  def __init__(self,block_size, data_format='NHWC', **kwargs):
    """
    Constructor
    :param block_size:  An `int` that is >= 2. The size of the spatial block
    :param data_format: Data layout 'NHWC' or 'NCHW'
    :param kwargs:      Extra keyword arguments
    """
    self.block_sz = block_size
    self.data_frmt = data_format
    super(PixelShuffling, self).__init__(**kwargs)

  def call(self, inputs, **kwargs):
    """
    Run forward computation
    :param inputs:  Tensor to upsample, convert depth channel to space
    :param kwargs:  Extra-keyword arguments
    :return:  Upsampled tensor
    """
    return _tf.nn.depth_to_space(input=inputs,
                                 block_size=self.block_sz,
                                 data_format=self.data_frmt)

  def get_config(self):
    """
    Provide layer's configuration
    :return: dict
    """
    cfg = super(PixelShuffling, self).get_config()
    cfg.update('block_size', self.block_sz)
    cfg.update('data_format', self.data_frmt)
    return cfg
