# coding=utf-8
import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Layer, DepthwiseConv2D
from tensorflow.keras.initializers import Constant as Const

__author__ = 'Christophe Ecabert'


def _get_kernel(ksize, sigma):
  """
  Create 1D gaussian kernel. Based on OpenCV
  :param ksize: int, Filter size
  :param sigma: float, Gaussian standard deviation
  :return:  Kernel
  """
  sigmaX = sigma if sigma > 0.0 else ((ksize - 1) * 0.5 - 1) * 0.3 + 0.8
  scale2X = -0.5 / (sigmaX ** 2.0)
  k = np.arange(ksize).reshape(-1, 1)
  k = k - ((ksize - 1.0) * 0.5)
  k = np.exp((k ** 2.0) * scale2X)
  k /= k.sum()
  return k


def _create_gaussian_kernel(shape, ksize, stddev):
  """
  Create gaussian kernel for a given shape [H, W, C]
  :param shape: Input shape
  :param ksize: int, Kernel size
  :param stddev: float, Kernel
  :return:  Gaussian kernel
  """
  # Gaussian kernel of dims [ksize x ksize]
  k = _get_kernel(ksize=ksize, sigma=stddev)
  k = k @ k.T
  # k = tf.constant(k, self.dtype, shape=[self.ksize, self.ksize, 1, 1])
  k = k.reshape([ksize, ksize, 1, 1])
  # Duplicate kernel based on the number of channels in images
  # Filter must have dims: [ksize, ksize, in_channels, channel_multiplier]
  n_channel = shape[-1]
  kernel = np.tile(k, [1, 1, n_channel, 1])
  return kernel


class GaussianBlur(Layer):
  """ Apply gaussian filter on a given image """

  def __init__(self, ksize, stddev=1.6, name='gaussian_blur', **kwargs):
    """
    Constructor
    :param ksize:   int, Gaussian kernel size
    :param stddev:  float, Gaussian kernel standard deviation
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    super(GaussianBlur, self).__init__(name=name, **kwargs)
    self.ksize = ksize
    self.sigma = stddev
    # Build gaussian kernel + filter
    self.kernel = None
    self.filter = None

  def build(self, input_shape):
    """
    Initialize gaussian kernel
    :param input_shape: Image dimensions inputs
    """
    kernel = _create_gaussian_kernel(input_shape,
                                     ksize=self.ksize,
                                     stddev=self.sigma)
    self.filter = DepthwiseConv2D(kernel_size=self.ksize,
                                  strides=1,
                                  padding='same',
                                  use_bias=False,
                                  depthwise_initializer=Const(kernel),
                                  trainable=False)
    super(GaussianBlur, self).build(input_shape)

  def call(self, inputs):
    """
    Forward pass
    :param inputs:  Image to filter
    :return:  Transformed image
    """
    in_rank = inputs.shape.rank  # Conv2D needs 4D Tensor
    if in_rank == 3:
      image = tf.expand_dims(inputs, 0)
    output = self.filter(image)
    if in_rank == 3:
      output = tf.squeeze(output, 0)
    return output


class GaussianPyramid(Layer):
  """ Create gaussian image pyramid """

  def __init__(self,
               n_level,
               ksize=5,
               sigma=1.6,
               name='gaussian_pyramid',
               **kwargs):
    """
    Contructor
    :param n_level: Number of level in the pyramid
    :param ksize:   Gaussian kernel size
    :param sigma:   Gaussian kernel standard deviation
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    super(GaussianPyramid, self).__init__(name=name, **kwargs)
    self.n_level = n_level
    self.ksize = ksize
    self.sigma = sigma
    # Build gaussian kernel for downsampling
    self.kernel = None
    self.filter = None

  def build(self, input_shape):
    """
    Initialize gaussian kernel
    :param input_shape: Image dimensions inputs
    """
    kernel = _create_gaussian_kernel(input_shape,
                                     ksize=self.ksize,
                                     stddev=self.sigma)
    self.filter = DepthwiseConv2D(kernel_size=self.ksize,
                                  strides=2,
                                  padding='valid',
                                  use_bias=False,
                                  depthwise_initializer=Const(kernel),
                                  trainable=False)
    super(GaussianPyramid, self).build(input_shape)

  def call(self, inputs):
    """
    Forward, create images for each level in the pyramid
    :param inputs:  Image to process
    :return:  List of images
    """
    output_pyr = [inputs]
    for k in range(1, self.n_level):
      im = self._create_single_level(output_pyr[-1])
      output_pyr.append(im)
    return output_pyr

  def _create_single_level(self, image):
    """
    Create a single level of the pyramid
    :param image: Image to process
    :return: Downsampled image
    """
    in_rank = image.shape.rank  # Conv3D needs 4D Tensor
    if in_rank == 3:
      image = tf.expand_dims(image, 0)
    output = self.filter(image)
    if in_rank == 3:
      output = tf.squeeze(output, 0)
    return output


def _create_kernel(gtype, n_filter):
  """
  Build derivative kernel
  :param gtype: Type of approximation
  :param n_filter: Number of tile filter must be replicated
  :return:  Kernel initializer
  """
  if gtype == 'fwd_diff':
    ky = [[0.0, 0.0, 0.0], [0.0, -1.0, 0.0], [0.0, 1.0, 0.0]]
    kx = [[0.0, 0.0, 0.0], [0.0, -1.0, 1.0], [0.0, 0.0, 0.0]]
  elif gtype == 'bwd_diff':
    ky = [[0.0, -1.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 0.0]]
    kx = [[0.0, 0.0, 0.0], [-1.0, 1.0, 0.0], [0.0, 0.0, 0.0]]
  else:
    ky = [[0.0, -0.5, 0.0], [0.0, 0.0, 0.0], [0.0, 0.5, 0.0]]
    kx = [[0.0, 0.0, 0.0], [-0.5, 0.0, 0.5], [0.0, 0.0, 0.0]]
  kernels = np.asarray([ky, kx], dtype=np.float32)  # (2, 3, 3)
  kernels = np.transpose(kernels, (1, 2, 0))         # (3, 3, 2)
  kernels = np.expand_dims(kernels, -2)              # (3, 3, 1, 2)
  kernels = np.tile(kernels, [1, 1, n_filter, 1])    # (3, 3, #Filt, 2)
  return kernels


class SpatialGradient(Layer):
  """
  Compute spatial gradient in y/x direction using finite difference
  approximation.

  Given Image tensor with shape [batch, h, w, d] and type float32, compute
  spatial gradient and return it as a tensor [batch, h, w, d, 2] where the last
  dimension hold [d_dy, d_dx]. There is multiple possible approximation.
  https://en.wikipedia.org/wiki/Finite_difference
  """

  def __init__(self,
               gtype='bwd_diff',
               name='spatial_gradient',
               **kwargs):
    """
    Constructor
    :param gtype: Type of gradient approximation. Options are 'fwd_diff',
                  'bwd_diff' or 'central_diff'
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    super(SpatialGradient, self).__init__(name=name, **kwargs)
    if gtype not in ['fwd_diff', 'bwd_diff', 'central_diff']:
      raise ValueError('Unknown derivatie approximation: {}'.format(gtype))
    self.grad_type = gtype
    self.pad_size = [[0, 0], [1, 1], [1, 1], [0, 0]]
    self.filter = None


  def build(self, input_shape):
    """
    Build layer
    :param input_shape: Input dimensions
    """
    # Define derivative kernel
    kernel = _create_kernel(self.grad_type, input_shape[-1])
    self.filter = DepthwiseConv2D(kernel_size=3,
                                  strides=1,
                                  depth_multiplier=2,
                                  padding='valid',
                                  use_bias=False,
                                  depthwise_initializer=Const(kernel),
                                  trainable=False)
    super(SpatialGradient, self).build(input_shape)

  def call(self, inputs):
    """
    Forward
    :param inputs:  Image
    :return:  Spatial gradient in Y-X direction
    """
    in_rank = inputs.shape.rank  # Conv2D needs 4D Tensor
    if in_rank == 3:
      inputs = tf.expand_dims(inputs, 0)
    # Pad inputs
    shp = inputs.shape
    inputs = tf.pad(inputs, self.pad_size, mode='SYMMETRIC')
    output = self.filter(inputs)
    output = tf.reshape(output, shp + [2])
    if in_rank == 3:
      output = tf.squeeze(output, 0)
    return output


def _build_filter_bank(size: int, in_channel: int):
  """
  Build filter banks that compute pixel difference between center pixel of a
  patch and all of its neighbours
  :param size:  Patch size
  :param in_channel:  Number of channel in the image, usually 3.
  :return:  Set of filters
  """
  filters = np.zeros(shape=(size, size, in_channel, size * size -1),
                     dtype=np.float32)
  n = 0
  half_sz = int(size // 2)
  for k in range(size * size):
    i = k // size
    j = k % size
    if not (i == half_sz and j == half_sz):
      filters[i, j, :, n] = -1.0
      filters[half_sz, half_sz, :, n] = 1.0
      n += 1
  return filters


class PatchPixelDifference(Layer):
  """
  Compute difference between pixel ij and its surroundings neighbor in a given
  patch size. Assume image are in `NHWC` format
  """

  def __init__(self,
               size: int,
               name='PatchPixelDifference',
               **kwargs):
    """
    Constructor
    :param size:  Patch/Neighbour size in pixel
    :param name:  Layer's name
    :param kwargs:  Extra keyword arguments
    """
    super(PatchPixelDifference, self).__init__(name=name, **kwargs)
    if size % 2 == 0:
      raise ValueError('Patch size must be odd, {} % 2 != 1'.format(size))
    self.size = size
    self.in_channels = -1
    self.n_pairs = -1
    self.fbank = None

  def build(self, input_shape):
    """
    Build filter bank
    :param input_shape: Input dimension
    """
    self.in_channels = input_shape[-1]
    self.fbank = tf.constant(_build_filter_bank(self.size, self.in_channels),
                             name='fbanks')
    self.n_pairs = self.fbank.shape[-1]
    super(PatchPixelDifference, self).build(input_shape)

  def call(self, inputs):
    """
    Compute pixel difference via convolution with filter banks
    :param inputs:  Tensor, image
    :return:  Patch pixel difference for each pixel, [B, H, W, #Pair, C]
    """
    h, w = inputs.shape[1:3]
    px_diff = tf.nn.depthwise_conv2d(inputs,
                                     self.fbank,
                                     strides=[1, 1, 1, 1],
                                     padding='SAME')  # [B, H, W, C * #Pair]
    px_diff = tf.reshape(px_diff, (-1, h, w, self.in_channels, self.n_pairs))
    px_diff = tf.transpose(px_diff, (0, 1, 2, 4, 3))
    return px_diff
