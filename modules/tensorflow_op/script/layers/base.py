# coding=utf-8
""" Common function for layers """

__author__ = 'Christophe Ecabert'


def _get_layer_params(type, name, **kwargs):
  """
  Extra layer parameters from kwargs
  :param type:    Type of layer from keras, keras.Layer
  :param name:    Name of the layer parameters needs to be queried, str
  :param kwargs:  keyword arguments
  :return:  dict of params, or empty dict
  """
  layers_params = kwargs.get('layer_params') or {}
  if layers_params is not None:
    # Search for the type of layer
    params = layers_params.get(type.__name__) or {}
    if params is not None:
      # Is parameters for all layer of this type or specific for one layer ?
      # params['all'] is for setting parameters globally otherwise
      # params['layer_name'] is for setting parameters to the layer 'layer_name'
      layer_cfg = params.get(name, None)
      if layer_cfg is not None:
        # We have a specific config
        return layer_cfg
      else:
        # Is there a set of global parameters
        return params.get('all') or {}
    return params
  return layers_params


def create_layer(type, name, **kwargs):
  """
  Create a layer object of a given type with custom parameters.
  The layer's basic parameters will be pass as keyword pair as usual. There
  is one special entry,`layer_params` that can be used to set extra parameters
  to a given layer from the top of the network. It is passed as dictionary
  formatted as:
  layer_params = {<LayerType>.__name__: <Dict, Layer config>} where
  <Layer config> can be parameters for all layers of the same type defined as:

  <Layer config> = {'all': {<params>}} or for a specific layer defined by it's
   name:

   <Layer config> = {'layer name1': {<params set1>},
                     'layer name2': {<params set2>}, ...}

  :param type:    Layer type, keras.Layer
  :param name:    Name of the layer
  :param kwargs:  Extra keyword arguments
  :return:  Layer object
  """
  extra_params = _get_layer_params(type, name, **kwargs)
  # Create layer
  kwargs.pop('layer_params', None)
  # Remove duplicates
  for k in extra_params.keys():
    if k in kwargs.keys():
      kwargs.pop(k)
  layer = type(name=name, **extra_params, **kwargs)
  return layer
