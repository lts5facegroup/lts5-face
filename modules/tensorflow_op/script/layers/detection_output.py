# coding=utf-8
"""
Tensorflow implementation of `DetectionOutputLayer` from caffe SSD

See:
  https://github.com/weiliu89/caffe/blob/ssd/include/caffe/layers/detection_output_layer.hpp
"""

from tensorflow.keras.layers import Layer
from tensorflow.python.framework.errors import InvalidArgumentError
from lts5.tensorflow_op import detection_output_op as d_out_op

__author__ = 'Christophe Ecabert'


class DetectionOutputConfig:
  """
  DetectionOutput layer configuration.

  Attributes
  ----------

  num_classes: int
    Number of classes predicted
  share_location: bool
    If true, bounding box are shared among different classes.
  background_label_id: int
    Background label id. If there is no background class, set it as -1.
  code_type: int
    Type of coding method for bbox [1 = Corner, 2 = CenterSize, 3 = CornerSize].
  variance_encoded_in_target: bool
    If true, variance is encoded in target; otherwise we need to adjust the
    predicted offset accordingly.
  keep_top_k: int
    Number of total bboxes to be kept per image after nms step. -1 means
    keeping all bboxes after nms step.
  confidence_threshold: float
    Only consider detections whose confidences are larger than a threshold.
    If -1.0, consider all boxes.
  nms_threshold: float
    Threshold to be used in nms.
  nms_top_k: int
    Maximum number of results to be kept. -1 keep them all
  nms_eta: float
    Adaptive nms

  """

  def __init__(self,
               num_classes,
               share_location: bool = True,
               background_label_id: int = 0,
               code_type: int = 1,
               variance_encoded_in_target: bool = False,
               keep_top_k: int = -1,
               confidence_threshold: float = -1.0,
               nms_threshold: float = 0.3,
               nms_top_k: int = -1,
               nms_eta: float = 1.0):
    """
    Constructor
    :param num_classes: Number of classes predicted
    :param share_location:  If true, bounding box are shared among different
                            classes.
    :param background_label_id: Background label id. If there is no background
                                class, set it as -1.
    :param code_type: Type of coding method for bbox
    :param variance_encoded_in_target:  If true, variance is encoded in target
    :param keep_top_k:  Number of total bboxes to be kept per image after nms
    :param confidence_threshold:  Only consider detections whose confidences are
                                  larger
    :param nms_threshold: Threshold to be used in nms.
    :param nms_top_k: Maximum number of results to be kept
    :param nms_eta: Adaptive nms
    """
    assert nms_threshold > 0.0, "nms_threshold must be non negative."
    assert 1.0 >= nms_eta > 0.0, "nms_eta must be in range ]0, 1]"
    assert 4 > code_type > 0, "code_type must be in {1, 2, 3}"
    self._num_classes = num_classes
    self._share_location = share_location
    self._background_label_id = background_label_id
    self._code_type = code_type
    self._variance_encoded_in_target = variance_encoded_in_target
    self._keep_top_k = keep_top_k
    self._confidence_threshold = confidence_threshold
    self._nms_threshold = nms_threshold
    self._nms_top_k = nms_top_k
    self._nms_eta = nms_eta

  @property
  def num_classes(self):
    return self._num_classes
  @property
  def share_location(self):
    return self._share_location

  @property
  def background_label_id(self):
    return self._background_label_id

  @property
  def code_type(self):
    return self._code_type

  @property
  def variance_encoded_in_target(self):
    return self._variance_encoded_in_target

  @property
  def keep_top_k(self):
    return self._keep_top_k

  @property
  def confidence_threshold(self):
    return self._confidence_threshold

  @property
  def nms_threshold(self):
    return self._nms_threshold

  @property
  def nms_top_k(self):
    return self._nms_top_k

  @property
  def nms_eta(self):
    return self._nms_eta


class DetectionOutput(Layer):
  """
  Generate the detection output based on location and confidence predictions by
   doing non maximum suppression.

  Attributes
  ----------
  cfg: DetectionOutputConfig
    Layer configuration
  """

  def __init__(self,
               config: DetectionOutputConfig,
               **kwargs):
    """
    Constructor
    :param config:  Layer configuration
    :param kwargs: Extra keywoard arguments
    """
    super(DetectionOutput, self).__init__(**kwargs)
    self._cfg = config

  def build(self, input_shape):
    """
    Check input dimensions list order [location, confidence, priors]
    :param input_shape: Input's dimensions
    """
    if len(input_shape) != 3:
      raise InvalidArgumentError(node_def=None,
                                 op=None,
                                 message='Missing input arguments: [location, ' 
                                         ' confidence, priors]')
    # Call base method
    super(DetectionOutput, self).build(input_shape)

  def call(self, inputs, **kwargs):
    """
    Forward pass, perform box detection
    :param inputs: List of tensors [location, confidence, priors]
    :param kwargs: Extra keyword arguments
    :return:  Box prediction formatted as:
              [image_id, label, confidence, xmin, ymin, xmax, ymax]
    """
    return d_out_op(num_classes=self._cfg.num_classes,
                    share_location=self._cfg.share_location,
                    background_label_id=self._cfg.background_label_id,
                    code_type=self._cfg.code_type,
                    variance_encoded_in_target=self._cfg.variance_encoded_in_target,
                    keep_top_k=self._cfg.keep_top_k,
                    confidence_threshold=self._cfg.confidence_threshold,
                    nms_threshold=self._cfg.nms_threshold,
                    nms_top_k=self._cfg.nms_top_k,
                    nms_eta=self._cfg.nms_eta,
                    location=inputs[0],
                    confidence=inputs[1],
                    priors=inputs[2])

  def get_config(self):
    """
    Provide layer's configuration
    :return:  dict
    """
    cfg = super(DetectionOutput, self).get_config()
    cfg.update('config', self._cfg)
    return cfg
