# coding=utf-8
""" Statistical Model Layer """
import numpy as _np
import tensorflow as tf
import tensorflow.keras.layers as kl
import tensorflow.keras.initializers as ki
from tensorflow.python.training.tracking import base as trackable
from lts5.tensorflow_op.layers.summary import SummaryLayer
from lts5.model import ColorMorphableModelFloat as ColorMorphableModel
from lts5.geometry.laplacian import graph_laplacian

__author__ = 'Christophe Ecabert'


def _parse_sym_idx(filename):
  indexes = []
  with open(filename, 'rt') as f:
    for line in f:
      line = line.strip()
      if len(line) > 0:
        idx = list(map(int, line.split(' ')))
        indexes.append(idx)
  return indexes


def _init_linear_model(model, name, input_shape=None):
  basis = model.variation @ _np.diagflat(model.prior)
  mean = model.mean.reshape(1, -1)
  layer = kl.Dense(units=mean.size,
                   use_bias=True,
                   kernel_initializer=ki.Constant(basis.T),
                   bias_initializer=ki.Constant(mean),
                   trainable=False,
                   name=name)
  if input_shape:
    layer.build(input_shape)
  return layer


def _init_mixing_matrix(m):
  """
  Initialize mixing matrix
  :param m: Matrix
  :return:  Sparse mixing matrix
  """
  n_seg, n_pts = m.shape
  r = _np.arange(n_pts)
  c = _np.concatenate([r + k * n_pts for k in range(n_seg)])
  r = _np.tile(r, n_seg)
  idx = _np.stack([r, c], 0).T
  value = m.ravel()
  mixing = tf.sparse.SparseTensor(indices=idx,
                                  values=value,
                                  dense_shape=(n_pts, n_seg * n_pts))
  return tf.sparse.reorder(mixing)


def _load_laplacian(path):
  """
  Load laplacian from a given file
  :param path:  Path to the numpy file storing sparse laplacian (index, value,
                dims)
  :return: tf.SparseTensor, graph laplacian
  """
  # Load laplacian
  data = _np.load(path, allow_pickle=True).item()
  indices = data['indices']
  values = data['values']
  dims = data['dims']
  lap = tf.sparse.SparseTensor(indices, values, dims)
  return tf.sparse.reorder(lap)


def _load_semantic_mask(path):
  """
  Load sementic segmentation mask from numpy file (.npy)
  :param path:  Path to file
  :return:  tf.Tensor
  """
  mask = _np.load(path, allow_pickle=True).astype(_np.float32)
  mask = _np.expand_dims(mask, 0)  # Add batch size
  return tf.constant(mask, dtype=tf.float32)


class MorphableModel(SummaryLayer):
  """
    3D Morphable Model

    See:
      - https://faces.dmi.unibas.ch/bfm/index.php?nav=1-1-0&id=details
    """

  def __init__(self,
               path,
               with_refinement=False,
               laplacian_path=None,
               sym_vertex_path=None,
               semantic_segmentation_path=None,
               manager=None,
               name='MorphableModel',
               **kwargs):
    """
    Constructor
    :param path:    Path to the morphable model
    :param with_refinement: If `True` corrective basis is added, otherwise no.
    :param laplacian_path:  Path to the file storing laplacian for
                            regularization
    :param sym_vertex_path: Path to the list of vertex symmetry in BFM
    :param semantic_segmentation_path: Path to semantic segmentation mask (i.e.
      face parts)
    :param manager: SummaryWriter manager
    :param name:    Layer's name/scope
    :param kwargs:  Extra keyword arguments
    :keyword w_sym: Weight for color symmetry loss
    :keyword w_smo: Weight for shape smoothness loss (laplacian)
    :keyword w_mag: Weight for correction magnitude loss
    :keyword w_id:  Weight for identity regularization
    :keyword w_exp: Weight for expression regularization
    :keyword w_tex: Weight for texture regularization
    """

    def _log_fn(inputs, step, training):
      l_sym = inputs[0]
      self._w_loss.scalar(data=l_sym,
                          name='Lalb_sym',
                          step=step,
                          training=training)
      if len(inputs) > 1:
        l_smo, l_mag = inputs[1:]
        self._w_loss.scalar(data=l_smo,
                            name='Lshp_smo',
                            step=step,
                            training=training)
        self._w_loss.scalar(data=l_mag,
                            name='Lshp_mag',
                            step=step,
                            training=training)

    # Remove keyword arguments before call base constructor, otherwise exception
    # will be thrown.
    kwargs.pop('layer_params', None)
    self.w_sym = kwargs.pop('w_sym', 1.0)
    self.w_smo = kwargs.pop('w_smo', 1.0)
    self.w_mag = kwargs.pop('w_mag', 1.0)
    self.w_id = kwargs.pop('w_id', 1.0)
    self.w_exp = kwargs.pop('w_exp', 1.0)
    self.w_tex = kwargs.pop('w_tex', 1.0)
    super(MorphableModel, self).__init__(name=name,
                                         manager=manager,
                                         logging_fn=_log_fn,
                                         traces=('loss',),
                                         **kwargs)

    # Morphable model path
    self._path = path
    self.with_segment = False
    # Color + Shape model, Triangulation
    self.n_vertex = -1
    self.n_segment = 1
    self.n_id = -1
    self.n_exp = -1
    self.n_tex = -1
    self.m_color = None
    self.m_shape = None
    self.tri = None
    self.mixing = None
    self._surface_generator_fn = None
    self.semantic_mask = semantic_segmentation_path
    if semantic_segmentation_path:
      self.semantic_mask = _load_semantic_mask(semantic_segmentation_path)
    # Model refinement
    self.with_refinement = with_refinement
    self.shp_corr = None
    self.color_corr = None
    # Laplacian regularization
    self._lap = laplacian_path
    if laplacian_path is not None:
      self._lap = self._load_laplacian(laplacian_path)
    # Albedo symmetry constraints
    self._sym_vertex = sym_vertex_path
    if sym_vertex_path is not None:
      self._sym_vertex = _parse_sym_idx(sym_vertex_path)
      self._sym_vertex = tf.convert_to_tensor(self._sym_vertex,
                                              dtype=tf.int32)

  def build(self, input_shape):
    """
    Check that input's dimensions fulfill the requirements
    :param input_shape: Input's dimensions
    """
    # Sanity check
    n_entry = 4 if self.with_refinement else 2
    if len(input_shape) != n_entry:
      msg = 'Missing input arguments: [w_shape, w_color'
      if n_entry == 4:
        msg += ', w_shape_corr, w_color_corr'
      msg += ']'
      raise ValueError(msg)

    # Check rank
    self._check_inputs_rank(input_shape)

    # Load morphable model (linear)
    mm = ColorMorphableModel()
    err = mm.load(self._path)
    if err != 0:
      raise ValueError('Can not load morphable model: {}'.format(self._path))

    # Sanity check on semantic segmentation mask
    if self.semantic_mask is not None:
      if mm.shape.n_vertex != self.semantic_mask.shape[1]:
        raise ValueError('Dimensions of `shape` model and semantic segmentation'
                         ' mask does not match: {} != {}'
                         .format(mm.shape.n_vertex,
                                 self.semantic_mask.shape[1]))

    # Init matrix / models
    self._init_morphable_model(model=mm, input_shapes=input_shape)

    # Define surface generator
    if self.with_segment:
      self._surface_generator_fn = self._generate_surface_with_segment
    else:
      self._surface_generator_fn = self._generate_surface_without_segment

    # model adaptation
    if self.with_refinement:
      self.shp_corr = kl.Dense(units=3 * self.n_vertex,
                               use_bias=False,
                               kernel_initializer='orthogonal',
                               name='shape_correction')
      self.tex_corr = kl.Dense(units=3 * self.n_vertex,
                               use_bias=False,
                               kernel_initializer='orthogonal',
                               name='color_correction')
      self.shp_corr.build(input_shape[2])
      self.tex_corr.build(input_shape[3])

    # Call base class function (mark the layer as build)
    super(MorphableModel, self).build(input_shape)

  def _check_inputs_rank(self, input_shape):
    """
    Check if input shape match the expected values / ranks
    :param input_shape: List of input dimensions
    """
    # Expected input format: [Batch, #Segment, #Params] => Rank-3
    for k in range(len(input_shape)):
      shp = input_shape[k]
      if k < 2:
        # Face model parameters
        if shp.rank != 3:
          raise ValueError('Input parameters must be of rank-3. Expected format:'
                           ' [Batch, #Segments, parameters]')
        n_segment = shp[1]
        if n_segment != 1 and n_segment != 4:
          raise ValueError('Model support only `1` or `4` segment(s), instead '
                           'received {} segments'.format(n_segment))
      else:
        # Correction, must be [Batch, 1, #Params]
        if shp.rank != 3:
          raise ValueError('Model correction parameters must be of rank 3, got'
                           ' {}'.format(shp.rank))
        n_segment = shp[1]
        if n_segment != 1:
          raise ValueError('Model correction parameters support only 1 segment,'
                           ' got {}.'.format(n_segment))

  @trackable.no_automatic_dependency_tracking
  def _init_morphable_model(self, model, input_shapes):
    """
    Initialization dense layer for 3D morphable model
    NOTE: Function is marked as `no_automatic_dependency_tracking` to avoid to
          dump statistical model into checkpoint. Moreover it allow to use
          different model with the same network.
    :param model: Instance of lts5.model.ColorMorpahbleModel
    :param input_shapes: List of TensorShape to initialize linear model

    """
    # Model parameters dimensions
    self.n_id, self.n_exp = model.shape.dims
    self.n_tex = model.color.prior.size
    # Linear model shp + color
    self.n_vertex = model.shape.n_vertex
    self.m_shape = _init_linear_model(model=model.shape,
                                      name='ShapeModel',
                                      input_shape=input_shapes[0])
    # Create color model
    self.m_color = _init_linear_model(model=model.color,
                                      name='ColorModel',
                                      input_shape=input_shapes[1])
    # Triangulation
    self.tri = tf.constant(model.shape.tri)
    # Mixing matrix for model with segment
    self.with_segment = hasattr(model.shape, 'mixing')
    if self.with_segment:
      # Create mixing matrix
      self.mixing = _init_mixing_matrix(model.shape.mixing)
      self.n_segment = self.mixing.shape[1] // self.mixing.shape[0]

  def call(self, inputs, training=None):
    """
    Forward pass, generate surface + color from a given set of parameters
    :param inputs:  List of tensors [w_shape, w_color(, w_shape_corr,
                    w_color_corr)].
    :param training: If `True` indicates training phase, otherwise no.
    :return:  [vertex, color, triangle, (semantic)], inner_loss
    """
    # Generate instance
    vertex, color = self._surface_generator_fn(inputs)
    # Add refinement
    d_shp, d_tex = self._generate_correction(inputs)
    if self.with_refinement:
      vertex = vertex + d_shp
      color = color + d_shp
    # Ensure proper reflectance range before adding any lighting on top of it
    color = tf.clip_by_value(color, 0.0, 1.0)
    # Add shape/color regularization
    l_smo, l_mag, l_sym = self._model_regularization(vertex,
                                                     color,
                                                     d_shp,
                                                     d_tex)
    l_reg = l_smo + l_mag + l_sym
    super(MorphableModel, self).call([l_sym, l_smo, l_mag], training)
    attributes = [vertex, color, self.tri]
    if self.semantic_mask is not None:
      # duplicate mask n time to have proper batch size
      bs = tf.shape(vertex)[0]
      rep = tf.stack([bs, 1, 1], axis=0)
      semantic_attr = tf.tile(self.semantic_mask, rep)
      attributes.append(semantic_attr)
    return attributes, l_reg

  def _generate_surface_without_segment(self, inputs):
    """
    Generate model instance
    :param inputs:  Input parameters
    :return:  [vertex, color]
    """
    # Convert params from [Batch, 1, #Params] -> [Batch, #Params]
    w_shp = tf.squeeze(inputs[0], axis=1)
    w_tex = tf.squeeze(inputs[1], axis=1)
    vertex = self.m_shape(w_shp)
    color = self.m_color(w_tex)
    vertex = tf.reshape(vertex, (-1, self.n_vertex, 3))
    color = tf.reshape(color, (-1, self.n_vertex, 3))
    return vertex, color

  def _generate_surface_with_segment(self, inputs):
    """
        Generate surface/color for a given set of parameters
        :param inputs:  List of tensors [w_shape, w_color, (w_shp_corr, w_tex_corr)]
        :return: [vertex, color]
        """
    # Generate surface + color
    ns = inputs[0].shape[-1]
    nt = inputs[1].shape[-1]
    w_shp = tf.reshape(inputs[0], (-1, ns))
    w_tex = tf.reshape(inputs[1], (-1, nt))
    vertex = self.m_shape(w_shp)  # [Bsize * #Segment, 3N]
    color = self.m_color(w_tex)  # [Bsize * #Segment, 3N]
    # Blend segment together
    with tf.name_scope('BlendSegment'):
      prod_shp = (-1, self.n_vertex * self.n_segment, 3)
      vs = tf.reshape(vertex, prod_shp)  # [Bsize, #Segment * N, 3]
      vc = tf.reshape(color, prod_shp)   # [Bsize, #Segment * N, 3]

      # Sparse-dense matmul for rank3 matrix
      # https://github.com/tensorflow/tensorflow/issues/9210#issuecomment-497889961
      def _slice_product(x):
        return tf.sparse.sparse_dense_matmul(self.mixing, x)

      # Blend vertex
      vertex = tf.map_fn(fn=_slice_product, elems=vs, dtype=tf.float32)
      vertex = tf.reshape(vertex, (-1, self.n_vertex, 3))
      # Blend colors
      color = tf.map_fn(fn=_slice_product, elems=vc, dtype=tf.float32)
      color = tf.reshape(color, (-1, self.n_vertex, 3))
      return vertex, color

  def _generate_correction(self, inputs):
    """
    Generate shape/color correction
    :param inputs:   inputs parameters
    :return: delta shape / color, or None if not used
    """
    if self.with_refinement:
      # Correction shape is: [Batch, 1, #Params], therefore needs to remove
      # second dimensions before feeding Dense layers.
      d_shp = self.shp_corr(tf.squeeze(inputs[2], axis=1))
      d_tex = self.tex_corr(tf.squeeze(inputs[3], axis=1))
      d_shp = tf.reshape(d_shp, (-1, self.n_vertex, 3))
      d_tex = tf.reshape(d_tex, (-1, self.n_vertex, 3))
      return d_shp, d_tex
    return None, None

  def _model_regularization(self, shape, color, d_shape, d_color):
    """
    Compute regularization for the model
    :param shape:     Vertex
    :param color:     Color
    :param d_shape:   Shape correction
    :param d_color:   Color correction
    :return:  Lsmooth, Lmag, Lsym
    """
    l_smo = tf.constant(0.0, dtype=tf.float32)
    l_mag = tf.constant(0.0, dtype=tf.float32)
    l_sym = tf.constant(0.0, dtype=tf.float32)
    # Shape
    if self.with_refinement and self._lap is not None:
      l_smo, l_mag = self._shape_refinement_regularization(d_shape)
    # Color
    if self._sym_vertex is not None:
      l_sym = self._color_regularization(color)
    return l_smo, l_mag, l_sym

  def _color_regularization(self, color):
    """
    Compute color regularization by imposing symmetry on the albedo
    :param color: Predicted color
    :return:  Cost
    """
    with tf.name_scope('ColorSymmetry'):
      # col[:, self._sym_vertex[:, 0], :]
      side1 = tf.gather(color, indices=self._sym_vertex[:, 0], axis=1)
      # col[:, self._sym_vertex[:, 1], :]
      side2 = tf.gather(color, indices=self._sym_vertex[:, 1], axis=1)
      diff = tf.math.squared_difference(side1, side2)
      diff = tf.reduce_sum(diff, axis=-1)          # [Batch, N]
      l_sym = tf.reduce_mean(diff, axis=-1)        # [Batch, ]
      l_sym = self.w_sym * tf.reduce_mean(l_sym)   # Scalar
      return l_sym

  def _shape_refinement_regularization(self, corr):
    """
    Compute shape regularization
    :param corr:  Shape correction to add
    :return:  Cost: l_smo, l_mag
    """
    # Smoothness, sparse-dense matmul for rank3 tensor
    def _slice_product(x):
      l = tf.sparse.sparse_dense_matmul(self._lap, x)  # [N x 3]
      return tf.matmul(x, l, transpose_a=True)

    l = tf.map_fn(fn=_slice_product, elems=corr)  # [Batch, 3, 3]
    l = tf.linalg.trace(l)                          # [Batch,]
    l_smo = self.w_smo * tf.reduce_mean(l)
    # Magnitude
    shp_c = tf.reduce_sum(tf.square(corr), axis=-1)  # (Batch, N)
    shp_c = tf.reduce_mean(shp_c, axis=1)              # (Batch,)
    l_mag = self.w_mag * tf.reduce_mean(shp_c)         # Scalar
    # Done
    return l_smo, l_mag

  @staticmethod
  def compute_laplacian(triangles, filename=None):
    """
    Compute laplacian
    :param triangles: ndarray of triangles of dimensions [T, 3]
    :param filename:  Optional name to save the laplacian matrix
    :return:  Laplacian matrix
    """
    lap = graph_laplacian(triangles, type='combinatorial', dtype=_np.float32)
    if filename is not None:
      v = {'indices' : _np.stack([lap.row, lap.col], 0).T,
           'values': lap.data,
           'dims': lap.shape}
      _np.save(filename, v)
    return lap


class AmbientOcclusion(kl.Layer):
  """ Generate ambient occlusion coefficient from shape or shape parameter """

  def __init__(self,
               path,
               from_parameter=False,
               name='ambient_occlusion',
               **kwargs):
    """
    Constructor
    :param path:  Path to the statistical model file
    :param from_parameter:  Bool, If `True` indicates the input are shape's
                            parameters. Otherwise it is  the complete shape.
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    super(AmbientOcclusion, self).__init__(name=name, **kwargs)
    self._model_path = path
    self._is_param = from_parameter
    # Shape model - PCA
    self._ishp_basis = None
    self._shp_mean = None
    # AO model - PCA
    self._ao_basis = None
    self._ao_mean = None
    # Joint model
    self._mu_shp_t = None
    self._K = None

  def build(self, input_shape):
    """
    Build layer
    :param input_shape: Input dimensions
    """
    # Load model
    d = _np.load(self._model_path, allow_pickle=True).item()
    # Shape model
    ishp_basis = d['ishp_basis']
    shp_mean = d['shp_mean'].T
    self._ishp_basis = self.add_weight(name='InvShpBasis',
                                       shape=ishp_basis.shape,
                                       dtype=tf.float32,
                                       trainable=False,
                                       initializer=ki.Constant(ishp_basis))
    self._shp_mean = self.add_weight(name='ShpMean',
                                     shape=shp_mean.shape,
                                     dtype=tf.float32,
                                     trainable=False,
                                     initializer=ki.Constant(shp_mean))
    # AO model
    ao_basis = d['ao_basis']
    ao_mean = d['ao_mean'].T
    self._ao_basis = self.add_weight(name='AoBasis',
                                     shape=ao_basis.shape,
                                     dtype=tf.float32,
                                     trainable=False,
                                     initializer=ki.Constant(ao_basis))
    self._ao_mean = self.add_weight(name='AoMean',
                                    shape=ao_mean.shape,
                                    dtype=tf.float32,
                                    trainable=False,
                                    initializer=ki.Constant(ao_mean))
    # Joint model
    w_ao = d['w_ao']
    w_shp = d['w_shp']
    mu_shp = d['mu_shp'].T
    iWtW = _np.linalg.inv(w_ao.T @ w_ao).astype(_np.float32)
    K = w_ao @ (iWtW @ w_shp.T)
    self._mu_shp = self.add_weight(name='MuShp',
                                   shape=mu_shp.shape,
                                   dtype=tf.float32,
                                   trainable=False,
                                   initializer=ki.Constant(mu_shp))
    self._K = self.add_weight(name='K',
                              shape=K.shape,
                              dtype=tf.float32,
                              trainable=False,
                              initializer=ki.Constant(K))
    # Check input dimensions
    input_shape.assert_has_rank(2)
    if self._is_param:
      # Input is shape parameters -> check dims
      if input_shape[1] != K.shape[1]:
        msg = 'Input dimensions does not match, expted value' \
              ' is {} got {}'.format(K.shape[1], input_shape[1])
        raise RuntimeError(msg)
    else:
      # Input is the whole shape
      if input_shape[1] != ishp_basis.shape[1]:
        msg = 'Input dimensions does not match, expted value' \
              ' is {} got {}'.format(ishp_basis.shape[1], input_shape[1])
        raise RuntimeError(msg)
    # Done
    super(AmbientOcclusion, self).build(input_shape)

  def call(self, inputs):
    """
    Forward pass
    :param inputs:  Input Shape / Shape parameters
    :return:  Ambient occlusion coefficients
    """
    theta_shp = inputs
    if not self._is_param:
      # Project shape
      theta_shp = theta_shp - self._shp_mean
      theta_shp = tf.matmul(theta_shp,
                            self._ishp_basis,
                            transpose_b=True)
    # Recover theta_ao
    theta_ao = tf.matmul(theta_shp - self._mu_shp, self._K, transpose_b=True)
    # Reconstruct AO
    ao_var = tf.matmul(theta_ao, self._ao_basis, transpose_b=True)
    ao = self._ao_mean + ao_var
    ao = tf.clip_by_value(ao, 0.0, 1.0) ** 2.0
    return ao  # [Batch, N]
