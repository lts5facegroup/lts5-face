# coding=utf-8
"""
Keras interface for tensorflow `tf.nn.local_response_normalization` function
"""
import tensorflow as _tf
from tensorflow.keras.layers import Layer
from tensorflow.keras.layers import InputSpec as KerasInputSpec
from tensorflow.python.ops.nn import lrn

__author__ = 'Christophe Ecabert'


class LocalResponseNormalization(Layer):
  """
  Local Response Normalization (Krizhevsky et al.)

  See:
    - http://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks
  """

  def __init__(self, depth_radius=5, bias=1, alpha=1.0, beta=0.5, **kwargs):
    """
    Constructor
    :param depth_radius:  An optional int. Defaults to 5. 0-D. Half-width of
                          the 1-D normalization window.
    :param bias:          An optional float. Defaults to 1. An offset
                          (usually positive to avoid dividing by 0).
    :param alpha:         An optional float. Defaults to 1. A scale factor,
                          usually positive.
    :param beta:          An optional float. Defaults to 0.5. An exponent.
    :param kwargs:        Extra keyword arguments
    """
    super(LocalResponseNormalization, self).__init__(**kwargs)
    self.depth_radius = depth_radius
    self.bias = bias
    self.alpha = alpha
    self.beta = beta

  def call(self, inputs, **kwargs):
    """
    Forward
    :param inputs:  A Tensor. Must be one of the following types: half,
                    bfloat16, float32. 4-D.
    :param kwargs:  Extra keyword arguments
    :return:  Normalized tensor
    """
    return lrn(inputs,
               depth_radius=self.depth_radius,
               bias=self.bias,
               alpha=self.alpha,
               beta=self.beta)

  def get_config(self):
    """
    Provide layer's config
    :return:  dict
    """
    cfg = super(LocalResponseNormalization, self).get_config()
    cfg.update('depth_radius', self.depth_radius)
    cfg.update('bias', self.bias)
    cfg.update('alpha', self.alpha)
    cfg.update('beta', self.beta)
    return cfg


class L2Normalize(Layer):
  """
  Performs L2 normalization on the input tensor with a learnable scaling parameter
    as described in the paper "Parsenet: Looking Wider to See Better" (see references)
    and as used in the original SSD model.
  See: http://cs.unc.edu/~wliu/papers/parsenet.pdf
  """

  def __init__(self,
               alpha_initializer=None,
               data_format=None,
               **kwargs):
    """
    Constructor
    :param alpha_initializer:  Initializer for the kernel scale matrix.
    :param data_format: A string, one of 'channels_last' (default) or
                        'channels_first'
    :param kwargs:  Extra keyword arguments
    """
    super(L2Normalize, self).__init__(**kwargs)
    self._alpha_initializer = alpha_initializer or _tf.constant_initializer(1.0)
    self._df = data_format or 'channels_last'
    if self._df not in ['channels_last', 'channels_first']:
      msg = 'data_format must be one of: {}'.format(['channels_last',
                                                     'channels_first'])
      raise ValueError(msg)
    self.axis = -1 if self._df == 'channels_last' else 1
    # Learnable scaling factor
    self.alpha = None

  def build(self, input_shape):
    """
    Initialize learnable scaling factor
    :param input_shape: Dimensions of input tensor
    """
    size = input_shape[self.axis]
    self.alpha = self.add_weight(shape=size,
                                 name='alpha',
                                 initializer=self._alpha_initializer,
                                 trainable=True)
    self.input_spec = KerasInputSpec(dtype=self.dtype,
                                     shape=input_shape,
                                     ndim=input_shape.rank)
    super(L2Normalize, self).build(input_shape)

  def call(self, inputs):
    """
    Forward
    :param inputs:  Tensor to be normalized
    :return:  Normalized tensor
    """
    # Normalize to unit length first, then apply scaling
    x = _tf.nn.l2_normalize(inputs, axis=self.axis)
    x = _tf.multiply(x, self.alpha)
    return x
