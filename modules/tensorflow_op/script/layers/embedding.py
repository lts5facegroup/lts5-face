# coding=utf-8
""" Layer for embedding learning """
import tensorflow as tf
import tensorflow.keras.layers as kl
from lts5.tensorflow_op.layers import create_layer


class CenterLossLayer(kl.Layer):
  """
  Central loss layer

  Ref: https://ydwen.github.io/papers/WenECCV16.pdf
  """

  def __init__(self,
               n_class,
               alpha=0.5,
               with_classification=True,
               name='CentralLossLayer',
               **kwargs):
    """
    Constructor
    :param n_class:   Number of class predicted
    :param alpha:  Moving average momentum for centroids estimation
    :param with_classification: If `True` classification layer is added before
      applying central loss.
    :param name:  Layer's name
    :param kwargs:  Extra keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(CenterLossLayer, self).__init__(name=name, **kwargs)

    # Build layer
    self.alpha = alpha
    self.n_class = n_class
    self.pred = None
    if with_classification:
      self.pred = create_layer(kl.Dense,
                               units=n_class,
                               name='prediction',
                               kernel_regularizer='l2',
                               layer_params=layer_params)
    # Centroids
    self.centers = None

  def build(self, input_shape):
    """
    Build layer
    :param input_shape: Input dimensions. Must be [embedding, labels]
    """
    # Sanity check
    if not isinstance(input_shape, list) and len(input_shape) != 2:
      raise ValueError('Required list of two Tensor: `embedding` and `labels`')

    # Centers. will be manually updated with moving average type of update
    embedding_sz = input_shape[0][1]
    self.centers = self.add_weight(shape=(self.n_class, embedding_sz),
                                   name='centroids',
                                   initializer='zeros',
                                   trainable=False)
    super(CenterLossLayer, self).build(input_shape)

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:  List of tensor: [embbeding, labels]
    :param training:  If `True` indicates training phase, centroids will be
      update at that time otherwise no.
    :return:  (logits), distance between embeddings and its centroids
    """
    embeddings, labels = inputs
    if training is None:
      training = tf.keras.backend.learning_phase()

    if training:
      # Update centers here
      batch_centers = tf.gather(self.centers, tf.reshape(labels, (-1,)))
      # Compute distance
      dist = embeddings - batch_centers  # [Batch, embedding_size]
      sum_dist = tf.tensor_scatter_nd_add(tf.zeros_like(self.centers),
                                          labels,
                                          dist)
      bsize = tf.shape(embeddings)[0]
      sum_labels = tf.tensor_scatter_nd_add(
        tf.zeros((self.n_class, 1), dtype=tf.float32),
        labels,
        tf.ones((bsize, 1), dtype=tf.float32))
      updates = self.alpha * (sum_dist / (1.0 + sum_labels))
      with tf.control_dependencies([updates]):
        centroids = self.centers.assign_add(updates)
    else:
      centroids = self.centers

      # Get updated centers
    batch_centers = tf.gather(centroids, tf.reshape(labels, (-1,)))
    dist = embeddings - batch_centers  # [Batch, embedding_size]
    loss = tf.reduce_sum(tf.square(dist), axis=1)

    # Prediction required ?
    # ------------------------------------------------
    if self.pred is not None:
      pred = self.pred(embeddings)
      return pred, loss
    return loss

