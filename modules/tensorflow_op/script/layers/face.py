# coding=utf-8
"""
Tensorflow Keras abstraction of a 'parametric face' model with the following
parameters:
  - w_id:   Identity parameters
  - w_exp:  Expression parameters
  - w_tex:  Reflectance parameters (i.e. albedo, color without lights
  - rvec:   Rotation parameters in axis angle format (i.e. Rodrigues)
  - tvec:   Position parameters
"""
import tensorflow as tf
from lts5.tensorflow_op.layers.summary import SummaryLayer
from lts5.tensorflow_op.layers.statistical_model import MorphableModel
from lts5.tensorflow_op.layers.transform import SurfaceNormal, RigidTransform
from lts5.tensorflow_op.layers.lighting import SHLighting
from lts5.tensorflow_op.probability.prior_evaluator import GaussianPrior


__author__ = 'Christpohe Ecabert'


class StatisticalRegularizer(SummaryLayer):
  """ Apply statistical regularization to Morphable model parameters """

  def __init__(self,
               identity_size=80,
               expression_size=40,
               texture_size=80,
               w_id=0.0,
               w_exp=0.0,
               w_tex=0.0,
               manager=None,
               name='StatisticalRegularization',
               **kwargs):
    """
    Constructor
    :param identity_size:     Number of identity parameters
    :param expression_size:   Number of expression parameters
    :param texture_size:      Number of texture parameters
    :param w_id: Weight for identity prior
    :param w_exp: Weight for expression prior
    :param w_tex: Weight for texture prior
    :param manager:           Summary writer manager
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """

    # Log
    def _log_fn(inputs, step, training):
      l_id = inputs[0]
      l_exp = inputs[1]
      l_tex = inputs[2]
      w_id = inputs[3]
      w_exp = inputs[4]
      w_tex = inputs[5]
      # Add scalars
      self._w_loss.scalar(name='Lid',
                          data=l_id,
                          step=step,
                          training=training)
      self._w_loss.scalar(name='Lexp',
                          data=l_exp,
                          step=step,
                          training=training)
      self._w_loss.scalar(name='Ltex',
                          data=l_tex,
                          step=step,
                          training=training)
      # Add histogram
      mean_id, var_id = tf.nn.moments(w_id, axes=[1, 2])
      mean_exp, var_exp = tf.nn.moments(w_exp, axes=[1, 2])
      mean_tex, var_tex = tf.nn.moments(w_tex, axes=[1, 2])
      self._w_loss.histogram(name='Wid',
                             data=w_id,
                             step=step,
                             training=training)
      self._w_loss.histogram(name='Wexp',
                             data=w_exp,
                             step=step,
                             training=training)
      self._w_loss.histogram(name='Wtex',
                             data=w_tex,
                             step=step,
                             training=training)
      # Mean
      self._w_loss.histogram(name='Mean/Wid',
                             data=mean_id,
                             step=step,
                             training=training)
      self._w_loss.histogram(name='Mean/Wexp',
                             data=mean_exp,
                             step=step,
                             training=training)
      self._w_loss.histogram(name='Mean/Wtex',
                             data=mean_tex,
                             step=step,
                             training=training)
      # Var
      self._w_loss.histogram(name='Var/Wid',
                             data=var_id,
                             step=step,
                             training=training)
      self._w_loss.histogram(name='Var/Wexp',
                             data=var_exp,
                             step=step,
                             training=training)
      self._w_loss.histogram(name='Var/Wtex',
                             data=var_tex,
                             step=step,
                             training=training)
      # Min/max
      id_min = tf.reduce_min(w_id)
      exp_min = tf.reduce_min(w_exp)
      tex_min = tf.reduce_min(w_tex)
      id_max = tf.reduce_max(w_id)
      exp_max = tf.reduce_max(w_exp)
      tex_max = tf.reduce_max(w_tex)
      self._w_minimum.scalar(name='Wid',
                             data=id_min,
                             step=step,
                             training=training)
      self._w_minimum.scalar(name='Wexp',
                             data=exp_min,
                             step=step,
                             training=training)
      self._w_minimum.scalar(name='Wtex',
                             data=tex_min,
                             step=step,
                             training=training)
      self._w_maximum.scalar(name='Wid',
                             data=id_max,
                             step=step,
                             training=training)
      self._w_maximum.scalar(name='Wexp',
                             data=exp_max,
                             step=step,
                             training=training)
      self._w_maximum.scalar(name='Wtex',
                             data=tex_max,
                             step=step,
                             training=training)

    # Base constructor
    super(StatisticalRegularizer, self).__init__(name=name,
                                                 manager=manager,
                                                 logging_fn=_log_fn,
                                                 traces=('loss',
                                                         'minimum',
                                                         'maximum'),
                                                 **kwargs)
    # Parameters dims
    self._nid = identity_size
    self._ne = expression_size
    self._nt = texture_size
    # Rate
    self._rate_id = w_id
    self._rate_exp = w_exp
    self._rate_tex = w_tex
    # Gaussian prior
    self.p_id = GaussianPrior(0.0, 1.0, self._nid, normalized=False)
    self.p_exp = GaussianPrior(0.0, 1.0, self._ne, normalized=False)
    self.p_tex = GaussianPrior(0.0, 1.0, self._nt, normalized=False)

  def call(self, inputs, training=None):
    """
    Compute negative log-likelihood
    :param inputs:  List of tensor: [Wshp, Wtex, (Willu)]
    :param training: If `True` indicates training phase, otherwise no.
    :return:  negative log-likelihood
    """
    w_shp = inputs[0]
    w_id = w_shp[..., :self._nid]
    w_exp = w_shp[..., self._nid:]
    w_tex = inputs[1]

    l_id = self._prior_loss(w_id, self.p_id, self._rate_id, 'Id')
    l_exp = self._prior_loss(w_exp, self.p_exp, self._rate_exp, 'Exp')
    l_tex = self._prior_loss(w_tex, self.p_tex, self._rate_tex, 'Tex')
    l_reg = l_id + l_exp + l_tex
    p_log = [l_id, l_exp, l_tex, w_id, w_exp, w_tex]
    # Log
    super(StatisticalRegularizer, self).call(p_log, training)
    return l_reg

  @staticmethod
  def _stats_loss(inputs, p_mean, p_var, weight, name):
    """
    Compute mean/var
    :param inputs:  Predicted parameters
    :param p_mean:  Prior mean
    :param p_var:   Prior variance
    :param weight:  Scaling weight
    :param name: Name
    :return:  Loss
    """
    with tf.name_scope('EmpiricalStatsLoss' + name):
      # Get empirical statistics
      mean, var = tf.nn.moments(inputs, axes=[1, 2])
      # Define cost, Genova et al.
      #  - Mean should be zero
      #  - Var should be one
      p_mean = tf.convert_to_tensor(p_mean)
      p_var = tf.convert_to_tensor(p_var)
      # L1 is not sensitive to outlier
      # l_mean = tf.reduce_mean(tf.abs(mean - p_mean))
      # l_var = tf.reduce_mean(tf.abs(var - p_var))
      # L2
      l_mean = tf.reduce_mean(tf.square(mean - p_mean))
      l_var = tf.reduce_mean(tf.square(var - p_var))
      return weight * (l_mean + l_var)

  @staticmethod
  def _prior_loss(inputs, prior, weight, name):
    """
    Apply gaussian prior loss
    :param inputs:  Parameters
    :param prior:   Instance of `GaussianPrior`
    ::param weight:  Scaling weight
    :param name: Name
    :return: Loss
    """
    with tf.name_scope('PriorLoss' + name):
      ll = 0.0
      if prior is not None:
        ll = prior.log_value(inputs)
        ll = -tf.reduce_mean(ll) * weight
      return ll

  @staticmethod
  def _l2_loss(inputs, weight, name):
    """
    Apply gaussian prior loss
    :param inputs:  Parameters
    ::param weight:  Scaling weight
    :param name: Name
    :return: Loss
    """
    with tf.name_scope('PriorLoss' + name):
      ll = tf.reduce_sum(tf.square(inputs), axis=-1)  # [B, #Seg]
      return tf.reduce_mean(ll) * weight


class ParametricFaceModel(SummaryLayer):
  """ Parametric face model """

  def __init__(self,
               face_model_path,
               illumination_prior_path,
               with_illumination=True,
               with_refinement=False,
               laplacian_path=None,
               sym_vertex_path=None,
               semantic_segmentation_path=None,
               w_id=1.0,
               w_exp=1.0,
               w_tex=1.0,
               w_illu=1.0,
               w_sym=1.0,
               w_smo=1.0,
               w_mag=1.0,
               manager=None,
               name='ParametricFaceModel',
               **kwargs):
    """
    Constructor
    :param face_model_path:
    :param illumination_prior_path: Path to illumination prior file, can be
      `None` if shading is not done at vertex level.
    :param with_illumination: If `True` add illumination at vertex level,
      otherwise no.
    :param with_refinement: If `True` corrective basis is added, otherwise no.
    :param laplacian_path: Path to the file storing laplacian for regularization
      Only needed if `with_refinement` is `True`
    :param sym_vertex_path: Path to the list of vertex symmetry in BFM for
      albedo symmetry regularization
    :param semantic_segmentation_path: Path to semantic segmentation mask (i.e.
      face parts)
    :param w_id: Weight for identity regularization
    :param w_exp: Weight for expression regularization
    :param w_tex: Weight for texture regularization
    :param w_illu: Weight for illumination regularization, required if shading
      is done at vertex level.
    :param w_sym: Weight for color symmetry loss
    :param w_smo: Weight for shape smoothness loss (laplacian)
    :param w_mag: Weight for correction magnitude loss
    :param manager: SummaryWriter manager
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """

    def _log_fn(inputs, step, training):
      rvec = inputs[0]
      tvec = inputs[1]
      # Log
      t_min = tf.reduce_min(tvec)
      r_min = tf.reduce_min(rvec)
      t_max = tf.reduce_max(tvec)
      r_max = tf.reduce_max(rvec)
      self._w_minimum.scalar(name='Tvec',
                             data=t_min,
                             step=step,
                             training=training)
      self._w_minimum.scalar(name='Rvec',
                             data=r_min,
                             step=step,
                             training=training)
      self._w_maximum.scalar(name='Tvec',
                             data=t_max,
                             step=step,
                             training=training)
      self._w_maximum.scalar(name='Rvec',
                             data=r_max,
                             step=step,
                             training=training)
      return inputs

    # Remove keyword arguments before call base constructor, otherwise exception
    # will be thrown.
    kwargs.pop('layer_params', None)
    super(ParametricFaceModel, self).__init__(name=name,
                                              manager=manager,
                                              logging_fn=_log_fn,
                                              traces=('minimum',
                                                      'maximum'),
                                              **kwargs)
    # Save input params
    self.face_model_path = face_model_path
    self.illu_prior_path = illumination_prior_path
    self.with_illu = with_illumination
    self.with_refinement = with_refinement
    self.laplacian_path = laplacian_path
    self.sym_vertex_path = sym_vertex_path
    self.semantic_segmentation_path = semantic_segmentation_path
    self.w_id = w_id
    self.w_exp = w_exp
    self.w_tex = w_tex
    self.w_illu = w_illu
    self.w_smo = w_smo
    self.w_mag = w_mag
    self.w_sym = w_sym
    self.manager = manager
    # Create sublayer
    self.face_model = MorphableModel(path=self.face_model_path,
                                     with_refinement=self.with_refinement,
                                     laplacian_path=self.laplacian_path,
                                     sym_vertex_path=self.sym_vertex_path,
                                     semantic_segmentation_path=self.semantic_segmentation_path,
                                     w_id=self.w_id,
                                     w_exp=self.w_exp,
                                     w_tex=self.w_tex,
                                     w_smo=self.w_smo,
                                     w_mag=self.w_mag,
                                     w_sym=self.w_sym,
                                     manager=self.manager)
    self.surf_normal = SurfaceNormal()
    self.transform = RigidTransform()
    self.lighting = None
    if self.with_illu:
      self.lighting = SHLighting(self.illu_prior_path,
                                 self.w_illu,
                                 self.manager)
    # Statistical regularization
    self.stat_reg = None

  def build(self, input_shape):
    """
    Build layer
    :param input_shape: List of dimensions of the inputs tensors
    """
    # Sanity check on inputs dimensions
    if not isinstance(input_shape, list):
      raise ValueError('Layer is expecting a list of parameters')
    n_param = 5 if self.with_illu else 4
    if len(input_shape) != n_param and not self.with_refinement:
      illu_name = ', w_illu' if self.with_illu else ''
      msg = 'Missing parameters, expected {} tensors ([w_shp, w_tex, rvec,' \
            ' tvec{}], but got instead {} tensors'.format(n_param,
                                                          illu_name,
                                                          len(input_shape))
      raise ValueError(msg)
    if len(input_shape) != (n_param + 2) and self.with_refinement:
      illu_name = 'w_illu ,' if self.with_illu else ' '
      msg = 'Missing parameters, expected {} tensors ([w_shp, w_tex, '
      'rvec, tvec,{}w_shp_corr, w_tex_corr], but got'
      ' instead {} tensors'.format(n_param + 2,
                                   illu_name,
                                   len(input_shape))
      raise ValueError(msg)
    # Build face model
    p_shp = input_shape[:2]
    if self.with_refinement:
      p_shp.extend(input_shape[-2:])
    self.face_model.build(p_shp)

    # Initialize statistical regularization
    illu_prior = self.illu_prior_path
    n_id = self.face_model.n_id
    n_exp = self.face_model.n_exp
    n_tex = self.face_model.n_tex
    self.stat_reg = StatisticalRegularizer(identity_size=n_id,
                                           expression_size=n_exp,
                                           texture_size=n_tex,
                                           w_id=self.w_id,
                                           w_exp=self.w_exp,
                                           w_tex=self.w_tex,
                                           manager=self.manager)
    # Done
    super(ParametricFaceModel, self).build(input_shape)

  def call(self, inputs, training=None):
    """
    Forward pass
    :param inputs:  Face parameters: [w_shp, w_tex, rvec, tvec, (w_illu,
      w_shp_corr, w_tex_corr)]
    :param kwargs:  Extra keyword arguments
    :return:  dict of tensors: {'vertex': [B, N, 3],
                                'albedo': [B, N, 3],
                                'normal': [B, N, 3],
                                'tri': [T, 3],
                                'shading': [B, N, 3] (opt)
                                'color': [B, N, 3]   (opt)
                                'semantic': [N, 1]   (opt)},
               Tensor, regularization loss
    """
    # Parameters
    w_shp, w_tex, rvec, tvec = inputs[:4]   # Shape, Appearance, Rot, Trans
    # Log pose
    super(ParametricFaceModel, self).call([rvec, tvec], training)
    # Generate surface
    model_p = [w_shp, w_tex]
    if self.with_refinement:
      # Add shp/tex correction parameters
      model_p += inputs[-2:]
    attributes, inner_loss = self.face_model(model_p)
    vertex = attributes[0]
    albedo = attributes[1]
    tri = attributes[2]
    # Rigid transform
    vertex = self.transform([vertex, rvec, tvec])
    # Surface normals
    normal, _ = self.surf_normal([vertex, tri])
    # Pack results into dict
    outputs = {'vertex': vertex,
               'albedo': albedo,
               'normal': normal,
               'tri': tri}
    if len(attributes) > 3:
      outputs['semantic'] = attributes[3]
    # Parameter to regularize
    p_reg = [w_shp, w_tex]
    # Add lighting
    if self.with_illu:
      w_illu = inputs[4]
      p_reg += [w_illu]
      # Lighting
      shading, inner_light_loss = self.lighting([normal, w_illu], training)
      color = albedo * shading
      outputs['shading'] = shading
      outputs['color'] = color
      inner_loss += inner_light_loss
    # Regularization
    inner_loss += self.stat_reg(p_reg, training)
    # Done
    return outputs, inner_loss
