# coding=utf-8
import tensorflow as _tf
from tensorflow.keras.layers import Layer
from lts5.tensorflow_op.layers.summary import SummaryLayer
from lts5.tensorflow_op.utils.math import build_perspective_matrix
from lts5.tensorflow_op.utils.math import eye_to_screen
from lts5.tensorflow_op import rodrigues_op as r_op
from lts5.tensorflow_op import surface_normal_op as surf_normal_op
from lts5.tensorflow_op import dynamic_landmark_op as dyn_proj
from lts5.tensorflow_op import dynamic_landmark_op_v2 as dyn_proj_v2
from lts5.tensorflow_op import eye_to_clip_op, clip_to_screen_op
from lts5.tensorflow_op.probability import GaussianPrior, UniformPrior

__author__ = 'Christophe Ecabert'


class RigidTransform(Layer):
  """
  Apply rigid transformation to a given flatten array [Batch x 3N]
  """

  def __init__(self,
               name='RigidTransform',
               **kwargs):
    """
    Constructor
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    super(RigidTransform, self).__init__(name=name, **kwargs)

  def build(self, input_shape):
    """
    Check that input dimensions match the requirements of the layers
    :param input_shape: Input dimensions
    """
    # Sanity check
    if len(input_shape) != 2 and len(input_shape) != 3:
      raise ValueError('Missing input arguments: [x, rvec, (tvec)]')
    # Check rank
    for k, shp in enumerate(input_shape):
      if shp.rank != 3:
        raise ValueError('Inputs must be rank-3 Tensor, got {} for'
                         ' {}-th input'.format(shp.rank, k))
      if shp[-1] != 3:
        raise ValueError('Last dimension if input {} is not valid,'
                         ' {} != 3'.format(k, shp[-1]))
    # Call base method
    super(RigidTransform, self).build(input_shape)

  def call(self, inputs):
    """
    Forward operation, apply rigid transform
    :param inputs:  List of tensors [x, rvec, (tvec)]
    :param kwargs:  Extra keyword arguments
    :return:  y = R(rvec)x + tvec
    """
    x = inputs[0]   # [Batch, N, 3]
    # Apply rotation
    rmat = r_op(rvec=inputs[1])
    x = _tf.matmul(x, rmat, transpose_a=False, transpose_b=True)
    # Add translation ?
    if len(inputs) > 2:
      tvec = inputs[2]
      x += tvec  # Should broadcast
    return x

  def compute_output_shape(self, input_shape):
    """
    Computes the output shape of the layer.
    :param input_shape: Shape tuple (tuple of integers) or list of shape tuples
                        (one per output tensor of the layer).
                        Shape tuples can include None for free dimensions,
                        instead of an integer.
    :return: Dimensions
    """
    return input_shape[0]


class PerspectiveProjection(Layer):
  """ Pinhole camera projection """

  def __init__(self,
               focal: float = 525.0,
               width: int = 200,
               height:int = 200,
               indices:list = None,
               top_left: bool = True,
               name='PerspectiveProjection',
               **kwargs):
    """
    Constructor
    :param focal:   Camera focal length
    :param width:   Image width
    :param height:  Image height
    :param indices: List of vertex index to project if only a subset is
                    required. `None` otherwise
    :param top_left:If `True` convert to top left coordinate system.
    :param name:    Layer's name
    :param kwargs:  Extra arguments
    """
    super(PerspectiveProjection, self).__init__(name=name, **kwargs)
    self.f = focal
    self.cx = width / 2.0
    self.cy = height / 2.0
    self.indices = indices
    self.idx = _tf.convert_to_tensor(indices) if indices is not None else indices
    # Build projection matrix
    # Set negate last column since the camera in opengl looks in `-z` direction.
    # NOTE: Should be moved to `build` function
    self.pmat = _tf.convert_to_tensor([[self.f, 0.0, -self.cx],
                                      [0.0, self.f, -self.cy],
                                      [0.0, 0.0, -1.0]])
    # TopLeft conversion
    self.top_left = top_left
    # NOTE: Should be moved to `build` function
    self.tl = _tf.convert_to_tensor([[1.0, 0.0, 0.0],
                                    [0.0, -1.0, height]], dtype=_tf.float32)

  def call(self, inputs, **kwargs):
    """
    Forward computation
    :param inputs:  Shape to project with dimensions of [Batch, 3N]
    :param kwargs:  Extra arguments
    :return:  Projected vertex
    """
    # Reshape input in [Batch, 3, N]
    x_shp = _tf.shape(inputs)
    x = _tf.reshape(inputs, shape=[x_shp[0], -1, 3])
    # Select vertex ?
    if self.idx is not None:
      x = _tf.gather(params=x, indices=self.idx, axis=1)
    # Project
    y = _tf.matmul(x, self.pmat, transpose_a=False, transpose_b=True)
    y /= _tf.expand_dims(y[:, :, 2], axis=-1)   # Divide by `z`
    # Convert to top left system ?
    if self.top_left:
      y = _tf.matmul(y,
                    self.tl,
                    transpose_a=False,
                    transpose_b=True)   # [Batch, N, 2]
    else:
      y = y[:, :, :2]                             # [Batch, N, 2]
    return _tf.reshape(y, shape=[x_shp[0], -1])    # [Batch, 2N]

  def compute_output_shape(self, input_shape):
    """
    Computes the output shape of the layer.
    :param input_shape: Shape tuple (tuple of integers) or list of shape tuples
                        (one per output tensor of the layer).
                        Shape tuples can include None for free dimensions,
                        instead of an integer.
    :return: Dimensions
    """
    return [input_shape[0], 2 * (input_shape[1] / 3)]

  def get_config(self):
    """
    Provide layer's configuration
    :return:  dict
    """
    cfg = super(PerspectiveProjection, self).get_config()
    cfg.update('focal', self.f)
    cfg.update('width', self.cx * 2.0)
    cfg.update('height', self.cy * 2.0)
    cfg.update('indices',self.indices)
    cfg.update('top_left', self.top_left)
    return cfg


def _check_vertex_shape(shape):
  # Sanity check on vertex input
  if shape.rank != 3:
    raise ValueError('Vertex input tensor must be rank 3, got'
                     ' {}'.format(shape.rank))
  if shape[-1] != 3:
    raise ValueError('Only accept vertices with 3 components, got {} !='
                     ' 3'.format(shape[-1]))


class OGLLandmarksProjection(Layer):
  """
  Project landmark from 3D surface using transformation from OpenGL pipeline.
  This layer can used fixed focal length or dynamic one.
  """

  def __init__(self,
               indices: list,
               focal: float,
               near: float,
               far: float,
               width: float,
               height: float,
               top_left=True,
               name='OGLLandmarks',
               **kwargs):
    """
    Constructor
    :param indices:   List of list of indices to project
    :param focal:     Focal length. If focal < 0.0, it indicates that it uses
      dynamic focal length and will be passed as input, otherwise consider it
      fixed.
    :param near:      Near plane
    :param far:       Far plane
    :param width:     Image width
    :param height:    Image height
    :param top_left:  If `True` convert to top left coordinate system
    :param name:      Layer's name
    :param kwargs:    Extra keyword arguments
    """
    super(OGLLandmarksProjection, self).__init__(name=name, **kwargs)
    self.focal = focal
    self.dyn_focal = focal < 0.0
    self.near = near
    self.far = far
    self.width = width
    self.height = height
    self.cx = width / 2.0
    self.cy = height / 2.0
    self.screen_size = _tf.constant([[[width, height]]],
                                    dtype=_tf.float32)  # [1, 1, 2]
    self.top_left = top_left
    self.indices = _tf.convert_to_tensor(indices, dtype=_tf.int32)
    self.perspective = None

  def build(self, input_shape):
    """
    Build layer
    :param input_shape: Input dimensions
    """
    if self.dyn_focal:
      if not isinstance(input_shape, (tuple, list)):
        raise ValueError('With dynamic focal, layer inputs must be a list of'
                         ' Tensor')
      if len(input_shape) != 2:
        raise ValueError('Layer expected two inputs: [vertex, focal], got {} '
                         '!= 2'.format(len(input_shape)))
      # Vertex shape sanity
      _check_vertex_shape(input_shape[0])
      # Focal sanity check
      f_shp = input_shape[1]
      if f_shp.rank != 2:
        raise ValueError('Focal length tensor must be of rank 2,'
                         ' got {}'.format(f_shp.rank))
      if f_shp[-1] != 1:
        raise ValueError('Last dimension of focal input must be equal to 1,'
                         ' got {}'.format(f_shp[-1]))
      # Build auxiliary vars: cx, cy, near, far ... or no ?

    else:
      if not isinstance(input_shape, _tf.TensorShape):
        raise ValueError('Layer input must be a single tensor of dimensions'
                         ' [B, N ,3]')
      # Vertex shape
      _check_vertex_shape(input_shape)
      # Build perspective matrix
      a = -(self.far + self.near) / (self.far - self.near)
      b = -(2.0 * self.far * self.near) / (self.far - self.near)
      m = [[[[self.focal / self.cx, 0.0, 0.0, 0.0],
           [0.0, self.focal / self.cy, 0.0, 0.0],
           [0.0, 0.0, a, b],
           [0.0, 0.0, -1.0, 0.0]]]]
      self.perspective = _tf.constant(m, dtype=_tf.float32)  # [1, 1, 4, 4]

    # Built
    super(OGLLandmarksProjection, self).build(input_shape)

  def call(self, inputs):
    """
    Foward pass
    :param inputs:  Tensor or list of tensor if using dynamic focal length
    :return:  Projected points
    """
    vertex = inputs[0] if self.dyn_focal else inputs
    focal = inputs[1] if self.dyn_focal else None
    # Gather vertex
    vertex = _tf.gather(vertex,
                        indices=self.indices,
                        axis=1)

    # Build projection matrix
    pmat, dims = self._build_perspective_matrix(vertex, focal)
    # Project points
    v_screen, _ = eye_to_screen(vertex, pmat, dims, self.top_left)
    # Done, only interested into x,y component
    return v_screen[..., :2]

  def _build_perspective_matrix(self, vertex, focal):
    """
    Build perspective matrix or reuse the pre-computed one
    :param vertex:  Tensor of vertex
    :param focal:   Tensor of focal or None
    :return:  Projection matrix, screen dimensions
    """
    if self.dyn_focal:
      bsize = _tf.shape(vertex)[:-1]
      # Projection
      shp = _tf.concat((bsize, (1, )), axis=-1)
      ones = _tf.ones(shp, _tf.float32)
      cx = ones * self.cx
      cy = ones * self.cy
      near = ones * self.near
      far = ones * self.far
      pmat = build_perspective_matrix(focal=focal,
                                      cx=cx,
                                      cy=cy,
                                      near=near,
                                      far=far)
      # Screen dims
      dims = _tf.tile(self.screen_size, shp)
    else:
      # Repeat projection matrix
      bsize = _tf.shape(vertex)[:-1]  # [Batch, K]  K == 68
      rep = _tf.concat((bsize, (1, 1)), axis=-1)
      pmat = _tf.tile(self.perspective, rep)
      # Screen dims
      rep = _tf.concat((bsize, (1,)), axis=-1)
      dims = _tf.tile(self.screen_size, rep)
    return pmat, dims


class DynamicLandmarkProjection(Layer):
  """
  Project landmark from 3D surface and optionally perform dynamic matching

  Attributes
  ----------
  """

  def __init__(self,
               indices: list,
               focal: float = 525.0,
               width: int = 200,
               height: int = 200,
               top_left: bool = True,
               thresh=5.0,
               output_index=False,
               name='DynamicLandmarks',
               **kwargs):
    """
    Constructor
    :param indices:   List of list of indices to project
    :param focal:     Focal length
    :param width:     Image width
    :param height:    Image height
    :param top_left:  If `True` convert to top left coordinate system
    :param thresh:    Rotation angle threshold for frontal detection
    :param name:      Layer's name
    :param kwargs:    Extra keyword arguments
    """
    super(DynamicLandmarkProjection, self).__init__(name=name, **kwargs)
    self.f = focal
    self.width = width
    self.height = height
    self.top_left = top_left
    self.thresh = thresh
    self.return_index = output_index
    # convert indices to matrix format
    max_idx = max(map(len, indices))
    index = []
    for idx in indices:
      row = [len(idx)] + idx + [0] * (max_idx - len(idx))
      index.append(row)
    self.indices = _tf.convert_to_tensor(index, dtype=_tf.int32)

  def build(self, input_shape):
    """
    Check that input dimensions match the requirements of the layers
    :param input_shape:
    """
    # Sanity check
    if (not isinstance(input_shape, list) or
        (len(input_shape) != 2 and len(input_shape) != 3)):
      raise ValueError('Missing input arguments: [vertex, normal (, rvec)]')
    # Call base method
    super(DynamicLandmarkProjection, self).build(input_shape)

  def call(self, inputs):
    """
    Forward pass computing landmarks projection
    :param inputs:  List of tensors: [vertex, normal].
    :return:  Landmarks
    """
    # [Batch, N, 3]
    vertex = inputs[0]
    normal = inputs[1]
    if len(inputs) == 2:
      lms, idx = dyn_proj(vertex=vertex,
                          normal=normal,
                          focal=self.f,
                          height=self.height,
                          width=self.width,
                          indices=self.indices,
                          top_left=self.top_left)  # [Batch, K, 2], [Batch, K]
    else:
      lms, idx = dyn_proj_v2(vertex=vertex,
                             normal=normal,
                             rvec=inputs[2],
                             focal=self.f,
                             height=self.height,
                             width=self.width,
                             indices=self.indices,
                             thresh=self.thresh,
                             top_left=self.top_left)
    if self.return_index:
      return lms, idx
    return lms

  def compute_output_shape(self, input_shape):
    """
    Computes the output shape of the layer.
    :param input_shape: Shape tuple (tuple of integers) or list of shape tuples
                        (one per output tensor of the layer).
                        Shape tuples can include None for free dimensions,
                        instead of an integer.
    :return: Dimensions
    """
    shp = input_shape[0]
    bsize = shp[0]
    n = shp[1]
    return [bsize, n, 2]


class EyeToScreenProjection(Layer):
  """ Implement vertex conversion from `eye` space (i.e. camera) into `screen`
   space using OpenGL pipeline"""

  def __init__(self,
               width: int,
               height: int,
               near: float,
               far: float,
               top_left: bool = True,
               subset=None,
               name='EyeToScreenProjection',
               **kwargs):
    """
    Constructor
    :param width:     Image width
    :param height:    Image height
    :param near:      Near plane
    :param far:       Far plane
    :param top_left:  If `True` indicate origin is located at top left corner,
      otherwise will be at bottom left corner
    :param subset:    Optional list of index to select to project
    :param name:      Op's name
    :param kwargs:    Extra keyword arguments
    """
    super(EyeToScreenProjection, self).__init__(name=name, **kwargs)
    self.width = width
    self.height = height
    self.near = near
    self.far = far
    self.top_left = top_left
    self.selection = None
    if subset is not None:
      self.selection = _tf.convert_to_tensor(subset)

  def build(self, input_shape):
    """
    Build layer and perform sanity check
    :param input_shape: List of TensorShape
    """
    if not isinstance(input_shape, (tuple, list)):
      raise ValueError('Requires multiple input Tensors: [vertex, focal]')

    if len(input_shape) != 2:
      raise ValueError('Missing input tensor, requires [Vertex, Focal], got {} '
                       '!= 2'.format(len(input_shape)))

    v_shp, f_shp = input_shape
    if v_shp.rank != 3:
      raise ValueError('Input vertex Tensor must be rank 3 with dimensions: '
                       '[Batch, N, 3], got rank {}'.format(v_shp.rank))
    if v_shp[-1] != 3:
      raise ValueError('Last dimension of vertex must be equal to 3, got'
                       ' {}'.format(v_shp[-1]))

    if f_shp.rank != 2:
      raise ValueError('Input focal Tensor must be rank 2 with dimensions: '
                       '[Batch, 1], got rank {}'.format(f_shp.rank))
    if f_shp[-1] != 1:
      raise ValueError('Last dimension of focal must be equal to 1, got'
                       ' {}'.format(f_shp[-1]))
    super(EyeToScreenProjection, self).build(input_shape)

  def call(self, inputs):
    """
    Forward
    :param inputs:  List of Tensors: [Vertex, Focal] of shape [B, N, 3] and
      [B, 1]
    :return:  Projected vertex or subset, [B, 2]
    """
    vertex, focal = inputs
    # Select subset ?
    if self.selection is not None:
      vertex = _tf.gather(vertex,
                          indices=self.selection,
                          axis=1,
                          batch_dims=0)
    # Project to screen in two steps
    v_clip = eye_to_clip_op(vertex=vertex,
                            focal=focal,
                            near=self.near,
                            far=self.far,
                            width=self.width,
                            height=self.height)
    v_screen = clip_to_screen_op(vertex=v_clip,
                                 width=self.width,
                                 height=self.height,
                                 top_left=self.top_left)
    return v_screen


class SurfaceNormal(Layer):
  """ Compute surface's normals from vertices and triangles """

  def __init__(self, name='SurfaceNormal', **kwargs):
    """
    Constructor
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    super(SurfaceNormal, self).__init__(name=name, **kwargs)

  def build(self, input_shape):
    """
    Check that input dimensions match the requirements of the layers
    :param input_shape:
    """
    # Sanity check
    if not isinstance(input_shape, list) or len(input_shape) != 2:
      raise ValueError('Missing input arguments: [vertex, triangle]')

    # Call base method
    super(SurfaceNormal, self).build(input_shape)

  def call(self, inputs):
    """
    Forward pass computing surface's normals
    :param inputs:  List of tensors: [vertex, triangle].
    :return:  Surface's normals, normal scale (i.e. norm) before normalization
    """
    return surf_normal_op(vertex=inputs[0], triangle=inputs[1])

  def compute_output_shape(self, input_shape):
    """
    Computes the output shape of the layer.
    :param input_shape: Shape tuple (tuple of integers) or list of shape tuples
                        (one per output tensor of the layer).
                        Shape tuples can include None for free dimensions,
                        instead of an integer.
    :return: Dimensions
    """
    return input_shape[0]


class ColorTransform(SummaryLayer):
  """
  Linear Color Transform
    Aldrian and Smith, , Inverse Rendering of Faces with a 3D Morphable Model,
     2013
  """

  def __init__(self,
               reg_cfg,
               manager=None,
               name='ColorTransform',
               **kwargs):
    """
    Constructor
    :param reg_cfg:  Regularization configuration
                      dict:
                        'weight': float
                        'offset': (float, float)  => mean, std
                        'gain': (float, float)  => mean, std
                     If `None` use default value
    :param manager: Summary writer manager
    :param kwargs:  Extra keyword arguments
    """

    # Log
    def _log_fn(inputs, step, training):
      gain = inputs[0]
      offset = inputs[1]
      contrast = inputs[2]
      likelihood = inputs[3]
      g_max = _tf.reduce_max(gain)
      g_min = _tf.reduce_min(gain)
      o_max = _tf.reduce_max(offset)
      o_min = _tf.reduce_min(offset)
      c_max = _tf.reduce_max(contrast)
      c_min = _tf.reduce_min(contrast)
      self._w_minimum.scalar(name='Gain',
                             data=g_min,
                             step=step,
                             training=training)
      self._w_minimum.scalar(name='Offset',
                             data=o_min,
                             step=step,
                             training=training)
      self._w_minimum.scalar(name='Constrast',
                             data=c_min,
                             step=step,
                             training=training)
      self._w_maximum.scalar(name='Gain',
                             data=g_max,
                             step=step,
                             training=training)
      self._w_maximum.scalar(name='Offset',
                             data=o_max,
                             step=step,
                             training=training)
      self._w_maximum.scalar(name='Constrast',
                             data=c_max,
                             step=step,
                             training=training)
      self._w_loss.scalar(name='Lct',
                          data=likelihood,
                          step=step,
                          training=training)
    # Base
    super(ColorTransform, self).__init__(name=name,
                                         manager=manager,
                                         logging_fn=_log_fn,
                                         traces=('minimum', 'maximum', 'loss'),
                                         **kwargs)
    self._gray = _tf.convert_to_tensor([[[0.3, 0.59, 0.11],
                                        [0.3, 0.59, 0.11],
                                        [0.3, 0.59, 0.11]]], dtype=_tf.float32)

    # Regularization
    reg_cfg = {'weight': 1.0,
               'offset': (0.0, 0.02),
               'gain': (1.0, 0.1)} if reg_cfg is None else reg_cfg
    self._w_reg = reg_cfg['weight']
    self._off_eval = GaussianPrior(mean=reg_cfg['offset'][0],
                                   sdev=reg_cfg['offset'][1],
                                   ndim=3)
    # self._gain_eval = GaussianPrior(mean=reg_cfg['gain'][0],
    #                                 sdev=reg_cfg['gain'][1],
    #                                 ndim=3)
    # self._c_eval = UniformPrior(a=0.0, b=1.0)

  def build(self, input_shape):
    """
    Check that input dimensions match the requirements of the layers
    :param input_shape: Input dimensions
    """
    # Input must be a list of two
    if not isinstance(input_shape, list) or len(input_shape) != 2:
      raise ValueError('Wrong number of inputs: [color, params]')
    # Sanity check, shape must be [Batch, 7]
    if input_shape[1].rank != 2 and input_shape.dims[-1] != 7:
      raise ValueError('Color transform parameters must have dimensions of'
                       ' [Batch, 7]')
    # Call base method
    super(ColorTransform, self).build(input_shape)

  def call(self, inputs, training=None):
    """
    Forward pass computing surface's normals
    :param inputs:  list of tensors: [color, params].
                    Color dims is [Batch, 3N]
                    Where each column of params is:
                    (gain_r, gain_g, gain_b, offset_r, offset_g, offset_b, c)
                    provided by logits [i.e. unnormalized]
    :return:  Transformed color [Batch, 3N]
    """
    c_shp = _tf.shape(inputs[0])
    bsize = c_shp[0]
    # Reshape [Batch, N, 3]
    color = _tf.reshape(inputs[0], (bsize, -1, 3))  # [Batch, N, 3]
    # Normalize transformation
    gain = _tf.maximum(inputs[1][:, :3], 0.0)  # should be > 0
    offset = inputs[1][:, 3:6]                 # untouched
    contrast = inputs[1][:, -1]
    # Regularization
    # --------------------------------
    l_reg = self._regularization(gain=gain,
                                 contrast=contrast,
                                 offset=offset)
    self.add_loss(l_reg, inputs=True)
    # Build linear transformation
    # --------------------------------
    # Log
    super(ColorTransform, self).call([gain, offset, contrast, l_reg], training)
    # Apply transform
    # --------------------------------
    # c = _tf.reshape(contrast, (-1, 1, 1))
    # g_mat = _tf.linalg.diag(gain)
    # c_mat = ((c * _tf.eye(3, batch_shape=[bsize])) +
    #          ((1.0 - c) * _tf.tile(self._gray, [bsize, 1, 1])))
    # trsfrm = _tf.matmul(g_mat, c_mat)
    # color_t = _tf.matmul(color, trsfrm, transpose_a=False, transpose_b=True)
    # color_t += _tf.reshape(offset, (-1, 1, 3))
    # color_t = _tf.reshape(color_t, c_shp)
    color_t = color + _tf.reshape(offset, (-1, 1, 3))
    color_t = _tf.reshape(color_t, c_shp)
    return color_t

  def _regularization(self, gain, contrast, offset):
    with _tf.name_scope('regularization'):
      l_off = self._off_eval.log_value(sample=offset)       # [B,]
      # l_gain = self._gain_eval.log_value(sample=gain)       # [B,]
      # l_c = self._c_eval.log_value(sample=contrast)         # [B,]
      # return -self._w_reg * _tf.reduce_mean(l_off + l_gain + l_c)
      return -self._w_reg * _tf.reduce_mean(l_off)

  def compute_output_shape(self, input_shape):
    """
    Computes the output shape of the layer.
    :param input_shape: Shape tuple (tuple of integers) or list of shape tuples
                        (one per output tensor of the layer).
                        Shape tuples can include None for free dimensions,
                        instead of an integer.
    :return: Dimensions
    """
    return input_shape[0]

