# coding:utf-8
"""
Augmentation of `keras.layers.Layer` class to enable easy summary recording.
"""
import tensorflow as _tf
from tensorflow.keras.backend import learning_phase
from tensorflow.keras.layers import Layer

__author__ = 'Christophe Ecabert'


class SummaryLayer(Layer):
  """ Layer base class augmented with summary recording capability """

  def __init__(self,
               name,
               manager=None,
               logging_fn=None,
               traces=(),
               **kwargs):
    """
    Constructor
    :param name:      Layer's name (i.e. scope)
    :param manager:   Instance of `SummaryWriterManager` used to manage writer.
                      If `None` no summary will be saved.
                      Note `manager` should outlive the layer!
    :param logging_fn: Callback responsible of recording to generate summaries.
                       The signature must be `log_fn(inputs, step, training)`
                        where:
                       `inputs` is a Tensor of list of tensor to be summarized.
                       `step` is the step internal step counter.
                       `training` indicates training phase.
    :param traces:    List of traces/writers to create. Used only if `manager`
                      is not `None`, in this case the layer will have attributes
                      named `_w_<trace>` which are writers available for
                      recording summaries
    :param kwargs:    Extra keyword arguments
    """
    super(SummaryLayer, self).__init__(name=name, **kwargs)
    # Recording enable
    self._manager = manager
    if self._manager is not None:
      # Create writers
      for tr in traces:
        setattr(self, '_w_{}'.format(tr), self._manager.get_writer(tr))

    # Create identity callback if not used
    def _dummy_log_fn(inputs, step, training):
      pass

    # set logging function, DO NOT decorate it with `tf.function` otherwise
    # it does not work!!!
    self._log_fn = _dummy_log_fn if self._manager is None else logging_fn

    # Create step variable
    self._step = self.add_weight(name='{}_step'.format(self.name),
                                 dtype=_tf.int64,
                                 initializer=_tf.keras.initializers.Constant(0),
                                 trainable=False)
    # Create step variable
    self._val_step = self.add_weight(name='{}_val_step'.format(self.name),
                                     dtype=_tf.int64,
                                     initializer=_tf.keras.initializers.Constant(0),
                                     trainable=False)

  def call(self, inputs, training=None):
    """
    Forward pass
    :param inputs:  Tensor or list of tensors to be summarized
    :param training: If `True` indicates training phase, otherwise no.
    :return:        inputs
    """
    with _tf.name_scope('logging') as scope:
      if training is None:
        training = learning_phase()
      if training:
        self._log_fn(inputs, self._step, training)
        self._step.assign_add(1)
      else:
        self._log_fn(inputs, self._val_step, training)
        self._val_step.assign_add(1)
      return inputs
