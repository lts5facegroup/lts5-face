# coding=utf-8
""" Differentiable rendering layer """
import tensorflow as tf
from tensorflow.keras.layers import Layer
from lts5.tensorflow_op import rendering_op
from lts5.tensorflow_op import rasterizing_op, eye_to_clip_op
from lts5.tensorflow_op import rasterizing_v2_op
from lts5.tensorflow_op.utils import perspective_corrected_barycentrics

__author__ = 'Christophe Ecabert'


def _build_perspective_matrix(focal, cx, cy, near, far):
  """
  Build opengl perspective matrix from focal length and image center points
  :param focal: Tensor, focal length [B, 1]
  :param cx:  Tensor, center point x [B, 1]
  :param cy:  Tensor, center point y [B, 1]
  :param near: Tensor, Near plane [B, 1]
  :param far:   Tensor, Far plane [B, 1]
  :return:  Tensor, perspective matrix [B, 4, 4]
  """
  zero = tf.zeros_like(focal)
  one = tf.ones_like(focal)
  a = -(far + near) / (far - near)
  b = -(2.0 * far * near) / (far - near)
  mat = tf.concat([focal / cx, zero, zero, zero,
                   zero, focal / cy, zero, zero,
                   zero, zero, a, b,
                   zero, zero, -one, zero], axis=-1)
  shp = tf.shape(mat)
  output_shp = tf.concat((shp[:-1], (4, 4)), axis=-1)
  return tf.reshape(mat, shape=output_shp)


class Renderer(Layer):
  """ Differentiable Rendering Layer based on OpenGL pipeline """

  def __init__(self,
               width=200,
               height=200,
               focal=525.0,
               near=1e0,
               far=1e3,
               visible=False,
               n_face=-1,
               extra_output=False,
               multisampling=4,
               **kwargs):
    """
    Constructor
    :param width:     Image width
    :param height:    Image height
    :param focal:     Focal length
    :param near:      Near plane
    :param far:       Far plane
    :param visible:   If `True` rendering window will be visible.
    :param n_face:    Number of primitive face to render, if `-1` render them
                      all
    :param extra_output:  If `True` add extra output tensor (i.e. Barycentric
                          coordinate, mask, spatial gradient)
    :param multisampling: Multisampling factor for aliasing, set to 0 to disable
    :param kwargs:    Extra keyword arguments
    """
    self._width = width
    self._height = height
    self._focal = focal
    self._near = near
    self._far = far
    self._visible = visible
    self._n_face = n_face
    self._extra_output = extra_output
    self._multisampling = multisampling
    super(Renderer, self).__init__(**kwargs)

  def build(self, input_shape):
    """
    Check input dimensions list order
    [vertex, normal, color, triangle, background]
    :param input_shape: Input's dimensions
    """
    if not isinstance(input_shape, list) or len(input_shape) != 5:
      raise RuntimeError('Missing input arguments: [vertex, '
                         'normal, color, tri, background]')
    # Base
    super(Renderer, self).build(input_shape)

  def call(self, inputs, **kwargs):
    """
    Forward pass, render a given scene
    :param inputs:      List of tensors [vertex, normal, color, triangle,
                        background]
    :param kwargs:      Extra keyword arguments
    :keyword training:  If `True` will compute extra parameters for gradient
    :return:  Rendered images + [barycentric coordinates, mask, spatial
              gradient] during training phase
    """
    renderings = rendering_op(vertex=inputs[0],
                              normal=inputs[1],
                              color=inputs[2],
                              background=inputs[4],
                              triangle=inputs[3],
                              width=self._width,
                              height=self._height,
                              near=self._near,
                              far=self._far,
                              focal=self._focal,
                              visible=self._visible,
                              n_face=self._n_face,
                              training=self._extra_output,
                              multisampling=self._multisampling)
    # renderings: [Image]
    # renderings: [Image, barycentric, spgrad] (training)
    return renderings


class DeferredRenderer(Layer):
  """ Differentiable Rendering Layer based on OpenGL pipeline. Based on
  rasterization + deferred rendering """

  def __init__(self,
               width=200,
               height=200,
               focal=525.0,
               near=1e0,
               far=1e3,
               with_normal=False,
               visible=False,
               name='DeferredRendering',
               **kwargs):
    """
    Constructor
    :param width:     Image width
    :param height:    Image height
    :param focal:     Focal length
    :param near:      Near plane
    :param far:       Far plane
    :param with_normal: If `True` rasterize normals as well, otherwise no.
    :param visible:   If `True` rendering window will be visible.
    :param name:      Layer's name
    :param kwargs:    Extra keyword arguments
    """
    super(DeferredRenderer, self).__init__(name=name, **kwargs)
    self._width = width
    self._height = height
    self._focal = focal
    self._near = near
    self._far = far
    self._visible = visible
    self._with_normals = with_normal
    # Pre-define some vars
    # Screen dimensions
    self._screen_dims = tf.constant([[width, height]],
                                    dtype=tf.float32,
                                    name='screen_dimensions')
    # Pixel grid
    px = tf.linspace(0.5, width - 0.5, num=width)
    py = tf.linspace(0.5, height - 0.5, num=height)
    xv, yv = tf.meshgrid(px, py)
    self._pixel_grid = tf.stack((xv, yv), axis=-1)
    # Perspective matrix
    a = 2.0 * focal / width
    b = 2.0 * focal / height
    c = -(far + near) / (far - near)
    d = -(2.0 * far * near) / (far - near)
    self._perspective_mat = tf.constant([[[a, 0.0, 0.0, 0.0],
                                          [0.0, b, 0.0, 0.0],
                                          [0.0, 0.0, c, d],
                                          [0.0, 0.0, -1.0, 0.0]]],
                                        dtype=tf.float32,
                                        name='perspective_matrix')

  def build(self, input_shape):
    """
    Check input dimensions list order
    [vertex, normal, color, triangle, background]
    :param input_shape: Input's dimensions
    """
    if not isinstance(input_shape, list) or len(input_shape) != 5:
      raise RuntimeError('Missing input arguments: [vertex, normal, color,'
                         ' tri, background]')
    # Base
    super(DeferredRenderer, self).build(input_shape)

  def call(self, inputs, **kwargs):
    """
    Forward pass, render a given scene
    :param inputs:      List of tensors [vertex, normal, color, triangle,
                        background]
    :param kwargs:      Extra keyword arguments
    :keyword training:  If `True` will compute extra parameters for gradient
    :return:  Rendered images
    """
    vertex = inputs[0]
    normal = inputs[1]
    color = inputs[2]
    tri = inputs[3]
    bg = inputs[4]

    # Rasterize
    tri_idx, bcoords, mask = self._rasterize_scene(vertex, tri)
    # Interpolate
    masked_tri = (tri_idx * mask)
    mask_float = tf.cast(tf.expand_dims(mask, axis=-1), vertex.dtype)
    image = self._interpolate(attribute=color,
                              barycentric=bcoords,
                              triangle=tri,
                              tri_index=masked_tri,
                              blend=mask_float,
                              background=bg)
    # Ensure image is in proper range
    image = tf.clip_by_value(image, 0.0, 1.0)
    image = [tf.concat([image, mask_float], axis=-1)]
    # Add normal if wanted
    if self._with_normals:
      normal_img = self._interpolate(attribute=normal,
                                     barycentric=bcoords,
                                     triangle=tri,
                                     tri_index=masked_tri,
                                     blend=mask_float,
                                     background=tf.zeros_like(bg))
      image.append(tf.concat([normal_img, mask_float], axis=-1))
    return image

  def _rasterize_scene(self, vertex, triangle):
    """
    Raszerize scene using OpenGL backend
    :param vertex:    Vertex, [Bs, 3N]
    :param triangle:  Triangle, [T, 3]
    :return:
    """
    with tf.name_scope('rasterize'):
      rasterized = rasterizing_op(width=self._width,
                                  height=self._height,
                                  near=self._near,
                                  far=self._far,
                                  focal=self._focal,
                                  vertex=vertex,
                                  triangle=triangle,
                                  visible=self._visible)
      # Convert output
      tri_index = tf.cast(rasterized[..., 0] - 1.0, tf.int32)
      bcoords = rasterized[..., 1:]
      mask = tf.cast(tri_index >= 0, tf.int32)
      return tri_index, bcoords, mask

  def _compute_barycenrics(self, vertex, tri, tri_index, mask):
    """
    Compute barycentric coordinates
    :param vertex:  Vertices, [Bs, 3N]
    :param tri:     Triangles [T, 3]
    :param tri_index: Triangle index at each pixels [Bs, H, W]
    :return:
    """
    with tf.name_scope('barycentric'):
      bs = tf.shape(vertex)[0]
      vertex = tf.reshape(vertex, (bs, -1, 3))
      vertex = tf.gather(vertex, tri, axis=-2)
      # Gather does not work on negative indices, which is the case for the
      # pixel associated to the background.
      tri_index = tri_index * mask
      vertices_per_pixel = tf.gather(vertex,
                                     tri_index,
                                     axis=-3,
                                     batch_dims=len(tri_index.shape[:-2]))
      return perspective_corrected_barycentrics(vertices_per_pixel,
                                              self._pixel_grid,
                                              self._perspective_mat,
                                              self._screen_dims)

  def _interpolate(self,
                   attribute,
                   barycentric,
                   triangle,
                   tri_index,
                   blend,
                   background):
    """
    Interpolate attributes
    :param attribute:     Attributes to interpolate, [B, N, 3]
    :param barycentric:   Barycentric coordinates, []
    :param triangle:      List of triangles, [T, 3]
    :param triangle_index:  Triangle index at each pixel location
    :param blend:         Binary mask where the rasterized surface live.
    :param background:    Background value
    :return:  Interpolated attributes [B, H, W, K]
    """
    with tf.name_scope('interpolate'):
      bs = tf.shape(attribute)[0]
      attr = tf.reshape(attribute, (bs, -1, 3))
      attr = tf.gather(attr, triangle, axis=-2)
      attribute_per_pixel = tf.gather(attr,
                                      tri_index,
                                      axis=-3,
                                      batch_dims=len(tri_index.shape[:-2]))
      interp = tf.expand_dims(barycentric, axis=-1) * attribute_per_pixel
      interp = tf.reduce_sum(interp, axis=-2)
      return (blend * interp) + ((1.0 - blend) * background)


class DeferredRendererV2(Layer):
  """ Differentiable Rendering Layer based on OpenGL pipeline. Based on
  rasterization + deferred rendering """

  def __init__(self,
               width=200,
               height=200,
               near=1e0,
               far=1e3,
               visible=False,
               name='DeferredRenderingV2',
               **kwargs):
    """
    Constructor
    :param width:     Image width
    :param height:    Image height
    :param near:      Near plane
    :param far:       Far plane
    :param visible:   If `True` rendering window will be visible.
    :param name:      Layer's name
    :param kwargs:    Extra keyword arguments
    """
    super(DeferredRendererV2, self).__init__(name=name, **kwargs)
    self._width = width
    self._height = height
    self._near = near
    self._far = far
    self._visible = visible

  def build(self, input_shape):
    """
    Build layer and perform sanity check on input shape
    :param input_shape: List of TensorShape
    """
    if not isinstance(input_shape, (tuple, list)):
      raise ValueError('Rasterizer requires multiple inputs: [vertex, focal, '
                       'tri]')

    if len(input_shape) != 3:
      raise ValueError('Missing input tensors, requires [vertex, focal, '
                       'tri], {} != 3'.format(len(input_shape)))

    v_shp, f_shp, tri_shp = input_shape
    if v_shp.rank != 3:
      raise ValueError('Input vertex Tensor must be rank 3 with dimensions: '
                       '[Batch, N, 3], got rank {}'.format(v_shp.rank))
    if v_shp[-1] != 3:
      raise ValueError('Last dimension of vertex must be equal to 3, got'
                       ' {}'.format(v_shp[-1]))

    if f_shp.rank != 2:
      raise ValueError('Input focal Tensor must be rank 2 with dimensions: '
                       '[Batch, 1], got rank {}'.format(f_shp.rank))
    if f_shp[-1] != 1:
      raise ValueError('Last dimension of focal must be equal to 1, got'
                       ' {}'.format(f_shp[-1]))

    if tri_shp.rank != 2:
      raise ValueError('Input triangle Tensor must be rank 2 with dimensions: '
                       '[T, 3], got rank {}'.format(tri_shp.rank))

    if tri_shp[-1] != 3:
      raise ValueError('Last dimension of triangle must be equal to 3, got'
                       ' {}'.format(tri_shp[-1]))
    super(DeferredRendererV2, self).build(input_shape)

  def call(self, inputs, attributes, **kwargs):
    """
    Forward pass, render a given scene
    :param inputs:      List of tensors [vertex, focal, triangle]
    :param attributes:  List of tuple of attributes defined  as (value,
        callable, background, type) where:
          value:  Value of the attributes to interpolate [Batch, N, K]
          callable: Post processing function to apply on the interpolated
                    attribute. If not used pass tf.identity
          background: Tensor to use as background, [B, H, W, K]
          type: Interpolation type: ('lin' or 'flat')
    :return:  Interpolated attributes
    """
    vertex = inputs[0]
    focal = inputs[1]
    tri = inputs[2]
    # Rasterize
    tri_idx, bcoords, mask = self._rasterize_scene(vertex, focal, tri)
    # Interpolate
    masked_tri = (tri_idx * mask)
    mask_float = tf.cast(tf.expand_dims(mask, axis=-1), vertex.dtype)
    images = []
    for attribute in attributes:
      attr, attr_fn, bg, interp_type = attribute
      value = self._interpolate(attribute=attr,
                                barycentric=bcoords,
                                triangle=tri,
                                tri_index=masked_tri,
                                blend=mask_float,
                                background=bg,
                                type=interp_type)
      # Post processing
      value = attr_fn(value)
      # Add mask
      value = tf.concat([value, mask_float], axis=-1)
      images.append(value)
    # Done
    return images

  def _rasterize_scene(self, vertex, focal, triangle):
    """
    Raszerize scene using OpenGL backend
    :param vertex:    Vertex, [Bs, N, 3]
    :param focal:     Focal length [Bs, 1]
    :param triangle:  Triangle, [T, 3]
    :return:  Triangle index map, barycentric coordinates, binary mask
    """
    with tf.name_scope('rasterize'):
      # vertex = tf.debugging.check_numerics(vertex, 'Vertex are not valid')
      vertex_clip = eye_to_clip_op(vertex=vertex,
                                   focal=focal,
                                   near=self._near,
                                   far=self._far,
                                   width=self._width,
                                   height=self._height)
      rasterized = rasterizing_op(width=self._width,
                                  height=self._height,
                                  vertex=vertex_clip,
                                  triangle=triangle,
                                  visible=self._visible)
      # Convert output
      tri_index = tf.cast(rasterized[..., 0] - 1.0, tf.int32)
      bcoords = rasterized[..., 1:]
      mask = tf.cast(tri_index >= 0, tf.int32)
      return tri_index, bcoords, mask

  def _interpolate(self,
                   attribute,
                   barycentric,
                   triangle,
                   tri_index,
                   blend,
                   background,
                   type):
    """
    Interpolate attributes
    :param attribute:     Attributes to interpolate, [B, N, K]
    :param barycentric:   Barycentric coordinates, []
    :param triangle:      List of triangles, [T, 3]
    :param tri_index:     Triangle index at each pixel location
    :param blend:         Binary mask where the rasterized surface live.
    :param background:    Background value
    :param type:          Type of interpolation: Linear or Flat
    :return:  Interpolated attributes [B, H, W, K]
    """
    with tf.name_scope('interpolate'):
      attr = tf.gather(attribute, triangle, axis=-2)
      attribute_per_pixel = tf.gather(attr,
                                      tri_index,
                                      axis=-3,
                                      batch_dims=len(tri_index.shape[:-2]))
      bary = barycentric
      if type == 'flat':
        # Flat interpolation, just pick the largest bcoords and set it to 1.0
        # i.e: (0.2, 0.35, 0.45) --> (0.0, 0.0, 1.0)
        bary_max = tf.reduce_max(bary, -1, keepdims=True)
        bary = tf.greater_equal(bary, bary_max)
        bary = tf.cast(bary, tf.float32)
      interp = tf.expand_dims(bary, axis=-1) * attribute_per_pixel
      interp = tf.reduce_sum(interp, axis=-2)
      return (blend * interp) + ((1.0 - blend) * background)


class DeferredRendererV3(DeferredRendererV2):

  def __init__(self,
               width=200,
               height=200,
               near=1e0,
               far=1e3,
               fixed_triangle=False,
               name='DeferredRenderingV3',
               **kwargs):
    """
    Constructor
    :param width:     Image width
    :param height:    Image height
    :param near:      Near plane
    :param far:       Far plane
    :param fixed_triangle:   If `True` indicates that triangles are shared
      across all rendering pass and therefore can only be uploaded in OpenGL
      buffer once.
    :param name:      Layer's name
    :param kwargs:    Extra keyword arguments
    """
    super(DeferredRendererV3, self).__init__(width=width,
                                             height=height,
                                             near=near,
                                             far=far,
                                             visible=False,
                                             name=name,
                                             **kwargs)
    self.fixed_triangle = fixed_triangle

  def _rasterize_scene(self, vertex, focal, triangle):
    """
    Raszerize scene using OpenGL backend
    :param vertex:    Vertex, [Bs, N, 3]
    :param focal:     Focal length [Bs, 1]
    :param triangle:  Triangle, [T, 3]
    :return:  Triangle index map, barycentric coordinates, binary mask
    """
    with tf.name_scope('rasterize'):
      # vertex = tf.debugging.check_numerics(vertex, 'Vertex are not valid')
      vertex_clip = eye_to_clip_op(vertex=vertex,
                                   focal=focal,
                                   near=self._near,
                                   far=self._far,
                                   width=self._width,
                                   height=self._height)
      rasterized = rasterizing_v2_op(width=self._width,
                                     height=self._height,
                                     vertex=vertex_clip,
                                     triangle=triangle,
                                     fixed_triangle=self.fixed_triangle)
      # Convert output
      tri_index = tf.cast(rasterized[..., 0] - 1.0, tf.int32)
      bcoords = rasterized[..., 1:]
      mask = tf.cast(tri_index >= 0, tf.int32)
      return tri_index, bcoords, mask
