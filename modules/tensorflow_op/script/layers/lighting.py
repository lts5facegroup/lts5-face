# coding=utf-8
""" Custom layer approximating lighting """
import tensorflow as tf
from tensorflow.keras.layers import Layer
from lts5.tensorflow_op.layers.summary import SummaryLayer
from lts5.tensorflow_op.utils.shape import shape_to_int
from lts5.tensorflow_op.probability.prior_evaluator import \
  MultivariateGaussianPrior as MulitGausPrior

__author__ = 'Christophe Ecabert'


def _int_prod(value):
  p = 1
  for v in value:
    p *= v
  return p


def _build_sh_basis(normal):
  """
  Build SH basis
  :param normal:  Surface normals [A0, ..., An, 3]
  :return:  Basis, [A0, ..., An, #Basis]
  """
  nx = normal[..., 0]  # [B, N]
  ny = normal[..., 1]  # [B, N]
  nz = normal[..., 2]  # [B, N]
  ones = tf.ones_like(nx)
  basis = [ones,
           nx, ny, nz,
           nx * ny, nx * nz, ny * nz,
           (nx * nx) - (ny * ny), (3.0 * (nz * nz)) - ones]
  basis = tf.stack(basis, axis=-1)   # [Batch, N, 9]
  return basis


class LightingCoefficientRegularizer(SummaryLayer):
  """ Statistical regularizer for light coefficients """

  def __init__(self,
               illumination_prior_path,
               w_illu=1.0,
               manager=None,
               name='LightingCoefficientRegularizer',
               **kwargs):
    """
    Constructor
    :param illumination_prior_path: Path to illumination prior file
    :param w_illu:  Weight for illumination prior
    :param manager: Summary writer manager
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """

    def _log_fn(inputs, step, training):
      l_illu, w_illu = inputs
      # Add scalars
      self._w_loss.scalar(name='Lillu',
                          data=l_illu,
                          step=step,
                          training=training)
      # Add histogram
      self._w_loss.histogram(name='Willu',
                             data=w_illu,
                             step=step,
                             training=training)
      # Min/max
      illu_min = tf.reduce_min(w_illu)
      illu_max = tf.reduce_max(w_illu)
      self._w_minimum.scalar(name='Willu',
                             data=illu_min,
                             step=step,
                             training=training)
      self._w_maximum.scalar(name='Willu',
                             data=illu_max,
                             step=step,
                             training=training)
      
    super(LightingCoefficientRegularizer, self).__init__(name=name,
                                                         manager=manager,
                                                         logging_fn=_log_fn,
                                                         traces=('loss',
                                                                 'minimum',
                                                                 'maximum'),
                                                         **kwargs)
    # Prior
    self._rate_illu = w_illu
    self._p_illu = None
    if illumination_prior_path is not None:
      self._p_illu = MulitGausPrior.from_file(illumination_prior_path,
                                              normalized=False)

  def call(self, inputs, training=None):
    """
    Compute negative log-likelihood
    :param inputs:  Tensor: w_illu
    :param training:  If `True` indicates training phase, otherwise no.
    :return:  negative log-likelihood
    """
    # Negative log-likelihood
    ll = 0.0
    if self._p_illu is not None:
      ll = self._p_illu.log_value(inputs)
      ll = -tf.reduce_mean(ll) * self._rate_illu
    # Log
    super(LightingCoefficientRegularizer, self).call([ll, inputs], training)
    # Return loss
    return ll


class SHLighting(Layer):
  """ Spherical harmonic lighting in pure TF op """

  def __init__(self,
               illumination_prior_path,
               w_illu=1.0,
               manager=None,
               name='SHLighting',
               **kwargs):
    """
    Constructor
    :param illumination_prior_path: Path to illumination prior file, can be
      `None` if shading is not done at vertex level.
    :param w_illu: Weight for illumination regularization, required if shading
      is done at vertex level.
    :param name:    Op's name
    :param kwargs:  Extra keyword arguments
    """
    super(SHLighting, self).__init__(name=name,
                                     **kwargs)
    self._n_shp = None
    self._basis_flat_shp = None
    # Regularizer
    self.illu_reg = None
    if illumination_prior_path:
      self.illu_reg = LightingCoefficientRegularizer(illumination_prior_path,
                                                     w_illu,
                                                     manager)

  def build(self, input_shape):
    """
    Check that input's dimensions fulfill the requirements
    :param input_shape: Input's dimensions
    """
    # Sanity check
    if not isinstance(input_shape, list) or len(input_shape) != 2:
      raise ValueError('Missing input arguments: [normal, illumination]')
    # Check normals dimensions
    n_shp, w_shp = input_shape
    if n_shp[-1] != 3:
      raise ValueError('Last dimension of `normal` input is not equal to 3,'
                       ' {} != 3'.format(n_shp[-1]))

    if w_shp.rank != 3 or w_shp[1] != 1:
      raise ValueError('Spherical harmonic parameters must have shape:'
                       ' [-1, 1, Ni], got: {}'.format(input_shape[-1][1]))

    if w_shp[-1] != 27:
      raise ValueError('Use spherical harmonics with 3 bands, needs 27 '
                       'illumination parameters, got {}'.format(w_shp[-1]))

    # Define flattening shape
    self._n_shp = shape_to_int(n_shp)
    shp = shape_to_int(n_shp)
    self._basis_flat_shp = [shp[0], _int_prod(shp[1:-1]), 9]

    # Call base class function (mark the layer as build)
    super(SHLighting, self).build(input_shape)

  def call(self, inputs, training=None):
    """
    Forward ops
    :param inputs:  List of inputs [normal, illumination] where `normal` shape
      is [A0, ..., An, 3] and `illumination` shape is [A0, 1, 27]
    :return:  Tensor, shading coefficient [A0, ..., An, 3]
              Tensor, regularization cost
    """
    normal, w_illu = inputs
    w_illu = tf.reshape(w_illu, (-1, 3, 9))           # [A0, 3, 9]
    w_illu = tf.transpose(w_illu, [0, 2, 1])          # [A0, 9, 3]
    # Build SH basis
    sh_basis = _build_sh_basis(normal=normal)         # [A0,  ..., An, 9]
    # Compute scaling
    sh_basis = tf.reshape(sh_basis, self._basis_flat_shp)
    sh_coef = tf.matmul(sh_basis, w_illu)             # [A0, A1 * ... * An, 3]
    sh_coef = tf.reshape(sh_coef, self._n_shp)
    # Sh coef must be positive
    sh_coef = tf.maximum(sh_coef, 0.0)                # [A0, ..., An, 3]
    # Regularization
    l_illu = 0.0
    if self.illu_reg:
      l_illu = self.illu_reg(inputs[1], training)
    return sh_coef, l_illu
