# coding=utf-8
"""
Tensorflow implementation of `PriorBox` layer from caffe SSD.

See:
  https://github.com/weiliu89/caffe/blob/ssd/include/caffe/layers/prior_box_layer.hpp
  https://github.com/rykov8/ssd_keras/blob/master/ssd_layers.py
"""
from tensorflow.keras.layers import Layer
from tensorflow.python.framework.errors import InvalidArgumentError
from tensorflow import pad
from lts5.tensorflow_op import prior_box_op as pbox_op

__author__ = 'Christophe Ecabert'


class PriorBoxConfig:
  """
  Prior box configuration

  Attributes
  ----------
  min_size: list(int)
    Minimum box size (in pixels), can be multiple
  step: int
    Step size.
  aspect_ratio: float
    Various of aspect ratios. Duplicate ratios will be ignored. If none is
    provided, we use default ratio 1.
  variance: list(float)
    Variance for adjusting the prior bboxes
  max_size: list(int)
    Maximum box size in pixels. can be ignored or same as he # of min_size
  flip: bool
    If true, will flip each aspect ratio (i.e. r => 1/r).
  clip: bool
    If true, will clip the prior so that it is within [0, 1]
  offset: float
    Offset to the top left corner of each cell.
  pad: int
    Padding to add to the feature maps, default=0
  """

  def __init__(self,
               min_size: list,
               step: int,
               aspect_ratio=(1.0,),
               variance=(0.1, 0.1, 0.2, 0.2),
               max_size=None,
               flip=True,
               clip=False,
               offset=0.5,
               pad=0):
    """
    Constructor
    :param min_size:      Minimum box size (in pixels), can be multiple
    :param step:          Step size.
    :param aspect_ratio:  Various of aspect ratios. Duplicate ratios will be
                          ignored. If none is provided, we use default ratio 1.
    :param variance:      Variance for adjusting the prior bboxes
    :param max_size:      Maximum box size in pixels. can be ignored or same as
                          the # of min_size
    :param flip:          If true, will flip each aspect ratio (i.e. r => 1/r).
    :param clip:          If true, will clip the prior so that it is
                          within [0, 1]
    :param offset:        Offset to the top left corner of each cell.
    :param pad:           Padding to add to the feature maps
    """
    # Min sizes
    self._min_sizes = []
    for s in min_size:
      assert s > 0, 'min_size must be positive! {} < 0.0'.format(s)
      self._min_sizes.append(s)
    # Flip
    self._flip = flip
    # Aspect ratio
    self._aspect_ratio = [1.0]  # Default
    for ar in aspect_ratio:
      # Already inside ?
      if ar not in self._aspect_ratio:
        self._aspect_ratio.append(ar)
        if self._flip:
          self._aspect_ratio.append(1.0 / ar)
    # Num prior
    self._num_prior = len(self._aspect_ratio) * len(min_size)
    # Max size is present ?
    self._max_sizes = []
    if max_size is not None:
      assert len(min_size) == len(max_size), \
        'Min/Max size list dims does not match'
      for s_max, s_min in (max_size, min_size):
        assert s_max > s_min, \
          'max_size must be greater than min_size ({} < {})'.format(s_max,
                                                                    s_min)
        self._max_sizes.append(s_max)
        self._num_prior += 1
    # Clip
    self._clip = clip
    # Variance
    self._variance = []
    assert len(variance) == 1 or len(variance) == 4, \
      'Must and only provide 1 or 4 variance '
    for v in variance:
      assert v > 0.0, 'Variance must be positive, {} < 0.0'.format(v)
      self._variance.append(v)
    # Step
    self._step = step
    # Offset
    self._offset = offset
    # padding
    self._pad = pad

  @property
  def min_sizes(self):
    return self._min_sizes

  @property
  def step(self):
    return self._step

  @property
  def aspect_ratio(self):
    return self._aspect_ratio

  @property
  def variance(self):
    return self._variance

  @property
  def max_sizes(self):
    return self._max_sizes

  @property
  def flip(self):
    return self._flip

  @property
  def clip(self):
    return self._clip

  @property
  def offset(self):
    return self._offset

  @property
  def pad(self):
    return self._pad


class PriorBox(Layer):
  """
  Generate the prior boxes of designated sizes and aspect ratios across all
  dimensions `H x W`
  """

  def __init__(self,
               config: PriorBoxConfig,
               **kwargs):
    """
    Constructor

    :param kwargs:        Extra keyword arguments
    """
    super(PriorBox, self).__init__(**kwargs)
    self._cfg = config

  def build(self, input_shape):
    """
    Check that input's dimensions fulfill the requirements
    :param input_shape: Input's dimensions
    """
    # Sanity check
    if len(input_shape) != 2:
      raise InvalidArgumentError(node_def=None,
                                 op=None,
                                 message='Missing input arguments: [image'
                                         ', feature_map]')
    # Call base class function (mark the layer as build)
    super(PriorBox, self).build(input_shape)

  def call(self, inputs, **kwargs):
    """
    Forward operation, generate priors
    :param inputs:  Layer inputs [image, feature_map]
    :param kwargs:  Extra keyword arguments
    :return:  Priors
    """
    # pad input if needed
    feature_maps = inputs[1]
    if self._cfg.pad != 0:
      p = self._cfg.pad
      feature_maps = pad(feature_maps, [[0, 0], [p, p], [p, p], [0, 0]])
    return pbox_op(image=inputs[0],
                   features=feature_maps,
                   min_size=self._cfg.min_sizes,
                   aspect_ratio=self._cfg.aspect_ratio,
                   variance=self._cfg.variance,
                   max_size=self._cfg.max_sizes,
                   flip=self._cfg.flip,
                   clip=self._cfg.clip,
                   step=self._cfg.step,
                   offset=self._cfg.offset)

  def get_config(self):
    """
    Provide layer's configuration
    :return:  dict
    """
    cfg = super(PriorBox, self).get_config()
    cfg.update('config', self._cfg)
    return cfg
