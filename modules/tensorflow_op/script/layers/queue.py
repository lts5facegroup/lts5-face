# coding=utf-8
"""
Implement Queue mechanism for Momentum Contrast learning (i.e. MoCo)

See:
  - https://arxiv.org/pdf/1911.05722.pdf
"""
import tensorflow as _tf
from tensorflow.keras.backend import learning_phase
from tensorflow.keras.metrics import SparseTopKCategoricalAccuracy
from lts5.tensorflow_op.layers.summary import SummaryLayer

__author__ = 'Christophe Ecabert'


def _queue_init(shape, dtype):
  return _tf.math.l2_normalize(_tf.random.normal(shape, dtype=dtype), axis=-1)


class MoCoQueue(SummaryLayer):
  """ Implement Momentum Contrast learning queue """

  def __init__(self,
               size: int,
               temperature: float,
               initializer=None,
               normalized: bool = True,
               top_k:int = 5,
               name='MoCoQueue',
               manager=None,
               **kwargs):
    """
    Constructor
    :param size:        Size of the queue. Note, it must be a multiple of the
      batch size.
    :param temperature: Temperature scaling to use in Info-NCE term
    :param initializer: Initializer instance (callable)
    :param normalized:  If `True` indicates that features are normalized. If
      `False` L2 normalization needs to be applied
    :param top_k:       Number of top elements to look at for computing accuracy
    :param name:        Layer's name
    :param manager:     Summary writer manager
    :param kwargs:      Extra keyword arguments
    """

    # Log
    def _log_fn(inputs, step, training):
      self._w_loss.scalar(name='Contrast',
                          data=inputs,
                          step=step,
                          training=training)

    super(MoCoQueue, self).__init__(name=name,
                                    manager=manager,
                                    logging_fn=_log_fn,
                                    traces=('loss', ),
                                    **kwargs)
    # Save info
    self.size = _tf.convert_to_tensor(size, _tf.int64)
    self.temp = _tf.convert_to_tensor(temperature)
    self.norm = _tf.convert_to_tensor(normalized, _tf.bool)
    self.queue = None
    self.queue_idx = None
    self.queue_init = _queue_init
    if initializer is not None:
      self.queue_init = initializer
    # Top-K accuracy metric
    m_name = self.name + '_Top{}Acc'.format(top_k)
    self.k = top_k
    self.top_k = SparseTopKCategoricalAccuracy(k=top_k,
                                               name=m_name)

  def build(self, input_shape):
    """
    Build layer and perform sanity check
    :param input_shape: List of TensorShape: [queries, keys]
    """
    if not isinstance(input_shape, (tuple, list)):
      raise ValueError('Layer requires multiple inputs: [queries, keys]')

    if len(input_shape) != 2:
      raise ValueError('Missing input tensors, requires [queries, keys], {} '
                       '!= 3'.format(len(input_shape)))

    q_shp, k_shp = input_shape
    if q_shp.rank != 2:
      raise ValueError('`Queries` Tensor must be rank 2 with dimensions: '
                       '[Batch, F], got rank {}'.format(q_shp.rank))
    if k_shp.rank != 2:
      raise ValueError('`Keys` Tensor must be rank 2 with dimensions: '
                       '[Batch, F], got rank {}'.format(k_shp.rank))

    if q_shp[-1] != k_shp[-1]:
      raise ValueError('`Queries` and `Keys` must have the same number of '
                       'features, got {} != {}'.format(q_shp[-1],
                                                       k_shp[-1]))
    # Create queue
    self.queue_idx = self.add_weight(name='queue_idx',
                                     dtype=_tf.int64,
                                     initializer='zeros',
                                     trainable=False)
    self.queue = self.add_weight(name='queue',
                                 shape=[self.size, q_shp[-1]],
                                 dtype=_tf.float32,
                                 initializer=self.queue_init,
                                 trainable=False)
    # Done
    super(MoCoQueue, self).build(input_shape)

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:  List of Tensors: [Queries, Keys]
    :param training: If `True` indicates training phase and queue will be
      updated, otherwise not.
    :return loss: Loss value
    """
    # Handle training phase
    if training is None:
      training = learning_phase()

    # Query + Key features
    q_feat, k_feat = inputs
    # Get batch size
    bsize = _tf.shape(q_feat, out_type=_tf.int64)[0]
    # Does features need to be normalized ?
    if not self.norm:
      q_feat = _tf.math.l2_normalize(q_feat, axis=1)
      k_feat = _tf.math.l2_normalize(k_feat, axis=1)
    # Stop gradient propagation through queue
    k_feat = _tf.stop_gradient(k_feat)
    # Similarity between batch of positive sample (query vs key)
    l_pos = _tf.expand_dims(_tf.einsum('nc,nc->n', q_feat, k_feat), 1)  # B x 1
    # Similarity between positive and negative samples (query vs queue)
    l_neg = _tf.einsum('nc,kc->nk', q_feat, self.queue)  # B x C
    # Combine into logits
    logits = _tf.concat([l_pos, l_neg], axis=1)  # B x (1 + C)
    logits_scale = logits / self.temp
    # Build sparse label, positive sample are located in first column
    labels = _tf.zeros(bsize, dtype=_tf.int64)
    loss = _tf.nn.sparse_softmax_cross_entropy_with_logits(labels=labels,
                                                           logits=logits_scale)
    loss = _tf.reduce_mean(loss, name='xentropy-loss')
    # Compute classification accuracy within batch
    self.add_metric(self.top_k(y_true=labels, y_pred=logits))
    # Update queue only during training phase
    if training:
      self._update_queue(k_feat, bsize)
    # Log
    super(MoCoQueue, self).call(loss, training)
    return loss

  def _update_queue(self, item, batch_size):
    """
    Push item into the queue
    :param item:  Feature to push into the queue, [B, C]
    :param batch_size:  Size of the given batch
    """
    queue_end_idx = self.queue_idx + batch_size
    inds = _tf.range(self.queue_idx, queue_end_idx, dtype=_tf.int64) % self.size
    # Update index of the queue
    self.queue_idx.assign(queue_end_idx % self.size)
    # Push features in queue
    slice = _tf.IndexedSlices(values=item, indices=inds)
    self.queue.scatter_update(slice)
    # self.queue.scatter_nd_update(inds, item)
