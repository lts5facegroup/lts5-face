# coding=utf-8
"""
Max pooling/un-pooling layers

See:
  - https://arxiv.org/pdf/1511.00561.pdf
  - https://github.com/ykamikawa/tf-keras-SegNet
"""
from math import ceil
import tensorflow as _tf
from tensorflow.keras.layers import Layer
from tensorflow.keras.layers import AveragePooling2D, ZeroPadding2D

__author__ = 'Christophe Ecabert'


class MaxPoolingWithArgmax2D(Layer):
  """ Max pooling layer with argmax, return pooled value and pooling index """

  def __init__(self,
               pool_size=(2, 2),
               strides=(2, 2),
               padding='same',
               name='MaxPoolingWithArgmax2D',
               **kwargs):
    """
    Constructor
    :param pool_size: Tuple of 2 integers, factors by which to downscale
                      (vertical, horizontal)
    :param strides: Tuple of 2 integers, strides values
    :param padding: One of "valid" or "same" (case-insensitive).
    :param name:    Layer's name (i.e. scope)
    :param kwargs:  Extra keyword arguments
    """
    super(MaxPoolingWithArgmax2D, self).__init__(name=name, **kwargs)
    self.pool_size = pool_size
    self.strides = strides
    self.padding = padding

  def call(self, inputs):
    """
    Forward
    :param inputs:  Tensor to pool
    :return: tuple: pooled value + index
    """
    ksize = [1, self.pool_size[0], self.pool_size[1], 1]
    padding = self.padding.upper()
    strides = [1, self.strides[0], self.strides[1], 1]
    output, argmax = _tf.nn.max_pool_with_argmax(inputs,
                                                 ksize=ksize,
                                                 strides=strides,
                                                 padding=padding)
    return [output, argmax]

  def compute_output_shape(self, input_shape):
    ratio = (1, 2, 2, 1) # Should be function of stride + ksize
    output_shape = [dim // ratio[idx] if dim is not None else None
                    for idx, dim in enumerate(input_shape)]
    output_shape = tuple(output_shape)
    return [output_shape, output_shape]

  def compute_mask(self, inputs, mask=None):
    return 2 * [None]


class MaxUnpooling2D(Layer):
  """ Unpooling layer using maxpooling index (i.e. shared) """

  def __init__(self,
               name='MaxUnpooling2D',
               **kwargs):
    """
    Constructor
    :param size:    Tuple of integers, unpooling factor in x/y direction
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    super(MaxUnpooling2D, self).__init__(name=name, **kwargs)

  def call(self, inputs, output_shape):
    """
    Foward, unpooling
    :param inputs:  List of tensors: Value, Index
    :param output_shape: Dimensions of the unpooled tensor
    :return:  Unpooled tensor
    """
    x, ind = inputs
    ind = _tf.cast(ind, _tf.int32)
    # Input dims
    input_shape = _tf.shape(x)
    # flatten input
    flat_x = _tf.reshape(x, [-1])
    # Batch range
    brange = _tf.range(input_shape[0], dtype=ind.dtype)
    brange = _tf.reshape(brange, [input_shape[0], 1, 1, 1])
    # Prepare index with some magic
    b = _tf.ones_like(ind) * brange
    b = _tf.reshape(b, [-1, 1])
    index = _tf.reshape(ind, [-1, 1])
    index = _tf.concat([b, index], axis=1)
    out_shp_prod = output_shape[1] * output_shape[2] * input_shape[3]
    unpool = _tf.scatter_nd(index,
                            flat_x,
                            shape=[input_shape[0], out_shp_prod])
    # the reason that we use tf.scatter_nd: if we use
    # tf.sparse_tensor_to_dense, then the gradient is None, which will cut off
    # the network. But if we use tf.scatter_nd, the gradients for all the
    # trainable variables will be tensors, instead of None. The usage for
    # tf.scatter_nd is that: create a new tensor by applying sparse
    # UPDATES(which is the pooling value) to individual values of slices
    # within a zero tensor of given shape (FLAT_OUTPUT_SHAPE) according to the
    # indices (index). If we ues the orignal code, the only thing we need to
    # change is: changing from tf.sparse_tensor_to_dense(sparse_tensor) to
    # tf.sparse_add(tf.zeros((output_sahpe)),sparse_tensor) which will give us
    # the gradients!!!
    unpool = _tf.reshape(unpool, [input_shape[0],
                                  output_shape[1],
                                  output_shape[2],
                                  input_shape[3]])
    return unpool


class AdaptiveAvgPooling2D(Layer):
  """ Adaptive Average Pooling layer similar to `nn.AdaptiveAvgPool2d`

  Applies a 2D adaptive average pooling over an input signal composed of several
  input planes. The output is of size H x W, for any input size. The number of
  output features is equal to the number of input planes.
  """

  def __init__(self, sizes, name='AdaptiveAvgPooling2D', **kwargs):
    """
    Constructor
    :param sizes: The target output size of the image of the form H x W. Can be
                  a tuple (H, W) or a single H for a square image H x H
    :param name:  Layer's name (i.e. scope)
    :param kwargs:  Extra keyword arguments
    """
    super(AdaptiveAvgPooling2D, self).__init__(name=name, **kwargs)
    # Size must be tuple of 2
    self.sizes = (sizes, sizes) if isinstance(sizes, int) else sizes
    # Strides + kernel dims
    self.ksize = []
    self.pad_before = []
    self.pad_after = []
    self.avg_pooling = None
    self.padding = None

  def build(self, input_shape):
    """
    Build the layer
    :param input_shape: Dimension of the input tensor
    """
    for sz, inp_sz in zip(self.sizes, input_shape[1:3]):
      # stride == ksize
      #   out = ceil(in + pad) / stride)
      #   ratio = ceil(in / out)
      #   in + pad = ratio * out => pad = (ratio * out) - in
      inp_sz = float(inp_sz)
      out_sz = float(sz)
      pad = (ceil(inp_sz / out_sz) * out_sz) - inp_sz
      pad0 = int(ceil(pad / 2.0))
      pad1 = int(pad - pad0)
      ks = int(ceil((inp_sz + pad) / out_sz))
      self.ksize.append(ks)
      self.pad_before.append(pad0)
      self.pad_after.append(pad1)
    # Build actual avg pooling layer
    self.avg_pooling = AveragePooling2D(pool_size=self.ksize,
                                        strides=self.ksize,
                                        padding='same')
    # Explicit padding
    padding = ((self.pad_before[0], self.pad_after[0]),
               (self.pad_before[1], self.pad_after[1]))
    self.padding = ZeroPadding2D(padding)
    # Done
    super(AdaptiveAvgPooling2D, self).build(input_shape)

  def call(self, inputs):
    """
    Forward
    :param inputs:  Tensor to be pooled
    :return:  Processed tensor
    """
    x = self.padding(inputs)
    return self.avg_pooling(x)
