# coding=utf-8
"""
Implement spectral normalization for tensorflow keras. From
"Spectral Normalization for Generative Adversarial Networks" Miyato et al,
 ICLR 2018

See:
  - https://arxiv.org/abs/1802.05957
  - https://medium.com/@FloydHsiu0618/spectral-normalization-implementation-of-tensorflow-2-0-keras-api-d9060d26de77
"""
import tensorflow as _tf
from tensorflow.keras.layers import Wrapper, Conv2D, Dense

__author__ = 'Christophe Ecabert'


class SpectralNormalization(Wrapper):
  """
  Spectral Normalization

  Attributes:
    layer: tensorflow keras Dense or Conv2D layers (with kernel attribute)

  See:  https://github.com/jason71995/tf2_gan_library
  """

  def __init__(self,
               layer,
               power_iterations=1,
               **kwargs):
    """
    Constructor
    :param layer:   Layer on which apply 'spectral normalization'
    :param power_iterations: Number of power iterations to perform during
                             spectral normalization
    :param kwargs:  Extra keyword arguments
    """
    # Check that the layer being wrapped is either Conv2D or Dense layer
    if not isinstance(layer, (Conv2D, Dense)):
      raise RuntimeError('Layer being wrapped must be of type `Conv2D` or '
                         '`Dense`')
    # Init
    super(SpectralNormalization, self).__init__(layer, **kwargs)
    self.w = None
    self.w_shape_perm = []  # Tranpose permutations
    self.u = None           # Approximated singular value
    self.power_iter = power_iterations

  def build(self, input_shape=None):
    """
    Build `Layer`
    :param input_shape: Dimensions of the input tensor
    """
    if not self.layer.built:
      self.layer.build(input_shape)

      if not hasattr(self.layer, 'kernel'):
        raise ValueError('`SpectralNormalization` must wrap a layer that'
                         ' contains a `kernel` for weights')

      self.w = self.layer.kernel
      if isinstance(self.layer, Dense):
        # Dense layer kernel dimensions are: [n_in, n_units]
        # => [n_units, n_in]
        self.w_shape_perm = [1, 0]
        shp = [1, self.w.shape[1]]   # [1, n_units]
      else:
        # Conv2D kernel dimensions are [f_h, f_w, in_chan, out_chan]
        # => [out_chan, in_chan, f_h, f_w]
        self.w_shape_perm = [3, 2, 0, 1]
        shp = [1, self.w.shape[3]]  # [1, out_chan]
      # Init singular value approximation
      self.u = self.add_weight(shape=shp,
                               initializer=_tf.keras.initializers.RandomNormal(),
                               name='sn_u',
                               trainable=False,
                               dtype=self.w.dtype)
    # Call base function
    super(SpectralNormalization, self).build(input_shape)

  def call(self, inputs, **kwargs):
    """
    Run layer's computation
    :param inputs: Input tensor
    :param kwargs: Extra keyword arguments
    """
    # Recompute weights for each forward pass
    self._spectral_norm()
    output = self.layer(inputs, **kwargs)
    return output

  def _spectral_norm(self):
    """
    Generate normalized weights.
    This method will update the value of self.layer.kernel with the
    normalized value, so that the layer is ready for call().
    """
    w_mat = _tf.transpose(self.w, self.w_shape_perm)
    w_mat = _tf.reshape(w_mat, [_tf.shape(w_mat)[0], -1])

    # Power iteration
    _u = _tf.identity(self.u)
    _v = None
    for _ in range(self.power_iter):
      _v = _tf.math.l2_normalize(_tf.matmul(_u, w_mat))
      _u = _tf.math.l2_normalize(_tf.matmul(_v, w_mat, transpose_b=True))
    # Spectral norm of w
    sigma = _tf.reduce_sum(_tf.matmul(_u, w_mat) * _v)
    sigma = _tf.maximum(sigma, 1e-8)
    self.u.assign(_tf.keras.backend.in_train_phase(_u, self.u))
    # normalize
    self.layer.kernel = self.w / sigma
