# coding=utf-8
"""
Network predicting normals from RGB images, taken from "Robust Learning Through
Cross-Task Consistency" Zamir et al, CVPR2020

See:
 - https://github.com/EPFL-VILAB/XTConsistency
"""
import tensorflow as tf
import tensorflow.keras.layers as kl
from lts5.tensorflow_op.network.network import InputConfigAbstract
from lts5.tensorflow_op.network.network import ImageFormat, InputSpec
from lts5.tensorflow_op.network import FrozenGraph
from lts5.tensorflow_op.image.util import smart_resize


class Rgb2NormalInputConfig(InputConfigAbstract):
  """ Input configuration: 256x256 RGB scaled between [0, 1] """

  def __init__(self):
    """ Constructor """
    super(Rgb2NormalInputConfig, self).__init__([InputSpec(dims=(256, 256, 3),
                                                           fmt=ImageFormat.RGB,
                                                           name='input')])

    def _pre_process_fn(x):
      """  Pre-processing """
      # Resize
      x = smart_resize(tf.cast(x, tf.float32), [256, 256])
      # move to [0, 1] range
      x = x / 255.0
      return x

    def _revert_pre_process_fn(x):
      x = x * 255.0
      return x

    self._preprocess_fn = kl.Lambda(_pre_process_fn,
                                    name='preprocessing')
    self._revert_preprocess_fn = kl.Lambda(_revert_pre_process_fn,
                                           name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn


  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


class Rgb2Normals(FrozenGraph):
  """ Compute surface normals from RGB images """

  def __init__(self, name='Rgb2Images', **kwargs):
    """
    Constructor
    :param name:    Network's name
    :param kwargs:  Extra keyword arguments
    """
    super(Rgb2Normals, self).__init__(tensor_input_names='image:0',
                                      tensor_output_names='normals:0',
                                      config=Rgb2NormalInputConfig(),
                                      name=name,
                                      **kwargs)
