# coding=utf-8
"""
ResNet TensorFlow Keras

See:
  https://github.com/keras-team/keras-applications/blob/master/keras_applications/resnet_common.py
"""
import tensorflow as _tf
import tensorflow.keras.layers as layers
from tensorflow.keras.layers import BatchNormalization as BatchNorm
from lts5.utils.files import get_file
from lts5.tensorflow_op.image.util import smart_resize
from lts5.tensorflow_op.layers import create_layer
from lts5.tensorflow_op.network import InputSpec, InputConfigAbstract
from lts5.tensorflow_op.network import ImageFormat, Network
from lts5.tensorflow_op.utils.shape import broadcast_shape

__author__ = 'Christophe Ecabert'

# ResNet models are taken from:
#  - keras-application: https://github.com/keras-team/keras-applications
#  - classification_models: https://github.com/qubvel/classification_models
WEIGHTS_COLLECTION = [
  # ResNet18
  {'name': 'resnet18',
   'url': 'https://github.com/qubvel/classification_models/releases/download/'
          '0.0.1/resnet18_imagenet_1000.h5',
   'include_top': True,
   'md5': '64da73012bb70e16c901316c201d9803'},
  # ResNet18 - No top
  {'name': 'resnet18',
   'url': 'https://github.com/qubvel/classification_models/releases/download/'
          '0.0.1/resnet18_imagenet_1000_no_top.h5',
   'include_top': False,
   'md5': '318e3ac0cd98d51e917526c9f62f0b50'},
  # ResNet34
  {'name': 'resnet34',
   'url': 'https://github.com/qubvel/classification_models/releases/download/'
          '0.0.1/resnet34_imagenet_1000.h5',
   'include_top': True,
   'md5': '2ac8277412f65e5d047f255bcbd10383'},
  # ResNet34 - No top
  {'name': 'resnet34',
   'url': 'https://github.com/qubvel/classification_models/releases/download/'
          '0.0.1/resnet34_imagenet_1000_no_top.h5',
   'include_top': False,
   'md5': '8caaa0ad39d927cb8ba5385bf945d582'},
  # ResNet50
  {'name': 'resnet50',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet50_weights_tf_dim_ordering_tf_kernels.h5',
   'include_top': True,
   'md5': '2cb95161c43110f7111970584f804107'},
  # ResNet50 - No top
  {'name': 'resnet50',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5',
   'include_top': False,
   'md5': '4d473c1dd8becc155b73f8504c6f6626'},
  # ResNet50v2
  {'name': 'resnet50v2',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet50v2_weights_tf_dim_ordering_tf_kernels.h5',
   'include_top': True,
   'md5': '3ef43a0b657b3be2300d5770ece849e0'},
  # ResNet50v2 - No top
  {'name': 'resnet50v2',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet50v2_weights_tf_dim_ordering_tf_kernels_notop.h5',
   'include_top': False,
   'md5': 'fac2f116257151a9d068a22e544a4917'},
  # ResNet101
  {'name': 'resnet101',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet101_weights_tf_dim_ordering_tf_kernels.h5',
   'include_top': True,
   'md5': 'f1aeb4b969a6efcfb50fad2f0c20cfc5'},
  # ResNet101 - No top
  {'name': 'resnet101',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet101_weights_tf_dim_ordering_tf_kernels_notop.h5',
   'include_top': False,
   'md5': '88cf7a10940856eca736dc7b7e228a21'},
  # ResNet101v2
  {'name': 'resnet101v2',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet101v2_weights_tf_dim_ordering_tf_kernels.h5',
   'include_top': True,
   'md5': '6343647c601c52e1368623803854d971'},
  # ResNet101v2 - No top
  {'name': 'resnet101v2',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet101v2_weights_tf_dim_ordering_tf_kernels_notop.h5',
   'include_top': False,
   'md5': 'c0ed64b8031c3730f411d2eb4eea35b5'},
  # ResNet152
  {'name': 'resnet152',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet152_weights_tf_dim_ordering_tf_kernels.h5',
   'include_top': True,
   'md5': '100835be76be38e30d865e96f2aaae62'},
  # ResNet152 - No top
  {'name': 'resnet152',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet152_weights_tf_dim_ordering_tf_kernels_notop.h5',
   'include_top': False,
   'md5': 'ee4c566cf9a93f14d82f913c2dc6dd0c'},
  # ResNet152v2
  {'name': 'resnet152v2',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet152v2_weights_tf_dim_ordering_tf_kernels.h5',
   'include_top': True,
   'md5': 'a49b44d1979771252814e80f8ec446f9'},
  # ResNet152v2 - No top
  {'name': 'resnet152v2',
   'url': 'https://github.com/keras-team/keras-applications/releases/download/'
          'resnet/resnet152v2_weights_tf_dim_ordering_tf_kernels_notop.h5',
   'include_top': False,
   'md5': 'ed17cf2e0169df9d443503ef94b23b33'}
]

# {name}_weights_tf_dim_ordering_tf_kernels.h5
# {name}_weights_tf_dim_ordering_tf_kernels_notop.h5
WEIGHTS_HASHES = {
  'resnext50': ('67a5b30d522ed92f75a1f16eef299d1a',     # Top
                '62527c363bdd9ec598bed41947b379fc'),    # No-top
  'resnext101': ('34fb605428fcc7aa4d62f44404c11509',
                 '0f678c91647380debd923963594981b3')
}


def basic_block_small_v1(inputs,
                         scales,
                         n_filters,
                         kernel_size=3,
                         stride=1,
                         cut='pre',
                         name=None,
                         **kwargs):
  """
  A Residual Block for ResNet v1.0
  :param inputs:        Input, [N, H, W, C]
  :param scales:        Scaling factor applied on the end of residual branch,
    [N,] (Optional)
  :param n_filters:     Number of filters to use in the bottleneck
  :param kernel_size:   Kernel size
  :param stride:        Kernel stride
  :param cut:           One of 'pre', 'post'. used to decide where skip
                        connection is taken
  :param name:          Block label.
  :param kwargs:        Extra keyword arguments
  :keyword layer_params:  Set of extra parameters for the underlying layers.
                          Expected format:
                          {type, dict()}
  :return:              Processed data
  """

  # Not used here, remove to avoid TypeError: ('Keyword argument not
  # understood:', 'conv_shortcut') in layer construction
  kwargs.pop('conv_shortcut', None)
  # Build block
  x = create_layer(BatchNorm,
                   name=name + '_bn1',
                   epsilon=2e-5,
                   **kwargs)(inputs)
  x = create_layer(layers.Activation,
                   name=name + '_relu1',
                   activation='relu',
                   **kwargs)(x)
  # defining shortcut connection
  if cut == 'pre':
    shortcut = inputs
  elif cut == 'post':
    shortcut = create_layer(layers.Conv2D,
                            name=name + '_sc',
                            filters=n_filters,
                            kernel_size=(1, 1),
                            strides=stride,
                            use_bias=False,
                            padding='valid',
                            **kwargs)(x)
  else:
    raise ValueError('Cut type not in ["pre", "post"]')

  # continue with convolution layers
  x = create_layer(layers.ZeroPadding2D,
                   name=name + '_pad1',
                   padding=(1, 1),
                   **kwargs)(x)
  x = create_layer(layers.Conv2D,
                   name=name + '_conv1',
                   filters=n_filters,
                   kernel_size=kernel_size,
                   strides=stride,
                   use_bias=False,
                   padding='valid',
                   **kwargs)(x)
  x = create_layer(BatchNorm,
                   name=name + '_bn2',
                   epsilon=2e-5,
                   **kwargs)(x)
  x = create_layer(layers.Activation,
                   name=name + '_relu2',
                   activation='relu',
                   **kwargs)(x)
  x = create_layer(layers.ZeroPadding2D,
                   name=name + '_pad2',
                   padding=(1, 1),
                   **kwargs)(x)
  x = create_layer(layers.Conv2D,
                   name=name + '_conv2',
                   filters=n_filters,
                   kernel_size=kernel_size,
                   use_bias=False,
                   padding='valid',
                   **kwargs)(x)
  if scales is not None:
    x, scales = broadcast_shape(x, scales)
    x = create_layer(layers.Multiply,
                     name=name + '_scale',
                     **kwargs)([x, scales])
  # add residual connection
  x = create_layer(layers.Add,
                   name=name + '_add',
                   **kwargs)([x, shortcut])
  return x


def basic_block_v1(x,
                   scales,
                   n_filters,
                   kernel_size=3,
                   stride=1,
                   conv_shortcut=True,
                   name=None,
                   **kwargs):
  """
   A Residual Block for ResNet v1.0

      --+--Conv-BN-Relu--Conv-BN-Relu--Conv-BN -- (s) --+-- Relu -->
        |										                            |
        +-------------- (Conv-BN) or Id ----------------+

  :param x:               Input, [N, H, W, C]
  :param scales:        Scaling factor applied on the end of residual branch,
    [N,] (Optional)
  :param n_filters:       Number of filters of the bottleneck layer.
  :param kernel_size:     Kernel size of the bottleneck layer, default 3.
  :param stride:          Stride of the first layer, default 1
  :param conv_shortcut:   Use convolution shortcut if True, otherwise identity
                          shortcut, default `True`
  :param name:            Block label.
  :param kwargs:          Extra keyword arguments
  :keyword layer_params:  Set of extra parameters for the underlying layers.
                          Expected format:
                          {type, dict()}
  :return:                Processed input
  """
  kwargs.pop('cut', None)
  # Create shortcut path
  if conv_shortcut:
    shortcut = create_layer(layers.Conv2D,
                            name=name + '_0_conv',
                            filters=4 * n_filters,
                            kernel_size=1,
                            strides=stride,
                            **kwargs)(x)
    shortcut = create_layer(BatchNorm,
                            name=name + '_0_bn',
                            epsilon=1.001e-5,
                            **kwargs)(shortcut)
  else:
    shortcut = x
  # Residual path
  x = create_layer(layers.Conv2D,
                   name=name + '_1_conv',
                   filters=n_filters,
                   kernel_size=1,
                   strides=stride,
                   **kwargs)(x)
  x = create_layer(BatchNorm,
                   name=name + '_1_bn',
                   epsilon=1.001e-5,
                   **kwargs)(x)
  x = create_layer(layers.Activation,
                   activation='relu',
                   name=name + '_1_relu',
                   **kwargs)(x)
  x = create_layer(layers.Conv2D,
                   name=name + '_2_conv',
                   filters=n_filters,
                   kernel_size=kernel_size,
                   padding='SAME',
                   **kwargs)(x)
  x = create_layer(BatchNorm,
                   name=name + '_2_bn',
                   epsilon=1.001e-5,
                   **kwargs)(x)
  x = create_layer(layers.Activation,
                   name=name + '_2_relu',
                   activation='relu',
                   **kwargs)(x)
  x = create_layer(layers.Conv2D,
                   name=name + '_3_conv',
                   filters=4 * n_filters,
                   kernel_size=1,
                   **kwargs)(x)
  x = create_layer(BatchNorm,
                   name=name + '_3_bn',
                   epsilon=1.001e-5,
                   **kwargs)(x)
  if scales is not None:
    x, scales = broadcast_shape(x, scales)
    x = create_layer(layers.Multiply,
                     name=name + '_scale',
                     **kwargs)([x, scales])
  # Merge
  x = create_layer(layers.Add, name=name + '_add', **kwargs)([shortcut, x])
  x = create_layer(layers.Activation,
                   name=name + '_out',
                   activation='relu',
                   **kwargs)(x)
  return x


def basic_block_v2(x,
                   scales,
                   n_filters,
                   kernel_size=3,
                   stride=1,
                   conv_shortcut=True,
                   name=None,
                   **kwargs):
  """
  A Residual Block for ResNet v2.0

    --+--BN-Relu--Conv-BN-Relu--Pad--Conv-BN-Relu--Conv--(s)--+-->
      |                                                       |
      +----------------- (Conv) or MaxPool -------------------+

  :param x:             Input, [N, H, W, C]
  :param scales:        Scaling factor applied on the end of residual branch,
    [N,] (Optional)
  :param n_filters:     Number of filters of the bottleneck layer.
  :param kernel_size:   Kernel size of the bottleneck layer, default 3.
  :param stride:        Stride of the first layer, default 1
  :param conv_shortcut: Use convolution shortcut if True, otherwise identity
                          shortcut, default `True`
  :param name:          Block label.
  :param kwargs:        Extra keyword arguments
  :return:  Processed input
  """
  kwargs.pop('cut', None)
  # Pre-activation
  preact = create_layer(BatchNorm,
                        name=name + '_preact_bn',
                        epsilon=1.001e-5,
                        **kwargs)(x)
  preact = create_layer(layers.Activation,
                        name=name + '_preact_relu',
                        activation='relu',
                        **kwargs)(preact)
  # Shortcut
  if conv_shortcut:
    shortcut = create_layer(layers.Conv2D,
                            name=name + '_0_conv',
                            filters=4 * n_filters,
                            kernel_size=1,
                            strides=stride,
                            **kwargs)(preact)
  else:
    shortcut = create_layer(layers.MaxPool2D,
                            name=name + 'pool',
                            pool_size=1,
                            strides=stride,
                            **kwargs)(x) if stride > 1 else x
  # Bottleneck
  x = create_layer(layers.Conv2D,
                   name=name + '_1_conv',
                   filters=n_filters,
                   kernel_size=1,
                   strides=1,
                   use_bias=False,
                   **kwargs)(preact)
  x = create_layer(BatchNorm,
                   name=name + '_1_bn',
                   epsilon=1.001e-5,
                   **kwargs)(x)
  x = create_layer(layers.Activation,
                   name=name + '_1_relu',
                   activation='relu',
                   **kwargs)(x)
  x = create_layer(layers.ZeroPadding2D,
                   name=name + '_2_pad',
                   padding=((1, 1), (1, 1)),
                   **kwargs)(x)
  x = create_layer(layers.Conv2D,
                   name=name + '_2_conv',
                   filters=n_filters,
                   kernel_size=kernel_size,
                   strides=stride,
                   use_bias=False,
                   **kwargs)(x)
  x = create_layer(BatchNorm,
                   name=name + '_2_bn',
                   epsilon=1.001e-5,
                   **kwargs)(x)
  x = create_layer(layers.Activation,
                   name=name + '_2_relu',
                   activation='relu',
                   **kwargs)(x)
  x = create_layer(layers.Conv2D,
                   name=name + '_3_conv',
                   filters=4 * n_filters,
                   kernel_size=1,
                   **kwargs)(x)
  if scales is not None:
    x, scales = broadcast_shape(x, scales)
    x = create_layer(layers.Multiply,
                     name=name + '_scale',
                     **kwargs)([x, scales])
  # Merge branch
  x = create_layer(layers.Add,
                   name=name + '_out',
                   **kwargs)([shortcut, x])
  return x


def blocks_fn(x,
              scales,
              block_type,
              block_idx: int,
              n_filters: int,
              n_blocks: int,
              stride: int,
              name: str,
              **kwargs):
  """
  Stack residual blocks

  :param x:           Input, [N, H, W, C]
  :param scales       Scaling factor applied on each ResBlock, [#ResBlock, N]
    (Optional).
  :param block_idx:   Number of ResBlock already instantiated before, must be
    smaller than total #ResBlock
  :param block_type:  Type of block to stack (i.e. basic_block_v{1, 2, 3})
  :param n_filters:   Number of filters in a block
  :param n_blocks:    Number of block to be stacked
  :param stride:      Stride of the first layer in the first block
  :param name:        Stack label
  :param kwargs:      Extra keyword arguments
  :keyword groups:    Group size for grouped convolution. (i.e. ResNext)
  :return:            Processed input
  """
  s0 = scales[block_idx, :] if scales is not None else scales
  x = block_type(x,
                 s0,
                 n_filters=n_filters,
                 stride=stride,
                 conv_shortcut=True,
                 cut='post',
                 name=name + '_block1',
                 **kwargs)
  for k in range(2, n_blocks + 1):
    bck_idx = block_idx + k - 1
    sk = scales[bck_idx, :] if scales is not None else scales
    x = block_type(x,
                   sk,
                   n_filters=n_filters,
                   conv_shortcut=False,
                   cut='pre',
                   name=name + '_block{}'.format(k),
                   **kwargs)
  return x


def _wrap_input_if_needed(inputs,
                          dims,
                          dtype,
                          name):
  """
  Wrap an input tensor into keras compliant tensor or create a new kl.Input() if
  `inputs` is None.
  :param inputs:  Tensor to wrap or None if a new input node is needed
  :param dims: Input dimensions if new input is needed
  :param name
  :return:  Tensor
  """
  if inputs is None:
    input_t = layers.Input(shape=dims, dtype=dtype, name=name)
  else:
    if not _tf.keras.backend.is_keras_tensor(inputs):
      # Wrapper tensor `inputs` into input layer
      input_t = layers.Input(tensor=inputs,
                             shape=inputs.shape,
                             dtype=inputs.dtype,
                             name=name)
    else:
      input_t = inputs
  return input_t


class ResNetSmallInputConfig(InputConfigAbstract):
  """ Input configuration for ResNet arch """

  def __init__(self):
    """ Constructor """
    super(ResNetSmallInputConfig, self).__init__([InputSpec(dims=(224, 224, 3),
                                                            fmt=ImageFormat.RGB,
                                                            name='input')])

    def _pre_process_fn(x):
      # Take only first 3 channels
      return smart_resize(x[..., :3], [224, 224])

    def _revert_fn(x):
      return x

    self._preprocess_fn = layers.Lambda(_pre_process_fn,
                                        name='preprocessing')
    self._revert_preprocess_fn = layers.Lambda(_revert_fn,
                                               name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


class ResNetInputConfig(InputConfigAbstract):
  """ Input configuration for ResNet arch """

  def __init__(self):
    """ Constructor """
    super(ResNetInputConfig, self).__init__([InputSpec(dims=(224, 224, 3),
                                                       fmt=ImageFormat.RGB,
                                                       name='input')])

    def _pre_process_fn(x):
      # Resize to required dims
      x = smart_resize(x[..., :3], [224, 224])
      # RGB -> BGR
      x = x[..., ::-1]
      # Zero-center by mean pixel
      x -= _tf.convert_to_tensor([103.939, 116.779, 123.68], dtype=_tf.float32)
      # Done
      return x

    def _revert_fn(x):
      x += _tf.convert_to_tensor([103.939, 116.779, 123.68], dtype=_tf.float32)
      x = _tf.reverse(x, axis=[-1])
      return x

    self._preprocess_fn = layers.Lambda(_pre_process_fn, name='preprocessing')
    self._revert__preprocess_fn = layers.Lambda(_revert_fn,
                                                name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert__preprocess_fn


class ResNetV2InputConfig(InputConfigAbstract):
  """ Input configuration for ResNet arch """

  def __init__(self):
    """ Constructor """
    super(ResNetV2InputConfig, self).__init__([InputSpec(dims=(224, 224, 3),
                                                         fmt=ImageFormat.RGB,
                                                         name='input')])

    def _pre_process_fn(x):
      x = smart_resize(x[..., :3], [224, 224])
      x /= 127.5
      x -= 1.
      return x

    def _revert_fn(x):
      x = x + 1.0
      x = x * 127.5
      return x

    self._preprocess_fn = layers.Lambda(_pre_process_fn, name='preprocessing')
    self._revert_preprocess_fn = layers.Lambda(_revert_fn,
                                               name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


class ResNet(Network):
  """
  Generic ResNet-base architecture

  Available arch:
    - ResNet
    - ResNetV2
    - ResNeXt
   """

  def __init__(self,
               input_tensor,
               scale_tensor,
               block_cfg,
               model_cfg,
               use_preactivation: bool,
               use_bias: bool,
               include_top: bool,
               pooling: str,
               classes: int,
               n_resolution: int,
               name: str,
               **kwargs):
    """
    Constructor
    :param input_tensor:      optional tensor (i.e. Input())
    :param scale_tensor:      optional tensor (i.e. Input())
    :param block_cfg:         Residual blocks configurations
    :param model_cfg:         Network configuration (Input dims + preprocess)
    :param use_preactivation: If `True` uses pre-activation, otherwise no.
                              (True for ResNetV2, False for ResNet and ResNeXt)
    :param use_bias:      Whether to use biases for convolutional layers or not.
                          (True for ResNet and ResNetV2, False for ResNeXt)
    :param include_top:   Whether to include the fully-connected layer at the
                          top of the network.
    :param pooling:       Optional pooling mode for feature extraction when
                          `include_top` is `False`.
                          - `None` means that the output of the model will be
                            the 4D tensor output of the last convolutional
                            layer.
                          - `avg` means that global average pooling will be
                            applied to the output of the last convolutional
                            layer, and thus the output of the model will be a
                            2D tensor.
                          - `max` means that global max pooling will be applied.
    :param classes:       optional number of classes to classify images into,
                          only to be specified if `include_top` is True, and if
                          no pretrained model is used
    :param n_resolution   Number of image resolution to use. If set to None,
                          disable the feature (optional)
    :param name:          Model name.
    :param kwargs:        Extra keyword arguments
    :keyword layer_params:  Set of extra parameters for the underlying layers.
                            Expected format:
                            {type, dict()}
    """

    self.block_cfg = block_cfg
    self.use_preactivation = use_preactivation
    self.use_bias = use_bias
    self.include_top = include_top
    self.pooling = pooling
    self.n_classes = classes
    self.n_resolution = n_resolution

    if scale_tensor is not None and n_resolution is None:
      # Feature scaling disable but still have provided an input -> K.O
      raise ValueError('Provided an `scale_tensor` input but resolution aware '
                       'feature scaling is disabled. Enable the feature or'
                       ' remove the unwanted input!')

    # Initialize feature scaling factor for each resolution
    self.resolution_scales = None
    if n_resolution is not None:
      n_blk = sum([cfg['n_blocks'] for cfg in self.block_cfg])
      shp = (n_blk, n_resolution)
      init_v = _tf.ones(shp, dtype=_tf.float32)
      self.resolution_scales = _tf.Variable(initial_value=init_v,
                                            shape=shp,
                                            name='resolution_scale')

    # Wraps Input
    input_t = _wrap_input_if_needed(input_tensor,
                                    model_cfg.inputs_spec[0].dims,
                                    _tf.float32,
                                    name='input_' + name)
    if self.resolution_scales is not None:
      scale_t = _wrap_input_if_needed(scale_tensor,
                                      (),
                                      _tf.int32,
                                      name='input_scale')

    x = input_t
    # Duct tape fixing for ResNet18/34
    if name in ['resnet18', 'resnet34']:
      x = create_layer(BatchNorm,
                       name='bn_data',
                       scale=False,
                       epsilon=2e-5,
                       **kwargs)(x)
    # Padding + conv1
    x = create_layer(layers.ZeroPadding2D,
                     name='conv1_pad',
                     padding=((3, 3), (3, 3)),
                     **kwargs)(x)
    x = create_layer(layers.Conv2D,
                     name='conv1_conv',
                     filters=64,
                     kernel_size=7,
                     strides=2,
                     use_bias=self.use_bias,
                     **kwargs)(x)
    # Batch norm + activation
    if not self.use_preactivation:
      x = create_layer(BatchNorm,
                       name='conv1_bn',
                       epsilon=1.001e-5,
                       **kwargs)(x)
      x = create_layer(layers.Activation,
                       name='conv1_relu',
                       activation='relu',
                       **kwargs)(x)
    # Padding + pool1
    x = create_layer(layers.ZeroPadding2D,
                     name='pool1_pad',
                     padding=((1, 1), (1, 1)),
                     **kwargs)(x)
    x = create_layer(layers.MaxPool2D,
                     name='pool1_pool',
                     pool_size=3,
                     strides=2,
                     **kwargs)(x)

    # Add scaling factor selection
    scales = None
    if self.resolution_scales is not None:
      scales = _tf.gather(self.resolution_scales,
                          indices=scale_t,
                          axis=1,
                          name='scale_selection')
    # Residual blocks - stack
    block_cnt = 0
    for bck_cfg in self.block_cfg:
      x = blocks_fn(x,
                    scales=scales,
                    block_idx=block_cnt,
                    **bck_cfg,
                    **kwargs)
      block_cnt += bck_cfg['n_blocks']

    # Batch norm + activation
    # fixing: https://github.com/qubvel/classification_models/issues/28
    if self.use_preactivation or name in ['resnet18', 'resnet34']:
      x = create_layer(BatchNorm,
                       name='post_bn',
                       epsilon=1.001e-5,
                       **kwargs)(x)
      x = create_layer(layers.Activation,
                       name='post_relu',
                       activation='relu',
                       **kwargs)(x)

    # Top or pooling ?
    if self.include_top:
      x = create_layer(layers.GlobalAveragePooling2D,
                       name='avg_pool',
                       **kwargs)(x)
      x = create_layer(layers.Dense,
                       name='probs',
                       units=self.n_classes,
                       activation='softmax',
                       **kwargs)(x)
    else:
      if self.pooling == 'avg':
        x = create_layer(layers.GlobalAveragePooling2D,
                         name='avg_pool',
                         **kwargs)(x)
      elif pooling == 'max':
        x = create_layer(layers.GlobalMaxPooling2D,
                         name='max_pool',
                         **kwargs)(x)

    # Ensure that the model takes into account any potential predecessors of
    # `input_tensor`.
    if input_tensor is not None:
      inputs = _tf.keras.utils.get_source_inputs(input_tensor)
    else:
      inputs = [input_t]
    # Has `scale` as input as well ?
    if self.resolution_scales is not None:
      if scale_tensor is not None:
        scale_inputs = _tf.keras.utils.get_source_inputs(scale_tensor)
      else:
        scale_inputs = [scale_t]
      inputs += scale_inputs


    kwargs.pop('layer_params', None)
    super(ResNet, self).__init__(model_cfg,   # config
                                 inputs,      # args1
                                 x,           # args2
                                 name=name,   # name
                                 **kwargs)    # kwargs

  @classmethod
  def ResNet18(cls,
               input_tensor=None,
               scale_tensor=None,
               include_top=True,
               pooling=None,
               classes=1000,
               n_resolution=None,
               outer_scope=None,
               **kwargs):
    """
    Create a ResNet18 v1.0 network
    :param input_tensor: optional tensor (i.e. Input())
    :param scale_tensor: optional tensor (i.e. Input())
    :param include_top:   If `True` add the classification layers
    :param pooling:       Type of pooling to use if top layers are not included
    :param classes:       Number of predicated class if top layres are included
    :param n_resolution   Number of image resolution to use. If set to None,
                          disable the feature (optional)
    :param outer_scope:   Outer scope's name
    :param kwargs:        Extra keyword arguments
    :keyword layer_params:  Set of extra parameters for the underlying layers.
                          Expected format:
                          {type, dict()}
    return Network
    """
    stack_cfg = [{'block_type': basic_block_small_v1,
                  'n_filters': 64,
                  'n_blocks': 2,
                  'stride': 1,
                  'name': 'stage1'},
                 {'block_type': basic_block_small_v1,
                  'n_filters': 128,
                  'n_blocks': 2,
                  'stride': 2,
                  'name': 'stage2'},
                 {'block_type': basic_block_small_v1,
                  'n_filters': 256,
                  'n_blocks': 2,
                  'stride': 2,
                  'name': 'stage3'},
                 {'block_type': basic_block_small_v1,
                  'n_filters': 512,
                  'n_blocks': 2,
                  'stride': 2,
                  'name': 'stage4'}]
    scp = outer_scope + '/' if outer_scope else ''
    scp += 'resnet18'
    return cls(input_tensor=input_tensor,
               scale_tensor=scale_tensor,
               block_cfg=stack_cfg,
               model_cfg=ResNetSmallInputConfig(),
               use_preactivation=False,
               use_bias=False,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               n_resolution=n_resolution,
               name=scp,
               **kwargs)

  @classmethod
  def ResNet34(cls,
               input_tensor=None,
               scale_tensor=None,
               include_top=True,
               pooling=None,
               classes=1000,
               n_resolution=None,
               outer_scope=None,
               **kwargs):
    """
    Create a ResNet34 v1.0 network
    :param input_tensor: optional tensor (i.e. Input())
    :param scale_tensor: optional tensor (i.e. Input())
    :param include_top:   If `True` add the classification layers
    :param pooling:       Type of pooling to use if top layers are not included
    :param classes:       Number of predicated class if top layres are included
    :param n_resolution   Number of image resolution to use. If set to None,
                          disable the feature (optional)
    :param outer_scope:   Outer scope's name
    :param kwargs:        Extra keyword arguments
    :keyword layer_params:  Set of extra parameters for the underlying layers.
                          Expected format:
                          {type, dict()}
    :return:  Network
    """
    stack_cfg = [{'block_type': basic_block_small_v1,
                  'n_filters': 64,
                  'n_blocks': 3,
                  'stride': 1,
                  'name': 'stage1'},
                 {'block_type': basic_block_small_v1,
                  'n_filters': 128,
                  'n_blocks': 4,
                  'stride': 2,
                  'name': 'stage2'},
                 {'block_type': basic_block_small_v1,
                  'n_filters': 256,
                  'n_blocks': 6,
                  'stride': 2,
                  'name': 'stage3'},
                 {'block_type': basic_block_small_v1,
                  'n_filters': 512,
                  'n_blocks': 3,
                  'stride': 2,
                  'name': 'stage4'}]
    scp = outer_scope + '/' if outer_scope else ''
    scp += 'resnet34'
    return cls(input_tensor=input_tensor,
               scale_tensor=scale_tensor,
               block_cfg=stack_cfg,
               model_cfg=ResNetSmallInputConfig(),
               use_preactivation=False,
               use_bias=False,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               n_resolution=n_resolution,
               name=scp,
               **kwargs)

  @classmethod
  def ResNet50(cls,
               input_tensor=None,
               scale_tensor=None,
               include_top=True,
               pooling=None,
               classes=1000,
               n_resolution=None,
               outer_scope=None,
               **kwargs):
    """
    Create a ResNet50 v1.0 network
    :param input_tensor: optional tensor (i.e. Input())
    :param scale_tensor: optional tensor (i.e. Input())
    :param include_top:   If `True` add the classification layers
    :param pooling:       Type of pooling to use if top layers are not included
    :param classes:       Number of predicated class if top layres are included
    :param n_resolution   Number of image resolution to use. If set to None,
                          disable the feature (optional)
    :param outer_scope:   Outer scope's name
    :param kwargs:        Extra keyword arguments
    :keyword layer_params:  Set of extra parameters for the underlying layers.
                          Expected format:
                          {type, dict()}
    return Network
    """
    stack_cfg = [{'block_type': basic_block_v1,
                  'n_filters': 64,
                  'n_blocks': 3,
                  'stride': 1,
                  'name': 'conv2'},
                 {'block_type': basic_block_v1,
                  'n_filters': 128,
                  'n_blocks': 4,
                  'stride': 2,
                  'name': 'conv3'},
                 {'block_type': basic_block_v1,
                  'n_filters': 256,
                  'n_blocks': 6,
                  'stride': 2,
                  'name': 'conv4'},
                 {'block_type': basic_block_v1,
                  'n_filters': 512,
                  'n_blocks': 3,
                  'stride': 2,
                  'name': 'conv5'}]
    scp = outer_scope + '/' if outer_scope else ''
    scp += 'resnet50'
    return cls(input_tensor=input_tensor,
               scale_tensor=scale_tensor,
               block_cfg=stack_cfg,
               model_cfg=ResNetInputConfig(),
               use_preactivation=False,
               use_bias=True,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               n_resolution=n_resolution,
               name=scp,
               **kwargs)

  @classmethod
  def ResNet101(cls,
                input_tensor=None,
                scale_tensor=None,
                include_top=True,
                pooling=None,
                classes=1000,
                n_resolution=None,
                outer_scope=None,
                **kwargs):
    """
    Create a ResNet101 v1.0 network
    :param input_tensor: optional tensor (i.e. Input())
    :param scale_tensor: optional tensor (i.e. Input())
    :param include_top:   If `True` add the classification layers
    :param pooling:       Type of pooling to use if top layers are not included
    :param classes:       Number of predicated class if top layres are included
    :param n_resolution   Number of image resolution to use. If set to None,
                          disable the feature (optional)
    :param outer_scope:   Outer scope's name
    :param kwargs:        Extra keyword arguments
    :keyword layer_params:  Set of extra parameters for the underlying layers.
                          Expected format:
                          {type, dict()}
    return Network
    """
    stack_cfg = [{'block_type': basic_block_v1,
                  'n_filters': 64,
                  'n_blocks': 3,
                  'stride': 1,
                  'name': 'conv2'},
                 {'block_type': basic_block_v1,
                  'n_filters': 128,
                  'n_blocks': 4,
                  'stride': 2,
                  'name': 'conv3'},
                 {'block_type': basic_block_v1,
                  'n_filters': 256,
                  'n_blocks': 23,
                  'stride': 2,
                  'name': 'conv4'},
                 {'block_type': basic_block_v1,
                  'n_filters': 512,
                  'n_blocks': 3,
                  'stride': 2,
                  'name': 'conv5'}]
    scp = outer_scope + '/' if outer_scope else ''
    scp += 'resnet101'
    return cls(input_tensor=input_tensor,
               scale_tensor=scale_tensor,
               block_cfg=stack_cfg,
               model_cfg=ResNetInputConfig(),
               use_preactivation=False,
               use_bias=True,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               n_resolution=n_resolution,
               name=scp,
               **kwargs)

  @classmethod
  def ResNet152(cls,
                input_tensor=None,
                scale_tensor=None,
                include_top=True,
                pooling=None,
                classes=1000,
                n_resolution=None,
                outer_scope=None,
                **kwargs):
    """
    Create a ResNet152 v1.0 network
    :param input_tensor: optional tensor (i.e. Input())
    :param scale_tensor: optional tensor (i.e. Input())
    :param include_top:   If `True` add the classification layers
    :param pooling:       Type of pooling to use if top layers are not included
    :param classes:       Number of predicated class if top layres are included
    :param n_resolution   Number of image resolution to use. If set to None,
                          disable the feature (optional)
    :param outer_scope:   Outer scope's name
    :param kwargs:        Extra keyword arguments
    :keyword layer_params:  Set of extra parameters for the underlying layers.
                          Expected format:
                          {type, dict()}
    return Network
    """
    stack_cfg = [{'block_type': basic_block_v1,
                  'n_filters': 64,
                  'n_blocks': 3,
                  'stride': 1,
                  'name': 'conv2'},
                 {'block_type': basic_block_v1,
                  'n_filters': 128,
                  'n_blocks': 8,
                  'stride': 2,
                  'name': 'conv3'},
                 {'block_type': basic_block_v1,
                  'n_filters': 256,
                  'n_blocks': 36,
                  'stride': 2,
                  'name': 'conv4'},
                 {'block_type': basic_block_v1,
                  'n_filters': 512,
                  'n_blocks': 3,
                  'stride': 2,
                  'name': 'conv5'}]
    scp = outer_scope + '/' if outer_scope else ''
    scp += 'resnet152'
    return cls(input_tensor=input_tensor,
               scale_tensor=scale_tensor,
               block_cfg=stack_cfg,
               model_cfg=ResNetInputConfig(),
               use_preactivation=False,
               use_bias=True,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               n_resolution=n_resolution,
               name=scp,
               **kwargs)

  @classmethod
  def ResNet50V2(cls,
                 input_tensor=None,
                 scale_tensor=None,
                 include_top=True,
                 pooling=None,
                 classes=1000,
                 n_resolution=None,
                 outer_scope=None,
                 **kwargs):
    """
    Create a ResNet50 v2.0 network
    :param input_tensor: optional tensor (i.e. Input())
    :param scale_tensor: optional tensor (i.e. Input())
    :param include_top:   If `True` add the classification layers
    :param pooling:       Type of pooling to use if top layers are not included
    :param classes:       Number of predicated class if top layres are included
    :param n_resolution   Number of image resolution to use. If set to None,
                          disable the feature (optional)
    :param outer_scope:   Outer scope's name
    :param kwargs:        Extra keyword arguments
    :keyword layer_params:  Set of extra parameters for the underlying layers.
                          Expected format:
                          {type, dict()}
    return Network
    """
    stack_cfg = [{'block_type': basic_block_v2,
                  'n_filters': 64,
                  'n_blocks': 3,
                  'stride': 2,
                  'name': 'conv2'},
                 {'block_type': basic_block_v2,
                  'n_filters': 128,
                  'n_blocks': 4,
                  'stride': 2,
                  'name': 'conv3'},
                 {'block_type': basic_block_v2,
                  'n_filters': 256,
                  'n_blocks': 6,
                  'stride': 2,
                  'name': 'conv4'},
                 {'block_type': basic_block_v2,
                  'n_filters': 512,
                  'n_blocks': 3,
                  'stride': 1,
                  'name': 'conv5'}]
    scp = outer_scope + '/' if outer_scope else ''
    scp += 'resnet50v2'
    return cls(input_tensor=input_tensor,
               scale_tensor=scale_tensor,
               block_cfg=stack_cfg,
               model_cfg=ResNetV2InputConfig(),
               use_preactivation=True,
               use_bias=True,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               n_resolution=n_resolution,
               name=scp,
               **kwargs)

  @classmethod
  def ResNet101V2(cls,
                  input_tensor=None,
                  scale_tensor=None,
                  include_top=True,
                  pooling=None,
                  classes=1000,
                  n_resolution=None,
                  outer_scope=None,
                  **kwargs):
    """
    Create a ResNet101 v2.0 network
    :param input_tensor: optional tensor (i.e. Input())
    :param scale_tensor: optional tensor (i.e. Input())
    :param include_top:   If `True` add the classification layers
    :param pooling:       Type of pooling to use if top layers are not included
    :param classes:       Number of predicated class if top layres are included
    :param n_resolution   Number of image resolution to use. If set to None,
                          disable the feature (optional)
    :param outer_scope:   Outer scope's name
    :param kwargs:        Extra keyword arguments
    :keyword layer_params:  Set of extra parameters for the underlying layers.
                          Expected format:
                          {type, dict()}
    return Network
    """
    stack_cfg = [{'block_type': basic_block_v2,
                  'n_filters': 64,
                  'n_blocks': 3,
                  'stride': 1,
                  'name': 'conv2'},
                 {'block_type': basic_block_v2,
                  'n_filters': 128,
                  'n_blocks': 4,
                  'stride': 2,
                  'name': 'conv3'},
                 {'block_type': basic_block_v2,
                  'n_filters': 256,
                  'n_blocks': 23,
                  'stride': 2,
                  'name': 'conv4'},
                 {'block_type': basic_block_v2,
                  'n_filters': 512,
                  'n_blocks': 3,
                  'stride': 2,
                  'name': 'conv5'}]
    scp = outer_scope + '/' if outer_scope else ''
    scp += 'resnet101v2'
    return cls(input_tensor=input_tensor,
               scale_tensor=scale_tensor,
               block_cfg=stack_cfg,
               model_cfg=ResNetV2InputConfig(),
               use_preactivation=True,
               use_bias=True,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               n_resolution=n_resolution,
               name=scp,
               **kwargs)

  @classmethod
  def ResNet152V2(cls,
                  input_tensor=None,
                  scale_tensor=None,
                  include_top=True,
                  pooling=None,
                  classes=1000,
                  n_resolution=None,
                  outer_scope=None,
                  **kwargs):
    """
    Create a ResNet152 v1.0 network
    :param input_tensor: optional tensor (i.e. Input())
    :param scale_tensor: optional tensor (i.e. Input())
    :param include_top:   If `True` add the classification layers
    :param pooling:       Type of pooling to use if top layers are not included
    :param classes:       Number of predicated class if top layres are included
    :param n_resolution   Number of image resolution to use. If set to None,
                          disable the feature (optional)
    :param outer_scope:   Outer scope's name
    :param kwargs:        Extra keyword arguments
    :keyword layer_params:  Set of extra parameters for the underlying layers.
                          Expected format:
                          {type, dict()}
    return Network
    """
    stack_cfg = [{'block_type': basic_block_v2,
                  'n_filters': 64,
                  'n_blocks': 3,
                  'stride': 1,
                  'name': 'conv2'},
                 {'block_type': basic_block_v2,
                  'n_filters': 128,
                  'n_blocks': 8,
                  'stride': 2,
                  'name': 'conv3'},
                 {'block_type': basic_block_v2,
                  'n_filters': 256,
                  'n_blocks': 36,
                  'stride': 2,
                  'name': 'conv4'},
                 {'block_type': basic_block_v2,
                  'n_filters': 512,
                  'n_blocks': 3,
                  'stride': 2,
                  'name': 'conv5'}]
    scp = outer_scope + '/' if outer_scope else ''
    scp += 'resnet152v2'
    return cls(input_tensor=input_tensor,
               scale_tensor=scale_tensor,
               block_cfg=stack_cfg,
               model_cfg=ResNetV2InputConfig(),
               use_preactivation=True,
               use_bias=True,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               n_resolution=n_resolution,
               name=scp,
               **kwargs)

  def load_weights(self, filepath, by_name=False):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights, or `imagenet` to use pre-trained network.
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    """

    def _get_url(name, include_top):
      """
      Retrieve model url
      :param name:        Model's name
      :param include_top: If top is included
      :return:  Lits of candidates or empty if no match is found
      """
      w = list(filter(lambda x: x['name'] == name, WEIGHTS_COLLECTION))
      w = list(filter(lambda x: x['include_top'] == include_top, w))
      return w

    if filepath == 'imagenet':
      # Get model weight
      url = _get_url(name=self.name, include_top=self.include_top)
      if url:
        url = url[0]  # Pick first element
        # Match known configuration
        weights_path = get_file(url['name'],
                                url['url'],
                                cache_subdir='models',
                                file_hash=url['md5'])
        # Reload
        return super(ResNet, self).load_weights(weights_path)
      else:
        raise ValueError('There is no weights for such configuration: ' +
                         'model = {}'.format(self.name) +
                         'classes = {}, '.format(self.n_classes) +
                         'include_top = {}'.format(self.include_top))
    else:
      # Load default
      return super(ResNet, self).load_weights(filepath, by_name)

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    return None

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    return []

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    return []
