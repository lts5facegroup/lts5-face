# coding=utf-8
"""Contains definitions for Residual Networks.

[1] Very Deep Convolutional Networks for Large-Scale Image Recognition

See:
- https://arxiv.org/abs/1409.1556
"""
from os.path import splitext, basename

import tensorflow as _tf
import tensorflow.keras.layers as layers
from lts5.tensorflow_op.network import ImageFormat, Network
from lts5.tensorflow_op.network import InputSpec, InputConfigAbstract
from lts5.utils.files import get_file

__author__ = 'Christophe Ecabert'

# VGG models are taken from:
#  - keras-application: https://github.com/keras-team/keras-applications
WEIGHTS_COLLECTION = [
  # VGG16 top
  {'name': 'vgg16',
   'url': 'https://github.com/qubvel/classification_models/releases/download/'
          '0.0.1/resnet18_imagenet_1000.h5',
   'include_top': True,
   'md5': '64373286793e3c8b2b4e3219cbf3544b'},
  {'name': 'vgg16',
   'url': 'https://github.com/fchollet/deep-learning-models/releases/download/'
          'v0.1/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
   'include_top': False,
   'md5': '6d6bbae143d832006294945121d1f1fc'},
  {'name': 'vgg19',
   'url': 'https://github.com/fchollet/deep-learning-models/releases/download/'
          'v0.1/vgg19_weights_tf_dim_ordering_tf_kernels.h5',
   'include_top': True,
   'md5': 'cbe5617147190e668d6c5d5026f83318'},
  {'name': 'vgg19',
   'url': 'https://github.com/fchollet/deep-learning-models/releases/download/'
          'v0.1/vgg19_weights_tf_dim_ordering_tf_kernels_notop.h5',
   'include_top': False,
   'md5': '253f8cb515780f3b799900260a226db6'}
]


class VGGBlock(layers.Layer):
  """
  VGG Basic block composed of

    - 3-4x Conv2D
    - Max-pooling
  """

  def __init__(self,
               filters: list,
               kernels: list,
               activation: str,
               index: int,
               renaming=None,
               **kwargs):
    """
    Constructor
    :param filters: List of integer,indicates the number of filters for each
                    convolutions
    :param kernels: List of integer, indicates the size of the kernel for each
                    convolutions
    :param activation:  Type of activation function to use
    :param index: Block index
    :param renaming:  Optional dictionary to rename variables
    :param kwargs: Extra keyword arguments
    """
    super(VGGBlock, self).__init__(name='block{}'.format(index), **kwargs)
    self.naming = renaming or {}
    self.filters = filters
    self.kernels = kernels
    self.activation = activation
    self.conv = []
    # Convolutional layers
    for k, (nf, ks) in enumerate(zip(filters, kernels)):
      default_name = 'conv{}_{}'.format(index, k + 1)
      name = self.naming.get(default_name, default_name)
      self.conv.append(layers.Conv2D(filters=nf,
                                     kernel_size=ks,
                                     padding='same',
                                     activation=activation,
                                     name=name))
    # Create Max-pooling
    default_name = 'pool{}'.format(index)
    name = self.naming.get(default_name, default_name)
    self.pool = layers.MaxPool2D(pool_size=2, strides=2, name=name)

  def call(self, inputs):
    """
    Forward pass
    :param inputs:  Tensor to process
    :return:  Transformed data
    """
    x = inputs
    for conv in self.conv:
      x = conv(x)
    # Pool
    x = self.pool(x)
    return x


class VGGInputConfig(InputConfigAbstract):
  """ Input configuration for VGG type network """

  def __init__(self):
    """ Constructor """
    super(VGGInputConfig, self).__init__([InputSpec(dims=(224, 224, 3),
                                                    fmt=ImageFormat.RGB,
                                                    name='input')])

    def _pre_process_fn(x):
      # RGB -> BGR
      x = _tf.reverse(x, axis=[-1])
      # Zero-center by mean pixel
      x -= _tf.convert_to_tensor([103.939, 116.779, 123.68],
                                 dtype=_tf.float32)
      # Done
      return x

    def _revert_fn(x):
      x += _tf.convert_to_tensor([103.939, 116.779, 123.68], dtype=_tf.float32)
      x = _tf.reverse(x, axis=[-1])
      return x

    self._preprocess_fn = layers.Lambda(_pre_process_fn,
                                        name='preprocessing')
    self._revert__preprocess_fn = layers.Lambda(_revert_fn,
                                                name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert__preprocess_fn


_vgg16_renamer = {
  # Block1
  'conv1_1': 'block1_conv1',
  'conv1_2': 'block1_conv2',
  'pool1': 'block1_pool',
  # Block2
  'conv2_1': 'block2_conv1',
  'conv2_2': 'block2_conv2',
  'pool2': 'block2_pool',
  # Block3
  'conv3_1': 'block3_conv1',
  'conv3_2': 'block3_conv2',
  'conv3_3': 'block3_conv3',
  'pool3': 'block3_pool',
  # Block4
  'conv4_1': 'block4_conv1',
  'conv4_2': 'block4_conv2',
  'conv4_3': 'block4_conv3',
  'pool4': 'block4_pool',
  # Block5
  'conv5_1': 'block5_conv1',
  'conv5_2': 'block5_conv2',
  'conv5_3': 'block5_conv3',
  'pool5': 'block5_pool',
}

_vgg19_renamer = {
  # Block1
  'conv1_1': 'block1_conv1',
  'conv1_2': 'block1_conv2',
  'pool1': 'block1_pool',
  # Block2
  'conv2_1': 'block2_conv1',
  'conv2_2': 'block2_conv2',
  'pool2': 'block2_pool',
  # Block3
  'conv3_1': 'block3_conv1',
  'conv3_2': 'block3_conv2',
  'conv3_3': 'block3_conv3',
  'conv3_4': 'block3_conv4',
  'pool3': 'block3_pool',
  # Block4
  'conv4_1': 'block4_conv1',
  'conv4_2': 'block4_conv2',
  'conv4_3': 'block4_conv3',
  'conv4_4': 'block4_conv4',
  'pool4': 'block4_pool',
  # Block5
  'conv5_1': 'block5_conv1',
  'conv5_2': 'block5_conv2',
  'conv5_3': 'block5_conv3',
  'conv5_4': 'block5_conv4',
  'pool5': 'block5_pool',
}


class VGG(Network):
  """
  Generic VGG-base architecture

  Available arch:
    - VGG16
    - VGG19
    - VGGFace
   """

  def __init__(self,
               input_tensor,
               block_cfg: list,
               fc_cfg: list,
               include_top: bool,
               pooling: str,
               classes: int,
               name: str,
               **kwargs):
    """
    Constructor
    :param input_tensor: optional tensor (i.e. Input())
    :param block_cfg:     List of block configuration
    :param fc_cfg:        List of fully connected configuration
    :param include_top:   If `True` and fully connected layer, other no.
    :param pooling:       Type of pooling to use if `include_top` is set to
                          `False`. Options are 'avg' or 'max'
    :param classes:       Number of classes to output. Need `include_top` to be
                          `True`
    :param name:          Network's name
    :param kwargs:        Extra keyword arguments
    """
    model_cfg = VGGInputConfig()
    self.block_cfg = block_cfg
    self.fc_cfg = fc_cfg
    self.include_top = include_top
    self.pooling = pooling
    self.classes = classes

    # Input
    if input_tensor is None:
      input_t = layers.Input(shape=model_cfg.inputs_spec[0].dims,
                             name='input_' + name)
    else:
      if not _tf.keras.backend.is_keras_tensor(input_tensor):
        # Wrapper tensor `input_tensor` into input layer
        input_t = layers.Input(tensor=input_tensor,
                               shape=input_tensor.shape)
      else:
        input_t = input_tensor

    # Build blocks
    x = input_t
    for cfg in self.block_cfg:
      naming = cfg['renaming'] or {}
      with _tf.name_scope('Block{}'.format(cfg['index'])) as scope:
        # Convolutions
        for k, (nf, ks) in enumerate(zip(cfg['filters'], cfg['kernels'])):
          default_name = 'conv{}_{}'.format(cfg['index'], k + 1)
          layer_name = naming.get(default_name, default_name)
          x = layers.Conv2D(filters=nf,
                            kernel_size=ks,
                            padding='same',
                            activation=cfg['activation'],
                            name=scope + layer_name)(x)
        # Create Max-pooling
        default_name = 'pool{}'.format(cfg['index'])
        layer_name = naming.get(default_name, default_name)
        x = layers.MaxPool2D(pool_size=2,
                             strides=2,
                             name=scope + layer_name)(x)

    # Include top ?
    if self.include_top:
      with _tf.name_scope('Predictor') as scope:
        x = layers.Flatten(name=scope + 'flatten')(x)
        k = len(self.block_cfg) + 1
        for cfg in self.fc_cfg:
          units = cfg['units']
          act = cfg['activation']
          layer_name = scope + cfg['name']
          x = layers.Dense(units, name=layer_name)(x)
          layer_name = scope + act + '{}'.format(k)
          x = layers.Activation(act, name=layer_name)(x)
          k += 1
    else:
      if self.pooling == 'avg':
        x = layers.GlobalAveragePooling2D(name='avg_pool')(x)
      elif self.pooling == 'max':
        x = layers.GlobalMaxPooling2D(name='max_pool')(x)

    # Ensure that the model takes into account any potential predecessors of
    # `input_tensor`.
    if input_tensor is not None:
      inputs = _tf.keras.utils.get_source_inputs(input_tensor)
    else:
      inputs = input_t
    # CTOR
    super(VGG, self).__init__(model_cfg,
                              inputs,
                              x,
                              name=name,
                              **kwargs)


  @classmethod
  def VGG16(cls,
            input_tensor=None,
            include_top=True,
            pooling=None,
            classes=1000):
    """
    Build VGG16 network
    :param input_tensor: optional tensor (i.e. Input())
    :param include_top: If `True` add the classification layers
    :param pooling:     Type of pooling to use if top layers are not included
    :param classes:     Number of predicated class if top layres are included
    :return:  Network
    """
    block_cfg = [{'filters': [64, 64],
                  'kernels': [3, 3],
                  'activation': 'relu',
                  'index': 1,
                  'renaming': _vgg16_renamer},
                 {'filters': [128, 128],
                  'kernels': [3, 3],
                  'activation': 'relu',
                  'index': 2,
                  'renaming': _vgg16_renamer},
                 {'filters': [256, 256, 256],
                  'kernels': [3, 3, 3],
                  'activation': 'relu',
                  'index': 3,
                  'renaming': _vgg16_renamer},
                 {'filters': [512, 512, 512],
                  'kernels': [3, 3, 3],
                  'activation': 'relu',
                  'index': 4,
                  'renaming': _vgg16_renamer},
                 {'filters': [512, 512, 512],
                  'kernels': [3, 3, 3],
                  'activation': 'relu',
                  'index': 5,
                  'renaming': _vgg16_renamer}]
    fc_cfg = None
    if include_top:
      fc_cfg = [{'units': 4096,
                 'activation': 'relu',
                 'name': 'fc1'},
                {'units': 4096,
                 'activation': 'relu',
                 'name': 'fc2'},
                {'units': classes,
                 'activation': 'softmax',
                 'name': 'predictions'}]
    return cls(input_tensor=input_tensor,
               block_cfg=block_cfg,
               fc_cfg=fc_cfg,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               name='vgg16')

  @classmethod
  def VGG19(cls,
            input_tensor=None,
            include_top=True,
            pooling=None,
            classes=1000):
    """
    Build VGG19 network
    :param input_tensor: optional tensor (i.e. Input())
    :param include_top: If `True` add the classification layers
    :param pooling:     Type of pooling to use if top layers are not included
    :param classes:     Number of predicated class if top layres are included
    :return:  Network
    """
    block_cfg = [{'filters': [64, 64],
                  'kernels': [3, 3],
                  'activation': 'relu',
                  'index': 1,
                  'renaming': _vgg19_renamer},
                 {'filters': [128, 128],
                  'kernels': [3, 3],
                  'activation': 'relu',
                  'index': 2,
                  'renaming': _vgg19_renamer},
                 {'filters': [256, 256, 256, 256],
                  'kernels': [3, 3, 3, 3],
                  'activation': 'relu',
                  'index': 3,
                  'renaming': _vgg19_renamer},
                 {'filters': [512, 512, 512, 512],
                  'kernels': [3, 3, 3, 3],
                  'activation': 'relu',
                  'index': 4,
                  'renaming': _vgg19_renamer},
                 {'filters': [512, 512, 512, 512],
                  'kernels': [3, 3, 3, 512],
                  'activation': 'relu',
                  'index': 5,
                  'renaming': _vgg19_renamer}]
    fc_cfg = None
    if include_top:
      fc_cfg = [{'units': 4096,
                 'activation': 'relu',
                 'name': 'fc1'},
                {'units': 4096,
                 'activation': 'relu',
                 'name': 'fc2'},
                {'units': classes,
                 'activation': 'softmax',
                 'name': 'predictions'}]
    return cls(input_tensor=input_tensor,
               block_cfg=block_cfg,
               fc_cfg=fc_cfg,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               name='vgg19')

  @classmethod
  def VGGFace(cls,
              input_tensor=None,
              include_top=True,
              pooling=None,
              classes=2622):
    """
    Build VGGFace network
    :param input_tensor: optional tensor (i.e. Input())
    :param include_top: If `True` add the classification layers
    :param pooling:     Type of pooling to use if top layers are not included
    :param classes:     Number of predicated class if top layres are included
    :return:  Network
    """
    block_cfg = [{'filters': [64, 64],
                  'kernels': [3, 3],
                  'activation': 'relu',
                  'index': 1,
                  'renaming': None},
                 {'filters': [128, 128],
                  'kernels': [3, 3],
                  'activation': 'relu',
                  'index': 2,
                  'renaming': None},
                 {'filters': [256, 256, 256],
                  'kernels': [3, 3, 3],
                  'activation': 'relu',
                  'index': 3,
                  'renaming': None},
                 {'filters': [512, 512, 512],
                  'kernels': [3, 3, 3],
                  'activation': 'relu',
                  'index': 4,
                  'renaming': None},
                 {'filters': [512, 512, 512],
                  'kernels': [3, 3, 3],
                  'activation': 'relu',
                  'index': 5,
                  'renaming': None}]
    fc_cfg = None
    if include_top:
      fc_cfg = [{'units': 4096,
                 'activation': 'relu',
                 'name': 'fc6'},
                {'units': 4096,
                 'activation': 'relu',
                 'name': 'fc7'},
                {'units': classes,
                 'activation': 'softmax',
                 'name': 'fc8'}]
    return cls(input_tensor=input_tensor,
               block_cfg=block_cfg,
               fc_cfg=fc_cfg,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               name='vggface')

  def load_weights(self, filepath, by_name=False, **kwargs):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights, or `imagenet` to use pre-trained network.
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    :param kwargs:    Extra keyword arguments
    :keyword skip_layer: List of layer's name to be trained from scratch
                         (i.e. skip restoration)
    """

    def _get_url(name, include_top):
      """
      Retrieve model url
      :param name:        Model's name
      :param include_top: If top is included
      :return:  Lits of candidates or empty if no match is found
      """
      w = list(filter(lambda x: x['name'] == name, WEIGHTS_COLLECTION))
      w = list(filter(lambda x: x['include_top'] == include_top, w))
      return w

    if filepath == 'imagenet':
      # Get model weight
      url = _get_url(name=self.name, include_top=self.include_top)
      if url:
        url = url[0]  # Pick first element
        # Match known configuration
        weights_path = get_file(url['name'],
                                url['url'],
                                cache_subdir='models',
                                file_hash=url['md5'])
        # Reload
        super(VGG, self).load_weights(weights_path)
      else:
        raise ValueError('There is no weights for such configuration: ' +
                         'model = {} '.format(self.name) +
                         'classes = {}, '.format(self.classes) +
                         'include_top = {}'.format(self.include_top))
    else:
      # numpy array ?
      # Check if numpy file
      _, ext = splitext(basename(filepath))
      if ext == '.npy':
        import numpy as _np
        # Restore pre-trained model from numpy file
        tf_converter = {'kernel': 'weights',
                        'bias': 'biases'}
        # Define layers that are trainable
        skip_layer = kwargs.get('skip_layer', [])
        # Load the weights into memory
        w_dict = _np.load(filepath, encoding="bytes", allow_pickle=True).item()
        # Iterate over every layers in the model and set weights if needed.
        for layer in self.layers:
          # Remove outter scope
          no_scope_layer_name = basename(layer.name)
          if no_scope_layer_name not in skip_layer:
            # Need to initialize from pre-trained
            data = w_dict.get(no_scope_layer_name, None)
            if data is not None:
              np_weight = []
              for w in layer.weights:
                start = w.name.rfind('/') + 1
                stop = w.name.find(':')
                wname = w.name[start:stop]
                wname = tf_converter.get(wname, wname).encode()
                np_weight.append(data[wname])
              layer.set_weights(np_weight)
      else:
        # Load default
        super(VGG, self).load_weights(filepath, by_name)

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :param kwargs: Extra keyword arguments
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    return None

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :param kwargs: Extra keyword arguments
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    return []

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :param kwargs: Extra keyword arguments
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    return []
