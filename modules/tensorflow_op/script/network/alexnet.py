# coding=utf-8
"""This is an TensorFlow implementation of AlexNet by Alex Krizhevsky at all.

Paper:
- http://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf

Explanation can be found in my blog post:
- https://kratzert.github.io/2017/02/24/finetuning-alexnet-with-tensorflow.html

This script enables finetuning AlexNet on any given Dataset with any number of
classes. The structure of this script is strongly inspired by the fast.ai
Deep Learning class by Jeremy Howard and Rachel Thomas, especially their vgg16
finetuning script:

Link:
- https://github.com/fastai/courses/blob/master/deeplearning1/nbs/vgg16.py

The pretrained weights can be downloaded here and should be placed in the same
folder as this file:
- http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/

@author: Frederik Kratzert (contact: f.kratzert(at)gmail.com)
Link:
- https://github.com/kratzert/finetune_alexnet_with_tensorflow/blob/master/alexnet.py

"""
from os.path import splitext, basename

import tensorflow as _tf
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Lambda
from tensorflow.keras.layers import Flatten, Dense, Dropout
from tensorflow.keras.layers import InputSpec as KerasInputSpec
from tensorflow.keras.layers import Layer
from lts5.tensorflow_op.data.preprocessing import central_crop, rgb_to_bgr
from lts5.tensorflow_op.layers.normalization import LocalResponseNormalization
from lts5.tensorflow_op.network.network import ImageFormat
from lts5.tensorflow_op.network.network import InputConfigAbstract
from lts5.tensorflow_op.network.network import InputSpec
from lts5.tensorflow_op.network.network import Network
from lts5.tensorflow_op.layers import create_layer
from lts5.tensorflow_op.image.util import smart_resize


__author__ = 'Christophe Ecabert'


class AlexNetPreProcessingLayer(Layer):
  """ Pre-processing used by AlexNet """

  def __init__(self,
               image_size: int,
               crop_size: int,
               from_rgb: bool = True):
    """
    Constructor
    :param image_size:  Size to which the input image is resized (anisotropic)
    :param crop_size:   Cropping dimensions
    :param from_rgb:    Indicate if conversion from RGB to BGR is required
    """
    super(AlexNetPreProcessingLayer, self).__init__(name='AlexNetPreProcessing',
                                                    trainable=False)
    self._image_sz = image_size
    self._crop_sz = crop_size
    self._from_rgb = from_rgb

  def call(self, inputs, **kwargs):
    """
    Apply preprocessing
    :param inputs:  List of input tensors
    :param kwargs:  Extra keyword arguments
    :return:  Preprocessed images
    """
    # Anisotropic resize
    img = smart_resize(images=inputs,
                       size=[self._image_sz, self._image_sz])
    # Central crop
    img = central_crop(image=img,
                       crop_h=self._crop_sz,
                       crop_w=self._crop_sz)
    # Convert to BGR if required
    if self._from_rgb:
      img = rgb_to_bgr(img)
    return img


class AlexNetInputConfig(InputConfigAbstract):
  """ Input specification for AlexNet """

  def __init__(self):
    """ Constructor """
    super(AlexNetInputConfig, self).__init__([InputSpec(dims=(227, 227, 3),
                                                        fmt=ImageFormat.BGR,
                                                        name='input')])
    crop_sz = self.inputs_spec[0].dims[0]
    self._preprocess_fn = AlexNetPreProcessingLayer(image_size=256,
                                                    crop_size=crop_sz)

    def _revert_proc_fn(x):
      x = _tf.reverse(x, axis=[-1])
      x = smart_resize(x, size=(256, 256))
      return x

    self._revert_preprocess_fn = Lambda(_revert_proc_fn,
                                        name='invert_preprocessing')


  @property
  def preprocessing_fn(self):
    """
    Provide pre-processing layer
    :return:  Instance of `tf.keras.layer.Layer`
    """
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """ Invert preprocessing """
    return self._revert_preprocess_fn


class Transform(Layer):
  """ Image normalization, remove pixel-wise mean from a given input """

  def __init__(self, **kwargs):
    """
    Constructor
    :param kwargs:  Extra keyword arguments
    :keyword trainable:   Boolean, whether the layer's variables should be
                          trainable.
    :keyword name:        String name of the layer.
    """
    super(Transform, self).__init__(**kwargs)
    self.value = None   # Value to subtract

  def build(self, input_shape):
    """
    Set up internal var
    :param input_shape: Layer's input dimensions
    """
    self.value = self.add_weight(shape=input_shape[1:], name='weights')
    # Call base class function
    super(Transform, self).build(input_shape)

  def call(self, inputs, **kwargs):
    """
    Forward function
    :param inputs:  A `Tensor`
    :param kwargs:  Extra keyword arguments
    :return:  Transformed tensor: input - kernel
    """
    return inputs - self.value


class GroupedConv2D(Conv2D):
  """ Grouped convolution for AlexNet """

  def __init__(self, group=1, **kwargs):
    """
    Constructor
    :param group:   Number of group (i.e. split of feature map)
    :param kwargs:  Extra keyword arguments
    """
    super(GroupedConv2D, self).__init__(**kwargs)
    self.group = group
    if not self.group > 1:
      raise ValueError('Group must be larger than 1, got {}'.format(self.group))

  def build(self, input_shape):
    """ Build layer """
    shp = input_shape.as_list()
    chan_idx = 1 if self.data_format == 'channels_first' else -1
    shp[chan_idx] = shp[chan_idx] // self.group
    # Call base function with proper dimensions
    super(GroupedConv2D, self).build(_tf.TensorShape(shp))
    # Override inputs specs otherwise input dimensions will be halfed
    input_dim = int(input_shape[chan_idx])
    self.input_spec = KerasInputSpec(ndim=self.rank + 2,
                                     axes={chan_idx: input_dim})

  def call(self, inputs):
    """
    Forward pass
    :param inputs:  Tensor to be convoloved
    :param kwargs:  Extra keyword arguments
    :return:  Result of convolution
    """
    # Split the input into groups and then convolve each of them
    # independently
    chan_idx = 1 if self.data_format == 'channels_first' else -1
    feat_g = _tf.split(value=inputs,
                       num_or_size_splits=self.group,
                       axis=chan_idx)
    kernel_g = _tf.split(value=self.kernel,
                         num_or_size_splits=self.group,
                         axis=chan_idx)
    outputs = [self._convolution_op(feat, k) for feat, k in zip(feat_g,
                                                                kernel_g)]
    outputs = _tf.concat(outputs, axis=chan_idx)
    # Add the biases
    if self.use_bias:
      if self.data_format == 'channels_first':
        if self.rank == 1:
          # nn.bias_add does not accept a 1D input tensor.
          bias = _tf.reshape(self.bias, (1, self.filters, 1))
          outputs += bias
        else:
          outputs = _tf.nn.bias_add(outputs, self.bias, data_format='NCHW')
      else:
        outputs = _tf.nn.bias_add(outputs, self.bias, data_format='NHWC')
    # Apply activation
    if self.activation is not None:
      return self.activation(outputs)
    return outputs


class AlexNet(Network):
  """ AlexNet using keras interface """

  def __init__(self,
               add_transform: bool = True,
               include_top: bool = True,
               n_classes: int = 1000,
               rate: float = 0.5,
               activation=None,
               normalization='lrn',
               name='AlexNet',
               **kwargs):
    """
    Constructor
    :param add_transform: If `True` add transform layer that remove mean value
                        of imagenet (i.e. pixel-wise).
    :param include_top: If `True` the fully connected layers are also part of
                        the model. Otherwise only convolutional blocks are
                        created
    :param n_classes:   Number of predicted class (used only if
                        include_top=True)
    :param rate:        Dropout rate
    :param activation:  Top layer's activation function
    :param normalization: Type of normalization to use, Either `lrn`, `batch`,
                          `none`.
    :param kwargs       Extra keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(AlexNet, self).__init__(config=AlexNetInputConfig(),
                                  name=name,
                                  **kwargs)
    self._add_transform = add_transform
    self._include_top = include_top
    self._n_classes = n_classes
    self._rate = rate
    self._activation = activation
    self._normalization = normalization.lower()
    if self._normalization not in ['lrn', 'batch', 'none']:
      raise ValueError('Normalization: {} not '
                       'supported ({})'.format(self._normalization,
                                               ['lrn', 'batch', 'none']))
    # Remove mean
    if self._add_transform:
      self.im_transform = Transform(trainable=False, name='transform')
    else:
      self.im_transform = Lambda(lambda x: x, name='transform')
    # 1st Layer: Conv (w ReLu) -> Lrn -> Pool
    self.conv1 = create_layer(Conv2D,
                              filters=96,
                              kernel_size=(11, 11),
                              strides=(4, 4),
                              padding='VALID',
                              activation='relu',
                              use_bias=True,
                              name='conv1',
                              layer_params=layer_params)
    if self._normalization == 'lrn':
      self.norm1 = create_layer(LocalResponseNormalization,
                                depth_radius=2,
                                alpha=1.99999994948e-5,
                                beta=0.75,
                                name='norm1',
                                layer_params=layer_params)
      self.norm1_fn = lambda x, **kw: self.norm1(x)
    elif self._normalization == 'batch':
      self.norm1 = create_layer(BatchNormalization,
                                name='norm1',
                                layer_params=layer_params)
      self.norm1_fn = lambda x, **kw: self.norm1(x, **kw)
    else:
      self.norm1_fn = Lambda(lambda x: x, name='norm1')
    self.pool1 = create_layer(MaxPooling2D,
                              pool_size=(3, 3),
                              strides=(2, 2),
                              padding='VALID',
                              name='pool1',
                              layer_params=layer_params)
    # 2nd Layer: Conv (w ReLu)  -> Lrn -> Pool with 2 groups
    self.conv2 = create_layer(GroupedConv2D,
                              filters=256,
                              group=2,
                              kernel_size=(5, 5),
                              strides=(1, 1),
                              padding='SAME',
                              activation='relu',
                              use_bias=True,
                              name='conv2',
                              layer_params=layer_params)
    if self._normalization == 'lrn':
      self.norm2 = create_layer(LocalResponseNormalization,
                                depth_radius=2,
                                alpha=1.99999994948e-5,
                                beta=0.75,
                                name='norm2',
                                layer_params=layer_params)
      self.norm2_fn = lambda x, **kw: self.norm2(x)
    elif self._normalization == 'batch':
      self.norm2 = create_layer(BatchNormalization,
                                name='norm2',
                                layer_params=layer_params)
      self.norm2_fn = lambda x, **kw: self.norm2(x, **kw)
    else:
      self.norm2_fn = Lambda(lambda x: x, name='norm2')
    self.pool2 = create_layer(MaxPooling2D,
                              pool_size=(3, 3),
                              strides=(2, 2),
                              padding='VALID',
                              name='pool2',
                              layer_params=layer_params)
    # 3rd Layer: Conv (w ReLu)
    self.conv3 = create_layer(Conv2D,
                              filters=384,
                              kernel_size=(3, 3),
                              strides=(1, 1),
                              padding='SAME',
                              activation='relu',
                              use_bias=True,
                              name='conv3',
                              layer_params=layer_params)
    # 4th Layer: Conv (w ReLu) split into two groups
    self.conv4 = create_layer(GroupedConv2D,
                              filters=384,
                              group=2,
                              kernel_size=(3, 3),
                              strides=(1, 1),
                              padding='SAME',
                              activation='relu',
                              use_bias=True,
                              name='conv4',
                              layer_params=layer_params)
    # 5th Layer: Conv (w ReLu) -> Pool split into two groups
    self.conv5 = create_layer(GroupedConv2D,
                              filters=256,
                              group=2,
                              kernel_size=(3, 3),
                              strides=(1, 1),
                              padding='SAME',
                              activation='relu',
                              use_bias=True,
                              name='conv5',
                              layer_params=layer_params)
    self.pool5 = create_layer(MaxPooling2D,
                              pool_size=(3, 3),
                              strides=(2, 2),
                              padding='VALID',
                              name='pool5',
                              layer_params=layer_params)
    # Add top ?
    if self._include_top:
      # 6th Layer: Flatten -> FC (w ReLu) -> Dropout
      self.flat6 = create_layer(Flatten,
                                name='flat',
                                layer_params=layer_params)
      self.fc6 = create_layer(Dense,
                              units=4096,
                              activation='relu',
                              name='fc6',
                              layer_params=layer_params)
      self.drop6 = create_layer(Dropout,
                                rate=self._rate,
                                name='dropout6',
                                layer_params=layer_params)
      # 7th Layer: FC (w ReLu) -> Dropout
      self.fc7 = create_layer(Dense,
                              units=4096,
                              activation='relu',
                              name='fc7',
                              layer_params=layer_params)
      self.drop7 = create_layer(Dropout,
                                rate=self._rate,
                                name='dropout7',
                                layer_params=layer_params)
      # 8th Layer: FC and return unscaled activations
      self.fc8 = create_layer(Dense,
                              units=self._n_classes,
                              name='fc8',
                              activation=self._activation,
                              layer_params=layer_params)

  def call(self, inputs, training):
    """
    Build forward pass
    :param inputs:    Input tensor ( i.e. image)
    :param training:  Indicate if in training
    :return: Output tensors. Either `pool5` or unscaled logits of `fc8`
    """
    x = self.im_transform(inputs)
    # 1st Layer: Conv (w ReLu) -> Lrn -> Pool
    x = self.conv1(x)
    x = self.norm1_fn(x, training=training)
    x = self.pool1(x)
    # 2nd Layer: Conv (w ReLu) -> Lrn -> Pool
    x = self.conv2(x)
    x = self.norm2_fn(x, training=training)
    x = self.pool2(x)
    # 3rd Layer: Conv (w ReLu)
    x = self.conv3(x)
    # 4th Layer: Conv (w ReLu)
    x = self.conv4(x)
    # 5th Layer: Conv (w ReLu) -> Pool
    x = self.conv5(x)
    x = self.pool5(x)
    # Add top
    if self._include_top:
      # 6th Layer: Flatten -> FC (w ReLu) -> Dropout
      x = self.flat6(x)
      x = self.fc6(x)
      x = self.drop6(x, training)
      # 7th Layer: FC (w ReLu) -> Dropout
      x = self.fc7(x)
      x = self.drop7(x, training)
      # 8th Layer: FC and return unscaled activations
      x = self.fc8(x)
    return x

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    return None

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    return []

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    return []

  def load_weights(self, filepath, by_name=False, **kwargs):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    :param kwargs:    Extra keyword arguments
    :keyword skip_layer: List of layer's name to be trained from scratch
                         (i.e. skip restoration)
    """
    # Check if numpy file
    _, ext = splitext(basename(filepath))
    if ext == '.npy':
      import numpy as _np
      # Restore pre-trained model from numpy file
      tf_converter = {'kernel': 'weights',
                      'bias': 'biases'}
      # Define layers that are trainable
      skip_layer = kwargs.get('skip_layer', [])
      # Load the weights into memory
      w_dict = _np.load(filepath, encoding="bytes", allow_pickle=True).item()
      # Iterate over every layers in the model and set weights if needed.
      for layer in self.layers:
        if layer.name not in skip_layer:
          # Need to initialize from pre-trained
          data = w_dict.get(layer.name, None)
          if data is not None:
            np_weight = []
            for w in layer.weights:
              start = w.name.rfind('/') + 1
              stop = w.name.find(':')
              wname = w.name[start:stop]
              wname = tf_converter.get(wname, wname).encode()
              np_weight.append(data[wname])
            layer.set_weights(np_weight)
    else:
      # Try with base class function
      super(AlexNet, self).load_weights(filepath, by_name)