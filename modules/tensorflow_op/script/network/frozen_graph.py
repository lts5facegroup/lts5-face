# coding=utf-8
"""
Network interface for tensorflow 1.x frozen graph. This network is not trainable
and can only be used in inference mode.
Can be used with model converted using ONNX tools.

See:
  - https://www.tensorflow.org/guide/migrate#a_graphpb_or_graphpbtxt
"""
from os.path import basename, splitext
import tensorflow as tf
from lts5.tensorflow_op.network import Network, InputConfigAbstract

__author__ = 'Christophe Ecabert'


class FrozenGraph(Network):
  """ Interface for tensorflow 1.x frozen graph """

  def __init__(self,
               tensor_input_names,
               tensor_output_names,
               config: InputConfigAbstract,
               name:str,
               **kwargs):
    # Base constructor
    super(FrozenGraph, self).__init__(name=name, config=config, **kwargs)
    # Inference function
    self._input_names = tensor_input_names
    self._output_names = tensor_output_names
    self._inference_fn = None

  def call(self, inputs, **kwargs):
    """
    Call forward pass
    :param inputs:    Image to be processed
    :param kwargs:    Extra keyword arguments
    :param training:  If `True` indicates training phase
    :return:  Network's output
    """
    images = tf.transpose(inputs, (0, 3, 1, 2))  # Convert NHWC to NCHW
    return self._inference_fn(images)

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    return None  # Load model from frozen graph, no training

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    return []

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    return []

  def load_weights(self, filepath, by_name=False, **kwargs):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    :param kwargs:    Extra keyword arguments
    :keyword skip_layer: List of layer's name to be trained from scratch
                         (i.e. skip restoration)
    """
    _, ext = splitext(basename(filepath))
    if ext == '.pb':
      # Load frozen graph definition
      def _wrap_frozen_graph(graph_def, inputs, outputs):
        def _imports_graph_def():
          tf.compat.v1.import_graph_def(graph_def, name="")

        wrapped_import = tf.compat.v1.wrap_function(_imports_graph_def, [])
        import_graph = wrapped_import.graph
        nest_in = tf.nest.map_structure(import_graph.as_graph_element, inputs)
        nest_out = tf.nest.map_structure(import_graph.as_graph_element, outputs)
        return wrapped_import.prune(nest_in, nest_out)

      # Load graph definition
      graph_def = tf.compat.v1.GraphDef()
      graph_def.ParseFromString(open(filepath, 'rb').read())
      # map to function
      self._inference_fn = _wrap_frozen_graph(graph_def,
                                              self._input_names,
                                              self._output_names)
    else:
      raise ValueError('model file should be a *.pb file, got: '
                       '`{}`'.format(filepath))
