# coding=utf-8
"""
Network predicting albedo, normals and lighting from a given RGB image.

See:
 - https://senguptaumd.github.io/SfSNet/
"""
import tensorflow as tf
import tensorflow.keras.layers as kl
from lts5.tensorflow_op.network.network import InputConfigAbstract
from lts5.tensorflow_op.network.network import ImageFormat, InputSpec
from lts5.tensorflow_op.network import FrozenGraph
from lts5.tensorflow_op.image.util import smart_resize


class SfSNetInputConfig(InputConfigAbstract):
  """ Input configuration: 128x128 RGB scaled between [0, 1] """

  def __init__(self):
    """ Constructor """
    super(SfSNetInputConfig, self).__init__([InputSpec(dims=(128, 128, 3),
                                                       fmt=ImageFormat.RGB,
                                                       name='input')])

    def _pre_process_fn(x):
      """  Pre-processing """
      # Resize
      x = smart_resize(tf.cast(x, tf.float32), [128, 128])
      # move to [0, 1] range
      x = x / 255.0
      # convert to bgr
      x = tf.reverse(x, axis=[-1])
      return x

    def _revert_pre_process_fn(x):
      x = tf.reverse(x, axis=[-1])  # Back to RGB
      x = x * 255.0
      return x

    self._preprocess_fn = kl.Lambda(_pre_process_fn,
                                    name='preprocessing')
    self._revert_preprocess_fn = kl.Lambda(_revert_pre_process_fn,
                                           name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn


  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


class SfSNet(FrozenGraph):
  """ Compute surface normals from RGB images """

  def __init__(self, add_preprocessing=True, name='SfSNet', **kwargs):
    """
    Constructor
    :param name:    Network's name
    :param add_preprocessing: If `True` add preprocessing step in forward pass,
      otherwise not.
    :param kwargs:  Extra keyword arguments
    """
    super(SfSNet, self).__init__(tensor_input_names='image:0',
                                 tensor_output_names='normals:0',
                                 config=SfSNetInputConfig(),
                                 name=name,
                                 **kwargs)
    self.with_preproc = add_preprocessing

  def call(self, inputs, **kwargs):
    """
    Forward
    :param inputs:
    :param kwargs:
    :return:
    """
    x = inputs
    # Pre-processing ?
    if self.with_preproc:
      # Resize + normalize [0-1] + BGR
      x = self.inputs_config.preprocessing_fn(x)
    # Convert NHWC to NCHW
    x = tf.transpose(x, (0, 3, 1, 2))
    # normals
    n = self._inference_fn(x)
    # Post-processing
    n = tf.transpose(n, (0, 2, 3, 1))  # Back to NHWC
    # Convert back to RGB format since initial model was trained in `caffe`
    n = n[..., ::-1]
    # Normals are in [-1.0, 1.0] and are mapped to [0.0, 1.0] therefore ensure
    # proper output
    n = tf.clip_by_value(n, 0.0, 1.0)
    # Convert back to [-1.0 - 1.0]
    n = (2.0 * n) - 1.0
    # Normalize to ensure |n| == 1
    n, _ = tf.linalg.normalize(n, ord=2, axis=-1)
    return n
