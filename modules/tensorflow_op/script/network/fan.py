# coding=utf-8
"""
Tensorflow implementation of FAN2D face alignment network proposed by A. Bulat
in "How far are we from solving the 2D & 3D Face Alignment problem?"

Note:
  Model converted from pytorch using ONNX tools
  For Tensorflow 2.0

See:
  - https://www.adrianbulat.com/downloads/FaceAlignment/FaceAlignment.pdf
  - https://www.tensorflow.org/guide/migrate#a_graphpb_or_graphpbtxt
"""
from os.path import basename, splitext
import tensorflow as tf
import numpy as np
from imageio import imread
import cv2 as _cv
from tensorflow.keras.layers import Lambda
from lts5.tensorflow_op.network import FrozenGraph
from lts5.tensorflow_op.network.network import ImageFormat, InputSpec
from lts5.tensorflow_op.network.network import InputConfigAbstract


__author__ = 'Christophe Ecabert'


def _transform(point, center, scale, resolution, invert=False):
  """Generate and affine transformation matrix.
  Given a set of points, a center, a scale and a targer resolution, the
  function generates and affine transformation matrix. If invert is ``True``
  it will produce the inverse transformation.
  Arguments:
      point {numpy.array} -- the input 2D point
      center {numpy.array} -- the center around which to perform the
                              transformations
      scale {float} -- the scale of the face/object
      resolution {float} -- the output resolution
  Keyword Arguments:
      invert {bool} -- define wherever the function should produce the direct or the
      inverse transformation matrix (default: {False})
  """
  _pt = np.ones(3)
  _pt[0] = point[0]
  _pt[1] = point[1]

  h = 200.0 * scale
  t = np.eye(3)
  t[0, 0] = resolution / h
  t[1, 1] = resolution / h
  t[0, 2] = resolution * (-center[0] / h + 0.5)
  t[1, 2] = resolution * (-center[1] / h + 0.5)

  if invert:
    t = np.linalg.inv(t)

  new_point = (t @ _pt)[0:2]

  return new_point.astype(np.int32)


def _crop(image, center, scale, resolution=256.0):
  """ Center crops an image or set of heatmaps

  :param image:       {numpy.array} an rgb image
  :param center:      {numpy.array} the center of the object, usually the same
                      as of the bounding box
  :param scale:       {float} scale of the face
  :param resolution:  {float} the size of the output cropped image
                      (default: 256.0)
  :return:  Cropped region
  """
  # Crop around the center point
  ul = _transform([1, 1], center, scale, resolution, True)
  br = _transform([resolution, resolution], center, scale, resolution, True)
  # pad = math.ceil(torch.norm((ul - br).float()) / 2.0 - (br[0] - ul[0]) / 2.0)
  if image.ndim > 2:
    newDim = np.array([br[1] - ul[1], br[0] - ul[0],
                       image.shape[2]], dtype=np.int32)
    newImg = np.zeros(newDim, dtype=np.uint8)
  else:
    newDim = np.array([br[1] - ul[1], br[0] - ul[0]], dtype=np.int)
    newImg = np.zeros(newDim, dtype=np.uint8)
  ht = image.shape[0]
  wd = image.shape[1]
  newX = np.array(
    [max(1, -ul[0] + 1), min(br[0], wd) - ul[0]], dtype=np.int32)
  newY = np.array(
    [max(1, -ul[1] + 1), min(br[1], ht) - ul[1]], dtype=np.int32)
  oldX = np.array([max(1, ul[0] + 1), min(br[0], wd)], dtype=np.int32)
  oldY = np.array([max(1, ul[1] + 1), min(br[1], ht)], dtype=np.int32)
  newImg[newY[0] - 1:newY[1], newX[0] - 1:newX[1]
  ] = image[oldY[0] - 1:oldY[1], oldX[0] - 1:oldX[1], :]
  newImg = _cv.resize(newImg,
                      dsize=(int(resolution), int(resolution)),
                      interpolation=_cv.INTER_LINEAR)
  return newImg


def _get_preds_fromhm(hm, center=None, scale=None):
  """
  Obtain (x,y) coordinates given a set of N heatmaps. If the center
  and the scale is provided the function will return the points also in
  the original coordinate frame.
  Arguments:
      hm {numpy.array} -- the predicted heatmaps, of shape [B, N, W, H]
  Keyword Arguments:
      center {numpy.array} -- the center of the bounding box (default: {None})
      scale {float} -- face scale (default: {None})
  """
  # max, idx = torch.max(hm.view(hm.size(0), hm.size(1), hm.size(2) * hm.size(3)),2)
  # idx += 1
  hm_shp = hm.shape
  hm_ = hm.reshape(hm_shp[0], hm_shp[1], hm_shp[2] * hm_shp[3])
  v_idx = np.argmax(hm_, axis=2)
  v_idx += 1
  v_idx = np.expand_dims(v_idx, -1)

  # Convert flat index into x,y pair
  # preds = idx.view(idx.size(0), idx.size(1), 1).repeat(1, 1, 2).float()
  # preds[..., 0].apply_(lambda x: (x - 1) % hm.size(3) + 1)
  # preds[..., 1].add_(-1).div_(hm.size(2)).floor_().add_(1)
  preds = np.tile(v_idx, [1, 1, 2]).astype(np.float32)
  preds[..., 0] = ((preds[..., 0] - 1.0) % hm.shape[3]) + 1.0
  preds[..., 1] = np.floor((preds[..., 1] - 1.0) / hm.shape[2]) + 1.0

  # Iterate over all predictions
  for i in range(preds.shape[0]):     # Batch
    for j in range(preds.shape[1]):   # number pts
      hm_ = hm[i, j, :]
      pX, pY = int(preds[i, j, 0]) - 1, int(preds[i, j, 1]) - 1
      if pX > 0 and pX < 63 and pY > 0 and pY < 63:
        diff = np.asarray([hm_[pY, pX + 1] - hm_[pY, pX - 1],
                           hm_[pY + 1, pX] - hm_[pY - 1, pX]])
        preds[i, j, :] += np.sign(diff) * 0.25
  preds += -0.5


  preds_orig = np.zeros(preds.shape)
  if center is not None and scale is not None:
    for i in range(hm.shape[0]):
      for j in range(hm.shape[1]):
        preds_orig[i, j, :] = _transform(preds[i, j, :],
                                         center,
                                         scale,
                                         hm.shape[2],
                                         True)
  return preds, preds_orig


class FAN2DInputConfig(InputConfigAbstract):
  """ Input configuration """

  def __init__(self):
    """ Constructor """
    super(FAN2DInputConfig, self).__init__([InputSpec(dims=(3, 256, 256),
                                                      fmt=ImageFormat.RGB,
                                                      name='input')])
    self._preprocess_fn = Lambda(lambda x: tf.transpose(x, (0, 3, 1, 2)),
                               name='preprocessing')
    self._revert_preprocess_fn = Lambda(lambda x: tf.transpose(x, (0, 2, 3, 1)),
                                        name='revert_preprocssing')

  @property
  def preprocessing_fn(self):
    """
    Provide pre-processing layer
    :return:  Instance of `tf.keras.layer.Layer`
    """
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


class FAN2D(FrozenGraph):
  """ Face alignment network """

  def __init__(self, name='Fan2D', **kwargs):
    """
    Constructor
    :param name:    Network's name
    :param kwargs:  Extra keyword arguments
    """
    super(FAN2D, self).__init__(tensor_input_names='image:0',
                                tensor_output_names='transpose_647:0',
                                config=FAN2DInputConfig(),
                                name=name,
                                **kwargs)

  def get_landmark_from_image(self, image_or_path, face_bboxes):
    """ Predict the landmarks for each face present in the image.

    This function predicts a set of 68 2D landmarks, one for each image present.

    :param image_or_path: The input image or path to it.
    :param face_bboxes:   list of bounding boxes, one for each face found
                          in the image
    :return:  Set of landmarks
    """
    # Check input type
    if isinstance(image_or_path, str):
      try:
        image = np.asarray(imread(image_or_path)).astype(np.float32)
      except IOError:
        print("error opening file :: ", image_or_path)
        return None
    elif isinstance(image_or_path, np.ndarray):
      image = image_or_path.astype(np.float32)
    else:
      raise ValueError('`image_or_path` must be a `numpy.ndarray` or `str`')

    # Check if bboxes are provided
    if len(face_bboxes) == 0:
      print("Warning: No faces were detected.")
      return None

    # Run network
    landmarks = []
    for i, d in enumerate(face_bboxes):
      # Compute bbox center
      center = np.asarray([d[2] - (d[2] - d[0]) / 2.0,
                           d[3] - (d[3] - d[1]) / 2.0],
                          dtype=np.float32)
      center[1] = center[1] - (d[3] - d[1]) * 0.12
      scale = (d[2] - d[0] + d[3] - d[1]) / 195.0

      # Crop face region
      img_cropped = _crop(image, center, scale).astype(np.float32)
      # normalize + convert to CHW format (i.e. pytorch format)
      # Conversion to NCHW format is done internally in `FrozenGraph'
      # img_cropped = img_cropped.transpose((2, 0, 1))
      img_cropped /= 255.0
      img_cropped = np.expand_dims(img_cropped, 0)

      # Run network
      out = self(tf.convert_to_tensor(img_cropped))

      # Convert heatmap to points
      pts, pts_img = _get_preds_fromhm(out.numpy(), center, scale)
      pts, pts_img = pts.reshape(-1, 2) * 4.0, pts_img.reshape(-1, 2)
      landmarks.append(pts_img)
    return landmarks

