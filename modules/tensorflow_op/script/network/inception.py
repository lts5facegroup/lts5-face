# coding=utf-8
"""
Inception network from keras application:
  - https://github.com/keras-team/keras-applications
"""
from os.path import splitext, basename

import tensorflow as _tf
import tensorflow.keras.layers as layers
import tensorflow.keras.backend as K
from lts5.tensorflow_op.network.network import ImageFormat, Network
from lts5.tensorflow_op.network.network import InputSpec, InputConfigAbstract
from lts5.utils.files import get_file

__author__ = 'Christophe Ecabert'

# Inception V3 model taken from:
#  - keras-application: https://github.com/keras-team/keras-applications
WEIGHTS_COLLECTION = [
  # Top
  {'name': 'inception_v3',
   'url': 'https://github.com/fchollet/deep-learning-models/releases/download/'
          'v0.5/inception_v3_weights_tf_dim_ordering_tf_kernels.h5',
   'include_top': True,
   'md5': '9a0d58056eeedaa3f26cb7ebd46da564'},
  # No Top
  {'name': 'inception_v3',
   'url': 'https://github.com/fchollet/deep-learning-models/releases/download/'
          'v0.5/inception_v3_weights_tf_dim_ordering_tf_kernels_notop.h5',
   'include_top': False,
   'md5': 'bcbd6486424b2319ff4ef7d526e38f63'}
]


def conv2d_bn(x,
              filters,
              num_row,
              num_col,
              padding='same',
              strides=(1, 1),
              name=None):
  """
  Utility function to apply conv + BN.
  :param x:       Input tensor
  :param filters: Number of filters in `Conv2D`
  :param num_row: Height of the convolution kernel
  :param num_col: Width of the convolution kernel
  :param padding: Padding mode in `Conv2D`
  :param strides: Strides in `Conv2D`
  :param name:  name of the ops; will become `name + '_conv'` for the
                convolution and `name + '_bn'` for the batch norm layer.
  :return:  Output tensor after applying `Conv2D` and `BatchNormalization`.
  """
  if name is not None:
    bn_name = name + '_bn'
    conv_name = name + '_conv'
  else:
    bn_name = None
    conv_name = None
  # normalization axis
  bn_axis = 1 if K.image_data_format() == 'channels_first' else 3
  # Conv
  x = layers.Conv2D(filters,
                    (num_row, num_col),
                    strides=strides,
                    padding=padding,
                    use_bias=False,
                    name=conv_name)(x)
  x = layers.BatchNormalization(axis=bn_axis, scale=False, name=bn_name)(x)
  x = layers.Activation('relu', name=name)(x)
  return x


class InceptionV3InputConfig(InputConfigAbstract):

  def __init__(self):
    """ Constructor """
    super(InceptionV3InputConfig, self).__init__([InputSpec(dims=(299, 299, 3),
                                                            fmt=ImageFormat.RGB,
                                                            name='input')])

    def _preproces_fn(x):
      """ convert to [-1, 1] range"""
      x = (x - 127.5) / 127.5
      return x

    def _revert_fn(x):
      x = (x * 127.5) + 127.5
      return x

    self._preprocess_fn = layers.Lambda(_preproces_fn,
                                        name='preprocessing')
    self._revert__preprocess_fn = layers.Lambda(_revert_fn,
                                                name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert__preprocess_fn


class InceptionV3(Network):
  """ Inception network v3 """

  def __init__(self,
               input_tensor,
               include_top: bool,
               pooling: str,
               classes: int,
               name: str,
               **kwargs):
    """
    Constructor
    :param input_tensor: Optional Keras tensor (i.e. output of `layers.Input()`)
                         to use as image input for the model.
    :param include_top: Whether to include the fully-connected layer at the
                        top of the network.
    :param pooling: Optional pooling mode for feature extraction when
                    `include_top` is `False`.
                    - `None` means that the output of the model will be the 4D
                      tensor output of the last convolutional block.
                    - `avg` means that global average pooling will be applied
                      to the output of the last convolutional block, and thus
                      the output of the model will be a 2D tensor.
                    - `max` means that global max pooling will be applied.
    :param classes:  optional number of classes to classify images into,
                      only to be specified if `include_top` is True
    :param name:  Network's name
    :param kwargs:  Extra keyword arguments
    """

    model_cfg = InceptionV3InputConfig()
    self._include_top = include_top
    self._pooling = pooling
    self._classes = classes

    if input_tensor is None:
      img_input = layers.Input(shape=model_cfg.inputs_spec[0].dims,
                               name='input_' + name)
    else:
      # Wrapper tensor `input_tensor` into input layer
      if not K.is_keras_tensor(input_tensor):
        img_input = layers.Input(tensor=input_tensor,
                                 shape=input_tensor.shape)
      else:
        img_input = input_tensor

    # Build STEM
    # -------------------------------------------------------------------------
    channel_axis = 1 if K.image_data_format() == 'channels_first' else 3

    x = conv2d_bn(img_input, 32, 3, 3, strides=(2, 2), padding='valid')
    x = conv2d_bn(x, 32, 3, 3, padding='valid')
    x = conv2d_bn(x, 64, 3, 3)
    x = layers.MaxPooling2D((3, 3), strides=(2, 2))(x)

    x = conv2d_bn(x, 80, 1, 1, padding='valid')
    x = conv2d_bn(x, 192, 3, 3, padding='valid')
    x = layers.MaxPooling2D((3, 3), strides=(2, 2))(x)

    # Mixed 0: 35 x 35 x 256
    # -------------------------------------------------------------------------
    branch1x1 = conv2d_bn(x, 64, 1, 1)

    branch5x5 = conv2d_bn(x, 48, 1, 1)
    branch5x5 = conv2d_bn(branch5x5, 64, 5, 5)

    branch3x3dbl = conv2d_bn(x, 64, 1, 1)
    branch3x3dbl = conv2d_bn(branch3x3dbl, 96, 3, 3)
    branch3x3dbl = conv2d_bn(branch3x3dbl, 96, 3, 3)

    branch_pool = layers.AveragePooling2D((3, 3),
                                          strides=(1, 1),
                                          padding='same')(x)
    branch_pool = conv2d_bn(branch_pool, 32, 1, 1)
    x = layers.concatenate([branch1x1, branch5x5, branch3x3dbl, branch_pool],
                           axis=channel_axis,
                           name='mixed0')
    # Mixed 1: 35 x 35 x 288
    # -------------------------------------------------------------------------
    branch1x1 = conv2d_bn(x, 64, 1, 1)

    branch5x5 = conv2d_bn(x, 48, 1, 1)
    branch5x5 = conv2d_bn(branch5x5, 64, 5, 5)

    branch3x3dbl = conv2d_bn(x, 64, 1, 1)
    branch3x3dbl = conv2d_bn(branch3x3dbl, 96, 3, 3)
    branch3x3dbl = conv2d_bn(branch3x3dbl, 96, 3, 3)

    branch_pool = layers.AveragePooling2D((3, 3),
                                          strides=(1, 1),
                                          padding='same')(x)
    branch_pool = conv2d_bn(branch_pool, 64, 1, 1)
    x = layers.concatenate([branch1x1, branch5x5, branch3x3dbl, branch_pool],
                           axis=channel_axis,
                           name='mixed1')
    # Mixed 2: 35 x 35 x 288
    # -------------------------------------------------------------------------
    branch1x1 = conv2d_bn(x, 64, 1, 1)

    branch5x5 = conv2d_bn(x, 48, 1, 1)
    branch5x5 = conv2d_bn(branch5x5, 64, 5, 5)

    branch3x3dbl = conv2d_bn(x, 64, 1, 1)
    branch3x3dbl = conv2d_bn(branch3x3dbl, 96, 3, 3)
    branch3x3dbl = conv2d_bn(branch3x3dbl, 96, 3, 3)

    branch_pool = layers.AveragePooling2D((3, 3),
                                          strides=(1, 1),
                                          padding='same')(x)
    branch_pool = conv2d_bn(branch_pool, 64, 1, 1)
    x = layers.concatenate([branch1x1, branch5x5, branch3x3dbl, branch_pool],
                           axis=channel_axis,
                           name='mixed2')
    # Mixed 3: 17 x 17 x 768
    # -------------------------------------------------------------------------
    branch3x3 = conv2d_bn(x, 384, 3, 3, strides=(2, 2), padding='valid')

    branch3x3dbl = conv2d_bn(x, 64, 1, 1)
    branch3x3dbl = conv2d_bn(branch3x3dbl, 96, 3, 3)
    branch3x3dbl = conv2d_bn(
      branch3x3dbl, 96, 3, 3, strides=(2, 2), padding='valid')

    branch_pool = layers.MaxPooling2D((3, 3), strides=(2, 2))(x)
    x = layers.concatenate([branch3x3, branch3x3dbl, branch_pool],
                           axis=channel_axis,
                           name='mixed3')
    # Mixed 4: 17 x 17 x 768
    # -------------------------------------------------------------------------
    branch1x1 = conv2d_bn(x, 192, 1, 1)

    branch7x7 = conv2d_bn(x, 128, 1, 1)
    branch7x7 = conv2d_bn(branch7x7, 128, 1, 7)
    branch7x7 = conv2d_bn(branch7x7, 192, 7, 1)

    branch7x7dbl = conv2d_bn(x, 128, 1, 1)
    branch7x7dbl = conv2d_bn(branch7x7dbl, 128, 7, 1)
    branch7x7dbl = conv2d_bn(branch7x7dbl, 128, 1, 7)
    branch7x7dbl = conv2d_bn(branch7x7dbl, 128, 7, 1)
    branch7x7dbl = conv2d_bn(branch7x7dbl, 192, 1, 7)

    branch_pool = layers.AveragePooling2D((3, 3),
                                          strides=(1, 1),
                                          padding='same')(x)
    branch_pool = conv2d_bn(branch_pool, 192, 1, 1)
    x = layers.concatenate([branch1x1, branch7x7, branch7x7dbl, branch_pool],
                           axis=channel_axis,
                           name='mixed4')
    # Mixed 5, 6: 17 x 17 x 768
    # -------------------------------------------------------------------------
    for i in range(2):
      branch1x1 = conv2d_bn(x, 192, 1, 1)

      branch7x7 = conv2d_bn(x, 160, 1, 1)
      branch7x7 = conv2d_bn(branch7x7, 160, 1, 7)
      branch7x7 = conv2d_bn(branch7x7, 192, 7, 1)

      branch7x7dbl = conv2d_bn(x, 160, 1, 1)
      branch7x7dbl = conv2d_bn(branch7x7dbl, 160, 7, 1)
      branch7x7dbl = conv2d_bn(branch7x7dbl, 160, 1, 7)
      branch7x7dbl = conv2d_bn(branch7x7dbl, 160, 7, 1)
      branch7x7dbl = conv2d_bn(branch7x7dbl, 192, 1, 7)

      branch_pool = layers.AveragePooling2D((3, 3),
                                            strides=(1, 1),
                                            padding='same')(x)
      branch_pool = conv2d_bn(branch_pool, 192, 1, 1)
      x = layers.concatenate([branch1x1, branch7x7, branch7x7dbl, branch_pool],
                             axis=channel_axis,
                             name='mixed' + str(5 + i))
    # Mixed 7: 17 x 17 x 768
    # -------------------------------------------------------------------------
    branch1x1 = conv2d_bn(x, 192, 1, 1)

    branch7x7 = conv2d_bn(x, 192, 1, 1)
    branch7x7 = conv2d_bn(branch7x7, 192, 1, 7)
    branch7x7 = conv2d_bn(branch7x7, 192, 7, 1)

    branch7x7dbl = conv2d_bn(x, 192, 1, 1)
    branch7x7dbl = conv2d_bn(branch7x7dbl, 192, 7, 1)
    branch7x7dbl = conv2d_bn(branch7x7dbl, 192, 1, 7)
    branch7x7dbl = conv2d_bn(branch7x7dbl, 192, 7, 1)
    branch7x7dbl = conv2d_bn(branch7x7dbl, 192, 1, 7)

    branch_pool = layers.AveragePooling2D((3, 3),
                                          strides=(1, 1),
                                          padding='same')(x)
    branch_pool = conv2d_bn(branch_pool, 192, 1, 1)
    x = layers.concatenate([branch1x1, branch7x7, branch7x7dbl, branch_pool],
                           axis=channel_axis,
                           name='mixed7')
    # Mixed 8: 8 x 8 x 1280
    # -------------------------------------------------------------------------
    branch3x3 = conv2d_bn(x, 192, 1, 1)
    branch3x3 = conv2d_bn(branch3x3, 320, 3, 3,
                          strides=(2, 2), padding='valid')

    branch7x7x3 = conv2d_bn(x, 192, 1, 1)
    branch7x7x3 = conv2d_bn(branch7x7x3, 192, 1, 7)
    branch7x7x3 = conv2d_bn(branch7x7x3, 192, 7, 1)
    branch7x7x3 = conv2d_bn(branch7x7x3, 192, 3, 3,
                            strides=(2, 2),
                            padding='valid')

    branch_pool = layers.MaxPooling2D((3, 3), strides=(2, 2))(x)
    x = layers.concatenate([branch3x3, branch7x7x3, branch_pool],
                           axis=channel_axis,
                           name='mixed8')
    # Mixed 9: 8 x 8 x 2048
    # -------------------------------------------------------------------------
    for i in range(2):
      branch1x1 = conv2d_bn(x, 320, 1, 1)

      branch3x3 = conv2d_bn(x, 384, 1, 1)
      branch3x3_1 = conv2d_bn(branch3x3, 384, 1, 3)
      branch3x3_2 = conv2d_bn(branch3x3, 384, 3, 1)
      branch3x3 = layers.concatenate([branch3x3_1, branch3x3_2],
                                     axis=channel_axis,
                                     name='mixed9_' + str(i))

      branch3x3dbl = conv2d_bn(x, 448, 1, 1)
      branch3x3dbl = conv2d_bn(branch3x3dbl, 384, 3, 3)
      branch3x3dbl_1 = conv2d_bn(branch3x3dbl, 384, 1, 3)
      branch3x3dbl_2 = conv2d_bn(branch3x3dbl, 384, 3, 1)
      branch3x3dbl = layers.concatenate([branch3x3dbl_1, branch3x3dbl_2],
                                        axis=channel_axis)

      branch_pool = layers.AveragePooling2D((3, 3),
                                            strides=(1, 1),
                                            padding='same')(x)
      branch_pool = conv2d_bn(branch_pool, 192, 1, 1)
      x = layers.concatenate([branch1x1, branch3x3, branch3x3dbl, branch_pool],
                             axis=channel_axis,
                             name='mixed' + str(9 + i))

    if self._include_top:
      # Classification block
      x = layers.GlobalAveragePooling2D(name='avg_pool')(x)
      x = layers.Dense(classes,
                       activation='softmax',
                       name='predictions')(x)
    else:
      if self._pooling == 'avg':
        x = layers.GlobalAveragePooling2D()(x)
      elif self._pooling == 'max':
        x = layers.GlobalMaxPooling2D()(x)

    # Ensure that the model takes into account
    # any potential predecessors of `input_tensor`.
    if input_tensor is not None:
      inputs = _tf.keras.utils.get_source_inputs(input_tensor)
    else:
      inputs = img_input
    # CTOR
    super(InceptionV3, self).__init__(model_cfg,
                                      inputs,
                                      x,
                                      name=name,
                                      **kwargs)

  @classmethod
  def InceptionV3(cls,
                  input_tensor=None,
                  include_top=True,
                  pooling=None,
                  classes=1000,
                  **kwargs):
    """
    Build InceptionV3 network
    :param input_tensor: optional tensor (i.e. Input())
    :param include_top: If `True` add the classification layers
    :param pooling:     Type of pooling to use if top layers are not included
    :param classes:     Number of predicated class if top layres are included
    :param kwargs:  Extra keyword arguments
    :return:  Network
    """
    return cls(input_tensor=input_tensor,
               include_top=include_top,
               pooling=pooling,
               classes=classes,
               name='inception_v3',
               **kwargs)

  def load_weights(self, filepath, by_name=False):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights, or `imagenet` to use pre-trained network.
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    """

    def _get_url(name, include_top):
      """
      Retrieve model url
      :param name:        Model's name
      :param include_top: If top is included
      :return:  Lits of candidates or empty if no match is found
      """
      w = list(filter(lambda x: x['name'] == name, WEIGHTS_COLLECTION))
      w = list(filter(lambda x: x['include_top'] == include_top, w))
      return w

    if filepath == 'imagenet':
      # Get model weight
      url = _get_url(name=self.name, include_top=self._include_top)
      if url:
        url = url[0]  # Pick first element
        # Match known configuration
        weights_path = get_file(url['name'],
                                url['url'],
                                cache_subdir='models',
                                file_hash=url['md5'])
        # Reload
        super(InceptionV3, self).load_weights(weights_path)
      else:
        raise ValueError('There is no weights for such configuration: ' +
                         'model = {}'.format(self.name) +
                         'classes = {}, '.format(self.n_classes) +
                         'include_top = {}'.format(self.include_top))
    else:
      # Load default
      super(InceptionV3, self).load_weights(filepath, by_name)

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    return None

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    return []

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    return []
