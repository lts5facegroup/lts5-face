# coding=utf-8
"""
Resolution-aware MorphNet for 3D Face reconstruction in low resolution settings.
This is based on:
  - Self-Supervision
  - Contrastive Learning
See:
  - https://arxiv.org/abs/2007.13666
"""
import tensorflow as tf
import numpy as np
from tensorflow.python.keras.engine import data_adapter
from tensorflow.keras.losses import Loss
from tensorflow.keras.metrics import Mean
from lts5.tensorflow_op.layers.queue import MoCoQueue
from lts5.tensorflow_op.network.morphnet import MorphNet, MorphNetParameter

__author__ = 'Christophe Ecabert'


def _pack_parameters(params: list):
  """
  Combine a list of parameters into a single tensor
  :param params:  List of Tensor of Rank3 or Rank2
  :return:  Combined Rank2 tensor
  """
  elems = []
  for p in params:
    if p.shape.rank != 2:
      # p needs to be flatten
      flat_sz = np.prod(p.shape[1:])
      p = tf.reshape(p, (-1, flat_sz))
    elems.append(p)
  return tf.concat(elems)


class ParameterSelfSupervision(Loss):
  """
  Parameter Self-Supervision Loss: Ensure parameters across different resolution
  level are the same, or close. More over gradient is only propagated to lower
  resolution level.
  """

  def __init__(self,
               weight: float,
               name='ParameterSelfSupervision',
               **kwargs):
    """
    Constructor
    :param weight:  Loss contribution
    :param name:    Loss name
    :param kwargs:  Extra keyword arguments
    """
    super(ParameterSelfSupervision, self).__init__(name=name, **kwargs)
    self._weight = weight
    self._built = False
    self._n_resolution = None

  def _build(self, y_pred):
    """
    Build loss
    :param y_pred:  List of rank-2 tensors
    """
    # Number of resolutions
    self._n_resolution = len(y_pred)
    # Create metrics here ?
    for i in range(self._n_resolution):
      for j in range(i + 1, self._n_resolution):
        metric_name = 'ParamDiff({}, {})'.format(i, j)
        setattr(self,
                'param_diff_{}_{}_loss'.format(i, j),
                Mean(metric_name))
    # Mark as built
    self._built = True

  def call(self, y_true, y_pred):
    """
    Compute
    :param y_true: Not used, should be `None`
    :param y_pred:  List of Tensor of shape [B, #TotalParams] in decreasing
      order of resolution
    :return: Loss, scalar
    """
    # Init loss
    if not self._built:
      self._build(y_pred)
    # Compute loss
    loss_level = []
    batch_dim = tf.shape(y_pred[0])[0]
    for i in range(self._n_resolution):
      for j in range(i + 1, self._n_resolution):
        # Compute parameters difference between resolution level
        # Do not propagate grad in higher resolution
        x_i = tf.stop_gradient(y_pred[i])
        x_j = y_pred[j]
        # Define weight
        w_ij = tf.cast(j - i, tf.float32)
        level_err = w_ij * tf.keras.losses.mean_squared_error(x_i, x_j)
        # Update metrics here, taken from `LossesContainer`
        m = getattr(self, 'param_diff_{}_{}_loss'.format(i, j))
        m.update_state(level_err, sample_weight=batch_dim)
        # Store
        loss_level.append(level_err)
    return self._weight * tf.add_n(loss_level)

  @property
  def metrics(self):
    """ Per-level loss metrics """
    if not self._built:
      return []
    per_level_metrics = []
    for i in range(self._n_resolution):
      for j in range(i + 1, self._n_resolution):
        m = getattr(self, 'param_diff_{}_{}_loss'.format(i, j))
        per_level_metrics.append(m)
    return per_level_metrics


class FeatureContrastiveLoss(Loss):
  """
  Contrastive loss to ensure similarity in the feature space across different
  resolution level
  """

  def __init__(self,
               weight,
               size: int,
               temperature: float,
               initializer=None,
               normalized: bool = True,
               top_k: int = 5,
               name='FeatureContrastiveLoss',
               **kwargs):
    """
    Constructor
    :param weight:  Loss contribution
    :param size:        Size of the queue. Note, it must be a multiple of the
      batch size.
    :param temperature: Temperature scaling to use in Info-NCE term
    :param initializer: Initializer instance (callable)
    :param normalized:  If `True` indicates that features are normalized. If
      `False` L2 normalization needs to be applied
    :param top_k:       Number of top elements to look at for computing accuracy
    :param name:    Loss name
    :param kwargs:  Extra keyword arguments
    """
    super(FeatureContrastiveLoss, self).__init__(name=name, **kwargs)
    self._weight = weight
    self._size = size
    self._temp = temperature
    self._queue_init = initializer
    self._normalized = normalized
    self._k = top_k
    self._built = False
    self._n_resolution = None
    self._training = tf.constant(False, name='training_flag')

  def _build(self, y_pred):
    """
    Build loss
    :param y_pred:  List of rank-2 tensors
    """
    # Number of resolutions
    self._n_resolution = len(y_pred)
    # Create MoCo queue for each pair of resolution level and metrics
    for i in range(self._n_resolution):
      for j in range(i + 1, self._n_resolution):
        # Moco queue
        loss_name = 'FeatureMocoQueue({}, {})'.format(i, j)
        setattr(self,
                'queue_{}_{}_loss'.format(i, j),
                MoCoQueue(size=self._size,
                          temperature=self._temp,
                          initializer=self._queue_init,
                          normalized=self._normalized,
                          top_k=self._k,
                          name=loss_name))
        # Metric
        metric_name = 'FeatureContrast({}, {})'.format(i, j)
        setattr(self,
                'feat_queue_{}_{}_loss'.format(i, j),
                Mean(metric_name))
    # Mark as built
    self._built = True

  def call(self, y_true, y_pred):
    """
    Compute loss
    :param y_true:  Not used.
    :param y_pred:  List of rank-2 tensor of features.
    :return:  Loss
    """
    # Init loss
    if not self._built:
      self._build(y_pred)
    # Compute loss
    loss_level = []
    batch_dim = tf.shape(y_pred[0])[0]
    for i in range(self._n_resolution):
      for j in range(i + 1, self._n_resolution):
        # Compute loss
        loss_obj = getattr(self, 'queue_{}_{}_loss'.format(i, j))
        x_i = y_pred[i]
        x_j = y_pred[j]
        # Define weight
        w_ij = tf.cast(j - i, tf.float32)
        level_err = w_ij * loss_obj([x_j, x_i], self._training)
        # Update metrics here, taken from `LossesContainer`
        m = getattr(self, 'feat_queue_{}_{}_loss'.format(i, j))
        m.update_state(level_err, sample_weight=batch_dim)
        # Store
        loss_level.append(level_err)
    return self._weight * tf.add_n(loss_level)

  def set_training(self, flag: bool):
    """
    Update training flag for MoCo queue
    :param flag:  If `True` indicates training, otherwise not.
    """
    self._training = tf.constant(flag, name='training_flag')

  @property
  def metrics(self):
    """ Per-level loss metrics """
    if not self._built:
      return []
    per_level_metrics = []
    for i in range(self._n_resolution):
      for j in range(i + 1, self._n_resolution):
        # Error metrics
        m = getattr(self, 'feat_queue_{}_{}_loss'.format(i, j))
        per_level_metrics.append(m)
        # TopK accuracy metrics
        l = getattr(self, 'queue_{}_{}_loss'.format(i, j))
        per_level_metrics.extend(l.metrics)
    return per_level_metrics


class MorphNetResAware(MorphNet):
  """ Resolution-aware MorphNet """

  def __init__(self,
               config: MorphNetParameter,
               manager=None,
               input_names=None,
               output_names=None,
               output_mid_value: bool = False,
               name: str = 'MorphNetResAware',
               **kwargs):
    """
    Constructor
    :param config:        Network configuration
    :param manager:       Summary manager
    :param input_names:   List/tuple of name to gives to the input tensors.
                          If `None` default to 'input_{k}'
    :param output_names:  List/tuple of name to gives to the output tensors
                          If `None` default to 'output_{k}'
    :param output_mid_value: If `True` forward call will output intermediate
      values (i.e. feature, parameters) as well as image/landmarks
    :param name:          Top level name scope
    :param kwargs:        Extra keyword arguments
    """
    # Sanity check
    if config.image_resolution is None:
      raise ValueError('`config.image_resolution` can\'t be `None` otherwise'
                       ' use `MorphNet` class instead.')

    # Build MorphNet
    super(MorphNetResAware, self).__init__(config=config,
                                           manager=manager,
                                           input_names=input_names,
                                           output_names=output_names,
                                           output_mid_value=output_mid_value,
                                           name=name,
                                           **kwargs)
    # Add extra losses
    self._params_loss = ParameterSelfSupervision(weight=1.0)
    self._feat_loss = FeatureContrastiveLoss(weight=1.0,
                                             size=self._c_reg.queue_size,
                                             temperature=self._c_reg.temp_id,
                                             normalized=False)
    # Extra metrics ? what is needed ?

  @property
  def metrics(self):
    """ List all metrics """
    base_metrics = super(MorphNetResAware, self).metrics()
    # Add metrics from losses
    base_metrics.extend(self._params_loss.metrics)
    base_metrics.extend(self._feat_loss.metrics)
    return base_metrics

  def train_step(self, data):
    """The logic for one training step.

    This method can be overridden to support custom training logic.
    This method is called by `Model.make_train_function`.

    This method should contain the mathemetical logic for one step of training.
    This typically includes the forward pass, loss calculation, backpropagation,
    and metric updates.

    Configuration details for *how* this logic is run (e.g. `tf.function` and
    `tf.distribute.Strategy` settings), should be left to
    `Model.make_train_function`, which can also be overridden.

    :param data: A nested structure of `Tensor`s.
    :return: A `dict` containing values that will be passed to
            `tf.keras.callbacks.CallbackList.on_train_batch_end`. Typically, the
            values of the `Model`'s metrics are returned.
            Example: `{'loss': 0.2, 'accuracy': 0.7}`.
    """
    # These are the only transformations `Model.fit` applies to user-input
    # data when a `tf.data.Dataset` is provided. These utilities will be exposed
    # publicly.
    data = data_adapter.expand_1d(data)
    x, y, sample_weight = data_adapter.unpack_x_y_sample_weight(data)

    # x == image, 5D Tensor where first dimensions is the resolution level,
    # 0 == High resolution, 1... == lower resolution
    # y == ground truth (i.e. image + landmarks)
    self._feat_loss.set_training(True)

    # Compute base loss for every level
    with tf.GradientTape() as tape:
      # Network outputs: Image + landmarks
      outputs = []
      # ResNet features after AveragePooling [B, 2048]
      features = []
      # Regressed parameters: w_shp, w_tex, w_illu, rv, tv ...
      parameters = []
      # Losses to constraint each level to produce correct output
      losses = []
      bsize = tf.shape(x)[1]
      res_level = tf.ones(shape=bsize, dtype=tf.float32)
      for i in range(self.n_resolution):
        # Forward, generate outputs for each levels
        res_idx = res_level * i
        image = x[i, ...]
        output, feat, param = self([image, res_idx], training=True)

        # Compute basic loss in order to have each resolution level to output
        # correct predictions
        inner_loss = self.get_losses_for([image, res_idx])
        basic_loss = self.compiled_loss(y_true=y,
                                        y_pred=output,
                                        sample_weight=sample_weight,
                                        regularization_losses=inner_loss)
        # Save outputs + losses
        outputs.append(output)
        features.append(feat)
        parameters.append(_pack_parameters(param))
        losses.append(basic_loss)

      # Constraint model's parameters across different resolution level
      loss_params = self._params_loss(y_true=None, y_pred=parameters)
      losses.append(loss_params)

      # Constraint model's features to be similar across different resolution
      # level -> Contrastive learning
      loss_feat = self._feat_loss(y_true=None, y_pred=features)
      losses.append(loss_feat)
      # Combine all loss together
      loss = tf.add_n(losses)
    # Compute grad
    train_vars = self.trainable_variables
    gradients = tape.gradient(loss, train_vars)
    self.optimizer.apply_gradients(zip(gradients, train_vars))

    # Return metrics
    return {m.name: m.result() for m in self.metrics}

  def test_step(self, data):
    """
    The logic for one evaluation step.

    This method can be overridden to support custom evaluation logic.
    This method is called by `Model.make_test_function`.

    This function should contain the mathemetical logic for one step of
    evaluation.
    This typically includes the forward pass, loss calculation, and metrics
    updates.

    Configuration details for *how* this logic is run (e.g. `tf.function` and
    `tf.distribute.Strategy` settings), should be left to
    `Model.make_test_function`, which can also be overridden.

    :param data:  A nested structure of `Tensor`s.
    :return:  A `dict` containing values that will be passed to
      `tf.keras.callbacks.CallbackList.on_train_batch_end`. Typically, the
      values of the `Model`'s metrics are returned.
    """

    data = data_adapter.expand_1d(data)
    x, y, sample_weight = data_adapter.unpack_x_y_sample_weight(data)

    # x == image, 5D Tensor where first dimensions is the resolution level,
    # 0 == High resolution, 1... == lower resolution
    # y == ground truth (i.e. image + landmarks)
    self._feat_loss.set_training(False)

    # Network outputs: Image + landmarks
    outputs = []
    # ResNet features after AveragePooling [B, 2048]
    features = []
    # Regressed parameters: w_shp, w_tex, w_illu, rv, tv ...
    parameters = []
    bsize = tf.shape(x)[1]
    res_level = tf.ones(shape=bsize, dtype=tf.float32)
    for i in range(self.n_resolution):
      # Forward, generate outputs for each levels
      res_idx = res_level * i
      image = x[i, ...]
      output, feat, param = self([image, res_idx], training=False)

      # Updates stateful loss metrics.
      inner_loss = self.get_losses_for([image, res_idx])
      self.compiled_loss(y_true=y,
                         y_pred=output,
                         sample_weight=sample_weight,
                         regularization_losses=inner_loss)
      self.compiled_metrics.update_state(y, output, sample_weight)
      # Save outputs + losses
      outputs.append(output)
      features.append(feat)
      parameters.append(_pack_parameters(param))
    # Constraint model's parameters across different resolution level
    self._params_loss(y_true=None, y_pred=parameters)
    # Constraint model's features to be similar across different resolution
    # level -> Contrastive learning
    self._feat_loss(y_true=None, y_pred=features)
    # Get results
    return {m.name: m.result() for m in self.metrics}

