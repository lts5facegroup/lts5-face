# coding=utf-8
"""
Face embedding network FaceNet implementation

based on:
 - http://arxiv.org/abs/1602.07261
 - https://github.com/nyoki-mtl/keras-facenet
 - https://github.com/davidsandberg/facenet
"""
from os.path import exists as _exists
from os.path import join as _join
from os import makedirs as _mkdir
import re
from datetime import datetime
import tensorflow as tf
import tensorflow.keras.layers as kl
import tensorflow.keras.backend as K
from lts5.tensorflow_op.network import create_layer
from lts5.tensorflow_op.network import InputConfigAbstract, InputSpec
from lts5.tensorflow_op.network import ImageFormat, Network
from lts5.tensorflow_op.image.util import smart_resize

__author__ = 'Christophe Ecabert'


def _generate_layer_name(name, branch_idx=None, prefix=None):
  """
  Generate layer's name
  :param name:        Name
  :param branch_idx:  Branch index if any
  :param prefix:      Prefix
  :return:  Name
  """
  if prefix is None:
    return None
  if branch_idx is None:
    return '_'.join((prefix, name))
  return '_'.join((prefix, 'Branch', str(branch_idx), name))


def _scaling(x, factor):
  """
  Scale tensor `x` by a given factor
  :param x:       Tensor to scale
  :param factor:  Scaling factor
  :return:  `x` * `factor`
  """
  return x * factor


def _conv2d_bn(x,
               filters,
               kernel_size,
               strides=1,
               padding='same',
               activation='relu',
               use_bias=False,
               name=None,
               **kwargs):
  """
  Create a Conv2D layer and optionally add BatchNorm + Activation layer
  :param x:             Input tensor
  :param filters:       Number of filters in the Conv2D layer
  :param kernel_size:   Dimensions of the kernel in Conv2D
  :param strides:       Strides for Conv2D
  :param padding:       Padding type to use for the convolution
  :param activation:    Optional activation function to use
  :param use_bias:      If `True` add bias to the Conv2D layer, otherwise use
                        BatchNorm instead.
  :param name:          Convolution layer's name
  :param kwargs:        Extra keyword arguments
  :keyword layer_params: Dictionary of extra parameters for a given layer
  :return:              Transformed tensors
  """

  # Convolution
  x = create_layer(kl.Conv2D,
                   filters=filters,
                   kernel_size=kernel_size,
                   strides=strides,
                   padding=padding,
                   use_bias=use_bias,
                   name=name,
                   **kwargs)(x)
  # Add BatchNorm ?
  if not use_bias:
    bn_axis = 1 if K.image_data_format() == 'channels_first' else 3
    bn_name = _generate_layer_name('BatchNorm', prefix=name)
    x = create_layer(kl.BatchNormalization,
                     axis=bn_axis,
                     momentum=0.995,
                     epsilon=0.001,
                     scale=False,
                     name=bn_name,
                     **kwargs)(x)
  if activation is not None:
    ac_name = _generate_layer_name('Activation', prefix=name)
    x = create_layer(kl.Activation,
                     activation=activation,
                     name=ac_name,
                     **kwargs)(x)
  return x


def _inception_resnet_block(x,
                            scale,
                            block_type,
                            block_idx,
                            activation='relu',
                            **kwargs):
  """
  Create ResNetInception block
  :param x:           Input data of the block
  :param scale:       Scaling factor
  :param block_type:  Type of block: `Block35`, `Block17`, or `Block8`
  :param block_idx:   Block index
  :param activation:  Activation function to use
  :param kwargs:      Extra keyword arguments
  :keyword layer_params: Dictionary of extra parameters for a given layer
  :return:  Transformed tensor
  """

  channel_axis = 1 if K.image_data_format() == 'channels_first' else 3
  if block_idx is None:
    prefix = None
  else:
    prefix = '_'.join((block_type, str(block_idx)))

  # Create blocks
  if block_type == 'Block35':
    lname = _generate_layer_name('Conv2d_1x1', 0, prefix)
    branch_0 = _conv2d_bn(x, 32, 1, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0a_1x1', 1, prefix)
    branch_1 = _conv2d_bn(x, 32, 1, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0b_3x3', 1, prefix)
    branch_1 = _conv2d_bn(branch_1, 32, 3, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0a_1x1', 2, prefix)
    branch_2 = _conv2d_bn(x, 32, 1, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0b_3x3', 2, prefix)
    branch_2 = _conv2d_bn(branch_2, 32, 3, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0c_3x3', 2, prefix)
    branch_2 = _conv2d_bn(branch_2, 32, 3, name=lname, **kwargs)
    branches = [branch_0, branch_1, branch_2]
  elif block_type == 'Block17':
    lname = _generate_layer_name('Conv2d_1x1', 0, prefix)
    branch_0 = _conv2d_bn(x, 128, 1, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0a_1x1', 1, prefix)
    branch_1 = _conv2d_bn(x, 128, 1, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0b_1x7', 1, prefix)
    branch_1 = _conv2d_bn(branch_1, 128, [1, 7], name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0c_7x1', 1, prefix)
    branch_1 = _conv2d_bn(branch_1, 128, [7, 1], name=lname, **kwargs)
    branches = [branch_0, branch_1]
  elif block_type == 'Block8':
    lname = _generate_layer_name('Conv2d_1x1', 0, prefix)
    branch_0 = _conv2d_bn(x, 192, 1, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0a_1x1', 1, prefix)
    branch_1 = _conv2d_bn(x, 192, 1, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0b_1x3', 1, prefix)
    branch_1 = _conv2d_bn(branch_1, 192, [1, 3], name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0c_3x1', 1, prefix)
    branch_1 = _conv2d_bn(branch_1, 192, [3, 1], name=lname, **kwargs)
    branches = [branch_0, branch_1]
  else:
    raise ValueError('Unknown Inception-ResNet block type. '
                     'Expects "Block35", "Block17" or "Block8", '
                     'but got: ' + str(block_type))

  # Fuse branches
  mixed = create_layer(kl.Concatenate,
                       axis=channel_axis,
                       name=_generate_layer_name('Concatenate', prefix=prefix),
                       **kwargs)(branches)
  up = _conv2d_bn(mixed,
                  K.int_shape(x)[channel_axis],
                  1,
                  activation=None,
                  use_bias=True,
                  name=_generate_layer_name('Conv2d_1x1', prefix=prefix),
                  **kwargs)
  up = create_layer(kl.Lambda,
                    function=_scaling,
                    output_shape=K.int_shape(up)[1:],
                    arguments={'factor': scale},
                    name=_generate_layer_name('Scaling', prefix=prefix),
                    **kwargs)(up)
  x = create_layer(kl.Add,
                   name=_generate_layer_name('Add', prefix=prefix),
                   **kwargs)([x, up])
  if activation is not None:
    x = create_layer(kl.Activation,
                     activation=activation,
                     name=_generate_layer_name('Activation', prefix=prefix),
                     **kwargs)(x)
  return x


def _inception_reduction_block(x,
                               reduction_type,
                               name,
                               **kwargs):
  """
  Create Inception reduction block
  :param x:               Input tensor
  :param reduction_type:  Type of reduction: `A` or `B`
  :param name:            Reduction's name
  :param kwargs:          Extra keyword arguments
  :return:  Transformed tensors
  """
  channel_axis = 1 if K.image_data_format() == 'channels_first' else 3

  # Build reduction
  if reduction_type == 'A':
    # Branch 0
    lname = _generate_layer_name('Conv2d_1a_3x3', 0, prefix=name)
    branch_0 = _conv2d_bn(x, 384, 3, strides=2, padding='valid',
                          name=lname, **kwargs)
    # Branch 1
    lname = _generate_layer_name('Conv2d_0a_1x1', 1, prefix=name)
    branch_1 = _conv2d_bn(x, 192, 1, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0b_3x3', 1, prefix=name)
    branch_1 = _conv2d_bn(branch_1, 192, 3, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_1a_3x3', 1, prefix=name)
    branch_1 = _conv2d_bn(branch_1, 256, 3, strides=2, padding='valid',
                          name=lname, **kwargs)
    # Branch 2
    lname = _generate_layer_name('MaxPool_1a_3x3', 2, prefix=name)
    branch_pool = create_layer(kl.MaxPooling2D,
                               pool_size=3,
                               strides=2,
                               padding='valid',
                               name=lname,
                               **kwargs)(x)
    branches = [branch_0, branch_1, branch_pool]
  elif reduction_type == 'B':
    # Branch 0
    lname = _generate_layer_name('Conv2d_0a_1x1', 0, prefix=name)
    branch_0 = _conv2d_bn(x, 256, 1, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_1a_3x3', 0, prefix=name)
    branch_0 = _conv2d_bn(branch_0, 384, 3, strides=2, padding='valid',
                          name=lname, **kwargs)
    # Branch 1
    lname = _generate_layer_name('Conv2d_0a_1x1', 1, prefix=name)
    branch_1 = _conv2d_bn(x, 256, 1, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_1a_3x3', 1, prefix=name)
    branch_1 = _conv2d_bn(branch_1, 256, 3, strides=2, padding='valid',
                          name=lname, **kwargs)
    # Branch 2
    lname = _generate_layer_name('Conv2d_0a_1x1', 2, prefix=name)
    branch_2 = _conv2d_bn(x, 256, 1, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_0b_3x3', 2, prefix=name)
    branch_2 = _conv2d_bn(branch_2, 256, 3, name=lname, **kwargs)
    lname = _generate_layer_name('Conv2d_1a_3x3', 2, prefix=name)
    branch_2 = _conv2d_bn(branch_2, 256, 3, strides=2, padding='valid',
                          name=lname, **kwargs)
    # Branch 3
    lname = _generate_layer_name('MaxPool_1a_3x3', 3, prefix=name)
    branch_pool = create_layer(kl.MaxPooling2D,
                               pool_size=3,
                               strides=2,
                               padding='valid',
                               name=lname,
                               **kwargs)(x)
    branches = [branch_0, branch_1, branch_2, branch_pool]
  else:
    raise ValueError('Unknown Inception-ResNet reduction type. '
                     'Expects "A" or "B", '
                     'but got: ' + str(reduction_type))
  # Fuse them
  x = create_layer(kl.Concatenate,
                   axis=channel_axis,
                   name=name,
                   **kwargs)(branches)
  return x


# regex for renaming the tensors to their corresponding Keras counterpart
_re_repeat = re.compile(r'Repeat_[0-9_]*b')
_re_block8 = re.compile(r'Block8_[A-Za-z]')


def get_weight_name(key):
  """
  Convert a given key into proper weight name (i.e. used by our corresponding
  keras layer
  https://github.com/nyoki-mtl/keras-facenet/blob/master/notebook/tf_to_keras.ipynb
  :param key: Weight name to convert
  :return:  Weight's name
  """
  kname = str(key)
  kname = kname.replace('/', '_')
  kname = kname.replace('InceptionResnetV1_', '')
  # remove "Repeat" scope from kname
  kname = _re_repeat.sub('B', kname)
  if _re_block8.match(kname):
    # the last block8 has different name with the previous 5 occurrences
    kname = kname.replace('Block8', 'Block8_6')
  # from TF to Keras naming
  kname = kname.replace('_weights', '_kernel')
  kname = kname.replace('_biases', '_bias')
  return kname


class FaceNetInputConfig(InputConfigAbstract):
  """ FaceNet input configuration """

  def __init__(self):
    """ Constructor """
    super(FaceNetInputConfig, self).__init__([InputSpec(dims=(160, 160, 3),
                                                        fmt=ImageFormat.RGB,
                                                        name='input')])

    def _pre_process_fn(x):
      """  Pre-processing """
      # Resize
      x = smart_resize(tf.cast(x, tf.float32), [160, 160])
      # move to [-1, 1] range
      # https://github.com/davidsandberg/facenet/blob/096ed770f163957c1e56efa7feeb194773920f6e/src/facenet.py#L120
      x = (x - 127.5) / 128.0
      return x

    def _revert_pre_process_fn(x):
      x = (128.0 * x) + 127.5
      return x

    self._preprocess_fn = kl.Lambda(_pre_process_fn,
                                    name='preprocessing')
    self._revert_preprocess_fn = kl.Lambda(_revert_pre_process_fn,
                                           name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


class FaceNet(Network):
  """ FaceNet architecture based on ResNet-Inception """

  def __init__(self,
               input_tensor=None,
               embedding_size=512,
               dropout_rate=0.2,
               name='FaceNet',
               **kwargs):
    """
    Constructor
    :param input_tensor: optional tensor (i.e. Input())
    :param embedding_size:  Size of the embedding
    :param dropout_rate:    Dropout rate
    :param name:            Network's name
    :param kwargs:          Extra keyword arguments
    :keyword layer_params:  Per-layer configuration changes
    """

    # Config
    model_cfg = FaceNetInputConfig()
    self.embedding_sz = embedding_size
    self.drop_rate = dropout_rate

    # Input
    if input_tensor is None:
      input_t = kl.Input(shape=model_cfg.inputs_spec[0].dims,
                         name='input_' + name)
    else:
      if not tf.keras.backend.is_keras_tensor(input_tensor):
        # Wrapper tensor `input_tensor` into input layer
        input_t = kl.Input(tensor=input_tensor, shape=input_tensor.shape)
      else:
        input_t = input_tensor

    # FaceNet structure
    x = _conv2d_bn(input_t, 32, 3, strides=2, padding='valid',
                   name='Conv2d_1a_3x3',
                   **kwargs)
    x = _conv2d_bn(x, 32, 3, padding='valid',
                   name='Conv2d_2a_3x3',
                   **kwargs)
    x = _conv2d_bn(x, 64, 3,
                   name='Conv2d_2b_3x3',
                   **kwargs)
    x = create_layer(kl.MaxPooling2D,
                     pool_size=3,
                     strides=2,
                     name='MaxPool_3a_3x3',
                     **kwargs)(x)
    x = _conv2d_bn(x,
                   80, 1, padding='valid',
                   name='Conv2d_3b_1x1',
                   **kwargs)
    x = _conv2d_bn(x, 192, 3, padding='valid',
                   name='Conv2d_4a_3x3',
                   **kwargs)
    x = _conv2d_bn(x, 256, 3, strides=2, padding='valid',
                   name='Conv2d_4b_3x3',
                   **kwargs)

    # 5x Block35 (Inception-ResNet-A block):
    for block_idx in range(1, 6):
      x = _inception_resnet_block(x,
                                  scale=0.17,
                                  block_type='Block35',
                                  block_idx=block_idx,
                                  **kwargs)
    # Mixed 6a (Reduction-A block):
    x = _inception_reduction_block(x,
                                   reduction_type='A',
                                   name='Mixed_6a',
                                   **kwargs)

    # 10x Block17 (Inception-ResNet-B block):
    for block_idx in range(1, 11):
      x = _inception_resnet_block(x,
                                  scale=0.1,
                                  block_type='Block17',
                                  block_idx=block_idx,
                                  **kwargs)

    # Mixed 7a (Reduction-B block): 8 x 8 x 2080
    x = _inception_reduction_block(x,
                                   reduction_type='B',
                                   name='Mixed_7a',
                                   **kwargs)

    # 5x Block8 (Inception-ResNet-C block):
    for block_idx in range(1, 6):
      x = _inception_resnet_block(x,
                                  scale=0.2,
                                  block_type='Block8',
                                  block_idx=block_idx,
                                  **kwargs)
    x = _inception_resnet_block(x,
                                scale=1.,
                                activation=None,
                                block_type='Block8',
                                block_idx=6,
                                **kwargs)

    # Emmbeding block
    x = create_layer(kl.GlobalAveragePooling2D,
                     name='AvgPool',
                     **kwargs)(x)
    x = create_layer(kl.Dropout,
                     rate=self.drop_rate,
                     name='Dropout',
                     **kwargs)(x)
    # Bottleneck
    x = create_layer(kl.Dense,
                     units=self.embedding_sz,
                     use_bias=False,
                     name='Bottleneck',
                     **kwargs)(x)
    bn_name = _generate_layer_name('BatchNorm', prefix='Bottleneck')
    x = create_layer(kl.BatchNormalization,
                     momentum=0.995,
                     epsilon=0.001,
                     scale=False,
                     name=bn_name,
                     **kwargs)(x)

    # Ensure that the model takes into account any potential predecessors of
    # `input_tensor`.
    if input_tensor is not None:
      inputs = tf.keras.utils.get_source_inputs(input_tensor)
    else:
      inputs = input_t

    # Create model
    kwargs.pop('layer_params', None)
    super(FaceNet, self).__init__(model_cfg, inputs, x, name=name, **kwargs)

  @staticmethod
  def convert_weight_to_keras(ckpt_file, output_dir, embedding_size=512):
    """
    Convert tensorflow checkpoint to keras model file
    https://github.com/nyoki-mtl/keras-facenet/blob/master/notebook/tf_to_keras.ipynb
    :param ckpt_file: Path to checkpoint file (i.e. *.ckpt)
    :param output_dir:  Location where to place the converted model
    :param embedding_size: Dimension of the model's embedding
    """
    # Read checkpoint file (tf v1)
    weight_map = {}
    reader = tf.train.load_checkpoint(ckpt_file)
    for key in reader.get_variable_to_shape_map():
      # not saving the following tensors
      if key == 'global_step':
        continue
      if 'AuxLogit' in key:
        continue
      if 'Logits' in key:
        continue
      # convert tensor name into the corresponding Keras layer weight name
      # and save
      weight_name = get_weight_name(key)
      arr = reader.get_tensor(key)
      weight_map[weight_name] = arr

    # Create keras model and set manually each weights
    model = FaceNet(embedding_size=embedding_size)
    for layer in model.layers:
      if layer.weights:
        weights = []
        for w in layer.weights:
          weight_name = w.name.split('/')[-1].replace(':0', '')
          weight_name = layer.name + '_' + weight_name
          weight_arr = weight_map[weight_name]
          weights.append(weight_arr)
        layer.set_weights(weights)

    model_dir = 'face_net_' + datetime.today().strftime('%Y_%m_%d')
    model_dir = _join(output_dir, model_dir)
    if not _exists(model_dir):
      _mkdir(model_dir)
    model.save_weights(_join(model_dir, 'FaceNet'))

  def load_weights(self, filepath, by_name=False, skip_mismatch=False):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights, or `imagenet` to use pre-trained network.
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    """
    return super(FaceNet, self).load_weights(filepath,
                                             by_name=by_name,
                                             skip_mismatch=skip_mismatch)

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    return None

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    return []

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    return []
