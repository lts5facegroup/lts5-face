# coding=utf-8
""" Interface definition for neural network architecture based on Tensorflow
  keras implementation

Link:
  - https://github.com/aicodes/tf-bestpractice
"""
from enum import Enum, unique
from typing import Tuple, List
from abc import ABC, abstractmethod
from tensorflow.keras import Model
from tensorflow.keras.backend import learning_phase


__author__ = 'Christophe Ecabert'


@unique
class ImageFormat(Enum):
  """ Image data format """
  RGB = 1   # Reg, Green, Blue
  BGR = 2   # Blue, Green, Red (i.e. opencv)


class InputSpec(object):
  """ Single input specification """

  def __init__(self, dims: tuple, fmt: ImageFormat, name: str):
    """
    Constructor
    :param dims:    List if `int` specifying the dimensions
    :param fmt:     Image format (i.e. RGB/BGR)
    :param name:    Input name
    """
    self._dims = dims
    self._format = fmt
    self._name = name

  @property
  def dims(self) -> tuple:
    """
    Provide input dimensions
    :return: tuple of int
    """
    return self._dims

  @property
  def format(self) -> ImageFormat:
    """
    Provide image format
    :return:  ImageFormat enum
    """
    return self._format

  @property
  def name(self) -> str:
    """
    Provide input's name
    :return:  str
    """
    return self._name


class InputConfigAbstract(ABC):
  """ Network input configuration interface """

  def __init__(self, input_specs: List[InputSpec]):
    """
    Constructor
    :param input_specs: List of `InputSpec` object for each inputs of the
                        network
    """
    self._input_specs = input_specs

  @property
  @abstractmethod
  def preprocessing_fn(self):
    """
    Return the default pre-processing function used by the network. The
    signature of the function must be: preprocessing_fn(inputs, **kwargs) where
    `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  pre-processing function
    """
    pass

  @property
  @abstractmethod
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    pass

  @property
  def inputs_spec(self):
    """
    Provide input specification for each entries of the network
    :return: List[InputSpec]
    """
    return self._input_specs


class Network(Model, ABC):
  """ Neural network interface """

  def __init__(self,
               config: InputConfigAbstract,
               *args,
               input_names=None,
               output_names=None,
               name=None,
               **kwargs):
    """
    Constructor
    :param config         Network inputs configuration
    :param args:          Extra arguments supported by `tf.keras.Model`
    :param input_names:   List/tuple of name to gives to the input tensors.
                          If `None` default to 'input_{k}'
    :param output_names:  List/tuple of name to gives to the output tensors
                          If `None` default to 'output_{k}'
    :param name:          Network's name
    :param kwargs:        Extra keyword arguments supported by `tf.keras.Model`
                          class
    """
    # Call base class constructor
    super(Network, self).__init__(name=name, *args, **kwargs)
    self._config = config
    if not isinstance(input_names, (type(None), list, tuple)):
      raise ValueError('`input_names` must be: `None`, `tuple` or `list`')
    self._input_tensor_names = input_names
    if not isinstance(output_names, (type(None), list, tuple)):
      raise ValueError('`output_names` must be: `None`, `tuple` or `list`')
    self.output_names = output_names

  @property
  def inputs_config(self) -> InputConfigAbstract:
    """
    Return the inputs configuration
    :return:  InputConfigAbstract instance
    """
    return self._config

  def _get_learning_phase(self, training=None):
    """
    Return current learning phase
    :param training:  Current training phase if any, otherwise will be deduced
                      by framework
    :return:  Boolean, learning phase: True == Training, False == Testing
    """
    if training is None:
      training = learning_phase()
    return training

  def _set_inputs(self, inputs, outputs=None, training=None):
    """
    Set model's input and output specs based on the input data received.

    This is to be used for Model subclasses, which do not know at instantiation
    time what their inputs look like.

    Args:
      inputs: Single array, or list of arrays. The arrays could be placeholders,
        Numpy arrays, data tensors, or TensorSpecs.
        - if placeholders: the model is built on top of these placeholders,
          and we expect Numpy data to be fed for them when calling `fit`/etc.
        - if Numpy data or TensorShapes: we create placeholders matching the
          TensorShapes or shapes of the Numpy arrays. We expect Numpy data to be
          fed for these placeholders when calling `fit`/etc.
        - if data tensors: the model is built on top of these tensors.
          We do not expect any Numpy data to be provided when calling `fit`/etc.
      outputs: None, a data tensor, or a list of tensors. If None, the
        outputs will be determined by invoking `self.call()`, otherwise the
        provided value will be used.
      training: Boolean or None. Only relevant in symbolic mode. Specifies
        whether to build the model's graph in inference mode (False), training
        mode (True), or using the Keras learning phase (None).
    Raises:
      ValueError: If dict inputs are passed to a Sequential Model where the
        first layer isn't FeatureLayer.
    """
    # Default behaviour (i.e. input_tensor_name == None)
    data_inputs = inputs
    if self._input_tensor_names is not None:
      # Convert inputs to `dict` with user defined name
      if not isinstance(inputs, (list, tuple)):
        inputs = [inputs]
      data_inputs = dict(zip(self._input_tensor_names, inputs))
    super(Network, self)._set_inputs(data_inputs, outputs, training)

  @abstractmethod
  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :param kwargs: Extra keyword arguments
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    pass

  @abstractmethod
  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :param kwargs: Extra keyword arguments
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    pass

  @abstractmethod
  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :param kwargs: Extra keyword arguments
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    pass