# coding=utf-8
"""
Face reconstruction network based on autoencoder architecture. The
particularities lies in the decoder where it is handcrafted to generate image
from a given instance of a Morphable Model (BFM).

Link:
- https://arxiv.org/abs/1703.10580
"""
import json as _json
from os.path import splitext, basename

import numpy as _np
import tensorflow as _tf
import tensorflow.keras.layers as kl
from lts5.tensorflow_op.layers import create_layer
from lts5.tensorflow_op.layers import SummaryLayer
from lts5.tensorflow_op.layers import SHLighting
from lts5.tensorflow_op.layers import EyeToScreenProjection
from lts5.tensorflow_op.layers import ParametricFaceModel as FaceModel
from lts5.tensorflow_op.layers import DeferredRendererV3
from lts5.tensorflow_op.layers import MoCoQueue
from lts5.tensorflow_op.losses import LandmarkLogLikelihood
from lts5.tensorflow_op.losses import LandmarksPairLogLikelihood
from lts5.tensorflow_op.losses import PhotometricLogLikelihoodV2 as PhotoLL
from lts5.tensorflow_op.losses import ImageGradientLikelihood
from lts5.tensorflow_op.losses import LossAggregator
from lts5.tensorflow_op.network.network import ImageFormat, InputSpec
from lts5.tensorflow_op.network.network import InputConfigAbstract
from lts5.tensorflow_op.network.network import Network
from lts5.tensorflow_op.network.resnet import ResNet
from lts5.tensorflow_op.network.utils import get_optimizer
from lts5.tensorflow_op.utils import Params

__author__ = 'Christophe Ecabert'


class RegularizerParam(Params):
  """ Class holding regularization parameters

  bg_prior: Background prior likelihood model
  sdev:     Residual standard deviation
  n_level:  Number of level in gaussian pyramid
  w_photo:  Photometric alignment error regularization factor
  w_id:     List of regularization factor for the 'Identity' coefficients
  w_m_id:   Regularization factor to constraint mean of `Identity`
  w_exp:    List of regularization factor for the 'Expression' coefficients
  w_m_exp:  Regularization factor to constraint mean of `Expression`
  w_tex:    Regularization factor for the 'Texture' coefficients
  w_m_tex:  Regularization factor to constraint mean of `Texture`
  w_lms:    Regularization factor for facial landmark projection error
  w_alb:    Albedo photometric alignment error regularization factor
  w_sym:    Albedo symmetry error regularization factor
  w_recog:  Identity recognition loss
  sdev_id:  Standard deviation of identity's parameters
  sdev_exp: Standard deviation of expressions's parameters
  sdev_tex: Standard deviation of texture's parameters
  sdev_lms  Standard deviation of landmard's residual
  """
  @classmethod
  def default(cls):
    """ Default constructor """
    return cls(bg_prior='background_prior.npy',
               sdev=1.0,
               n_level=1,
               w_photo=1.0,
               w_id=1.0,
               w_exp=1.0,
               w_tex=1.0,
               w_m_id=1.0,
               w_m_exp=1.0,
               w_m_tex=1.0,
               w_lms=1.0,
               w_alb=0.0,
               w_sym=1.0,
               w_smo=1.0,
               w_mag=1.0,
               w_recog=1.0,
               sdev_id=1.0,
               sdev_exp=1.0,
               sdev_tex=1.0,
               sdev_lms=1.0)

  @classmethod
  def from_json(cls, json_str):
    """ Populate the structure from a given json string
    :param json_str:  JSON string
    """
    reg = _json.loads(json_str)['regularizer']
    return cls(**reg)


class RenderingParam(Params):
  """ Structure holding rendering parameters

  focal:      Focal distance of the camera
  near:       Near plane
  far:        Far plane
  width:      Width of the generated image.
  height:     Height of the generated image
  channel:    Number of channel of the generated image
  visible:    Indicate if the rendering windows is visible or not
  n_face:     Number of primitive face to render (-1 == all, K only the k
              first)
  """

  @classmethod
  def default(cls):
    return cls(focal=525,
               near=0.1,
               far=5000,
               width=224,
               height=224,
               channel=3,
               visible=False,
               n_face=-1)

  @classmethod
  def from_json(cls, json_str):
    """ Populate the structure from a given json string
    :param json_str:  JSON string holding parameters
    """
    ren = _json.loads(json_str)['rendering']
    return cls(**ren)


class ModelParam(Params):
  """ Structure holding morphable model parameters

  type:       Type of feature encoder
  path:       Path to the morphable model to use
  laplacian_path: Path to graph laplacian file
  ns:         Number of shape parameters (identity + expression)
  ne:         Number of expression parameters
  nt:         Number of texture parameters
  ni:         Number of illumination parameters
  n_es:       Number of parameters for shape corrective basis
  n_et:       Number of parameters for texture corrective basis
  n_vertex:   Total number of vertex in the mesh
  indices:    List of vertex index corresponding to facial landmarks. If
              empty not used
  weights:    Per landmarks penalizing weights
  """
  @classmethod
  def default(cls):
    """ Default constructor """
    return cls(type="AlexNet",
               path='basel_face_exp_model.bin',
               symmetric_vertex_path=None,
               laplacian_path=None,
               ns=199,
               ne=40,
               nt=199,
               ni=27,
               n_es=20,
               n_et=20,
               n_vertex=53490,
               indices=[],
               weights=[])

  @classmethod
  def from_json(cls, json_str):
    """ Populate the structure from a given json string
    :param json_str:  JSON string holding parameters
    """
    p = _json.loads(json_str)['model']
    return cls(**p)


class OptimizerParam(Params):
  """ Structure holding optimizer parameters
  type:           Type of optimizer to use (i.e. SGD, Adam, AdaDelta, ...)
  params          Extra parameters of the optimizer (dict)
  min_lr:         Learning minimum
  lr_decay:       Learning rate decay after each epochs, default 1.0
                  (no decay)
  patience:       Number of epoch to wait before applying LR schedule
  batch_sz:       Batch size
  rate            Probability to drop an element (dropout)
  n_epoch         Number of epoch
  """
  @classmethod
  def default(cls):
    """ Default constructor """
    return cls(type='Adam',
               params={"learning_rate": 1e-5},
               min_lr=1e-5,
               lr_decay=1.0,
               patience=0,
               batch_sz=5,
               rate=0.0,
               n_epoch=1)

  @classmethod
  def from_json(cls, json_str):
    """ Populate the structure from a given json string
    :param json_str:  JSON string holding parameters
    """
    opt = _json.loads(json_str)['optimizer']
    return cls(**opt)


class PredictorParam(Params):

  @classmethod
  def default(cls):
    """ Default constructor """
    return cls(subspace={'dims': [96, 64, 96, 32, 8],
                         'flatten': False,
                         'activation': None,
                         },
               multihead={
                 'shape_head': {'units': 144,
                                'rate': 0.2,
                                'normalize': None,
                                'n_layer': 1,
                                'n_segment': 1,
                                'kernel_init': None,
                                'kernel_reg': None,
                                'bias_init': 'zeros'},
                 'shape_corr_head': {'units': 64,
                                     'rate': 0.2,
                                     'normalize': None,
                                     'n_layer': 1,
                                     'n_segment': 1,
                                     'kernel_init': None,
                                     'kernel_reg': None,
                                     'bias_init': 'zeros'},
                 'texture_head': {'units': 80,
                                  'rate': 0.2,
                                  'normalize': None,
                                  'n_layer': 1,
                                  'n_segment': 1,
                                  'kernel_init': None,
                                  'kernel_reg': None,
                                  'bias_init':
                                    'zeros'},
                 'texture_corr_head': {'units': 64,
                                       'rate': 0.2,
                                       'normalize': None,
                                       'n_layer': 1,
                                       'n_segment': 1,
                                       'kernel_init': None,
                                       'kernel_reg': None,
                                       'bias_init': 'zeros'},
                 'illu_head': {'units': 27,
                               'rate': 0.2,
                               'normalize': None,
                               'n_layer': 1,
                               'n_segment': 1,
                               'kernel_init': {'class_name': 'TruncatedNormal',
                                               'config': {'mean': 0.0,
                                                          'stddev': 0.001,
                                                          'seed': None}},
                               'kernel_reg': None,
                               'bias_init': [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                             0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
                                             0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
                                             0.0, 0.0, 0.0, 0.0, 0.0, 0.0]},
                 'pose_head': {'units': 6,
                               'rate': 0.2,
                               'normalize': None,
                               'n_layer': 1,
                               'n_segment': 1,
                               'kernel_init': {'class_name': 'TruncatedNormal',
                                               'config': {'mean': 0.0,
                                                          'stddev': 0.0001,
                                                          'seed': None}},
                               'kernel_reg': None,
                               'bias_init': [0.0, 0.0, 0.0, 0.0, -0.0285,
                                             -0.95]}
               })

  @classmethod
  def from_json(cls, json_str):
    """ Populate the structure from a given json string
    :param json_str:  JSON string holding parameters
    """
    opt = _json.loads(json_str)['predictor']
    return cls(**opt)


class MorphNetParameter(Params):
  """
  A MorphNetParameter instance contains all the parameters needed to train a
  reconstruction network.

  img_dims:           Input image dimensions
  model:              Morphable model parameters
  rendering:          Rendering parameters
  optimizer:          Optimizer parameters
  predictor:          Predictor configuration
  regularizer:        Regularization parameters
  """

  @classmethod
  def default(cls):
    """
    Default constructor
    """
    return cls(img_dims=[224, 224, 3],
               model=ModelParam.default(),
               rendering=RenderingParam.default(),
               optimizer=OptimizerParam.default(),
               predictor=PredictorParam.default(),
               regularizer=RegularizerParam.default())

  @classmethod
  def from_json(cls, filename):
    """ Populate the structure from a given json string
    :param filename:  File hodling JSON formatted configuration
    """
    with open(filename, 'r') as f:
      json_str = f.read()
      cfg = _json.loads(json_str)
      return cls(img_dims=cfg['img_dims'],
                 model=ModelParam.from_json(json_str=json_str),
                 rendering=RenderingParam.from_json(json_str=json_str),
                 optimizer=OptimizerParam.from_json(json_str=json_str),
                 predictor=PredictorParam.from_json(json_str=json_str),
                 regularizer=RegularizerParam.from_json(json_str=json_str))

  def generate_default_parameters(self, with_aug=False):
    """ Generate default parameters """
    params = []
    # Shape parameters -> meanshape
    sp = _np.zeros((1, self.model.ns + self.model.ne), dtype=_np.float32)
    params.append(sp)
    # Texture parameters -> meantexture
    tp = _np.zeros((1, self.model.nt), dtype=_np.float32)
    params.append(tp)
    # Illumination
    ip = _np.zeros((1, self.model.ni), dtype=_np.float32)
    ip[0, ::9] = 1.0  # 0.7
    params.append(ip)
    # Corrective basis
    if with_aug:
      es = _np.zeros((1, self.model.n_es), dtype=_np.float32)   # Extra shape
      et = _np.zeros((1, self.model.n_et), dtype=_np.float32)   # Extra tex
      params.append(es)
      params.append(et)
    # Camera
    tx = 0.0
    ty = -0.01
    tz = -0.975 #-1.15
    # cam = _np.asarray([[0.0, 0.0, 0.0, tx, ty, tz]], dtype=_np.float32)
    cam = _np.asarray([[0.0, 0.0, 0.0, tx, ty, tz]], dtype=_np.float32)
    params.append(cam)
    # Stack everything together
    return _np.hstack(params).reshape(-1)


class DecoupledFeatureSpace(kl.Layer):
  """
  Learn feature space decoupling for various face attributes (i.e. identity,
  expression, lighting, pose, ...). Each attribute are decoupled using fully
  connected layers
  """

  def __init__(self,
               feature_space_dims: list,
               flatten=False,
               activation=None,
               name='DecoupledFeatureSpace',
               **kwargs):
    """
    Constructor
    :param feature_space_dims:  list, Dimensions of the feature space to learn
    :param flatten: If `True` flatten the feature space before decoupling.
    :param activation:  Type of activation to apply after the dense layer
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(DecoupledFeatureSpace, self).__init__(name=name, **kwargs)
    self.act = activation
    self.n_subspace = 0
    self.feat_dims = feature_space_dims
    self.flatten = flatten
    self.flat = (create_layer(kl.Flatten,
                              name='flat',
                              layer_params=layer_params) if flatten
                 else _tf.identity)
    for d in feature_space_dims:
      n = 'subspace{}'.format(self.n_subspace)
      layer = create_layer(kl.Dense,
                           units=d,
                           activation=activation,
                           name=n,
                           layer_params=layer_params)
      setattr(self, n, layer)
      self.n_subspace += 1

  def call(self, inputs):
    """
    Forward
    :param inputs:  Feature space to decoupled
    :return:  List of subspaces
    """
    subspace = []
    feat = self.flat(inputs)
    for k in range(self.n_subspace):
      layer_name = 'subspace{}'.format(k)
      layer = getattr(self, layer_name)
      subfeat = layer(feat)
      subspace.append(subfeat)
    return subspace


class IdentityFeatureSpace(kl.Layer):
  """ Identity Feature space, i.e. no transformation """

  def __init__(self,
               rate,
               name='IdentityFeatureSpace',
               **kwargs):
    """
    Constructor
    :param name:  Layer's name
    :param rate:  Dropout rate
    :param kwargs:  Extra keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(IdentityFeatureSpace, self).__init__(name=name, **kwargs)
    self.drop = create_layer(kl.Dropout,
                             name='FeatureDrop',
                             rate=rate,
                             layer_params=layer_params)

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:  Input tensor
    :return:  Tensor of input
    """
    return self.drop(inputs, training=training)


class RegressionHead(kl.Layer):
  """
  Regression of one attributes with following steps:

  - LayerNormalization (no scale, no bias) / BatchNorm / No normalization
    - Understanding and Improving Layer Normalization, NIPS
  - (Dense + Relu + Drop) x 2
  - Dense

  The layer can be used to predict parameters for segmented face model, meaning
  each regression will be duplicated for each segment
  """

  def __init__(self,
               units,
               rate,
               normalize,
               n_layer=3,
               n_segment=1,
               scale=None,
               clip=None,
               kernel_initializer=None,
               kernel_regularizer=None,
               bias_initializer=None,
               name='RegressionHeadV2',
               **kwargs):
    """
    Construcor
    :param units:     Number of element to regress
    :param rate:      Drop output rate
    :param normalize: str, indicate the type of feature normalization. Possible
                      values are: `ln`, `bn` or `None`
    :param n_layer:  Number of layer to have in regression head
    :param n_segment: Number of segment in the model
    :param scale:   Scaling factor to apply on the regressed parameters
    :param clip:    float, list of float indicating the range of allowed values.
      If single value provided will define the range as +/-, list must have
      length of two with first element being the minimum value and second being
      the maximum value. Use `None` to not clip the output
    :param kernel_initializer: Kernel initializer for last layer
    :param kernel_regularizer: Kerel regularization for last layer
    :param bias_initializer: Bias initializer for last layer
    :param name:      Layer's name
    :param kwargs:    Extra keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(RegressionHead, self).__init__(name=name, **kwargs)

    # Input normalization:
    # ln == layer normalization
    # bn == batch normalization
    # None == identity
    if normalize == 'ln':
      self.norm = create_layer(kl.LayerNormalization,
                               name='norm',
                               center=False,
                               scale=False,
                               layer_params=layer_params)
    elif normalize == 'bn':
      self.norm = create_layer(kl.BatchNormalization,
                               name='norm',
                               layer_params=layer_params)
    else:
      self.norm = lambda x: _tf.identity(x, name='norm')
    # Regression
    self.n_layer = n_layer
    self.n_segment = n_segment
    self.scale = None
    if scale is not None:
      self.scale = _tf.convert_to_tensor(scale, name='scale')
    self.clip_fn = _tf.identity
    if clip is not None:
      # Clip regressed value for a given range
      self.min_value = 0.0
      self.max_value = 0.0
      if isinstance(clip, (list, tuple)):
        self.min_value, self.max_value = clip
      elif isinstance(clip, float):
        self.min_value = -clip
        self.max_value = clip
      else:
        raise ValueError('Unsupported value for `clip` parameter. Must be '
                         'either a `float` or `list`/`tuple`')

      # Assign function
      self.clip_fn = lambda x: _tf.clip_by_value(x,
                                                 self.min_value,
                                                 self.max_value)
    # Build regressor
    self.segment_layers = []
    for s in range(n_segment):
      # Create one regressor for each segment
      with _tf.name_scope('segment{}'.format(s)) as scope:
        reg_layers = []
        for k in range(n_layer):
          act = 'relu' if k != n_layer - 1 else None
          k_init = None if k != n_layer - 1 else kernel_initializer
          k_reg = None if k != n_layer - 1 else kernel_regularizer
          b_init = None if k != n_layer - 1 else bias_initializer
          layer = create_layer(kl.Dense,
                               name=scope + 'dense{}'.format(k),
                               units=units,
                               activation=act,
                               kernel_initializer=k_init,
                               kernel_regularizer=k_reg,
                               bias_initializer=b_init,
                               layer_params=layer_params)
          reg_layers.append(layer)
          # Add dropout if not at the last layer
          if k != n_layer - 1 and rate > 0.0:
            drop = create_layer(kl.Dropout,
                                name=scope + 'drop{}'.format(k),
                                rate=rate,
                                layer_params=layer_params)
            reg_layers.append(drop)
        # Add to segments
        self.segment_layers.append(reg_layers)
    self.units = units
    self.rate = rate
    self.normalize = normalize

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:      Features
    :param training:    If `True` indicates training phase, otherwise no.
    :return:  Regressed values
    """
    # Normalize features
    norm_args = [inputs]
    if isinstance(self.norm, kl.BatchNormalization):
      norm_args.append(training)
    feat = self.norm(*norm_args)
    # Apply regression
    pred = []
    for seg in self.segment_layers:
      x = feat
      for layer in seg:
        layer_args = [x]
        if isinstance(layer, kl.Dropout):
          layer_args.append(training)
        # Call layer
        x = layer(*layer_args)
      pred.append(x)
    pred = _tf.stack(pred, axis=1)  # [Batch, #Segment, #Params]
    # Apply scaling factor ?
    if self.scale is not None:
      pred *= self.scale
    return self.clip_fn(pred)


class MultiRegressionHead(kl.Layer):
  """ Multiple regression head in one layer """

  def __init__(self,
               config,
               with_refinement=False,
               name='MultiRegressionHead',
               **kwargs):
    """
    Constrcutor
    :param config:  Dict of configuration
    :param with_refinement: If `True` add model refiniment regressor
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(MultiRegressionHead, self).__init__(name=name, **kwargs)

    # Build each individual regression head
    def _head_builder(cfg, hname, **kwargs):
      # Bias init
      b_init = cfg['bias_init']
      if isinstance(b_init, list):
        b_init = _tf.keras.initializers.Constant(b_init)
      return RegressionHead(units=cfg['units'],
                            rate=cfg['rate'],
                            normalize=cfg['normalize'],
                            n_layer=cfg['n_layer'],
                            n_segment=cfg['n_segment'],
                            scale=cfg['scale'],
                            clip=cfg['clip'],
                            kernel_initializer=cfg['kernel_init'],
                            kernel_regularizer=cfg['kernel_reg'],
                            bias_initializer=b_init,
                            name=hname,
                            **kwargs)

    # Shape
    self.shp_head = _head_builder(config.shape_head,
                                  'shape_head',
                                  layer_params=layer_params)
    # Texture
    self.tex_head = _head_builder(config.texture_head,
                                  'texture_head',
                                  layer_params=layer_params)
    # Illumination
    self.illu_head = _head_builder(config.illu_head,
                                   'illumination_head',
                                   layer_params=layer_params)
    # Pose - Rotation
    self.rot_head = _head_builder(config.rotation_head,
                                  'rotation_head',
                                  layer_params=layer_params)
    # Pose - Position
    self.pos_head = _head_builder(config.position_head,
                                  'position_head',
                                  layer_params=layer_params)
    # Pose - Focal
    if config.focal > 0.0:
      # Fixed focal length, no regressor
      self._focal_length = config.focal
      self.focal_head = kl.Lambda(self._fake_focal_head, name='focal_head')
    else:
      self.focal_head = _head_builder(config.focal_head,
                                      'focal_head',
                                      layer_params=layer_params)
    # Corretive basis ?
    self.shp_corr_head = None
    self.tex_corr_head = None
    if with_refinement:
      self.shp_corr_head = _head_builder(config.shape_corr_head,
                                         'shape_refinement_head',
                                         layer_params=layer_params)
      self.tex_corr_head = _head_builder(config.texture_corr_head,
                                         'texture_refinement_head',
                                         layer_params=layer_params)

    # Forward function
    self._forward_fn = None

  def build(self, input_shape):
    """
    Build layer
    :param input_shape: TensorShape or list of TensorShape
    """
    if isinstance(input_shape, (tuple, list)):
      # Split subspace
      self._forward_fn = self._split_subspace_forward
    else:
      self._forward_fn = self._joint_subspace_forward
    # Done
    super(MultiRegressionHead, self).build(input_shape)

  def _fake_focal_head(self, inputs, training):
    """
    Predict fixed focal length
    :param inputs:    Features (unused)
    :param training:  If `True` indicates training, otherwise not (unused)
    :return:  Focal length, [B, 1, 1]
    """
    shp = _tf.stack((_tf.shape(inputs)[0], 1, 1), axis=0)  # Output dims
    return _tf.ones(shp, dtype=inputs.dtype) * self._focal_length

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:  List of tensor [feat_id, feat_exp, feat_illu, feat_pose]
    :param training:  If `True` indicates training phase, otherwise no.
    :return:  Regress value, in the same order
    """
    # Forward call
    return self._forward_fn(inputs, training)

  def _joint_subspace_forward(self, inputs, training):
    """
    Prediction from joint subspace
    :param inputs:    Features
    :param training:  If `True` indicates training phase, otherwise no.
    :return:  List of predicted parameters
    """
    # Shape
    w_shp = self.shp_head(inputs, training=training)
    # Texture
    w_tex = self.tex_head(inputs, training=training)
    # Illumination
    w_illu = self.illu_head(inputs, training=training)
    # Pose - Rotation
    rvec = self.rot_head(inputs, training=training)
    # Pose - Position
    tvec = self.pos_head(inputs, training=training)
    # Focal
    focal = self.focal_head(inputs, training=training)
    params = [w_shp, w_tex, w_illu, rvec, tvec, focal]
    if self.shp_corr_head is not None:
      # Corrections
      w_shp_corr = self.shp_corr_head(inputs, training=training)
      w_tex_corr = self.tex_corr_head(inputs, training=training)
      params += [w_shp_corr, w_tex_corr]
    return params

  def _split_subspace_forward(self, inputs, training):
    """
    Prediction from split subspace
    :param inputs:    List of input features
    :param training:  If `True` indicates training phase, otherwise no.
    :return:  List of predicted parameters
    """
    feat_id, feat_exp, feat_tex, feat_illu, feat_pose = inputs

    # Shape predicted from feat_id + feat_exp
    feat_shp = _tf.concat([feat_id, feat_exp], axis=1)
    w_shp = self.shp_head(feat_shp, training=training)
    # Texture predicted from feat_id
    w_tex = self.tex_head(feat_tex, training=training)
    # Illumination predicted from feat_illu
    w_illu = self.illu_head(feat_illu, training=training)
    # Pose - Rotation
    rvec = self.rot_head(feat_pose, training=training)
    # Pose - Position
    tvec = self.pos_head(feat_pose, training=training)
    # Focal
    focal = self.focal_head(feat_pose, training=training)
    params = [w_shp, w_tex, w_illu, rvec, tvec, focal]
    if self.shp_corr_head is not None:
      # Predict corrective basis parameters
      w_shp_corr = self.shp_corr_head(feat_shp, training=training)
      w_tex_corr = self.tex_corr_head(feat_tex, training=training)
      params += [w_shp_corr, w_tex_corr]
    return params


class LoopbackLogger(SummaryLayer):
  """ Logging mechanism for loopback losses """

  def __init__(self,
               name='LoopbackLogger',
               manager=None,
               **kwargs):
    """
    Constructor
    :param name:  Layer's name
    :param manager: SummaryWriterManager instance
    :param kwargs:  Extra keywork arguments
    """

    def _log_fn(inputs, step, training):
      contrast_id = inputs[0]
      contrast_tex = inputs[1]
      loop_exp = inputs[2]
      l_loop = inputs[3]
      self._w_loss.scalar(name='ContrastiveId',
                          data=contrast_id,
                          step=step,
                          training=training)
      self._w_loss.scalar(name='ContrastiveTex',
                          data=contrast_tex,
                          step=step,
                          training=training)
      self._w_loss.scalar(name='LoopExp',
                          data=loop_exp,
                          step=step,
                          training=training)
      self._w_loss.scalar(name='LoopTotal',
                          data=l_loop,
                          step=step,
                          training=training)

    super(LoopbackLogger, self).__init__(name=name,
                                         manager=manager,
                                         logging_fn=_log_fn,
                                         traces=('loss', ),
                                         **kwargs)

  def call(self, inputs, training=None):
    """
    Forward logging
    :param inputs:  Input
    :param training:  If `True` indicates training, otherwise no.
    :return:  Input
    """
    return super(LoopbackLogger, self).call(inputs, training)


class MorphNetInputConfig(InputConfigAbstract):
  """ Input specs for MorphNet """

  def __init__(self):
    """ Constructor """
    super(MorphNetInputConfig, self).__init__([InputSpec(dims=(227, 227, 3),
                                                         fmt=ImageFormat.RGB,
                                                         name='input')])
    self._preprocess_fn = kl.Lambda(_tf.identity,
                                    name='preprocessing')
    self._revert_preprocess_fn = kl.Lambda(_tf.identity,
                                           name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    """
    Provide pre-processing layer
    :return:  Instance of `tf.keras.layer.Layer`
    """
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


_resnet_ctor = {'resnet18': ResNet.ResNet18,
                'resnet34': ResNet.ResNet34,
                'resnet50': ResNet.ResNet50,
                'resnet50v2': ResNet.ResNet50V2,
                'resnet101': ResNet.ResNet101,
                'resnet101v2': ResNet.ResNet101V2}


def _shuffle_params(p, name):
  """
  Shuffle a given Tensor `p` along the batch dimensions
  :param p: Tensor of parameters to be shuffled
  :param name: Name of the parameter being shuffled
  :return:  Shuffled tensor, same size
  """
  with _tf.name_scope('parameter_shuffling_' + name):
    bsize = _tf.shape(p)[0]
    idx = _tf.random.shuffle(_tf.range(bsize))
    return _tf.gather(p, idx, axis=0)


class MorphNet(Network):
  """ Morphable Model Network """

  def __init__(self,
               config: MorphNetParameter,
               manager=None,
               input_names=None,
               output_names=None,
               output_mid_value: bool = False,
               name: str = 'MorphNet',
               **kwargs):
    """
    Constructor
    :param config:        Network configuration
    :param manager:       Summary manager
    :param input_names:   List/tuple of name to gives to the input tensors.
                          If `None` default to 'input_{k}'
    :param output_names:  List/tuple of name to gives to the output tensors
                          If `None` default to 'output_{k}'
    :param output_mid_value: If `True` forward call will output intermediate
      values (i.e. feature, parameters) as well as image/landmarks
    :param name:          Top level name scope
    :param kwargs:        Extra keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(MorphNet, self).__init__(config=MorphNetInputConfig(),
                                   input_names=input_names,
                                   output_names=output_names,
                                   name=name,
                                   **kwargs)

    # Configuration
    self._cfg = config
    self._c_model = config.model
    self._c_render = config.rendering
    self._c_reg = config.regularizer
    self._c_pred = config.predictor
    self._c_opt = config.optimizer
    self._input_tensor_names = input_names
    self._output_tensor_names = output_names
    self._outout_mid_values = output_mid_value
    # Feature extractor
    # ----------------------------------------------------
    model_ctor = _resnet_ctor[self._c_model.type.lower()]
    self.n_resolution = None
    if self._cfg.image_resolution:
      self.n_resolution = 1 + len(self._cfg.image_resolution)
    # Resolution aware ResNet feature extractor
    self._feat = model_ctor(include_top=False,
                            pooling='avg',
                            n_resolution=self.n_resolution,
                            layer_params=layer_params)
    self._preprocessing_fn = self._feat.inputs_config.preprocessing_fn

    # Decoupled Feature Space
    # ----------------------------------------------------
    n_subspace = self._c_pred.subspace['dims']
    if isinstance(n_subspace, (list, tuple)):
      # Decoupled subspace
      self.feat_subspace = DecoupledFeatureSpace(self._c_pred.subspace['dims'],
                                                  activation=self._c_pred.subspace['activation'],
                                                  layer_params=layer_params)
    else:
      self.feat_subspace = IdentityFeatureSpace(self._c_pred.subspace['p_drop'])
    # Multi Head regression
    # ----------------------------------------------------
    with_refinement = self._c_model.n_es > 0
    self.reg_head = MultiRegressionHead(config=self._c_pred.multihead,
                                        with_refinement=with_refinement,
                                        layer_params=layer_params)
    # Decoder
    # ----------------------------------------------------
    # Parametric face model
    refine = self._c_model.n_es > 0 or self._c_model.n_et > 0
    lap_path = self._c_model.laplacian_path
    sym_path = self._c_model.symmetric_vertex_path
    with_illu = not self._c_model.screen_space_shading
    self.face_model = FaceModel(face_model_path=self._c_model.path,
                                illumination_prior_path=self._c_reg.illu_prior,
                                with_illumination=with_illu,
                                with_refinement=refine,
                                laplacian_path=lap_path,
                                sym_vertex_path=sym_path,
                                w_id=self._c_reg.w_id,
                                w_exp=self._c_reg.w_exp,
                                w_tex=self._c_reg.w_tex,
                                w_illu=self._c_reg.w_illu,
                                w_sym=self._c_reg.w_sym,
                                w_smo=self._c_reg.w_smo,
                                w_mag=self._c_reg.w_mag,
                                manager=manager)
    # Deferred renderer
    self.renderer = DeferredRendererV3(width=self._c_render.width,
                                       height=self._c_render.height,
                                       near=self._c_render.near,
                                       far=self._c_render.far,
                                       fixed_triangle=True)
    # Screen space shading ?
    self.sh_light = None
    if self._c_model.screen_space_shading:
      self.sh_light = SHLighting(self._c_reg.illu_prior,
                                  w_illu=self._c_reg.w_illu,
                                  manager=manager)

    # Landmarks
    self.projector = None
    if self._c_model.indices:
      self.projector = EyeToScreenProjection(subset=self._c_model.indices,
                                             width=self._c_render.width,
                                             height=self._c_render.height,
                                             near=self._c_render.near,
                                             far=self._c_render.far)

    # Contrastive-loss for w_id constency across pose.
    self.id_contrastive_loss = None
    self.tex_contrastive_loss = None
    if self._c_reg.w_contrast_id > 0.0:
      self.id_contrastive_loss = MoCoQueue(size=self._c_reg.queue_size,
                                           temperature=self._c_reg.temp_id,
                                           normalized=False,
                                           name='MocoQueueId')
    if self._c_reg.w_contrast_tex > 0.0:
      self.tex_contrastive_loss = MoCoQueue(size=self._c_reg.queue_size,
                                            temperature=self._c_reg.temp_id,
                                            normalized=False,
                                            name='MocoQueueTex')

    # Logging
    # ----------------------------------------------------
    self.manager = manager
    self.add_loopback = (self._c_reg.w_contrast_id > 0.0 or
                         self._c_reg.w_contrast_tex > 0.0 or
                         self._c_reg.w_loop_exp > 0.0)
    self.loopback_logger = None
    if self.add_loopback:
      self.loopback_logger = LoopbackLogger(manager=manager)

    # Optimizer + metrics
    # ----------------------------------------------------
    self._optimizer_instance = None
    self._metrics_list = None
    self._losses_list = None

  @staticmethod
  def get_output_name_for_config(config):
    """
    Generate list of output name for a given config
    :param config:  Model configuration
    :return:  List of names
    """
    out_name = ['image']
    albedo_name = []
    # Generate image output names
    if config.regularizer.w_alb > 0.0:
      albedo_name.append('albedo')
    out_name.extend(albedo_name)
    # Landmarks
    if config.model.indices:
      out_name.append('landmarks')
    return out_name

  @classmethod
  def ForTraining(cls, config, manager=None, **kwargs):
    """
    Create an instance of MorphNet for training phase
    :param config:     Network configuration
    :param manager: Summary writer manager
    :param kwargs:  Extra keyword arguments
    :return:  Network
    """
    in_name = ['image', 'background']
    out_name = MorphNet.get_output_name_for_config(config)
    return cls(config=config,
               input_names=in_name,
               output_names=out_name,
               manager=manager,
               **kwargs)

  def call(self, inputs, training=None):
    """
    Build forward pass
    :param inputs:    A List Tensor of images to reconstruct + backgrounds
    :keyword training:  If `True` indicates training phase
    :return:  Reconstructed image, (features, regressed parameters)
    """
    # Get current learning phase for BN/Dropout
    training = self._get_learning_phase(training)
    # Graph mode
    if _tf.is_tensor(inputs):
      image, scale_index = inputs
    else:
      image = inputs
      scale_index = None
    bg = image / 255.0
    # Model coefficients prediction
    # ----------------------------------------------------------
    # reg_params = [w_shp, w_tex, w_illu, rvec, tvec, focal,
    # (w_shp_corr, w_tex_corr)]
    reg_params, features = self._predict_params(image, scale_index, training)
    w_shp = reg_params[0]
    w_tex = reg_params[1]
    w_illu = reg_params[2]
    rvec = reg_params[3]
    tvec = reg_params[4]
    focal = reg_params[5]

    # Decoding (3dmm parameters to image)
    # ----------------------------------------------------------
    params = [w_shp, w_tex, rvec, tvec, w_illu]
    if len(reg_params) > 6:
      # Corrective basis parameters are present
      params += reg_params[-2:]
    w_lms = self.projector is not None
    w_albedo = self._c_reg.w_alb > 0.0
    decoded_value, inner_loss = self._decoder(params=params,
                                              focal=focal,
                                              background=bg,
                                              with_albedo=w_albedo,
                                              with_landmarks=w_lms,
                                              training=training)
    self.add_loss(inner_loss, inputs=True)

    # Cross pose contrastive loss
    # ----------------------------------------------------------
    l_loop = 0.0
    if self.add_loopback:
      l_loop = self._looback_loss(params=params,
                                  res_index=scale_index,
                                  focal=focal,
                                  background=bg,
                                  training=training)
    self.add_loss(l_loop, inputs=True)

    # Done
    if self._outout_mid_values:
      return decoded_value, features, reg_params
    return decoded_value

  def _predict_params(self, image, res_index, training):
    """
    Run parameters regression from a given image
    :param image:     Image to predict parameters from, 4D Tensor
    :param res_index: Resolution index (which level it is)
    :param training:  If `True` indicates training phase
    :return:  - Predicted parameters, 2D Tensor
              - Feature space
    """
    with _tf.name_scope('Prediction'):
      # Extract feat
      # ----------------------------------------------------------
      x = self._preprocessing_fn(image)
      if self.n_resolution:
        x = [x, res_index]
      feat_in = self._feat(x, training)
      # Decoupled feature space
      # ----------------------------------------------------------
      feat_subspace = self.feat_subspace(feat_in, training)
      # Predict parameters
      # ----------------------------------------------------------
      # w_shp, w_tex, w_illu, w_pose
      params = self.reg_head(feat_subspace, training)
      # Done
      return params, feat_subspace

  def _decoder(self,
               params,
               focal,
               background,
               with_albedo,
               with_landmarks,
               training):
    """
    Convert morphable model into image using differentiable rasterizer.
    :param params:  List of parameters: w_shp, w_tex, rvec, tvec, w_illu,
      (shp_c, tex_c)]
    :param focal: Focal length to use
    :param background: Tensor to use as background
    :param with_albedo:  If `True` rasterize also albedo
    :param with_landmarks:  If `True` compute projection of 3d landmarks
    :param training: If `True` indicates training phase, otherwise no.
    :return: Tuple: [color, (albedo), (landmarks)], inner_loss
    """
    with _tf.name_scope('MMDecoder'):
      # Generate surface
      model_params = params[:4]       # shp, tex, rvec, tvec
      if self.sh_light is None:
        model_params += [params[4]]   # illu
      if len(params) > 5:
        model_params += params[-2:]   # shp_c, tex_c
      attributes, inner_loss = self.face_model(model_params, training=training)
      shape = attributes['vertex']
      albedo = attributes['albedo']
      tri = attributes['tri']

      def _clip_value(x):
        return _tf.clip_by_value(x, 0.0, 1.0)

      def _normalize(x):
        return _tf.linalg.l2_normalize(x, axis=-1)

      if self.sh_light is None:
        # Color
        attr = [(attributes['color'], _clip_value, background, 'lin')]
        # Albedo if needed
        if with_albedo:
          attr.append((albedo, _clip_value, background, 'lin'))
      else:
        # Albedo
        normal = attributes['normal']
        attr = [(albedo, _clip_value, background, 'lin'),
                # Normals for shading
                (normal, _normalize, _tf.zeros_like(background), 'lin')]
      # Rasterize + interpolate required attributes
      focal = _tf.squeeze(focal, 1)
      interp = self.renderer([shape, focal, tri], attributes=attr)

      # Define output
      outputs = []
      if self.sh_light is None:
        # Shading done at vertex level, nothing to do
        outputs += interp
      else:
        im_albedo = interp[0]
        im_normal = interp[1]
        w_illu = params[4]
        im_color, inner_light_loss = self._screen_space_shading(im_albedo,
                                                                im_normal,
                                                                w_illu,
                                                                training)
        inner_loss += inner_light_loss
        outputs += [im_color]
        if self._c_reg.w_alb > 0.0:
          outputs += [_tf.clip_by_value(interp[0], 0.0, 1.0)]
      # Landmarks
      if with_landmarks:
        lms = self.projector([shape, focal])
        outputs.append(lms)
      return outputs, inner_loss

  def _screen_space_shading(self, albedo, normal, w_illu, training):
    """
    Perform shading in image space with interpolated inputs (i.e. albedo &
    normals)
    :param albedo:  Tensor, surface albedo [B, H, W, 4]
    :param normal:  Tensor, surface normals [B, H, W, 4]
    :param w_illu:  Tensor, spherical harmonics coefficients [B, 1, 27]
    :param training: If `True` indicates training phase, otherwise no.
    :return:  Tensor, shaded color [B, H, W, 4],
              Tensor, light regularization
    """
    im_albedo = albedo[..., :3]
    im_mask = albedo[..., 3:]
    im_normal = normal[..., :3]
    shading, inner_loss = self.sh_light([im_normal, w_illu], training=training)
    # Set background shading to 1 to avoid changing color
    shading = (im_mask * shading) + (1.0 - im_mask)
    color = _tf.clip_by_value(shading * im_albedo, 0.0, 1.0)
    color = _tf.concat([color, im_mask], axis=-1)
    return color, inner_loss

  def _looback_loss(self,
                    params,
                    focal,
                    background,
                    res_index,
                    training):
    """
    Contrastive learning term for cross pose consistency
    :param params:      Regressed parameters [w_id, w_tex, rvec, tvec, w_illu,
      (w_shp_corr, w_tex_corr)]
    :param focal:       Estimated focal length
    :param background:  Background
    :param res_index: Resolution index (which level it is)
    :param training:     If `True` indicates training phase, otherwise not.
    :return:  Loss
    """
    with _tf.name_scope('LoopBack'):
      # Set expression to zero
      # ---------------------------------------------------------------
      with _tf.name_scope('ExpressionZeroing'):
        shp_size = [self._c_model.ns, self._c_model.ne]
        w_id, w_exp = _tf.split(params[0],
                                shp_size,
                                axis=-1)
        w_exp = _tf.zeros_like(w_exp)  # Remove expression
        w_shp = _tf.concat((w_id, w_exp), axis=-1)

      # Generate random pose (rvec)
      # ---------------------------------------------------------------
      with _tf.name_scope('PoseGenerator'):
        rvec = params[3]
        shp = _tf.shape(rvec)
        rvec_shp = _tf.stack((shp[0], 1, 3), axis=0)
        rnd_axis = _tf.random.normal(shape=rvec_shp, dtype=rvec.dtype)
        rnd_axis = _tf.math.l2_normalize(rnd_axis, axis=-1)
        rnd_ang = _tf.random.uniform(_tf.stack((shp[0], 1, 1), axis=0),
                                     minval=-0.785398,  # +/- 60°
                                     maxval=0.785398,
                                     dtype=rvec.dtype)  # +/- 45° = 0.785398
        rnd_rvec = rnd_axis * rnd_ang

      # Combine parameters
      rand_p = [w_shp] + params[1:3] + [rnd_rvec] + params[4:]
      # Decoded to image [0, 1]
      renderings, inner_loss = self._decoder(params=rand_p,
                                             focal=focal,
                                             background=_tf.zeros_like(background),
                                             with_albedo=False,
                                             with_landmarks=False,
                                             training=training)
      # Re-encode samples
      # ---------------------------------------------------------------
      image = renderings[0] * 255.0
      # Set `training=False` to not update BN with synth image statistics.
      p_pred, _ = self._predict_params(image, res_index, training=False)

      # Contrastive loss
      # ---------------------------------------------------------------
      with _tf.name_scope('ContrastiveID'):
        l_contrast_id = 0.0
        if self.id_contrastive_loss is not None:
          pred_w_id, pred_w_exp = _tf.split(p_pred[0],
                                      shp_size,
                                      axis=-1)
          # Flatten, w_shp is in form of [B, #Segment, #params]. Therefore
          # each segment together -> reshape it as [B, #Segment * #params]
          flat_shp = _tf.stack((-1, pred_w_id.shape[-2] * pred_w_id.shape[-1]),
                               axis=0)
          w_id = _tf.reshape(w_id, flat_shp)
          pred_w_id = _tf.reshape(pred_w_id, flat_shp)
          l_contrast_id = self.id_contrastive_loss(inputs=[w_id, pred_w_id])
          l_contrast_id *= self._c_reg.w_contrast_id
      with _tf.name_scope('ContrastiveTex'):
        l_contrast_tex = 0.0
        if self.tex_contrastive_loss is not None:
          pred_w_tex = p_pred[1]
          # Flatten, w_tex is in form of [B, #Segment, #params]. Therefore
          # consider each segment together -> reshape it as [B, #Segment *
          # #params]
          flat_shp = _tf.stack((-1,
                                pred_w_tex.shape[-2] * pred_w_tex.shape[-1]),
                               axis=0)
          w_tex = _tf.reshape(params[1], flat_shp)
          pred_w_tex = _tf.reshape(pred_w_tex, flat_shp)
          l_contrast_tex = self.tex_contrastive_loss(inputs=[w_tex, pred_w_tex])
          l_contrast_tex *= self._c_reg.w_contrast_tex

      # Expression loss
      # ---------------------------------------------------------------
      with _tf.name_scope('ExpressionCost'):
        loop_exp = 0.0
        if self._c_reg.w_loop_exp > 0.0:
          loop_exp = _tf.reduce_sum(_tf.square(pred_w_exp), axis=-1)  # [B, #Seg]
          loop_exp = _tf.reduce_mean(loop_exp)
          loop_exp *= self._c_reg.w_loop_exp

      # Combine losses
      l_loop = l_contrast_id + l_contrast_tex + loop_exp
      # Log
      self.loopback_logger([l_contrast_id, l_contrast_tex, loop_exp, l_loop])
      return l_loop + inner_loss

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    # Create optimizer + parameters
    if self._optimizer_instance is None:
      if self._c_opt.type.lower() == 'rectifiedadam':
        from tensorflow_addons.optimizers import RectifiedAdam
        optimizer_cls = RectifiedAdam
      elif self._c_opt.type.lower() == 'adamw':
        from tensorflow_addons.optimizers import AdamW
        optimizer_cls = AdamW
      else:
        optimizer_cls = get_optimizer(self._c_opt.type)
      self._optimizer_instance = optimizer_cls(**self._c_opt.params)
    return self._optimizer_instance

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :param kwargs:  Extra keyword arguments
    :keyword type:  Type of loss, valid entry is `l1` or `l2`
    :keyword normalized: If `True` likelihoods are normalized
    :return:  List of `tf.keras.losses` or str for canned losses
    """

    if self._losses_list is None:
      # Photometric + Landmarks
      loss_type = kwargs.get('type', 'l1')
      normalized = kwargs.get('normalized', True)
      outputs = MorphNet.get_output_name_for_config(self._cfg)
      self._losses_list = []
      if 'image' in outputs:
        lname = 'LikelihoodPhoto'
        l_ph = PhotoLL(background_prior=self._c_reg.bg_prior,
                       sigma=self._c_reg.sdev,
                       weight=self._c_reg.w_photo,
                       n_level=self._c_reg.n_level,
                       type=loss_type,
                       normalized=normalized,
                       name=lname)
        # Spatial image gradient
        lname = 'LikelihoodSpatialGrad'
        l_im_grad = ImageGradientLikelihood(n_level=self._c_reg.n_level,
                                            sigma=self._c_reg.sdev_im_grad,
                                            weight=self._c_reg.w_im_grad,
                                            type=loss_type,
                                            normalized=normalized,
                                            name=lname)
        # Aggregator
        l_agg = LossAggregator(losses=[l_ph, l_im_grad], manager=self.manager)
        # Final loss instance
        self._losses_list.append(l_agg)
      if 'albedo' in outputs:
        lname = 'LikelihoodAlbedo'
        w_alb = self._c_reg.w_photo * self._c_reg.w_alb
        loss = PhotoLL(background_prior=self._c_reg.bg_prior,
                       sigma=self._c_reg.sdev,
                       weight=w_alb,
                       n_level=self._c_reg.n_level,
                       type=loss_type,
                       normalized=normalized,
                       name=lname)
        self._losses_list.append(loss)
      if 'landmarks' in outputs:
        l_lms = LandmarkLogLikelihood(sigma=self._c_reg.sdev_lms,
                                      weight=self._c_reg.w_lms,
                                      sample_weights=self._c_model.weights,
                                      type=loss_type,
                                      normalized=normalized,
                                      name='LikelihoodLandmark')
        l_pair = LandmarksPairLogLikelihood(pairs=self._c_model.pairs,
                                            sigma=self._c_reg.sdev_pair,
                                            weight=self._c_reg.w_lms_p,
                                            sample_weights=None,
                                            type=loss_type,
                                            normalized=normalized,
                                            name='LikelihoodPairLandmark')
        # Aggregator
        l_agg = LossAggregator(losses=[l_lms, l_pair], manager=self.manager)
        # Final loss instance
        self._losses_list.append(l_agg)
    return self._losses_list

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    from lts5.tensorflow_op.metrics.base import ImagePair
    from lts5.tensorflow_op.metrics.geometry import MeanLandmarkError
    from lts5.tensorflow_op.metrics.image import MeanPhotometricError

    if self._metrics_list is None:

      def _clip_range(x):
        """ Clip range [0.0 - 1.0] + remove alpha channel, only for display"""
        return _tf.clip_by_value(x[..., :3], 0.0, 1.0)

      # Photo + landmarks
      self._metrics_list = []
      m = [MeanPhotometricError(name='mean_photometric_err'),
           ImagePair(dtype=_tf.float32,
                     y_true_fn=_clip_range,
                     y_pred_fn=_clip_range,
                     name='reconstruction')]
      self._metrics_list.append(m)
      # Optional albedo reconstruction
      if self._c_reg.w_alb > 0.0:

        def _select_and_clip(x):
          # x is 5D Tensor where first dim select between albedo and lights
          return _tf.clip_by_value(x[0, ..., :3], 0.0, 1.0)

        m = ImagePair(dtype=_tf.float32,
                      y_true_fn=_clip_range,
                      y_pred_fn=_select_and_clip,
                      name='albedo_rec')
        self._metrics_list.append(m)
      # Landmarks
      if self._c_model.indices:
        m = MeanLandmarkError(name='mean_landmark_error')
        self._metrics_list.append(m)
    return self._metrics_list

  def load_weights(self, filepath, by_name=False, **kwargs):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    :param kwargs:    Extra keyword arguments
    :keyword skip_layer: List of layer's name to be trained from scratch
                         (i.e. skip restoration)
    """
    # Check if numpy file
    _, ext = splitext(basename(filepath))
    if ext == '.npy':
      # Reload pre-trained encoder ?
      return self._feat.load_weights(filepath, by_name)
    elif filepath == 'imagenet':
      # Reload pre-trained resnet encoder
      return self._feat.load_weights(filepath)
    else:
      # Try with base class function
      return super(MorphNet, self).load_weights(filepath, by_name)
