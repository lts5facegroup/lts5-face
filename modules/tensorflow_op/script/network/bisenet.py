# coding=utf-8
""" Implementation of Bilateral Segmentation Network - BiSeNet for realtime
semantic segmentation
See: https://arxiv.org/pdf/1808.00897.pdf
"""
import tensorflow as tf
import tensorflow.keras.layers as kl
from tensorflow.keras.losses import BinaryCrossentropy
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from tensorflow.keras.metrics import BinaryAccuracy, SparseCategoricalAccuracy
from lts5.tensorflow_op.network.network import Network
from lts5.tensorflow_op.network.network import ImageFormat
from lts5.tensorflow_op.network.network import InputSpec, InputConfigAbstract
from lts5.tensorflow_op.network.utils import get_optimizer
from lts5.tensorflow_op.network.resnet import ResNet
from lts5.tensorflow_op.layers import create_layer
from lts5.tensorflow_op.metrics import ConfusionMatrix, SegmentationMask
from lts5.tensorflow_op.metrics import MeanIoU
from lts5.tensorflow_op.losses import SparseCategoricalCrossentropy as SparseCE
from lts5.tensorflow_op.losses import BinaryCrossentropy as BinCE
from lts5.tensorflow_op.losses import OnlineHardExampleMining as Ohem

__author__ = 'Christophe Ecabert'


class ConvBlock(kl.Layer):
  """ Convolutional block in form: Conv + BN + Relu """

  def __init__(self,
               filters,
               kernel_size=3,
               strides=2,
               padding=1,
               name='ConvBlock',
               **kwargs):
    """
    Constructor
    :param filters:     Number of filters in convolution
    :param kernel_size: Convolution kernal size
    :param strides:     Convolution stride
    :param padding:     Padding
    :param name:        Layer's name
    :param kwargs:      Extra keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(ConvBlock, self).__init__(name=name, **kwargs)
    self.pad = create_layer(kl.ZeroPadding2D,
                            name=name + '_pad',
                            padding=padding,
                            layer_params=layer_params)
    self.conv = create_layer(kl.Conv2D,
                             name=name + '_conv',
                             filters=filters,
                             strides=strides,
                             kernel_size=kernel_size,
                             use_bias=False,
                             layer_params=layer_params)
    self.bn = create_layer(kl.BatchNormalization,
                           name=name + '_bn',
                           momentum=0.9,
                           layer_params=layer_params)
    self.act = create_layer(kl.Activation,
                            name=name + '_relu',
                            activation='relu')

  def call(self, inputs, training=None):
    """
    Forward pass
    :param inputs:    Tensor to be transformed
    :param training:  If `True` indicates training phase.
    :return:          Processed data
    """
    x = self.pad(inputs)
    x = self.conv(x)
    x = self.bn(x, training=training)
    return self.act(x)


class SpatialPath(kl.Layer):

  def __init__(self, filters, name='SpatialPath', **kwargs):
    """
    Constructor
    :param filters: Number of filter if the first ConvBlock, double each time
    :param name:      Layer's name
    :param kwargs:    Extra keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(SpatialPath, self).__init__(name=name, **kwargs)

    # Create Conv+BN+Relu blocks
    self.block1 = ConvBlock(filters=filters,
                            name='ConvBlock1',
                            layer_params=layer_params)
    self.block2 = ConvBlock(filters=2 * filters,
                            name='ConvBlock2',
                            layer_params=layer_params)
    self.block3 = ConvBlock(filters=4 * filters,
                            name='ConvBlock3',
                            layer_params=layer_params)

  def call(self, inputs, training=None):
    """
    Forward pass
    :param inputs:    Tensor to be transformed
    :param training:  If `True` indicates training phase.
    :return:          Processed data
    """
    x = self.block1(inputs, training=training)
    x = self.block2(x, training=training)
    return self.block3(x, training=training)  # [B, H/8, W/8, 256]


class AttentionRefinementModule(kl.Layer):

  def __init__(self,
               filters,
               name='AttentionRefinementModule',
               **kwargs):
    """
    Constructor
    :param filters: Number of filters in thge attention branch
    :param name:    Layer's name
    :param kwargs:  Keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(AttentionRefinementModule, self).__init__(name=name, **kwargs)
    self.conv = ConvBlock(filters=filters,
                          strides=1,
                          layer_params=layer_params)
    self.avgpool = create_layer(kl.GlobalAveragePooling2D,
                                name=name + '_pool',
                                layer_params=layer_params)
    self.reshape = create_layer(kl.Reshape,
                                name=name + '_reshape',
                                target_shape=(1, 1, filters),
                                layer_params=layer_params)
    self.conv_attn = create_layer(kl.Conv2D,
                                  name=name + '_conv_attn',
                                  filters=filters,
                                  kernel_size=1,
                                  use_bias=False,
                                  layer_params=layer_params)
    self.bn_attn = create_layer(kl.BatchNormalization,
                                name=name + '_bn_attn',
                                momentum=0.9,
                                layer_params=layer_params)
    self.act_attn = create_layer(kl.Activation,
                                 name=name + '_sigmoid_attn',
                                 activation='sigmoid',
                                 layer_params=layer_params)
    self.mul = create_layer(kl.Multiply,
                            name=name + '_multiply',
                            layer_params=layer_params)

  def call(self, inputs, training=None):
    """
    Forward pass
    :param inputs:    Tensor to be transformed
    :param training:  If `True` indicates training phase.
    :return:          Processed data
    """
    feat = self.conv(inputs, training=training)
    x = self.avgpool(feat)
    x = self.reshape(x)   # [B, 1, 1, C]
    x = self.conv_attn(x)
    x = self.bn_attn(x, training=training)
    x = self.act_attn(x)
    # channels of input and x should be same
    x = self.mul([feat, x])
    return x


class PredictionHead(kl.Layer):
  """ Classification head """

  def __init__(self,
               filters,
               n_classes,
               name='PredictionHead',
               **kwargs):
    """
    Constructor
    Build a layer composed of (Conv + BN + Relu) block followed by Conv
    :param filters:     Number of filters un the conv block
    :param n_classes:   Number of classes to predict
    :param name:        Layer's name
    :param kwargs:      Extra keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(PredictionHead, self).__init__(name=name, **kwargs)
    # Conv block
    self.conv_block = ConvBlock(filters=filters,
                                strides=1,
                                layer_params=layer_params,
                                **kwargs)
    # Prediction
    self.pred = create_layer(kl.Conv2D,
                             kernel_size=1,
                             filters=n_classes,
                             use_bias=False,
                             name='pred',
                             layer_params=layer_params)

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:    Features
    :param training:  If `True` indicates training phase, otherwise no.
    :return:  Prediction
    """
    x = self.conv_block(inputs, training=training)
    return self.pred(x)


class FeatureFusionModule(kl.Layer):

  def __init__(self, filters, name='FeatureFusionModel', **kwargs):
    """
    Constructor
    :param n_classes: Number of filters
    :param name:      Layer's name
    :param kwargs:    Keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(FeatureFusionModule, self).__init__(name=name, **kwargs)
    # Build layer
    self.cat = create_layer(kl.Concatenate,
                            name=name + '_concat',
                            axis=-1,    # Feature channel
                            layer_params=layer_params)
    self.avgpool = create_layer(kl.GlobalAveragePooling2D,
                                name=name + '_pool',
                                layer_params=layer_params)
    self.reshape = create_layer(kl.Reshape,
                                name=name + '_reshape',
                                target_shape=(1, 1, filters),
                                layer_params=layer_params)
    self.convblock = ConvBlock(filters=filters,
                               strides=1,
                               kernel_size=1,
                               padding=0,
                               name=name + '_conv_block',
                               layer_params=layer_params)
    # Conv1 = Conv + relu
    self.conv1 = create_layer(kl.Conv2D,
                              name=name + '_conv1',
                              filters=filters // 4,
                              kernel_size=1,
                              strides=1,
                              use_bias=False,
                              activation='relu',
                              layer_params=layer_params)
    # Conv1 = Conv + sigmoid
    self.conv2 = create_layer(kl.Conv2D,
                              name=name + '_conv2',
                              filters=filters,
                              kernel_size=1,
                              strides=1,
                              activation='sigmoid',
                              use_bias=False,
                              layer_params=layer_params)
    self.mul = create_layer(kl.Multiply,
                            name=name + '_mul',
                            layer_params=layer_params)
    self.add = create_layer(kl.Add,
                            name=name + '_add',
                            layer_params=layer_params)

  def call(self, inputs, training=None):
    """
    Forward pass
    :param inputs:    Lis of tensor
    :param training:  If `True` indicates training phase.
    :return:          Processed data
    """

    x = self.cat(inputs)
    feature = self.convblock(x, training=training)

    # Attention
    x = self.avgpool(feature)
    x = self.reshape(x)
    x = self.conv1(x)             # + relu
    x = self.conv2(x)             # + relu
    x = self.mul([feature, x])
    x = self.add([x, feature])
    return x


_backbone_ctor_fn = {'resnet18': ResNet.ResNet18,
                     'resnet34': ResNet.ResNet34,
                     'resnet50v2': ResNet.ResNet50V2,
                     'resnet101v2': ResNet.ResNet101V2}

_feat8_layer_name = {'resnet18': 'stage2_block2_add',
                     'resnet34': 'stage2_block4_add',
                     'resnet50v2': 'conv3_block4_out',
                     'resnet101v2': 'conv3_block4_out'}
_feat16_layer_name = {'resnet18': 'stage3_block2_add',
                      'resnet34': 'stage3_block6_add',
                      'resnet50v2': 'conv4_block6_out',
                      'resnet101v2': 'conv4_block23_out'}
_feat32_layer_name = {'resnet18': 'stage4_block2_add',
                      'resnet34': 'stage4_block3_add',
                      'resnet50v2': 'conv5_block3_out',
                      'resnet101v2': 'conv5_block3_out'}


class ContextPath(kl.Layer):
  """ Contextual path: ResNet18 or ResNet101 """

  def __init__(self,
               type='resnet18',
               name='ContextPath',
               **kwargs):
    """
    Constructor
    :param type:    Type of contextual extractor: 'resnet18' or 'resnet101'
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    layer_params = kwargs.pop('layer_params', None)
    super(ContextPath, self).__init__(name=name, **kwargs)

    # Backbone
    self.backbone = None
    self.preprocessing_fn = None

    # Backbone ctor function
    model_fn = _backbone_ctor_fn.get(type, None)
    model = model_fn(include_top=False,
                     pooling='avg',
                     layer_params=layer_params)
    feat8 = _feat8_layer_name.get(type, None)
    feat16 = _feat16_layer_name.get(type, None)
    feat32 = _feat32_layer_name.get(type, None)
    inputs = model.inputs
    outputs = [model.get_layer(feat8).output,      # 1/8
               model.get_layer(feat16).output,      # 1/16
               model.get_layer(feat32).output,      # 1/32
               model.output]
    self.backbone = tf.keras.Model(inputs, outputs, name=model.name)
    self.preprocessing_fn = model.inputs_config.preprocessing_fn

    # Attention refinement modules
    self.arm16 = AttentionRefinementModule(filters=128,
                                           name='Arm16',
                                           layer_params=layer_params)
    self.arm32 = AttentionRefinementModule(filters=128,
                                           name='Arm32',
                                           layer_params=layer_params)
    shp = (1, 1, self.backbone.output_shape[-1][1])
    self.reshape = create_layer(kl.Reshape,
                                name='reshape',
                                target_shape=shp,
                                layer_params=layer_params)
    # Up sampling layer
    self.up16 = create_layer(kl.UpSampling2D,
                             name='up16',
                             size=2,
                             interpolation='nearest',
                             layer_params=layer_params)
    self.up32 = create_layer(kl.UpSampling2D,
                             name='up32',
                             size=2,
                             interpolation='nearest',
                             layer_params=layer_params)

    # Conv block
    self.conv_head32 = ConvBlock(filters=128,
                                 strides=1,
                                 name='conv_head32',
                                 layer_params=layer_params)
    self.conv_head16 = ConvBlock(filters=128,
                                 strides=1,
                                 name='conv_head16',
                                 layer_params=layer_params)
    self.conv_avg = ConvBlock(filters=128,
                              strides=1,
                              kernel_size=1,
                              padding=0,
                              name='conv_avg',
                              layer_params=layer_params)

  def call(self, inputs, training=None):
    """
    Forward pass
    :param inputs:    Image to process
    :param training:  If `True` indicates training phase ortherwise no.
    :return:  feat8, feat16, feat32
              feat8:   (None, 28, 28, 128)
              feat16:  (None, 28, 28, 128)
              feat32:  (None, 14, 14, 128)
    """

    # Backbone
    x = self.preprocessing_fn(inputs)
    feat8, feat16, feat32, tail = self.backbone(x, training=training)
    tail = self.reshape(tail)

    # Aggregation tail + feat32
    feat_avg = self.conv_avg(tail, training=training)
    feat32_arm = self.arm32(feat32, training=training)
    feat32_sum = feat32_arm + feat_avg  # No upsamling use broadcasting
    feat32_up = self.up32(feat32_sum)
    feat32_up = self.conv_head32(feat32_up, training=training)

    # Aggregation feat32 + feat16
    feat16_arm = self.arm16(feat16, training=training)
    feat16_sum = feat16_arm + feat32_up
    feat16_up = self.up32(feat16_sum)
    feat16_up = self.conv_head16(feat16_up)
    return feat8, feat16_up, feat32_up


class BiSeNetInputConfig(InputConfigAbstract):

  def __init__(self):
    """ Constructor """
    super(BiSeNetInputConfig, self).__init__([InputSpec(dims=(None, None, 3),
                                                        fmt=ImageFormat.RGB,
                                                        name='input')])

    self._preprocess_fn = kl.Lambda(tf.identity, name='preprocessing')
    self._revert_preprocess_fn = kl.Lambda(tf.identity,
                                           name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


class BiSeNet(Network):
  """ Bilateral Segmentation Network """

  def __init__(self,
               n_classes,
               with_auxiliary_head=True,
               type='resnet18',
               name='BiSeNet',
               **kwargs):
    """
    Constructor
    :param n_classes:   Number of classes to predict
    :param with_auxiliary_head:  If `True add auxiliary prediction head
    :param type:  Type of context extractor, `resnet18` or `resnet101`
    :param name:  Network's name
    :param kwargs:  Extra keyword argumentss
    """
    layer_params = kwargs.pop('layer_params', None)
    output_name = ['main_prediction']
    if with_auxiliary_head:
      output_name.append('aux_prediction_1')
      output_name.append('aux_prediction_2')
    super(BiSeNet, self).__init__(config=BiSeNetInputConfig(),
                                  output_names=output_name,
                                  name=name,
                                  **kwargs)


    # Spatial path -> Use first block of resnet instead!!!
    # self.sp = SpatialPath(filters=64, layer_params=layer_params)
    # Context path (i.e. resnet)
    self.cp = ContextPath(type=type, layer_params=layer_params)
    # Feature Fusion module
    self.n_classes = n_classes
    self.ffm = FeatureFusionModule(filters=256,
                                   layer_params=layer_params)
    # Final
    self.up = create_layer(kl.UpSampling2D,
                           name='up',
                           size=8,
                           interpolation='bilinear',
                           layer_params=layer_params)
    self.up32 = create_layer(kl.UpSampling2D,
                             name='up32',
                             size=16,
                             interpolation='bilinear',
                             layer_params=layer_params)
    self.pred = PredictionHead(filters=256,
                               n_classes=n_classes,
                               name='main_pred',
                               layer_params=layer_params)
    # Auxiliary prediction layer
    self.aux_head1 = None
    self.aux_head2 = None
    self.with_auxliary_head = with_auxiliary_head
    if self.with_auxliary_head:
      self.aux_head1 = PredictionHead(filters=64,
                                      n_classes=n_classes,
                                      name='aux_pred1',
                                      layer_params=layer_params)
      self.aux_head2 = PredictionHead(filters=64,
                                      n_classes=n_classes,
                                      name='aux_pred2',
                                      layer_params=layer_params)

    # Training object
    self._optimizer_instance = None
    self._losses_instance = None
    self._metrics_instance = None

  @classmethod
  def CelebA(cls, type, name='BiSeNet', **kwargs):
    """
    Create inference model for CelebA Mask segmentation
    :param type:    Type of backbone
    :param name:    Network name
    :param kwargs:  Extra keyword arguments
    :return:  Model
    """
    return cls(n_classes=19,
               with_auxiliary_head=False,
               type=type,
               name=name,
               **kwargs)

  @classmethod
  def Helen(cls, type, name='BiSeNet', **kwargs):
    """
    Create inference model for Helen Mask segmentation
    :param type:    Type of backbone
    :param name:    Network name
    :param kwargs:  Extra keyword arguments
    :return:  Model
    """
    return cls(n_classes=11,
               with_auxiliary_head=False,
               type=type,
               name=name,
               **kwargs)

  def call(self, inputs, training=None):
    """
    Forward passs
    :param inputs:  Image to segment
    :param training:  If `True` indicates training phase, otherwise not.
    :return: Segmentation + auxiliary prediction if needed
    """
    # Context path
    feat8, feat16up, feat32up = self.cp(inputs, training=training)
    # output of feature fusion module
    feat_sp = feat8  # use res3b1 feature to replace spatial path feature
    feat_fuse = self.ffm([feat_sp, feat16up], training=training)
    # Main prediction
    feat_out = self.pred(feat_fuse, training=training)
    feat_out = self.up(feat_out)
    if self.with_auxliary_head:
      feat_out16 = self.aux_head1(feat16up, training=training)
      feat_out32 = self.aux_head2(feat32up, training=training)
      feat_out16 = self.up(feat_out16)
      feat_out32 = self.up32(feat_out32)
      return feat_out, feat_out16, feat_out32
    return feat_out

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :param kwargs: Extra keyword arguments
    :keyword type:  Type of optimizer
    :keyword learning_rate: Learning rate
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    if self._optimizer_instance is None:
      opt_type = kwargs.pop('type', 'Adam')
      weight_decay = kwargs.get('weight_decay', None)
      if weight_decay is not None:
        if opt_type == 'RectifiedAdam':
          from tensorflow_addons.optimizers import RectifiedAdam
          self._optimizer_instance = RectifiedAdam(**kwargs)
        else:
          from tensorflow_addons.optimizers import AdamW
          self._optimizer_instance = AdamW(**kwargs)
      else:
        optimizer_cls = get_optimizer(opt_type)
        self._optimizer_instance = optimizer_cls(**kwargs)
    return self._optimizer_instance

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :param kwargs: Extra keyword arguments
    :keyword from_logits:
    :keyword ignore_label:  Label to ignore during fitting (i.e. background
                            index)
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    if self._losses_instance is None:
      nc = self.n_classes
      ignore_idx = kwargs.get('ignore_label', None)
      if ignore_idx is None:
        # Use standard losses -> keras
        def _loss_fn_wrapper():
          if nc == 2:
            return Ohem(BinaryCrossentropy(from_logits=True),
                        threshold=0.7,
                        min_sample=(224 * 224 // 16),
                        from_logits=True)
          else:
            # https://github.com/zllrunning/face-parsing.PyTorch/tree/master
            # value taken from train.py / loss.py
            return Ohem(SparseCategoricalCrossentropy(from_logits=True),
                        threshold=0.7,
                        min_sample=(224 * 224 // 16),
                        from_logits=True)
            # return SparseCategoricalCrossentropy(from_logits=True)
        loss_fn = _loss_fn_wrapper
      else:
        # Use custom losses
        def _loss_fn_wrapper():
          if nc == 2:
            return BinCE(ignore_idx)
          else:
            return SparseCE(ignore_idx)
        loss_fn = _loss_fn_wrapper

      self._losses_instance = [loss_fn()]
      if self.with_auxliary_head:
        self._losses_instance.append(loss_fn())
        self._losses_instance.append(loss_fn())
    return self._losses_instance

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :param kwargs:  Extra keyword arguments
    :keyword cmap:  Colormap
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    if self._metrics_instance is None:
      nc = self.n_classes
      cmap = kwargs.get('cmap', None)

      def _meaniou_fn_wrapper():
        if nc == 2:
          return MeanIoU(num_classes=nc, name='MeanIoU')
        else:
          def _ypred_fn(x):
            return tf.expand_dims(tf.argmax(x, axis=-1), -1)

          return MeanIoU(num_classes=nc, name='MeanIoU', y_pred_fn=_ypred_fn)

      def _acc_fn_wrapper():
        if nc == 2:
          return BinaryAccuracy()
        else:
          return SparseCategoricalAccuracy()
      # List of metrics for each outputs
      self._metrics_instance = [_meaniou_fn_wrapper(),
                                _acc_fn_wrapper(),
                                ConfusionMatrix(num_classes=nc),
                                SegmentationMask(nc, cmap=cmap)]
    return self._metrics_instance

  def load_weights(self, filepath, by_name=False):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights, or `imagenet` to use pre-trained network.
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    """
    if filepath == 'imagenet':
      from lts5.utils.files import get_file
      from lts5.tensorflow_op.network.resnet import WEIGHTS_COLLECTION

      def _get_url(name, include_top):
        """
        Retrieve model url
        :param name:        Model's name
        :param include_top: If top is included
        :return:  Lits of candidates or empty if no match is found
        """
        w = list(filter(lambda x: x['name'] == name, WEIGHTS_COLLECTION))
        w = list(filter(lambda x: x['include_top'] == include_top, w))
        return w

      # Get model weight
      url = _get_url(name=self.cp.backbone.name, include_top=False)
      if url:
        url = url[0]  # Pick first element
        # Match known configuration
        weights_path = get_file(url['name'],
                                url['url'],
                                cache_subdir='models',
                                file_hash=url['md5'])
        # initialize backbone with pretraind model
        return self.cp.backbone.load_weights(weights_path)
      else:
        raise ValueError('There is no weights for such configuration: ' +
                         'model = {}'.format(self.name) +
                         'classes = {}, '.format(self.n_classes) +
                         'include_top = {}'.format(self.include_top))
    else:
      # Load default
      return super(BiSeNet, self).load_weights(filepath, by_name)
