# coding=utf-8
"""
S3FD: Single Shot Scale-invariant Face Detector implementation

See:
  https://arxiv.org/pdf/1708.05237.pdf
  https://github.com/HiKapok/DAN
"""
from os.path import basename, splitext
import tensorflow as _tf
import numpy as _np
from imageio import imread as _imread
import cv2 as _cv
from tensorflow.keras.layers import Layer, Lambda, Dense, Flatten
from tensorflow.keras.layers import Conv2D, ZeroPadding2D, MaxPool2D
from lts5.tensorflow_op.network.network import ImageFormat, InputSpec
from lts5.tensorflow_op.network.network import Network, InputConfigAbstract
from lts5.tensorflow_op.data.preprocessing import rgb_to_bgr
from lts5.tensorflow_op.layers.prior_box import PriorBoxConfig, PriorBox
from lts5.tensorflow_op.layers.normalization import L2Normalize
from lts5.tensorflow_op.layers.detection_output import DetectionOutput
from lts5.tensorflow_op.layers.detection_output import DetectionOutputConfig

__author__ = 'Christophe Ecabert'


class BlockConfiguration(object):
  """ Feature extractor configuration -> VGG style """

  def __init__(self,
               filters: list,
               kernel_sizes,
               strides,
               padding: list,
               pooling: bool,
               names: list):
    """
    Construction
    :param filters:       List of integer representing the number of filters
                          present in the block
    :param kernel_sizes:  List of all filter's kernel dimensions. If tuple
                          replicate n times
    :param strides:       List of strides for every filters. If tuple
                          replicate n times
    :param padding:       List of type of explicit padding to apply before each
                          convolution
    :param pooling:       If `True` add padding at the end of the block
    :param names:   List of str
    """

    def _convert_to_list_if(x, n):
      if isinstance(x, (tuple, str)):
        return [x] * n
      return x

    nf = len(filters)
    self._filters = filters
    self._k_sizes = _convert_to_list_if(kernel_sizes, n=nf)
    self._strides = _convert_to_list_if(strides, n=nf)
    self._padding = padding
    self._pooling = pooling
    self._names = names

  @property
  def filters(self):
    return self._filters

  @property
  def kernel_sizes(self):
    return self._k_sizes

  @property
  def strides(self):
    return self._strides

  @property
  def paddings(self):
    return self._padding

  @property
  def pooling(self):
    return self._pooling

  @property
  def names(self):
      return self._names


class PredictionBranchConfig(object):
  """ Configuration for one bounding box prediction branch """

  def __init__(self,
               n_score: int,
               feature_name: str,
               feature_normalization: bool,
               pbox_config:PriorBoxConfig):
    """
    Constructor
    :param n_score: Number of score to predict for each boxes
    :param feature_name:  Name of the layer to select feature from
    :param feature_normalization: If `True` apply feature normalization first
    :param pbox_config:  PriorBox layer configuration
    """
    self._n_score = n_score
    self._feat_name = feature_name
    self._feat_norm = feature_normalization
    self._prior_box_cfg = pbox_config

  @property
  def n_score(self):
      return self._n_score

  @property
  def feature_name(self):
    return self._feat_name

  @property
  def normalize(self):
    return self._feat_norm

  @property
  def prior_box_config(self):
    return self._prior_box_cfg


class VGGBlock(Layer):
  """ Building block for VGG style architecture """

  def __init__(self, config, name, index, **kwargs):
    """
    Constructor
    :param config:  Configuration of the block
    :param name:    Block's name
    :param index:   Block index
    :param kwargs:  Extra keyword arguments
    """
    super(VGGBlock, self).__init__(name=name, **kwargs)
    self._cfg = config
    # Build layers
    # ---------------------------------------------------------------
    # Explicit padding + Convolutions
    self.pad = []
    self.conv = []
    for idx, (nf, ks, s, p, n) in enumerate(zip(self._cfg.filters,
                                                self._cfg.kernel_sizes,
                                                self._cfg.strides,
                                                self._cfg.paddings,
                                                self._cfg.names)):
      self.pad.append(ZeroPadding2D(padding=(p, p)))
      self.conv.append(Conv2D(filters=nf,
                              kernel_size=ks,
                              strides=s,
                              padding='valid',
                              activation='relu',
                              use_bias=True,
                              name=n))
    # Pooling
    self.pool = None
    if self._cfg.pooling:
      self.pool = MaxPool2D(pool_size=(2, 2),
                            strides=(2, 2),
                            padding='same',
                            name='pool{}'.format(index))

  def call(self, inputs):
    """
    Forward
    :param inputs:  Tensor to process
    :param kwargs:  Extra keyword arguments
    :keyword training: If `True` indicates training phase
    :return:  Transformed inputs
    """
    # Go through all conv
    x = inputs
    for pad_layer, conv_layer in zip(self.pad, self.conv):
      x = pad_layer(x)
      x = conv_layer(x)
    # Pooling ?
    if self.pool is not None:
      return self.pool(x), x
    # Done
    return x, None

  def load_weights(self, filepath, by_name=False, **kwargs):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    :param kwargs:    Extra keyword arguments
    :keyword skip_layer: List of layer's name to be trained from scratch
                         (i.e. skip restoration)
    """

    def _reload_from_dict(w_dict: dict):
      """
      Load model from dictionnary of weights
      :param w_dict:  Weights
      """
      # Restore pre-trained model from numpy file
      tf_converter = {'kernel': 'weights',
                      'bias': 'biases'}
      # Go through all conv
      for conv in self.conv:
        layer_name = conv.name.encode()
        # Need to initialize from pre-trained
        data = w_dict.get(layer_name, None)
        np_weights = []
        for w in conv.weights:
          start = w.name.rfind('/') + 1
          stop = w.name.find(':')
          wname = w.name[start:stop]
          wname = tf_converter.get(wname, wname).encode()
          np_weights.append(data[wname])
        conv.set_weights(np_weights)

    # Check if numpy file
    if isinstance(filepath, dict):
      _reload_from_dict(w_dict=filepath)
    elif splitext(basename(filepath))[1] == '.npy':
      # Load the weights into memory
      w_dict = _np.load(filepath, encoding="bytes", allow_pickle=True).item()
      _reload_from_dict(w_dict=w_dict)
    else:
      super(VGGBlock, self).load_weights(filepath, by_name)


class PredictionBranch(Layer):
  """ Implement a single bounding box prediction branch (score, bbox, prior) """

  def __init__(self,
               config: PredictionBranchConfig,
               name: str,
               **kwargs):
    """
    Constructor
    :param config:    Branch configuration
    :param name:      Branch's name
    :param kwargs:    Extra keyword arguments
    """
    super(PredictionBranch, self).__init__(name=name, **kwargs)
    self._cfg = config

    feat_name = self._cfg.feature_name
    # Normalization
    self.norm = None
    if self._cfg.normalize:
      feat_name += '_norm'
      self.norm = L2Normalize(name=feat_name)
    # Confidence (i.e. score)
    self.conf = Conv2D(filters=self._cfg.n_score,
                      kernel_size=(3, 3),
                      strides=(1, 1),
                      padding='same',
                      use_bias=True,
                      name='{}_mbox_pred_conf'.format(feat_name))
    # Location regression
    self.loc = Conv2D(filters=4,
                      kernel_size=(3, 3),
                      strides=(1, 1),
                      padding='same',
                      use_bias=True,
                      name='{}_mbox_pred_loc'.format(feat_name))
    # BBox prior
    self.prior = PriorBox(config=self._cfg.prior_box_config)

  def call(self, inputs):
    """
    Forward pass
    :param inputs:  List of tensors: [image, features]
    :return:  tuple: location, confidence, prior
    """
    image = inputs[0]
    x = inputs[1]
    # Normalize first
    if self.norm is not None:
      x = self.norm(x)
    # Confidence
    conf = self._confidence(x)
    # Location
    loc = self._location(x)
    # Prior
    prior = self._prior(image, x)
    # Done
    return loc, conf, prior

  def _confidence(self, feature):
    """
    Predict confidence from feature
    :param feature: Feature
    :return:  Confidence score for each pixel
    """
    bs = _tf.shape(feature)[0]
    mbox_conf = self.conf(feature)
    # BBox prediction is binary but could have multiple class if ns > 2.
    # Therefore pick maximum value
    if self._cfg.n_score > 2:
      n_max = self._cfg.n_score - 1
      max_out = _tf.reduce_max(mbox_conf[..., :n_max], axis=-1)
      mbox_conf = _tf.stack([max_out, mbox_conf[..., -1]], axis=-1)
    mbox_conf = _tf.reshape(mbox_conf, shape=[bs, -1, 2])
    mbox_conf = _tf.nn.softmax(mbox_conf, axis=-1)
    return mbox_conf

  def _location(self, feature):
    """
    Regress bounding box location
    :param feature: Feature maps
    :return:  Bbox location
    """
    bs = _tf.shape(feature)[0]
    loc = self.loc(feature)
    return _tf.reshape(loc, shape=[bs, -1, 4])

  def _prior(self, image, feature):
    """
    Generate bounding box prior
    :param image:   Input image (full scale)
    :param feature: Feature
    :return:  Priors
    """
    return self.prior(inputs=[image, feature])

  def load_weights(self, filepath, by_name=False, **kwargs):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    :param kwargs:    Extra keyword arguments
    :keyword skip_layer: List of layer's name to be trained from scratch
                         (i.e. skip restoration)
    """
    def _reload_from_dict(w_dict: dict):
      """
      Load model from dictionnary of weights
      :param w_dict:  Weights
      """
      # Restore pre-trained model from numpy file
      tf_converter = {'kernel': 'weights',
                      'bias': 'biases'}
      fname = self._cfg.feature_name
      fname += '_norm' if self._cfg.normalize else ''
      naming_converter = {fname + '_mbox_pred_conf': fname + '_mbox_conf',
                          fname + '_mbox_pred_loc': fname + '_mbox_loc'}
      # Go through all layers
      layers = [self.conf, self.loc]
      if self.norm is not None:
        layers.append(self.norm)
      for layer in layers:
        layer_name = naming_converter.get(layer.name, layer.name).encode()
        # Need to initialize from pre-trained
        data = w_dict.get(layer_name, None)
        np_weights = []
        for w in layer.weights:
          start = w.name.rfind('/') + 1
          stop = w.name.find(':')
          wname = w.name[start:stop]
          wname = tf_converter.get(wname, wname).encode()
          np_weights.append(data[wname])
        layer.set_weights(np_weights)

    # Check if numpy file
    if isinstance(filepath, dict):
      _reload_from_dict(w_dict=filepath)
    elif splitext(basename(filepath))[1] == '.npy':
      # Load the weights into memory
      w_dict = _np.load(filepath, encoding="bytes", allow_pickle=True).item()
      _reload_from_dict(w_dict=w_dict)
    else:
      super(PredictionBranch, self).load_weights(filepath, by_name)


# Backbone configuration
_cfg_feat = [BlockConfiguration(filters=[64, 64],
                                kernel_sizes=(3, 3),
                                strides=(1, 1),
                                padding=[1, 1],
                                pooling=True,
                                names=['conv1_1', 'conv1_2']),
             BlockConfiguration(filters=[128, 128],
                                kernel_sizes=(3, 3),
                                strides=(1, 1),
                                padding=[1, 1],
                                pooling=True,
                                names=['conv2_1', 'conv2_2']),
             BlockConfiguration(filters=[256, 256, 256],
                                kernel_sizes=(3, 3),
                                strides=(1, 1),
                                padding=[1, 1, 1],
                                pooling=True,
                                names=['conv3_1', 'conv3_2', 'conv3_3']),
             BlockConfiguration(filters=[512, 512, 512],
                                kernel_sizes=(3, 3),
                                strides=(1, 1),
                                padding=[1, 1, 1],
                                pooling=True,
                                names=['conv4_1', 'conv4_2', 'conv4_3']),
             BlockConfiguration(filters=[512, 512, 512],
                                kernel_sizes=(3, 3),
                                strides=(1, 1),
                                padding=[1, 1, 1],
                                pooling=True,
                                names=['conv5_1', 'conv5_2', 'conv5_3']),
             BlockConfiguration(filters=[1024],
                                kernel_sizes=(3, 3),
                                strides=(1, 1),
                                padding=[3],
                                pooling=False,
                                names=['fc6']),
             BlockConfiguration(filters=[1024],
                                kernel_sizes=(1, 1),
                                strides=(1, 1),
                                padding=[0],
                                pooling=False,
                                names=['fc7']),
             BlockConfiguration(filters=[256, 512],
                                kernel_sizes=[(1, 1), (3, 3)],
                                strides=[(1, 1), (2, 2)],
                                padding=[0, 1],
                                pooling=False,
                                names=['conv6_1', 'conv6_2']),
             BlockConfiguration(filters=[128, 256],
                                kernel_sizes=[(1, 1), (3, 3)],
                                strides=[(1, 1), (2, 2)],
                                padding=[0, 1],
                                pooling=False,
                                names=['conv7_1', 'conv7_2'])]
# Default prediction branches configuration
_cfg_pred = [PredictionBranchConfig(n_score=4,
                                    feature_name='conv3_3',
                                    feature_normalization=True,
                                    pbox_config=PriorBoxConfig(min_size=[16.0],
                                                               step=4,
                                                               clip=False)),
             PredictionBranchConfig(n_score=2,
                                    feature_name='conv4_3',
                                    feature_normalization=True,
                                    pbox_config=PriorBoxConfig(min_size=[32.0],
                                                               step=8,
                                                               clip=False)),
             PredictionBranchConfig(n_score=2,
                                    feature_name='conv5_3',
                                    feature_normalization=True,
                                    pbox_config=PriorBoxConfig(min_size=[64.0],
                                                               step=16,
                                                               clip=False)),
             PredictionBranchConfig(n_score=2,
                                    feature_name='fc7',
                                    feature_normalization=False,
                                    pbox_config=PriorBoxConfig(min_size=[128.0],
                                                               step=32,
                                                               clip=False)),
             PredictionBranchConfig(n_score=2,
                                    feature_name='conv6_2',
                                    feature_normalization=False,
                                    pbox_config=PriorBoxConfig(min_size=[256.0],
                                                               step=64,
                                                               clip=False)),
             PredictionBranchConfig(n_score=2,
                                    feature_name='conv7_2',
                                    feature_normalization=False,
                                    pbox_config=PriorBoxConfig(min_size=[512.0],
                                                               step=128,
                                                               clip=False))]
# Default detection configuration
_cfg_det = DetectionOutputConfig(num_classes=2,
                                 share_location=True,
                                 background_label_id=0,
                                 nms_threshold=0.3,
                                 nms_top_k=5000,
                                 code_type=2,
                                 keep_top_k=750,
                                 confidence_threshold=0.05)


class SFDInputConfig(InputConfigAbstract):
  """ Input specs for SFD """

  def __init__(self):
    """ Constructor """
    super(SFDInputConfig, self).__init__([InputSpec(dims=(None, None, 3),
                                                    fmt=ImageFormat.RGB,
                                                    name='input')])

    def _preprocess_fn(image):
      x = image
      x = rgb_to_bgr(x)
      x = _tf.subtract(x, [104.0, 117.0, 123.0])
      if x.get_shape().rank == 3:
        x = _tf.expand_dims(x, 0)
      return x

    def _revert_fn(x):
      x = x + _tf.convert_to_tensor([104.0, 117.0, 123.0])
      return _tf.reverse(x, axis=[-1])

    self._preprocess_fn = Lambda(_preprocess_fn, name='preprocessing')
    self._revert_preprocess_fn = Lambda(_revert_fn, name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    """
    Provide pre-processing layer
    :return:  Instance of `tf.keras.layer.Layer`
    """
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


class SFD(Network):
  """ Scale invariant Single-Shot Face Detector Network """

  def __init__(self,
               add_preprocess=True,
               extractor_config=None,
               prediction_config=None,
               detection_config=None,
               name='SFD',
               **kwargs):
    """
    Constructor
    :param add_preprocess:  If `True` add preprocessing step.
    :param extractor_config:  Feature extractor configuration (Based on VGG). If
                              `None` use default one.
    :param prediction_config: Configuration of each prediction branch. If
                              `None` use default one.
    :param detection_config:  DetectionOutput configuration. If
                              `None` use default one.
    :param name:    Network's name (top level scope)
    :param kwargs:  Extra keyword arguments
    """
    super(SFD, self).__init__(config=SFDInputConfig(),
                              input_names=None,
                              output_names=None,
                              name=name,
                              **kwargs)
    # Pre-processing
    self._need_preprocess = add_preprocess
    # Backbone config
    self._feat_cfg = extractor_config or _cfg_feat
    self._blocks = []
    for idx, cfg in enumerate(self._feat_cfg):
      self._blocks.append(VGGBlock(config=cfg,
                                   name='Block{}'.format(idx + 1),
                                   index=idx))
    # Prediction branches
    self._pred_cfg = prediction_config or _cfg_pred
    self._preds = []
    for idx, cfg in enumerate(self._pred_cfg):
      self._preds.append(PredictionBranch(config=cfg,
                                          name='Prediction{}'.format(idx)))
    # Detection output
    self._det_cfg = detection_config or _cfg_det
    self.detection = DetectionOutput(config=self._det_cfg)

  def call(self, inputs):
    """
    Forward pass
    :param inputs:  Tensor holding image to processing
    :return:  Detection output
    """
    image = inputs
    # Pre-processing required ?
    if self._need_preprocess:
      image = self.inputs_config.preprocessing_fn(image)
    # Backbone
    feature_maps = {}
    x = image
    for block in self._blocks:
      x, feat = block(x)
      # Log feature for later use in prediction branch
      conv_name = block._layers[1].layers[-1].name
      feature_maps[conv_name] = feat if feat is not None else x
    # BBox prediction branches, uses features from various levels
    loc = []
    conf = []
    prior = []
    for cfg, pred in zip(self._pred_cfg, self._preds):
      # Get feature name
      x = feature_maps[cfg.feature_name]
      lc, cf, pr = pred([image, x])
      loc.append(lc)
      conf.append(cf)
      prior.append(pr)
    # Fuse them all together
    mbox_loc = _tf.concat(loc, axis=1)
    mbox_conf = _tf.concat(conf, axis=1)
    mbox_prior = _tf.concat(prior, axis=1)
    # BBox detection
    bboxes = self.detection(inputs=[mbox_loc, mbox_conf, mbox_prior])
    return bboxes

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    return None

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    return []

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    return []

  def load_weights(self, filepath, by_name=False, **kwargs):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    :param kwargs:    Extra keyword arguments
    :keyword skip_layer: List of layer's name to be trained from scratch
                         (i.e. skip restoration)
    """
    # Check if numpy file
    _, ext = splitext(basename(filepath))
    if ext == '.npy':
      # Load the weights into memory
      w_dict = _np.load(filepath, encoding="bytes", allow_pickle=True).item()
      for bck in self._blocks:
        bck.load_weights(w_dict)
      for pred in self._preds:
        pred.load_weights(w_dict)
    else:
      super(SFD, self).load_weights(filepath, by_name)


class S3FaceDetector:
  """ User friendly class encapsulating pre/post processing for SFD network """

  def __init__(self, model_path):
    """
    Constructor
    :param model_path:  File storing detection model
    """
    # Create network
    self._model = SFD()
    self._model.build(self._model.inputs_config.inputs_spec[0].dims)
    self._model.load_weights(model_path)

  def process(self, image_or_path, threshold=0.5, image_size=640.0):
    """
    Run detection on a given image or path to an image.
    :param image_or_path:   Path or image (`str` or `numpy.ndarray`)
    :param threshold:       Detection threshold
    :param image_size:      Size to resize the input image
    :return:  List of bounding box
    """
    # Load image
    if isinstance(image_or_path, str):
      image = _np.asarray(_imread(image_or_path)).astype(_np.float32)
    elif isinstance(image_or_path, _np.ndarray):
      image = image_or_path.astype(_np.float32)
    else:
      raise ValueError('`image_or_path` must be a `str` or `numpy.ndarray`')

    # Rescale it
    height = image.shape[0]
    width = image.shape[1]
    if image_size > 0.0:
      im_shrink = image_size / max(height, width)
      image = _cv.resize(image,
                         None,
                         None,
                         fx=im_shrink,
                         fy=im_shrink,
                         interpolation=_cv.INTER_LINEAR)
    # Run detection
    dets = self._model(image).numpy()
    # Select only detection larger than thresholds
    select = _np.where(dets[:, 2] > threshold)[0]
    dets = dets[select, :]
    for k, d in enumerate(dets):
      xmin = d[3] * width
      ymin = d[4] * height
      xmax = d[5] * width
      ymax = d[6] * height
      dets[k, 3:] = (xmin, ymin, xmax, ymax)
    return dets
