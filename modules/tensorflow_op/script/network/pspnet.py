# coding=utf-8
"""
Pyramid Scene Parsing Network for semantic segmentation

See:
  - https://arxiv.org/pdf/1612.01105.pdf
"""
import tensorflow as _tf
from tensorflow.keras.layers import Layer
from tensorflow.keras.layers import Conv2D, BatchNormalization, Activation
from tensorflow.keras.layers import ZeroPadding2D, SpatialDropout2D
from tensorflow.keras.layers import MaxPooling2D, Lambda
from tensorflow.keras.losses import BinaryCrossentropy
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from tensorflow.keras.metrics import BinaryAccuracy, SparseCategoricalAccuracy
from lts5.tensorflow_op.utils import Params
from lts5.utils.decorator import deprecated
from lts5.tensorflow_op.network.network import Network
from lts5.tensorflow_op.network.network import ImageFormat
from lts5.tensorflow_op.network.network import InputSpec, InputConfigAbstract
from lts5.tensorflow_op.layers.pooling import AdaptiveAvgPooling2D
from lts5.tensorflow_op.network.resnet import ResNet, WEIGHTS_COLLECTION
from lts5.tensorflow_op.network.utils import get_optimizer
from lts5.tensorflow_op.metrics import ConfusionMatrix, SegmentationMask
from lts5.tensorflow_op.metrics import MeanIoU
from lts5.tensorflow_op.losses import SparseCategoricalCrossentropy as SparseCE
from lts5.tensorflow_op.losses import BinaryCrossentropy as BinCE
__author__ = 'Christophe Ecabert'


class PredictionHeadConfig(Params):
  """
  Main prediction head parameters

  Args:
    n_classes:  Number of classes to be predicted
    n_filter:   Number of filters to use for the branch
    rate:       Probability of droping a feature map
  """

  @classmethod
  def default(cls):
    return cls(n_classes=2, n_filter=512, rate=0.1)


class ResBlockTuning(Params):
  """
  Configuration for tuning individual res block to change dilation rate in
  order to change the dimensions of the feature maps

  Args:
    block:    int, Index of the block to change
    level     int, Level inside the block
    filters:  list, List of name pattern to look for
  """

  def __init__(self, block, level, filters):
    super(ResBlockTuning, self).__init__(block=block,
                                         level=level,
                                         filters=filters)


class PSPNetConfig(Params):
  """
  Configuration for PSPNet

  Args:
    backbone_ctor:  Callable initializing backbone feature extractor
    pyr_level:      List of integers defining the spatial resolution in the
                    pyramid pooling module
    main_head_cfg:  Configuration for the main prediction head
    aux_head_cfg:   Configuration for the auxiliary prediction head
    aux_input_layer:  Name of input layer for the auxiliary prediction head
    extra_cfg:      Dictionnary with extra configuration for variouss blocks
                    Dilation_rate/padding/.. in order to increase feature output
                    dimensions
    block3_cfg:     How to update block3 configuration
    block4_cfg:     How to update block4 configuration
  """

  @classmethod
  def from_name(cls, name):

    if name == 'resnet18':
      return cls.resnet18()
    elif name == 'resnet34':
      return cls.resnet34()
    elif name == 'resnet50v2':
      return cls.resnet50v2()
    else:
      raise ValueError('Unknown configuration: {}'.format(name))

  @classmethod
  def resnet18(cls):
    def _backbone_ctor(**kwargs):
      return ResNet.ResNet18(include_top=False, **kwargs)

    # Change dilation rate to get feature 1/8 of the size of the input image
    # i.e. if img = 224 x 224 => feat = 28 x 28
    # Each block divide the resolution by 2, therefore only the block 3/4 will
    # will be updated
    conv_name = Conv2D.__name__
    pad_name = ZeroPadding2D.__name__
    p = {conv_name: {'stage3_block1_conv1': {'dilation_rate': (2, 2),
                                             'strides': (1, 1)},
                     'stage3_block1_sc': {'dilation_rate': (2, 2),
                                          'strides': (1, 1)},
                     'stage4_block1_conv1': {'dilation_rate': (4, 4),
                                             'strides': (1, 1)},
                     'stage4_block1_sc': {'dilation_rate': (4, 4),
                                          'strides': (1, 1)}},
         pad_name: {'stage3_block1_pad1': {'padding': ((2, 2), (2, 2))},
                    'stage4_block1_pad1': {'padding': ((4, 4), (4, 4))}}}

    return cls(backbone_ctor=_backbone_ctor,
               pyr_level=[1, 2, 3, 6],
               main_head_cfg=PredictionHeadConfig(n_classes=2,
                                                  n_filter=128,
                                                  rate=0.1),
               aux_head_cfg=PredictionHeadConfig(n_classes=2,
                                                 n_filter=64,
                                                 rate=0.1),
               aux_input_layer='stage3_block2_add',
               extra_cfg=p,
               block3_cfg=ResBlockTuning(block=3,
                                         level=1,
                                         filters=['_sc',
                                                  '_pad1',
                                                  '_conv1']),
               block4_cfg=ResBlockTuning(block=4,
                                         level=1,
                                         filters=['_sc',
                                                  '_pad1',
                                                  '_conv1']))

  @classmethod
  def resnet34(cls):
    def _backbone_ctor(**kwargs):
      return ResNet.ResNet34(include_top=False, **kwargs)

    # Change dilation rate to get feature 1/8 of the size of the input image
    # i.e. if img = 224 x 224 => feat = 28 x 28
    # Each block divide the resolution by 2, therefore only the block 3/4 will
    # will be updated
    conv_name = Conv2D.__name__
    pad_name = ZeroPadding2D.__name__
    p = {conv_name: {'stage3_block1_conv1': {'dilation_rate': (2, 2),
                                             'strides': (1, 1)},
                     'stage3_block1_sc': {'dilation_rate': (2, 2),
                                          'strides': (1, 1)},
                     'stage4_block1_conv1': {'dilation_rate': (4, 4),
                                             'strides': (1, 1)},
                     'stage4_block1_sc': {'dilation_rate': (4, 4),
                                          'strides': (1, 1)}},
         pad_name: {'stage3_block1_pad1': {'padding': ((2, 2), (2, 2))},
                    'stage4_block1_pad1': {'padding': ((4, 4), (4, 4))}}}

    return cls(backbone_ctor=_backbone_ctor,
               pyr_level=[1, 2, 3, 6],
               main_head_cfg=PredictionHeadConfig(n_classes=2,
                                                  n_filter=128,
                                                  rate=0.1),
               aux_head_cfg=PredictionHeadConfig(n_classes=2,
                                                 n_filter=64,
                                                 rate=0.1),
               aux_input_layer='stage3_block6_add',
               extra_cfg=p,
               block3_cfg=ResBlockTuning(block=3,
                                         level=1,
                                         filters=['_sc',
                                                  '_pad1',
                                                  '_conv1']),
               block4_cfg=ResBlockTuning(block=4,
                                         level=1,
                                         filters=['_sc',
                                                  '_pad1',
                                                  '_conv1']))

  @classmethod
  def resnet50v2(cls):
    def _backbone_ctor(**kwargs):
      return ResNet.ResNet50V2(include_top=False, **kwargs)

    # Change dilation rate to get feature 1/8 of the size of the input image
    # i.e. if img = 224 x 224 => feat = 28 x 28
    # Each block divide the resolution by 2, therefore only the block 3/4 will
    # will be updated
    conv_name = Conv2D.__name__
    pad_name = ZeroPadding2D.__name__
    p = {conv_name: {'conv3_block1_2_conv': {'dilation_rate': (2, 2),
                                             'strides': (1, 1)},
                     'conv3_block1_0_conv': {'dilation_rate': (2, 2),
                                             'strides': (1, 1)},
                     'conv4_block1_2_conv': {'dilation_rate': (4, 4),
                                             'strides': (1, 1)},
                     'conv4_block1_0_conv': {'dilation_rate': (4, 4),
                                             'strides': (1, 1)}},
         pad_name: {'conv3_block1_2_pad': {'padding': ((2, 2), (2, 2))},
                    'conv4_block1_2_pad': {'padding': ((4, 4), (4, 4))}}}

    return cls(backbone_ctor=_backbone_ctor,
               pyr_level=[1, 2, 3, 6],
               main_head_cfg=PredictionHeadConfig.default(),
               aux_head_cfg=PredictionHeadConfig(n_classes=2,
                                                 n_filter=256,
                                                 rate=0.1),
               aux_input_layer='conv4_block6_out',
               extra_cfg=p,
               block3_cfg=ResBlockTuning(block=3,
                                         level=1,
                                         filters=['_0_conv',
                                                  '_max_pooling',
                                                  '_2_pad',
                                                  '_2_conv']),
               block4_cfg=ResBlockTuning(block=4,
                                         level=1,
                                         filters=['_0_conv',
                                                  '_max_pooling',
                                                  '_2_pad',
                                                  '_2_conv']))


class PyramidLevel(Layer):
  """ Single level of pyramid pooling module """

  def __init__(self,
               size,
               channel_out,
               sampling_mode='bilinear',
               name='PyramidLevel',
               **kwargs):
    """
    Constructor
    :param size:    Integer or tuple(H, W) of dimensions after adpative pooling
    :param channel_out: Number of feature after reduction
    :param sampling_mode: str, mode of sampling during feature interpolation
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    super(PyramidLevel, self).__init__(name=name, **kwargs)
    # Build internal layers
    # AdaptivePooling + Conv + BN + Relu + UpSampling
    self.pool = AdaptiveAvgPooling2D(size,
                                     name='pool1')
    # Conv
    self.conv = Conv2D(filters=channel_out,
                       kernel_size=(1, 1),
                       strides=(1, 1),
                       use_bias=False,
                       name='conv1')
    # Bn
    self.bn = BatchNormalization(name='bn1')
    # Activation
    self.relu = Activation('relu')
    # Upsampling
    self.out_height = -1
    self.out_width = -1
    self.sampling_mode = sampling_mode

  def build(self, input_shape):
    """
    Finalize layer
    :param input_shape: Dimensions of input tensor
    """
    self.out_height = input_shape[1]
    self.out_width = input_shape[2]
    super(PyramidLevel, self).build(input_shape)

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:  Tensor to be processed
    :return:  Transformed tensor
    """
    # Pool
    x = self.pool(inputs)
    x = self.conv(x)
    x = self.bn(x, training=training)
    x = self.relu(x)
    # Upsampling
    x = _tf.image.resize(images=x,
                         size=(self.out_height, self.out_width),
                         method=self.sampling_mode)
    return x


class PyramidPoolingModule(Layer):
  """ Pyramid Pooling Module for multiscale feature learning """

  def __init__(self, levels, name='PyramidPoolingModule', **kwargs):
    """
    Constructor
    :param levels:  List/tuple of integers defining the size of the downsampled
                    feature maps
    :param name:    Layer's name
    :param kwargs:  Extra keyword arguments
    """
    super(PyramidPoolingModule, self).__init__(name=name, **kwargs)
    # levels
    self._sizes = levels
    self._levels = []

  def build(self, input_shape):
    """
    Build layer
    :param input_shape: Dimensions of the input tensors
    """
    # Build pyramid
    n_feature = input_shape[-1]
    n_channel = int(n_feature // len(self._sizes))
    for k, lvl in enumerate(self._sizes):
      feat = PyramidLevel(size=lvl,
                          channel_out=n_channel,
                          name='PyramidLevel{}'.format(k + 1))
      self._levels.append(feat)
    # Done
    super(PyramidPoolingModule, self).build(input_shape)

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:    Tensor to process
    :param training:  If `True` indicates training phase, otherwise test
    :return:  Processed features
    """
    # Goes through all level of the pyramid
    feats = [inputs]
    for lvl in self._levels:
      x = lvl(inputs, training=training)
      feats.append(x)
    # Fuse all features together
    return _tf.concat(feats, axis=-1)


class PredictionHead(Layer):
  """ Segmentation prediction head """

  def __init__(self,
               n_classes: int,
               n_filters: int = 512,
               kernel_size: tuple = (3, 3),
               rate: float = 0.1,
               name='PredictionHead',
               **kwargs):
    """
    Constructor
    :param n_filters:   Number of filter used to predict labels
    :param kernel_size: Filters size
    :param rate:        Spatial dropout rate
    :param n_classes:   Number of classes to predict
    :param name:        Layer's name
    :param kwargs:      Extra keyword arguments
    """
    super(PredictionHead, self).__init__(name=name, **kwargs)
    # Save parameters
    self._n_classes = n_classes
    self._rate = rate
    self._ksize = kernel_size
    self._nf = n_filters
    # Build layers
    self.pad = ZeroPadding2D(padding=((1, 1), (1, 1)),
                             name='pad1')
    self.conv1 = Conv2D(filters=self._nf,
                        kernel_size=self._ksize,
                        use_bias=False,
                        kernel_initializer='he_normal',
                        name='conv1')
    self.bn1 = BatchNormalization(name='bn1')
    self.relu1 = Activation('relu',
                            name='relu1')
    self.drop1 = SpatialDropout2D(rate=self._rate,
                                  name='drop1')

    activation = 'sigmoid' if self._n_classes == 2 else 'softmax'
    nf = 1 if self._n_classes == 2 else self._n_classes
    self.pred = Conv2D(filters=nf,
                       kernel_size=1,
                       kernel_initializer='he_normal',
                       activation=activation,
                       name='pred')

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:    Feature tensor
    :param training:  If `True` indicates training phase
    :return:  Class probability map
    """
    x = self.pad(inputs)
    x = self.conv1(x)
    x = self.bn1(x, training=training)
    x = self.relu1(x)
    x = self.drop1(x, training=training)
    return self.pred(x)


class PSPNetInputConfig(InputConfigAbstract):
  """ Input configuration for PSPNet """

  def __init__(self):
    """ Constructor """
    super(PSPNetInputConfig, self).__init__([InputSpec(dims=(None, None, 3),
                                                       fmt=ImageFormat.RGB,
                                                       name='input')])

    self._preprocess_fn = Lambda(_tf.identity, name='preprocessing')
    self._revert_preprocess_fn = Lambda(_tf.identity,
                                        name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


@deprecated('Will be removed later on, use `PSPNetV2` instead.')
class PSPNet(Network):
  """ Pyramid-Scene-Parsing network """

  def __init__(self,
               config=None,
               with_auxiliary_head=False,
               name='PSPNet',
               **kwargs):
    """
    Constructor
    :param config:  Network configuration, if `None` use default one
    :param name:    Network's name
    :param kwargs:  Extra keyword arguments
    """
    output_name = ['main_prediction']
    if with_auxiliary_head:
      output_name.append('aux_prediction')
    super(PSPNet, self).__init__(config=PSPNetInputConfig(),
                                 output_names=output_name,
                                 name=name,
                                 **kwargs)
    if config is None:
      cfg = PSPNetConfig.from_name('resnet50v2')
    elif isinstance(config, str):
      cfg = PSPNetConfig.from_name(config)
    else:
      cfg = config
    self.config = cfg
    # Backbone
    cfg, preproc_fn, invert_preproc_fn = self._initialize_backbone(self.config)
    self.preprocess_fn = preproc_fn
    self.revert_preprocess_fn = invert_preproc_fn
    self.backbone = _tf.keras.Model.from_config(cfg)

    # Pyramid
    self.pyr = PyramidPoolingModule(levels=self.config.pyr_level)
    # Main prediction head
    mh_cfg = self.config.main_head_cfg
    self.main_pred = PredictionHead(n_classes=mh_cfg.n_classes,
                                    n_filters=mh_cfg.n_filter,
                                    kernel_size=(3, 3),
                                    rate=mh_cfg.rate,
                                    name='MainPredictionHead')
    self.aux_pred = None
    if with_auxiliary_head:
      aux_cfg = self.config.main_head_cfg
      self.aux_feat = None
      self.aux_pred = PredictionHead(n_classes=aux_cfg.n_classes,
                                     n_filters=aux_cfg.n_filter,
                                     kernel_size=(3, 3),
                                     rate=aux_cfg.rate,
                                     name='AuxPredictionHead')
    # Optimizer, losses, metrics
    self._optimizer_instance = None
    self._losses_instance = None
    self._metrics_instance = None

  def call(self, inputs, training=None):
    """
    Forward pass
    :param inputs:    Images to be segmented
    :param training:  If `True` indicates training phase
    :return:  Class probability maps
    """

    # Backbone
    shp = _tf.shape(inputs)
    x = self.preprocess_fn(inputs)
    feat, aux_feat = self.backbone(x, training=training)
    # Pyramid
    pyr_feat = self.pyr(feat, training=training)
    # Main head
    main_pred = self.main_pred(pyr_feat, training=training)
    main_pred = _tf.image.resize(main_pred, size=shp[1:3])
    if self.aux_pred:
      # Auxiliary prediction path
      aux_pred = self.aux_pred(aux_feat, training=training)
      aux_pred = _tf.image.resize(aux_pred, size=shp[1:3])
      return main_pred, aux_pred
    # Only main prediction
    return main_pred

  @staticmethod
  def _initialize_backbone(config):
    """
    Update ResNet to output proper feature maps
    :param config: PSPNet config
    :return:  config dict, preprocessing_fn, revert_preprocessing_fn
    """

    # Create temporary graph
    with _tf.name_scope('PSPNet') as scope:
      with _tf.Graph().as_default() as g:
        def _get_residual_layer_component(m,
                                          block: int,
                                          level: int,
                                          filters: list):
          """
          Search for individual layers in ResNet backbone
          :param m:         Model instance lts5.tensorflow_op.netowrk.Network
          :param block:     Index of the block
          :param level:     Level in the block
          :param filters:   Names to look for
          :return:  Matched layers
          """
          candidates = []
          pattern = '{}_block{}'.format(block, level)
          for layer in m.layers:
            if pattern in layer.name:
              # Search if given layers match the expected values
              for kw in filters:
                if kw in layer.name:
                  candidates.append((layer.name, layer))
          return candidates

        # Build model
        model = config.backbone_ctor()

        # Change dilation rate to get feature 1/8 of the size of the input image
        # i.e. if img = 224 x 224 => feat = 28 x 28
        # Each block divide the resolution by 2, therefore only the block 3/4 will
        # will be updated
        bck3_cfg = config.block3_cfg
        block3 = _get_residual_layer_component(m=model,
                                               block=bck3_cfg.block,
                                               level=bck3_cfg.level,
                                               filters=bck3_cfg.filters)
        bck4_cfg = config.block4_cfg
        block4 = _get_residual_layer_component(m=model,
                                               block=bck4_cfg.block,
                                               level=bck4_cfg.level,
                                               filters=bck4_cfg.filters)
        # Change properties, i.e. add dilation rate + remove stride + correct
        # padding if any
        for n, l in block3:
          if isinstance(l, Conv2D):
            # 2D Conv, change dilation + stride
            l.dilation_rate = (2, 2)
            l.strides = (1, 1)
          elif isinstance(l, ZeroPadding2D):
            l.padding = ((2, 2), (2, 2))
          elif isinstance(l, MaxPooling2D):
            l.strides = (1, 1)
        for n, l in block4:
          if isinstance(l, Conv2D):
            # 2D Conv, change dilation + stride
            l.dilation_rate = (4, 4)
            l.strides = (1, 1)
          elif isinstance(l, ZeroPadding2D):
            l.padding = ((4, 4), (4, 4))
          elif isinstance(l, MaxPooling2D):
            l.strides = (1, 1)
        # Create new model with the modified version of the original ResNet
        cfg = model.get_config()
      # Update input dimensions to be unknown
      idx = [k for k, x in enumerate(cfg['layers']) if
             x['class_name'] == 'InputLayer']
      input_cfg = cfg['layers'][idx[0]]
      input_cfg['config']['batch_input_shape'] = (None, None, None, 3)
      # Add output for auxiliary features
      cfg['output_layers'].append([config.aux_input_layer, 0, 0])
      return (cfg,
              model.inputs_config.preprocessing_fn,
              model.inputs_config.revert_preprocessing_fn)

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :param kwargs: Extra keyword arguments
    :keyword type:  Type of optimizer
    :keyword learning_rate: Learning rate
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    if self._optimizer_instance is None:
      opt_type = kwargs.pop('type', 'Adam')
      optimizer_cls = get_optimizer(opt_type)
      self._optimizer_instance = optimizer_cls(**kwargs)
    return self._optimizer_instance

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :param kwargs: Extra keyword arguments
    :keyword from_logits:
    :keyword ignore_label:  Label to ignore during fitting (i.e. background
                            index)
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    if self._losses_instance is None:
      nc = self.config.main_head_cfg.n_classes
      ignore_idx = kwargs.get('ignore_label', None)
      if ignore_idx is None:
        # Use standard losses -> keras
        loss_fn = (BinaryCrossentropy if nc == 2 else
                   SparseCategoricalCrossentropy)
      else:
        # Use custom losses
        def _loss_fn_wrapper():
          if nc == 2:
            return BinCE(ignore_idx)
          else:
            return SparseCE(ignore_idx)

        loss_fn = _loss_fn_wrapper
      self._losses_instance = [loss_fn()]
      if self.aux_pred:
        self._losses_instance.append(loss_fn())
    return self._losses_instance

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :param kwargs:  Extra keyword arguments
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    if self._metrics_instance is None:
      nc = self.config.main_head_cfg.n_classes

      def _meaniou_fn_wrapper():
        if nc == 2:
          return MeanIoU(num_classes=nc, name='MeanIoU')
        else:
          def _ypred_fn(x):
            return _tf.expand_dims(_tf.argmax(x, axis=-1), -1)

          return MeanIoU(num_classes=nc, name='MeanIoU', y_pred_fn=_ypred_fn)

      def _acc_fn_wrapper():
        if nc == 2:
          return BinaryAccuracy()
        else:
          return SparseCategoricalAccuracy()
      # List of metrics for each outputs
      self._metrics_instance = [_meaniou_fn_wrapper(),
                                _acc_fn_wrapper(),
                                ConfusionMatrix(num_classes=nc),
                                SegmentationMask(nc)]
    return self._metrics_instance

  def load_weights(self, filepath, by_name=False):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights, or `imagenet` to use pre-trained network.
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    """
    if filepath == 'imagenet':
      from lts5.utils.files import get_file

      def _get_url(name, include_top):
        """
        Retrieve model url
        :param name:        Model's name
        :param include_top: If top is included
        :return:  Lits of candidates or empty if no match is found
        """
        w = list(filter(lambda x: x['name'] == name, WEIGHTS_COLLECTION))
        w = list(filter(lambda x: x['include_top'] == include_top, w))
        return w

      # Get model weight
      url = _get_url(name=self.backbone.name, include_top=False)
      if url:
        url = url[0]  # Pick first element
        # Match known configuration
        weights_path = get_file(url['name'],
                                url['url'],
                                cache_subdir='models',
                                file_hash=url['md5'])
        # Reload
        self.backbone.load_weights(weights_path)
      else:
        raise ValueError('There is no weights for such configuration: ' +
                         'model = {}'.format(self.name) +
                         'classes = {}, '.format(self.n_classes) +
                         'include_top = {}'.format(self.include_top))
    else:
      # Load default
      super(PSPNet, self).load_weights(filepath, by_name)


class PSPNetV2(Network):
  """ Pyramid-Scene-Parsing network """

  def __init__(self,
               config=None,
               with_auxiliary_head=False,
               name='PSPNet',
               **kwargs):
    """
    Constructor
    :param config:  Network configuration, if `None` use default one
    :param name:    Network's name
    :param kwargs:  Extra keyword arguments
    """
    output_name = ['main_prediction']
    if with_auxiliary_head:
      output_name.append('aux_prediction')
    super(PSPNetV2, self).__init__(config=PSPNetInputConfig(),
                                   output_names=output_name,
                                   name=name,
                                   **kwargs)
    if config is None:
      cfg = PSPNetConfig.from_name('resnet50v2')
    elif isinstance(config, str):
      cfg = PSPNetConfig.from_name(config)
    else:
      cfg = config
    self.config = cfg

    # Backbone initialization with parameters adaptation
    model = cfg.backbone_ctor(layer_params=cfg.extra_cfg)
    inputs = model.inputs
    outputs = [model.output,
               model.get_layer(self.config.aux_input_layer).output]
    self.backbone = _tf.keras.Model(inputs, outputs, name=model.name)
    self.preprocess_fn = model.inputs_config.preprocessing_fn
    self.revert_preprocess_fn = model.inputs_config.revert_preprocessing_fn
    # Pyramid
    self.pyr = PyramidPoolingModule(levels=self.config.pyr_level)
    # Main prediction head
    mh_cfg = self.config.main_head_cfg
    self.main_pred = PredictionHead(n_classes=mh_cfg.n_classes,
                                    n_filters=mh_cfg.n_filter,
                                    kernel_size=(3, 3),
                                    rate=mh_cfg.rate,
                                    name='MainPredictionHead')
    self.aux_pred = None
    if with_auxiliary_head:
      aux_cfg = self.config.main_head_cfg
      self.aux_feat = None
      self.aux_pred = PredictionHead(n_classes=aux_cfg.n_classes,
                                     n_filters=aux_cfg.n_filter,
                                     kernel_size=(3, 3),
                                     rate=aux_cfg.rate,
                                     name='AuxPredictionHead')
    # Optimizer, losses, metrics
    self._optimizer_instance = None
    self._losses_instance = None
    self._metrics_instance = None

  def call(self, inputs, training=None):
    """
    Forward pass
    :param inputs:    Images to be segmented
    :param training:  If `True` indicates training phase
    :return:  Class probablity maps
    """

    # Backbone
    shp = _tf.shape(inputs)
    x = self.preprocess_fn(inputs)
    feat, aux_feat = self.backbone(x, training=training)
    # Pyramid
    pyr_feat = self.pyr(feat, training=training)
    # Main head
    main_pred = self.main_pred(pyr_feat, training=training)
    main_pred = _tf.image.resize(main_pred, size=shp[1:3])
    if self.aux_pred:
      # Auxiliary prediction path
      aux_pred = self.aux_pred(aux_feat, training=training)
      aux_pred = _tf.image.resize(aux_pred, size=shp[1:3])
      return main_pred, aux_pred
    # Only main prediction
    return main_pred

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :param kwargs: Extra keyword arguments
    :keyword type:  Type of optimizer
    :keyword learning_rate: Learning rate
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    if self._optimizer_instance is None:
      opt_type = kwargs.pop('type', 'Adam')
      optimizer_cls = get_optimizer(opt_type)
      self._optimizer_instance = optimizer_cls(**kwargs)
    return self._optimizer_instance

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :param kwargs: Extra keyword arguments
    :keyword from_logits:
    :keyword ignore_label:  Label to ignore during fitting (i.e. background
                            index)
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    if self._losses_instance is None:
      nc = self.config.main_head_cfg.n_classes
      ignore_idx = kwargs.get('ignore_label', None)
      if ignore_idx is None:
        # Use standard losses -> keras
        loss_fn = (BinaryCrossentropy if nc == 2 else
                   SparseCategoricalCrossentropy)
      else:
        # Use custom losses
        def _loss_fn_wrapper():
          if nc == 2:
            return BinCE(ignore_idx)
          else:
            return SparseCE(ignore_idx)

        loss_fn = _loss_fn_wrapper
      self._losses_instance = [loss_fn()]
      if self.aux_pred:
        self._losses_instance.append(loss_fn())
    return self._losses_instance

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :param kwargs:  Extra keyword arguments
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    if self._metrics_instance is None:
      nc = self.config.main_head_cfg.n_classes

      def _meaniou_fn_wrapper():
        if nc == 2:
          return MeanIoU(num_classes=nc, name='MeanIoU')
        else:
          def _ypred_fn(x):
            return _tf.expand_dims(_tf.argmax(x, axis=-1), -1)

          return MeanIoU(num_classes=nc, name='MeanIoU', y_pred_fn=_ypred_fn)

      def _acc_fn_wrapper():
        if nc == 2:
          return BinaryAccuracy()
        else:
          return SparseCategoricalAccuracy()
      # List of metrics for each outputs
      self._metrics_instance = [_meaniou_fn_wrapper(),
                                _acc_fn_wrapper(),
                                ConfusionMatrix(num_classes=nc),
                                SegmentationMask(nc)]
    return self._metrics_instance

  def load_weights(self, filepath, by_name=False):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights, or `imagenet` to use pre-trained network.
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    """
    if filepath == 'imagenet':
      from lts5.utils.files import get_file

      def _get_url(name, include_top):
        """
        Retrieve model url
        :param name:        Model's name
        :param include_top: If top is included
        :return:  Lits of candidates or empty if no match is found
        """
        w = list(filter(lambda x: x['name'] == name, WEIGHTS_COLLECTION))
        w = list(filter(lambda x: x['include_top'] == include_top, w))
        return w

      # Get model weight
      url = _get_url(name=self.backbone.name, include_top=False)
      if url:
        url = url[0]  # Pick first element
        # Match known configuration
        weights_path = get_file(url['name'],
                                url['url'],
                                cache_subdir='models',
                                file_hash=url['md5'])
        # Reload
        return self.backbone.load_weights(weights_path)
      else:
        raise ValueError('There is no weights for such configuration: ' +
                         'model = {}'.format(self.name) +
                         'classes = {}, '.format(self.n_classes) +
                         'include_top = {}'.format(self.include_top))
    else:
      # Load default
      return super(PSPNetV2, self).load_weights(filepath, by_name)
