# coding=utf-8
""" Implementation "To learn image super-resolution, use a GAN to
 learn how to do image degradation first" from A. Bulat, ECCV 2018.

 The implementation only consider the High-to-Low mapping using GANs and
 Tensorflow keras API
"""
from time import time
from os.path import join as _join
from os.path import exists as _exists
from os import makedirs as _makedirs
import tensorflow as _tf
from tensorflow.python.framework.errors import InvalidArgumentError
from tensorflow.keras.layers import Layer
from tensorflow.keras.layers import Input, BatchNormalization, Flatten
from tensorflow.keras.layers import Dense, ReLU, Add, Reshape, Conv2D, LeakyReLU
from tensorflow.keras.layers import Concatenate, Lambda, MaxPool2D, UpSampling2D
from tensorflow.keras.models import Model
from tensorflow.python.ops import summary_ops_v2
from tensorflow.python.keras import backend as K
from lts5.tensorflow_op.layers import PixelShuffling
from lts5.tensorflow_op.normalizers.spectral import SpectralNormalization
from lts5.tensorflow_op.network.utils import get_optimizer
from lts5.tensorflow_op.utils import SummaryWriterManager
from lts5.utils.tools import init_logger

__author__ = 'Christophe Ecabert'

logger = init_logger()


class ResBlockConfig(object):
  """ Single residual block configuration """

  def __init__(self,
               with_bn,
               n_filters,
               activation='relu',
               sampling=None,
               with_sn=True):
    """
    Constructor
    :param with_bn:   If `True` add batch normalization before activation
                      function
    :param n_filters: List of 2 ints defining the number of filters in each
                      'Conv3x3' in the top branch.
    :param activation:  Type of activation layer
    :param sampling:  Sampling operation, possible value: ['up', 'down', None]
    :param with_sn:   If `True` add spectral normalization for all convolutions
                      in the layers. Default to `False`
    """
    # Sanity check
    if not isinstance(n_filters, list) or len(n_filters) != 2:
      raise ValueError('`n_filters` must be a list of two `int`!')
    if sampling not in ('up', 'down', None):
      raise ValueError('`sampling` must be from (\'up\', \'down\', None)')
    # Set properties
    self._with_bn = with_bn
    self._with_sn = with_sn
    self._n_filters = n_filters
    self._sampling = sampling
    self._act = activation.lower()

  @property
  def with_bn(self):
    return self._with_bn

  @property
  def with_sn(self):
    return self._with_sn

  @property
  def n_filters(self):
    return self._n_filters

  @property
  def sampling(self):
      return self._sampling

  @property
  def act(self):
    return self._act


class ResidualBlock(Layer):
  """ Residual block block with pre-activation.

    - Optional Batch Normalization
    - Optional Up-sampling layer (i.e. PixelShuffling)

  Structure:
  --.--> (BN) --> ReLU --> Conv3x3 --> (BN) --> ReLU --> Conv3x3 --+--(Up)-->
    |                                                              |
    + ----------------- (Conv1x1 / MaxPool2x2) --------------------+

  """

  def __init__(self, cfg, **kwargs):
    """
    Constructor
    :param cfg:       Block's configuration
    :param kwargs:    Extra keyword arguments
    """
    super(ResidualBlock, self).__init__(**kwargs)
    # Init layers
    self.cfg = cfg
    self.stride = 1 if self.cfg.sampling != 'down' else 2
    # self.upsampling = (PixelShuffling(block_size=2, name='up') if
    #                    self.cfg.sampling == 'up' else
    #                    None)
    self.upsampling = (UpSampling2D(size=(2, 2),
                                    interpolation='bilinear',
                                    name='up') if
                       self.cfg.sampling == 'up' else
                       None)
    # Forward
    self.bn1 = (BatchNormalization(name='bn1') if self.cfg.with_bn else
                None)
    # SN-GAN https://openreview.net/pdf?id=B1QRgziT-
    self.relu1 = (LeakyReLU(alpha=0.1, name='relu1') if self.cfg.act != 'relu'
                  else ReLU(name='relu1'))
    self.conv1 = Conv2D(filters=self.cfg.n_filters[0],
                        kernel_size=3,
                        strides=(self.stride, self.stride),
                        padding='SAME',
                        use_bias=False,
                        name='conv1')
    self.bn2 = (BatchNormalization(name='bn2') if self.cfg.with_bn else
                None)
    self.relu2 = (LeakyReLU(alpha=0.1, name='relu2') if self.cfg.act != 'relu'
                  else ReLU(name='relu2'))
    self.conv2 = Conv2D(filters=self.cfg.n_filters[1],
                        kernel_size=3,
                        strides=(1, 1),
                        padding='SAME',
                        use_bias=False,
                        name='conv2')
    # Normalization ?
    if self.cfg.with_sn:
      self.conv1 = SpectralNormalization(self.conv1)
      self.conv2 = SpectralNormalization(self.conv2)
    # Skip connection adaptation, needed if n_in != n_filters[1]
    self.skip = None
    # Fuse
    self.add = Add(name='add')

  def build(self, input_shape):
    """
    Build layers
    :param input_shape: Input dimensions
    """
    n_in = input_shape[-1]
    if ((n_in != self.cfg.n_filters[1]) or
        n_in == self.cfg.n_filters[1] and self.stride > 1):
      # Add convolution for adaptation
      self.skip = Conv2D(filters=self.cfg.n_filters[1],
                         kernel_size=1,
                         strides=(self.stride, self.stride),
                         padding='SAME',
                         use_bias=False,
                         name='skip')
      if self.cfg.with_sn:
        self.skip = SpectralNormalization(self.skip)
    # elif n_in == self.cfg.n_filters[1] and self.stride > 1:
    #   # Adapt spatial dimension with pooling
    #   self.skip = MaxPool2D(self.stride, name='skip')

    # Skip
    skip_shape = input_shape
    if self.skip:
      self.skip.build(input_shape)
      skip_shape = self.skip.compute_output_shape(input_shape)

    # Forward
    # -------------------------------
    shape = input_shape
    # Bn1 + relu1 (opt)
    if self.bn1:
      self.bn1.build(shape)
    self.relu1.build(shape)
    # Conv1
    self.conv1.build(shape)
    shape = self.conv1.compute_output_shape(shape)
    self.relu2.build(shape)
    # Conv2
    self.conv2.build(shape)
    shape = self.conv2.compute_output_shape(shape)
    # Merge
    self.add.build([shape, skip_shape])
    # Up (opt)
    if self.upsampling:
      self.upsampling.build(shape)
    super(ResidualBlock, self).build(input_shape)

  def call(self, inputs, **kwargs):
    """
    Forward pass
    :param inputs:    Input tensor
    :keyword training:  If `True` indicate training phase.
    :return:  Transformed input
    """
    inp = _tf.identity(inputs)
    # Skip connection
    res = self.skip(inp) if self.skip else inp
    # Forward - Optional Batch norm
    x = inp
    if self.bn1:
      x = self.bn1(x, **kwargs)
    x = self.relu1(x)
    x = self.conv1(x)
    if self.bn2:
      x = self.bn2(x, **kwargs)
    x = self.relu2(x)
    x = self.conv2(x)
    # Add together residual + skip
    y = self.add([x, res])
    # Up-sampling needed ?
    if self.upsampling:
      y = self.upsampling(y)
    return y

  def get_config(self):
    """
    Provide layer's configuration
    :return:  dict
    """
    cfg = super(ResidualBlock, self).get_config()
    cfg['cfg'] = self.cfg
    return cfg


# Default generator encoder configuration -> UNet style
_def_gen_enc_cfg = [ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling=None),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling='down'),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling=None),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling='down'),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling=None),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling='down'),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling='down')]
# Generator decoder
_def_gen_dec_cfg = [ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling='up'),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling=None),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling='up'),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling=None),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling=None)]

# Default discriminator configuration
_default_dis_cfg = [ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling=None,
                                   with_sn=True,
                                   activation='relu'),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling=None,
                                   with_sn=True,
                                   activation='relu'),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling=None,
                                   with_sn=True,
                                   activation='relu'),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling='down',
                                   with_sn=True,
                                   activation='relu'),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling='down',
                                   with_sn=True,
                                   activation='relu'),
                    ResBlockConfig(with_bn=False,
                                   n_filters=[64, 64],
                                   sampling='down',
                                   with_sn=True,
                                   activation='relu')]


class H2LGenerator(Model):
  """Generator

   Take a high resolution image as input and generate a realistic low resolution
   equivalent.

   See:
    - https://www.adrianbulat.com/downloads/ECCV18/image-super-resolution.pdf
   """

  def __init__(self,
               encoder_cfg=None,
               decoder_cfg=None,
               skip_con=(),
               activation='sigmoid',
               **kwargs):
    """
    Constructor
    :param encoder_cfg: List of residual block configuration object. If `None`
                        use default encoder configuration
    :param decoder_cfg: List of residual block configuration object. If `None`
                        use default decoder configuration
    :param skip_con:    Skip connection configuration
    :param kwargs:  Extra keyword arguments
    :keyword name:  Model's name (i.e. scope)
    """
    super(H2LGenerator, self).__init__(**kwargs)
    self._enc_cfg = encoder_cfg or _def_gen_enc_cfg
    self._dec_cfg = decoder_cfg or _def_gen_dec_cfg
    self._skip = list(skip_con) or [0, 2]
    # Create residual block
    self._n_encoder_block = 0
    self._n_decoder_block = 0
    for k, conf in enumerate(self._enc_cfg):
      bck = ResidualBlock(cfg=conf, name='EncoderBlock{}'.format(k))
      setattr(self, 'encoder_block_{}'.format(k), bck)
      self._n_encoder_block += 1
    for k, conf in enumerate(self._dec_cfg):
      bck = ResidualBlock(cfg=conf, name='DecoderBlock{}'.format(k))
      setattr(self, 'decoder_block_{}'.format(k), bck)
      self._n_decoder_block += 1

    for k in self._skip:
      # op = Add(name='merge_{}'.format(k))
      op = Concatenate(name='merge_{}'.format(k))
      setattr(self, 'fusion_layer_{}'.format(k), op)
    self._activation = activation
    self._image = Conv2D(filters=3,
                         kernel_size=3,
                         padding='SAME',
                         activation=self._activation,
                         name='output')

  def call(self, inputs, **kwargs):
    """
    Forward pass
    :param inputs:     `Tensor`, image to transform
    :param kwargs:     Extra keyword arguments
    :keyword training: If `True` indicate training phase.
    :return:  Down-sampled images
    """
    # Project noise
    x = inputs
    # Encoder
    enc_feat = []
    for k in range(self._n_encoder_block):
      bck = getattr(self, 'encoder_block_{}'.format(k))
      x = bck(x, **kwargs)
      # Store features ?
      if (k % 2) == 1:
        enc_feat.append(x)
    # Decoder
    for k in range(self._n_decoder_block):
      bck = getattr(self, 'decoder_block_{}'.format(k))
      x = bck(x, **kwargs)
      # merge features ?
      if k in self._skip:
        grp = k // 2
        op = getattr(self, 'fusion_layer_{}'.format(k))
        x = op([x, enc_feat[-(grp + 1)]])
    # Format output
    return self._image(x)


class H2LDiscriminator(Model):
  """ Discriminator

  Classifier taking a bunch of low resolution images and tell if they are 'real'
  or 'fake'

  See:
    - https://www.adrianbulat.com/downloads/ECCV18/image-super-resolution.pdf
  """

  def __init__(self,
               cfg=None,
               activation='sigmoid',
               **kwargs):
    """
    Constructor
    :param cfg:     List of residual block configuration object. If `None` use
                    default configuration
    :param activation: Prediction activation layer
    :param kwargs:  Extra keyword arguments
    :keyword name:  Model's name (i.e. scope)
    """
    super(H2LDiscriminator, self).__init__(**kwargs)
    # Setup config
    self._cfg = cfg or _default_dis_cfg
    # Create residual block
    self.n_block = 0
    for k, conf in enumerate(self._cfg):
      bck = ResidualBlock(cfg=conf, name='Block{}'.format(k))
      setattr(self, 'block_{}'.format(k), bck)
      self.n_block += 1
    # Add final prediction layer
    self._activation = activation
    self.gap = _tf.keras.layers.GlobalAveragePooling2D()
    self.pred = Dense(1,
                      activation=self._activation,
                      name='prediction')
    if self._cfg[0].with_sn:
      self.pred = SpectralNormalization(self.pred)

  def call(self, inputs, **kwargs):
    """
    Forward pass
    :param inputs:     `Tensor` of images to classify
    :param kwargs:     Extra keyword arguments
    :return:  logits
    """
    x = inputs
    for k in range(self.n_block):
      bck = getattr(self, 'block_{}'.format(k))
      x = bck(x, **kwargs)
    # Prediction
    x = self.gap(x)  # Global pooling
    return self.pred(x)


class DiscriminatorConfig(object):
  """ Structure holding discriminator configuration """

  def __init__(self,
               residual_config=None,
               optimizer_config=None,
               step=5,
               name=None):
    """
    Constructor
    :param residual_config: List of `ResBlockConfig` instance. If `None` use
                            default configuration with 12 residual blocks.
    :param optimizer_config: Optimizer configuration
                            dict:
                              - name: str,
                              - config: dict
    :param step:            Number of steps to train the discriminator before
                            doing a single training step for the generator
    :param name:            Discriminator's name (scope)
    """
    self._res_cfg = residual_config
    # Adam paramaters from Gulrajani et al.
    self._opt_cfg = optimizer_config or {'name': 'Adam',
                                         'config': {'learning_rate': 2e-4,
                                                    'beta_1': 0.0,
                                                    'beta_2': 0.9,
                                                    'name': 'dis_optimizer'}}
    self._step = step
    self._name = 'h2l_discriminator' if name is None else name
    self._optimizer = None

  @property
  def config(self):
      return self._res_cfg

  @property
  def optimizer(self):
    if self._optimizer is None:
      opt_cls = get_optimizer(self._opt_cfg['name'])
      self._optimizer = opt_cls(**self._opt_cfg['config'])
    return self._optimizer

  @property
  def step(self):
      return self._step

  @property
  def name(self):
      return self._name


class GeneratorConfig(object):
  """ Generator configuration """

  def __init__(self,
               encoder_cfg=None,
               decoder_cfg=None,
               optimizer_config=None,
               name=None):
    """
    Constructor
    :param encoder_cfg: Encoder configuration
    :param decoder_cfg: Decoder configuration
    :param optimizer_config: Optimizer configuration
                            dict:
                              - name: str,
                              - config: dict
    :param name:    Generator's name (scope)
    """
    self._encoder_cfg = encoder_cfg
    self._decoder_cfg = decoder_cfg
    self._name = 'h2l_generator' if name is None else name
    # Adam paramaters from Gulrajani et al.
    self._opt_cfg = optimizer_config or {'name': 'Adam',
                                         'config': {'learning_rate': 2e-4,
                                                    'beta_1': 0.0,
                                                    'beta_2': 0.9,
                                                    'name': 'gen_optimizer'}}
    self._optimizer = None

  @property
  def encoder_cfg(self):
    return self._encoder_cfg

  @property
  def decoder_cfg(self):
    return self._decoder_cfg

  @property
  def optimizer(self):
    if self._optimizer is None:
      opt_cls = get_optimizer(self._opt_cfg['name'])
      self._optimizer = opt_cls(**self._opt_cfg['config'])
    return self._optimizer

  @property
  def name(self):
    return self._name


class H2LSupervisor:
  """
  Supervisor for H2L gan training

  See:
   - https://www.tensorflow.org/beta/tutorials/generative/dcgan
  """

  def __init__(self,
               model_dir,
               dis_config: DiscriminatorConfig,
               gen_config: GeneratorConfig,
               weight: float = 0.05,
               log_freq: int = 1000):
    """
        Constructor, create H2L Supervisor
        :param opt_config:  Optimizer configuration
        :param dis_config:  Discriminator configuration (i.e. `DiscriminatorConfig`)
        :param gen_config:  Generator configuration (i.e. `GeneratorConfig`)
        :param hr_shape:    Dimensions of the high res image
        :param lr_shape:    Dimensions of the low res generated image
        """
    # Ratio between discriminator and generator training iteration
    self._n_step = dis_config.step

    # Summary
    # ------------------------------------------------------------------------
    self._log_step = log_freq
    self._log_dir = _join(model_dir, 'logs')
    if not _exists(self._log_dir):
      _makedirs(self._log_dir)

    self._summary = SummaryWriterManager(log_dir=self._log_dir,
                                         log_freqency=self._log_step)
    self._w_train = self._summary.get_writer('train')

    # Generator
    # ------------------------------------------------------------------------
    self._gen = H2LGenerator(encoder_cfg=gen_config.encoder_cfg,
                             decoder_cfg=gen_config.decoder_cfg,
                             activation='tanh',
                             name=gen_config.name)
    self._gen.build((None, 256, 256, 3))
    self._gen.summary()
    # Discriminator
    # ------------------------------------------------------------------------
    self._dis = H2LDiscriminator(cfg=dis_config.config,
                                 activation='linear',
                                 name=dis_config.name)
    self._dis.build((None, 64, 64, 3))
    self._dis.summary()
    # Loss
    # ------------------------------------------------------------------------
    self._bce = _tf.keras.losses.BinaryCrossentropy(from_logits=True)
    self._mae = _tf.keras.losses.MeanAbsoluteError()
    self._hinge = _tf.keras.losses.Hinge()
    self._w_rec = weight

    # Metric
    # ------------------------------------------------------------------------
    self._d_acc = _tf.keras.metrics.BinaryAccuracy(name='dis_acc',
                                                   threshold=0.0)  # logits
    self._g_acc = _tf.keras.metrics.BinaryAccuracy(name='gen_acc',
                                                   threshold=0.5)  # sigmoid
    self._d_loss = _tf.keras.metrics.Mean(name='d_loss')
    self._g_loss = _tf.keras.metrics.Mean(name='g_loss')
    self._gan_loss = _tf.keras.metrics.Mean(name='gan_loss')
    self._rec_loss = _tf.keras.metrics.Mean(name='rec_loss')

    # optimizers
    # ------------------------------------------------------------------------
    self._gen_optimizer = gen_config.optimizer
    self._dis_optimizer = dis_config.optimizer

    # Checkpoints
    # ------------------------------------------------------------------------
    # See: https://www.tensorflow.org/guide/checkpoint
    self._model_dir = model_dir
    if not _exists(self._model_dir):
      _makedirs(self._model_dir)
    self._step = _tf.Variable(0, trainable=False)
    self._log_g_step = _tf.Variable(0, dtype=_tf.int64, trainable=False)
    self._log_d_step = _tf.Variable(0, dtype=_tf.int64, trainable=False)
    self._ckpt = _tf.train.Checkpoint(step=self._step,
                                      log_g_step=self._log_g_step,
                                      log_d_step=self._log_d_step,
                                      gen=self._gen,
                                      dis=self._dis,
                                      gen_optimizer=self._gen_optimizer,
                                      dis_optimizer=self._dis_optimizer)
    self._manager = _tf.train.CheckpointManager(self._ckpt,
                                                directory=self._model_dir,
                                                max_to_keep=5)
    # Try to restore previous run of the training
    self._ckpt.restore(self._manager.latest_checkpoint)
    if self._manager.latest_checkpoint:
      logger.info('Restored from %s', self._manager.latest_checkpoint)
    else:
      logger.info('Initialize from scratch...')



  @staticmethod
  def load_networks(folder,
                    generator=True,
                    gen_input_shape=(None, 256, 256, 3),
                    discriminator=True,
                    dis_input_shape=(None, 64, 64, 3)):
    """
    Reload network from checkpoints
    :param folder:        Location where model is saved
    :param generator:     If `True` reload generator, otherwise not.
    :param gen_input_shape: Generator input dimensions
    :param discriminator: If `True` reload discriminator, otherwise not.
    :param dis_input_shape: Discriminator input dimensions
    """
    ckpt_args = {}
    ret_value = []
    if generator:
      gen = H2LGenerator(name='h2l_generator')
      gen.build(gen_input_shape)
      ckpt_args['gen'] = gen
      ret_value.append(gen)
    if discriminator:
      dis = H2LDiscriminator(name='h2l_discriminator')
      dis.build(dis_input_shape)
      ckpt_args['dis'] = dis
      ret_value.append(dis)
    # Reload
    latest = _tf.train.latest_checkpoint(folder)
    ckpt = _tf.train.Checkpoint(**ckpt_args)
    ckpt.restore(latest).expect_partial().assert_existing_objects_matched()
    return ret_value

  def train(self,
            gen_dataset,
            dis_dataset,
            n_epoch=100):
    """
    Training loop
    :param gen_dataset: Generator dataset
    :param dis_dataset: Discriminator dataset
    :param n_epoch:     Number of epoch to train for
    """
    # progress bar, total number of steps to take. Unknown at the beginning
    target = None
    for epoch in range(n_epoch):
      # Go through dataset
      gen_it = iter(gen_dataset.one_shot_iterator())
      dis_it = iter(dis_dataset.one_shot_iterator())
      self._d_acc.reset_states()
      self._g_acc.reset_states()
      batch_seen = 0
      logger.info('Epoch {}/{}'.format(epoch + 1,
                                       n_epoch))
      progbar = _tf.keras.utils.Progbar(target=target)
      while True:
        try:

          d_loss = 0.0
          real_hr, proxy_lr = next(gen_it)

          # Train discriminator for n-steps
          for i in range(self._n_step):
            # Train discriminator
            # -------------------------------------------------------------
            real_lr, label_lr = next(dis_it)
            d_loss = self._train_dis(real_hr, real_lr)
            # Log loss
            self._w_train.scalar(data=d_loss,
                                 name='Dis/Loss',
                                 step=self._log_d_step)
            self._w_train.scalar(data=self._d_acc.result(),
                                 name='Dis/Acc',
                                 step=self._log_d_step)
            self._log_d_step.assign_add(1)
          # Train generator
          # -------------------------------------------------------------
          if batch_seen == 0 and epoch == 0:
            print('Trace network...')
            _tf.summary.trace_on()
            g_losses, g_image = self._train_gen(real_hr, proxy_lr)
            with self._w_train._writer.as_default():
              _tf.summary.trace_export(name='Generator', step=0)
          else:
            g_losses, g_image = self._train_gen(real_hr, proxy_lr)
          g_loss, gan_loss, rec_loss = g_losses
          # Log loss
          self._w_train.scalar(data=g_loss,
                               name='Gen/Loss',
                               step=self._log_g_step)
          self._w_train.scalar(data=gan_loss,
                               name='Gen/GanLoss',
                               step=self._log_g_step)
          self._w_train.scalar(data=rec_loss,
                               name='Gen/RecLoss',
                               step=self._log_g_step)
          gen_image = (g_image + 1.0) / 2.0
          self._w_train.image(data=gen_image,
                              name='Gen/fake',
                              step=self._log_g_step, max_outputs=3)
          proxy_img = (proxy_lr + 1.0) / 2.0
          self._w_train.image(data=proxy_img,
                              name='Gen/proxy',
                              step=self._log_g_step, max_outputs=3)
          img = (real_hr + 1.0) / 2.0
          self._w_train.image(data=img,
                              name='Gen/input',
                              step=self._log_g_step, max_outputs=3)
          lr_img = (real_lr + 1.0) / 2.0
          self._w_train.image(data=lr_img,
                              name='Gen/real',
                              step=self._log_g_step, max_outputs=3)
          self._w_train.scalar(data=self._g_acc.result(),
                               name='Gen/Acc',
                               step=self._log_g_step)
          self._log_g_step.assign_add(self._n_step)



          # Update progress bar
          progbar.update(current=batch_seen,
                         values=[('d_loss', d_loss.numpy()),
                                 ('g_loss', g_loss.numpy()),
                                 ('gan_loss', gan_loss.numpy()),
                                 ('rec_loss', rec_loss.numpy())
                                 ])
          self._step.assign_add(1)
          batch_seen += 1

        except StopIteration:
          # Reach the end of dataset -> should be generator reach an epoch
          target = batch_seen
          # Clear metrics
          self._d_acc.reset_states()
          self._g_acc.reset_states()
          self._d_loss.reset_states()
          self._g_loss.reset_states()
          self._gan_loss.reset_states()
          self._rec_loss.reset_states()

          print('\n')  # Put progbar to the next line
          break
      # Save the model
      self._manager.save()

  def train_generator_only(self,
                           gen_dataset,
                           n_epoch=100):
    """
    Training loop
    :param gen_dataset: Generator dataset
    :param n_epoch:     Number of epoch to train for
    """

    # progress bar, total number of steps to take. Unknown at the beginning
    target = None
    for epoch in range(n_epoch):
      # Go through dataset
      gen_it = iter(gen_dataset.one_shot_iterator())
      self._g_acc.reset_states()
      batch_seen = 0
      logger.info('Epoch {}/{}'.format(epoch + 1,
                                       n_epoch))
      progbar = _tf.keras.utils.Progbar(target=target)
      while True:
        try:
          d_loss = 0.0
          real_hr, proxy_lr = next(gen_it)

          if batch_seen == 0 and epoch == 0:
            print('Trace network...')
            _tf.summary.trace_on()
            g_loss, g_image = self._train_gen_only(real_hr, proxy_lr)
            with self._w_train._writer.as_default():
              _tf.summary.trace_export(name='Generator', step=0)
          else:
            g_loss, g_image = self._train_gen_only(real_hr, proxy_lr)


          # Log loss
          self._w_train.scalar(data=g_loss,
                               name='Gen/Loss',
                               step=self._log_g_step)
          gen_image = (g_image + 1.0) / 2.0
          self._w_train.image(data=gen_image,
                              name='Gen/fake',
                              step=self._log_g_step, max_outputs=3)
          proxy_img = (proxy_lr + 1.0) / 2.0
          self._w_train.image(data=proxy_img,
                              name='Gen/proxy',
                              step=self._log_g_step, max_outputs=3)
          img = (real_hr + 1.0) / 2.0
          self._w_train.image(data=img,
                              name='Gen/input',
                              step=self._log_g_step, max_outputs=3)
          self._w_train.scalar(data=self._g_acc.result(),
                               name='Gen/Acc',
                               step=self._log_g_step)
          self._log_g_step.assign_add(self._n_step)

          # Update progress bar
          progbar.update(current=batch_seen,
                         values=[('g_loss', g_loss.numpy())])
          self._step.assign_add(1)
          batch_seen += 1

        except StopIteration:
          # Reach the end of dataset -> should be generator reach an epoch
          target = batch_seen
          # Clear metrics
          self._g_acc.reset_states()
          self._g_loss.reset_states()

          print('\n')  # Put progbar to the next line
          break
      # Save the model
      self._manager.save()

  def train_discriminator_only(self,
                               gen_dataset,
                               dis_dataset,
                               n_epoch=100):
    """
    Training loop
    :param gen_dataset: Generator dataset
    :param n_epoch:     Number of epoch to train for
    """

    # progress bar, total number of steps to take. Unknown at the beginning
    target = None
    for epoch in range(n_epoch):
      # Go through dataset
      proxy_it = iter(gen_dataset.one_shot_iterator())
      real_it = iter(dis_dataset.one_shot_iterator())

      self._d_acc.reset_states()
      batch_seen = 0
      logger.info('Epoch {}/{}'.format(epoch + 1,
                                       n_epoch))
      progbar = _tf.keras.utils.Progbar(target=target)
      while True:
        try:
          # Get data
          real_hr, proxy_lr = next(proxy_it)
          real_lr, _ = next(real_it)
          d_loss = self._train_dis_only(real_lr=real_lr, proxy_lr=proxy_lr)
          # Update progress bar
          progbar.update(current=batch_seen,
                         values=[('d_loss', d_loss.numpy()),
                                 ('d_acc', self._d_acc.result().numpy())])
          self._step.assign_add(1)
          batch_seen += 1

        except StopIteration:
          # Reach the end of dataset -> should be generator reach an epoch
          target = batch_seen
          # Clear metrics
          self._d_acc.reset_states()
          self._d_loss.reset_states()

          print('\n')  # Put progbar to the next line
          break
      # Save the model
      self._manager.save()

  @_tf.function
  def _train_dis(self, real_hr, real_lr):
    """
    Train discriminator
    :param real_hr:   Real high resolution image
    :param real_lr:   Real low resolution image
    :return:  Loss
    """
    with _tf.GradientTape() as tape:
      # Generate fake data
      fake_lr = self._gen(real_hr, training=True)
      # Predict if real or not
      fake_pred = self._dis(fake_lr, training=True)
      real_pred = self._dis(real_lr, training=True)
      # Compute loss
      d_loss = self._discriminator_loss(real_pred, fake_pred)
    # Get gradient + Update discriminator
    d_grad = tape.gradient(d_loss, self._dis.trainable_variables)
    self._dis_optimizer.apply_gradients(zip(d_grad,
                                            self._dis.trainable_variables))
    # Add metrics
    self._d_acc.update_state(_tf.ones_like(real_pred), real_pred)
    self._d_acc.update_state(_tf.zeros_like(fake_pred), fake_pred)
    self._d_loss.update_state(d_loss)
    return d_loss

  @_tf.function
  def _train_dis_only(self, real_lr, proxy_lr):
    """
    Train discriminator to differentiate between real LR image and downsampled
    image
    :param real_lr:   Real low resolution image
    :param proxy_lr:  Downsampled image
    :return:  Loss
    """
    with _tf.GradientTape() as tape:
      # Predict if real or not
      fake_pred = self._dis(proxy_lr, training=True)
      real_pred = self._dis(real_lr, training=True)
      # Compute loss
      d_loss = self._discriminator_loss(real_pred, fake_pred)
    # Get gradient + Update discriminator
    d_grad = tape.gradient(d_loss, self._dis.trainable_variables)
    self._dis_optimizer.apply_gradients(zip(d_grad,
                                            self._dis.trainable_variables))
    # Add metrics
    self._d_acc.update_state(_tf.ones_like(real_pred), real_pred)
    self._d_acc.update_state(_tf.zeros_like(fake_pred), fake_pred)
    self._d_loss.update_state(d_loss)
    return d_loss

  @_tf.function
  def _train_gen(self, real_hr, proxy_lr):
    """
    Train generator
    :param real_hr: Real high resolution image
    :param proxy_lr:  Lower resolution proxy (i.e. downsampled HR image)
    :return: Loss
    """
    with _tf.GradientTape() as tape:
      # Generate data
      fake_lr = self._gen(real_hr, training=True)
      # Predict if real or not
      fake_pred = self._dis(fake_lr, training=True)
      # Compute loss
      g_loss, gan_loss, rec_loss = self._generator_loss(fake_lr,
                                                        proxy_lr,
                                                        fake_pred,
                                                        self._w_rec)
    # Get gradient + Update generator
    g_grad = tape.gradient(g_loss, self._gen.trainable_variables)
    self._gen_optimizer.apply_gradients(zip(g_grad,
                                            self._gen.trainable_variables))
    # Add metrics
    self._g_acc.update_state(_tf.ones_like(fake_pred), _tf.sigmoid(fake_pred))
    self._g_loss.update_state(g_loss)
    self._gan_loss.update_state(gan_loss)
    self._rec_loss.update_state(rec_loss)
    return (g_loss, gan_loss, rec_loss), fake_lr

  @_tf.function
  def _train_gen_only(self, real_hr, proxy_lr):
    """
    Train generator
    :param real_hr: Real high resolution image
    :param proxy_lr:  Lower resolution proxy (i.e. downsampled HR image)
    :return: Loss
    """
    with _tf.GradientTape() as tape:
      # Generate data
      fake_lr = self._gen(real_hr, training=True)
      # Compute loss
      g_loss = self._mae(proxy_lr, fake_lr)
    # Get gradient + Update generator
    g_grad = tape.gradient(g_loss, self._gen.trainable_variables)
    self._gen_optimizer.apply_gradients(zip(g_grad,
                                            self._gen.trainable_variables))
    # Add metrics
    self._g_loss.update_state(g_loss)
    return g_loss, fake_lr

  def _discriminator_loss(self, real_pred, fake_pred):
    """
    Compute loss function for discriminator
    :param real_pred:  Prediction from real image
    :param fake_pred:  Prediction from generated image
    :return:  Loss value
    """
    # real_loss = self._bce(_tf.ones_like(real_pred), real_pred)
    # fake_loss = self._bce(_tf.zeros_like(fake_pred), fake_pred)

    real_loss = self._hinge(_tf.ones_like(real_pred), real_pred)
    fake_loss = self._hinge(_tf.zeros_like(fake_pred), fake_pred)

    # Hinge loss
    # real_loss = _tf.reduce_mean(_tf.minimum(0., -1.0 + real_pred))
    # fake_loss = _tf.reduce_mean(_tf.minimum(0., -1.0 - fake_pred))
    total_loss = real_loss + fake_loss
    return total_loss

  def _generator_loss(self, fake_lr, proxy_lr, fake_pred, weight=1.0):
    """
    Generator loss
    :param fake_lr:   Generated low resolution image
    :param proxy_lr:  Low resolution proxy image from HR (i.e. "ground truth")
    :param fake_pred: Prediction results for `fake_lr`
    :param weight:    Weight penalty for rec_loss
    :return:  Total_loss, Gan_loss, Rec_loss
    """
    gan_loss = self._bce(_tf.ones_like(fake_pred), fake_pred)
    # gan_loss = self._hinge(_tf.ones_like(fake_pred), fake_pred)
    # gan_loss = -_tf.reduce_mean(fake_pred)
    # Reconstruction loss
    rec_loss = weight * self._mae(proxy_lr, fake_lr)
    # Combined
    loss = gan_loss + rec_loss
    return loss, gan_loss, rec_loss