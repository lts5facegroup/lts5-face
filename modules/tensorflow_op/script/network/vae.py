# coding=utf-8
""" Variational Auto-Encoder based on residual block """
import tensorflow as _tf
from tensorflow.keras.layers import Layer, Input, BatchNormalization
from tensorflow.keras.layers import Dense, ReLU, Add, Reshape, Conv2D
from tensorflow.keras.layers import Concatenate, Flatten, Lambda
from tensorflow.keras.layers import Activation, MaxPool2D
from tensorflow.keras.losses import BinaryCrossentropy
from lts5.tensorflow_op.network.network import Network, InputConfigAbstract
from lts5.tensorflow_op.network.network import InputSpec as ISpec
from lts5.tensorflow_op.network.network import ImageFormat as ImgFrmt
from lts5.tensorflow_op.layers.summary import SummaryLayer
from lts5.tensorflow_op.network.utils import get_optimizer
from lts5.tensorflow_op.layers import PixelShuffling
from lts5.tensorflow_op.normalizers import SpectralNormalization
from lts5.tensorflow_op.metrics.base import ImagePair

__author__ = 'Christophe Ecabert'


class ResBlockConfig(object):
  """ Single residual block configuration """

  def __init__(self, with_bn, n_filters, sampling=None, with_sn=False):
    """
    Constructor
    :param with_bn:   If `True` add batch normalization before activation
                      function
    :param n_filters: List of 2 ints defining the number of filters in each
                      'Conv3x3' in the top branch.
    :param sampling:  Sampling operation, possible value: ['up', 'down', None]
    :param with_sn:   If `True` add spectral normalization for all convolutions
                      in the layers. Default to `False`
    """
    # Sanity check
    if not isinstance(n_filters, list) or len(n_filters) != 2:
      raise ValueError('`n_filters` must be a list of two `int`!')
    if sampling not in ('up', 'down', None):
      raise ValueError('`sampling` must be from (\'up\', \'down\', None)')
    # Set properties
    self._with_bn = with_bn
    self._with_sn = with_sn
    self._n_filters = n_filters
    self._sampling = sampling

  @classmethod
  def default_encoder(cls):
    """ Create default encoder configuration """
    return [ResBlockConfig(with_bn=True,
                           n_filters=[64, 64],
                           sampling='down'),
            ResBlockConfig(with_bn=True,
                           n_filters=[64, 64],
                           sampling='down'),
            ResBlockConfig(with_bn=True,
                           n_filters=[64, 64],
                           sampling='down'),
            ResBlockConfig(with_bn=True,
                           n_filters=[64, 64],
                           sampling='down'),
            ResBlockConfig(with_bn=True,
                           n_filters=[64, 64],
                           sampling='down')]

  @classmethod
  def default_encoder_div16(cls):
    """ Create default encoder configuration """
    return [ResBlockConfig(with_bn=False,
                           n_filters=[64, 64],
                           sampling='down'),
            ResBlockConfig(with_bn=False,
                           n_filters=[64, 64],
                           sampling='down'),
            ResBlockConfig(with_bn=False,
                           n_filters=[64, 64],
                           sampling='down'),
            ResBlockConfig(with_bn=False,
                           n_filters=[64, 64],
                           sampling='down')]

  @classmethod
  def default_decoder(cls):
    """ Create default decoder configuration """
    return [ResBlockConfig(with_bn=True,
                           n_filters=[64, 256],
                           sampling='up'),
            ResBlockConfig(with_bn=True,
                           n_filters=[64, 256],
                           sampling='up'),
            ResBlockConfig(with_bn=True,
                           n_filters=[64, 256],
                           sampling='up'),
            ResBlockConfig(with_bn=True,
                           n_filters=[64, 256],
                           sampling='up'),
            ResBlockConfig(with_bn=True,
                           n_filters=[64, 256],
                           sampling='up')]

  @classmethod
  def default_decoder_up16(cls):
    """ Create default decoder configuration """
    return [ResBlockConfig(with_bn=False,
                           n_filters=[64, 256],
                           sampling='up'),
            ResBlockConfig(with_bn=False,
                           n_filters=[64, 256],
                           sampling='up'),
            ResBlockConfig(with_bn=False,
                           n_filters=[64, 256],
                           sampling='up'),
            ResBlockConfig(with_bn=False,
                           n_filters=[64, 256],
                           sampling='up')]

  @property
  def with_bn(self):
    return self._with_bn

  @property
  def with_sn(self):
    return self._with_sn

  @property
  def n_filters(self):
    return self._n_filters

  @property
  def sampling(self):
      return self._sampling


class ResBlock(Layer):
  """ Residual block block with pre-activation.

    - Optional Batch Normalization
    - Optional Up-sampling layer (i.e. PixelShuffling)

  Structure:
  --.--> (BN) --> ReLU --> Conv3x3 --> (BN) --> ReLU --> Conv3x3 --+--(Up)-->
    |                                                              |
    + ------------------------- (Conv1x1) -------------------------+

  """

  def __init__(self, cfg, name, **kwargs):
    """
    Constructor
    :param cfg:       Block's configuration
    :param kwargs:    Extra keyword arguments
    """
    super(ResBlock, self).__init__(name=name, **kwargs)
    # Init layers
    self.cfg = cfg
    self.stride = 1 if self.cfg.sampling != 'down' else 2
    self.upsampling = (Lambda(_tf.identity,
                              name='skip_up') if self.cfg.sampling != 'up' else
                       PixelShuffling(block_size=2, name='up'))
    # Forward
    self.bn1 = (BatchNormalization(name='bn1') if self.cfg.with_bn else
                Lambda(_tf.identity, name='skip_bn1'))
    self.relu1 = (ReLU(name='relu1') if self.cfg.with_bn else
                  Lambda(_tf.identity, name='skip_relu1'))
    self.conv1 = Conv2D(filters=self.cfg.n_filters[0],
                        kernel_size=3,
                        strides=(self.stride, self.stride),
                        padding='SAME',
                        use_bias=False,
                        name='conv1')
    self.bn2 = (BatchNormalization(name='bn2') if self.cfg.with_bn else
                Lambda(_tf.identity, name='skip_bn2'))
    self.relu2 = ReLU(name='relu2')
    self.conv2 = Conv2D(filters=self.cfg.n_filters[1],
                        kernel_size=3,
                        strides=(1, 1),
                        padding='SAME',
                        use_bias=False,
                        name='conv2')
    # Normalization ?
    if self.cfg.with_sn:
      self.conv1 = SpectralNormalization(self.conv1)
      self.conv2 = SpectralNormalization(self.conv2)
    # Skip connection adaptation, needed if n_in != n_filters[1]
    self.skip = Lambda(_tf.identity, name='skip')
    # Fuse
    self.add = Add(name='add')

  def build(self, input_shape):
    """
    Build layers
    :param input_shape: Input dimensions
    """
    n_in = input_shape[-1]
    if n_in != self.cfg.n_filters[1]:
      # Add convolution for adaptation
      self.skip = Conv2D(filters=self.cfg.n_filters[1],
                         kernel_size=1,
                         strides=(self.stride, self.stride),
                         padding='SAME',
                         use_bias=False,
                         name='skip')
      if self.cfg.with_sn:
        self.skip = SpectralNormalization(self.skip)
    elif n_in == self.cfg.n_filters[1] and self.stride > 1:
      # Adapt spatial dimension with pooling
      self.skip = MaxPool2D(self.stride, name='skip')

    # if (n_in != self.cfg.n_filters[1] or
    #    (n_in == self.cfg.n_filters[1] and self.stride > 1)):
    #   self.skip = Conv2D(filters=self.cfg.n_filters[1],
    #                      kernel_size=1,
    #                      strides=(self.stride, self.stride),
    #                      padding='SAME',
    #                      use_bias=False,
    #                      name='skip')
    #   if self.cfg.with_sn:
    #     self.skip = SpectralNormalization(self.skip)
    super(ResBlock, self).build(input_shape)

  def call(self, inputs, **kwargs):
    """
    Forward pass
    :param inputs:    Input tensor
    :keyword training:  If `True` indicate training phase.
    :return:  Transformed input
    """
    # Skip connection
    res = self.skip(inputs)
    # Forward - Optional Batch norm
    x = self.bn1(inputs, **kwargs)
    x = self.relu1(x)
    x = self.conv1(x)
    x = self.bn2(x, **kwargs)
    x = self.relu2(x)
    x = self.conv2(x)
    # Add together residual + skip
    y = self.add([x, res])
    # Up-sampling needed ?
    y = self.upsampling(y)
    return y

  def get_config(self):
    """
    Provide layer's configuration
    :return:  dict
    """
    cfg = super(ResBlock, self).get_config()
    cfg['cfg'] = self.cfg
    return cfg


def _sample_z(args):
  """
  Sample the distribution given a `mean` and `log_sigma` using the
  re-parametrisation trick
  :param args:  tuple: mean, log_sigma
  :return:  Sampled value
  """
  mu, log_sigma = args
  shp = _tf.shape(mu)
  eps = _tf.random.normal(shape=shp, mean=0.0, stddev=1.0)
  return mu + _tf.exp(log_sigma * 0.5) * eps


class Encoder(Layer):
  """ Encoder, goes from image to latent space """

  def __init__(self, config, latent_dim, name, **kwargs):
    """
    Constructor
    :param config:  List of residual block configuration or `None` to use the
                    default one.
    :param latent_dim: Dimension of the latent space
    :param name:    Encoder name scope
    :param kwargs:  Extra keyword arguments
    """
    super(Encoder, self).__init__(name=name, **kwargs)
    # Config
    self._cfg = ResBlockConfig.default_encoder() if config is None else config
    # Create residual block
    self._block = []
    for k, conf in enumerate(self._cfg):
      b = ResBlock(cfg=conf, name='Block{}'.format(k))
      self._block.append(b)

    # Create mean + log(var) predictor
    self._flat = Flatten(name='flatten')
    self._mean = Dense(latent_dim, name='mean')
    self._log_sigma = Dense(latent_dim, name='log_sigma')
    self._sample = Lambda(_sample_z, name='sample')

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:    Tensor, image to encode
    :param training:  If `True` indicates training phase
    :return:  tuple: mean, log_sigma, z
    """

    # Residual block
    x = inputs
    for bck in self._block:
      x = bck(x, training=training)
    # Predict mean, log_sigma, z
    flat_x = self._flat(x)
    mean = self._mean(flat_x)
    log_sigma = self._log_sigma(flat_x)
    z = self._sample([mean, log_sigma])
    # Done
    return mean, log_sigma, z


class EncoderV2(Layer):
  """ Encoder, goes from image to latent space """

  def __init__(self, config, latent_dim, name, **kwargs):
    """
    Constructor
    :param config:  List of residual block configuration or `None` to use the
                    default one.
    :param latent_dim: Dimension of the latent space
    :param name:    Encoder name scope
    :param kwargs:  Extra keyword arguments
    """
    super(EncoderV2, self).__init__(name=name, **kwargs)
    # Config
    self._cfg = config or ResBlockConfig.default_encoder_div16()
    # Create residual block
    self._block = []
    for k, conf in enumerate(self._cfg):
      b = ResBlock(cfg=conf, name='Block{}'.format(k))
      self._block.append(b)

    # Create mean + log(var) predictor
    self._mean = Conv2D(latent_dim,
                        kernel_size=1,
                        strides=1,
                        name='mean')
    self._log_sigma = Conv2D(latent_dim,
                             kernel_size=1,
                             strides=1,
                             name='log_sigma')
    self._sample = Lambda(_sample_z, name='sample')

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:    Tensor, image to encode
    :param training:  If `True` indicates training phase
    :return:  tuple: mean, log_sigma, z
    """

    # Residual block
    x = inputs
    for bck in self._block:
      x = bck(x, training=training)
    # Predict mean, log_sigma, z
    mean = self._mean(x)
    log_sigma = self._log_sigma(x)
    z = self._sample([mean, log_sigma])
    # Done
    return mean, log_sigma, z


class Decoder(Layer):
  """ Encoder, goes from latent space to image """

  def __init__(self,
               config,
               feature_size,
               name, **kwargs):
    """
    Constructor
    :param config:  List of residual block configuration or `None` to use the
                    default one.
    :param feature_size: Feature space dimensions, tuple
    :param name:    Decoder name scope
    :param kwargs:  Extra keyword arguments
    """
    super(Decoder, self).__init__(name=name, **kwargs)
    # Config
    self._cfg = ResBlockConfig.default_decoder() if config is None else config
    # Convert latent space
    units = 1
    for x in feature_size:
      units *= x
    self._proj = Dense(units, name='projection')
    self._reshaper = Reshape(target_shape=feature_size)
    # Create residual block
    self._block = []
    for k, conf in enumerate(self._cfg):
      b = ResBlock(cfg=conf, name='Block{}'.format(k))
      self._block.append(b)

    self._image = Conv2D(filters=3,
                         kernel_size=(3, 3),
                         strides=(1, 1),
                         padding='same')

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:  Tensors, latent code
    :param training:  If `True` indicates training phase
    :return:  Generated image
    """
    x = self._proj(inputs)
    x = self._reshaper(x)
    for bck in self._block:
      x = bck(x, training=training)
    # Finalize image
    return self._image(x)


class DecoderV2(Layer):
  """ Encoder, goes from latent space to image """

  def __init__(self,
               config,
               activation='sigmoid',
               name='decoder',
               **kwargs):
    """
    Constructor
    :param config:  List of residual block configuration or `None` to use the
                    default one.
    :param activation: Decoder final activation type
    :param name:    Decoder name scope
    :param kwargs:  Extra keyword arguments
    """
    super(DecoderV2, self).__init__(name=name, **kwargs)
    # Config
    self._cfg = config or ResBlockConfig.default_decoder_up16()
    self._act = activation
    # Create residual block
    self._block = []
    for k, conf in enumerate(self._cfg):
      b = ResBlock(cfg=conf, name='Block{}'.format(k))
      self._block.append(b)
    # Create image
    self._image = Conv2D(filters=3,
                         kernel_size=(3, 3),
                         strides=(1, 1),
                         activation=self._act,
                         padding='same')

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:  Tensors, latent code
    :param training:  If `True` indicates training phase
    :return:  Generated image
    """
    x = inputs
    for bck in self._block:
      x = bck(x, training=training)
    # Finalize image
    return self._image(x)


class KLDivergence(SummaryLayer):
  """ KL Divergence cost """

  def __init__(self,
               weight=1.0,
               manager=None,
               name='KLDivergence',
               **kwargs):
    """
    Constructor
    :param weight:  KL divergence weight (i.e. hyperparameter)
    :param name:    Op's name
    :param kwargs:  Extra keyword argument
    """

    def _log_fn(inputs, step):
      """ Log KL divergence loss """
      self._w_train.scalar(name=name + '/cost',
                           data=inputs,
                           step=step)

    super(KLDivergence, self).__init__(name=name,
                                       manager=manager,
                                       logging_fn=_log_fn,
                                       traces=('train',),
                                       **kwargs)
    self._kl_weight = weight

  def call(self, inputs):
    """
    Forward, explicit KL divergence computation assuming gaussian prior with
    0 mean and unit variance
    :param inputs:  List of tensors: mean, log_sigma
    :return:  scalar, KL divergence cost
    """
    mean, log_sigma = inputs
    kl = 1.0 + log_sigma - _tf.square(mean) - _tf.exp(log_sigma)
    bs = _tf.shape(mean)[0]
    kl = _tf.reshape(kl, [bs, -1])
    kl = -0.5 * _tf.reduce_sum(kl, axis=-1)   # [B,]
    kl = _tf.reduce_mean(kl)                  # Scalar
    # Log and return
    return super(KLDivergenceV2, self).call(kl)


class KLDivergenceV2(SummaryLayer):
  """ KL Divergence cost """

  def __init__(self,
               weight=1.0,
               manager=None,
               name='KLDivergence',
               **kwargs):
    """
    Constructor
    :param weight:  KL divergence weight (i.e. hyperparameter)
    :param name:    Op's name
    :param kwargs:  Extra keyword argument
    """

    def _log_fn(inputs, step):
      """ Log KL divergence loss """
      self._w_train.scalar(name=name + '/cost',
                           data=inputs,
                           step=step)

    super(KLDivergenceV2, self).__init__(name=name,
                                       manager=manager,
                                       logging_fn=_log_fn,
                                       traces=('train',),
                                       **kwargs)
    self._kl_weight = weight

  def call(self, inputs):
    """
    Forward, explicit KL divergence computation assuming gaussian prior with
    0 mean and unit variance
    :param inputs:  List of tensors: mean, log_sigma
    :return:  scalar, KL divergence cost
    """
    mean, log_sigma = inputs
    kl = 1.0 + log_sigma - _tf.square(mean) - _tf.exp(log_sigma)  # [B, H, W, C]
    kl = -0.5 * _tf.reduce_sum(kl, axis=-1)   # [B, H, W]
    kl = _tf.reduce_sum(kl, axis=[1, 2])      # [B,]
    kl = _tf.reduce_mean(kl)                  # Scalar
    # Log and return
    return super(KLDivergenceV2, self).call(kl)


class VariationalAutoEncoderConfig(InputConfigAbstract):
  """ Input's configuration for ResidualVAE model """

  def __init__(self):
    """ Constructor """
    super(VariationalAutoEncoderConfig, self).__init__([ISpec(dims=(None,
                                                                    None,
                                                                    3),
                                                        fmt=ImgFrmt.RGB,
                                                        name='input')])

    def _pre_process_fn(x):
      return (x / 127.5) - 1.0

    def _revert_fn(x):
      return 127.5 * (x + 1.0)

    self._preprocess_fn = Lambda(_pre_process_fn, name='preprocessing')
    self._revert__preprocess_fn = Lambda(_revert_fn,
                                         name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert__preprocess_fn


class VariationalAutoEncoder(Network):
  """ Residual Variational Auto-Encoder """

  def __init__(self,
               encoder_cfg,
               decoder_cfg,
               latent_dim,
               manager=None,
               name='VariationalAutoEncoder',
               **kwargs):
    """

    :param encoder_cfg: Encoder configuration
    :param decoder_cfg: Decoder configuration
    :param manager: Instance of `SummaryWriterManager` to enable logging into
                    TensorBoard
    :param name:  Model's name
    :param kwargs:  Extra keyword arguments
    """
    super(VariationalAutoEncoder,
          self).__init__(config=VariationalAutoEncoderConfig(),
                         name=name,
                         **kwargs)
    self.manager = manager
    # Build components
    self.encoder = Encoder(config=encoder_cfg,
                           latent_dim=latent_dim,
                           name='Encoder')
    self.decoder = Decoder(config=decoder_cfg,
                           feature_size=(8, 8, 64),
                           name='Decoder')
    self.kl_divergence = KLDivergence(manager=self.manager)
    self.activation = Activation('sigmoid')
    # Extra optimizer
    self._optimizer_instance = None
    self._losses_instance = None
    self._metrics_instance = None

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:    Tensor, image to encode/decode
    :param training:  If `True` indicate training phase
    :return:  Decoded image
    """
    # Encode
    image = inputs
    mean, log_sigma, z = self.encoder(image, training=training)
    # Add KL divergence
    kl = self.kl_divergence([mean, log_sigma])
    self.add_loss(kl)
    # Decode
    rec = self.decoder(z, training=training)
    return self.activation(rec)

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :param kwargs: Extra keyword arguments
    :keyword learning_rate: Learning rate
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    # Create optimizer + parameters
    if self._optimizer_instance is None:
      lr = kwargs.get('learning_rate', 1e-4)
      optimizer_cls = get_optimizer('adam')
      self._optimizer_instance = optimizer_cls(learning_rate=lr)
    return self._optimizer_instance

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    # Photometric + Landmarks
    if self._losses_instance is None:

      def _reconstruction_error(y_true, y_pred):
        """ Compute reconstruction error based on binary cross-entropy """
        # Binary cross-entropy => [B, H, W, C]
        recon = _tf.keras.backend.binary_crossentropy(y_pred, y_true)
        return _tf.reduce_sum(recon, axis=(1, 2, 3))

      # Add reconstruction loss
      self._losses_instance = [_reconstruction_error]
    return self._losses_instance

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :param kwargs:  Extra keyword arguments
    :keyword input_shape: Dimensions of the input tensor
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    if self._metrics_instance is None:
      shp = kwargs.get('input_shape', None)
      if shp is None:
        raise ValueError('Input shape, is required for proper reconstruction'
                         ' monitoring')
      self._metrics_instance = [ImagePair(shape=shp,
                                          dtype=_tf.float32,
                                          name='reconstruction_display')]
    return self._metrics_instance


class VariationalAutoEncoderV2(Network):
  """ Residual Variational Auto-Encoder """

  def __init__(self,
               encoder_cfg,
               decoder_cfg,
               latent_dim,
               activation='sigmoid',
               manager=None,
               name='VariationalAutoEncoder',
               **kwargs):
    """

    :param encoder_cfg: Encoder configuration
    :param decoder_cfg: Decoder configuration
    :param manager: Instance of `SummaryWriterManager` to enable logging into
                    TensorBoard
    :param name:  Model's name
    :param kwargs:  Extra keyword arguments
    """
    super(VariationalAutoEncoderV2,
          self).__init__(config=VariationalAutoEncoderConfig(),
                         name=name,
                         **kwargs)
    self.manager = manager
    # Build components
    self.encoder = EncoderV2(config=encoder_cfg,
                             latent_dim=latent_dim,
                             name='Encoder')
    self.activation = activation
    self.decoder = DecoderV2(config=decoder_cfg,
                             activation=activation,
                             name='Decoder')
    self.kl_divergence = KLDivergenceV2(manager=self.manager)
    # Extra optimizer
    self._optimizer_instance = None
    self._losses_instance = None
    self._metrics_instance = None

  def call(self, inputs, training=None):
    """
    Forward
    :param inputs:    Tensor, image to encode/decode
    :param training:  If `True` indicate training phase
    :return:  Decoded image
    """
    # Encode
    image = inputs
    mean, log_sigma, z = self.encoder(image, training=training)
    # Add KL divergence
    kl = self.kl_divergence([mean, log_sigma])
    self.add_loss(kl)
    # Decode
    rec = self.decoder(z, training=training)
    return rec

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :param kwargs: Extra keyword arguments
    :keyword learning_rate: Learning rate
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    # Create optimizer + parameters
    if self._optimizer_instance is None:
      lr = kwargs.get('learning_rate', 1e-4)
      optimizer_cls = get_optimizer('adam')
      self._optimizer_instance = optimizer_cls(learning_rate=lr)
    return self._optimizer_instance

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    # Photometric + Landmarks
    if self._losses_instance is None:
      # Add reconstruction loss
      from_logits = self.activation == 'linear'
      bce = BinaryCrossentropy(from_logits=from_logits)
      self._losses_instance = [bce]
    return self._losses_instance

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :param kwargs:  Extra keyword arguments
    :keyword input_shape: Dimensions of the input tensor
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    if self._metrics_instance is None:
      shp = kwargs.get('input_shape', None)
      if shp is None:
        raise ValueError('Input shape, is required for proper reconstruction'
                         ' monitoring')
      pred_fn = _tf.math.sigmoid if self.activation == 'linear' else None
      self._metrics_instance = [ImagePair(shape=shp,
                                          dtype=_tf.float32,
                                          y_pred_fn=pred_fn,
                                          name='reconstruction_display')]
    return self._metrics_instance
