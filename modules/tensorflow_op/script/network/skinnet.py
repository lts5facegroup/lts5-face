#coding=utf-8
"""
Skin Segmentation network based on SegNet

Link:
- http://mi.eng.cam.ac.uk/projects/segnet/
"""
import tensorflow as _tf
from lts5.tensorflow_op.layers.pooling import MaxPoolingWithArgmax2D
from lts5.tensorflow_op.layers.pooling import MaxUnpooling2D
from lts5.tensorflow_op.losses.classification import BinaryFocalLoss
from lts5.tensorflow_op.network.utils import get_optimizer
from lts5.tensorflow_op.network.network import InputConfigAbstract, InputSpec
from lts5.tensorflow_op.network.network import Network, ImageFormat
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Layer, Conv2D, BatchNormalization, Lambda
from tensorflow.keras.metrics import MeanIoU, BinaryAccuracy
from lts5.tensorflow_op.metrics.classification import ConfusionMatrix

__author__ = 'Christophe Ecabert'


class SkinNetConfig(InputConfigAbstract):
  """ Input configuration for SkinNet arch """

  def __init__(self):
    """ Constructor """
    super(SkinNetConfig, self).__init__([InputSpec(dims=(None, None, 3),
                                                   fmt=ImageFormat.RGB,
                                                   name='input')])
    def _pre_process_fn(x):
      return (x * 0.007843137) - 1.0

    def _revert_fn(x):
      return (x + 1.0) * 127.5

    self._preprocess_fn = Lambda(_pre_process_fn, name='preprocessing')
    self._revert_preprocess_fn = Lambda(_revert_fn, name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


class EncoderBlock(Layer):
  """
  Encoder block: Conv + BN + Relu + Conv + BN + Relu + Pooling
  """

  def __init__(self,
               n_filter,
               kernel_size,
               strides,
               name, **kwargs):
    """
    Constructor
    :param n_filter:      Number of filters
    :param kernel_size:   Tuple of integers, kernel size in x/y direction
    :param strides:       Tuple of integers, strides
    :param name:          Block's name
    :param kwargs:        Extra keyword arguments
    """
    super(EncoderBlock, self).__init__(name=name, **kwargs)
    self.n_filter = n_filter
    self.ksize = kernel_size
    self.strides = strides

    # Build layers
    self.conv1 = Conv2D(filters=self.n_filter,
                        kernel_size=self.ksize,
                        strides=self.strides,
                        padding='same',
                        name='conv1')
    self.bn1 = BatchNormalization(name='bn1')
    self.relu1 = Activation('relu',
                            name='relu1')
    self.conv2 = Conv2D(filters=self.n_filter,
                        kernel_size=self.ksize,
                        strides=self.strides,
                        padding='same',
                        name='conv2')
    self.bn2 = BatchNormalization(name='bn2')
    self.relu2 = Activation('relu',
                            name='relu2')
    # Max-pooling
    self.pool1 = MaxPoolingWithArgmax2D(pool_size=(2, 2),
                                        strides=(2, 2),
                                        padding='same',
                                        name='pool1')

  def call(self, inputs, training=None):
    """
    Forward pass
    :param inputs:    Tensor to be processed
    :param training:  If `True` indicates training phase, otherwise no.
    :return:  tuple: Processed tensor, pooling index
    """
    x = self.conv1(inputs)
    x = self.bn1(x, training)
    x = self.relu1(x)
    x = self.conv2(x)
    x = self.bn2(x, training)
    x = self.relu2(x)
    return self.pool1(x)


class DecoderBlock(Layer):
  """
  Decoder block: Unpool + Conv + BN + Relu + Conv + BN + Relu
  """

  def __init__(self,
               n_filter,
               kernel_size,
               strides,
               name,
               **kwargs):
    """
    Constructor
    :param n_filter:      Tuple of integers, number of filters in the block
    :param kernel_size:   Tuple of integers, kernel size in x/y direction
    :param strides:       Tuple of integers, strides
    :param name:          Block's name
    :param kwargs:        Extra keyword arguments
    """
    super(DecoderBlock, self).__init__(name=name, **kwargs)
    self.n_filter = n_filter
    self.ksize = kernel_size
    self.strides = strides
    # Build layers
    self.unpool1 = MaxUnpooling2D(name='unpool1')
    self.conv1 = Conv2D(filters=self.n_filter[0],
                        kernel_size=self.ksize,
                        strides=self.strides,
                        padding='same',
                        name='conv1')
    self.bn1 = BatchNormalization(name='bn1')
    self.relu1 = Activation('relu',
                            name='relu1')
    self.conv2 = Conv2D(filters=self.n_filter[1],
                        kernel_size=self.ksize,
                        strides=self.strides,
                        padding='same',
                        name='conv2')
    self.bn2 = BatchNormalization(name='bn2')
    self.relu2 = Activation('relu',
                            name='relu2')

  def call(self, inputs, output_shape, training=None):
    """
    Forward pass
    :param inputs:    List of tensor: Value, Shared pooling index
    :param output_shape:  Dimensions of the output. Required for unpooling
    :param training:  If `True` indicates training phase, otherwise no.
    :return:  tuple: Processed tensor
    """

    # Unpooling: x, ind == inputs
    x = self.unpool1(inputs=inputs, output_shape=output_shape)
    x = self.conv1(x)
    x = self.bn1(x, training=training)
    x = self.relu1(x)
    x = self.conv2(x)
    x = self.bn2(x, training=training)
    return self.relu2(x)


class SkinNet(Network):
  """ SkinNet implementation """

  def __init__(self,
               n_block=4,
               n_filter=32,
               n_classes=2,
               name='SkinNet',
               **kwargs):
    """
    Constructor
    :param n_block:   Number of encoder/decoder block
    :param n_filter:  Number of filter in first block
    :param n_classes: Number of classes to predict
    :param name:      Network's name (i.e. scope)
    :param kwargs:    Extra keyword arguments
    """
    super(SkinNet, self).__init__(config=SkinNetConfig(),
                                  name=name,
                                  **kwargs)
    self.n_block = n_block
    self.n_filter = n_filter
    self.n_classes = n_classes
    # Build encoder blocks
    self.encoder_blocks = []
    for k in range(self.n_block):
      nf = self.n_filter << k
      bck = EncoderBlock(n_filter=nf,
                         kernel_size=(3, 3),
                         strides=(1, 1),
                         name='block{}'.format(k))
      self.encoder_blocks.append(bck)
    # Build decoder blocks
    self.decoder_blocks = []
    for k in range(self.n_block - 1, -1, -1):
      n_in = self.n_filter << k
      n_out = self.n_filter << (k - 1) if k > 0 else n_in
      bck = DecoderBlock(n_filter=[n_in, n_out],
                         kernel_size=(3, 3),
                         strides=(1, 1),
                         name='block{}D'.format(k))
      self.decoder_blocks.append(bck)

    # Prediction
    activation = 'sigmoid' if self.n_classes == 2 else 'softmax'
    nf = 1 if self.n_classes == 2 else self.n_classes
    self.pred = Conv2D(filters=nf,
                       kernel_size=1,
                       strides=1,
                       padding='same',
                       activation=activation,
                       name='pred')
    # Optimizer, losses, metrics
    self._optimizer_instance = None
    self._losses_instance = None
    self._metrics_instance = None

  def call(self, inputs, training=None):
    """
    Forward, run segmentation
    :param inputs:    Image to segment
    :param training:  If `True` indicate training phase
    :return:  Segmentation mask
    """

    # Forward - Encoder
    indexes = []
    x = [inputs]
    for k, e_bck in enumerate(self.encoder_blocks):
      out, ind = e_bck(x[k], training=training)
      x.append(out)
      indexes.append(ind)
    # Decoder
    up = x[-1]
    for k, d_bck in enumerate(self.decoder_blocks):
      up = d_bck(inputs=[up,
                         indexes[self.n_block - k - 1]],
                 output_shape=_tf.shape(x[self.n_block - k - 1]),
                 training=training)
    # Pred
    return self.pred(up)

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :param kwargs: Extra keyword arguments
    :keyword type:  Type of optimizer
    :keyword learning_rate: Learning rate
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    if self._optimizer_instance is None:
      opt_type = kwargs.pop('type', 'Adam')
      optimizer_cls = get_optimizer(opt_type)
      self._optimizer_instance = optimizer_cls(**kwargs)
    return self._optimizer_instance

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    if self._losses_instance is None:
      self._losses_instance = [BinaryFocalLoss(gamma=2.0,
                                               alpha=0.25,
                                               from_logits=False)]
    return self._losses_instance

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    if self._metrics_instance is None:
      self._metrics_instance = [MeanIoU(num_classes=self.n_classes,
                                        name='MeanIoU'),
                                BinaryAccuracy(),
                                ConfusionMatrix(num_classes=self.n_classes)]
    return self._metrics_instance
