# coding=utf-8
""" Utility functions """

__author__ = 'Christophe Ecabert'


def _get_keras_optimizer_classes():
  """
  Provide a list of tf.keras.optimizers.Optimizer classes available
  :return:  Dictionary of Optimizer
  """
  from importlib import import_module
  # Import modules
  # See: https://codereview.stackexchange.com/a/70282
  m = import_module('tensorflow.python.keras.optimizer_v2.optimizer_v2')
  opt = {}
  for cls in m.OptimizerV2.__subclasses__():
    if cls.__name__[0] != '_' and 'Replicas' not in cls.__name__:
      # Find suitable optimizer
      name = cls.__name__.lower()
      opt[name] = cls
  return opt


_keras_optimizers = _get_keras_optimizer_classes()


def get_optimizer(name):
  """
  Select an `tf.keras.Optimizer` from a given name
  :param name:  Name of the optimizer
  :return:  Optimizer class
  """
  oname = name.lower()
  opt = _keras_optimizers.get(oname, None)
  if opt is None:
    raise ValueError('Unknown optimizer name: {}'.format(name))
  return opt
