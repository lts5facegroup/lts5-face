# coding=utf-8
"""
Implementation of the MTCNN face detector proposed by Zhang et al in "Joint
face detection and alignment using multitask cascaded convolutional networks"

This work is derived from the implementation of Iván de Paz Centeno proposed in
https://github.com/ipazc/mtcnn

"""
from os.path import splitext, basename
import numpy as _np
import tensorflow as _tf
import cv2 as _cv
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Conv2D, PReLU, MaxPool2D, Dense
from tensorflow.keras.layers import Lambda, Flatten
from lts5.tensorflow_op.network.network import Network, InputSpec
from lts5.tensorflow_op.network.network import InputConfigAbstract
from lts5.tensorflow_op.network.network import ImageFormat
from lts5.tensorflow_op.data.preprocessing import rgb_to_bgr
from lts5.tensorflow_op.network.utils import get_optimizer

__author__ = 'Christophe Ecabert'


def _preprocessing(image, is_bgr=False):
  """
  Convert to ro BGR + normalize image between [-1, 1]
  :param image:   Image to pre-process
  :param is_bgr:  If `True` indicates image is in BGR format
  :return:  Pre-processed image
  """
  # Convert to BGR (Caffe model) + normalize
  if not is_bgr:
    image = rgb_to_bgr(image=image)
  return (_tf.cast(image, _tf.float32) - 127.5) * 0.0078125


def _convert_shape_if_needed(layer, weight):
  """
  Based on layer type, reshape the given weights
  :param layer:   Object of type keras.layers.Layer
  :param weight:  Numpy array
  :return:  weights
  """
  if isinstance(layer, PReLU):
    return weight.reshape(layer.weights[0].shape.as_list())
  # Leave it untouched
  return weight


class PNet(Model):
  """ Proposal network `PNet` implementation """

  def __init__(self, name='PNet', is_bgr=False, **kwargs):
    """
    Constructor
    :param name:    Network's name
    :param is_bgr:  If `True` indicates in is already in BGR format
    :param kwargs:  Extra keyword arguments
    """
    super(PNet, self).__init__(name=name, **kwargs)

    # Pre-processing
    self.norm1 = Lambda(lambda x: _preprocessing(image=x, is_bgr=is_bgr))
    # Conv + PReLU + max-pool
    self.conv1 = Conv2D(filters=10,
                        kernel_size=3,
                        strides=(1, 1),
                        padding='valid',
                        use_bias=True,
                        name='conv1')
    self.prelu1 = PReLU(name='PReLU1', shared_axes=[1, 2])
    self.pool1 = MaxPool2D(pool_size=(2, 2),
                           strides=(2, 2),
                           padding='same',
                           name='pool1')
    # Conv + PReLU
    self.conv2 = Conv2D(filters=16,
                        kernel_size=3,
                        strides=(1, 1),
                        padding='valid',
                        use_bias=True,
                        name='conv2')
    self.prelu2 = PReLU(name='PReLU2', shared_axes=[1, 2])
    # Conv + PReLU
    self.conv3 = Conv2D(filters=32,
                        kernel_size=3,
                        strides=(1, 1),
                        padding='valid',
                        use_bias=True,
                        name='conv3')
    self.prelu3 = PReLU(name='PReLU3', shared_axes=[1, 2])
    # Box score
    self.conv4_1 = Conv2D(filters=2,
                          kernel_size=1,
                          strides=(1, 1),
                          padding='same',
                          use_bias=True,
                          activation='softmax',
                          name='conv4-1')
    # Box regression
    self.conv4_2 = Conv2D(filters=4,
                          kernel_size=1,
                          strides=(1, 1),
                          padding='same',
                          use_bias=True,
                          name='conv4-2')

  def call(self, inputs, **kwargs):
    """
    Forward
    :param inputs:  Image to process in BGR format
    :param kwargs:  Extra keyword arguments
    :return:  tuple: Box score, Box regression
    """
    # Normalization
    x0 = self.norm1(inputs)
    # Conv + PReLU + max-pool
    x1 = self.conv1(x0)
    x1 = self.prelu1(x1)
    x1 = self.pool1(x1)
    # # Conv + PReLU
    x2 = self.conv2(x1)
    x2 = self.prelu2(x2)
    # Conv + PReLU
    x3 = self.conv3(x2)
    x3 = self.prelu3(x3)
    # Box score
    bbox_score = self.conv4_1(x3)
    # Box regression
    bbox_reg = self.conv4_2(x3)
    return bbox_score, bbox_reg

  def load_weights(self, filepath, by_name=False, **kwargs):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    :param kwargs:    Extra keyword arguments
    :keyword skip_layer: List of layer's name to be trained from scratch
                         (i.e. skip restoration)
    """

    # Reload from numpy dict
    _, ext = splitext(basename(filepath))
    if ext == '.npy':
      # Restore pre-trained model from numpy file
      tf_converter = {'kernel': 'weights',
                      'bias': 'biases'}
      # Define layers that are trainable
      skip_layer = kwargs.get('skip_layer', [])
      # Load the weights into memory
      w_dict = _np.load(filepath, encoding="bytes", allow_pickle=True).item()
      w_dict = w_dict.get(self.name, None) or w_dict
      # Iterate over every layers in the model and set weights if needed.
      for layer in self.layers:
        if layer.name not in skip_layer:
          # Need to initialize from pre-trained
          data = w_dict.get(layer.name, None)
          if data is not None:
            np_weight = []
            for w in layer.weights:
              start = w.name.rfind('/') + 1
              stop = w.name.find(':')
              wname = w.name[start:stop]
              wname = tf_converter.get(wname, wname).encode()
              weights = data[wname]
              weights = _convert_shape_if_needed(layer=layer, weight=weights)
              np_weight.append(weights)
            layer.set_weights(np_weight)
    else:
      # Try with base class function
      super(PNet, self).load_weights(filepath, by_name)


class RNet(Model):
  """ Refinement network `RNet` implementation """

  def __init__(self, name='RNet', is_bgr=False, **kwargs):
    """
    Constructor
    :param name:    Network's name
    :param is_bgr:  If `True` indicates in is already in BGR format
    :param kwargs:  Extra keyword arguments
    """
    super(RNet, self).__init__(name=name, **kwargs)
    # Pre-processing
    self.norm1 = Lambda(lambda x: _preprocessing(image=x, is_bgr=is_bgr))
    # Conv + PReLU + pooling
    self.conv1 = Conv2D(filters=28,
                        kernel_size=3,
                        strides=(1, 1),
                        padding='valid',
                        use_bias=True,
                        name='conv1')
    self.prelu1 = PReLU(name='prelu1', shared_axes=[1, 2])
    self.pool1 = MaxPool2D(pool_size=(3, 3),
                           strides=(2, 2),
                           padding='same',
                           name='pool1')
    # Conv + PReLU + pool
    self.conv2 = Conv2D(filters=48,
                        kernel_size=3,
                        strides=(1, 1),
                        padding='valid',
                        use_bias=True,
                        name='conv2')
    self.prelu2 = PReLU(name='prelu2', shared_axes=[1, 2])
    self.pool2 = MaxPool2D(pool_size=(3, 3),
                           strides=(2, 2),
                           padding='valid',
                           name='pool2')
    # Conv + PReLU
    self.conv3 = Conv2D(filters=64,
                        kernel_size=2,
                        strides=(1, 1),
                        padding='valid',
                        use_bias=True,
                        name='conv3')
    self.prelu3 = PReLU(name='prelu3', shared_axes=[1, 2])
    # FC + PReLU
    self.flat1 = Flatten()
    self.fc1 = Dense(units=128, name='fc1')
    self.prelu4 = PReLU(name='prelu4')
    # Class prob
    self.fc2_1 = Dense(units=2, activation='softmax', name='fc2-1')
    # bbox regression
    self.fc2_2 = Dense(units=4, name='fc2-2')

  def call(self, inputs):
    """
    Forward
    :param inputs:  Image
    :return:  tuple: box score, box regression
    """
    # Normalization
    x0 = self.norm1(inputs)
    # Conv + PReLU + pooling
    x1 = self.conv1(x0)
    x1 = self.prelu1(x1)
    x1 = self.pool1(x1)
    # Conv + PReLU + pool
    x2 = self.conv2(x1)
    x2 = self.prelu2(x2)
    x2 = self.pool2(x2)
    # Conv + PReLU
    x3 = self.conv3(x2)
    x3 = self.prelu3(x3)
    # FC + PReLU
    x4 = self.flat1(x3)
    x4 = self.fc1(x4)
    x4 = self.prelu4(x4)
    # Class prob
    score = self.fc2_1(x4)
    # bbox regression
    reg = self.fc2_2(x4)
    return score, reg

  def load_weights(self, filepath, by_name=False, **kwargs):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    :param kwargs:    Extra keyword arguments
    :keyword skip_layer: List of layer's name to be trained from scratch
                         (i.e. skip restoration)
    """

    # Reload from numpy dict
    _, ext = splitext(basename(filepath))
    if ext == '.npy':
      # Restore pre-trained model from numpy file
      tf_converter = {'kernel': 'weights',
                      'bias': 'biases'}
      layer_name_converter = {'fc1': 'conv4',
                              'fc2-1': 'conv5-1',
                              'fc2-2': 'conv5-2'}
      # Define layers that are trainable
      skip_layer = kwargs.get('skip_layer', [])
      # Load the weights into memory
      w_dict = _np.load(filepath, encoding="bytes", allow_pickle=True).item()
      w_dict = w_dict.get(self.name, None) or w_dict
      # Iterate over every layers in the model and set weights if needed.
      for layer in self.layers:
        layer_name = layer_name_converter.get(layer.name, layer.name)
        if layer_name not in skip_layer:
          # Need to initialize from pre-trained
          data = w_dict.get(layer_name, None)
          if data is not None:
            np_weight = []
            for w in layer.weights:
              start = w.name.rfind('/') + 1
              stop = w.name.find(':')
              wname = w.name[start:stop]
              wname = tf_converter.get(wname, wname).encode()
              weights = data[wname]
              weights = _convert_shape_if_needed(layer=layer, weight=weights)
              np_weight.append(weights)
            layer.set_weights(np_weight)
    else:
      # Try with base class function
      super(RNet, self).load_weights(filepath, by_name)
      

class ONet(Model):
  """ Network `ONet` implementation """

  def __init__(self, name='ONet', is_bgr=False, **kwargs):
    """
    Constructor
    :param name:    Network's name
    :param is_bgr:  If `True` indicates in is already in BGR format
    :param kwargs:  Extra keyword arguments
    """
    super(ONet, self).__init__(name=name, **kwargs)

    # Pre-processing
    self.norm1 = Lambda(lambda x: _preprocessing(image=x, is_bgr=is_bgr))
    # Conv + PReLU + Pool
    self.conv1 = Conv2D(filters=32,
                        kernel_size=3,
                        strides=(1, 1),
                        padding='valid',
                        use_bias=True,
                        name='conv1')
    self.prelu1 = PReLU(name='prelu1', shared_axes=[1, 2])
    self.pool1 = MaxPool2D(pool_size=(3, 3),
                           strides=(2, 2),
                           padding='same',
                           name='pool1')
    # Conv + PReLU + Pool
    self.conv2 = Conv2D(filters=64,
                        kernel_size=3,
                        strides=(1, 1),
                        padding='valid',
                        use_bias=True,
                        name='conv2')
    self.prelu2 = PReLU(name='prelu2', shared_axes=[1, 2])
    self.pool2 = MaxPool2D(pool_size=(3, 3),
                           strides=(2, 2),
                           padding='valid',
                           name='pool2')
    # Conv + PReLU + Pool
    self.conv3 = Conv2D(filters=64,
                        kernel_size=3,
                        strides=(1, 1),
                        padding='valid',
                        use_bias=True,
                        name='conv3')
    self.prelu3 = PReLU(name='prelu3', shared_axes=[1, 2])
    self.pool3 = MaxPool2D(pool_size=(2, 2),
                           strides=(2, 2),
                           padding='SAME',
                           name='pool3')
    # Conv + PReLU
    self.conv4 = Conv2D(filters=128,
                        kernel_size=2,
                        strides=(1, 1),
                        padding='valid',
                        use_bias=True,
                        name='conv4')
    self.prelu4 = PReLU(name='prelu4', shared_axes=[1, 2])
    # FC + PReLU
    self.flat1 = Flatten()
    self.fc1 = Dense(units=256, name='fc1')
    self.prelu5 = PReLU(name='prelu5')
    # Class prob
    self.fc2_1 = Dense(units=2, activation='softmax', name='fc2-1')
    # bbox regression
    self.fc2_2 = Dense(units=4, name='fc2-2')
    # landmark regression
    self.fc2_3 = Dense(units=10, name='fc2-3')

  def call(self, inputs, **kwargs):
    """
    Forward
    :param inputs:  Image
    :param kwargs:  Extra keyword arguments
    :return:  tuple: box score, box regression, landmarks regression
    """
    # Normalization
    x0 = self.norm1(inputs)
    # Conv + PReLU + Pool
    x1 = self.conv1(x0)
    x1 = self.prelu1(x1)
    x1 = self.pool1(x1)
    # Conv + PReLU + Pool
    x2 = self.conv2(x1)
    x2 = self.prelu2(x2)
    x2 = self.pool2(x2)
    # Conv + PReLU + Pool
    x3 = self.conv3(x2)
    x3 = self.prelu3(x3)
    x3 = self.pool3(x3)
    # Conv + PReLU
    x4 = self.conv4(x3)
    x4 = self.prelu4(x4)
    # FC + PReLU
    x5 = self.flat1(x4)
    x5 = self.fc1(x5)
    x5 = self.prelu5(x5)
    # Class prob
    score = self.fc2_1(x5)
    # bbox regression
    bbox = self.fc2_2(x5)
    # landmark regression
    lms = self.fc2_3(x5)
    return score, bbox, lms

  def load_weights(self, filepath, by_name=False, **kwargs):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    :param kwargs:    Extra keyword arguments
    :keyword skip_layer: List of layer's name to be trained from scratch
                         (i.e. skip restoration)
    """
    # Reload from numpy dict
    _, ext = splitext(basename(filepath))
    if ext == '.npy':
      # Restore pre-trained model from numpy file
      tf_converter = {'kernel': 'weights',
                      'bias': 'biases'}
      layer_name_converter = {'fc1': 'conv5',
                              'fc2-1': 'conv6-1',
                              'fc2-2': 'conv6-2',
                              'fc2-3': 'conv6-3'}
      # Define layers that are trainable
      skip_layer = kwargs.get('skip_layer', [])
      # Load the weights into memory
      w_dict = _np.load(filepath, encoding="bytes", allow_pickle=True).item()
      w_dict = w_dict.get(self.name, None) or w_dict
      # Iterate over every layers in the model and set weights if needed.
      for layer in self.layers:
        layer_name = layer_name_converter.get(layer.name, layer.name)
        if layer_name not in skip_layer:
          # Need to initialize from pre-trained
          data = w_dict.get(layer_name, None)
          if data is not None:
            np_weight = []
            for w in layer.weights:
              start = w.name.rfind('/') + 1
              stop = w.name.find(':')
              wname = w.name[start:stop]
              wname = tf_converter.get(wname, wname).encode()
              weights = data[wname]
              weights = _convert_shape_if_needed(layer=layer, weight=weights)
              np_weight.append(weights)
            layer.set_weights(np_weight)
    else:
      # Try with base class function
      super(ONet, self).load_weights(filepath, by_name)


class StageStatus(object):
  """ Keeps status between MTCNN stages """

  def __init__(self, pad_result: tuple = None, width=0, height=0):
    self.width = width
    self.height = height
    self.dy = self.edy = self.dx = self.edx = self.y = self.ey = self.x = self.ex = self.tmpw = self.tmph = []

    if pad_result is not None:
      self.update(pad_result)

  def update(self, pad_result: tuple):
    s = self
    s.dy, s.edy, s.dx, s.edx, s.y, s.ey, s.x, s.ex, s.tmpw, s.tmph = pad_result


class MTCNNInputConfig(InputConfigAbstract):
  """ Input configuration """

  def __init__(self):
    """ Constructor """
    super(MTCNNInputConfig, self).__init__([InputSpec(dims=(None, None, 3),
                                                      fmt=ImageFormat.BGR,
                                                      name='input')])
    self._preprocess_fn = Lambda(rgb_to_bgr, name='preprocessing')
    self._revert_preprocess_fn = Lambda(lambda x: _tf.reverse(x, axis=[-1]),
                                        name='revert_preprocessing')

  @property
  def preprocessing_fn(self):
    """
    Provide pre-processing layer
    :return:  Instance of `tf.keras.layer.Layer`
    """
    return self._preprocess_fn

  @property
  def revert_preprocessing_fn(self):
    """
    Return the default inverse pre-processing function used by the network. The
    signature of the function must be: revert_preprocessing_fn(inputs, **kwargs)
    where `inputs` is a list of input tensors to the network.
    Must be implemented by subclass
    :return:  revert pre-processing function
    """
    return self._revert_preprocess_fn


class MTCNN(Network):
  """
  MTCNN Implementation:
    a)  Detection of faces (with the confidence probability)
    b)  Detection of keypoints (left eye, right eye, nose, mouth_left, mouth_right)
  """

  def __init__(self,
               min_face_size: int = 20,
               steps_threshold: list = None,
               scale_factor: float = 0.709,
               is_bgr=False,
               name='MTCNN',
               **kwargs):
    """
    Constructor
    :param min_face_size:   Minimum face dimension
    :param steps_threshold: List of tresholds for the 3 stages.
                            Default: [0.6, 0.7, 0.7]
    :param scale_factor:  Scaling factor between each scale
    :param is_bgr:        If `True` indicates in is already in BGR format
    :param name: Network name
    """
    super(MTCNN, self).__init__(config=MTCNNInputConfig(),
                                name=name,
                                **kwargs)

    self.__min_face_size = min_face_size
    self.__steps_threshold = steps_threshold or [0.6, 0.7, 0.7]
    self.__scale_factor = scale_factor

    # Create networks
    self._pnet = PNet(is_bgr=is_bgr)
    self._rnet = RNet(is_bgr=is_bgr)
    self._onet = ONet(is_bgr=is_bgr)
    # Input dims
    self._pnet_dims = (1, None, None, 3)
    self._rnet_dims = (None, 24, 24, 3)
    self._onet_dims = (None, 48, 48, 3)
    # Build graph
    self._pnet.build(input_shape=self._pnet_dims)
    self._rnet.build(input_shape=self._rnet_dims)
    self._onet.build(input_shape=self._onet_dims)

  def detect(self, image):
    """
    Run face detection
    :param image: Image to feed to the network (numpy array)
    :return:  dict: Detection output
    """

    # Check image
    if image is None or not hasattr(image, 'shape'):
      raise ValueError('Image not valid')
    if image.dtype != _np.uint8:
      raise ValueError('Image must be of uint8 type')
    # Define image pyramid
    n, height, width, _ = image.shape
    if n != 1:
      raise ValueError('Too many images to process')

    # Define stage status
    stage_status = StageStatus(width=width, height=height)
    # Define image pyramid scale
    m = 12 / self.__min_face_size
    min_layer = _np.amin([height, width]) * m
    scales = self.__compute_scale_pyramid(m, min_layer)
    # Apply each procssing stage at each pyramid level
    stages = [self.__stage1, self.__stage2, self.__stage3]
    result = [scales, stage_status]
    for stg in stages:
      result = stg(image, result[0], result[1])
    # Process prediction
    bboxes, landmarks = result
    res = []
    for bbox, pts in zip(bboxes, landmarks):
      res.append({
        'box': [int(bbox[0]),
                int(bbox[1]),
                int(bbox[2] - bbox[0]),
                int(bbox[3] - bbox[1])],
        'confidence': bbox[-1],
        'keypoints': {
          'left_eye': (int(pts[0]), int(pts[5])),
          'right_eye': (int(pts[1]), int(pts[6])),
          'nose': (int(pts[2]), int(pts[7])),
          'mouth_left': (int(pts[3]), int(pts[8])),
          'mouth_right': (int(pts[4]), int(pts[9])),
        }
      })
    return res

  def load_weights(self, filepath, by_name=False, **kwargs):
    """
    Loads all layer weights, either from a TensorFlow, an HDF5 file or a numpy
    dictionary
    :param filepath:  Path to weights
    :param by_name:   Boolean, whether to load weights by name or by topological
                      order. Only topological loading is supported for weight
                      files in TensorFlow format.
    :param kwargs:    Extra keyword arguments
    :keyword skip_layer: List of layer's name to be trained from scratch
                         (i.e. skip restoration)
    """
    # Reload from numpy dict
    _, ext = splitext(basename(filepath))
    if ext == '.npy':
      self._pnet.load_weights(filepath, by_name, **kwargs)
      self._rnet.load_weights(filepath, by_name, **kwargs)
      self._onet.load_weights(filepath, by_name, **kwargs)
    else:
      super(MTCNN, self).load_weights(filepath, by_name, **kwargs)

  def get_optimizer(self, **kwargs):
    """
    Return the optimizer needed to train the network
    :return:  instance of `tf.keras.optimizer` or str for canned optimizer
    """
    # Create optimizer + parameters
    if self._optimizer_instance is None:
      optimizer_cls = get_optimizer(self._c_opt.type)
      self._optimizer_instance = optimizer_cls(**self._c_opt.params)
    return self._optimizer_instance

  def get_losses(self, **kwargs):
    """
    Return the loss function object to use while training the network
    :return:  List of `tf.keras.losses` or str for canned losses
    """
    # Photometric + Landmarks
    return []

  def get_metrics(self, **kwargs):
    """
    Return the metrics to use for monitoring the training process
    :return:  List of `tf.keras.metrics` or str for canned metrics
    """
    # Photo + landmarks
    return []

  def __compute_scale_pyramid(self, m, min_layer):
    """
    Compute scale at each pyramid's level
    :param m:
    :param min_layer:
    :return:  List of scales
    """
    scales = []
    factor_count = 0

    while min_layer >= 12:
      scales += [m * _np.power(self.__scale_factor, factor_count)]
      min_layer = min_layer * self.__scale_factor
      factor_count += 1
    return scales

  @staticmethod
  def __scale_image(image, scale: float, normalize=True):
    """
    Rescale a given image
    :param image:       Image to be rescaled
    :param scale:       Scaling factor
    :param normalized:  Indicate if normalization should be applied on the
                        output
    :return:  Scaled image
    """
    n, height, width, _ = image.shape
    scaled_w = int(_np.ceil(width * scale))
    scaled_h = int(_np.ceil(height * scale))
    # Resize
    im = _cv.resize(src=image[0, :, :, :],
                    dsize=(scaled_w, scaled_h),
                    interpolation=_cv.INTER_AREA)
    im = _np.expand_dims(im, 0)
    # Normalize the image's pixels
    if normalize:
      im = (im - 127.5) * 0.0078125
    return im

  def __stage1(self, image, scales, status):
    """
    Apply proposal network `PNet`
    :param image:   Image to process
    :param scales:  List of scales in the pyramid images
    :param status:  Status for the current stage
    :return:  tuple with proposed bbox, status update
    """
    bboxes = _np.empty((0, 9))
    new_status = status
    # Go through all scales
    for s in scales:
      # Generate image
      im = self.__scale_image(image=image, scale=s, normalize=False)
      # bbox proposal
      im = _np.transpose(im, (0, 2, 1, 3))  # Matlab uses WHC convention
      bb_score, bbox_reg = self._pnet(im)
      bb_score = _tf.transpose(bb_score, (0, 2, 1, 3))  # Convert back 2 TF
      bbox_reg = _tf.transpose(bbox_reg, (0, 2, 1, 3))
      bbox = self.__generate_bbox(score=bb_score.numpy(),
                                  bbox_reg=bbox_reg.numpy(),
                                  scale=s,
                                  thresh=self.__steps_threshold[0])
      # inter-scale nms
      pick = self.__nms(bbox, 0.5, 'Union')
      if bbox.size > 0 and pick.size > 0:
        bbox = bbox[pick, :]
        bboxes = _np.append(bboxes, bbox, axis=0)
    # Apply NMS again on bbox from all level of the pyramid
    if bboxes.shape[0] > 0:
      pick = self.__nms(bboxes, 0.7, 'Union')
      bboxes = bboxes[pick, :]
      # Refine bbox, convert back to image scale
      reg_w = bboxes[:, 2] - bboxes[:, 0] + 1
      reg_h = bboxes[:, 3] - bboxes[:, 1] + 1
      qq1 = bboxes[:, 0] + bboxes[:, 5] * reg_w
      qq2 = bboxes[:, 1] + bboxes[:, 6] * reg_h
      qq3 = bboxes[:, 2] + bboxes[:, 7] * reg_w
      qq4 = bboxes[:, 3] + bboxes[:, 8] * reg_h
      bboxes = _np.vstack([qq1, qq2, qq3, qq4, bboxes[:, 4]]).T
      # Convert to square boxes
      bboxes = self.__square_bbox(bbox=bboxes)
      # Update status
      bb_pad = self.__pad(bbox=bboxes,
                          width=status.width,
                          height=status.height)
      new_status = StageStatus(pad_result=bb_pad,
                               width=status.width,
                               height=status.height)
    return bboxes, new_status

  def __stage2(self, image, bboxes, status):
    """
    Apply refinement network `RNet`
    :param image:   Image to process
    :param bboxes:   List of bbox detected so far
    :param status:   Status for the current stage
    :return:
    """
    n_bbox = bboxes.shape[0]
    if bboxes.shape[0] == 0:
      return bboxes, status
    # Crop patches
    _, in_h, in_w, in_c = self._rnet_dims
    im_batch = _np.zeros((n_bbox, in_h, in_w, in_c), dtype=_np.float32)
    s = status
    for k in range(n_bbox):
      tmp = _np.zeros((int(status.tmph[k]), int(status.tmpw[k]), 3),
                      dtype=_np.float32)
      tmp[(s.dy[k] - 1):s.edy[k], (s.dx[k] - 1):s.edx[k], :] = \
        image[0, (s.y[k] - 1):s.ey[k], (s.x[k] - 1):s.ex[k], :]
      im_batch[k, :, :, :] = _cv.resize(tmp,
                                        (in_h, in_w),
                                        interpolation=_cv.INTER_AREA)
    # Refine
    im_batch = _np.transpose(im_batch, (0, 2, 1, 3))  # magic transpose
    bb_score, bb_reg = self._rnet(im_batch)
    bb_score = bb_score.numpy()
    bb_reg = bb_reg.numpy()
    # Pick box that pass threshold
    pick = _np.where(bb_score[:, 1] > self.__steps_threshold[1])
    # Define bbox
    bboxes = _np.hstack([bboxes[pick[0], 0:4],
                         _np.expand_dims(bb_score[pick[0], 1], -1)])
    if bboxes.shape[0] > 0:
      # NMS
      pick = self.__nms(bbox=bboxes, thresh=0.7, type='Union')
      bboxes = bboxes[pick, :]
      # Regress + square
      a = 0
      bboxes = self.__calibrate_bbox(bbox=bboxes, reg=bb_reg[pick, :])
      bboxes = self.__square_bbox(bbox=bboxes)
    return bboxes, status

  def __stage3(self, image, bboxes, status):
    """
    Third stage, refine bbox and detect landmarks
    :param image:   Image to process
    :param bboxes:   List of bbox detected so far
    :param status:   Status for the current stage
    :return:  bbox + landmarks
    """
    n_bbox = bboxes.shape[0]
    if bboxes.shape[0] == 0:
      return bboxes, _np.empty(shape=(0,))
    # Calibrate bbox
    bboxes = _np.fix(bboxes).astype(_np.int32)
    bb_pad = self.__pad(bboxes, status.width, status.height)
    status = StageStatus(bb_pad, width=status.width, height=status.height)
    # Crop patches
    _, in_h, in_w, in_c = self._onet_dims
    im_batch = _np.zeros((n_bbox, in_h, in_w, in_c), dtype=_np.float32)
    s = status
    for k in range(n_bbox):
      tmp = _np.zeros((int(status.tmph[k]), int(status.tmpw[k]), 3),
                      dtype=_np.float32)
      tmp[(s.dy[k] - 1):s.edy[k], (s.dx[k] - 1):s.edx[k], :] = \
        image[0, (s.y[k] - 1):s.ey[k], (s.x[k] - 1):s.ex[k], :]
      im_batch[k, :, :, :] = _cv.resize(tmp,
                                        (in_h, in_w),
                                        interpolation=_cv.INTER_AREA)
    # Refine
    im_batch = _np.transpose(im_batch, (0, 2, 1, 3))
    bb_score, bb_reg, lms = self._onet(im_batch)
    bb_score = bb_score.numpy()
    bb_reg = bb_reg.numpy()
    lms = lms.numpy()
    pick = _np.where(bb_score[:, 1] > self.__steps_threshold[2])
    # Pick landmarks + bbox
    lms = lms[pick[0], :]
    bboxes = _np.hstack([bboxes[pick[0], 0:4],
                         _np.expand_dims(bb_score[pick[0], 1], 1)])
    if bboxes.shape[0] > 0:
      # compute landmarks + bbox
      bbw = bboxes[:, 2] - bboxes[:, 0] + 1
      bbh = bboxes[:, 3] - bboxes[:, 1] + 1

      lms[:, 0:5] = _np.expand_dims(bboxes[:, 0], 1) + \
                    _np.expand_dims(bbw, 1) * lms[:, 0:5]
      lms[:, 5:10] = _np.expand_dims(bboxes[:, 1], 1) + \
                     _np.expand_dims(bbh, 1) * lms[:, 5:10]
      # nms
      bboxes = self.__calibrate_bbox(bbox=bboxes, reg=bb_reg[pick[0], :])
      pick = self.__nms(bboxes, 0.7, 'Min')
      bboxes = bboxes[pick, :]
      lms = lms[pick, :]
    return bboxes, lms



  @staticmethod
  def __generate_bbox(score, bbox_reg, scale, thresh):
    """
    Generate bounding box from regressed output of `PNet`
    :param score:     Bbox scores
    :param bbox_reg:  Regressed bbox
    :param scale:     Current scale
    :param thresh:    Score threshold for acceptance
    :return: Array of bouding box of size [K x 9] where each line is
            [x1, y1, x2, y2, score, dx1, dy1, dx2, dy2]
    """
    # use heatmap to generate bounding boxes
    stride = 2
    cellsize = 12
    # Find candidate with score larger than current `thresh`.
    # Score: [1, H, W, 2] where first channel is `not bbox` second `bbox`
    y, x = _np.where(score[0, :, :, 1] >= thresh)
    score = score[0, y, x, 1]
    # Offset
    offset = _np.array([bbox_reg[0, y, x, i] for i in range(4)])
    # Position
    pos = _np.array([_np.fix((stride * x + 1) / scale),
                     _np.fix((stride * y + 1) / scale),
                     _np.fix((stride * x + cellsize) / scale),
                     _np.fix((stride * y + cellsize) / scale)])
    return _np.vstack([pos, score, offset]).T

  @staticmethod
  def __nms(bbox, thresh, type='Union'):
    """
    Non-maximum suppression
    :param bbox:    List of predicted bounding box
    :param thresh:  Scoring threshold
    :param type:    Type of NMS ('Min', 'Union')
    :return:  Indexes to keep
    """
    x1 = bbox[:, 0]
    y1 = bbox[:, 1]
    x2 = bbox[:, 2]
    y2 = bbox[:, 3]
    scores = bbox[:, 4]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1]

    keep = []
    while order.size > 0:
      i = order[0]
      keep.append(i)
      xx1 = _np.maximum(x1[i], x1[order[1:]])
      yy1 = _np.maximum(y1[i], y1[order[1:]])
      xx2 = _np.minimum(x2[i], x2[order[1:]])
      yy2 = _np.minimum(y2[i], y2[order[1:]])

      w = _np.maximum(0.0, xx2 - xx1 + 1)
      h = _np.maximum(0.0, yy2 - yy1 + 1)
      inter = w * h
      if type == 'Union':
        ovr = inter / (areas[i] + areas[order[1:]] - inter)
      else:
        ovr = inter / _np.minimum(areas[i], areas[order[1:]])
      # keep
      inds = _np.where(ovr <= thresh)[0]
      order = order[inds + 1]

    return _np.asarray(keep, dtype=_np.int32)

  @staticmethod
  def __square_bbox(bbox):
    """
    Convert convert box to square
    :param bbox:  bounding boxes
    :return:      Squared bounding boxes
    """
    sbbox = _np.empty(bbox.shape, dtype=bbox.dtype)
    # Define largest side
    bbw = bbox[:, 2] - bbox[:, 0]
    bbh = bbox[:, 3] - bbox[:, 1]
    side = _np.maximum(bbh, bbw)
    # Create square
    sbbox[:, 0] = bbox[:, 0] + (bbw * 0.5) - (side * 0.5)
    sbbox[:, 1] = bbox[:, 1] + (bbh * 0.5) - (side * 0.5)
    sbbox[:, 2] = sbbox[:, 0] + side
    sbbox[:, 3] = sbbox[:, 1] + side
    sbbox[:, :4] = _np.fix(sbbox[:, :4])
    sbbox[:, 4] = bbox[:, 4]
    return sbbox

  @staticmethod
  def __calibrate_bbox(bbox, reg):
    """
    Calibrate bounding box (i.e. refined the prediction)
    :param bbox:  Input bbox
    :param reg:   Refinement predicted by `RNet`
    :return:      Refined bbox
    """
    w = bbox[:, 2] - bbox[:, 0] + 1
    h = bbox[:, 3] - bbox[:, 1] + 1
    b1 = bbox[:, 0] + reg[:, 0] * w
    b2 = bbox[:, 1] + reg[:, 1] * h
    b3 = bbox[:, 2] + reg[:, 2] * w
    b4 = bbox[:, 3] + reg[:, 3] * h
    bbox[:, 0:4] = _np.transpose(_np.vstack([b1, b2, b3, b4]))
    return bbox

  @staticmethod
  def __pad(bbox, width, height):
    """
    Compute the padding coordinates (pad the bounding boxes to square)
    :param bbox:    Bboxes to be paded
    :param width:   Stage width
    :param height:  Stage height
    :return: Bbox padding
    """
    tmpw = (bbox[:, 2] - bbox[:, 0] + 1).astype(_np.int32)
    tmph = (bbox[:, 3] - bbox[:, 1] + 1).astype(_np.int32)
    numbox = bbox.shape[0]

    dx = _np.ones(numbox, dtype=_np.int32)
    dy = _np.ones(numbox, dtype=_np.int32)
    edx = tmpw.copy().astype(_np.int32)
    edy = tmph.copy().astype(_np.int32)

    x = bbox[:, 0].astype(_np.int32)
    y = bbox[:, 1].astype(_np.int32)
    ex = bbox[:, 2].astype(_np.int32)
    ey = bbox[:, 3].astype(_np.int32)

    tmp = _np.where(ex > width)
    edx.flat[tmp] = _np.expand_dims(-ex[tmp] + width + tmpw[tmp], 1)
    ex[tmp] = width

    tmp = _np.where(ey > height)
    edy.flat[tmp] = _np.expand_dims(-ey[tmp] + height + tmph[tmp], 1)
    ey[tmp] = height

    tmp = _np.where(x < 1)
    dx.flat[tmp] = _np.expand_dims(2 - x[tmp], 1)
    x[tmp] = 1

    tmp = _np.where(y < 1)
    dy.flat[tmp] = _np.expand_dims(2 - y[tmp], 1)
    y[tmp] = 1

    return dy, edy, dx, edx, y, ey, x, ex, tmpw, tmph


def plot_prediction(image: _np.ndarray,
                    pred: list,
                    linewidth: int=2,
                    edgecolor: str='r'):
  """
  Draw prediction
  :param image: Canvas
  :param pred:  List of prediction
  """
  import matplotlib.pyplot as plt
  from matplotlib.patches import Rectangle, Circle
  fig, ax = plt.subplots(1)
  # Display the image
  ax.imshow(image[0, :, :, :] / 255.0)
  for p in pred:
    x, y, w, h = p['box']
    rect = Rectangle((x, y), w, h,
                     linewidth=linewidth,
                     edgecolor=edgecolor,
                     facecolor='none')
    # Add the patch to the Axes
    ax.add_patch(rect)
    # Keypoint
    for key, pts in p['keypoints'].items():
      x, y = pts
      c = Circle(xy=pts,
                 radius=2,
                 linewidth=linewidth,
                 edgecolor=edgecolor,
                 facecolor=edgecolor)
      # Add the patch to the Axes
      ax.add_patch(c)
  plt.show(block=False)