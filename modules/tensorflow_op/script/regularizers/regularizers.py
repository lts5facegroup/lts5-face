# coding=utf-8
""" Custom keras regularizers """
from math import pi
import tensorflow as _tf
import tensorflow.keras.backend as K
from tensorflow.keras.regularizers import Regularizer
from tensorflow.python.eager.def_function import Function
from tensorflow.python.framework.errors import InvalidArgumentError
from lts5.tensorflow_op.utils.naming import create_unique_name
from lts5.tensorflow_op.utils.naming import to_camel_case




class SummaryRegularizer(Regularizer):
  """ Base class for regularizer with logging capabilities """

  def __init__(self,
               manager=None,
               logging_fn=None,
               traces=(),
               name=None):
    """
    :param manager:   Instance of `SummaryWriterManager` used to manage writer.
                      If `None` no summary will be saved.
                      Note `manager` should outlive the layer!
    :param logging_fn: Callback responsible of recording to generate summaries.
                       The signature must be `log_fn(inputs, step)` where
                       `inputs` is a Tensor of list of tensor to be summarized.
                       `step` is the step internal step counter.
    :param traces:    List of traces/writers to create. Used only if `manager`
                      is not `None`, in this case the layer will have attributes
                      named `_w_<trace>` which are writers available for
                      recording summaries
    :param name:      Regularizer name
    """

    # Recording enable
    self._manager = manager
    self._init_set_name(name)
    if self._manager is not None:
      # Create writers
      for tr in traces:
        setattr(self, '_w_{}'.format(tr), self._manager.get_writer(tr))

        # Create identity callback if not used
        def _dummy_log_fn(inputs, step):
          pass

        self._log_fn = _dummy_log_fn if self._manager is None else logging_fn

        # Create step variable
        self._step = _tf.Variable(0,
                                  trainable=False,
                                  dtype=_tf.int64,
                                  name='{}_step'.format(self.name))

  def __call__(self, inputs):
    """
    Logging call
    :param inputs:  List of tensor to log
    :return:  same value as inputs
    """
    with _tf.name_scope(self.name + '_logging'):
      self._log_fn(inputs, self._step)
      self._increment_step_by(value=1)
      return inputs

  def _increment_step_by(self, value=1):
    """
    Increment step counter
    :param value: Value to increment the counter for
    """
    self._step.assign_add(value)

  def _init_set_name(self, name):
    """
    Initialize regularizer name
    :param name:  Regularizer's name candidate
    """
    # Set default candidate to class name if nothing provided
    name = self.__class__.__name__ if not name else name
    # Pick unique name
    self.name = create_unique_name(to_camel_case(name))


class GaussianRegularizer(SummaryRegularizer):
  """ Negative gaussian log-likelihood regularizer """

  def __init__(self,
               weight,
               mean=0.0,
               sdev=1.0,
               dtype=_tf.float32,
               manager=None):
    """
    Constructor
    :param weight:  Amount of regularization to add
    :param mean:    Gaussian mean
    :param sdev:    Gaussian standard deviation
    :param dtype:   Data type
    """

    # Log
    def _log_fn(inputs, step, phase):
      _tf.print('Logging regularizer...')
      self._w_train.scalar(data=inputs,
                           name='regularizer/' + self.name,
                           step=step,
                           phase=phase)
    
    super(GaussianRegularizer, self).__init__(manager=manager,
                                              logging_fn=_log_fn,
                                              traces=('train',))
    # Gaussian regularizer
    self._dtype = dtype
    self._mean = _tf.convert_to_tensor(mean, dtype=dtype)
    self._sdev = _tf.convert_to_tensor(sdev, dtype=dtype)
    self._norm = -0.5 * (_tf.math.log(2.0 * pi) + 2.0 * _tf.math.log(sdev))
    self._w = _tf.convert_to_tensor(weight, dtype=dtype)

  def __call__(self, x):
    """
    Compute regularization cost
    :param x: Kernel to regularize
    :return:  Cost
    """
    xn = (x - self._mean) ** 2.0
    xn = xn / (self._sdev * self._sdev)
    xn = 0.5 * _tf.reduce_sum(xn)
    xn = self._norm - xn
    reg = -self._w * xn
    return super(GaussianRegularizer, self).__call__(reg)

  def get_config(self):
    return {'weight': self._w,
            'mean': self._mean,
            'sdev': self._sdev,
            'dtype': self._dtype,
            'manager': self._manager}


class Orthogonal2DRegularizer(Regularizer):
  """
  Orthogonality regularization for Dense layer

  See:
  https://towardsdatascience.com/build-the-right-autoencoder-tune-and-optimize-using-pca-principles-part-ii-24b9cca69bd6
  """

  def __init__(self,
               weight=1.0,
               axis=0):
    if axis not in [0, 1]:
      raise InvalidArgumentError(node_def=None,
                                 op=None,
                                 message='Support only 2D weights')

    self.weight = weight
    self.axis = axis

  def __call__(self, w):
    """
    Compute orthogonality regularization
    :param w: Weight
    :return:  Cost
    """
    if self.axis == 1:
      w = K.transpose(w)
    m = K.dot(K.transpose(w), w)
    m -= K.eye(m.shape)
    return self.weight * K.sqrt(K.sum(K.square(m)))   # Frobenius norm
