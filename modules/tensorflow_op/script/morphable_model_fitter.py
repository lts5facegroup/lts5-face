# coding=utf-8
"""
3DMM Fitting process
"""
import os
from os.path import join, exists
from os import makedirs as mkdir
from argparse import ArgumentParser
import numpy as np
from imageio import imread, imwrite
import cv2 as cv
import tensorflow as tf
import matplotlib.pyplot as plt
from lts5.utils.tools import search_folder
from lts5.face_tracker import SDMTracker as SDM
from lts5.utils import RodriguesFloat as Rodrigues
from lts5.tensorflow_op.utils.summary import SummaryWriterManager
from lts5.tensorflow_op.network.utils import get_optimizer
from lts5.tensorflow_op.network.morphnet import MorphNetParameter
from lts5.tensorflow_op.layers.face import ParametricFaceModel as FaceModel
from lts5.tensorflow_op.layers.lighting import SHLighting
from lts5.tensorflow_op.layers.rendering import DeferredRendererV3
from lts5.tensorflow_op.layers.transform import EyeToScreenProjection
from lts5.tensorflow_op.losses import PhotometricLogLikelihoodV2 as PhotoNLL
from lts5.tensorflow_op.losses import ImageGradientLikelihood as GradNLL
from lts5.tensorflow_op.losses import LandmarkLogLikelihood as LmsNLL
from lts5.tensorflow_op.losses import LandmarksPairLogLikelihood as LmsPairNLL
from lts5.tensorflow_op.metrics import MeanPhotometricError
from lts5.tensorflow_op.metrics import MeanLandmarkError, MeanLandmarkPairError


def _draw_landmarks(image, landmarks):
  # Convert to BGR
  canvas = image.copy().astype(np.float32)
  if canvas.max() > 1.0:
    canvas /= 255.0
  for lm in landmarks:
    canvas = cv.circle(canvas,
                       (int(lm[0]), int(lm[1])),
                       1,
                       (0, 1.0, 0), -1, cv.LINE_AA)
  plt.imshow(canvas)
  plt.show(block=False)


class ModelFitter:
  """ 3DMM Fitting algorithm """

  def __init__(self,
               config,
               image_space_shading=False,
               manager=None):
    """
    Constructor
    :param config:  Configuration
    :param image_space_shading: If `True` perform shading in image space,
      otherwise at vertex level
    :param manager: SummaryWriter manager to use to dump value into TensorBoard
    """
    self.cfg = config
    self.img_space_shading = image_space_shading
    self.tvec_scale = cfg.predictor.multihead['position_head']['scale']
    # FORWARD
    # -----------------------------------------------
    # Parametric face model
    illu_prior_path = config.regularizer.illu_prior
    sym_vertex_path = config.model.symmetric_vertex_path
    sem_path = config.model.semantic_segmentation_path
    self.face_model = FaceModel(face_model_path=config.model.path,
                                with_illumination=not self.img_space_shading,
                                illumination_prior_path=illu_prior_path,
                                with_refinement=config.model.n_es > 0,
                                laplacian_path=config.model.laplacian_path,
                                sym_vertex_path=sym_vertex_path,
                                semantic_segmentation_path=sem_path,
                                w_id=config.regularizer.w_id,
                                w_exp=config.regularizer.w_exp,
                                w_tex=config.regularizer.w_tex,
                                w_illu=config.regularizer.w_illu,
                                w_sym=config.regularizer.w_sym,
                                w_smo=config.regularizer.w_smo,
                                w_mag=config.regularizer.w_mag,
                                manager=manager)
    # Deferred renderer
    self.renderer = DeferredRendererV3(width=config.rendering.width,
                                       height=config.rendering.height,
                                       near=config.rendering.near,
                                       far=config.rendering.far,
                                       fixed_triangle=True)
    # Image space shading ?
    self.sh_light = None
    if self.img_space_shading:
      self.sh_light = SHLighting(illu_prior_path,
                                 w_illu=config.regularizer.w_illu,
                                 manager=manager)

    # Landmarks
    self.lms = None
    if config.model.indices is not None:
      self.lms = EyeToScreenProjection(width=config.rendering.width,
                                       height=config.rendering.height,
                                       near=config.rendering.near,
                                       far=config.rendering.far,
                                       subset=config.model.indices)

    # BACKWARD
    # -----------------------------------------------
    # Losses
    w_im_grad = config.regularizer.w_im_grad
    w_ph = config.regularizer.w_photo
    w_alb = config.regularizer.w_alb * w_ph
    self.photo_loss = PhotoNLL(background_prior=config.regularizer.bg_prior,
                               weight=w_ph,
                               n_level=config.regularizer.n_level,
                               sigma=config.regularizer.sdev,
                               type='l1',
                               normalized=False,
                               name='LikelihoodPhoto')
    self.albedo_loss = None
    if w_alb > 0.0:
      self.albedo_loss = PhotoNLL(background_prior=config.regularizer.bg_prior,
                                  weight=w_alb,
                                  n_level=config.regularizer.n_level,
                                  sigma=config.regularizer.sdev,
                                  type='l1',
                                  normalized=False,
                                  name='LikelihoodAlbedo')

    # Image gradients
    self.img_grad = None
    if w_im_grad > 0.0:
      self.img_grad = GradNLL(n_level=config.regularizer.n_level,
                              sigma=config.regularizer.sdev_im_grad,
                              weight=w_im_grad,
                              type='l2',
                              normalized=False)

    self.lms_loss = None
    self.lms_pair_loss = None
    if self.lms:
      self.lms_loss = LmsNLL(sigma=config.regularizer.sdev_lms,
                             weight=config.regularizer.w_lms,
                             sample_weights=config.model.weights,
                             type='l1',
                             normalized=False,
                             name='LikelihoodLandmark')
      self.lms_pair_loss = LmsPairNLL(pairs=config.model.pairs,
                                      sigma=config.regularizer.sdev_pair,
                                      weight=config.regularizer.w_lms_p,
                                      sample_weights=None,
                                      type='l1',
                                      normalized=False,
                                      name='LikelihoodPairLandmark')

    # Optimizer
    if config.optimizer.type.lower() != 'rectifiedadam':
      ctor_fn = get_optimizer(config.optimizer.type)
      self.optimizer = ctor_fn(**config.optimizer.params)
    else:
      from tensorflow_addons.optimizers import RectifiedAdam
      self.optimizer = RectifiedAdam(**config.optimizer.params)

    # metrics
    self.ph_metric = MeanPhotometricError()
    self.lms_metric = None
    self.lms_pair_metric = None
    if self.lms:
      self.lms_metric = MeanLandmarkError(name='MeanLandmarkError')
      self.lms_pair_metric = MeanLandmarkPairError(pairs=config.model.pairs,
                                                   name='MeanLandmarkPairError')

    # Logging
    # -----------------------------------------------
    self.w_loss = None
    if manager is not None:
      self.w_loss = manager.get_writer('loss')

  def _forward(self, params: list, attributes: list=None):
    """
    Generate image from a given set of parameters
    :param params:  List of batched parameters:
      - w_shp, [Batch, #Segment, Ns]
      - w_tex, [Batch, #Segment, Nt]
      - w_illu, [Batch, 1, Ni]
      - rvec, [Batch, 1, 3]
      - tvec, [Batch, 1, 3]
      - focal, [Batch, 1]
      - bg, [B, H, W, 3]
      - w_shp_corr, [B, 1, Nsc] (optional)
      - w_tex_corr, [B, 1, Nst] (optional)
    :param attributes: List of extra attributes to interpolate in the image.
    :return:  Image + landmarks, inner loss
    """
    # Generate face
    face_params = params[:2]    # w_shp, w_tex
    face_params += [params[3]]          # rvec
    face_params += [params[4] * self.tvec_scale]  # tvec
    if not self.img_space_shading:
      face_params += [params[2]]  # w_illu
    if len(params) > 7:
      face_params += params[-2:]
    focal = params[5]
    bg = params[6]
    face_attr, inner_loss = self.face_model(face_params)
    shape = face_attr['vertex']
    albedo = face_attr['albedo']
    tri = face_attr['tri']

    # Define attributes to interpolate
    attr = []
    if not self.img_space_shading:
      color = face_attr['color']
      # Color
      attr += [(color, lambda x: tf.clip_by_value(x, 0.0, 1.0), bg, 'lin')]
      # Albedo required ?
      if self.cfg.regularizer.w_alb > 0.0:
        attr += [(albedo, lambda x: tf.clip_by_value(x, 0.0, 1.0), bg, 'lin')]
    else:
      # Albedo
      attr += [(albedo, lambda x: tf.clip_by_value(x, 0.0, 1.0), bg, 'lin')]
      # attr += [(albedo, tf.identity, bg, 'lin')]
      # Normals for shading
      normal = face_attr['normal']
      attr += [(normal,
                lambda x: tf.math.l2_normalize(x, axis=-1),
                tf.zeros_like(bg),
                'lin')]
    # Semantic ?
    if len(face_attr) > 6:
      attr.append((face_attr['semantic'],
                   tf.identity,
                   tf.zeros(shape=bg.shape[:-1] + [1]),
                   'flat'))
    # Extra attributes
    if attributes is not None:
      for attribute in attributes:
        attr.append((attribute,
                     lambda x: tf.clip_by_value(x, 0.0, 1.0),
                     tf.zeros_like(bg),
                     'lin'))
    # Rasterize + interpolate required attributes
    interp = self.renderer([shape, focal, tri], attributes=attr)
    # Define output
    outputs = []
    if not self.img_space_shading:
      outputs += interp
    else:
      im_albedo = interp[0]
      im_normal = interp[1]
      im_color, inner_light_loss = self._screen_space_shading(im_albedo,
                                                              im_normal,
                                                              w_illu=params[2])
      inner_loss += inner_light_loss
      outputs += [im_color]
      if self.cfg.regularizer.w_alb > 0.0:
        outputs += [interp[0]]
      # Extra attributes (i.e. semantic + external attributes)
      outputs += interp[2:]
    # Landmarks ?
    if self.lms:
      lms = self.lms(inputs=[shape, focal])
      outputs.append(lms)
    return outputs, inner_loss

  def _backward(self,
                params: list,
                labels: list,
                step: tf.Tensor,
                attributes: list=None):
    """
    Compute forward + backward steps
    :param params:  List of batched parameters:
      - w_shp, [Batch, #Segment, Ns]
      - w_tex, [Batch, #Segment, Nt]
      - w_illu, [Batch, 1, Ni]
      - rvec, [Batch, 1, 3]
      - tvec, [Batch, 1, 3]
      - bg, [B, H, W, 3]
      - w_shp_corr, [B, 1, Nsc] (optional)
      - w_tex_corr, [B, 1, Nst] (optional)
    :param labels: List of batched labels:
      - image, [B, H, W, 4] where alpha channel is segmentation mask
      - landmarks, [B, L, 2] (optional)
    :param step:    Optimization step counter
    :param attributes: List of extra attributes to interpolate in the image.
    :return:  Forward outputs, loss
    """
    # Generate images
    # Statistical regularization is part of `inner_loss`
    # params: [w_shp, w_tex, w_illu, rvec, tvec, focal, bg,
    # (w_shp_corr, w_tex_corr)]
    fwd_params = params[:6]
    if len(params) > 7:
      fwd_params += params[-2:]
    fwd_params.append(labels[0][..., :3])  # Add background
    outputs, inner_loss = self._forward(params=fwd_params,
                                        attributes=attributes)
    # Compute loss
    img_true = labels[0]
    img_pred = outputs[0]
    # Photometric error with lighting
    l_ph = self.photo_loss(img_true, img_pred)
    # Photometric error without lighting
    l_alb = 0.0
    if self.albedo_loss:
      alb_pred = outputs[1]
      l_alb = self.albedo_loss(img_true, alb_pred)
    l_im_grad = 0.0
    if self.img_grad:
      l_im_grad = self.img_grad(img_true, img_pred)

    l_lms = 0.0
    l_pair = 0.0
    # Landmarks ?
    if self.lms_loss:
      lms_true = labels[1]
      lms_pred = outputs[-1]
      l_lms = self.lms_loss(lms_true, lms_pred)
      l_pair = self.lms_pair_loss(lms_true, lms_pred)

    # Sum everything
    loss = l_ph + l_alb + l_lms + l_pair + l_im_grad + inner_loss

    # Metrics
    self.ph_metric.reset_states()
    m_ph = self.ph_metric(img_true, img_pred)
    m_lms = 0.0
    m_lms_p = 0.0
    if self.lms_metric:
      self.lms_metric.reset_states()
      self.lms_pair_metric.reset_states()
      lms_true = labels[1]
      lms_pred = outputs[-1]
      m_lms = self.lms_metric(lms_true, lms_pred)
      m_lms_p = self.lms_pair_metric(lms_true, lms_pred)

    # Log ?
    if self.w_loss:
      # Loss
      self.w_loss.scalar(name='Loss/Photo', data=l_ph, step=step)
      self.w_loss.scalar(name='Loss/Albedo', data=l_alb, step=step)
      self.w_loss.scalar(name='Loss/Lms', data=l_lms, step=step)
      self.w_loss.scalar(name='Loss/LmsPair', data=l_pair, step=step)
      self.w_loss.scalar(name='Loss/Reg', data=inner_loss, step=step)
      self.w_loss.scalar(name='Loss/Loss', data=loss, step=step)
      self.w_loss.scalar(name='Loss/ImGrad', data=l_im_grad, step=step)
      # Metrics
      self.w_loss.scalar(name='Metric/Photo', data=m_ph, step=step)
      self.w_loss.scalar(name='Metric/Lms', data=m_lms, step=step)
      self.w_loss.scalar(name='Metric/LmsPair', data=m_lms_p, step=step)
    # Done
    return outputs, loss

  def _screen_space_shading(self, albedo, normal, w_illu):
    """
    Perform shading in image space with interpolated inputs (i.e. albedo &
    normals)
    :param albedo:  Tensor, surface albedo [B, H, W, 4]
    :param normal:  Tensor, surface normals [B, H, W, 4]
    :param w_illu:  Tensor, spherical harmonics coefficients [B, 1, 27]
    :return:  Tensor, shaded color [B, H, W, 4],
              Tensor, light regularization
    """
    im_albedo = albedo[..., :3]
    im_mask = albedo[..., 3:]
    im_normal = normal[..., :3]
    shading, inner_loss = self.sh_light([im_normal, w_illu])
    # Set background shading to 1 to avoid changing color
    shading = (im_mask * shading) + (1.0 - im_mask)
    color = tf.clip_by_value(shading * im_albedo, 0.0, 1.0)
    color = tf.concat([color, im_mask], axis=-1)
    return color, inner_loss

  def _initialize_variables(self, size, segment):
    """
    Initialize face model variables to be optimized
    :param size:  Batch size
    :param segment: Number of segment in face model
    :return:  List of variables
    """
    # Shape
    w_shp = np.zeros(shape=(size,
                            segment,
                            self.cfg.model.ns + self.cfg.model.ne),
                     dtype=np.float32)
    params = [tf.Variable(shape=w_shp.shape,
                          initial_value=w_shp,
                          name='w_shp')]

    # Tex
    w_tex = np.zeros(shape=(size, segment, self.cfg.model.nt),
                     dtype=np.float32)
    params.append(tf.Variable(shape=w_tex.shape,
                              initial_value=w_tex,
                              name='w_tex'))
    # Illumination
    w_illu = np.zeros(shape=(size, 1, 27), dtype=np.float32)
    w_illu[:, :, 0:-1:9] = 1.0
    params.append(tf.Variable(shape=w_illu.shape,
                              initial_value=w_illu,
                              name='w_illu'))
    # Rotation
    rvec = np.zeros(shape=(size, 1, 3), dtype=np.float32)
    params.append(tf.Variable(shape=rvec.shape,
                              initial_value=rvec,
                              name='rvec'))
    # Translation
    tvec = np.zeros(shape=(size, 1, 3), dtype=np.float32)
    tvec[..., 1] = -0.01  # 0.0285
    tvec[..., 2] = -0.975
    params.append(tf.Variable(shape=tvec.shape,
                              initial_value=tvec,
                              name='tvec'))
    # Focal
    focal = (np.ones(shape=(size, 1), dtype=np.float32) *
             self.cfg.rendering.focal)
    params.append(tf.Variable(shape=focal.shape,
                              initial_value=focal,
                              trainable=False,
                              name='focal'))

    # Shape+Texture correction
    if self.cfg.model.n_es > 0:
      w_shp_corr = np.zeros(shape=(size, 1, self.cfg.model.n_es),
                            dtype=np.float32)
      w_tex_corr = np.zeros(shape=(size, 1, self.cfg.model.n_et),
                            dtype=np.float32)
      params.append(tf.Variable(shape=w_shp_corr.shape,
                                initial_value=w_shp_corr,
                                name='w_shp_corr'))
      params.append(tf.Variable(shape=w_tex_corr.shape,
                                initial_value=w_tex_corr,
                                name='w_tex_corr'))
    return params

  @tf.function
  def _optimization_step(self, params, labels, step, attributes: list=None):
    """
    Perform a single step of gradient descent
    :param params:  List of variables to be optimized
    :param labels:  List of labels (i.e. ground truth)
    :param step:    Optimization step counter
    :param attributes: List of extra attributes to interpolate in the image.
    :return:  outputs, loss
    """
    with tf.GradientTape() as tape:
      tape.watch(params)
      # Forward + Backward pass
      outputs, loss = self._backward(params, labels, step, attributes)
    # Compute gradients
    params = [p for p in params if p.trainable]
    grads = tape.gradient(loss, params)
    # Apply optimizer step
    self.optimizer.apply_gradients(zip(grads, params))
    # Done
    return outputs, loss

  def _default_random_parameters(self, n_image):
    """
    Generate random set of parameters
    :param n_image: Number of images to generate
    :return:  List of parameters
    """
    ns = self.cfg.model.ns
    ne = self.cfg.model.ne
    nt = self.cfg.model.nt
    w_shp = np.zeros(shape=(n_image, 1, ns + ne), dtype=np.float32)
    w_tex = np.zeros(shape=(n_image, 1, nt), dtype=np.float32)
    w_illu = np.zeros(shape=(n_image, 1, 27), dtype=np.float32)
    rvec = np.zeros(shape=(n_image, 1, 3), dtype=np.float32)
    tvec = np.zeros(shape=(n_image, 1, 3), dtype=np.float32)
    bg = np.zeros(shape=(n_image,
                         self.cfg.rendering.height,
                         self.cfg.rendering.width,
                         3),
                  dtype=np.float32)
    sign = 1.0
    for k in range(n_image):
      # Shape + Texture
      c = k // 2
      w_shp[k, 0, c] = sign * 3.0
      w_shp[k, 0, ne + c] = sign * 3.0
      w_tex[k, 0, c] = sign * 3.0
      ang = np.random.uniform(-45.0, 45.0)
      # Pose
      axis = np.random.normal(size=(3, ))
      axis /= np.linalg.norm(axis)
      r = Rodrigues(axis=(axis[0], axis[1], axis[2]), angle=np.deg2rad(ang))
      rvec[k, 0, :] = (r.x, r.y, r.z)
      tvec[k, 0, :] = (0.0, -0.01, -1.0)
      # Change sign
      sign *= -1.0
      # Default illumination param, ensure ambient light have some value
      w_illu[k, 0, ::9] = 1.0
    params = [w_shp, w_tex, w_illu, rvec, tvec, bg]
    # Convert to Tensor
    return [tf.convert_to_tensor(p) for p in params]

  def generate_image(self, folder, n_image, param_generator_fn=None):
    """
    Generate a series of image
    :param folder:  Location where to save the images
    :param n_image: Number of image to generate
    :param param_generator_fn: Function taking the number of image to generate
      as input and output a list of parameters used to generate the images.
      Default generate random face
    """
    # Output exist ?
    if not exists(folder):
      mkdir(folder)
    # Generate parameters
    params_fn = param_generator_fn or self._default_random_parameters
    params = params_fn(n_image)
    # Generate image
    renderings, _ = self._forward(params)
    images = renderings[0].numpy()
    # Dump them
    for k, img in enumerate(images):
      fname = join(folder, 'generated_image_{:04d}.jpg'.format(k))
      im = (img[..., :3] * 255.0).astype(np.uint8)
      imwrite(fname, im)

  def fit_image(self, folder_or_images, tracker_path, steps, **kwargs):
    """
    Fit a given batch of images
    :param folder_or_images:  List of images (`str` or `numpy.ndarray`) or a
      folder location where to search for images
    :param tracker_path:  Path to the tracker location
    :param steps: Number of optimization step to perform
    :param kwargs: extra keyword arguments
    :keyword tracker_threshold: Face tracker treshold, default -1.0
    :keyword n_segment: number of segment in face model, default 1
    :keyword attribute_path: str or list of str, Path to a file storing an
      attribute shared among all reconstruction
    """

    # Create face tracker
    tracker = SDM()
    tracker.load(tracker=tracker_path)
    tracker.thresh = kwargs.get('tracker_threshold', -1.0)

    # Load images
    targets = []
    if isinstance(folder_or_images, str):
      targets = search_folder(folder_or_images, ext=['.jpg'], relative=False)
    elif isinstance(folder_or_images, (list, tuple)):
      targets = folder_or_images
    else:
      raise ValueError('`folder_or_images` must be either `str`, `list`or '
                       '`tuple` ')

    images = []
    landmarks = []
    for t in targets:
      # Load image
      if isinstance(t, str):
        im = np.asarray(imread(t)).astype(np.float32)
      elif isinstance(t, np.ndarray):
        im = t.astype(np.float32)
      else:
        raise ValueError('Images must be either `str` or `np.ndarray`')

      # Define skin region -> assume the whole image is "skin"
      pskin = np.ones((im.shape[0], im.shape[1], 1), dtype=np.float32)
      img = np.concatenate([im / 255.0, pskin], axis=-1)
      images.append(img)
      # Detect landmarks
      img = im[..., ::-1]  # RGB -> BGR
      lms = tracker.detect(img, (0, 0, im.shape[0], im.shape[1]))
      lms = np.zeros((1, 136),
                     dtype=np.float32) if lms is None else \
        lms.astype(np.float32)
      lms = lms.reshape(2, -1).T.reshape(-1, 2)
      landmarks.append(lms)
      _draw_landmarks(image=img[..., ::-1], landmarks=lms)

    attributes = None
    attributes_path = kwargs.get('attribute_path', None)
    if attributes_path is not None:
      attributes = []
      for path in attributes_path:
        attr = np.load(path)
        assert attr.ndim == 2, 'Attributes must have 2 dims'
        # Replicate it N times
        attr = np.tile(tf.expand_dims(attr, 0), [len(images), 1, 1])
        attributes.append(tf.convert_to_tensor(attr, tf.float32))

    # Batch everything up
    images = tf.convert_to_tensor(np.stack(images, axis=0))
    landmarks = tf.convert_to_tensor(np.stack(landmarks, axis=0))

    # Init parameters
    n_img = len(images)
    n_segment = kwargs.get('n_segment', 1)
    mm_params = self._initialize_variables(n_img, n_segment)

    # Start optimization
    outputs = []
    loss = 0.0
    for step in range(steps):
      # Single step of gradient descent
      tf_step = tf.convert_to_tensor(step, dtype=tf.int64)
      outputs, loss = self._optimization_step(params=mm_params,
                                              labels=[images, landmarks],
                                              step=tf_step,
                                              attributes=attributes)
      # Track progress
      print('{:05d} Loss: {:.2f},'.format(step, loss.numpy()))
    return outputs, mm_params, loss, images


if __name__ == '__main__':
  # Parser
  p = ArgumentParser('Morphable Model Fitting Debugging / Testing')

  # Config
  p.add_argument('--config',
                 type=str,
                 required=True,
                 help='JSON Configuration file')

  p.add_argument('--folder',
                 type=str,
                 required=True,
                 help='Location where to search/store images')

  p.add_argument('--n_image',
                 type=int,
                 required=False,
                 help='Number of images to be generated')
  p.add_argument('--tracker',
                 type=str,
                 required=True,
                 help='Path to SDM model')
  args = p.parse_args()

  # Load config
  cfg = MorphNetParameter.from_json(args.config)
  cfg.model.n_es = 0
  cfg.model.n_et = 0

  cfg.optimizer.type = 'Adam'
  cfg.optimizer.params = {'learning_rate': 1.0e-3}  # Adam
  cfg.regularizer.w_photo = 1.0
  cfg.regularizer.w_id = 1.5e-4 * 2.0
  cfg.regularizer.w_exp = 1.25e-4 * 2.0
  cfg.regularizer.w_tex = 5e-5 * 2.0
  cfg.regularizer.w_illu = 1e-4
  cfg.regularizer.w_alb = 0.0
  cfg.regularizer.w_lms = 75e-2
  cfg.regularizer.w_lms_p = 25e-1

  cfg.model.path = '/home/christophe/Documents/LTS5/Data/models/basel_face_exp_dm_deng_paper_segment_model.bin'
  # cfg.model.path = '/home/christophe/Documents/LTS5/Data/models/basel_face_exp_dm_deng_paper_model.bin'
  cfg.model.symmetric_vertex_path = '/home/christophe/Documents/LTS5/Data/models/bfm_deng_face05_symlist_dm.txt'
  cfg.model.indices = [16644, 16888, 16467, 16264, 32244, 32939,
                       # 33375, 33654, 33838, 34022, 34312, 34766,
                       33375, 33635, 33835, 34037, 34312, 34766,
                       35472, 27816, 27608, 27208, 27440, 28111,
                       28787, 29177, 29382, 29549, 30288, 30454,
                       30662, 31056, 31716, 8161, 8177, 8187,
                       8191, 6515, 7243, 8204, 9163, 9883, 1958,
                       3887, 5048, 5958, 4804, 3643, 10328,
                       11352, 12384, 14326, 12656, 11495, 5522,
                       6025, 7495, 8215, 8935, 10395, 10795,
                       9555, 8836, 8236, 7636, 6915, 5909, 7384,
                       8223, 9064, 10537, 8829, 8229, 7629]

  # Summary manager
  runs = os.listdir('dbg/')
  logdir = 'dbg/run_{:03d}'.format(len(runs))
  manager = SummaryWriterManager(log_dir=logdir, log_freqency=50, use_v2=True)

  # Create model fitter
  algo = ModelFitter(config=cfg,
                     image_space_shading=True,
                     manager=manager)

  if args.n_image is not None:
    # Rotate image along y-axis
    def _p_generator(n_image):
      """
      Generate random set of parameters
      :param n_image: Number of images to generate
      :return:  List of parameters
      """
      w_shp = np.zeros(shape=(n_image, 1, 80 + 64), dtype=np.float32)
      w_tex = np.zeros(shape=(n_image, 1, 80), dtype=np.float32)
      w_illu = np.zeros(shape=(n_image, 1, 27), dtype=np.float32)
      rvec = np.zeros(shape=(n_image, 1, 3), dtype=np.float32)
      tvec = np.zeros(shape=(n_image, 1, 3), dtype=np.float32)
      bg = np.zeros(shape=(n_image, 224, 224, 3), dtype=np.float32)
      sign = 1.0
      delta_ang = 90.0 / (n_image - 1)
      for k in range(n_image):
        # Shape + Texture
        c = 0   # Pick first mod
        w_shp[k, 0, c] = sign * 3.0
        w_shp[k, 0, 64 + c] = sign * 3.0
        w_tex[k, 0, c] = sign * 3.0
        ang = -45.0 + k * delta_ang
        # Pose
        r = Rodrigues(axis=(0.0, 1.0, 0.0), angle=np.deg2rad(ang))
        rvec[k, 0, :] = (r.x, r.y, r.z)
        tvec[k, 0, :] = (0.0, -0.01, -1.0)
        # Default illumination param, ensure ambient light have some value
        w_illu[k, 0, ::9] = 1.0
      params = [w_shp, w_tex, w_illu, rvec, tvec, bg]
      # Convert to Tensor
      return [tf.convert_to_tensor(p) for p in params]

    # Generate image
    algo.generate_image(folder=args.folder,
                        n_image=args.n_image,
                        param_generator_fn=_p_generator)
  else:
    # Fit image
    images_or_folder = args.folder
    n_segment = 4 if '_segment' in cfg.model.path else 1
    outputs = algo.fit_image(folder_or_images=images_or_folder,
                             tracker_path=args.tracker,
                             steps=4000,
                             n_segment=n_segment)#,
                             #attribute_path=['models/wrinkle_mask.npy'])
    renderings, params, loss, target = outputs

    preds = renderings[0][..., :3].numpy()
    for k, im in enumerate(preds):
      fig = plt.figure()
      im = (im * 255.0).astype(np.uint8)
      plt.imshow(im)
      plt.show()
      plt.close(fig)
      # Dump image
      imwrite('dbg/reconstruction{:02d}.jpg'.format(k), im)

    images = target[..., :3].numpy()
    # for pred, img in zip(preds, images):
    #   diff = img - pred
    #   fig = plt.figure()
    #   plt.imshow(diff, cmap='RdBu')
    #   plt.colorbar()
    #   plt.show()
    #   plt.close(fig)


    from lts5.geometry import MeshFloat as Mesh


    def _create_mesh(model, w_shp, w_tex):
      attributes, _ = model([w_shp, w_tex])
      shp = attributes[0]
      color = attributes[1]
      tri = attributes[2]
      # Create meshes
      tri = tri.numpy()
      for k, (surf, refl) in enumerate(zip(shp.numpy(), color.numpy())):
        m = Mesh()
        m.vertex = surf.reshape(-1, 3)
        m.vertex_color = refl.reshape(-1, 3)
        m.tri = tri.astype(np.int32)
        m.save('dbg/optim_mesh{}.ply'.format(k))

    _create_mesh(algo.face_model.face_model, w_shp=params[0], w_tex=params[1])




  a = 0