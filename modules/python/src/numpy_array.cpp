/**
 *  @file   numpy_array.cpp
 *  @brief  Initialize numpy array
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   6/5/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#define LTS5_FACE_USE_IMPORT
#include "lts5/python/numpy_array.hpp"

// http://numpy-discussion.10968.n7.nabble.com/How-to-call-import-array-properly-td1383.html
#if PY_MAJOR_VERSION >= 3 
void* numpy_init_array() {
  import_array();
  return (void*)1; // make compiler happy
}
#else
void numpy_init_array() {
  import_array();
}
#endif

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/*
 * @name  InitNumpyArray
 * @fn    void InitNumpyArray(void)
 * @brief Initialize numpy array module
 */
void InitNumpyArray() {
  numpy_init_array();
}

}  // namespace Pythonp
}  // namespace LTS5