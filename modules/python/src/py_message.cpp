/**
 *  @file   py_message.cpp
 *  @brief  Wrapper to write into python console
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   05/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "Python.h"
#include "lts5/python/py_message.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    PyErrorMessage
 * @fn  void PyErrorMessage(const char* frmt, ...)
 * @brief   Write into python error console
 * @param[in] frmt  Message format
 * @param[in] ...   Message content
 */
void PyErrorMessage(const char* frmt, ...) {
  char str[1000];

  va_list ap;
  va_start(ap, frmt);
  vsnprintf(str, sizeof(str), frmt, ap);
  va_end(ap);

  PyErr_SetString(PyExc_TypeError, str);
}

}  // namepsace Python
}  // namepsace LTS5