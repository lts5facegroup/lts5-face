/**
 *  @file   ocv_converter.cpp
 *  @brief  Convert numpy array into opencv matrix
 *  @see https://github.com/yati-sagade/opencv-ndarray-conversion/
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   05/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <cmath>  // https://stackoverflow.com/questions/28683358 mingw64
#include <cstring>
#include "Python.h"

#include "opencv2/core.hpp"

#include "lts5/python/ocv_converter.hpp"
#include "lts5/python/numpy_array.hpp"
#include "lts5/python/py_message.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

static PyObject* opencv_error = 0;
#define ERRWRAP2(expr) \
try { \
    pybind11::gil_scoped_release gil; \
    expr; \
} catch (const cv::Exception &e) \
{ \
    PyErr_SetString(opencv_error, e.what()); \
    return 0; \
}

#pragma mark -
#pragma mark Numpy Allocator


#if (CV_MAJOR_VERSION > 3)
using FlagType = cv::AccessFlag;
#else
using FlagType = int;
#endif


/**
 * @class   NumpyAllocator
 * @brief   Custom memory allocator
 */
class NumpyAllocator : public cv::MatAllocator {
 public:
  /**
   * @name  NumpyAllocator
   * @fn    NumpyAllocator()
   * @brief Constructor
   */
  NumpyAllocator() {
    stdAllocator = cv::Mat::getDefaultAllocator();
  }

  /**
   * @name  ~NumpyAllocator
   * @fn    ~NumpyAllocator()
   * @brief Destructor
   */
  ~NumpyAllocator() override = default;

  cv::UMatData* allocate(PyObject* o,
                         int dims,
                         const int* sizes,
                         int type, size_t* step) const {
    cv::UMatData* u = new cv::UMatData(this);
    u->data = u->origdata = (uchar*)PyArray_DATA((PyArrayObject*) o);
    npy_intp* _strides = PyArray_STRIDES((PyArrayObject*) o);
    for( int i = 0; i < dims - 1; i++ )
      step[i] = (size_t)_strides[i];
    step[dims-1] = CV_ELEM_SIZE(type);
    u->size = sizes[0]*step[0];
    u->userdata = o;
    return u;
  }

  cv::UMatData* allocate(int dims0,
                         const int* sizes,
                         int type,
                         void* data,
                         size_t* step,
                         FlagType flags,
                         cv::UMatUsageFlags usageFlags) const override {
    if( data != nullptr)
    {
      CV_Error(cv::Error::StsAssert, "The data should normally be NULL!");
      // probably this is safe to do in such extreme case
      return stdAllocator->allocate(dims0,
                                    sizes,
                                    type,
                                    data,
                                    step,
                                    flags,
                                    usageFlags);
    }
    pybind11::gil_scoped_acquire gil;

    int depth = CV_MAT_DEPTH(type);
    int cn = CV_MAT_CN(type);
    const int f = (int)(sizeof(size_t)/8);
    int typenum = depth == CV_8U ?
                  NPY_UBYTE :
                  depth == CV_8S ?
                  NPY_BYTE :
                  depth == CV_16U ?
                  NPY_USHORT :
                  depth == CV_16S ?
                  NPY_SHORT :
                  depth == CV_32S ?
                  NPY_INT :
                  depth == CV_32F ?
                  NPY_FLOAT :
                  depth == CV_64F ?
                  NPY_DOUBLE :
                  f*NPY_ULONGLONG + (f^1)*NPY_UINT;
    int i, dims = dims0;
    cv::AutoBuffer<npy_intp> _sizes(dims + 1);
    for( i = 0; i < dims; i++ )
      _sizes[i] = sizes[i];
    if( cn > 1 )
      _sizes[dims++] = cn;
    PyObject* o = PyArray_SimpleNew(dims, _sizes, typenum);
    if(!o)
      CV_Error_(cv::Error::StsError,
                ("The numpy array of typenum=%d, ndims=%d can not be created", typenum, dims));
    return allocate(o, dims0, sizes, type, step);
  }

  bool allocate(cv::UMatData* u,
                FlagType accessFlags,
                cv::UMatUsageFlags usageFlags) const override {
    return stdAllocator->allocate(u, accessFlags, usageFlags);
  }

  void deallocate(cv::UMatData* u) const override {
    if(!u)
      return;
    pybind11::gil_scoped_acquire gil;
    CV_Assert(u->urefcount >= 0);
    CV_Assert(u->refcount >= 0);
    if(u->refcount == 0)
    {
      PyObject* o = (PyObject*)u->userdata;
      Py_XDECREF(o);
      delete u;
    }
  }

  const cv::MatAllocator* stdAllocator;
};
/** Allocator instance */
NumpyAllocator g_numpyAllocator;

/*
 * @name  toRect
 * @fn    static cv::Rect toRect(PyObject* o)
 * @brief Convert a python object into a cv::Rect
 * @param[in] o   Python object
 * @return    Converted rectangle
 */
cv::Rect NDArrayConverter::toRect(PyObject* o) {
  cv::Rect r;
  PyArg_ParseTuple(o, "iiii", &r.x, &r.y, &r.width, &r.height);
  return r;
}

/*
 * @name  toMat
 * @fn    cv::Mat toMat(const PyObject* o)
 * @brief Convert python object into opencv mat
 * @param[in] o   Python object to convert
 * @return    Converted opencv matrix
 */
cv::Mat NDArrayConverter::toMat(PyObject* o) {
  cv::Mat mat;
  bool allowND = true;
  if (!PyArray_Check(o)) {
    LTS5::Python::PyErrorMessage("argument is not a numpy array");
    if (!mat.data)
      mat.allocator = &g_numpyAllocator;
  } else {
    PyArrayObject* oarr = (PyArrayObject*) o;

    bool needcopy = false, needcast = false;
    int typenum = PyArray_TYPE(oarr), new_typenum = typenum;
    int type = typenum == NPY_UBYTE ? CV_8U : typenum == NPY_BYTE ? CV_8S :
                                              typenum == NPY_USHORT ? CV_16U :
                                              typenum == NPY_SHORT ? CV_16S :
                                              typenum == NPY_INT ? CV_32S :
                                              typenum == NPY_INT32 ? CV_32S :
                                              typenum == NPY_FLOAT ? CV_32F :
                                              typenum == NPY_DOUBLE ? CV_64F : -1;

    if (type < 0) {
      if (typenum == NPY_INT64 || typenum == NPY_UINT64
          || type == NPY_LONG) {
        needcopy = needcast = true;
        new_typenum = NPY_INT;
        type = CV_32S;
      } else {
        LTS5::Python::PyErrorMessage("Argument data type is not supported");
        mat.allocator = &g_numpyAllocator;
        return mat;
      }
    }

#ifndef CV_MAX_DIM
    const int CV_MAX_DIM = 32;
#endif

    int ndims = PyArray_NDIM(oarr);
    if (ndims >= CV_MAX_DIM) {
      LTS5::Python::PyErrorMessage("Dimensionality of argument is too high");
      if (!mat.data)
        mat.allocator = &g_numpyAllocator;
      return mat;
    }

    int size[CV_MAX_DIM + 1];
    size_t step[CV_MAX_DIM + 1];
    size_t elemsize = CV_ELEM_SIZE1(type);
    const npy_intp* _sizes = PyArray_DIMS(oarr);
    const npy_intp* _strides = PyArray_STRIDES(oarr);
    bool ismultichannel = ndims == 3 && _sizes[2] <= CV_CN_MAX;

    for (int i = ndims - 1; i >= 0 && !needcopy; i--) {
      // these checks handle cases of
      //  a) multi-dimensional (ndims > 2) arrays, as well as simpler 1- and 2-dimensional cases
      //  b) transposed arrays, where _strides[] elements go in non-descending order
      //  c) flipped arrays, where some of _strides[] elements are negative
      if ((i == ndims - 1 && (size_t) _strides[i] != elemsize)
          || (i < ndims - 1 && _strides[i] < _strides[i + 1]))
        needcopy = true;
    }

    if (ismultichannel && _strides[1] != (npy_intp) elemsize * _sizes[2])
      needcopy = true;

    if (needcopy) {
      if (needcast) {
        o = PyArray_Cast(oarr, new_typenum);
        oarr = (PyArrayObject*) o;
      } else {
        oarr = PyArray_GETCONTIGUOUS(oarr);
        o = (PyObject*) oarr;
      }
      _strides = PyArray_STRIDES(oarr);
    }

    for (int i = 0; i < ndims; i++) {
      size[i] = (int) _sizes[i];
      step[i] = (size_t) _strides[i];
    }

    // handle degenerate case
    if (ndims == 0) {
      size[ndims] = 1;
      step[ndims] = elemsize;
      ndims++;
    }

    if (ismultichannel) {
      ndims--;
      type |= CV_MAKETYPE(0, size[2]);
    }

    if (ndims > 2 && !allowND) {
      LTS5::Python::PyErrorMessage("%s has more than 2 dimensions");
    } else {

      mat = cv::Mat(ndims, size, type, PyArray_DATA(oarr), step);
      mat.u = g_numpyAllocator.allocate(o, ndims, size, type, step);
      mat.addref();

      if (!needcopy) {
        Py_INCREF(o);
      }
    }
    mat.allocator = &g_numpyAllocator;
  }
  return mat;
}

/*
 * @name  toPyObject
 * @fn    static PyObject* toPyObject(const cv::Rect& r)
 * @brief Convert cv::Rect into python object
 * @param[in] r Rectangle to convert
 * @return    Converted rectangle
 */
PyObject* NDArrayConverter::toPyObject(const cv::Rect& r) {
  return Py_BuildValue("(iiii)", r.x, r.y, r.width, r.height);
}

/*
 * @name  toNDArray
 * @fn    PyObject* toNDArray(const cv::Mat& mat)
 * @brief Convert opencv matrix into python object
 * @param[in] mat Matrix to convert
 * @return    Converted matrix
 */
PyObject* NDArrayConverter::toNDArray(const cv::Mat& mat) {
  if (!mat.data)
    Py_RETURN_NONE;
  cv::Mat temp,
          *p = (cv::Mat*) &mat;
  if (!p->u || p->allocator != &g_numpyAllocator) {
    temp.allocator = &g_numpyAllocator;
    ERRWRAP2(mat.copyTo(temp));
    p = &temp;
  }
  PyObject* o = (PyObject*) p->u->userdata;
  Py_INCREF(o);
  return o;
}

#pragma mark -
#pragma mark Module

/*
 * @name  PrintMatrix
 * @fn static void PrintMatrix(const cv::Mat& mat)
 * @brief Dump matrix into `std::cout`
 * @param[in] mat
 * @return String representation
 */
std::string OCVMatConverterTest::PrintMatrix(const cv::Mat& mat) {
  std::ostringstream stream;
  stream << mat << std::endl;
  return stream.str();
}

/*
 * @name  Print
 * @fn static std::string PrintRect(const cv::Rect& rect)
 * @brief Dump rectangle into `std::cout`
 * @param[in] rect    Rectangle
 * @return String representation
 */
std::string OCVMatConverterTest::PrintRect(const cv::Rect& rect) {
  std::ostringstream stream;
  stream << rect << std::endl;
  return stream.str();
}

/*
 * @name  CreateMatrix
 * @fn    static cv::Mat CreateMatrix()
 * @brief Create an opencv matrix
 * @return    Opencv matrix
 */
cv::Mat OCVMatConverterTest::CreateMatrix() {
  cv::Mat data = cv::Mat::zeros(3, 4, CV_32FC1);
  data.at<float>(0, 0) = 1.f;
  data.at<float>(1, 1) = 2.f;
  data.at<float>(2, 2) = 3.f;
  return data;
}

/*
 * @name  CreateRect
 * @fn    static cv::Rect CreateRect()
 * @brief Create an opencv rectangle
 * @return    Opencv rectangle
 */
cv::Rect OCVMatConverterTest::CreateRect() {
  return cv::Rect(100, 200, 300, 400);
}

/*
 * @name AddOCVMatConverterTestCase
 * @brief Add OCVMatConverter testcase to the module
 * @param[in,out] module Python module where to add the testcase
 */
void AddOCVMatConverterTestCase(pybind11::module& module) {

  // Print
  module.def("ocvmat_converter_print_mat",
             &OCVMatConverterTest::PrintMatrix,
             R"doc(Dump a given matrix into `cout` string)doc");
  module.def("ocvmat_converter_print_rect",
             &OCVMatConverterTest::PrintRect,
             R"doc(Dump a given rectangle into `cout` string)doc");
  // Create
  module.def("ocvmat_converter_create_mat",
             &OCVMatConverterTest::CreateMatrix,
             R"doc(Create a matrix and return it as np.array)doc");
  module.def("ocvmat_converter_create_rect",
             &OCVMatConverterTest::CreateRect,
             R"doc(Create a rectanle and return it as tuple)doc");
}

}  // namepsace Python
}  // namepsace LTS5
