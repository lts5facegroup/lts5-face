/**
 *  @file   py_streambuf.cpp
 *  @brief Python wrapper for steambuf
 *  @ingroup python
 *  @see  https://gist.github.com/asford/544323a5da7dddad2c9174490eb5ed06
 *
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  Copyright © 2018 Christophe Ecabert. All rights reserved.
 */

#include "lts5/python/py_streambuf.hpp"

namespace py = pybind11;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
  
/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {


#pragma mark -
#pragma mark Implementation

/*
 *  @name   IStream
 *  @brief  constructor
 *  @param[in] buf Python buffer to wrapper
 */
PyStreambuf::IStream::IStream(PyStreambuf& buf) : std::istream(&buf) {
  exceptions(std::ios_base::badbit);
}

/*
 *  @name   ~IStream
 *  @brief  destructor
 */
PyStreambuf::IStream::~IStream() {
  if (this->good()) {
    this->sync();
  }
}

/*
 *  @name   OStream
 *  @brief  constructor
 *  @param[in] buf Python buffer to
 */
PyStreambuf::OStream::OStream(PyStreambuf& buf) : std::ostream(&buf) {
  exceptions(std::ios_base::badbit);
}

/*
 *  @name   ~OStream
 *  @brief  destructor
 */
PyStreambuf::OStream::~OStream() {
  if (this->good()) {
    this->flush();
  }
}

/*
 *  @name   PyStreambuf
 *  @brief  Constructor
 *  @param[in] obj_file   Python object file to use as input/output
 *  @param[in] buff_size  Buffer size
 */
PyStreambuf::PyStreambuf(py::object& obj_file, size_t buffer_size) :
        py_stream_(obj_file),
        py_read_(getattr(obj_file, "read", py::none())),
        py_write_(getattr(obj_file, "write", py::none())),
        py_seek_(getattr(obj_file, "seek", py::none())),
        py_tell_(getattr(obj_file, "tell", py::none())),
        buffer_size_(buffer_size),
        write_buffer_(nullptr),
        pos_of_read_buffer_end_in_py_file_(0),
        pos_of_write_buffer_end_in_py_file_(0),
        farthest_pptr_(nullptr),
        bytes_like_(false) {
  // Acquire GIL, since python function will be called
  py::gil_scoped_acquire gil;
  // Sanity check
  assert(buffer_size_ != 0);
  // Some Python file objects (e.g. sys.stdout and sys.stdin) have
  // non-functional seek and tell. If so, assign None to py_tell and py_seek.
  if (!py_tell_.is_none()) {
    try {
      py_tell_();
    }
    catch (py::error_already_set& err) {
      py_tell_ = py::none();
      py_seek_ = py::none();
      err.restore();
      PyErr_Clear();
    }
  }
  if (!py_write_.is_none()) {
    // C-like string to make debugging easier
    write_buffer_ = new char[buffer_size + 1];
    write_buffer_[buffer_size] = '\0';
    setp(write_buffer_, write_buffer_ + buffer_size_);  // 27.5.2.4.5 (5)
    farthest_pptr_ = pptr();
    // Define the type of input required by the write() function
    try {
      py::bytes chunk;
      py_write_(chunk);
      bytes_like_ = true;
    } catch (py::error_already_set& err) {
      err.restore();
      PyErr_Clear();
      bytes_like_ = false;
    }
  }
  else {
    // The first attempt at output will result in a call to overflow
    setp(nullptr, nullptr);
  }

  if (!py_tell_.is_none()){
    auto py_pos = py_tell_().cast<off_type>();
    pos_of_read_buffer_end_in_py_file_ = py_pos;
    pos_of_write_buffer_end_in_py_file_ = py_pos;
  }
}

/*
 *  @name   ~PyStreambuf
 *  @brief  Destructor
 */
PyStreambuf::~PyStreambuf() {
  delete[] write_buffer_;
}

/*
 *  @name   showmanyc
 *  @brief  It is essential to override this virtual function for the stream
 *          member function readsome to work correctly
 *          (c.f. 27.6.1.3, alinea 30)
 */
std::streamsize PyStreambuf::showmanyc() {
  int_type const failure = traits_type::eof();
  int_type status = underflow();
  if (status == failure) {
    return -1;
  }
  return egptr() - gptr();
}

/*
 *  @name   underflow
 *  @brief  C.f. C++ standard section 27.5.2.4.3
 */
PyStreambuf::int_type PyStreambuf::underflow() {
  int_type const failure = traits_type::eof();
  if (this->py_read_.is_none()) {
    throw std::invalid_argument("That Python file object has no 'read' "
                                "attribute");
  }
  // Acquire GIL, since python function will be called
  py::gil_scoped_acquire gil;
  // Read
  read_buffer_ = this->py_read_(this->buffer_size_);
  char* data;
  py::ssize_t py_n_read;
  if (PYBIND11_BYTES_AS_STRING_AND_SIZE(read_buffer_.ptr(),
                                        &data,
                                        &py_n_read) == -1) {
    setg(nullptr, nullptr, nullptr);
    throw std::invalid_argument("The method 'read' of the Python file object "
                                "did not return a string.");
  }
  auto n_read = static_cast<off_type>(py_n_read);
  this->pos_of_read_buffer_end_in_py_file_ += n_read;
  this->setg(data, data, data + n_read);

  // ^^^27.5.2.3.1 (4)
  if (n_read == 0) {
    return failure;
  }
  return traits_type::to_int_type(data[0]);
}

/*
 *  @name   overflow
 *  @brief  C.f. C++ standard section 27.5.2.4.5
 */
PyStreambuf::int_type PyStreambuf::overflow(int_type c) {
  if (this->py_write_.is_none()) {
    throw std::invalid_argument("That Python file object has no 'write' "
                                "attribute");
  }
  // Acquire GIL, since python function will be called
  py::gil_scoped_acquire gil;
  // Write
  this->farthest_pptr_ = std::max(this->farthest_pptr_, pptr());
  off_type n_written = (off_type)(this->farthest_pptr_ - pbase());
  py::bytes chunk(pbase(), n_written);
  if (bytes_like_) {
    this->py_write_(chunk);
  } else {
    py::str chunk_str(chunk);
    this->py_write_(chunk_str);
  }
  if (!traits_type::eq_int_type(c, traits_type::eof())) {
    this->py_write_(traits_type::to_char_type(c));
    n_written++;
  }
  if (n_written) {
    this->pos_of_write_buffer_end_in_py_file_ += n_written;
    setp(pbase(), epptr());
    // ^^^ 27.5.2.4.5 (5)
    farthest_pptr_ = pptr();
  }
  return (traits_type::eq_int_type(c,
                                   traits_type::eof()) ?
          traits_type::not_eof(c) :
          c);
}

/*
 *  @name   sync
 *  @brief  Empty the write buffer into the Python file object and set the
 *          seek position of the latter accordingly (C++ standard section
 *          27.5.2.4.2). If there is no write buffer or it is empty, but
 *          there is a non-empty read buffer, set the Python file object seek
 *          position to the seek position in that read buffer.
 */
int PyStreambuf::sync() {
  int result = 0;
  // Acquire GIL, since python function will be called
  py::gil_scoped_acquire gil;
  // Sync
  this->farthest_pptr_ = std::max(this->farthest_pptr_, pptr());
  if (this->farthest_pptr_ && this->farthest_pptr_ > pbase()) {
    off_type delta = pptr() - this->farthest_pptr_;
    int_type status = overflow(traits_type::eof());
    if (traits_type::eq_int_type(status, traits_type::eof())) {
      result = -1;
    }
    if (!this->py_seek_.is_none()) {
      this->py_seek_(delta, 1);
    }
  } else if (gptr() && gptr() < egptr()) {
    if (!this->py_seek_.is_none()) {
      this->py_seek_(gptr() - egptr(), 1);
    }
  }
  return result;
}

/*
 *  @name   seekoff
 *  @brief  This implementation is optimised to look whether the position is
 *          within the buffers, so as to avoid calling Python seek or tell.
 *          It is important for many applications that the overhead of calling
 *          into Python is avoided as much as possible (e.g. parsers which
 *          may do a lot of backtracking)
 */
PyStreambuf::pos_type PyStreambuf::seekoff(off_type off,
                                           std::ios_base::seekdir way,
                                           std::ios_base::openmode which) {
  // Acquire GIL, since python function will be called
  py::gil_scoped_acquire gil;
  // In practice, "which" is either std::ios_base::in or out since we end up
  // here because either seekp or seekg was called on the stream using this
  // buffer. That simplifies the code in a few places.
  int const failure = off_type(-1);
  if (this->py_seek_.is_none()) {
    throw std::invalid_argument("That Python file object has no 'seek' "
                                "attribute");
  }
  // we need the read buffer to contain something!
  if (which == std::ios_base::in && !gptr()) {
    if (traits_type::eq_int_type(underflow(), traits_type::eof())) {
      return failure;
    }
  }
  // compute the whence parameter for Python seek
  int whence;
  switch (way) {
    case std::ios_base::beg:
      whence = 0;
      break;
    case std::ios_base::cur:
      whence = 1;
      break;
    case std::ios_base::end:
      whence = 2;
      break;
    default:
      return failure;
  }
  // Let's have a go
  off_type result;
  if (!seekoff_without_calling_python(off, way, which, &result)) {
    // we need to call Python
    if (which == std::ios_base::out) {
      overflow(traits_type::eof());
    }
    if (way == std::ios_base::cur) {
      if (which == std::ios_base::in) {
        off -= egptr() - gptr();
      } else if (which == std::ios_base::out) {
        off += pptr() - pbase();
      }
    }
    this->py_seek_(off, whence);
    result = off_type(py_tell_().cast<off_type>());
    if (which == std::ios_base::in) {
      underflow();
    }
  }
  return result;
}

/*
 *  @name   seekpos
 *  @brief  C.f. C++ standard section 27.5.2.4.2
 */
PyStreambuf::pos_type PyStreambuf::seekpos(pos_type sp,
                                           std::ios_base::openmode which) {
  return base_t::seekoff(sp, std::ios_base::beg, which);
}

/*
 *  @name   seekoff_without_calling_python
 *  @brief  Seek into the underlying stream
 *  @param[in] offset Position to look for
 *  @param[in] way    Seek direction
 *  @param[in] which  How the stream is open
 *  @param[out] result  Found position
 *  @return True on success, false otherwise
 */
bool PyStreambuf::seekoff_without_calling_python(const off_type& offset,
                                                 const std::ios_base::seekdir& way,
                                                 const std::ios_base::openmode& which,
                                                 off_type* result) {
  // Buffer range and current position
  off_type buf_begin = 0, buf_end = 0, buf_cur = 0, upper_bound = 0;
  off_type pos_of_buffer_end_in_py_file = 0;
  if (which == std::ios_base::in) {
    // Stream open as input
    pos_of_buffer_end_in_py_file = pos_of_read_buffer_end_in_py_file_;
    buf_begin = reinterpret_cast<std::streamsize>(eback());
    buf_cur = reinterpret_cast<std::streamsize>(gptr());
    buf_end = reinterpret_cast<std::streamsize>(egptr());
    upper_bound = buf_end;
  } else if (which == std::ios_base::out) {
    // Stream open as output
    pos_of_buffer_end_in_py_file = pos_of_write_buffer_end_in_py_file_;
    buf_begin = reinterpret_cast<std::streamsize>(pbase());
    buf_cur = reinterpret_cast<std::streamsize>(pptr());
    buf_end = reinterpret_cast<std::streamsize>(epptr());
    farthest_pptr_ = std::max(farthest_pptr_, pptr());
    upper_bound = reinterpret_cast<std::streamsize>(farthest_pptr_) + 1;
  } else {
    throw std::runtime_error("Control flow passes through branch that should "
                             "be unreachable.");
  }
  // Sought position in "buffer coordinate"
  off_type buf_sought = 0;
  if (way == std::ios_base::cur) {
    // offset from current position
    buf_sought = buf_cur + offset;
  } else if (way == std::ios_base::beg) {
    // offset from begining position
    buf_sought = buf_end + (offset - pos_of_buffer_end_in_py_file);
  } else if (way == std::ios_base::end) {
    return false;
  } else {
    throw std::runtime_error("Control flow passes through branch that should "
                             "be unreachable.");
  }
  // if the sought position is not in the buffer, give up
  if (buf_sought < buf_begin || buf_sought >= upper_bound) {
    return false;
  }
  // we are in wonderland
  if (which == std::ios_base::in) {
    gbump(buf_sought - buf_cur);
  } else if (which == std::ios_base::out) {
    pbump(buf_sought - buf_cur);
  }
  *result = pos_of_buffer_end_in_py_file + (buf_sought - buf_end);
  return true;
}

/*
 *  @name   OStreamWrapper
 *  @fn     OStreamWrapper(py::object& obj,
 size_t buffer_size=PyStreambuf::default_buffer_size)
 *  @brief  Constructor
 *  @param[in] obj  Python file descriptor to wrap
 *  @param[in] buff_sz  Buffer size
 */
OStreamWrapper::OStreamWrapper(py::object& obj,
                               size_t buff_sz) :
        PyStreambufCapsule(obj, buff_sz),
        PyStreambuf::OStream(streambuf_) {
}

/*
 *  @name   ~OStreamWrapper
 *  @fn     ~OStreamWrapper() override
 *  @brief  Destructor
 */
OStreamWrapper::~OStreamWrapper() {
  if (this->good()) {
    this->flush();
  }
}

/*
 *  @name   IStreamWrapper
 *  @brief  Constructor
 *  @param[in] obj  Python file descriptor to wrap
 *  @param[in] buffer_size  Buffer size
 */
IStreamWrapper::IStreamWrapper(py::object& obj,
                               size_t buff_sz) :
        PyStreambufCapsule(obj, buff_sz),
        PyStreambuf::IStream(streambuf_) {}

/*
 *  @name   ~IStreamWrapper
 *  @brief  Destructor
 */
IStreamWrapper::~IStreamWrapper() {
  if (this->good()) {
    this->sync();
  }
}


#pragma mark -
#pragma mark Module

/*
 * @name  TestPrint
 * @brief Test case for ostream with flush
 * @param[in] o Ostream where to dump `testprint` string
 */
void IOStreamTest::Print(std::ostream& o) {
  o << "testprint" << std::endl;
}

/*
 * @name  TestPrintNoFlush
 * @brief Test case for ostream without flush
 * @param[in] o Ostream where to dump `testprint_noflush` string
 */
void IOStreamTest::PrintNoFlush(std::ostream& o) {
  o << "testprint_noflush";
}

/*
 * @name
 * @param[in] i Input stream
 * @return Value extracted from the stream
 */
int IOStreamTest::Parse(std::istream& i) {
  int result = 0;
  i >> result;
  //i.read(reinterpret_cast<char*>(&result), sizeof(result));
  return result;
}

/*
 * @name AddIOStreamTestCase
 * @brief Add IOStream testcase to the module
 * @param[in,out] module Python module where to add the testcase
 */
void AddIOStreamTestCase(py::module& module)  {
  // print with flust
  module.def("iostream_print",
             &IOStreamTest::Print,
             R"doc(Dump `testprint` into the given ostream)doc");
  // print without flust
  module.def("iostream_print_noflush",
             &IOStreamTest::PrintNoFlush,
             R"doc(Dump `testprint_noflush` into the given ostream without "
             "flushing it)doc");
  // Parse input stream
  module.def("iostream_parse",
             &IOStreamTest::Parse,
             R"doc(Extract an integer from the input stream and return it)doc");
}


};  // namespace Python
}  // namespace LTS5
