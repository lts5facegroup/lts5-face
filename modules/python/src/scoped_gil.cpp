/**
 *  @file   scoped_gil.cpp
 *  @brief  Wrapper for safely unblock Python threads
 *  @see https://stackoverflow.com/questions/8009613
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   05/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "Python.h"
#include "pystate.h"

#include "lts5/python/scoped_gil.hpp"
#include "lts5/python/py_message.hpp"

#pragma mark -
#pragma mark Release GIL

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/*
 * @name    InitThread
 * @fn  void LTS5_EXPORTS InitThread()
 * @brief Initialize python thread
 * @see https://stackoverflow.com/a/16609899/4546884
 */
void InitThread() {
  // Make sure the GIL has been created since we need to acquire it in our
  // callback to safely call into the python application.
  if (! PyEval_ThreadsInitialized()) {
    PyEval_InitThreads();
  }
}

/*
 * @name  ReleaseGIL
 * @fn    ReleaseGIL()
 * @brief Constructor
 */
ReleaseGIL::ReleaseGIL() : state_(nullptr) {
  state_ = PyEval_SaveThread();
}

/*
 * @name  ~ReleaseGIL
 * @fn    ~ReleaseGIL()
 * @brief Constructor
 */
ReleaseGIL::~ReleaseGIL() {
  if (state_) {
    PyEval_RestoreThread(state_);
    state_ = nullptr;
  }
}

#pragma mark -
#pragma mark Acquire GIL

/*
 * @name  AcquireGIL
 * @fn    AcquireGIL()
 * @brief Constructor
 */
AcquireGIL::AcquireGIL() : state_(0) {
  state_ = PyGILState_Ensure();
}

/*
 * @name  ~AcquireGIL
 * @fn    ~AcquireGIL()
 * @brief Constructor
 */
AcquireGIL::~AcquireGIL() {
  PyGILState_Release(static_cast<PyGILState_STATE>(state_));
}

}  // namepsace Python
}  // namepsace LTS5