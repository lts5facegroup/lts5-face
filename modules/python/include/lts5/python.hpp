/**
 *  @file   lts5/python.hpp
 *  @brief  Utility tools for python wrapper
 *
 *  @author Christophe Ecabert
 *  @date   05/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#include "lts5/python/python.hpp"
