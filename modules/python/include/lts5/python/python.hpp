/**
 *  @file   lts5/python/python.hpp
 *  @brief  Utility tools for python wrapper
 *
 *  @author Christophe Ecabert
 *  @date   05/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_PYTHON__
#define __LTS5_PYTHON__

/** Scoped GIL */
#include "lts5/python/scoped_gil.hpp"
/** OpenCV converter */
#include "lts5/python/ocv_converter.hpp"

#endif //__LTS5_PYTHON__
