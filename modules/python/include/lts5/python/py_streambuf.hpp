/**
 *  @file   py_streambuf.hpp
 *  @brief Python wrapper for steambuf
 *  @ingroup python
 *  @see  https://gist.github.com/asford/544323a5da7dddad2c9174490eb5ed06
 *
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  Copyright © 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_PY_STREAMBUF__
#define __LTS5_PY_STREAMBUF__

#include <cstdio>
#include <iostream>
#include <streambuf>
#include <memory>
#include <string>
#include <algorithm>

#include "pybind11/pybind11.h"

namespace py = pybind11;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 *  @class  PyStreambuf
 *  @brief  Streambuf wrapper for python object file descriptor
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  @ingroup python
 */
class PyStreambuf : public std::basic_streambuf<char, std::char_traits<char>> {
 private:
  /** Base class type */
  using base_t = std::basic_streambuf<char, std::char_traits<char>>;

 public:
#pragma mark -
#pragma mark Type definition

  /** Char type */
  using char_type = typename base_t::char_type;
  /** Integer type */
  using int_type = typename base_t::int_type;
  /** Position type */
  using pos_type = typename base_t::pos_type;
  /** Offset type */
  using off_type = typename base_t::off_type;
  /** Traits type */
  using traits_type = typename base_t::traits_type;

  /**
   *  @class  IStream
   *  @brief  Input stream python interface wrapper
   *  @author Christophe Ecabert
   *  @date   30.07.18
   *  @ingroup python
   */
  class IStream : public std::istream {
   public:
    /**
     *  @name   IStream
     *  @fn     IStream(PyStreambuf& buf)
     *  @brief  constructor
     *  @param[in] buf Python buffer to wrapper
     */
    explicit IStream(PyStreambuf& buf);

    /**
     *  @name   ~IStream
     *  @fn     ~IStream(void) override
     *  @brief  destructor
     */
    ~IStream() override;
  };

  /**
   *  @class  OStream
   *  @brief  Output stream python interface wrapper
   *  @author Christophe Ecabert
   *  @date   30.07.18
   *  @ingroup python
   */
  class OStream : public std::ostream {
   public:
    /**
     *  @name   OStream
     *  @fn     OStream(PyStreambuf& buf)
     *  @brief  constructor
     *  @param[in] buf Python buffer to wrapper
     */
    explicit OStream(PyStreambuf& buf);

    /**
     *  @name   ~OStream
     *  @fn     ~OStream(void) override
     *  @brief  destructor
     */
    ~OStream() override;
  };

#pragma mark -
#pragma mark Initialisation

  /** Default buffer size - 1024 */
  static constexpr size_t default_buffer_size = 1024;

  /**
   *  @name   PyStreambuf
   *  @fn     PyStreambuf(py::object& obj_file,
   *                      size_t buff_size = default_buffer_size)
   *  @brief  Constructor
   *  @param[in] obj_file   Python object file to use as input/output
   *  @param[in] buff_size  Buffer size
   */
  explicit PyStreambuf(py::object& obj_file,
                       size_t buff_size = default_buffer_size);

  /**
   *  @name   ~PyStreambuf
   *  @fn     virtual ~PyStreambuf(void)
   *  @brief  Destructor
   */
  ~PyStreambuf() override;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   showmanyc
   *  @fn     std::streamsize showmanyc() override
   *  @brief  It is essential to override this virtual function for the stream
   *          member function readsome to work correctly
   *          (c.f. 27.6.1.3, alinea 30)
   */
  std::streamsize showmanyc() override;

  /**
   *  @name   underflow
   *  @fn     int_type underflow() override
   *  @brief  C.f. C++ standard section 27.5.2.4.3
   */
  int_type underflow() override;

  /**
   *  @name   overflow
   *  @fn     int_type overflow(int_type c) overrie
   *  @brief  C.f. C++ standard section 27.5.2.4.5
   *  @param[in] c  char
   */
  int_type overflow(int_type c/*=traits_type::eof()*/) override;

  /**
   *  @name   sync
   *  @fn     int sync() override
   *  @brief  Empty the write buffer into the Python file object and set the
   *          seek position of the latter accordingly (C++ standard section 
   *          27.5.2.4.2). If there is no write buffer or it is empty, but 
   *          there is a non-empty read buffer, set the Python file object seek 
   *          position to the seek position in that read buffer.
   */
  int sync() override;

  /**
   *  @name   seekoff
   *  @fn     pos_type seekoff(off_type off,
                               std::ios_base::seekdir way,
                               std::ios_base::openmode which) override
   *  @brief  This implementation is optimised to look whether the position is 
   *          within the buffers, so as to avoid calling Python seek or tell. 
   *          It is important for many applications that the overhead of calling
   *          into Python is avoided as much as possible (e.g. parsers which 
   *          may do a lot of backtracking)
   *  @param[in] off  Offset
   *  @param[in] way  Which direction to search in
   *  @param[in] which Stream opening's mode
   */
  pos_type seekoff(off_type off,
                   std::ios_base::seekdir way,
                   std::ios_base::openmode which) override;

  /**
   *  @name   seekpos
   *  @fn     pos_type seekpos(pos_type sp,
                               std::ios_base::openmode which) override
   *  @brief  C.f. C++ standard section 27.5.2.4.2
   *  @param[in]  sp    Position to look for
   *  @param[in]  which Stream opening's mode
   */
  pos_type seekpos(pos_type sp, std::ios_base::openmode which) override;

#pragma mark -
#pragma mark Private

 private:
  /**
   *  @name   seekoff_without_calling_python
   *  @brief  Seek into the underlying stream
   *  @param[in] offset Position to look for
   *  @param[in] way    Seek direction
   *  @param[in] which  How the stream is open
   *  @param[out] result  Found position
   *  @return True on success, false otherwise
   */
  bool seekoff_without_calling_python(const off_type& offset,
                                      const std::ios_base::seekdir& way,
                                      const std::ios_base::openmode& which,
                                      off_type* result);
  /** Python stream object */
  py::object py_stream_;
  /** Python read function */
  py::object py_read_;
  /** Python write function */
  py::object py_write_;
  /** Python seek function */
  py::object py_seek_;
  /** Python tell function */
  py::object py_tell_;
  /** Buffer size */
  size_t buffer_size_;
  /** This is actually a Python bytes object and the actual read buffer is
   its internal data, i.e. an array of characters */
  py::bytes read_buffer_;
  /** A mere array of char's allocated on the heap at construction time and
   de-allocated only at destruction time */
  char* write_buffer_;
  /** Position of python read buffer */
  off_type pos_of_read_buffer_end_in_py_file_;
  /** Position of python write buffer */
  off_type pos_of_write_buffer_end_in_py_file_;
  /** the farthest place the buffer has been written into */
  char* farthest_pptr_;
  /** Indicate if stream required bytes-like or text-like input */
  bool bytes_like_;
};

#pragma mark -
#pragma mark IOStream Wrapper

/**
 *  @class  PyStreambufCapsule
 *  @brief  PyStreambuf capsue
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  @ingroup python
 */
class PyStreambufCapsule {
 public:
  /**
   * @name  PyStreambufCapsule
   * @fn PyStreambufCapsule(py::object& obj, size_t buff_sz)
   * @brief Constructor
   * @param[in] obj Python object file descriptor
   * @param[in] buff_sz Buffer size
   */
  PyStreambufCapsule(py::object& obj, size_t buff_sz) : streambuf_(obj,
                                                                   buff_sz) {}

  /**
   * @name  ~PyStreambufCapsule
   * @fn    ~PyStreambufCapsule() = default
   * @brief Destructor
   */
  ~PyStreambufCapsule() = default;

  /** PyStreambuf instance */
  PyStreambuf streambuf_;
};

/**
 *  @class  OStreamWrapper
 *  @brief  Wrapper for OStream class
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  @ingroup python
 */
class OStreamWrapper : private PyStreambufCapsule,
                       public PyStreambuf::OStream {
 public:
  /**
   *  @name   OStreamWrapper
   *  @fn     OStreamWrapper(py::object& obj, size_t buff_sz)
   *  @brief  Constructor
   *  @param[in] obj  Python file descriptor to wrap
   *  @param[in] buff_sz  Buffer size
   */
  OStreamWrapper(py::object& obj, size_t buff_sz);

  /**
   *  @name   ~OStreamWrapper
   *  @fn     ~OStreamWrapper() override
   *  @brief  Destructor
   */
  ~OStreamWrapper() override;
};

/**
 *  @class  IStreamWrapper
 *  @brief  Wrapper for IStream class
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  @ingroup python
 */
class IStreamWrapper : private PyStreambufCapsule,
                       public PyStreambuf::IStream {
 public:
  /**
   *  @name   IStreamWrapper
   *  @fn     IStreamWrapper(py::object& obj, size_t buff_sz)
   *  @brief  Constructor
   *  @param[in] obj  Python file descriptor to wrap
   *  @param[in] buff_sz  Buffer size
   */
  IStreamWrapper(py::object& obj, size_t buff_sz);

  /**
   *  @name   ~IStreamWrapper
   *  @fn     ~IStreamWrapper() override
   *  @brief  Destructor
   */
  ~IStreamWrapper() override;
};

#pragma mark -
#pragma mark Test module

/**
 *  @struct IOStreamTest
 *  @brief  Unit test for wrapper validation
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  @ingroup python
 */
struct IOStreamTest {
  /**
   * @name  TestPrint
   * @fn static void Print(std::ostream& o)
   * @brief Test case for ostream with flush
   * @param[in] o Ostream where to dump `testprint` string
   */
  static void Print(std::ostream& o);

  /**
   * @name  TestPrintNoFlush
   * @fn static void PrintNoFlush(std::ostream& o)
   * @brief Test case for ostream without flush
   * @param[in] o Ostream where to dump `testprint_noflush` string
   */
  static void PrintNoFlush(std::ostream& o);

  /**
   * @name Parse
   * @fn static int Parse(std::istream& i)
   * @param[in] i Input stream
   * @return Value extracted from the stream
   */
  static int Parse(std::istream& i);
};

/**
 * @name AddIOStreamTestCase
 * @brief Add IOStream testcase to the module
 * @ingroup python
 * @param[in,out] module Python module where to add the testcase
 */
void AddIOStreamTestCase(py::module& module);

};  // namespace Python
}  // namespace LTS5

#pragma mark -
#pragma mark Python converter

namespace pybind11 {
namespace detail {

/**
 *  @struct type_caster
 *  @brief  Partial spetialisation for `std::istream` pyhton converter
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  @ingroup python
 *  @tparam std::istream
 */
template<>
struct type_caster<std::istream> {
 public:
  /**
   * This macro establishes the name 'std::istream' in
   * function signatures and declares a local variable
   * 'value' of type std::istream
   */
  static constexpr auto name = _("std::istream");

  /**
   * @name  load
   * @fn    bool load(handle src, bool)
   * @brief Check if a python object can be converted to a std::istream.
   * @param[in] src Object to check and convert.
   * @return    True if conversion was successful, false otherwise
   */
  bool load(handle src, bool) {
    using IStream = LTS5::Python::IStreamWrapper;
    using PyStreambuf = LTS5::Python::PyStreambuf;
    // Check if PyObject can be converted
    if (getattr(src, "read", py::none()).is_none()) {
      return false;
    }
    // Convert
    obj_ = py::reinterpret_borrow<object>(src);  // Borrow reference
    stream_ = std::unique_ptr<IStream>(new IStream(obj_,
                                             PyStreambuf::default_buffer_size));
    return true;
  }

  /**
   * @name  cast
   * @brief Convert a stream to python object
   * @param[in] src Source stream to be converted
   * @param[in] policy  Conversion policy
   * @param[in] parent  Parent handle
   * @return Converted stream
   */
  static handle cast(const std::istream* src,
                     return_value_policy policy,
                     handle parent) {
    return none().release();
  }

  explicit operator std::istream*() {return stream_.get();}
  explicit operator std::istream&() {return *stream_;}

  template<typename T>
  using cast_op_type = pybind11::detail::cast_op_type<T>;

 protected:
  /** Python object */
  py::object obj_;
  /** IStreamWrapper instance */
  std::unique_ptr<LTS5::Python::IStreamWrapper> stream_;
};

/**
 *  @struct type_caster
 *  @brief  Partial spetialisation for `std::ostream` pyhton converter
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  @ingroup python
 *  @tparam std::ostream
 */
template<>
struct type_caster<std::ostream> {
 public:

  type_caster() {
    std::cout << "type_caster<std::ostream> CTOR" << std::endl;
  }
  ~type_caster() {
    std::cout << "type_caster<std::ostream> DTOR" << std::endl;
  }

  /**
   * This macro establishes the name 'std::ostream' in
   * function signatures and declares a local variable
   * 'value' of type std::ostream
   */
  static constexpr auto name = _("std::ostream");

  /**
   * @name  load
   * @fn    bool load(handle src, bool)
   * @brief Check if a python object can be converted to a std::ostream.
   * @param[in] src Object to check and convert.
   * @return    True if conversion was successful, false otherwise
   */
  bool load(handle src, bool) {
    using OStream = LTS5::Python::OStreamWrapper;
    using PyStreambuf = LTS5::Python::PyStreambuf;
    // Check if PyObject can be converted
    if (getattr(src, "write", py::none()).is_none()) {
      return false;
    }
    // Convert
    obj_ = py::reinterpret_borrow<object>(src);  // Borrow reference
    stream_ = std::unique_ptr<OStream>(new OStream(obj_,
                                            PyStreambuf::default_buffer_size));
    return true;
  }

  /**
   * @name  cast
   * @brief Convert a stream to python object
   * @param[in] src Source stream to be converted
   * @param[in] policy  Conversion policy
   * @param[in] parent  Parent handle
   * @return Converted stream
   */
  static handle cast(const std::istream* src,
                     return_value_policy policy,
                     handle parent) {
    return none().release();
  }

  explicit operator std::ostream*() {return stream_.get();}
  explicit operator std::ostream&() {
    return *stream_;
  }

  template<typename T>
  using cast_op_type = pybind11::detail::cast_op_type<T>;

 protected:
  /** Python object */
  py::object obj_;
  /** IStreamWrapper instance */
  std::unique_ptr<LTS5::Python::OStreamWrapper> stream_;
};


}  // namespace detail
}  // namespace pybind11



#endif /* __LTS5_PY_STREAMBUF__ */
