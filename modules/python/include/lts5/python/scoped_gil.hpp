/**
 *  @file   scoped_gil.hpp
 *  @brief  Wrapper for safely unblock Python threads
 *  @see https://stackoverflow.com/questions/8009613
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   05/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __SCOPED_GIL__
#define __SCOPED_GIL__

#include "lts5/utils/library_export.hpp"

/** Forward declare PyThread */
struct _ts;
typedef _ts PyThreadState;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    InitThread
 * @fn  void LTS5_EXPORTS InitThread()
 * @ingroup python
 * @brief Initialize python thread
 * @see https://stackoverflow.com/a/16609899/4546884
 */
void LTS5_EXPORTS InitThread();

#if 0
/**
 * @class   ReleaseGIL
 * @brief   Wrapper for safely unblock Python threads
 * @author  Christophe Ecabert
 * @date    05/07/2017
 * @ingroup python
 */
class LTS5_EXPORTS ReleaseGIL {
 public:

  /**
   * @name  ReleaseGIL
   * @fn    ReleaseGIL()
   * @brief Constructor
   */
  ReleaseGIL();

  /**
   * @name  ReleaseGIL
   * @fn    ReleaseGIL(const ReleaseGIL& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  ReleaseGIL(const ReleaseGIL& other) = delete;

  /**
   * @name  operator=
   * @fn    ReleaseGIL& operator=(const ReleaseGIL& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return Newly assigned object
   */
  ReleaseGIL& operator=(const ReleaseGIL& rhs) = delete;

  /**
   * @name  ~ReleaseGIL
   * @fn    ~ReleaseGIL()
   * @brief Destructor
   */
  ~ReleaseGIL();

 private:
  /** Python thread state */
  PyThreadState* state_;
};

/**
 * @class   AcquireGIL
 * @brief   Wrapper for safely acquire Python threads
 * @author  Christophe Ecabert
 * @date    05/07/2017
 * @ingroup python
 */
class LTS5_EXPORTS AcquireGIL {
 public:

  /**
   * @name  AcquireGIL
   * @fn    AcquireGIL()
   * @brief Constructor
   */
  AcquireGIL();

  /**
   * @name  AcquireGIL
   * @fn    AcquireGIL(const AcquireGIL& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  AcquireGIL(const AcquireGIL& other) = delete;

  /**
   * @name  operator=
   * @fn    AcquireGIL& operator=(const AcquireGIL& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return Newly assigned object
   */
  AcquireGIL& operator=(const AcquireGIL& rhs) = delete;

  /**
   * @name  ~AcquireGIL
   * @fn    ~AcquireGIL()
   * @brief Destructor
   */
  ~AcquireGIL();

 private:
  /** Python GIL state */
  int state_;
};
#endif

}  // namepsace Python
}  // namepsace LTS5
#endif //__SCOPED_GIL__
