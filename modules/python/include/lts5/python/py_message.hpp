/**
 *  @file   py_message.hpp
 *  @brief  Wrapper to write into python console
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   05/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __PY_MESSAGE__
#define __PY_MESSAGE__

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    PyErrorMessage
 * @fn  void PyErrorMessage(const char* frmt, ...)
 * @brief   Write into python console
 * @ingroup python
 * @param[in] frmt  Message format
 * @param[in] ...   Message content
 */
void LTS5_EXPORTS PyErrorMessage(const char* frmt, ...);

}  // namepsace Python
}  // namepsace LTS5

#endif //__PY_MESSAGE__
