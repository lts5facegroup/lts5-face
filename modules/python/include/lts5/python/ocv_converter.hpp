/**
 *  @file   ocv_converter.hpp
 *  @brief  Convert numpy array into opencv matrix
 *  @see https://github.com/yati-sagade/opencv-ndarray-conversion/
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   05/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __OCV_CONVERTER__
#define __OCV_CONVERTER__

#include "opencv2/core.hpp"
#include "pybind11/pybind11.h"

#include "lts5/utils/library_export.hpp"
#include "lts5/python/numpy_array.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @class   NDArrayConverter
 * @brief   Convert numpy array into opencv matrix
 * @author  Yati Sagade
 * @date    05/07/2017
 * @ingroup python
 * @see https://github.com/yati-sagade/opencv-ndarray-conversion/
 */
class NDArrayConverter {
 public:

  /**
   * @name  toRect
   * @fn    static cv::Rect toRect(PyObject* o)
   * @brief Convert a python object into a cv::Rect
   * @param[in] o   Python object
   * @return    Converted rectangle
   */
  static cv::Rect toRect(PyObject* o);

  /**
   * @name  toMat
   * @fn    cv::Mat toMat(const PyObject* o)
   * @brief Convert python object into opencv mat
   * @param[in] o   Python object to convert
   * @return    Converted opencv matrix
   */
  static cv::Mat toMat(PyObject* o);

  /**
   * @name  toPyObject
   * @fn    static PyObject* toPyObject(const cv::Rect& r)
   * @brief Convert cv::Rect into python object
   * @param[in] r Rectangle to convert
   * @return    Converted rectangle
   */
  static PyObject* toPyObject(const cv::Rect& r);

  /**
   * @name  toNDArray
   * @fn    PyObject* toNDArray(const cv::Mat& mat)
   * @brief Convert opencv matrix into python object
   * @param[in] mat Matrix to convert
   * @return    Converted matrix
   */
  static PyObject* toNDArray(const cv::Mat& mat);
};

#pragma mark -
#pragma mark Test module

/**
 *  @struct OCVMatConverterTest
 *  @brief  Unit test for wrapper validation
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  @ingroup python
 */
struct OCVMatConverterTest {

  /**
   * @name  PrintMatrix
   * @fn static void PrintMatrix(const cv::Mat& mat)
   * @brief Dump matrix into `std::cout`
   * @param[in] mat
   * @return String representation
   */
  static std::string PrintMatrix(const cv::Mat& mat);

  /**
   * @name  PrintRect
   * @fn static std::string PrintRect(const cv::Rect& rect)
   * @brief Dump rectangle into `std::cout`
   * @param[in] rect    Rectangle
   * @return String representation
   */
  static std::string PrintRect(const cv::Rect& rect);

  /**
   * @name  CreateMatrix
   * @fn    static cv::Mat CreateMatrix()
   * @brief Create an opencv matrix
   * @return    Opencv matrix
   */
  static cv::Mat CreateMatrix();

  /**
   * @name  CreateRect
   * @fn    static cv::Rect CreateRect()
   * @brief Create an opencv rectangle
   * @return    Opencv rectangle
   */
  static cv::Rect CreateRect();
};
  
#pragma mark - 
#pragma mark Module definition

/**
 * @name AddOCVMatConverterTestCase
 * @brief Add OCVMatConverter testcase to the module
 * @ingroup python
 * @param[in,out] module Python module where to add the testcase
 */
void AddOCVMatConverterTestCase(pybind11::module& module);

}  // namepsace Python
}  // namepsace LTS5

#pragma mark -
#pragma mark Python converter

namespace pybind11 {
namespace detail {

/**
 *  @struct type_caster
 *  @brief  Partial spetialisation for `cv::Rect` pyhton converter
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  @ingroup python
 *  @tparam cv::Rect
 */
template<>
struct type_caster<cv::Rect> {
 public:

  /**
   * This macro establishes the name 'inty' in
   * function signatures and declares a local variable
   * 'value' of type inty
   */
  PYBIND11_TYPE_CASTER(cv::Rect, _("cv::Rect"));

  /**
   * @name  load
   * @fn bool load(handle src, bool)
   * @brief Convert python object to `cv::Rect` object.
   * @param[in] src Python object
   * @return    True if success, false otherwise
   */
  bool load(handle src, bool) {
    // Is `src` convertible, extract PyObject from handle
    PyObject* source = src.ptr();
    if (!source || source == Py_None) {
      return false;
    }
    // Yes
    value = LTS5::Python::NDArrayConverter::toRect(source);
    return true;
  }

  /**
   * @name  cast
   * @fn static handle cast(const cv::Rect& src, return_value_policy policy,
                            handle parent)
   * @brief Convert from c++ to python object
   * @param[in] src Object to convert
   * @param[in] policy  Policy
   * @param[in] parent  Parent
   * @return Python object
   */
  static handle cast(const cv::Rect& src,
                     return_value_policy policy,
                     handle parent) {
    return LTS5::Python::NDArrayConverter::toPyObject(src);
  }
};

/**
 *  @struct type_caster
 *  @brief  Partial spetialisation for `cv::Mat` pyhton converter
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  @ingroup python
 *  @tparam cv::Mat
 */
template<>
struct type_caster<cv::Mat> {
 public:

  /**
   * This macro establishes the name 'inty' in
   * function signatures and declares a local variable
   * 'value' of type inty
   */
  PYBIND11_TYPE_CASTER(cv::Mat, _("cv::Mat"));

  /**
   * @name  load
   * @fn bool load(handle src, bool)
   * @brief Convert python object to `cv::Mat` object.
   * @param[in] src Python object
   * @return    True if success, false otherwise
   */
  bool load(handle src, bool) {
    // Is `src` convertible, extract PyObject from handle
    PyObject* source = src.ptr();
    if (!PyArray_Check(source)) {
      return false;
    }
    // Yes
    value = LTS5::Python::NDArrayConverter::toMat(source);
    return true;
  }

  /**
   * @name  cast
   * @fn static handle cast(const cv::Mat& src,
          return_value_policy policy,
          handle parent)
   * @brief Convert from c++ to python object
   * @param[in] src Object to convert
   * @param[in] policy  Policy
   * @param[in] parent  Parent
   * @return Python object
   */
  static handle cast(const cv::Mat& src,
          return_value_policy policy,
          handle parent) {
    return LTS5::Python::NDArrayConverter::toNDArray(src);
  }
};
}
}


#endif //__OCV_CONVERTER__

