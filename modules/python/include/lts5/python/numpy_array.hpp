/**
 *  @file   numpy_array.hpp
 *  @brief  Initialize numpy array
 *  @see https://stackoverflow.com/a/31973355/4546884
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   6/5/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_NUMPY_ARRAY__
#define __LTS5_NUMPY_ARRAY__

#include <iostream>

// https://stackoverflow.com/a/31973355/4546884
#ifndef LTS5_FACE_USE_IMPORT
#define NO_IMPORT
#endif

#define PY_ARRAY_UNIQUE_SYMBOL LTS5_FACE_ARRAY_API
#define PY_UFUNC_UNIQUE_SYMBOL LTS5_FACE_UFUNC_API

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include "numpy/npy_3kcompat.h"
#include "numpy/arrayobject.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name  InitNumpyArray
 * @fn    void InitNumpyArray()
 * @ingroup python
 * @brief Initialize numpy array module
 */
void InitNumpyArray();

}  // namespace Python
}  // namespace LTS5
#endif //__LTS5_NUMPY_ARRAY__
