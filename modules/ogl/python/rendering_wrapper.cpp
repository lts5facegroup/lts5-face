/**
 *  @file   rendering_wrapper.cpp
 *  @brief  
 *  @ingroup 
 *
 *  @author Christophe Ecabert
 *  @date   1/20/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <cmath>
#include "pybind11/stl.h"

#include "include/rendering_wrapper.hpp"
#include "lts5/ogl/offscreen_renderer.hpp"
#include "lts5/python/ocv_converter.hpp"
#include "math_converter.hpp"

namespace py = pybind11;
using namespace pybind11::literals; // to bring in the `_a` literal

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

using ShdType = OGLShader::ShaderType;
using Attr = OGLProgram::Attributes;
using AttrType = OGLProgram::AttributeType;

template<typename>
struct TypeToStr {};

template<>
struct TypeToStr<float> {
  static constexpr const char* type = "Float";
};

template<>
struct TypeToStr<double> {
  static constexpr const char* type = "Double";
};

std::string ShaderEnumToString(const ShdType& s) {
  switch (s) {
    case ShdType::kVertex: return "kVertex";
    case ShdType::kGeometry: return "kGeometry";
    case ShdType::kFragment: return "kFragment";
    default: return "kUnknown";
  }
}

std::string AttributeEnumToString(const AttrType& attr) {
  switch (attr) {
    case AttrType::kPoint: return "kPoint";
    case AttrType::kNormal: return "kNormal";
    case AttrType::kColor: return "kColor";
    case AttrType::kTexCoord: return "kTexCoord";
    default: return "kUnknown";
  }
}

/**
 * @name ExposeOGLFramework
 * @brief Expose OpenGL framework to python
 * @param[in,out] m Python module where to expose class
 * @tparam T    Data type
 */
void ExposeOGLFramework(py::module& m) {


  // OGLShader
  // ------------------------------------------------------------------
  py::class_<OGLShader> py_shader(m,
                                  "Shader",
                                  R"doc(Basic OpenGL shader)doc");
  py_shader.def(py::init<const std::string&,const OGLShader::ShaderType&>(),
                "code"_a,
                "type"_a,
                R"doc(Constructor
> code: Shader code
> type: Type of shader (i.e. vertex/geometry/fragment))doc");

  // Enum
  // See: https://pybind11.readthedocs.io/en/stable/classes.html#enumerations-and-internal-types
  py::enum_<ShdType>(py_shader, "Type")
          .value("kVertex", ShdType::kVertex)
          .value("kGeometry", ShdType::kGeometry)
          .value("kFragment", ShdType::kFragment)
          .export_values();

  // Attributes
  // ------------------------------------------------------------------
  py::class_<Attr> py_attr(m,
                           "Attributes",
                           R"doc(Properties for shader attributes.
This class hold informations defined in shader's code. More specifically it
stores the `name` and `location` for a given attribute.)doc");
  py_attr.def(py::init<const AttrType&, const unsigned int&, const std::string&>(),
              "type"_a,
              "location"_a,
              "name"_a,
              R"doc(Constructor
> type: Type of attribute
> location: Location of the attribute
> name: Name of the attribute)doc")
         .def_property_readonly("type",
                                [](const Attr& attr) {
                                  return attr.type;
                                },
                                R"doc(Type of attribute)doc")
         .def_property_readonly("location",
                                [](const Attr& attr) {
                                  return attr.location;
                                },
                                R"doc(Location of attribute)doc")
         .def_property_readonly("name",
                                [](const Attr& attr) {
                                  return attr.name;
                                },
                                R"doc(Name of attribute)doc")
         .def("__repr__",
              [](const Attr& a) {
                auto r = "Type: " + AttributeEnumToString(a.type) +
                         ", location: " + std::to_string(a.location) +
                         ", name: " + a.name;
                return r;
              });

  // Enum
  // See: https://pybind11.readthedocs.io/en/stable/classes.html#enumerations-and-internal-types
  py::enum_<AttrType>(py_attr, "Type")
          .value("kPoint", AttrType::kPoint)
          .value("kNormal", AttrType::kNormal)
          .value("kTexCoord", AttrType::kTexCoord)
          .value("kColor", AttrType::kColor)
          .export_values();

  // Program
  // ------------------------------------------------------------------
  using Attrib1f = void (OGLProgram::*) (const char*, float) const;
  using Attrib1i = void (OGLProgram::*) (const char*, int) const;
  using UnifVec2f = void(OGLProgram::*) (const char*, const Vector2<float>&) const;
  using UnifVec3f = void(OGLProgram::*) (const char*, const Vector3<float>&) const;
  using UnifVec4f = void(OGLProgram::*) (const char*, const Vector4<float>&) const;
  using UnifMat2f = void(OGLProgram::*) (const char*, const Matrix2x2<float>&, bool) const;
  using UnifMat3f = void(OGLProgram::*) (const char*, const Matrix3x3<float>&, bool) const;
  using UnifMat4f = void(OGLProgram::*) (const char*, const Matrix4x4<float>&, bool) const;

  using Mesh_3f = Mesh<float, Vector3<float>>;
  using Mesh_3d = Mesh<double, Vector3<double>>;


  py::class_<OGLProgram> py_prog(m,
                                 "Program",
                                 R"doc(OpenGL program encapsulating at
least two shaders)doc");
  py_prog.def(py::init<const std::vector<OGLShader>&,
                      const std::vector<Attr>&>(),
          "shaders"_a,
          "attributes"_a,
          R"doc(Constructor:
> shaders: List of shader instance
> attributes: List of attributes
)doc")
         .def(py::init(&OGLProgram::FromMesh<Mesh_3f>),
                 "mesh"_a,
                 R"doc(Create a shading program from a given mesh file.
> mesh: Mesh object to generate the shading program for)doc" )
         .def(py::init(&OGLProgram::FromMesh<Mesh_3d>),
              "mesh"_a,
              R"doc(Create a shading program from a given mesh file.
> mesh: Mesh object to generate the shading program for)doc" )
         .def("use",
              &OGLProgram::Use,
              R"doc(Start using this shading program)doc")
         .def("stop_using",
              &OGLProgram::StopUsing,
              R"doc(Stop using this shading program)doc")
         .def("is_valid",
              &OGLProgram::IsValid,
              R"doc(Check if program is valid or not.)doc")
         .def("is_using",
              &OGLProgram::IsUsing,
              R"doc(Indicate if the current program is being used)doc")
         .def("attribute",
              &OGLProgram::Attrib,
              "name"_a,
              R"doc(Provide the attribute index for a given `name`
> name: Name of the attribute to look for)doc")
         .def("uniform",
              &OGLProgram::Uniform,
              "name"_a,
              R"doc(Provide the uniform reference for a given `name`
> name: Name of the uniform to look for)doc")
         .def("set_attrib_1f",
              (Attrib1f)&OGLProgram::SetAttrib,
              "name"_a,
              "v0"_a,
              R"doc(Set a given value `v0` to an attribute.
> name: Name of the attribute to set
> v0: Float value to set)doc")
         .def("set_attrib_1i",
              (Attrib1i)&OGLProgram::SetAttrib,
              "name"_a,
              "v0"_a,
              R"doc(Set a given value `v0` to an attribute.
> name: Name of the attribute to set
> v0: Integer value to set)doc")

         .def("set_uniform_vec2f",
              (UnifVec2f)&OGLProgram::SetUniform<float>,
              "name"_a,
              "value"_a,
              R"doc(Setup uniform value of type vec2 float.
> name: Name of the uniform to set
> value: Vector value)doc")
         .def("set_uniform_vec3f",
              (UnifVec3f)&OGLProgram::SetUniform<float>,
              "name"_a,
              "value"_a,
              R"doc(Setup uniform value of type vec3 float.
> name: Name of the uniform to set
> value: Vector value)doc")
         .def("set_uniform_vec4f",
              (UnifVec4f)&OGLProgram::SetUniform<float>,
              "name"_a,
              "value"_a,
              R"doc(Setup uniform value of type vec4 float.
> name: Name of the uniform to set
> value: Vector value)doc")

         .def("set_uniform_mat2f",
              (UnifMat2f)&OGLProgram::SetUniform<float>,
              "name"_a,
              "value"_a,
              "transpose"_a,
              R"doc(Setup uniform value of type Mat2 float.
> name: Name of the uniform to set
> value: Matrix 2x2
> transpose: Bool, indicate if matrix needs to be transposed)doc")
         .def("set_uniform_mat3f",
              (UnifMat3f)&OGLProgram::SetUniform<float>,
              "name"_a,
              "value"_a,
              "transpose"_a,
              R"doc(Setup uniform value of type Mat3 float.
> name: Name of the uniform to set
> value: Matrix 3x3
> transpose: Bool, indicate if matrix needs to be transposed)doc")
         .def("set_uniform_mat4f",
              (UnifMat4f)&OGLProgram::SetUniform<float>,
              "name"_a,
              "value"_a,
              "transpose"_a,
              R"doc(Setup uniform value of type Mat4 float.
> name: Name of the uniform to set
> value: Matrix 4x4
> transpose: Bool, indicate if matrix needs to be transposed)doc")

         .def_property_readonly("error_message",
                                [](const OGLProgram& p) {
                                  return p.get_error_message();
                                },
                                R"doc(Provide OpenGL error message)doc");
}

/**
 * @name ExposeTypedOGLFramework
 * @brief Expose OpenGL framework to python
 * @param[in,out] m Python module where to expose class
 * @tparam T    Data type
 */
template<typename T>
void ExposeTypedOGLFramework(py::module& m) {
  using Vec3 = Vector3<T>;
  using Rend = OGLOffscreenRenderer<T, Vector3<T>>;
  using ImType = typename Rend::ImageType;
  using BuffInfo = typename Rend::BufferInfo;
  using MeshType = typename Rend::Mesh;
  using PrimType = typename Rend::PrimitiveType;
  using ShaderType = OGLProgram;

  // Camera
  // ------------------------------------------------------------------
  using Cam_t = OGLCamera<T>;
  using look_at_t = void(Cam_t::*)(const Vec3&, const Vec3&);
  using pos_getter = const Vec3&(Cam_t::*)() const;

  auto name = "Camera" + std::string(TypeToStr<T>::type);
  py::class_<Cam_t> py_cam(m,
                           name.c_str(),
                           R"doc(OpenGL camera abstraction)doc");
  py_cam.def(py::init<const int&, const int&>(),
             "width"_a,
             "height"_a,
             R"doc(Constructor
> width: Image width
> height: Image height)doc")
        .def("look_at",
             (look_at_t)&Cam_t::LookAt,
             "position"_a,
             "target"_a,
             R"doc(Orients the camera so that it is directly facing a
given target.
> position: Location of the camera
> target: Where to look at)doc")

                // Properties
        .def_property("position",
                      (pos_getter)&Cam_t::get_position,
                      &Cam_t::set_position,
                      R"doc(Camera position)doc")
        .def_property("field_of_view",
                      &Cam_t::get_field_of_view,
                      &Cam_t::set_field_of_view,
                      R"doc(Camera field of view in degree)doc")
        .def_property("near",
                      &Cam_t::get_near_plane,
                      [](Cam_t& cam, const T& near) {
                        cam.set_planes(near, cam.get_far_plane());
                      },
                      R"doc(Camera near plane)doc")
        .def_property("far",
                      &Cam_t::get_far_plane,
                      [](Cam_t& cam, const T& far) {
                        cam.set_planes(cam.get_near_plane(), far);
                      },
                      R"doc(Camera far plane)doc")
        .def_property_readonly("view",
                               [](Cam_t& cam) {
                                 return cam.view(false);
                               },
                               R"doc(Translation and rotation transform)doc")
        .def_property_readonly("projection",
                               &Cam_t::projection,
                               R"doc(Perspective projection transform)doc")
        .def_property_readonly("matrix",
                               &Cam_t::matrix,
                               R"doc(The combined camera transformation
matrix, including perspective projection)doc");

  // RenderingComponent
  // ------------------------------------------------------------------
  using BaseRend = OGLBaseRenderer<T, Vector3<T>>;
  using RendComp = typename BaseRend::RenderingComponent;

  name = "RenderingComponent" + std::string(TypeToStr<T>::type);
  py::class_<RendComp> py_rcomp(m,
                                name.c_str(),
                                R"doc(Basic holder for mesh rendering.
It include the following elements: Mesh, Camera, Shading program and a
texture (opt))doc");

  py_rcomp.def(py::init<const int&, const int&>(),
               "width"_a,
               "height"_a,
               R"doc(Constructor
> width: Window width to render in
> height: Window height to render in)doc")
          .def(py::init<const std::string&, const int&, const int&>(),
               "path"_a,
               "width"_a,
               "height"_a,
               R"doc(Constructor
> path: Path to mesh file
> width: Window width to render in
> height: Window height to render in)doc")
          .def(py::init<const MeshType&, const ShaderType&, const Cam_t&>(),
                  "mesh"_a,
                  "shader"_a,
                  "camera"_a,
                  R"doc(Constructor
> mesh: Mesh object
> shader: Shading program
> camera: Camera object)doc")
          .def("init",
               &RendComp::Init,
               "path"_a,
               R"doc(Initialize internal elements for a given mesh
> path: Path to the mesh object.
Return '-1' if something went wrong while loading file, otherwise '0'.)doc");

  // Renderer
  // ------------------------------------------------------------------
  using bind_info_t = int(Rend::*)(const BuffInfo&);
  using bind_rcomp_t = int(Rend::*)(const RendComp&);
  using render_all_t = void(Rend::*)();
  using render_t = void(Rend::*)(const PrimType&, const size_t&, const size_t&);

  name = std::string("OGLRenderer") + std::string(TypeToStr<T>::type);
  py::class_<Rend> py_rend(m,
                           name.c_str(),
                           R"doc(OpenGL offscreen renderer)doc");
  py_rend.def(py::init<>(),
              R"doc(Default constructor)doc")
         .def("init",
                 &Rend::Init,
              "width"_a,
              "height"_a,
              "m_sample"_a,
              "visible"_a,
              "type"_a,
              R"doc(Initialize offscreen renderer (FBO + Buffer)
> width: Width of the image
> height: Height of the image
> m_sample: Multisampling factor for anti-aliasing. If set to `0`, feature is
 disabled
> visible: If `True` rendering window is displayed on the screen as well. Imply
 two rendering passes.
> type: Type of offscreen buffer
)doc")
         .def("bind",
              (bind_info_t)&Rend::BindToOpenGL,
              "infos"_a,
              R"doc(Upload data into OpenGL context
> infos: Mesh attributes informations)doc")
         .def("bind",
              (bind_rcomp_t)&Rend::BindToOpenGL,
              "component"_a,
              R"doc(Upload data into OpenGL context
> component: Rendering component object)doc")
         .def("enable",
              &Rend::ActiveContext,
              R"doc(Enable OpenGL context)doc")
         .def("disable",
              &Rend::DisableContext,
              R"doc(Disable OpenGL context)doc")
         .def("render",
              (render_all_t)&Rend::Render,
              R"doc(Render into OpenGL context)doc")
         .def("render",
              (render_t)&Rend::Render,
              "type"_a,
              "count"_a,
              "offset"_a,
              R"doc(Render part of/all the mesh into OpenGL context
> type: Type of primitive (i.e. Triangle/Edge)
> count: Number of elements to draw
> offset: Where to start in the element buffer (VBO))doc")
         .def("image",
              [](const Rend& rend) {
                cv::Mat m;
                rend.GetImage(&m);
                if (!m.empty()) {
                  cv::flip(m, m, 0);
                }
                return m;
              }, R"doc(Get last rendered image)doc")
         .def("depth",
              [](const Rend& rend) {
                cv::Mat m;
                rend.GetDepth(&m);
                if (!m.empty()) {
                  cv::flip(m, m, 0);
                }
                return m;
              }, R"doc(Get depth buffer from last rendered image)doc")
         .def("set_program",
              &Rend::set_program,
              "program"_a,
              R"doc(Set shading program to use)doc")
         .def("set_clear_color",
              [](Rend& rend, py::tuple& color) {
                if (color.size() > 2) {
                  T r = color[0].cast<T>();
                  T g = color[1].cast<T>();
                  T b = color[2].cast<T>();
                  T a =  color.size() == 3 ? T(1.0) : color[2].cast<T>();
                  rend.set_clear_color(r, g, b, a);
                }
              },
              "color"_a,
              R"doc(Define background color
> color: Tuple, rgba value)doc")
          ;

  // Buffer type
  py::class_<BuffInfo>(py_rend,
                       "BufferInfo",
                       R"doc(Hold information about the size/data to push into
OpenGL buffers)doc")
          .def(py::init<MeshType>(),
               "mesh"_a,
               R"doc(Constructor
> mesh: Mesh instance to initialize from)doc")
          .def("__repr__",
               [](const BuffInfo& info) {
                 std::stringstream stream;
                 stream << info << std::endl;
                 return stream.str();
               },
               R"doc(BufferInfo string representation)doc");

  // Image type
  py::enum_<ImType>(py_rend, "ImageType")
          .value("Grayscale image",
                  ImType::kGrayscale,
                  R"doc(Grayscale image)doc")
          .value("kRGB",
                 ImType::kRGB,
                 R"doc(Color image (3 channels))doc")
          .value("kRGBf",
                 ImType::kRGBf,
                 R"doc(Color image (3 float channels))doc")
          .export_values();

  // PrimitiveType
  py::enum_<PrimType>(py_rend, "PrimitiveType")
          .value("kTriangle",
                 PrimType::kOglTriangle,
                 R"doc(Triangle primitive)doc")
          .value("kEdge",
                 PrimType::kOglEdge,
                 R"doc(Edge primitive)doc")
          .export_values();
}


/**
 * @name    ExposeRendering
 * @fn  void ExposeRendering(pybind11::module& m)
 * @brief Expose rendering framework into python
 * @param[in,out] m Python module where to add the SHViewer
 */
void ExposeRendering(pybind11::module& m) {
  ExposeOGLFramework(m);                // Un-typed object
  ExposeTypedOGLFramework<float>(m);    // Typed object
  ExposeTypedOGLFramework<double>(m);
}

}  // namespace Python
}  // namespace LTS5
