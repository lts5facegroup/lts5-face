/**
 *  @file   sh_viewer_wrapper.hpp
 *  @brief  Spherical harmonic viewer wrapper
 *  @ingroup pyhton
 *
 *  @author Christophe Ecabert
 *  @date   9/28/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_SH_VIEWER_WRAPPER__
#define __LTS5_SH_VIEWER_WRAPPER__

#include "pybind11/pybind11.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    ExposeSHViewer
 * @fn  void ExposeSHViewer(pybind11::module& m)
 * @brief Expose SHViewer into python
 * @param[in,out] m Python module where to add the SHViewer
 */
void ExposeSHViewer(pybind11::module& m);


}  // namespace Python
}  // namespace LTS5
#endif  // __LTS5_SH_VIEWER_WRAPPER__
