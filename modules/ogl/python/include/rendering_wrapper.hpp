/**
 *  @file   rendering_wrapper.hpp
 *  @brief  Expose rendering framework
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   20/01/2020
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_RENDERING_WRAPPER__
#define __LTS5_RENDERING_WRAPPER__

#include "pybind11/pybind11.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    ExposeRendering
 * @fn  void ExposeRendering(pybind11::module& m)
 * @brief Expose rendering framework into python
 * @param[in,out] m Python module where to add the SHViewer
 */
void ExposeRendering(pybind11::module& m);


}  // namespace Python
}  // namespace LTS5
#endif  // __LTS5_RENDERING_WRAPPER__
