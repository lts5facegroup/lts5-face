/**
 *  @file   ogl_wrapper.cpp
 *  @brief  Python wrapper for OGL modules
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   9/28/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <cmath>

#include "pybind11/pybind11.h"

#include "lts5/python/ocv_converter.hpp"

#include "include/rendering_wrapper.hpp"
#include "include/sh_viewer_wrapper.hpp"

namespace py = pybind11;

PYBIND11_MODULE(pyogl, m) {

  // Init numpy
  // LTS5::Python::InitThread();
  LTS5::Python::InitNumpyArray();

  // Doc
  m.doc() = R"doc(LTS5 OpenGL modules)doc";

  LTS5::Python::ExposeRendering(m);
  LTS5::Python::ExposeSHViewer(m);
}