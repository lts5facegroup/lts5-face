/**
 *  @file   sh_view_wrapper.cpp
 *  @brief  Spherical harmonic viewer wrapper
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   9/28/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <cmath>

#include "include/sh_viewer_wrapper.hpp"

#include "lts5/python/ocv_converter.hpp"
#include "lts5/ogl/sh_viewer.hpp"

namespace py = pybind11;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    ExposeSHViewer
 * @fn  void ExposeSHViewer(pybind11::module& m)
 * @brief Expose SHViewer into python
 * @param[in,out] m Python module where to add the SHViewer
 */
void ExposeSHViewer(pybind11::module& m) {
  using SHV = SHViewer<float>;
  // Define "python class"
  py::class_<SHV>(m,
                  "SHViewer",
                  R"doc(Spherical Harmonics Lighting visualisation tools)doc")
          .def(py::init<int, int, int>(),
               py::arg("width") = 200,
               py::arg("height") = 200,
               py::arg("step") = 50,
               R"doc(Create a Spherical Harmonic Viewer)doc")
          .def("generate",
               [](SHV& viewer, const cv::Mat& aldebo, const cv::Mat& sh_coef) {
                 cv::Mat img;
                 int err = viewer.Generate(aldebo, sh_coef, &img);
                 return py::make_tuple(err, img);
               },
               py::arg("albedo"),
               py::arg("sh_coef"),
               R"doc(Generate an image representation the estimated lighting scheme rendered on a sphere)doc")
          .def_property("background_color",
                  nullptr,
                  [](SHV& self, const py::tuple& rgb) {
                    auto r = rgb[0].cast<float>();
                    auto g = rgb[1].cast<float>();
                    auto b = rgb[2].cast<float>();
                    self.set_background_color(r, g, b);
                  },
          R"doc(Set background color from an RGB tuple)doc")
          .def_property_readonly("dims",
                                 &SHV::dims,
                                 R"doc(Indicate how many vertices are in the sphere)doc");
}

}  // namespace Python
}  // namespace LTS5