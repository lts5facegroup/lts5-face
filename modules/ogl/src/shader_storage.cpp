/**
 *  @file   shader_storage.cpp
 *  @brief  Implement interface for Shader Storage Buffer Object
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   7/9/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#include <algorithm>

#include "GL/glew.h"

#include "lts5/ogl/shader_storage.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/**
 * @name  OGLShaderStorageBuffer
 * @fn    OGLShaderStorageBuffer()
 * @brief Constructor
 */
OGLShaderStorageBuffer::OGLShaderStorageBuffer() : buffer_(0) {
  glGenBuffers(1, &buffer_);
}

/**
 * @name  ~OGLShaderStorageBuffer
 * @fn    ~OGLShaderStorageBuffer()
 * @brief Destructor
 */
OGLShaderStorageBuffer::~OGLShaderStorageBuffer() {
  if (buffer_ > 0) {
    glDeleteBuffers(1, &buffer_);
  }
}

/*
 * @name  OGLShaderStorageBuffer
 * @fn    OGLShaderStorageBuffer(OGLShaderStorageBuffer&& other) = default
 * @brief Move constructor
 * @param[in] other Object to move from
 */
OGLShaderStorageBuffer::
OGLShaderStorageBuffer(OGLShaderStorageBuffer&& other) noexcept : buffer_(0) {
  *this = std::move(other);
}

/*
 * @name  operator=
 * @fn    OGLShaderStorageBuffer& operator=(OGLShaderStorageBuffer&& rhs)
 * @brief Move assignment
 * @param[in] rhs Object to assign from
 * @return    Newly assigned object
 */
OGLShaderStorageBuffer&
OGLShaderStorageBuffer::operator=(OGLShaderStorageBuffer&& rhs) noexcept {
  if (this != &rhs) {
    // Release current buffer
    if (buffer_) {
      glDeleteBuffers(1, &buffer_);
    }
    // Take ownership of other
    buffer_ = rhs.buffer_;
    // Set other to 0 to avoid double delete
    rhs.buffer_ = 0;
  }
  return *this;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Bind
 * @fn    void Bind() const
 * @brief Bind the underlying buffer
 * @see  glBindBuffer
 */
void OGLShaderStorageBuffer::Bind() const {
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer_);
}

/*
 * @name  Bind
 * @fn    void Bind(const int& index) const
 * @brief Bind the underlying buffer at a given binding index.
 * @see  glBindBufferBase
 */
void OGLShaderStorageBuffer::BindBufferBase(const int& index) const {
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, index, buffer_);
}

/*
 * @name  Unbind
 * @fn    void Unbind() const
 * @brief Unbind underlying shader storage
 */
void OGLShaderStorageBuffer::Unbind() const {
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

/**
 * @name  UploadData
 * @fn    int UploadData(const void* data, const size_t& n_bytes) const
 * @brief Push data from cpu to opengl buffer
 * @param[in] data    Pointer to data to push to storage
 * @param[in] n_bytes Number of bytes to copy.
 * @return -1 if error, 0 otherwise
 */
int OGLShaderStorageBuffer::UploadData(const void* data,
                                       const size_t& n_bytes) const {
  this->Bind();
  glBufferData(GL_SHADER_STORAGE_BUFFER, n_bytes, data, GL_DYNAMIC_COPY);
  this->Unbind();
  return glGetError() == GL_NO_ERROR ? 0 : -1;
}




}  // namespace LTS5