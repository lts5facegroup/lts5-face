/**
 *  @file   rasterizer.cpp
 *  @brief  Implement triangle rasterizer based on OpenGL
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   7/9/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <string>

#include "GL/glew.h"

#include "lts5/ogl/rasterizer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @name  Rasterizer
 * @brief Constructor
 * @param[in] width           Image width
 * @param[in] height          Image height
 * @param[in] vertex_code     Vertex shader code
 * @param[in] geometry_code   Geometry shader code
 * @param[in] fragment_code   Fragment shader code
 */
Rasterizer::Rasterizer(const int& width,
                       const int& height,
                       const std::string& vertex_code,
                       const std::string& geometry_code,
                       const std::string& fragment_code) :
        Rasterizer::Rasterizer(width,
                               height,
                               vertex_code,
                               geometry_code,
                               fragment_code,
                               0.0,
                               0.0,
                               0.0,
                               1.0) {
}

/*
 * @name  Rasterizer
 * @brief Constructor
 * @param[in] width           Image width
 * @param[in] height          Image height
 * @param[in] vertex_code     Vertex shader code
 * @param[in] geometry_code   Geometry shader code
 * @param[in] fragment_code   Fragment shader code
 * @param[in] clear_red       Red channel cleared value in [0, 1]
 * @param[in] clear_green     Green channel cleared value in [0, 1]
 * @param[in] clear_blue      Blue channel cleared value in [0, 1]
 * @param[in] clear_depth     Depth channel cleared value in [0, 1]
 */
Rasterizer::Rasterizer(const int& width,
                       const int& height,
                       const std::string& vertex_code,
                       const std::string& geometry_code,
                       const std::string& fragment_code,
                       const float& clear_red,
                       const float& clear_green,
                       const float& clear_blue,
                       const float& clear_depth) :
        ctx_(width, height, 0, 4, 3, true),
        clear_r_(clear_red),
        clear_g_(clear_green),
        clear_b_(clear_blue),
        clear_d_(clear_depth) {
  ctx_.ActiveContext();
  // Create rendering program
  if (!vertex_code.empty()) {
    shader_.AddShader(OGLShader(vertex_code,
                                OGLShader::ShaderType::kVertex));
  }
  if (!geometry_code.empty()) {
    shader_.AddShader(OGLShader(geometry_code,
                                OGLShader::ShaderType::kGeometry));
  }
  if (!fragment_code.empty()) {
    shader_.AddShader(OGLShader(fragment_code,
                                OGLShader::ShaderType::kFragment));
  }
  shader_.Build();
  // Create Framebuffer
  fmb_.Init(width,
            height,
            0,
            true,
            OGLIOBuffer::BufferType::kRGBAf);
  // Disable / Enable some function
  glDisable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);
  // Disable context, no used anymore
  ctx_.DisableContext();
}

#pragma mark -
#pragma mark - Usage

void Rasterizer::Enable() const {
  ctx_.ActiveContext();
}

void Rasterizer::Disable() const {
  ctx_.DisableContext();
}

int Rasterizer::SetShaderStorage(const std::string& name,
                                 const void* data,
                                 const size_t& n_bytes) {
  // Buffer already exists ?
  if (buffer_.count(name) == 0) {
    // Create new buffer
    buffer_[name] = new OGLShaderStorageBuffer();
  }
  // Upload new data
  int err = buffer_.at(name)->UploadData(data, n_bytes);
  return err;
}

int Rasterizer::SetUniform(const std::string& name,
                           const Mat4& matrix) const {
  shader_.Use();
  shader_.SetUniform(name.c_str(), matrix, false);
  shader_.StopUsing();
  return glGetError() == GL_NO_ERROR ? 0 : -1;
}

int Rasterizer::Render(const int& n_pts, const int& offset) {

  auto e1 = glGetError();

  // Bind buffers to shaders
  for (const auto& buff : buffer_) {
    const auto& name = buff.first;
    int slot = -1;
    GLint resource_idx = glGetProgramResourceIndex(shader_.get_ref(),
                                                   GL_SHADER_STORAGE_BLOCK,
                                                   name.c_str());
    GLenum property = GL_BUFFER_BINDING;
    GLsizei length;
    glGetProgramResourceiv(shader_.get_ref(),
            GL_SHADER_STORAGE_BLOCK,
            resource_idx,
            1,
            &property,
            1,
            &length,
            &slot);
    buff.second->BindBufferBase(slot);
  }
  // Enable program
  shader_.Use();
  // Bind Framebuffer
  fmb_.BindForWriting();
  // Clear framebuffer
  glClearColor(clear_r_, clear_g_, clear_b_, 1.f);
  glClearDepth(clear_d_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  // Draw
  glDrawArrays(GL_POINTS, (GLint)offset, (GLsizei)n_pts);
  fmb_.BlitBuffer();
  fmb_.Unbind();
  shader_.StopUsing();
  glFlush();
  for (const auto& buff : buffer_) {
    buff.second->Unbind();
  }
  return glGetError() == GL_NO_ERROR ? 0 : -1;
}

int Rasterizer::GetImage(cv::Mat* image) const {
  int err = fmb_.ReadColorBuffer(image);
  return err;
}


}  // namespace LTS5
