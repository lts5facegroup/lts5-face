/**
 *  @file   glut_backend.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   11/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <cassert>

#include "GL/glew.h"
#ifdef HAS_GLUT_
  #include <GL/freeglut.h>
#endif

#include "lts5/ogl/key_types.hpp"
#include "lts5/ogl/glut_backend.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/**
 * @name  OGLGlutBackend
 * @fn  OGLGlutBackend(void)
 * @brief Constructor
 */
OGLGlutBackend::OGLGlutBackend(void) {
  with_depth_ = false;
  with_stencil_ = false;
}

/**
 * @name  ~OGLGlutBackend
 * @fn  ~OGLGlutBackend(void)
 * @brief Constructor
 */
OGLGlutBackend::~OGLGlutBackend(void) {}

/*
 * @name  Init
 * @fn  void Init(int argc, const char** argv,
 *                bool with_depth, bool with_stencil)
 * @brief Initialize GLUT Backend
 * @param[in] argc          Number of parameters in Command line
 * @param[in] argv          Command line parameters
 * @param[in] with_depth    Use depth
 * @param[in] with_stencil  Use stencil
 */
void OGLGlutBackend::Init(int argc,
                          const char** argv,
                          bool with_depth,
                          bool with_stencil,
                          bool with_back_culling) {
#ifdef HAS_GLUT_
  with_depth_ = with_depth;
  with_stencil_ = with_stencil;
  with_back_cull_ = with_back_culling;
  // Init
  glutInit(&argc, const_cast<char**>(argv));
  // Param
  unsigned int display_mode = GLUT_DOUBLE|GLUT_RGBA;
  if (with_depth_) {
    display_mode |= GLUT_DEPTH;
  }
  if (with_stencil_) {
    display_mode |= GLUT_STENCIL;
  }
  glutInitContextVersion(3, 2);
  glutInitDisplayMode(display_mode);
  glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
#else
  std::cout << "GLUT Not supported" << std::endl;
#endif
}

/*
 * @name  Terminate
 * @fn  void Terminate(void)
 * @brief Shutdown GLUT backend
 */
void OGLGlutBackend::Terminate(void) {

}

/**
 * @name  CreateWindows
 * @fn  bool CreateWindows(const int width, const int height,
 *                         const bool fullscreen,
 *                         const char* window_title)
 * @brief Initialiaze GLUT window
 * @param[in] width         Window's width
 * @param[in] height        Window's height
 * @param[in] fullscreen    True for fullscreen window
 * @param[in] window_title  Window's title
 */
bool OGLGlutBackend::CreateWindows(const int width,
                                   const int height,
                                   const bool fullscreen,
                                   const char* window_title) {
  bool ret = false;
#ifdef HAS_GLUT_
  // Create window
  if (fullscreen) {
    char mode[64] = {0};
    int bpp = 32; // Bit per pixel -> RGBA
    sprintf(mode, "%dx%d:%d@60",width, height, bpp);
    glutGameModeString(mode);
    glutEnterGameMode();
  } else {
    glutInitWindowSize(width, height);
    glutCreateWindow(window_title);
  }
  // Init GLEW ()
#ifndef __APPLE__
  glewExperimental = GL_TRUE;
  GLenum res = glewInit();
  if (res != GLEW_OK) {
    std::cout << glewGetErrorString(res) << std::endl;
    return false;
  }
  // GLEW throws some errors, so discard all the errors so far
  while(glGetError() != GL_NO_ERROR) {}
  ret = true;
#endif
#else
  std::cout << "GLUT Not supported" << std::endl;
#endif
  return ret;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Run
 * @fn  void Run(OGLCallback* ogl_callback)
 * @brief Enter into GLUT main loop
 * @param[in] ogl_callback  Pointer to object holding opengl callback
 *                          implementation
 */
void OGLGlutBackend::Run(OGLCallback* ogl_callback) {
  assert(ogl_callback);
#ifdef HAS_GLUT_
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glFrontFace(GL_CCW);
  if (with_back_cull_) {
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
  }
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  if (with_depth_) {
    glEnable(GL_DEPTH_TEST);
  }
  callback_ = ogl_callback;
  InitCallbacks();
  glutMainLoop();
#else
  std::cout << "GLUT Not supported" << std::endl;
#endif
}

/*
 * @name  SwapBuffer
 * @fn  void SwapBuffer(void)
 * @brief Swap opengl buffer in order to display what has been rendered
 */
void OGLGlutBackend::SwapBuffer(void) {
#ifdef HAS_GLUT_
  glutSwapBuffers();
#else
  std::cout << "GLUT Not supported" << std::endl;
#endif
}

/*
 * @name  LeaveMainLoop
 * @fn  void LeaveMainLoop(void)
 * @brief Leave backend mainloop
 */
void OGLGlutBackend::LeaveMainLoop(void) {
#ifdef HAS_GLUT_
  glutLeaveMainLoop();
#else
  std::cout << "GLUT Not supported" << std::endl;
#endif
}

/*
 * @name  SetMousePos
 * @fn  void SetMousePos(const int x, const int y)
 * @brief Set mouse position inside window
 * @param[in] x   Cursor X position
 * @param[in] y   Cursor Y position
 */
void OGLGlutBackend::SetMousePos(const int x, const int y) {
  std::cout << "Not supported by GLUT backend" << std::endl;
}

#pragma mark -
#pragma mark Private

/** Pointer to the object implementing OGLCallback interface. All event are
  * forwarded to this object */
OGLCallback * OGLGlutBackend::callback_;


/*
 * @name  GlutKeyToOGLKey
 * @fn  static OGLKey GlutKeyToOGLKey(unsigned int glfw_key)
 * @brief Convert GLUT key into OGLKey format
 * @param[in] key GLUT key to convert
 * @return  OGLKey equivalent if exist otherwise kUndefined
 */
OGLKey OGLGlutBackend::GlutKeyToOGLKey(unsigned int key) {
#ifdef HAS_GLUT_
  switch (key) {
    case GLUT_KEY_F1:
      return OGLKey::kF1;
    case GLUT_KEY_F2:
      return OGLKey::kF2;
    case GLUT_KEY_F3:
      return OGLKey::kF3;
    case GLUT_KEY_F4:
      return OGLKey::kF4;
    case GLUT_KEY_F5:
      return OGLKey::kF5;
    case GLUT_KEY_F6:
      return OGLKey::kF6;
    case GLUT_KEY_F7:
      return OGLKey::kF7;
    case GLUT_KEY_F8:
      return OGLKey::kF8;
    case GLUT_KEY_F9:
      return OGLKey::kF9;
    case GLUT_KEY_F10:
      return OGLKey::kF10;
    case GLUT_KEY_F11:
      return OGLKey::kF11;
    case GLUT_KEY_F12:
      return OGLKey::kF12;
    case GLUT_KEY_LEFT:
      return OGLKey::kLEFT;
    case GLUT_KEY_UP:
      return OGLKey::kUP;
    case GLUT_KEY_RIGHT:
      return OGLKey::kRIGHT;
    case GLUT_KEY_DOWN:
      return OGLKey::kDOWN;
    case GLUT_KEY_PAGE_UP:
      return OGLKey::kPAGE_UP;
    case GLUT_KEY_PAGE_DOWN:
      return OGLKey::kPAGE_DOWN;
    case GLUT_KEY_HOME:
      return OGLKey::kHOME;
    case GLUT_KEY_END:
      return OGLKey::kEND;
    case GLUT_KEY_INSERT:
      return OGLKey::kINSERT;
    case GLUT_KEY_DELETE:
      return OGLKey::kDELETE;
    default:
    std::cout << "Unimplemented GLUT key" << std::endl;
  }
#else
  std::cout << "GLUT Not supported" << std::endl;
#endif
  return OGLKey::kUNDEFINED;
}

/*
 * @name  GlutMouseToOGLMouse
 * @fn  static OGLMouse GlutMouseToOGLMouse(unsigned int button)
 * @brief Convert GLUT button into OGLMouse format
 * @param[in] button GLUT button to convert
 * @return  OGLMouse equivalent if exist otherwise kUndefined
 */
OGLMouse OGLGlutBackend::GlutMouseToOGLMouse(unsigned int button) {
#ifdef HAS_GLUT_
  switch (button) {
    case GLUT_LEFT_BUTTON:
      return OGLMouse::kMouseLeft;
    case GLUT_RIGHT_BUTTON:
      return OGLMouse::kMouseRight;
    case GLUT_MIDDLE_BUTTON:
      return OGLMouse::kMouseMiddle;
    default:
      std::cout << "Unimplemented OGLMouse button" << std::endl;
  }
#else
  std::cout << "GLUT Not supported" << std::endl;
#endif
  return OGLMouse::kUndefined;
}

/*
 * @name  SpecialKeyboardCb
 * @fn  static void SpecialKeyboardCb(int key, int x, int y)
 * @brief Handle special keyboard key
 * @param[in] key   Key that trigger the event
 * @param[in] x     X (Not used)
 * @param[in] y     Y (Not used)
 */
void OGLGlutBackend::SpecialKeyboardCb(int key, int x, int y) {
  OGLKey ogl_key = GlutKeyToOGLKey(key);
  callback_->OGLKeyboardCb(ogl_key, OGLKeyState::kPress);
}

/*
 * @name  KeyboardCb
 * @fn  static void KeyboardCb(unsigned char key, int x, int y)
 * @brief Handle keyboard key
 * @param[in] key   Key that trigger the event
 * @param[in] x     X (Not used)
 * @param[in] y     Y (Not used)
 */
void OGLGlutBackend::KeyboardCb(unsigned char key, int x, int y) {

  if (((key >= '+') && (key <= '9')) ||
      ((key >= 'A') && (key <= 'Z')) ||
      ((key >= 'a') && (key <= 'z'))) {
    OGLKey ogl_key = static_cast<OGLKey>(key);
    callback_->OGLKeyboardCb(ogl_key, OGLKeyState::kPress);
  } else {
    if (key == 27) {
      callback_->OGLKeyboardCb(OGLKey::kESCAPE , OGLKeyState::kPress);
    }
    std::cout << "Unimplemented GLUT key (" << key << ")" << std::endl;
  }
}

/*
 * @name  PassiveMouseCb
 * @fn  static void PassiveMouseCb(int x, int y)
 * @brief Mouse displacement callback
 * @param[in] x     Cursor's X position
 * @param[in] y     Cursor's Y position
 */
void OGLGlutBackend::PassiveMouseCb(int x, int y) {
  callback_->OGLPassiveMouseCb(x, y);
}

/*
 * @name  RenderCb
 * @fn  static void RenderCb(void)
 * @brief Render scene
 */
void OGLGlutBackend::RenderCb(void) {
  callback_->OGLRenderCb();
#ifdef HAS_GLUT_
  glutSwapBuffers();
#else
  std::cout << "GLUT Not supported" << std::endl;
#endif
}

/*
 * @name  IdleCb
 * @fn  static void IdleCb(void)
 * @brief Idle callback
 */
void OGLGlutBackend::IdleCb(void) {
  callback_->OGLRenderCb();
#ifdef HAS_GLUT_
  glutSwapBuffers();
#else
  std::cout << "GLUT Not supported" << std::endl;
#endif
}

/*
 * @name  KeyboardCb
 * @fn  static void MouseCb(int button, int state, int x, int y)
 * @brief Handle Mouse event
 * @param[in] button  Button that trigger the callback
 * @param[in] state   Button state at that time
 * @param[in] x       Cursor's X position
 * @param[in] y       Cursor's y position
 */
void OGLGlutBackend::MouseCb(int button, int state, int x, int y) {
#ifdef HAS_GLUT_
  OGLMouse ogl_mouse = GlutMouseToOGLMouse(button);
  OGLKeyState key_state = ((state == GLUT_DOWN) ?
                           OGLKeyState ::kPress:
                           OGLKeyState ::kRelease);
  callback_->OGLMouseCb(ogl_mouse, key_state, x, y);
#else
  std::cout << "GLUT Not supported" << std::endl;
#endif
}

/*
 * @name  InitCallbacks
 * @fn  static void InitCallbacks(void)
 * @brief Initialize GLUT callbacks
 */
void OGLGlutBackend::InitCallbacks(void) {
#ifdef HAS_GLUT_
  glutDisplayFunc(RenderCb);
  glutIdleFunc(IdleCb);
  glutSpecialFunc(SpecialKeyboardCb);
  glutPassiveMotionFunc(PassiveMouseCb);
  glutKeyboardFunc(KeyboardCb);
  glutMouseFunc(MouseCb);
#else
  std::cout << "GLUT Not supported" << std::endl;
#endif
}
}
