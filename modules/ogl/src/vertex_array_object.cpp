/**
 *  @file   vertex_array_object.cpp
 *  @brief  OpenGL Vertex Array Object (VAO) abstration layer
 *  @ingroup    ogl
 *
 *  @author Christophe Ecabert
 *  @date   12/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "GL/glew.h"

#include <cstring>
#include <cassert>
#include <iostream>

#include "lts5/ogl/vertex_array_object.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  OGLVertexArrayObject
 * @fn    OGLVertexArrayObject()
 * @brief Constructor
 */
template<typename T>
OGLVertexArrayObject<T>::OGLVertexArrayObject() : vao_(0),
                                                  vbo_(nullptr),
                                                  n_buffer_(0) {
}

/*
 * @name  ~OGLVertexArrayObject
 * @fn    ~OGLVertexArrayObject()
 * @brief Destructor
 */
template<typename T>
OGLVertexArrayObject<T>::~OGLVertexArrayObject() {
  // Release VBOs
  if (vbo_ != nullptr) {
    glDeleteBuffers(n_buffer_, vbo_);
    delete[] vbo_;
    vbo_ = nullptr;
  }
  // Release VAO
  if (vao_ != 0) {
    glDeleteVertexArrays(1, &vao_);
    vao_ = 0;
  }
}

/*
 * @name  Create
 * @fn    int Create(const int& n_buffer)
 * @brief Initialize VAO and create a given number of buffer
 * @param[in] n_buffer Number of buffer to create
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int OGLVertexArrayObject<T>::Create(const int& n_buffer) {
  int err = -1;
  // Create/Bind VAO
  if (vao_ == 0) {
    glGenVertexArrays(1, &vao_);
  }
  glBindVertexArray(vao_);
  // VBOs
  if (n_buffer != n_buffer_) {
    if (vbo_ != nullptr) {
      // Release current VBOs
      glDeleteBuffers(n_buffer_, vbo_);
      delete[] vbo_;
      vbo_ = nullptr;
    }
    // Create new one
    n_buffer_ = n_buffer;
    vbo_ = new unsigned int[n_buffer_];
    glGenBuffers(n_buffer_, vbo_);
    // Error check
    err = glGetError() == GL_NO_ERROR ? 0 : -1;
  } else {
    err = 0;
  }
  // Make sure the VAO is not changed from the outside
  glBindVertexArray(0);
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Bind
 * @fn    void Bind() const
 * @brief Bind VAO
 */
template<typename T>
void OGLVertexArrayObject<T>::Bind() const {
  glBindVertexArray(vao_);
}

/*
 * @name  Unbind
 * @fn    void Unbind() const
 * @brief Unbind VAO
 */
template<typename T>
void OGLVertexArrayObject<T>::Unbind() const {
  glBindVertexArray(0);
}

/*
 * @name  BindBuffer
 * @fn    void BindBuffer(const size_t& index, const BufferType& type) const
 * @brief Bind a specific VBO
 * @param[in] index   Buffer index to bind
 * @param[in] type    Buffer's type
 */
template<typename T>
void OGLVertexArrayObject<T>::BindBuffer(const size_t& index,
                                         const BufferType& type) const {
  assert(index < n_buffer_);
  GLenum tp = type == BufferType::kAttribute ?
              GL_ARRAY_BUFFER :
              GL_ELEMENT_ARRAY_BUFFER;
  glBindBuffer(tp, vbo_[index]);
}

/*
 * @name  Map
 * @fn    T* Map(const BufferType& type) const
 * @brief Map an OpenGL resource to the host
 * @param[in] index   Buffer index to map
 * @param[in] type    Type of buffer to map
 * @return Pointer to the mapped resource or nullptr if failed.
 */
template<typename T>
void* OGLVertexArrayObject<T>::Map(const size_t& index,
                                   const BufferType& type) const {
  void* ptr = nullptr;
  if(index < n_buffer_) {
    // Bind
    GLenum tp = type == BufferType::kAttribute ?
                GL_ARRAY_BUFFER :
                GL_ELEMENT_ARRAY_BUFFER;
    glBindBuffer(tp, vbo_[index]);
    // Map
    GLvoid* dest = glMapBuffer(tp, GL_READ_WRITE);
    // Check error
    GLenum e = glGetError();
    ptr = e == GL_NO_ERROR ? dest : nullptr;
  } else {
    LTS5_LOG_ERROR("Index out of bound, provided " << index << ", limit" << n_buffer_);
  }
  return ptr;
}

/*
 * @name  Unmap
 * @fn    int Unmap(const size_t& index, const BufferType& type) const
 * @brief Unmmap an OpenGL resource from the host
 * @param[in] index   Buffer index to unmap
 * @param[in] type    Type of buffer to unmap
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int OGLVertexArrayObject<T>::Unmap(const size_t& index,
                                   const BufferType& type) const {
  int err = -1;
  if(index < n_buffer_) {
    // Bind
    GLenum tp = type == BufferType::kAttribute ?
                GL_ARRAY_BUFFER :
                GL_ELEMENT_ARRAY_BUFFER;
    glBindBuffer(tp, vbo_[index]);
    // Unmap
    glUnmapBuffer(tp);
    // Sanity check
    err = glGetError() == GL_NO_ERROR ? 0 : -1;
  }
  return err;
}

/*
 * @name  AddData
 * @fn    int AddData(const size_t& n_bytes,
                      const void* data,
                      const BufferType& type) const
 * @brief Add data to the currently linked VBO
 * @param[in] n_bytes Number of bytes to copy to OpenGL memory
 * @param[in] data    Array storing data
 * @param[in] type    Buffer's type
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int OGLVertexArrayObject<T>::AddData(const size_t& n_bytes,
                                     const void* data,
                                     const BufferType& type) const {
  auto e1 = glGetError();
  // Push data to opengl memory
  GLenum tp = type == BufferType::kAttribute ?
              GL_ARRAY_BUFFER :
              GL_ELEMENT_ARRAY_BUFFER;
  glBufferData(tp, static_cast<GLsizeiptr>(n_bytes), data, /*GL_STATIC_DRAW*/ GL_DYNAMIC_DRAW);
  return glGetError() == GL_NO_ERROR ? 0 : -1;
}

/*
 * @name  UpdateData
 * @fn    int UpdateData(const size_t& n_bytes,
                         const void* data,
                         const BufferType& type) const
 * @brief Update data to the currently linked VBO
 * @param[in] n_bytes Number of bytes to copy to OpenGL memory
 * @param[in] data    Array storing data
 * @param[in] type    Buffer's type
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int OGLVertexArrayObject<T>::UpdateData(const size_t& n_bytes,
                                        const void* data,
                                        const BufferType& type) const {
  int err = -1;
  GLenum tp = type == BufferType::kAttribute ?
              GL_ARRAY_BUFFER :
              GL_ELEMENT_ARRAY_BUFFER;
  GLvoid* dest = glMapBuffer(tp, GL_WRITE_ONLY);
  if (dest != nullptr) {
    // Copy new data
    memcpy(dest, data, n_bytes);
    // Unmap memory
    glUnmapBuffer(tp);
    // Done check
    err = (glGetError() == GL_NO_ERROR ? 0 : -1);
  }
  return err;
}

/*
 * @name  SetAttributePointer
 * @fn    int SetAttributePointer(const size_t& index, const int& size) const
 * @brief Set attribute's properties for a given buffer in the VAO
 * @param[in] index   Buffer index
 * @param[in] size    Attribute's size (vertex=3, tcorrd=2, ...)
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int OGLVertexArrayObject<T>::SetAttributePointer(const size_t& index,
                                                 const int& size) const {
  assert(index < n_buffer_);
  GLenum dtype = sizeof(T) == 4 ? GL_FLOAT : GL_DOUBLE;
  glEnableVertexAttribArray(static_cast<GLuint>(index));
  glVertexAttribPointer(static_cast<GLuint>(index),
                        size, dtype, GL_FALSE, 0, NULL);
  return glGetError() == GL_NO_ERROR ? 0 : -1;
}

#pragma mark -
#pragma mark Explicit instantiation

/** Float - VAO */
template class OGLVertexArrayObject<float>;
/** Double - VAO */
template class OGLVertexArrayObject<double>;

}  // namepsace LTS5