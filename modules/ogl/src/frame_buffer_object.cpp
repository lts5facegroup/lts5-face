/**
 *  @file   frame_buffer_object.cpp
 *  @brief  Frame Buffer Object (FBO) abstraction
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   11/11/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <iomanip>
#include "GL/glew.h"

#include "lts5/ogl/frame_buffer_object.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#define ROUND_UP(x, y) ((((x) + ((y) - 1)) / (y)) * (y))


/**
 * @name    OGLInternalFormatConverter
 * @brief   Convert custom buffer type into OpenGL internal format
 * @param[in] type  Buffer type
 * @return  OpenGL internal format
 */
GLenum OGLInternalFormatConverter(const FrameBufferObject::BufferType type) {
  switch (type) {
    case FrameBufferObject::BufferType::kDepth: return GL_DEPTH24_STENCIL8;
    case FrameBufferObject::BufferType::kR: return GL_R8;
    case FrameBufferObject::BufferType::kRf: return GL_R32F;
    case FrameBufferObject::BufferType::kRGB: return GL_RGB8;
    case FrameBufferObject::BufferType::kRGBf: return GL_RGB32F;
    case FrameBufferObject::BufferType::kRGBA:  return GL_RGBA8;
    case FrameBufferObject::BufferType::kRGBAf: return GL_RGBA32F;
    default:  return GL_NONE;
  }
}

/**
 * @name    OGLInternalFormatConverter
 * @brief   Convert custom buffer type into OpenGL internal format
 * @param[in] type  Buffer type
 * @return  OpenGL internal format
 */
GLenum OGLFormatConverter(const FrameBufferObject::BufferType type) {
  switch (type) {
    case FrameBufferObject::BufferType::kDepth: return GL_DEPTH_STENCIL;
    case FrameBufferObject::BufferType::kR:
    case FrameBufferObject::BufferType::kRf: return GL_RED;
    case FrameBufferObject::BufferType::kRGB:
    case FrameBufferObject::BufferType::kRGBf: return GL_RGB;
    case FrameBufferObject::BufferType::kRGBA:
    case FrameBufferObject::BufferType::kRGBAf: return GL_RGBA;
    default:  return GL_NONE;
  }
}


GLenum OGLTypeConverter(const FrameBufferObject::BufferType type) {
  switch (type) {
    case FrameBufferObject::BufferType::kR:
    case FrameBufferObject::BufferType::kRGB:
    case FrameBufferObject::BufferType::kRGBA: return GL_UNSIGNED_BYTE;
    case FrameBufferObject::BufferType::kUnknown:
    case FrameBufferObject::BufferType::kDepth:
    case FrameBufferObject::BufferType::kRf:
    case FrameBufferObject::BufferType::kRGBf:
    case FrameBufferObject::BufferType::kRGBAf: return GL_FLOAT;
  }
  return GL_NONE;
}

#pragma mark -
#pragma mark Initialization

/*
 * @name  FrameBufferObject
 * @fn    FrameBufferObject()
 * @brief Constructor
 */
FrameBufferObject::FrameBufferObject() : fbo_(0),
                                         color_buffer_(0),
                                         depth_buffer_(0),
                                         width_(0),
                                         height_(0),
                                         depth_(0),
                                         type_(BufferType::kUnknown) {
  // Construct FBO
  glGenFramebuffers(1, &fbo_);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo_);
  // Enable one color attachments.
  GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT0};
  glDrawBuffers(1, draw_buffers);
  // Create and bind color buffer.
  glGenTextures(1, &color_buffer_);
  glBindTexture(GL_TEXTURE_2D_ARRAY, color_buffer_);
  glFramebufferTexture(GL_FRAMEBUFFER,
                       GL_COLOR_ATTACHMENT0,
                       color_buffer_,
                       0);

  // Create and bind depth buffer
  glGenTextures(1, &depth_buffer_);
  glBindTexture(GL_TEXTURE_2D_ARRAY, depth_buffer_);
  glFramebufferTexture(GL_FRAMEBUFFER,
                       GL_DEPTH_STENCIL_ATTACHMENT,
                       depth_buffer_,
                       0);
  // Error check
  int err = glGetError() == GL_NO_ERROR ? 0 : -1;
  if (err) {
    LTS5_LOG_ERROR("Error while creating FBO");
  }
  // Unbind
  glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/*
 * @name  ~FrameBufferObject
 * @fn    ~FrameBufferObject()
 * @brief Destructor
 */
FrameBufferObject::~FrameBufferObject() {
  glDeleteBuffers(1, &color_buffer_);
  glDeleteBuffers(1, &depth_buffer_);
  glDeleteFramebuffers(1, &fbo_);
}

/*
 * @name  Create
 * @fn    int Create(const int& height, const int& width, const int& depth,
 *                    const BufferType& type)
 * @brief Create internel buffer for a given configuration. If configuration
 *    match, does nothing
 * @param[in] height  Image height
 * @param[in] width   Image width
 * @param[in] depth   Number of image to render at once
 * @param[in] type    Type of buffer
 * @return    -1 if error, 0 otherwise
 */
int FrameBufferObject::Create(const int& height,
                              const int& width,
                              const int& depth,
                              const BufferType& type) {
  int err = 0;
  if (height > height_ ||
      width > width_ ||
      depth > depth_ ||
      type != type_) {
    // New framebuffer size.
    width_  = (width > width_) ? width : width_;
    height_ = (height > height_) ? height : height_;
    depth_  = (depth > depth_) ? depth : depth_;
    width_  = ROUND_UP(width_, 32);
    height_ = ROUND_UP(height_, 32);
    type_ = type;
    LTS5_LOG_DEBUG("Increasing frame buffer size to (width, height, depth)"
                   " = (" << width_ << ", " << height_ << ", "
                   << depth_ << ")");
    // Bind
    glBindFramebuffer(GL_FRAMEBUFFER, fbo_);
    // Color
    glBindTexture(GL_TEXTURE_2D_ARRAY, color_buffer_);
    auto ifmt = OGLInternalFormatConverter(type_);
    auto fmt = OGLFormatConverter(type_);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0,   // Target, level
                 ifmt,
                 width_, height_, depth_, 0,
                 fmt, GL_UNSIGNED_BYTE, nullptr);

    // Depth
    glBindTexture(GL_TEXTURE_2D_ARRAY, depth_buffer_);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0,   // Target, level
                 GL_DEPTH24_STENCIL8,
                 width_, height_, depth_, 0,
                 GL_DEPTH_STENCIL,
                 GL_UNSIGNED_INT_24_8,
                 nullptr);
    // Ubind
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    err = glGetError() == GL_NO_ERROR ? 0 : -1;
  }
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 * @name Validate
 * @fn    int Validate() const
 * @brief Check if framebuffer is valide
 * @return -1 if invalid, 0 otherwise
 */
int FrameBufferObject::Validate() const {
  // Bind
  glBindFramebuffer(GL_FRAMEBUFFER, fbo_);
  // Check status
  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  // Unbind
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  return status == GL_FRAMEBUFFER_COMPLETE ? 0 : -1;
}

/**
 * @name  Clear
 * @fn int Clear()
 * @brief Clear color/depth buffer
 * @param[in] height  Image height
 * @param[in] width   Image width
 * @param[in] depth   Number of image
 * @return    -1 if error, 0 otherwise
 */
int FrameBufferObject::Clear(const int& height,
                             const int& width,
                             const int& depth) {
  auto fmt = OGLFormatConverter(type_);
  auto dtype = OGLTypeConverter(type_);
  glClearTexSubImage(color_buffer_, 0, 0, 0, 0, width, height, depth,
                     fmt, dtype, nullptr);
  glClearTexSubImage(depth_buffer_, 0, 0, 0, 0, width, height, depth,
                     GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, nullptr);
  return glGetError() == GL_NO_ERROR ? 0 : -1;
}

/*
 * @name  BindForWriting
 * @fn  void BindForWriting() const;
 * @brief Bind internal buffer for writting
 */
void FrameBufferObject::BindForWriting() const {
  glBindFramebuffer(GL_FRAMEBUFFER, fbo_);
}

/*
 * @name  BindForReading
 * @fn  void BindForReading(const unsigned int& texture_unit) const
 * @brief Bind for reading
 * @param[in] texture_unit  Texture unit to activate
 */
void FrameBufferObject::BindForReading(const unsigned int& texture_unit) const {
  // Activate texture unit
  glActiveTexture(GL_TEXTURE0 + texture_unit);
  if (type_ == BufferType::kDepth) {
    glBindTexture(GL_TEXTURE_2D_ARRAY, depth_buffer_);
  } else {
    glBindTexture(GL_TEXTURE_2D_ARRAY, color_buffer_);
  }
}

/*
 * @name  Unbind
 * @fn  void Unbind()
 * @brief Unbind the underlying framebuffer
 */
void FrameBufferObject::Unbind() const {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/*
 * @name  GetOglBuffer
 * @fn    unsigned int GetOglBuffer(const BufferType& buff_type) const
 * @brief Provide reference to the underlaying OpenGL buffer
 * @param[in] buff_type
 * @return    OpenGL reference
 */
unsigned int FrameBufferObject::
GetOglBuffer(const BufferType& buff_type) const {
  if (buff_type == BufferType::kDepth) {
    return depth_buffer_;
  } else {
    return color_buffer_;
  }
}

}  // namespace LTS5
