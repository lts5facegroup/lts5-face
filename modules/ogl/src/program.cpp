/**
 *  @file   programm.cpp
 *  @brief  OpenGL Programm helper interface
 *
 *  @author Christophe Ecabert
 *  @date   14/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <cassert>
#include <string>
#include <limits>
#include <vector>

#include "GL/glew.h"

#include "lts5/ogl/program.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization
/**
 *  @name OGLProgram
 *  @fn OGLProgram(const std::vector<OGLShader>& shaders)
 *  @brief  Create OpenGL program interface
 *  @param[in]  shaders     List of shader to use in this program
 *  @throw  Exception if something goes wrong
 */
OGLProgram::OGLProgram(const std::vector<OGLShader>& shaders) :
OGLProgram::OGLProgram(shaders, {}) {}

/*
 *  @name OGLProgram
 *  @fn OGLProgram(const std::vector<OGLShader>& shaders)
 *  @brief  Create OpenGL program interface
 *  @param[in]  shaders List of shader to use in this program
 *  @throw  Exception if something goes wrong
 */
OGLProgram::OGLProgram(const std::vector<OGLShader>& shaders,
                       const std::vector<Attributes>& attributes) :
        object_(0),
        shaders_(shaders),
        attributes_(attributes) {
  // Directly build shading program
  this->Build();
}

/**
 *  @name OGLProgram
 *  @fn OGLProgram(const OGLProgram& other) = delete
 *  @brief  Copy constructor
 *  @param[in]  other Program to copy from
 */
OGLProgram::OGLProgram(const OGLProgram& other) :
        RefCounted::RefCounted(other),
        object_(other.object_),
        msg_(other.msg_),
        shaders_(other.shaders_),
        attributes_(other.attributes_) {
}

/*
 *  @name operator=
 *  @fn OGLProgram& operator=(const OGLProgram& rhs)
 *  @brief  Assignment operator
 *  @param[in]  rhs   Program to assign from
 */
OGLProgram& OGLProgram::operator=(const OGLProgram& rhs) {
  if (this != &rhs) {
    if (this->RefCountIsOne() && object_) {
      glDeleteProgram(object_);
    }
    RefCounted::operator=(rhs);
    object_ = rhs.object_;
    msg_ = rhs.msg_;
    shaders_ = rhs.shaders_;
    attributes_ = rhs.attributes_;
  }
  return *this;
}

/*
 *  @name ~OGLProgram
 *  @fn ~OGLProgram()
 *  @brief  Destructor
 */
OGLProgram::~OGLProgram() {
  if (this->Unref() && object_) {
    glDeleteProgram(object_);
    object_ = 0;
  }
}

/*
 * @name  AddShader
 * @fn    void AddShader(const OGLShader& shader,
                         const std::vector<Attributes>& attributes)
 * @brief Add shader with attributes to the program.
 * @param[in] shader      Shader instance to add
 * @param[in] attributes  List of attributes for the shader
 */
void OGLProgram::AddShader(const OGLShader& shader,
                           const std::vector<Attributes>& attributes) {
  shaders_.push_back(shader);
  attributes_.insert(attributes_.end(),
                     attributes.begin(),
                     attributes.end());
}

#pragma mark -
#pragma mark Usage

/**
 * @name  Build
 * @fn    void Build()
 * @brief Build shading program
 * @throw Throw exception if something went wrong during the compilation of
 *        the shading program.
 */
void OGLProgram::Build() {
  // At least one shader is provided ?
  if (shaders_.empty()) {
    throw ProcessError(ProcessError::ProcessErrorEnum::kErr,
                       "No shader provided",
                       FUNC_NAME);
  }
  // Create program object, if already created delete it first and create a new
  // one
  if (object_ != 0) { glDeleteProgram(object_); }
  object_ = glCreateProgram();
  if (object_ == 0) {
    throw ProcessError(ProcessError::ProcessErrorEnum::kErr,
                       "glCreateProgram failed",
                       FUNC_NAME);
  }
  // Attach all the shader to this program
  for (const auto& s : shaders_) {
    glAttachShader(object_, s.Object());
  }
  // Link program
  glLinkProgram(object_);
  // Detach all shader
  for (const auto& s : shaders_) {
    glDetachShader(object_, s.Object());
  }
  // Test if everything went smoothly
  int status;
  glGetProgramiv(object_, GL_LINK_STATUS, &status);
  if (status == GL_FALSE) {
    // Somethink went wrong while linking
    std::string msg("Program linking failure : ");
    int info_length;
    glGetProgramiv(object_, GL_INFO_LOG_LENGTH, &info_length);
    char* str_info = new char[info_length + 1];
    glGetProgramInfoLog(object_, info_length, NULL, str_info);
    msg += str_info;
    delete [] str_info;
    // Clear program
    glDeleteProgram(object_);
    object_ = 0;
    // throw
    throw ProcessError(ProcessError::ProcessErrorEnum::kErr,
                       msg,
                       FUNC_NAME);
  }
  // Shaders not needed anymore
  shaders_.clear();
}

/*
 *  @name Use
 *  @fn void Use()
 *  @brief  Start to use this program
 */
void OGLProgram::Use() const {
  glUseProgram(object_);
}

/*
 *  @name StopUsing
 *  @fn void StopUsing()
 *  @brief  Stop using this program
 */
void OGLProgram::StopUsing() const {
  assert(this->IsUsing());
  glUseProgram(0);
}

/**
 *  @name IsValid
 *  @fn int IsValid()
 *  @brief  Check if program is valid or not.
 *  @return -1 if not, otherwise
 */
int OGLProgram::IsValid() {
  int valid = 0;
  // Valid program
  int status;
  glValidateProgram(object_);
  glGetProgramiv(object_, GL_VALIDATE_STATUS, &status);
  if (!status) {
    // Somethink went wrong while linking
    msg_ = "Program validation failure : ";
    int info_length;
    glGetProgramiv(object_, GL_INFO_LOG_LENGTH, &info_length);
    char* str_info = new char[info_length + 1];
    glGetProgramInfoLog(object_, info_length, NULL, str_info);
    msg_ += str_info;
    delete [] str_info;
    valid = -1;
  }
  return valid;
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name IsUsing
 *  @fn bool IsUsing()
 *  @brief  Indicate if the current program is being used
 *  @return True if currently used
 */
bool OGLProgram::IsUsing() const {
  int curr_prog = -1;
  glGetIntegerv(GL_CURRENT_PROGRAM, &curr_prog);
  return (curr_prog == static_cast<int>(object_));
}

/*
 *  @name Attrib
 *  @fn int Attrib(const char* attrib_name)
 *  @brief  Provide the attribute index for a given name
 *  @param[in]  attrib_name Name of the attribute to look for
 *  @return The uniform index for the given name, as returned
 *          from glGetUniformLocation.
 */
int OGLProgram::Attrib(const char* attrib_name) const {
  int attrib = -1;
  if (attrib_name != nullptr) {
    attrib = glGetAttribLocation(object_, attrib_name);
  }
  return attrib;
}

/*
 *  @name Uniform
 *  @fn int Uniform(const char* uniform_name)
 *  @brief  Provide uniform reference for a given name
 *  @param[in]  uniform_name  Name of the uniform to look for
 *  @return The uniform index for the given name, as returned
 *          from glGetUniformLocation.
 */
int OGLProgram::Uniform(const char* uniform_name) const {
  int uniform = -1;
  if (uniform_name != nullptr) {
    uniform = glGetUniformLocation(object_, uniform_name);
  }
  return uniform;
}

/*
 * @name  SetUniformBlockBinding
 * @brief Link a uniform block name to a given mounting/binding point
 * @param[in] uniform_name Name of the uniform's block
 * @param[in] binding_point   Binding point
 * @return -1 if error, 0 otherwise
 */
int OGLProgram::SetUniformBlockBinding(const char* uniform_name,
                                       const uint32_t& binding_point) {
  int err = -1;

  uint32_t uBlockIdx = std::numeric_limits<uint32_t>::max();
  if (object_ != 0 && uniform_name != nullptr) {
    // Query uniform block index
    uBlockIdx = glGetUniformBlockIndex(object_, uniform_name);
    // Set binding point
    glUniformBlockBinding(object_, uBlockIdx, binding_point);
    // Sanity check
    err = glGetError() == GL_NO_ERROR ? 0 : -1;
  }
  return err;
}

#define ATTRIB_N_UNIFORM_SETTERS(OGL_TYPE, TYPE_PREFIX, TYPE_SUFFIX) \
void OGLProgram::SetAttrib(const char* name, OGL_TYPE v0) const \
{ assert(IsUsing()); glVertexAttrib ## TYPE_PREFIX ## 1 ## TYPE_SUFFIX (Attrib(name), v0); } \
void OGLProgram::SetAttrib(const char* name, OGL_TYPE v0, OGL_TYPE v1) const \
{ assert(IsUsing()); glVertexAttrib ## TYPE_PREFIX ## 2 ## TYPE_SUFFIX (Attrib(name), v0, v1); } \
void OGLProgram::SetAttrib(const char* name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2) const \
{ assert(IsUsing()); glVertexAttrib ## TYPE_PREFIX ## 3 ## TYPE_SUFFIX (Attrib(name), v0, v1, v2); } \
void OGLProgram::SetAttrib(const char* name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2, OGL_TYPE v3) const \
{ assert(IsUsing()); glVertexAttrib ## TYPE_PREFIX ## 4 ## TYPE_SUFFIX (Attrib(name), v0, v1, v2, v3); } \
\
void OGLProgram::SetAttrib1v(const char* name, const OGL_TYPE* v) const \
{ assert(IsUsing()); glVertexAttrib ## TYPE_PREFIX ## 1 ## TYPE_SUFFIX ## v (Attrib(name), v); } \
void OGLProgram::SetAttrib2v(const char* name, const OGL_TYPE* v) const \
{ assert(IsUsing()); glVertexAttrib ## TYPE_PREFIX ## 2 ## TYPE_SUFFIX ## v (Attrib(name), v); } \
void OGLProgram::SetAttrib3v(const char* name, const OGL_TYPE* v) const \
{ assert(IsUsing()); glVertexAttrib ## TYPE_PREFIX ## 3 ## TYPE_SUFFIX ## v (Attrib(name), v); } \
void OGLProgram::SetAttrib4v(const char* name, const OGL_TYPE* v) const \
{ assert(IsUsing()); glVertexAttrib ## TYPE_PREFIX ## 4 ## TYPE_SUFFIX ## v (Attrib(name), v); } \
\
void OGLProgram::SetUniform(const char* name, OGL_TYPE v0) const \
{ assert(IsUsing()); glUniform1 ## TYPE_SUFFIX (Uniform(name), v0); } \
void OGLProgram::SetUniform(const char* name, OGL_TYPE v0, OGL_TYPE v1) const \
{ assert(IsUsing()); glUniform2 ## TYPE_SUFFIX (Uniform(name), v0, v1); } \
void OGLProgram::SetUniform(const char* name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2) const \
{ assert(IsUsing()); glUniform3 ## TYPE_SUFFIX (Uniform(name), v0, v1, v2); } \
void OGLProgram::SetUniform(const char* name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2, OGL_TYPE v3) const \
{ assert(IsUsing()); glUniform4 ## TYPE_SUFFIX (Uniform(name), v0, v1, v2, v3); } \
\
void OGLProgram::SetUniform1v(const char* name, const OGL_TYPE* v, int count) const \
{ assert(IsUsing()); glUniform1 ## TYPE_SUFFIX ## v (Uniform(name), count, v); } \
void OGLProgram::SetUniform2v(const char* name, const OGL_TYPE* v, int count) const \
{ assert(IsUsing()); glUniform2 ## TYPE_SUFFIX ## v (Uniform(name), count, v); } \
void OGLProgram::SetUniform3v(const char* name, const OGL_TYPE* v, int count) const \
{ assert(IsUsing()); glUniform3 ## TYPE_SUFFIX ## v (Uniform(name), count, v); } \
void OGLProgram::SetUniform4v(const char* name, const OGL_TYPE* v, int count) const \
{ assert(IsUsing()); glUniform4 ## TYPE_SUFFIX ## v (Uniform(name), count, v); }

ATTRIB_N_UNIFORM_SETTERS(float, , f);
ATTRIB_N_UNIFORM_SETTERS(double, , d);
ATTRIB_N_UNIFORM_SETTERS(int, I, i);
ATTRIB_N_UNIFORM_SETTERS(GLuint, I, ui);

template<>
void OGLProgram::SetUniform(const char* name,
                            const Mat2<float>& m,
                            bool transpose) const {
  assert(IsUsing());
  auto transp = transpose ? GL_FALSE : GL_TRUE;  // OpenGL is column major
  glUniformMatrix2fv(Uniform(name), 1, transp, m.Data());
}
template<>
void OGLProgram::SetUniform(const char* name,
                            const Mat2<double>& m,
                            bool transpose) const {
  assert(IsUsing());
  auto transp = transpose ? GL_FALSE : GL_TRUE;  // OpenGL is column major
  glUniformMatrix2dv(Uniform(name), 1, transp, m.Data());
}

template<>
void OGLProgram::SetUniform(const char* name,
                            const Mat3<float>& m,
                            bool transpose) const {
  auto tr = transpose ? GL_FALSE : GL_TRUE;  // OpenGL is column major
  glUniformMatrix3fv(Uniform(name), 1, tr, m.Data());
}
template<>
void OGLProgram::SetUniform(const char* name,
                            const Mat3<double>& m,
                            bool transpose) const {
  assert(IsUsing());
  auto tr = transpose ? GL_FALSE : GL_TRUE;  // OpenGL is column major
  glUniformMatrix3dv(Uniform(name), 1, tr, m.Data());
}

template<>
void OGLProgram::SetUniform(const char* name,
                            const Mat4<float>& m,
                            bool transpose) const {
  assert(IsUsing());
  auto transp = transpose ? GL_FALSE : GL_TRUE;
  glUniformMatrix4fv(Uniform(name), 1, transp, m.Data());
}

template<>
void OGLProgram::SetUniform(const char* name,
                            const Mat4<double>& m,
                            bool transpose) const {
  assert(IsUsing());
  auto transp = transpose ? GL_FALSE : GL_TRUE;
  glUniformMatrix4dv(Uniform(name), 1, transp, m.Data());
}

template<typename T>
void OGLProgram::SetUniform(const char* uniformName,
                            const Vector2<T>& v) const {
  assert(IsUsing());
  SetUniform2v(uniformName, &v.x_);
}

template<typename T>
void OGLProgram::SetUniform(const char* uniformName,
                            const Vector3<T>& v) const {
  assert(IsUsing());
  SetUniform3v(uniformName, &v.x_);
}

template<typename T>
void OGLProgram::SetUniform(const char* uniformName,
                            const Vector4<T>& v) const {
  assert(IsUsing());
  SetUniform4v(uniformName, &v.x_);
}

#pragma mark -
#pragma mark Declaration

/** Vector2 - Integer */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector2<int>& v) const;
/** Vector2 - Unsigned Integer */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector2<unsigned int>& v) const;
/** Vector2 - Float */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector2<float>& v) const;
/** Vector2 - Double */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector2<double>& v) const;

/** Vector3 - Integer */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector3<int>& v) const;
/** Vector3 - Unsigned Integer */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector3<unsigned int>& v) const;
/** Vector3 - Float */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector3<float>& v) const;
/** Vector3 - Double */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector3<double>& v) const;

/** Vector4 - Integer */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector4<int>& v) const;
/** Vector4 - Unsigned Integer */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector4<unsigned int>& v) const;
/** Vector4 - Float */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector4<float>& v) const;
/** Vector4 - Double */
template void OGLProgram::SetUniform(const char* uniformName,
                                     const Vector4<double>& v) const;

}  // namespace LTS5
