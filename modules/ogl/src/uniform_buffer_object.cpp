/**
 *  @file   uniform_buffer_object.cpp
 *  @brief  Uniform Buffer Object (UBO) abstraction
 *  @ingroup ogl
 *  @see https://learnopengl.com/Advanced-OpenGL/Advanced-GLSL
 *
 *  @author Christophe Ecabert
 *  @date   11/28/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include "GL/glew.h"

#include "lts5/ogl/uniform_buffer_object.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  UniformBufferObject
 * @fn    UniformBufferObject()
 * @brief Constructor
 * @param[in] n_buffer Number of buffer to create
 */
UniformBufferObject::UniformBufferObject() : ubo_(nullptr), n_buffer_(0) {}

/*
 * @name  ~UniformBufferObject
 * @fn    ~UniformBufferObject()
 * @brief Destructor
 */
UniformBufferObject::~UniformBufferObject() {
  if (ubo_) {
    glDeleteBuffers(static_cast<GLsizei>(n_buffer_), ubo_);
    delete[] ubo_;
  }
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Create
 * @fn    int Create(const std::vector<UboBindings>& bindings)
 * @brief Create an array of uniform buffer object. The length of the vector
 *        define the number of buffer to allocate.
 *        Each elements are a pair where the first element is the binding
 *        point and the second one is the size of the buffer in bytes
 * @param[in] bindings Individual buffer binding informations
 * @return -1 if error, 0 otherwise
 */
int UniformBufferObject::Create(const std::vector<UboBindings>& bindings) {
  int err = 0;
  if (bindings.size() != n_buffer_) {
    if (ubo_) {
      // Release already allocated buffer
      glDeleteBuffers(static_cast<GLsizei>(n_buffer_), ubo_);
      delete[] ubo_;
      ubo_ = nullptr;
    }
    // Create new buffers
    n_buffer_ = bindings.size();
    ubo_ = new uint32_t[n_buffer_];
    glGenBuffers(static_cast<GLsizei>(n_buffer_), ubo_);
    // Init buffer
    for (size_t k = 0; k < n_buffer_; ++k) {
      // Bind
      this->Bind(k);
      // Define buffer lenght
      const auto& b = bindings[k];
      glBufferData(GL_UNIFORM_BUFFER, b.second, nullptr, /*GL_STATIC_DRAW*/ GL_STREAM_DRAW);
      glBindBufferRange(GL_UNIFORM_BUFFER,
                        (GLuint)b.first,
                        ubo_[k],
                        0,
                        b.second);
      // unbind
      this->Unbind();
      glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }

    // Error check
    err = glGetError() == GL_NO_ERROR ? 0 : -1;
  }
  return err;
}

/*
 * @name  Bind
 * @fn    void Bind(const size_t& idx) const
 * @brief Bind a given buffer
 * @paramþ[in] idx Buffer index
 */
void UniformBufferObject::Bind(const size_t& idx) const {
  if (ubo_ && idx < n_buffer_) {
    glBindBuffer(GL_UNIFORM_BUFFER, ubo_[idx]);
  }
}

/*
 * @name  Unbind
 * @fn    void Unbind() const
 * @brief Unbind current buffer
 */
void UniformBufferObject::Unbind() const {
  glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

/*
 * @name  AddData
 * @fn    int AddData(const size_t& buffer, const size_t& offset,
 *                    const size_t& size, const void* data)
 * @brief Fill buffer with data
 * @param[in] buffer  Buffer index
 * @param[in] offset  Offset within the buffer in bytes (where to start)
 * @param[in] size    Dimensions of the data to be copied (in bytes)
 * @param[in] data    Pointer to the data to be copied
 * @return -1 if error, 0 otherwise
 */
int UniformBufferObject::AddData(const size_t& buffer,
                                 const size_t& offset,
                                 const size_t& size,
                                 const void* data) {
  // Bind buffer
  this->Bind(buffer);
  // Push data
  glBufferSubData(GL_UNIFORM_BUFFER, (GLintptr)offset, (GLsizeiptr)size, data);
  // Unbind
  this->Unbind();
  // error check
  return glGetError() == GL_NO_ERROR ? 0 : -1;
}

}  // namespace LTS5
