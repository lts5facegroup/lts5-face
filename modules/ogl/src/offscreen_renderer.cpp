/**
 *  @file   offscreen_renderer.cpp
 *  @brief  OpenGL offscreen renderer
 *          For debug purpose, one can define DEBUG_RENDERER variable to display
 *          on screen what is actually rendered by OpenGL pipeline
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   14/07/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <chrono>

#include "GL/glew.h"

#include "lts5/utils/logger.hpp"
#include "lts5/ogl/offscreen_renderer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  OGLOffscreenRenderer
 * @fn    OGLOffscreenRenderer()
 * @brief Constructor
 */
template<typename T, typename ColorContainer>
OGLOffscreenRenderer<T, ColorContainer>::
OGLOffscreenRenderer() : ctx_(new OGLContext()),
                         width_(0),
                         height_(0),
                         buffer_(nullptr),
                         clear_needed_(true),
                         visible_(false) {
}

/*
 * @name  OGLOffscreenRenderer
 * @fn    OGLOffscreenRenderer(const int& width,
                               const int& height,
                               const bool& visible,
                               const bool& visible,
                               const ImageType& type)
 * @brief Constructor
 * @param[in] width   View's width
 * @param[in] height  View's height
 * @param[in] m_sample    Number of multisampling for antialiasing. A value
 *                        of `0` indicates without antialiasing
 * @param[in] visible Flag to indicate if window is displayed on the screen
 * @param[in] type    Type of the the image
 */
template<typename T, typename ColorContainer>
OGLOffscreenRenderer<T, ColorContainer>::
OGLOffscreenRenderer(const int& width,
                     const int& height,
                     const int& m_sample,
                     const bool& visible,
                     const ImageType& type) : ctx_(new OGLContext()),
                                              width_(width),
                                              height_(height),
                                              buffer_(nullptr),
                                              clear_needed_(true),
                                              visible_(visible) {
  ctx_->Initialize(width, height, m_sample, visible);
}

/*
 * @name  ~OGLOffscreenRenderer
 * @fn  ~OGLOffscreenRenderer()
 * @brief Destructor
 */
template<typename T, typename ColorContainer>
OGLOffscreenRenderer<T, ColorContainer>::~OGLOffscreenRenderer() {
  // Clear color texture
  if (buffer_) {
    delete buffer_;
    buffer_ = nullptr;
  }
  if (ctx_) {
    delete ctx_;
  }
}

/*
 * @name  Init
 * @fn  int Init(const int& width,
                 const int& height,
                 const bool& visible,
                 const ImageType& type)
 * @brief Initialize offscreen renderer (FBO + Buffer)
 * @param[in] width  Width of the image
 * @param[in] height  Heightof the image
 * @param[in] visible Flag to indicate if window is displayed on the screen
 * @param[in] type  Type of the the image
 * @return  -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLOffscreenRenderer<T, ColorContainer>::Init(const int& width,
                                                  const int& height,
                                                  const int& m_sample,
                                                  const bool& visible,
                                                  const ImageType& type) {
  int err = -1;
  visible_ = visible;
  err = ctx_->Initialize(width, height, m_sample, visible);
  if (err == 0) {
    // Setup error callback
    ctx_->SetErrorCallbackFunction(GLFWErrorCallback);
    // FBO
    // ---------------------------------------------
    if (!buffer_) {
      buffer_ = new OGLIOBuffer();
    }
    if (type == ImageType::kGrayscale) {
      err = buffer_->Init(width, height, m_sample, true,
                          OGLIOBuffer::BufferType::kR);
    } else if (type == ImageType::kRGB) {
      err = buffer_->Init(width, height, m_sample,  true,
                          OGLIOBuffer::BufferType::kRGB);
    } else if (type == ImageType::kRGBf) {
      err = buffer_->Init(width, height, m_sample,  true,
                          OGLIOBuffer::BufferType::kRGBf);
    } else if (type == ImageType::kRGBA) {
      err = buffer_->Init(width, height, m_sample,  true,
                          OGLIOBuffer::BufferType::kRGBA);
    } else if (type == ImageType::kRGBAf) {
      err = buffer_->Init(width, height, m_sample,  true,
                          OGLIOBuffer::BufferType::kRGBAf);
    } else {
      err = -1;
    }
  } else {
    LTS5_LOG_ERROR("Unable to create GLFW window");
  }
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name ActiveContext
 *  @fn void ActiveContext() const
 *  @brief  Make internal OpenGL context active
 */
template<typename T, typename ColorContainer>
void OGLOffscreenRenderer<T, ColorContainer>::ActiveContext() const {
  // LTS5_LOG_INFO("Activate context");
  ctx_->ActiveContext();
  // TODO (christophe): Figure out solution
  // Sometime `ActiveContext` produce GL_INVALID_OPERATION. So will clear it
  // in order to have proper error management later on. This might introduce
  // bugs along the way ...
  glGetError();
}

/*
 *  @name DisableContext
 *  @fn void DisableContext() const
 *  @brief  Detach current OpenGL context
 */
template<typename T, typename ColorContainer>
void OGLOffscreenRenderer<T, ColorContainer>::DisableContext() const {
  // LTS5_LOG_INFO("Deactivate context");
  ctx_->DisableContext();
}

/*
 * @name  Render
 * @fn  void Render()
 * @brief Render into OpenGL context
 */
template<typename T, typename ColorContainer>
void OGLOffscreenRenderer<T, ColorContainer>::Render(const PrimitiveType& type,
                                                     const size_t& count,
                                                     const size_t& elem_offset) {
  // Bind frame buffer
  buffer_->BindForWriting();
  if (clear_needed_) {
    this->ClearBuffer();
  }
  // Render
  OGLBaseRenderer<T, ColorContainer>::Render(type, count, elem_offset);
  buffer_->BlitBuffer();
  // Unbind
  buffer_->Unbind();
  // Trigger rendering, don't use swapbuffer since FBO is not double buffered!
  glFlush();
  if (visible_) {
    if (clear_needed_) {
      this->ClearBuffer();
    }
    // Render
    OGLBaseRenderer<T, ColorContainer>::Render(type, count, elem_offset);
    ctx_->SwapBuffer();
  }
}

/*
 * @name  AddMesh
 * @fn  void AddMesh(const Mesh<T>& mesh)
 * @brief Add geometry to render
 * @param[in] mesh
 */
template<typename T, typename ColorContainer>
void OGLOffscreenRenderer<T, ColorContainer>::AddMesh(const Mesh& mesh) {
  this->mesh_ = const_cast<Mesh*>(&mesh);
}

/*
 * @name  AddProgram
 * @fn  void AddProgram(const OGLProgram& program)
 * @brief Add OpenGL program to tell how to draw a given mesh
 * @param[in] program
 */
template<typename T, typename ColorContainer>
void OGLOffscreenRenderer<T, ColorContainer>::AddProgram(const OGLProgram& program) {
  this->set_program(&program);
}

/*
 * @name  AddTexture
 * @fn  void AddTexture(const OGLTexture& texture)
 * @brief Add OpenGL texture (used to "paint" geometry)
 * @param[in] texture
 */
template<typename T, typename ColorContainer>
void OGLOffscreenRenderer<T, ColorContainer>::AddTexture(const OGLTexture& texture) {
  this->texture_ = &texture;
}

/*
 * @name  GetImage
 * @fn  int GetImage(cv::Mat* image) const
 * @brief Write the last rendered image into \p image
 * @param[out]  image Last rendered image
 * @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLOffscreenRenderer<T, ColorContainer>::GetImage(cv::Mat* image) const {
  int err = -1;
  if (buffer_) {
    err = buffer_->ReadColorBuffer(image);
  }
  return err;
}

/*
 * @name  GetImage
 * @fn  int GetImage(void* buffer) const
 * @brief Write the last rendered image into \p image
 * @param[out]  buffer Location where to write the image. The buffer needs to
 *                     be allocated with the proper size
 * @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLOffscreenRenderer<T, ColorContainer>::GetImage(void* buffer) const {
  int err = -1;
  if (buffer_) {
    err = buffer_->ReadColorBuffer(buffer);
  }
  return err;
}

/*
 * @name  GetDepth
 * @fn  int GetDepth(cv::Mat* depth_map) const
 * @brief Write the last rendered depth buffer into \p depth_map
 * @param[out]  depth_map Depth map
 * @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLOffscreenRenderer<T, ColorContainer>::GetDepth(cv::Mat* depth_map) const {
  int err = -1;
  if (buffer_) {
    err = buffer_->ReadDepthBuffer(depth_map);
  }
  return err;
}

/*
 * @name  GLFWErrorCallback
 * @fn  static void GLFWErrorCallback(int error, const char* description)
 * @brief GLFW error callbacks
 * @param[in] error         Error number
 * @param[in] description   Error message
 */
template<typename T, typename ColorContainer>
void OGLOffscreenRenderer<T, ColorContainer>::GLFWErrorCallback(int error,
                                                const char* description) {
  LTS5_LOG_ERROR("GLFW error " << error << " - " << description);
  exit(0);
}


#pragma mark -
#pragma mark Declaration

/** Float - Vector3*/
template class OGLOffscreenRenderer<float, Vector3<float>>;
/** Float - Vector4*/
template class OGLOffscreenRenderer<float, Vector4<float>>;
/** Double - Vector3 */
template class OGLOffscreenRenderer<double, Vector3<double>>;
/** Double - Vector4 */
template class OGLOffscreenRenderer<double, Vector4<double>>;

}  // namepsace LTS5
