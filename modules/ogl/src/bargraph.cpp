/**
 *  @file   bargraph.cpp
 *  @brief  Draw bargraph into OpenGL context
 *
 *  @author Christophe Ecabert
 *  @date   24/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/ogl/bargraph.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @struct  OGLBar
 * @brief   Attribute for one bar
 */
struct OGLBarGraph::OGLBar {
  /** Label */
  std::string label;
  /** value */
  float value;
  /** X position */
  float x;
  /** Y position */
  float y;
  /** Width */
  float width;
  /** Height */
  float height;
  /** Label X position */
  float label_x;
  /** Label Y position */
  float label_y;
  /** Label width */
  float label_width;
  /** Label height position */
  float label_height;
  /** Label scale */
  float label_scale;
  /** Label padding top/bottom */
  float label_pad_y;
  /** Label Color */
  Color label_color;

  OGLBar() : label(""),
             value(0.0f),
             x(0.f),
             y(0.f),
             width(0.f),
             height(0.f),
             label_x(0.f),
             label_y(0.f),
             label_width(0.f),
             label_height(0.f),
             label_scale(1.f),
             label_pad_y(5.f),
             label_color(1.f, 1.f, 1.f, 1.f) {}
};

/*
 * @name  OGLBarGraph
 * @fn  OGLBarGraph()
 * @brief Constructor
 */
OGLBarGraph::OGLBarGraph() : OGLBaseRenderer::OGLBaseRenderer(),
                             x_(0.f),
                             y_(0.f),
                             width_(0.f),
                             height_(0.f),
                             bar_pad_(0.f),
                             label_m_width_(0.f),
                             label_m_height_(0.f),
                             recompute_anchor_(true),
                             orientation_(kVertical),
                             text_renderer_(nullptr){
  using Vec4f = Vector4<float>;
  // Mesh
  this->mesh_ = new Mesh();
  // Shader
  const std::string vert_shader_str = "#version 330 core\n"
          "layout (location = 0) in vec3 position;\n"
          "uniform vec4 color;\n"
          "uniform mat4 camera;\n"
          "out vec4 color0;\n"
          "void main() {\n"
          "  gl_Position = camera * vec4(position, 1.f);\n"
          "  color0 = color;\n"
          "}";
  const std::string frag_shader_str = "#version 330 core\n"
          "in vec4 color0;\n"
          "out vec4 fragColor;\n"
          "void main() {    \n"
          "  fragColor = color0;\n"
          "}";
  std::vector<OGLShader> shader;
  shader.emplace_back(vert_shader_str, OGLShader::ShaderType::kVertex);
  shader.emplace_back(frag_shader_str, OGLShader::ShaderType::kFragment);
  // Program
  using ProgAtt = LTS5::OGLProgram::Attributes;
  using ProgTyp = LTS5::OGLProgram::AttributeType;
  std::vector<ProgAtt> attrib;
  attrib.emplace_back(ProgTyp::kPoint, 0, "position");
  this->program_ = new OGLProgram(shader, attrib);
  // Set defaut color to white
  this->program_->Use();
  this->program_->SetUniform("color", Vec4f(1.f, 1.f, 1.f, 1.f));
  this->program_->StopUsing();
  // TextRenderer
  text_renderer_ = new OGLTextRenderer();
}

/*
 * @name  ~OGLBarGraph
 * @fn  virtual ~OGLBarGraph()
 * @brief Destructor
 */
OGLBarGraph::~OGLBarGraph() {
  if (this->program_) {
    delete this->program_;
    this->program_ = nullptr;
  }
  if (this->mesh_) {
    delete this->mesh_;
    this->mesh_ = nullptr;
  }
  if (text_renderer_) {
    delete text_renderer_;
    text_renderer_ = nullptr;
  }
}

/*
 * @name  SetupBar
 * @fn  void SetupView(const int x,
                       const int y,
                       const int width,
                       const int height,
                       const int n_bar)
 * @brief Setup view position/dimension and number of bin
 * @param[in] x       View X position
 * @param[in] y       View Y position
 * @param[in] width   View width
 * @param[in] height  View height
 * @param[in] n_bar   Number of bar
 */
void OGLBarGraph::SetupView(const int x,
                            const int y,
                            const int width,
                            const int height,
                            const int n_bar) {
  // Pos
  x_ = static_cast<float>(x);
  y_ = static_cast<float>(y);
  // Set dimension
  width_ = static_cast<float>(width);
  height_ = static_cast<float>(height);
  // Init bar struct
  bars_.resize(static_cast<std::size_t >(n_bar));
  // Compute delta
  delta_bar_ = orientation_ == kVertical ?
               width_ / static_cast<float>(n_bar + 1) : height /
                       static_cast<float>(n_bar + 1);
  // Init mesh
  this->mesh_->get_vertex().resize(static_cast<std::size_t>(n_bar * 4));
  this->mesh_->get_triangle().resize(static_cast<std::size_t>(n_bar * 2));
  // Indicate refresh
  recompute_anchor_ = true;
}

/*
 *  @name BindToOpenGL
 *  @fn virtual int BindToOpenGL();
 *  @brief  Upload data into OpenGL context
 *  @return -1 if error, 0 otherwise
 */
int OGLBarGraph::BindToOpenGL() {
  // Compute anchors ?
  if (recompute_anchor_) {
    this->ComputeBarProperty();
  }
  // Update text labels
  this->UpdateText();
  // Update vertex
  this->UpdateMeshVertex();
  // Bind
  return OGLBaseRenderer::BindToOpenGL();
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Render
 * @fn  void Render()
 * @brief Render into OpenGL context
 */
void OGLBarGraph::Render() {
  // Render text ?
  if (!bars_[0].label.empty()) {
      text_renderer_->Render();
  }
  OGLBaseRenderer::Render();
}

#pragma mark -
#pragma mark Accessors

/*
 * @name  SetWindowDimension
 * @fn  void SetWindowDimension(const int width, const int height)
 * @brief Set dimension of the window to render in
 * @param[in] width   Window width
 * @param[in] height  Window height
 * @return -1 if error, 0 otherwise
 */
void OGLBarGraph::SetWindowDimension(const int width, const int height) {
  using Mat4f = Matrix4x4<float>;
  // Setup projection matrix
  auto proj = Mat4f::Ortho(0.f,
                           static_cast<float>(width),
                           static_cast<float>(height),
                           0.f);
  this->program_->Use();
  this->program_->SetUniform("camera", proj);
  this->program_->StopUsing();
  // Set for text render
  text_renderer_->SetWindowDimension(width, height);
}

/*
 * @name  SetFont
 * @fn  int SetFont(const std::string& font)
 * @brief Set label's font type
 * @param[in] font    Path to font file (i.e. arial.ttf), if empty,
 *                    load default Arial font
 * @return -1 if error, 0 otherwise
 */
int OGLBarGraph::SetFont(const std::string& font) {
  int err = text_renderer_->GenerateAtlas(font);
  if (!err) {
    label_m_width_ = 0.f;
    label_m_height_ = 0.f;
    for (const OGLBar& bar : bars_) {
      if (!bar.label.empty()) {
        float w, h;
        text_renderer_->QueryTextDimension(bar.label, 1.f, &w, &h);
        label_m_width_ = std::max(label_m_width_, w);
        label_m_height_ = std::max(label_m_height_, h);
      }
    }
    recompute_anchor_ = true;
  }
  return err;
}

/*
 * @name  SetLabel
 * @fn  void SetLabel(const std::vector<std::string>& labels)
 * @brief Set \p labels for all bars
 * @param[in] labels   List of labels
 */
void OGLBarGraph::SetLabel(const std::vector<std::string>& labels) {
  assert(labels.size() == bars_.size());
  std::size_t n = std::max(labels.size(), bars_.size());
  bool has_font = true;
  label_m_width_ = 0.f;
  label_m_height_ = 0.f;
  for (std::size_t i = 0; i < n; ++i) {
    bars_[i].label = labels[i];
    if (has_font) {
      float w, h;
      if(!text_renderer_->QueryTextDimension(labels[i], 1.f, &w, &h)) {
        label_m_width_ = std::max(label_m_width_, w);
        label_m_height_ = std::max(label_m_height_, h);
        bars_[i].label_width = w;
        bars_[i].label_height = h;
        bars_[i].label_scale = 1.f;
      } else {
        // Init default
        int err = text_renderer_->GenerateAtlas("");
        has_font = err == 0;
        i -= 1;
      }
    }
  }
  recompute_anchor_ = true;
}

/*
 * @name  SetBarOrientation
 * @fn  void SetBarOrientation(const BarOrientationType orientation)
 * @brief Set bar's orientation
 * @param[in] orientation   Bar's orientation
 */
void OGLBarGraph::SetBarOrientation(const BarOrientationType orientation) {
  orientation_ = orientation;
  recompute_anchor_ = true;
}

/*
 * @name  SetColor
 * @fn  SetColor(const Color& color)
 * @brief Set bar's color. Range should lie in [0,1]
 * @param[in] color   Bar's color
 */
void OGLBarGraph::SetColor(const Color& color) {
  LTS5::Vector4<float> colorf;
  colorf.x_ = static_cast<float>(color.x_) / 255.f;
  colorf.y_ = static_cast<float>(color.y_) / 255.f;
  colorf.z_ = static_cast<float>(color.z_) / 255.f;
  colorf.w_ = static_cast<float>(color.w_) / 255.f;
  this->program_->Use();
  this->program_->SetUniform("color", colorf);
  this->program_->StopUsing();
  for (OGLBar& bar : bars_) {
    bar.label_color = color / 255.f;
  }
  recompute_anchor_ = true;
}

/*
 * @name  SetValue
 * @fn  void SetValue(const std::vector<float>& values)
 * @brief Update bar's \p values
 * @param[in] values  Bar's value (percentage)
 */
void OGLBarGraph::SetValue(const std::vector<float>& values) {
  std::size_t n = std::max(bars_.size(), values.size());
  // Loop over value
  for (std::size_t i = 0; i < n; ++i) {
    // Update value
    bars_[i].value = values[i];
    // Update Width / height
    if (orientation_ == kVertical) {
      bars_[i].height = (bars_[i].y - y_) * bars_[i].value;
    } else {
      bars_[i].width = (x_ + width_ - bars_[i].x) * bars_[i].value;
    }
  }
  this->UpdateMeshVertex();
  this->UpdateVertex();
}

#pragma mark -
#pragma mark Private

/*
 * @name  ComputeBarProperty
 * @fn    void ComputeBarProperty()
 * @brief Compute bar properties
 */
void OGLBarGraph::ComputeBarProperty() {
  // Label ?
  if (label_m_height_ != 0.f && label_m_width_ != 0.f) {
    bar_pad_ = orientation_ == kVertical ? label_m_height_ : label_m_width_;
  } else {
    bar_pad_ = orientation_ == kVertical ? 0.1f * height_ : 0.1f * width_;
  }
  // Loop over all bars
  float bar_x = orientation_ == kVertical ? x_ + delta_bar_ : x_ + bar_pad_;
  float bar_y = orientation_ == kVertical ? y_ + height_ - bar_pad_ : y_ + delta_bar_;
  for (OGLBar& bar : bars_) {
    // Define bar position
    bar.x = bar_x;
    bar.y = bar_y;
    // Define height
    if (orientation_ == kVertical) {
      bar.width = 10.f;
      bar.height = (bar_y - y_) * bar.value;
    } else {
      bar.height = 10.f;
      bar.width = (x_ + width_ - bar_x) * bar.value;
    }
    // Text ?
    if (!bar.label.empty()) {
      bar.label_scale = bar_pad_ / (bar_pad_ + 2.f * bar.label_pad_y);
      text_renderer_->QueryTextDimension(bar.label,
                                         bar.label_scale,
                                         &bar.label_width,
                                         &bar.label_height);
      // Define position
      if (orientation_ == kVertical) {
        bar.label_x = bar_x - (bar.label_width / 2.f);
        bar.label_y = bar_y + bar.label_pad_y;
      } else {
        //bar.label_x = bar_x - (bar.label_width + bar.label_pad_y);
        bar.label_x = x_ + bar.label_pad_y;
        bar.label_y = bar_y - (bar.label_height / 2.f);
      }
    }
    // Increase position
    if (orientation_ == kVertical) {
      bar_x += delta_bar_;
    } else {
      bar_y += delta_bar_;
    }
  }
  recompute_anchor_ = false;
}

/*
 * @name  UpdateText
 * @fn  void UpdateText()
 * @brief Update text into renderer
 */
void OGLBarGraph::UpdateText() {
  bool text = false;
  for (const OGLBar& bar : bars_) {
    if (!bar.label.empty()) {
      text = true;
      // Add to renderer
      text_renderer_->AddText(bar.label,
                              bar.label_x,
                              bar.label_y,
                              bar.label_scale,
                              bar.label_color,
                              -1);
    }
  }
  if (text) {
    text_renderer_->BindToOpenGL();
  }
}

/*
 * @name  UpdateMeshVertex
 * @fn  void UpdateMeshVertex()
 * @brief Update vertex position in the mesh
 */
void OGLBarGraph::UpdateMeshVertex() {
  std::vector<Vertex>& vertex = this->mesh_->get_vertex();
  std::vector<Triangle>& tri = this->mesh_->get_triangle();
  int n_vert = 0;
  int n_tri = 0;
  for (const OGLBar& bar : bars_) {
    float x = bar.x;
    float y = bar.y;
    float dx, dy;
    if (orientation_ == kVertical) {
      dx = bar.width / 2.f;
      dy = bar.height;
      vertex[n_vert] = Vertex(x - dx, y - dy, 0.f);
      vertex[n_vert + 1] = Vertex(x - dx, y, 0.f);
      vertex[n_vert + 2] = Vertex(x + dx, y, 0.f);
      vertex[n_vert + 3] = Vertex(x + dx, y - dy, 0.f);
    } else {
      dx = bar.width;
      dy = bar.height / 2.f;
      vertex[n_vert] = Vertex(x, y + dy, 0.f);
      vertex[n_vert + 1] = Vertex(x, y - dy, 0.f);
      vertex[n_vert + 2] = Vertex(x + dx, y - dy, 0.f);
      vertex[n_vert + 3] = Vertex(x + dx, y + dy, 0.f);
    }
    tri[n_tri] = Triangle(n_vert, n_vert + 1, n_vert + 2);
    tri[n_tri + 1] = Triangle(n_vert, n_vert + 2, n_vert + 3);
    n_tri += 2;
    n_vert += 4;
  }
}




}  // namepsace LTS5
