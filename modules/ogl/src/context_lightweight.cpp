/**
 *  @file   context_lightweight.cpp
 *  @brief  
 *  @ingroup 
 *
 *  @author Christophe Ecabert
 *  @date   11/11/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <iomanip>
#include <cstring>

#define GLEW_NO_GLU
#include "GL/glew.h"

#include "lts5/ogl/context_lightweight.hpp"
#include "platform/internal.hpp"

// ---- MAC ----

#pragma mark -
#pragma mark Platform dependant implementation (Mac)

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  OGLContextLightweight
 * @fn    OGLContextLightweight()
 * @brief Constructor
 */
OGLContextLightweight::OGLContextLightweight() : ctx_(nullptr) {
  createGLContext(&ctx_);
}

/*
 * @name  OGLContextLightweight
 * @fn    OGLContextLightweight(bool activate_ctx)
 * @brief Constructor
 * @param[in] activate_ctx If `True` will directly active the context,
 *    otherwise no.
 */
OGLContextLightweight::
OGLContextLightweight(bool activate_ctx) : OGLContextLightweight() {
  if (activate_ctx) {
    this->ActiveContext();
  }
}

/**
 * @name  ~OGLContextLightweight
 * @fn    ~OGLContextLightweight()
 * @brief Destructor
 */
OGLContextLightweight::~OGLContextLightweight() {
  destroyGLContext(ctx_);
}

#pragma mark -
#pragma mark Usage

/*
 * @name Version
 * @brief Provide opengl version in use.
 * @param[out] major  Version major
 * @param[out] minor  Version minor
 */
void OGLContextLightweight::Version(int* major, int* minor) const {
  glGetIntegerv(GL_MAJOR_VERSION, major);
  glGetIntegerv(GL_MINOR_VERSION, minor);
  glGetError(); // Clear possible GL_INVALID_ENUM error in version query.
}

/*
 * @name  ActiveContext
 * @fn    void ActiveContext() const
 * @brief Make this context the current one
 */
void OGLContextLightweight::ActiveContext() const {
  setGLContext(ctx_);
}

/*
 * @name  DisableContext
 * @fn    void DisableContext() const
 * @brief Detach from current context
 */
void OGLContextLightweight::DisableContext() const {
  releaseGLContext();
}


}  // namespace LTS5