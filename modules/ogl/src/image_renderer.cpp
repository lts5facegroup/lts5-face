/**
 *  @file   image_renderer.cpp
 *  @brief  2D Renderer for image drawing
 *
 *  @author Christophe Ecabert
 *  @date   19/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/ogl/image_renderer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name  OGLImageRenderer
 * @fn  OGLImageRenderer()
 * @brief Constructor
 */
OGLImageRenderer::OGLImageRenderer() {
  // Create mesh, -> square (vertex, tcoord, tri)
  this->mesh_ = new LTS5::Mesh<float>();
  std::vector<Vertex>& vertex = this->mesh_->get_vertex();
  std::vector<TCoord >& tcoord = this->mesh_->get_tex_coord();
  std::vector<Triangle >& tri = this->mesh_->get_triangle();
  // Vertex
  vertex.emplace_back(Vertex(0.f, 0.f, 0.f));
  vertex.emplace_back(Vertex(1.f, 0.f, 0.f));
  vertex.emplace_back(Vertex(0.f, 1.f, 0.f));
  vertex.emplace_back(Vertex(1.f, 1.f, 0.f));
  // Tex coordinate
  tcoord.emplace_back(Vector2<float>(0.f, 0.f));
  tcoord.emplace_back(Vector2<float>(1.f, 0.f));
  tcoord.emplace_back(Vector2<float>(0.f, 1.f));
  tcoord.emplace_back(Vector2<float>(1.f, 1.f));
  // Triangle
  tri.emplace_back(Triangle(0, 1, 2));
  tri.emplace_back(Triangle(1, 3, 2));

  // Create shader
  const std::string vertex_shader_str = "#version 330\n"
          "layout (location = 0) in vec3 position;\n"
          "layout (location = 1) in vec2 texCoord;\n"
          "uniform mat4 camera;\n"
          "uniform mat4 model;\n"
          "out vec2 texCoord0;\n"
          "void main() {\n"
          "  gl_Position = camera * model * vec4(position, 1);\n"
          "  texCoord0 = texCoord;\n"
          "}";
  const std::string frag_shader_str = "#version 330\n"
          "in vec2 texCoord0;\n"
          "uniform sampler2D tex_sampler;\n"
          "uniform float gray_flag;\n"
          "out vec4 fragColor;\n"
          "void main() {\n"
          "  if (gray_flag < 0.5f) {\n"
          "    fragColor = vec4(texture(tex_sampler, texCoord0).rgb, 1.f);\n"
          "  } else {\n"
          "    float pix = texture(tex_sampler, texCoord0).r;\n"
          "    fragColor = vec4(pix, pix, pix, 1.f);\n"
          "  }\n"
          "}";
  std::vector<OGLShader> shader;
  shader.emplace_back(vertex_shader_str, OGLShader::ShaderType::kVertex);
  shader.emplace_back(frag_shader_str, OGLShader::ShaderType::kFragment);
  // Create program
  using ProgAtt = LTS5::OGLProgram::Attributes;
  using ProgTyp = LTS5::OGLProgram::AttributeType;
  std::vector<ProgAtt> attrib;
  attrib.emplace_back(ProgTyp::kPoint, 0, "position");
  attrib.emplace_back(ProgTyp::kTexCoord, 1, "texCoord");
  this->program_ = new OGLProgram(shader, attrib);
}

/*
 * @name  ~OGLImageRenderer
 * @fn  ~OGLImageRenderer()
 * @brief Destructor
 */
OGLImageRenderer::~OGLImageRenderer() {
  if (this->mesh_) {
    delete this->mesh_;
    this->mesh_ = nullptr;
  }
  if (this->program_) {
    delete this->program_;
    this->program_ = nullptr;
  }
}

/*
 * @name  AddTexture
 * @fn  void AddTexture(const OGLTexture* texture)
 * @brief Add OpenGL texture (used to "paint" geometry)
 * @param[in] texture
 */
void OGLImageRenderer::AddTexture(const OGLTexture* texture) {
  this->texture_ = const_cast<OGLTexture*>(texture);
  this->program_->Use();
  float flag = texture_->is_grayscale() ? 1.f : 0.f;
  this->program_->SetUniform("gray_flag", flag);
  this->program_->SetUniform("tex_sampler", 0);
  this->program_->StopUsing();
}

/*
 * @name  SetWindowDimension
 * @fn  void SetWindowDimension(const int& width, const int& height)
 * @brief Set the dimension of the window to render inside
 * @param[in] width   Window width
 * @param[in] height  Window height
 */
void OGLImageRenderer::SetWindowDimension(const int& width, const int& height) {
  using Mat4f = Matrix4x4<float>;
  // Setup projection matrix
  auto proj = Mat4f::Ortho(0.f,
                           static_cast<float>(width),
                           static_cast<float>(height),
                           0.f);
  this->program_->Use();
  this->program_->SetUniform("camera", proj);
  this->program_->StopUsing();
}

/*
 * @name  SetupView
 * @fn  void SetupView(const int& x,
                       const int& y,
                       const int& width,
                       const int& height,
                       const int& n_bar);
 * @brief Setup bargraph view dimension and number of bin
 * @param[in] x       View X position
 * @param[in] y       View Y position
 * @param[in] width   View width
 * @param[in] height  View height
 * @param[in] n_bar   Number of bar
 */
void OGLImageRenderer::SetupView(const int& x,
                                 const int& y,
                                 const int& width,
                                 const int& height) {
  using Mat4f = Matrix4x4<float>;
  auto sw = static_cast<float>(width);
  auto sh = static_cast<float>(height);
  Mat4f model;
  model(0, 0) = sw;
  model(1, 1) = sh;
  model(3, 3) = 1.f;
  float tx = static_cast<float>(x) / sw;
  float ty = static_cast<float>(y) / sh;
  model(0, 3) = tx;
  model(1, 3) = ty;
  model(2, 3) = 0.f;
  this->program_->Use();
  this->program_->SetUniform("model", model);
  this->program_->StopUsing();
}

/*
 * @name  UpdateVertex
 * @fn  int UpdateVertex()
 * @brief Update vertex position into OpenGL context
 * @return  -1 if error, 0 otherwise
*/
int OGLImageRenderer::UpdateVertex() {
  return -1;
}

/*
 * @name  UpdateNormal
 * @fn  int UpdateNormal()
 * @brief Update normal into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
int OGLImageRenderer::UpdateTCoord() {
  return -1;
}
}  // namepsace LTS5
