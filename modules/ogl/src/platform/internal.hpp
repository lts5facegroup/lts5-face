/**
 *  @file   platform/internal.hpp
 *  @brief  Platform specific function handling opengl context
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   11/11/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_OGL_PLATFORM_INTERNAL__
#define __LTS5_OGL_PLATFORM_INTERNAL__

struct GLContext;

/**
 * @name    setGLContext
 * @brief   Make given context active
 * @param[in] ctx   Context
 */
void setGLContext(GLContext* ctx);

/**
 * @name    releaseGLContext
 * @brief   Release the current active context
 */
void releaseGLContext();

/**
 * @name    createGLContext
 * @brief Create an OPenGL context
 * @param[out] ctx Context
 */
void createGLContext(GLContext** ctx);

/**
 * @name    destroyGLContext
 * @brief   Destroy a given context
 * @param[in] ctx   Context to destroy
 */
void destroyGLContext(GLContext* ctx);

#endif //__LTS5_OGL_PLATFORM_INTERNAL__
