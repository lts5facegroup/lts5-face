/**
 *  @file   platform/linux_context.cpp
 *  @brief  Linux Platform function handling opengl context
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   11/11/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <cstring>

#include "internal.hpp"
#include "lts5/utils/logger.hpp"

#pragma mark -
#pragma mark Platform dependant implementation (Linux)

#define GLEW_NO_GLU
// X11/Xlib.h has "#define Status int" which breaks Tensorflow. Avoid it.
#define EGL_NO_X11
#define MESA_EGL_NO_X11_HEADERS
#include "GL/glew.h"
#include <EGL/egl.h>
#include <GL/gl.h>

struct GLContext {

  GLContext(EGLDisplay disp,
            EGLSurface surf,
            EGLContext ctx) : display(disp),
                              surface(surf),
                              context(ctx),
                              glewInitialized(0) {}

  EGLDisplay  display;
  EGLSurface  surface;
  EGLContext  context;
  int         glewInitialized;
};

void setGLContext(GLContext* ctx) {
  if (!ctx->context) {
    LTS5_LOG_ERROR("setGLContext() called with null `ctx`");
  }

  if (!eglMakeCurrent(ctx->display, ctx->surface, ctx->surface, ctx->context)) {
    LTS5_LOG_ERROR("eglMakeCurrent() failed when setting GL context");
  }

  if (ctx->glewInitialized) {
    return;
  }

  GLenum result = glewInit();
  if (result != GLEW_OK) {
    LTS5_LOG_ERROR("glewInit() failed:" << glewGetErrorString(result));
  }
  // Get glew version
  LTS5_LOG_DEBUG("Glew Version: " << GLEW_VERSION_MAJOR << "."
                                  << GLEW_VERSION_MINOR << "."
                                  << GLEW_VERSION_MICRO);
  LTS5_LOG_DEBUG("OpenGL Version: " << glGetString(GL_VERSION));
  LTS5_LOG_DEBUG("Vendor: " << glGetString(GL_VENDOR));
  LTS5_LOG_DEBUG("Renderer: " << glGetString(GL_RENDERER));
  ctx->glewInitialized = 1;
}

void releaseGLContext() {
  EGLDisplay disp = eglGetCurrentDisplay();
  if (disp == EGL_NO_DISPLAY) {
    LTS5_LOG_WARNING("releaseGLContext() called with no active display");
  }
  if (!eglMakeCurrent(disp, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT)) {
    LTS5_LOG_ERROR("eglMakeCurrent() failed when releasing GL context");
  }
}

void createGLContext(GLContext** ctx) {
  // Initialize.
  EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
  if (display == EGL_NO_DISPLAY) {
    LTS5_LOG_ERROR("eglGetDisplay() failed");
  }
  EGLint major;
  EGLint minor;
  if (!eglInitialize(display, &major, &minor)) {
    LTS5_LOG_ERROR("eglInitialize() failed");
  }
  // Choose configuration.
  const EGLint context_attribs[] = {
          EGL_RED_SIZE,           8,
          EGL_GREEN_SIZE,         8,
          EGL_BLUE_SIZE,          8,
          EGL_ALPHA_SIZE,         8,
          EGL_DEPTH_SIZE,         24,
          EGL_STENCIL_SIZE,       8,
          EGL_RENDERABLE_TYPE,    EGL_OPENGL_BIT,
          EGL_SURFACE_TYPE,       EGL_PBUFFER_BIT,
          EGL_NONE
  };

  EGLConfig config;
  EGLint num_config;
  if (!eglChooseConfig(display, context_attribs, &config, 1, &num_config)) {
    LTS5_LOG_ERROR("eglChooseConfig() failed");
  }

  // Create dummy pbuffer surface.
  const EGLint surface_attribs[] = {
          EGL_WIDTH,      1,
          EGL_HEIGHT,     1,
          EGL_NONE
  };

  EGLSurface surface = eglCreatePbufferSurface(display,
                                               config,
                                               surface_attribs);
  if (surface == EGL_NO_SURFACE) {
    LTS5_LOG_ERROR("eglCreatePbufferSurface() failed");
  }

  // Create GL context.
  if (!eglBindAPI(EGL_OPENGL_API)) {
    LTS5_LOG_ERROR("eglBindAPI() failed");
  }

  EGLContext context = eglCreateContext(display,
                                        config,
                                        EGL_NO_CONTEXT,
                                        NULL);
  if (context == EGL_NO_CONTEXT) {
    LTS5_LOG_ERROR("eglCreateContext() failed");
  }

  // Done.
  LTS5_LOG_DEBUG("EGL " << (int)major << "." << (int)minor <<
                        " OpenGL context created (disp: 0x" <<
                        std::hex << (uintptr_t)display <<
                        ", surf: 0x" << (uintptr_t)surface << ", ctx: 0x" <<
                        (uintptr_t)context << ")" << std::dec);
  *ctx = new GLContext(display, surface, context);
}

void destroyGLContext(GLContext* ctx) {
  if (!ctx->context) {
    LTS5_LOG_ERROR("destroyGLContext() called with null `ctx`");
  }
  // If this is the current context, release it.
  if (eglGetCurrentContext() == ctx->context) {
    releaseGLContext();
  }

  if (!eglDestroyContext(ctx->display, ctx->context)) {
    LTS5_LOG_ERROR("eglDestroyContext() failed");
  }
  if (!eglDestroySurface(ctx->display, ctx->surface)) {
    LTS5_LOG_ERROR("eglDestroySurface() failed");
  }

  LTS5_LOG_DEBUG("EGL OpenGL context destroyed (disp: 0x" <<
    std::hex << (uintptr_t)ctx->display <<
    ", surf: 0x" << (uintptr_t)ctx->surface <<
    ", ctx: 0x" << (uintptr_t)ctx->context << ")" << std::dec);

  // Release memory
  delete ctx;
}
