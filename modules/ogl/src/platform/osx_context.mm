/**
 *  @file   platform/osx_context.m
 *  @brief  MacOS Platform function handling opengl context
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   11/11/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#define GLEW_NO_GLU
#include "GL/glew.h"

#include <AppKit/AppKit.h>

#include "internal.hpp"
#include "lts5/utils/logger.hpp"

struct GLContext {

  GLContext() : pixel_format(nil), context(nil), glewInitialized(0) {}

  /** Pixel Format - NSOpenGLPixelFormat */
  id pixel_format;
  /** Context - NSOpenGLContext */
  id context;
  /** Glew initialized  */
  int glewInitialized;
};

void setGLContext(GLContext* ctx) {
  @autoreleasepool {
    if (ctx->context == nil) {
      LTS5_LOG_ERROR("setGLContext() called with null `ctx`");
    }
    // Set context
    [ctx->context makeCurrentContext];
    // Glew init ?
    if (ctx->glewInitialized) {
      return;
    }
    glewExperimental = GL_TRUE;
    GLenum result = glewInit();
    if (result != GLEW_OK) {
      LTS5_LOG_ERROR("glewInit() failed:" << glewGetErrorString(result));
    }
    // Get glew version
    LTS5_LOG_DEBUG("Glew Version: " << GLEW_VERSION_MAJOR << "."
                                    << GLEW_VERSION_MINOR << "."
                                    << GLEW_VERSION_MICRO);
    LTS5_LOG_DEBUG("OpenGL Version: " << glGetString(GL_VERSION));
    LTS5_LOG_DEBUG("Vendor: " << glGetString(GL_VENDOR));
    LTS5_LOG_DEBUG("Renderer: " << glGetString(GL_RENDERER));
    ctx->glewInitialized = 1;
  }
}

/**
 * @name    releaseGLContext
 * @brief   Release the current active context
 */
void releaseGLContext() {
  @autoreleasepool {
    [NSOpenGLContext clearCurrentContext];
  }
}

/**
 * @name    createGLContext
 * @brief Create an OPenGL context
 * @param[out] ctx Context
 */
void createGLContext(GLContext** ctx) {
  // Create new context
  *ctx = new GLContext();

  // Define pixel attributes
  NSOpenGLPixelFormatAttribute attribs[] = {
          NSOpenGLPFAAccelerated,
          NSOpenGLPFAClosestPolicy,
#if MAC_OS_X_VERSION_MAX_ALLOWED >= 101000
          NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
#else
          NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core,
#endif
          /*NSOpenGLPFAColorSize, 24,     // Each channel is 8 bits
          NSOpenGLPFAAlphaSize, 8,      // Alpha is 8 bits
          NSOpenGLPFADepthSize, 24,     // Depth buffer of 24bits
          NSOpenGLPFAStencilSize, 8,    // Sentil buffer of 8bits
          NSOpenGLPFASampleBuffers, 0,  // No multisampling*/
          0
  };
  // Init pixel format
  (*ctx)->pixel_format = [[NSOpenGLPixelFormat alloc] initWithAttributes:attribs];
  if ((*ctx)->pixel_format == nil) {
    LTS5_LOG_ERROR("NSOpenGLPixelFormat alloc failed");
  }

  // Create context
  (*ctx)->context = [[NSOpenGLContext alloc]
                     initWithFormat: (*ctx)->pixel_format
                     shareContext: nil];
  if ((*ctx)->context == nil) {
    LTS5_LOG_ERROR("NSOpenGLContext alloc failed");
  }
}

/**
 * @name    destroyGLContext
 * @brief   Destroy a given context
 * @param[in] ctx   Context to destroy
 */
void destroyGLContext(GLContext* ctx) {
  @autoreleasepool {
    if (ctx->context == nil) {
      LTS5_LOG_ERROR("destroyGLContext() called with null `ctx`");
    }
    // Release pixel format
    [ctx->pixel_format release];
    ctx->pixel_format = nil;
    // Release context
    [ctx->context release];
    ctx->context = nil;
    // Release struct
    delete ctx;
  }
}
