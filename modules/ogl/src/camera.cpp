/**
 *  @file   camera.cpp
 *  @brief  Camera interface to use render scene in OpenGL
 *
 *  @author Christophe Ecabert
 *  @date   14/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#include <iostream>
#include <lts5/utils/math/constant.hpp>

#include "lts5/ogl/camera.hpp"
#include "lts5/utils/math/quaternion.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name OGLCamera
 *  @fn OGLCamera(const int& win_width, const int& win_height)
 *  @brief  Constructor
 *  @param[in]  win_width   Window width
 *  @param[in]  win_height  Window height
 */
template<typename T>
OGLCamera<T>::OGLCamera(const int& win_width, const int& win_height) :
        position_(0.0, 0.0, 0.0),
        forward_(0.0, 0.0, 1.0),
        up_(0.0, 1.0, 0.0),
        right_(1.0, 0.0, 0.0),
        win_width_(T(win_width)),
        win_height_(T(win_height)),
        field_of_view_(30.0),
        near_plane_(0.001),
        far_plane_(1000.0),
        state_(kNone),
        move_speed_(0.05),
        rotation_speed_(T(1.0)),
        rotations_start_(T(0.0), T(0.0), T(0.0)),
        rotations_end_(T(0.0), T(0.0), T(0.0)) {
  this->projection();
}

#pragma mark -
#pragma mark Usage

/*
 * @name  LookAt
 * @fn    void LookAt(const Bbox_t& volume)
 * @brief Orients camera to look at the given volume
 * @param[in] volume Volume to look at
 */
template<typename T>
void OGLCamera<T>::LookAt(const Bbox_t& volume) {
  // Target => center of volume
  Vec3 target = volume.center_;
  // Define camera position
  auto dt = volume.max_ - volume.min_;
  T r = std::sqrt((dt.x_ * dt.x_) + (dt.y_ * dt.y_) + (dt.z_ * dt.z_));
  // Define displacement in Z direction
  float dz = r / std::sin(this->field_of_view_ * Constants<T>::Deg2Rad);
  Vec3 pos(target.x_, target.y_, target.z_ + dz);
  this->LookAt(pos, target);
}

/*
 *  @name LookAt
 *  @fn void LookAt(const Vec3 position)
 *  @brief  Orients the camera so that it is directly facing a given #position
 *  @param[in]  position  Location to look at
 */
template<typename T>
void OGLCamera<T>::LookAt(const Vec3& position, const Vec3& target) {
  // Define target direction
  position_ = position;
  forward_ = position_ - target;
  forward_.Normalize();
  // Define right
  Vec3 worldUp(T(0.0), T(1.0), T(0.0));
  right_ = worldUp ^ forward_;
  // Define up vector
  up_ = forward_ ^ right_;
  // Define view transform
  this->orientation();
}

/*
 * @name OnKeyboard
 * @fn    void OnKeyboard(const OGLKey& key, const OGLKeyState& state)
 * @brief Keyboard event
 * @param[in] key     Key that trigger the event
 * @param[in] state   State of the key
 * @return true if callback is defined, false otherwise
 */
template<typename T>
bool OGLCamera<T>::OnKeyboard(const OGLKey& key, const OGLKeyState& state) {
  // Action based on key
  switch (key) {
    case OGLKey::kW : position_ -= forward_ * move_speed_;
      return true;

    case OGLKey::kS : position_ += forward_ * move_speed_;
      return true;

    case OGLKey::kD : position_ += right_ * move_speed_;
      return true;

    case OGLKey::kA : position_ -= right_ * move_speed_;
      return true;

    default:
      return false;
  }
}

/*
 * @name  OnMouseClick
 * @fn    void OnMouseClick(const OGLMouse& button,
                          const OGLKeyState& state,
                          const int& x,
                          const int& y)
 * @brief Method to invoke when key is pressed/released
 * @param button  Which mouse's button has trigger the event
 * @param state   Button state : pressed, released
 * @param x       Cursor X position
 * @param y       Cursor Y position
 * @return true if callback is defined, false otherwise
 */
template<typename T>
bool OGLCamera<T>::OnMouseClick(const OGLMouse& button,
                             const OGLKeyState& state,
                             const int& x,
                             const int& y) {
  if (state != OGLKeyState::kPress) {
    state_ = State::kNone;
    return false;
  } else {
    state_ = State::kRotate;
    // Get projection onto ball
    this->GetMouseProjectionOnBall(x, y, &rotations_start_);
    rotations_end_ = rotations_start_;
    return true;
  }
}

/*
 * @name  OnMouseMove
 * @fn    void OnMouseMove(const int x, const int y)
 * @brief Method to invoke when mouse moves on the screen
 * @param x   Current cursor X position
 * @param y   Current cursor Y position
 * @return true if callback is defined, false otherwise
 */
template<typename T>
bool OGLCamera<T>::OnMouseMove(const int& x, const int& y) {
  if (state_ == State::kRotate) {
    // Should update transformation
    // Get new projection
    this->GetMouseProjectionOnBall(x, y, &rotations_end_);
    // update transform, compute axis + angle
    T angle = std::acos(rotations_start_ * rotations_end_);
    if (!std::isnan(angle) && angle != T(0.0)) {
      angle *= rotation_speed_;
//      Vec3 axis = rotations_start_ ^ rotations_end_;
      Vec3 axis = rotations_end_ ^ rotations_start_;
      if (axis.x_ != T(0.0) || axis.y_ != T(0.0) || axis.z_ != T(0.0)) {
        // Save position
        rotations_start_ = rotations_end_;
        // Setup quaternion
        Quaternion<T> quat(axis, angle);
        quat.Normalize();
        // Get transformation
        Matrix3x3<T> rot;
        quat.ToRotationMatrix(&rot);
        // Update camera orientation
        forward_ = rot * forward_;
        right_ = rot * right_;
        up_ = rot * up_;
        position_ = rot * position_;
      }
    }
    return true;
  }
  return false;
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name orientation
 *  @fn T orientation() const
 *  @brief  Rotation matrix that determines the direction the matrix is
 *          looking at. Translation is not included
 *  @return Camera far plane
 */
template<typename T>
typename OGLCamera<T>::Mat4& OGLCamera<T>::orientation(bool flip /*= false*/) {
  orientation_.Identity();
  orientation_(0, 0) = right_.x_;
  orientation_(0, 1) = right_.y_;
  orientation_(0, 2) = right_.z_;
  orientation_(1, 0) = up_.x_;
  orientation_(1, 1) = up_.y_;
  orientation_(1, 2) = up_.z_;
  orientation_(2, 0) = forward_.x_;
  orientation_(2, 1) = forward_.y_;
  orientation_(2, 2) = forward_.z_;
  if (flip) {
    Mat4 rot;
    Quaternion<T> q(forward_, Constants<T>::PI);
    q.ToRotationMatrix(&rot);
    orientation_ = rot * orientation_;
  }
  return orientation_;
}

/*
 *  @name matrix
 *  @fn Mat4 matrix() const
 *  @brief  The combined camera transformation matrix, including perspective
 *          projection. This complete matrix must be used in the vertex shader
 *  @return Matrix transform
 */
template<typename T>
typename OGLCamera<T>::Mat4 OGLCamera<T>::matrix() {
  return projection_ * view();
}

/*
 *  @name projection
 *  @fn Mat4 projection() const
 *  @brief  The perspective projection transform
 *  @return Projection transform
 */
template<typename T>
typename OGLCamera<T>::Mat4 OGLCamera<T>::projection() {
  T ratio = win_width_/win_height_;
  projection_ = Mat4::Perspective(field_of_view_,
          ratio,
          near_plane_,
          far_plane_);
  return projection_;
}

/*
 *  @name view
 *  @fn Mat4 view() const
 *  @brief  Translation and rotation of the camera. Same as 'matrix' without
 *          projection transformation matrix included in.
 *  @return view matrix
 */
template<typename T>
typename OGLCamera<T>::Mat4 OGLCamera<T>::view(bool flip /*= false*/) {
  auto transform = Mat4().Identity();
  transform(0, 3) = -position_.x_;
  transform(1, 3) = -position_.y_;
  transform(2, 3) = -position_.z_;
  return orientation(flip) * transform;
}

#pragma mark -
#pragma mark Private

/*
 * @name  GetMouseProjectionOnBall
 * @fn    void GetMouseProjectionOnBall(const int x,
                                        const int y,
                                        Vec3* pts)
 * @brief Project screen loation onto ball
 * @param[in] x       Cursor X position on the screen
 * @param[in] y       Cursor Y position on the screen
 * @param[out] pts    Corresponding points on sphere
 * @see https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Arcball
 */
template<typename T>
void OGLCamera<T>::GetMouseProjectionOnBall(const int& x,
                                            const int& y,
                                            Vec3* pts) {
  // Convert screen coordinate into [-1, 1] range,
  pts->x_ = ((T(2.0) * T(x)) / win_width_) - T(1.0);
  pts->y_ = T(1.0) - ((T(2.0) * T(y)) / win_height_);
  pts->z_ = T(0.0);
  // Compute distance from O -> Pts
  T norm = pts->x_ * pts->x_ + pts->y_ * pts->y_;
  if (norm <= T(1.0)) {
    // pythagore
    pts->z_ = std::sqrt(T(1.0) - norm);
  } else {
    // Nearest points
    pts->Normalize();
  }
}

template class OGLCamera<float>;
template class OGLCamera<double>;

}  // namespace LTS5
