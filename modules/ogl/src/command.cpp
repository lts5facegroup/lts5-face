/**
 *  @file   command.cpp
 *  @brief  Thin wrapper for opengl command
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   11/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <GL/glew.h>

#include "lts5/ogl/command.hpp"

#ifdef DEBUG
  #define OGL_WRAPPER_LAYER_DEBUG 1
#endif

#ifndef OGL_WRAPPER_LAYER_DEBUG
  #define OGL_CMD(CMD, ...)             \
    do {                                \
      (CMD)(__VA_ARGS__);               \
      return 0;                         \
    } while(0)
#else
  #define OGL_CMD(CMD, ...)                           \
      do {                                            \
        (CMD)(__VA_ARGS__);                           \
        return glGetError() == GL_NO_ERROR ? 0 : -1;  \
      } while(0)
#endif

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name  GlGetError
 * @brief Return error information
 * @return Return error code
 */
int OGLCommand::GlGetError() {
  return glGetError();
}

/*
 * @name  GlEnable
 * @brief Trun on a given GL capability
 * @param[in] cap
 * @return -1 if error, 0 otherwise
 */
int OGLCommand::GlEnable(unsigned int cap) {
  OGL_CMD(glEnable, cap);
}

/*
 * @name  GlDisable
 * @brief Trun off a given GL capability
 * @param[in] cap
 * @return -1 if error, 0 otherwise
 */
int OGLCommand::GlDisable(unsigned int cap) {
  OGL_CMD(glDisable, cap);
}

/*
 * @name  GlBlendFunc
 * @brief specify pixel arithmetic
 * @param[in] sfactor Specifies how the red, green, blue, and alpha source
 *    blending factors are computed. The initial value is GL_ONE.
 * @param[in] dfactor Specifies how the red, green, blue, and alpha
 *    destination blending factors are computed.
 * @return    -1 if error, 0 otherwise
 */
int OGLCommand::GlBlendFunc(unsigned int sfactor, unsigned int dfactor) {
  OGL_CMD(glBlendFunc, sfactor, dfactor);
}

/*
 * @name  GlClear
 * @brief  clear buffers to preset values
 * @param[in] mask Bitwise OR of masks that indicate the buffers to be
 *    cleared. The four masks are GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT,
 *    GL_ACCUM_BUFFER_BIT, and GL_STENCIL_BUFFER_BIT.
 * @return -1 if error, 0 otherwise
 */
int OGLCommand::GlClear(unsigned int mask) {
  OGL_CMD(glClear, mask);
}

/*
 * @name  GlClearColor
 * @brief specify clear values for the color buffers
 * @param[in] r   Red color
 * @param[in] g   Green color
 * @param[in] b   Blue color
 * @param[in] a   Aplha color
 * @return    -1 if error, 0 otherwise
 */
int OGLCommand::GlClearColor(float r, float g, float b, float a) {
  OGL_CMD(glClearColor, r, g, b, a);
}

/*
 * @name  glViewport
 * @brief Set the viewport
 * @param[in] x   Lower left x component
 * @param[in] y   Lower left y component
 * @param[in] width   Width of the viewport
 * @param[in] height  Height of the viewport
 * @return -1 if error , 0 otherwise
 */
int OGLCommand::GlViewport(int x, int y, int width, int height) {
  OGL_CMD(glViewport, x, y, width, height);
}

/*
 * @name  GlFrontFace
 * @brief define front- and back-facing polygons
 * @param[in] mode    Specifies the orientation of front-facing polygons.
 *    GL_CW and GL_CCW are accepted. The initial value is GL_CCW.
 * @return -1 if error, 0 otherwise
 */
int OGLCommand::GlFrontFace(unsigned int mode) {
  OGL_CMD(glFrontFace, mode);
}

/*
 * @name  GlCullFace
 * @brief specify whether front- or back-facing facets can be culled
 * @param[in] mode    Specifies whether front- or back-facing facets are
 *    candidates for culling. Symbolic constants GL_FRONT, GL_BACK, and
 *    GL_FRONT_AND_BACK are accepted. The initial value is GL_BACK.
 * @return -1 if error, 0 otherwise
 */
int OGLCommand::GlCullFace(unsigned int mode) {
  OGL_CMD(glCullFace, mode);
}

/*
 * @name      glDrawElements
 * @brief     Render primitives from array data
 * @param[in] mode    Specifies what kind of primitives to render.
 * @param[in] count   Specifies the number of elements to be rendered.
 * @param[in] type    Specifies the type of the values in indices
 * @param[in] indices Specifies a pointer to the location where the indices
 *    are stored.
 * @return -1 if error, 0 otherwise
 */
int OGLCommand::GlDrawElements(unsigned int mode,
                               int count,
                               unsigned int type,
                               const void* indices) {
  OGL_CMD(glDrawElements, mode, count, type, indices);
}

/**
 * @name  glMultiDrawElementsIndirect
 * @brief Render indexed primitives from array data, taking parameters from
 *    memory
 * @param[in] mode    Specifies what kind of primitives to render.
 * @param[in] type    Specifies the type of data in the buffer bound to the
 *    GL_ELEMENT_ARRAY_BUFFER binding.
 * @param[in] indirect    Specifies the address of a structure containing an
 *    array of draw parameters.
 * @param[in] drawcount   Specifies the number of elements in the array
 *    addressed by indirect.
 * @param[in] stride  Specifies the distance in basic machine units between
 *    elements of the draw parameter array.
 * @return -1 if error, 0 otherwise
 */
int OGLCommand::GlMultiDrawElementsIndirect(unsigned int mode,
                                            unsigned int type,
                                            const void* indirect,
                                            int drawcount,
                                            int stride) {
  OGL_CMD(glMultiDrawElementsIndirect, mode, type, indirect, drawcount, stride);
}

}  // namespace LTS5
