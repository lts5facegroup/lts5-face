/**
 *  @file   sh_viewer.cpp
 *  @brief  Utility tool for rendering estimated spherical harmonics on a ball
 *          for visualisation / debugging
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   9/28/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "lts5/ogl/sh_viewer.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/geometry/mesh_factory.hpp"
#include "lts5/ogl/camera.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


#pragma mark -
#pragma mark Shaders

static constexpr const char* vs_str = "#version 330\n"
                              "layout (location = 0) in vec3 position;\n"
                              "layout (location = 1) in vec3 normal;\n"
                              "layout (location = 2) in vec4 color;\n"
                              "uniform mat4 camera;\n"
                              "out vec3 normal0;\n"
                              "out vec4 color0;\n"
                              "void main() {\n"
                              "  gl_Position = camera * vec4(position, 1.0);\n"
                              "  normal0 = normal;\n"
                              "  color0 = color;\n"
                              "}";
static constexpr const char* fs_str = "#version 330\n"
                                      "in vec3 normal0;\n"
                                      "in vec4 color0;\n"
                                      "out vec4 fragColor;\n"
                                      "void main() {\n"
                                      "  fragColor = clamp(color0, 0.0, 1.0);\n"
                                      "}";

#pragma mark -
#pragma mark Initialization

/*
 * @name  SHViewer
 * @fn    SHViewer(const int& width, const int& height, const int& step)
 * @brief Constructor
 * @param[in] width   Image width
 * @param[in] height  Image heigh
 * @param[in] step    Number of section on the ball
 */
template<typename T>
SHViewer<T>::SHViewer(const int& width,
                      const int& height,
                      const int& step) : renderer_(nullptr),
                                         shader_(nullptr) {
  using Factory = MeshFactory<T>;
  using SphParam = typename Factory::SphereParameters;
  using ImgType = typename OGLOffscreenRenderer<T>::ImageType;
  using ShaderType = typename OGLShader::ShaderType;
  using Att = OGLProgram::Attributes;
  using AttTyp = OGLProgram::AttributeType;
  // Create sphere
  SphParam p;
  p.phi_step = step;
  p.theta_step = step;
  Factory::GenerateSphere(&p, &sphere_);
  n_pts_ = sphere_.get_vertex().size();
  // Add dummy color
  std::vector<Vector4<T>> color(n_pts_, {1.0, 0.0, 0.0, 0.0});
  sphere_.set_vertex_color(color);
  // Create renderer
  renderer_ = new OGLOffscreenRenderer<T>();
  int err = renderer_->Init(width, height, 4, false, ImgType::kRGBAf);
  renderer_->ActiveContext();
  if (!err) {
    // Create shader
    std::vector<OGLShader> shaders;
    shaders.emplace_back(vs_str, ShaderType::kVertex);
    shaders.emplace_back(fs_str, ShaderType::kFragment);
    std::vector<Att> attrib;
    attrib.emplace_back(Att(AttTyp::kPoint,  0, "position"));
    attrib.emplace_back(Att(AttTyp::kNormal,  1, "normal"));
    attrib.emplace_back(Att(AttTyp::kColor,  2, "color"));
    shader_ = new LTS5::OGLProgram(shaders, attrib);
    // Camera
    OGLCamera<T> cam(width, height);
    T alpha = cam.get_field_of_view() * Constants<T>::Deg2Rad / T(2.0);
    T dz = T(1.0) / std::sin(alpha);
    cam.set_position({T(0.0), T(0.0), T(1.25) * dz});
    // Update shader
    shader_->Use();
    shader_->SetUniform("camera", cam.matrix());
    shader_->StopUsing();
    // Add mesh, program + Bind to opengl
    renderer_->AddMesh(sphere_);
    renderer_->AddProgram(*shader_);
    err |= renderer_->BindToOpenGL();

    if (!err) {
      renderer_->ActiveContext();
      renderer_->Render();
      renderer_->DisableContext();
    }
  }
  // Disable ogl context
  renderer_->DisableContext();
  if (err) {
    throw ProcessError(ProcessError::kErr,
                       "Error while initializing viewer",
                       FUNC_NAME);
  }
}

/*
 * @name  ~SHViewer
 * @fn    ~SHViewer()
 * @brief Destructor
 */
template<typename T>
SHViewer<T>::~SHViewer() {
  if (renderer_) {
    delete renderer_;
  }
  if (shader_) {
    delete shader_;
  }
}

#pragma mark -
#pragma mark Usage

template<typename T>
Vector3<T> ComputeSHCoef(const Vector3<T>& r,
                         const Vector3<T>& n,
                         const T* sh_p) {
  using Vec3 = Vector3<T>;
  Vec3 coef;
  std::vector<T> sh_basis = {T(1.0),
                             n.x_,
                             n.y_,
                             n.z_,
                             n.x_ * n.y_,
                             n.x_ * n.z_,
                             n.y_ * n.z_,
                             (n.x_ * n.x_) - (n.y_ * n.y_),
                             (T(3.0) * n.z_ * n.z_) - T(1.0)};
  for (size_t k = 0; k < 9; ++k) {
    coef.x_ += sh_basis[k] * sh_p[k];
    coef.y_ += sh_basis[k] * sh_p[9 + k];
    coef.z_ += sh_basis[k] * sh_p[18 + k];
  }
  coef.x_ *= r.r_;
  coef.y_ *= r.g_;
  coef.z_ *= r.b_;
  return coef;
}

/*
 * @name Generate
 * @fn    int Generate(const cv::Mat& aldebo, const cv::Mat& sh_coef,
 *                     cv::Mat* image)
 * @brief Render an estimation of the environmental lighting on a sphere
 * @param[in] aldebo
 * @param[in] sh_coef
 * @param[out] image
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int SHViewer<T>::Generate(const cv::Mat& aldebo,
                          const cv::Mat& sh_coef,
                          cv::Mat* image) {
  using Color = typename Mesh<T>::Color;
  using Vec3 = Vector3<T>;
  assert(aldebo.rows == n_pts_ &&
         "Aldebo has not the proper number of element");
  assert(sh_coef.rows == 3 && sh_coef.cols == 9 &&
         "SH coef have not the proper number of element");
  // Activate context
  renderer_->ActiveContext();
  // Compute colors
  const auto& n = sphere_.get_normal();
  const auto* r_ptr = reinterpret_cast<const Vec3*>(aldebo.data);
  const auto* sh_coef_ptr = reinterpret_cast<const T*>(sh_coef.data);
  std::vector<Color> colors(n_pts_);
  for (size_t k = 0; k < n_pts_; ++k) {
    // Compute SH coeff
    auto coef = ComputeSHCoef(r_ptr[k], n[k], sh_coef_ptr);
    // Scale colors
    colors[k] = {coef.r_, coef.g_, coef.b_, T(1.0)};
  }
  // Update shapder
  sphere_.set_vertex_color(colors);
  int err = renderer_->UpdateColor();
  // Render
  renderer_->Render();
  // Get image
  cv::Mat img;
  renderer_->GetImage(&img);
  cv::flip(img, *image, 0);
  // Disable context
  renderer_->DisableContext();
  return err;
}

#pragma mark -
#pragma mark Explicite instantiation

/** Float */
template class SHViewer<float>;
/** Double */
template class SHViewer<double>;

}  // namespace LTS5
