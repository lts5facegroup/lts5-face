/**
 *  @file   io_buffer.cpp
 *  @brief  OpenGL FBO I/O buffers
 *
 *  @author Christophe Ecabert
 *  @date   15/07/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "GL/glew.h"

#include <iostream>

#include "lts5/utils/logger.hpp"
#include "lts5/ogl/io_buffer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @name    OGLFormatConverter
 * @brief   Convert custom buffer type into OpenGL format
 * @param[in] type  Buffer type
 * @return  OpenGL format
 */
GLenum OGLFormatConverter(const OGLIOBuffer::BufferType type) {
  switch (type) {
    case OGLIOBuffer::BufferType::kDepth: return GL_DEPTH_COMPONENT;
    case OGLIOBuffer::BufferType::kR: return GL_RED;
    case OGLIOBuffer::BufferType::kRf: return GL_RED;
    case OGLIOBuffer::BufferType::kRGB: return GL_RGB;
    case OGLIOBuffer::BufferType::kRGBf: return GL_RGB;
    case OGLIOBuffer::BufferType::kRGBA: return GL_RGBA;
    case OGLIOBuffer::BufferType::kRGBAf: return GL_RGBA;
  }
  return GL_NONE;
}

/**
 * @name    OGLInternalFormatConverter
 * @brief   Convert custom buffer type into OpenGL internal format
 * @param[in] type  Buffer type
 * @return  OpenGL internal format
 */
GLenum OGLInternalFormatConverter(const OGLIOBuffer::BufferType type) {
  switch (type) {
    case OGLIOBuffer::BufferType::kDepth: return GL_DEPTH_COMPONENT32F;
    case OGLIOBuffer::BufferType::kR: return GL_RED;
    case OGLIOBuffer::BufferType::kRf: return GL_R32F;
    case OGLIOBuffer::BufferType::kRGB: return GL_RGB;
    case OGLIOBuffer::BufferType::kRGBf: return GL_RGB32F;
    case OGLIOBuffer::BufferType::kRGBA: return GL_RGBA;
    case OGLIOBuffer::BufferType::kRGBAf: return GL_RGBA32F;
  }
  return GL_NONE;
}

/**
 * @name    OGLTypeConverter
 * @brief   Convert custom buffer type into OpenGL format
 * @param[in] type  Buffer type
 * @return  OpenGL type
 */
GLenum OGLTypeConverter(const OGLIOBuffer::BufferType type) {
  switch (type) {
    case OGLIOBuffer::BufferType::kDepth: return GL_FLOAT;
    case OGLIOBuffer::BufferType::kR: return GL_UNSIGNED_BYTE;
    case OGLIOBuffer::BufferType::kRf: return GL_FLOAT;
    case OGLIOBuffer::BufferType::kRGB: return GL_UNSIGNED_BYTE;
    case OGLIOBuffer::BufferType::kRGBf: return GL_FLOAT;
    case OGLIOBuffer::BufferType::kRGBA: return GL_UNSIGNED_BYTE;
    case OGLIOBuffer::BufferType::kRGBAf: return GL_FLOAT;
  }
  return GL_NONE;
}

/**
 * @name    OCVTypeConverter
 * @brief   Convert custom buffer type into OpenCV format
 * @param[in] type  Buffer type
 * @return  OpenCV type
 */
int OCVTypeConverter(const OGLIOBuffer::BufferType type) {
  switch (type) {
    case OGLIOBuffer::BufferType::kDepth: return CV_32FC1;
    case OGLIOBuffer::BufferType::kR: return CV_8UC1;
    case OGLIOBuffer::BufferType::kRf: return CV_32FC1;
    case OGLIOBuffer::BufferType::kRGB: return CV_8UC3;
    case OGLIOBuffer::BufferType::kRGBf: return CV_32FC3;
    case OGLIOBuffer::BufferType::kRGBA: return CV_8UC4;
    case OGLIOBuffer::BufferType::kRGBAf: return CV_32FC4;
  }
  return 0;
}

/**
 * @name    ChannelConverter
 * @brief   Compute the number of channel for a given buffer type
 * @param[in] type  Buffer type
 * @return Number of channel for a given type
 */
int ChannelConverter(const OGLIOBuffer::BufferType type) {
  switch (type) {
    case OGLIOBuffer::BufferType::kDepth: return 1;
    case OGLIOBuffer::BufferType::kR: return 1;
    case OGLIOBuffer::BufferType::kRf: return 1;
    case OGLIOBuffer::BufferType::kRGB: return 3;
    case OGLIOBuffer::BufferType::kRGBf: return 3;
    case OGLIOBuffer::BufferType::kRGBA: return 4;
    case OGLIOBuffer::BufferType::kRGBAf: return 4;
  }
  return 0;
}

/**
 * @name    ElemSizeConverter
 * @brief   Compute the number of byte is used for each pixel for a given
 *          buffer type
 * @param[in] type  Buffer type
 * @return Number of byte for each pixel
 */
int ElemSizeConverter(const OGLIOBuffer::BufferType type) {
  switch (type) {
    case OGLIOBuffer::BufferType::kDepth: return 4;
    case OGLIOBuffer::BufferType::kR: return 1;
    case OGLIOBuffer::BufferType::kRf: return 4;
    case OGLIOBuffer::BufferType::kRGB: return 1;
    case OGLIOBuffer::BufferType::kRGBf: return 4;
    case OGLIOBuffer::BufferType::kRGBA: return 1;
    case OGLIOBuffer::BufferType::kRGBAf: return 4;
  }
  return 0;
}

/*
 * @name  OGLIOBuffer
 * @fn  OGLIOBuffer()
 * @brief Constructor
 */
OGLIOBuffer::OGLIOBuffer() : width_(0),
                             height_(0),
                             fbo_{0},
                             color_buffer_{0},
                             depth_buffer_{0},
                             type_(BufferType::kDepth),
                             m_sample_(0),
                             w_depth_(false) {
}

/*
 * @name  ~OGLIOBuffer
 * @fn  ~OGLIOBuffer()
 * @brief Destructor
 */
OGLIOBuffer::~OGLIOBuffer() {
  // Clear buffers
  if (depth_buffer_) {
    glDeleteTextures(2, depth_buffer_);
  }
  if (color_buffer_) {
    glDeleteTextures(2, color_buffer_);
  }
  if (fbo_) {
    glDeleteFramebuffers(2, fbo_);
  }
}

/*
 * @name  Init
 * @fn  int Init(const int& width,
                 const int& height,
                 const bool& with_depth,
                 const BufferType& internal_type);
 * @brief Initialize OpenGL buffer
 * @param[in] width         Window width
 * @param[in] height        Window height
 * @param[in] width_depth   Include or not depth buffer
 * @param[in] internal_type Type of buffer (GL_RBG32F, GL_R32F, GL_RGB, GL_RED,
 *                          GL_NONE = depth only)
 * @return  -1 if error, 0 otherwise
 */
int OGLIOBuffer::Init(const int& width,
                      const int& height,
                      const int& m_sample,
                      const bool& with_depth,
                      const BufferType& type) {
  // Define properties
  width_ = width;
  height_ = height;
  type_ = type;
  m_sample_ = m_sample;
  w_depth_ = with_depth;
  // Create buffer
  if (!fbo_[0]) {
    // Frame buffer
    glGenFramebuffers(2, fbo_);
  }
  if (!color_buffer_[0] && type_ != BufferType::kDepth) {
    glGenTextures(2, color_buffer_);
  }
  if (!depth_buffer_[0] && w_depth_) {
    glGenTextures(2, depth_buffer_);
  }
  int err = glGetError() == GL_NO_ERROR ? 0 : -1;
  // Init both framerenderer
  if (m_sample_ > 0) {
    err |= InitFramebuffer(FramebufferType::kMultisampling);
  }
  err |= InitFramebuffer(FramebufferType::kStandard);
  return err;
}

/*
 * @name  InitFramebuffer
 * @fn    int InitFramebuffer(const FramebufferType& fb_type)
 * @brief Initialize framebuffer
 * @param[in] fb_type Type of framebuffer
 * @return    -1 if error, 0 otherwise
 */
int OGLIOBuffer::InitFramebuffer(const FramebufferType& fb_type) {
  int err = -1;
  // Bind frame buffer
  glBindFramebuffer(GL_FRAMEBUFFER, fbo_[fb_type]);
  // Init color buffer
  if (type_ != BufferType::kDepth) {
    if (fb_type == FramebufferType::kStandard) {
      // Bind color texture
      glBindTexture(GL_TEXTURE_2D, color_buffer_[fb_type]);
      // Create texture
      glTexImage2D(GL_TEXTURE_2D,
                   0,
                   OGLInternalFormatConverter(type_),
                   width_,
                   height_,
                   0,
                   OGLFormatConverter(type_),
                   OGLTypeConverter(type_),
                   nullptr);
      // Set properties
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      // Attach to fbo
      glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,
                             GL_COLOR_ATTACHMENT0,
                             GL_TEXTURE_2D,
                             color_buffer_[fb_type],
                             0);
      // Unbind color texture
      glBindTexture(GL_TEXTURE_2D, 0);
    } else {
      // Bind MS-color texture
      glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, color_buffer_[fb_type]);
      // Create MS-texture
      glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE,
                              m_sample_,
                              OGLInternalFormatConverter(type_),
                              width_,
                              height_,
                              GL_TRUE);
      // No sampling properties, https://stackoverflow.com/questions/22678146
      // Attach to fbo
      glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,
                             GL_COLOR_ATTACHMENT0,
                             GL_TEXTURE_2D_MULTISAMPLE,
                             color_buffer_[fb_type],
                             0);
      // Unbind MS color texture
      glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
    }
  }
  // Add depth buffer ?
  if (w_depth_) {
    if (fb_type == FramebufferType::kStandard) {
      // Bind depth texture
      glBindTexture(GL_TEXTURE_2D, depth_buffer_[fb_type]);
      // Create texture
      glTexImage2D(GL_TEXTURE_2D,
                   0,
                   GL_DEPTH_COMPONENT,
                   width_,
                   height_,
                   0,
                   GL_DEPTH_COMPONENT,
                   GL_FLOAT,
                   nullptr);
      // Set properties
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      // Attach to fbo
      glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,
                             GL_DEPTH_ATTACHMENT,
                             GL_TEXTURE_2D,
                             depth_buffer_[fb_type],
                             0);
      // Unbind depth texture
      glBindTexture(GL_TEXTURE_2D, 0);
    } else {
      // Bind MS depth texture
      glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, depth_buffer_[fb_type]);
      // Create MS-texture
      glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE,
                              m_sample_,
                              GL_DEPTH_COMPONENT,
                              width_,
                              height_,
                              GL_TRUE);
      // No sampling properties, https://stackoverflow.com/questions/22678146
      // Attach to fbo
      glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,
                             GL_DEPTH_ATTACHMENT,
                             GL_TEXTURE_2D_MULTISAMPLE,
                             depth_buffer_[fb_type],
                             0);
      // Unbind MS color texture
      glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
    }
  }
  // Check fbo status + ogl errors
  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  err = glGetError() == GL_NO_ERROR ? 0 : -1;
  if (status != GL_FRAMEBUFFER_COMPLETE) {
    LTS5_LOG_ERROR("FBO status: 0x" << std::hex << status << std::dec);
    err |= -1;
  }
  // Unbind
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  return err;
}

#pragma mark -
#pragma mark Usage

/**
 * @name  BlitBuffer
 * @fn    void BlitBuffer() const
 * @brief Blit multisampled buffer(s) to normal FBO
 */
void  OGLIOBuffer::BlitBuffer() const {
  // Define input/output
  if (m_sample_ > 0) {
    glBindFramebuffer(GL_READ_FRAMEBUFFER,
                      fbo_[FramebufferType::kMultisampling]);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_[FramebufferType::kStandard]);
    // Move data
    glBlitFramebuffer(0, 0, width_, height_,
                      0, 0, width_, height_,
                      GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT,
                      GL_NEAREST);
  }
}


/*
 * @name  BindForWriting
 * @fn  void BindForWriting() const
 * @brief Bind internal buffer for writting
 */
void OGLIOBuffer::BindForWriting() const {
  auto idx = m_sample_ > 0 ?
          FramebufferType::kMultisampling :
          FramebufferType::kStandard;
  glBindFramebuffer(GL_FRAMEBUFFER, fbo_[idx]);
}

/*
 * @name  BindForReading
 * @fn  void BindForReading(const unsigned int& texture_unit) const
 * @brief Bind for reaeding
 * @param[in] texture_unit  Texture unit to activate
 */
void OGLIOBuffer::BindForReading(const unsigned int& texture_unit) const {
  // Activate texture unit
  glActiveTexture(GL_TEXTURE0 + texture_unit);
  if (type_ == BufferType::kDepth) {
    glBindTexture(GL_TEXTURE_2D, depth_buffer_[FramebufferType::kStandard]);
  } else {
    glBindTexture(GL_TEXTURE_2D, color_buffer_[FramebufferType::kStandard]);
  }
}

/*
 * @name  Unbind
 * @fn  void Unbind() const
 * @brief Unbind the underlying framebuffer
 */
void OGLIOBuffer::Unbind() const {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/*
 * @name  TransferColorToTexture
 * @fn    int TransferColorToTexture() const
 * @brief Transfer color pixel from FBO to Texture object
 * @return -1 if error, 0 otherwise
 */
int OGLIOBuffer::TransferColorToTexture() const {
  int err = -1;
  if (color_buffer_) {
    // Bind Framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, fbo_[FramebufferType::kStandard]);
    // Bind to color buffer
    glBindTexture(GL_TEXTURE_2D, color_buffer_[FramebufferType::kStandard]);
    // Transfer
    glCopyTexSubImage2D(GL_TEXTURE_2D,
                        0,
                        0,
                        0,
                        0,
                        0,
                        static_cast<GLsizei>(width_),
                        static_cast<GLsizei>(height_));
    err = glGetError() == GL_NO_ERROR ? 0 : -1;
    // Unbind texture + framebuffer
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }
  return err;
}

/*
 * @name  ReadColorBuffer
 * @fn  int ReadColorBuffer(cv::Mat* image)
 * @brief Write the content of a FBO into a given \p image
 * @param[out]  image   Where to write image
 * @return  -1 if error, 0 otherwise
 */
int OGLIOBuffer::ReadColorBuffer(cv::Mat* image) const {
  int err = -1;
  if (color_buffer_) {
    image->create(height_, width_, OCVTypeConverter(type_));
    err = this->ReadColorBuffer(image->data);
  }
  return err;
}

/*
 * @name  ReadColorBuffer
 * @fn  int ReadColorBuffer(void* buffer)
 * @brief Write the content of a FBO into a given \p image
 * @param[out] buffer Buffer where to copy the data
 * @return  -1 if error, 0 otherwise
 */
int OGLIOBuffer::ReadColorBuffer(void* buffer) const {
  int err = -1;
  if (color_buffer_) {
    // Bind to color buffer
    glBindTexture(GL_TEXTURE_2D, color_buffer_[FramebufferType::kStandard]);
    // use fast 4-byte alignment (default anyway) if possible
    // https://stackoverflow.com/questions/9097756
    int step = (width_ * ChannelConverter(type_) * ElemSizeConverter(type_));
    glPixelStorei(GL_PACK_ALIGNMENT, 1);// (step & 3) ? 1 : 4);
    // set length of one complete row in destination data (doesn't need to equal
    // img.cols)
    int rl = step / (ChannelConverter(type_) * ElemSizeConverter(type_));
    glPixelStorei(GL_PACK_ROW_LENGTH, (GLint)(rl));
    // Read back
    glGetTexImage(GL_TEXTURE_2D,
                  0,
                  OGLFormatConverter(type_),
                  OGLTypeConverter(type_),
                  reinterpret_cast<GLvoid*>(buffer));
    err = glGetError() == GL_NO_ERROR ? 0 : -1;
    // Unbind texture
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  return err;
}

/*
 * @name  ReadDepthBuffer
 * @fn  int ReadDepthBuffer(cv::Mat* depth_map)
 * @brief Write the content of a FBO into a given \p depth_map
 * @param[out]  depth_map   Where to write depth_map
 * @return  -1 if error, 0 otherwise
 */
int OGLIOBuffer::ReadDepthBuffer(cv::Mat* depth_map) const {
  int err = -1;
  if (depth_buffer_) {
    depth_map->create(height_, width_, CV_32F);
    // Bind to color buffer
    glBindTexture(GL_TEXTURE_2D, depth_buffer_[FramebufferType::kStandard]);
    // use fast 4-byte alignment (default anyway) if possible
    // https://stackoverflow.com/questions/9097756
    size_t step = depth_map->step[0];
    glPixelStorei(GL_PACK_ALIGNMENT, (step & 3) ? 1 : 4);
    // set length of one complete row in destination data (doesn't need to equal
    // img.cols)
    int rl = (int)step / (ChannelConverter(BufferType::kDepth) * ElemSizeConverter(BufferType::kDepth));
    glPixelStorei(GL_PACK_ROW_LENGTH, (GLint)(rl));
    // Read back
    glGetTexImage(GL_TEXTURE_2D,
                  0,
                  GL_DEPTH_COMPONENT,
                  GL_FLOAT,
                  reinterpret_cast<void*>(depth_map->data));
    err = glGetError() == GL_NO_ERROR ? 0 : -1;
    // Unbind texture
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  return err;
}

/*
   * @name  GetOglBuffer
   * @fn    unsigned int GetOglBuffer(const BufferType& buff_type) const
   * @brief Provide reference to the underlaying OpenGL buffer
   * @param[in] buff_type
   * @return    OpenGL reference
   */
unsigned int OGLIOBuffer::GetOglBuffer(const BufferType& buff_type) const {
  switch (buff_type) {
    case BufferType::kDepth: return depth_buffer_[FramebufferType::kStandard];
    default: return color_buffer_[FramebufferType::kStandard];
  }
}
}  // namepsace LTS5
