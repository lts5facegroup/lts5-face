/**
 *  @file   wired_box_renderer.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   28/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <stack>

#include "GL/glew.h"

#include "lts5/ogl/wired_renderer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Vertex shader str */
static constexpr const char* vertex_shader_str = "#version 330\n"
        "// Input data\n"
        "layout (location = 0) in vec3 position;\n"
        "// uniform input -> matrix\n"
        "uniform mat4 camera;\n"
        "uniform mat4 model;\n"
        "// output\n"
        "void main() {\n"
        "  // Apply transformation onto points\n"
        "  gl_Position = camera * model * vec4(position, 1);\n"
        "}";
/** Fragment shader str */
static constexpr const char* fragment_shader_str = "#version 330\n"
        "out vec4 fragColor;\n"
        "void main() {\n"
        "  fragColor = vec4(0.0, 1.0, 0.0, 1.0);\n"
        "}";

#pragma mark -
#pragma mark Initialization

/*
 * @name  OGLWiredRenderer
 * @fn    OGLWiredRenderer()
 * @brief Constructor
 */
template<typename T>
OGLWiredRenderer<T>::OGLWiredRenderer() :
        OGLBaseRenderer<T>::OGLBaseRenderer() {
  //Shader
  std::vector<OGLShader> shader;
  shader.emplace_back(OGLShader(vertex_shader_str, OGLShader::ShaderType::kVertex));
  shader.emplace_back(OGLShader(fragment_shader_str, OGLShader::ShaderType::kFragment));
  // Program
  std::vector<OGLProgram::Attributes> attrib;
  attrib.emplace_back(OGLProgram::Attributes(OGLProgram::kPoint,
                                             0,
                                             "position"));
  this->program_ = new LTS5::OGLProgram(shader, attrib);
  // Add mesh
  this->mesh_ = new LTS5::Mesh<T>();
  // Sanity check (in debug only)
  assert((this->vao_ != nullptr) && (this->program_ != nullptr) &&
         (glGetError() == GL_NO_ERROR));
}

/*
 * @name  OGLWiredRenderer @fn    OGLWiredRendererid)
 * @brief Destructor
 */
template<typename T>
OGLWiredRenderer<T>::~OGLWiredRenderer() {
  if (this->program_) {
    delete this->program_;
  }
}

/*
 *  @name BindToOpenGL
 *  @fn int BindToOpenGL();
 *  @brief  Upload data into OpenGL context
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int OGLWiredRenderer<T>::BindToOpenGL() {
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;
  // Get vao and bind to it
  int err = this->vao_->Create(2);
  this->vao_->Bind();
  // Bind vertex
  if (!vertex_.empty()) {
    this->vao_->BindBuffer(0, BufferType::kAttribute);
    err |= this->vao_->AddData(vertex_.size() * sizeof(vertex_[0]),
                               vertex_.data(),
                               BufferType::kAttribute);
    err |= this->vao_->SetAttributePointer(0, 3);
  }
  // Lines
  if (tri_.size() > 1) {
    this->vao_->BindBuffer(1, BufferType::kElement);
    err |= this->vao_->AddData(tri_.size() * sizeof(tri_[0]),
                               tri_.data(),
                               BufferType::kElement);
  }
  // Make sure the VAO is not changed from the outside
  this->vao_->Unbind();
  this->is_init_ = (err == 0);
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  UpdateVertex
 * @fn  int UpdateVertex()
 * @brief Update vertex position into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
int OGLWiredRenderer<T>::UpdateVertex() {
  return -1;
}

/*
 * @name  UpdateNormal
 * @fn  int UpdateNormal()
 * @brief Update normal into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
int OGLWiredRenderer<T>::UpdateNormal() {
  return -1;
}

/*
 * @name  UpdateNormal
 * @fn  int UpdateNormal()
 * @brief Update normal into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
int OGLWiredRenderer<T>::UpdateTCoord() {
  return -1;
}

/*
 * @name  UpdateColor
 * @fn  int UpdateColor()
 * @brief Update vertex color into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
int OGLWiredRenderer<T>::UpdateColor() {
  return -1;
}

/*
 * @name    AddBoundingBox
 * @brief   Add new bbox into data buffer
 * @param[in] bbox  Bounding boxes
 */
template<typename T>
void OGLWiredRenderer<T>::AddBoundingBox(const AABB<T>& bbox) {
  // Vertex
  int start = static_cast<int>(vertex_.size());
  vertex_.push_back(Vertex(bbox.min_.x_, bbox.min_.y_, bbox.min_.z_));
  vertex_.push_back(Vertex(bbox.max_.x_, bbox.min_.y_, bbox.min_.z_));
  vertex_.push_back(Vertex(bbox.max_.x_, bbox.max_.y_, bbox.min_.z_));
  vertex_.push_back(Vertex(bbox.min_.x_, bbox.max_.y_, bbox.min_.z_));
  vertex_.push_back(Vertex(bbox.min_.x_, bbox.max_.y_, bbox.max_.z_));
  vertex_.push_back(Vertex(bbox.min_.x_, bbox.min_.y_, bbox.max_.z_));
  vertex_.push_back(Vertex(bbox.max_.x_, bbox.min_.y_, bbox.max_.z_));
  vertex_.push_back(Vertex(bbox.max_.x_, bbox.max_.y_, bbox.max_.z_));
  // Connection
  // Back
  tri_.push_back(Line(start, start + 1));
  tri_.push_back(Line(start + 1, start + 2));
  tri_.push_back(Line(start + 2, start + 3));
  tri_.push_back(Line(start + 3, start));
  // Front
  tri_.push_back(Line(start + 4, start + 5));
  tri_.push_back(Line(start + 5, start + 6));
  tri_.push_back(Line(start + 6, start + 7));
  tri_.push_back(Line(start + 7, start + 4));
  // Missing
  tri_.push_back(Line(start + 3, start + 4));
  tri_.push_back(Line(start + 2, start + 7));
  tri_.push_back(Line(start + 1, start + 6));
  tri_.push_back(Line(start, start + 5));
}

/**
 * @name  Render
 * @fn  void Render()
 * @brief Render into OpenGL context
 */
template<typename T>
void OGLWiredRenderer<T>::Render() {}

/*
 * @name  Render
 * @fn  void Render(const OGLCamera& cam, const glm::mat4& model)
 * @brief Render wired boxes
 * @param[in] cam     Camera transformation (view + projection)
 * @param[in] model   Object transformation (rigid)
 */
template<typename T>
void OGLWiredRenderer<T>::Render(const OGLCamera<T>& cam,
                                 const Mat4& model) {
  assert(this->is_init_);
  // Link to VAO
  this->vao_->Bind();
  this->program_->Use();
  this->program_->SetUniform("camera", const_cast<OGLCamera<T>&>(cam).matrix());
  this->program_->SetUniform("model", model);
  // Draw stuff
  glDrawElementsBaseVertex(GL_LINES,
                           static_cast<GLsizei>(tri_.size() * 2),
                           GL_UNSIGNED_INT,
                           0,
                           0);
  assert(glGetError() == GL_NO_ERROR);
  // Make sure the VAO is not changed from the outside
  this->vao_->Unbind();
  this->program_->StopUsing();
}

/*
   * @name  FillFromAABBTree
   * @fn  static void FillFromAABBTree(const AABBTree& tree,
                                       OGLWiredRenderer* renderer
   * @brief Helper function to populate wired renderer
   * @param[in]     tree      Tree where to get cells
   * @param[in]     max_level How far to dive into the tree
   * @param[in,out] renderer  Wired renderer to populate
   */
template<typename T>
void OGLWiredRenderer<T>::FillFromAABBTree(const AABBTree<T>& tree,
                                           const int max_level,
                                           OGLWiredRenderer<T>* renderer){
  // Get root nodel
  const AABBNode<T>* root = tree.get_root_node();
  if (root && renderer) {
      // Get tree size
    std::size_t size = tree.Size() - 1;
    for (int i = 0; i < size; ++i) {
      // Add bbox
      if (root[i].level_ < max_level) {
        renderer->AddBoundingBox(root[i].get_bbox());
        if (root[i].is_leaf()) {
          renderer->AddBoundingBox(*(root[i].get_left_data()));
          renderer->AddBoundingBox(*(root[i].get_right_data()));
        } else {

          if ((root[i].last_ - root[i].first_) == 3) {
            renderer->AddBoundingBox(*root[i].get_left_data());
          }
        }
      }
    }
    // Bind
    renderer->BindToOpenGL();
  }
}

/*
 * @name  FillFromOCTree
 * @fn  static void FillFromOCTree(const OCTree& tree,
                                     OGLWiredRenderer* renderer
 * @brief Helper function to populate wired renderer
 * @param[in]     tree      Tree where to get cells
 * @param[in,out] renderer  Wired renderer to populate
 */
template<typename T>
void OGLWiredRenderer<T>::FillFromOCTree(const OCTree<T>& tree,
                                            OGLWiredRenderer<T>* renderer) {
  // Get root node
  const OCTreeNode<T>* root = tree.get_root_node();
  if (root && renderer) {
    std::stack<const OCTreeNode<T>*> stack;
    stack.push(root);
    while (!stack.empty()) {
      const OCTreeNode<T>* node = stack.top();
      stack.pop();
      // Add bbox
      renderer->AddBoundingBox(node->bbox_);
      // Add children if any
      for (int i = 0; i < 8; ++i) {
        if (node->child_[i]) {
          stack.push(node->child_[i]);
        }
      }
    }
    // Bind
    renderer->BindToOpenGL();
  }
}

#pragma mark -
#pragma mark Declaration

/** Float type OGLWiredRenderer */
template class OGLWiredRenderer<float>;
/** Float type OGLWiredRenderer */
template class OGLWiredRenderer<double>;

}  // namepsace LTS5