/**
 *  @file   shader.cpp
 *  @brief  OpenGL Shader helper
 *
 *  @author Christophe Ecabert
 *  @date   13/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "GL/glew.h"

#include <stdexcept>
#include <fstream>
#include <sstream>
#include <cassert>

#include "lts5/ogl/shader.hpp"
#include "lts5/utils/process_error.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Helper Function

GLenum ConvertShaderType(const OGLShader::ShaderType& type) {
  switch (type) {
    case OGLShader::ShaderType::kVertex: return GL_VERTEX_SHADER;
    case OGLShader::ShaderType::kGeometry: return GL_GEOMETRY_SHADER;
    case OGLShader::ShaderType::kFragment: return GL_FRAGMENT_SHADER;
  }
  return 0;
}

#pragma mark -
#pragma mark Initialization

/*
 *  @name OGLShaderFromFile
 *  @fn static OGLShader OGLShaderFromFile(const std::string& filepath,
                                           const ShaderType& type)
 *  @brief  Create shader from text file
 *  @param[in]  filepath    Path to the text file where shader code is stored
 *  @param[in]  shader_type Type of shader, GL_VERTEX_SHADER, GL_FRAGMENT_SHADER
 *  @return Pointer to the created shader
 *  @throw  Throw exception if error occurs during the construction
 */
OGLShader OGLShader::OGLShaderFromFile(const std::string& filepath,
                                       const ShaderType& type) {
  // Read file
  std::ifstream f(filepath.c_str(), std::ios::in | std::ios::binary);
  if(!f.is_open()){
    throw ProcessError(ProcessError::ProcessErrorEnum::kErrOpeningFile,
                       "Failed to open file: " + filepath,
                       FUNC_NAME);
  }
  //read whole file into stringstream buffer
  std::stringstream buffer;
  buffer << f.rdbuf();
  // Create shader
  return OGLShader(buffer.str(), type);
}

/*
 *  @name OGLShader
 *  @fn OGLShader(const std::string& code, const ShaderType& type)
 *  @brief  Create shader from text file
 *  @param[in]  code        Shader's code
 *  @param[in]  shader_type Type of shader, GL_VERTEX_SHADER, GL_FRAGMENT_SHADER
 *  @throw  Throw exception if error occurs during the construction
 */
OGLShader::OGLShader(const std::string& code,
                     const ShaderType& type) :
  object_(0) /*, ref_count_(nullptr)*/ {
  // Create OpenGL shader object
  object_ = glCreateShader(ConvertShaderType(type));
  if (object_ == 0) {
    throw ProcessError(ProcessError::ProcessErrorEnum::kErr,
                       "OpenGL Failed to create Shader",
                       FUNC_NAME);
  }
  // Ok, define source code
  const char* code_ptr = code.c_str();
  glShaderSource(object_, 1, (const GLchar**)&code_ptr, NULL);
  //compile
  glCompileShader(object_);
  //throw exception if compile error occurred
  GLint status;
  glGetShaderiv(object_, GL_COMPILE_STATUS, &status);
  if (status == GL_FALSE) {
    // Compilation fail, get error message
    std::string msg = "Compile failure in shader:\n\n" + code + "\n\n";
    GLint infoLogLength;
    glGetShaderiv(object_, GL_INFO_LOG_LENGTH, &infoLogLength);
    char* strInfoLog = new char[infoLogLength + 1];
    glGetShaderInfoLog(object_, infoLogLength, NULL, strInfoLog);
    msg += strInfoLog;
    delete[] strInfoLog;
    // Clear shader object
    glDeleteShader(object_);
    object_ = 0;
    throw ProcessError(ProcessError::ProcessErrorEnum::kErr,
                       msg,
                       FUNC_NAME);
  }
//  ref_count_ = new unsigned;
//  *ref_count_ = 1;
}

/*
 *  @name OGLShader
 *  @fn OGLShader(const OGLShader& other)
 *  @brief  Copy constructor
 *  @param[in]  other Shader to copy from
 */
OGLShader::OGLShader(const OGLShader& other) : RefCounted::RefCounted(other),
                                               object_(other.object_) {
}

/*
 *  @name ~OGLShader
 *  @fn ~OGLShader()
 *  @brief  Destructor
 */
OGLShader::~OGLShader() {
  if (this->Unref() && object_) {
    // Object not referenced, can be deleted
    glDeleteShader(object_);
    object_ = 0;
  }

//  //ref_count_ will be NULL if constructor failed and threw an exception
//  if (ref_count_) {
//    this->Release();
//  }
}

/*
 *  @name operator=
 *  @fn OGLShader& operator=(const OGLShader& rhs)
 *  @brief  Assignment operator
 *  @param[in]  rhs Shader to assign from
 *  @return Assigned shader
 */
OGLShader& OGLShader::operator=(const OGLShader& rhs) {
  if (this != &rhs) {
    if (this->RefCountIsOne() && object_) {
      glDeleteShader(object_);
    }
    RefCounted::operator=(rhs);
    object_ = rhs.object_;
  }
  return *this;
}

/*
 *  @name Retain
 *  @fn void Retain()
 *  @brief  Increase reference counter
 */
//void OGLShader::Retain() {
//  assert(ref_count_);
//  *ref_count_ += 1;
//}

/*
 *  @name Release
 *  @fn void Release()
 *  @brief  Release object memory if reference counter reach zero
 */
//void OGLShader::Release() {
//  assert(ref_count_ && *ref_count_ > 0);
//  *ref_count_ -= 1;
//  if (*ref_count_ == 0) {
//    // Object not referenced, can be deleted
//    glDeleteShader(object_);
//    object_ = 0;
//    delete ref_count_;
//    ref_count_ = nullptr;
//  }
//}

}  // namespace LTS5
