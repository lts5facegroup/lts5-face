/**
 *  @file   base_renderer.cpp
 *  @brief  Base class for OpenGL renderer
 *
 *  @author Christophe Ecabert
 *  @date   19/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "GL/glew.h"

#include "lts5/ogl/base_renderer.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Capability */
template<typename T, typename ColorContainer>
using Capability = typename OGLBaseRenderer<T, ColorContainer>::Capability;
/** Depth Func */
template<typename T, typename ColorContainer>
using DepthFunc = typename OGLBaseRenderer<T, ColorContainer>::DepthFunc;
/** PolyMode */
template<typename T, typename ColorContainer>
using PolyMode = typename OGLBaseRenderer<T, ColorContainer>::PolyMode;

template<typename T, typename ColorContainer>
GLenum ConvertToOgl(const Capability<T, ColorContainer>& cap) {
  GLenum v = GL_INVALID_ENUM;
  switch (cap) {
    case Capability<T, ColorContainer>::kBlend: v = GL_BLEND; break;
    case Capability<T, ColorContainer>::kClipDistance0: v = GL_CLIP_DISTANCE0; break;
    case Capability<T, ColorContainer>::kClipDistance1: v = GL_CLIP_DISTANCE1; break;
    case Capability<T, ColorContainer>::kClipDistance2: v = GL_CLIP_DISTANCE2; break;
    case Capability<T, ColorContainer>::kClipDistance3: v = GL_CLIP_DISTANCE3; break;
    case Capability<T, ColorContainer>::kClipDistance4: v = GL_CLIP_DISTANCE4; break;
    case Capability<T, ColorContainer>::kClipDistance5: v = GL_CLIP_DISTANCE5; break;
    case Capability<T, ColorContainer>::kColorLogicOp: v = GL_COLOR_LOGIC_OP; break;
    case Capability<T, ColorContainer>::kCullFace: v = GL_CULL_FACE; break;
    case Capability<T, ColorContainer>::kDepthClamp: v = GL_DEPTH_CLAMP; break;
    case Capability<T, ColorContainer>::kDepthTest: v = GL_DEPTH_TEST; break;
    case Capability<T, ColorContainer>::kDither: v = GL_DITHER; break;
    case Capability<T, ColorContainer>::kLineSmooth: v = GL_LINE_SMOOTH; break;
    case Capability<T, ColorContainer>::kMultisample: v = GL_MULTISAMPLE; break;
    case Capability<T, ColorContainer>::kPolygonOffsetFill: v = GL_POLYGON_OFFSET_FILL; break;
    case Capability<T, ColorContainer>::kPolygonOffsetLine: v = GL_POLYGON_OFFSET_LINE; break;
    case Capability<T, ColorContainer>::kPolygonOffsetPoint: v = GL_POLYGON_OFFSET_POINT; break;
    case Capability<T, ColorContainer>::kPolygonSmooth: v = GL_POLYGON_SMOOTH; break;
    case Capability<T, ColorContainer>::kRasterizerDiscard: v = GL_RASTERIZER_DISCARD; break;
    case Capability<T, ColorContainer>::kSampleAlphaToCoverage: v = GL_SAMPLE_ALPHA_TO_COVERAGE; break;
    case Capability<T, ColorContainer>::kSampleAlphaToOne: v = GL_SAMPLE_ALPHA_TO_ONE; break;
    case Capability<T, ColorContainer>::kSampleCoverage: v = GL_SAMPLE_COVERAGE; break;
    case Capability<T, ColorContainer>::kSampleShading: v = GL_SAMPLE_SHADING; break;
    case Capability<T, ColorContainer>::kSampleMask: v = GL_SAMPLE_MASK; break;
    case Capability<T, ColorContainer>::kScissorTest: v = GL_SCISSOR_TEST; break;
    case Capability<T, ColorContainer>::kStencilTest: v = GL_STENCIL_TEST; break;
    case Capability<T, ColorContainer>::kTextureCubeMapSeamless: v = GL_TEXTURE_CUBE_MAP_SEAMLESS; break;
    case Capability<T, ColorContainer>::kProgramPointSize: v = GL_PROGRAM_POINT_SIZE; break;
  }
  return v;
}

template<typename T, typename ColorContainer>
GLenum ConvertToOgl(const DepthFunc<T, ColorContainer>& func) {
  GLenum v = GL_INVALID_ENUM;
  switch (func) {
    case DepthFunc<T, ColorContainer>::kNever: v = GL_NEVER; break;
    case DepthFunc<T, ColorContainer>::kLess: v = GL_LESS; break;
    case DepthFunc<T, ColorContainer>::kEqual: v = GL_EQUAL; break;
    case DepthFunc<T, ColorContainer>::kLessOrEqual: v = GL_LEQUAL; break;
    case DepthFunc<T, ColorContainer>::kGreater: v = GL_GREATER; break;
    case DepthFunc<T, ColorContainer>::kNotEqual: v = GL_NOTEQUAL; break;
    case DepthFunc<T, ColorContainer>::kGreaterOrEqual: v = GL_GEQUAL; break;
    case DepthFunc<T, ColorContainer>::kAlways: v = GL_ALWAYS; break;
  }
  return v;
}

template<typename T, typename ColorContainer>
GLenum ConvertToOgl(const PolyMode<T, ColorContainer>& mode) {
  GLenum v = GL_INVALID_ENUM;
  switch (mode) {
    case PolyMode<T, ColorContainer>::kPoint: v = GL_POINT; break;
    case PolyMode<T, ColorContainer>::kLine: v = GL_LINE; break;
    case PolyMode<T, ColorContainer>::kFill: v = GL_FILL; break;
  }
  return v;
}

template<typename T, typename Container>
struct ColorSize {};

template<typename T>
struct ColorSize<T, Vector3<T>> {
  static constexpr int value = 3;
};

template<typename T>
struct ColorSize<T, Vector4<T>> {
  static constexpr int value = 4;
};

/*
 * @name    BufferInfo
 * @fn  BufferInfo() = default
 * @brief   Constructor
 */
template<typename T, typename ColorContainer>
OGLBaseRenderer<T, ColorContainer>::BufferInfo::BufferInfo() :
        dim_vertex_(0),
        size_vertex_(3),
        dim_normal_(0),
        size_normal_(3),
        dim_tex_coord_(0),
        size_tex_coord_(2),
        dim_color_(0),
        size_color_(ColorSize<T, ColorContainer>::value),
        n_elements_(0),
        data_vertex_(nullptr),
        data_normal_(nullptr),
        data_tex_coord_(nullptr),
        data_color_(nullptr),
        data_elements_(nullptr) {
}


/*
 * @name    BufferInfo
 * @fn  BufferInfo() = default
 * @brief   Constructor
 */
template<typename T, typename ColorContainer>
OGLBaseRenderer<T, ColorContainer>::BufferInfo::BufferInfo(const Mesh& m) {
  // Go through all properties of the mesh
  // Vertex
  const auto& vertex = m.get_vertex();
  dim_vertex_ = vertex.size() * sizeof(Vertex);
  size_vertex_ = 3;
  data_vertex_ = vertex.data();
  // Normal
  const auto& normal = m.get_normal();
  dim_normal_ = normal.size() * sizeof(Normal);
  size_normal_ = 3;
  data_normal_ = normal.data();
  // TCoord
  const auto& tc = m.get_tex_coord();
  dim_tex_coord_ = tc.size() * sizeof(TCoord);
  size_tex_coord_ = 2;
  data_tex_coord_ = tc.data();
  // Color
  const auto& color = m.get_vertex_color();
  dim_color_ = color.size() * sizeof(Color);
  size_color_ = ColorSize<T, ColorContainer>::value;
  data_color_ = color.data();
  // Elements
  const auto& tri = m.get_triangle();
  n_elements_ = tri.size() * 3;
  data_elements_ = reinterpret_cast<const int*>(tri.data());
}

/*
 * @name    RenderingComponent
 * @fn  RenderingComponent(const int& width,
 *                         const int& height)
 * @brief Constructor
 * @param[in] width     Window width
 * @param[in] height    Window height
 */
template<typename T, typename ColorContainer>
OGLBaseRenderer<T, ColorContainer>::RenderingComponent::
RenderingComponent(const int &width, const int &height) : cam_(width, height),
                                                          tex_() {
}

/**
 * @name    RenderingComponent
 * @fn  RenderingComponent(const std::string& path,
 *                         const int& width,
 *                         const int& height)
 * @brief Constructor
 * @param[in] path      Path to mesh file
 * @param[in] width     Window width
 * @param[in] height    Window height
 */
template<typename T, typename ColorContainer>
OGLBaseRenderer<T, ColorContainer>::RenderingComponent::
RenderingComponent(const std::string& path,
                   const int& width,
                   const int& height) : cam_(width, height),
                                        tex_() {
  this->Init(path);
}

/*
 * @name    RenderingComponent
 * @fn      RenderingComponent(const Mesh& mesh, const Shader& shader,
                                const Camera_t cam)
 * @brief Constructor
 * @param[in] mesh      Mesh instance
 * @param[in] shader    Opengl shading program to use
 * @param[in] cam       Camera
 */
template<typename T, typename ColorContainer>
OGLBaseRenderer<T, ColorContainer>::RenderingComponent::
RenderingComponent(const Mesh& mesh,
                   const Shader& shader,
                   const Camera_t& cam) : mesh_(mesh),
                                          cam_(cam),
                                          program_(shader) {}

/*
 * @name    Init
 * @fn  int Init(const std::string& path)
 * @brief Initialize internal elements for a given mesh
 * @param[in] path Path to the mesh object
 * @return  -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLBaseRenderer<T, ColorContainer>::RenderingComponent::
Init(const std::string& path) {
  // Load mesh
  int err = mesh_.Load(path);
  // Set camera to look at the mesh
  const auto& bbox = mesh_.bbox();
  T max_z = bbox.max_.z_ * T(100.0);
  T min_z = bbox.max_.z_ * T(0.001);
  LTS5_LOG_DEBUG("near=" << min_z << " far=" << max_z);
  cam_.LookAt(bbox);
  cam_.set_planes(min_z, max_z);
  // Create shading program + setup camera
  program_ = OGLProgram::FromMesh(mesh_);
  program_.Use();
  program_.SetUniform("camera", cam_.matrix());
  program_.StopUsing();
  // Init texture for materials if any, support only ONE ambient texture at the
  // moment.
  const auto& material = mesh_.get_materials();
  if (!material.empty()) {
    const auto& mat_path = material.front().get_ambient_texture();
    if (!material.empty()) {
      tex_ = OGLTexture::FromFile(mat_path);
    }
  }
  return err;
}

/*
 * @name  OGLBaseRenderer
 * @fn  OGLBaseRenderer()
 * @brief Constructor
 */
template<typename T, typename ColorContainer>
OGLBaseRenderer<T, ColorContainer>::OGLBaseRenderer() : mesh_(nullptr),
                                                        program_(nullptr),
                                                        texture_(nullptr),
                                                        vao_(nullptr),
                                                        n_elements_(0),
                                                        clear_color_(0.0, 0.0, 0.0, 1.0),
                                                        is_init_(false) {
  vao_ = new LTS5::OGLVertexArrayObject<T>();
}

/*
 * @name  ~OGLBaseRenderer
 * @fn  virtual ~OGLBaseRenderer()
 * @brief Destructor
 */
template<typename T, typename ColorContainer>
OGLBaseRenderer<T, ColorContainer>::~OGLBaseRenderer() {
  // Release VAO
  if (vao_ != nullptr) {
    delete vao_;
    vao_ = nullptr;
  }
}

template<typename T, typename ColorContainer>
int GetVertexColorSize() {
  return 0;
}

template<>
int GetVertexColorSize<float, Vector3<float>>() {
  return 3;
}
template<>
int GetVertexColorSize<double, Vector3<double>>() {
  return 3;
}
template<>
int GetVertexColorSize<float, Vector4<float>>() {
  return 4;
}
template<>
int GetVertexColorSize<double, Vector4<double>>() {
  return 4;
}

/*
 *  @name BindToOpenGL
 *  @fn virtual int BindToOpenGL();
 *  @brief  Upload data into OpenGL context
 *  @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLBaseRenderer<T, ColorContainer>::BindToOpenGL() {
  int err = -1;
  if ((program_ != nullptr) && (mesh_ != nullptr)) {
    BufferInfo info(*mesh_);
    err = this->BindToOpenGL(info);
  } else {
    LTS5_LOG_ERROR("Renderer need at least an Mesh/OGLProgram !");
  }
  return err;
}

/*
 *  @name BindToOpenGL
 *  @fn virtual int BindToOpenGL(const BufferInfo& info);
 *  @brief  Upload data into OpenGL context
 *  @param[in] info   Buffer information (size, data) to instantiate
 *  @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLBaseRenderer<T, ColorContainer>::BindToOpenGL(const BufferInfo& info) {
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;
  // Init VAO + Bind it
  int err = vao_->Create(5);
  vao_->Bind();
  n_elements_ = info.n_elements_;
  if (err == 0 && program_ != nullptr) {
    // Get shader's input
    const std::vector<Attribute>& attrib = program_->get_attributes();
    AttributeType n = AttributeType::kPoint;
    auto fcn = [&](const Attribute& val) -> bool {
      return val.type == n;
    };
    // Vertex
    if (info.dim_vertex_ != 0) {
      // Bind data to ogl buffer
      vao_->BindBuffer(BufferIdx::kVertex, BufferType::kAttribute);
      err |= vao_->AddData(info.dim_vertex_,
                           info.data_vertex_,
                           BufferType::kAttribute);
      n = AttributeType::kPoint;
      auto it = std::find_if(attrib.begin(), attrib.end(), fcn);
      if (it != attrib.end()) {
        err |= vao_->SetAttributePointer(it->location, info.size_vertex_);
      }
    }
    // Normal
    if (info.dim_normal_ != 0) {
      vao_->BindBuffer(BufferIdx::kNormal, BufferType::kAttribute);
      err |= vao_->AddData(info.dim_normal_,
                           info.data_normal_,
                           BufferType::kAttribute);
      n = AttributeType::kNormal;
      auto it = std::find_if(attrib.begin(), attrib.end(), fcn);
      if (it != attrib.end()) {
        err |= vao_->SetAttributePointer(it->location, info.size_normal_);
      }
    }
    // Tex coordinate
    if (info.dim_tex_coord_ != 0) {
      vao_->BindBuffer(BufferIdx::kTexCoord, BufferType::kAttribute);
      err |= vao_->AddData(info.dim_tex_coord_,
                           info.data_tex_coord_,
                           BufferType::kAttribute);
      n = AttributeType::kTexCoord;
      auto it = std::find_if(attrib.begin(), attrib.end(), fcn);
      if (it != attrib.end()) {
        err |= vao_->SetAttributePointer(it->location, info.size_tex_coord_);
      }
    }
    // Vertex color
    if (info.dim_color_ != 0) {
      vao_->BindBuffer(BufferIdx::kVertexColor, BufferType::kAttribute);
      err |= vao_->AddData(info.dim_color_,
                           info.data_color_,
                           BufferType::kAttribute);
      n = AttributeType::kColor;
      auto it = std::find_if(attrib.begin(), attrib.end(), fcn);
      if (it != attrib.end()) {
        err |= vao_->SetAttributePointer(it->location, info.size_color_);
      }
    }
    // Triangulation
    if(info.n_elements_ != 0) {
      vao_->BindBuffer(BufferIdx::kTriangle, BufferType::kElement);
      err |= vao_->AddData(info.n_elements_ * sizeof(int),
                           info.data_elements_,
                           BufferType::kElement);
    }
    is_init_ = (err == 0);
  } else {
    if (program_ == nullptr) {
      LTS5_LOG_ERROR("Renderer need an OGLProgram");
      err = -1;
    }
    if (err != 0) {
      LTS5_LOG_ERROR("Can not create vertex array buffer");
    }
  }
  // Make sure the VAO is not changed from the outside
  vao_->Unbind();
  return err;
}

/*
 *  @name BindToOpenGL
 *  @fn virtual int BindToOpenGL(const BufferInfo& info);
 *  @brief  Upload data into OpenGL context
 *  @param[in] component   Rendering component holder
 *  @return -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLBaseRenderer<T, ColorContainer>::
BindToOpenGL(const RenderingComponent& component) {
  // Setup component "program" + "texture"
  const auto& program = component.get_shader();
  const auto& tex = component.get_texture();
  this->set_program(&program);
  if (tex.get_width() != 0) {
    this->texture_ = &tex;
  }
  // Setup buffers
  BufferInfo infos(component.get_mesh());
  return this->BindToOpenGL(infos);
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Enable
 * @fn    void Enable(const Capability& cap) const
 * @brief Enable an OpenGL capability
 * @param[in] cap OpenGL's capability to enable
 */
template<typename T, typename ColorContainer>
void OGLBaseRenderer<T, ColorContainer>::Enable(const Capability& cap) const {
  glEnable(ConvertToOgl<T, ColorContainer>(cap));
}

/*
 * @name  Disable
 * @fn    void Disable(const Capability& cap) const
 * @brief Disable an OpenGL capability
 * @param[in] cap OpenGL's capability to disable
 */
template<typename T, typename ColorContainer>
void OGLBaseRenderer<T, ColorContainer>::Disable(const Capability& cap) const {
  glDisable(ConvertToOgl<T, ColorContainer>(cap));
}

/*
 * @name  DepthFunction
 * @fn    void DepthFunction(const DepthFunc& func) const
 * @brief Define which criteria is used to performed depth test when enabled
 * @param[in] func Depth test criteria
 */
template<typename T, typename ColorContainer>
void OGLBaseRenderer<T, ColorContainer>::DepthFunction(const DepthFunc& func) const {
  glDepthFunc(ConvertToOgl<T, ColorContainer>(func));
}

/*
 * @name PolygonMode
 * @fn void PolygonMode(const PolyMode& mode, const T& factor,
                        const T& units) const
 * @brief Select a polygon rasterization mode and set the scale and units
 *        used to calculate depth values
 * @param[in] mode    Specifies how polygons will be rasterized
 * @param[in] factor  Specifies a scale factor that is used to create a
 *                    variable depth offset for each polygon.
 *                    DEFAULT value is 0.
 * @param[in] units   Is multiplied by an implementation-specific value to
 *                    create a constant depth offset. DEFAULT value is 0.
 */
template<typename T, typename ColorContainer>
void OGLBaseRenderer<T, ColorContainer>::PolygonMode(const PolyMode& mode,
                                                     const T& factor,
                                                     const T& units) const {
  glPolygonMode(GL_FRONT_AND_BACK, ConvertToOgl<T, ColorContainer>(mode));
  glPolygonOffset(factor, units);
}

/**
 * @name PolygonMode
 * @fn void PolygonMode(const PolyMode& mode) const
 * @brief Select a polygon rasterization mode
 * @param[in] mode    Specifies how polygons will be rasterized
 */
template<typename T, typename ColorContainer>
void OGLBaseRenderer<T, ColorContainer>::
PolygonMode(const PolyMode& mode) const {
  glPolygonMode(GL_FRONT_AND_BACK, ConvertToOgl<T, ColorContainer>(mode));
}

/**
 * @name
 * @fn
 * @brief Set polygon offset
 * @param factor  Specifies a scale factor that is used to create a
 *                variable depth offset for each polygon.
 *                DEFAULT value is 0.
 * @param units   Is multiplied by an implementation-specific value to
 *                create a constant depth offset. DEFAULT value is 0.
 */
template<typename T, typename ColorContainer>
void OGLBaseRenderer<T, ColorContainer>::PolygonOffset(const T& factor,
                                                       const T& units) const {
  glPolygonOffset(factor, units);
}


/*
 * @name  Map
 * @fn    virtual void* Map(const BufferIdx& index) const
 * @brief Map an OpenGL resource to a host pointer
 * @param[in] index Index of the buffer to map
 * @return    Pointer to the mapped resource or nullptr if something went
 *            wrong
 */
template<typename T, typename ColorContainer>
void* OGLBaseRenderer<T, ColorContainer>::Map(const BufferIdx& index) const {
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;
  BufferType type = index == BufferIdx::kTriangle ?
                    BufferType::kElement :
                    BufferType::kAttribute;
  return vao_->Map(index, type);
}

/*
 * @name  Unmap
 * @fn    virtual int Unmap(const BufferIdx& index) const
 * @brief Unmap an OpenGL resource from a host pointer
 * @param[in] index Index of the buffer to unmap
 * @return    -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLBaseRenderer<T, ColorContainer>::Unmap(const BufferIdx& index) const {
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;
  BufferType type = index == BufferIdx::kTriangle ?
                    BufferType::kElement :
                    BufferType::kAttribute;
  return vao_->Unmap(index, type);
}

/*
 * @name  UpdateVertex
 * @fn  int UpdateVertex()
 * @brief Update vertex position into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLBaseRenderer<T, ColorContainer>::UpdateVertex() {
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;
  int err = -1;
  const auto& vertex = mesh_->get_vertex();
  if (!vertex.empty()) {
    // Bind to vertex vbo
    vao_->BindBuffer(BufferIdx::kVertex, BufferType::kAttribute);
    err = vao_->UpdateData(vertex.size() * sizeof(vertex[0]),
                           vertex.data(),
                           BufferType::kAttribute);
  }
  return err;
}

/*
 * @name  UpdateNormal
 * @fn  int UpdateNormal()
 * @brief Update normal into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLBaseRenderer<T, ColorContainer>::UpdateNormal() {
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;
  int err = -1;
  const auto& normal = mesh_->get_normal();
  if (!normal.empty()) {
    // Bind to vertex vbo
    vao_->BindBuffer(BufferIdx::kNormal, BufferType::kAttribute);
    err = vao_->UpdateData(normal.size() * sizeof(normal[0]),
                           normal.data(),
                           BufferType::kAttribute);
  }
  return err;
}

/*
 * @name  UpdateTCoord
 * @fn  int UpdateTCoord()
 * @brief Update tcoord into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLBaseRenderer<T, ColorContainer>::UpdateTCoord() {
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;
  int err = -1;
  const auto& tex_coord = mesh_->get_tex_coord();
  if (!tex_coord.empty()) {
    // Bind to vertex vbo
    vao_->BindBuffer(BufferIdx::kTexCoord, BufferType::kAttribute);
    err = vao_->UpdateData(tex_coord.size() * sizeof(tex_coord[0]),
                           tex_coord.data(),
                           BufferType::kAttribute);
  }
  return err;
}

/*
 * @name  UpdateColor
 * @fn  int UpdateColor()
 * @brief Update vertex color into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLBaseRenderer<T, ColorContainer>::UpdateColor() {
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;
  int err = -1;
  const auto& vertex_color = mesh_->get_vertex_color();
  if (!vertex_color.empty()) {
    // Bind to vertex vbo
    vao_->BindBuffer(BufferIdx::kVertexColor, BufferType::kAttribute);
    err = vao_->UpdateData(vertex_color.size() * sizeof(vertex_color[0]),
                           vertex_color.data(),
                           BufferType::kAttribute);
  }
  return err;
}

/*
 * @name  UpdateTriangle
 * @fn  int UpdateTriangle()
 * @brief Update triangle into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
template<typename T, typename ColorContainer>
int OGLBaseRenderer<T, ColorContainer>::UpdateTriangle() {
  using BufferType = typename OGLVertexArrayObject<T>::BufferType;
  int err = -1;
  const auto& tri = mesh_->get_triangle();
  if (!tri.empty()) {
    // Bind to vertex vbo
    vao_->BindBuffer(BufferIdx::kTriangle, BufferType::kElement);
    err = vao_->UpdateData(tri.size() * sizeof(tri[0]),
                           tri.data(),
                           BufferType::kElement);
  }
  return err;
}

/*
 * @name  ClearBuffer
 * @fn  virtual void ClearBuffer()
 * @brief Clear OpenGL Buffer
 */
template<typename T, typename ColorContainer>
void OGLBaseRenderer<T, ColorContainer>::ClearBuffer() {
  glClearColor(clear_color_.r_,
               clear_color_.g_,
               clear_color_.b_,
               clear_color_.a_); // black
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

/*
 * @name  Render
 * @fn  void Render(const PrimitiveType& type,
 *                  const size_t& count,
 *                  const size_t& elem_offset)
 * @brief Render part of the mesh into OpenGL context
 * @param[in] type    Type of primitive to be rendered
 * @param[in] count   Number of elements to be rendered
 * @param[in] elem_offset Where to start in the element buffer (VBO)
 */
template<typename T, typename ColorContainer>
void OGLBaseRenderer<T, ColorContainer>::Render(const PrimitiveType& type,
                                                const size_t& count,
                                                const size_t& elem_offset) {
  // Link to VAO
  vao_->Bind();
  // Enable program
  program_->Use();
  // Bind texture if any
  if (texture_) {
    texture_->Bind(GL_TEXTURE0);
  }
  // Draw triangle
  glDrawElements(type == PrimitiveType::kOglTriangle ? GL_TRIANGLES : GL_LINES,
                 count,
                 GL_UNSIGNED_INT,
                 (void*)(elem_offset * sizeof(GLuint)));
  // Make sure the VAO is not changed from the outside
  vao_->Unbind();
  if(texture_) {
    texture_->Unbind();
  }
  program_->StopUsing();
}

#pragma mark -
#pragma mark Declaration

/** Float BaseRenderer - Vector3 */
template class OGLBaseRenderer<float, Vector3<float>>;
/** Float BaseRenderer - Vector4 */
template class OGLBaseRenderer<float, Vector4<float>>;
/** Double BaseRenderer - Vector3 */
template class OGLBaseRenderer<double, Vector3<double>>;
/** Double BaseRenderer - Vector4 */
template class OGLBaseRenderer<double, Vector4<double>>;

}  // namepsace LTS5
