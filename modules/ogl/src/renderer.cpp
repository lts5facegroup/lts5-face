/**
 *  @file   renderer.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   27/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <cstring>
#include <iostream>
#include <algorithm>

#include "lts5/ogl/renderer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  OGLRenderer
 * @fn  OGLRenderer()
 * @brief Constructor
 */
template<typename T, typename ColorContainer>
OGLRenderer<T, ColorContainer>::OGLRenderer() :
        OGLBaseRenderer<T, ColorContainer>::OGLBaseRenderer() {
}

/*
 * @name  ~OGLRenderer
 * @fn  ~OGLRenderer()
 * @brief Destructor
 */
template<typename T, typename ColorContainer>
OGLRenderer<T, ColorContainer>::~OGLRenderer() {
}

/*
 * @name  AddMesh
 * @fn  void AddMesh(const Mesh* mesh)
 * @brief Add geometry to render
 * @param[in] mesh
 */
template<typename T, typename ColorContainer>
void OGLRenderer<T, ColorContainer>::AddMesh(const Mesh* mesh) {
  this->mesh_ = const_cast<Mesh*>(mesh);
}

/*
 * @name  AddProgram
 * @fn  void AddProgram(const OGLProgram* program)
 * @brief Add OpenGL program to tell how to draw a given mesh
 * @param[in] program
 */
template<typename T, typename ColorContainer>
void OGLRenderer<T, ColorContainer>::AddProgram(const OGLProgram* program) {
  this->program_ = const_cast<OGLProgram*>(program);
}

/*
 * @name  AddTexture
 * @fn  void AddTexture(const OGLTexture* texture)
 * @brief Add OpenGL texture (used to "paint" geometry)
 * @param[in] texture
 */
template<typename T, typename ColorContainer>
void OGLRenderer<T, ColorContainer>::AddTexture(const OGLTexture* texture) {
  this->texture_ = const_cast<OGLTexture*>(texture);
}

#pragma mark -
#pragma mark Declaration

/** Float Mesh - Vector3 */
template class OGLRenderer<float, Vector3<float>>;
/** Float Mesh - Vector4 */
template class OGLRenderer<float, Vector4<float>>;
/** Double Mesh - Vector3 */
template class OGLRenderer<double, Vector3<double>>;
/** Double Mesh - Vector4 */
template class OGLRenderer<double, Vector4<double>>;

}  // namepsace LTS5
