/**
 *  @file   context.cpp
 *  @brief  Abstraction for OpenGL Context
 *  @ingroup    ogl
 *
 *  @author Christophe Ecabert
 *  @date   23/11/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "GL/glew.h"

#include "lts5/ogl/context.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @name    DefaultErrorCallback
 * @brief Default error callback
 * @param[in] code      Error number
 * @param[in] message   Message
 */
void DefaultErrorCallback(int code, const char* message) {
  LTS5_LOG_ERROR("OpenGL error 0x" << std::hex << std::uppercase <<
                                   code << ", " << message);
}

/*
 * @name  OGLContext
 * @fn    OGLContext()
 * @brief Constructor
 */
OGLContext::OGLContext() : win_(nullptr) {
  std::atomic_fetch_add_explicit(&counter_, 1, std::memory_order_relaxed);
  OGLContext::SetErrorCallbackFunction(DefaultErrorCallback);
}

/*
 * @name  OGLContext
 * @fn    OGLContext(const int& width, const int& height, const int& m_sample,
              const int& ogl_major, const int& ogl_minor, const bool& visible)
 * @brief Constructor
 * @param[in] width   Window width
 * @param[in] height  Window height
 * @param[in] m_sample Indicate if renderer uses multisampled texture
 *                     for antialiasing. 0 indicates no multisampling
 * @param[in] ogl_major   Major OpenGL version to use
 * @param[in] ogl_minor   Minor OpenGL version to use
 * @param[in] visible Indicate if the windows is displayed on the screen
 */
OGLContext::OGLContext(const int& width,
                       const int& height,
                       const int& m_sample,
                       const int& ogl_major,
                       const int& ogl_minor,
                       const bool& visible) : OGLContext::OGLContext() {
  int err = this->Initialize(width,
                             height,
                             m_sample,
                             ogl_major,
                             ogl_minor,
                             visible);
  if (err) {
    throw std::runtime_error("Error while initializing OpenGL context");
  }
}

/*
 * @name  ~OGLContext
 * @fn    ~OGLContext()
 * @brief Destructor
 */
OGLContext::~OGLContext() {
  if (win_) {
#ifdef HAS_GLFW_
    glfwMakeContextCurrent(win_);
    glfwDestroyWindow(win_);
    // Shutdown glfw if number of instance reach 0
    if (std::atomic_fetch_sub_explicit(&counter_,
                                       1,
                                       std::memory_order_release) == 1) {
      std::atomic_thread_fence(std::memory_order_acquire);
      context_init_.store(false);
      glfwTerminate();
      LTS5_LOG_DEBUG("GLFW Terminated");
    }
#else
    LTS5_LOG_ERROR("Need GLFW to use this class");
#endif
  }
}

/*
 * @name  Initialize
 * @fn    int Initialize(const int& width,
                         const int& height,
                         const int& m_sample,
                         const int& ogl_major,
                         const int& ogl_minor,
                         const bool& visible)
 * @brief Initialize OpenGL window with a specific OpenGL version
 * @param[in] width   Window width
 * @param[in] height  Window height
 * @param[in] m_sample Indicate if renderer uses multisampled texture
 *                     for antialiasing. 0 indicates no multisampling
 * @param[in] ogl_major   Major OpenGL version to use
 * @param[in] ogl_minor   Minor OpenGL version to use
 * @param[in] visible Indicate if the windows is displayed on the screen
 * @return -1 if error, 0 otherwise
 */
int OGLContext::Initialize(const int& width,
                           const int& height,
                           const int& m_sample,
                           const int& ogl_major,
                           const int& ogl_minor,
                           const bool& visible) {
  int err = -1;
#ifdef HAS_GLFW_
  int ret = 1;
  if (!context_init_) {
    std::atomic_thread_fence(std::memory_order_acquire);
    ret = glfwInit();
    context_init_ = true;
  }
  if (ret == 1) {
    // Init ok
    // Print out version
    int Major, Minor, Rev;
    glfwGetVersion(&Major, &Minor, &Rev);
    LTS5_LOG_DEBUG("GLFW " << Major << "." << Minor << "." << Rev);
    // Set window prop
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, ogl_major);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, ogl_minor);
    glfwWindowHint(GLFW_REFRESH_RATE, GL_DONT_CARE);
    if (m_sample > 0) {
      glfwWindowHint(GLFW_SAMPLES, m_sample); // Anti-aliasing
    }
    int visi = visible ? GL_TRUE : GL_FALSE;
    glfwWindowHint(GLFW_VISIBLE, visi);
    // Create window
    if (!win_) {
      win_ = glfwCreateWindow(width, height, "Mesh2Screen", nullptr, nullptr);
    }
    if (win_) {
      // Enable context
      this->ActiveContext();
//#ifndef __APPLE__
      glewExperimental = GL_TRUE;
      GLenum res = glewInit();
      if (res == GLEW_OK) {
        // GLEW throws some errors, so discard all the errors so far
        while(glGetError() != GL_NO_ERROR) {}
      } else {
        LTS5_LOG_ERROR(glewGetErrorString(res));
        return err;
      }
//#endif
      // Query current OpenGL version
      std::string ogl_version((const char*)glGetString(GL_VERSION));
      LTS5_LOG_DEBUG("OpenGL Version: " << ogl_version);
      // OpenGL extension initialized for Linux
      // Setup viewport
      glViewport(0, 0, width, height);
      // Default setting
      glFrontFace(GL_CCW);
      glCullFace(GL_BACK);
      glEnable(GL_CULL_FACE);
      glEnable(GL_DEPTH_TEST);
      if (m_sample > 0) {
        // https://learnopengl.com/Advanced-OpenGL/Anti-Aliasing
        glEnable(GL_MULTISAMPLE); // Anti-aliasing
      }
      // Sanity check
      err = glGetError() == GL_NO_ERROR ? 0 : -1;
    } else {
      LTS5_LOG_ERROR("Unable to create GLFW window");
    }
  } else {
    LTS5_LOG_ERROR("Can not initialize GLFW");
  }
#else
  LTS5_LOG_ERROR("Need GLFW to use this class");
#endif
  return err;
}

/*
 * @name  SetErrorCallbackFunction
 * @fn    int SetErrorCallbackFunction(ErrorCallbackFcn fcn)
 * @brief Define the error callback function to invoke when the backend fail.
 * @param[in] fcn Error callback function
 * @return -1 if error, 0 otherwise
 */
int OGLContext::SetErrorCallbackFunction(ErrorCallbackFcn fcn) {
  int err = -1;
#ifdef HAS_GLFW_
  glfwSetErrorCallback(fcn);
  err = 0;
#else
  LTS5_LOG_ERROR("Need GLFW to use this class");
#endif
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  ActiveContext
 * @fn    void ActiveContext() const
 * @brief Make this context the current one
 */
void OGLContext::ActiveContext() const {
#ifdef HAS_GLFW_
  if(glfwGetCurrentContext() != this->win_) {
    glfwMakeContextCurrent(this->win_);
  }
#else
  LTS5_LOG_ERROR("Need GLFW to use this class");
#endif
}

/**
 * @name  DisableContext
 * @fn    void DisableContext() const
 * @brief Detach from current context
 */
void OGLContext::DisableContext() const {
#ifdef HAS_GLFW_
  glfwMakeContextCurrent(nullptr);
#else
  LTS5_LOG_ERROR("Need GLFW to use this class");
#endif
}

/*
 * @name  SwapBuffer
 * @fn void SwapBuffer() const
 * @brief Update front buffer, by swaping with back
 */
void OGLContext::SwapBuffer() const {
#ifdef HAS_GLFW_
  glfwSwapBuffers(this->win_);
#else
  LTS5_LOG_ERROR("Need GLFW to use this class");
#endif
}

/** Instance counter */
std::atomic<int> OGLContext::counter_(0);
/** Initialization flag */
std::atomic<bool> OGLContext::context_init_(false);

}  // namepsace LTS5
