/**
 *  @file   texture.cpp
 *  @brief  OpenGL Texture helper
 *
 *  @author Christophe Ecabert
 *  @date   15/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <stdexcept>
#include <cassert>
#include <iostream>

#include "GL/glew.h"

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "lts5/ogl/texture.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

static bool IsPowerOfTwo(int x) {
  return (x != 0) && ((x & (x - 1)) == 0);
}

/**
 * @name  TexFrmtFromBitmap
 * @fn  static GLenum TexFrmtFromBitmap(const Bitmap::Format format)
 * @brief Provide corresponding OpenGL format for a given Bitmap
 * @param[in] format  Bitmap format to convert into OpenGL format
 * @return  Corresponding OpenGL format
 */
static GLenum TexFrmtFromBitmap(const Bitmap::Format format) {
  switch (format) {
    case Bitmap::Format::kGray : return GL_RED;
    case Bitmap::Format::kRGB : return GL_RGB;
    case Bitmap::Format::kRGBA : return GL_RGBA;
    default: throw std::runtime_error("Unrecognised Bitmap::Format");
  }
}

/**
 * @name  TexFrmtFromBitmap
 * @fn  static GLenum TexFrmtFromBitmap(const Bitmap::Format format)
 * @brief Provide corresponding OpenGL format for a given Bitmap
 * @param[in] format  Bitmap format to convert into OpenGL format
 * @return  Corresponding OpenGL format
 */
static GLenum TexFrmtFromBitmap(const cv::Mat& image) {
  switch (image.channels()) {
    case 1 : return GL_RED;
    case 3 : return GL_RGB;
    case 4 : return GL_RGBA;
    default: throw std::runtime_error("Unrecognised cv::Mat::channels()");
  }
}

#pragma mark -
#pragma mark Initialization

/**
 * @name  FromFile
 * @fn    static OGLTexture& FromFile(const std::string& filename)
 * @brief Create a texture from a given filename
 * @param[in] filename Path to the texture file
 * @return    OGLTexture object
 */
OGLTexture OGLTexture::FromFile(const std::string& filename) {
  // Load image
  cv::Mat mat = cv::imread(filename);
  if (!mat.empty()) {
    if (mat.channels() == 3) {
      cv::cvtColor(mat, mat, cv::COLOR_BGR2RGB);
    } else if (mat.channels() == 4) {
      cv::cvtColor(mat, mat, cv::COLOR_BGRA2RGBA);
    }
    return OGLTexture(mat, GL_LINEAR, GL_CLAMP_TO_EDGE);
  }
  return OGLTexture();
}

/*
 *  @name OGLTexture
 *  @fn OGLTexture(const Bitmap& bitmap,
                       const int& mag_filter,
                       const int& wrap_mode)
 *  @brief  Constructor, create an empty OpenGL texture buffer
 *  @param[in]  bitmap      Bitmap object to initialize from
 *  @param[in]  mag_filter  Texture interpolation filter : GL_LINEAR, GL_NEAREST
 *  @param[in]  wrap_mode   Texture wrap : GL_REPEAT, GL_MIRRORED_REPEAT,
 *                          GL_CLAMP_TO_EDGE, or GL_CLAMP_TO_BORDER
 */
OGLTexture::OGLTexture(const Bitmap& bitmap,
                       const int& mag_filter,
                       const int& wrap_mode) : OGLTexture::OGLTexture() {
  this->Init(bitmap.get_width(),
             bitmap.get_height(),
             bitmap.data(),
             TexFrmtFromBitmap(bitmap.get_format()),
             mag_filter,
             wrap_mode);
}

/*
 *  @name OGLTexture
 *  @fn OGLTexture(const cv::Mat& image,
                   const GLint mag_filter,
                   const GLint wrap_mod,
                   const bool is_stream)
 *  @brief  Constructor, create an empty OpenGL texture buffer
 *  @param[in]  image       Image matrix to initialize from
 *  @param[in]  mag_filter  Texture interpolation filter : GL_LINEAR, GL_NEAREST
 *  @param[in]  wrap_mode   Texture wrap : GL_REPEAT, GL_MIRRORED_REPEAT,
 *                          GL_CLAMP_TO_EDGE, or GL_CLAMP_TO_BORDER
 *  @param[in]  is_stream   True indicate that texture will be streamed
 */
OGLTexture::OGLTexture(const cv::Mat& image,
                       const int& mag_filter,
                       const int& wrap_mode) : OGLTexture::OGLTexture() {
  this->Init(image.cols,
             image.rows,
             image.data,
             TexFrmtFromBitmap(image),
             mag_filter,
             wrap_mode);
}

/*
 *  @name OGLTexture
 *  @fn OGLTexture(const OGLTexture& other)
 *  @brief  Copy constructor
 *  @param[in]  other Texture to copy from
 */
OGLTexture::OGLTexture(const OGLTexture& other) : RefCounted::RefCounted(other),
                                                  object_(other.object_),
                                                  width_(other.width_),
                                                  height_(other.height_),
                                                  frmt_(other.frmt_) {
}

/**
 *  @name operator=
 *  @fn OGLTexture& operator=(const OGLTexture& rhs)
 *  @brief  Assignment operator
 *  @param[in]  rhs   Texture to assign from
 */
OGLTexture& OGLTexture::operator=(const OGLTexture& rhs) {
  if (this != &rhs) {
    if (this->RefCountIsOne() && object_) {
      glDeleteTextures(1, &object_);
    }
    RefCounted::operator=(rhs);
    object_ = rhs.object_;
    width_ = rhs.width_;
    height_ = rhs.height_;
    frmt_ = rhs.frmt_;
  }
  return *this;
}


/*
 *  @name ~OGLTexture
 *  @fn ~OGLTexture()
 *  @brief  Destructor
 */
OGLTexture::~OGLTexture() {
  // Release texture
  if (this->Unref() && object_) {
    glDeleteTextures(1, &object_);
  }
}

/**
 * @name Init
 * @fn Init(const int& width,
                  const int& height,
                  const unsigned char* data,
                  const int& mag_filter,
                  const int& wrap_mod)
 * @brief Initialize texture
 * @param[in] width       Texture width
 * @param[in] height      Texture height
 * @param[in] data        Data array holding pixel value to upload. Can be set
 *                        to `nullptr`
 * @param[in] format      Data format
 * @param[in] mag_filter  Magnitude filter
 * @param[in] wrap_mode   Wrapping mode
 */
void OGLTexture::Init(const int& width,
                      const int& height,
                      const unsigned char* data,
                      const unsigned int& format,
                      const int& mag_filter,
                      const int& wrap_mode) {
  width_ = width;
  height_ = height;
  frmt_ = format;
  // Create texture object
  if (object_ == 0) {
    glGenTextures(1, &object_);
  }
  // Bind
  glBindTexture(GL_TEXTURE_2D, object_);
  // Alignment
  if (IsPowerOfTwo(width_)) {
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
  } else {
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  }
  // Set some properties
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mag_filter);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filter);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_mode);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_mode);
  // Add data
  glTexImage2D(GL_TEXTURE_2D,
               0,
               frmt_,
               width_,
               height_,
               0,
               static_cast<GLenum>(frmt_),
               GL_UNSIGNED_BYTE,
               data);
  glBindTexture(GL_TEXTURE_2D, 0);
}

#pragma mark -
#pragma mark Process

/*
 * @name  Upload
 * @fn  int Upload(const Bitmap& bitmap)
 * @brief Upload data into OpenGL buffer
 * @param[in] bitmap  Bitmap image to upload
 * @return -1 if error, 0 otherwise
 */
int OGLTexture::Upload(const Bitmap& bitmap) {
  int err = -1;
  if (object_) {
    // Upload data to opengl buffer
    glBindTexture(GL_TEXTURE_2D, object_);
    glTexSubImage2D(GL_TEXTURE_2D,
                    0,
                    0,
                    0,
                    width_,
                    height_,
                    frmt_,
                    GL_UNSIGNED_BYTE,
                    bitmap.data());
    glBindTexture(GL_TEXTURE_2D, 0);
    err = glGetError() == GL_NO_ERROR ? 0 : -1;
  }
  return err;
}

/*
 * @name  Upload
 * @fn  int Upload(const cv::Mat& image)
 * @brief Upload data into OpenGL buffer
 * @param[in] image  Image to upload
 * @return -1 if error, 0 otherwise
 */
int OGLTexture::Upload(const cv::Mat& image) {
  int err = -1;
  if (object_) {
    // Upload data to opengl buffer
    glBindTexture(GL_TEXTURE_2D, object_);
    glTexSubImage2D(GL_TEXTURE_2D,
                    0,
                    0,
                    0,
                    width_,
                    height_,
                    frmt_,
                    GL_UNSIGNED_BYTE,
                    image.data);
    glBindTexture(GL_TEXTURE_2D, 0);
    err = glGetError() == GL_NO_ERROR ? 0 : -1;
  }
  return err;
}

/*
 *  @name Bind
 *  @fn void Bind(unsigned int texture_unit)
 *  @brief  Bind texture
 *  @param[in]  texture_unit  Unit to bind to
 */
void OGLTexture::Bind(unsigned int texture_unit) const {
  assert(object_ != 0);
  // bind the texture and PBO
  glActiveTexture(texture_unit);
  glBindTexture(GL_TEXTURE_2D, object_);
}

/*
 *  @name Unbind
 *  @fn void Unbind()
 *  @brief  Unbind texture
 */
void OGLTexture::Unbind() const {
  glBindTexture(GL_TEXTURE_2D, 0);
}

#pragma mark -
#pragma mark Accessors

/*
 * @name  is_grayscale
 * @fn  bool is_grayscale() const
 * @brief Indicate if the texture is grayscale or not
 * @return  True if greyscale, False otherwise
 */
bool OGLTexture::is_grayscale() const {
  return frmt_ == GL_RED;
}

}  // namespace LTS5
