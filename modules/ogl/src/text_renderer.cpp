/**
 *  @file   text_renderer.cpp
 *  @brief  Text renderer for OpenGL context
 *
 *  @author Christophe Ecabert
 *  @date   20/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <vector>

#include "GL/glew.h"

#include "ft2build.h"
#include FT_FREETYPE_H

#include "lts5/ogl/text_renderer.hpp"
#include "lts5/ogl/arial.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

static constexpr const char* vert_shader_str = "#version 330 core\n"
        "layout (location = 0) in vec3 position;\n"
        "layout (location = 1) in vec4 vColor;\n"
        "layout (location = 2) in vec2 texCoord;\n"
        "uniform mat4 camera;\n"
        "out vec2 texCoord0;\n"
        "out vec4 vColor0;\n"
        "void main() {\n"
        "  gl_Position = camera * vec4(position, 1.f);\n"
        "  vColor0 = vColor;\n"
        "  texCoord0 = texCoord;\n"
        "}";
static constexpr const char* frag_shader_str = "#version 330 core\n"
        "in vec2 texCoord0;\n"
        "uniform sampler2D tex_sampler;\n"
        "in vec4 vColor0;\n"
        "out vec4 fragColor;\n"
        "void main() {    \n"
        "  vec4 sampled=vec4(1.f,1.f,1.f,texture(tex_sampler,texCoord0).r);\n"
        "  fragColor = vColor0 * sampled;\n"
        "}";

#pragma mark -
#pragma mark Initialization

/*
 * @name  OGLTextRenderer
 * @fn  OGLTextRenderer()
 * @brief Constructor
 */
OGLTextRenderer::OGLTextRenderer() : OGLBaseRenderer::OGLBaseRenderer(), tex_(0) {
  // Create mesh
  this->mesh_ = new LTS5::Mesh<float>();
  // Create shader
  std::vector<OGLShader> shader;
  shader.emplace_back(OGLShader(vert_shader_str, OGLShader::ShaderType::kVertex));
  shader.emplace_back(OGLShader(frag_shader_str, OGLShader::ShaderType::kFragment));
  // Create program
  using ProgAtt = LTS5::OGLProgram::Attributes;
  using ProgTyp = LTS5::OGLProgram::AttributeType;
  std::vector<ProgAtt> attrib;
  attrib.push_back(ProgAtt(ProgTyp::kPoint, 0, "position"));
  attrib.push_back(ProgAtt(ProgTyp::kColor, 1, "vColor"));
  attrib.push_back(ProgAtt(ProgTyp::kTexCoord, 2, "texCoord"));
  this->program_ = new OGLProgram(shader, attrib);
  program_->Use();
  program_->SetUniform("tex_sampler", 0);
  program_->StopUsing();
}

/*
 * @name  ~OGLTextRenderer
 * @fn  virtual ~OGLTextRenderer()
 * @brief Destructor
 */
OGLTextRenderer::~OGLTextRenderer() {
  // Clear mesh
  if (this->mesh_) {
    delete this->mesh_;
    this->mesh_ = nullptr;
  }
  // Clear program
  if (this->program_) {
    delete this->program_;
    this->program_ = nullptr;
  }
  // Clear dictionary
  if (tex_) {
    glDeleteTextures(1, &tex_);
  }
}

/*
 * @name  GenerateAtlas
 * @fn  int GenerateAtlas(const std::string& font_path)
 * @brief Generate texture for corresponding font
 * @param[in] font_path Path to font file (.ttf). If empty, load default
 *                      Arial font
 * @return -1 if error, 0 otherwise
 */
int OGLTextRenderer::GenerateAtlas(const std::string& font_path) {
  int err = -1;
  // Open font
  FT_Library ft;
  FT_Face face;
  if (!FT_Init_FreeType(&ft)) {
    // Init ok, load font
    FT_Error face_err;
    if (font_path.empty()) {
      face_err = FT_New_Memory_Face(ft, arial_ttf, arial_ttf_len, 0, &face);
    } else {
      face_err = FT_New_Face(ft, font_path.c_str(), 0, &face);
    }
    if (!face_err) {
      // Set glyph size
      FT_Set_Pixel_Sizes(face, 0, 25);
      // Compute atlas width + maximum glyph height
      GLsizei atlas_w = 0;
      GLsizei atlas_h = 0;
      dictionary_.clear();
      int n_row = 1;
      for (GLubyte c = 32; c < 128 ; ++c) {
        if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
          continue;
        }
        atlas_w += face->glyph->bitmap.width;
        atlas_h = std::max(atlas_h,
                           static_cast<GLsizei>(face->glyph->bitmap.rows));
        if (atlas_w > 512) {
          n_row += 1;
          atlas_w = face->glyph->bitmap.width;
        }
      }
      GLsizei tex_w = 512;
      GLsizei tex_h = 0;
      this->FindNextPowerOfTwo(n_row * atlas_h, &tex_h);
      // Create texture
      if (!tex_) {
        glGenTextures(1, &tex_);
      }
      glBindTexture(GL_TEXTURE_2D, tex_);
      // Disable byte-alignment restriction
      glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
      glTexImage2D(GL_TEXTURE_2D,
                   0,
                   GL_RED,
                   tex_w,
                   tex_h,
                   0,
                   GL_RED,
                   GL_UNSIGNED_BYTE,
                   0);
      // Set texture options
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      // Fill texture
      // Load first 128 characters of ASCII set
      GLint x_off = 0;
      GLint y_off = 0;
      for (GLubyte c = 32; c < 128; ++c) {
        // Load char
        if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
          std::cout << "Unable to load glyph : " << c << std::endl;
          continue;
        }
        // Check x_off/y_off boundaries
        if (x_off + face->glyph->bitmap.width > 512) {
          x_off = 0;
          y_off += atlas_h;
        }

        // Ok, fill the texture atlas
        glTexSubImage2D(GL_TEXTURE_2D,
                        0,
                        x_off,
                        y_off,
                        face->glyph->bitmap.width,
                        face->glyph->bitmap.rows,
                        GL_RED,
                        GL_UNSIGNED_BYTE,
                        face->glyph->bitmap.buffer);
        // Save reference
        GLfloat ux = static_cast<float>(x_off) / static_cast<float>(tex_w);
        GLfloat uy = static_cast<float>(y_off) / static_cast<float>(tex_h);
        GLfloat dx = (static_cast<float>(face->glyph->bitmap.width) /
                      static_cast<float>(tex_w));
        GLfloat dy = (static_cast<float>(face->glyph->bitmap.rows) /
                      static_cast<float>(tex_h));
        OGLCharacter character = {
                Vec2i(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                Vec2i(face->glyph->bitmap_left, face->glyph->bitmap_top),
                Vec2i(face->glyph->advance.x >> 6,
                      face->glyph->advance.y >> 6),
                {{ux, uy + dy}, {ux, uy}, {ux + dx, uy}, {ux + dx, uy + dy}}
        };
        dictionary_.emplace(c, character);
        // Define x_off/y_off
        x_off += face->glyph->bitmap.width;
      }
      // Unbind opengl
      glBindTexture(GL_TEXTURE_2D, 0);
      // Disable byte-alignment restriction
      glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
      // Sanity check
      err = glGetError() == GL_NO_ERROR ? 0 : -1;
    } else {
      std::cout << "Unable to load font : " << font_path << std::endl;
    }
  } else {
    std::cout << "Unable to init FontLibary" << std::endl;
  }
  // Release freetype
  FT_Done_Face(face);
  FT_Done_FreeType(ft);
  return err;
}

/*
 * @name  SetWindowDimension
 * @fn  void SetWindowDimension(const int width, const int height)
 * @brief Set dimension of the window to render in
 * @param[in] width   Window width
 * @param[in] height  Window height
 * @return -1 if error, 0 otherwise
 */
void OGLTextRenderer::SetWindowDimension(const int& width, const int& height) {
  using Mat4f = Matrix4x4<float>;
  // Setup projection matrix
  auto proj = Mat4f::Ortho(0.f,
                           static_cast<float>(width),
                           static_cast<float>(height),
                           0.f);
  this->program_->Use();
  this->program_->SetUniform("camera", proj);
  this->program_->StopUsing();
}

#pragma mark -
#pragma mark Usage

/*
 * @name  AddText
 * @fn  int AddText(const std::string& text,
                    const float x,
                    const float y,
                    const float scale,
                    const Color color,
                    const bool append);
 * @brief Add text to the current render for a given position, scale and color
 * @param[in] text      Text to display
 * @param[in] x         On screen X position
 * @param[in] y         On screen Y position
 * @param[in] scale     Font scale
 * @param[in] color     Font color
 * @param[in] location  Location where to start to add text.
 *                      Append at the end = -1, otherwise specify position
 *                      in term of char
 * @return -1 if error, 0 otherwise
 */
int OGLTextRenderer::AddText(const std::string& text,
                             const float& x,
                             const float& y,
                             const float& scale,
                             const Color& color,
                             const int& location) {
  int err = text.empty() ? -1 : 0;
  // Get mesh reference + Init
  std::vector<Vertex>& vertex = this->mesh_->get_vertex();
  std::vector<Triangle>& tri = this->mesh_->get_triangle();
  std::vector<TCoord>& tcoord = this->mesh_->get_tex_coord();
  std::vector<Color>& v_color = this->mesh_->get_vertex_color();
  // Loop over string
  std::string::const_iterator c = text.cbegin();
  GLfloat xx = x;
  int start_vert;
  int start_tri;
  if (location == -1) {
    start_vert = static_cast<int>(vertex.size());
    start_tri = static_cast<int>(tri.size());
  } else {
    start_vert = location * 4;
    start_tri = location * 2;
  }
  const OGLCharacter& ch_H = dictionary_.find('H')->second;
  for (; c != text.cend() ; ++c) {
    // Search in dict for corresponding OGLCharacters
    const auto pair = dictionary_.find(*c);
    if (pair != dictionary_.end()) {
      // Access char
      const OGLCharacter& ch = pair->second;
      // Define position + w + h
      GLfloat x_pos = xx + ch.bearing.x_ *scale;
      GLfloat y_pos = y + (ch_H.bearing.y_ - ch.bearing.y_) *scale;
      GLfloat w = ch.size.x_ * scale;
      GLfloat h = ch.size.y_ * scale;
      // Advance the cursor to the start of the next character
      xx += ch.advance.x_ * scale;
      // Skip glyphs that have no pixels
      if (!w || !h) {
        continue;
      }
      // Setup vertex + tex coord
      const Vertex v0 = Vertex(x_pos, y_pos + h, 0.f);
      const Vertex v1 = Vertex(x_pos, y_pos, 0.f);
      const Vertex v2 = Vertex(x_pos + w, y_pos, 0.f);
      const Vertex v3 = Vertex(x_pos + w, y_pos + h, 0.f);
      const TCoord t0 = ch.tcoord[0];
      const TCoord t1 = ch.tcoord[1];
      const TCoord t2 = ch.tcoord[2];
      const TCoord t3 = ch.tcoord[3];
      const Triangle T0 = Triangle(start_vert, start_vert + 1, start_vert + 2);
      const Triangle T1 = Triangle(start_vert, start_vert + 2, start_vert + 3);
      if (location == -1) {
        vertex.push_back(v0);
        vertex.push_back(v1);
        vertex.push_back(v2);
        vertex.push_back(v3);
        tcoord.push_back(t0);
        tcoord.push_back(t1);
        tcoord.push_back(t2);
        tcoord.push_back(t3);
        tri.push_back(T0);
        tri.push_back(T1);
        v_color.push_back(color);
        v_color.push_back(color);
        v_color.push_back(color);
        v_color.push_back(color);
      } else {
        vertex[start_vert] = v0;
        vertex[start_vert + 1] = v1;
        vertex[start_vert + 2] = v2;
        vertex[start_vert + 3] = v3;
        tcoord[start_vert] = t0;
        tcoord[start_vert + 1] = t1;
        tcoord[start_vert + 2] = t2;
        tcoord[start_vert + 3] = t3;
        tri[start_tri] = T0;
        tri[start_tri + 1] = T1;
        v_color[start_vert] = color;
        v_color[start_vert + 1] = color;
        v_color[start_vert + 2] = color;
        v_color[start_vert + 3] = color;
      }
      start_vert += 4;
      start_tri += 2;
    } else {
      std::cout << "Unknow character : " << *c << std::endl;
    }
  }
  return err;
}

/*
 * @name
 * @fn  int QueryTextDimension(const std::string& text,
                               const float scale,
                               float* width,
                               float* height)
 * @brief Compute on screen \p text size
 * @param[in]   text    Text to estimate dimension
 * @param[in]   scale   Scaling factor (i.e. Font size)
 * @param[out]  width   Total \p text width
 * @param[out]  height  Total \p text height
 * @return -1 if error, 0 otherwise
 */
int OGLTextRenderer::QueryTextDimension(const std::string& text,
                                        const float& scale,
                                        float* width,
                                        float* height) {
  GLsizei w = 0;
  GLsizei h = 0;
  int err = -1;
  if (!dictionary_.empty()) {
    for (const auto &c : text) {
      // Search in dict for corresponding OGLCharacters
      const auto pair = dictionary_.find(c);
      if (pair != dictionary_.end()) {
        // Access char cached data
        const OGLCharacter &ch = pair->second;
        w += ch.advance.x_ * scale;
        h = std::max(h, ch.size.y_);
      }
    }
    *width = static_cast<float>(w);
    *height = static_cast<float>(h);
    err = 0;
  }
  return err;
}

/*
 * @name  UpdateVertex
 * @fn  int UpdateVertex()
 * @brief Update vertex position into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
int OGLTextRenderer::UpdateVertex() {
  return -1;
}

/*
 * @name  UpdateTCoord
 * @fn  int UpdateTCoord()
 * @brief Update texture coordinate into OpenGL context
 * @return  -1 if error, 0 otherwise
 */
int OGLTextRenderer::UpdateTCoord() {
  return -1;
}

/*
 * @name  Render
 * @fn  void Render()
 * @brief Render into OpenGL context
 */
void OGLTextRenderer::Render() {
  // Link to VAO
  this->vao_->Bind();
  // Enable program
  program_->Use();
  // Enable texture
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex_);
  // Render
  const std::vector<Triangle>& tri = this->mesh_->get_triangle();
  glDrawElementsBaseVertex(GL_TRIANGLES,
                           static_cast<GLsizei>(tri.size() * 3),
                           GL_UNSIGNED_INT,
                           0,
                           0);
  // Unbind
  this->vao_->Unbind();
  glBindTexture(GL_TEXTURE_2D, 0);
  this->program_->StopUsing();
}

/*
 * @name  FindNextPowerOfTwo
 * @fn  void FindNextPowerOfTwo(const int x, int* pow2)
 * @brief Round up to the next highest power of 2
 * @param[in]   x     X value
 * @param[out]  pow2  Next power of 2 for a given value
 */
void OGLTextRenderer::FindNextPowerOfTwo(const int& x, int* pow2) {
  int v = x - 1;
  v |= v >> 1;
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
  *pow2 = v + 1;
}

}  // namepsace LTS5
