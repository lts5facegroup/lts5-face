/**
 *  @file   glfw_backend.cpp
 *  @brief  Backend helper for GLFW.
 *          Based on : http://ogldev.atspace.co.uk/
 *
 *  @author Christophe Ecabert
 *  @date   10/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <cassert>

#include "GL/glew.h"

#include "lts5/utils/logger.hpp"
#include "lts5/ogl/key_types.hpp"
#include "lts5/ogl/glfw_backend.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  OGLGlfwBackend
 * @fn  OGLGlfwBackend()
 * @brief Constructor
 */
OGLGlfwBackend::OGLGlfwBackend() {
  with_depth_ = false;
  with_stencil_ = false;
}

/*
 * @name  ~OGLGlfwBackend
 * @fn  ~OGLGlfwBackend()
 * @brief Destructor
 */
OGLGlfwBackend::~OGLGlfwBackend() {
  this->Terminate();
}

/*
 * @name  Init
 * @fn  void Init(int argc, const char** argv,
 *                              bool with_depth, bool with_stencil)
 * @brief Initialize GLFW Backend
 * @param[in] argc          Number of parameters in Command line
 * @param[in] argv          Command line parameters
 * @param[in] with_depth    Use depth
 * @param[in] with_stencil  Use stencil
 */
void OGLGlfwBackend::Init(int argc,
                          const char** argv,
                          bool with_depth,
                          bool with_stencil,
                          bool with_back_culling) {
  with_depth_ = with_depth;
  with_stencil_ = with_stencil;
  with_back_cull_ = with_back_culling;
#ifdef HAS_GLFW_
  // Setup error callback
  glfwSetErrorCallback(GLFWErrorCallback);
  // Init
  if (glfwInit() != 1) {
    LTS5_LOG_ERROR("Error initializing GLFW");
    exit(1);
  }
  // Get version
  int Major, Minor, Rev;
  glfwGetVersion(&Major, &Minor, &Rev);
  LTS5_LOG_DEBUG("GLFW " << Major << "." << Minor << "." << Rev);
#else
  LTS5_LOG_ERROR("GLFW Not supported");
#endif
}

/*
 * @name  Terminate
 * @fn  void Terminate()
 * @brief Shutdown GLFW backend
 */
void OGLGlfwBackend::Terminate() {
#ifdef HAS_GLFW_
  glfwDestroyWindow(windows_);
  glfwTerminate();
#else
  LTS5_LOG_ERROR("GLFW Not supported");
#endif
}

/*
 * @name  CreateWindows
 * @fn  bool CreateWindows(const int width, const int height,
 *                                       const bool fullscreen,
 *                                       const char* window_title)
 * @brief Initialiaze GLFW window
 * @param[in] width         Window's width
 * @param[in] height        Window's height
 * @param[in] fullscreen    True for fullscreen window
 * @param[in] window_title  Window's title
 */
bool OGLGlfwBackend::CreateWindows(const int width,
                                   const int height,
                                   const bool fullscreen,
                                   const char* window_title) {
  bool ret = false;
#ifdef HAS_GLFW_
  // Get monitor
  GLFWmonitor* monitor = fullscreen ? glfwGetPrimaryMonitor() : nullptr;
  // Set window
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_SAMPLES, 4);  // Anti-aliasing
  glfwWindowHint(GLFW_REFRESH_RATE, 30);
  windows_ = glfwCreateWindow(width, height, window_title,monitor, nullptr);
  if (!windows_) {
    LTS5_LOG_ERROR("Unable to create GLFW window");
    exit(1);
  }
  // Enable current context
  glfwMakeContextCurrent(windows_);

  // Init GLEW ()
#ifndef __APPLE__
  glewExperimental = GL_TRUE;
  GLenum res = glewInit();
  if (res != GLEW_OK) {
    LTS5_LOG_ERROR(glewGetErrorString(res));
    return false;
  }
  // GLEW throws some errors, so discard all the errors so far
  while(glGetError() != GL_NO_ERROR) {}
#endif
  ret = (windows_ != nullptr);
#else
  LTS5_LOG_ERROR("GLFW Not supported");
#endif
  return ret;
}

/*
 * @name  EnableBlending
 * @fn  void EnableBlending()
 * @brief Enable OpenGL blending
 */
void OGLGlfwBackend::EnableBlending() {
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

#pragma mark -
#pragma mark Usage

/*
 * @name  OGLGlfwBackendRun
 * @fn  void OGLGlfwBackendRun(OGLCallback* ogl_callback)
 * @brief Enter into GLFW main loop
 * @param[in] ogl_callback  Pointer to object holding opengl callback
 *                          implementation
 */
void OGLGlfwBackend::Run(OGLCallback* ogl_callback) {
  assert(ogl_callback);
  // OpenGL basic settings
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glFrontFace(GL_CCW);
  if (with_back_cull_) {
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
  }
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  if (with_depth_) {
    glEnable(GL_DEPTH_TEST);
  }
  // Anti-aliasing
  glEnable(GL_MULTISAMPLE);
  // Set callback ref
  callback_ = ogl_callback;
  // Init
  InitCallbacks();
#ifdef HAS_GLFW_
  // Enter main loop
  while (!glfwWindowShouldClose(windows_)) {
    callback_->OGLRenderCb();
    glfwSwapBuffers(windows_);
    glfwPollEvents();
  }
#else
  LTS5_LOG_ERROR("GLFW Not supported");
#endif
}

/*
 * @name  OGLGlfwBackendSwapBuffer
 * @fn  void OGLGlfwBackendSwapBuffer()
 * @brief Swap opengl buffer in order to display what has been rendered
 */
void OGLGlfwBackend::SwapBuffer() {
  // Nothing to do here -> swap into main loop
  glfwSwapBuffers(windows_);
}

/*
 * @name  OGLGlfwBackendLeaveMainLoop
 * @fn  void OGLGlfwBackendLeaveMainLoop()
 * @brief Leave backend mainloop
 */
void OGLGlfwBackend::LeaveMainLoop() {
#ifdef HAS_GLFW_
  glfwSetWindowShouldClose(windows_, 1);
#else
  LTS5_LOG_ERROR("GLFW Not supported");
#endif
}

/**
 * @name  PollEvents
 * @fn  void PollEvents()
 * @brief Poll event from OpenGL context
 */
void OGLGlfwBackend::PollEvents() {
#ifdef HAS_GLFW_
  glfwPollEvents();
#else
  LTS5_LOG_ERROR("GLFW Not supported");
#endif
}

/*
 * @name  OGLGlfwBackendSetMousePos
 * @fn  void OGLGlfwBackendSetMousePos(const int x, const int y)
 * @brief Set mouse position inside window
 * @param[in] x   Cursor X position
 * @param[in] y   Cursor Y position
 */
void OGLGlfwBackend::SetMousePos(const int x, const int y) {
#ifdef HAS_GLFW_
  glfwSetCursorPos(windows_, static_cast<double>(x), static_cast<double>(y));
#else
  LTS5_LOG_ERROR("GLFW Not supported");
#endif
}

/*
 * @name  ClearBuffer
 * @fn  void ClearBuffer()
 * @brief Clear OpenGL buffer
 */
void OGLGlfwBackend::ClearBuffer() {
  glClearColor(0.f, 0.f, 0.f, 1.f); // black
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

#pragma mark -
#pragma mark Setter

/*
 * @name  SetCallbacks
 * @fn  SetCallbacks(const OGLCallback* callback)
 * @brief Setup callbacks
 * @param[in] callback Callback to invoke when event happens
 */
void OGLGlfwBackend::SetCallbacks(const OGLCallback* callback) {
  callback_ = const_cast<OGLCallback*>(callback);
  this->InitCallbacks();
}


#pragma mark -
#pragma mark Private

/* Pointer to the object implementing OGLCallback interface. All event are
 * forwarded to this object */
OGLCallback* OGLGlfwBackend::callback_;

/*
 * @name  GlfwKeyToOGLKey
 * @fn  static OGLKey GlfwKeyToOGLKey(unsigned int glfw_key)
 * @brief Convert GLFW key into OGLKey format
 * @param[in] key GLFW key to convert
 * @return  OGLKey equivalent if exist otherwise kUndefined
 */
OGLKey OGLGlfwBackend::GlfwKeyToOGLKey(unsigned int glfw_key) {
#ifdef HAS_GLFW_
  if (glfw_key >= GLFW_KEY_SPACE && glfw_key <= GLFW_KEY_RIGHT_BRACKET) {
    return (OGLKey)glfw_key;
  }
  switch (glfw_key) {
    case GLFW_KEY_ESCAPE:
      return OGLKey::kESCAPE;
    case GLFW_KEY_ENTER:
      return OGLKey::kENTER;
    case GLFW_KEY_TAB:
      return OGLKey::kTAB;
    case GLFW_KEY_BACKSPACE:
      return OGLKey::kBACKSPACE;
    case GLFW_KEY_INSERT:
      return OGLKey::kINSERT;
    case GLFW_KEY_DELETE:
      return OGLKey::kDELETE;
    case GLFW_KEY_RIGHT:
      return OGLKey::kRIGHT;
    case GLFW_KEY_LEFT:
      return OGLKey::kLEFT;
    case GLFW_KEY_DOWN:
      return OGLKey::kDOWN;
    case GLFW_KEY_UP:
      return OGLKey::kUP;
    case GLFW_KEY_PAGE_UP:
      return OGLKey::kPAGE_UP;
    case GLFW_KEY_PAGE_DOWN:
      return OGLKey::kPAGE_DOWN;
    case GLFW_KEY_HOME:
      return OGLKey::kHOME;
    case GLFW_KEY_END:
      return OGLKey::kEND;
    case GLFW_KEY_F1:
      return OGLKey::kF1;
    case GLFW_KEY_F2:
      return OGLKey::kF2;
    case GLFW_KEY_F3:
      return OGLKey::kF3;
    case GLFW_KEY_F4:
      return OGLKey::kF4;
    case GLFW_KEY_F5:
      return OGLKey::kF5;
    case GLFW_KEY_F6:
      return OGLKey::kF6;
    case GLFW_KEY_F7:
      return OGLKey::kF7;
    case GLFW_KEY_F8:
      return OGLKey::kF8;
    case GLFW_KEY_F9:
      return OGLKey::kF9;
    case GLFW_KEY_F10:
      return OGLKey::kF10;
    case GLFW_KEY_F11:
      return OGLKey::kF11;
    case GLFW_KEY_F12:
      return OGLKey::kF12;
    default:
      LTS5_LOG_WARNING("Unimplemented OGLkey");
  }
#else
  LTS5_LOG_ERROR("GLFW Not supported");
#endif
  return OGLKey::kUNDEFINED;
}

/*
 * @name  GlfwMouseToOGLMouse
 * @fn  static OGLMouse GlfwMouseToOGLMouse(unsigned int button)
 * @brief Convert GLFW button into OGLMouse format
 * @param[in] button GLFW button to convert
 * @return  OGLMouse equivalent if exist otherwise kUndefined
 */
OGLMouse OGLGlfwBackend::GlfwMouseToOGLMouse(unsigned int button) {
#ifdef HAS_GLFW_
  switch (button) {
    case GLFW_MOUSE_BUTTON_LEFT:
      return OGLMouse::kMouseLeft;
    case GLFW_MOUSE_BUTTON_RIGHT:
      return OGLMouse::kMouseRight;
    case GLFW_MOUSE_BUTTON_MIDDLE:
      return OGLMouse::kMouseMiddle;
    default:
      LTS5_LOG_WARNING("Unimplemented OGLMouse button");
  }
#else
  LTS5_LOG_ERROR("GLFW Not supported");
#endif
  return OGLMouse::kUndefined;
}

/*
 * @name  KeyCallback
 * @fn  static void KeyCallback(GLFWwindow* window,int key,int scancode,
                                int action, int mods)
 * @brief Wrapper for GLFW callback and OGLCallback
 * @param[in] window    GLFW window that invoke callback
 * @param[in] key       Key that trigger callback
 * @param[in] scancode  GLFW scan code
 * @param[in] action    Key action
 * @param[in] mods      GLFW mod
 */
#ifdef HAS_GLFW_
void OGLGlfwBackend::KeyCallback(GLFWwindow* window,
                 int key,
                 int scancode,
                 int action,
                 int mods) {
  OGLKey ogl_key = GlfwKeyToOGLKey(key);
  OGLKeyState ogl_key_state = ((action == GLFW_PRESS) ?
                               OGLKeyState::kPress :
                               OGLKeyState::kRelease);
  callback_->OGLKeyboardCb(ogl_key, ogl_key_state);
}
#endif

/*
 * @name  CursorPosCallback
 * @fn  static void CursorPosCallback(GLFWwindow* window, double x, double y)
 * @brief Callback for handling mouse displacement inside GLFW window
 * @param[in] window  GLFW window that invoke the callback
 * @param[in] x Cursor X position inside window
 * @param[in] y Cursor Y position inside window
 */
#ifdef HAS_GLFW_
void OGLGlfwBackend::CursorPosCallback(GLFWwindow* window, double x, double y) {
  callback_->OGLPassiveMouseCb(static_cast<int>(x),static_cast<int>(y));
}
#endif

/*
 * @name  MouseButtonCallback
 * @fn  static void MouseButtonCallback(GLFWwindow* window,int button,
 *                                      int action,int mode)
 * @brief Callback for handling mouse click event inside GLFW window
 * @param[in] window  GLFW window that invoke the callback
 * @param[in] button  Which mouse button trigger the callback
 * @param[in] action  Button action
 * @param[in] mode    GLFW Mode
 */
#ifdef HAS_GLFW_
void OGLGlfwBackend::MouseButtonCallback(GLFWwindow* window,
                                  int button,
                                  int action,
                                  int mode) {
  OGLMouse ogl_mouse = GlfwMouseToOGLMouse(button);

  OGLKeyState ogl_state = ((action == GLFW_PRESS) ?
                           OGLKeyState ::kPress :
                           OGLKeyState::kRelease);
  double x = -1.0, y = -1.0;
#ifdef HAS_GLFW_
  glfwGetCursorPos(window, &x, &y);
#else
  LTS5_LOG_ERROR("GLFW Not supported");
#endif
  callback_->OGLMouseCb(ogl_mouse,
                        ogl_state,
                        static_cast<int>(x),
                        static_cast<int>(y));
}
#endif

/*
 * @name  InitCallbacks
 * @fn  static void InitCallbacks()
 * @brief Initialize GLFW callbacks
 */
void OGLGlfwBackend::InitCallbacks()
{
#ifdef HAS_GLFW_
  glfwSetKeyCallback(windows_, KeyCallback);
  glfwSetCursorPosCallback(windows_, CursorPosCallback);
  glfwSetMouseButtonCallback(windows_, MouseButtonCallback);
#else
  LTS5_LOG_ERROR("GLFW Not supported");
#endif
}

/*
 * @name  GLFWErrorCallback
 * @fn  static void GLFWErrorCallback(int error, const char* description)
 * @brief GLFW error callbacks
 * @param[in] error         Error number
 * @param[in] description   Error message
 */
void OGLGlfwBackend::GLFWErrorCallback(int error, const char* description) {
  LTS5_LOG_ERROR("GLFW error " << error << " - " << description);
  exit(0);
}
}