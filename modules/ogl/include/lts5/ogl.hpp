/**
 *  @file   ogl.hpp
 *  @brief  3D Structure + Rendering abstraction
 *
 *  @author Christophe Ecabert
 *  @date   13/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/ogl/ogl.hpp"
