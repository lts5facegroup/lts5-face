/**
 *  @file   vertex_array_object.hpp
 *  @brief  OpenGL Vertex Array Object (VAO) abstration layer
 *  @ingroup    ogl
 *
 *  @author Christophe Ecabert
 *  @date   12/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_VERTEX_ARRAY_OBJECT__
#define __LTS5_VERTEX_ARRAY_OBJECT__

#include <cstdio>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLVertexArrayObject
 * @brief   OpenGL Vertex Array Object (VAO) abstration layer
 * @author  Christophe Ecabert
 * @date    12/09/2017
 * @ingroup ogl
 * @tparam T Data type
 */
template<typename T>
class LTS5_EXPORTS OGLVertexArrayObject {
 public:

#pragma mark -
#pragma mark Type Definition

  /**
   * @enum  BufferType
   * @brief List of possible buffer type in a VAO.
   *            Attribute : vertex, normal, ...
   *            Element : Triangle indexes
   */
  enum class BufferType : char {
    /** Attribute */
    kAttribute,
    /** Element */
    kElement
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name  OGLVertexArrayObject
   * @fn    OGLVertexArrayObject()
   * @brief Constructor
   */
  OGLVertexArrayObject();

  /**
   * @name  OGLVertexArrayObject
   * @fn    OGLVertexArrayObject(const OGLVertexArrayObject& other) = delete
   * @brief Copy Constructor
   */
  OGLVertexArrayObject(const OGLVertexArrayObject& other) = delete;

  /**
   * @name  OGLVertexArrayObject
   * @fn    OGLVertexArrayObject(const OGLVertexArrayObject& other) = delete
   * @brief Move Constructor
   */
  OGLVertexArrayObject(OGLVertexArrayObject&& other) = delete;

  /**
   * @name operator=
   * @fn OGLVertexArrayObject& operator=(const OGLVertexArrayObject& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assigned from
   * @return    Newly assigned bobject
   */
  OGLVertexArrayObject& operator=(const OGLVertexArrayObject& rhs) = delete;

  /**
   * @name operator=
   * @fn OGLVertexArrayObject& operator=(OGLVertexArrayObject&& rhs) = delete
   * @brief Move assignment operator
   * @param[in] rhs Object to move assign from
   * @return    Newly moved assign bobject
   */
  OGLVertexArrayObject& operator=(OGLVertexArrayObject&& rhs) = delete;

  /**
   * @name  ~OGLVertexArrayObject
   * @fn    ~OGLVertexArrayObject()
   * @brief Destructor
   */
  ~OGLVertexArrayObject();

  /**
   * @name  Create
   * @fn    int Create(const int& n_buffer)
   * @brief Initialize VAO and create a given number of buffer
   * @param[in] n_buffer Number of buffer to create
   * @return -1 if error, 0 otherwise
   */
  int Create(const int& n_buffer);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Bind
   * @fn    void Bind() const
   * @brief Bind VAO
   */
  void Bind() const;

  /**
   * @name  Unbind
   * @fn    void Unbind() const
   * @brief Unbind VAO
   */
  void Unbind() const;

  /**
   * @name  BindBuffer
   * @fn    void BindBuffer(const size_t& index, const BufferType& type) const
   * @brief Bind a specific VBO
   * @param[in] index   Buffer index to bind
   * @param[in] type    Buffer's type
   */
  void BindBuffer(const size_t& index, const BufferType& type) const;

  /**
   * @name  Map
   * @fn    void* Map(const size_t& index, const BufferType& type) const
   * @brief Map an OpenGL resource to the host
   * @param[in] index   Buffer index to map
   * @param[in] type    Type of buffer to map
   * @return Pointer to the mapped resource or nullptr if failed.
   */
  void* Map(const size_t& index, const BufferType& type) const;

  /**
   * @name  Unmap
   * @fn    int Unmap(const size_t& index, const BufferType& type) const
   * @brief Unmmap an OpenGL resource from the host
   * @param[in] index   Buffer index to unmap
   * @param[in] type    Type of buffer to unmap
   * @return -1 if error, 0 otherwise
   */
  int Unmap(const size_t& index, const BufferType& type) const;

  /**
   * @name  AddData
   * @fn    int AddData(const size_t& n_bytes,
                        const void* data,
                        const BufferType& type) const
   * @brief Add data to the currently linked VBO
   * @param[in] n_bytes Number of bytes to copy to OpenGL memory
   * @param[in] data    Array storing data
   * @param[in] type    Buffer's type
   * @return    -1 if error, 0 otherwise
   */
  int AddData(const size_t& n_bytes,
              const void* data,
              const BufferType& type) const;

  /**
   * @name  UpdateData
   * @fn    int UpdateData(const size_t& n_bytes,
                           const void* data,
                           const BufferType& type) const
   * @brief Update data to the currently linked VBO
   * @param[in] n_bytes Number of bytes to copy to OpenGL memory
   * @param[in] data    Array storing data
   * @param[in] type    Buffer's type
   * @return    -1 if error, 0 otherwise
   */
  int UpdateData(const size_t& n_bytes,
                 const void* data,
                 const BufferType& type) const;

  /**
   * @name  SetAttributePointer
   * @fn    int SetAttributePointer(const size_t& index, const int& size) const
   * @brief Set attribute's properties for a given buffer in the VAO
   * @param[in] index   Buffer index
   * @param[in] size    Attribute's size (vertex=3, tcorrd=2, ...)
   * @return    -1 if error, 0 otherwise
   */
  int SetAttributePointer(const size_t& index, const int& size) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_ogl_buffer
   * @fn    unsigned int get_ogl_buffer(const int& buff_idx) const
   * @brief Provide reference to OpenGL buffer object
   * @param[in] buff_idx Buffer index for which to query reference
   * @return    OpenGL Buffer reference or 0 if something went wrong
   */
  unsigned int get_ogl_buffer(const int& buff_idx) const {
    if (buff_idx < n_buffer_) {
      return vbo_[buff_idx];
    }
    return 0;
  }

#pragma mark -
#pragma mark Private
 private:
  /** Vertex Array Object */
  unsigned int vao_;
  /** Vertex Buffer Object */
  unsigned int* vbo_;
  /** Number of buffer linked to this VAO */
  int n_buffer_;
};

}  // namepsace LTS5

#endif //__LTS5_VERTEX_ARRAY_OBJECT__
