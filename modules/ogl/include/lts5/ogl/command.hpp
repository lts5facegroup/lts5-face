/**
 *  @file   command.hpp
 *  @brief  Thin wrapper for opengl command
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   11/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_OGL_COMMAND__
#define __LTS5_OGL_COMMAND__

#include <cstdint>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @struct  OGLDrawCommand
 * @brief Indexed rendering command
 */
struct OGLDrawCommand {
  /** Specifies the number of elements to be rendered */
  uint32_t count;
  /** Specifies the number of instances of the indexed geometry that should
   * be drawn */
  uint32_t instance_count;
  /** Specifies a pointer to the location where the indices are stored. */
  uint32_t first_index;
  /** Specifies a constant that should be added to each element of indices when
   * chosing elements from the enabled vertex arrays */
  uint32_t base_vertex;
  /** Specifies the base instance for use in fetching instanced vertex
   * attributes. */
  uint32_t base_instance;
};


/**
 * @class   OGLCommand
 * @brief   Thin wrapper for opengl command
 * @author  Christophe Ecabert
 * @date    12/11/2020
 * @ingroup ogl
 */
class LTS5_EXPORTS OGLCommand {
 public:

  enum Constant {
    kGLZero = 0,
    kGLFalse = 0,
    kGLNone = 0,
    kGLNoError = 0,
    kGLPoints = 0x0000,
    kGLTrue = 1,
    kGLOne = 1,
    kGLLines = 0x0001,
    kGLLineLoop = 0x0002,
    kGLLineStrip = 0x0003,
    kGLTriangles = 0x0004,
    kGLTriangleStrip = 0x0005,
    kGLTriangleFan = 0x0006,
    kGLQuads = 0x0007,
    kGLQuadStrip = 0x0008,
    kGLPolygon = 0x0009,

    kGLSrcAlpha = 0x0302,
    kGLOneMinusSrcAlpha = 0x0303,
    kGLDstAlpha = 0x0304,
    kGLOneMinusDstAlpha = 0x0305,

    kGLDepthBufferBit = 0x00000100,
    kGLAccumBufferBit = 0x00000200,
    kGLStencilBufferBit =  0x00000400,
    kGLFront = 0x0404,
    kGLBack = 0x0405,
    kGLLeft = 0x0406,
    kGLRight = 0x0407,
    kGLFrontAndBack = 0x0408,
    kGLCw = 0x0900,
    kGLCcw = 0x0901,

    kGLCullFace = 0x0B44,
    kGLDepthTest = 0x0B71,

    kGLAlphaTest = 0x0BC0,
    kGLAlphaTestFunc = 0x0BC1,
    kGLAlphaTestRef = 0x0BC2,
    kGLDither = 0x0BD0,
    kGLBlendDst = 0x0BE0,
    kGLBlendSrc = 0x0BE1,
    kGLBlend = 0x0BE2,

    kGLByte = 0x1400,
    kGLUnsignedByte = 0x1401,
    kGLShort = 0x1402,
    kGLUnsignedShort = 0x1403,
    kGLInt = 0x1404,
    kGLUnsignedInt = 0x1405,
    kGLFloat = 0x1406,

    kGLNearest = 0x2600,
    kGLLinear = 0x2601,
    kGLNearestMipMapNearest = 0x2700,
    kGLLinearMipMapNearest = 0x2701,
    kGLNearestMipMapLinear = 0x2702,
    kGLLinearMipMapLinear = 0x2703,
    kGLTextureMagFilter = 0x2800,
    kGLTextureMinFilter = 0x2801,
    kGLTestureWrapS = 0x2802,
    kGLTextureWrapT = 0x2803,

    kGLColorBufferBit =  0x00004000,
    kGLClampToEdge = 0x812F
  };

  /**
   * @name  GlGetError
   * @brief Return error information
   * @return Return error code
   */
  static int GlGetError();

  /**
   * @name  GlEnable
   * @brief Trun on a given GL capability
   * @param[in] cap
   * @return -1 if error, 0 otherwise
   */
  static int GlEnable(unsigned int cap);

  /**
   * @name  GlDisable
   * @brief Trun off a given GL capability
   * @param[in] cap
   * @return -1 if error, 0 otherwise
   */
  static int GlDisable(unsigned int cap);

  /**
   * @name  GlBlendFunc
   * @brief specify pixel arithmetic
   * @param[in] sfactor Specifies how the red, green, blue, and alpha source
   *    blending factors are computed. The initial value is GL_ONE.
   * @param[in] dfactor Specifies how the red, green, blue, and alpha
   *    destination blending factors are computed.
   * @return    -1 if error, 0 otherwise
   */
  static int GlBlendFunc(unsigned int sfactor, unsigned int dfactor);

  /**
   * @name  GlClear
   * @brief  clear buffers to preset values
   * @param[in] mask Bitwise OR of masks that indicate the buffers to be
   *    cleared. The four masks are GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT,
   *    GL_ACCUM_BUFFER_BIT, and GL_STENCIL_BUFFER_BIT.
   * @return -1 if error, 0 otherwise
   */
  static int GlClear(unsigned int mask);

  /**
   * @name  GlClearColor
   * @brief specify clear values for the color buffers
   * @param[in] r   Red color
   * @param[in] g   Green color
   * @param[in] b   Blue color
   * @param[in] a   Aplha color
   * @return    -1 if error, 0 otherwise
   */
  static int GlClearColor(float r, float g, float b, float a);

  /**
   * @name  GlViewport
   * @brief Set the viewport
   * @param[in] x   Lower left x component
   * @param[in] y   Lower left y component
   * @param[in] width   Width of the viewport
   * @param[in] height  Height of the viewport
   * @return -1 if error , 0 otherwise
   */
  static int GlViewport(int x, int y, int width, int height);

  /**
   * @name  GlFrontFace
   * @brief define front- and back-facing polygons
   * @param[in] mode    Specifies the orientation of front-facing polygons.
   *    GL_CW and GL_CCW are accepted. The initial value is GL_CCW.
   * @return -1 if error, 0 otherwise
   */
  static int GlFrontFace(unsigned int mode);

  /**
   * @name  GlCullFace
   * @brief specify whether front- or back-facing facets can be culled
   * @param[in] mode    Specifies whether front- or back-facing facets are
   *    candidates for culling. Symbolic constants GL_FRONT, GL_BACK, and
   *    GL_FRONT_AND_BACK are accepted. The initial value is GL_BACK.
   * @return -1 if error, 0 otherwise
   */
  static int GlCullFace(unsigned int mode);

  /**
   * @name      GlDrawElements
   * @brief     Render primitives from array data
   * @param[in] mode    Specifies what kind of primitives to render.
   * @param[in] count   Specifies the number of elements to be rendered.     
   * @param[in] type    Specifies the type of the values in indices
   * @param[in] indices Specifies a pointer to the location where the indices
   *    are stored.
   * @return -1 if error, 0 otherwise
   */
  static int GlDrawElements(unsigned int mode,
                            int count,
                            unsigned int type,
                            const void* indices);

  /**
   * @name  GlMultiDrawElementsIndirect
   * @brief Render indexed primitives from array data, taking parameters from
   *    memory
   * @param[in] mode    Specifies what kind of primitives to render.
   * @param[in] type    Specifies the type of data in the buffer bound to the
   *    GL_ELEMENT_ARRAY_BUFFER binding.
   * @param[in] indirect    Specifies the address of a structure containing an
   *    array of draw parameters.
   * @param[in] drawcount   Specifies the number of elements in the array
   *    addressed by indirect.
   * @param[in] stride  Specifies the distance in basic machine units between
   *    elements of the draw parameter array.
   * @return -1 if error, 0 otherwise
   */
  static int GlMultiDrawElementsIndirect(unsigned int mode,
                                         unsigned int type,
                                         const void* indirect,
                                         int drawcount,
                                         int stride);
};

}  // namespace LTS5
#endif  // __LTS5_OGL_COMMAND__
