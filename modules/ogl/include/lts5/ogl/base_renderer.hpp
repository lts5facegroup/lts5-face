/**
 *  @file   base_renderer.hpp
 *  @brief  Base class for OpenGL renderer
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   19/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_BASE_RENDERER__
#define __LTS5_BASE_RENDERER__

#include <cstdint>

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/ogl/program.hpp"
#include "lts5/ogl/texture.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/ogl/vertex_array_object.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLBaseRenderer
 * @brief   Base OpenGL rendering class
 * @author  Christophe Ecabert
 * @date    19/05/2016
 * @ingroup ogl
 * @tparam T Data Type
 * @tparam ColorContainer Container type in which vertex color is stored
 */
template<typename T, typename ColorContainer=Vector4<T>>
class LTS5_EXPORTS OGLBaseRenderer {

#pragma mark -
#pragma mark Initialization
 public:

  /** Mesh type */
  using Mesh = LTS5::Mesh<T, ColorContainer>;
  /** Vertex type */
  using Vertex = typename LTS5::Mesh<T, ColorContainer>::Vertex;
  /** Triangle type */
  using Triangle = typename LTS5::Mesh<T, ColorContainer>::Triangle;
  /** Normal type */
  using Normal = typename LTS5::Mesh<T, ColorContainer>::Normal;
  /** Texture coordinate */
  using TCoord = typename LTS5::Mesh<T, ColorContainer>::TCoord;
  /** Vertex color */
  using Color = typename LTS5::Mesh<T, ColorContainer>::Color;

  /**
   * @enum Capability
   * @brief List of opengl capability
   */
  enum class Capability : std::uint16_t {
    /** Blend the computed fragment color values with the values in the
     * color buffers */
    kBlend,
    /** clip geometry against user-defined half space 0 */
    kClipDistance0,
    /** clip geometry against user-defined half space 1 */
    kClipDistance1,
    /** clip geometry against user-defined half space 2 */
    kClipDistance2,
    /** clip geometry against user-defined half space 3 */
    kClipDistance3,
    /** clip geometry against user-defined half space 4 */
    kClipDistance4,
    /** clip geometry against user-defined half space 5 */
    kClipDistance5,
    /** apply the currently selected logical operation to the computed fragment
     * color and color buffer values */
    kColorLogicOp,
    /** cull polygons based on their winding in window coordinates */
    kCullFace,
    /** No near or far plane clipping */
    kDepthClamp,
    /** Do depth comparisons and update the depth buffer */
    kDepthTest,
    /** Dither color components or indices before they are written to the color
     * buffer */
    kDither,
    /** Draw lines with correct filtering. Otherwise, draw aliased lines */
    kLineSmooth,
    /** Uuse multiple fragment samples in computing the final color of a
     * pixel */
    kMultisample,
    /** If the polygon is rendered in GL_FILL mode, an offset is added to depth
     * values of a polygon's fragments before the depth comparison is
     * performed */
    kPolygonOffsetFill,
    /** if the polygon is rendered in GL_LINE mode, an offset is added to depth
     * values of a polygon's fragments before the depth comparison is
     * performed */
    kPolygonOffsetLine,
    /** An offset is added to depth values of a polygon's fragments before the
     * depth comparison is performed, if the polygon is rendered in GL_POINT
     * mode */
    kPolygonOffsetPoint,
    /** Draw polygons with proper filtering */
    kPolygonSmooth,
    /** Primitives are discarded after the optional transform feedback stage,
     * but before rasterization */
    kRasterizerDiscard,
    /** Compute a temporary coverage value where each bit is determined by the
     * alpha value at the corresponding sample location */
    kSampleAlphaToCoverage,
    /** Each sample alpha value is replaced by the maximum representable alpha
     * value */
    kSampleAlphaToOne,
    /** The fragment's coverage is ANDed with the temporary coverage value */
    kSampleCoverage,
    /** The active fragment shader is run once for each covered sample, or at
     * fraction of this rate as determined by the current value of
     * GL_MIN_SAMPLE_SHADING_VALUE */
    kSampleShading,
    /** The sample coverage mask generated for a fragment during rasterization
     * will be ANDed with the value of GL_SAMPLE_MASK_VALUE before shading
     * occurs */
    kSampleMask,
    /** Discard fragments that are outside the scissor rectangle */
    kScissorTest,
    /** Do stencil testing and update the stencil buffer */
    kStencilTest,
    /** Cubemap textures are sampled such that when linearly sampling from the
     * border between two adjacent faces, texels from both faces are used to
     * generate the final sample value */
    kTextureCubeMapSeamless,
    /** If enabled and a vertex or geometry shader is active, then the derived
     * point size is taken from the (potentially clipped) shader builtin
     * gl_PointSize and clamped to the implementation-dependent point size
     * range */
    kProgramPointSize
  };

  /**
   * @enum  DepthFunc
   * @brief List of depth function usable during depth test
   */
  enum class DepthFunc : std::uint8_t {
    /** Never passes */
    kNever,
    /** Passes if the incoming depth value is less than the stored depth value
     * - DEFAULT BEHAVIOUR */
    kLess,
    /** Passes if the incoming depth value is equal to the stored depth value */
    kEqual,
    /** Passes if the incoming depth value is less than or equal to the stored
     * depth value */
    kLessOrEqual,
    /** Passes if the incoming depth value is greater than the stored depth
     * value */
    kGreater,
    /** Passes if the incoming depth value is not equal to the stored depth
     * value */
    kNotEqual,
    /** Passes if the incoming depth value is greater than or equal to the
     * stored depth value */
    kGreaterOrEqual,
    /** Always passes */
    kAlways
  };

  /**
   * @enum PolyMode
   * @brief List of possible polygon mode
   */
  enum class PolyMode : std::uint8_t {
    /** Point */
    kPoint,
    /** Line */
    kLine,
    /** Fill */
    kFill
  };

  /**
   *  @enum BufferIdx
   *  @brief  Position of the attributes in the buffer array
   */
  enum BufferIdx {
    /** Triangulation buffer */
    kTriangle = 0,
    /** Position buffer */
    kVertex = 1,
    /** Normal buffer */
    kNormal = 2,
    /** Texture coordinate */
    kTexCoord = 3,
    /** Vertex Color */
    kVertexColor = 4
  };

  /**
   * @enum  PrimitiveType
   * @brief List of possible element's type to render
   */
  enum PrimitiveType {
    /** Triangle */
    kOglTriangle,
    /** Edge */
    kOglEdge
  };

  /**
   * @class BufferInfo
   * @brief Hold information about the size/data to push into OpenGL buffers
   * @author    Christophe Ecabert
   * @date      7/12/2017
   * @ingroup   ogl
   */
  class BufferInfo {
   public:
    /**
     * @name    BufferInfo
     * @fn  BufferInfo() = default
     * @brief   Constructor
     */
    BufferInfo();

    /**
     * @name    BufferInfo
     * @fn  BufferInfo() = default
     * @brief   Constructor
     */
    explicit BufferInfo(const Mesh& m);

    /**
     * @name    ~BufferInfo
     * @fn  ~BufferInfo() = default
     * @brief Destructor
     */
    ~BufferInfo() = default;

    /**
     * @name    operator<<
     * @fn  friend std::ostream& operator<<(std::ostream& os, const BufferInfo& info)
     * @brief   Dump object content into a given stream
     * @param[in] os    Stream in which to write
     * @param[in] info  Information to dump
     * @return ostream use to dump data in order to support chaining
     */
    friend std::ostream& operator<<(std::ostream& os, const BufferInfo& info) {
      os << "#Byte Vertex : " << info.dim_vertex_ << std::endl;
      os << "#Byte Normal : " << info.dim_normal_ << std::endl;
      os << "#Byte TCoord : " << info.dim_tex_coord_ << std::endl;
      os << "#Byte VColor : " << info.dim_color_ << std::endl;
      os << "#Element     : " << info.n_elements_ << std::endl;
      return os;
    }


    /** Vertex buffer dimension in bytes */
    size_t dim_vertex_;
    /** Vertex attribute dimensions, i.e. size of the vector 3/4 */
    size_t size_vertex_;
    /** Normal buffer dimension in bytes */
    size_t dim_normal_;
    /** Normal attribute dimensions, i.e. size of the vector */
    size_t size_normal_;
    /** Texture coordinate buffer dimension in bytes */
    size_t dim_tex_coord_;
    /** Texture coordinate attribute dimensions, i.e. size of the vector */
    size_t size_tex_coord_;
    /** Color buffer dimension in bytes */
    size_t dim_color_;
    /** Color attribute dimensions, i.e. size of the vector 3/4 */
    size_t size_color_;
    /** Total number of elements inside the index buffer (VBO) */
    size_t n_elements_;
    /** Vertex buffer data */
    const Vertex* data_vertex_;
    /** Normal buffer data */
    const Normal* data_normal_;
    /** Texture coordinate data */
    const TCoord* data_tex_coord_;
    /** Color buffer data */
    const Color* data_color_;
    /** Triangle buffer data */
    const int* data_elements_;
  };

  /**
   * @class     RenderingComponent
   * @brief     Basic holder for mesh rendering. It include the following
   *            elements: Mesh, Camera, Shading program and a texture (opt)
   * @date      23/01/2020
   * @author    Christophe Ecabert
   * @ingroup   ogl
   */
  class RenderingComponent {
   public:
#pragma mark -
#pragma mark Type Definitions
    /** Camera */
    using Camera_t = OGLCamera<T>;
    /** Shading program */
    using Shader = OGLProgram;
    /** Texture */
    using Tex_t = OGLTexture;

#pragma mark -
#pragma mark Initialization

    /**
     * @name    RenderingComponent
     * @fn  RenderingComponent(const int& width,
     *                         const int& height)
     * @brief Constructor
     * @param[in] width     Window width
     * @param[in] height    Window height
     */
    RenderingComponent(const int& width, const int& height);

    /**
     * @name    RenderingComponent
     * @fn  RenderingComponent(const std::string& path,
     *                         const int& width,
     *                         const int& height)
     * @brief Constructor
     * @param[in] path      Path to mesh file
     * @param[in] width     Window width
     * @param[in] height    Window height
     */
    RenderingComponent(const std::string& path,
                       const int& width,
                       const int& height);

    /**
     * @name    RenderingComponent
     * @fn      RenderingComponent(const Mesh& mesh, const Shader& shader,
                                    const Camera_t& cam)
     * @brief Constructor
     * @param[in] mesh      Mesh instance
     * @param[in] shader    Opengl shading program to use
     * @param[in] cam       Camera
     */
    RenderingComponent(const Mesh& mesh,
                       const Shader& shader,
                       const Camera_t& cam);

    /**
     * @name    Init
     * @fn  int Init(const std::string& path)
     * @brief Initialize internal elements for a given mesh
     * @param[in] path Path to the mesh object
     * @return  -1 if error, 0 otherwise
     */
    int Init(const std::string& path);

#pragma mark -
#pragma mark Accessors

    /**
     * @name    get_mesh
     * @fn  const Mesh& get_mesh() const
     * @brief   Access mesh object
     * @return  Const ref to the underlying mesh object
     */
    const Mesh& get_mesh() const {
      return mesh_;
    }

    /**
     * @name    get_shader
     * @fn  const Shader& get_shader() const
     * @brief   Access shading program
     * @return  Const ref to the shading program
     */
    const Shader& get_shader() const {
      return program_;
    }

    /**
     * @name    get_texture
     * @fn  const Tex_t& get_texture() const
     * @brief   Access texture
     * @return  Const ref to the texture
     */
    const Tex_t& get_texture() const {
      return tex_;
    }

#pragma mark -
#pragma mark Private
   private:
    /** Mesh instance */
    Mesh mesh_;
    /** Camera */
    Camera_t cam_;
    /** Shading program */
    Shader program_;
    /** Texture */
    Tex_t tex_;
  };

  /**
   * @name  OGLBaseRenderer
   * @fn  OGLBaseRenderer()
   * @brief Constructor
   */
  OGLBaseRenderer();

  /**
   * @name  OGLBaseRenderer
   * @fn  OGLBaseRenderer(const OGLBaseRenderer<T>& other) = delete
   * @brief Copy constructor, disable
   * @param[in] other  OGLBaseRenderer to copy from
   */
  OGLBaseRenderer(const OGLBaseRenderer<T>& other) = delete;

  /**
   * @name  operator=
   * @fn  OGLBaseRenderer& operator=(const OGLBaseRenderer<T>& rhs) = delete
   * @brief Assignment operator, disable
   * @param[in] rhs OGLBaseRenderer to assign from
   * @return  Newly assigned OGLBaseRenderer
   */
  OGLBaseRenderer& operator=(const OGLBaseRenderer<T>& rhs) = delete;

  /**
   * @name  ~OGLBaseRenderer
   * @fn  virtual ~OGLBaseRenderer()
   * @brief Destructor
   */
  virtual ~OGLBaseRenderer();

  /**
   *  @name BindToOpenGL
   *  @fn virtual int BindToOpenGL();
   *  @brief  Upload data into OpenGL context
   *  @return -1 if error, 0 otherwise
   */
  virtual int BindToOpenGL();

  /**
   *  @name BindToOpenGL
   *  @fn virtual int BindToOpenGL(const BufferInfo& info);
   *  @brief  Upload data into OpenGL context
   *  @param[in] info   Buffer information (size, data) to instantiate
   *  @return -1 if error, 0 otherwise
   */
  virtual int BindToOpenGL(const BufferInfo& info);

  /**
   *  @name BindToOpenGL
   *  @fn virtual int BindToOpenGL(const RenderingComponent& component);
   *  @brief  Upload data into OpenGL context
   *  @param[in] component   Rendering component holder
   *  @return -1 if error, 0 otherwise
   */
  virtual int BindToOpenGL(const RenderingComponent& component);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Enable
   * @fn    void Enable(const Capability& cap) const
   * @brief Enable an OpenGL capability
   * @param[in] cap OpenGL's capability to enable
   */
  void Enable(const Capability& cap) const;

  /**
   * @name  Disable
   * @fn    void Disable(const Capability& cap) const
   * @brief Disable an OpenGL capability
   * @param[in] cap OpenGL's capability to disable
   */
  void Disable(const Capability& cap) const;

  /**
   * @name  DepthFunction
   * @fn    void DepthFunction(const DepthFunc& func) const
   * @brief Define which criteria is used to performed depth test when enabled
   * @param[in] func Depth test criteria
   */
  void DepthFunction(const DepthFunc& func) const;

  /**
   * @name PolygonMode
   * @fn void PolygonMode(const PolyMode& mode, const T& factor,
                          const T& units) const
   * @brief Select a polygon rasterization mode and set the scale and units
   *        used to calculate depth values
   * @param[in] mode    Specifies how polygons will be rasterized
   * @param[in] factor  Specifies a scale factor that is used to create a
   *                    variable depth offset for each polygon.
   *                    DEFAULT value is 0.
   * @param[in] units   Is multiplied by an implementation-specific value to
   *                    create a constant depth offset. DEFAULT value is 0.
   */
  void PolygonMode(const PolyMode& mode,
                   const T& factor,
                   const T& units) const;

  /**
   * @name PolygonMode
   * @fn void PolygonMode(const PolyMode& mode) const
   * @brief Select a polygon rasterization mode
   * @param[in] mode    Specifies how polygons will be rasterized
   */
  void PolygonMode(const PolyMode& mode) const;

  /**
   * @name  PolygonOffset
   * @fn    void PolygonOffset(const T& factor, const T& units) const
   * @brief Set polygon offset
   * @param factor  Specifies a scale factor that is used to create a
   *                variable depth offset for each polygon.
   *                DEFAULT value is 0.
   * @param units   Is multiplied by an implementation-specific value to
   *                create a constant depth offset. DEFAULT value is 0.
   */
  void PolygonOffset(const T& factor, const T& units) const;

  /**
   * @name  Map
   * @fn    virtual void* Map(const BufferIdx& index) const
   * @brief Map an OpenGL resource to a host pointer
   * @param[in] index Index of the buffer to map
   * @return    Pointer to the mapped resource or nullptr if something went
   *            wrong
   */
  void* Map(const BufferIdx& index) const;

  /**
   * @name  Unmap
   * @fn    virtual int Unmap(const BufferIdx& index) const
   * @brief Unmap an OpenGL resource from a host pointer
   * @param[in] index Index of the buffer to unmap
   * @return    -1 if error, 0 otherwise
   */
  int Unmap(const BufferIdx& index) const;

  /**
   * @name  UpdateVertex
   * @fn  int UpdateVertex()
   * @brief Update vertex position into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  virtual int UpdateVertex();

  /**
   * @name  UpdateNormal
   * @fn  int UpdateNormal()
   * @brief Update normal into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  virtual int UpdateNormal();

  /**
   * @name  UpdateTCoord
   * @fn  int UpdateTCoord()
   * @brief Update texture coordinate into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  virtual int UpdateTCoord();

  /**
   * @name  UpdateColor
   * @fn  int UpdateColor()
   * @brief Update vertex color into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  virtual int UpdateColor();

  /**
   * @name  UpdateTriangle
   * @fn  int UpdateTriangle()
   * @brief Update triangle into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  virtual int UpdateTriangle();

  /**
   * @name  ClearBuffer
   * @fn  virtual void ClearBuffer()
   * @brief Clear OpenGL Buffer
   */
  void ClearBuffer();

  /**
   * @name  Render
   * @fn  virtual void Render()
   * @brief Render into OpenGL context
   */
  virtual void Render() {
    this->Render(PrimitiveType::kOglTriangle, n_elements_, 0);
  }

  /**
   * @name  Render
   * @fn  void Render(const PrimitiveType& type,
   *                  const size_t& count,
   *                  const size_t& elem_offset)
   * @brief Render part of the mesh into OpenGL context
   * @param[in] type    Type of primitive to be rendered
   * @param[in] count   Number of elements to be rendered
   * @param[in] elem_offset Where to start in the element buffer (VBO)
   */
  virtual void Render(const PrimitiveType& type,
                      const size_t& count,
                      const size_t& elem_offset);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  set_program
   * @fn    void set_program(OGLProgram* program)
   * @brief Change glsl program used for rendering
   * @param[in] program GLSL rendering program
   */
  void set_program(const OGLProgram* program) {
    program_ = program;
  }

  /**
   * @name  set_clear_color
   * @brief Define background color
   * @param[in] r   Value for red channel
   * @param[in] g   Value for green channel
   * @param[in] b   Value for blue channel
   * @param[in] a   Value for alpha channel
   */
  void set_clear_color(const T& r, const T& g, const T& b, const T& a) {
    clear_color_.r_ = r;
    clear_color_.g_ = g;
    clear_color_.b_ = b;
    clear_color_.a_ = a;
  }

  /**
   * @name  is_init
   * @fn    const bool& is_init() const
   * @brief Indicate if renderer is properly initialized
   * @return   True if OK, False otherwise
   */
  const bool& is_init() const {
    return is_init_;
  }

  /**
   * @name  get_ogl_buffer
   * @fn    unsigned int get_ogl_buffer(const BufferIdx& buffer_idx) const
   * @brief Provide reference to OpenGL buffer object
   * @param[in] buffer_idx Buffer index for which to query reference
   * @return    OpenGL Buffer reference or 0 if something went wrong
   */
  unsigned int get_ogl_buffer(const BufferIdx& buffer_idx) const {
    if (is_init_) {
      return vao_->get_ogl_buffer(buffer_idx);
    }
    return 0;
  }

#pragma mark -
#pragma mark Protected
 protected:

  /** Program attributes */
  using Attribute = LTS5::OGLProgram::Attributes;
  /** Program attribute's type */
  using AttributeType = LTS5::OGLProgram::AttributeType;

  /** Mesh */
  Mesh* mesh_;
  /** Program */
  const OGLProgram* program_;
  /** Texture */
  const OGLTexture* texture_;
  /** Vertex Array Object */
  OGLVertexArrayObject<T>* vao_;
  /** Number of elements inside the index buffer (VBO) */
  size_t n_elements_;
  /** Clear color */
  Vector4<T> clear_color_;
  /** OpenGL data initialized */
  bool is_init_;
};
}  // namepsace LTS5
#endif //__LTS5_BASE_RENDERER__
