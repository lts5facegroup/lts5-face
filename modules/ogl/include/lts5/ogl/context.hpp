/**
 *  @file   context.hpp
 *  @brief  Abstraction for OpenGL Context
 *  @ingroup    ogl
 *
 *  @author Christophe Ecabert
 *  @date   23/11/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <atomic>

#ifndef LTS5_CONTEXT_HPP
#define LTS5_CONTEXT_HPP

#ifdef HAS_GLFW_
#include "GLFW/glfw3.h"
typedef GLFWwindow Window;
#else
typedef void Window;
#endif

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLContext
 * @brief   Abstraction for OpenGL Context
 * @author  Christophe Ecabert
 * @date    23/11/2017
 * @ingroup ogl
 */
class LTS5_EXPORTS OGLContext {
 public:

#pragma mark -
#pragma mark Type Definition

  /** Errror call back */
  typedef void  (ErrorCallbackFcn)(int, const char*);

#pragma mark -
#pragma mark Initialization

  /**
   * @name  OGLContext
   * @fn    OGLContext()
   * @brief Constructor
   */
  OGLContext();

  /**
   * @name  OGLContext
   * @fn    OGLContext(const int& width, const int& height, const int& m_sample,
                const int& ogl_major, const int& ogl_minor, const bool& visible)
   * @brief Constructor
   * @param[in] width   Window width
   * @param[in] height  Window height
   * @param[in] m_sample Indicate if renderer uses multisampled texture
   *                     for antialiasing. 0 indicates no multisampling
   * @param[in] ogl_major   Major OpenGL version to use
   * @param[in] ogl_minor   Minor OpenGL version to use
   * @param[in] visible Indicate if the windows is displayed on the screen
   */
  OGLContext(const int& width,
             const int& height,
             const int& m_sample,
             const int& ogl_major,
             const int& ogl_minor,
             const bool& visible);

  /**
   * @name  OGLContext
   * @fn OGLContext(const OGLContext& other) = delete
   * @brief Copy Constructor
   * @param[in] other Object to copy from
   */
  OGLContext(const OGLContext& other) = delete;

  /**
   * @name  OGLContext
   * @fn OGLContext(OGLContext&& other) = delete
   * @brief Move Constructor
   * @param[in] other Object to move
   */
  OGLContext(OGLContext&& other) = delete;

  /**
   * @name  operator=
   * @fn    OGLContext& operator=(const OGLContext& rhs) = delete
   * @brief Copy assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  OGLContext& operator=(const OGLContext& rhs) = delete;

  /**
   * @name  operator=
   * @fn    OGLContext& operator=(OGLContext&& rhs) = delete
   * @brief Move assignment operator
   * @param[in] rhs Object to move-assign from
   * @return    Newly moved assign object
   */
  OGLContext& operator=(OGLContext&& rhs) = delete;

  /**
   * @name  ~OGLContext
   * @fn    ~OGLContext()
   * @brief Destructor
   */
  ~OGLContext();

  /**
   * @name  Initialize
   * @fn    int Initialize(const int& width,
                           const int& height,
                           const int& m_sample,
                           const int& ogl_major,
                           const int& ogl_minor,
                           const bool& visible)
   * @brief Initialize OpenGL window with a specific OpenGL version
   * @param[in] width   Window width
   * @param[in] height  Window height
   * @param[in] m_sample Indicate if renderer uses multisampled texture
   *                     for antialiasing. 0 indicates no multisampling
   * @param[in] ogl_major   Major OpenGL version to use
   * @param[in] ogl_minor   Minor OpenGL version to use
   * @param[in] visible Indicate if the windows is displayed on the screen
   * @return -1 if error, 0 otherwise
   */
  int Initialize(const int& width,
                 const int& height,
                 const int& m_sample,
                 const int& ogl_major,
                 const int& ogl_minor,
                 const bool& visible);

  /**
   * @name  Initialize
   * @fn    int Initialize(const int& width,
                           const int& height,
                           const int& m_sample,
                           const bool& visible)
   * @brief Initialize OpenGL window (OpenGL 3.3)
   * @param[in] width   Window width
   * @param[in] height  Window height
   * @param[in] m_sample Indicate if renderer uses multisampled texture
 *                       for antialiasing. 0 indicates no multisampling
   * @param[in] visible Indicate if the windows is displayed on the screen
   * @return -1 if error, 0 otherwise
   */
  int Initialize(const int& width,
                 const int& height,
                 const int& m_sample,
                 const bool& visible) {
    // Defaut OpenGL version 3.3
    return this->Initialize(width,
                            height,
                            m_sample,
                            3,
                            3,
                            visible);
  }

  /**
   * @name  Initialize
   * @fn    int Initialize(const int& width,
                           const int& height)
   * @brief Initialize OpenGL window with default parameters
   * @param[in] width   Window width
   * @param[in] height  Window height
   * @return -1 if error, 0 otherwise
   */
  int Initialize(const int& width,
                 const int& height) {
    return this->Initialize(width, height, 0, false);
  }

  /**
   * @name  Initialize
   * @fn    int Initialize(const int& width,
                           const int& height,
                           const bool& visible)
   * @brief Initialize OpenGL window (OpenGL 3.3)
   * @param[in] width   Window width
   * @param[in] height  Window height
   * @param[in] visible Indicate if the windows is displayed on the screen
   * @return -1 if error, 0 otherwise
   */
  int Initialize(const int& width,
                 const int& height,
                 const bool& visible) {
    return this->Initialize(width, height, 0, 3, 3, visible);
  }

  /**
   * @name  SetErrorCallbackFunction
   * @fn    int SetErrorCallbackFunction(ErrorCallbackFcn fcn)
   * @brief Define the error callback function to invoke when the backend fail.
   * @param[in] fcn Error callback function
   * @return -1 if error, 0 otherwise
   */
  static int SetErrorCallbackFunction(ErrorCallbackFcn fcn);

#pragma mark -
#pragma mark Usage

  /**
   * @name  ActiveContext
   * @fn    void ActiveContext() const
   * @brief Make this context the current one
   */
  void ActiveContext() const;

  /**
   * @name  DisableContext
   * @fn    void DisableContext() const
   * @brief Detach from current context
   */
  void DisableContext() const;

  /**
   * @name  SwapBuffer
   * @fn void SwapBuffer() const
   * @brief Update front buffer, by swaping with back
   */
  void SwapBuffer() const;

#pragma mark -
#pragma mark Private

 private:
  /** Instance counter */
  static std::atomic<int> counter_;
  /** Initialization flag */
  static std::atomic<bool> context_init_;
  /** Context */
  Window* win_;
};


}  // namepsace LTS5
#endif //LTS5_CONTEXT_HPP
