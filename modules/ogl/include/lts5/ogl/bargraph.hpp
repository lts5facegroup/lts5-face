/**
 *  @file   bargraph.hpp
 *  @brief  Draw bargraph into OpenGL context
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   24/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_BARGRAPH__
#define __LTS5_BARGRAPH__

#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/ogl/base_renderer.hpp"
#include "lts5/ogl/text_renderer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLBarGraph
 * @brief   Draw bargraph into OpenGL context
 * @author  Christophe Ecabert
 * @date    24/05/2016
 * @ingroup ogl
 */
class LTS5_EXPORTS OGLBarGraph : public OGLBaseRenderer<float> {
 public:

#pragma mark -
#pragma mark Type

  /**
   * @enum  BarOrientationType
   * @brief Define possible bar's orientation
   */
  enum BarOrientationType {
    /** Horizontal */
    kHorizontal,
    /** Vertical */
    kVertical
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name  OGLBarGraph
   * @fn  OGLBarGraph()
   * @brief Constructor
   */
  OGLBarGraph();

  /**
   * @name  OGLBarGraph
   * @fn  OGLBarGraph(const OGLBarGraph& other) = delete
   * @brief Copy constructor, disable
   * @param[in] other  OGLBarGraph to copy from
   */
  OGLBarGraph(const OGLBarGraph& other) = delete;

  /**
   * @name  operator=
   * @fn  OGLBarGraph& operator=(const OGLBarGraph& rhs) = delete
   * @brief Assignment operator, disable
   * @param[in] rhs OGLBarGraph to assign from
   * @return  Newly assigned OGLBarGraph
   */
  OGLBarGraph& operator=(const OGLBarGraph& rhs) = delete;

  /**
   * @name  ~OGLBarGraph
   * @fn  virtual ~OGLBarGraph()
   * @brief Destructor
   */
  ~OGLBarGraph();

  /**
   * @name  SetupView
   * @fn  void SetupView(const int x,
                         const int y,
                         const int width,
                         const int height,
                         const int n_bar);
   * @brief Setup bargraph view dimension and number of bin
   * @param[in] x       View X position
   * @param[in] y       View Y position
   * @param[in] width   View width
   * @param[in] height  View height
   * @param[in] n_bar   Number of bar
   */
  void SetupView(const int x,
                 const int y,
                 const int width,
                 const int height,
                 const int n_bar);

  /**
   *  @name BindToOpenGL
   *  @fn virtual int BindToOpenGL() override
   *  @brief  Upload data into OpenGL context
   *  @return -1 if error, 0 otherwise
   */
  int BindToOpenGL() override;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Render
   * @fn  void Render() override
   * @brief Render into OpenGL context
   */
  void Render() override;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  SetWindowDimension
   * @fn  void SetWindowDimension(const int width, const int height)
   * @brief Set dimension of the window to render in
   * @param[in] width   Window width
   * @param[in] height  Window height
   */
  void SetWindowDimension(const int width, const int height);

  /**
   * @name  SetFont
   * @fn  int SetFont(const std::string& font)
   * @brief Set label's font type
   * @param[in] font    Path to font file (i.e. arial.ttf), if empty,
   *                    load default Arial font
   * @return -1 if error, 0 otherwise
   */
  int SetFont(const std::string& font);

  /**
   * @name  SetLabel
   * @fn  void SetLabel(const std::vector<std::string>& labels)
   * @brief Set \p labels for all bars
   * @param[in] labels   List of labels
   */
  void SetLabel(const std::vector<std::string>& labels);

  /**
   * @name  SetBarOrientation
   * @fn  void SetBarOrientation(const BarOrientationType orientation)
   * @brief Set bar's orientation
   * @param[in] orientation   Bar's orientation
   */
  void SetBarOrientation(const BarOrientationType orientation);

  /**
   * @name  SetColor
   * @fn  SetColor(const Color& color)
   * @brief Set bar's color. Range should lie in [0,1]
   * @param[in] color   Bar's color
   */
  void SetColor(const Color& color);

  /**
   * @name  SetValue
   * @fn  void SetValue(const std::vector<float>& values)
   * @brief Update bar's \p values
   * @param[in] values  Bar's value (percentage)
   */
  void SetValue(const std::vector<float>& values);

#pragma mark -
#pragma mark Private

private:

  /**
   * @name  ComputeBarProperty
   * @fn    void ComputeBarProperty()
   * @brief Compute bar properties
   */
  void ComputeBarProperty();

  /**
   * @name  UpdateMeshVertex
   * @fn  void UpdateMeshVertex()
   * @brief Update vertex position in the mesh
   */
  void UpdateMeshVertex();

  /**
   * @name  UpdateText
   * @fn  void UpdateText()
   * @brief Update text into renderer
   */
  void UpdateText();

  /**
   * @struct  OGLBar
   * @brief   Forward declaration
   */
  struct OGLBar;

  /** X position */
  float x_;
  /** Y position */
  float y_;
  /** View Wdith */
  float width_;
  /** View Height */
  float height_;
  /** Delta bar */
  float delta_bar_;
  /** Bars padding bottom/left */
  float bar_pad_;
  /** Label maximum width */
  float label_m_width_;
  /** Label maximum heigtht */
  float label_m_height_;
  /** Bars */
  std::vector<OGLBar> bars_;
  /** Recompute anchor flag */
  bool recompute_anchor_;
  /** Orientation */
  BarOrientationType orientation_;
  /** Text renderer */
  LTS5::OGLTextRenderer* text_renderer_;
};
}  // namepsace LTS5
#endif //__BARGRAPH__
