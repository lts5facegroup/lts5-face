/**
 *  @file   image_renderer.hpp
 *  @brief  2D Renderer for image drawing
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   19/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_IMAGE_RENDERER__
#define __LTS5_IMAGE_RENDERER__

#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/ogl/base_renderer.hpp"
#include "lts5/ogl/shader.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLImageRenderer
 * @brief 2D Renderer for image drawing
 * @author  Christophe Ecabert
 * @date    19/05/2016
 * @ingroup ogl
 */
class LTS5_EXPORTS OGLImageRenderer : public OGLBaseRenderer<float> {

 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  OGLImageRenderer
   * @fn  OGLImageRenderer()
   * @brief Constructor
   */
  OGLImageRenderer();

  /**
   * @name  OGLImageRenderer
   * @fn  OGLImageRenderer(const OGLImageRenderer& other) = delete
   * @brief Copy constructor, disable
   * @param[in] other  OGLImageRenderer to copy from
   */
  OGLImageRenderer(const OGLImageRenderer& other) = delete;

  /**
   * @name  operator=
   * @fn  OGLImageRenderer& operator=(const OGLImageRenderer& rhs) = delete
   * @brief Assignment operator, disable
   * @param[in] rhs OGLImageRenderer to assign from
   * @return  Newly assigned OGLImageRenderer
   */
  OGLImageRenderer& operator=(const OGLImageRenderer& rhs) = delete;

  /**
   * @name  ~OGLImageRenderer
   * @fn  ~OGLImageRenderer() override
   * @brief Destructor
   */
  ~OGLImageRenderer() override;

  /**
   * @name  AddTexture
   * @fn  void AddTexture(const OGLTexture* texture)
   * @brief Add OpenGL texture (used to "paint" geometry)
   * @param[in] texture
   */
  void AddTexture(const OGLTexture* texture);

  /**
   * @name  SetWindowDimension
   * @fn  void SetWindowDimension(const int& width, const int& height)
   * @brief Set the dimension of the window to render inside
   * @param[in] width   Window width
   * @param[in] height  Window height
   */
  void SetWindowDimension(const int& width, const int& height);

  /**
   * @name  SetupView
   * @fn  void SetupView(const int& x,
                 const int& y,
                 const int& width,
                 const int& height)
   * @brief Setup bargraph view dimension and number of bin
   * @param[in] x       View X position
   * @param[in] y       View Y position
   * @param[in] width   View width
   * @param[in] height  View height
   */
  void SetupView(const int& x,
                 const int& y,
                 const int& width,
                 const int& height);

#pragma mark -
#pragma mark Usage

  /**
   * @name  UpdateVertex
   * @fn  int UpdateVertex() override
   * @brief Update vertex position into OpenGL context
   * @return  -1 if error, 0 otherwise
 */
  int UpdateVertex() override;

  /**
   * @name  UpdateTCoord
   * @fn  int UpdateTCoord() override
   * @brief Update texture coordinate into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  int UpdateTCoord() override;

private:

  /** Window width */
  float window_width_;
  /** Window height */
  float window_height_;
};
}  // namepsace LTS5
#endif //__LTS5_IMAGE_RENDERER__
