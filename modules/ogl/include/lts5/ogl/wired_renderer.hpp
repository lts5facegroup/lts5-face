/**
 *  @file   wired_renderer.hpp
 *  @brief  Helper class to draw wired box in OpenGL context
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   28/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __WIRED_BOX_RENDERER__
#define __WIRED_BOX_RENDERER__

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/ogl/base_renderer.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/geometry/aabb.hpp"
#include "lts5/geometry/aabb_tree.hpp"
#include "lts5/geometry/octree.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLWiredRenderer
 * @brief   Helper class to draw wired box in OpenGL context
 * @author  Christophe Ecabert
 * @date    28/04/16
 * @ingroup ogl
 */
template<typename T>
class LTS5_EXPORTS OGLWiredRenderer : public LTS5::OGLBaseRenderer<T> {
 public:
  /** Matrix 4x4 */
  using Mat4 = Matrix4x4<T>;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  OGLWiredRenderer
   * @fn    OGLWiredRenderer()
   * @brief Constructor
   */
  OGLWiredRenderer();

  /**
   * @name  OGLWiredRenderer
   * @fn  OGLWiredRenderer(const OGLWiredRenderer<T>& other) = delete
   * @brief Copy constructor, disable
   * @param[in] other  OGLWiredRenderer to copy from
   */
  OGLWiredRenderer(const OGLWiredRenderer<T>& other) = delete;

  /**
   * @name  operator=
   * @fn  OGLWiredRenderer& operator=(const OGLWiredRenderer<T>& rhs) = delete
   * @brief Assignment operator, disable
   * @param[in] rhs OGLWiredRenderer to assign from
   * @return  Newly assigned OGLWiredRenderer
   */
  OGLWiredRenderer& operator=(const OGLWiredRenderer<T>& rhs) = delete;

  /**
   * @name  ~OGLWiredRenderer
   * @fn    ~OGLWiredRenderer()
   * @brief Destructor
   */
  ~OGLWiredRenderer() override;

  /**
   *  @name BindToOpenGL
   *  @fn int BindToOpenGL();
   *  @brief  Upload data into OpenGL context
   *  @return -1 if error, 0 otherwise
   */
  int BindToOpenGL() override;

#pragma mark -
#pragma mark Usage

  /**
   * @name  UpdateVertex
   * @fn  int UpdateVertex() override
   * @brief Update vertex position into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  int UpdateVertex() override;

  /**
   * @name  UpdateNormal
   * @fn  int UpdateNormal() override
   * @brief Update normal into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  int UpdateNormal() override;

  /**
   * @name  UpdateNormal
   * @fn  int UpdateNormal()
   * @brief Update normal into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  int UpdateTCoord() override;

  /**
   * @name  UpdateColor
   * @fn  int UpdateColor() override
   * @brief Update vertex color into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  int UpdateColor() override;

  /**
   * @name    AddBoundingBox
   * @brief   Add new bbox into data buffer
   * @param[in] bbox  Bounding boxes
   */
  void AddBoundingBox(const AABB<T>& bbox);

  /**
   * @name  Render
   * @fn  void Render() override
   * @brief Render into OpenGL context
   */
  void Render() override;

  /**
   * @name  Render
   * @fn  void Render(const OGLCamera& cam, const Mat4& model)
   * @brief Render wired boxes
   * @param[in] cam     Camera transformation (view + projection)
   * @param[in] model   Object transformation (rigid)
   */
  void Render(const OGLCamera<T>& cam, const Mat4& model);

  /**
   * @name  FillFromAABBTree
   * @fn  static void FillFromAABBTree(const AABBTree<T>& tree,
                               const int max_level,
                               OGLWiredRenderer<T>* renderer)
   * @brief Helper function to populate wired renderer
   * @param[in]     tree      Tree where to get cells
   * @param[in]     max_level How far to dive into the tree
   * @param[in,out] renderer  Wired renderer to populate
   */
  static void FillFromAABBTree(const AABBTree<T>& tree,
                               const int max_level,
                               OGLWiredRenderer<T>* renderer);

  /**
   * @name  FillFromOCTree
   * @fn  static void FillFromOCTree(const OCTree<T>& tree,
                                     OGLWiredRenderer<T>* renderer)
   * @brief Helper function to populate wired renderer
   * @param[in]     tree      Tree where to get cells
   * @param[in,out] renderer  Wired renderer to populate
   */
  static void FillFromOCTree(const OCTree<T>& tree,
                             OGLWiredRenderer<T>* renderer);

#pragma mark -
#pragma mark Private
 private:

  /** Line type */
  using Line = LTS5::Vector2<int>;
  /** Vertex Type */
  using Vertex = typename LTS5::OGLBaseRenderer<T>::Vertex;

  /** Bounding box vertex array */
  std::vector<Vertex> vertex_;
  /** Wiring */
  std::vector<Line> tri_;

};
}  // namepsace LTS5

#endif //__WIRED_BOX_RENDERER__
