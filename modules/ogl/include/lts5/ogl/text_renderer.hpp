/**
 *  @file   text_renderer.hpp
 *  @brief  Text renderer for OpenGL context
 *          Based on : http://learnopengl.com/#!In-Practice/Text-Rendering
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   20/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __TEXT_RENDERER__
#define __TEXT_RENDERER__

#include <unordered_map>

#include "lts5/utils/library_export.hpp"
#include "lts5/ogl/base_renderer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLTextRenderer
 * @brief   OpenGL rendering class for text
 * @author  Christophe Ecabert
 * @date    20/05/2016
 * @ingroup ogl
 */
class LTS5_EXPORTS OGLTextRenderer : public LTS5::OGLBaseRenderer<float> {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  OGLTextRenderer
   * @fn  OGLTextRenderer()
   * @brief Constructor
   */
  OGLTextRenderer();

  /**
   * @name  OGLTextRenderer
   * @fn  OGLTextRenderer(const OGLTextRenderer& other) = delete
   * @brief Copy constructor, disable
   * @param[in] other  OGLTextRenderer to copy from
   */
  OGLTextRenderer(const OGLTextRenderer& other) = delete;

  /**
   * @name  operator=
   * @fn  OGLTextRenderer& operator=(const OGLTextRenderer& rhs) = delete
   * @brief Assignment operator, disable
   * @param[in] rhs OGLTextRenderer to assign from
   * @return  Newly assigned OGLTextRenderer
   */
  OGLTextRenderer& operator=(const OGLTextRenderer& rhs) = delete;

  /**
   * @name  ~OGLTextRenderer
   * @fn  virtual ~OGLTextRenderer() override
   * @brief Destructor
   */
  ~OGLTextRenderer() override;

  /**
   * @name  GenerateAtlas
   * @fn  int GenerateAtlas(const std::string& font_path)
   * @brief Generate altas texture for corresponding font
   * @param[in] font_path Path to font file (.ttf). If empty, load default
   *                      Arial font
   * @return -1 if error, 0 otherwise
   */
  int GenerateAtlas(const std::string& font_path);

  /**
   * @name  SetWindowDimension
   * @fn  void SetWindowDimension(const int width, const int height)
   * @brief Set dimension of the window to render in
   * @param[in] width   Window width
   * @param[in] height  Window height
   */
  void SetWindowDimension(const int& width, const int& height);

#pragma mark -
#pragma mark Usage

  /**
   * @name  AddText
   * @fn  int AddText(const std::string& text,
                      const float x,
                      const float y,
                      const float scale,
                      const Color color,
                      const int location);
   * @brief Add text to the current render for a given position, scale and color
   * @param[in] text      Text to display
   * @param[in] x         On screen X position
   * @param[in] y         On screen Y position
   * @param[in] scale     Font scale
   * @param[in] color     Font color
   * @param[in] location  Location where to start to add text.
   *                      Append at the end = -1, otherwise specify position
   *                      in term of char
   * @return -1 if error, 0 otherwise
   */
  int AddText(const std::string& text,
              const float& x,
              const float& y,
              const float& scale,
              const Color& color,
              const int& location);

  /**
   * @name
   * @fn  void QueryTextDimension(const std::string& text,
                                 const float scale,
                                 float* width,
                                 float* height)
   * @brief Compute on screen \p text size
   * @param[in]   text    Text to estimate dimension
   * @param[in]   scale   Scaling factor (i.e. Font size)
   * @param[out]  width   Total \p text width
   * @param[out]  height  Total \p text height
   * @return -1 if error, 0 otherwise
   */
  int QueryTextDimension(const std::string& text,
                         const float& scale,
                         float* width,
                         float* height);

  /**
   * @name  UpdateVertex
   * @fn  int UpdateVertex() override
   * @brief Update vertex position into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  int UpdateVertex() override;

  /**
   * @name  UpdateTCoord
   * @fn  int UpdateTCoord() override
   * @brief Update texture coordinate into OpenGL context
   * @return  -1 if error, 0 otherwise
   */
  int UpdateTCoord() override;

  /**
   * @name  Render
   * @fn  void Render() override
   * @brief Render into OpenGL context
   */
  void Render() override;

#pragma mark -
#pragma mark Private

private:

  using Vec2i = Vector2<int>;

  /**
   * @struct  OGLCharacter
   * @brief OpenGL equivalent for 'char'
   */
  struct OGLCharacter {
    /** Size of glyph */
    Vec2i size;
    /** Bearing - Offset from baseline to left/top of glyph */
    Vec2i bearing;
    /** Advance - Offset to advance to next glyph */
    Vec2i advance;
    /** Texture coordinate */
    TCoord tcoord[4];
  };

  /**
   * @name  FindNextPowerOfTwo
   * @fn  void FindNextPowerOfTwo(const int x, int* pow2)
   * @brief Round up to the next highest power of 2
   * @param[in]   x     X value
   * @param[out]  pow2  Next power of 2 for a given value
   */
  void FindNextPowerOfTwo(const int& x, int* pow2);

  /** Alphabet dictionary */
  std::unordered_map<char, OGLCharacter> dictionary_;
  /** Texture ID */
  unsigned int tex_;
};

}  // namepsace LTS5
#endif //__TEXT_RENDERER__
