/**
 *  @file   glfw_backend.hpp
 *  @brief  Backend helper for GLFW.
 *          Based on : http://ogldev.atspace.co.uk/
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   10/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_GLFW_BACKEND__
#define __LTS5_GLFW_BACKEND__

#include <memory>
#ifdef HAS_GLFW_
#include "GLFW/glfw3.h"
#endif

#include "lts5/utils/library_export.hpp"
#include "lts5/ogl/ogl_backend.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLGlfwBackend
 * @brief   GLFW Backend
 * @author  Christophe
 * @date    10/02/2016
 * @ingroup ogl
 */
class LTS5_EXPORTS OGLGlfwBackend : public OGLBackend {

public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  OGLGlfwBackend
   * @fn  OGLGlfwBackend()
   * @brief Constructor
   */
  OGLGlfwBackend();

  /**
   * @name  OGLGlfwBackend
   * @fn  OGLGlfwBackend(const OGLGlfwBackend& other) = delete
   * @brief Copy Constructor
   * @param[in] other   Backend to copy
   */
  OGLGlfwBackend(const OGLGlfwBackend& other) = delete;

  /**
   *  @name operator=
   *  @fn OGLGlfwBackend& operator=(const OGLGlfwBackend& rhs) = delete
   *  @brief  Assignment operator
   *  @param[in]  rhs   Backend to assign from
   */
  OGLGlfwBackend& operator=(const OGLGlfwBackend& rhs) = delete;

  /**
   * @name  ~OGLGlfwBackend
   * @fn  ~OGLGlfwBackend() override
   * @brief Destructor
   */
  ~OGLGlfwBackend() override;

  /**
   * @name  Init
   * @fn  void Init(int argc, const char** argv,
   *                bool with_depth,
   *                bool with_stencil,
   *                bool with_back_culling) override
   * @brief Initialize GLFW Backend
   * @param[in] argc          Number of parameters in Command line
   * @param[in] argv          Command line parameters
   * @param[in] with_depth    Use depth
   * @param[in] with_stencil  Use stencil
   * @param[in] with_back_culling   Indicate if triangle facing back will be
   *                                removed
   */
  void Init(int argc,
            const char **argv,
            bool with_depth,
            bool with_stencil,
            bool with_back_culling) override;

  /**
   * @name  Terminate
   * @fn  void Terminate() override
   * @brief Shutdown GLFW backend
   */
  void Terminate() override;

  /**
   * @name  CreateWindows
   * @fn  bool CreateWindows(const int width, const int height,
   *                         const bool fullscreen,
   *                         const char* window_title) override
   * @brief Initialiaze GLFW window
   * @param[in] width         Window's width
   * @param[in] height        Window's height
   * @param[in] fullscreen    True for fullscreen window
   * @param[in] window_title  Window's title
   */
  bool CreateWindows(const int width,
                     const int height,
                     const bool fullscreen,
                     const char *window_title) override;

  /**
   * @name  EnableBlending
   * @fn  void EnableBlending()
   * @brief Enable OpenGL blending
   */
  void EnableBlending();

#pragma mark -
#pragma mark Usage

  /**
   * @name  Run
   * @fn  void Run(OGLCallback* ogl_callback) override
   * @brief Enter into GLFW main loop
   * @param[in] ogl_callback  Pointer to object holding opengl callback
   *                          implementation
   */
  void Run(OGLCallback *ogl_callback) override;

  /**
   * @name  SwapBuffer
   * @fn  void SwapBuffer() override
   * @brief Swap opengl buffer in order to display what has been rendered
   */
  void SwapBuffer() override;

  /**
   * @name  LeaveMainLoop
   * @fn  void LeaveMainLoop() override
   * @brief Leave backend mainloop
   */
  void LeaveMainLoop() override;

  /**
   * @name  PollEvents
   * @fn  void PollEvents()
   * @brief Poll event from OpenGL context
   */
  void PollEvents();

  /**
   * @name  SetMousePos
   * @fn  void SetMousePos(const int x, const int y) override
   * @brief Set mouse position inside window
   * @param[in] x   Cursor X position
   * @param[in] y   Cursor Y position
   */
  void SetMousePos(const int x, const int y) override;

  /**
   * @name  ClearBuffer
   * @fn  void ClearBuffer()
   * @brief Clear OpenGL buffer
   */
  void ClearBuffer();

#pragma mark -
#pragma mark Setter

  /**
   * @name  SetCallbacks
   * @fn  SetCallbacks(const OGLCallback* callback)
   * @brief Setup callbacks
   * @param[in] callback Callback to invoke when event happens
   */
  void SetCallbacks(const OGLCallback* callback);

#pragma mark -
#pragma mark Private

 private:

  /** Pointer to the object implementing OGLCallback interface. All event are
   * forwarded to this object */
  static OGLCallback* callback_;
  /** Indicate if we're using depth */
  bool with_depth_;
  /** Indicate if we're using stencil */
  bool with_stencil_;
#ifdef HAS_GLFW_
  /** GLFW window instance */
  GLFWwindow* windows_;
#endif


  /**
   * @name  GlfwKeyToOGLKey
   * @fn  static OGLKey GlfwKeyToOGLKey(unsigned int glfw_key)
   * @brief Convert GLFW key into OGLKey format
   * @param[in] glfw_key Key to convert
   * @return  OGLKey equivalent if exist otherwise kUndefined
   */
  static OGLKey GlfwKeyToOGLKey(unsigned int glfw_key);

  /**
   * @name  GlfwMouseToOGLMouse
   * @fn  static OGLMouse GlfwMouseToOGLMouse(unsigned int button)
   * @brief Convert GLFW button into OGLMouse format
   * @param[in] button GLFW button to convert
   * @return  OGLMouse equivalent if exist otherwise kUndefined
   */
  static OGLMouse GlfwMouseToOGLMouse(unsigned int button);

  /**
   * @name  GLFWErrorCallback
   * @fn  static void GLFWErrorCallback(int error, const char* description)
   * @brief GLFW error callbacks
   * @param[in] error         Error number
   * @param[in] description   Error message
   */
  static void GLFWErrorCallback(int error, const char* description);

#ifdef HAS_GLFW_
  /**
   * @name  KeyCallback
   * @fn  static void KeyCallback(GLFWwindow* window,int key,int scancode,
                           int action, int mods)
   * @brief Wrapper for GLFW callback and OGLCallback
   * @param[in] window    GLFW window that invoke callback
   * @param[in] key       Key that trigger callback
   * @param[in] scancode  GLFW scan code
   * @param[in] action    Key action
   * @param[in] mods      GLFW mod
   */
  static void KeyCallback(GLFWwindow* window,
                          int key,
                          int scancode,
                          int action,
                          int mods);
#endif

#ifdef HAS_GLFW_
  /**
   * @name  CursorPosCallback
   * @fn  static void CursorPosCallback(GLFWwindow* window, double x, double y)
   * @brief Callback for handling mouse displacement inside GLFW window
   * @param[in] window  GLFW window that invoke the callback
   * @param[in] x Cursor X position inside window
   * @param[in] y Cursor Y position inside window
   */
  static void CursorPosCallback(GLFWwindow* window, double x, double y);
#endif

#ifdef HAS_GLFW_
  /**
   * @name  MouseButtonCallback
   * @fn  static void MouseButtonCallback(GLFWwindow* window,
                                  int button,
                                  int action,
                                  int mode)
   * @brief Callback for handling mouse click event inside GLFW window
   * @param[in] window  GLFW window that invoke the callback
   * @param[in] button  Which mouse button trigger the callback
   * @param[in] action  Button action
   * @param[in] mode    GLFW Mode
   */
  static void MouseButtonCallback(GLFWwindow* window,
                                  int button,
                                  int action,
                                  int mode);
#endif

  /**
   * @name  InitCallbacks
   * @fn  static void InitCallbacks()
   * @brief Initialize GLFW callbacks
   */
  void InitCallbacks();

};
}  // namespce LTS5
#endif //__LTS5_GLFW_BACKEND__
