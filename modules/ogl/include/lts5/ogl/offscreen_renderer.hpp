/**
 *  @file   offscreen_renderer.hpp
 *  @brief  OpenGL offscreen renderer
 *          For debug purpose, one can define DEBUG_RENDERER variable to display
 *          on screen what is actually rendered by OpenGL pipeline
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   14/07/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_OFFSCREEN_RENDERER__
#define __LTS5_OFFSCREEN_RENDERER__


#include "lts5/utils/library_export.hpp"
#include "lts5/ogl/base_renderer.hpp"
#include "lts5/ogl/io_buffer.hpp"
#include "lts5/ogl/context.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLOffscreenRenderer
 * @brief   OpenGL offscreen renderer
 * @author  Christophe Ecabert
 * @date    14/07/2016
 * @ingroup ogl
 * @tparam T Data Type
 * @tparam ColorContainer Container type in which vertex color is stored
 */
template<typename T, typename ColorContainer = Vector4<T>>
class LTS5_EXPORTS OGLOffscreenRenderer : public OGLBaseRenderer<T, ColorContainer> {
 public:
#pragma mark -
#pragma mark Typedef

  /** Mesh Type */
  using Mesh = LTS5::Mesh<T, ColorContainer>;
  /** BufferInfo type */
  using BufferInfo = typename OGLBaseRenderer<T, ColorContainer>::BufferInfo;
  /** Primitive */
  using PrimitiveType = typename OGLBaseRenderer<T, ColorContainer>::PrimitiveType;

  /**
   *  @enum  ImageType
   *  @brief  Internal buffer type
   */
  enum ImageType {
    /** Grayscale image */
    kGrayscale,
    /** Color image (3 channels) */
    kRGB,
    /** Color image (3 float channels) */
    kRGBf,
    /** Color image (4 channels) */
    kRGBA,
    /** Color image (4 float channels) */
    kRGBAf
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name  OGLOffscreenRenderer
   * @fn    OGLOffscreenRenderer()
   * @brief Constructor
   */
  OGLOffscreenRenderer();

  /**
   * @name  OGLOffscreenRenderer
   * @fn    OGLOffscreenRenderer(const int& width,
                                 const int& height,
                                 const int& m_sample,
                                 const bool& visible,
                                 const ImageType& type)
   * @brief Constructor
   * @param[in] width   View's width
   * @param[in] height  View's height
   * @param[in] m_sample    Number of multisampling for antialiasing. A value
   *                        of `0` indicates without antialiasing
   * @param[in] visible Flag to indicate if window is displayed on the screen
   * @param[in] type    Type of the the image
   */
  OGLOffscreenRenderer(const int& width,
                       const int& height,
                       const int& m_sample,
                       const bool& visible,
                       const ImageType& type);

  /**
   * @name  OGLOffscreenRenderer
   * @fn  OGLOffscreenRenderer(const OGLOffscreenRenderer<T, ColorContainer>& other)
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  OGLOffscreenRenderer(const OGLOffscreenRenderer<T, ColorContainer>& other) = delete;


  /**
   * @name  operator=
   * @fn  OGLOffscreenRenderer& operator=(const OGLOffscreenRenderer<T, ColorContainer>& rhs)
   * @brief Assignment operator
   * @param[in] rhs   Object to assign from
   * @return  Newly assigned object
   */
  OGLOffscreenRenderer& operator=(const OGLOffscreenRenderer<T, ColorContainer>& rhs) = delete;

  /**
   * @name  ~OGLOffscreenRenderer
   * @fn  ~OGLOffscreenRenderer()
   * @brief Destructor
   */
  ~OGLOffscreenRenderer();

  /**
   * @name  Init
   * @fn  int Init(const int& width,
                   const int& height,
                   const int& m_sample,
                   const bool& visible,
                   const ImageType& type)
   * @brief Initialize offscreen renderer (FBO + Buffer)
   * @param[in] width       Width of the image
   * @param[in] height      Heightof the image
   * @param[in] m_sample    Number of multisampling for antialiasing. A value
   *                        of `0` indicates without antialiasing
   * @param[in] visible     Flag to indicate if window is displayed on the screen
   * @param[in] type        Type of the the image
   * @return  -1 if error, 0 otherwise
   */
  int Init(const int& width,
           const int& height,
           const int& m_sample,
           const bool& visible,
           const ImageType& type);

#pragma mark -
#pragma mark Usage

  /**
   *  @name ActiveContext
   *  @fn void ActiveContext() const
   *  @brief  Make internal OpenGL context active
   */
  void ActiveContext() const;

  /**
   *  @name DisableContext
   *  @fn void DisableContext() const
   *  @brief  Detach current OpenGL context
   */
  void DisableContext() const;

  /**
   * @name  Render
   * @fn  virtual void Render()
   * @brief Render into OpenGL context
   */
  void Render() override {
    this->Render(PrimitiveType::kOglTriangle, this->n_elements_, 0);
  }

  /**
   * @name  Render
   * @fn  void Render(const PrimitiveType& type,
   *                  const size_t& count,
   *                  const size_t& elem_offset)
   * @brief Render part of the mesh into OpenGL context
   * @param[in] type    Type of primitive to be rendered
   * @param[in] count   Number of elements to be rendered
   * @param[in] elem_offset Where to start in the element buffer (VBO)
   */
  void Render(const PrimitiveType& type,
              const size_t& count,
              const size_t& elem_offset) override;

  /**
   * @name  AddMesh
   * @fn  void AddMesh(const Mesh& mesh)
   * @brief Add geometry to render
   * @param[in] mesh  Mesh to render
   */
  void AddMesh(const Mesh& mesh);

  /**
   * @name  AddProgram
   * @fn  void AddProgram(const OGLProgram& program)
   * @brief Add OpenGL program to tell how to draw a given mesh
   * @param[in] program Program to use for rendering
   */
  void AddProgram(const OGLProgram& program);

  /**
   * @name  AddTexture
   * @fn  void AddTexture(const OGLTexture& texture)
   * @brief Add OpenGL texture (used to "paint" geometry)
   * @param[in] texture Texture tu use while rendering
   */
  void AddTexture(const OGLTexture& texture);

  /**
   * @name  GetImage
   * @fn  int GetImage(cv::Mat* image) const
   * @brief Write the last rendered image into \p image
   * @param[out]  image Last rendered image
   * @return -1 if error, 0 otherwise
   */
  int GetImage(cv::Mat* image) const;

  /**
   * @name  GetImage
   * @fn  int GetImage(void* buffer) const
   * @brief Write the last rendered image into \p image
   * @param[out]  buffer Location where to write the image. The buffer needs to
   *                     be allocated with the proper size
   * @return -1 if error, 0 otherwise
   */
  int GetImage(void* buffer) const;

  /**
   * @name  GetDepth
   * @fn  int GetDepth(cv::Mat* depth_map) const
   * @brief Write the last rendered depth buffer into \p depth_map
   * @param[out]  depth_map Depth map
   * @return -1 if error, 0 otherwise
   */
  int GetDepth(cv::Mat* depth_map) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  need_to_clear
   * @fn    void need_to_clear(const bool& v)
   * @brief Indicate if the underlying buffer needs to be cleared during
   *        rendering pass
   * @param[in] v Flag to indicating if clear is needed.
   */
  void need_to_clear(const bool& v) {
    clear_needed_ = v;
  }

  /**
   * @name  get_buffer
   * @fn    const OGLIOBuffer* get_buffer() const
   * @brief Provide access to underling offscreen OpenGL buffer
   * @return    offscreen buffer
   */
  const OGLIOBuffer* get_buffer() const {
    return buffer_;
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  GLFWErrorCallback
   * @fn  static void GLFWErrorCallback(int error, const char* description)
   * @brief GLFW error callbacks
   * @param[in] error         Error number
   * @param[in] description   Error message
   */
  static void GLFWErrorCallback(int error, const char* description);

  /** OpenGL Context */
  OGLContext* ctx_;
  /** Window width */
  int width_;
  /** Window height */
  int height_;
  /** Frame buffer object */
  OGLIOBuffer* buffer_;
  /** Clear buffer indicator */
  bool clear_needed_;
  /** Window visibility */
  bool visible_;
};

}  // namepsace LTS5

#endif //__LTS5_OFFSCREEN_RENDERER__
