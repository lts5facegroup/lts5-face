/**
 *  @file   sh_viewer.hpp
 *  @brief  Utility tool for rendering estimated spherical harmonics on a ball
 *          for visualisation / debugging
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   9/28/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_SH_VIEWER__
#define __LTS5_SH_VIEWER__

#include "lts5/utils/library_export.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/ogl/offscreen_renderer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


/**
 * @class   SHViewer
 * @brief   Utility tool for rendering estimated spherical harmonics on a ball
 * @author  Christophe Ecabert
 * @date    28.09.17
 * @ingroup ogl
 * @tparam T    Data type
 */
template<typename T>
class SHViewer {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  SHViewer
   * @fn    SHViewer(const int& width, const int& height, const int& step)
   * @brief Constructor
   * @param[in] width   Image width
   * @param[in] height  Image heigh
   * @param[in] step    Number of section on the ball
   */
  SHViewer(const int& width, const int& height, const int& step);

  /**
   * @name  SHViewer
   * @fn    SHViewer(const SHViewer& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  SHViewer(const SHViewer& other) = delete;

  /**
   * @name  operator=
   * @fn    SHViewer& operator=(const SHViewer& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  SHViewer& operator=(const SHViewer& rhs) = delete;

  /**
   * @name  ~SHViewer
   * @fn    ~SHViewer()
   * @brief Destructor
   */
  ~SHViewer();

#pragma mark -
#pragma mark Usage

  /**
   * @name Generate
   * @fn    int Generate(const cv::Mat& aldebo, const cv::Mat& sh_coef,
   *                     cv::Mat* image)
   * @brief Render an estimation of the environmental lighting on a sphere
   * @param[in] aldebo  Vertex colors to use for rendering [N x RGBA]
   * @param[in] sh_coef Spherical Harmonics coefficients estimated [3 x 9]
   * @param[out] image  Lighting visualization
   * @return -1 if error, 0 otherwise
   */
  int Generate(const cv::Mat& aldebo, const cv::Mat& sh_coef, cv::Mat* image);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  dims
   * @fn    size_t dims()
   * @brief Provide the number of points on the sphere.
   * @return    Number points
   */
  size_t dims() {
    return n_pts_;
  }

  /**
   * @name  set_background_color
   * @fn    void set_background_color(const T& r, const T& g, const T& b)
   * @brief Define background color
   * @param[in] r   Red color
   * @param[in] g   Green color
   * @param[in] b   Blue color
   */
  void set_background_color(const T& r, const T& g, const T& b) {
    renderer_->set_clear_color(r, g, b, T(1.0));
  }

 private:
  /** Renderer */
  OGLOffscreenRenderer<T>* renderer_;
  /** Shader */
  OGLProgram* shader_;
  /** Sphere */
  Mesh<T> sphere_;
  /** Number of vertex */
  size_t n_pts_;
};

}  // namespace LTS5
#endif  // __LTS5_SH_VIEWER__
