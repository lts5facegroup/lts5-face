/**
 *  @file   io_buffer.hpp
 *  @brief  OpenGL FBO I/O buffers
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   15/07/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_IO_BUFFER__
#define __LTS5_IO_BUFFER__

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLIOBuffer
 * @brief   Framebuffer I/O buffer
 * @author  Christophe Ecabert
 * @date    15/07/2016
 * @see     http://ogldev.atspace.co.uk/
 * @ingroup ogl
 */
class LTS5_EXPORTS OGLIOBuffer {

 public:

#pragma mark -
#pragma mark Type Definition

  /**
   * @enum  BufferType
   * @brief Type of buffer
   */
  enum class BufferType : char {
    /** Depth only */
    kDepth,
    /** Single channel - uchar */
    kR,
    /** Single channel - float */
    kRf,
    /** Three channels - uchar */
    kRGB,
    /** Three channels - float */
    kRGBf,
    /** Four channels - uchar */
    kRGBA,
    /** Four channels - float */
    kRGBAf
  };

#pragma mark -
#pragma mark -
#pragma mark Initialization

  /**
   * @name  OGLIOBuffer
   * @fn  OGLIOBuffer()
   * @brief Constructor
   */
  OGLIOBuffer();

  /**
   * @name  OGLIOBuffer
   * @fn  OGLIOBuffer(const OGLIOBuffer& other) = delete
   * @brief Copy constructor
   */
  OGLIOBuffer(const OGLIOBuffer& other) = delete;

  /**
   * @name  operator=
   * @fn  OGLIOBuffer& operator=(const OGLIOBuffer& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return  Newly assigned object
   */
  OGLIOBuffer& operator=(const OGLIOBuffer& rhs) = delete;

  /**
   * @name  ~OGLIOBuffer
   * @fn  ~OGLIOBuffer()
   * @brief Destructor
   */
  ~OGLIOBuffer();

  /**
   * @name  Init
   * @fn  int Init(const int& width,
                   const int& height,
                   const int& m_sample,
                   const bool& with_depth,
                   const BufferType& type)
   * @brief Initialize OpenGL buffer
   * @param[in] width       Window width
   * @param[in] height      Window height
   * @param[in] m_sample    Number of multisampling for antialiasing. A value
   *                        of `0` indicates without antialiasing
   * @param[in] with_depth  Include or not depth buffer
   * @param[in] type        Type of buffer
   * @return  -1 if error, 0 otherwise
   */
  int Init(const int& width,
           const int& height,
           const int& m_sample,
           const bool& with_depth,
           const BufferType& type);

#pragma mark -
#pragma mark Usage

  /**
   * @name  BlitBuffer
   * @fn    void BlitBuffer() const
   * @brief Blit multisampled buffer(s) to normal FBO
   */
  void BlitBuffer() const;

  /**
   * @name  BindForWriting
   * @fn  void BindForWriting() const;
   * @brief Bind internal buffer for writting
   */
  void BindForWriting() const;

  /**
   * @name  BindForReading
   * @fn  void BindForReading(const unsigned int& texture_unit) const
   * @brief Bind for reaeding
   * @param[in] texture_unit  Texture unit to activate
   */
  void BindForReading(const unsigned int& texture_unit) const;

  /**
   * @name  Unbind
   * @fn  void Unbind()
   * @brief Unbind the underlying framebuffer
   */
  void Unbind() const;

  /**
   * @name  TransferColorToTexture
   * @fn    int TransferColorToTexture() const
   * @brief Transfer color pixel from FBO to Texture object
   * @return -1 if error, 0 otherwise
   */
  int TransferColorToTexture() const;

  /**
   * @name  ReadColorBuffer
   * @fn  int ReadColorBuffer(cv::Mat* image) const
   * @brief Write the content of a FBO into a given \p image
   * @param[out]  image   Where to write image
   * @return  -1 if error, 0 otherwise
   */
  int ReadColorBuffer(cv::Mat* image) const;

  /**
   * @name  ReadColorBuffer
   * @fn  int ReadColorBuffer(void* buffer) const
   * @brief Write the content of a FBO into a given \p buffer
   * @param[out] buffer Buffer where to copy the data
   * @return  -1 if error, 0 otherwise
   */
  int ReadColorBuffer(void* buffer) const;

  /**
   * @name  ReadDepthBuffer
   * @fn  int ReadDepthBuffer(cv::Mat* depth_map)
   * @brief Write the content of a FBO into a given \p depth_map
   * @param[out]  depth_map   Where to write depth_map
   * @return  -1 if error, 0 otherwise
   */
  int ReadDepthBuffer(cv::Mat* depth_map) const;

  /**
   * @name  GetOglBuffer
   * @fn    unsigned int GetOglBuffer(const BufferType& buff_type) const
   * @brief Provide reference to the underlaying OpenGL buffer
   * @param[in] buff_type
   * @return    OpenGL reference
   */
  unsigned int GetOglBuffer(const BufferType& buff_type) const;

#pragma mark -
#pragma mark Private

 private:

  /**
   * @enum  FramebufferType
   * @brief Type of framebuffer
   */
  enum FramebufferType {
    /** Standard */
    kStandard = 0,
    /** Multi-sampled */
    kMultisampling = 1
  };

  /**
   * @name  InitFramebuffer
   * @fn    int InitFramebuffer(const FramebufferType& fb_type)
   * @brief Initialize framebuffer
   * @param[in] fb_type Type of framebuffer
   * @return    -1 if error, 0 otherwise
   */
  int InitFramebuffer(const FramebufferType& fb_type);


  /** Buffer width */
  int width_;
  /** Buffer height */
  int height_;
  /** Frame buffer object */
  unsigned int fbo_[2];
  /** Color buffer */
  unsigned int color_buffer_[2];
  /** Depth buffer */
  unsigned int depth_buffer_[2];
  /** Internal type */
  BufferType type_;
  /** Indicate if the buffer multisampling ratio, 0 mean no multiplsampling */
  int m_sample_;
  /** Depth is included */
  bool w_depth_;


};
}  // namepsace LTS5

#endif //__LTS5_IO_BUFFER__
