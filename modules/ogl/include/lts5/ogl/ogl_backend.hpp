/**
 *  @file   ogl_backend.hpp
 *  @brief  Abstract OpenGL Backend
 *          Based on : http://ogldev.atspace.co.uk/
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   11/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_OGL_BACKEND__
#define __LTS5_OGL_BACKEND__

#include "lts5/utils/library_export.hpp"
#include "lts5/ogl/callbacks.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLBackend
 * @brief   Abstract OpenGL Backend
 * @author  Christophe
 * @date    11/02/2016
 * @ingroup ogl
 */
class OGLBackend {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  ~OGLBackend
   * @fn  virtual ~OGLBackend(void)
   * @brief Destructor
   */
  virtual ~OGLBackend(void) {}

  /**
   * @name  Init
   * @fn  virtual void Init(int argc,
                            const char** argv,
                            bool with_depth,
                            bool with_stencil) = 0
   * @brief Initialize OpenGL Backend
   * @param[in] argc                Number of parameters in Command line
   * @param[in] argv                Command line parameters
   * @param[in] with_depth          Use depth
   * @param[in] with_stencil        Use stencil
   * @param[in] with_back_culling   Indicate if triangle facing back will be
   *                                removed
   */
  virtual void Init(int argc,
                    const char** argv,
                    bool with_depth,
                    bool with_stencil,
                    bool with_back_culling) = 0;

  /**
   * @name  Terminate
   * @fn  void Terminate(void)
   * @brief Shutdown OpenGL backend
   */
  virtual void Terminate(void) = 0;

  /**
   * @name  CreateWindows
   * @fn  virtual bool CreateWindows(const int width,
                                      const int height,
                                      const bool fullscreen,
                                      const char* window_title) = 0
   * @brief Initialiaze OpenGL window
   * @param[in] width         Window's width
   * @param[in] height        Window's height
   * @param[in] fullscreen    True for fullscreen window
   * @param[in] window_title  Window's title
   */
  virtual bool CreateWindows(const int width,
                             const int height,
                             const bool fullscreen,
                             const char* window_title) = 0;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Run
   * @fn  virtual void Run(OGLCallback* ogl_callback) = 0;
   * @brief Enter into GLFW main loop
   * @param[in] ogl_callback  Pointer to object holding opengl callback
   *                          implementation
   */
  virtual void Run(OGLCallback* ogl_callback) = 0;

  /**
   * @name  SwapBuffer
   * @fn  virtual void SwapBuffer(void) = 0
   * @brief Swap opengl buffer in order to display what has been rendered
   */
  virtual void SwapBuffer(void) = 0;

  /**
   * @name  LeaveMainLoop
   * @fn  void LeaveMainLoop(void)
   * @brief Leave backend mainloop
   */
  virtual void LeaveMainLoop(void) = 0;

  /**
   * @name  SetMousePos
   * @fn  virtual void SetMousePos(const int x, const int y) = 0
   * @brief Set mouse position inside window
   * @param[in] x   Cursor X position
   * @param[in] y   Cursor Y position
   */
  virtual void SetMousePos(const int x, const int y) = 0;

#pragma mark -
#pragma mark Protected
 protected:

  /** Indicate if we're using depth */
  bool with_depth_;
  /** Indicate if we're using stencil */
  bool with_stencil_;
  /** Back face culling */
  bool with_back_cull_;
};
}  // namespace LTS5

#endif //LTS5_OGL_BACKEND_HPP
