/**
 *  @file   uniform_buffer_object.hpp
 *  @brief  Uniform Buffer Object (UBO) abstraction
 *  @ingroup ogl
 *  @see https://learnopengl.com/Advanced-OpenGL/Advanced-GLSL
 *
 *  @author Christophe Ecabert
 *  @date   11/28/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_UNIFORM_BUFFER_OBJECT__
#define __LTS5_UNIFORM_BUFFER_OBJECT__

#include <vector>
#include <cstdint>
#include <cstddef>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   UniformBufferObject
 * @brief   Uniform Buffer Object (UBO) abstraction
 * @author  Christophe Ecabert
 * @date    28.11.18
 * @ingroup ogl
 */
class UniformBufferObject {
 public:

#pragma mark -
#pragma mark Type definition

  /** Binding information */
  using UboBindings = std::pair<size_t, size_t>;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  UniformBufferObject
   * @fn    UniformBufferObject()
   * @brief Constructor
   */
  UniformBufferObject();

  /**
   * @name  UniformBufferObject
   * @fn    UniformBufferObject(const UniformBufferObject& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  UniformBufferObject(const UniformBufferObject& other) = delete;

  /**
   * @name  operator=
   * @fn UniformBufferObject& operator=(const UniformBufferObject& rhs) = delete;
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  UniformBufferObject& operator=(const UniformBufferObject& rhs) = delete;

  /**
   * @name  ~UniformBufferObject
   * @fn    ~UniformBufferObject()
   * @brief Destructor
   */
  ~UniformBufferObject();

#pragma mark -
#pragma mark Usage

  /**
   * @name  Create
   * @fn    int Create(const std::vector<UboBindings>& bindings)
   * @brief Create an array of uniform buffer object. The length of the vector
   *        define the number of buffer to allocate.
   *        Each elements are a pair where the first element is the binding
   *        point and the second one is the size of the buffer in bytes
   * @param[in] bindings Individual buffer binding informations
   * @return -1 if error, 0 otherwise
   */
  int Create(const std::vector<UboBindings>& bindings);

  /**
   * @name  Bind
   * @fn    void Bind(const size_t& idx) const
   * @brief Bind a given buffer
   * @param[in] idx Buffer index
   */
  void Bind(const size_t& idx) const;

  /**
   * @name  Unbind
   * @fn    void Unbind() const
   * @brief Unbind current buffer
   */
  void Unbind() const;

  /**
   * @name  AddData
   * @fn    int AddData(const size_t& buffer, const size_t& offset,
   *                    const size_t& size, const void* data)
   * @brief Fill buffer with data
   * @param[in] buffer  Buffer index
   * @param[in] offset  Offset within the buffer in bytes (where to start)
   * @param[in] size    Dimensions of the data to be copied (in bytes)
   * @param[in] data    Pointer to the data to be copied
   * @return -1 if error, 0 otherwise
   */
  int AddData(const size_t& buffer,
          const size_t& offset,
          const size_t& size,
          const void* data);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_object
   * @fn    uint32_t get_object() const
   * @brief Provide reference to underlying opengl buffer.
   * @return    OpenGL object
   */
  uint32_t get_object(const size_t& idx) const {
    if (ubo_) {
      return ubo_[idx];
    } else {
      return 0;
    }
  }

#pragma mark -
#pragma mark Private
 private:
  /** Uniform Buffer Object */
  uint32_t* ubo_;
  /** Number of buffer generated */
  size_t n_buffer_;
};

}  // namespace LTS5
#endif  // __LTS5_UNIFORM_BUFFER_OBJECT__
