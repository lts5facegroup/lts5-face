/**
 *  @file   lts5/ogl/camera.hpp
 *  @brief  Camera interface to use render scene in OpenGL
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   14/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_OGL_CAMERA__
#define __LTS5_OGL_CAMERA__

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/math/matrix.hpp"
#include "lts5/geometry/aabb.hpp"
#include "lts5/ogl/key_types.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  OGLCamera
 *  @brief  Camera interface to use render scene in OpenGL, based on :
 *          Thomas Dalling - http://tomdalling.com/
 *  @author Christophe Ecabert
 *  @date   14/01/16
 *  @tparam T Data type
 *  @ingroup ogl
 */
template<typename T>
class LTS5_EXPORTS OGLCamera {
 public :
#pragma mark -
#pragma mark Type definitions
  /** Vector 3 */
  using Vec3 = Vector3<T>;
  /** Matrix 4x4 */
  using Mat4 = Matrix4x4<T>;
  /** Bounding box */
  using Bbox_t = AABB<T>;

#pragma mark -
#pragma mark Initialization

  /**
   *  @name OGLCamera
   *  @fn OGLCamera(const int& win_width, const int& win_height)
   *  @brief  Constructor
   *  @param[in]  win_width   Window width
   *  @param[in]  win_height  Window height
   */
  OGLCamera(const int& win_width, const int& win_height);

  /**
   * @name  OGLCamera
   * @fn    OGLCamera(const OGLCamera& other) = delete
   * @brief Copy constructor
   * @param[in] other   Object to copy from
   */
  OGLCamera(const OGLCamera& other) = default;

  /**
   * @name  operator=
   * @fn    OGLCamera& operator=(const OGLCamera& rhs) = delete
   * @brief Assignment operator
   * @param rhs Object to assign from
   * @return    Newly assigned object
   */
  OGLCamera& operator=(const OGLCamera& rhs) = default;

  /**
   *  @name ~OGLCamera
   *  @fn ~OGLCamera() = default;
   *  @brief  Destructor
   */
  ~OGLCamera() = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  LookAt
   * @fn    void LookAt(const Bbox_t& volume)
   * @brief Orients camera to look at the given volume
   * @param[in] volume Volume to look at
   */
  void LookAt(const Bbox_t& volume);

  /**
   *  @name LookAt
   *  @fn void LookAt(const Vec3& position, const Vec3& target)
   *  @brief  Orients the camera so that it is directly facing a given \p target
   *  @param[in]  position  Location of the camera
   *  @param[in]  target    Where to look at
   */
  void LookAt(const Vec3& position, const Vec3& target);

  /**
 * @name OnKeyboard
 * @fn    void OnKeyboard(const OGLKey& key, const OGLKeyState& state)
 * @brief Keyboard event
 * @param[in] key     Key that trigger the event
 * @param[in] state   State of the key
 * @return true if callback is defined, false otherwise
 */
  bool OnKeyboard(const OGLKey& key, const OGLKeyState& state);

  /**
   * @name  OnMouseClick
   * @fn    void OnMouseClick(const OGLMouse& button,
                            const OGLKeyState& state,
                            const int& x,
                            const int& y)
   * @brief Method to invoke when key is pressed/released
   * @param button  Which mouse's button has trigger the event
   * @param state   Button state : pressed, released
   * @param x       Cursor X position
   * @param y       Cursor Y position
   * @return true if callback is defined, false otherwise
   */
  bool OnMouseClick(const OGLMouse& button,
                    const OGLKeyState& state,
                    const int& x,
                    const int& y);

  /**
   * @name  OnMouseMove
   * @fn    void OnMouseMove(const int& x, const int& y)
   * @brief Method to invoke when mouse moves on the screen
   * @param x   Current cursor X position
   * @param y   Current cursor Y position
   * @return true if callback is defined, false otherwise
   */
  bool OnMouseMove(const int& x, const int& y);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name set_position
   *  @fn void set_position(const Vec3& position)
   *  @brief  Set location of the camera
   *  @param[in]  position  Position of the camera
   */
  void set_position(const Vec3& position) {
    position_ = position;
  }

  /**
   *  @name set_field_of_view
   *  @fn void set_field_of_view(const T& fov)
   *  @brief  Determines how "wide" the view of the camera is.
   *  @param[in]  fov Field of view angle
   */
  void set_field_of_view(const T& fov) {
    assert(fov > T(0.0) && fov < T(180.0));
    field_of_view_ = fov;
    this->projection();
  }

  /**
   *  @name set_planes
   *  @fn void set_planes(const T& near, const T& far)
   *  @brief  Set the \p near and \p far plane distance. Everything between the
   *          \p near plane and the \p far plane will be visible. Everything closer
   *          than the \p near plane, or farther than the \p far plane, will not
   *          be visible.
   *  @param[in]  near  Minimum visible distance. Must be > 0
   *  @param[in]  far   Maximum visible distance. Must be > \p near
   */
  void set_planes(const T& near, const T& far) {
    assert(near > T(0.0) && far > near);
    near_plane_ = near;
    far_plane_ = far;
    this->projection();
  }

  /**
   *  @name get_position
   *  @fn const Vec3& get_position() const
   *  @brief  Provide camera position
   *  @return Camera location
   */
  const Vec3& get_position() const {
    return position_;
  }

  /**
   *  @name get_position
   *  @fn Vec3& get_position()
   *  @brief  Provide camera position
   *  @return Camera location
   */
  Vec3& get_position() {
    return position_;
  }

  /**
   *  @name get_field_of_view
   *  @fn T get_field_of_view() const
   *  @brief  Provide camera field of view
   *  @return Camera field of view
   */
  T get_field_of_view() const {
    return field_of_view_;
  }

  /**
   *  @name get_near_plane
   *  @fn T get_near_plane() const
   *  @brief  Provide camera near plane
   *  @return Camera near plane
   */
  T get_near_plane() const {
    return near_plane_;
  }

  /**
   *  @name get_far_plane
   *  @fn T get_far_plane() const
   *  @brief  Provide camera far plane
   *  @return Camera far plane
   */
  T get_far_plane() const {
    return far_plane_;
  }

  /**
   *  @name orientation
   *  @fn Mat4& orientation(const bool flip = false)
   *  @brief  Rotation matrix that determines the camera orientation
   *          (i.e. extrinsic parameters)
   *  @param[in]  flip  Indicate if the camera need to be place upside down
   *  @return Camera orientation
   */
  Mat4& orientation(bool flip = false);

  /**
   *  @name forward
   *  @fn const Vec3& forward() const
   *  @brief  Unit vector indicating the direction the camera is facing
   *  @return Facing direction
   */
  const Vec3& forward() const {
    return forward_;
  }

  /**
   *  @name forward
   *  @fn Vec3& forward()
   *  @brief  Unit vector indicating the direction the camera is facing
   *  @return Facing direction
   */
  Vec3& forward() {
    return forward_;
  }

  /**
   *  @name right
   *  @fn const Vec3& right() const
   *  @brief  Unit vector indicating the direction to the right of the camera
   *  @return Right direction
   */
  const Vec3& right() const {
    return right_;
  }

  /**
   *  @name right
   *  @fn Vec3& right()
   *  @brief  Unit vector indicating the direction to the right of the camera
   *  @return Right direction
   */
  Vec3& right() {
    return right_;
  }

  /**
   *  @name up
   *  @fn const Vec3& up() const
   *  @brief  Unit vector indicating the direction to the top of the camera
   *  @return Top direction
   */
  const Vec3& up() const {
    return up_;
  }

  /**
   *  @name up
   *  @fn const Vec3& up() const
   *  @brief  Unit vector indicating the direction to the top of the camera
   *  @return Top direction
   */
  Vec3& up() {
    return up_;
  }

  /**
   *  @name matrix
   *  @fn Mat4 matrix() const
   *  @brief  The combined camera transformation matrix, including perspective
   *          projection. This complete matrix must be used in the vertex shader
   *  @return Matrix transform
   */
  Mat4 matrix();

  /**
   *  @name projection
   *  @fn Mat4 projection() const
   *  @brief  The perspective projection transform
   *  @return Projection transform
   */
  Mat4 projection();

  /**
   *  @name view
   *  @fn Mat4 view(bool flip = false) const
   *  @brief  Translation and rotation of the camera. Same as 'matrix' without
   *          projection transformation matrix included in.
   *  @param[in]  flip  Indicate if the camera need to be place upside down
   *  @return view matrix
   */
  Mat4 view(bool flip = false);

  /**
   * @name  get_move_speed
   * @fn    const T& get_move_speed() const
   * @brief Provide the speed at which the camera moves in space
   * @return    Camera's Displacement speed
   */
  const T& get_move_speed() const {
    return move_speed_;
  }

  /**
   * @name  get_move_speed
   * @fn    T& get_move_speed()
   * @brief Provide the speed at which the camera moves in space
   * @return    Camera's Displacement speed
   */
  T& get_move_speed() {
    return move_speed_;
  }

  /**
   * @name  get_rotation_speed
   * @fn    const T& get_rotation_speed() const
   * @brief Provide the speed at which the camera rotates in space
   * @return    Camera's rotation speed
   */
  const T& get_rotation_speed() const {
    return rotation_speed_;
  }

  /**
   * @name  get_rotation_speed
   * @fn    T& get_rotation_speed()
   * @brief Provide the speed at which the camera rotates in space
   * @return    Camera's rotation speed
   */
  T& get_rotation_speed() {
    return rotation_speed_;
  }

  /**
   * @name  get_window_width
   * @fn    T get_window_width()
   * @brief Provide the width of the window
   * @return    Window's width
   */
  T get_window_width() const {
    return win_width_;
  }

  /**
   * @name  get_window_height
   * @fn    T get_window_height()
   * @brief Provide the height of the window
   * @return    Window's height
   */
  T get_window_height() const {
    return win_height_;
  }

#pragma mark -
#pragma mark Private
 private :

  /**
 * @enum  State
 * @brief List all possible state in the trackball state machine
 */
  enum State {
    /** Nothing currently happening */
    kNone,
    /** Doing rotation */
    kRotate
  };

  /**
   * @name  GetMouseProjectionOnBall
   * @fn    void GetMouseProjectionOnBall(const int& x,
                                          const int& y,
                                          Vec3* pts)
   * @brief Project screen loation onto ball
   * @param[in] x       Cursor X position on the screen
   * @param[in] y       Cursor Y position on the screen
   * @param[out] pts    Corresponding points on sphere
   */
  void GetMouseProjectionOnBall(const int& x,
                                const int& y,
                                Vec3* pts);

  /** Camera position */
  Vec3 position_;
  /** Camera forward */
  Vec3 forward_;
  /** Camera up */
  Vec3 up_;
  /** Camera up */
  Vec3 right_;
  /** Camera orientation */
  Mat4 orientation_;
  /** Camera Projection */
  Mat4 projection_;
  /** Window width */
  T win_width_;
  /** Window height */
  T win_height_;
  /** Field of View */
  T field_of_view_;
  /** Near plane */
  T near_plane_;
  /** Far plane */
  T far_plane_;

  /** State machine */
  State state_;
  /** Displacement speed */
  T move_speed_;
  /** Rotation speed */
  T rotation_speed_;
  /** Rotation starting position */
  Vec3 rotations_start_;
  /** Rotation end position */
  Vec3 rotations_end_;
};
}  // namespace LTS5
#endif /* __LTS5_OGL_CAMERA__ */
