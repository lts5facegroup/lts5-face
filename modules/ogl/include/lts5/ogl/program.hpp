/**
 *  @file   program.hpp
 *  @brief  OpenGL Programm helper interface
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   14/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_OGL_PROGRAM__
#define __LTS5_OGL_PROGRAM__

#include <vector>
#include <string>
#include <utility>
#include <sstream>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/sys/refcount.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/math/matrix.hpp"
#include "lts5/utils/process_error.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/ogl/shader.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  OGLProgram
 *  @brief  OpenGL program helper interface, based on :
 *          Thomas Dalling - http://tomdalling.com/
 *  @author Christophe Ecabert
 *  @date   14/01/16
 *  @ingroup ogl
 */
class LTS5_EXPORTS OGLProgram : RefCounted {
 public :
#pragma mark -
#pragma mark Type

  template<typename T>
  using Vec2 = Vector2<T>;
  template<typename T>
  using Vec3 = Vector3<T>;
  template<typename T>
  using Vec4 = Vector4<T>;
  template<typename T>
  using Mat2 = Matrix2x2<T>;
  template<typename T>
  using Mat3 = Matrix3x3<T>;
  template<typename T>
  using Mat4 = Matrix4x4<T>;

  /**
   *  @enum AttributeType
   *  @brief  List of possible type for shader attributes
   */
  enum AttributeType {
    /** Vertex type */
    kPoint,
    /** Normal type */
    kNormal,
    /** Texture coordinate type */
    kTexCoord,
    /** Vertex color type */
    kColor
  };

  /**
   *  @struct Attributes
   *  @brief  Property of shader's attribute
   */
  struct Attributes {
    /** Type of attribute */
    AttributeType type;
    /** Layout location */
    unsigned int location;
    /** Attribute name (i.e. input in shader) */
    std::string name;
    /** Size */
    int size;

    /**
     *  @name Attributes
     *  @fn Attributes(const AttributeType& type,
                       const unsigned int& loc,
                       const std::string& name,
                       const int& size)
     *  @brief  Construct one Attribute's property object
     *  @param[in]  type  Attribute's type
     *  @param[in]  loc   Attribute's location
     *  @param[in]  name  Attribute's name
     *  @param[in]  size  Number of element in the attribute. For example a vec2
     *                    attribute has a size==2
     */
    Attributes(const AttributeType& type,
               const unsigned int& loc,
               const std::string& name,
               const int& size) : type(type),
                                  location(loc),
                                  name(name),
                                  size(size) {}

    /**
     *  @name Attributes
     *  @fn Attributes(const AttributeType& type,
                       const unsigned int& loc,
                       const std::string& name)
     *  @brief  Construct one Attribute's property object
     *  @param[in]  type  Attribute's type
     *  @param[in]  loc   Attribute's location
     *  @param[in]  name  Attribute's name
     */
    Attributes(const AttributeType& type,
               const unsigned int& loc,
               const std::string& name) : Attributes::Attributes(type,
                                                                 loc,
                                                                 name,
                                                                 -1) {}
  };

  /**
   * @class     VSProperties
   * @brief     Vertex shader properties generated from mesh
   * @date      22/01/2020
   * @author    Christophe Ecabert
   * @ingroup   ogl
   */
  template<class Mesh_t>
  class VSProperties {
   public:
    /** Vertex */
    using Vertex = typename Mesh_t::Vertex;
    /** Normal */
    using Normal = typename Mesh_t::Normal;
    /** TCoord */
    using TCoord = typename Mesh_t::TCoord;
    /** Color */
    using Color = typename Mesh_t::Color;
    /** Output: {data type, name} */
    using Output_t = std::pair<std::string, std::string>;
    /** Uniform: {data type: name} */
    using Uniform_t = std::pair<std::string, std::string>;

    /**
     * @name    VSProperties
     * @fn      VSProperties(const Mesh_t& mesh)
     * @brief   Constructor
     * @param[in] mesh  Mesh object to generate code for
     */
    explicit VSProperties(const Mesh_t& mesh);

    /**
     * @name    Code
     * @fn  std::string Code()
     * @brief Generate vertex shader code
     * @return  Shader's code
     */
    std::string Code() const;

    /** Attributes */
    std::vector<Attributes> attributes_;
    /** Outputs */
    std::vector<Output_t> outputs_;
    /** Uniform */
    std::vector<Uniform_t> uniforms_;
  };

  /**
   * @class     FSProperties
   * @brief     Fragment shader properties generated from mesh
   * @date      22/01/2020
   * @author    Christophe Ecabert
   * @ingroup   ogl
   */
  template<typename Mesh_t>
  class FSProperties {
   public:
    /** Normal */
    using Normal = typename Mesh_t::Normal;
    /** TCoord */
    using TCoord = typename Mesh_t::TCoord;
    /** Color */
    using Color = typename Mesh_t::Color;
    /** Input: <data type, name> */
    using Input_t = std::pair<std::string, std::string>;
    /** Output: <data type, name> */
    using Output_t = std::pair<std::string, std::string>;
    /** Uniform: {data type: name} */
    using Uniform_t = std::pair<std::string, std::string>;
    /** Sampler: {type, name} */
    using Sampler_t = std::pair<std::string, std::string>;

    /**
     * @name    FSProperties
     * @fn      FSProperties(const Mesh_t& mesh)
     * @brief   Constructor
     * @param[in] mesh  Mesh object to generate code for
     */
    explicit FSProperties(const Mesh_t& mesh);

    /**
     * @name    Code
     * @fn  std::string Code()
     * @brief Generate fragment shader code
     * @return  Shader's code
     */
    std::string Code() const;

    /** Inputs */
    std::vector<Input_t> inputs_;
    /** Outputs */
    std::vector<Output_t> outputs_;
    /** Samplers */
    std::vector<Sampler_t> samplers_;
    /** Vertex color indicator */
    bool have_color_;
    /** Texture map indicator */
    bool have_tex_;
  };


#pragma mark -
#pragma mark Initialization

  /**
   * @name  FromMeshFile
   * @fn    static OGLProgram FromMesh(const Mesh_t& mesh)
   * @brief Create a shading program from a given mesh object. Works only for
   *        simple case at the moment.
   * @param[in] mesh    Mesh from which to create shading program
   * @tparam Mesh_t Mesh type
   * @return    Shading program
   */
  template<typename Mesh_t>
  static OGLProgram FromMesh(const Mesh_t& mesh);

  /**
   * @name  OGLProgram
   * @fn    OGLProgram()
   * @brief Constructor, empty shading program
   */
  OGLProgram() = default;

  /**
   *  @name OGLProgram
   *  @fn OGLProgram(const std::vector<OGLShader>& shaders)
   *  @brief  Create OpenGL program interface
   *  @param[in]  shaders     List of shader to use in this program
   *  @throw  Exception if something goes wrong
   */
  OGLProgram(const std::vector<OGLShader>& shaders);

  /**
   *  @name OGLProgram
   *  @fn OGLProgram(const std::vector<OGLShader>& shaders,
                     const std::vector<Attributes>& attributes)
   *  @brief  Create OpenGL program interface
   *  @param[in]  shaders     List of shader to use in this program
   *  @param[in]  attributes  List of shader input attributes
   *  @throw  Exception if something goes wrong
   */
  OGLProgram(const std::vector<OGLShader>& shaders,
             const std::vector<Attributes>& attributes);

  /**
   *  @name OGLProgram
   *  @fn OGLProgram(const OGLProgram& other)
   *  @brief  Copy constructor
   *  @param[in]  other Program to copy from
   */
  OGLProgram(const OGLProgram& other);

  /**
   *  @name operator=
   *  @fn OGLProgram& operator=(const OGLProgram& rhs)
   *  @brief  Assignment operator
   *  @param[in]  rhs   Program to assign from
   */
  OGLProgram& operator=(const OGLProgram& rhs);

  /**
   *  @name ~OGLProgram
   *  @fn ~OGLProgram() override
   *  @brief  Destructor
   */
  ~OGLProgram() override;

  /**
   * @name  AddShader
   * @fn    void AddShader(const OGLShader& shader)
   * @brief Add shader to the program.
   * @param[in] shader      Shader instance to add
   */
  void AddShader(const OGLShader& shader) {
    this->AddShader(shader, std::vector<Attributes>());
  }

  /**
   * @name  AddShader
   * @fn    void AddShader(const OGLShader& shader,
                           const std::vector<Attributes>& attributes)
   * @brief Add shader with attributes to the program.
   * @param[in] shader      Shader instance to add
   * @param[in] attributes  List of attributes for the shader
   */
  void AddShader(const OGLShader& shader,
                 const std::vector<Attributes>& attributes);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Build
   * @fn    void Build()
   * @brief Build shading program
   * @throw Throw exception if something went wrong during the compilation of
   *        the shading program.
   */
  void Build();

  /**
   *  @name Use
   *  @fn void Use()
   *  @brief  Start to use this program
   */
  void Use() const;

  /**
   *  @name StopUsing
   *  @fn void StopUsing()
   *  @brief  Stop using this program
   */
  void StopUsing() const;

  /**
   *  @name IsValid
   *  @fn int IsValid()
   *  @brief  Check if program is valid or not.
   *  @return -1 if not, otherwise
   */
  int IsValid();

#pragma mark -
#pragma mark Accessors

  /**
   *  @name IsUsing
   *  @fn bool IsUsing()
   *  @brief  Indicate if the current program is being used
   *  @return True if currently used
   */
  bool IsUsing() const;

  /**
   *  @name Attrib
   *  @fn int Attrib(const char* attrib_name)
   *  @brief  Provide the attribute index for a given name
   *  @param[in]  attrib_name Name of the attribute to look for
   *  @return The uniform index for the given name, as returned
   *          from glGetUniformLocation.
   */
  int Attrib(const char* attrib_name) const;

  /**
   *  @name Uniform
   *  @fn int Uniform(const char* uniform_name)
   *  @brief  Provide uniform reference for a given name
   *  @param[in]  uniform_name  Name of the uniform to look for
   *  @return The uniform index for the given name, as returned
   *          from glGetUniformLocation.
   */
  int Uniform(const char* uniform_name) const;

  /**
   * @name  SetUniformBlockBinding
   * @brief Link a uniform block name to a given mounting/binding point
   * @param[in] uniform_name Name of the uniform's block
   * @param[in] binding_point   Binding point
   * @return -1 if error, 0 otherwise
   */
  int SetUniformBlockBinding(const char* uniform_name,
                             const uint32_t& binding_point);

  /**
   *  @name get_error_message
   *  @fn std::string get_error_message() const
   *  @brief  Provide OpenGL error message
   *  @return Error message
   */
  std::string get_error_message() const {
    return msg_;
  }

  /**
   *  @name get_attributes
   *  @fn const std::vector<Attributes>& get_attributes() const
   *  @brief  Provide access to attributes
   *  @return List of Attributes
   */
  const std::vector<Attributes>& get_attributes() const {
    return attributes_;
  }

  const unsigned int& get_ref() const {
    return object_;
  }


#define OGL_PROGRAM_ATTRIB_N_UNIFORM_SETTERS(OGL_TYPE) \
  void SetAttrib(const char* attrib_name, OGL_TYPE v0) const; \
  void SetAttrib(const char* attrib_name, OGL_TYPE v0, OGL_TYPE v1) const; \
  void SetAttrib(const char* attrib_name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2) const; \
  void SetAttrib(const char* attrib_name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2, OGL_TYPE v3) const; \
\
  void SetAttrib1v(const char* attrib_name, const OGL_TYPE* v) const; \
  void SetAttrib2v(const char* attrib_name, const OGL_TYPE* v) const; \
  void SetAttrib3v(const char* attrib_name, const OGL_TYPE* v) const; \
  void SetAttrib4v(const char* attrib_name, const OGL_TYPE* v) const; \
\
  void SetUniform(const char* uniform_name, const OGL_TYPE v0) const;  \
  void SetUniform(const char* uniform_name, const OGL_TYPE v0, const OGL_TYPE v1) const;  \
  void SetUniform(const char* uniform_name, const OGL_TYPE v0, const OGL_TYPE v1, const OGL_TYPE v2) const;  \
  void SetUniform(const char* uniform_name, const OGL_TYPE v0, const OGL_TYPE v1, const OGL_TYPE v2, const OGL_TYPE v3) const;\
\
  void SetUniform1v(const char* uniform_name, const OGL_TYPE* v, int count=1) const;  \
  void SetUniform2v(const char* uniform_name, const OGL_TYPE* v, int count=1) const;  \
  void SetUniform3v(const char* uniform_name, const OGL_TYPE* v, int count=1) const;  \
  void SetUniform4v(const char* uniform_name, const OGL_TYPE* v, int count=1) const;  \

  OGL_PROGRAM_ATTRIB_N_UNIFORM_SETTERS(float)
  OGL_PROGRAM_ATTRIB_N_UNIFORM_SETTERS(double)
  OGL_PROGRAM_ATTRIB_N_UNIFORM_SETTERS(int)
  OGL_PROGRAM_ATTRIB_N_UNIFORM_SETTERS(unsigned int)

  /**
   * @name  SetUniform
   * @brief Setup uniform value of type Mat2
   * @param[in] uniform_name  Uniform name variable to set
   * @param[in] m             Matrix data
   * @param[in] transpose     transpose flag (default=false)
   * @tparam T Data type
   */
  template<typename T>
  void SetUniform(const char* uniform_name,
                  const Mat2<T>& m,
                  bool transpose=false) const;
  /**
   * @name  SetUniform
   * @brief Setup uniform value of type Mat3
   * @param[in] uniform_name  Uniform name variable to set
   * @param[in] m             Matrix data
   * @param[in] transpose     transpose flag (default=false)
   */
  template<typename T>
  void SetUniform(const char* uniform_name,
                  const Mat3<T>& m,
                  bool transpose=false) const;
  /**
   * @name  SetUniform
   * @brief Setup uniform value of type Mat4
   * @param[in] uniform_name  Uniform name variable to set
   * @param[in] m             Matrix data
   * @param[in] transpose     transpose flag (default=false)
   * @tparam T Data type
   */
  template<typename T>
  void SetUniform(const char* uniform_name,
                  const Mat4<T>& m,
                  bool transpose=false) const;

  /**
   * @name  SetUniform
   * @brief Setup uniform value of type vec2
   * @param[in] uniform_name  Uniform name variable to set
   * @param[in] v             Vector data
   */
  template<typename T>
  void SetUniform(const char* uniform_name,
                  const Vec2<T>& v) const;

  /**
   * @name  SetUniform
   * @brief Setup uniform value of type vec3
   * @param[in] uniform_name  Uniform name variable to set
   * @param[in] v             Vector data
   */
  template<typename T>
  void SetUniform(const char* uniform_name,
                  const Vec3<T>& v) const;
  /**
   * @name  SetUniform
   * @brief Setup uniform value of type vec4
   * @param[in] uniform_name  Uniform name variable to set
   * @param[in] v             Vector data
   */
  template<typename T>
  void SetUniform(const char* uniform_name,
                  const Vec4<T>& v) const;


#pragma mark -
#pragma mark Private

 private :

  /** OpenGL program reference */
  unsigned int object_;
  /** Error message */
  std::string msg_;
  /** Shaders, keep shader's until the shading program is build */
  std::vector<OGLShader> shaders_;
  /** Input attributes (shader parameters) */
  std::vector<Attributes> attributes_;
};


#pragma mark -
#pragma mark Inlined implementation

/*
   * @name  FromMesh
   * @fn    static OGLProgram FromMesh(const Mesh_t& mesh)
   * @brief Create a shading program from a given mesh object. Works only for
   *        simple case at the moment.
   * @param[in] mesh    Mesh from which to create shading program
   * @tparam Mesh_t Mesh type
   * @return    Shading program
   */
template<typename Mesh_t>
OGLProgram OGLProgram::FromMesh(const Mesh_t& mesh) {
  using VSProp = VSProperties<Mesh_t>;
  using FSProp = FSProperties<Mesh_t>;
  using ShaderType = typename OGLShader::ShaderType;
  // Get vertex shader properties
  VSProp vprop(mesh);
  FSProp fprop(mesh);

  // Create both shader
  std::vector<OGLShader> shaders;
  shaders.emplace_back(vprop.Code(), ShaderType::kVertex);
  shaders.emplace_back(fprop.Code(), ShaderType::kFragment);
  return OGLProgram(shaders, vprop.attributes_);
}


template<typename T, typename Container>
struct VectorProperty {};

template<typename T>
struct VectorProperty<T, Vector2<T>> {
  static constexpr int dim = 2;
  static constexpr const char* str = "vec2";
};
template<typename T>
struct VectorProperty<T, Vector3<T>> {
  static constexpr int dim = 3;
  static constexpr const char* str = "vec3";
};
template<typename T>
struct VectorProperty<T, Vector4<T>> {
  static constexpr int dim = 4;
  static constexpr const char* str = "vec4";
};


/*
 * @name    VSProperties
 * @fn      VSProperties(const Mesh_t& mesh)
 * @brief   Constructor
 * @param[in] mesh  Mesh object to generate code for
 * @tparam T    Data type
 * @tparam ColorT Color container type
 */
template<class Mesh_t>
OGLProgram::VSProperties<Mesh_t>::VSProperties(const Mesh_t& mesh) {
  using dtype = typename Mesh_t::DType;
  using AttrType = AttributeType;
  const auto& vertex = mesh.get_vertex();
  const auto& normal = mesh.get_normal();
  const auto& tcoord = mesh.get_tex_coord();
  const auto& color = mesh.get_vertex_color();
  // Vertex ?
  int loc = 0;
  if (!vertex.empty()) {
    attributes_.emplace_back(AttrType::kPoint, loc, "position", 3);
    loc += 1;
  }
  // Normal ?
  if (!normal.empty()) {
    attributes_.emplace_back(AttrType::kNormal, loc, "normal", 3);
    outputs_.emplace_back("vec3", "normal0");
    loc += 1;
  }
  // Tex coordinate ?
  if (!tcoord.empty()) {
    attributes_.emplace_back(AttrType::kTexCoord, loc, "tcoord", 2);
    outputs_.emplace_back("vec2", "tcoord0");
    loc += 1;
  }
  // Vertex color ?
  if (!color.empty()) {
    int sz = VectorProperty<dtype, Color>::dim;
    auto str = std::string(VectorProperty<dtype, Color>::str);
    attributes_.emplace_back(AttrType::kColor, loc, "color", sz);
    outputs_.emplace_back(str, "color0");
    loc += 1;
  }
  // Set default uniform: Model-View-Projection transform, i.e. camera
  uniforms_.emplace_back("mat4", "camera");
}

/*
 * @name    VSProperties
 * @fn      VSProperties(const Mesh_t& mesh)
 * @brief   Constructor
 * @param[in] mesh  Mesh object to generate code for
 * @tparam T    Data type
 * @tparam ColorT Color container type
 */
template<class Mesh_t>
std::string OGLProgram::VSProperties<Mesh_t>::Code() const {
  // Build shader code
  std::stringstream code;
  code << "#version 330" << std::endl;
  // Attributes
  for (const auto& a : attributes_) {
    code << "layout (location = " << a.location << ") in vec" << a.size << " "
         << a.name << ";" << std::endl;
  }
  // Outputs
  for (const auto& o : outputs_) {
    code << "out " << o.first << " " << o.second << ";" << std::endl;
  }
  // Uniforms
  for (const auto& u : uniforms_) {
    code << "uniform " << u.first << " " << u.second << ";" << std::endl;
  }
  // Code
  code << "void main() {" << std::endl;
  for (auto i = 0; i < attributes_.size(); ++i) {
    const auto& a = attributes_[i];
    if (i == 0) { // Assume position is the first entry ... might be wrong
      const auto& u = uniforms_[i];
      code << "  gl_Position = " << u.second << " * vec4(" << a.name << ", 1);"
           << std::endl;
    } else {
      // Copy attribute to output, assume proper ordering
      const auto& o = outputs_[i - 1];
      code << "  " << o.second << " = " << a.name << ";" << std::endl;
    }
  }
  code << "}" << std::endl;
  return code.str();
}

/*
 * @name    FSProperties
 * @fn      FSProperties(const Mesh_t& mesh)
 * @brief   Constructor
 * @param[in] mesh  Mesh object to generate code for
 */
template<class Mesh_t>
OGLProgram::FSProperties<Mesh_t>::
FSProperties(const Mesh_t& mesh) : have_color_(false),
                                   have_tex_(false) {
  using dtype = typename Mesh_t::DType;
  const auto& normal = mesh.get_normal();
  const auto& tcoord = mesh.get_tex_coord();
  const auto& color = mesh.get_vertex_color();
  const auto& mat = mesh.get_materials();
  // Normal ?
  if (!normal.empty()) {
    inputs_.emplace_back("vec3", "normal0");
  }
  // Tex coordinate ?
  if (!tcoord.empty()) {
    inputs_.emplace_back("vec2", "tcoord0");
  }
  // Vertex color ?
  if (!color.empty()) {
    auto str = std::string(VectorProperty<dtype, Color>::str);
    inputs_.emplace_back(str, "color0");
    have_color_ = true;
  }
  // Sampler
  for (auto i = 0; i < mat.size(); ++i) {
    // Handle only ambient texture for the moment
    const auto& m = mat[i];
    if (!m.get_ambient_texture().empty()) {
      auto sname = "tex_sampler" + std::to_string(i);
      samplers_.emplace_back("sampler2D", sname);
      have_tex_ = true;
    }
  }
  // Output
  auto out_str = have_color_ ? VectorProperty<dtype, Color>::str : "vec4";
  outputs_.emplace_back(out_str, "frag_color");

  // Check
  if (have_color_ && have_tex_) {
    // Can not have both color entry at the moment ... might change later
    throw ProcessError(ProcessError::kErr,
                       "Can not have both vertex color + texture map",
                       FUNC_NAME);
  }
}

/*
 * @name    Code
 * @fn  std::string Code()
 * @brief Generate fragment shader code
 * @return  Shader's code
 */
template<class Mesh_t>
std::string OGLProgram::FSProperties<Mesh_t>::Code() const {
  using dtype = typename Mesh_t::DType;
  // Build shader code
  std::stringstream code;
  code << "#version 330" << std::endl;

  // Inputs
  for (const auto& in : inputs_) {
    code << "in " << in.first << " " << in.second << ";" << std::endl;
  }
  // Outputs
  for (const auto& out : outputs_) {
    code << "out " << out.first << " " << out.second << ";" << std::endl;
  }
  // Samplers
  for (const auto& s : samplers_) {
    code << "uniform " << s.first << " " << s.second << ";" << std::endl;
  }

  // Code, no shading applied
  // Fill only first output / first sampler
  code << "void main() {" << std::endl;
  const auto& out = outputs_.front();
  if (have_color_) {
    // Vertex color always last entry of `inputs_`
    const auto& in = inputs_.back();
    code << "  " << out.second << " = " << in.second << ";" << std::endl;
  } else if (have_tex_) {
    const auto& s = samplers_.front();

    code << "  " << out.second << " = texture(" << s.second << ", tcoord0);"
         << std::endl;
  } else {
    // Set default color to white
    auto stype = VectorProperty<dtype, Color>::str;
    code << "  " << out.second << " = " << stype << "(1.f);"<< std::endl;
  }
  code << "}" << std::endl;
  return code.str();
}
}  // namespace LTS5
#endif /* __LTS5_OGL_PROGRAM__ */
