/**
 *  @file   frame_buffer_object.hpp
 *  @brief  Frame Buffer Object (FBO) abstraction
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   11/11/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_FRAME_BUFFER_OBJECT__
#define __LTS5_FRAME_BUFFER_OBJECT__

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   FrameBufferObject
 * @brief   Frame Buffer Object (FBO)
 * @author  Christophe Ecabert
 * @date    11/10/2020
 * @ingroup ogl
 */
class LTS5_EXPORTS FrameBufferObject {
 public:
#pragma mark -
#pragma mark Type Definition

  /**
   * @enum  BufferType
   * @brief Type of buffer
   */
  enum class BufferType : char {
    /** Default (not initialized) */
    kUnknown,
    /** Depth only */
    kDepth,
    /** Single channel - uchar */
    kR,
    /** Single channel - float */
    kRf,
    /** Three channels - uchar */
    kRGB,
    /** Three channels - float */
    kRGBf,
    /** Four channels - uchar */
    kRGBA,
    /** Four channels - float */
    kRGBAf
  };
#pragma mark -
#pragma mark Initialization
  /**
   * @name  FrameBufferObject
   * @fn    FrameBufferObject()
   * @brief Constructor
   */
  FrameBufferObject();

  /**
   * @name  FrameBufferObject
   * @fn  FrameBufferObject(const FrameBufferObject& other) = delete
   * @brief Copy constructor
   */
  FrameBufferObject(const FrameBufferObject& other) = delete;

  /**
   * @name  operator=
   * @fn  FrameBufferObject& operator=(const FrameBufferObject& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return  Newly assigned object
   */
  FrameBufferObject& operator=(const FrameBufferObject& rhs) = delete;

  /**
   * @name  ~FrameBufferObject
   * @fn    ~FrameBufferObject()
   * @brief Destructor
   */
  ~FrameBufferObject();

  /**
   * @name  Create
   * @fn    int Create(const int& height, const int& width, const int& depth,
   *                    const BufferType& type)
   * @brief Create internel buffer for a given configuration. If configuration
   *    match, does nothing
   * @param[in] height  Image height
   * @param[in] width   Image width
   * @param[in] depth   Number of image to render at once
   * @param[in] type    Type of buffer
   * @return    -1 if error, 0 otherwise
   */
  int Create(const int& height,
             const int& width,
             const int& depth,
             const BufferType& type);

#pragma mark -
#pragma mark Usage

  /**
   * @name Validate
   * @fn    int Validate() const
   * @brief Check if framebuffer is valide
   * @return -1 if invalid, 0 otherwise
   */
  int Validate() const;

  /**
   * @name  Clear
   * @fn int Clear()
   * @brief Clear color/depth buffer
   * @param[in] height  Image height
   * @param[in] width   Image width
   * @param[in] depth   Number of image
   * @return    -1 if error, 0 otherwise
   */
  int Clear(const int& height, const int& width, const int& depth);

  /**
   * @name  BindForWriting
   * @fn  void BindForWriting() const;
   * @brief Bind internal buffer for writting
   */
  void BindForWriting() const;

  /**
   * @name  BindForReading
   * @fn  void BindForReading(const unsigned int& texture_unit) const
   * @brief Bind for reading
   * @param[in] texture_unit  Texture unit to activate
   */
  void BindForReading(const unsigned int& texture_unit) const;

  /**
   * @name  Unbind
   * @fn  void Unbind()
   * @brief Unbind the underlying framebuffer
   */
  void Unbind() const;

  /**
   * @name  GetOglBuffer
   * @fn    unsigned int GetOglBuffer(const BufferType& buff_type) const
   * @brief Provide reference to the underlaying OpenGL buffer
   * @param[in] buff_type
   * @return    OpenGL reference
   */
  unsigned int GetOglBuffer(const BufferType& buff_type) const;

#pragma mark -
#pragma mark Private
 private:
  /** Frame buffer object */
  unsigned int fbo_;
  /** Color buffer */
  unsigned int color_buffer_;
  /** Depth buffer */
  unsigned int depth_buffer_;
  /** Buffer width */
  int width_;
  /** Buffer height */
  int height_;
  /** Buffer depth */
  int depth_;
  /** Internal type */
  BufferType type_;
};

}  // namespace LTS5
#endif  // __LTS5_FRAME_BUFFER_OBJECT__
