/**
 *  @file   context_lightweight.hpp
 *  @brief  Lightweight opengl context from https://github.com/NVlabs/nvdiffrast
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   11/11/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_CONTEXT_LIGHTWEIGHT___
#define __LTS5_CONTEXT_LIGHTWEIGHT___

#include "lts5/utils/library_export.hpp"

struct GLContext;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLContextLightweight
 * @brief   Lightweight opengl context container used for various rendering task
 * @author  Christophe Ecabert
 * @date    11/10/2020
 * @ingroup ogl
 */
class LTS5_EXPORTS OGLContextLightweight {
 public:

#pragma mark -
#pragma mark Initialization
  /**
   * @name  OGLContextLightweight
   * @fn    OGLContextLightweight()
   * @brief Constructor
   */
  OGLContextLightweight();

  /**
   * @name  OGLContextLightweight
   * @fn    OGLContextLightweight(bool activate_ctx)
   * @brief Constructor
   * @param[in] activate_ctx If `True` will directly active the context,
   *    otherwise no.
   */
  explicit OGLContextLightweight(bool activate_ctx);

  /**
   * @name  OGLContextLightweight
   * @fn OGLContextLightweight(const OGLContextLightweight& other) = delete
   * @brief Copy Constructor
   * @param[in] other Object to copy from
   */
  OGLContextLightweight(const OGLContextLightweight& other) = delete;

  /**
   * @name  OGLContextLightweight
   * @fn OGLContextLightweight(OGLContextLightweight&& other) = delete
   * @brief Move Constructor
   * @param[in] other Object to move
   */
  OGLContextLightweight(OGLContextLightweight&& other) = delete;

  /**
   * @name  operator=
   * @fn    OGLContextLightweight& operator=(const OGLContextLightweight& rhs) = delete
   * @brief Copy assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  OGLContextLightweight& operator=(const OGLContextLightweight& rhs) = delete;

  /**
   * @name  operator=
   * @fn    OGLContextLightweight& operator=(OGLContextLightweight&& rhs) = delete
   * @brief Move assignment operator
   * @param[in] rhs Object to move-assign from
   * @return    Newly moved assign object
   */
  OGLContextLightweight& operator=(OGLContextLightweight&& rhs) = delete;

  /**
   * @name  ~OGLContextLightweight
   * @fn    ~OGLContextLightweight()
   * @brief Destructor
   */
  ~OGLContextLightweight();

#pragma mark -
#pragma mark Usage

  /**
   * @name Version
   * @brief Provide opengl version in use.
   * @param[out] major  Version major
   * @param[out] minor  Version minor
   */
  void Version(int* major, int* minor) const;

  /**
   * @name  ActiveContext
   * @fn    void ActiveContext() const
   * @brief Make this context the current one
   */
  void ActiveContext() const;

  /**
   * @name  DisableContext
   * @fn    void DisableContext() const
   * @brief Detach from current context
   */
  void DisableContext() const;

#pragma mark -
#pragma mark Private
 private:
  /** Context */
  GLContext* ctx_;
};


}  // namespace LTS5
#endif  // __LTS5_CONTEXT_LIGHTWEIGHT___
