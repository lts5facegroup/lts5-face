/**
 *  @file   shader.hpp
 *  @brief  OpenGL Shader helper
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   13/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_SHADER__
#define __LTS5_SHADER__

#include <string>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/sys/refcount.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  OGLShader
 *  @brief  OpenGL shader helper, based on :
 *          Thomas Dalling - http://tomdalling.com/
 *  @author Christophe Ecabert
 *  @date   13/01/16
 *  @ingroup ogl
 */
class LTS5_EXPORTS OGLShader : RefCounted {
 public :
#pragma mark -
#pragma mark Type Definition

  /**
   * @enum  ShaderType
   * @brief List of possible shader
   */
  enum class ShaderType : char {
    /** Vertex */
    kVertex = 1,
    /** Geometry */
    kGeometry,
    /** Fragment */
    kFragment
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name OGLShaderFromFile
   *  @fn static OGLShader OGLShaderFromFile(const std::string& filepath,
                                             const ShaderType& type)
   *  @brief  Create shader from text file
   *  @param[in]  filepath    Path to the text file where shader code is stored
   *  @param[in]  type        Type of shader
   *  @return Shader
   *  @throw  Throw exception if error occurs during the construction
   */
  static OGLShader OGLShaderFromFile(const std::string& filepath,
                                     const ShaderType& type);

  /**
   *  @name OGLShader
   *  @fn OGLShader(const std::string& code, const ShaderType& type)
   *  @brief  Create shader from text file
   *  @param[in]  code Shader's code
   *  @param[in]  type Type of shader, GL_VERTEX_SHADER, GL_FRAGMENT_SHADER
   *  @throw  Throw exception if error occurs during the construction
   */
  OGLShader(const std::string& code, const ShaderType& shader);

  /**
   *  @name OGLShader
   *  @fn OGLShader(const OGLShader& other)
   *  @brief  Copy constructor
   *  @param[in]  other Shader to copy from
   */
  OGLShader(const OGLShader& other);

  /**
   *  @name ~OGLShader
   *  @fn ~OGLShader() override
   *  @brief  Destructor
   */
  ~OGLShader() override;

  /**
   *  @name operator=
   *  @fn OGLShader& operator=(const OGLShader& rhs)
   *  @brief  Assignment operator
   *  @param[in]  rhs Shader to assign from
   *  @return Assigned shader
   */
  OGLShader& operator=(const OGLShader& rhs);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name Object
   *  @fn unsigned int Object() const
   *  @brief  Provide the reference to OpenGL shader
   *  @return Shader's reference
   */
  unsigned int Object() {
    return object_;
  }

  /**
   *  @name Object
   *  @fn unsigned int Object() const
   *  @brief  Provide the reference to OpenGL shader
   *  @return Shader's reference
   */
  unsigned int Object() const {
    return object_;
  }

#pragma mark -
#pragma mark Private
 private :
  /** OpenGL shader reference */
  unsigned int object_;
};

}  // namespace LTS5
#endif /* __LTS5_SHADER__ */
