/**
 *  @file   texture.hpp
 *  @brief  OpenGL Texture helper
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   15/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_OGL_TEXTURE__
#define __LTS5_OGL_TEXTURE__

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/sys/refcount.hpp"
#include "lts5/utils/bitmap.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  OGLTexture
 *  @brief  OpenGL Texture interface, based on :
 *          Thomas Dalling - http://tomdalling.com/
 *  @author Christophe Ecabert
 *  @date   15/01/16
 *  @ingroup ogl
 */
class LTS5_EXPORTS OGLTexture : RefCounted {
 public :
#pragma mark -
#pragma mark Initialization

  /**
   * @name  FromFile
   * @fn    static OGLTexture FromFile(const std::string& filename)
   * @brief Create a texture from a given filename
   * @param[in] filename Path to the texture file
   * @return    OGLTexture object
   */
  static OGLTexture FromFile(const std::string& filename);

  /**
   * @name  OGLTexture
   * @fn    OGLTexture() = default
   * @brief Default constructor
   */
  OGLTexture() = default;

  /**
   *  @name OGLTexture
   *  @fn OGLTexture(const Bitmap& bitmap,
                     const int& mag_filter,
                     const int& wrap_mode)
   *  @brief  Constructor, create an empty OpenGL texture buffer
   *  @param[in]  bitmap      Bitmap object to initialize from
   *  @param[in]  mag_filter  Texture interpolation filter : GL_LINEAR, GL_NEAREST
   *  @param[in]  wrap_mode   Texture wrap : GL_REPEAT, GL_MIRRORED_REPEAT,
   *                          GL_CLAMP_TO_EDGE, or GL_CLAMP_TO_BORDER
   */
  OGLTexture(const Bitmap& bitmap,
             const int& mag_filter,
             const int& wrap_mod);

  /**
   *  @name OGLTexture
   *  @fn OGLTexture(const cv::Mat& image,
                     const int& mag_filter,
                     const int& wrap_mode)
   *  @brief  Constructor, create an empty OpenGL texture buffer
   *  @param[in]  image       Image matrix to initialize from
   *  @param[in]  mag_filter  Texture interpolation filter : GL_LINEAR, GL_NEAREST
   *  @param[in]  wrap_mode   Texture wrap : GL_REPEAT, GL_MIRRORED_REPEAT,
   *                          GL_CLAMP_TO_EDGE, or GL_CLAMP_TO_BORDER
   */
  OGLTexture(const cv::Mat& image,
             const int& mag_filter,
             const int& wrap_mode);

  /**
   *  @name OGLTexture
   *  @fn OGLTexture(const OGLTexture& other)
   *  @brief  Copy constructor
   *  @param[in]  other Texture to copy from
   */
  OGLTexture(const OGLTexture& other);

  /**
   *  @name operator=
   *  @fn OGLTexture& operator=(const OGLTexture& rhs)
   *  @brief  Assignment operator
   *  @param[in]  rhs   Texture to assign from
   */
  OGLTexture& operator=(const OGLTexture& rhs);

  /**
   *  @name ~OGLTexture
   *  @fn ~OGLTexture() override
   *  @brief  Destructor
   */
  ~OGLTexture() override;

  /**
   * @name Init
   * @fn Init(const int& width,
                    const int& height,
                    const unsigned char* data,
                    const int& mag_filter,
                    const int& wrap_mod)
   * @brief Initialize texture
   * @param[in] width       Texture width
   * @param[in] height      Texture height
   * @param[in] data        Data array holding pixel value to upload. Can be set
   *                        to `nullptr`
   * @param[in] format      Data format
   * @param[in] mag_filter  Magnitude filter
   * @param[in] wrap_mode   Wrapping mode
   */
  void Init(const int& width,
            const int& height,
            const unsigned char* data,
            const unsigned int& format,
            const int& mag_filter,
            const int& wrap_mode);

#pragma mark -
#pragma mark Process

  /**
   * @name  Upload
   * @fn  int Upload(const Bitmap& bitmap)
   * @brief Upload data into OpenGL buffer
   * @param[in] bitmap  Bitmap image to upload
   * @return -1 if error, 0 otherwise
   */
  int Upload(const Bitmap& bitmap);

  /**
   * @name  Upload
   * @fn  int Upload(const cv::Mat& image)
   * @brief Upload data into OpenGL buffer
   * @param[in] image  Image to upload
   * @return -1 if error, 0 otherwise
   */
  int Upload(const cv::Mat& image);

  /**
   *  @name Bind
   *  @fn void Bind(unsigned int texture_unit) const
   *  @brief  Bind texture
   *  @param[in]  texture_unit  Unit to bind to
   */
  void Bind(unsigned int texture_unit) const;

  /**
   *  @name Unbind
   *  @fn void Unbind() const
   *  @brief  Unbind texture
   */
  void Unbind() const;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name Object
   *  @fn unsigned int Object() const
   *  @brief  Provide reference of texture object
   *  @return Texture reference
   */
  unsigned int Object() const {
    return object_;
  }

  /**
   *  @name get_width
   *  @fn int get_width() const
   *  @brief  Provide original width of the bitmap used as texture
   *  @return Width
   */
  int get_width() const {
    return width_;
  }

  /**
   *  @name get_height
   *  @fn GLsizei get_height() const
   *  @brief  Provide original height of the bitmap used as texture
   *  @return Height
   */
  int get_height() const {
    return height_;
  }

  /**
   * @name  is_grayscale
   * @fn  bool is_grayscale() const
   * @brief Indicate if the texture is grayscale or not
   * @return  True if greyscale, False otherwise
   */
  bool is_grayscale() const;

  /**
   * @name  get_ogl_buffer
   * @fn    unsigned int get_ogl_buffer() const
   * @brief Provide reference to OpenGL buffer holding this texture
   * @return    OpenGL buffer ID
   */
  unsigned int get_ogl_buffer() const {
    return object_;
  }

#pragma mark -
#pragma mark Private
 private :
  /** Opengl texture reference */
  unsigned int object_;
  /** Texture width */
  int width_;
  /** Texture height */
  int height_;
  /** Texture pixel format */
  unsigned int frmt_;

};
}  // namespace LTS5
#endif /* __LTS5_OGL_TEXTURE__ */
