/**
 *  @file   callbacks.hpp
 *  @brief  Interface defining all callbacks needed by OpenGL backend
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   10/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_CALLBACKS__
#define __LTS5_CALLBACKS__

#include "lts5/ogl/key_types.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLCallback
 * @brief   Unified for OpenGL backend.
 *          Based on : http://ogldev.atspace.co.uk/
 * @author  Christophe Ecabert
 * @date    10/02/16
 * @ingroup ogl
 */
class OGLCallback {
 public :

#pragma mark -
#pragma mark Initialization

  /**
   * @name  ~OGLCallback
   * @fn    virtual ~OGLCallback() = default
   * @brief Destructor
   */
  virtual ~OGLCallback() = default;

#pragma mark -
#pragma mark Callbacks

  /**
   * @name  OGLKeyboardCb
   * @fn  virtual void OGLKeyboardCb(const OGLKey& key,
   *                                 const OGLKeyState& state)
   * @brief Callback for keyboard event
   * @param[in] key     Key that trigger the callback
   * @param[in] state   Key state at that time
   */
  virtual void OGLKeyboardCb(const OGLKey& key, const OGLKeyState& state) {}

  /**
   * @name  OGLPassiveMouseCb
   * @fn  virtual void OGLPassiveMouseCb(const int x, const int y)
   * @brief Callback handling mouse movement inside OpenGL window
   * @param[in] x   Mouse's X coordinate
   * @param[in] y   Mouse's Y coordinate
   */
  virtual void OGLPassiveMouseCb(const int x, const int y) {}

  /**
   * @name  OGLRenderCb
   * @fn  virtual void OGLRenderCb()
   * @brief Callback invoked when scene need to be rendered
   */
  virtual void OGLRenderCb() {}

  /**
   * @name  OGLMouseCb
   * @fn  virtual void OGLMouseCb(const OGLMouse& button, const OGLKeyState& state,
                          const int x, const int y)
   * @brief Mouse callback
   * @param[in] button  Button that trigger the callback
   * @param[in] state   Button's state at that time
   * @param[in] x       Mouse's X coordinate
   * @param[in] y       Mouse's Y coordinate
   */
  virtual void OGLMouseCb(const OGLMouse& button,
                          const OGLKeyState& state,
                          const int x,
                          const int y) {}
};
}

#endif //__LTS5_CALLBACKS__
