/**
 *  @file   lts5/ogl/rasterizer.hpp
 *  @brief  Implement triangle rasterizer based on OpenGL
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   7/9/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_RASTERIZER__
#define __LTS5_RASTERIZER__

#include <string>
#include <unordered_map>

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/matrix.hpp"
#include "lts5/ogl/context.hpp"
#include "lts5/ogl/shader_storage.hpp"
#include "lts5/ogl/io_buffer.hpp"
#include "lts5/ogl/program.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   Rasterizer
 * @brief   Triangle rasterizer based on OpenGL
 * @author  Christophe Ecabert
 * @date    09/07/2020
 * @ingroup ogl
 */
class LTS5_EXPORTS Rasterizer {
 public:

  using Mat4 = Matrix4x4<float>;
#pragma mark -
#pragma mark - Initialization

  /**
   * @name  Rasterizer
   * @brief Constructor
   * @param[in] width           Image width
   * @param[in] height          Image height
   * @param[in] vertex_code     Vertex shader code
   * @param[in] geometry_code   Geometry shader code
   * @param[in] fragment_code   Fragment shader code
   */
  Rasterizer(const int& width,
             const int& height,
             const std::string& vertex_code,
             const std::string& geometry_code,
             const std::string& fragment_code);

  /**
   * @name  Rasterizer
   * @brief Constructor
   * @param[in] width           Image width
   * @param[in] height          Image height
   * @param[in] vertex_code     Vertex shader code
   * @param[in] geometry_code   Geometry shader code
   * @param[in] fragment_code   Fragment shader code
   * @param[in] clear_red       Red channel cleared value in [0, 1]
   * @param[in] clear_green     Green channel cleared value in [0, 1]
   * @param[in] clear_blue      Blue channel cleared value in [0, 1]
   * @param[in] clear_depth     Depth channel cleared value in [0, 1]
   */
  Rasterizer(const int& width,
             const int& height,
             const std::string& vertex_code,
             const std::string& geometry_code,
             const std::string& fragment_code,
             const float& clear_red,
             const float& clear_green,
             const float& clear_blue,
             const float& clear_depth);

#pragma mark -
#pragma mark - Usage

  void Enable() const;

  void Disable() const;

  int SetShaderStorage(const std::string& name,
                       const void* data,
                       const size_t& n_bytes);

  int SetUniform(const std::string& name, const Mat4& matrix) const;

  int Render(const int& n_pts, const int& offset);

  int GetImage(cv::Mat* image) const;

#pragma mark -
#pragma mark - Private

 private:
  /** OpenGL context */
  OGLContext ctx_;
  /** Rendering program */
  OGLProgram shader_;
  /** Framebuffer */
  OGLIOBuffer fmb_;
  /** Shader storage */
  std::unordered_map<std::string, OGLShaderStorageBuffer*> buffer_;
  /** Red clear value */
  float clear_r_;
  /** Green clear value */
  float clear_g_;
  /** Blue clear value */
  float clear_b_;
  /** Depth clear value */
  float clear_d_;

};

}  // namespace LTS5
#endif  // __LTS5_RASTERIZER__
