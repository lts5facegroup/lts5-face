/**
 *  @file   lts5/ogl/ogl.hpp
 *  @brief  3D Structure + Rendering abstraction
 *
 *  @author Christophe Ecabert
 *  @date   13/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_OGL__
#define __LTS5_OGL__

/** Shader */
#include "lts5/ogl/shader.hpp"
/** Program */
#include "lts5/ogl/program.hpp"
/** Camera */
#include "lts5/ogl/camera.hpp"
/** Texture */
#include "lts5/ogl/texture.hpp"
/** Key abstraction layer */
#include "lts5/ogl/key_types.hpp"
/** OpenGL Backend interface */
#include "lts5/ogl/callbacks.hpp"
/** GLFW Backend */
#include "lts5/ogl/glfw_backend.hpp"
/** GLUT Backend */
#include "lts5/ogl/glut_backend.hpp"
/** Generalized abstract Backend */
#include "lts5/ogl/ogl_backend.hpp"
/** Base Renderer */
#include "lts5/ogl/base_renderer.hpp"
/** OpenGL Renderer */
#include "lts5/ogl/renderer.hpp"
/** Wired box OpenGL Renderer */
#include "lts5/ogl/wired_renderer.hpp"
/** Image Renderer */
#include "lts5/ogl/image_renderer.hpp"
/** Text Renderer */
#include "lts5/ogl/text_renderer.hpp"
/** Bargraph */
#include "lts5/ogl/bargraph.hpp"
/** FBO IOBuffer */
#include "lts5/ogl/io_buffer.hpp"
/** Offscreen Renderer */
#include "lts5/ogl/offscreen_renderer.hpp"

#endif /* __LTS5_OGL__ */
