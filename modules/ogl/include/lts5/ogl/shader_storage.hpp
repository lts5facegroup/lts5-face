/**
 *  @file   lts5/ogl/shader_storage.hpp
 *  @brief  Implement interface for Shader Storage Buffer Object
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   7/9/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_SHADER_STORAGE__
#define __LTS5_SHADER_STORAGE__

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLShaderStorageBuffer
 * @brief   Interface for Shader Storage Buffer Object (OpenGL 4.3)
 * @author  Christophe Ecabert
 * @date    09/07/2020
 * @ingroup ogl
 */
class LTS5_EXPORTS OGLShaderStorageBuffer {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  OGLShaderStorageBuffer
   * @fn    OGLShaderStorageBuffer()
   * @brief Constructor
   */
  OGLShaderStorageBuffer();

  /**
   * @name  ~OGLShaderStorageBuffer
   * @fn    ~OGLShaderStorageBuffer()
   * @brief Destructor
   */
  ~OGLShaderStorageBuffer();

  /**
   * @name  OGLShaderStorageBuffer
   * @fn    OGLShaderStorageBuffer(OGLShaderStorageBuffer&& other) noexcept
   * @brief Move constructor
   * @param[in] other Object to move from
   */
  OGLShaderStorageBuffer(OGLShaderStorageBuffer&& other) noexcept;

  /**
   * @name  operator=
   * @fn    OGLShaderStorageBuffer& operator=(OGLShaderStorageBuffer&& rhs)
   * @brief Move assignment
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  OGLShaderStorageBuffer& operator=(OGLShaderStorageBuffer&& rhs) noexcept;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Bind
   * @fn    void Bind() const
   * @brief Bind the underlying buffer
   * @see  glBindBuffer
   */
  void Bind() const;

  /**
   * @name  BindBufferBase
   * @fn    void BindBufferBase(const int& index) const
   * @brief Bind the underlying buffer at a given binding index.
   * @param[in] index Binding index
   * @see  glBindBufferBase
   */
  void BindBufferBase(const int& index) const;

  /**
   * @name  Unbind
   * @fn    void Unbind() const
   * @brief Unbind underlying shader storage
   */
  void Unbind() const;

  /**
   * @name  UploadData
   * @fn    int UploadData(const void* data, const size_t& n_bytes) const
   * @brief Push data from cpu to opengl buffer
   * @param[in] data    Pointer to data to push to storage
   * @param[in] n_bytes Number of bytes to copy.
   * @return -1 if error, 0 otherwise
   */
  int UploadData(const void* data, const size_t& n_bytes) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_buffer
   * @fn    const unsigned int& get_buffer() const
   * @brief Return underlying buffer handle
   * @return    Buffer handle
   */
  const unsigned int& get_buffer() const {
    return buffer_;
  }

#pragma mark -
#pragma mark Private

 private:
  /** Buffer handle */
  unsigned int buffer_;
};

}  // namespace LTS5
#endif  // __LTS5_SHADER_STORAGE__
