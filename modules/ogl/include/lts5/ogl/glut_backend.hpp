/**
 *  @file   glut_backend.hpp
 *  @brief  Backend helper for GLUT.
 *          Based on : http://ogldev.atspace.co.uk/
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   10/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_GLUT_BACKEND__
#define __LTS5_GLUT_BACKEND__

#include "lts5/utils/library_export.hpp"
#include "lts5/ogl/ogl_backend.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLGlutBackend
 * @brief   GLUT Backend
 * @author  Christophe
 * @date    10/02/2016
 * @ingroup ogl
 */
class LTS5_EXPORTS OGLGlutBackend : public OGLBackend {

 public :

#pragma mark -
#pragma mark Initialization

  /**
   * @name  OGLGlutBackend
   * @fn  OGLGlutBackend(void)
   * @brief Constructor
   */
  OGLGlutBackend(void);

  /**
   * @name  OGLGlutBackend
   * @fn  OGLGlutBackend(const OGLGlutBackend& other) = delete
   * @brief Copy Constructor
   * @param[in] other   Backend to copy
   */
  OGLGlutBackend(const OGLGlutBackend& other) = delete;

  /**
   *  @name operator=
   *  @fn OGLGlutBackend& operator=(const OGLGlutBackend& rhs) = delete
   *  @brief  Assignment operator
   *  @param[in]  rhs   Backend to assign from
   */
  OGLGlutBackend& operator=(const OGLGlutBackend& rhs) = delete;

  /**
   * @name  ~OGLGlutBackend
   * @fn  ~OGLGlutBackend(void)
   * @brief Constructor
   */
  ~OGLGlutBackend(void);

  /**
   * @name  Init
   * @fn  void Init(int argc, const char** argv,
   *                bool with_depth, bool with_stencil)
   * @brief Initialize GLUT Backend
   * @param[in] argc          Number of parameters in Command line
   * @param[in] argv          Command line parameters
   * @param[in] with_depth    Use depth
   * @param[in] with_stencil  Use stencil
   * @param[in] with_back_culling   Indicate if triangle facing back will be
   *                                removed
   */
  void Init(int argc,
            const char** argv,
            bool with_depth,
            bool with_stencil,
            bool with_back_culling);

  /**
   * @name  Terminate
   * @fn  void Terminate(void)
   * @brief Shutdown GLUT backend
   */
  void Terminate(void);

  /**
   * @name  CreateWindows
   * @fn  bool CreateWindows(const int width, const int height,
   *                         const bool fullscreen,
   *                         const char* window_title)
   * @brief Initialiaze GLUT window
   * @param[in] width         Window's width
   * @param[in] height        Window's height
   * @param[in] fullscreen    True for fullscreen window
   * @param[in] window_title  Window's title
   */
  bool CreateWindows(const int width,
                     const int height,
                     const bool fullscreen,
                     const char* window_title);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Run
   * @fn  void Run(OGLCallback* ogl_callback)
   * @brief Enter into GLUT main loop
   * @param[in] ogl_callback  Pointer to object holding opengl callback
   *                          implementation
   */
  void Run(OGLCallback* ogl_callback);

  /**
   * @name  SwapBuffer
   * @fn  void SwapBuffer(void)
   * @brief Swap opengl buffer in order to display what has been rendered
   */
  void SwapBuffer(void);

  /**
   * @name  LeaveMainLoop
   * @fn  void LeaveMainLoop(void)
   * @brief Leave backend mainloop
   */
  void LeaveMainLoop(void);

  /**
   * @name  SetMousePos
   * @fn  void SetMousePos(const int x, const int y)
   * @brief Set mouse position inside window
   * @param[in] x   Cursor X position
   * @param[in] y   Cursor Y position
   */
  void SetMousePos(const int x, const int y);

#pragma mark -
#pragma mark Private

 private:
  /** Pointer to the object implementing OGLCallback interface. All event are
   * forwarded to this object */
  static OGLCallback *callback_;

  /**
   * @name  GlutKeyToOGLKey
   * @fn  static OGLKey GlutKeyToOGLKey(unsigned int key)
   * @brief Convert GLUT key into OGLKey format
   * @param[in] key GLUT key to convert
   * @return  OGLKey equivalent if exist otherwise kUndefined
   */
  static OGLKey GlutKeyToOGLKey(unsigned int key);

  /**
   * @name  GlutMouseToOGLMouse
   * @fn  static OGLMouse GlutMouseToOGLMouse(unsigned int button)
   * @brief Convert GLUT button into OGLMouse format
   * @param[in] button GLUT button to convert
   * @return  OGLMouse equivalent if exist otherwise kUndefined
   */
  static OGLMouse GlutMouseToOGLMouse(unsigned int button);

  /**
   * @name  SpecialKeyboardCb
   * @fn  static void SpecialKeyboardCb(int key, int x, int y)
   * @brief Handle special keyboard key
   * @param[in] key   Key that trigger the event
   * @param[in] x     X (Not used)
   * @param[in] y     Y (Not used)
   */
  static void SpecialKeyboardCb(int key, int x, int y);

  /**
   * @name  KeyboardCb
   * @fn  static void KeyboardCb(unsigned char key, int x, int y)
   * @brief Handle keyboard key
   * @param[in] key   Key that trigger the event
   * @param[in] x     X (Not used)
   * @param[in] y     Y (Not used)
   */
  static void KeyboardCb(unsigned char key, int x, int y);

  /**
   * @name  PassiveMouseCb
   * @fn  static void PassiveMouseCb(int x, int y)
   * @brief Mouse displacement callback
   * @param[in] x     Cursor's X position
   * @param[in] y     Cursor's Y position
   */
  static void PassiveMouseCb(int x, int y);

  /**
   * @name  RenderCb
   * @fn  static void RenderCb(void)
   * @brief Render scene
   */
  static void RenderCb(void);

  /**
   * @name  IdleCb
   * @fn  static void IdleCb(void)
   * @brief Idle callback
   */
  static void IdleCb(void);

  /**
   * @name  KeyboardCb
   * @fn  static void MouseCb(int button, int state, int x, int y)
   * @brief Handle Mouse event
   * @param[in] button  Button that trigger the callback
   * @param[in] state   Button state at that time
   * @param[in] x       Cursor's X position
   * @param[in] y       Cursor's y position
   */
  static void MouseCb(int button, int state, int x, int y);

  /**
   * @name  InitCallbacks
   * @fn  static void InitCallbacks(void)
   * @brief Initialize GLUT callbacks
   */
  static void InitCallbacks(void);

};
}  // Namespace LTS5
#endif //__LTS5_GLUT_BACKEND__
