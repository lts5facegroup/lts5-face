/**
 *  @file   renderer.hpp
 *  @brief  OpenGL rendering class
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   27/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_RENDERER__
#define __LTS5_RENDERER__

#include "lts5/utils/library_export.hpp"
#include "lts5/ogl/base_renderer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OGLRenderer
 * @brief   OpenGL rendering class
 * @author  Christophe Ecabert
 * @date    27/04/2016
 * @ingroup ogl
 * @tparam T Data Type
 * @tparam ColorContainer Container type in which vertex color is stored
 */
template<typename T, typename ColorContainer=Vector4<T>>
class LTS5_EXPORTS OGLRenderer : public OGLBaseRenderer<T, ColorContainer> {
 public:
#pragma mark -
#pragma mark Initialization

  /** Mesh Type */
  using Mesh = LTS5::Mesh<T, ColorContainer>;

#pragma mark -
#pragma mark Initialization


  /**
   * @name  OGLRenderer
   * @fn  OGLRenderer()
   * @brief Constructor
   */
  OGLRenderer();

  /**
   * @name  OGLRenderer
   * @fn  OGLRenderer(const OGLRenderer<T, ColorContainer>& other) = delete
   * @brief Copy constructor, disable
   * @param[in] other  OGLRenderer to copy from
   */
  OGLRenderer(const OGLRenderer<T, ColorContainer>& other) = delete;

  /**
   * @name  operator=
   * @fn  OGLRenderer& operator=(const OGLRenderer<T, ColorContainer>& rhs) = delete
   * @brief Assignment operator, disable
   * @param[in] rhs OGLRenderer to assign from
   * @return  Newly assigned OGLRenderer
   */
  OGLRenderer& operator=(const OGLRenderer<T, ColorContainer>& rhs) = delete;

  /**
   * @name  ~OGLRenderer
   * @fn  ~OGLRenderer()
   * @brief Destructor
   */
  ~OGLRenderer();

  /**
   * @name  AddMesh
   * @fn  void AddMesh(const Mesh* mesh)
   * @brief Add geometry to render
   * @param[in] mesh
   */
  void AddMesh(const Mesh* mesh);

  /**
   * @name  AddProgram
   * @fn  void AddProgram(const OGLProgram* program)
   * @brief Add OpenGL program to tell how to draw a given mesh
   * @param[in] program
   */
  void AddProgram(const OGLProgram* program);

  /**
   * @name  AddTexture
   * @fn  void AddTexture(const OGLTexture* texture)
   * @brief Add OpenGL texture (used to "paint" geometry)
   * @param[in] texture
   */
  void AddTexture(const OGLTexture* texture);
};
}  // namepsace LTS5
#endif //__LTS5_RENDERER__
