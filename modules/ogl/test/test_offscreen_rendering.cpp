/**
 *  @file   test_offscreen_rendering.cpp
 *  @brief  Offscreen rendering test target
 *
 *  @author Christophe Ecabert
 *  @date   14/07/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#include "opencv2/highgui.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/ogl/offscreen_renderer.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/utils/math/quaternion.hpp"

#include "lts5/utils/logger.hpp"

int main(int argc, const char * argv[]) {

  // Mesh type
  typedef  LTS5::Mesh<float> Mesh;
  // OffscreenRenderer
  typedef LTS5::OGLOffscreenRenderer<float> OffscreenRenderer;

  using Att = LTS5::OGLProgram::Attributes;
  using AttTyp = LTS5::OGLProgram::AttributeType;
  using Vec3f = LTS5::Vector3<float>;
  using Mat4f = LTS5::Matrix4x4<float>;
  using Quat = LTS5::Quaternion<float>;

  int err = -1;

  LTS5::CmdLineParser parser;
  parser.AddArgument("-m",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Mesh to render");
  parser.AddArgument("-v",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Vertex shader");
  parser.AddArgument("-f",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Fragment shader");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Output name/location");
  // Parse command line
  err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    std::string mesh_path;
    std::string vertex_shader_path;
    std::string frag_shader_path;
    std::string output_path;
    parser.HasArgument("-m",&mesh_path);
    parser.HasArgument("-v",&vertex_shader_path);
    parser.HasArgument("-f",&frag_shader_path);
    parser.HasArgument("-o",&output_path);

    // Load mesh
    Mesh* mesh = new Mesh();
    err = mesh->Load(mesh_path);
    if (!err) {
      mesh->ComputeVertexNormal();
      // Create Offscreen renderer
      // ---------------------------------------------
      OffscreenRenderer* renderer = new OffscreenRenderer();
      err = renderer->Init(512,
                           512,
                           0,
                           false,
                           OffscreenRenderer::ImageType::kRGBf);
      renderer->ActiveContext();
      // Create program
      // Must be done after OpenGL CTX is created !!!
      // ---------------------------------------------
      using ShaderType = typename LTS5::OGLShader::ShaderType;
      std::vector<LTS5::OGLShader> shaders;
      shaders.emplace_back(LTS5::OGLShader::OGLShaderFromFile(vertex_shader_path,
                                                           ShaderType::kVertex));
      shaders.emplace_back(LTS5::OGLShader::OGLShaderFromFile(frag_shader_path,
                                                           ShaderType::kFragment));
      std::vector<Att> attrib;
      attrib.emplace_back(Att(AttTyp::kPoint,  0, "position"));
      attrib.emplace_back(Att(AttTyp::kNormal,  1, "normal"));
      attrib.emplace_back(Att(AttTyp::kTexCoord,  2, "texCoord"));
      auto* program = new LTS5::OGLProgram(shaders, attrib);

      // Create camera + Define transform
      auto* cam = new LTS5::OGLCamera<float>(512, 512);
      cam->set_planes(0.01f, 10.f);
      cam->set_position(Vec3f(0, 0.14, 0.4));
      Mat4f transform;
      Quat q(Vec3f(0, 1, 0), 0.523599f);
      q.ToRotationMatrix(&transform);
      program->Use();
      program->SetUniform("camera", cam->projection() * cam->view());
      program->SetUniform("model",  transform);
      program->StopUsing();

      // Add mesh, program + Bind to opengl
      // ---------------------------------------------
      renderer->AddMesh(*mesh);
      renderer->AddProgram(*program);
      err |= renderer->BindToOpenGL();
      if (!err) {
        // Render
        renderer->ActiveContext();
        renderer->Render();
        // Get image
        cv::Mat img;
        cv::Mat depth;
        renderer->GetImage(&img);
        renderer->GetDepth(&depth);
        // Flip
        cv::flip(img, img, 0);
        cv::flip(depth, depth, 0);

        // Save image of asked
        if (!output_path.empty()) {
          cv::Mat uimg = img * 255.f;
          uimg.convertTo(uimg, CV_8UC1);
          cv::imwrite(output_path, uimg);
        }
        renderer->DisableContext();

        // Convert back to real depth
        auto f = cam->get_far_plane();
        auto n = cam->get_near_plane();

        for (int r = 0; r < depth.rows; ++r) {
          for (int c = 0; c < depth.cols; ++c) {
            auto zb = depth.at<float>(r, c);
            auto zn = (2.f * zb) - 1.f;
            auto ze = (2.f * f * n) / ((f + n) - ((f - n) * zn));
            if (ze > (10.f - 0.001f)) {
              // 10.f == far plane -> remove far plane for visualization purpose
              ze = 0.f;
            }
            depth.at<float>(r, c) = ze;
          }
        }

        double minVal, maxVal;
        cv::minMaxLoc(depth, &minVal, &maxVal);

        depth = depth / float(maxVal);

        cv::imshow("depth", depth);
        cv::imshow("img", img);

        cv::waitKey(0);
      } else {
        std::cout << "Error, can not Init/Bind renderer" << std::endl;
      }
      delete renderer;
      delete program;
      delete cam;
    } else {
      std::cout << "Error, can not load mesh : " << mesh_path << std::endl;
    }
    // Clean Up
    delete mesh;

  } else {
    std::cout << "Unable to parse command line" << std::endl;
  }
  return err;
}
