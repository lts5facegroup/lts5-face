#version 330

// input
in vec2 texCoord0;
in vec3 normal0;

uniform sampler2D gSampler;

// output
out vec4 fragColor;

void main() {
  //note: the texture function was called texture2D in older versions of GLSL
  fragColor = texture(gSampler, texCoord0);
  //fragColor = vec4(1.0, 1.0, 1.0, 1.0);
}