/**
 *  @file   test_stream_ogl.cpp
 *  @brief  Testing texture streaming into OpenGL context using PBO.
 *  @author Christophe Ecabert
 *  @date   18/05/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <cstdio>
#include <iostream>

#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/ogl/image_renderer.hpp"
#include "lts5/ogl/text_renderer.hpp"
#include "lts5/ogl/bargraph.hpp"
#include "lts5/ogl/glfw_backend.hpp"
#include "lts5/ogl/command.hpp"


#include "lts5/utils/circular_buffer.hpp"

using Color = typename LTS5::Mesh<float>::Color;
using OGLCmd = LTS5::OGLCommand;
using OGLCst = LTS5::OGLCommand::Constant;

struct CustomCallback : LTS5::OGLCallback {

  /** Shutdown flag */
  bool quit_;

  /**
   * @name  CustomCallback
   * @fn  CustomCallback()
   * @brief Constructor
   */
  CustomCallback() : quit_(false) {}

  /**
   * @name  OGLKeyboardCb
   * @fn  void OGLKeyboardCb(const OGLKey& key,
   *                                 const OGLKeyState& state)
   * @brief Callback for keyboard event
   * @param[in] key     Key that trigger the callback
   * @param[in] state   Key state at that time
   */
  void OGLKeyboardCb(const LTS5::OGLKey& key, const LTS5::OGLKeyState& state) override {
    // Quit
    if ((key == LTS5::OGLKey::kESCAPE) &&
        (state == LTS5::OGLKeyState::kPress)) {
      // Leave
      quit_ = true;
    }
  }
};


int main(int argc, const char * argv[]) {
  using CmdState = LTS5::CmdLineParser::ArgState;
  LTS5::CmdLineParser parser;
  parser.AddArgument("-v", CmdState::kNeeded, "Video file to stream");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Var
    LTS5::OGLTexture* tex = nullptr;
    LTS5::OGLImageRenderer* img_renderer = nullptr;
    LTS5::OGLTextRenderer* text_renderer = nullptr;
    LTS5::OGLBarGraph* bar_graph = nullptr;
    LTS5::OGLGlfwBackend* backend = nullptr;


    // Parse
    std::string video_path;
    parser.HasArgument("-v", &video_path);
    cv::VideoCapture cap;
    cap.open(video_path);
    if (cap.isOpened()) {
      // read first frame -> Get dimension/ frmt
      cv::Mat img;
      cap >> img;
      // Init backend
      backend = new LTS5::OGLGlfwBackend();
      backend->Init(argc, argv, false, false, true);
      CustomCallback callback;
      if (backend->CreateWindows(img.cols, img.rows, false, "Texture streaming")) {
        // Init some opengl
        OGLCmd::GlEnable(OGLCst::kGLBlend);
        OGLCmd::GlBlendFunc(OGLCst::kGLSrcAlpha,OGLCst::kGLOneMinusSrcAlpha);
        backend->SetCallbacks(&callback);
        // Init texture
        tex = new LTS5::OGLTexture(img,
                                   OGLCst::kGLLinear,
                                   OGLCst::kGLClampToEdge);
        tex->Upload(img);
        // Create text renderer
        text_renderer = new LTS5::OGLTextRenderer();
        text_renderer->GenerateAtlas("");
        err = text_renderer->AddText("OpenGL Text Rendering",
                                     20.f,
                                     20.f,
                                     1.f,
                                     Color(128, 204, 51, 204),
                                     -1);
        err = text_renderer->AddText("Texture streaming",
                                     20.f,
                                     80.f,
                                     1.f,
                                     Color(77, 179, 230, 77),
                                     -1);
        err |= text_renderer->BindToOpenGL();
        text_renderer->SetWindowDimension(img.cols, img.rows);
        // Bargraph
        bar_graph = new LTS5::OGLBarGraph();
        bar_graph->SetWindowDimension(img.cols, img.rows);
        bar_graph->SetupView(20, 200, 200, 400, 2);
        std::vector<std::string> label = {"Toto", "Titi"};
        bar_graph->SetLabel(label);
        bar_graph->SetBarOrientation(LTS5::OGLBarGraph::BarOrientationType::kHorizontal);
        bar_graph->SetColor(Color(255, 153, 77, 255));
        bar_graph->BindToOpenGL();


        // Create image renderer
        img_renderer = new LTS5::OGLImageRenderer();
        img_renderer->AddTexture(tex);
        err |= img_renderer->BindToOpenGL();
        img_renderer->SetWindowDimension(img.cols, img.rows);
        img_renderer->SetupView(0, 0, img.cols, img.rows);


        unsigned int perc = 0;
        if (!err) {
          bool process = true;
          while(process && cap.isOpened()) {
            // Clear opengl
            OGLCmd::GlClearColor(0.f, 0.f, 0.f, 1.f);
            OGLCmd::GlClear(OGLCst::kGLColorBufferBit | OGLCst::kGLDepthBufferBit);
            float bar_1 = std::fmod(perc / 100.f, 1.f);
            float bar_2 = std::fmod(perc / 50.f, 1.f);
            std::vector<float> val = {bar_1, bar_2};
            bar_graph->SetValue(val);
            perc += 1;


            // Read
            cap >> img;
            // Process
            // Update texture
            tex->Upload(img);
            // Render
            img_renderer->Render();
            // Render text
            text_renderer->Render();
            // BarGraph
            bar_graph->Render();
            // Swap buffer
            backend->SwapBuffer();
            // Poll event
            backend->PollEvents();
            if (callback.quit_) {
              break;
            }
          }
        } else {
          std::cout << "Error while creating renderers" << std::endl;
        }
      } else {
        std::cout << "Unable to create OpenGL Window" << std::endl;
        err = -1;
      }
    } else {
      std::cout << "Unable to open video : " << video_path << std::endl;
    }
    // Clean up
    if (cap.isOpened()) {
      cap.release();
    }
    if (img_renderer) {
      delete img_renderer;
      img_renderer = nullptr;
    }
    if (text_renderer) {
      delete text_renderer;
      text_renderer = nullptr;
    }
    if (tex) {
      delete tex;
      tex = nullptr;
    }
    if (bar_graph) {
      delete bar_graph;
      bar_graph = nullptr;
    }
    if (backend) {
      delete backend;
      backend = nullptr;
    }
  } else {
    std::cout << "Unable to parse command line !" << std::endl;
  }
  return err;
}
