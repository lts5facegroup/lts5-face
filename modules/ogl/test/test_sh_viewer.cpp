/**
 *  @file   test_sh_viewer.cpp
 *  @brief  SHView demo / test
 *  @ingroup ogl
 *
 *  @author Christophe Ecabert
 *  @date   9/28/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/ogl/sh_viewer.hpp"
#include "lts5/utils/logger.hpp"

int main(int argc, const char * argv[]) {
  using T = float;
  using Color = LTS5::Vector3<T>;

  // Viewer
  LTS5::SHViewer<float> sh_viewer(200, 200, 50);
  auto n_pts = static_cast<int>(sh_viewer.dims());

  // Reflectance
  Color base = {T(32.0) / T(255.0), T(255.0) / T(255.0), T(213.0) / T(255.0)};
  cv::Mat aldebo(n_pts, 3, CV_32FC1);
  for (int k = 0; k < n_pts; ++k) {
    aldebo.at<T>(k, 0) = base.r_;
    aldebo.at<T>(k, 1) = base.g_;
    aldebo.at<T>(k, 2) = base.b_;
  }

  // Go through all elements of the first 3 bands.
  for (int k = 0; k < 9; ++k) {
    // Sh coef
    cv::Mat sh_coef = cv::Mat::zeros(3, 9, CV_32FC1);
    if (k == 0) {
      sh_coef.at<T>(0, k) = T(1.0);
      sh_coef.at<T>(1, k) = T(1.0);
      sh_coef.at<T>(2, k) = T(1.0);
    } else if (k > 3) {
      sh_coef.at<T>(0, k) = T(3.0);
      sh_coef.at<T>(1, k) = T(3.0);
      sh_coef.at<T>(2, k) = T(3.0);
    } else {
      sh_coef.at<T>(0, k) = T(2.0);
      sh_coef.at<T>(1, k) = T(2.0);
      sh_coef.at<T>(2, k) = T(2.0);
    }
    // Generator
    cv::Mat img;
    int err = sh_viewer.Generate(aldebo, sh_coef, &img);
    cv::cvtColor(img, img, cv::COLOR_RGB2BGR);

    LTS5_LOG_INFO("Spherical Harmonic coefficient: " << k);
    LTS5_LOG_INFO("Press any key to continue");
    cv::imshow("sh", img);
    cv::waitKey();
  }
}