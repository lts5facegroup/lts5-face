/**
 *  @file   test_ogl_interpolation.cpp
 *  @brief  Test for barycentric interpolation with OpenGL Shader
 *
 *  @author Christophe Ecabert
 *  @date   12/09/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/ogl/offscreen_renderer.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/utils/file_io.hpp"

#define WINDOWS_SIZE 512

/** Vertex shader */
const std::string vs_str = "#version 330\n"
        "layout (location = 0) in vec3 position;\n"
        "layout (location = 1) in vec2 tcoord;\n"
        "uniform mat4 camera;\n"
        "uniform mat4 model;\n"
        "out VS_OUT {\n"
        "  flat vec2 tcoord0;\n"
        "} vs_out;\n"
        "void main() {\n"
        "  gl_Position = camera * model * vec4(position, 1);\n"
        "  vs_out.tcoord0 = tcoord;\n"
        "}";
/** Geometry shader - https://stackoverflow.com/a/25508724/4546884 */
const std::string gs_str = "#version 330\n"
        "layout (triangles) in;\n"
        "layout (triangle_strip, max_vertices = 3) out;\n"
        "in VS_OUT {\n"
        "  flat vec2 tcoord0;\n"
        "} gs_in[];\n"
        "flat out vec2 tcoord1;\n"
        "out vec3 bary0;\n"
        "void main() {\n"
        "  for (int i = 0; i < 3; ++i) {\n"
        "    gl_Position = gl_in[i].gl_Position;\n"
        "    tcoord1 = gs_in[i].tcoord0;\n"
        "    bary0 = vec3(0.f);\n"
        "    bary0[i] = 1.f;\n"
        "    EmitVertex();\n"
        "  }\n"
        "  EndPrimitive();\n"
        "}";
/** Fragment shader */
const std::string fs_str = "#version 330\n"
        "flat in vec2 tcoord1;\n"
        "in vec3 bary0;\n"
        "out vec4 fragColor;\n"
        "void main() {\n"
        "  fragColor = vec4(tcoord1.x, bary0.x, bary0.y, bary0.z);\n"
        //"  fragColor = vec4(1, 1, 1, 1);\n"
        "}";

template<typename T>
using Mesh = LTS5::Mesh<T, LTS5::Vector3<T>>;

template<typename T>
void ConvertMesh(Mesh<T>* m) {
  // Types
  using Vertex = typename Mesh<T>::Vertex;
  using TCoord = typename Mesh<T>::TCoord;
  using Tri = typename Mesh<T>::Triangle;
  // Original data
  auto vert = m->get_vertex();
  auto tri = m->get_triangle();

  // Shuffle vertex / tri
  auto n_tri = tri.size();
  std::vector<Vertex> new_v(3 * n_tri);
  std::vector<TCoord> new_tc(3 * n_tri);
  std::vector<Tri> new_tri(n_tri);
  for (auto k = 0; k < n_tri; ++k) {
    const auto& t = tri[k];
    // shuffle vertex
    new_v[(3 * k) + 0] = vert[t.x_];
    new_v[(3 * k) + 1] = vert[t.y_];
    new_v[(3 * k) + 2] = vert[t.z_];
    // set tcoords to triangle index
    TCoord t_idx(k, k);
    new_tc[(3 * k) + 0] = t_idx;
    new_tc[(3 * k) + 1] = t_idx;
    new_tc[(3 * k) + 2] = t_idx;
    // Set triangle
    auto base_t = static_cast<int>(3 * k);
    Tri new_t(base_t, base_t + 1, base_t + 2);
    new_tri[k] = new_t;
  }
  // Reset vertex/tri/tcoord
  m->set_vertex(new_v);
  m->set_tex_coord(new_tc);
  m->set_triangle(new_tri);
}

int main(int argc, const char * argv[]) {
  // Parser
  using T = float;
  using Att = LTS5::OGLProgram::Attributes;
  using AttTyp = LTS5::OGLProgram::AttributeType;
  using Vec3f = LTS5::Vector3<float>;
  using Mat4f = LTS5::Matrix4x4<float>;

  LTS5::CmdLineParser parser;
  parser.AddArgument("-m",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Input Mesh");

  int err = parser.ParseCmdLine(argc, argv);
  if (err == 0) {
    // Retrieve args
    std::string mesh_file;
    parser.HasArgument("-m", &mesh_file);

    // Load it
    Mesh<T> mesh;
    err = mesh.Load(mesh_file);
    if (err == 0) {
      // Convert mesh from shared topology to splitted
      ConvertMesh<T>(&mesh);
      // Create renderer
      LTS5::OGLOffscreenRenderer<T, LTS5::Vector3<T>> renderer;
      err = renderer.Init(WINDOWS_SIZE,
                          WINDOWS_SIZE,
                          0,
                          true,
                          LTS5::OGLOffscreenRenderer<T, LTS5::Vector3<T>>::ImageType::kRGBAf);
      if (err == 0) {
        // Create shaders (Vertex, Geometry, Fragment)
        using ShaderType = typename LTS5::OGLShader::ShaderType;
        std::vector<LTS5::OGLShader> shaders;
        shaders.emplace_back(LTS5::OGLShader(vs_str, ShaderType::kVertex));
        shaders.emplace_back(LTS5::OGLShader(gs_str, ShaderType::kGeometry));
        shaders.emplace_back(LTS5::OGLShader(fs_str, ShaderType::kFragment));
        std::vector<Att> attrib;
        attrib.emplace_back(AttTyp::kPoint,  0, "position");
        attrib.emplace_back(AttTyp::kTexCoord, 1, "tcoord");
        LTS5::OGLProgram program(shaders, attrib);
        // Create camera
        LTS5::OGLCamera<T> cam(WINDOWS_SIZE, WINDOWS_SIZE);
        cam.set_planes(1e1, 1e5);
        const auto& bbox = mesh.bbox();
        const auto& center = bbox.center_;
        const auto wz = (bbox.max_.z_ - bbox.min_.z_) * 3.f;
        cam.LookAt(Vec3f(center.x_, center.y_, center.z_ + wz),
                   Vec3f(center.x_, center.y_, center.z_));
        Mat4f transform = Mat4f().Identity();

        // Set shader's input
        program.Use();
        program.SetUniform("camera", cam.projection() * cam.view());
        program.SetUniform("model", transform);
        program.StopUsing();

        // Add mesh, program + Bind to opengl
        renderer.AddMesh(mesh);
        renderer.AddProgram(program);
        err = renderer.BindToOpenGL();
        if (err == 0) {
          // Render / Interpolate
          renderer.Render();
          // Get output
          cv::Mat interpolation, depth;
          renderer.GetImage(&interpolation);
          renderer.GetDepth(&depth);

          cv::flip(interpolation, interpolation, 0);
          cv::flip(depth, depth, 0);

          std::vector<cv::Mat> chans;
          cv::split(interpolation, chans);

          LTS5::SaveMatToBin("dbg/tri.bin", chans[0]);
          LTS5::SaveMatToBin("dbg/a.bin", chans[1]);
          LTS5::SaveMatToBin("dbg/b.bin", chans[2]);
          LTS5::SaveMatToBin("dbg/c.bin", chans[3]);
          LTS5::SaveMatToBin("dbg/depth.bin", depth);

          int a = 0;

        } else {
          LTS5_LOG_ERROR("Can not bind to OpenGL Context");
        }
      } else {
        LTS5_LOG_ERROR("Can not initialized OpenGL Context");
      }
    } else {
      LTS5_LOG_ERROR("Can not load mesh : " << mesh_file);
    }
  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
  return err;
}