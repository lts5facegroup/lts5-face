/**
 *  @file   test_ogl.cpp
 *  @brief  Testing target for ogl module
 *  @author Christophe Ecabert
 *  @date   13/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>
#include <iostream>
#include <vector>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/math/matrix.hpp"
#include "lts5/utils/math/quaternion.hpp"
#include "lts5/geometry/octree.hpp"
#include "lts5/ogl/renderer.hpp"
#include "lts5/ogl/wired_renderer.hpp"
#include "lts5/ogl/glfw_backend.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/ogl/command.hpp"



static LTS5::OGLGlfwBackend* ogl_backend;
static std::string mesh_path;
static std::string vertex_shader_path;
static std::string fragment_shader_path;
static LTS5::Matrix4x4<float> transform_;

using ProgAtt = LTS5::OGLProgram::Attributes;
using ProgTyp = LTS5::OGLProgram::AttributeType;
using OGLCmd = LTS5::OGLCommand;
using OGLCst = LTS5::OGLCommand::Constant;

struct App : public LTS5::OGLCallback {
  LTS5::OGLProgram* program_;
  LTS5::OGLCamera<float>* cam_;
  LTS5::Mesh<float>* mesh_;
  LTS5::OGLRenderer<float>* renderer_;
  LTS5::OCTree<float>* tree_;
  LTS5::OGLWiredRenderer<float>* tree_renderer_;

  App(void) : program_(nullptr),
              cam_(nullptr),
              mesh_(nullptr),
              tree_(nullptr),
              tree_renderer_(nullptr){
  }

  ~App() {
    // Clean up
    delete program_;
    delete cam_;
    delete mesh_;
    delete renderer_;
    delete tree_;
    delete tree_renderer_;

  }

  void Init(void) {
    // Load shader
    using ShaderType = typename LTS5::OGLShader::ShaderType;
    using Vec3f = LTS5::Vector3<float>;
    using Quat = LTS5::Quaternion<float>;
    std::vector<LTS5::OGLShader> shader;
    shader.emplace_back(LTS5::OGLShader::OGLShaderFromFile(vertex_shader_path,
                                                           ShaderType::kVertex));
    shader.emplace_back(LTS5::OGLShader::OGLShaderFromFile(fragment_shader_path,
                                                           ShaderType::kFragment));
    // Create program
    std::vector<ProgAtt> attrib_list;
    attrib_list.push_back(ProgAtt(ProgTyp::kPoint, 0, "position"));
    attrib_list.push_back(ProgAtt(ProgTyp::kNormal, 1, "normal"));
    program_ = new LTS5::OGLProgram(shader, attrib_list);
    // Create mesh
    mesh_ = new LTS5::Mesh<float>();
    int err = mesh_->Load(mesh_path);
    mesh_->NormalizeMesh();
    mesh_->ComputeVertexNormal();
    // Create Cam
    cam_ = new LTS5::OGLCamera<float>(1280, 1024);
    cam_->LookAt(mesh_->bbox());

//    const auto& center = mesh_->bbox().center_;
//    cam_->LookAt(Vec3f(center.x_, center.y_, center.z_ + 1.5f),
//                 Vec3f(center.x_, center.y_, center.z_));
    // Define transformation, set identity
    Quat q(Vec3f(0, 1, 0), 0.785398f);
    q.ToRotationMatrix(&transform_);
    // Create renderer
    renderer_ = new LTS5::OGLRenderer<float>();
    renderer_->AddMesh(mesh_);
    renderer_->AddProgram(program_);
    err |= renderer_->BindToOpenGL();
    // Create tree renderer
    tree_renderer_ = new LTS5::OGLWiredRenderer<float>();

    if (!err) {
      program_->Use();
      program_->SetUniform("camera", cam_->matrix());
      program_->SetUniform("model", transform_);
      program_->StopUsing();

      // Create OCTree
      tree_ = new LTS5::OCTree<float>();
      tree_->Insert(*mesh_, LTS5::Mesh<float>::PrimitiveType::kTriangle);
      tree_->Build();
      // Populate wired renderer
      LTS5::OGLWiredRenderer<float>::FillFromOCTree(*tree_, tree_renderer_);

      // Define parallel ray and test for intersection
      const LTS5::Vector3<float> p(0.f, 0.05f, -0.5f);
      const LTS5::Vector3<float> d(-0.05f, 0.f, 1.f);
      std::vector<int> idx;
      tree_->DoIntersect(p, d, &idx);
    }
  }

  void Run(void) {
    ogl_backend->Run(this);
  }

  void OGLKeyboardCb(const LTS5::OGLKey& key, const LTS5::OGLKeyState& state) {
    // Quit
    if ((key == LTS5::OGLKey::kESCAPE) &&
        (state == LTS5::OGLKeyState::kPress)) {
      // Leave
      ogl_backend->LeaveMainLoop();
    }
    // Cam displacement
    cam_->OnKeyboard(key, state);
  }

  void OGLRenderCb(void) {
    // draw one frame
    program_->Use();
    program_->SetUniform("camera", cam_->matrix());
    program_->SetUniform("model", transform_);
    program_->StopUsing();

    OGLCmd::GlClearColor(0.53f, 0.80f, 0.98f, 1); // light blue
    OGLCmd::GlClearColor(0.f, 0.f, 0.f, 1.f);         // Black
    OGLCmd::GlClear(OGLCmd::kGLColorBufferBit| OGLCmd::kGLDepthBufferBit);
    renderer_->Render();
    tree_renderer_->Render(*cam_, transform_);
  }

  void OGLPassiveMouseCb(const int x, const int y) {
    cam_->OnMouseMove(x, y);
  }

  void OGLMouseCb(const LTS5::OGLMouse& button,
                  const LTS5::OGLKeyState& state,
                  const int x,
                  const int y) {
    cam_->OnMouseClick(button, state, x, y);
  }
};


int main(int argc, const char * argv[]) {
  using CmdState = LTS5::CmdLineParser::ArgState;
  LTS5::CmdLineParser parser;
  parser.AddArgument("-m", CmdState::kNeeded, "mesh filename");
  parser.AddArgument("-v", CmdState::kNeeded, "vertex shader");
  parser.AddArgument("-f", CmdState::kNeeded, "fragment shader");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    parser.HasArgument("-m", &mesh_path);
    parser.HasArgument("-v", &vertex_shader_path);
    parser.HasArgument("-f", &fragment_shader_path);

    App* app = new App();
    ogl_backend = new LTS5::OGLGlfwBackend();

    try {

      ogl_backend->Init(argc, argv, true, false, true);
      if (!ogl_backend->CreateWindows(1280,
                                      1024,
                                      false,
                                      "OCTree")) {
        ogl_backend->Terminate();
      } else {
        app->Init();
        app->Run();
      }
    } catch (const std::exception& e){
      std::cerr << "ERROR: " << e.what() << std::endl;
      delete app;
      ogl_backend->Terminate();
      return -1;
    }

    delete app;
    delete ogl_backend;


    return 0;
  } else {
    std::cout << "Unable to parse command line !" << std::endl;
    return -1;
  }

  return 0;
}