/**
 *  @file   test_ogl.cpp
 *  @brief  Testing target for ogl module
 *  @author Christophe Ecabert
 *  @date   13/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <cstdio>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <vector>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/utils/math/matrix.hpp"
#include "lts5/utils/math/quaternion.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/ogl/renderer.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/ogl/glfw_backend.hpp"
#include "lts5/ogl/command.hpp"


// globals
static std::string v_shader_path;
static std::string f_shader_path;
static std::string mesh_path;
static std::string texture_path;
static LTS5::Matrix4x4<float> transform_;
static LTS5::OGLBackend* ogl_backend;

using ProgAtt = LTS5::OGLProgram::Attributes;
using ProgTyp = LTS5::OGLProgram::AttributeType;
using OGLCmd = LTS5::OGLCommand;
using OGLCst = LTS5::OGLCommand::Constant;

struct App : public LTS5::OGLCallback {

  LTS5::OGLProgram* program_;
  LTS5::OGLCamera<float>* cam_;
  std::vector<LTS5::Bitmap> img_;
  LTS5::OGLTexture* tex_;
  LTS5::Mesh<float>* mesh_;
  LTS5::OGLRenderer<float>* renderer_;

  App() : program_(nullptr),
          cam_(nullptr),
          img_(0),
          tex_(nullptr),
          mesh_(nullptr),
          renderer_(nullptr) {
  }

  ~App() override {
    // Clean up
    delete program_;
    delete cam_;
    delete tex_;
    delete mesh_;
    delete renderer_;
  }

  void Init() {
    // Create shaders
    using ShaderType = typename LTS5::OGLShader::ShaderType;
    using Vec3f = LTS5::Vector3<float>;
    using Quat = LTS5::Quaternion<float>;
    std::vector<LTS5::OGLShader> shader;
    shader.emplace_back(LTS5::OGLShader::OGLShaderFromFile(v_shader_path,
                                                           ShaderType::kVertex));
    shader.emplace_back(LTS5::OGLShader::OGLShaderFromFile(f_shader_path,
                                                           ShaderType::kFragment));
    // Create program
    std::vector<ProgAtt> attrib_list;
    attrib_list.emplace_back(ProgTyp::kPoint, 0, "position");
    attrib_list.emplace_back(ProgTyp::kNormal, 1, "normal");
    attrib_list.emplace_back(ProgTyp::kTexCoord, 2, "texCoord");
    program_ = new LTS5::OGLProgram(shader, attrib_list);

    // Load bitmat + Create/upload texture
    if (!texture_path.empty()) {
      std::vector<std::string> texture_path_part;
      LTS5::SplitString(texture_path, ";", &texture_path_part);

      for (int i = 0; i < texture_path_part.size(); ++i) {
        img_.push_back(LTS5::Bitmap::LoadFromFile(texture_path_part[i]));
      }
      tex_ = new LTS5::OGLTexture(img_[0],
                                  OGLCst::kGLLinear,
                                  OGLCst::kGLClampToEdge);
      tex_->Upload(img_[0]);
    }
    // Create mesh
    mesh_ = new LTS5::Mesh<float>();
    int err = mesh_->Load(mesh_path);
    mesh_->ComputeVertexNormal();
    // Create camera
    cam_ = new LTS5::OGLCamera<float>(1280, 1024);
    cam_->LookAt(mesh_->bbox());
    Quat q(Vec3f(1.0, 0.0, 0.0), 0.0);
    q.ToRotationMatrix(&transform_);
    // Create renderer
    renderer_ = new LTS5::OGLRenderer<float>();
    renderer_->AddMesh(mesh_);
    renderer_->AddProgram(program_);
    if (tex_) {
      renderer_->AddTexture(tex_);
    }
    err |= renderer_->BindToOpenGL();

    program_->Use();
    program_->SetUniform("camera", cam_->matrix());
    program_->SetUniform("model", transform_);
    if (tex_) {
      program_->SetUniform("gSampler", 0);
    }
    program_->StopUsing();
  }

  void Run() {
    ogl_backend->Run(this);
  }

  void OGLRenderCb() override {
    // draw one frame
    program_->Use();
    program_->SetUniform("camera", cam_->matrix());
    program_->SetUniform("model", transform_);
    program_->StopUsing();
    OGLCmd::GlClearColor(0.f, 0.f, 0.f, 1.f); // black
    OGLCmd::GlClear(OGLCst::kGLColorBufferBit | OGLCst::kGLDepthBufferBit);
    renderer_->Render();
  }

  void OGLKeyboardCb(const LTS5::OGLKey& key,
                     const LTS5::OGLKeyState& state) override {
    // Hanlde camera displacement
    cam_->OnKeyboard(key, state);
    // Handle app lifecycle
    if ((key == LTS5::OGLKey::kESCAPE) &&
        (state == LTS5::OGLKeyState::kPress)) {
      // Leave
      ogl_backend->LeaveMainLoop();
    }
    // Handle texture switching
    if ((key == LTS5::OGLKey::kT) &&
        (state == LTS5::OGLKeyState::kPress)) {
      static unsigned int cnt = 1;
      // Update texture
      if (img_.size() > 1) {
        tex_->Upload(img_[cnt % img_.size()]);
        cnt++;
      }
    }
  }

  void OGLMouseCb(const LTS5::OGLMouse& button,
                  const LTS5::OGLKeyState& state,
                  const int x,
                  const int y) override {
    cam_->OnMouseClick(button, state, x, y);
  }

  void OGLPassiveMouseCb(const int x, const int y) override {
    cam_->OnMouseMove(x, y);
  }
};

int main(int argc, const char * argv[]) {




  using CmdState = LTS5::CmdLineParser::ArgState;
  LTS5::CmdLineParser parser;
  parser.AddArgument("-m", CmdState::kNeeded, "mesh filename");
  parser.AddArgument("-v", CmdState::kNeeded, "vertex shader filename");
  parser.AddArgument("-f", CmdState::kNeeded, "fragment shader filename");
  parser.AddArgument("-t", CmdState::kOptional, "texture filename");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    parser.HasArgument("-m", &mesh_path);
    parser.HasArgument("-v", &v_shader_path);
    parser.HasArgument("-f", &f_shader_path);
    parser.HasArgument("-t", &texture_path);

    App* app = new App();
    ogl_backend = new LTS5::OGLGlfwBackend();
    //ogl_backend = new LTS5::OGLGlutBackend();

    try {
      ogl_backend->Init(argc, argv, true, false, true);
      if (!ogl_backend->CreateWindows(1280,
                                      1024,
                                      false,
                                      "OpenGL windows")) {
        ogl_backend->Terminate();
      } else {
        app->Init();
        app->Run();
      }
    } catch (const std::exception& e){
      LTS5_LOG_ERROR(e.what());
      delete app;
      ogl_backend->Terminate();
      return -1;
    }

    delete app;
    delete ogl_backend;


    return 0;
  } else {
    std::cout << "Unable to parse command line !" << std::endl;
    return -1;
  }
}
