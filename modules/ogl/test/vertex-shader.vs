#version 330

// Input data
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;

// uniform input -> matrix
uniform mat4 camera;
uniform mat4 model;

// output
out vec2 texCoord0;
out vec3 normal0;

void main() {
  // Apply transformation onto points
  gl_Position = camera * model * vec4(position, 1);
  // Transfer normal
  normal0 = (model * vec4(normal, 0.0)).xyz;
  // transfert TCoord
  texCoord0 = texCoord;
}