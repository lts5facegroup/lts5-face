/**
 *  @file   test_rasterizer.cpp
 *  @brief  Example for surface rasterization
 *  @ingroup    ogl
 *
 *  @author Christophe Ecabert
 *  @date   7/9/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#include "opencv2/highgui.hpp"

#include "lts5/ogl/rasterizer.hpp"

#include "lts5/ogl/offscreen_renderer.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/rodrigues.hpp"

using namespace LTS5;

int main(int argc, const char * argv[]) {

  using Mat4 = Matrix4x4<float>;
  using Mesh_t = Mesh<float>;


#if 1
  OGLOffscreenRenderer<float> renderer;
  auto er = renderer.Init(512,
                          512,
                          0,
                          false,
                          OGLOffscreenRenderer<float>::ImageType::kRGBAf);
  renderer.ActiveContext();

  // Shader
  std::string vs = "#version 330\n"
                   "layout (location = 0) in vec3 position;\n"
                   "uniform mat4 camera;\n"
                   "void main() {\n"
                   " gl_Position = camera * vec4(position, 1);\n"
                   "}";

  std::string gs = "#version 330\n"
                   "layout (triangles) in;\n"
                   "layout (triangle_strip, max_vertices = 3) out;\n"
                   "in int gl_PrimitiveIDIn;\n"
                   "out vec2 bcoords;\n"
                   "out float tri_idx;\n"
                   "void main() {\n"
                   "  for (int i = 0; i < 3; ++i) {\n"
                   "    gl_Position = gl_in[i].gl_Position;\n"
                   "    bcoords = vec2(i==0 ? 1.f : 0.f, i==1 ? 1.f : 0.f);\n"
                   "    tri_idx = gl_PrimitiveIDIn;\n"
                   "    EmitVertex();\n"
                   "  }\n"
                   "  EndPrimitive();\n"
                   "}";

  std::string fs = "#version 330\n"
                   "in vec2 bcoords;\n"
                   "in float tri_idx;\n"
                   "out vec4 fragColor;\n"
                   "void main() {\n"
                   " //fragColor = vec4(vec3(1.0, 1.0, 1.0), 1.0);\n"
                   "  fragColor = vec4(round(tri_idx + 1.0), bcoords, 0.0);\n"
                   "}";

  using ShaderType = typename LTS5::OGLShader::ShaderType;
  using Att = LTS5::OGLProgram::Attributes;
  using AttTyp = LTS5::OGLProgram::AttributeType;
  std::vector<LTS5::OGLShader> shaders;
  shaders.emplace_back(vs, ShaderType::kVertex);
  shaders.emplace_back(gs, ShaderType::kGeometry);
  shaders.emplace_back(fs, ShaderType::kFragment);
  std::vector<Att> attrib;
  attrib.emplace_back(Att(AttTyp::kPoint,  0, "position"));
  OGLProgram program(shaders, attrib);

  auto proj_matrix = Mat4::Perspective(30.f, 1.f, 10.f, 1000.f);
  program.Use();
  program.SetUniform("camera", proj_matrix);
  program.StopUsing();


  // Create mesh
  Mesh_t m;
//  m.get_vertex() = {{-50.f, 50.f, -500.f},   //Top left
//                    {-50.f, -50.f, -500.f},  // Bottom left
//                    {50.f, 50.f, -500.f},    // Top right
//                    {-50.f, -50.f, -500.f},  // Bottom left
//                    {50.f, -50.f, -500.f},   // Bottom right
//                    {50.f, 50.f, -500.f}};   // Top right
//  m.get_triangle() = {{0, 1, 2}, {3, 4, 5}};

  m.get_vertex() = {{-50.f, 50.f, 50.f},   //Top left
                    {-50.f, -50.f, 50.f},  // Bottom left
                    {50.f, 50.f, 50.f},    // Top right
                    {50.f, -50.f, 50.f},    // Bottom right
                    {-50.f, 50.f, -50.f},    //Top left
                    {-50.f, -50.f, -50.f},   // Bottom left
                    {50.f, 50.f, -50.f},     // Top right
                    {50.f, -50.f, -50.f}};   // Bottom right
  m.get_triangle() = {{1, 3, 2},
                      {0, 1, 2},
                      {4, 6, 5},
                      {6, 7, 5},
                      {2, 3, 6},
                      {3, 7, 6},
                      {0, 4, 5},
                      {0, 5, 1}};

  Mat4 rmat;
  Rodrigues<float> rodrigues({0.f, 1.f, 0.f}, 0.785398f);
  rodrigues.ToRotationMatrix(&rmat);

  cv::Mat Rmat = cv::Mat::zeros(4, 4, CV_32FC1);
  Rmat.at<float>(0, 0) = rmat(0, 0);
  Rmat.at<float>(0, 1) = rmat(0, 1);
  Rmat.at<float>(0, 2) = rmat(0, 2);
  Rmat.at<float>(1, 0) = rmat(1, 0);
  Rmat.at<float>(1, 1) = rmat(1, 1);
  Rmat.at<float>(1, 2) = rmat(1, 2);
  Rmat.at<float>(2, 0) = rmat(2, 0);
  Rmat.at<float>(2, 1) = rmat(2, 1);
  Rmat.at<float>(2, 2) = rmat(2, 2);
  Rmat.at<float>(2, 3) = -500.f;
  Rmat.at<float>(3, 3) = 1.f;
  std::cout << Rmat << std::endl;
  m.Transform(Rmat);

  // Bind
  renderer.AddMesh(m);
  renderer.AddProgram(program);
  er |= renderer.BindToOpenGL();

  cv::Mat img;
  renderer.Render();
  renderer.GetImage(&img);
  cv::flip(img, img, 0);

  cv::Mat im = img.reshape(1);
  SaveMatToBin("dbg/interp.bin", im);

  renderer.DisableContext();

//  cv::imshow("Image", img);
//  cv::waitKey(0);

  return 0;

#else


  // Vertex shader -> do nothing
  std::string v_str =
          "#version 430\n"
          "void main() {}";
  // Geometry shader
  std::string g_str =
          "#version 430\n"
          "layout(points) in;\n"
          "layout(triangle_strip, max_vertices=3) out;\n"
          "in int gl_PrimitiveIDIn;\n"
          "out layout(location = 0) vec2 bcoords;\n"
          "out layout(location = 1) float tri_idx;\n"
          "layout(binding=0) buffer tri_mesh { float vertex_buff[];};\n"
          "uniform mat4 proj_mat;\n"
          "\n"
          "vec3 get_vertex_pos(int v_index) {\n"
          "  // Tri are packed as 3 consecutive vertices, each with 3 elements.\n"
          "  int offset = (gl_PrimitiveIDIn * 9) + v_index * 3;\n"
          "  return vec3(vertex_buff[offset], \n"
          "              vertex_buff[offset + 1],\n"
          "              vertex_buff[offset + 2]);\n"
          "}\n"
          "\n"
          "// Note that this function can cause artifacts for triangles that cross the eye\n"
          "// plane.\n"
          "bool is_back_facing(vec4 proj_v0, vec4 proj_v1, vec4 proj_v2) {\n"
          "  proj_v0 /= proj_v0.w;\n"
          "  proj_v1 /= proj_v1.w;\n"
          "  proj_v2 /= proj_v2.w;\n"
          "  vec2 a = (proj_v1.xy - proj_v0.xy);\n"
          "  vec2 b = (proj_v2.xy - proj_v0.xy);\n"
          "  return (a.x * b.y - b.x * a.y) <= 0;\n"
          "}\n"
          "\n"
          "void main() {\n"
          "  vec3 pos[3] = {get_vertex_pos(0),\n"
          "                 get_vertex_pos(1),\n"
          "                 get_vertex_pos(2)};\n"
          "  vec4 proj_v[3] = {proj_mat * vec4(pos[0], 1.0),\n"
          "                    proj_mat * vec4(pos[1], 1.0),\n"
          "                    proj_mat * vec4(pos[2], 1.0)};\n"
          "  // Cull back-facing triangles.\n"
          "  //if (is_back_facing(proj_v[0], proj_v[1], proj_v[2])) {\n"
          "  //  return;\n"
          "  //}\n"
          "  for (int i = 0; i < 3; ++i) {\n"
          "    gl_Position = proj_v[i];\n"
          "    bcoords = vec2(i==0 ? 1.0 : 0.0, i==1 ? 1.0 : 0.0);\n"
          "    tri_idx = gl_PrimitiveIDIn;\n"
          "    EmitVertex();\n"
          "  }\n"
          "  EndPrimitive();\n"
          "}";

  std::string f_str =
          "#version 430\n"
          "in layout(location = 0) vec2 bcoords;\n"
          "in layout(location = 1) float tri_idx;\n"
          "out vec4 output_color;\n"
          "void main() {\n"
          "  //output_color = vec4(round(tri_idx + 1.0), bcoords, 0.0);\n"
          "  output_color = vec4(1.0, 0.0, 0.0, 1.0);\n"
          "}"
          "";


  std::vector<float> vertices = {-50.f, 50.f, -500.f,   //Top left
                                 -50.f, -50.f, -500.f,  // Bottom left
                                 50.f, 50.f, -500.f};//,    // Top right
//                                 -50.f, -50.f, -500.f,  // Bottom left
//                                 50.f, -50.f, -500.f,   // Bottom right
//                                 50.f, 50.f, -500.f};   // Top right

   auto proj_matrix = Mat4::Perspective(30.f, 1.f, 10.f, 1000.f);


  // Create OpenGL context
  Rasterizer rasterizer(512,
                        512,
                        v_str,
                        g_str,
                        f_str, 0.f, 1.f, 0.f, 1.f);
  rasterizer.Enable();

  int err = 0.0;//rasterizer.SetUniform("proj_mat", proj_matrix);
  err |= rasterizer.SetShaderStorage("tri_mesh",
                                        vertices.data(),
                                        vertices.size() * sizeof(float));


  err |= rasterizer.Render(/*vertices.size()*/1, 0);

  cv::Mat image;
  err |= rasterizer.GetImage(&image);

  rasterizer.Disable();
  cv::imshow("Image", image);
  cv::waitKey(0);


  int a = 0;


  return err;
#endif
}
