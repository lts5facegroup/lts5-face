/**
 *  @file   test_au_analysis.cpp
 *  @brief  Testing target for AU analysis
 *
 *  @author Christophe Ecabert
 *  @date   13/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>
#include <iostream>
#include <lts5/face_tracker/sdm_tracker.hpp>
#include <lts5/facial_analysis/au_analysis.hpp>

#include "lts5/utils/cmd_parser.hpp"

int main(int argc, const char * argv[]) {
  int error = -1;
  // Define command line
  LTS5::CmdLineParser parser;
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Tracker model");
  parser.AddArgument("-f",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face detector model");
  parser.AddArgument("-a",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "AU Analysis model");
  parser.AddArgument("-d",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Data root folder");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Analysis result filename");
  error = parser.ParseCmdLine(argc, argv);
  if (!error) {
    // Get command values
    std::string tracker_path;
    std::string f_detect_path;
    std::string au_path;
    std::string data_path;
    std::string result_path;
    parser.HasArgument("-t", &tracker_path);
    parser.HasArgument("-f", &f_detect_path);
    parser.HasArgument("-a", &au_path);
    parser.HasArgument("-d", &data_path);
    parser.HasArgument("-o", &result_path);
    // Init tracker + au_analysis
    LTS5::SdmTracker* tracker = new LTS5::SdmTracker();
    error = tracker->Load(tracker_path, f_detect_path);
    LTS5::AUAnalysis* au_analysis = new LTS5::AUAnalysis();
    error |= au_analysis->Load(au_path);
    if (!error) {
      // Everything is loaded




    } else {
      std::cout << "Unable to load tracker/au_analysis module ..." << std::endl;
    }

  } else {
    std::cout << "Unable to parse command line" << std::endl;
  }
  return error;
}