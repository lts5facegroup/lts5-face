/**
 *  @file   test_expression_detector.cpp
 *  @brief  Testing target for emotion detector
 *  @author Christophe Ecabert
 *  @date   23/04/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/classifier/binarylinearsvmclassifier.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/facial_analysis/facial_analysis.hpp"
#include "lts5/utils/file_io.hpp"

int main(int argc, const char * argv[]) {

  if (argc < 5) {
    std::cout << "Usage : \n" << argv[0] << " FaceTracker Haar ExpressionAnalysis image" << std::endl;
    return -1;
  }

#if 0
  //Convert all emotion detector into centralized binary one.
  int error = -1;

  std::vector<std::string> detectors_filename;
  std::vector<std::string> emotion_labels;

  // Anger
  detectors_filename.push_back("anger.svm");
  emotion_labels.push_back("Anger");
  // Disgust
  detectors_filename.push_back("disgust.svm");
  emotion_labels.push_back("Disgust");
  // Fear
  detectors_filename.push_back("fear.svm");
  emotion_labels.push_back("Fear");
  // Happy
  detectors_filename.push_back("happy.svm");
  emotion_labels.push_back("Happy");
  // Neutral
  detectors_filename.push_back("neutral.svm");
  emotion_labels.push_back("Neutral");
  // Sad
  detectors_filename.push_back("sad.svm");
  emotion_labels.push_back("Sad");
  // Surprise
  detectors_filename.push_back("surprise.svm");
  emotion_labels.push_back("Surprise");

  error = LTS5::ExpressionDetectorTrainer::ConvertModelFromCharToBin(detectors_filename,
                                                                  emotion_labels,
                                                                  argv[1]);

  // Test load specific detector from centralized file
  LTS5::ExpressionDetector* expression_detector = new LTS5::ExpressionDetector();
  error |= expression_detector->Load(argv[1],"Surprise");
  delete expression_detector;

  // Convert ExpressionDetectors collection into ExpressionAnalysis module
  error |= (LTS5::
            ExpressionAnalysisTrainer::
            ConvertModelFromCharToBin<double>("pca_basis.csv",
                                              "mean_neutral.csv",
                                              "stdev_neutral.csv",
                                              argv[1],
                                              "test_expression_analysis"));

  // Convert ExpressionDetectors collection into ExpressionAnalysis module
  error |= (LTS5::
            ExpressionAnalysisTrainer::
            ConvertModelFromCharToBin<double>("pca_basis.csv",
                                              "mean_neutral.csv",
                                              "stdev_neutral.csv",
                                              detectors_filename,
                                              emotion_labels,
                                              "test_expression_analysis_vector"));


  // Load ExpressionAnalysis module
  LTS5::ExpressionAnalysis* expression_analysis = new LTS5::ExpressionAnalysis();
  error |= expression_analysis->Load("test_expression_analysis");
  delete expression_analysis;

  // Load ExpressionAnalysis module
  LTS5::ExpressionAnalysis* expression_analysis_vector = new LTS5::ExpressionAnalysis();
  error |= expression_analysis_vector->Load("test_expression_analysis_vector");
  delete expression_analysis_vector;

  std::vector<std::string> labels;
  labels.push_back("Fear");
  labels.push_back("Surprise");
  LTS5::ExpressionAnalysis* expression_analysis_spec = new LTS5::ExpressionAnalysis();
  error |= expression_analysis_spec->Load("test_expression_analysis", labels);
  delete expression_analysis_spec;
#endif

  // Create face tracker + PDM
  LTS5::SdmTracker* face_tracker = new LTS5::SdmTracker();
  LTS5::PointDistributionModel* pose_normalization = new LTS5::PointDistributionModel();
  std::ifstream input_stream(argv[1],std::ios_base::binary);
  if (!input_stream.is_open()) {
    std::cout << "Unable to open file ..." << std::endl;
    return -1;
  }
  int error = -1;
  int status = LTS5::ScanStreamForObject(input_stream, LTS5::HeaderObjectType::kSdm);
  if (status == 0) {
    error = face_tracker->Load(input_stream,true,argv[2]);
  }
  status = LTS5::ScanStreamForObject(input_stream, LTS5::HeaderObjectType::kPointDistributionModel);
  if (status == 0) {
    error |= pose_normalization->Load(input_stream, true);
  }
  input_stream.close();

  // Create emotion analysis module
  LTS5::ExpressionAnalysis* expression_analysis = new LTS5::ExpressionAnalysis();
  error |= expression_analysis->Load(argv[3]);
  // Loaded properly ?
  if (error == 0) {
    // Read image
    cv::Mat image = cv::imread(argv[4]);
    cv::cvtColor(image, image, CV_BGR2GRAY);

    // Track
    cv::Mat shape;
    status = face_tracker->Track(image, &shape);
    if (face_tracker->is_tracking()) {
      // Tracked
      // Transform to CLM Shape
      int n_point = shape.rows / 2;
      cv::Mat clm_shape = cv::Mat(132,1,CV_64FC1);
      int index_clm = 0;
      int clm_shift = clm_shape.rows / 2;
      for (int i = 0; i < n_point; ++i) {
        if (i == 60 || i == 64) {
          continue;
        }
        clm_shape.at<double>(index_clm) = shape.at<double>(i);
        clm_shape.at<double>(index_clm + clm_shift) = shape.at<double>(i + n_point);
        index_clm++;
      }

      // normalize pose
      cv::Mat normalized_shape;
      cv::Mat normalized_image = pose_normalization->NormSDMImage(image,
                                                                  clm_shape,
                                                                  &normalized_shape);

      // Get inner shape
      cv::Mat inner_shape = cv::Mat(98,1,CV_64FC1);
      int inner_index = 0;
      int inner_shift = inner_shape.rows / 2;
      int n_clm = clm_shape.rows / 2;
      for (int i = 0; i < n_clm; ++i) {
        if (i < 17) {
          continue;
        }
        inner_shape.at<double>(inner_index) = normalized_shape.at<double>(i);
        inner_shape.at<double>(inner_index + inner_shift) = normalized_shape.at<double>(i + clm_shift);
        inner_index++;
      }

      // Extract sift
      int n_inner_shpae = inner_shape.rows / 2;
      std::vector<cv::KeyPoint> keypoints;
      for (int i = 0; i < n_inner_shpae; ++i) {
        int x = std::round(inner_shape.at<double>(i));
        int y = std::round(inner_shape.at<double>(i + n_inner_shpae));
        keypoints.push_back(cv::KeyPoint(x,y,32));
      }
      cv::Mat inner_sift_features;
      LTS5::SSift::SSiftParameters extractor_config;
      LTS5::SSift::ComputeDescriptor(normalized_image,
                                     keypoints,
                                     extractor_config,
                                     &inner_sift_features);
      inner_sift_features = inner_sift_features.reshape(inner_sift_features.channels(), 1);
      LTS5::SaveMatToChar("sift", inner_sift_features);
      inner_sift_features.convertTo(inner_sift_features, CV_64FC1);
      // Prediction
      std::vector<double> scores;
      std::vector<std::string> labels;
      expression_analysis->get_emotion_labels(&labels);
      expression_analysis->Predict(inner_sift_features, &scores);



      for (int i = 0; i < scores.size(); ++i) {
        std::cout << labels[i] << " : " << scores[i] << std::endl;
      }
      int index = (int)std::distance(scores.begin(),
                                     std::max_element(scores.begin(),
                                                      scores.end()));
      std::cout << "Emotion : " << labels[index] << std::endl;


      cv::Mat norm_canvas = face_tracker->draw_shape(normalized_image,
                                                     inner_shape,
                                                     CV_RGB(0, 255, 0));
      cv::imshow("norm_image", norm_canvas);
    }

    // Draw alignement
    cv::Mat canvas = face_tracker->draw_shape(image, shape, CV_RGB(0, 255, 0));
    cv::imshow(argv[4], canvas);
    cv::waitKey();
  }


  delete face_tracker;
  delete pose_normalization;
  delete expression_analysis;

  return 0;
}