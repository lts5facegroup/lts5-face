/**
 *  @file   test_au_analysis_trainer.cpp
 *  @brief  Testing target for au analysis modules
 *  @author Christophe Ecabert
 *  @date   08/04/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#include "opencv2/core/core.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"

#include "lts5/facial_analysis/au_analysis_trainer.hpp"

int main(int argc, const char * argv[]) {
  int error = -1;
  // Define command line
  LTS5::CmdLineParser parser;
  parser.AddArgument("-c",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Configuration file");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Output model name");
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "0 = training, >0 = testing");
  // Parse command line
  error = parser.ParseCmdLine(argc, argv);
  if (!error) {
    // Get command values
    std::string config_path;
    std::string output_name;
    std::string test_mode;
    std::string preproc_mode;
    parser.HasArgument("-c", &config_path);
    parser.HasArgument("-o", &output_name);
    parser.HasArgument("-t", &test_mode);
    parser.HasArgument("-p", &preproc_mode);
    bool is_testing = (std::stoi(test_mode) > 0);

    // Open configuration file
    error = -1;
    cv::FileStorage storage(config_path, cv::FileStorage::READ);
    if (storage.isOpened()) {
      // Ok, can read config
      double retain_var = -1;
      int n_fold = -1;
      std::string sdm_path;
      std::string f_detect_path;
      std::string pdm_path;
      std::string data_path;
      std::string test_path;
      // Get top level node
      cv::FileNode node = storage.getFirstTopLevelNode();
      LTS5::ReadNode(node, "retain_var", &retain_var);
      LTS5::ReadNode(node, "n_fold", &n_fold);
      LTS5::ReadNode(node, "sdm_path", &sdm_path);
      LTS5::ReadNode(node, "f_detect", &f_detect_path);
      LTS5::ReadNode(node, "pdm_path", &pdm_path);
      LTS5::ReadNode(node, "data_path", &data_path);
      LTS5::ReadNode(node, "test_path", &test_path);

      // Deal with test case and train
      if (is_testing) {
        // Define output filename
        std::string result_path;
        size_t pos = output_name.rfind(".");
        if (pos != std::string::npos) {
          result_path = output_name.substr(0, pos) + "_test_result.txt";
        } else {
          result_path = output_name + "_test_result.txt";
        }
        LTS5::SdmTracker* tracker = new LTS5::SdmTracker();
        error = tracker->Load(sdm_path, f_detect_path);
        // Testing
        error |= LTS5::AUAnalysisTrainer::
                 TestExpressionAnalysisModule(output_name,
                                              tracker,
                                              test_path,
                                              result_path);

        // Clean up
        delete tracker;


      } else {
        using AUType = LTS5::AUAnalysisTrainer::AUType;
        // Define Labels
        std::vector<LTS5::AUAnalysisTrainer::AULabel> au_labels;
        au_labels.push_back(std::make_pair(AUType::kAU_01, "AU_01"));
        au_labels.push_back(std::make_pair(AUType::kAU_02, "AU_02"));
        au_labels.push_back(std::make_pair(AUType::kAU_04, "AU_04"));
        au_labels.push_back(std::make_pair(AUType::kAU_06, "AU_06"));
        au_labels.push_back(std::make_pair(AUType::kAU_07, "AU_07"));
        au_labels.push_back(std::make_pair(AUType::kAU_10, "AU_10"));
        au_labels.push_back(std::make_pair(AUType::kAU_12, "AU_12"));
        au_labels.push_back(std::make_pair(AUType::kAU_14, "AU_14"));
        au_labels.push_back(std::make_pair(AUType::kAU_15, "AU_15"));
        au_labels.push_back(std::make_pair(AUType::kAU_17, "AU_17"));
        au_labels.push_back(std::make_pair(AUType::kAU_23, "AU_23"));
        au_labels.push_back(std::make_pair(AUType::kAU_25, "AU_25"));
        au_labels.push_back(std::make_pair(AUType::kAU_28, "AU_28"));
        au_labels.push_back(std::make_pair(AUType::kAU_45, "AU_45"));
        // Define subspace dimension to try
        std::vector<int> subspace_dimension;
        subspace_dimension.push_back(50);
        subspace_dimension.push_back(100);
        subspace_dimension.push_back(200);
        subspace_dimension.push_back(400);
        subspace_dimension.push_back(600);
        subspace_dimension.push_back(800);
        subspace_dimension.push_back(1000);
        // Train stuff
        LTS5::AUAnalysisTrainer* au_trainer = new LTS5::AUAnalysisTrainer();
        error = au_trainer->Load(sdm_path, f_detect_path, pdm_path);
        if (!error) {
          // start tot train
          std::cout << "Start to train AU detectors" << std::endl;
          au_trainer->Train(data_path,
                            retain_var,
                            LTS5::BinaryLinearSVMTraining::L1R_L2LOSS_SVC,
                            LTS5::AUAnalysisTrainer::ScoringType::kF1,
                            au_labels,
                            subspace_dimension);
          // Save model
          au_trainer->Save(output_name);
          // Clean up
          delete au_trainer;
        }
      }
    } else {
      std::cout << "Unable to open configuration file" << std::endl;
    }
  } else {
    std::cout << "Unable to parse command line" << std::endl;
  }
  return error;
}