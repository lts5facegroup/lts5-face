/**
 *  @file   test_expression_analysis_trainer.cpp
 *  @brief  Testing target for emotion analysis trainer
 *  @author Christophe Ecabert
 *  @date   29/06/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>
#include <string>
#include <vector>
#include <utility>

#include "opencv2/highgui/highgui.hpp"

#include "lts5/facial_analysis/expression_analysis_trainer.hpp"
#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"

int main(int argc, const char * argv[]) {
  int error = -1;
  // Define required input
  LTS5::CmdLineParser parser;
  parser.AddArgument("-c",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Configuration file");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Output model name");
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "0 = training, >0 = testing");
  parser.AddArgument("-p",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "0 = skip, >0 = preprocessing, ");
  // Parse command line
  error = parser.ParseCmdLine(argc, argv);
  if (!error) {
    // Get command values
    std::string config_path;
    std::string output_name;
    std::string test_mode;
    std::string preproc_mode;
    parser.HasArgument("-c", &config_path);
    parser.HasArgument("-o", &output_name);
    parser.HasArgument("-t", &test_mode);
    parser.HasArgument("-p", &preproc_mode);
    bool is_testing = (std::stoi(test_mode) > 0);
    bool is_preprocessing = (std::stoi(preproc_mode) > 0);


    // Parse config file
    error = -1;
    cv::FileStorage storage(config_path, cv::FileStorage::READ);
    if (storage.isOpened()) {
      double retain_var = -1;
      int n_fold = -1;
      std::string sdm_path;
      std::string f_detect_path;
      std::string pdm_path;
      std::string data_path;
      std::string feature_path;
      std::string test_path;

      // Get top level node
      cv::FileNode node = storage.getFirstTopLevelNode();
      LTS5::ReadNode(node, "retain_var", &retain_var);
      LTS5::ReadNode(node, "n_fold", &n_fold);
      LTS5::ReadNode(node, "sdm_path", &sdm_path);
      LTS5::ReadNode(node, "f_detect", &f_detect_path);
      LTS5::ReadNode(node, "pdm_path", &pdm_path);
      LTS5::ReadNode(node, "data_path", &data_path);
      LTS5::ReadNode(node, "feature_path", &feature_path);
      LTS5::ReadNode(node, "test_path", &test_path);


      // Is it training or testing ?
      if (is_testing) {
        std::string result_path;
        size_t pos = output_name.rfind(".");
        if (pos != std::string::npos) {
          result_path = output_name.substr(0, pos) + "_test_result.txt";
        } else {
          result_path = output_name + "_test_result.txt";
        }
        LTS5::SdmTracker* tracker = new LTS5::SdmTracker();
        error = tracker->Load(sdm_path, f_detect_path);
        // Testing
        error |= (LTS5::
                 ExpressionAnalysisTrainer::
                 TestExpressionAnalysisModule(output_name,
                                              tracker,
                                              test_path,
                                              result_path));
        delete tracker;

      } else {
        // Define training inputs
        std::vector<std::pair<LTS5::ExpressionAnalysisTrainer::EmotionType,
        std::string>> emotion_label;
        typedef LTS5::ExpressionAnalysisTrainer::EmotionType EmotionList;
        emotion_label.push_back(std::make_pair(EmotionList::kAnger,"Anger"));
        emotion_label.push_back(std::make_pair(EmotionList::kDisgust,"Disgust"));
        emotion_label.push_back(std::make_pair(EmotionList::kFear,"Fear"));
        emotion_label.push_back(std::make_pair(EmotionList::kHappy,"Happy"));
        emotion_label.push_back(std::make_pair(EmotionList::kNeutral,"Neutral"));
        emotion_label.push_back(std::make_pair(EmotionList::kSad,"Sad"));
        emotion_label.push_back(std::make_pair(EmotionList::kSurprise,"Surprise"));

        LTS5::ExpressionAnalysis::ExpressionAnalysisParameters config;
        LTS5::ExpressionAnalysisTrainer* emotion_trainer = new LTS5::ExpressionAnalysisTrainer();
        error = emotion_trainer->Load(sdm_path,
                                      f_detect_path,
                                      pdm_path);
        if (!error) {
          // start tot train
          std::cout << "Module loaded, start procedure." << std::endl;
          if (is_preprocessing) {
            std::cout << "Start to preprocess data ..." << std::endl;
            emotion_trainer->Preprocess(data_path,
                                        feature_path);
          }
          std::cout << "Start to train expression detector" << std::endl;
          emotion_trainer->Train(data_path,
                                 feature_path,
                                 retain_var,
                                 LTS5::BinaryLinearSVMTraining::L1R_L2LOSS_SVC,
                                 LTS5::ExpressionAnalysisTrainer::ScoringType::kAccuracy,
                                 emotion_label);
          emotion_trainer->Save(output_name);
          // Clean up
          delete emotion_trainer;
        }
      }
    } else {
      std::cout << "Unable to open configuration file" << std::endl;
    }
  } else {
    std::cout << "Unable to parse command line" << std::endl;
  }
  return error;
}