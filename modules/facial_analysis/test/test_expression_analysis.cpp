/**
 *  @file   test_expression_analysis.cpp
 *  @brief  Testing target for emotion analysis
 *  @author Christophe Ecabert
 *  @date   27/08/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>
#include <iostream>
#include <vector>
#include <ostream>
#include <limits>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/cmd_parser.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/facial_analysis/expression_analysis.hpp"


int main(int argc, const char * argv[]) {

  using Parser = LTS5::CmdLineParser;

  Parser parser;
  parser.AddArgument("-i",
                     Parser::ArgState::kNeeded,
                     "Input data : folder/image/video");
  parser.AddArgument("-t",
                     Parser::ArgState::kNeeded,
                     "Tracker model");
  parser.AddArgument("-f",
                     Parser::ArgState::kNeeded,
                     "Face detector model");
  parser.AddArgument("-e",
                     Parser::ArgState::kNeeded,
                     "Expression model");
  parser.AddArgument("-o",
                     Parser::ArgState::kOptional,
                     "Output reporting");
  parser.AddArgument("-th",
                     Parser::ArgState::kOptional,
                     "tracker threshold");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Get params
    std::string input_str;
    std::string tracker_model;
    std::string f_detector_model;
    std::string expression_model;
    std::string report_file;
    std::string threshold;
    parser.HasArgument("-i", &input_str);
    parser.HasArgument("-t", &tracker_model);
    parser.HasArgument("-f", &f_detector_model);
    parser.HasArgument("-e", &expression_model);
    parser.HasArgument("-o", &report_file);
    parser.HasArgument("-th", &threshold);

    // Load tracker
    LTS5::SdmTracker* tracker = new LTS5::SdmTracker();
    err = tracker->Load(tracker_model, f_detector_model);
    if (!threshold.empty()) {
      double th = std::stod(threshold);
      tracker->set_score_threshold(th);
    }
    if (!err) {
      // Expression module
      std::vector<std::string> expression_labels;
      LTS5::ExpressionAnalysis* expression_kit;
      expression_kit = new LTS5::ExpressionAnalysis();
      err = expression_kit->Load(expression_model);
      if (!err) {
        // Get label
        expression_kit->get_expression_labels(&expression_labels);
        // Treat input
        // Scan folder or open video
        std::vector<std::string> image_path;
        bool is_dir = input_str[input_str.length() - 1] == '/';
        if (is_dir) {
          image_path = LTS5::ScanImagesInFolder(input_str, true);
        }
        // Reporting
        std::ofstream writer;
        if (!report_file.empty()) {
          writer.open(report_file.c_str());
        }
        // Try to open video
        cv::VideoCapture cap(input_str);
        // Process if something is available
        if (cap.isOpened() || !image_path.empty()) {
          // Dump header, if any
          if (writer.is_open()) {
            writer << "ImagePath,";
            for (int k = 0; k < expression_labels.size(); ++k) {
              writer << expression_labels[k];
              writer << (k == expression_labels.size() - 1 ? "" : ",");
            }
            writer << std::endl;
          }
          // Can process stuff
          cv::Mat image;
          cv::Mat shape;
          std::vector<double> score(expression_labels.size(), 0.0);
          int frame_cnt = 0;
          // Loop
          std::string name;
          while(cap.grab() || frame_cnt < image_path.size()) {
            // Load input
            if (is_dir) {
              name = image_path[frame_cnt];
              std::cout << "Load : " << name << std::endl;
              image = cv::imread(name,cv::IMREAD_UNCHANGED);
            } else {
              name = std::to_string(frame_cnt);
              std::cout << "Load : Frame " << frame_cnt << std::endl;
              cap.retrieve(image);
            }
            frame_cnt++;
            // Convert to grayscale
            if (image.channels() != 1) {
              cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
            }
            // Track
            tracker->Track(image, &shape);
            if (shape.empty()) {
              tracker->Detect(image, &shape);
              std::cout << "RESET " << tracker->get_score() << std::endl;
            }
            if (!shape.empty()) {
              // Predict expression
              expression_kit->Predict(image, shape, &score);
              // Dump result
              std::cout << "\t" << name << " : ";
              auto idx = std::distance(score.begin(),
                                       std::max_element(score.begin(),
                                                        score.end()));
              std::cout << expression_labels[idx] << std::endl;
              if (writer.is_open()) {
                writer << name << ",";
                for (int k = 0; k < score.size(); ++k) {
                  writer << score[k] << (k == score.size() - 1 ? "" : ",");
                }
                writer << std::endl;
              }

              /*cv::Mat canvas = tracker->draw_shape(image, shape, CV_RGB(0, 255, 0));
              auto p = name.rfind(".");
              std::string track_name;
              if (p != std::string::npos) {
                track_name = name.substr(0, p) + "_track.jpg";
              } else {
                track_name = name + "_track.jpg";
              }
              cv::imwrite(track_name, canvas);*/

            } else {
              // Something went wrong, fill nan if reporting
              if (writer.is_open()) {
                writer << name << ",";
                for (int k = 0; k < score.size(); ++k) {
                  writer << std::numeric_limits<double>::quiet_NaN();
                  writer << (k == score.size() - 1 ? "" : ",");
                }
                writer << std::endl;
              }
            }
          }
          std::cout << "Done !" << std::endl;
          if (writer.is_open()) {
            writer.close();
          }
        } else {
          std::cout << "Error, nothing to process !" << std::endl;
          err = -1;
        }
      } else {
        std::cout << "Unable to load expression detector" << std::endl;
      }
      delete expression_kit;
    } else {
      std::cout << "Unable to load face tracker" << std::endl;
    }
    // Clean up
    delete tracker;
  } else {
    std::cout << "Unable to parse command line" << std::endl;
    parser.PrintHelp();
  }
  return err;
}
