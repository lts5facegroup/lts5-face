/**
 *  @file   lts5/facial_analysis.hpp
 *  @brief  Facial analysis classes definition
 *
 *  @author Hua Gao
 *  @date   06/05/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#include "lts5/facial_analysis/facial_analysis.hpp"
