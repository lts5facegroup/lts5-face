/**
 *  @file   au_detector_trainer.hpp
 *  @brief  Train classifier for AU detection
 *  @ingroup facial_analysis
 *
 *  @author Christophe Ecabert
 *  @date   08/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_AU_DETECTOR_TRAINER__
#define __LTS5_AU_DETECTOR_TRAINER__

#include <string>
#include <ostream>

#include "lts5/utils/library_export.hpp"
#include "lts5/classifier/binarylinearsvmclassifier.hpp"
#include "lts5/classifier/base_scoring_metric.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   AUDetectorTrainer
 * @brief   Train classifier for AU detection
 * @author  Christophe Ecabert
 * @date    08/04/16
 * @ingroup facial_analysis
 */
class LTS5_EXPORTS AUDetectorTrainer {
 public:
  /** Type of scoring */
  using ScoringType = LTS5::BaseScoringMetric::ScoringType;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  AUDetectorTrainer
   * @fn  AUDetectorTrainer()
   * @brief Constructor
   */
  AUDetectorTrainer();

  /**
   *  @name   AUDetectorTrainer
   *  @fn AUDetectorTrainer(const std::string& au_label)
   *  @brief  Constructor
   *  @param[in]  au_label Name of the AU detected
   */
  AUDetectorTrainer(const std::string& au_label);

  /**
   * @name  ~AUDetectorTrainer
   * @fn  ~AUDetectorTrainer()
   * @brief Destructor
   */
  ~AUDetectorTrainer();


#pragma mark -
#pragma mark Train

  /**
   *  @name   Train
   *  @fn void Train(const cv::Mat& samples,
             const std::vector<int>& subspace_dim,
             const cv::Mat& labels,
             const BinaryLinearSVMTraining& svm_type,
             const ScoringType scoring_type)
   *  @brief  Train emotion class doing cross validation on sample to find
   *          best C parameter
   *  @param[in]  samples         Samples for training
   *  @param[in]  subspace_dim    List of subspace dimension
   *  @param[in]  labels          Labels for training, format [1xN] or [Nx1]
   *                              with {-1,1} for negative and positive sample
   *                              (flt32).
   *  @param[in]  svm_type  Type of svm
   *  @param[in]  scoring_type  Type of metric to use for scoring performance
   */
  void Train(const cv::Mat& samples,
             const std::vector<int>& subspace_dim,
             const cv::Mat& labels,
             const BinaryLinearSVMTraining& svm_type,
             const ScoringType scoring_type);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& output_filename) const
   *  @brief  Save Detector model into file
   *  @param[in] output_filename File to save the model
   */
  int Save(const std::string& output_filename) const;

  /**
   *  @name   Save
   *  @fn int Save(std::ofstream& out_stream) const
   *  @brief  Save Detector model into binary stream
   *  @param[in] out_stream Binary stream where to save the model
   */
  int Save(std::ofstream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize() const
   *  @brief  Compute the object size in bytes
   *  @return Object memory size
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_au_label
   *  @fn void set_au_label(const std::string& au_label)
   *  @brief  Define the current label for this detector
   *  @param[in]  au_label   AU label
   */
  void set_au_label(const std::string& au_label) {
   au_label_ = au_label;
  }

  /**
   *  @name   get_au_label
   *  @fn const std::string& get_au_label() const
   *  @brief  Provide the label for this detector
   *  @return AU label
   */
  const std::string& get_au_label() const {
    return au_label_;
  }

 private :
  /** AU label */
  std::string au_label_;
  /** Classifier */
  LTS5::BinaryLinearSVMClassifier* classifier_;
};

}  // namepsace LTS5
#endif //__LTS5_AU_DETECTOR_TRAINER__
