/**
 *  @file   au_detector.hpp
 *  @brief  Detection of action unit
 *  @ingroup facial_analysis
 *
 *  @author Christophe Ecabert
 *  @date   11/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_AU_DETECTOR__
#define __LTS5_AU_DETECTOR__

#include <string>
#include <istream>

#include "lts5/utils/library_export.hpp"
#include "lts5/classifier/binarylinearsvmclassifier.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  AUDetector
 *  @brief  Detection of action unit
 *  @author Christophe Ecabert
 *  @date   11/04/16
 *  @ingroup facial_analysis
 */
class LTS5_EXPORTS AUDetector {

 public:

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   AUDetector
   *  @fn AUDetector()
   *  @brief  Constructor
   */
  AUDetector();

  /**
   *  @name   ~AUDetector
   *  @fn ~AUDetector()
   *  @brief  Destructor
   */
  ~AUDetector();

  /**
   *  @name   Load
   *  @fn int Load(const std::string& filename)
   *  @brief  Load saved SVM model
   *  @param[in] filename File to load the model from
   *  @return -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   *  @name   Load
   *  @fn int Load(std::ifstream& input_stream)
   *  @brief  Load saved SVM model
   *  @param[in] input_stream Binary stream to load the model from
   *  @return -1 if error, 0 otherwise
   */
  int Load(std::ifstream& input_stream);

  /**
   *  @name   Write
   *  @fn int Write(std::ofstream& output_stream) const
   *  @brief  Dump object into the given binary stream
   *  @param[in] output_stream  Output stream to file
   *  @return -1 if error, 0 otherwise
   */
  int Write(std::ofstream& output_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the object size in bytes
   *  @return Object memory size
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Predict
   *  @fn double Predict(const cv::Mat& features) const
   *  @brief  Detect AU from a given set of features (vector)
   *  @param[in] features Sample to classify
   *  @return emotion class
   */
  double Predict(const cv::Mat& features) const;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   get_au_label
   *  @fn const std::string& get_au_label(void) const
   *  @brief  Give AU detected by this detector
   *  @return AU label
   */
  const std::string& get_au_label(void) const {
    return this->au_label_;
  }

private :
  /** Linear classifier */
  BinaryLinearSVMClassifier* classifier_;
  /** AU label */
  std::string au_label_;

};

}  // namepsace LTS5

#endif //__LTS5_AU_DETECTOR__
