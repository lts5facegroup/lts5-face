/**
 *  @file   lts5/facial_analysis/facial_analysis.hpp
 *  @brief  Facial analysis classes definition
 *
 *  @author Hua Gao
 *  @date   06/05/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#ifndef __LTS5_FACIAL_ANALYSIS__
#define __LTS5_FACIAL_ANALYSIS__

/** Emotion detection */
#include "lts5/facial_analysis/expression_detector.hpp"
/** Emotion analysis */
#include "lts5/facial_analysis/expression_analysis.hpp"
/** AU detection */
#include "lts5/facial_analysis/au_detector.hpp"
/** AU Analysis */
#include "lts5/facial_analysis/au_analysis.hpp"

#endif  /* __LTS5_FACIAL_ANALYSIS__ */