/**
 *  @file   expression_detector_trainer.hpp
 *  @brief  Train model for emotion detection class definition
 *  @ingroup facial_analysis
 *
 *  @author Christophe Ecabert
 *  @date   22/06/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_EXPRESSION_DETECTOR_TRAINER__
#define __LTS5_EXPRESSION_DETECTOR_TRAINER__

#include <ostream>
#include <string>

#include "lts5/utils/library_export.hpp"
#include "lts5/classifier/binarylinearsvmclassifier.hpp"
#include "lts5/classifier/base_scoring_metric.hpp"
#include "lts5/facial_analysis/expression_detector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  ExpressionDetectorTrainer
 *  @brief  Train model for detection of seven basic facial emotions
 *  @author Christophe Ecabert
 *  @date   22/06/15
 *  @ingroup facial_analysis
 */
class LTS5_EXPORTS ExpressionDetectorTrainer {
 public:
  /** Classification scoring metric */
  using ScoringType = BaseScoringMetric::ScoringType;

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   ExpressionDetectorTrainer
   *  @fn ExpressionDetectorTrainer()
   *  @brief  Constructor
   */
  ExpressionDetectorTrainer();

  /**
   *  @name   ExpressionDetectorTrainer
   *  @fn ExpressionDetectorTrainer(const std::string& emotion_label)
   *  @brief  Constructor
   *  @param[in]  emotion_label Name of the emotion detected
   */
  ExpressionDetectorTrainer(const std::string& emotion_label);

  /**
   *  @name   ~ExpressionDetectorTrainer
   *  @fn ~ExpressionDetectorTrainer()
   *  @brief  Destructor
   */
  ~ExpressionDetectorTrainer();

#pragma mark -
#pragma mark Train

  /**
   *  @name   Train
   *  @fn void Train(const cv::Mat& samples,
                      const cv::Mat& labels,
                      const BinaryLinearSVMTraining& svm_type,
                      const double& c_coef)
   *  @brief  Train emotion class given a C parameters
   *  @param[in]  samples  Samples for training
   *  @param[in]  labels   Labels for training, format [1xN] or [Nx1] with
   *                       {-1,1} for negative and positive sample (flt32).
   *  @param[in]  svm_type  Type of svm
   *  @param[in]  c_coef   SVM's C parameter
   */
  void Train(const cv::Mat& samples,
             const cv::Mat& labels,
             const BinaryLinearSVMTraining& svm_type,
             const double& c_coef);

  /**
   *  @name   Train
   *  @fn void Train(const cv::Mat& samples,
                     const cv::Mat& labels,
                     const BinaryLinearSVMTraining& svm_type,
                     const ScoringType scoring_type)
   *  @brief  Train emotion class doing cross validation on sample to find
   *          best C parameter
   *  @param[in]  samples         Samples for training
   *  @param[in]  labels          Labels for training, format [1xN] or [Nx1]
   *                              with {-1,1} for negative and positive sample
   *                              (flt32).
   *  @param[in]  svm_type        Type of svm
   *  @param[in]  scoring_type    Type of metric to use for scoring performance
   */
  void Train(const cv::Mat& samples,
             const cv::Mat& labels,
             const BinaryLinearSVMTraining& svm_type,
             const ScoringType scoring_type);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& output_filename) const
   *  @brief  Save Detector model into file
   *  @param[in] output_filename File to save the model
   */
  int Save(const std::string& output_filename) const;

  /**
   *  @name   Save
   *  @fn int Save(std::ofstream& out_stream) const
   *  @brief  Save Detector model into binary stream
   *  @param[in] out_stream Binary stream where to save the model
   */
  int Save(std::ofstream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize() const
   *  @brief  Compute the object size in bytes
   *  @return Object memory size
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_emotion_label
   *  @fn void set_emotion_label(const std::string& emotion_label)
   *  @brief  Define the current label for this detector
   *  @param[in]  emotion_label   Emotion label
   */
  void set_emotion_label(const std::string& emotion_label) {
    emotion_label_ = emotion_label;
  }

  /**
   *  @name   get_emotion_label
   *  @fn const std::string& get_emotion_label() const
   *  @brief  Provide the label for this detector
   *  @return Emotion label
   */
  const std::string& get_emotion_label() const {
    return emotion_label_;
  }

  /**
   *  @name get_svm_wieghts
   *  @fn cv::Mat get_svm_wieghts(void) const
   *  @brief  Provide access to SVM weights
   *  @return SVM hyperplane normal
   */
  cv::Mat get_svm_wieghts(void) const {
    return classifier_->GetW();
  }

  /**
   *  @name get_svm_wieghts
   *  @fn cv::Mat get_svm_wieghts(void) const
   *  @brief  Provide access to SVM weights
   *  @return SVM hyperplane normal
   */
  int get_feature_length(void) const {
    return classifier_->GetFeatureLength();
  }

  /**
   *  @name get_svm_bias
   *  @fn double get_svm_bias(void) const
   *  @brief  Provide access to SVM bias
   *  @return SVM bias
   */
  double get_svm_bias(void) const {
    return classifier_->GetBias();
  }

#pragma mark -
#pragma mark Utility

  /**
   *  @name   ConvertModelFromCharToBin
   *  @fn static int ConvertModelFromCharToBin(const std::string& detector_filename,
                                              const std::string& emotion_label)
   *  @brief  Use to convert old model (char) into binary one
   *  @param[in]  detector_filename   Detector parameters file
   *  @param[in]  emotion_label       Name of the emotion
   *  @return -1 if error, 0 otherwise
   */
  static int ConvertModelFromCharToBin(const std::string& detector_filename,
                                       const std::string& emotion_label);

  /**
   *  @name   ConvertModelFromCharToBin
   *  @fn static int ConvertModelFromCharToBin(const std::string& detector_filename,
                                            const std::string& emotion_label,
                                            std::ofstream& output_stream)
   *  @brief  Use to convert old model (char) into binary one
   *  @param[in]  detector_filename     Detector parameters file
   *  @param[in]  emotion_label         Name of the emotion
   *  @param[out] output_stream         Stream where data are stored
   *  @return -1 if error, 0 otherwise
   */
  static int ConvertModelFromCharToBin(const std::string& detector_filename,
                                       const std::string& emotion_label,
                                       std::ofstream& output_stream);

  /**
   *  @name   ConvertModelFromCharToBin
   *  @fn static int ConvertModelFromCharToBin(const std::vector<std::string>& detector_filenames,
                                const std::vector<std::string>& emotion_labels,
                                const std::string& output_filename)
   *  @brief  Use to convert old model (char) into binary one
   *  @param[in]  detector_filenames List of Detector parameters file
   *  @param[in]  emotion_labels     List of emotion's name
   *  @param[out] output_filename    File where data are stored
   *  @return -1 if error, 0 otherwise
   */
  static int ConvertModelFromCharToBin(const std::vector<std::string>& detector_filenames,
                                       const std::vector<std::string>& emotion_labels,
                                       const std::string& output_filename);

  /**
   *  @name   ConvertModelFromCharToBin
   *  @fn static int ConvertModelFromCharToBin(const std::vector<std::string>& detector_filenames,
                                const std::vector<std::string>& emotion_labels,
                                std::ofstream& output_stream)
   *  @brief  Use to convert old model (char) into binary one
   *  @param[in]  detector_filenames List of Detector parameters file
   *  @param[in]  emotion_labels     List of emotion's name
   *  @param[out] output_stream      Stream where data are stored
   *  @return -1 if error, 0 otherwise
   */
  static int ConvertModelFromCharToBin(const std::vector<std::string>& detector_filenames,
                                       const std::vector<std::string>& emotion_labels,
                                       std::ofstream& output_stream);

 private :
  /** Emotion label */
  std::string emotion_label_;
  /** Classifier */
  LTS5::BinaryLinearSVMClassifier* classifier_;
};
}
#endif  /* __LTS5_EXPRESSION_DETECTOR_TRAINER__ */
