/**
 *  @file   expression_detector.hpp
 *  @brief  Detection of facial emotion class definition
 *  @ingroup facial_analysis
 *
 *  @author Hua Gao / Ecabert Christophe
 *  @date   24/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */
#ifndef __LTS5_EXPRESSION_DETECTOR__
#define __LTS5_EXPRESSION_DETECTOR__

#include <istream>
#include <string>
#include <memory>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/classifier/binarylinearsvmclassifier.hpp"

/**
*  @namespace  LTS5
*  @brief      LTS5 laboratory dev space
*/
namespace LTS5 {

/**
 *  @class  ExpressionDetector
 *  @brief  Detection of seven basic facial emotions
 *  @author Christophe Ecabert
 *  @date   24/04/15
 *  @ingroup facial_analysis
 */
class LTS5_EXPORTS ExpressionDetector {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   ExpressionDetector
   *  @fn ExpressionDetector()
   *  @brief  Constructor
   */
  ExpressionDetector();

  /**
   *  @name   ~ExpressionDetector
   *  @fn ~ExpressionDetector()
   *  @brief  Destructor
   */
  ~ExpressionDetector();

  /**
   *  @name   Load
   *  @fn int Load(const std::string& filename)
   *  @brief  Load saved SVM model
   *  @param[in] filename File to load the model from
   *  @return -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   *  @name   Load
   *  @fn int Load(std::ifstream& input_stream)
   *  @brief  Load saved SVM model
   *  @param[in] input_stream Binary stream to load the model from
   *  @return -1 if error, 0 otherwise
   */
  int Load(std::ifstream& input_stream);

  /**
   *  @name   Load
   *  @fn int Load(const std::string& model_filename,const std::string& emotion_label)
   *  @brief  Load saved SVM model
   *  @param[in] model_filename Binary file to load the model from
   *  @param[in] emotion_label  Corresponding emotion label to look for
   *  @return -1 if error, 0 otherwise
   */
  int Load(const std::string& model_filename,const std::string& emotion_label);

  /**
   *  @name   Load
   *  @fn int Load(std::ifstream& input_stream,const std::string& emotion_label)
   *  @brief  Load saved SVM model
   *  @param[in] input_stream   Binary stream to load the model from
   *  @param[in] emotion_label  Corresponding emotion label to look for
   *  @return -1 if error, 0 otherwise
   */
  int Load(std::ifstream& input_stream,const std::string& emotion_label);

  /**
   *  @name   Write
   *  @fn int Write(std::ofstream& output_stream) const
   *  @brief  Dump object into the given binary stream
   *  @param[in] output_stream  Output stream to file
   *  @return -1 if error, 0 otherwise
   */
  int Write(std::ofstream& output_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize() const
   *  @brief  Compute the object size in bytes
   *  @return Object memory size
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Predict
   *  @fn double Predict(const cv::Mat& features) const
   *  @brief  Predict emotion class
   *  @param[in] features Sample to classify
   *  @return emotion class
   */
  double Predict(const cv::Mat& features) const;

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   get_emotion_label
   *  @fn const std::string& get_emotion_label() const
   *  @brief  Give emotion detected by this detector
   *  @return Emotion label
   */
  const std::string& get_emotion_label() const {
    return this->emotion_label_;
  }

  /**
   *  @name   get_feature_length
   *  @fn int get_feature_length() const
   *  @brief  Provide the size of the feature space used by this detector
   *  @return Size of the feature space
   */
  int get_feature_length() const {
    return feature_length_;
  }

 private :
  /** Linear classifier */
  BinaryLinearSVMClassifier* classifier_;
  /** Emotion label */
  std::string emotion_label_;
  /** Feature length */
  int feature_length_;
};
}  // namepsace LTS5
#endif  /* __LTS5_EXPRESSION_DETECTOR__ */

