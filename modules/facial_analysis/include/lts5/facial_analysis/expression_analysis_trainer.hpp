/**
 *  @file   expression_analysis_trainer.hpp
 *  @brief  Detection of facial emotion class definition
 *  @ingroup facial_analysis
 *
 *  @author Hua Gao / Ecabert Christophe
 *  @date   20/06/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_EXPRESSION_ANALYSIS_TRAINER__
#define __LTS5_EXPRESSION_ANALYSIS_TRAINER__

#include <stdio.h>
#include <string>
#include <vector>
#include <list>
#include <fstream>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/facial_analysis/expression_detector_trainer.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/facial_analysis/expression_analysis.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  ExpressionAnalysisTrainer
 *  @brief  Training class for emotion analysis object
 *  @author Christophe Ecabert
 *  @date   22/06/15
 *  @ingroup facial_analysis
 */
class LTS5_EXPORTS ExpressionAnalysisTrainer {
 public :

#pragma mark -
#pragma mark Types definition
  /**
   *  @enum EmotionType
   *  @brief  Define supported emotion
   */
  enum EmotionType {
    /** Anger */
    kAnger = 0x01,
    /** Disgust */
    kDisgust = 0x02,
    /** Fear */
    kFear = 0x04,
    /** Happy */
    kHappy = 0x08,
    /** Neutral */
    kNeutral = 0x010,
    /** Sad */
    kSad = 0x020,
    /** Surprise */
    kSurprise = 0x40
  };

  /** Emotion label names */
  using EmotionLabels = std::vector<std::pair<EmotionType, std::string>>;
  /** SVM Type */
  using SvmType = LTS5::BinaryLinearSVMTraining;
  /** Classification scoring metric */
  using ScoringType = BaseScoringMetric::ScoringType;
  /** Expression Analysis parameter type */
  using ExpAnalysisParameter = ExpressionAnalysis::ExpressionAnalysisParameters;

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   ExpressionAnalysisTrainer
   *  @fn ExpressionAnalysisTrainer()
   *  @brief  Constructor
   */
  ExpressionAnalysisTrainer();

  /**
   *  @name   ~ExpressionAnalysisTrainer
   *  @fn   ~ExpressionAnalysisTrainer()
   *  @brief  Constructor
   */
  ~ExpressionAnalysisTrainer();

  /**
   *  @name   Load
   *  @fn int Load(const std::string& tracker_filename,
                   const std::string& face_detector_model,
                   const std::string& pose_filename)
   *  @brief  Load face tracker and pose normalization object
   *  @param[in]  tracker_filename   Face tracker model
   *  @param[in]  face_detector_model Face detector configuration
   *  @param[in]  pose_filename      Point distribution model
   *  @return -1 if error, 0 otherwise
  */
  int Load(const std::string& tracker_filename,
           const std::string& face_detector_model,
           const std::string& pose_filename);

  /**
   *  @name   Load
   *  @fn int Load(std::ifstream& tracker_stream,
           const bool&  tracker_is_binary,
           const std::string& face_detector_model,
           std::ifstream& pose_stream,
           const bool& pose_is_binary)
   *  @brief  Load face tracker and pose normalization object
   *  @param[in]  tracker_stream      Face tracker model
   *  @param[in]  tracker_is_binary   Indicate face tracker is binary
   *  @param[in]  face_detector_model Face detector configuration
   *  @param[in]  pose_stream         Point distribution model
   *  @param[in]  pose_is_binary   Indicate pose stream is binary
   *  @return -1 if error, 0 otherwise
   */
  int Load(std::ifstream& tracker_stream,
           const bool&  tracker_is_binary,
           const std::string& face_detector_model,
           std::ifstream& pose_stream,
           const bool& pose_is_binary);

#pragma mark -
#pragma mark Train

  /**
   *  @name Preprocess
   *  @fn int Preprocess(const std::string& data_filename,
                         const std::string& feature_filename)
   *  @brief  Run preprocessing and extract feature
   *  @param[in]  data_filename         Input image candidate
   *  @param[in]  feature_filename      Path where feature will be stored
   *  @return -1 if error, 0 otherwise
   */
  int Preprocess(const std::string& data_filename,
                 const std::string& feature_filename);

  /**
   *  @name   Train
   *  @fn int Train(const std::string& data_filename,
            const std::string& feature_filename,
            const double retain_variance,
            const BinaryLinearSVMTraining& svm_type,
            const ScoringType scoring_type,
            const EmotionLabels& emotion_labels)
   *  @brief  Train detectors given a set of annotations/labels
   *  @param[in]  data_filename         File holding image + annotation
   *  @param[in]  feature_filename      File holding raw feature data
   *  @param[in]  retain_variance       Amount of variance to retain
   *  @param[in]  svm_type              Type of regularization for svm training
   *  @param[in]  scoring_type          Type of metric to use for scoring
   *                                    performance
   *  @param[in]  emotion_labels        Expression label/name
   *  @return -1 if error, 0 otherwise
   */
  int Train(const std::string& data_filename,
            const std::string& feature_filename,
            const double retain_variance,
            const BinaryLinearSVMTraining& svm_type,
            const ScoringType scoring_type,
            const EmotionLabels& emotion_labels);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& filename)
   *  @brief  Save model into binary file
   *  @param[in]  filename  Name of the file where to save model(s)
   *  @return -1 if error, 0 otherwise
   */
  int Save(const std::string& filename);

  /**
   *  @name   Save
   *  @fn int Save(std::ofstream& output_stream)
   *  @brief  Save model into binary file
   *  @param[in]  output_stream  Stream to binary file where to save model(s)
   *  @return -1 if error, 0 otherwise
   */
  int Save(std::ofstream& output_stream);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_configuration
   *  @fn void set_configuration(const ExpressionAnalysis::ExpressionAnalysisParameters& configuration)
   *  @brief  Provide access to the module configuration
   *  @param[in] configuration    Module configuration
   */
  void set_configuration(const ExpressionAnalysis::ExpressionAnalysisParameters& configuration) {
    configuration_ = configuration;
    pose_model_->set_pdm_configuration(configuration_.pose_normalization_config);
  }

  /**
   *  @name   get_configuration
   *  @fn const ExpressionAnalysis::ExpressionAnalysisParameters& get_configuration() const
   *  @brief  Provide access to the module configuration
   *  @return Configuration
   */
  const ExpressionAnalysis::ExpressionAnalysisParameters& get_configuration() const {
    return configuration_;
  }

#pragma mark -
#pragma mark Testing / Validation

  /**
   *  @name   LoadTestingConfiguration
   *  @fn static int LoadTestingConfiguration(const std::string& config,
                                               std::vector<std::string>* image,
                                               std::vector<EmotionType>* label)
   *  @brief  Load data from testing configuration file
   *  @param[in]  config  Testing configuration file
   *  @param[out] image   Image paths
   *  @param[out] label   Labels
   *  @return -1 if error, 0 otherwise
   */
  static int LoadTestingConfiguration(const std::string& config,
                                      std::vector<std::string>* image,
                                      std::vector<EmotionType>* label);
  /**
   *  @name   TestExpressionAnalysisModule
   *  @fn static int TestExpressionAnalysisModule(const std::string& emotion_model,
                                             BaseFaceTracker<cv::Mat, cv::Mat>* face_tracker,
                                             const std::string& test_config,
                                             const std::string& output_filename)
   *  @brief  Test the performance of an ExpressionAnalysis module
   *  @param[in]  emotion_model       ExpressionAnalysis model
   *  @param[in]  face_tracker        Face tracker instance
   *  @param[in]  test_config         List of image/label
   *  @param[in]  output_filename     File with ground truth/ prediction are
   *                                  dumped
   *  @return -1 if error, 0 otherwise
   */
  static int TestExpressionAnalysisModule(const std::string& emotion_model,
                                          BaseFaceTracker<cv::Mat, cv::Mat>* face_tracker,
                                          const std::string& test_config,
                                          const std::string& output_filename);

#pragma mark -
#pragma mark Converter utility function

  /**
   *  @name   ConvertModelFronCharToBin
   *  @fn static int ConvertModelFromCharToBin(const ExpAnalysisParameter& params,
                                       const PointDistributionModel& pdm,
                                       const std::string& feature_space_filename,
                                       const std::string& normalization_mean_filename,
                                       const std::string& normalization_std_filename,
                                       const std::string& detector_models,
                                       const std::string& model_filename)
   *  @brief  Convert char model (emotion detector) into binary one
   *  @param[in]  params                        Module parameters
   *  @param[in]  pdm                           Point distribution model
   *  @param[in]  feature_space_filename        PCA space
   *  @param[in]  normalization_mean_filename   Normalization file (Mean)
   *  @param[in]  normalization_std_filename    Normalization file (std)
   *  @param[in]  detector_models               ExpressionDetector binary file
   *  @param[in]  model_filename                Filename where to put model
   */
  template<typename T>
  static int ConvertModelFromCharToBin(const ExpAnalysisParameter& params,
                                       const PointDistributionModel& pdm,
                                       const std::string& feature_space_filename,
                                       const std::string& normalization_mean_filename,
                                       const std::string& normalization_std_filename,
                                       const std::string& detector_models,
                                       const std::string& model_filename);

  /**
   *  @name   ConvertModelFronCharToBin
   *  @fn static int ConvertModelFromCharToBin(const ExpAnalysisParameter& params,
                                       const PointDistributionModel& pdm,
                                       const std::string& feature_space_filename,
                                       const std::string& normalization_mean_filename,
                                       const std::string& normalization_std_filename,
                                       const std::string& detector_models,
                                       std::ofstream& output_stream);
   *  @brief  Convert char model (emotion detector) into binary one
   *  @param[in]  params                        Module parameters
   *  @param[in]  pdm                           Point distribution model
   *  @param[in]  feature_space_filename        PCA space
   *	@param[in]  normalization_mean_filename   Normalization file (Mean)
   *	@param[in]  normalization_std_filename    Normalization file (std)
   *  @param[in]  detector_models               ExpressionDetector binary file
   *  @param[in]  output_stream                 Stream where to put model
   */
  template<typename T>
  static int ConvertModelFromCharToBin(const ExpAnalysisParameter& params,
                                       const PointDistributionModel& pdm,
                                       const std::string& feature_space_filename,
                                       const std::string& normalization_mean_filename,
                                       const std::string& normalization_std_filename,
                                       const std::string& detector_models,
                                       std::ofstream& output_stream);

#pragma mark -
#pragma mark Private

 private :

  /**
   *  @struct CandidateNode
   *  @brief  Hold data (image + annotation) for a given candidate
   */
  struct CandidateNode {
    /** ID */
    int k_id;
    /** Image location */
    std::string k_image_location;
    /** Landmarks */
    cv::Mat k_shape;
    /** Ground truth */
    ExpressionAnalysisTrainer::EmotionType k_ground_truth;
    /** Indicate if sample if used */
    int k_selection;

    /**
     *  @name   CandidateNode
     *  @fn CandidateNode()
     *  @brief  Constructor
     */
    CandidateNode() : k_id(-1), k_selection(-1) {
    }
    /**
     *  @name SetEntry
     *  @fn void SetEntry(const int id,
                         const std::string& path,
                         const int label,
                         const int selected)
     *  @brief  Add one element to this node
     *  @param[in]  id        Candidate ID
     *  @param[in]  path      Image path
     *  @param[in]  label     Emotion label
     *  @param[in]  selected  Selected or not (-1 = undef, 0 = no, 1 = yes)
     */
    void SetEntry(const int id,
                  const std::string& path,
                  const int label,
                  const int selected) {
      k_id = id;
      k_image_location = path;
      k_ground_truth = static_cast<EmotionType>(label);
      k_selection = selected;
    }
  };

  /**
   *  @typedef CandidateList_t
   *  @brief  List of Candidate type
   */
  typedef std::vector<ExpressionAnalysisTrainer::CandidateNode> CandidateList_t;

  /**
   *  @name   InitializeInternalContainer
   *  @fn void InitializeInternalContainer()
   *  @brief  Initialize internal container
   */
  void InitializeInternalContainer();

  /**
   *  @name   LoadCandidateData
   *  @fn int LoadCandidateData(const std::string& data_filename,
                                 CandidateList_t* list,
                                 int* n_sample)
   *  @brief  Load candidate data (image location + ground truth)
   *  @param[in]  data_filename   File holding candidate data (image location +
   *                              annotation)
   *  @param[out] list            List of candidate to populate
   *  @param[out] n_sample        Number of sample loaded
   *  @return -1 if error, 0 otherwise.
   */
  int LoadCandidateData(const std::string& data_filename,
                        CandidateList_t* list,
                        int* n_sample);

  /**
   *  @name CleanUpCandidateData
   *  @fn int CleanUpCandidateData(CandidateList_t* list,
                                   int* n_usable_sample)
   *  @brief  Remove un-selected sample
   *  @param[in,out]  list            List of candidate to clean
   *  @param[out]     n_usable_sample Number of sample usable after cleaning
   *                                  (Should be the size of list)
   *  @return -1 if error, 0 otherwise
   */
  int CleanUpCandidateData(CandidateList_t* list,
                           int* n_usable_sample);

  /**
   *  @name   DetectLandmark
   *  @fn void DetectLandmark(const std::string& data_filename,
                               CandidateList_t* list,
                               int* n_sample)
   *  @brief  Detect landmark for each CandidateNode and remove if entry in case
   *          of bad detection
   *  @param[in]      data_filename Path to original data candidate
   *  @param[in,out]  list          List of candidate where image are stored
   *  @param[out]     n_sample      Number of usable sample
   */
  void DetectLandmark(const std::string& data_filename,
                      CandidateList_t* list,
                      int* n_sample);

  /**
   *  @name  ExtractFeature
   *  @fn int ExtractFeature(const std::string& feature_filename,
                             const int n_feature,
                             const CandidateList_t& list,
                             cv::Mat* feature)
   *  @brief  Extract sift features for training and testing set
   *  @param[in]  feature_filename  Place where to save feature
   *  @param[in]  n_feature         Number of feature to extract
   *  @param[in]  list              List of candidate holding shape information
   *  @param[out] feature           Extracted sift features
   *  @return -1 if error, 0 otherwise
   */
  int ExtractFeature(const std::string& feature_filename,
                     const int n_feature,
                     const CandidateList_t& list,
                     cv::Mat* feature);

  /**
   *  @name ComputePcaSubspace
   *  @fn void ComputePcaSubspace(const cv::Mat& data,
                                 const double retain_variance)
   *  @brief  Compute pca feature subspace
   *  @param[in]  data              DAta to perform PCA on
   *  @param[in]  retain_variance   Amount of variance to retain
   */
  void ComputePcaSubspace(const cv::Mat& data,
                          const double retain_variance);

  /**
   *  @name   PrepareDataSample
   *  @fn void PrepareDataSample(const EmotionType& emotion_label,
                                 const CandidateList_t& candidates,
                                 const int n_label,
                                 cv::Mat* labels)
   *  @brief  Generate the labels for a given emotion label
   *  @param[in]  emotion_label   Emotion label, if -1 just copy label into array
   *  @param[in]  candidates      List of candidate to extract label from
   *  @param[in]  n_label         Number of label to generate
   *  @param[out] labels          Positive example
   */
  void PrepareDataSample(const EmotionType& emotion_label,
                         const CandidateList_t& candidates,
                         const int n_label,
                         cv::Mat* labels);


  /**
   *  @name   ProjectDataSample
   *  @fn void ProjectDataSample(const cv::Mat& data,
                                 cv::Mat* projected_data)
   *  @brief  Project feature into subspace for a given size
   *  @param[in]  data              Data samples
   *  @param[out] projected_data    Projected data into subspace + normalized if
   *                                asked
   */
  void ProjectDataSample(const cv::Mat& data,
                         cv::Mat* projected_data);

  /**
   *  @name ComputeNormalization
   *  @fn void ComputeNormalization(const cv::Mat& data)
   *  @brief  Compute normalization factor for data sample (z-score)
   *  @param[in]  data  Data to use to estimate normalization
   */
  void ComputeNormalization(const cv::Mat& data);

  /**
   *  @name NormalizeDataSample
   *  @fn void NormalizeDataSample(const cv::Mat& data)
   *  @brief  Apply normalization to data sample (z-score)
   *  @param[in,out]  data  Data to normalize
   */
  void NormalizeDataSample(const cv::Mat& data);

  /** Module properly init */
  bool initialized_;
  /** Configuration */
  ExpressionAnalysis::ExpressionAnalysisParameters configuration_;
  /** Candidates */
  std::vector<ExpressionAnalysisTrainer::CandidateNode> candidate_;
  /** Number total of samples */
  int n_available_sample_;
  /** Number of usable samples */
  int n_usable_sample_;
  /** Pool of sift features */
  cv::Mat sift_features_;
  /** PCA subspaces */
  cv::Mat feature_space_;
  /** Z-Score, mean */
  cv::Mat feature_normalization_mean_;
  /** Z-Score, standard deviation */
  cv::Mat feature_normalization_std_;
  /** Face tracker */
  LTS5::SdmTracker* face_tracker_;
  /** Point distribution model */
  LTS5::PointDistributionModel* pose_model_;
  /** Best trained detectors */
  std::vector<LTS5::ExpressionDetectorTrainer*> expression_detector_trainer_;
};
}  // namespace LTS5
#endif /* __LTS5_EXPRESSION_ANALYSIS_TRAINER__ */
