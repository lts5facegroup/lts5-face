/**
 *  @file   expression_analysis.hpp
 *  @brief  Detection of facial emotion class definition
 *  @ingroup facial_analysis
 *
 *  @author Hua Gao / Ecabert Christophe
 *  @date   20/06/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_EXPRESSION_ANALYSIS__
#define __LTS5_EXPRESSION_ANALYSIS__

#include <stdio.h>
#include <vector>
#include <unordered_map>
#include <string>
#include <fstream>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/facial_analysis/expression_detector.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/utils/ssift.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  ExpressionAnalysis
 *  @brief  Facial emotion analysis module definition
 *  @author Christophe Ecabert
 *  @date   20/06/2015
 *  @see    http://www.pitt.edu/~jeffcohn/CVPR2010_CK%2B2.pdf
 *  @ingroup facial_analysis
 */
class LTS5_EXPORTS ExpressionAnalysis {
 public :

#pragma mark -
#pragma mark Type definition

  /**
   *  @struct ExpressionAnalysisParameters
   *  @brief  Structure encapsulating configuration for ExpressionAnalysis class
   */
  struct ExpressionAnalysisParameters {
    /** Sift size */
    int sift_size;
    /** Sift configuration */
    LTS5::SSift::SSiftParameters sift_configuration;
    /** Image normalization */
    LTS5::PointDistributionModel::PDMParameters pose_normalization_config;

    /**
     *  @name   ExpressionAnalysisParameters
     *  @fn ExpressionAnalysisParameters()
     *  @brief  Constructor
     */
    ExpressionAnalysisParameters() {
      sift_size = 32;
    }

    /**
     *  @name   ComputeHeaderSize
     *  @fn int ComputeHeaderSize() const
     *  @brief  Compute the size of the header in byte
     *  @return Length in bytes
     */
    int ComputeHeaderSize() const {
      // Sift size
      int size = sizeof(int);
      // Sift config
      size += sift_configuration.ComputeObjectSize();
      // Module configuration
      size += pose_normalization_config.ComputeObjectSize();
      return size;
    }
    /**
     *  @name WriteParameterHeader
     *  @fn int WriteParameterHeader(std::ostream& output_stream) const
     *  @brief  Dump module parameters into a given stream.
     *  @param[in]  output_stream Stream where data will be written
     *  @return -1 If error, 0 otherwise
     */
    int WriteParameterHeader(std::ostream& output_stream) const {
      int error = -1;
      if (output_stream.good()) {
        output_stream.write(reinterpret_cast<const char*>(&sift_size),
                            sizeof(sift_size));
        // Write sift configuration
        error = sift_configuration.WriteParameterHeader(output_stream);
        // Pdm
        error |= pose_normalization_config.WriteParameterHeader(output_stream);
      }
      return error;
    }
    /**
     *  @name   ReadParameterHeader
     *  @fn int ReadParameterHeader(std::istream& input_stream)
     *  @brief  Initialize parameters structures from input stream
     *  @param[in]  input_stream  Stream from where to read data
     *  @return -1 If error, 0 otherwise
     */
    int ReadParameterHeader(std::istream& input_stream) {
      int error = -1;
      if (input_stream.good()) {
        input_stream.read(reinterpret_cast<char*>(&sift_size),
                          sizeof(sift_size));
        error = sift_configuration.ReadParameterHeader(input_stream);
        error |= pose_normalization_config.ReadParameterHeader(input_stream);
      }
      return error;
    }
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   ExpressionAnalysis
   *  @fn ExpressionAnalysis()
   *  @brief  Constructor
   */
  ExpressionAnalysis();

  /**
   *  @name   ~ExpressionAnalysis
   *  @fn ~ExpressionAnalysis()
   *  @brief  Destructor
   */
  ~ExpressionAnalysis();

  /**
   *  @name Load
   *  @fn int Load(std::ifstream& input_stream)
   *  @brief  Initialize module with a given stream
   *  @param[in]  input_stream  Input stream pointing to models
   *  @return -1 if error, 0 otherwise
   */
  int Load(std::ifstream& input_stream);

  /**
   *  @name Load
   *  @fn int Load(std::ifstream& input_stream,
                   const std::vector<std::string>& emotion_labels)
   *  @brief  Initialize module with a given stream
   *  @param[in]  input_stream    Input stream pointing to models
   *  @param[in]  emotion_labels  List of emotion of interest
   *  @return -1 if error, 0 otherwise
   */
  int Load(std::ifstream& input_stream,
           const std::vector<std::string>& emotion_labels);

  /**
   *  @name Load
   *  @fn int Load(const std::string& filename)
   *  @brief  Initialize module with a given filename
   *  @param[in]  filename  Model filename
   *  @return -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   *  @name Load
   *  @fn int Load(const std::string& filename,
                   const std::vector<std::string>& emotion_labels)
   *  @brief  Initialize module with a given filename
   *  @param[in]  filename  Model filename
   *  @param[in]  emotion_labels  List of emotion of interest
   *  @return -1 if error, 0 otherwise
   */
  int Load(const std::string& filename,
           const std::vector<std::string>& emotion_labels);

  /**
   *  @name   Write
   *  @fn int Write(std::ofstream& out_stream) const
   *  @brief  Copy the object into a binary stream
   *  @param[in]  out_stream  Output binary stream
   *  @return -1 if error, 0 otherwise
   */
  int Write(std::ofstream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize() const
   *  @brief  Compute the object size in bytes
   *  @return Object memory size
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Prediction

  /**
   *  @name   Predict
   *  @fn void Predict(const cv::Mat& image,
                       const cv::Mat& shape,
                       std::vector<double>* scores)
   *  @brief  Classify one sample for each emotion
   *  @param[in]  image   Source image
   *  @param[in]  shape   Facial landmarks
   *  @param[out] scores  Individual score for each emotion
   */
  void Predict(const cv::Mat& image,
               const cv::Mat& shape,
               std::vector<double>* scores);

  /**
   *  @name   Predict
   *  @fn void Predict(const cv::Mat& image,
                       const cv::Mat& shape,
                       const std::vector<std::string>& emotion_label,
                       std::vector<double>* scores)
   *  @brief  Classify one sample for a specific emotion
   *  @param[in]  image         Source image
   *  @param[in]  shape         Facial landmarks
   *  @param[in]  emotion_label Label of the emotion of interest
   *  @param[out] scores        Individual score for each emotion
   */
  void Predict(const cv::Mat& image,
               const cv::Mat& shape,
               const std::vector<std::string>& emotion_label,
               std::vector<double>* scores);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_configuration
   *  @fn void set_configuration(const ExpressionAnalysisParameters& configuration)
   *  @brief  Provide access to the module configuration
   *  @param[in] configuration    Module configuration
   */
  void set_configuration(const ExpressionAnalysisParameters& configuration) {
    configuration_ = configuration;
  }

  /**
   *  @name   get_expression_labels
   *  @fn void get_expression_labels(std::vector<std::string>* expression_labels) const
   *  @brief  Give access to expression labels
   *  @param[out] expression_labels  List of expression labels
   */
  void get_expression_labels(std::vector<std::string>* expression_labels) const;

  /**
   *  @name   get_configuration
   *  @fn const ExpressionAnalysisParameters& get_configuration() const
   *  @brief  Provide access to the module configuration
   *  @return Configuration
   */
  const ExpressionAnalysisParameters& get_configuration() const {
    return configuration_;
  }

  /**
   * @name  get_normalized_image
   * @fn    cv::Mat get_normalized_image() const
   * @brief Provide access to normalized images
   * @return  Normalized image
   */
  cv::Mat get_normalized_image() const {
    return normalized_image_;
  }

  /**
   * @name  get_features
   * @fn    const cv::Mat& get_features() const
   * @brief Get features used for classification
   * @return    Features
   */
  const cv::Mat& get_features() const {
    return features_;
  }

 private :

  /** Configuration */
  ExpressionAnalysisParameters configuration_;
  /** Pose normalization */
  LTS5::PointDistributionModel* pose_normalization_;
  /** Working image */
  cv::Mat working_image_;
  /** Normalized image */
  cv::Mat normalized_image_;
  /** Keypoints */
  std::vector<cv::KeyPoint> shape_keypoint_;
  /** Emotion feature space */
  cv::Mat feature_basis_;
  /** Features */
  cv::Mat features_;
  /** Projected sample */
  cv::Mat projected_samples_;
  /** Feature normalization */
  bool do_feature_normalization_;
  /** Normalization, mean */
  cv::Mat feature_normalization_mean_;
  /** Normalization, std */
  cv::Mat feature_normalization_std_;
  /** Classifier */
  std::vector<LTS5::ExpressionDetector*> expression_detectors_;
  /** Label */
  std::unordered_map<std::string, int> emotion_labels_;

  /**
   *  @name   ReleaseMemory
   *  @fn void ReleaseMemory()
   *  @brief  Release classifer and stuff
   */
  void ReleaseMemory();
};
}  // namespace LTS5
#endif /* __LTS5_EXPRESSION_ANALYSIS__ */
