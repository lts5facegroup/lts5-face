/**
 *  @file   au_analysis.hpp
 *  @brief  AU Analysis processing module
 *  @ingroup facial_analysis
 *
 *  @author Christophe Ecabert
 *  @date   11/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_AU_ANALYSIS__
#define __LTS5_AU_ANALYSIS__

#include <string>
#include <fstream>
#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/ssift.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/facial_analysis/au_detector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  AUAnalysis
 *  @brief  Facial action unit analysis processing module
 *  @author Christophe Ecabert
 *  @date   11/04/2016
 *  @ingroup facial_analysis
 */
class LTS5_EXPORTS AUAnalysis {

 public :

#pragma mark -
#pragma mark Type definition

  /**
   *  @struct AUAnalysisParameters
   *  @brief  Structure encapsulating configuration for ExpressionAnalysis class
   */
  struct AUAnalysisParameters {
    /** Sift size */
    int sift_size;
    /** Sift configuration */
    LTS5::SSift::SSiftParameters sift_configuration;
    /** Image normalization */
    LTS5::PointDistributionModel::PDMParameters pose_normalization_config;

    /**
     *  @name   AUAnalysisParameters
     *  @fn AUAnalysisParameters()
     *  @brief  Constructor
     */
    AUAnalysisParameters() {
      sift_size = 32;
    }

    /**
     *  @name   ComputeHeaderSize
     *  @fn int ComputeHeaderSize() const
     *  @brief  Compute the size of the header in byte
     *  @return Length in bytes
     */
    int ComputeHeaderSize() const {
      // Sift size
      int size = sizeof(int);
      // Sift config
      size += sift_configuration.ComputeObjectSize();
      // Module configuration
      size += pose_normalization_config.ComputeObjectSize();
      return size;
    }
    /**
     *  @name WriteParameterHeader
     *  @fn int WriteParameterHeader(std::ostream& output_stream) const
     *  @brief  Dump module parameters into a given stream.
     *  @param[in]  output_stream Stream where data will be written
     *  @return -1 If error, 0 otherwise
     */
    int WriteParameterHeader(std::ostream& output_stream) const {
      int error = -1;
      if (output_stream.good()) {
        output_stream.write(reinterpret_cast<const char*>(&sift_size),
                            sizeof(sift_size));
        // Write sift configuration
        error = sift_configuration.WriteParameterHeader(output_stream);
        // Pdm
        error |= pose_normalization_config.WriteParameterHeader(output_stream);
      }
      return error;
    }
    /**
     *  @name   ReadParameterHeader
     *  @fn int ReadParameterHeader(std::istream& input_stream)
     *  @brief  Initialize parameters structures from input stream
     *  @param[in]  input_stream  Stream from where to read data
     *  @return -1 If error, 0 otherwise
     */
    int ReadParameterHeader(std::istream& input_stream) {
      int error = -1;
      if (input_stream.good()) {
        input_stream.read(reinterpret_cast<char*>(&sift_size),
                          sizeof(sift_size));
        error = sift_configuration.ReadParameterHeader(input_stream);
        error |= pose_normalization_config.ReadParameterHeader(input_stream);
      }
      return error;
    }
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   AUAnalysis
   *  @fn AUAnalysis()
   *  @brief  Constructor
   */
  AUAnalysis();

  /**
   *  @name   ~AUAnalysis
   *  @fn ~AUAnalysis()
   *  @brief  Destructor
   */
  ~AUAnalysis();

  /**
   *  @name Load
   *  @fn int Load(std::ifstream& input_stream)
   *  @brief  Initialize module with a given stream
   *  @param[in]  input_stream  Input stream pointing to models
   *  @return -1 if error, 0 otherwise
   */
  int Load(std::ifstream& input_stream);

  /**
   *  @name Load
   *  @fn int Load(const std::string& filename)
   *  @brief  Initialize module with a given filename
   *  @param[in]  filename  Model filename
   *  @return -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   *  @name   Write
   *  @fn int Write(std::ofstream& out_stream) const
   *  @brief  Copy the object into a binary stream
   *  @param[in]  out_stream  Output binary stream
   *  @return -1 if error, 0 otherwise
   */
  int Write(std::ofstream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize() const
   *  @brief  Compute the object size in bytes
   *  @return Object memory size
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Prediction

  /**
   *  @name   Predict
   *  @fn void Predict(const cv::Mat& image,
                       const cv::Mat& shape,
                       std::vector<double>* scores)
   *  @brief  Classify one sample for each Action unit
   *  @param[in]  image   Source image
   *  @param[in]  shape   Facial landmarks
   *  @param[out] scores  Individual score for each action unit
   */
  void Predict(const cv::Mat& image,
               const cv::Mat& shape,
               std::vector<double>* scores);

  /**
   *  @name   NormalizedPredict
   *  @fn void NormalizedPredict(const cv::Mat& normalized_image,
                         const cv::Mat& normalized_shape,
                         std::vector<double>* scores)
   *  @brief  Classify one sample for each action unit
   *  @param[in]  normalized_image   Normalized source image
   *  @param[in]  normalized_shape   Normalized Facial landmarks
   *  @param[out] scores             Individual score for each action unit
   */
  void NormalizedPredict(const cv::Mat& normalized_image,
                         const cv::Mat& normalized_shape,
                         std::vector<double>* scores);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   set_configuration
   *  @fn void set_configuration(const AUAnalysisParameters& configuration)
   *  @brief  Provide access to the module configuration
   *  @param[in] configuration    Module configuration
   */
  void set_configuration(const AUAnalysisParameters& configuration) {
    configuration_ = configuration;
  }

  /**
   *  @name   get_au_labels
   *  @fn void get_au_labels(std::vector<std::string>* au_labels) const
   *  @brief  Give access to emotion labels
   *  @param[out] au_labels  List of AU labels
   */
  void get_au_labels(std::vector<std::string>* au_labels) const {
    *au_labels = au_labels_;
  }

  /**
   *  @name   get_configuration
   *  @fn const AUAnalysisParameters& get_configuration() const
   *  @brief  Provide access to the module configuration
   *  @return Configuration
   */
  const AUAnalysisParameters& get_configuration() const {
    return configuration_;
  }

  /**
   * @name  get_normalized_image
   * @fn    cv::Mat get_normalized_image() const
   * @brief Provide access to normalized images
   * @return  Normalized image
   */
  cv::Mat get_normalized_image() const {
    return normalized_image_;
  }

#pragma mark -
#pragma mark Private
 private:

  /** Configuration */
  AUAnalysisParameters configuration_;
  /** Pose normalization */
  LTS5::PointDistributionModel* pose_normalization_;
  /** Working image */
  cv::Mat working_image_;
  /** Normalized image */
  cv::Mat normalized_image_;
  /** Keypoints */
  std::vector<cv::KeyPoint> shape_keypoint_;
  /** Emotion feature space */
  cv::Mat feature_basis_;
  /** Projected sample */
  cv::Mat projected_samples_;
  /** Feature normalization */
  bool do_feature_normalization_;
  /** Normalization, mean */
  cv::Mat feature_normalization_mean_;
  /** Normalization, std */
  cv::Mat feature_normalization_std_;
  /** Classifier */
  std::vector<LTS5::AUDetector*> au_detectors_;
  /** Label */
  std::vector<std::string> au_labels_;

  /**
   *  @name   ReleaseMemory
   *  @fn void ReleaseMemory()
   *  @brief  Release classifer and stuff
   */
  void ReleaseMemory();

};

}  // namepsace LTS5

#endif //__LTS5_AU_ANALYSIS__
