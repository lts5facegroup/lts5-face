/**
 *  @file   au_analysis_trainer.hpp
 *  @brief  AU Analysis trainer module
 *  @ingroup facial_analysis
 *
 *  @author Christophe Ecabert
 *  @date   11/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_AU_ANALYSIS_TRAINER__
#define __LTS5_AU_ANALYSIS_TRAINER__

#include <string>
#include <utility>
#include <vector>

#include "lts5/utils/library_export.hpp"
#include "lts5/classifier/binarylinearsvmclassifier.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/facial_analysis/au_detector_trainer.hpp"
#include "lts5/facial_analysis/au_analysis.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  AUAnalysisTrainer
 *  @brief  Training class for au analysis object
 *  @author Christophe Ecabert
 *  @date   11/04/16
 *  @ingroup facial_analysis
 */
class LTS5_EXPORTS AUAnalysisTrainer {

 public:
#pragma mark -
#pragma mark Types definition

  /**
   *  @enum AUType
   *  @brief  Define supported emotion
   */
  enum AUType {
    /** AU 01 */
    kAU_01 = 0x0001,
    /** AU 02 */
    kAU_02 = 0x0002,
    /** AU 04 */
    kAU_04 = 0x0004,
    /** AU 06 */
    kAU_06 = 0x0008,
    /** AU 07 */
    kAU_07 = 0x0010,
    /** AU 10 */
    kAU_10 = 0x0020,
    /** AU 12 */
    kAU_12 = 0x0040,
    /** AU 14 */
    kAU_14 = 0x0080,
    /** AU 15 */
    kAU_15 = 0x0100,
    /** AU 17 */
    kAU_17 = 0x0200,
    /** AU 23 */
    kAU_23 = 0x0400,
    /** AU 25 */
    kAU_25 = 0x0800,
    /** AU 28 */
    kAU_28 = 0x1000,
    /** AU 45 */
    kAU_45 = 0x2000,
  };

  /** AU Label */
  using AULabel = std::pair<AUType, std::string>;
  /** SVM types */
  using SvmType = LTS5::BinaryLinearSVMTraining;
  /** Classification scoring metric */
  using ScoringType = BaseScoringMetric::ScoringType;

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   AUAnalysisTrainer
   *  @fn AUAnalysisTrainer()
   *  @brief  Constructor
   */
  AUAnalysisTrainer();

  /**
   *  @name   ~AUAnalysisTrainer
   *  @fn   ~AUAnalysisTrainer()
   *  @brief  Constructor
   */
  ~AUAnalysisTrainer();

  /**
   *  @name   Load
   *  @fn int Load(const std::string& tracker_filename,
           const std::string& face_detector_model,
           const std::string& pose_filename)
   *  @brief  Load face tracker and pose normalization object
   *  @param[in]  tracker_filename   Face tracker model
   *  @param[in]  face_detector_model Face detector configuration
   *  @param[in]  pose_filename      Point distribution model
   *  @return -1 if error, 0 otherwise
  */
  int Load(const std::string& tracker_filename,
           const std::string& face_detector_model,
           const std::string& pose_filename);

  /**
   *  @name   Load
   *  @fn int Load(std::ifstream& tracker_stream,
           const bool&  tracker_is_binary,
           const std::string& face_detector_model,
           std::ifstream& pose_stream,
           const bool& pose_is_binary)
   *  @brief  Load face tracker and pose normalization object
   *  @param[in]  tracker_stream      Face tracker model
   *  @param[in]  tracker_is_binary   Indicate face tracker is binary
   *  @param[in]  face_detector_model Face detector configuration
   *  @param[in]  pose_stream         Point distribution model
   *  @param[in]  pose_is_binary   Indicate pose stream is binary
   *  @return -1 if error, 0 otherwise
   */
  int Load(std::ifstream& tracker_stream,
           const bool&  tracker_is_binary,
           const std::string& face_detector_model,
           std::ifstream& pose_stream,
           const bool& pose_is_binary);

#pragma mark -
#pragma mark Train



  /**
   *  @name   Train
   *  @fn int Train(const std::string& data_filename,
                     const double retain_variance,
                     const BinaryLinearSVMTraining& svm_type,
                     const std::vector<AULabel>& au_labels,
                     const std::vector<int>& subspace_dim)
   *  @brief  Train detectors given a set of annotations/labels
   *  @param[in]  data_filename         File holding image + annotation
   *  @param[in]  retain_variance       Amount of variance to retain
   *  @param[in]  svm_type              Type of regularization for svm training
   *  @param[in]  scoring_type          Type of metric to use for scoring
   *                                    performance
   *  @param[in]  au_labels             AU label/name
   *  @param[in]  subspace_dim          Subspace dimension to try
   *  @return -1 if error, 0 otherwise
   */
  int Train(const std::string& data_filename,
            const double retain_variance,
            const BinaryLinearSVMTraining& svm_type,
            const ScoringType scoring_type,
            const std::vector<AULabel>& au_labels,
            const std::vector<int>& subspace_dim);

  /**
   *  @name   Save
   *  @fn int Save(const std::string& filename)
   *  @brief  Save model into binary file
   *  @param[in]  filename  Name of the file where to save model(s)
   *  @return -1 if error, 0 otherwise
   */
  int Save(const std::string& filename);

  /**
   *  @name   Save
   *  @fn int Save(std::ofstream& output_stream)
   *  @brief  Save model into binary file
   *  @param[in]  output_stream  Stream to binary file where to save model(s)
   *  @return -1 if error, 0 otherwise
   */
  int Save(std::ofstream& output_stream);

#pragma mark -
#pragma mark Accessors

#pragma mark -
#pragma mark Testing / Validation

  /**
   *  @name   LoadTestingConfiguration
   *  @fn static int LoadTestingConfiguration(const std::string& config,
                                               std::vector<std::string>* image,
                                               std::vector<int>* label)
   *  @brief  Load data from testing configuration file
   *  @param[in]  config  Testing configuration file
   *  @param[out] image   Image paths
   *  @param[out] label   Labels
   *  @return -1 if error, 0 otherwise
   */
  static int LoadTestingConfiguration(const std::string& config,
                                      std::vector<std::string>* image,
                                      std::vector<int>* label);
  /**
   *  @name   TestExpressionAnalysisModule
   *  @fn static int TestExpressionAnalysisModule(const std::string& au_model,
                                             BaseFaceTracker<cv::Mat, cv::Mat>* face_tracker,
                                             const std::string& test_config,
                                             const std::string& output_filename)
   *  @brief  Test the performance of an ExpressionAnalysis module
   *  @param[in]  au_model            AUAnalysis model
   *  @param[in]  face_tracker        Face tracker instance
   *  @param[in]  test_config         List of image/label
   *  @param[in]  output_filename     File with ground truth/ prediction are
   *                                  dumped
   *  @return -1 if error, 0 otherwise
   */
  static int TestExpressionAnalysisModule(const std::string& au_model,
                                          BaseFaceTracker<cv::Mat, cv::Mat>* face_tracker,
                                          const std::string& test_config,
                                          const std::string& output_filename);



#pragma mark -
#pragma mark Private

 private:

  /**
   *  @struct CandidateNode
   *  @brief  Hold data (image + annotation) for a given candidate
   */
  struct CandidateNode {
    /** Image location */
    std::string k_image_location;
    /** Landmarks */
    cv::Mat k_shape;
    /** Ground truth */
    int k_ground_truth;
  };

  /**
   *  @name   LoadCandidateData
   *  @fn int LoadCandidateData(const std::string& data_filename,
                                 std::vector<CandidateNode>* list,
                                 int* n_sample)
   *  @brief  Load candidate data (image location + ground truth)
   *  @param[in]  data_filename   File holding candidate data (image location +
   *                              annotation)
   *  @param[out] list            List of candidate to populate
   *  @param[out] n_sample        Number of sample loaded
   *  @return -1 if error, 0 otherwise.
   */
  int LoadCandidateData(const std::string& data_filename,
                        std::vector<CandidateNode>* list,
                        int* n_sample);

  /**
   * @name  TrainingLabelStats
   * @fn    void TrainingLabelStats(const std::vector<AULabel>& au_label,
                                    const std::vector<CandidateNode>& list)
   * @brief Compute number of positive sample for each AU
   * @param[in] au_label  List of AU labels
   * @param[in] list      List of training samples
   */
  void TrainingLabelStats(const std::vector<AULabel>& au_label,
                          const std::vector<CandidateNode>& list);

  /**
   *  @name   DetectLandmark
   *  @fn void DetectLandmark(const std::string& data_filename,
                               std::vector<CandidateNode>* list,
                               int* n_sample)
   *  @brief  Detect landmark for each CandidateNode and remove if entry in case
   *          of bad detection
   *  @param[in]      data_filename Path to original data candidate
   *  @param[in,out]  list          List of candidate where image are stored
   *  @param[out]     n_sample      Number of usable sample
   */
  void DetectLandmark(const std::string& data_filename,
                      std::vector<CandidateNode>* list,
                      int* n_sample);

  /**
   *  @name  ExtractFeature
   *  @fn void ExtractFeature(const int n_feature,
                              const std::vector<CandidateNode>& list,
                              cv::Mat* feature)
   *  @brief  Extract sift features for training and testing set
   *  @param[in]  n_feature         Number of feature to extract
   *  @param[in]  list              List of candidate holding shape information
   *  @param[out] feature           Extracted sift features
   */
  void ExtractFeature(const int n_feature,
                      const std::vector<CandidateNode>& list,
                      cv::Mat* feature);

  /**
   *  @name ComputePcaSubspace
   *  @fn void ComputePcaSubspace(const cv::Mat& data,
                                 const double retain_variance)
   *  @brief  Compute pca feature subspace
   *  @param[in]  data              DAta to perform PCA on
   *  @param[in]  retain_variance   Amount of variance to retain
   */
  void ComputePcaSubspace(const cv::Mat& data,
                          const double retain_variance);

  /**
   *  @name   PrepareDataSample
   *  @fn void PrepareDataSample(const AUType& au_label,
                                 const std::vector<CandidateNode>& candidates,
                                 const int n_label,
                                 cv::Mat* labels)
   *  @brief  Generate the labels for a given emotion label
   *  @param[in]  au_label        AU label
   *  @param[in]  candidates      List of candidate to extract label from
   *  @param[in]  n_label         Number of label to generate
   *  @param[out] labels          Positive example
   */
  void PrepareDataSample(const AUType & au_label,
                         const std::vector<CandidateNode>& candidates,
                         const int n_label,
                         cv::Mat* labels);


  /**
   *  @name   ProjectDataSample
   *  @fn void ProjectDataSample(const cv::Mat& data,
                                 cv::Mat* projected_data)
   *  @brief  Project feature into subspace for a given size
   *  @param[in]  data              Data samples
   *  @param[out] projected_data    Projected data into subspace + normalized if
   *                                asked
   */
  void ProjectDataSample(const cv::Mat& data,
                         cv::Mat* projected_data);

  /**
   *  @name ComputeNormalization
   *  @fn void ComputeNormalization(const cv::Mat& data)
   *  @brief  Compute normalization factor for data sample (z-score)
   *  @param[in]  data  Data to use to estimate normalization
   */
  void ComputeNormalization(const cv::Mat& data);

  /**
   *  @name NormalizeDataSample
   *  @fn void NormalizeDataSample(const cv::Mat& data)
   *  @brief  Apply normalization to data sample (z-score)
   *  @param[in,out]  data  Data to normalize
   */
  void NormalizeDataSample(const cv::Mat& data);

  /** Initialization flag */
  bool initialized_;
  /** Face tracker */
  LTS5::SdmTracker* tracker_;
  /** Point distribution model */
  LTS5::PointDistributionModel* pose_model_;
  /** AU Detector trainer */
  std::vector<LTS5::AUDetectorTrainer*> au_detector_trainers_;
  /** Configuration */
  AUAnalysis::AUAnalysisParameters configuration_;
  /** Candidate */
  std::vector<CandidateNode> candidate_;
  /** Number total of samples */
  int n_available_sample_;
  /** Number of usable sample */
  int n_usable_sample_;
  /** Pool of sift features */
  cv::Mat sift_features_;
  /** PCA subspaces */
  cv::Mat feature_space_;
  /** Z-Score, mean */
  cv::Mat feature_normalization_mean_;
  /** Z-Score, standard deviation */
  cv::Mat feature_normalization_std_;
};
}  // namepsace LTS5

#endif //__LTS5_AU_ANALYSIS_TRAINER__
