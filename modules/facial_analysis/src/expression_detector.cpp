/**
*  @file   expression_detector.cpp
*  @brief  Detection of facial emotion class definition
*  @author Hua Gao / Ecabert Christophe
*  @date   24/04/15
*  Copyright (c) 2015 Hua Gao. All rights reserved.
*/

#include <fstream>

#include "lts5/facial_analysis/expression_detector.hpp"
#include "lts5/utils/file_io.hpp"

/**
*  @namespace  LTS5
*  @brief      LTS5 laboratory dev space
*/
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   ExpressionDetector
 *  @brief  Constructor
 */
ExpressionDetector::ExpressionDetector() : classifier_(nullptr), emotion_label_("") {
}

/*
 *  @name   ~ExpressionDetector
 *  @brief  Destructor
 */
ExpressionDetector::~ExpressionDetector() {
  if (classifier_) {
    delete classifier_;
  }
}

/*
 *  @name   load
 *  @brief  Load saved SVM model
 *  @param[in] filename File to load the model from
 */
int ExpressionDetector::Load(const std::string& filename) {
  int error = -1;
  std::string name;
  size_t pos = filename.rfind(".");
  if (pos != std::string::npos) {
    name = filename.substr(0,pos) + ".bin";
  } else {
    name = filename + ".bin";
  }
  std::ifstream input_stream(name.c_str(),
                             std::ios_base::in | std::ios_base::binary);
  if (input_stream.is_open()) {
    // Look for correct header
    int status = LTS5::ScanStreamForObject(input_stream,
                                           LTS5::HeaderObjectType::kExpressionDetector);
    if (status == 0) {
      error = this->Load(input_stream);
    }
    // Close stream
    input_stream.close();
  }
  return error;
}

/*
 *  @name   Load
 *  @brief  Load saved SVM model
 *  @param[in] filename File to load the model from
 */
int ExpressionDetector::Load(std::ifstream& input_stream) {
  int error = -1;
  if (input_stream.good()) {
    // Load
    int dummy = 0;
    // Read label size
    input_stream.read(reinterpret_cast<char*>(&dummy), sizeof(dummy));
    // Set + read label
    this->emotion_label_ = std::string(dummy,'x');
    input_stream.read(const_cast<char*>(emotion_label_.data()),
                      dummy * sizeof(char));
    // Load svm
    if (this->classifier_ != nullptr) {
      delete this->classifier_;
    }
    classifier_ = new BinaryLinearSVMClassifier();
    error = classifier_->Load(input_stream);
    // Feature length
    feature_length_ = classifier_->GetFeatureLength();
    // Done sanity check
    error |= input_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name   Load
 *  @brief  Load saved SVM model
 *  @param[in] model_filename Binary file to load the model from
 *  @param[in] emotion_label  Corresponding emotion label to look for
 *  @return -1 if error, 0 otherwise
 */
int ExpressionDetector::Load(const std::string& model_filename,
                          const std::string& emotion_label) {
  int error = -1;
  std::string name;
  size_t pos = model_filename.rfind(".");
  if (pos != std::string::npos) {
    name = model_filename.substr(0,pos) + ".bin";
  } else {
    name = model_filename + ".bin";
  }
  std::ifstream input_stream(name.c_str(),
                             std::ios_base::in | std::ios_base::binary);
  if (input_stream.is_open()) {
    int status = LTS5::ScanStreamForObject(input_stream,
                                           LTS5::HeaderObjectType::kExpressionDetector);
    if (status == 0) {
      error = this->Load(input_stream, emotion_label);
    }
  }
  return error;
}

/*
 *  @name   Load
 *  @brief  Load saved SVM model
 *  @param[in] input_stream   Binary stream to load the model from
 *  @param[in] emotion_label  Corresponding emotion label to look for
 *  @return -1 if error, 0 otherwise
 */
int ExpressionDetector::Load(std::ifstream& input_stream,
                          const std::string& emotion_label) {
  int error = -1;
  if (input_stream.good()) {
    // Ok can search for specific emotion label
    this->emotion_label_ = "";
    int dummy = 0;
    std::string label = "";
    std::streamoff pos;
    while (input_stream.good() && label != emotion_label) {
      // Read label
      pos = input_stream.tellg();
      input_stream.read(reinterpret_cast<char*>(&dummy), sizeof(dummy));
      label = std::string(dummy,'x');
      input_stream.read(const_cast<char*>(label.data()), dummy);
      if (label != emotion_label) {
        // Rewind stream to ExpressionDetector header to be able to jump to the
        // next one.
        pos -= 2 * sizeof(int);
        input_stream.seekg(pos,std::ios_base::beg);
        //Read object type + size
        int object_type = 0;
        int object_size = 0;
        input_stream.read(reinterpret_cast<char*>(&object_type),
                          sizeof(object_type));
        input_stream.read(reinterpret_cast<char*>(&object_size),
                          sizeof(object_size));
        // Fast forward stream to next obecj
        input_stream.seekg(object_size,std::ios_base::cur);
        LTS5::ScanStreamForObject(input_stream,
                                  LTS5::HeaderObjectType::kExpressionDetector);
      }
    }
    // Done searching, result ok ?
    if (label == emotion_label) {
      // Found correct detector
      this->emotion_label_ = label;
      // Load classifier
      if (this->classifier_) {
        delete this->classifier_;
      }
      classifier_ = new BinaryLinearSVMClassifier();
      int status = LTS5::ScanStreamForObject(input_stream,
                                             LTS5::HeaderObjectType::kBinaryLinearSvm);
      if (status == 0) {
        error = classifier_->Load(input_stream);
        // Feature length
        feature_length_ = classifier_->GetFeatureLength();
        // Done sanity check
        error |= input_stream.good() ? 0 : -1;
      }
    }
  }
  return error;
}

/*
 *  @name   Write
 *  @brief  Dump object into the given binary stream
 *  @param[in] output_stream  Output stream to file
 *  @return -1 if error, 0 otherwise
 */
int ExpressionDetector::Write(std::ofstream& output_stream) const {
  int error = -1;
  if (output_stream.good()) {
    // Can write,
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kExpressionDetector);
    int obj_size = this->ComputeObjectSize();
    // Start to write
    output_stream.write(reinterpret_cast<const char*>(&obj_type),
                        sizeof(obj_type));
    output_stream.write(reinterpret_cast<const char*>(&obj_size),
                        sizeof(obj_size));
    // Label length
    int label_length = static_cast<int>(emotion_label_.length());
    output_stream.write(reinterpret_cast<const char*>(&label_length),
                        sizeof(label_length));
    // Label
    output_stream.write(reinterpret_cast<const char*>(emotion_label_.data()),
                        label_length);
    // Classifier
    error = classifier_->Save(output_stream);
    // Done sanity check
    error |= output_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name ComputeObjectSize
 *  @brief  Compute the object size in bytes
 *  @return Object memory size
 */
int ExpressionDetector::ComputeObjectSize(void) const {
  int size = static_cast<int>(emotion_label_.length());
  size += 3 * sizeof(int); // Label length + svm header + feature_length
  size += classifier_->ComputeObjectSize();
  return size;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Predict
 *  @brief  Predict emotion class
 *  @param[in] features Sample to classify
 *  @return emotion class
 */
double ExpressionDetector::Predict(const cv::Mat& features) const {
  //Predict
  return this->classifier_->Classify(features);
}

}  // namepsace LTS5
