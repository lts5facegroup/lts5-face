/**
 *  @file   expression_analysis_trainer.cpp
 *  @brief  Training for detection of facial emotion class definition
 *  @author Ecabert Christophe
 *  @date   20/06/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <vector>
#include <algorithm>
#include <fstream>
#include <random>
#include <chrono>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/facial_analysis/expression_analysis_trainer.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/classifier/crossvalidation.hpp"
#include "lts5/classifier/pca.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   ExpressionAnalysisTrainer
 *  @brief  Constructor
 */
ExpressionAnalysisTrainer::ExpressionAnalysisTrainer(void) :
initialized_(false),
face_tracker_(nullptr),
pose_model_(nullptr) {
}

/*
 *  @name   ~ExpressionAnalysisTrainer
 *  @brief  Constructor
 */
ExpressionAnalysisTrainer::~ExpressionAnalysisTrainer(void) {
  // Release all trainer used
  for (int i = 0; i < expression_detector_trainer_.size(); ++i) {
    delete expression_detector_trainer_[i];
  }
  if (face_tracker_ != nullptr) {
    delete face_tracker_;
  }
  if (pose_model_ != nullptr) {
    delete pose_model_;
  }
}

/*
 *  @name   Load
 *  @brief  Load face tracker and pose normalization object
 *  @param[in]  tracker_filename   Face tracker model
 *  @param[in]  face_detector_model Face detector configuration
 *  @param[in]  pose_filename      Point distribution model
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysisTrainer::Load(const std::string& tracker_filename,
                                 const std::string& face_detector_model,
                                 const std::string& pose_filename){
  int error = -1;
  const bool tracker_is_binary = (tracker_filename.rfind(".bin") !=
                                  std::string::npos);
  const bool pose_is_binary = (pose_filename.rfind(".bin") !=
                               std::string::npos);
  std::ios_base::openmode flag_tracker = (tracker_is_binary ?
                                          std::ios_base::in|std::ios_base::binary :
                                          std::ios_base::in);
  std::ios_base::openmode flag_pose = (tracker_is_binary ?
                                       std::ios_base::in|std::ios_base::binary :
                                       std::ios_base::in);
  std::ifstream tracker_stream(tracker_filename.c_str(),flag_tracker);
  std::ifstream pose_stream(pose_filename.c_str(),flag_pose);
  if (tracker_stream.good() && pose_stream.good()) {
    int status = 0;
    if (tracker_is_binary) {
      status = LTS5::ScanStreamForObject(tracker_stream, LTS5::HeaderObjectType::kSdm);
    }
    if (pose_is_binary) {
      status |= LTS5::ScanStreamForObject(pose_stream,
                                          LTS5::HeaderObjectType::kPointDistributionModel);
    }
    if (status == 0) {
      error = this->Load(tracker_stream,
                         tracker_is_binary,
                         face_detector_model,
                         pose_stream,
                         pose_is_binary);
    }
  }
  return error;
}

/*
 *  @name   Load
 *  @brief  Load face tracker and pose normalization object
 *  @param[in]  tracker_stream      Face tracker model
 *  @param[in]  tracker_is_binary   Indicate face tracker is binary
 *  @param[in]  face_detector_model Face detector configuration
 *  @param[in]  pose_stream         Point distribution model
 *  @param[in]  tracker_is_binary   Indicate pose stream is binary
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysisTrainer::Load(std::ifstream& tracker_stream,
                                    const bool&  tracker_is_binary,
                                    const std::string& face_detector_model,
                                    std::ifstream& pose_stream,
                                    const bool& pose_is_binary) {
  int error = -1;
  if (tracker_stream.good() && pose_stream.good()) {
    // Both streams are Ok, load sdm first
    if (face_tracker_ == nullptr) {
      face_tracker_ = new LTS5::SdmTracker();
    }
    error = face_tracker_->Load(tracker_stream,
                                tracker_is_binary,
                                face_detector_model);
    // Pdm
    if (pose_model_ == nullptr) {
      pose_model_ = new LTS5::PointDistributionModel();
    }
    error |= pose_model_->Load(pose_stream, pose_is_binary);
    // Properly initialized ?
    initialized_ = (error == 0);
  }
  return error;
}

#pragma mark -
#pragma mark Train

/*
 *  @name Preprocess
 *  @fn int Preprocess(const std::string& data_filename,
                       const std::string& feature_filename)
 *  @brief  Run preprocessing and extract feature
 *  @param[in]  data_filename         Input image candidate
 *  @param[in]  feature_filename      Path where feature will be stored
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysisTrainer::Preprocess(const std::string& data_filename,
                                          const std::string& feature_filename) {
  int error = -1;
  // Init holder
  this->InitializeInternalContainer();
  // Load candidate data for source/target
  error = this->LoadCandidateData(data_filename,
                                  &candidate_,
                                  &n_available_sample_);
  if (!error) {
    // Detect landmark and remove un-reliable candidate/node if not defined
    this->DetectLandmark(data_filename, &candidate_, &n_usable_sample_);
    // Extract features
    error = this->ExtractFeature(feature_filename,
                                 n_usable_sample_,
                                 candidate_,
                                 &sift_features_);
  }
  return error;
}

/*
 *  @name   Train
 *  @fn int Train(const std::string& data_filename,
                 const std::string& feature_filename,
                 const double retain_variance,
                 const BinaryLinearSVMTraining& svm_type,
                 const EmotionLabels& emotion_labels)
 *  @brief  Train detectors given a set of annotations/labels
 *  @param[in]  data_filename     File holding image + annotation
 *  @param[in]  feature_filename  File holding raw feature data
 *  @param[in]  retain_variance   Amount of variance to retain
 *  @param[in]  svm_type          Type of optimization used in svm training
 *  @param[in]  scoring_type      Type of metric to use for scoring performance
 *  @param[in]  emotion_labels    Name of the different emotions
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysisTrainer::Train(const std::string& data_filename,
                                     const std::string& feature_filename,
                                     const double retain_variance,
                                     const BinaryLinearSVMTraining& svm_type,
                                     const ScoringType scoring_type,
                                     const EmotionLabels& emotion_labels) {
  int error = 0;
  if (initialized_) {
    // Load candidate
    if (candidate_.empty()) {
      error |= this->LoadCandidateData(data_filename,
                                       &candidate_,
                                       &n_available_sample_);
      // Clean up entries
      error |= this->CleanUpCandidateData(&candidate_,
                                          &n_usable_sample_);
    }
    // Load extracted features
    if (sift_features_.empty()) {
      error |= LTS5::LoadMatFromBin(feature_filename, &sift_features_);
    }
    if (error) {
      std::cout << "Unable to load labels/features, stop." << std::endl;
      return error;
    }
    // Compute PCA Subspace
    this->ComputePcaSubspace(sift_features_, retain_variance);
    // Project data
    cv::Mat projected_sift;
    this->ProjectDataSample(sift_features_, &projected_sift);
    // Compute normalization factor
    this->ComputeNormalization(projected_sift);
    // Normalize
    this->NormalizeDataSample(projected_sift);
    // Put back into
    projected_sift.copyTo(sift_features_);
    projected_sift.release();

    // Data ready to train classifier
    int idx = 0;
    for (auto &emotion : emotion_labels) {  // Access by reference, no copy
      EmotionType emotion_type = emotion.first;
      std::string emotion_name = emotion.second;
      auto start = std::chrono::system_clock::now();
      std::cout << "\t-----------------------------------------" << std::endl;
      std::cout << "\tStart to train emotion : " << emotion_name << std::endl;
      cv::Mat src_labels;
      this->PrepareDataSample(emotion_type,
                              candidate_,
                              n_usable_sample_,
                              &src_labels);
      // Cross valid to estimate svm parameter
      if (expression_detector_trainer_.size() < emotion_labels.size()) {
        expression_detector_trainer_.push_back(
                new LTS5::ExpressionDetectorTrainer(emotion_name));
      }
      expression_detector_trainer_[idx]->Train(sift_features_,
                                               src_labels,
                                               svm_type,
                                               scoring_type);

      idx++;
      // Timing
      auto stop = std::chrono::system_clock::now();
      auto delta_t = std::chrono::duration_cast<std::chrono::seconds>(
              stop - start);
      std::cout << "\tTook : " << delta_t.count() << "s" << std::endl;
    }
  }
  std::cout << "Done ..." << std::endl;
  return error;
}

/*
 *  @name   Save
 *  @brief  Save model into binary file
 *  @param[in]  filename  Name of the file where to save model(s)
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysisTrainer::Save(const std::string& filename) {
  int error = -1;
  if (initialized_) {
    // Check name validity, and add .bin extension if none provided. If there
    // is already one, remove it and append the new one.
    std::string final_name;
    int position_dot = static_cast<int>(filename.rfind("."));
    if (position_dot == std::string::npos) {
      // No ext
      final_name = filename + ".bin";
    } else {
      // Existing extension -> remove it and add .bin
      final_name = filename.substr(0, position_dot) + ".bin";
    }
    // Write binary file
    std::ofstream output_stream(final_name,
                                std::ios_base::out | std::ios_base::binary);
    if (output_stream.is_open()) {
      error = this->Save(output_stream);
      output_stream.close();
      std::string msg = error == 0 ? "Success" : "Fail";
      std::cout << "Save with " << msg << std::endl;
    } else {
      std::cout << "Cannot open file" << final_name << std::endl;
    }
  }
  return error;
}

/*
 *  @name   Save
 *  @brief  Save model into binary file
 *  @param[in]  output_stream  Stream to binary file where to save model(s)
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysisTrainer::Save(std::ofstream& output_stream) {
  int error = -1;
  if (output_stream.good()) {
    // Compute obj size
    // -------------------------------
    int obj_size = configuration_.ComputeHeaderSize();
    // Header + size
    obj_size += 2 * sizeof(int);
    obj_size += pose_model_->ComputeObjectSize();
    // Pca
    obj_size += 3 * sizeof(int);
    obj_size += feature_space_.total() * feature_space_.elemSize();
    // Mean
    obj_size += 3 * sizeof(int);
    obj_size += (feature_normalization_mean_.total() *
                 feature_normalization_mean_.elemSize());
    // Std
    obj_size += 3 * sizeof(int);
    obj_size += (feature_normalization_std_.total() *
                 feature_normalization_std_.elemSize());
    // Detectors
    obj_size += sizeof(int);
    for (int i = 0; i < expression_detector_trainer_.size(); ++i) {
      obj_size += 2 * sizeof(int);
      obj_size += expression_detector_trainer_[i]->ComputeObjectSize();
    }
    // Write
    // -------------------------------
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kExpressionAnalysis);
    output_stream.write(reinterpret_cast<const char*>(&obj_type),
                        sizeof(obj_type));
    output_stream.write(reinterpret_cast<const char*>(&obj_size),
                        sizeof(obj_size));
    // Config
    error = configuration_.WriteParameterHeader(output_stream);
    // PDM
    error |= pose_model_->Write(output_stream);
    // PCA
    error |= LTS5::WriteMatToBin(output_stream, feature_space_);
    // Mean
    error |= LTS5::WriteMatToBin(output_stream, feature_normalization_mean_);
    // Std
    error |= LTS5::WriteMatToBin(output_stream, feature_normalization_std_);
    // Detector
    int n_detector = static_cast<int>(expression_detector_trainer_.size());
    output_stream.write(reinterpret_cast<const char*>(&n_detector),
                        sizeof(n_detector));
    for (int i = 0; i < n_detector; ++i) {
      error |= expression_detector_trainer_[i]->Save(output_stream);
    }
    // Done
    error |= output_stream.good() ? 0 : -1;
  }
  return error;
}

#pragma mark -
#pragma mark Private

/*
 *  @name   InitializeInternalContainer
 *  @brief  Initialize internal container
 */
void ExpressionAnalysisTrainer::InitializeInternalContainer(void){
  // Reset vector
  candidate_.clear();
  // Trainer ?
  if (expression_detector_trainer_.size() > 0) {
    for (int i = 0; i < expression_detector_trainer_.size(); ++i) {
      delete expression_detector_trainer_[i];
    }
    expression_detector_trainer_.clear();
  }
}

/*
 *  @name   LoadCandidateData
 *  @fn int LoadCandidateData(const std::string& data_filename,
                               CandidateList_t* list,
                               int* n_sample)
 *  @brief  Load candidate data (image location + ground truth)
 *  @param[in]  data_filename   File holding candidate data (image location +
 *                              annotation)
 *  @param[out] list            List of candidate to populate
 *  @param[out] n_sample        Number of sample loaded
 *  @return -1 if error, 0 otherwise.
 */
int ExpressionAnalysisTrainer::LoadCandidateData(const std::string& data_filename,
                                                 CandidateList_t* list,
                                                 int* n_sample) {
  int error = -1;
  std::cout << "Load candidate data ..." << std::endl;
  std::ifstream in_stream(data_filename.c_str());
  if (in_stream.is_open()) {
    // Ok, read first line
    std::stringstream str_stream;
    std::string line;
    int id = -1;
    int selection = -1;
    std::string image_path;
    int emotion_label = -1;
    LTS5::ExpressionAnalysisTrainer::CandidateNode node;
    *n_sample = 0;
    // Loop over all file entry
    do {
      // Read line
      std::getline(in_stream, line);
      if (line.length() > 0) {
        // Init stream
        str_stream.str(line);
        // Read data
        str_stream >> id >> image_path >> emotion_label >> selection;
        str_stream.clear();
        // Set node members
        node.SetEntry(id, image_path, emotion_label, selection);
        // Add node
        list->push_back(node);
        // Count
        *n_sample += 1;
      }
    } while (!in_stream.eof());
    // Reached the end
    std::cout << "available samples : " << *n_sample << std::endl;
    error = 0;
  }
  return error;
}

/*
 *  @name CleanUpCandidateData
 *  @fn int CleanUpCandidateData(void)
 *  @brief  Remove un-selected sample
 *  @param[in,out]  list            List of candidate to clean
 *  @param[out]     n_usable_sample Number of sample usable after cleaning
 *                                  (Should be the size of #list)
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysisTrainer::CleanUpCandidateData(CandidateList_t* list,
                                                    int* n_usable_sample) {
  auto candidate_it = list->begin();
  int sample = 0;
  int n_drop = 0;
  int val = 0;
  int available_sample = static_cast<int>(list->size());
  while (candidate_it != list->end()) {
    // Loop over every image
    val++;
    if (candidate_it->k_selection == 0) {
      std::cout << "Drop : " << candidate_it->k_image_location << std::endl;
      candidate_it = list->erase(candidate_it);
      n_drop++;
    } else if(candidate_it->k_selection == -1) {
      return -1;
    } else {
      ++sample;
      // Move to next candidate
      ++candidate_it;
    }
  }
  *n_usable_sample = sample;
  assert((sample + n_drop) == available_sample);
  // Ok
  return 0;
}

/*
 *  @name   DetectLandmark
 *  @brief  Detect landmark for each CandidateNode and remove if entry in case
 *          of bad detection
 *  @param[in]      data_filename Path to original data candidate
 *  @param[in,out]  list          List of candidate where image are stored
 *  @param[out]     n_sample      Number of usable sample
 */
void ExpressionAnalysisTrainer::DetectLandmark(const std::string& data_filename,
                                               CandidateList_t* list,
                                               int* n_sample) {
  // Loop over all node
  std::cout << "Detecting facial landmarks ..." << std::endl;
  auto start = std::chrono::system_clock::now();
  auto candidate_it = list->begin();
  cv::Mat shape;
  int error = -1;
  *n_sample = 0;
  // Prepare annotation file
  std::ofstream out_stream(data_filename.c_str());
  while (candidate_it != list->end()) {
    // Loop over every image
    // Already treated ?
    if (candidate_it->k_selection == -1 || candidate_it->k_selection == 1) {
      // Load image
      cv::Mat image = cv::imread(candidate_it->k_image_location,
                                 cv::IMREAD_UNCHANGED);
      if (image.channels() != 1) {
        cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
      }
      // Detect landmark
      error = face_tracker_->Detect(image, &shape);
      if (!error && candidate_it->k_selection == -1) {
        // Ask user to confirm selection
        cv::Mat canvas = face_tracker_->draw_shape(image,
                                                   shape,
                                                   CV_RGB(0, 255, 0));
        std::cout << candidate_it->k_image_location << ", is valid ? y/n" << std::endl;
        cv::imshow("Shape", canvas);
        char c = -1;
        do {
          c = static_cast<char>(cv::waitKey());
        } while (c != 'y' && c != 'n');
        // Valid ?
        candidate_it->k_selection = c == 'y' ? 1 : 0;
      } else if (error) {
        candidate_it->k_selection = 0;
      }
    }
    // Save
    out_stream << candidate_it->k_id << " " << candidate_it->k_image_location <<
    " " << candidate_it->k_ground_truth << " " << candidate_it->k_selection <<
    std::endl;
    // Remove if needed
    if (candidate_it->k_selection == 0) {
      // Remove unselected sample
      std::cout << "Drop : " << candidate_it->k_image_location << std::endl;
      candidate_it = list->erase(candidate_it);
    } else {
      // Add
      candidate_it->k_shape = shape.clone();
      // Count sample
      *n_sample += 1;
      // Go to the next candidate
      ++candidate_it;
    }
  }
  // Done
  out_stream.close();
  auto stop = std::chrono::system_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
  std::cout << "  ... done in : " << elapsed.count() << "s" << std::endl;
  std::cout << "Usable samples : " << *n_sample << std::endl;
}

/*
 *  @name  ExtractFeature
 *  @fn void ExtractFeature(void)
 *  @brief  Extract sift features for training and testing set
 *  @param[in]  feature_filename  Place where to save feature
 *  @param[in]  n_feature         Number of feature to extract
 *  @param[in]  list              List of candidate holding shape information
 *  @param[out] feature           Extracted sift features
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysisTrainer::ExtractFeature(const std::string& feature_filename,
                                              const int n_feature,
                                              const CandidateList_t& list,
                                              cv::Mat* feature) {
  std::cout << "Extracting feature for training set ..." << std::endl;
  auto start = std::chrono::system_clock::now();
  // Features extraction
  int idx = 0;
  auto candidate_it = list.begin();
  while (candidate_it != list.end()) {
    // Loop over all image for this candidate
    // Load image
    cv::Mat image = cv::imread(candidate_it->k_image_location,
                               cv::IMREAD_UNCHANGED);
    if (image.channels() > 1) {
      cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
    }
    // Normalize the image
    cv::Mat norm_shape;
    cv::Mat norm_image;
    int n_pts = std::max(candidate_it->k_shape.rows,
                         candidate_it->k_shape.cols) / 2;
    if (n_pts != 68) {
      pose_model_->NormSDMImage_intra(image,
                                      candidate_it->k_shape,
                                      &norm_image,
                                      &norm_shape);
    } else {
      pose_model_->NormSDMImage(image,
                                candidate_it->k_shape,
                                true,
                                &norm_image,
                                &norm_shape);
    }
    // ---------------------------------------------------------
    // Illumination normalization
#if defined(USE_MEAN_NORMALIZATION)
    cv::Mat img32;
    norm_image.convertTo(img32, CV_32FC1);
    const int n_elem = static_cast<int>(img32.total());
    float* img_ptr = reinterpret_cast<float*>(img32.data);
    float sum = 0;
    for (int i = 0; i < n_elem; ++i) {
      sum += *img_ptr++;
    }
    const float img_mean = sum/static_cast<float>(n_elem);
    img_ptr = reinterpret_cast<float*>(img32.data);
    for (int i = 0; i < n_elem; ++i) {
      *img_ptr++ /= img_mean;
    }
    // TODO:(Christophe) Check if it can be avoid in future
    // Copy back into source Image
    img32.copyTo(norm_image);
#elif defined(USE_HIST_EQUA_NORMALIZATION)
    // Apply histogram equalization first
    cv::equalizeHist(norm_image, norm_image);
#endif
    // Init holder
    if (feature->empty()) {
      int shape_size = std::max(norm_shape.rows, norm_shape.cols) / 2;
      feature->create(n_feature,
                      shape_size * configuration_.sift_configuration.sift_dim,
                      CV_64FC1);
    }
    // Get key point
    std::vector<cv::KeyPoint> key_points;
    key_points = LTS5::KeypointsFromShape(norm_shape,
                                          configuration_.sift_size);
    // Compute sift
    cv::Mat sift_feature;
    LTS5::SSift::ComputeDescriptor(norm_image,
                                   key_points,
                                   configuration_.sift_configuration,
                                   &sift_feature);
    sift_feature = sift_feature.reshape(sift_feature.channels(), 1);
    sift_feature.convertTo(feature->row(idx), CV_64FC1);
    // Done
    ++idx;
    // Move to next candidate
    ++candidate_it;
  }
  assert(idx == n_feature);
  // Save raw features
  int error = LTS5::SaveMatToBin(feature_filename, *feature);
  // Feedback
  auto stop = std::chrono::system_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
  std::cout << "  ... done in : " << elapsed.count() << "s" << std::endl;
  return error;
}

/*
 *  @name ComputePcaSubspace
 *  @fn void ComputePcaSubspace(const double retain_variance)
 *  @brief  Compute pca feature subspace
 *  @param[in]  data              DAta to perform PCA on
 *  @param[in]  retain_variance   Amount of variance to retain
 */
void ExpressionAnalysisTrainer::ComputePcaSubspace(const cv::Mat& data,
                                                const double retain_variance) {
  std::cout << "Compute pca subspace ..." << std::endl;
  auto start = std::chrono::system_clock::now();
  // Compute PCA with all sample
  cv::Mat eigen_val;
  LTS5::ComputePca(data,
                   retain_variance,
                   kLapack,
                   &feature_space_,
                   &eigen_val);
  std::cout << "Pca space dimension : " << feature_space_.rows << std::endl;


  /*
  // Normalization
  cv::Mat pca_features;
  LTS5::LinearAlgebra<double>::Gemm(training_sift_features_,
                                    TransposeType::kNoTranspose,
                                    1.0,
                                    feature_space_,
                                    TransposeType::kNoTranspose, 0.0, &pca_features);
  // Setup matrices
  feature_normalization_mean_.create(1, pca_features.cols, pca_features.type());
  feature_normalization_std_.create(1, pca_features.cols, pca_features.type());
  feature_normalization_std_.setTo(0.0);
  // Compute mean
  cv::reduce(pca_features, feature_normalization_mean_, 0, CV_REDUCE_AVG);
  // Compute stdev
  cv::Mat row_i;
  for (int i = 0; i < pca_features.rows; ++i) {
    // Remove mean
    row_i = pca_features.row(i) - feature_normalization_mean_;
    cv::pow(row_i, 2, row_i);
    feature_normalization_std_ += row_i;
  }
  feature_normalization_std_ /= (pca_features.rows - 1);
  cv::pow(feature_normalization_std_, 0.5, feature_normalization_std_);
   */
  auto stop = std::chrono::system_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
  std::cout << " ... done in : " << elapsed.count() << "s" << std::endl;
}

/*
 *  @name   PrepareDataSample
 *  @fn void PrepareDataSample(const EmotionType& emotion_label,
 const CandidateList_t& candidates,
 const int n_label,
 cv::Mat* labels)
 *  @brief  Generate the labels for a given emotion label
 *  @param[in]  emotion_label   Emotion label
 *  @param[in]  candidates      List of candidate to extract label from
 *  @param[in]  n_label         Number of label to generate
 *  @param[out] labels          Positive example
 */
void ExpressionAnalysisTrainer::PrepareDataSample(const EmotionType& emotion_label,
                                                  const CandidateList_t& candidates,
                                                  const int n_label,
                                                  cv::Mat* labels) {
  // Reset container
  labels->create(n_label, 1, CV_32FC1);
  // Fill
  int cnt = 0;
  auto candidate_it = candidates.begin();
  for (; candidate_it != candidates.end(); ++candidate_it) {
    // Loop over all annotation
    if (static_cast<int>(emotion_label) != -1) {
      if (candidate_it->k_ground_truth == emotion_label) {
        // Pos sample
        labels->at<float>(cnt) = 1.0f;
      } else {
        // Neg sample
        labels->at<float>(cnt) = -1.0f;
      }
    } else {
      labels->at<float>(cnt) = static_cast<float>(candidate_it->k_ground_truth);
    }
    cnt++;
  }
  assert(cnt == n_label);
}

/*
 *  @name   ProjectDataSample
 *  @brief  Project feature into subspace for a given size
 *  @param[in]  data              Data samples
 *  @param[out] projected_data    Projected data into subspace + normalized if
 *                                asked
 */
void ExpressionAnalysisTrainer:: ProjectDataSample(const cv::Mat& data,
                                                cv::Mat* projected_data) {
  using TransposeType = LTS5::LinearAlgebra<double>::TransposeType;
  // Project
  projected_data->create(data.rows, feature_space_.cols, data.type());
  LTS5::LinearAlgebra<double>::Gemm(data,
                                    TransposeType::kNoTranspose,
                                    1.0,
                                    feature_space_,
                                    TransposeType::kTranspose,
                                    0.0,
                                    projected_data);
}

/*
 *  @name ComputeNormalization
 *  @fn void ComputeNormalization(const cv::Mat& data)
 *  @brief  Compute normalization factor for data sample (z-score)
 *  @param[in]  data  Data to use to estimate normalization
 */
void ExpressionAnalysisTrainer::ComputeNormalization(const cv::Mat& data) {
  // Compute mean
  LTS5::LinearAlgebra<double>::Mean(data, 1, &feature_normalization_mean_);
  // Compute std
  cv::Mat row_sqr;
  feature_normalization_std_.create(1, data.cols, data.type());
  feature_normalization_std_.setTo(0.0);
  // Sum(x_i^2)
  for (int i = 0; i < data.rows; ++i) {
    cv::pow(data.row(i), 2.0, row_sqr);
    feature_normalization_std_ += row_sqr;
  }
  // N * mean^2
  cv::pow(feature_normalization_mean_, 2.0, row_sqr);
  row_sqr *= data.rows;
  // Sum(x_i^2) - N * mean^2
  row_sqr = (feature_normalization_std_ - row_sqr) / (data.rows -1);
  // Stdev
  cv::pow(row_sqr, 0.5, feature_normalization_std_);
}

/*
 *  @name NormalizeDataSample
 *  @fn void NormalizeDataSample(const cv::Mat& data)
 *  @brief  Apply normalization to data sample (z-score)
 *  @param[in,out]  data  Data to normalize
 */
void ExpressionAnalysisTrainer::NormalizeDataSample(const cv::Mat& data) {
  // Loop over all sample
  for (int i = 0; i < data.rows; ++i) {
    data.row(i) = data.row(i) - feature_normalization_mean_;
    data.row(i) = data.row(i) / feature_normalization_std_;
  }
}

#pragma mark -
#pragma mark Testing / Validation

/*
 *  @name   LoadTestingConfiguration
 *  @brief  Load data from testing configuration file
 *  @param[in]  config  Testing configuration file
 *  @param[out] image   Image paths
 *  @param[out] label   Labels
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysisTrainer::LoadTestingConfiguration(const std::string& config,
                                                     std::vector<std::string>* image,
                                                     std::vector<EmotionType>* label) {
  int error = -1;
  std::ifstream in_stream(config.c_str());
  if (in_stream.is_open()) {
    // Ok, read first line
    std::stringstream str_stream;
    std::string line;
    std::string image_path;
    int emotion_label;
    while (in_stream.good()) {
      // Read line
      std::getline(in_stream, line);
      if (line.length() > 0) {
        // Read stuff
        str_stream.str(line);
        // Read data
        str_stream >> image_path >> emotion_label;
        image->push_back(image_path);
        label->push_back(static_cast<EmotionType>(emotion_label));
        // Clear
        str_stream.clear();
      }
    }
    error = 0;
  }
  return error;
}


/*
 *  @name   TestExpressionAnalysisModule
 *  @brief  Test the performance of an ExpressionAnalysis module
 *  @param[in]  emotion_model       ExpressionAnalysis model
 *  @param[in]  tracker_model       Face tracker model
 *  @param[in]  face_detector_model Viola-Jones face detector
 *  @param[in]  test_config         List of image/label
 *  @param[in]  output_filename     File with ground truth/ prediction are
 *                                  dumped
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysisTrainer::TestExpressionAnalysisModule(const std::string& emotion_model,
                                                      BaseFaceTracker<cv::Mat, cv::Mat>* face_tracker,
                                                      const std::string& test_config,
                                                      const std::string& output_filename) {
  int error = -1;
  // Load configuration
  std::vector<std::string> image_path;
  std::vector<EmotionType> labels;
  std::vector<std::string> emotion_labels;
  std::vector<double> scores;
  int correct_pred = 0;
  error = ExpressionAnalysisTrainer::LoadTestingConfiguration(test_config,
                                                           &image_path,
                                                          &labels);

  // Load stuff
  ExpressionAnalysis* expression_analysis = new ExpressionAnalysis();
  error |= expression_analysis->Load(emotion_model);
  expression_analysis->get_expression_labels(&emotion_labels);
  // Open output file
  std::ofstream out_stream(output_filename.c_str());
  error |= out_stream.is_open() ? 0 : -1;
  if (error == 0) {
    // plot label and value
    out_stream << emotion_labels.size() << std::endl;
    for (int i = 0; i < emotion_labels.size(); ++i) {
      out_stream << (0x01<<i) << " " << emotion_labels[i] << std::endl;
    }

    // Loop over all image
    for (int i = 0 ; i < image_path.size() ; ++i) {
      // Load image
      std::cout << image_path[i] << std::endl;
      cv::Mat image = cv::imread(image_path[i],cv::IMREAD_UNCHANGED);
      if (image.empty()) {
        std::cout << "Unable to load image : " << image_path[i] << std::endl;
        continue;
      }
      if (image.channels() != 1) {
        cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
      }
      // Align
      cv::Mat shape;
      int status = face_tracker->Detect(image, &shape);
      if ((status == 0 && !shape.empty())) {
        // Predict
        expression_analysis->Predict(image, shape, &scores);
        // Find max index
        int idx = static_cast<int>(std::distance(scores.begin(),
                                                 std::max_element(scores.begin(),
                                                                  scores.end())));
        std::cout << "Predicted : " << emotion_labels[idx] << std::endl;
        out_stream << labels[i] << " " << (0x01<<idx) << " "
        <<image_path[i] << " "  << emotion_labels[idx]<<  std::endl;
        // Correct label ?
        if ((labels[i] >> idx) == 0x01) {
          correct_pred++;
        }
      } else {
        out_stream << "NaN NaN " << image_path[i] <<  std::endl;
      }
    }
    // Done
    out_stream.close();
    // Info
    std::cout << "Correct classification : " << correct_pred << "/" <<
    image_path.size() << std::endl;
  }

  if (expression_analysis) {
    delete expression_analysis;
  }
  return error;
}

#pragma mark -
#pragma mark Converter utility function

/*
 *  @name   ConvertModelFronCharToBin
 *  @fn static int ConvertModelFromCharToBin(const ExpressionAnalysis::ExpressionAnalysisParameters& params,
                               const PointDistributionModel& pdm,
                               const std::string& feature_space_filename,
                               const std::string& normalization_mean_filename,
                               const std::string& normalization_std_filename,
                               const std::string& detector_models,
                               const std::string& model_filename)
 *  @brief  Convert char model (emotion detector) into binary one
 *  @param[in]  params                        Module parameters
 *  @param[in]  pdm                           Point distribution model
 *  @param[in]  feature_space_filename        PCA space
 *	@param[in]  normalization_mean_filename   Normalization file (Mean)
 *	@param[in]  normalization_std_filename    Normalization file (std)
 *  @param[in]  detector_models               ExpressionDetector binary file
 *  @param[in]  model_filename                Filename where to put model
 */
template<typename T>
int ExpressionAnalysisTrainer::
  ConvertModelFromCharToBin(const ExpressionAnalysis::ExpressionAnalysisParameters& params,
                            const PointDistributionModel& pdm,
                            const std::string& feature_space_filename,
                            const std::string& normalization_mean_filename,
                            const std::string& normalization_std_filename,
                            const std::string& detector_models,
                            const std::string& model_filename) {
  int error = -1;
  size_t pos = detector_models.rfind(".bin");
  if (pos != std::string::npos) {
    // Ok, correct extension
    std::string filename;
    pos = model_filename.rfind(".");
    if (pos != std::string::npos) {
      filename = model_filename.substr(0,pos) + ".bin";
    } else {
      filename = model_filename + ".bin";
    }
    // Open stream
    std::ofstream out_stream(filename.c_str(),
                             std::ios_base::out | std::ios_base::binary);
    if (out_stream.is_open()) {
      error = (LTS5::ExpressionAnalysisTrainer::
               ConvertModelFromCharToBin<T>(params,
                                            pdm,
                                            feature_space_filename,
                                            normalization_mean_filename,
                                            normalization_std_filename,
                                            detector_models,
                                            out_stream));
      out_stream.close();
    }
  }
  return error;
}

/*
 *  @name   ConvertModelFronCharToBin
 *  @fn static int ConvertModelFromCharToBin(const ExpressionAnalysis::ExpressionAnalysisParameters& params,
                               const PointDistributionModel& pdm,
                               const std::string& feature_space_filename,
                               const std::string& normalization_mean_filename,
                               const std::string& normalization_std_filename,
                               const std::string& detector_models,
                               std::ofstream& output_stream);
 *  @brief  Convert char model (emotion detector) into binary one
 *  @param[in]  params                        Module parameters
 *  @param[in]  pdm                           Point distribution model
 *  @param[in]  feature_space_filename        PCA space
 *	@param[in]  normalization_mean_filename   Normalization file (Mean)
 *	@param[in]  normalization_std_filename    Normalization file (std)
 *  @param[in]  detector_models               ExpressionDetector binary file
 *  @param[in]  output_stream                 Stream where to put model
 */
template<typename T>
int ExpressionAnalysisTrainer::
ConvertModelFromCharToBin(const ExpressionAnalysis::ExpressionAnalysisParameters& params,
                          const PointDistributionModel& pdm,
                          const std::string& feature_space_filename,
                          const std::string& normalization_mean_filename,
                          const std::string& normalization_std_filename,
                          const std::string& detector_models,
                          std::ofstream& output_stream) {
  int error = -1;
  // Open stream
  std::ifstream in_stream(detector_models.c_str(),
                          std::ios_base::in | std::ios_base::binary);
  std::vector<LTS5::ExpressionDetector*> expression_detector;

  if (in_stream.is_open()) {
    // Ok can read file
    // Scan stream for ExpressionDetector object
    std::cout << "Load ExpressionDetectors ... " << std::endl;
    int status = 0;
    int i = 0;
    std::string label;
    while (status == 0 && in_stream.good()) {
      error = 0;
      status = ScanStreamForObject(in_stream, HeaderObjectType::kExpressionDetector);
      if (status == 0) {
        // Load emotion detector
        expression_detector.push_back(new LTS5::ExpressionDetector());
        error |= expression_detector[i]->Load(in_stream);
        // Label
        label = expression_detector[i]->get_emotion_label();
        ++i;
        std::cout << "Emotion found : " << label << std::endl;
      }
    }
    // Reach the end of input stream
    if (expression_detector.size() > 0) {
      // Some detectors have been loaded, can loaded other stuff
      // Feature space
      std::cout << "Load feature normalization data ... " << std::endl;
      cv::Mat feature_space;
      error = LTS5::ReadMatFromCSV<T>(feature_space_filename, &feature_space);
      // Normalization
      cv::Mat normalization_mean;
      cv::Mat normalization_std;
      error |= LTS5::ReadMatFromCSV<T>(normalization_mean_filename,
                                       &normalization_mean);
      error |= LTS5::ReadMatFromCSV<T>(normalization_std_filename,
                                       &normalization_std);
      // All stuff, error so far ?
      if (error == 0) {
        std::cout << "Normalization loaded with success ... " << std::endl;
        // No, can save new model
        // Compute object size
        int object_type = static_cast<int>(HeaderObjectType::kExpressionAnalysis);
        int object_size = 0;
        // Params
        object_size += params.ComputeHeaderSize();
        // PDM
        object_size += 2 * sizeof(int);
        object_size += pdm.ComputeObjectSize();
        // Feature space
        object_size += feature_space.total() * feature_space.elemSize();
        object_size += normalization_mean.total() * normalization_mean.elemSize();
        object_size += normalization_std.total() * normalization_std.elemSize();
        object_size += 3 * 3 * sizeof(int);  // Matrix header
        // Number of detectors
        object_size += sizeof(int);
        // Size of each detector
        for (i = 0; i < expression_detector.size(); ++i) {
          // Emotion detector Header
          object_size += 2 * sizeof(int);   // Header
          object_size += expression_detector[i]->ComputeObjectSize();
        }
        // Save data, header
        std::cout << "Save data ... " << std::endl;
        output_stream.write(reinterpret_cast<const char*>(&object_type),
                            sizeof(object_type));
        output_stream.write(reinterpret_cast<const char*>(&object_size),
                            sizeof(object_size));
        // Params
        error = params.WriteParameterHeader(output_stream);
        // PDM
        error |= pdm.Write(output_stream);
        // Feature space
        error |= LTS5::WriteMatToBin(output_stream, feature_space);
        // Normalization
        error |= LTS5::WriteMatToBin(output_stream, normalization_mean);
        error |= LTS5::WriteMatToBin(output_stream, normalization_std);
        // Number of detector
        int n_detecor = static_cast<int>(expression_detector.size());
        output_stream.write(reinterpret_cast<const char*>(&n_detecor),
                            sizeof(n_detecor));
        for (i = 0; i < n_detecor; ++i) {
          // Header computation
          error |= expression_detector[i]->Write(output_stream);
        }
        // Clean up
        if (expression_detector.size() > 0) {
          for (i = 0; i < expression_detector.size(); ++i) {
            delete expression_detector[i];
          }
        }
      }
    }
  }
  std::cout << "Done with : " << (error == 0 ? "Success" : "Fail") << std::endl;
  return error;
}

template
int ExpressionAnalysisTrainer::
ConvertModelFromCharToBin<double>(const ExpressionAnalysis::ExpressionAnalysisParameters& params,
                                  const PointDistributionModel& pdm,
                                  const std::string& feature_space_filename,
                                  const std::string& normalization_mean_filename,
                                  const std::string& normalization_std_filename,
                                  const std::string& detector_models,
                                  const std::string& model_filename);
template
int ExpressionAnalysisTrainer::
ConvertModelFromCharToBin<float>(const ExpressionAnalysis::ExpressionAnalysisParameters& params,
                                 const PointDistributionModel& pdm,
                                 const std::string& feature_space_filename,
                                 const std::string& normalization_mean_filename,
                                 const std::string& normalization_std_filename,
                                 const std::string& detector_models,
                                 const std::string& model_filename);

template
int ExpressionAnalysisTrainer::
ConvertModelFromCharToBin<double>(const ExpressionAnalysis::ExpressionAnalysisParameters& params,
                                  const PointDistributionModel& pdm,
                                  const std::string& feature_space_filename,
                                  const std::string& normalization_mean_filename,
                                  const std::string& normalization_std_filename,
                                  const std::string& detector_models,
                                  std::ofstream& output_stream);
template
int ExpressionAnalysisTrainer::
ConvertModelFromCharToBin<float>(const ExpressionAnalysis::ExpressionAnalysisParameters& params,
                                 const PointDistributionModel& pdm,
                                 const std::string& feature_space_filename,
                                 const std::string& normalization_mean_filename,
                                 const std::string& normalization_std_filename,
                                 const std::string& detector_models,
                                 std::ofstream& output_stream);

}  // namespace LTS5
