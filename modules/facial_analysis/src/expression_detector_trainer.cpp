/**
 *  @file   expression_detector_trainer.cpp
 *  @brief  Train model for emotion detection class definition
 *  @author Christophe Ecabert
 *  @date   22/06/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */
#include <numeric>
#include <algorithm>
#include <iostream>

#include "lts5/facial_analysis/expression_detector_trainer.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/classifier/binarylinearsvmclassifier.hpp"
#include "lts5/classifier/crossvalidation.hpp"

namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   ExpressionDetectorTrainer
 *  @brief  Constructor
 */
  ExpressionDetectorTrainer::ExpressionDetectorTrainer() : emotion_label_(""),
  classifier_(nullptr) {}

/*
 *  @name   ExpressionDetectorTrainer
 *  @brief  Constructor
 *  @param[in]  emotion_label Name of the emotion detected
 */
ExpressionDetectorTrainer::ExpressionDetectorTrainer(const std::string& emotion_label) :
  classifier_(nullptr) {
  emotion_label_ = emotion_label;
}

/*
 *  @name   ~ExpressionDetectorTrainer
 *  @brief  Destructor
 */
ExpressionDetectorTrainer::~ExpressionDetectorTrainer() {
  if (classifier_ != nullptr) {
    delete classifier_;
  }
}

#pragma mark -
#pragma mark Train

/*
 *  @name   Train
 *  @brief  Train emotion class
 *  @param[in]  samples  Samples for training
 *  @param[in]  labels   Labels for training, format [1xN] or [Nx1] with
 *                       {-1,1} for negative and positive sample (flt32).
 *  @param[in]  c_coef   SVM's C parameter
 */
void ExpressionDetectorTrainer::Train(const cv::Mat& samples,
                                      const cv::Mat& labels,
                                      const BinaryLinearSVMTraining& svm_type,
                                      const double& c_coef) {
  assert(((labels.cols == 1 && labels.rows > 1) ||
          (labels.cols > 1 && labels.rows == 1)) &&
         labels.type() == CV_32FC1);
  // Split sample based on labels
  const int n_sample = std::max(labels.cols, labels.rows);
  std::vector<cv::Mat> pos_features;
  std::vector<cv::Mat> neg_features;
  for (int i = 0; i < n_sample; ++i) {
    if (labels.at<float>(i) > 0.f) {
      // Pos
      pos_features.push_back(samples.row(i));
    } else {
      // Neg
      neg_features.push_back(samples.row(i));
    }
  }
  // Init svm
  if (classifier_ != nullptr) {
    delete classifier_;
    classifier_ = nullptr;
  }
  classifier_ = new LTS5::BinaryLinearSVMClassifier(c_coef,svm_type);
  // Train
  classifier_->Train(pos_features, neg_features);
}

/*
 *  @name   Train
 *  @fn void Train(const cv::Mat& samples,
 const cv::Mat& labels,
 const BinaryLinearSVMTraining& svm_type)
 *  @brief  Train emotion class doing cross validation on sample to find
 *          best C parameter
 *  @param[in]  samples         Samples for training
 *  @param[in]  labels          Labels for training, format [1xN] or [Nx1]
 *                              with {-1,1} for negative and positive sample
 *                              (flt32).
 *  @param[in]  svm_type  Type of svm
 *  @param[in]  scoring_type  Type of metric to use for scoring performance
 */
void ExpressionDetectorTrainer::Train(const cv::Mat& samples,
                                      const cv::Mat& labels,
                                      const BinaryLinearSVMTraining& svm_type,
                                      const ScoringType scoring_type) {
  using BinaryCrossValidation = BinaryCrossValidation<cv::Mat>;
  assert(((labels.cols == 1 && labels.rows > 1) ||
          (labels.cols > 1 && labels.rows == 1)) &&
         labels.type() == CV_32FC1);
  // Prepare sample
  std::vector<cv::Mat> pos_samples;
  std::vector<cv::Mat> neg_samples;
  const int n_sample = std::max(labels.rows, labels.cols);;
  for (int s = 0; s < n_sample; ++s) {
    if (labels.at<float>(s) > 0.f) {
      pos_samples.push_back(samples.row(s));
    } else {
      neg_samples.push_back(samples.row(s));
    }
  }
  // Cross validation
  double best_c;
  float balance = -1.0;
  balance = BinaryCrossValidation::EstimateLinearCoefficient(pos_samples,
                                                             neg_samples,
                                                             svm_type,
                                                             true,
                                                             scoring_type,
                                                             &best_c,
                                                             nullptr);
  std::cout << "\tPos. balance (neg/pos) : " << balance << std::endl;
  // Train
  std::cout << "\tC : " << best_c << std::endl;
  if (classifier_ == nullptr) {
    classifier_ = new BinaryLinearSVMClassifier(best_c, svm_type);
  } else {
    classifier_->set_c_coef(best_c);
    classifier_->set_svm_type(svm_type);
  }
  classifier_->Train(pos_samples, neg_samples);
}

/*
 *  @name   Save
 *  @brief  Save Detector model into file
 *  @param[in] output_filename File to save the model
 */
int ExpressionDetectorTrainer::Save(const std::string& output_filename) const {
  int error = -1;
  std::string name;
  size_t pos_ext = output_filename.rfind(".");
  if (pos_ext != std::string::npos) {
    // No ext
    name = output_filename.substr(0,pos_ext) + ".bin";
  } else {
    name = output_filename + ".bin";
  }
  // Write binary file
  std::ofstream output_stream(name,
                              std::ios_base::out | std::ios_base::binary);
  if (output_stream.is_open()) {
    error = this->Save(output_stream);
  } else {
    std::cout << "Cannot open output stream " << name << std::endl;
  }
  return error;
}

/*
 *  @name   Save
 *  @brief  Save Detector model into binary stream
 *  @param[in] out_stream Binary stream where to save the model
 */
int ExpressionDetectorTrainer::Save(std::ofstream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Ok, header
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kExpressionDetector);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type),
                     sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size),
                     sizeof(obj_size));
    // Label length
    int label_length = static_cast<int>(emotion_label_.length());
    out_stream.write(reinterpret_cast<const char*>(&label_length),
                     sizeof(label_length));
    // Label
    out_stream.write(reinterpret_cast<const char*>(emotion_label_.data()),
                     label_length);
    // Classifier
    error = classifier_->Save(out_stream);
    // Done, sanity check
    error |= out_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name ComputeObjectSize
 *  @brief  Compute the object size in bytes
 *  @return Object memory size
 */
int ExpressionDetectorTrainer::ComputeObjectSize() const {
  int size = classifier_->ComputeObjectSize();
  size += 3 * sizeof(int);  //Svm header + Emotion label size
  size += static_cast<int>(emotion_label_.length());  // Label length
  return size;
}

#pragma mark -
#pragma mark Utility

/*
 *  @name   ConvertModelFromCharToBin
 *  @brief  Use to convert old model (char) into binary one
 *  @param[in]  detector_filename   Detector parameters file
 *  @param[in]  emotion_label       Name of the emotion
 *  @return -1 if error, 0 otherwise
 */
int ExpressionDetectorTrainer::
ConvertModelFromCharToBin(const std::string& detector_filename,
                          const std::string& emotion_label) {
  int error = -1;
  std::string filename;
  size_t pos = detector_filename.rfind(".");
  if (pos != std::string::npos) {
    filename = detector_filename.substr(0,pos) + ".bin";
  } else {
    filename = detector_filename + ".bin";
  }
  // Open stream
  std::ofstream output_stream(filename.c_str(),
                              std::ios_base::out | std::ios_base::binary);
  if (output_stream.is_open()) {
    error = ExpressionDetectorTrainer::ConvertModelFromCharToBin(detector_filename,
                                                              emotion_label,
                                                              output_stream);
    output_stream.close();
  }
  return error;
}

/*
 *  @name   ConvertModelFromCharToBin
 *  @brief  Use to convert old model (char) into binary one
 *  @param[in]  detector_filename     Detector parameters file
 *  @param[in]  emotion_label         Name of the emotion
 *  @param[out] output_stream         Stream where data are stored
 *  @return -1 if error, 0 otherwise
 */
int ExpressionDetectorTrainer::
ConvertModelFromCharToBin(const std::string& detector_filename,
                          const std::string& emotion_label,
                          std::ofstream& output_stream) {
  int error = -1;
  // Load svm and normalization if needed.
  BinaryLinearSVMClassifier* classifier = new BinaryLinearSVMClassifier();
  error = classifier->Load(detector_filename);

  if (error == 0) {
    // Compute object size
    int object_type = static_cast<int>(LTS5::HeaderObjectType::kExpressionDetector);
    // Svm size
    int object_size = classifier->ComputeObjectSize();
    // Label size + Header (obj type + obj size)
    object_size += 3 * sizeof(int);
    // Label name
    object_size += emotion_label.length() * sizeof(char);

    if (output_stream.good()) {
      // Can  write - Header
      output_stream.write(reinterpret_cast<const char*>(&object_type),
                          sizeof(object_type));
      output_stream.write(reinterpret_cast<const char*>(&object_size),
                          sizeof(object_size));
      // Label size
      int dummy = static_cast<int>(emotion_label.length());
      output_stream.write(reinterpret_cast<const char*>(&dummy), sizeof(dummy));
      // label
      output_stream.write(reinterpret_cast<const char*>(emotion_label.data()),
                          dummy * sizeof(char));
      // CLassifier
      error = classifier->Save(output_stream);
      // Done, sanity check
      error |= output_stream.good() ? 0 : -1;
    } else {
      error = -1;
    }
  }
  return error;
}

/*
 *  @name   ConvertModelFromCharToBin
 *  @brief  Use to convert old model (char) into binary one
 *  @param[in]  detector_filenames List of Detector parameters file
 *  @param[in]  emotion_label      List of emotion's name
 *  @param[out] output_stream      Stream where data are stored
 *  @return -1 if error, 0 otherwise
 */
int ExpressionDetectorTrainer::
ConvertModelFromCharToBin(const std::vector<std::string>& detector_filenames,
                          const std::vector<std::string>& emotion_labels,
                          const std::string& output_filename) {
  int error = -1;
  std::string filename;
  size_t pos = output_filename.rfind(".");
  if (pos != std::string::npos) {
    filename = output_filename.substr(0,pos) + ".bin";
  } else {
    filename = output_filename + ".bin";
  }
  // Open stream
  std::ofstream output_stream(filename.c_str(),
                              std::ios_base::out | std::ios_base::binary);
  if (output_stream.is_open()) {
    error = ExpressionDetectorTrainer::ConvertModelFromCharToBin(detector_filenames,
                                                              emotion_labels,
                                                              output_stream);
    output_stream.close();
  }

  return error;
}

/*
 *  @name   ConvertModelFromCharToBin
 *  @brief  Use to convert old model (char) into binary one
 *  @param[in]  detector_filenames List of Detector parameters file
 *  @param[in]  emotion_label      List of emotion's name
 *  @param[out] output_stream      Stream where data are stored
 *  @return -1 if error, 0 otherwise
 */
int ExpressionDetectorTrainer::
ConvertModelFromCharToBin(const std::vector<std::string>& detector_filenames,
                          const std::vector<std::string>& emotion_labels,
                          std::ofstream& output_stream) {
  int error = 0;
  if (detector_filenames.size() != emotion_labels.size()) {
    std::cout << "Detector configuration and emotion label have different size" << std::endl;
    return -1;
  }
  if (output_stream.good()) {
    for (int i = 0; i < static_cast<int>(detector_filenames.size()); ++i) {
      // Convert given model
      error |= ExpressionDetectorTrainer::ConvertModelFromCharToBin(detector_filenames[i],
                                                               emotion_labels[i],
                                                               output_stream);
    }
  } else {
    error = -1;
  }

  return error;
}
}  // namepsace LTS5
