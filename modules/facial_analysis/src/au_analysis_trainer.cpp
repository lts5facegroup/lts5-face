/**
 *  @file   au_analysis_trainer.cpp
 *  @brief  AU Analysis trainer module
 *
 *  @author Christophe Ecabert
 *  @date   11/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <chrono>
#include <iostream>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/facial_analysis/au_analysis_trainer.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   AUAnalysisTrainer
 *  @fn AUAnalysisTrainer(void)
 *  @brief  Constructor
 */
AUAnalysisTrainer::AUAnalysisTrainer(void) :
        initialized_(false),
        tracker_(nullptr),
        pose_model_(nullptr),
        au_detector_trainers_(0,nullptr) {
}

/*
 *  @name   ~AUAnalysisTrainer
 *  @fn   ~AUAnalysisTrainer(void)
 *  @brief  Constructor
 */
AUAnalysisTrainer::~AUAnalysisTrainer(void) {
  // Release all trainer used
  for (int i = 0; i < au_detector_trainers_.size(); ++i) {
    delete au_detector_trainers_[i];
  }
  if (tracker_ != nullptr) {
    delete tracker_;
    tracker_ = nullptr;
  }
  if (pose_model_ != nullptr) {
    delete pose_model_;
    pose_model_ = nullptr;
  }
}

/*
 *  @name   Load
 *  @fn int Load(const std::string& tracker_filename,
                 const std::string& face_detector_model,
                 const std::string& pose_filename)
 *  @brief  Load face tracker and pose normalization object
 *  @param[in]  tracker_filename   Face tracker model
 *  @param[in]  face_detector_model Face detector configuration
 *  @param[in]  pose_filename      Point distribution model
 *  @return -1 if error, 0 otherwise
*/
int AUAnalysisTrainer::Load(const std::string& tracker_filename,
                            const std::string& face_detector_model,
                            const std::string& pose_filename) {
  int error = -1;
  const bool tracker_is_binary = (tracker_filename.rfind(".bin") !=
                                  std::string::npos);
  const bool pose_is_binary = (pose_filename.rfind(".bin") !=
                               std::string::npos);
  std::ios_base::openmode flag_tracker = (tracker_is_binary ?
                                          std::ios_base::in|std::ios_base::binary :
                                          std::ios_base::in);
  std::ios_base::openmode flag_pose = (tracker_is_binary ?
                                       std::ios_base::in|std::ios_base::binary :
                                       std::ios_base::in);
  std::ifstream tracker_stream(tracker_filename.c_str(),flag_tracker);
  std::ifstream pose_stream(pose_filename.c_str(),flag_pose);
  if (tracker_stream.good() && pose_stream.good()) {
    int status = 0;
    if (tracker_is_binary) {
      status = LTS5::ScanStreamForObject(tracker_stream, LTS5::HeaderObjectType::kSdm);
    }
    if (pose_is_binary) {
      status |= LTS5::ScanStreamForObject(pose_stream,
                                          LTS5::HeaderObjectType::kPointDistributionModel);
    }
    if (status == 0) {
      error = this->Load(tracker_stream,
                         tracker_is_binary,
                         face_detector_model,
                         pose_stream,
                         pose_is_binary);
    }
  }
  return error;
}

/*
 *  @name   Load
 *  @fn int Load(std::ifstream& tracker_stream,
                 const bool&  tracker_is_binary,
                 const std::string& face_detector_model,
                 std::ifstream& pose_stream,
                 const bool& pose_is_binary)
 *  @brief  Load face tracker and pose normalization object
 *  @param[in]  tracker_stream      Face tracker model
 *  @param[in]  tracker_is_binary   Indicate face tracker is binary
 *  @param[in]  face_detector_model Face detector configuration
 *  @param[in]  pose_stream         Point distribution model
 *  @param[in]  pose_is_binary   Indicate pose stream is binary
 *  @return -1 if error, 0 otherwise
 */
int AUAnalysisTrainer::Load(std::ifstream& tracker_stream,
                            const bool&  tracker_is_binary,
                            const std::string& face_detector_model,
                            std::ifstream& pose_stream,
                            const bool& pose_is_binary) {
  int error = -1;
  if (tracker_stream.good() && pose_stream.good()) {
    // Both streams are Ok, load sdm first
    if (tracker_ == nullptr) {
      tracker_ = new LTS5::SdmTracker();
    }
    error = tracker_->Load(tracker_stream,
                           tracker_is_binary,
                           face_detector_model);
    // Pdm
    if (pose_model_ == nullptr) {
      pose_model_ = new LTS5::PointDistributionModel();
    }
    error |= pose_model_->Load(pose_stream, pose_is_binary);
    // Properly initialized ?
    initialized_ = (error == 0);
  }
  return error;
}

#pragma mark -
#pragma mark Train

/*
 *  @name   Train
 *  @fn int Train(const std::string& data_filename,
                   const double retain_variance,
                   const BinaryLinearSVMTraining& svm_type,
                   const std::vector<AULabel>& au_labels,
                   const std::vector<int>& subspace_dim)
 *  @brief  Train detectors given a set of annotations/labels
 *  @param[in]  data_filename         File holding image + annotation
 *  @param[in]  retain_variance       Amount of variance to retain
 *  @param[in]  svm_type              Type of regularization for svm training
 *  @param[in]  scoring_type          Type of metric to use for scoring
 *                                    performance
 *  @param[in]  au_labels             AU label/name
 *  @param[in]  subspace_dim          Subspace dimension to try
 *  @return -1 if error, 0 otherwise
 */
int AUAnalysisTrainer::Train(const std::string& data_filename,
                             const double retain_variance,
                             const BinaryLinearSVMTraining& svm_type,
                             const ScoringType scoring_type,
                             const std::vector<AULabel>& au_labels,
                             const std::vector<int>& subspace_dim) {
  int error = -1;
  if (initialized_) {
    error = this->LoadCandidateData(data_filename,
                                    &candidate_,
                                    &n_available_sample_);
    if (!error) {
      // Detect landmark and remove un-reliable candidate/node if not defined
      this->DetectLandmark(data_filename, &candidate_, &n_usable_sample_);
      // Training set statistics
      this->TrainingLabelStats(au_labels, candidate_);
      // Extract features
      this->ExtractFeature(n_usable_sample_,
                           candidate_,
                           &sift_features_);
      // Compute PCA Subspace
      this->ComputePcaSubspace(sift_features_, retain_variance);
      // Project data
      cv::Mat projected_sift;
      this->ProjectDataSample(sift_features_, &projected_sift);
      // Compute normalization factor
      this->ComputeNormalization(projected_sift);
      // Normalize
      this->NormalizeDataSample(projected_sift);
      // Put back into
      projected_sift.copyTo(sift_features_);
      projected_sift.release();

      // Data ready to train classifier
      int idx = 0;
      for (auto& au : au_labels) {
        // Get corresponding info
        AUType au_type = au.first;
        std::string au_name = au.second;
        auto start = std::chrono::system_clock::now();
        std::cout << "\t-----------------------------------------" << std::endl;
        std::cout << "\tStart to train : " << au_name << std::endl;
        cv::Mat labels;
        this->PrepareDataSample(au_type,
                                candidate_,
                                n_usable_sample_,
                                &labels);
        // Cross valid to estimate svm parameter
        if (au_detector_trainers_.size() < au_labels.size()) {
          au_detector_trainers_.push_back(
                  new LTS5::AUDetectorTrainer(au_name));
        }
        au_detector_trainers_[idx]->Train(sift_features_,
                                          subspace_dim,
                                          labels,
                                          svm_type,
                                          scoring_type);
        idx++;
        // Timing
        auto stop = std::chrono::system_clock::now();
        auto delta_t = std::chrono::duration_cast<std::chrono::seconds>(
                stop - start);
        std::cout << "\tTook : " << delta_t.count() << "s" << std::endl;
      }
    } else {
      std::cout << "ERROR, Unable to load configuration file ..." << std::endl;
    }
  }
  return error;
}

/*
 *  @name   Save
 *  @fn int Save(const std::string& filename)
 *  @brief  Save model into binary file
 *  @param[in]  filename  Name of the file where to save model(s)
 *  @return -1 if error, 0 otherwise
 */
int AUAnalysisTrainer::Save(const std::string& filename) {
  int error = -1;
  if (initialized_) {
    // Check name validity, and add .bin extension if none provided. If there
    // is already one, remove it and append the new one.
    std::string final_name;
    int position_dot = static_cast<int>(filename.rfind("."));
    if (position_dot == std::string::npos) {
      // No ext
      final_name = filename + ".bin";
    } else {
      // Existing extension -> remove it and add .bin
      final_name = filename.substr(0, position_dot) + ".bin";
    }
    // Write binary file
    std::ofstream output_stream(final_name,
                                std::ios_base::out | std::ios_base::binary);
    if (output_stream.is_open()) {
      error = this->Save(output_stream);
      output_stream.close();
      std::string msg = error == 0 ? "Success" : "Fail";
      std::cout << "Save with " << msg << std::endl;
    } else {
      std::cout << "Cannot open file" << final_name << std::endl;
    }
  }
  return error;
}

/*
 *  @name   Save
 *  @fn int Save(std::ofstream& output_stream)
 *  @brief  Save model into binary file
 *  @param[in]  output_stream  Stream to binary file where to save model(s)
 *  @return -1 if error, 0 otherwise
 */
int AUAnalysisTrainer::Save(std::ofstream& output_stream) {
  int error = -1;
  if (output_stream.good()) {
    // Compute obj size
    // -------------------------------
    int obj_size = configuration_.ComputeHeaderSize();
    // Header + size
    obj_size += 2 * sizeof(int);
    obj_size += pose_model_->ComputeObjectSize();
    // Pca
    obj_size += 3 * sizeof(int);
    obj_size += feature_space_.total() * feature_space_.elemSize();
    // Mean
    obj_size += 3 * sizeof(int);
    obj_size += (feature_normalization_mean_.total() *
                 feature_normalization_mean_.elemSize());
    // Std
    obj_size += 3 * sizeof(int);
    obj_size += (feature_normalization_std_.total() *
                 feature_normalization_std_.elemSize());
    // Detectors
    obj_size += sizeof(int);
    for (int i = 0; i < au_detector_trainers_.size(); ++i) {
      obj_size += 2 * sizeof(int);
      obj_size += au_detector_trainers_[i]->ComputeObjectSize();
    }
    // Write
    // -------------------------------
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kAUAnalysis);
    output_stream.write(reinterpret_cast<const char*>(&obj_type),
                        sizeof(obj_type));
    output_stream.write(reinterpret_cast<const char*>(&obj_size),
                        sizeof(obj_size));
    // Config
    error = configuration_.WriteParameterHeader(output_stream);
    // PDM
    error |= pose_model_->Write(output_stream);
    // PCA
    error |= LTS5::WriteMatToBin(output_stream, feature_space_);
    // Mean
    error |= LTS5::WriteMatToBin(output_stream, feature_normalization_mean_);
    // Std
    error |= LTS5::WriteMatToBin(output_stream, feature_normalization_std_);
    // Detector
    int n_detector = static_cast<int>(au_detector_trainers_.size());
    output_stream.write(reinterpret_cast<const char*>(&n_detector),
                        sizeof(n_detector));
    for (int i = 0; i < n_detector; ++i) {
      error |= au_detector_trainers_[i]->Save(output_stream);
    }
    // Done
    error |= output_stream.good() ? 0 : -1;
  }
  return error;
}

#pragma mark -
#pragma mark Testing / Validation

/**
 *  @name   LoadTestingConfiguration
 *  @fn static int LoadTestingConfiguration(const std::string& config,
                                             std::vector<std::string>* image,
                                             std::vector<int>* label)
 *  @brief  Load data from testing configuration file
 *  @param[in]  config  Testing configuration file
 *  @param[out] image   Image paths
 *  @param[out] label   Labels
 *  @return -1 if error, 0 otherwise
 */
int AUAnalysisTrainer::LoadTestingConfiguration(const std::string& config,
                                                std::vector<std::string>* image,
                                                std::vector<int>* label) {
  int error = -1;
  std::ifstream in_stream(config.c_str());
  if (in_stream.is_open()) {
    std::stringstream str_stream;
    std::string line;
    std::string image_path;
    int au_label;
    // Parse the whole file
    while (!in_stream.eof()) {
      // Read line
      std::getline(in_stream, line);
      if (line.length() > 0) {
        // Init stream
        str_stream.str(line);
        // Read data
        str_stream >> image_path >> au_label;
        str_stream.clear();
        // Push data
        image->push_back(image_path);
        label->push_back(au_label);
      }
    }
    // Reached the end
    error = 0;
  }
  return error;
}

/*
 *  @name   TestExpressionAnalysisModule
 *  @fn static int TestExpressionAnalysisModule(const std::string& au_model,
                                           BaseFaceTracker<cv::Mat, cv::Mat>* face_tracker,
                                           const std::string& test_config,
                                           const std::string& output_filename)
 *  @brief  Test the performance of an ExpressionAnalysis module
 *  @param[in]  au_model            AUAnalysis model
 *  @param[in]  face_tracker        Face tracker instance
 *  @param[in]  test_config         List of image/label
 *  @param[in]  output_filename     File with ground truth/ prediction are
 *                                  dumped
 *  @return -1 if error, 0 otherwise
 */
int AUAnalysisTrainer::TestExpressionAnalysisModule(const std::string& au_model,
                                                    BaseFaceTracker<cv::Mat, cv::Mat>* face_tracker,
                                                    const std::string& test_config,
                                                    const std::string& output_filename) {
  // Load configuration
  std::vector<std::string> image_path;
  std::vector<int> labels;
  std::vector<std::string> au_label_names;
  std::vector<double> scores;
  // Load stuff
  int error = AUAnalysisTrainer::LoadTestingConfiguration(test_config,
                                                          &image_path,
                                                          &labels);
  AUAnalysis* au_analysis = new AUAnalysis();
  error |= au_analysis->Load(au_model);
  au_analysis->get_au_labels(&au_label_names);
  // Open output file
  std::ofstream out_stream(output_filename.c_str());
  error |= out_stream.is_open() ? 0 : -1;
  if (error == 0) {
    // plot label and value
    out_stream << au_label_names.size() << std::endl;
    for (int i = 0; i < au_label_names.size(); ++i) {
      out_stream << (0x01<<i) << " " << au_label_names[i] << std::endl;
    }
    // Loop over all image
    for (int i = 0 ; i < image_path.size() ; ++i) {
      // Load image
      std::cout << image_path[i] << std::endl;
      cv::Mat image = cv::imread(image_path[i]);
      if (image.empty()) {
        std::cout << "Unable to load image : " << image_path[i] << std::endl;
        continue;
      }
      if (image.channels() != 1) {
        cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
      }
      // Align
      cv::Mat shape;
      int status = face_tracker->Detect(image, &shape);
      if ((status == 0 && !shape.empty())) {
        // Predict
        au_analysis->Predict(image, shape, &scores);
        int value = 0;
        for (int j = 0; j < scores.size(); ++j) {
          if (scores[j] > 0.0) {
            value += (0x0001 << j);
          }
        }
        // Dump prediction
        out_stream << labels[i] << " " << value << " " << image_path[i] << std::endl;
      }  else {
        // Dump error
        out_stream << "NaN NaN " << image_path[i] <<  std::endl;
      }
      out_stream.flush();
    }
    // Done
    out_stream.close();
  }
  // Clean up
  if (au_analysis) {
    delete au_analysis;
  }
  return error;
}

#pragma mark -
#pragma mark Private

/*
 *  @name   LoadCandidateData
 *  @fn int LoadCandidateData(const std::string& data_filename,
                               std::vector<CandidateNode>* list,
                               int* n_sample)
 *  @brief  Load candidate data (image location + ground truth)
 *  @param[in]  data_filename   File holding candidate data (image location +
 *                              annotation)
 *  @param[out] list            List of candidate to populate
 *  @param[out] n_sample        Number of sample loaded
 *  @return -1 if error, 0 otherwise.
 */
int AUAnalysisTrainer::LoadCandidateData(const std::string& data_filename,
                                         std::vector<CandidateNode>* list,
                                         int* n_sample) {
  int error = -1;
  std::cout << "Load candidate data ..." << std::endl;
  std::ifstream in_stream(data_filename.c_str());
  if (in_stream.is_open()) {
    std::stringstream str_stream;
    std::string line;
    AUAnalysisTrainer::CandidateNode node;
    // Parse the whole file
    while (!in_stream.eof()) {
      // Read line
      std::getline(in_stream, line);
      if (line.length() > 0) {
        // Init stream
        str_stream.str(line);
        // Read data
        str_stream >> node.k_image_location >> node.k_ground_truth;
        str_stream.clear();
        // Add node
        list->push_back(node);
        // Count
        *n_sample += 1;
      }
    }
    // Reached the end
    std::cout << "available samples : " << *n_sample << std::endl;
    error = 0;
  }
  return error;
}

/*
 * @name  TrainingLabelStats
 * @fn    void TrainingLabelStats(const std::vector<AULabel>& au_label,
                                  const std::vector<CandidateNode>& list)
 * @brief Compute number of positive sample for each AU
 * @param[in] au_label  List of AU labels
 * @param[in] list      List of training samples
 */
void AUAnalysisTrainer::TrainingLabelStats(const std::vector<AULabel>& au_label,
                                           const std::vector<CandidateNode>& list) {
  for (auto& au : au_label) {
    int n_pos_sample = 0;
    AUType au_type = au.first;
    // Loop over all samples
    for (int i = 0; i < list.size(); ++i) {
      if ((list[i].k_ground_truth & au_type) == au_type) {
        ++n_pos_sample;
      }
    }
    // Output
    std::cout << au.second << " has " << n_pos_sample;
    std::cout << " pos sample" << std::endl;
  }
}

/*
 *  @name   DetectLandmark
 *  @fn void DetectLandmark(const std::string& data_filename,
                             std::vector<CandidateNode>* list,
                             int* n_sample)
 *  @brief  Detect landmark for each CandidateNode and remove if entry in case
 *          of bad detection
 *  @param[in]      data_filename Path to original data candidate
 *  @param[in,out]  list          List of candidate where image are stored
 *  @param[out]     n_sample      Number of usable sample
 */
void AUAnalysisTrainer::DetectLandmark(const std::string& data_filename,
                                       std::vector<CandidateNode>* list,
                                       int* n_sample) {
  // Loop over all node
  std::cout << "Detecting facial landmarks ..." << std::endl;
  auto start = std::chrono::system_clock::now();
  auto candidate_it = list->begin();
  cv::Mat shape;
  int error = -1;
  *n_sample = 0;
  // Loop over candidate
  while (candidate_it != list->end()) {
    // Display stuff
    if ((*n_sample % 100) == 0){
      std::cout << "\tprocess image : " << candidate_it->k_image_location << std::endl;
    }
    // Load image
    cv::Mat image = cv::imread(candidate_it->k_image_location,
                               cv::IMREAD_UNCHANGED);
    if (image.channels() > 1) {
      cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
    }
    // Detect landmark
    error = tracker_->Detect(image, &shape);
    if (!error) {
      // Add
      candidate_it->k_shape = shape.clone();
      // Count sample
      *n_sample += 1;
      // Go to the next candidate
      ++candidate_it;
    } else {
      // Drop candidate
      std::cout << "\t\tDrop : " << candidate_it->k_image_location << std::endl;
      candidate_it = list->erase(candidate_it);
    }
  }
  auto stop = std::chrono::system_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
  std::cout << "  ... done in : " << elapsed.count() << "s" << std::endl;
  std::cout << "Usable samples : " << *n_sample << std::endl;
}

/*
 *  @name  ExtractFeature
 *  @fn void ExtractFeature(const int n_feature,
                            const std::vector<CandidateNode>& list,
                            cv::Mat* feature)
 *  @brief  Extract sift features for training and testing set
 *  @param[in]  n_feature         Number of feature to extract
 *  @param[in]  list              List of candidate holding shape information
 *  @param[out] feature           Extracted sift features
 *  @return -1 if error, 0 otherwise
 */
void AUAnalysisTrainer::ExtractFeature(const int n_feature,
                                       const std::vector<CandidateNode>& list,
                                       cv::Mat* feature) {
  std::cout << "Extracting feature for training set ..." << std::endl;
  auto start = std::chrono::system_clock::now();
  // Features extraction
  int idx = 0;
  auto candidate_it = list.begin();
  while (candidate_it != list.end()) {
    // Load image
    cv::Mat image = cv::imread(candidate_it->k_image_location);
    if (image.channels() > 1) {
      cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
    }
    // ---------------------------------------------------------
    // Normalize shape + image
    cv::Mat norm_shape;
    cv::Mat norm_image;
    pose_model_->NormSDMImage(image,
                              candidate_it->k_shape,
                              true,
                              &norm_image,
                              &norm_shape);
    // ---------------------------------------------------------
    // Illumination normalization
#ifdef USE_MEAN_NORMALIZATION
    /*cv::Mat img32;
    norm_image.convertTo(img32, CV_32FC1);
    const int n_elem = static_cast<int>(img32.total());
    float* img_ptr = reinterpret_cast<float*>(img32.data);
    float sum = 0;
    for (int i = 0; i < n_elem; ++i) {
      sum += *img_ptr++;
    }
    const float img_mean = sum/static_cast<float>(n_elem);
    img_ptr = reinterpret_cast<float*>(img32.data);
    for (int i = 0; i < n_elem; ++i) {
      *img_ptr++ /= img_mean;
    }
    // TODO:(Christophe) Check if it can be avoid in future
    // Copy back into source Image
    img32.copyTo(norm_image);*/
#endif
    // ---------------------------------------------------------
    // Augement shape
    cv::Mat augmented_norm_shape = cv::Mat(norm_shape.rows + 16,
                                           norm_shape.cols,
                                           norm_shape.type());
    // Copy norm shape, x
    memcpy(reinterpret_cast<void*>(augmented_norm_shape.data),
           reinterpret_cast<const void*>(norm_shape.data),
           392); // 49 x 8bytes = 392
    // Copy norm shape, y
    memcpy(reinterpret_cast<void*>(&(augmented_norm_shape.data[456])),
           reinterpret_cast<const void*>(&(norm_shape.data[392])),
           392); // 49 x 8bytes = 392
    // ADD, mid eyebrows
    double* aug_ptr = reinterpret_cast<double*>(augmented_norm_shape.data);
    aug_ptr[49] = (aug_ptr[4] + aug_ptr[5]) / 2.0;
    aug_ptr[106] = (aug_ptr[61] + aug_ptr[62]) / 2.0;
    // ADD, crow-feet left
    aug_ptr[50] = aug_ptr[0];
    aug_ptr[107] = aug_ptr[76];
    // ADD, crow-feet right
    aug_ptr[51] = aug_ptr[9];
    aug_ptr[108] = aug_ptr[85];
    // ADD, nostril left
    aug_ptr[52] = aug_ptr[14];
    aug_ptr[109] = aug_ptr[69];
    // ADD, nostril right
    aug_ptr[53] = aug_ptr[18];
    aug_ptr[110] = aug_ptr[69];
    // ADD, zygomaticus left
    aug_ptr[54] = aug_ptr[31];
    aug_ptr[111] = aug_ptr[71];
    // ADD, zygomaticus left
    aug_ptr[55] = aug_ptr[37];
    aug_ptr[112] = aug_ptr[75];
    // ADD, Chin
    // (Assumes the vertical distance between mid-eyebrows and nose tip
    // is equal to that between nose tip and chin tip  )
    double dist = (aug_ptr[16 + 57] - aug_ptr[61]) + aug_ptr[73];
    aug_ptr[56] = aug_ptr[40];
    aug_ptr[113] = (aug_ptr[97] + dist) / 2.0;
    // ---------------------------------------------------------
    // Extract features
    if (feature->empty()) {
      int shape_size = std::max(augmented_norm_shape.rows,
                                augmented_norm_shape.cols) / 2;
      feature->create(n_feature,
                      shape_size * configuration_.sift_configuration.sift_dim,
                      CV_64FC1);
    }
#ifndef HAS_INTRAFACE_DESCR
    std::vector<cv::KeyPoint> key_points;
    key_points = LTS5::KeypointsFromShape(augmented_norm_shape,
                                          configuration_.sift_size);
    // Compute sift
    cv::Mat sift_feature;
    LTS5::SSift::ComputeDescriptor(norm_image,
                                   key_points,
                                   configuration_.sift_configuration,
                                   &sift_feature);
    sift_feature = sift_feature.reshape(sift_feature.channels(), 1);
#endif
    sift_feature.convertTo(feature->row(idx), CV_64FC1);
    // Done
    ++idx;
    // Move to next candidate
    ++candidate_it;

  }
  assert(idx == n_feature);
  // Feedback
  auto stop = std::chrono::system_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
  std::cout << "  ... done in : " << elapsed.count() << "s" << std::endl;
}

/*
 *  @name ComputePcaSubspace
 *  @fn void ComputePcaSubspace(const cv::Mat& data,
                               const double retain_variance)
 *  @brief  Compute pca feature subspace
 *  @param[in]  data              DAta to perform PCA on
 *  @param[in]  retain_variance   Amount of variance to retain
 */
void AUAnalysisTrainer::ComputePcaSubspace(const cv::Mat& data,
                                           const double retain_variance) {
  std::cout << "Compute pca subspace ..." << std::endl;
  auto start = std::chrono::system_clock::now();
  LTS5::LoadMatFromBin("au_detection/train/eigenspace.bin",&feature_space_);
  if (feature_space_.empty()) {
    // Compute PCA with all sample
    cv::Mat eigen_val;
    LTS5::ComputePca(data,
                     retain_variance,
                     kLapack,
                     &feature_space_,
                     &eigen_val);
    std::cout << "Pca space dimension : " << feature_space_.rows << std::endl;
    auto stop = std::chrono::system_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
    std::cout << " ... done in : " << elapsed.count() << "s" << std::endl;
    LTS5::SaveMatToBin("au_detection/train/eigen_val.bin", eigen_val);
    LTS5::SaveMatToBin("au_detection/train/eigenspace.bin",feature_space_);
  }
}

/*
 *  @name   PrepareDataSample
 *  @fn void PrepareDataSample(const EmotionType& emotion_label,
                               const std::vector<CandidateNode>& candidates,
                               const int n_label,
                               cv::Mat* labels)
 *  @brief  Generate the labels for a given emotion label
 *  @param[in]  emotion_label   Emotion label, if -1 just copy label into array
 *  @param[in]  candidates      List of candidate to extract label from
 *  @param[in]  n_label         Number of label to generate
 *  @param[out] labels          Positive example
 */
void AUAnalysisTrainer::PrepareDataSample(const AUType & au_label,
                                          const std::vector<CandidateNode>& candidates,
                                          const int n_label,
                                          cv::Mat* labels) {
  // Reset container
  labels->create(n_label, 1, CV_32FC1);
  // Fill
  int cnt = 0;
  auto candidate_it = candidates.begin();
  // Loop over all annotation
  for (; candidate_it != candidates.end(); ++candidate_it, ++cnt) {
    // Define label
    if ((candidate_it->k_ground_truth & au_label) == au_label) {
      // Pos sample
      labels->at<float>(cnt) = 1.0f;
    } else {
      // Neg sample
      labels->at<float>(cnt) = -1.0f;
    }
  }
  assert(cnt == n_label);
}


/*
 *  @name   ProjectDataSample
 *  @fn void ProjectDataSample(const cv::Mat& data,
                               cv::Mat* projected_data)
 *  @brief  Project feature into subspace for a given size
 *  @param[in]  data              Data samples
 *  @param[out] projected_data    Projected data into subspace + normalized if
 *                                asked
 */
void AUAnalysisTrainer::ProjectDataSample(const cv::Mat& data,
                                          cv::Mat* projected_data) {
  using TransposeType = LTS5::LinearAlgebra<double>::TransposeType;
  // Project
  projected_data->create(data.rows, feature_space_.cols, data.type());
  LTS5::LinearAlgebra<double>::Gemm(data,
                                    TransposeType::kNoTranspose,
                                    1.0,
                                    feature_space_,
                                    TransposeType::kTranspose,
                                    0.0,
                                    projected_data);
}

/*
 *  @name ComputeNormalization
 *  @fn void ComputeNormalization(const cv::Mat& data)
 *  @brief  Compute normalization factor for data sample (z-score)
 *  @param[in]  data  Data to use to estimate normalization
 */
void AUAnalysisTrainer::ComputeNormalization(const cv::Mat& data) {
  // Compute mean
  LTS5::LinearAlgebra<double>::Mean(data, 1, &feature_normalization_mean_);
  // Compute std
  cv::Mat row_sqr;
  feature_normalization_std_.create(1, data.cols, data.type());
  feature_normalization_std_.setTo(0.0);
  // Sum(x_i^2)
  for (int i = 0; i < data.rows; ++i) {
    cv::pow(data.row(i), 2.0, row_sqr);
    feature_normalization_std_ += row_sqr;
  }
  // N * mean^2
  cv::pow(feature_normalization_mean_, 2.0, row_sqr);
  row_sqr *= data.rows;
  // Sum(x_i^2) - N * mean^2
  row_sqr = (feature_normalization_std_ - row_sqr) / (data.rows -1);
  // Stdev
  cv::pow(row_sqr, 0.5, feature_normalization_std_);
}

/*
 *  @name NormalizeDataSample
 *  @fn void NormalizeDataSample(const cv::Mat& data)
 *  @brief  Apply normalization to data sample (z-score)
 *  @param[in,out]  data  Data to normalize
 */
void AUAnalysisTrainer::NormalizeDataSample(const cv::Mat& data) {
  // Loop over all sample
  for (int i = 0; i < data.rows; ++i) {
    data.row(i) = data.row(i) - feature_normalization_mean_;
    data.row(i) = data.row(i) / feature_normalization_std_;
  }
}

}  // namepsace LTS5
