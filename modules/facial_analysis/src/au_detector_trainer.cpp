/**
 *  @file   au_detector_trainer.cpp
 *  @brief  Train classifier for AU detection
 *
 *  @author Christophe Ecabert
 *  @date   08/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <fstream>

#include "lts5/utils/object_type.hpp"
#include "lts5/facial_analysis/au_detector_trainer.hpp"
#include "lts5/classifier/crossvalidation.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  AUDetectorTrainer
 * @fn  AUDetectorTrainer(void)
 * @brief Constructor
 */
AUDetectorTrainer::AUDetectorTrainer(void) : au_label_(""),
                                             classifier_(nullptr) {
}

/*
 *  @name   AUDetectorTrainer
 *  @fn AUDetectorTrainer(const std::string& au_label)
 *  @brief  Constructor
 *  @param[in]  au_label Name of the AU detected
 */
AUDetectorTrainer::AUDetectorTrainer(const std::string& au_label) :
        au_label_(au_label),
        classifier_(nullptr) {
}

/*
 * @name  ~AUDetectorTrainer
 * @fn  ~AUDetectorTrainer(void)
 * @brief Destructor
 */
AUDetectorTrainer::~AUDetectorTrainer(void) {
  if (classifier_) {
    delete classifier_;
    classifier_ = nullptr;
  }
}

#pragma mark -
#pragma mark Train

/*
 *  @name   Train
 *  @fn void Train(const cv::Mat& samples,
                   const std::vector<int>& subspace_dim,
                   const cv::Mat& labels,
                   const BinaryLinearSVMTraining& svm_type)
 *  @brief  Train emotion class doing cross validation on sample to find
 *          best C parameter
 *  @param[in]  samples         Samples for training
 *  @param[in]  subspace_dim    List of subspace dimension
 *  @param[in]  labels          Labels for training, format [1xN] or [Nx1]
 *                              with {-1,1} for negative and positive sample
 *                              (flt32).
 *  @param[in]  svm_type  Type of svm
 *  @param[in]  scoring_type  Type of metric to use for scoring performance
 */
void AUDetectorTrainer::Train(const cv::Mat& samples,
                              const std::vector<int>& subspace_dim,
                              const cv::Mat& labels,
                              const BinaryLinearSVMTraining& svm_type,
                              const ScoringType scoring_type) {
  using BinaryCrossValidation = BinaryCrossValidation<cv::Mat>;
  assert(((labels.cols == 1 && labels.rows > 1) ||
          (labels.cols > 1 && labels.rows == 1)) &&
         labels.type() == CV_32FC1);
  // Split sample based on labels
  const int n_sample = std::max(labels.cols, labels.rows);
  // Loop over possible subspace dimension
  double best_score = 0.0;
  double best_std = std::numeric_limits<double>::max();
  int dim_idx = -1;
  for (int i = 0; i < subspace_dim.size(); ++i) {
    // Define features
    std::vector<cv::Mat> pos_features;
    std::vector<cv::Mat> neg_features;
    for (int j = 0; j < n_sample; ++j) {
      if (labels.at<float>(j) > 0.f) {
        // Pos
        pos_features.push_back(samples.row(j).colRange(0, subspace_dim[i]));
      } else {
        // Neg
        neg_features.push_back(samples.row(j).colRange(0, subspace_dim[i]));
      }
    }
    assert(neg_features.size() > 0);
    // Define best C parameters through CV
    double best_c = -1.0;
    BinaryCrossValidation::Stats stats;
    BinaryCrossValidation::EstimateLinearCoefficient(pos_features,
                                                     neg_features,
                                                     svm_type,
                                                     true,
                                                     scoring_type,
                                                     &best_c,
                                                     &stats);
    std::cout << "\t\tScore : " << stats.k_mean_score;
    std::cout << " +/- " << stats.k_mean_stddev << std::endl;
    // Best dimension so far ?
    if (stats.k_mean_score > best_score) {
      // Best accuracy so far
      best_score = stats.k_mean_score;
      best_std = stats.k_mean_stddev;
      dim_idx = i;
    } else if ((stats.k_mean_score == best_score) &&
               (stats.k_mean_stddev < best_std)) {
      // Not better but less variation
      best_score = stats.k_mean_score;
      best_std = stats.k_mean_stddev;
      dim_idx = i;
    }
  }
  // Setup feature for best dimension
  std::vector<cv::Mat> pos_features;
  std::vector<cv::Mat> neg_features;
  for (int i = 0; i < n_sample; ++i) {
    if (labels.at<float>(i) > 0.f) {
      // Pos
      pos_features.push_back(samples.row(i).colRange(0, subspace_dim[dim_idx]));
    } else {
      // Neg
      neg_features.push_back(samples.row(i).colRange(0, subspace_dim[dim_idx]));
    }
  }
  // Define best C parameters through CV
  double best_c = -1.0;
  BinaryCrossValidation::Stats stats;
  float b = BinaryCrossValidation::EstimateLinearCoefficient(pos_features,
                                                             neg_features,
                                                             svm_type,
                                                             true,
                                                             scoring_type,
                                                             &best_c,
                                                             &stats);
  // Init svm
  if (classifier_ != nullptr) {
    delete classifier_;
    classifier_ = nullptr;
  }
  // Train
  std::cout << "\tPos. balance (neg/pos) : " << b << std::endl;
  std::cout << "\tC   : " << best_c << std::endl;
  std::cout << "\tDim : " << subspace_dim[dim_idx] << std::endl;
  classifier_ = new LTS5::BinaryLinearSVMClassifier(best_c, svm_type);
  classifier_->Train(pos_features, neg_features);
}

/*
 *  @name   Save
 *  @fn int Save(const std::string& output_filename) const
 *  @brief  Save Detector model into file
 *  @param[in] output_filename File to save the model
 */
int AUDetectorTrainer::Save(const std::string& output_filename) const {
  int error = -1;
  std::string name;
  size_t pos_ext = output_filename.rfind(".");
  if (pos_ext != std::string::npos) {
    // No ext
    name = output_filename.substr(0,pos_ext) + ".bin";
  } else {
    name = output_filename + ".bin";
  }
  // Write binary file
  std::ofstream output_stream(name,
                              std::ios_base::out | std::ios_base::binary);
  if (output_stream.is_open()) {
    error = this->Save(output_stream);
  } else {
    std::cout << "Cannot open output stream " << name << std::endl;
  }
  return error;
}

/*
 *  @name   Save
 *  @fn int Save(std::ofstream& out_stream) const
 *  @brief  Save Detector model into binary stream
 *  @param[in] out_stream Binary stream where to save the model
 */
int AUDetectorTrainer::Save(std::ofstream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Ok, header
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kAUDetector);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type),
                     sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size),
                     sizeof(obj_size));
    // Label length
    int label_length = static_cast<int>(au_label_.length());
    out_stream.write(reinterpret_cast<const char*>(&label_length),
                     sizeof(label_length));
    // Label
    out_stream.write(reinterpret_cast<const char*>(au_label_.data()),
                     label_length);
    // Classifier
    error = classifier_->Save(out_stream);
    // Done, sanity check
    error |= out_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name ComputeObjectSize
 *  @fn int ComputeObjectSize() const
 *  @brief  Compute the object size in bytes
 *  @return Object memory size
 */
int AUDetectorTrainer::ComputeObjectSize() const {
  int size = classifier_->ComputeObjectSize();
  size += 3 * sizeof(int);  //Svm header + Emotion label size
  size += static_cast<int>(au_label_.length());  // Label length
  return size;
}
}  // namepsace LTS5