/**
 *  @file   au_analysis.cpp
 *  @brief  AU Analysis processing module
 *
 *  @author Christophe Ecabert
 *  @date   11/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/facial_analysis/au_analysis.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   AUAnalysis
 *  @fn AUAnalysis(void)
 *  @brief  Constructor
 */
AUAnalysis::AUAnalysis(void) : pose_normalization_(nullptr),
                               do_feature_normalization_(true),
                               au_detectors_(0, nullptr) {
}

/*
 *  @name   ~AUAnalysis
 *  @fn ~AUAnalysis(void)
 *  @brief  Destructor
 */
AUAnalysis::~AUAnalysis(void) {
  this->ReleaseMemory();
}

/*
 *  @name Load
 *  @fn int Load(std::ifstream& input_stream)
 *  @brief  Initialize module with a given stream
 *  @param[in]  input_stream  Input stream pointing to models
 *  @return -1 if error, 0 otherwise
 */
int AUAnalysis::Load(std::ifstream& input_stream) {
  int error = -1;
  if (input_stream.good()) {
    // Read configuration
    error = configuration_.ReadParameterHeader(input_stream);
    // Load PDM
    if (!pose_normalization_) {
      pose_normalization_ = new LTS5::PointDistributionModel();
      pose_normalization_->set_pdm_configuration(configuration_.pose_normalization_config);
    }
    int status = LTS5::ScanStreamForObject(input_stream,
                                           HeaderObjectType::kPointDistributionModel);
    if (!status) {
      error |= pose_normalization_->Load(input_stream, true);
    } else {
      return -1;
    }
    // Load feature space
    error |= LTS5::ReadMatFromBin(input_stream, &feature_basis_);
    // Load normalization
    error |= LTS5::ReadMatFromBin(input_stream, &feature_normalization_mean_);
    error |= LTS5::ReadMatFromBin(input_stream, &feature_normalization_std_);
    // Define number of detector
    int n_detector = 0;
    input_stream.read(reinterpret_cast<char*>(&n_detector), sizeof(n_detector));
    // Clear current detector if any
    if (au_detectors_.size() > 0) {
      this->ReleaseMemory();
    }
    // Load new detectors
    for (int i = 0; i < n_detector; ++i) {
      int status = LTS5::ScanStreamForObject(input_stream,
                                             LTS5::HeaderObjectType::kAUDetector);
      if (status == 0) {
        au_detectors_.push_back(new LTS5::AUDetector());
        error |= au_detectors_[i]->Load(input_stream);
        au_labels_.push_back(au_detectors_[i]->get_au_label());
      }
    }
  }
  return error;
}

/*
 *  @name Load
 *  @fn int Load(const std::string& filename)
 *  @brief  Initialize module with a given filename
 *  @param[in]  filename  Model filename
 *  @return -1 if error, 0 otherwise
 */
int AUAnalysis::Load(const std::string& filename) {
  int error = -1;
  std::string name;
  size_t pos = filename.rfind(".");
  if (pos != std::string::npos) {
    name = filename.substr(0,pos) + ".bin";
  } else {
    name = filename + ".bin";
  }
  // Open stream
  std::ifstream in_stream(name.c_str(),
                          std::ios_base::in | std::ios_base::binary);
  if (in_stream.is_open()) {
    int status = LTS5::ScanStreamForObject(in_stream,
                                           LTS5::HeaderObjectType::kAUAnalysis);
    if (status == 0) {
      error = this->Load(in_stream);
    }
    in_stream.close();
  }
  return error;
}

/*
 *  @name   Write
 *  @fn int Write(std::ofstream& out_stream) const
 *  @brief  Copy the object into a binary stream
 *  @param[in]  out_stream  Output binary stream
 *  @return -1 if error, 0 otherwise
 */
int AUAnalysis::Write(std::ofstream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Ok
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kAUAnalysis);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type),
                     sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size),
                     sizeof(obj_size));
    // Configuration
    error = configuration_.WriteParameterHeader(out_stream);
    // PDM
    error |= pose_normalization_->Write(out_stream);
    // Feature space
    error |= LTS5::WriteMatToBin(out_stream, feature_basis_);
    // Normlaization mean
    error |= LTS5::WriteMatToBin(out_stream,
                                 feature_normalization_mean_);
    // Normlaization std
    error |= LTS5::WriteMatToBin(out_stream,
                                 feature_normalization_std_);
    // Number of detector
    int n_detector = static_cast<int>(au_detectors_.size());
    out_stream.write(reinterpret_cast<const char*>(&n_detector),
                     sizeof(n_detector));
    // Detector
    for (int i = 0; i < n_detector; ++i) {
      //Detector
      error |= au_detectors_[i]->Write(out_stream);
    }
  }
  return error;
}

/*
 *  @name ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the object size in bytes
 *  @return Object memory size
 */
int AUAnalysis::ComputeObjectSize(void) const {
  // Configuration
  int size = configuration_.ComputeHeaderSize();
  // Pdm + header
  size += pose_normalization_->ComputeObjectSize();
  size += 2 * sizeof(int);
  // Feature space
  size += feature_basis_.total() * feature_basis_.elemSize();
  // Feature normalization
  size += (feature_normalization_mean_.total() *
           feature_normalization_mean_.elemSize());
  size += (feature_normalization_std_.total() *
           feature_normalization_std_.elemSize());
  // Matrix header
  size += 3 * 3 * sizeof(int);
  // Number of detector
  size += sizeof(int);
  // Detector size
  for (int i = 0; i < au_detectors_.size(); ++i) {
    // Detector size
    size += au_detectors_[i]->ComputeObjectSize();
    // Detector header
    size += 2 * sizeof(int);
  }
  // Done
  return size;
}

#pragma mark -
#pragma mark Prediction

/*
 *  @name   Predict
 *  @fn void Predict(const cv::Mat& image,
                     const cv::Mat& shape,
                     std::vector<double>* scores)
 *  @brief  Classify one sample for each action unit
 *  @param[in]  image   Source image
 *  @param[in]  shape   Facial landmarks
 *  @param[out] scores  Individual score for each action unit
 */
void AUAnalysis::Predict(const cv::Mat& image,
                         const cv::Mat& shape,
                         std::vector<double>* scores) {
  if (image.channels() > 1) {
    cv::cvtColor(image, working_image_, cv::COLOR_BGR2GRAY);
  } else {
    working_image_ = image;
  }
  // Normalize shape + image
  cv::Mat norm_shape;
  pose_normalization_->NormSDMImage(working_image_,
                                    shape,
                                    true,
                                    &normalized_image_,
                                    &norm_shape);
  // Predict
  this->NormalizedPredict(normalized_image_, norm_shape, scores);
}

/*
 *  @name   NormalizedPredict
 *  @fn void NormalizedPredict(const cv::Mat& image,
                     const cv::Mat& shape,
                     std::vector<double>* scores)
 *  @brief  Classify one sample for each action unit
 *  @param[in]  normalized_image   Normalized source image
 *  @param[in]  normalized_shape   Normalized Facial landmarks
 *  @param[out] scores             Individual score for each action unit
 */
void AUAnalysis::NormalizedPredict(const cv::Mat& normalized_image,
                                   const cv::Mat& normalized_shape,
                                   std::vector<double>* scores) {
  using TransposeType = LTS5::LinearAlgebra<double>::TransposeType;
  // Sanity check
  assert((normalized_image.channels() == 1) &&
         (normalized_image.rows == configuration_.pose_normalization_config.normalize_image_width) &&
         (normalized_image.cols == configuration_.pose_normalization_config.normalize_image_height));

  // ADD Eight extra point to the normalized shape
  cv::Mat augmented_norm_shape = cv::Mat(normalized_shape.rows + 16,
                                         normalized_shape.cols,
                                         normalized_shape.type());
  // Copy norm shape, x
  memcpy(reinterpret_cast<void*>(augmented_norm_shape.data),
         reinterpret_cast<const void*>(normalized_shape.data),
         392); // 49 x 8bytes = 392
  // Copy norm shape, y
  memcpy(reinterpret_cast<void*>(&(augmented_norm_shape.data[456])),
         reinterpret_cast<const void*>(&(normalized_shape.data[392])),
         392); // 49 x 8bytes = 392
  // ADD, mid eyebrows
  double* aug_ptr = reinterpret_cast<double*>(augmented_norm_shape.data);
  aug_ptr[49] = (aug_ptr[4] + aug_ptr[5]) / 2.0;
  aug_ptr[106] = (aug_ptr[61] + aug_ptr[62]) / 2.0;
  // ADD, crow-feet left
  aug_ptr[50] = aug_ptr[0];
  aug_ptr[107] = aug_ptr[76];
  // ADD, crow-feet right
  aug_ptr[51] = aug_ptr[9];
  aug_ptr[108] = aug_ptr[85];
  // ADD, nostril left
  aug_ptr[52] = aug_ptr[14];
  aug_ptr[109] = aug_ptr[69];
  // ADD, nostril right
  aug_ptr[53] = aug_ptr[18];
  aug_ptr[110] = aug_ptr[69];
  // ADD, zygomaticus left
  aug_ptr[54] = aug_ptr[31];
  aug_ptr[111] = aug_ptr[71];
  // ADD, zygomaticus left
  aug_ptr[55] = aug_ptr[37];
  aug_ptr[112] = aug_ptr[75];
  // ADD, Chin
  // (Assumes the vertical distance between mid-eyebrows and nose tip
  // is equal to that between nose tip and chin tip  )
  double dist = (aug_ptr[16 + 57] - aug_ptr[61]) + aug_ptr[73];
  aug_ptr[56] = aug_ptr[40];
  aug_ptr[113] = (aug_ptr[97] + dist) / 2.0;
  // Extract features
  cv::Mat sift_features;
  int n_inner_points = std::max(augmented_norm_shape.rows,
                                augmented_norm_shape.cols) / 2;
#ifndef HAS_INTRAFACE_DESCR
  float x, y;
  if (shape_keypoint_.size() != n_inner_points) {
    shape_keypoint_.clear();
    for (int i = 0; i < n_inner_points; ++i) {
      x = (float)augmented_norm_shape.at<double>(i);
      y = (float)augmented_norm_shape.at<double>(i + n_inner_points);
      shape_keypoint_.push_back(cv::KeyPoint(x, y, configuration_.sift_size));
    }
  } else {
    auto kp_it = shape_keypoint_.begin();
    for (int i = 0; i < n_inner_points; ++i, ++kp_it) {
      x = (float)augmented_norm_shape.at<double>(i);
      y = (float)augmented_norm_shape.at<double>(i + n_inner_points);
      kp_it->pt.x = x;
      kp_it->pt.y = y;
    }
  }
  LTS5::SSift::ComputeDescriptor(normalized_image,
                                 shape_keypoint_,
                                 configuration_.sift_configuration,
                                 &sift_features);
#else
  cv::Mat kp = cv::Mat(2, n_inner_points, CV_32FC1);
  for (int p = 0; p < n_inner_points; ++p) {
    kp.at<float>(0, p) = normalized_shape.at<double>(p);
    kp.at<float>(1, p) = normalized_shape.at<double>(p + n_inner_points);
  }
  cv::Mat img64;
  normalized_image.convertTo(img64, CV_64FC1);
  INTRAFACE::XXDescriptor xxd(4);
  xxd.Compute(img64, kp, sift_features);
#endif
  sift_features = sift_features.reshape(sift_features.channels(),1);
  sift_features.convertTo(sift_features, CV_64FC1);
  // Classify with all classifier
  scores->clear();
  scores->reserve(au_detectors_.size());
  // Project data into pca subspace and normalize
  cv::Mat sample;
  double* sample_ptr;
  const double* mean_ptr;
  const double* std_ptr;
  int n_elem;
  LinearAlgebra<double>::Gemv(feature_basis_,
                              TransposeType::kNoTranspose,
                              1.0,
                              sift_features,
                              0.0,
                              &sample);
  sample_ptr = reinterpret_cast<double*>(sample.data);
  mean_ptr = reinterpret_cast<const double*>(feature_normalization_mean_.data);
  std_ptr = reinterpret_cast<const double*>(feature_normalization_std_.data);
  n_elem = static_cast<int>(feature_normalization_mean_.total());
  for(int k = 0; k < n_elem; ++k) {
    *sample_ptr -= *mean_ptr++;
    *sample_ptr /= *std_ptr++;
    ++sample_ptr;
  }
  // Predict
  for (int i = 0; i < au_detectors_.size(); ++i) {
    scores->push_back(au_detectors_[i]->Predict(sample));
  }
}

#pragma mark -
#pragma mark Private

/*
 *  @name   ReleaseMemory
 *  @fn void ReleaseMemory(void)
 *  @brief  Release classifer and stuff
 */
void AUAnalysis::ReleaseMemory(void) {
  // Clear detectors
  au_labels_.clear();
  for (int i = 0; i < au_detectors_.size(); ++i) {
    delete au_detectors_[i];
  }
  // Clear pdm
  if (pose_normalization_ != nullptr) {
    delete pose_normalization_;
    pose_normalization_ = nullptr;
  }
}

}  // namepsace LTS5
