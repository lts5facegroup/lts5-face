/**
 *  @file   expression_analysis.cpp
 *  @brief  Detection of facial emotion class definition
 *  @author Ecabert Christophe
 *  @date   20/06/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <limits>

#include "opencv2/imgproc/imgproc.hpp"

#ifdef HAS_INTRAFACE
#include "XXDescriptor.h"
#endif

#include "lts5/facial_analysis/expression_analysis.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   ExpressionAnalysis
 *  @brief  Constructor
 */
ExpressionAnalysis::ExpressionAnalysis(void) : pose_normalization_(nullptr),
  do_feature_normalization_(true) {
  expression_detectors_.clear();
}

/*
 *  @name   ~ExpressionAnalysis
 *  @brief  Destructor
 */
ExpressionAnalysis::~ExpressionAnalysis(void) {
  // Release memory
  this->ReleaseMemory();
}

/*
 *  @name Load
 *  @brief  Initialize module with a given stream
 *  @param[in]  input_stream  Input stream pointing to models
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysis::Load(std::ifstream& input_stream) {
  int error = -1;
  if (input_stream.good()) {
    // Read configuration
    error = configuration_.ReadParameterHeader(input_stream);
    // Load Pdm
    if (pose_normalization_ == nullptr) {
      pose_normalization_ = new LTS5::PointDistributionModel();
      pose_normalization_->set_pdm_configuration(configuration_.pose_normalization_config);
    }
    int status = LTS5::ScanStreamForObject(input_stream,
                                           HeaderObjectType::kPointDistributionModel);
    if (status == 0) {
      error |= pose_normalization_->Load(input_stream, true);
    } else {
      return -1;
    }
    // Load feature space
    error |= LTS5::ReadMatFromBin(input_stream, &feature_basis_);
    // Load normalization data
    error |= LTS5::ReadMatFromBin(input_stream, &feature_normalization_mean_);
    error |= LTS5::ReadMatFromBin(input_stream, &feature_normalization_std_);
    // Define number of detector
    int n_detector = 0;
    input_stream.read(reinterpret_cast<char*>(&n_detector), sizeof(n_detector));
    // Clear current detector if any
    if (expression_detectors_.size() > 0) {
      this->ReleaseMemory();
    }
    // Load new detectors
    for (int i = 0; i < n_detector; ++i) {
      int status = LTS5::ScanStreamForObject(input_stream,
                                             LTS5::HeaderObjectType::kExpressionDetector);
      if (status == 0) {
        expression_detectors_.push_back(new LTS5::ExpressionDetector());
        error |= expression_detectors_[i]->Load(input_stream);
        emotion_labels_.emplace(expression_detectors_[i]->get_emotion_label(),i);
      }
    }
  }
  return error;
}

/*
 *  @name Load
 *  @brief  Initialize module with a given stream
 *  @param[in]  input_stream    Input stream pointing to models
 *  @param[in]  emotion_labels  List of emotion of interest
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysis::Load(std::ifstream& input_stream,
                          const std::vector<std::string>& emotion_labels) {
  int error = -1;
  if (input_stream.good()) {
    // Ok, read configuration
    error = configuration_.ReadParameterHeader(input_stream);
    // Load PDM
    if (pose_normalization_ == nullptr) {
      pose_normalization_ = new LTS5::PointDistributionModel();
      pose_normalization_->set_pdm_configuration(configuration_.pose_normalization_config);
    }
    int status = LTS5::ScanStreamForObject(input_stream,
                                           HeaderObjectType::kPointDistributionModel);
    if (status == 0) {
      error |= pose_normalization_->Load(input_stream, true);
    }
    // Load feature space
    error = LTS5::ReadMatFromBin(input_stream, &feature_basis_);
    // Load normalization
    error |= LTS5::ReadMatFromBin(input_stream, &feature_normalization_mean_);
    error |= LTS5::ReadMatFromBin(input_stream, &feature_normalization_std_);
    // Define number detector
    int n_detector_file = 0;
    input_stream.read(reinterpret_cast<char*>(&n_detector_file), sizeof(n_detector_file));
    int n_detector = static_cast<int>(emotion_labels.size());
    if (n_detector > n_detector_file) {
      // More label than emotion detector in the model
      error = -1;
      return error;
    }
    // Ok, labels don't exceed model
    if (expression_detectors_.size() > 0) {
      this->ReleaseMemory();
    }
    // Load new detectors
    for (int i = 0; i < n_detector; ++i) {
      // Load specific detector
      int status = LTS5::ScanStreamForObject(input_stream,
                                             LTS5::HeaderObjectType::kExpressionDetector);
      if (status == 0) {
        expression_detectors_.push_back(new LTS5::ExpressionDetector());
        error |= expression_detectors_[i]->Load(input_stream, emotion_labels[i]);
        emotion_labels_.emplace(expression_detectors_[i]->get_emotion_label(),i);
      }
    }
  }
  return error;
}

/*
 *  @name Load
 *  @brief  Initialize module with a given filename
 *  @param[in]  filename  Model filename
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysis::Load(const std::string& filename) {
  int error = -1;
  std::string name;
  size_t pos = filename.rfind(".");
  if (pos != std::string::npos) {
    name = filename.substr(0,pos) + ".bin";
  } else {
    name = filename + ".bin";
  }
  // Open stream
  std::ifstream in_stream(name.c_str(),
                          std::ios_base::in | std::ios_base::binary);
  if (in_stream.is_open()) {
    int status = LTS5::ScanStreamForObject(in_stream,
                                           LTS5::HeaderObjectType::kExpressionAnalysis);
    if (status == 0) {
      error = this->Load(in_stream);
    }
    in_stream.close();
  }
  return error;
}

/*
 *  @name Load
 *  @brief  Initialize module with a given filename
 *  @param[in]  filename  Model filename
 *  @param[in]  emotion_labels  List of emotion of interest
 *  @return -1 if error, 0 otherwise
 */
int ExpressionAnalysis::Load(const std::string& filename,
                          const std::vector<std::string>& emotion_labels) {
  int error = -1;
  std::string name;
  size_t pos = filename.rfind(".");
  if (pos != std::string::npos) {
    name = filename.substr(0,pos) + ".bin";
  } else {
    name = filename + ".bin";
  }
  // Open stream
  std::ifstream in_stream(name.c_str(),
                          std::ios_base::in | std::ios_base::binary);
  if (in_stream.is_open()) {
    int status = LTS5::ScanStreamForObject(in_stream,
                                           LTS5::HeaderObjectType::kExpressionAnalysis);
    if (status == 0) {
      error = this->Load(in_stream, emotion_labels);
    }
    in_stream.close();
  }
  return error;
}

/*
 *  @name   Write
 *  @brief  Copy the object into a binary stream
 *  @param[in]  out_stream  Output binary stream
 *  @return -1 if error, 0 otherwise
 */
  int ExpressionAnalysis::Write(std::ofstream& out_stream) const {
    int error = -1;
    if (out_stream.good()) {
      // Ok
      int obj_type = static_cast<int>(LTS5::HeaderObjectType::kExpressionAnalysis);
      int obj_size = this->ComputeObjectSize();
      out_stream.write(reinterpret_cast<const char*>(&obj_type),
                       sizeof(obj_type));
      out_stream.write(reinterpret_cast<const char*>(&obj_size),
                       sizeof(obj_size));
      // Configuration
      error = configuration_.WriteParameterHeader(out_stream);
      // PDM
      error |= pose_normalization_->Write(out_stream);
      // Feature space
      error |= LTS5::WriteMatToBin(out_stream, feature_basis_);
      // Normlaization mean
      error |= LTS5::WriteMatToBin(out_stream,
                                   feature_normalization_mean_);
      // Normlaization std
      error |= LTS5::WriteMatToBin(out_stream,
                                   feature_normalization_std_);
      // Number of detector
      int n_detector = static_cast<int>(expression_detectors_.size());
      out_stream.write(reinterpret_cast<const char*>(&n_detector),
                       sizeof(n_detector));
      // Detector
      for (int i = 0; i < n_detector; ++i) {
        //Detector
        error |= expression_detectors_[i]->Write(out_stream);
      }
    }
    return error;
  }

/*
 *  @name ComputeObjectSize
 *  @brief  Compute the object size in bytes
 *  @return Object memory size
 */
int ExpressionAnalysis::ComputeObjectSize(void) const {
  // Configuration
  int size = configuration_.ComputeHeaderSize();
  // Pdm + header
  size += pose_normalization_->ComputeObjectSize();
  size += 2 * sizeof(int);
  // Feature space
  size += feature_basis_.total() * feature_basis_.elemSize();
  // Feature normalization
  size += (feature_normalization_mean_.total() *
           feature_normalization_mean_.elemSize());
  size += (feature_normalization_std_.total() *
           feature_normalization_std_.elemSize());
  // Matrix header
  size += 3 * 3 * sizeof(int);
  // Number of detector
  size += sizeof(int);
  // Detector size
  for (int i = 0; i < expression_detectors_.size(); ++i) {
    // Detector size
    size += expression_detectors_[i]->ComputeObjectSize();
    // Detector header
    size += 2 * sizeof(int);
  }
  // Done
  return size;
}

#pragma mark -
#pragma mark Prediction

/*
 *  @name   Predict
 *  @brief  Classify one sample for each emotion
 *  @param[in]  image   Source image
 *  @param[in]  shape   Facial landmarks
 *  @param[out] scores  Individual score for each emotion
 */
void ExpressionAnalysis::Predict(const cv::Mat& image,
                                 const cv::Mat& shape,
                                 std::vector<double>* scores) {
  using TransposeType = LTS5::LinearAlgebra<double>::TransposeType;
  if (image.channels() > 1) {
    cv::cvtColor(image, working_image_, cv::COLOR_BGR2GRAY);
  } else {
    working_image_ = image;
  }
  // Normalize input image
  cv::Mat normalized_shape;
  int n_pts = std::max(shape.rows,
                       shape.cols) / 2;
  if (n_pts != 68) {
    pose_normalization_->NormSDMImage_intra(working_image_,
                                            shape,
                                            &normalized_image_,
                                            &normalized_shape);
  } else {
    pose_normalization_->NormSDMImage(working_image_,
                                      shape,
                                      true,
                                      &normalized_image_,
                                      &normalized_shape);
  }
  // Illumination normalization
#if defined(USE_MEAN_NORMALIZATION)
  cv::Mat img32;
  normalized_image_.convertTo(img32, CV_32FC1);
  const int n_elem_img = static_cast<int>(img32.total());
  float* img_ptr = reinterpret_cast<float*>(img32.data);
  float sum = 0;
  for (int i = 0; i < n_elem_img; ++i) {
    sum += *img_ptr++;
  }
  const float img_mean = sum/static_cast<float>(n_elem_img);
  img_ptr = reinterpret_cast<float*>(img32.data);
  for (int i = 0; i < n_elem_img; ++i) {
    *img_ptr++ /= img_mean;
  }
  // TODO:(Christophe) Check if it can be avoid in future
  // Copy back into source Image
  img32.copyTo(normalized_image_);
#elif defined(USE_HIST_EQUA_NORMALIZATION)
  // Apply histogram equalization first
  cv::equalizeHist(normalized_image_, normalized_image_);
#endif

  // Extract features
  cv::Mat sift_features;
  int n_inner_points = normalized_shape.rows / 2;

#ifndef HAS_INTRAFACE_DESCR
  float x, y;
  if (shape_keypoint_.size() != n_inner_points) {
    shape_keypoint_.clear();
    for (int i = 0; i < n_inner_points; ++i) {
      x = (float)normalized_shape.at<double>(i);
      y = (float)normalized_shape.at<double>(i + n_inner_points);
      shape_keypoint_.push_back(cv::KeyPoint(x, y, configuration_.sift_size));
    }
  } else {
    auto kp_it = shape_keypoint_.begin();
    for (int i = 0; i < n_inner_points; ++i, ++kp_it) {
      x = (float)normalized_shape.at<double>(i);
      y = (float)normalized_shape.at<double>(i + n_inner_points);
      kp_it->pt.x = x;
      kp_it->pt.y = y;
    }
  }
  LTS5::SSift::ComputeDescriptorOpt(normalized_image_,
                                    shape_keypoint_,
                                    configuration_.sift_configuration,
                                    &features_);
#else
      cv::Mat kp = cv::Mat(2, n_inner_points, CV_32FC1);
      for (int p = 0; p < n_inner_points; ++p) {
        kp.at<float>(0, p) = normalized_shape.at<double>(p);
        kp.at<float>(1, p) = normalized_shape.at<double>(p + n_inner_points);
      }

      cv::Mat img64;
      normalized_image.convertTo(img64, CV_64FC1);
      INTRAFACE::XXDescriptor xxd(4);
      xxd.Compute(img64, kp, sift_features);

#endif

  features_ = features_.reshape(features_.channels(),1);
  features_.convertTo(sift_features, CV_64FC1);
  // Classify with all classifier
  scores->clear();
  scores->reserve(expression_detectors_.size());
  // Project data into pca subspace and normalize
  cv::Mat sample;
  double* sample_ptr;
  const double* mean_ptr;
  const double* std_ptr;
  int n_elem;
  LinearAlgebra<double>::Gemv(feature_basis_,
                              TransposeType::kNoTranspose,
                              1.0,
                              sift_features,
                              0.0,
                              &sample);
  sample_ptr = reinterpret_cast<double*>(sample.data);
  mean_ptr = reinterpret_cast<const double*>(feature_normalization_mean_.data);
  std_ptr = reinterpret_cast<const double*>(feature_normalization_std_.data);
  n_elem = static_cast<int>(feature_normalization_mean_.total());
  for(int k = 0; k < n_elem; ++k) {
    *sample_ptr -= *mean_ptr++;
    *sample_ptr /= *std_ptr++;
    ++sample_ptr;
  }
  // Predict
  for (int i = 0; i < expression_detectors_.size(); ++i) {
    scores->push_back(expression_detectors_[i]->Predict(sample));
  }
}

/*
 *  @name   Predict
 *  @brief  Classify one sample for a specific emotion
 *  @param[in]  image         Source image
 *  @param[in]  shape         Facial landmarks
 *  @param[in]  emotion_label Label of the emotion of interest
 *  @param[out] scores        Individual score for each emotion
 */
void ExpressionAnalysis::Predict(const cv::Mat& image,
                              const cv::Mat& shape,
                              const std::vector<std::string>& emotion_label,
                              std::vector<double>* scores) {
  throw std::runtime_error("Not valid implementation");
  // Image channels ?
  /*if (image.channels() > 1) {
    cv::cvtColor(image, working_image_, cv::COLOR_BGR2GRAY);
  } else {
    working_image_ = image;
  }
  // Normalize input image
  cv::Mat clm_shape;
  cv::Mat normalized_shape;
  cv::Mat normalized_image;
  LTS5::Convert68ShapeTo66(shape, &clm_shape);
  pose_normalization_->NormSDMImage(working_image_,
                                    clm_shape,
                                    true,
                                    &normalized_image,
                                    &normalized_shape);
  // Extract features
  cv::Mat inner_shape;
  cv::Mat sift_features;
  LTS5::ExtractInnerShape(normalized_shape, &inner_shape);
  int n_inner_points = inner_shape.rows / 2;
  float x, y;
  if (shape_keypoint_.size() != n_inner_points) {
    shape_keypoint_.clear();
    for (int i = 0; i < n_inner_points; ++i) {
      x = (float)inner_shape.at<double>(i);
      y = (float)inner_shape.at<double>(i + n_inner_points);
      shape_keypoint_.push_back(cv::KeyPoint(x, y, configuration_.sift_size));
    }
  } else {
    auto kp_it = shape_keypoint_.begin();
    for (int i = 0; i < n_inner_points; ++i, ++kp_it) {
      x = (float)inner_shape.at<double>(i);
      y = (float)inner_shape.at<double>(i + n_inner_points);
      kp_it->pt.x = x;
      kp_it->pt.y = y;
    }
  }
  LTS5::SSift::ComputeDescriptor(normalized_image,
                                 shape_keypoint_,
                                 configuration_.sift_configuration,
                                 &sift_features);
  sift_features = sift_features.reshape(sift_features.channels(),1);
  sift_features.convertTo(sift_features, CV_64FC1);
  // Prepare output
  scores->clear();
  scores->reserve(emotion_label.size());
  // Project data into pca subspace and normalize
  cv::Mat sample;
  double* sample_ptr;
  const double* mean_ptr;
  const double* std_ptr;
  int n_elem;
  LinearAlgebra::Gemv(feature_basis_,
                      CBLAS_TRANSPOSE::CblasNoTrans,
                      1.0,
                      sift_features,
                      0.0,
                      &sample);
  sample_ptr = reinterpret_cast<double*>(sample.data);
  mean_ptr = reinterpret_cast<const double*>(feature_normalization_mean_.data);
  std_ptr = reinterpret_cast<const double*>(feature_normalization_std_.data);
  n_elem = static_cast<int>(feature_normalization_mean_.total());
  for(int k = 0; k < n_elem; ++k) {
    *sample_ptr -= *mean_ptr++;
    *sample_ptr /= *std_ptr++;
    ++sample_ptr;
  }
  for (int i = 0 ; i < emotion_label.size(); ++i) {
    // find corresponding key
    auto it = emotion_labels_.find(emotion_label[i]);
    if (it != emotion_labels_.end()) {
      scores->push_back(expression_detectors_[i]->Predict(sample));
    } else {
      scores->push_back(std::numeric_limits<double>::min());
    }
  }*/
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name   get_emotion_labels
 *  @brief  Give access to emotion labels
 *  @param[out] emotion_labels  List of emotion labels
 */
void ExpressionAnalysis::get_expression_labels(std::vector<std::string>* expression_labels) const {
  // Clear current vector
  expression_labels->clear();
  expression_labels->resize(emotion_labels_.size(),"x");
  // Copy labels
  std::unordered_map<std::string,int>::const_iterator label_it = emotion_labels_.cbegin();
  for (; label_it != emotion_labels_.cend(); ++label_it) {
    expression_labels->at(label_it->second) = label_it->first;
  }
}

#pragma mark -
#pragma mark Private

/*
 *  @name   ReleaseMemory
 *  @brief  Release classifer and stuff
 */
void ExpressionAnalysis::ReleaseMemory(void) {
  // Clear map
  emotion_labels_.clear();
  // Clear detectors
  for (int i = 0; i < expression_detectors_.size(); ++i) {
    delete expression_detectors_[i];
  }
  // Clear pdm
  if (pose_normalization_ != nullptr) {
    delete pose_normalization_;
    pose_normalization_ = nullptr;
  }
}

}  // namespace LTS5
