/**
 *  @file   au_detector.cpp
 *  @brief  Detection of action unit
 *
 *  @author Christophe Ecabert
 *  @date   11/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <fstream>

#include "lts5/utils/file_io.hpp"
#include "lts5/facial_analysis/au_detector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   AUDetector
 *  @fn AUDetector()
 *  @brief  Constructor
 */
AUDetector::AUDetector() : classifier_(nullptr), au_label_("")  {
}

/*
 *  @name   ~AUDetector
 *  @fn ~AUDetector()
 *  @brief  Destructor
 */
AUDetector::~AUDetector() {
  if (classifier_) {
    delete classifier_;
    classifier_ = nullptr;
  }
}

/*
 *  @name   Load
 *  @fn int Load(const std::string& filename)
 *  @brief  Load saved SVM model
 *  @param[in] filename File to load the model from
 *  @return -1 if error, 0 otherwise
 */
int AUDetector::Load(const std::string& filename) {
  int error = -1;
  std::string name;
  size_t pos = filename.rfind(".");
  if (pos != std::string::npos) {
    name = filename.substr(0,pos) + ".bin";
  } else {
    name = filename + ".bin";
  }
  std::ifstream input_stream(name.c_str(),
                             std::ios_base::in | std::ios_base::binary);
  if (input_stream.is_open()) {
    // Look for correct header
    int status = LTS5::ScanStreamForObject(input_stream,
                                           LTS5::HeaderObjectType::kAUDetector);
    if (status == 0) {
      error = this->Load(input_stream);
    }
    // Close stream
    input_stream.close();
  }
  return error;
}

/*
 *  @name   Load
 *  @fn int Load(std::ifstream& input_stream)
 *  @brief  Load saved SVM model
 *  @param[in] input_stream Binary stream to load the model from
 *  @return -1 if error, 0 otherwise
 */
int AUDetector::Load(std::ifstream& input_stream) {
  int error = -1;
  if (input_stream.good()) {
    // Load
    int dummy = 0;
    // Read label size
    input_stream.read(reinterpret_cast<char*>(&dummy), sizeof(dummy));
    // Set + read label
    this->au_label_ = std::string(dummy,'x');
    input_stream.read(const_cast<char*>(au_label_.data()),
                      dummy * sizeof(char));
    // Load svm
    if (this->classifier_ != nullptr) {
      delete this->classifier_;
    }
    classifier_ = new BinaryLinearSVMClassifier();
    int status = LTS5::ScanStreamForObject(input_stream,
                                           HeaderObjectType::kBinaryLinearSvm);
    if (status == 0) {
      error = classifier_->Load(input_stream);
      // Done sanity check
      error |= input_stream.good() ? 0 : -1;
    } else {
      error = -1;
    }
  }
  return error;
}

/*
 *  @name   Write
 *  @fn int Write(std::ofstream& output_stream) const
 *  @brief  Dump object into the given binary stream
 *  @param[in] output_stream  Output stream to file
 *  @return -1 if error, 0 otherwise
 */
int AUDetector::Write(std::ofstream& output_stream) const {
  int error = -1;
  if (output_stream.good()) {
    // Can write,
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kExpressionDetector);
    int obj_size = this->ComputeObjectSize();
    // Start to write
    output_stream.write(reinterpret_cast<const char*>(&obj_type),
                        sizeof(obj_type));
    output_stream.write(reinterpret_cast<const char*>(&obj_size),
                        sizeof(obj_size));
    // Label length
    int label_length = static_cast<int>(au_label_.length());
    output_stream.write(reinterpret_cast<const char*>(&label_length),
                        sizeof(label_length));
    // Label
    output_stream.write(reinterpret_cast<const char*>(au_label_.data()),
                        label_length);
    // Classifier
    error = classifier_->Save(output_stream);
    // Done sanity check
    error |= output_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the object size in bytes
 *  @return Object memory size
 */
int AUDetector::ComputeObjectSize(void) const {
  int size = static_cast<int>(au_label_.length());
  size += 3 * sizeof(int); // Label length + svm header
  size += classifier_->ComputeObjectSize();
  return size;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   Predict
 *  @fn double Predict(const cv::Mat& features) const
 *  @brief  Predict emotion class
 *  @param[in] features Sample to classify
 *  @return emotion class
 */
double AUDetector::Predict(const cv::Mat& features) const {
  //Predict
  return this->classifier_->Classify(features.rowRange(0, classifier_->GetFeatureLength()));
}
}  // namepsace LTS5
