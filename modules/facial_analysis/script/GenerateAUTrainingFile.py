""" Generate training file for action unit detector """
import sys
import os
import glob
import shutil
import re
__author__ = 'Christophe Ecabert'


class AUTrainGenerator:
  """ Action Unit training generator """

  def __init__(self, data_folder):
    """
    Constructor
    :param data_folder:  Folder where training data are stored
    """
    # Root folder to look from
    self.root_folder = data_folder
    if self.root_folder[-1] != '/':
      self.root_folder += '/'
    # Supported AU
    self.au_label = {1: ('AU_01', 0x0001),
                     2: ('AU_02', 0x0002),
                     4: ('AU_04', 0x0004),
                     6: ('AU_06', 0x0008),
                     7: ('AU_07', 0x0010),
                     10: ('AU_10', 0x0020),
                     12: ('AU_12', 0x0040),
                     14: ('AU_14', 0x0080),
                     15: ('AU_15', 0x0100),
                     17: ('AU_17', 0x0200),
                     23: ('AU_23', 0x0400),
                     25: ('AU_25', 0x0800),
                     28: ('AU_28', 0x1000),
                     45: ('AU_45', 0x2000)}
    # Training data
    self.training_data = []

  def scan_folder(self, folder_name, file_ext):
    """
    Search recursively in folder for files with a given extension

    :param folder_name: Folder to search in
    :param file_ext:  File extension of interest
    :return:  List of file with the given extension
    """
    pick_file = []
    for root, _, files in os.walk(self.root_folder + folder_name):
      # Loop over files
      for f in files:
        # Correct extension ?
        if f.endswith(file_ext):
          pick_file.append(root + '/' + f)
    return pick_file

  def export_bp4d_data(self, folder_name, output_name):
    """
    Reformat BP4D dataset into standard data structor
    :param folder_name: Location of the BP4D Dataset
    :param label_folder_name: Location of the label folder
    :param output_name: Output name
    """
    # Open label
    fname = self.root_folder + folder_name + 'bp4d_ground.csv'
    with open(fname, 'r') as f_label:
      # Gather list of image
      img_list = glob.glob(self.root_folder + folder_name + '*.jpg')
      img_list.sort()
      # Create list of label writer
      lbl_writers = {}
      id_map = {}
      for row in f_label:
        part = row.strip('\n').split(',')
        dummy = part[0]
        session_id = part[1]
        n_frame = part[2]
        au_label = part[3:]
        # Define image path, find gender
        pattern = '_T' + session_id + '_' + n_frame.zfill(4) + '.jpg'
        val = [s for s in img_list if pattern in s]
        if not val:
          print('ERROR, unable to find corresponding image : ' + pattern)
          continue
        elif len(val) != 1:
          # Multiple match, need to select proper one
          key = id_map.get(dummy)
          if key:
            # ID known
            val = [s for s in val if key in s]
          else:
            # ID not known, take first who's not in the dict
            for v in val:
              idx = v.rfind(pattern)
              tag = v[idx - 4: idx]
              if not tag in id_map.values():
                id_map[dummy] = tag
                val = [v]
                break
        else:
          # Update id map
          if dummy not in id_map:
            idx = val[0].rfind(pattern)
            id_map[dummy] = val[0][idx - 4 : idx]

        idx = val[0].rfind(pattern)
        subject_id = val[0][idx - 4 : idx]
        img_path = self.root_folder + folder_name + subject_id + pattern
        # define output folder
        out_folder = output_name + subject_id + '/T' + session_id + '/'
        out_name = out_folder + n_frame.zfill(4) + '.jpg'
        if not os.path.exists(out_folder):
          os.makedirs(out_folder)
        # Copy image
        shutil.copy(img_path, out_name)
        # Is label writer already in the map ?
        key = (subject_id, 'T' + session_id)
        writer = lbl_writers.get(key)
        if not writer:
          writer = open(out_folder + '_label.csv', 'w')
          lbl_writers[key] = writer
          # Write header
          writer.write('image_name')
          for _, value in self.au_label.items():
            writer.write(',' + value[0])
          writer.write('\n')

        # write into corresponding writer
        writer.write(n_frame.zfill(4) + '.jpg,')
        writer.write(','.join(au_label))
        writer.write('\n')
        writer.flush()
      # Close writer
      for w in lbl_writers.values():
        w.close()

  def export_semaine_data(self, folder_name, output_name):
    """
    Reformat SEMAINE dataset into standard data structor
    :param folder_name: Location of the SEMAINE Dataset
    :param label_folder_name: Location of the label folder
    :param output_name: Output name
    """
    # Open label
    fname = self.root_folder + folder_name + 'semaine_ground.csv'
    with open(fname, 'r') as f_label:
      # Gather list of image
      img_list = glob.glob(self.root_folder + folder_name + '*.jpg')
      img_list.sort()
      # Create list of label writer
      lbl_writers = {}
      id_map = {}
      # Loop over label
      for row in f_label:
        part = row.strip('\n').split(',')
        dummy = part[0]
        n_frame = part[1]
        au_label = part[2:]
        # Define image path, find subject ID
        pattern = '_frame_' + n_frame + '.0000.jpg'
        val = [s for s in img_list if pattern in s]
        if not val:
          print('ERROR, unable to find corresponding image : ' + pattern)
          continue
        elif len(val) != 1:
          # Multiple possibility
          key = id_map.get(dummy)
          if key:
            # ID known
            val = [s for s in val if key in s]
          else:
            # ID not known, take first who's not in the dict
            for v in val:
              idx = v.rfind(pattern)
              tag = v[idx - 5: idx]
              if tag not in id_map.values():
                id_map[dummy] = tag
                val = [v]
                break
        else:
          # Single find ID and add it to the map
          if dummy not in id_map:
            idx = val[0].rfind(pattern)
            id_map[dummy] = val[0][idx - 5: idx]
        # Proper file selected, define image path
        idx = val[0].rfind(pattern)
        subject_id = val[0][idx - 5: idx]
        img_path = self.root_folder + folder_name + subject_id + pattern
        # Define output folder+name, create if does not exist
        out_folder = output_name + subject_id + '/'
        out_image_name = out_folder + 'frame_' + n_frame + '.jpg'
        # Folder struct exist ?
        if not os.path.exists(out_folder):
          os.makedirs(out_folder)
        # Copy image
        shutil.copy(img_path, out_image_name)
        # Label
        writer = lbl_writers.get(subject_id)
        if not writer:
          # create
          writer = open(out_folder + '_label.csv', 'w')
          lbl_writers[subject_id] = writer
          # write header
          writer.write('image_name')
          for _, value in self.au_label.items():
            writer.write(',' + value[0])
          writer.write('\n')
        # write into corresponding writer
        writer.write('frame_' + n_frame + '.jpg,')
        writer.write(','.join(au_label))
        writer.write('\n')
        writer.flush()
      # Close writer
      for w in lbl_writers.values():
        w.close()

  # Reformat GEMEP data/label into proper data structure
  def export_gemep_data(self, folder_name, output_name):
    """
    Reformat GEMEP dataset into standard data structor
    :param folder_name: Location of the GEMEP Dataset
    :param label_folder_name: Location of the label folder
    :param output_name: Output name
    """
    # Open label
    fname = self.root_folder + folder_name + 'Gemep_ground.csv'
    with open(fname, 'r') as f_label:
      # Output folder
      if not os.path.exists(output_name):
        os.makedirs(output_name)
      # LAbel
      writer = open(output_name + '_label.csv', 'w')
      writer.write('image_data')
      for _, value in self.au_label.items():
        writer.write(',' + value[0])
      writer.write('\n')
      # Loop over data
      next(f_label)
      for row in f_label:
        part = row.strip('\n').split(',')
        # Get image name
        img_name = self.root_folder + folder_name + part[0]
        # define output name
        part_name = part[0].split('_')
        out_img_name = output_name + part_name[2] + '_' + part_name[1] + '_' \
                       + part_name[3]
        # Copy if exist
        if os.path.isfile(img_name):
          shutil.copy(img_name, out_img_name)
          # Setup label
          writer.write(part_name[2] + '_' + part_name[1] + '_' + \
                       part_name[3] + ',')
          writer.write(','.join(map(str,
                                    map(int, map(float,
                                                 part[1:len(part) - 1])))))
          writer.write('\n')
          writer.flush()
        else:
          print('ERROR, Unable to find : ' + img_name)
    return

  def append_bp4d(self, folder_name):
    """
    Add BP4D to the test

    :param folder_name: Where the dataset is stored
    """
    # Scan the whole folder for label files (*.csv)
    label_files = self.scan_folder(folder_name, '.csv')
    # Sort
    label_files.sort()
    # Loop over all file
    for label_file in label_files:
      with open(label_file, 'r') as f_label:
        # Map
        label_map = {}
        # Define folder
        current_folder = label_file[0:label_file.rfind('/')+1]
        # Check header
        label_ok = True
        header = f_label.readline()
        header_part = header.strip('\n').split(',')
        for _, value in self.au_label.items():
          if not value[0] in header:
            label_ok = False
            print('Missing labels, abort !')
            break
          else:
            # Build correspondance between au label and column indexes
            idx = header_part.index(value[0])
            label_map[value[0]] = idx
        if label_ok:
          # Loop over label
          for row in f_label:
            row_part = row.strip('\n').split(',')
            label = 0
            for _, value in self.au_label.items():
              # get part index
              part_idx = label_map[value[0]]
              if int(row_part[part_idx]) != 0:
                label += value[1]
            # Create entry
            self.training_data.append((current_folder + row_part[0], label))

  def append_ck_plus(self, folder_name):
    """
    Add CK+ to the test

    :param folder_name: Where the dataset is stored
    """
    # Scan for FACS labels
    label_files = self.scan_folder(folder_name, '.txt')
    label_files.sort()
    # Loop over
    for label_file in label_files:
      # Get corresponding image path
      image_path = label_file[0:label_file.rfind('_facs.txt')] + '.png'
      # Change Emotion folder to image folder
      image_path = re.sub('(FACS)', 'cohn-kanade-images', image_path)
      # Read label
      with open(label_file, 'r') as f_label:
        label = 0
        for au in f_label:
          part = list(map(float, au.strip('\n').split()))
          if part:
            value = self.au_label.get(int(part[0]))
            if value:
              label += value[1]
        self.training_data.append((image_path, label))
        # Create entry for neutral
        image_path = image_path[0:len(image_path)- 12] + '00000001.png'
        self.training_data.append((image_path, 0))
    return

  def save(self, filename):
    """
    Save testing file
    :param filename:  Location where to save
    """
    part = filename.strip('\n').split('.')
    if len(part) == 1:
      filename += '.txt'
    elif part[-1] != 'txt':
      part[-1] = '.txt'
      filename = ''.join(part)
    # Open file
    with open(filename, 'w') as f:
      # Iterate over training data
      for (path, label) in self.training_data:
        f.write(path + ' ' + str(label) + '\n')
    return


# How to call script
def use():
  print(sys.argv[0] + ' <data_root_folder> <#dataset> <output_file>')
  print('#dataset values :')
  print('<0 Reformat data into proper data structure')
  print('BP4D : 1')
  print('SEMAINE : 2')
  print('GEMEP : 4')
  print('CK+ : 8')


if __name__ == '__main__':
  if len(sys.argv) != 4:
    use()
  else:
    # Create generator
    gen = AUTrainGenerator(sys.argv[1])
    # Selection
    sel = int(sys.argv[2])
    # Export data struct
    if sel < 0:
      # Reformat SEMAINE training
      print('Reformat BP4D training samples/labels')
      gen.export_bp4d_data('bp4d/', sys.argv[3] + '/bp4d/training/')
      print('Reformat SEMAINE training samples/labels')
      gen.export_semaine_data('semaine/', sys.argv[3] + '/semaine/training/')
      print('Reformat GEMEP training samples/labels')
      gen.export_gemep_data('gemep/', sys.argv[3] + '/gemep/training/')
    else:
      # Append BP4D
      if (sel & 0x01) == 0x01:
        print('Add BP4D')
        gen.append_bp4d('bp4d/training/')
      # Append SEMAINE
      if (sel & 0x02) == 0x02:
        print('Add Semaine')
        gen.append_bp4d('semaine/training/')
      # Append SEMAINE
      if (sel & 0x04) == 0x04:
        print('Add GEMEP')
        gen.append_bp4d('gemep/training/')
      # Append SEMAINE
      if (sel & 0x08) == 0x08:
        print('Add CK+')
        fpath = '../ExpressionDatasets/Cohn-Kanade/Cohn-Kanade_extended/FACS/'
        gen.append_ck_plus(fpath)
      # Save
      gen.save(sys.argv[3])
