""" Generate configuration file for facial expression detector testing """
import sys
import os
import re

__author__ = 'Christophe Ecabert'


class TestingDataGenerator:
  """ Configuration generator """

  def __init__(self, image_root_folder):
    """
    Constructor
    :param image_root_folder: Image location
    """
    # Base folder location
    self.image_root_folder = image_root_folder
    # Supported emotion
    self.emotion_label = {'ANGER': 0x01,
                          'DISGUST': 0x02,
                          'FEAR': 0x04,
                          'HAPPY': 0x08,
                          'NEUTRAL': 0x10,
                          'SAD': 0x20,
                          'SURPRISE': 0x40}
    # Emotion label convert
    self.faces_collection_converter = {'f': 'FEAR',
                                       'a': 'ANGER',
                                       'd': 'DISGUST',
                                       'h': 'HAPPY',
                                       'n': 'NEUTRAL',
                                       's': 'SAD'}
    self.kdef_converter = {'AF': 'FEAR',
                           'AN': 'ANGER',
                           'DI': 'DISGUST',
                           'HA': 'HAPPY',
                           'NE': 'NEUTRAL',
                           'SA': 'SAD',
                           'SU': 'SURPRISE'}
    self.rafd_converter = {'fearful': 'FEAR',
                           'angry': 'ANGER',
                           'disgusted': 'DISGUST',
                           'happy': 'HAPPY',
                           'neutral': 'NEUTRAL',
                           'sad': 'SAD',
                           'surprised': 'SURPRISE'}
    self.ck_plus_converter = {4: 'FEAR',
                              1: 'ANGER',
                              3: 'DISGUST',
                              5: 'HAPPY',
                              0: 'NEUTRAL',
                              6: 'SAD',
                              7: 'SURPRISE'}
    self.lts5_converter = {'AN': 'ANGER',
                           'DIS': 'DISGUST',
                           'FEAR': 'FEAR',
                           'HAP': 'HAPPY',
                           'NEU': 'NEUTRAL',
                           'SAD': 'SAD',
                           'SUP': 'SURPRISE'}
    self.lts5_expr_converter = {'anger': 'ANGER',
                                'disgust': 'DISGUST',
                                'fear': 'FEAR',
                                'happy': 'HAPPY',
                                'neutral': 'NEUTRAL',
                                'sad': 'SAD',
                                'surprise': 'SURPRISE'}
    self.oulu_converter = {'Anger': 'ANGER',
                           'Disgust': 'DISGUST',
                           'Fear': 'FEAR',
                           'Happiness': 'HAPPY',
                           'Neutral': 'NEUTRAL',
                           'Sadness': 'SAD',
                           'Surprise': 'SURPRISE'}
    # Data container <img_path,label>
    self.image_data = []

  def append_cohn_kanade(self, folder_name):
    """
    Add CK+ dataset to the training
    :param folder_name: Path to the `CK+` dataset
    """
    # Scan folder for Emotion
    emotion_label_file = self.scan_folder(folder_name + '/Emotion', '.txt')
    emotion_label_file.sort()
    for emotion in emotion_label_file:
      # Get corresponding image path
      ext_pos = emotion.find('_emotion.txt')
      image_path = emotion[0:ext_pos] + '.png'
      # Change Emotion folder to image folder
      image_path = re.sub('Emotion(?!.*Emotion)',
                          'cohn-kanade-images',
                          image_path)
      # Add neutral face
      image_path_n = re.sub('([0-9]{8})', '00000001', image_path)
      # Extract emotion label
      f = open(emotion, 'r')
      data = f.readline().strip(' \n')
      f.close()
      expr = self.ck_plus_converter.get(int(float(data)))
      label = self.emotion_label.get(expr)
      if label is not None:
        # Add to data
        self.image_data.append((image_path, label))
        self.image_data.append((image_path_n,
                                self.emotion_label.get('NEUTRAL')))

  def append_kdef(self, folder_name):
    """
    Add KDef dataset to the training
    :param folder_name: Path to the `KDef` dataset
    """
    # Get all images
    image_file = self.scan_folder(folder_name, '.JPG')
    image_file.sort()
    # Pick only the one in frontal pose
    # Iterate backward and delete element accordingly
    session_a = []
    session_b = []
    for i in range(len(image_file) - 1, -1, -1):
      part_name = image_file[i].split('/')
      part_len = len(part_name)
      image_name = part_name[part_len - 1]
      # Remove extension
      image_name_part = image_name.split('.')
      image_name = image_name_part[0]
      if image_name[len(image_name) - 1] != 'S':
        # Not frontal, remove from list
        del image_file[i]
      else:
        # Extract information session information
        session = image_name[0:1]
        if session == 'A':
          session_a.append(image_file[i])
        else:
          session_b.append(image_file[i])

    # Loop over both session
    if len(session_a) == len(session_b):
      # Ok, same length
      for image_a, image_b in zip(session_a, session_b):
        part_name = image_a.split('/')
        image_name_a = part_name[len(part_name) - 1]
        part_name = image_b.split('/')
        # image_name_b = part_name[len(part_name) - 1]
        # store data
        expr = self.kdef_converter[image_name_a[4:6]]
        emotion_label = self.emotion_label[expr]
        img_tuple = (image_a, emotion_label)
        self.image_data.append(img_tuple)
        img_tuple = (image_b, emotion_label)
        self.image_data.append(img_tuple)

  def append_rafd(self, folder_name):
    """
    Add FaFD dataset to the training
    :param folder_name: Path to the `RaFD` dataset
    """
    # Scan folder
    image_list = self.scan_folder(folder_name, '.jpg')
    image_list.sort()
    # Pick only frontal image
    for image in image_list:
      image_name = image.split('/')
      image_name = image_name[len(image_name) - 1]
      name_part = image_name.split('_')
      if name_part[0] == 'Rafd090':
        # Frontal
        emotion_name = name_part[4]
        expr = self.rafd_converter.get(emotion_name)
        emotion_label = self.emotion_label.get(expr)
        if emotion_label is not None:
          self.image_data.append((image, emotion_label))

  def append_lts5_infrared(self, folder_name):
    """
    Add lts5 dataset to the training
    :param folder_name: Path to the `LTS5-IR` dataset
    """
    # Scan folder for Emotion
    image_list = self.scan_folder(folder_name, '.png')
    image_list.sort()
    # Loop over image
    for img in image_list:
      # Extract image name
      image_name = img.split('/')
      image_name = image_name[len(image_name) - 1]
      part = image_name.split('-')
      # Extract emotion label
      emotion_label = self.emotion_label.get(self.lts5_converter.get(part[1]))
      if emotion_label is not None:
        self.image_data.append((img, emotion_label))

  def append_oulu(self, folder_name):
    """
    Add OULU dataset to the training
    :param folder_name: Path to the `OULU` dataset
    """
    # Scan folder
    image_list = self.scan_folder(folder_name, '.jpeg')
    # Loop over list of image
    img_id = -1
    old_img_id = 1e6
    seq_len = -1
    for img in image_list:
      # Extract
      part = img.split('/')
      img_id = int(part[-1].split('.')[0])

      if img_id < old_img_id:
        # Neutral
        expr = self.oulu_converter.get('Neutral')
        emotion_label = self.emotion_label.get(expr)
        if emotion_label is not None:
          self.image_data.append((img, emotion_label))
        # Find sequence length
        subpath_idx = img.rfind('/')
        subpath = img[len(self.image_root_folder):subpath_idx]
        seq_len = len(self.scan_folder(subpath, '.jpeg')) - 1
      if img_id == seq_len:
        # Expression, extract label
        expr = self.oulu_converter.get(part[-2])
        emotion_label = self.emotion_label.get(expr)
        if emotion_label is not None:
          self.image_data.append((img, emotion_label))
      # Save previous id
      old_img_id = img_id

  def append_lts5_spontaeous(self, folder_name):
    """
    Add lts5 dataset to the training
    :param folder_name: Path to the `LTS5-IR-spontaneous` dataset
    """
    # Scan folder for expression sample
    image_list = self.scan_folder(folder_name, 'jpg')
    # Sort by number ..
    image_list.sort()
    # Loop over all image
    for img in image_list:
      # Extract id/label
      part = img.strip('\n').split('/')
      # Extract emotion label
      expr = self.lts5_expr_converter.get(part[-2])
      emotion_label = self.emotion_label.get(expr)
      if emotion_label:
        self.image_data.append((img, emotion_label))

  def save(self, filename):
    """
    Dump configuration file
    :param filename:  Location where to place it
    """
    part = filename.strip('\n').split('.')
    if len(part) == 1:
      filename += '.txt'
    elif part[-1] != 'txt':
      part[-1] = '.txt'
      filename = ''.join(part)
    f = open(filename, 'w')
    # Write data
    for image_path, emotion_label in self.image_data:
      f.write(image_path + ' ' + str(emotion_label) + '\n')
    # Done close
    f.close()

  # Scan folder
  def scan_folder(self, folder, extension):
    """
    Search recursively in folder for files with a given extension

    :param folder: Folder to search in
    :param extension:  File extension of interest
    :return:  List of file with the given extension
    """
    picked_file = []
    for root, _, files in os.walk(self.image_root_folder + folder):
      # Loop over files
      for f in files:
        # Correct extension ?
        if f.endswith(extension):
          picked_file.append(root + '/' + f)
    return picked_file


def usage():
  print("executable image_root_folder #dataset output_data_filename")
  print("KDEF : 1")
  print("Rafd : 2")
  print("ck+ : 4")
  print("lts5_ir : 8")
  print("Oulu : 16")
  print("LTS5 spontaneous : 32")


if __name__ == '__main__':
  if len(sys.argv) != 4:
    # Print usage
    usage()
  else:
    # Ok
    generator = TestingDataGenerator(sys.argv[1])
    sel = int(sys.argv[2])
    # Append KDEF
    if (sel & 0x01) == 0x01:
      print('KDEF')
      generator.append_kdef('KDEF')
    # Append Rafd
    if (sel & 0x02) == 0x02:
      print('RaFD')
      generator.append_rafd('RaFD')
    # Append Cohn-Kanade extended
    if (sel & 0x04) == 0x04:
      print('ck+')
      generator.append_cohn_kanade('Cohn-Kanade/Cohn-Kanade_extended')
    # Append LTS5 infrared images
    if (sel & 0x08) == 0x08:
      print('lts5_ir')
      generator.append_lts5_infrared('lts5_ir_image')
    # Add Oulu
    if (sel & 0x10) == 0x10:
      print('Oulu')
      generator.append_oulu('Oulu/OriginalImg/NI/Strong')
    if (sel & 0x20) == 0x20:
      print('lts5_ir_spontaneous')
      generator.append_lts5_spontaeous('lts5_ir_spon')
    # Save
    generator.save(sys.argv[3])
