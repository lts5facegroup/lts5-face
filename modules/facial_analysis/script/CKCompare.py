""" Compare against CK+ dataset """
import sys
from sklearn.metrics import f1_score


__author__ = 'Christophe Ecabert'


def load_files(ref_data, sys_data):
  """
  Load data from both system at the same time
  :param ref_data:    Reference system to comapre against
  :param sys_data:    New system
  :return:    Detected result
  """
  ref_f = open(ref_data, 'r')
  sys_f = open(sys_data, 'r')

  # Skip first line
  data_gt = list()
  data_ref = list()
  data_sys = list()
  for dummy_ref, dummy_sys in zip(ref_f, sys_f):
    ref_data = dummy_ref.strip('\n').split(' ')
    sys_data = dummy_sys.strip('\n').split(' ')
    if len(ref_data) >= 3 and len(sys_data) >= 3:
      break
  if ref_data[0] != 'NaN' and sys_data[0] != 'NaN':
    data_gt.append(ref_data[0])
    data_ref.append(ref_data[1])
    data_sys.append(sys_data[1])
  for ref_line, sys_line in zip(ref_f, sys_f):
    ref_data = ref_line.strip('\n').split(' ')
    sys_data = sys_line.strip('\n').split(' ')
    if ref_data[0] != 'NaN' and sys_data[0] != 'NaN':
      data_gt.append(ref_data[0])
      data_ref.append(ref_data[1])
      data_sys.append(sys_data[1])
  return data_gt, data_ref, data_sys


def usage():
  print('ref_system_data, sys_system_data')


if __name__ == '__main__':
  if len(sys.argv) != 3:
    usage()
  else:
    # Process
    gt, ref, sys = load_files(sys.argv[1], sys.argv[2])

    # Count difference
    same_pred = 0
    for (gt_label, ref_label, sys_label) in zip(gt, ref, sys):
      if ref_label == sys_label:
        same_pred += 1

    # Ref F1
    f1_ref = f1_score(gt, ref, average='weighted')
    # Sys F1
    f1_sys = f1_score(gt, sys, average='weighted')

    # Print
    print('F1 ref = ' + str(f1_ref) + ', sys = ' + str(f1_sys))
    print('There is : {} / {} same predictions'.format(same_pred, len(gt)))
