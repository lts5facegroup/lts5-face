""" Evaluate face tracker performance based on Benchmark app output """
import sys
import os
import numpy
from PIL import Image, ImageDraw

__author__ = 'Christophe Ecabert'


def print_usage():
  """ Print executable usage """
  print('<sdm_shape> <intra_shape> <img_folder> <output_folder>')


def load_shapes_from_csv(filename):
  """
  Load detection from csv files
  :param filename:  File holding shape
  :return: Numpy array with shape inside
  """
  # Open file
  with open(filename, 'r') as f:
    # Can read
    return numpy.genfromtxt(f, delimiter=',', filling_values=-1, dtype='float')


def scan_folder(folder, extension):
  """
  Search recursively in folder for files with a given extension

  :param folder: Folder to search in
  :param extension:  File extension of interest
  :return:  List of file with the given extension
  """
  picked_file = []
  for root, _, files in os.walk(folder):
    # Loop over files
    for f in files:
      # Correct extension ?
      if f.endswith(extension):
        picked_file.append(root + '/' + f)
  return picked_file


def draw_crosses(drawing, points, size, color):
  """
  Draw a cross of a given `size` at location `points`
  :param drawing: Canvas to draw on
  :param points:  Points to draw
  :param size:  Size of the cross
  :param color: Color of the cross
  """
  for p in range(0, points.shape[0]/2):
    pts = (points[2 * p], points[2 * p + 1])
    drawing.line([pts[0] - size, pts[1], pts[0] + size, pts[1]], fill=color)
    drawing.line([pts[0], pts[1] - size, pts[0], pts[1] + size], fill=color)


def get_inner_shape(shape):
  """
  Extract inner shape
  :param shape: Complete shape
  :return:  Inner shape
  """
  n_pts = shape.shape[0]/2
  if n_pts != 49:  # pylint: disable=no-else-return
    inner_shape = numpy.zeros(98, dtype=float)
    in_p = 0
    for p in range(17, n_pts):
      if p == 60 or p == 64:
        continue
      inner_shape[in_p] = shape[2 * p]
      in_p += 1
      inner_shape[in_p] = shape[2 * p + 1]
      in_p += 1
    return inner_shape
  else:
    return shape


def draw_shape(input_img_list,
               sdm_tracker_shapes,
               intra_tracker_shapes,
               output_folder):
  """
  Draw shapes
  :param input_img_list:  List of images
  :param sdm_tracker_shapes:  SDM detection
  :param intra_tracker_shapes: Intraface detection
  :param output_folder: Location where to dump images
  """
  # Sanit check
  if output_folder[len(output_folder) - 1] != '/':
    output_folder += '/'
  assert len(input_img_list) == sdm_tracker_shapes.shape[0] and \
         len(input_img_list) == intra_tracker_shapes.shape[0]
  # Loop over all image
  img_n = 0
  for img in input_img_list:
    # Define output path
    _, filename = os.path.split(img)
    output_filename = output_folder + filename
    print('Process : ' + filename)
    # Open input image
    im = Image.open(img)
    # Define canvas
    draw = ImageDraw.Draw(im)
    # Get shape
    sdm_shape = sdm_tracker_shapes[img_n, 1:]
    intra_shape = intra_tracker_shapes[img_n, 1:]
    if sdm_shape[1] != -1.0 and intra_shape[1] != -1.0:
      # Draw cross if both shape present
      draw_crosses(draw, get_inner_shape(sdm_shape), 5, 'green')
      draw_crosses(draw, get_inner_shape(intra_shape), 5, 'red')
      # Save image
      im.save(output_filename, 'JPEG')
    # Next image
    img_n += 1


def iod(shape):
  """
  Compute Intra-orricular distance for a given `shape`
  :param shape: Shape
  :return: Iod
  """
  n_pts = shape.shape[0] / 2
  assert n_pts == 49
  dx1 = shape[50] - shape[38]
  dx2 = shape[56] - shape[44]
  dy1 = shape[51] - shape[39]
  dy2 = shape[57] - shape[45]
  return (((dx1 ** 2 + dy1 ** 2) ** 0.5) + ((dx2 ** 2 + dy2 ** 2) ** 0.5)) * 0.5


def rmse(shape_a, shape_b):
  """
  Compute the root mean squared error between two shapes
  :param shape_a: Shape A
  :param shape_b: Shape B
  :return: RMSE
  """
  assert shape_a.shape[0] == shape_b.shape[0]
  n_pts = shape_a.shape[0] / 2
  err = 0.0
  for p in range(0, n_pts):
    x1 = shape_a[2 * p]
    y1 = shape_a[2 * p + 1]
    x2 = shape_b[2 * p]
    y2 = shape_b[2 * p + 1]
    err += ((x1 - x2) ** 2 + (y1 - y2) ** 2)
  err /= n_pts
  d = (iod(shape_a) + iod(shape_b)) / 2
  err /= d ** 2
  return err ** 0.5


def compute_average_distance(shapes_a, shapes_b):
  """
  Average distance between two shapes
  :param shapes_a:  Shape A
  :param shapes_b:  Shape B
  :return: Average distance
  """
  assert shapes_a.shape[0] == shapes_b.shape[0]
  n_shape = shapes_a.shape[0]
  # Loop over all shape
  n = 0
  dist = 0.0
  for s in range(0, n_shape):
    shape_a = shapes_a[s, 1:]
    shape_b = shapes_b[s, 1:]
    if shape_a[0] != -1.0 and shape_b[0] != -1.0:
      shp_a = get_inner_shape(shape_a)
      shp_b = get_inner_shape(shape_b)
      dist += rmse(shp_a, shp_b)
      n += 1
  dist /= n
  return dist


if __name__ == '__main__':
  if len(sys.argv) != 5:
    print_usage()
  else:
    # Check if output location exist
    if not os.path.exists(sys.argv[4]):
      os.mkdir(sys.argv[4])
    # Load data
    sdm_shapes = load_shapes_from_csv(sys.argv[1])
    intra_shapes = load_shapes_from_csv(sys.argv[2])
    # Gather input images
    img_list = scan_folder(sys.argv[3], '.jpg')
    img_list.sort()
    # Draw shapes
    #draw_shape(img_list, sdm_shapes, intra_shapes, sys.argv[4])
    # Compute normlized distance between shapes
    dist_shape = compute_average_distance(sdm_shapes, intra_shapes)
    print('Average distance ' + str(dist_shape))
