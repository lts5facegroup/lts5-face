""" Generate testing file for action unit detector """
import sys
import os
import glob
import shutil
import csv
import numpy
import cv2
__author__ = 'Christophe Ecabert'


class AUTestGenerator:
  """ Action Unit test file generator """
  def __init__(self, data_folder):
    # Root folder to look from
    self.root_folder = data_folder
    if self.root_folder[-1] != '/':
      self.root_folder += '/'
    # Supported AU
    self.au_label = {1: ('AU_01', 0x0001),
                     2: ('AU_02', 0x0002),
                     4: ('AU_04', 0x0004),
                     6: ('AU_06', 0x0008),
                     7: ('AU_07', 0x0010),
                     10: ('AU_10', 0x0020),
                     12: ('AU_12', 0x0040),
                     14: ('AU_14', 0x0080),
                     15: ('AU_15', 0x0100),
                     17: ('AU_17', 0x0200),
                     23: ('AU_23', 0x0400),
                     25: ('AU_25', 0x0800),
                     28: ('AU_28', 0x1000),
                     45: ('AU_45', 0x2000)}
    # Testing data
    self.testing_data = []

  def scan_folder(self, folder_name, file_ext):
    """
    Search recursively in folder for files with a given extension

    :param folder_name: Folder to search in
    :param file_ext:  File extension of interest
    :return:  List of file with the given extension
    """
    pick_file = []
    for root, _, files in os.walk(self.root_folder + folder_name):
      # Loop over files
      for f in files:
        # Correct extension ?
        if f.endswith(file_ext):
          pick_file.append(root + '/' + f)
    return pick_file

  def export_bp4d_data(self, folder_name, label_folder_name, output_name):
    """
    Reformat BP4D dataset into standard data structor
    :param folder_name: Location of the BP4D Dataset
    :param label_folder_name: Location of the label folder
    :param output_name: Output name
    """
    # Scan for label
    label_file = self.scan_folder(label_folder_name, '.csv')
    label_file.sort()
    # Loop over all label files
    for f in label_file:
      # Parse csv
      f_csv = open(f, 'r')
      header = list(map(int, f_csv.readline().strip('\n').split(',')))
      # Check if all labels are present
      all_label = True
      for key, _ in self.au_label.items():
        try:
          header.index(key)
        except ValueError:
          # Value missing
          all_label = False
          break
      if all_label:
        # Got all what we need can start to generate stuff
        print('Process file : ' + f)
        # Read data label
        lbl_data = numpy.genfromtxt(f_csv, dtype=int, delimiter=',')
        # define output
        part = f.strip('.csv').split('/')[-1].split('_')
        subject_id = part[0]
        session_id = part[1]
        out_label_folder = output_name + subject_id + '/' + session_id + '/'
        if not os.path.exists(out_label_folder):
          os.makedirs(out_label_folder)
        # Create label output + write header
        lbl_writer = open(out_label_folder + '_label.csv', 'w')
        lbl_writer.write('image_name')
        for key, val in self.au_label.items():
          lbl_writer.write(',' + val[0])
        lbl_writer.write('\n')
        lbl_writer.flush()
        # Detect number of leading zero
        img_folder = self.root_folder + folder_name + subject_id
        img_folder += '/' + session_id + '/'
        parts = glob.glob(img_folder + '*.jpg')[0].strip('.jpg').split('/')
        n_zero_leading = len(parts[-1])
        # Loop over all labels
        for row in lbl_data:
          img_n = row[0]
          img_path = img_folder + str(img_n).zfill(n_zero_leading) + '.jpg'
          img_path_out = out_label_folder + str(img_n).zfill(4) + '.jpg'
          # copy image
          shutil.copy(img_path, img_path_out)
          # Write label
          lbl_writer.write(str(img_n).zfill(4) + '.jpg')
          for key, val in self.au_label.items():
            lbl_writer.write(',' + str(row[key]))
          lbl_writer.write('\n')
          lbl_writer.flush()
        # Done close label file
        lbl_writer.close()
      else:
        print('Missing label for file : ' + f)

  def export_semaine_data(self, folder_name, output_name):
    """
    Reformat SEMAINE dataset into standard data structor
    :param folder_name: Location of the SEMAINE Dataset
    :param label_folder_name: Location of the label folder
    :param output_name: Output name
    """
    # Scan folder for video, labels, and range
    vids = self.scan_folder(folder_name, '.avi')
    labels = self.scan_folder(folder_name, '_full.csv')
    if len(vids) == len(labels):
      # Same number of videos and labels files
      # Loop
      for vid, lbl in zip(vids, labels):
        # Get file range
        part = vid.split('/')
        id_session = part[-2]
        rfile = '/'.join(part[0:-1]) + '/' + id_session + '.txt'
        # Get range
        frange = open(rfile, 'r')
        idx = list(map(int, frange.readline().strip('\n').split(' ')))
        start = idx[0]
        stop = idx[1]
        frange.close()
        # open video + label
        cap = cv2.VideoCapture(vid)
        f = open(lbl, 'r')
        if cap.isOpened() and not f.closed:
          # everything open, read csv header
          print('Process data : ' + vid)
          csv_reader = csv.reader(f, delimiter=',')
          header = next(csv_reader)
          # Check if all label are present
          all_label = True
          for v in header[1:]:
            t = self.au_label.get(int(v))
            if not t:
              all_label = False
              break
          if all_label:
            # Got all label, can process data, Output exist ?
            out_folder = output_name + id_session + '/'
            if not os.path.exists(out_folder):
              os.makedirs(out_folder)
            # Create label file
            with open(out_folder + '_label.csv', 'w') as lbl_writer:
              # Write header
              lbl_writer.write('image_name')
              for v in header[1:]:
                val = self.au_label.get(int(v))
                if val:
                  lbl_writer.write(',' + val[0])
              lbl_writer.write('\n')
              for i, row in zip(range(0, stop + 1), csv_reader):
                cap.grab()
                if i >= start:
                  # read image
                  _, img = cap.retrieve()
                  # Write image
                  img_name = out_folder + 'frame_%d.jpg' % i
                  cv2.imwrite(img_name, img)
                  # write label
                  lbl_writer.write('frame_%d.jpg,' % i)
                  lbl_writer.write(','.join(row[1:]))
                  lbl_writer.write('\n')
                  lbl_writer.flush()
              # Done
              lbl_writer.close()
          else:
            print('Missing label for video : ' + vid)
        else:
          print('Unable to open : ' + vid + ' / ' + lbl)
      # Done
      print('SEMAINE Exported ...')
    else:
      print('ERROR: Number of videos does not math number of label files')
    # Loop over video
    return

  def append_bp4d(self, folder_name):
    """
    Add BP4D to the test

    :param folder_name: Where the dataset is stored
    """
    # Scan the whole folder for label files (*.csv)
    label_files = self.scan_folder(folder_name, '.csv')
    # Sort
    label_files.sort()
    # Loop over all file
    for label_file in label_files:
      with open(label_file, 'r') as f_label:
        # Map
        label_map = {}
        # Define folder
        current_folder = label_file[0:label_file.rfind('/') + 1]
        # Check header
        label_ok = True
        header = f_label.readline()
        header_part = header.strip('\n').split(',')
        for _, value in self.au_label.items():
          if not value[0] in header:
            label_ok = False
            print('Missing labels, abort !')
            break
          else:
            # Build correspondance between au label and column indexes
            idx = header_part.index(value[0])
            label_map[value[0]] = idx
        if label_ok:
          # Loop over label
          for row in f_label:
            row_part = row.strip('\n').split(',')
            label = 0
            for _, value in self.au_label.items():
              # get part index
              part_idx = label_map[value[0]]
              if int(row_part[part_idx]) != 0:
                label += value[1]
            # Create entry
            self.testing_data.append((current_folder + row_part[0], label))

  def save(self, filename):
    """
    Save testing file
    :param filename:  Location where to save
    """
    part = filename.strip('\n').split('.')
    if len(part) == 1:
      filename += '.txt'
    elif part[-1] != 'txt':
      part[-1] = '.txt'
      filename = ''.join(part)
    # Open file
    with open(filename, 'w') as f:
      # Iterate over training data
      for (path, label) in self.testing_data:
        f.write(path + ' ' + str(label) + '\n')


# How to call script
def use():
  print(sys.argv[0] + ' <data_root_folder> <#dataset> <output_file>')
  print('#dataset values :')
  print('<0 Reformat data into proper data structure')
  print('BP4D : 1')
  print('SEMAINE : 2')


if __name__ == '__main__':
  if len(sys.argv) != 4:
    use()
  else:
    # Create generator
    gen = AUTestGenerator(sys.argv[1])
    # Selection
    sel = int(sys.argv[2])
    # Export data struct
    if sel < 0:
      # Reformat SEMAINE testing
      print('Reformat SEMAINE testing samples/labels')
      gen.export_semaine_data('SEMAINE-Sessions/Development/',
                              sys.argv[3] + '/semaine/testing/')
      print('Reformat BP4D testing samples/labels')
      gen.export_bp4d_data('BP4D-training/Development/',
                           'BP4D-AUCoding/AUCoding/AUCoding/Development',
                           sys.argv[3] + '/bp4d/testing/')
    else:
      # Append BP4D
      if (sel & 0x01) == 0x01:
        print('Add BP4D')
        gen.append_bp4d('bp4d/testing/')
      # Append SEMAINE
      if (sel & 0x02) == 0x02:
        print('Add Semaine')
        gen.append_bp4d('semaine/testing/')
      # Save
      gen.save(sys.argv[3])
