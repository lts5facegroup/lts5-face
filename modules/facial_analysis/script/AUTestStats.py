""" Generate facial analysis results statistics """
import argparse
import subprocess
import re
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score

__author__ = 'Christophe Ecabert'


class LatexGenerator:
  """ Latex table generator """
  def __init__(self, filename=None):
    """
    Constructor
    :param filename: Path to the latex file to generate
    """
    self.n_col = 0
    self.latex_str = []
    self.f_writer = None
    if filename:
      ext_idx = filename.rfind('.')
      if ext_idx != -1:
        if filename[ext_idx:] != '.tex':
          filename = filename[0:ext_idx] + '.tex'
      else:
        filename += '.tex'
      self.f_writer = open(filename, 'w')

  def begin_document(self, document_class):
    """
    Begin latex documents
    :param document_class:  Type of latex document
    """
    latex_str = r'\documentclass{%s}\n' % document_class
    self.latex_str.append(latex_str)
    latex_str = r'\begin{document}\n'
    self.latex_str.append(latex_str)

  def end_document(self):
    """ End of latex document """
    latex_str = r'\end{document}\n'
    self.latex_str.append(latex_str)

  def add_header(self, headers, title):
    """
    Add table headers
    :param headers: List of headers
    :param title:   Table title
    """
    self.n_col = len(headers)
    latex_str = r'\begin{tabular}{' + '|c' * self.n_col + '|}\n'
    self.latex_str.append(latex_str)
    latex_str = r'\multicolumn{%d}{|c|}{%s} \\ \hline\n' % (self.n_col, title)
    self.latex_str.append(latex_str)
    latex_str = ' & '.join(headers) + r'\ \hline\n'
    self.latex_str.append(latex_str)

  def add_row(self, datas):
    """
    Add new row in the table
    :param datas:   Data to add
    """
    if len(datas) == self.n_col:
      latex_str = ' & '.join(datas) + r'\\\n'
      self.latex_str.append(latex_str)
    else:
      print('ERROR, Data length does not match !')

  def end_table(self):
    """ Finalize the table """
    self.latex_str.append(r'\end{tabular}\n\n')

  def write(self):
    """ Dump latex string into file """
    if self.f_writer:
      for row in self.latex_str:
        self.f_writer.write(row)
      self.f_writer.close()
    return ''.join(self.latex_str)

  def compile(self, filename):
    """
    Generate pdf file from latex string
    :param filename: Pdf filename
    """
    # define path
    idx = filename.rfind('/')
    path = ''
    if idx != -1:
      path = filename[0:idx+1]
    cmd = ['pdflatex', '-output-directory=' + path, filename]
    subprocess.call(cmd)

def parse_result_file(result_file, labels):
  """
  Parse test outputs result from file
  :param result_file: File holding test results
  :param labels: List of labels
  :return: ground_truth, predictions
  """
  # Open file
  f_data = open(result_file, 'r')
  n_au = int(f_data.readline())
  # Query label's names
  for _ in range(0, n_au):
    line = f_data.readline()
    line = line.strip('\n').split(' ')
    labels.append(re.sub('[_]', '', line[1]))
  # Create output data struct
  ground_truth = np.empty((0, n_au), dtype=int)
  prediction = np.empty((0, n_au), dtype=int)
  # Query test samples
  for f in f_data:
    parts = f.strip('\n').split(' ')
    if parts[0] != 'NaN' and parts[1] != 'NaN':
      gt = np.zeros((1, n_au), dtype=int)
      pred_line = np.zeros((1, n_au), dtype=int)
      for n in range(0, n_au):
        mask = (0x0001 << n)
        gt[0, n] = 1 if (int(parts[0]) & mask) == mask else 0
        pred_line[0, n] = 1 if (int(parts[1]) & mask) == mask else 0
      ground_truth = np.append(ground_truth, gt, axis=0)
      prediction = np.append(prediction, pred_line, axis=0)
    else:
      print('Drop image : ' + parts[2])
  return [ground_truth, prediction]


def plot_confusion_matrix(cm,
                          labels,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
  """
  Draw confusion matrix. Check sklearn documentation for more details
  :param cm:  Confusion matrix
  :param labels:  Labels
  :param title: Plot's title
  :param cmap:  Colormap to use
  """
  plt.imshow(cm, interpolation='nearest', cmap=cmap)
  plt.title(title)
  plt.colorbar()
  tick_marks = np.arange(len(labels))
  plt.xticks(tick_marks, labels, rotation=45)
  plt.yticks(tick_marks, labels)
  plt.ylabel('True label')
  plt.xlabel('Predicted label')

if __name__ == '__main__':
  # Define cmdline parser
  parser = argparse.ArgumentParser()
  parser.add_argument('-f',
                      '--files',
                      help='List of files to process in a form : "A.txt;B.txt"')
  parser.add_argument('-d',
                      '--dataset',
                      help='List of corresponding dataset\'s name : "A;B"')
  parser.add_argument('-o',
                      '--output',
                      help='Output filename')

  args = parser.parse_args()

  # Define list of file to process + corresponding name
  list_file = args.files.strip('\n').split(';')
  list_name = args.dataset.strip('\n').split(';')
  if len(list_file) == len(list_name):
    # Create Latex generator
    # Create Latex generator
    gen = LatexGenerator(args.output)
    gen.begin_document('article')

    # Loop over result file
    for file, name in zip(list_file, list_name):
      # Define new header
      gen.add_header(['AU', 'Precision', 'Recall', 'F1', 'Accuracy', 'Support'],
                     name)
      # Load data
      label = []
      g_truth, pred = parse_result_file(file, label)
      # Compute metrics for each label
      f1_scores = []
      prec_scores = []
      recall_scores = []
      acc_scores = []
      for k, lbl in enumerate(label):
        prec = precision_score(g_truth[:, k], pred[:, k], average='binary')
        recall = recall_score(g_truth[:, k], pred[:, k], average='binary')
        f1 = f1_score(g_truth[:, k], pred[:, k], average='binary')
        support = (g_truth[:, k] > 0.0).sum()
        acc = accuracy_score(g_truth[:, k], pred[:, k])
        data = [lbl,
                '{0:.3f}'.format(prec),
                '{0:.3f}'.format(recall),
                '{0:.3f}'.format(f1),
                '{0:.3f}'.format(acc),
                str(support)]
        gen.add_row(data)
        f1_scores.append(f1)
        prec_scores.append(prec)
        recall_scores.append(recall)
        acc_scores.append(acc)
      # Compute average
      avg_f1 = sum(f1_scores) / len(f1_scores)
      avg_prec = sum(prec_scores) / len(prec_scores)
      avg_recall = sum(recall_scores) / len(recall_scores)
      avg_acc = sum(acc_scores) / len(acc_scores)
      data = ['Average',
              '{0:.3f}'.format(avg_prec),
              '{0:.3f}'.format(avg_recall),
              '{0:.3f}'.format(avg_f1),
              '{0:.3f}'.format(avg_acc),
              '-']
      gen.add_row(data)
      # End table
      gen.end_table()

    # End of file
    gen.end_document()
    gen.write()
    gen.compile(args.output)

  else:
    print('ERROR: Number of files/dataset name does not match !')
