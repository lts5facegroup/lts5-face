""" Generate configuration file for facial expression detector training """
import sys
import os
import re
import shutil
import random
import math

__author__ = 'Christophe Ecabert'


class CandidateDataGenerator:
  """ Configuration generator """

  def __init__(self,
               image_root_folder,
               export_image=False,
               export_folder=None,
               postfix_name=None):
    """
    Constructor
    :param image_root_folder: Image location
    :param export_image:  Where to export image
    :param export_folder: Where to export configuration
    :param postfix_name:  Postfix name
    """
    # Base location of images
    self.image_root_folder = image_root_folder
    # Supported emotion
    self.emotion_label = {'ANGER': 0x01,
                          'DISGUST': 0x02,
                          'FEAR': 0x04,
                          'HAPPY': 0x08,
                          'NEUTRAL': 0x10,
                          'SAD': 0x20,
                          'SURPRISE': 0x40}
    # Emotion label convert
    self.faces_collecion_converter = {'f': 'FEAR',
                                      'a': 'ANGER',
                                      'd': 'DISGUST',
                                      'h': 'HAPPY',
                                      'n': 'NEUTRAL',
                                      's': 'SAD'}
    self.kdef_converter = {'AF': 'FEAR',
                           'AN': 'ANGER',
                           'DI': 'DISGUST',
                           'HA': 'HAPPY',
                           'NE': 'NEUTRAL',
                           'SA': 'SAD',
                           'SU': 'SURPRISE'}
    self.rafd_converter = {'fearful': 'FEAR',
                           'angry': 'ANGER',
                           'disgusted': 'DISGUST',
                           'happy': 'HAPPY',
                           'neutral': 'NEUTRAL',
                           'sad': 'SAD',
                           'surprised': 'SURPRISE'}
    self.ck_plus_converter = {4: 'FEAR',
                              1: 'ANGER',
                              3: 'DISGUST',
                              5: 'HAPPY',
                              0: 'NEUTRAL',
                              6: 'SAD',
                              7: 'SURPRISE'}
    self.lts5_converter = {'AN': 'ANGER',
                           'DIS': 'DISGUST',
                           'FEAR': 'FEAR',
                           'HAP': 'HAPPY',
                           'NEU': 'NEUTRAL',
                           'SAD': 'SAD',
                           'SUP': 'SURPRISE'}

    self.lts5_expr_converter = {'anger': 'ANGER',
                                'disgust': 'DISGUST',
                                'fear': 'FEAR',
                                'happy': 'HAPPY',
                                'neutral': 'NEUTRAL',
                                'sad': 'SAD',
                                'surprise': 'SURPRISE'}

    # Candidate id counter
    self.candidate_id = -1
    # Data
    self.image_data = []
    self.export_image_data = []
    # If selected image need to be exported
    self.export_image = export_image
    # Export location
    self.export_location = export_folder
    # Postfix
    self.postfix_name = postfix_name

  def append_face_collection(self, folder_name):
    """
    Add Face-collection dataset to the training
    :param folder_name: Path to the `face-collection` dataset
    """
    # Scan folder
    image_list = self.scan_folder(folder_name, '.jpg')
    image_list.sort()
    # Pick only frontal image
    prev_id = -1
    curr_id = -1
    for image in image_list:
      image_name = image.split('/')
      image_name = image_name[len(image_name) - 1]
      name_part = image_name.split('_')
      # Extract ID
      curr_id = int(name_part[0])
      if curr_id != prev_id:
        self.candidate_id += 1
      # Extract emotion
      emotion_name = name_part[3]
      expr = self.faces_collecion_converter.get(emotion_name)
      emotion_label = self.emotion_label.get(expr)
      if emotion_label is not None:
        self.image_data.append((self.candidate_id, image, emotion_label))
        prev_id = curr_id
        # Export image if need
        if self.export_image:
          image_path = self.copy_image(folder_name, image)
          self.export_image_data.append((self.candidate_id,
                                         image_path,
                                         emotion_label))

  def append_kdef(self, folder_name):
    """
    Add KDef dataset to the training
    :param folder_name: Path to the `KDef` dataset
    """
    # Get all images
    image_file = self.scan_folder(folder_name, '.JPG')
    image_file.sort()
    # Pick only the one in frontal pose
    # Iterate backward and delete element accordingly
    a_session = []
    b_session = []
    for i in range(len(image_file) - 1, -1, -1):
      part_name = image_file[i].split('/')
      part_len = len(part_name)
      image_name = part_name[part_len - 1]
      # Remove extension
      image_name_part = image_name.split('.')
      image_name = image_name_part[0]
      if image_name[len(image_name) - 1] != 'S':
        # Not frontal, remove from list
        del image_file[i]
      else:
        # Extract information session information
        session = image_name[0:1]
        if session == 'A':
          a_session.append(image_file[i])
        else:
          b_session.append(image_file[i])

    # Loop over both session
    if len(a_session) == len(b_session):
      # Ok, same length
      prev_id = -1
      curr_id = -1
      for image_a, image_b in zip(a_session, b_session):
        part_name = image_a.split('/')
        image_name_a = part_name[len(part_name) - 1]
        part_name = image_b.split('/')
        image_name_b = part_name[len(part_name) - 1]
        # Get id + expr
        id_a = int(image_name_a[2:4])
        id_b = int(image_name_b[2:4])
        if id_a != id_b:
          # Error
          print('Identities are not identical ...')
          break
        else:
          # Ok
          curr_id = id_a
          if curr_id != prev_id:
            self.candidate_id += 1
          # store data
          expr = self.kdef_converter[image_name_a[4:6]]
          emotion_label = self.emotion_label[expr]
          t = (self.candidate_id, image_a, emotion_label)
          self.image_data.append(t)
          t = (self.candidate_id, image_b, emotion_label)
          self.image_data.append(t)
          # Save
          prev_id = curr_id
          # Export image if need
          if self.export_image:
            image_path = self.copy_image(folder_name, image_a)
            self.export_image_data.append((self.candidate_id,
                                           image_path,
                                           emotion_label))
            image_path = self.copy_image(folder_name, image_b)
            self.export_image_data.append((self.candidate_id,
                                           image_path,
                                           emotion_label))

  def append_rafd(self, folder_name):
    """
    Add FaFD dataset to the training
    :param folder_name: Path to the `RaFD` dataset
    """
    # Scan folder
    image_list = self.scan_folder(folder_name, '.jpg')
    image_list.sort()
    # Pick only frontal image
    prev_id = -1
    curr_id = -1
    for image in image_list:
      image_name = image.split('/')
      image_name = image_name[len(image_name) - 1]
      name_part = image_name.split('_')
      if name_part[0] == 'Rafd090' and name_part[5] == 'frontal.jpg':
        # Extract ID
        curr_id = int(name_part[1])
        if curr_id != prev_id:
          self.candidate_id += 1
        # Extract emotion
        emotion_name = name_part[4]
        expr = self.rafd_converter.get(emotion_name)
        emotion_label = self.emotion_label.get(expr)
        if emotion_label is not None:
          self.image_data.append((self.candidate_id, image, emotion_label))
          prev_id = curr_id
          # Export image if need
          if self.export_image:
            image_path = self.copy_image(folder_name, image)
            self.export_image_data.append((self.candidate_id,
                                           image_path,
                                           emotion_label))

  def append_cohn_kanade(self, folder_name):
    """
    Add CK+ dataset to the training
    :param folder_name: Path to the `CK+` dataset
    """
    # Scan folder for Emotion
    emotion_label_file = self.scan_folder(folder_name + '/Emotion', '.txt')
    emotion_label_file.sort()
    # Pick corresponding images
    prev_id = -1
    curr_id = -1
    for emotion in emotion_label_file:
      # Get corresponding image path
      ext_pos = emotion.find('_emotion.txt')
      image_path = emotion[0:ext_pos] + '.png'
      # Change Emotion folder to image folder
      image_path = re.sub('(Emotion)', 'cohn-kanade-images', image_path)
      # Extract ID
      subject = re.search('(S[0-9]{3})', emotion, re.M).group()
      curr_id = int(subject[1:])
      if curr_id != prev_id:
        self.candidate_id += 1
      # Extract emotion label
      f = open(emotion, 'r')
      data = f.readline().strip(' \n')
      f.close()
      expr = self.ck_plus_converter.get(int(float(data)))
      label = self.emotion_label.get(expr)
      if label is not None:
        # Add to data
        self.image_data.append((self.candidate_id, image_path, label))
        prev_id = curr_id
        # Export image if need
        if self.export_image:
          rel_path = image_path[len(self.image_root_folder):]
          pos = rel_path.find('/')
          # Copy image with expression
          export_path = self.copy_image(rel_path[0:pos], image_path)
          self.export_image_data.append((self.candidate_id, export_path, label))
          # Copy neutral as well
          image_path_n = re.sub('([0-9]{8})', '00000001', image_path)
          self.copy_image(rel_path[0:pos], image_path_n)

  def append_lts5_infrared(self, folder_name):
    """
    Add lts5 dataset to the training
    :param folder_name: Path to the `LTS5-IR` dataset
    """
    # Scan folder for Emotion
    image_list = self.scan_folder(folder_name, '.png')
    image_list.sort()
    # Pick corresponding images
    prev_id = -1
    curr_id = -1
    # Loop over image
    for img in image_list:
      # Extract image name
      image_name = img.split('/')
      image_name = image_name[len(image_name) - 1]
      part = image_name.split('-')
      # Get id
      curr_id = int(part[0][1:])
      if curr_id != prev_id:
        self.candidate_id += 1
      # Extract emotion label
      emotion_label = self.emotion_label.get(self.lts5_converter.get(part[1]))
      if emotion_label is not None:
        self.image_data.append((self.candidate_id, img, emotion_label))
        prev_id = curr_id
        # Export
        if self.export_image:
          image_path = self.copy_image(folder_name, img)
          self.export_image_data.append((self.candidate_id,
                                         image_path,
                                         emotion_label))

  def append_lts5_expression_recording(self, folder_name):
    """
    Add lts5 dataset to the training
    :param folder_name: Path to the `LTS5-IR-recordings` dataset
    """
    # Scan folder for expression sample
    image_list = self.scan_folder(folder_name, '.bmp')
    # Sort by number .. Fmt : XXXid####
    image_list.sort(key=extract_id)
    # Pick corresponding images
    prev_id = -1
    curr_id = -1
    # Loop over all image
    for img in image_list:
      # Extract image name
      part = img.strip('\n').split('/')
      label_number = re.match(r"([a-z]+)([0-9]+)", part[-1], re.I).groups()
      # Get ID
      curr_id = int(label_number[1][0:2])
      if curr_id != prev_id:
        self.candidate_id += 1
      # Extract emotion label
      expr = self.lts5_expr_converter.get(label_number[0])
      emotion_label = self.emotion_label.get(expr)
      if emotion_label:
        self.image_data.append((self.candidate_id, img, emotion_label))
        prev_id = curr_id
        # Export
        if self.export_image_data:
          image_path = self.copy_image(folder_name, img)
          self.export_image_data.append((self.candidate_id,
                                         image_path,
                                         emotion_label))

  def append_lts5_car_recording(self, folder_name):
    """
    Add lts5 dataset to the training
    :param folder_name: Path to the `LTS5-IR-car` dataset
    """
    # Scan folder for expression sample
    image_list = self.scan_folder(folder_name, '.jpg')
    # Sort by number .. Fmt : XXXid####
    image_list.sort()
    # Pick corresponding images
    prev_id = ""
    curr_id = ""
    # Loop over all image
    for img in image_list:
      # Extract image name
      part = img.strip('\n').split('/')
      # Get ID
      curr_id = part[-3]
      if curr_id != prev_id:
        self.candidate_id += 1
      # Extract emotion label
      expr = self.lts5_expr_converter.get(part[-2])
      emotion_label = self.emotion_label.get(expr)
      if emotion_label:
        self.image_data.append((self.candidate_id, img, emotion_label))
        prev_id = curr_id
        # Export
        if self.export_image_data:
          image_path = self.copy_image(folder_name, img)
          self.export_image_data.append((self.candidate_id,
                                         image_path,
                                         emotion_label))

  def append_lts5_spontaeous(self, folder_name):
    """
    Add lts5 dataset to the training
    :param folder_name: Path to the `LTS5-IR-spontaneous` dataset
    """
    # Scan folder for expression sample
    image_list = self.scan_folder(folder_name, 'jpg')
    # Sort by number .. Fmt : XXXid####
    image_list.sort()
    # Pick corresponding images
    prev_id = -1
    curr_id = -1
    # Loop over all image
    for img in image_list:
      # Extract id/label
      part = img.strip('\n').split('/')
      # Get id
      curr_id = int(part[-3][3:])
      if curr_id != prev_id:
        self.candidate_id += 1
      # Extract emotion label
      expr = self.lts5_expr_converter.get(part[-2])
      emotion_label = self.emotion_label.get(expr)
      if emotion_label:
        self.image_data.append((self.candidate_id, img, emotion_label))
        prev_id = curr_id
        # Export
        if self.export_image_data:
          image_path = self.copy_image(folder_name, img)
          self.export_image_data.append((self.candidate_id,
                                         image_path,
                                         emotion_label))

  def save(self, filename):
    """
    Dump configuration file
    :param filename:  Location where to place it
    """
    part = filename.strip('\n').split('.')
    if len(part) == 1:
      filename += '.txt'
    elif part[-1] != 'txt':
      part[-1] = '.txt'
      filename = ''.join(part)
    # Open file
    f = open(filename, 'w')
    # Write
    for (candidate_id, path, label) in self.image_data:
      f.write(str(candidate_id) + ' ' + path + ' ' + str(label) + '\n')
    # Close
    f.close()
    # Export data
    if self.export_image_data:
      f = open('export_' + filename, 'w')
      # Write
      for (candidate_id, path, label) in self.export_image_data:
        f.write(str(candidate_id) + ' ' + path + ' ' + str(label) + '\n')
      # Close
      f.close()

  def scan_folder(self, folder, extension):
    """
    Search recursively in folder for files with a given extension

    :param folder: Folder to search in
    :param extension:  File extension of interest
    :return:  List of file with the given extension
    """
    picked_file = []
    for root, _, files in os.walk(self.image_root_folder + folder):
      # Loop over files
      for f in files:
        # Correct extension ?
        if f.endswith(extension):
          picked_file.append(root + '/' + f)
    return picked_file

  def copy_image(self, input_subfolder, image_location):
    """
    Copy image from one location to another one
    :param input_subfolder: Input
    :param image_location:  Output
    :return:
    """
    # Find image name
    pos = image_location.index(input_subfolder)
    image_name = self.export_location + image_location[pos:len(image_location)]
    # Copy to export location
    image_dir = image_name[0:image_name.rfind('/')]
    if not os.path.exists(image_dir):
      os.makedirs(image_dir)
    shutil.copy2(image_location, image_dir)
    # Add post fix
    if not self.postfix_name:
      pos = image_name.rfind('.')
      ext = image_name[pos:len(image_name)]
      image_name = image_name[0:pos]
      image_name = image_name + self.postfix_name + ext
    return image_name

  # Generate K random fold for all the ID detected previously
  def generate_fold(self, n_fold, filename):
    """
    Generate fold for cross-validation training
    :param n_fold:  Number of fold to generate
    :param filename:  Filename
    """
    # Create random vector of IDs
    id_shuffle = range(0, self.candidate_id)
    random.shuffle(id_shuffle)
    fold_size = int(math.floor(len(id_shuffle) / int(n_fold)))
    if fold_size <= 0:
      print('not enough samples to generate ' + str(n_fold) + ' folds !')
      return -1
    # Create fold
    train_folds = []
    test_folds = []
    for k in range(0, n_fold):
      test_id = id_shuffle[k*fold_size:(k+1)*fold_size]
      test_folds.append(test_id)
      train_id = id_shuffle[0:k*fold_size]
      train_id += id_shuffle[(k + 1)*fold_size:]
      train_folds.append(train_id)
    for k in range(0, n_fold):
      # create output file
      part = filename.strip('\n').split('.')
      if len(part) == 1:
        # not extension
        train_file = filename + '_train_fold' + str(k) + '.txt'
        test_file = filename + '_test_fold' + str(k) + '.txt'
      elif part[-1] != 'txt':
        # extension but not correct, -> change name
        part[-1] = '.txt'
        name = part[-2]
        part[-2] += '_train_fold' + str(k)
        train_file = ''.join(part)
        part[-2] = name + '_test_fold' + str(k)
        test_file = ''.join(part)
      # Open file
      f_train = open(train_file, 'w')
      f_test = open(test_file, 'w')
      # Fill
      train_data = [item for item in self.image_data if item[0] in \
                    train_folds[k]]
      test_data = [item for item in self.image_data if item[0] in test_folds[k]]
      # Dump into file
      for (candidate_id, path, label) in train_data:
        f_train.write(str(candidate_id) + ' ' + path + ' ' + str(label) + '\n')
      for (candidate_id, path, label) in test_data:
        f_test.write(str(candidate_id) + ' ' + path + ' ' + str(label) + '\n')
      # Close
      f_train.close()
      f_test.close()
    return 0

def extract_id(name):
  """
  Extract image ID
  :param name:  Image name
  :return:  ID
  """
  part = name.strip('\n').split('/')
  label_number = re.match(r"([a-z]+)([0-9]+)", part[-1], re.I).groups()
  return int(label_number[1][0:2])


def usage():
  print("executable image_root_folder #Dataset output_data_filename (export_image_folder)")
  print('KDEF 1')
  print('RaFD 2')
  print('Faces-Collection 4')
  print('CK+ 8')
  print('lts5ir_im 16')
  print('lts5ir_exp 32')
  print('lts5ir_car 64')
  print('lts5ir_spon 128')


# Main
if __name__ == "__main__":
  if len(sys.argv) < 4:
    # Print usage
    usage()
  else:
    # Ok
    if len(sys.argv) == 4:
      generator = CandidateDataGenerator(sys.argv[1])
    else:
      generator = CandidateDataGenerator(sys.argv[1],
                                         True,
                                         sys.argv[4],
                                         '_synth')
    # Define dataset selection
    sel = int(sys.argv[2])
    # Append dataset
    if (sel & 0x01) == 0x01:
      generator.append_kdef('KDEF')
    # Append RaFD dataset
    if (sel & 0x02) == 0x02:
      generator.append_rafd('RaFD')
    # Append faces-collection dataset
    if (sel & 0x04) == 0x04:
      generator.append_face_collection('Faces-collection/small_resolution')
    # Append Cohn-Kanade extended
    if (sel & 0x08) == 0x08:
      generator.append_cohn_kanade('Cohn-Kanade/Cohn-Kanade_extended')
    # Append LTS5 infrared images
    if (sel & 0x10) == 0x10:
      generator.append_lts5_infrared('lts5_ir_image')
      #generator.generate_fold(5,sys.argv[3])
    if (sel & 0x20) == 0x20:
      generator.append_lts5_expression_recording('lts5_ir_exp')
    if (sel & 0x40) == 0x40:
      generator.append_lts5_car_recording('lts5_ir_record_car')
    if (sel & 0x80) == 0x80:
      generator.append_lts5_spontaeous('lts5_ir_spon')
    # Save into
    generator.save(sys.argv[3])
