"""
Plot the evolution overtime of facial expression. Create an expression profile
"""
import argparse
import numpy as np
import matplotlib.pyplot as plt

__author__ = 'Christophe Ecabert'


class DataPoint:
  """ Data point """
  def __init__(self):
    """ Constructor """
    self.label_idx = 0
    self.label_value = 0


def load_data_from_csv(path, skip_first=False):
  """
  Load emotion detection score from .csv file.
  :param path:        Path to the csv file
  :param skip_first:  Skip first column of csv
  :return data :      Numpy array with data stored inside
  :return header :     Emotion labels
  """

  # Open CSV
  with open(path, 'rb') as f:
    # Get header
    hdr = f.readline().strip('\n')
    if hdr[len(hdr) - 1] == ',':
      hdr = hdr[0:len(hdr) - 1]
    hdr = hdr.split(',')
    # Load data
    idx = 1 if skip_first else 0
    use_col = range(idx, len(hdr), 1)
    hdr = hdr[idx:len(hdr)]
    data = np.loadtxt(f, dtype=float, delimiter=',', usecols=use_col)
  return data, hdr


def create_emotion_label(data):
  """
  :param data:    Data to process
  :return :       Emotion label
  """
  data_pt = DataPoint()
  un_valid = np.all(np.isnan(data), axis=1)
  data_pt.label_idx = data.argmax(axis=1)
  data_pt.label_value = np.choose(data_pt.label_idx, data.transpose())
  data_pt.label_idx[un_valid] = -1
  return data_pt


def print_data(data_series, labels, fps, dpi, region, image_name='plot.png'):
  """
      Plot data
  :param data_series: List of data to plot
  :param labels:      Legend labels
  :param fps:         Fps of the sequence
  :param dpi:         Dpi to use when saving the graph
  :param region:      Region to highlight in the plot
  :param image_name:  Path where to save image
  :return:
  """
  # Assert input is not to large
  if len(data_series) > 2:
    raise ValueError('Can not plot more than two expression data series !')

  # Create plot
  colors = ('b', 'g', 'r', 'c', 'm', 'y', 'k')
  fig = plt.figure(figsize=(1920 / dpi, 1000 / dpi), dpi=dpi)
  # Go through all data
  shift = 0.0
  for data in data_series:
    # extract valid index, unknown index
    idx_valid = np.where(data.label_idx >= 0)
    idx_ko = np.where(data.label_idx < 0)
    # Shape data
    value_time = np.zeros((data.label_idx.shape[0], len(labels)),
                          dtype=float)
    value_time[idx_valid,
               data.label_idx[idx_valid]] = (data.label_value[idx_valid] / 3)
    value_time[idx_ko, :] = -0.2
    # Loop over all label
    for k, k_label in enumerate(labels):
      # shift
      data_to_print = value_time[:, k] + k + shift
      # plot
      plt.plot(np.arange(data_to_print.shape[0]),
               data_to_print,
               alpha=1.0 - shift,
               label=k_label if shift == 0.0 else '',
               color=colors[k])
    # Update shift to avoid overlap
    shift += 0.5
  # Setup limits + cosmetics
  # ---------------------------------------------------------------------
  # x,y limits
  plt.ylim([-0.2, len(labels)])
  plt.xlim([0, data_series[0].label_idx.shape[0]])
  # Y minor tick
  y_minor_ticks = np.arange(0, len(labels), 1.0 / len(data_series))
  # Set xtick every minutes
  space = fps * 60
  plt.xticks(np.arange(0, data_series[0].label_idx.shape[0], space))
  # Region
  for reg in region:
    time_region = reg[0].split('-')
    rgb = reg[1]
    # start idx
    part = time_region[0].split(':')
    idx_start = fps * (int(part[0]) * 60 + int(part[1]))
    # end idx
    part = time_region[1].split(':')
    idx_end = fps * (int(part[0]) * 60 + int(part[1]))
    plt.axvspan(idx_start, idx_end, color=rgb, alpha=0.2)
  # Add legend
  plt.legend(loc=7,
             bbox_to_anchor=(1.15, 0.5),
             fancybox=True)

  # Label
  plt.xlabel('Time')
  plt.ylabel('Emotion profile')
  # Xtick to time, force to draw canvas in order to have access to xtick
  plt.show(block=False)

  ax = fig.get_axes()[0]
  x_lbl = ax.get_xticklabels()
  x_lbl_t = []
  for ax_label in x_lbl:
    # Get current frame number
    txt = str(ax_label.get_text())
    if txt == '':
      txt = str(0)
    val = int(txt)
    # Convert to minutes/sec
    sec_tot = val/fps
    minute = int(sec_tot/60)
    sec = (sec_tot % 60)
    if minute == 0:
      txt = str(sec) + '\'\''
    else:
      txt = str(minute) + '\'' + str(sec) + '\'\''
    x_lbl_t.append(txt)

  ax.set_xticklabels(x_lbl_t)
  ax.set_yticks(y_minor_ticks, minor=True)
  # Grid
  plt.grid(which='both')
  # Save figure
  plt.savefig(image_name, bbox_inches='tight', dpi=dpi)
  return


if __name__ == '__main__':

  # Parse command line
  parser = argparse.ArgumentParser(description='Plot tool for expression '
                                               'analsyis')
  # Input data file, can be a list
  parser.add_argument('-d',
                      type=str,
                      required=True,
                      dest='data',
                      help='Expression data (*.csv), can be list of file '
                           'seperate by ;')
  # Output location
  parser.add_argument('-o',
                      type=str,
                      required=True,
                      dest='plot_name',
                      help='Output filename')
  # Parse input
  args = parser.parse_args()
  # deal with list
  args.data = args.data.split(';')
  # Loop over input
  header = []
  label = []
  for data_path in args.data:
    # Load data
    d, h = load_data_from_csv(data_path, skip_first=True)
    header.append(h)
    # Define label
    lbl = create_emotion_label(d)
    label.append(lbl)
  # Generate plot
  # Region of interest, frmt : ("min:sec-min:sec", "color")
  r = [('0:00-0:05', 'm'),
       ('0:06-0:10', 'b'),
       ('0:13-0:16', 'g'),
       ('0:24-0:27', 'c'),
       ('0:28-0:32', 'y'),
       ('0:33-0:37', 'k')]
  print_data(label,
             header[0],
             fps=30,
             dpi=100,
             region=r,
             image_name=args.plot_name)
