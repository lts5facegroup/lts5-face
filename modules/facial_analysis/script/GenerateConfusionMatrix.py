""" Generate confusation matrix from facial expression detection test
executable
"""
import sys
import os
import glob
import itertools
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score

__author__ = 'Christophe Ecabert'


def parse_result_file(result_file, ground_truth, prediction, labels):
  """
  Parse result file

  :param result_file: File holding detection results
  :param ground_truth:  Ground truth loaded from file
  :param prediction:  Prediction from file
  :param labels:  Labels from file
  """
  # Clear data structure
  del ground_truth[:]     # equivalent to  del ground_truth[0:len(ground_truth)]
  del prediction[:]
  del labels[:]
  # Open file
  file_data = open(result_file, 'r')
  n_detector = int(file_data.readline())
  for _ in range(0, n_detector):
    line = file_data.readline()
    line = line.strip('\n')
    line = line.split(' ')
    labels.append(line[1])

  for line in file_data:
    line = line.strip('\n')
    data = line.split(' ')
    if data[0] != 'NaN' and data[1] != 'NaN':
      ground_truth.append(int(data[0]))
      prediction.append(int(data[1]))
    else:
      print('Drop image : ' + data[2])


def plot_confusion_matrix(cm,
                          labels,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
  """
  Draw confusion matrix
  :param cm:  Confustion matrix
  :param labels:  True labels
  :param title:   Title
  :param cmap:  Colormap to use
  """
  plt.imshow(cm, interpolation='nearest', cmap=cmap)
  # Draw value onto plot
  thresh = cm.max() / 2.
  for i, j in itertools.product(range(cm.shape[0]),
                                range(cm.shape[1])):
    plt.text(j,
             i,
             round(cm[i, j], 2),
             horizontalalignment="center",
             verticalalignment="center",
             color="white" if round(cm[i, j], 2) > thresh else
             "black")
  # Cosmetics
  plt.title(title)
  plt.colorbar()
  tick_marks = np.arange(len(labels))
  plt.xticks(tick_marks, labels, rotation=45)
  plt.yticks(tick_marks, labels)
  plt.ylabel('True label')
  plt.xlabel('Predicted label')


if __name__ == '__main__':
  if len(sys.argv) != 3:
    print(sys.argv[0] + " <test_result> <image_export_name>")
  else:
    # Is it file or folder ?
    list_file = []
    if os.path.isdir(sys.argv[1]):
      # Grab text file
      path = sys.argv[1] + '*.txt' if sys.argv[1][-1] == '/' else \
        sys.argv[1] + '/*.txt'
      list_file = glob.glob(path)
    else:
      list_file.append(sys.argv[1])

    # Process list_file
    f1_scores = []
    precision_scores = []
    recall_scores = []
    f_cnter = 0
    avg_conf_mat = 0
    for f in list_file:
      g_truth = []
      pred = []
      label = []
      parse_result_file(f, g_truth, pred, label)
      # Compute confusion matrix
      conf_mat = confusion_matrix(g_truth, pred)
      conf_mat = conf_mat.astype('float') / conf_mat.sum(axis=1)[:, np.newaxis]
      avg_conf_mat += conf_mat
      # Display
      np.set_printoptions(precision=2)
      print('Confusion matrix, with normalization')
      print(conf_mat)
      # Compute metrics
      f1 = f1_score(g_truth, pred, average='weighted')
      f1s = f1_score(g_truth, pred, average=None)
      recall = recall_score(g_truth, pred, average='weighted')
      prec = precision_score(g_truth, pred, average='weighted')
      f1_scores.append(f1)
      recall_scores.append(recall)
      precision_scores.append(prec)
      print('Precision : ' + str(prec))
      print('Recall : ' + str(recall))
      print('F1 : %.2f %s' % (f1, ''.join(str(f1s))))

    # Average metrics if needed
    if len(f1_scores) > 1:
      print('Avg precision : ' + \
            str(sum(precision_scores) / len(precision_scores)))
      print('Avg recall : ' + \
            str(sum(recall_scores) / len(recall_scores)))
      print('Avg F1 : ' + str(sum(f1_scores) / len(f1_scores)))

    # Save confusion matrix
    avg_conf_mat = avg_conf_mat / len(list_file)
    fig = plt.figure()
    fig.set_tight_layout(True)
    plot_confusion_matrix(avg_conf_mat,
                          label,
                          title='Normalized confusion matrix')
    plt.show(block=False)
    part = sys.argv[2].split('.')
    if len(part) == 1:
      # no ext
      part.append('.png')
    else:
      part[-1] = '.png'
    # ok
    fig.savefig(''.join(part))
