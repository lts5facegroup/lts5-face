/**
 *  @file   lts5/regressor/regressor.hpp
 *  LTS5-Lib
 *
 *  Created by Christophe Ecabert on 28/10/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_REGRESSOR__
#define __LTS5_REGRESSOR__

/** Random fern */
#include "lts5/regressor/random_fern.hpp"
/** Cascaded Random fern */
#include "lts5/regressor/cascaded_random_fern.hpp"


#endif /* __LTS5_REGRESSOR__ */
