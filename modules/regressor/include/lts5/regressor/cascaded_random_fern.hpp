/**
 *  @file   cascaded_random_fern.hpp
 *  @brief  Cascaded random fern
 *  @ingroup regressor
 *
 *  @author Christophe Ecabert
 *  @date 29/10/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_CASCADED_RANDOM_FERN__
#define __LTS5_CASCADED_RANDOM_FERN__

#include <istream>
#include <string>
#include <vector>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/regressor/random_fern.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  CascadedRandomFern
 *  @brief  Cascade of Random Fern regressor for face alignment
 *  @author Christophe Ecabert
 *  @date   29/10/15
 *  @see    "Face alignment by explicit shape regression" by Cao
 *  @ingroup regressor
 */
class LTS5_EXPORTS CascadedRandomFern {

#pragma mark -
#pragma mark Type definition
 public :

  /**
   *  @struct IndexedFeature
   *  @brief  Position feature relative to a position
   */
  struct IndexedFeature {
    /** Shift in x direction */
    double delta_x;
    /** Shift in y direction */
    double delta_y;
    /** Index of the relative position */
    int index;

    /**
     *  @name IndexedFeature
     *  @fn IndexedFeature(void)
     *  @brief  Default constructor
     */
    IndexedFeature(void) : delta_x(-1.0), delta_y(-1.0), index(-1) {}

    /**
     *  @name Size
     *  @fn int Size(void) const
     *  @brief  Compute struct memory size
     *  @return Structure size in byte
     */
    int Size(void) const {
      return sizeof(delta_x) + sizeof(delta_y) + sizeof(index);
    }

    /**
     *  @name Write
     *  @fn int Write(std::ostream& out_stream) const
     *  @brief  Write parameters into stream
     *  @param[in] out_stream Stream where to write data
     *  @return -1 if error, 0 otherwise
     */
    int Write(std::ostream& out_stream) const {
      int error = -1;
      if (out_stream.good()) {
        out_stream.write(reinterpret_cast<const char*>(&delta_x),
                         sizeof(delta_x));
        out_stream.write(reinterpret_cast<const char*>(&delta_y),
                         sizeof(delta_y));
        out_stream.write(reinterpret_cast<const char*>(&index),
                         sizeof(index));
        error = out_stream.good() ? 0 : -1;
      }
      return error;
    }

    /**
     *  @name Read
     *  @fn int Read(std::istream& in_stream)
     *  @brief  Read parameters from stream
     *  @param[in] in_stream Stream where to read data
     *  @return -1 if error, 0 otherwise
     */
    int Read(std::istream& in_stream) {
      int error = -1;
      if (in_stream.good()) {
        in_stream.read(reinterpret_cast<char*>(&delta_x), sizeof(delta_x));
        in_stream.read(reinterpret_cast<char*>(&delta_y), sizeof(delta_y));
        in_stream.read(reinterpret_cast<char*>(&index), sizeof(index));
        error = in_stream.good() ? 0 : -1;
      }
      return error;
    }
  };

#pragma mark -
#pragma mark Initialization

 public :
  /**
   *  @name CascadedRandomFern
   *  @fn CascadedRandomFern(void)
   *  @brief  Constructor
   */
  CascadedRandomFern(void);

  /**
   *  @name CascadedRandomFern
   *  @fn explicit CascadedRandomFern(std::istream& in_stream)
   *  @brief  Constructor
   *  @param[in]  in_stream Stream from where to load
   */
  explicit CascadedRandomFern(std::istream& in_stream);

  /**
   *  @name CascadedRandomFern
   *  @fn explicit CascadedRandomFern(const std::string& filename)
   *  @brief  Constructor
   *  @param[in]  filename  File from where to load
   */
  explicit CascadedRandomFern(const std::string& filename);

  /**
   *  @name CascadedRandomFern
   *  @fn explicit CascadedRandomFern(const LTS5::CascadedRandomFern& other)
   *  @brief Copy constructor
   *  @param[in]  other Object to copy from
   */
  explicit CascadedRandomFern(const LTS5::CascadedRandomFern& other);

  /**
   *  @name ~CascadedRandomFern
   *  @fn ~CascadedRandomFern(void)
   *  @brief  Destructor
   */
  ~CascadedRandomFern(void);

  /**
   *  @name Load
   *  @fn int Load(std::istream& in_stream)
   *  @brief  Load cascaded random fern from stream
   *  @param[in]  in_stream Input binary stream
   */
  int Load(std::istream& in_stream);

  /**
   *  @name Load
   *  @fn int Load(const std::string& filename)
   *  @brief  Load cascaded random fern from file
   *  @param[in]  filename  Path to the file
   */
  int Load(const std::string& filename);

  /**
   *  @name Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Dump object into a given stream
   *  @param[in]  out_stream  Stream where to write object
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the size (in byte) of memory used by the object
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Operator

  /**
   *  @name operator=
   *  @fn LTS5::CascadedRandomFern& operator=(const LTS5::CascadedRandomFern& others);
   *  @brief  copy assignment operator
   *  @return New assigned cascaded random fern regressor
   */
  LTS5::CascadedRandomFern& operator=(const LTS5::CascadedRandomFern& others);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name set_mean_shape
   *  @fn void set_mean_shape(const cv::Mat& mean_shape)
   *  @brief  Set reference mean shape
   *  @param[in]  mean_shape  Mean shape
   */
  void set_mean_shape(const cv::Mat& mean_shape) {
    mean_shape_ = mean_shape;
  }

#pragma mark -
#pragma mark Predict

  void Predict(const cv::Mat& image,
               const cv::Rect& face_bbox,
               const cv::Mat& current_shape,
               cv::Mat* prediction);

#pragma mark -
#pragma mark Private

 private :
  /** Indexed features */
  std::vector<std::vector<IndexedFeature>*> index_features_;
  /** List of Random Fern */
  std::vector<LTS5::RandomFern*> fern_;
  /** Mean shape */
  cv::Mat mean_shape_;
  /** Number of fern */
  int n_fern_;
  /** Number of feature for each fern */
  int n_feat_fern_;
  /** Features extracted */
  cv::Mat extracted_features_;

};
}  // namespace LTS5
#endif /* __LTS5_CASCADED_RANDOM_FERN__ */
