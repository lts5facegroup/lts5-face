/**
 *  @file   random_fern_trainer.hpp
 *  @brief  Random fern trainer
 *  @ingroup regressor
 *
 *  @author Christophe Ecabert
 *  @date 28/10/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_RANDOM_FERN_TRAINER__
#define __LTS5_RANDOM_FERN_TRAINER__

#include <string>
#include <ostream>
#include <vector>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  RandomFernTrainer
 *  @brief  Training class for RandomFern
 *  @author Christophe Ecabert
 *  @date   28/10/15
 *  @see "Face alignment by explicit shape regression" by Cao
 *  @ingroup regressor
 */
class LTS5_EXPORTS RandomFernTrainer {

  public :

#pragma mark -
#pragma mark Initialization
  /**
   *  @name RandomFernTrainer
   *  @fn RandomFernTrainer(void)
   *  @brief  Constructor
   */
  RandomFernTrainer(void);

  /**
   *  @name ~RandomFernTrainer
   *  @fn ~RandomFernTrainer(void)
   *  @brief  Destructor
   */
  ~RandomFernTrainer(void);

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the size (in byte) of memory used by the object
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Train

  /**
   *  @name Train
   *  @fn void Train(const cv::Mat& features,
                     const cv::Mat& target,
                     const std::vector<double>& max_threshold,
                     const double shrinkage,
                     cv::Mat* prediction)
   *  @brief  Train one random fern for a given set of features
   *  @param[in]  features        Features stored by row [Nsample x Nfeat]
   *  @param[in]  target          Value to predict stored by row [Nsample x k]
   *  @param[in]  max_threshold   Upper bound for random threshold selection
   *  @param[in]  shrinkage       Shrinkage parameters
   *  @param[out] prediction      Shape update learnt [Nsample x k]
   */
  void Train(const cv::Mat& features,
             const cv::Mat& target,
             const std::vector<double>& max_threshold,
             const double shrinkage,
             cv::Mat* prediction);

  /**
   *  @name Save
   *  @fn int Save(const std::string& filename) const
   *  @brief  Save random fern into binary file
   *  @return -1 if error, 0 otherwise
   */
  int Save(const std::string& filename) const;

  /**
   *  @name Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Write random fern into binary stream
   *  @return -1 if error, 0 otherwise
   */
  int Write(std::ostream& out_stream) const;

#pragma mark -
#pragma mark Private

 private :

  /** Threshold */
  std::vector<double> threshold_;
  /** Regressor output */
  std::vector<cv::Mat> bins_regression_;
};
}  // namespace LTS5
#endif /* __LTS5_random_fern_trainer__ */
