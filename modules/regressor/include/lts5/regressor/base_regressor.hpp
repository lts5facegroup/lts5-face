/**
 *  @file   base_regressor.hpp
 *  @brief  Regressor interface
 *  @ingroup regressor
 *
 *  @author Christophe Ecabert
 *  @date 28/10/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_BASE_REGRESSOR__
#define __LTS5_BASE_REGRESSOR__

#include <ostream>

#include "opencv2/core/core.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BaseRegressor
 *  @brief  Interface for regressor object
 *  @author Christophe Ecabert
 *  @date   28/10/15
 *  @ingroup regressor
 */
class BaseRegressor {

 public :

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   ~BaseRegressor
   *  @fn virtual ~BaseRegressor(void)
   *  @brief  Destructor
   */
  virtual ~BaseRegressor(void) {}

  /**
   *  @name Write
   *  @fn virtual int Write(std::ostream& out_stream) const = 0
   *  @brief  Dump object into a given stream
   *  @param[in]  out_stream  Stream where to write object
   */
  virtual int Write(std::ostream& out_stream) const = 0;

  /**
   *  @name ComputeObjectSize
   *  @fn virtual int ComputeObjectSize(void) const = 0
   *  @brief  Compute the size (in byte) of memory used by the object
   */
  virtual int ComputeObjectSize(void) const = 0;

#pragma mark -
#pragma mark Process

  /**
   *  @name Predict
   *  @fn virtual void Predict(const cv::Mat& feature, cv::Mat* prediction) = 0
   *  @brief  Provide an update based on a selection of features
   *  @param[in]      feature     Feature to make prediction
   *  @param[in,out]  prediction  Result of the regression,
   *                              pred = pred + update
   */
  virtual void Predict(const cv::Mat& feature, cv::Mat* prediction) = 0;

};

}  // namespace LTS5


#endif /* __LTS5_BASE_REGRESSOR__ */
