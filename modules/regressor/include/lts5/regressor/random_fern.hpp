/**
 *  @file   random_fern.hpp
 *  @brief  Random fern
 *  @ingroup regressor
 *
 *  @author Christophe Ecabert
 *  @date 28/10/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_RANDOM_FERN__
#define __LTS5_RANDOM_FERN__

#include <istream>
#include <string>
#include <vector>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/regressor/base_regressor.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  RandomFern
 *  @brief  Random fern regressor
 *  @author Christophe Ecabert
 *  @date   28/10/15
 *  @see    "Face alignment by explicit shape regression" by Cao
 *  @ingroup regressor
 */
class LTS5_EXPORTS RandomFern : public BaseRegressor {

 public :
#pragma mark -
#pragma mark Initialization

  /**
   *  @name RandomFern
   *  @fn RandomFern(void)
   *  @brief Constructor
   */
  RandomFern(void);

  /**
   *  @name RandomFern
   *  @fn explicit RandomFern(std::istream& in_stream)
   *  @brief  Constructor
   *  @param[in]  in_stream Stream to load object from.
   */
  explicit RandomFern(std::istream& in_stream);

  /**
   *  @name RandomFern
   *  @fn explicit RandomFern(const std::string& filename)
   *  @brief  Constructor
   *  @param[in]  filename  File to load object from
   */
  explicit RandomFern(const std::string& filename);

  /**
   *  @name RandomFern
   *  @fn explicit RandomFern(const LTS5::RandomFern& other)
   *  @brief  Copy constructor Constructor
   *  @param[in]  other Object to copy from
   */
  explicit RandomFern(const LTS5::RandomFern& other);

  /**
   *  @name ~RandomFern
   *  @fn ~RandomFern(void)
   *  @brief  Destructor
   */
  ~RandomFern(void);

  /**
   *  @name Load
   *  @fn int Load(std::istream& in_stream)
   *  @brief  Load fern from stream
   *  @param[in]  in_stream Input binary stream
   */
  int Load(std::istream& in_stream);

  /**
   *  @name Load
   *  @fn int Load(const std::string& filename)
   *  @brief  Load fern from file
   *  @param[in]  filename  Path to the file
   */
  int Load(const std::string& filename);

  /**
   *  @name Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Dump object into a given stream
   *  @param[in]  out_stream  Stream where to write object
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the size (in byte) of memory used by the object
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Operator

  /**
   *  @name operator=
   *  @fn LTS5::RandomFern& operator=(const LTS5::RandomFern& others);
   *  @brief  copy assignment operator
   *  @return New assigned fern regressor
   */
  LTS5::RandomFern& operator=(const LTS5::RandomFern& others);

#pragma mark -
#pragma mark Predict

  /**
   *  @name Predict
   *  @fn void Predict(const cv::Mat& features, const cv::Mat* prediction)
   *  @brief  Provide an update based on a selection of features
   *  @param[in]      features    Feature to make prediction
   *  @param[in,out]  prediction  Result of the regression,
   *                              pred = pred + update
   */
  void Predict(const cv::Mat& features, cv::Mat* prediction);

 private :

  /** Binary thresold */
  std::vector<double> threshold_;
  /** Bins output */
  std::vector<cv::Mat> bins_regression_;
};
}  // namespace LTS5
#endif /* __LTS5_RANDOM_FERN__ */
