/**
 *  @file   cascaded_random_fern_trainer.hpp
 *  @brief  Cascade of Random Fern regressor training algorithm for face
 *          alignment
 *  @ingroup regressor
 *
 *  @author Christophe Ecabert
 *  @date 29/10/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_CASCADED_RANDOM_FERN_TRAINER__
#define __LTS5_CASCADED_RANDOM_FERN_TRAINER__

#include <vector>
#include <string>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/regressor/cascaded_random_fern.hpp"
#include "lts5/regressor/random_fern_trainer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  CascadedRandomFernTrainer
 *  @brief  Cascade of Random Fern regressor training algorithm for face
 *          alignment
 *  @author Christophe Ecabert
 *  @date   29/10/15
 *  @see    "Face alignment by explicit shape regression" by Cao
 *  @ingroup regressor
 */
class LTS5_EXPORTS CascadedRandomFernTrainer {
 public :

#pragma mark -
#pragma mark Initialization
  /**
   *  @name CascadedRandomFernTrainer
   *  @fn CascadedRandomFernTrainer(void)
   *  @brief  Constructor
   */
  CascadedRandomFernTrainer(void);

  /**
   *  @name ~CascadedRandomFernTrainer
   *  @fn ~CascadedRandomFernTrainer(void)
   *  @brief  Destructor
   */
  ~CascadedRandomFernTrainer(void);

#pragma mark -
#pragma mark Train

  /**
   *  @name Train
   *  @fn void void Train(const std::vector<std::string>& image_list,
                         const cv::Mat& current_shape,
                         const cv::Mat& ground_truth,
                         const std::vector<cv::Rect>& face_bbox,
                         const cv::Mat& mean_shape,
                         const int n_random_fern,
                         const int n_candidate_feature,
                         const int n_fern_feature,
                         const double shrinkage,
                         cv::Mat* prediction)
   *  @brief  Train cascaded random fern include feature selection based on
   *          correlation
   *  @param[in]  image_list          List of training image
   *  @param[in]  current_shape       Current shape (S_i)
   *  @param[in]  ground_truth        Ground truth (target)
   *  @param[in]  face_bbox           Face bounding box
   *  @param[in]  mean_shape          Mean shape
   *  @param[in]  n_random_fern       Number of random fern in the cascade
   *  @param[in]  n_candidate_feature Size of the feature pool
   *  @param[in]  n_fern_feature      Number of features to retain (select)
   *  @param[in]  shrinkage           Shrinkage coefficients
   *  @param[out] prediction          Shape update learnt [Nsample x k]
   */
  void Train(const std::vector<std::string>& image_list,
             const cv::Mat& current_shape,
             const cv::Mat& ground_truth,
             const std::vector<cv::Rect>& face_bbox,
             const cv::Mat& mean_shape,
             const int n_random_fern,
             const int n_candidate_feature,
             const int n_fern_feature,
             const double shrinkage,
             cv::Mat* prediction);

  /**
   *  @name Save
   *  @fn int Save(std::string& filename) const
   *  @brief  Save Random fern cascade into a given file
   *  @param[in]  filename  File where to dump object
   *  @return -1 if error, 0 otherwise
   */
  int Save(std::string& filename) const;

  /**
   *  @name Write
   *  @fn int Write(std::ostream& out_stream) const
   *  @brief  Write Random fern cascade into a given stream
   *  @param[in]  out_stream  Stream where to dump object
   *  @return -1 if error, 0 otherwise
   */
  int Write(std::ostream& out_stream) const;

  /**
   *  @name ComputeObjectSize
   *  @fn int ComputeObjectSize(void) const
   *  @brief  Compute the size (in byte) of memory used by the object
   */
  int ComputeObjectSize(void) const;

#pragma mark -
#pragma mark Private

 private :
  /** Indexed features candidate */
  std::vector<CascadedRandomFern::IndexedFeature> candidate_indexed_features_;
  /** Indexed features selected */
  std::vector<std::vector<CascadedRandomFern::IndexedFeature>> indexed_features_;
  /** Random fern regressor trainer */
  std::vector<RandomFernTrainer*> fern_trainer_;
};

}  // namespace LTS5
#endif /* __LTS5_CASCADED_RANDOM_FERN_TRAINER__ */
