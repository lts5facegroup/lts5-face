/**
 *  @file   random_fern.cpp
 *  LTS5-Lib
 *
 *  Created by Christophe Ecabert on 28/10/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <assert.h>
#include <utility>

#include "lts5/utils/object_type.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/regressor/regressor.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name RandomFern
 *  @fn RandomFern(void)
 *  @brief Constructor
 */
RandomFern::RandomFern(void) : threshold_(0), bins_regression_(0) {}

/*
 *  @name RandomFern
 *  @fn explicit RandomFern(std::istream& in_stream)
 *  @brief  Constructor
 *  @param[in]  in_stream Stream to load object from.
 */
RandomFern::RandomFern(std::istream& in_stream) :
  threshold_(0), bins_regression_(0) {
  this->Load(in_stream);
}

/*
 *  @name RandomFern
 *  @fn explicit RandomFern(const std::string& filename)
 *  @brief  Constructor
 *  @param[in]  filename  File to load object from
 */
RandomFern::RandomFern(const std::string& filename) :
  threshold_(0), bins_regression_(0) {
  this->Load(filename);
}

/*
 *  @name RandomFern
 *  @fn explicit RandomFern(const LTS5::RandomFern& other)
 *  @brief  Copy constructor Constructor
 *  @param[in]  other Object to copy from
 */
RandomFern::RandomFern(const LTS5::RandomFern& other) {
  // Copy thresholds
  threshold_ = other.threshold_;
  // Copy regressor updates
  const int n_bins = static_cast<int>(other.bins_regression_.size());
  bins_regression_.resize(n_bins);
  for (int i = 0; i < n_bins; ++i) {
    other.bins_regression_[i].copyTo(bins_regression_[i]);
  }
}

/*
 *  @name ~RandomFern
 *  @fn ~RandomFern(void)
 *  @brief  Destructor
 */
RandomFern::~RandomFern(void) {}

/*
 *  @name Load
 *  @fn int Load(std::istream& in_stream)
 *  @brief  Load fern from stream
 *  @param[in]  in_stream Input binary stream
 */
int RandomFern::Load(std::istream& in_stream) {
  int error = -1;
  if (in_stream.good()) {
    // Read number of thresh
    int n = -1;
    in_stream.read(reinterpret_cast<char*>(&n), sizeof(n));
    // Read threshold value
    threshold_.resize(n);
    in_stream.read(reinterpret_cast<char*>(threshold_.data()),
                   n * sizeof(threshold_[0]));
    // Read number of bins
    in_stream.read(reinterpret_cast<char*>(&n), sizeof(n));
    // Read bins
    error = in_stream.good() ? 0 : -1;
    bins_regression_.resize(n);
    for (int i = 0; i < n; ++i) {
      error |= LTS5::ReadMatFromBin(in_stream, &bins_regression_[i]);
    }
  }
  return error;
}

/*
 *  @name Load
 *  @fn int Load(const std::string& filename)
 *  @brief  Load fern from file
 *  @param[in]  filename  Path to the file
 */
int RandomFern::Load(const std::string& filename) {
  int error = -1;
  std::ifstream in_stream(filename.c_str(),std::ios_base::binary);
  if (in_stream.is_open()) {
    // Load random fern
    error = this->Load(in_stream);
    // Close stream
    in_stream.close();
  }
  return error;
}

/*
 *  @name Write
 *  @fn int Write(std::ostream& out_stream) const
 *  @brief  Dump object into a given stream
 *  @param[in]  out_stream  Stream where to write object
 */
int RandomFern::Write(std::ostream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Everything is fine, start to write header
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kRandomFern);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type),
                     sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size),
                     sizeof(obj_size));
    // #Threshold
    int n = static_cast<int>(threshold_.size());
    out_stream.write(reinterpret_cast<const char*>(&n), sizeof(n));
    // Data thresh
    out_stream.write(reinterpret_cast<const char*>(threshold_.data()),
                     n * sizeof(threshold_[0]));
    // #Bins
    n = static_cast<int>(bins_regression_.size());
    out_stream.write(reinterpret_cast<const char*>(&n), sizeof(n));
    // Bins
    error = out_stream.good() ? 0 : -1;
    for (int i = 0; i < n; ++i) {
      error |= LTS5::WriteMatToBin(out_stream, bins_regression_[i]);
    }
  }
  return error;
}

/*
 *  @name ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the size (in byte) of memory used by the object
 */
int RandomFern::ComputeObjectSize(void) const {
  // Threshold vector size
  int n = static_cast<int>(threshold_.size());
  int size = sizeof(n);
  // Thresholds vector
  size += n * sizeof(threshold_[0]);
  // Number of bins
  n = static_cast<int>(bins_regression_.size());
  size += sizeof(n);
  // Matrix
  for( int i = 0; i < n; ++i) {
    // Matrix header
    size += 3 * sizeof(int);
    // Matrix data
    size += bins_regression_[i].total() * bins_regression_[i].elemSize();
  }
  return size;
}

#pragma mark -
#pragma mark Operator

/**
 *  @name operator=
 *  @fn LTS5::RandomFern& operator=(const LTS5::RandomFern& others);
 *  @brief  copy assignment operator
 *  @return New assigned fern regressor
 */
LTS5::RandomFern& RandomFern::operator=(const LTS5::RandomFern& others) {
  if (this != &others) {
    threshold_ = others.threshold_;
    const int n_bin = static_cast<int>(others.bins_regression_.size());
    bins_regression_.resize(n_bin);
    for (int i = 0; i < n_bin; ++i) {
      others.bins_regression_[i].copyTo(bins_regression_[i]);
    }
  }
  return *this;
}

#pragma mark -
#pragma mark Predict

/*
 *  @name Predict
 *  @fn void Predict(const cv::Mat& features, const cv::Mat* prediction)
 *  @brief  Provide an update based on a selection of features
 *  @param[in]      features    Feature to make prediction
 *  @param[in,out]  prediction  Result of the regression,
 *                              pred = pred + update
 */
void RandomFern::Predict(const cv::Mat& features, cv::Mat* prediction) {
  // Sanity check
  assert((features.cols == 1 && features.rows == threshold_.size()) ||
         (features.rows == 1 && features.cols == threshold_.size()));
  assert(bins_regression_[0].cols == prediction->cols &&
         bins_regression_[0].rows == prediction->rows);
  assert(prediction->type() == CV_64FC1);

  // Find bin index
  int idx = 0;
  const double* ptr = reinterpret_cast<double*>(features.data);
  for (int i = 0; i < threshold_.size(); ++i) {
    if (ptr[i] >= threshold_[i]) {
      idx += (0x01 << i);
    }
  }
  // Update
  LTS5::LinearAlgebra<double>::Axpy(bins_regression_[idx], 1.0, prediction);
}

}  // namespace LTS5