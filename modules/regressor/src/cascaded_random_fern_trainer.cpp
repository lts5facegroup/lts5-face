/**
 *  @file   cascaded_random_fern_trainer.cpp
 *  LTS5-Lib
 *
 *  Created by Christophe Ecabert on 29/10/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <random>
#include <limits>
#include <fstream>
#include <chrono>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/shape_transforms.hpp"
#include "lts5/utils/object_type.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/regressor/cascaded_random_fern_trainer.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name CascadedRandomFernTrainer
 *  @fn CascadedRandomFernTrainer()
 *  @brief  Constructor
 */
CascadedRandomFernTrainer::CascadedRandomFernTrainer() :
candidate_indexed_features_(0), indexed_features_(0), fern_trainer_(0) {

}

/*
 *  @name ~CascadedRandomFernTrainer
 *  @fn ~CascadedRandomFernTrainer()
 *  @brief  Destructor
 */
CascadedRandomFernTrainer::~CascadedRandomFernTrainer() {
  // Clean up
  for (int i = 0; i < fern_trainer_.size(); ++i) {
    delete fern_trainer_[i];
  }
}

#pragma mark -
#pragma mark Train

/*
 *  @name Train
 *  @fn void Train(const std::vector<std::string>& image_list,
                   const std::vector<cv::Mat>& current_shape,
                   const std::vector<cv::Mat>& ground_truth,
                   const std::vector<cv::Rect>& face_bbox,
                   const cv::Mat& mean_shape,
                   const int n_random_fern,
                   const int n_candidate_feature,
                   const int n_fern_feature)
 *  @brief  Train cascaded random fern include feature selection based on
 *          correlation
 *  @param[in]  image_list          List of training image
 *  @param[in]  current_shape       Current shape (S_i)
 *  @param[in]  ground_truth        Ground truth (target)
 *  @param[in]  mean_shape          Mean shape
 *  @param[in]  n_random_fern       Number of random fern in the cascade
 *  @param[in]  n_candidate_feature Size of the feature pool
 *  @param[in]  n_fern_feature      Number of features to retain (select)
 *  @param[in]  shrinkage           Shrinkage coefficients
 *  @param[out] prediction          Shape update learnt [Nsample x k]
 */
void CascadedRandomFernTrainer::Train(const std::vector<std::string>& image_list,
                                      const cv::Mat& current_shape,
                                      const cv::Mat& ground_truth,
                                      const std::vector<cv::Rect>& face_bbox,
                                      const cv::Mat& mean_shape,
                                      const int n_random_fern,
                                      const int n_candidate_feature,
                                      const int n_fern_feature,
                                      const double shrinkage,
                                      cv::Mat* prediction) {
  // Sanity check
  assert(image_list.size() == current_shape.rows &&
         image_list.size() == ground_truth.rows);
  assert(current_shape.type() == CV_64FC1);
  assert(!mean_shape.empty());

  using Vec2 = LTS5::Vector2<double>;

  // Compute regression target for every sample in the normalized space
  // ---------------------------------------------------------------------------
  std::cout << "\t\tCompute regression target ..." << std::endl;
  const int n_sample = static_cast<int>(image_list.size());
  const int n_pts = current_shape.cols / 2;
  cv::Mat regression_delta(n_sample, current_shape.cols, CV_64FC1);// S_i - Shat_i
  std::vector<cv::Mat> transform;
  std::vector<cv::Mat> inv_transform;
  transform.resize(n_sample);
  inv_transform.resize(n_sample);
  cv::Mat delta;
  cv::Mat delta_t;
  cv::Mat norm_current;
  cv::Mat norm_ground_truth;
  for (int s = 0; s < n_sample; ++s) {
    // Normalize shapes
    LTS5::NormalizeShapeXY<double>(current_shape.row(s),
                                   face_bbox[s],
                                   &norm_current);
    LTS5::NormalizeShapeXY<double>(ground_truth.row(s),
                                   face_bbox[s],
                                   &norm_ground_truth);
    // Compute transform current_shape and mean_shape
    LTS5::SimilarityTransformXYXY(norm_current, mean_shape, &transform[s]);
    transform[s].at<double>(0,2) = 0.0;
    transform[s].at<double>(1,2) = 0.0;
    // Compute delta shape
    delta = norm_ground_truth - norm_current;
    delta = delta.reshape(2, n_pts);
    // Apply transformation
    cv::transform(delta, delta_t, transform[s]);
    // Convert back into row vector
    delta_t.reshape(1, 1).copyTo(regression_delta.row(s));
  }
  // Sample meanshape to generate indexed features
  // ---------------------------------------------------------------------------
  std::cout << "\t\tSample mean shape ..." << std::endl;
  // Find bounding box of the mean shape
  double xmin = std::numeric_limits<double>::max();
  double xmax = std::numeric_limits<double>::lowest();
  double ymin = std::numeric_limits<double>::max();
  double ymax = std::numeric_limits<double>::lowest();
  const Vec2* m_ptr = reinterpret_cast<Vec2*>(mean_shape.data);
  for (int p = 0; p < n_pts; ++p) {
    const Vec2& pts = m_ptr[p];
    xmin = pts.x_ < xmin ? pts.x_ : xmin;
    xmax = pts.x_ > xmax ? pts.x_ : xmax;
    ymin = pts.y_ < ymin ? pts.y_ : ymin;
    ymax = pts.y_ > ymax ? pts.y_ : ymax;
  }
  // Add 20% margin
  xmin *= 1.2;  xmax *= 1.2;
  ymin *= 1.2;  ymax *= 1.2;
  // Sample
  unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now()
                                        .time_since_epoch()
                                        .count());
  std::mt19937_64 gen_x;
  seed = static_cast<unsigned>(std::chrono::system_clock::now()
                               .time_since_epoch()
                               .count());
  std::mt19937_64 gen_y(seed);
  std::uniform_real_distribution<double> dist_x(xmin, xmax);
  std::uniform_real_distribution<double> dist_y(ymin, ymax);
  candidate_indexed_features_.resize(n_candidate_feature);
  double x, y;
  const Vec2* mean_ptr = reinterpret_cast<Vec2*>(mean_shape.data);
  double dx, dy, dist;
  CascadedRandomFern::IndexedFeature feature;
  for (int f = 0; f < n_candidate_feature; ++f) {
    // Generate random (x,y) pair
    x = dist_x(gen_x);
    y = dist_y(gen_y);
    // Find closest point in the mean_shape
    double min_dist = std::numeric_limits<double>::max();
    for (int p = 0; p < n_pts; ++p) {
      const Vec2& pts = mean_ptr[p];
      dx = x - pts.x_;
      dy = y - pts.y_;
      dist = (dx * dx) + (dy * dy);
      if (dist < min_dist) {
        min_dist = dist;
        feature.delta_x = dx;
        feature.delta_y = dy;
        feature.index = p * 2;
      }
    }
    // Save indexed features
    candidate_indexed_features_[f] = feature;
  }
  // Extract feature from image -> pixel values
  // ---------------------------------------------------------------------------
  std::cout << "\t\tExtract pool of random features ..." << std::endl;
  cv::Mat pix_value = cv::Mat(n_sample, n_candidate_feature, CV_64FC1);

  Parallel::For(n_sample,
                [&](const size_t& s) {
    cv::Mat center_shape;
    // Current reference
    const cv::Mat shape = current_shape.row(static_cast<int>(s));
    const cv::Rect& bbox = face_bbox[s];
    const double bbox_w = bbox.width / 2.0;
    const double bbox_h = bbox.height / 2.0;
    // Inverse transform, from mean_shape to shape
    LTS5::NormalizeShapeXY<double>(shape, bbox, &center_shape);
    LTS5::SimilarityTransformXYXY(mean_shape, center_shape, &inv_transform[s]);
    inv_transform[s].at<double>(0, 2) = 0.0;
    inv_transform[s].at<double>(1, 2) = 0.0;

    const double* t_ptr = reinterpret_cast<double*>(inv_transform[s].data);
    // Load image
    cv::Mat img = cv::imread(image_list[s]);
    if (img.channels() != 1) {
      cv::cvtColor(img, img, cv::COLOR_BGR2GRAY);
    }

    cv::Mat canvas;
    cv::cvtColor(img, canvas, cv::COLOR_GRAY2BGR);


    // Process each candidate
    auto idx_it = candidate_indexed_features_.begin();
    for (int f = 0; f < n_candidate_feature; ++f, ++ idx_it) {
      // Convert position
      double norm_x = (t_ptr[0] * idx_it->delta_x +
                       t_ptr[1] * idx_it->delta_y) * bbox_w ;
      double norm_y = (t_ptr[3] * idx_it->delta_x +
                       t_ptr[4] * idx_it->delta_y) * bbox_h;
      norm_x += shape.at<double>(idx_it->index);
      norm_y += shape.at<double>(idx_it->index + 1);
      // Check boundaries
      int img_x = static_cast<int>(norm_x);
      int img_y = static_cast<int>(norm_y);
      img_x = std::max(0, std::min(img_x, img.cols - 1));
      img_y = std::max(0, std::min(img_y, img.rows - 1));
      // Pick pixel intensity
      pix_value.at<double>((int)s, f) = static_cast<double>(img.at<uchar>(img_y,
                                                                          img_x));
    }
  });
  // Compute covariance between each pixel instensity [ cov(f_i, f_j) ]
  // ---------------------------------------------------------------------------
  using TransposeType = LTS5::LinearAlgebra<double>::TransposeType;
  std::cout << "\t\tCompute random features cov(f_i, f_j) ..." << std::endl;
  cv::Mat mean = cv::Mat(n_candidate_feature, 1, CV_64FC1);
  cv::Mat cov_pix_val = cv::Mat(n_candidate_feature,
                                n_candidate_feature,
                                current_shape.type());
  // Average over every column
  LTS5::LinearAlgebra<double>::Mean(pix_value, 1, &mean);
  // remove mean from features
  cv::Mat pix_value_no_mean = pix_value.clone();
  LTS5::LinearAlgebra<double>::Ger(cv::Mat::ones(n_sample, 1, CV_64FC1),
                                   mean,
                                   -1.0,
                                   &pix_value_no_mean);
  double alpha = 1.0 / static_cast<double>(pix_value_no_mean.rows);
  LTS5::LinearAlgebra<double>::Gemm(pix_value_no_mean,
                                    TransposeType::kTranspose,
                                    alpha,
                                    pix_value_no_mean,
                                    TransposeType::kNoTranspose,
                                    0.0,
                                    &cov_pix_val);
  // Train each fern
  // ---------------------------------------------------------------------------
  if (fern_trainer_.size() != 0) {
    for (int i = 0; i < fern_trainer_.size(); ++i) {
      delete fern_trainer_[i];
    }
  }
  fern_trainer_.resize(n_random_fern);
  indexed_features_.resize(n_random_fern);
  auto select_idx_feat_it = indexed_features_.begin();
  prediction->create(n_sample, n_pts * 2, CV_64FC1);
  prediction->setTo(0.0);
  for (int rf = 0; rf < n_random_fern; ++rf, ++select_idx_feat_it) {
    if (rf % 50 == 0) {
      std::cout << "\t\tStart processing random fern : " << rf + 1
      << "/" << n_random_fern << std::endl;
    }
    // Randomly project target regression
    unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now()
                                          .time_since_epoch()
                                          .count());
    std::mt19937_64 gen(seed);
    std::uniform_real_distribution<double> dist(-1.0, 1.0);
    cv::Mat axis = cv::Mat(1, 2 * n_pts, CV_64FC1);
    // Do it F times
    select_idx_feat_it->resize(n_fern_feature * 2);
    cv::Mat selected_idx = cv::Mat(n_fern_feature, 2, CV_32SC1, cv::Scalar(-1));







    for (int nff = 0; nff < n_fern_feature; ++nff) {
      // Random axis
      for (int i = 0; i < axis.cols; ++i) {
        axis.at<double>(i) = dist(gen);
      }
      cv::normalize(axis, axis);
      // Project target along the random direction
      std::vector<double> proj_target(n_sample, 0);
      cv::Mat proj_target_mat(1, n_sample, CV_64FC1, proj_target.data());
      double sum = 0.0;
      for (int i = 0; i < n_sample; ++i) {
        proj_target[i] = LTS5::LinearAlgebra<double>::Dot(regression_delta.row(i),
                                                          axis);
        sum += proj_target[i];
      }
      double mean_proj = sum / static_cast<double>(n_sample);
      cv::Mat proj = proj_target_mat - mean_proj;
      // Compute variance of projection's target at the same time
      cv::Mat proj_square;
      cv::pow(proj, 2.0,proj_square);
      double std_proj = std::sqrt(cv::mean(proj_square)[0]);
      // Compute cov(y, f_i) / cov(y, f_j),
      // y = projection on random axis
      // f_i = pixel value
      std::vector<double> cov_proj_pix(n_candidate_feature, 0.0);
      for (int i = 0; i < n_candidate_feature; ++i) {
        cv::Mat feat = pix_value_no_mean.col(i);

        const double* pix_ptr = pix_value_no_mean.ptr<double>(0, i);

        // Compute cov
        cov_proj_pix[i] = (LinearAlgebra<double>::Dot(pix_value_no_mean.rows,
                                                      reinterpret_cast<const double*>(proj.data),
                                                      1,
                                                      pix_ptr,
                                                      pix_value.cols)
                           / static_cast<double>(proj.cols));
      }
      // Find maximum correlation in the pool of feature
      // Size of n_candidate_feature^2
      double max_corr = -1.0;
      int max_pixel_idx_1 = -1;
      int max_pixel_idx_2 = -1;
      for (int j = 0; j < n_candidate_feature; ++j) {
        for (int k = 0; k < n_candidate_feature; ++k) {
          // Compute correlation
          double den = std_proj * std::sqrt(cov_pix_val.at<double>(j, j) +
                                            cov_pix_val.at<double>(k, k) -
                                            2.0 * cov_pix_val.at<double>(j, k));
          if (std::abs(den) < 1e-10) {
            continue;
          }
          // Pair already taken ?
          bool selected = false;
          for (int p = 0; p < nff; ++p) {
            if ((j == selected_idx.at<int>(p, 0) &&
                 k == selected_idx.at<int>(p, 1)) ||
                (k == selected_idx.at<int>(p, 0) &&
                 j == selected_idx.at<int>(p, 1))) {
              selected = true;
                  break;
            }
          }
          if (selected) {
            continue;
          }
          double corr = (cov_proj_pix[j] - cov_proj_pix[k]) / den;
          if (std::abs(corr) > max_corr) {
            max_corr = std::abs(corr);
            max_pixel_idx_1 = j;
            max_pixel_idx_2 = k;
          }
        }
      }
      // Save selection
      selected_idx.at<int>(nff, 0) = max_pixel_idx_1;
      selected_idx.at<int>(nff, 1) = max_pixel_idx_2;
      select_idx_feat_it->at(nff * 2) = candidate_indexed_features_[max_pixel_idx_1];
      select_idx_feat_it->at(nff * 2 + 1) = candidate_indexed_features_[max_pixel_idx_2];
    }
    // Copy data
    cv::Mat data_training = cv::Mat(n_sample, n_fern_feature, CV_64FC1);
    std::vector<double> threshold_upper_bound;
    for (int i = 0; i < n_fern_feature; ++i) {
      int idx1 = selected_idx.at<int>(i, 0);
      int idx2 = selected_idx.at<int>(i, 1);
      assert(idx1 != -1 && idx2 != -1);
      data_training.col(i) = pix_value.col(idx1) - pix_value.col(idx2);

      double minVal;
      double maxVal;
      cv::minMaxIdx(data_training.col(i), &minVal, &maxVal);
      threshold_upper_bound.push_back(std::max(std::abs(minVal), maxVal));
    }

    // Train
    cv::Mat shape_update;
    fern_trainer_[rf] = new LTS5::RandomFernTrainer();
    fern_trainer_[rf]->Train(data_training,
                             regression_delta,
                             threshold_upper_bound,
                             shrinkage,
                             &shape_update);

    // DEBUG
    if (rf == 0 || rf == (n_random_fern - 1)) {
      std::cout << "\t\t\treg target : " << cv::norm(regression_delta) << std::endl;
    }
    // Update prediction
    *prediction = *prediction + shape_update;
    // Update regression target
    regression_delta = regression_delta - shape_update;
  }
  // Transform back prediction into image reference
  std::cout << "\t\tConvert prediction back into image space" << std::endl;
  cv::Mat shape;
  cv::Mat shape_img;
  for (int s = 0; s < n_sample; ++s) {
    // Get shape
    shape = prediction->row(s).reshape(2, n_pts);
    // transform
    cv::transform(shape, shape_img, inv_transform[s]);
    // Rehshape
    shape_img = shape_img.reshape(1, 1);
    shape_img.copyTo(prediction->row(s));
  }
}

/*
 *  @name Save
 *  @fn int Save(std::string& filename) const
 *  @brief  Save Random fern cascade into a given file
 *  @param[in]  out_stream  File where to dump object
 *  @retrun -1 if error, 0 otherwise
 */
int CascadedRandomFernTrainer::Save(std::string& filename) const {
  int error = -1;
  size_t pos = filename.rfind(".");
  std::string fname;
  if (pos != std::string::npos) {
    fname = filename.substr(0, pos) + ".bin";
  } else {
    fname = filename;
  }
  // Open stream
  std::ofstream out_stream(fname.c_str(), std::ios_base::binary);
  if (out_stream.is_open()) {
    // Write
    error = this->Write(out_stream);
    out_stream.close();
  }
  return error;
}

/*
 *  @name Write
 *  @fn int Write(std::ostream& out_stream) const
 *  @brief  Write Random fern cascade into a given stream
 *  @param[in]  out_stream  Stream where to dump object
 *  @retrun -1 if error, 0 otherwise
 */
int CascadedRandomFernTrainer::Write(std::ostream& out_stream) const {
  int error = -1;
  assert(indexed_features_.size() == fern_trainer_.size());
  if (out_stream.good()) {
    // Compute object size
    const int obj_type = static_cast<int>(LTS5::HeaderObjectType::kCascadedRandomFern);
    const int obj_size = this->ComputeObjectSize();
    const int n_fern = static_cast<int>(fern_trainer_.size());
    const int n_feat_fern = static_cast<int>(indexed_features_[0].size());
    // Write header
    out_stream.write(reinterpret_cast<const char*>(&obj_type),
                     sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size),
                     sizeof(obj_size));
    // Write indexed features
    out_stream.write(reinterpret_cast<const char*>(&n_fern), sizeof(n_fern));
    out_stream.write(reinterpret_cast<const char*>(&n_feat_fern),
                     sizeof(n_feat_fern));
    error = out_stream.good() ? 0 : -1;
    for (int i = 0; i < n_fern; ++i) {
      for (int k = 0; k < n_feat_fern; ++k) {
        error |= indexed_features_[i][k].Write(out_stream);
      }
    }
    // Dump fern
    for (int i = 0; i < n_fern; ++i) {
      error |= fern_trainer_[i]->Write(out_stream);
    }
  }
  return error;
}

#pragma mark -
#pragma mark Private

/*
 *  @name ComputeObjectSize
 *  @fn int ComputeObjectSize() const
 *  @brief  Compute the size (in byte) of memory used by the object
 */
int CascadedRandomFernTrainer::ComputeObjectSize() const {
  // Feature selection
  int size = 2 * sizeof(int);   // n_fern + n_feat_fern
  int elemSize = indexed_features_[0][0].Size();
  const int n_fern = static_cast<int>(indexed_features_.size());
  const int n_feat_fern = static_cast<int>(indexed_features_[0].size());
  size += n_fern * n_feat_fern * elemSize;
  // Fern
  for (int i = 0; i < n_fern; ++i) {
    size += 2 * sizeof(int);    // Header + size
    size += fern_trainer_[i]->ComputeObjectSize();
  }
  return size;
}
}  // namespace LTS5
