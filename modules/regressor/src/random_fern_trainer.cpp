/**
 *  @file   random_fern_trainer.hpp
 *  LTS5-Lib
 *
 *  Created by Christophe Ecabert on 28/10/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <fstream>
#include <random>
#include <chrono>

#include "lts5/utils/object_type.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/regressor/random_fern_trainer.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization
/*
 *  @name RandomFernTrainer
 *  @fn RandomFernTrainer(void)
 *  @brief  Constructor
 */
RandomFernTrainer::RandomFernTrainer(void) :
  threshold_(0), bins_regression_(0) {}

/*
 *  @name ~RandomFernTrainer
 *  @fn ~RandomFernTrainer(void)
 *  @brief  Destructor
 */
RandomFernTrainer::~RandomFernTrainer(void) {}

#pragma mark -
#pragma mark Train

/*
 *  @name Train
 *  @fn void Train(const cv::Mat& features,
                   const cv::Mat& target,
                   const double max_threshold,
                   const double shrinkage)
 *  @brief  Train one random fern for a given set of features
 *  @param[in]  features        Features stored by row [Nsample x Nfeat]
 *  @param[in]  target          Value to predict stored by row [Nsample x k]
 *  @param[in]  max_threslold   Upper bound for random threshold selection
 *  @param[in]  shrinkage       Shrinkage parameters
 *  @param[out] prediction      Shape update learnt [Nsample x k]
 */
void RandomFernTrainer::Train(const cv::Mat& features,
                              const cv::Mat& target,
                              const std::vector<double>& max_threshold,
                              const double shrinkage,
                              cv::Mat* prediction) {
  // Sanity check
  assert(features.rows == target.rows);
  assert(features.type() == target.type() && features.type() == CV_64FC1);

  // Define random threshold
  const int n_feature = features.cols;
  threshold_.resize(n_feature);
  bins_regression_.resize(0x01 << n_feature);
  unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now()
                                        .time_since_epoch()
                                        .count());
  std::mt19937_64 generator(seed);
  for (int t = 0; t < n_feature ; ++t) {
    std::uniform_real_distribution<double> dist(-0.2 * max_threshold[t],
                                                0.2 * max_threshold[t]);
    threshold_[t] = dist(generator);
  }

  // Determine bin of each target
  const int n_bin = 0x01 << n_feature;
  std::vector<std::vector<int>> shape_idx_in_bin; // Idx of shape in bins
  shape_idx_in_bin.resize(n_bin, std::vector<int>(0));
  const int n_target = features.rows;
  for (int t = 0; t < n_target; ++t) {
    int idx = 0;
    for (int f = 0; f < n_feature; ++f) {
      if (features.at<double>(t, f) >= threshold_[f]) {
        idx += 0x01 << f;
      }
    }
    shape_idx_in_bin[idx].push_back(t);
  }

  // Compute regression out as mean of target into a specific bin
  prediction->create(n_target, target.cols, features.type());
  for (int b = 0; b < n_bin; ++b) {
    auto idx_it = shape_idx_in_bin[b].begin();
    auto it_end = shape_idx_in_bin[b].end();
    const int bin_size = static_cast<int>(shape_idx_in_bin[b].size());
    cv::Mat acc_target = cv::Mat::zeros(1, target.cols, CV_64FC1);
    for (; idx_it != it_end; ++idx_it) {
      // Accumulate
      LTS5::LinearAlgebra<double>::Axpy(target.row(*idx_it), 1.0, &acc_target);
    }
    // Empty bin ?
    if (bin_size == 0) {
      // Empty update
      acc_target.copyTo(bins_regression_[b]);
      continue;
    }
    // Average
    double shrink = 1.0 / ((1.0 + (shrinkage / bin_size)) * bin_size);
    assert((1.0 / ((1.0 + (shrinkage / bin_size)))) <= 1.0);
    acc_target *= shrink;
    acc_target.copyTo(bins_regression_[b]);
    // Update prediction for every shape in the current bin
    for (int i = 0; i < bin_size; ++i) {
      int idx = shape_idx_in_bin[b][i];
      acc_target.copyTo(prediction->row(idx));
    }
  }
}

/*
 *  @name Save
 *  @fn int Save(const std::string& filename) const
 *  @brief  Save random fern into binary file
 #  @return -1 if error, 0 otherwise
 */
int RandomFernTrainer::Save(const std::string& filename) const {
  int error = -1;
  size_t pos = filename.rfind(".");
  std::string name;
  if (pos == std::string::npos) {
    // Remove extension
    name = filename.substr(0, pos) + ".bin";
  } else {
    name = filename + ".bin";
  }
  // Open stream
  std::ofstream out(name, std::ios_base::binary);
  if (out.is_open()) {
    // Save
    error = this->Write(out);
    // Close
    out.close();
  }
  return error;
}

/*
 *  @name Write
 *  @fn int Write(std::ostream& out_stream) const
 *  @brief  Write random fern into binary stream
 #  @return -1 if error, 0 otherwise
 */
int RandomFernTrainer::Write(std::ostream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Everything is fine, start to write header
    int obj_type = static_cast<int>(LTS5::HeaderObjectType::kRandomFern);
    int obj_size = this->ComputeObjectSize();
    out_stream.write(reinterpret_cast<const char*>(&obj_type),
                     sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size),
                     sizeof(obj_size));
    // #Threshold
    int n = static_cast<int>(threshold_.size());
    out_stream.write(reinterpret_cast<const char*>(&n), sizeof(n));
    // Data thresh
    out_stream.write(reinterpret_cast<const char*>(threshold_.data()),
                     n * sizeof(threshold_[0]));
    // #Bins
    n = static_cast<int>(bins_regression_.size());
    out_stream.write(reinterpret_cast<const char*>(&n), sizeof(n));
    // Bins
    error = out_stream.good() ? 0 : -1;
    for (int i = 0; i < n; ++i) {
      error |= LTS5::WriteMatToBin(out_stream, bins_regression_[i]);
    }
  }
  return error;
}

/*
 *  @name ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the size (in byte) of memory used by the object
 */
int RandomFernTrainer::ComputeObjectSize(void) const {
  // Threshold vector size
  int n = static_cast<int>(threshold_.size());
  int size = sizeof(n);
  // Thresholds vector
  size += n * sizeof(threshold_[0]);
  // Number of bins
  n = static_cast<int>(bins_regression_.size());
  size += sizeof(n);
  // Matrix
  for( int i = 0; i < n; ++i) {
    // Matrix header
    size += 3 * sizeof(int);
    // Matrix data
    size += bins_regression_[i].total() * bins_regression_[i].elemSize();
  }
  return size;
}
}  // namespace LTS5
