/**
 *  @file   cascaded_random_fern.cpp
 *  LTS5-Lib
 *
 *  Created by Christophe Ecabert on 29/10/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>

#include "lts5/utils/object_type.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/regressor/cascaded_random_fern.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

using Vec2 = LTS5::Vector2<double>;

/*
 *  @name CascadedRandomFern
 *  @fn
 *  @brief  Constructor
 */
CascadedRandomFern::CascadedRandomFern(void) : index_features_(0), fern_(0) {}

/*
 *  @name CascadedRandomFern
 *  @fn explicit CascadedRandomFern(std::istream& in_stream)
 *  @brief  Constructor
 *  @param[in]  in_stream Stream from where to load
 */
CascadedRandomFern::CascadedRandomFern(std::istream& in_stream) :
index_features_(0), fern_(0) {
  this->Load(in_stream);
}

/*
 *  @name CascadedRandomFern
 *  @fn explicit CascadedRandomFern(const std::string& filename)
 *  @brief  Constructor
 *  @param[in]  filename  File from where to load
 */
CascadedRandomFern::CascadedRandomFern(const std::string& filename) {
  this->Load(filename);
}

/*
 *  @name CascadedRandomFern
 *  @fn explicit CascadedRandomFern(const LTS5::CascadedRandomFern& other)
 *  @brief Copy constructor
 *  @param[in]  other Object to copy from
 */
CascadedRandomFern::CascadedRandomFern(const LTS5::CascadedRandomFern& other) {
  throw std::runtime_error("Not supported yet");
}

/*
 *  @name ~CascadedRandomFern
 *  @fn ~CascadedRandomFern(void)
 *  @brief  Destructor
 */
CascadedRandomFern::~CascadedRandomFern(void) {
  // Clean up stuff
  for (int i = 0; i < index_features_.size(); ++i) {
    delete index_features_[i];
    delete fern_[i];
  }
}

/*
 *  @name Load
 *  @fn int Load(std::istream& in_stream)
 *  @brief  Load cascaded random fern from stream
 *  @param[in]  in_stream Input binary stream
 */
int CascadedRandomFern::Load(std::istream& in_stream) {
  int error = -1;
  if (in_stream.good()) {
    // Read number of fern
    in_stream.read(reinterpret_cast<char*>(&n_fern_), sizeof(n_fern_));
    // Number of feature for each fern
    in_stream.read(reinterpret_cast<char*>(&n_feat_fern_), sizeof(n_feat_fern_));
    error = in_stream.good() ? 0 : -1;
    // Load indexed features
    IndexedFeature feat;
    for (int nf = 0; nf < n_fern_; ++nf) {
      index_features_.push_back(new std::vector<IndexedFeature>(0));
      for (int f = 0; f < n_feat_fern_; ++f) {
        // Read
        error |= feat.Read(in_stream);
        index_features_[nf]->push_back(feat);
      }
    }
    // Devide by 2
    n_feat_fern_ /= 2;
    // Load fern
    for (int nf = 0; nf < n_fern_; ++nf) {
      fern_.push_back(new LTS5::RandomFern());
      error |= LTS5::ScanStreamForObject(in_stream,
                                         HeaderObjectType::kRandomFern);
      error |= fern_[nf]->Load(in_stream);
    }
  }
  return error;
}

/*
 *  @name Load
 *  @fn int Load(const std::string& filename)
 *  @brief  Load cascaded random fern from file
 *  @param[in]  filename  Path to the file
 */
int CascadedRandomFern::Load(const std::string& filename) {
  int error = -1;
  std::ifstream input_stream(filename.c_str(), std::ios_base::binary);
  if (input_stream.is_open()) {
    error = LTS5::ScanStreamForObject(input_stream,
                                      LTS5::HeaderObjectType::kCascadedRandomFern);
    if (!error) {
      error = this->Load(input_stream);
      input_stream.close();
    }
  }
  return error;
}

/*
 *  @name Write
 *  @fn int Write(std::ostream& out_stream) const
 *  @brief  Dump object into a given stream
 *  @param[in]  out_stream  Stream where to write object
 */
int CascadedRandomFern::Write(std::ostream& out_stream) const {
  int error = -1;
  if (out_stream.good()) {
    // Compute object size
    const int obj_type = static_cast<int>(HeaderObjectType::kCascadedRandomFern);
    const int obj_size = this->ComputeObjectSize();
    const int n_fern = static_cast<int>(fern_.size());
    const int n_feat_fern = static_cast<int>(index_features_[0]->size());
    // Write header
    out_stream.write(reinterpret_cast<const char*>(&obj_type),
                     sizeof(obj_type));
    out_stream.write(reinterpret_cast<const char*>(&obj_size),
                     sizeof(obj_size));
    // Write indexed features
    out_stream.write(reinterpret_cast<const char*>(&n_fern), sizeof(n_fern));
    out_stream.write(reinterpret_cast<const char*>(&n_feat_fern),
                     sizeof(n_feat_fern));
    error = out_stream.good() ? 0 : -1;
    for (int i = 0; i < n_fern; ++i) {
      for (int k = 0; k < n_feat_fern; ++k) {
        error |= index_features_[i]->at(k).Write(out_stream);
      }
    }
    // Dump fern
    for (int i = 0; i < n_fern; ++i) {
      error |= fern_[i]->Write(out_stream);
    }
  }
  return error;
}

/*
 *  @name ComputeObjectSize
 *  @fn int ComputeObjectSize(void) const
 *  @brief  Compute the size (in byte) of memory used by the object
 */
int CascadedRandomFern::ComputeObjectSize(void) const {
  // Feature selection
  int size = 2 * sizeof(int);   // n_fern + n_feat_fern
  int elemSize = index_features_[0]->at(0).Size();
  const int n_fern = static_cast<int>(index_features_.size());
  const int n_feat_fern = static_cast<int>(index_features_[0]->size());
  size += n_fern * n_feat_fern * elemSize;
  // Fern
  for (int i = 0; i < n_fern; ++i) {
    size += 2 * sizeof(int);    // Header + size
    size += fern_[i]->ComputeObjectSize();
  }
  return size;
}

#pragma mark -
#pragma mark Operator

/*
 *  @name operator=
 *  @fn LTS5::CascadedRandomFern& operator=(const LTS5::CascadedRandomFern& others);
 *  @brief  copy assignment operator
 *  @return New assigned cascaded random fern regressor
 */
LTS5::CascadedRandomFern& CascadedRandomFern::operator=(const LTS5::CascadedRandomFern& others) {
  if (this != &others) {
    // Do stuff
    throw std::runtime_error("Not implemented yet");
  }
  return *this;
}

#pragma mark -
#pragma mark Predict

void CascadedRandomFern::Predict(const cv::Mat& image,
                                 const cv::Rect& face_bbox,
                                 const cv::Mat& current_shape,
                                 cv::Mat* prediction) {
  // Define some usefull var
  extracted_features_.create(n_feat_fern_, 1, CV_64FC1);
  double* feat_ptr = reinterpret_cast<double*>(extracted_features_.data);
  const double bbox_w = static_cast<double>(face_bbox.width) / 2.0;
  const double bbox_h = static_cast<double>(face_bbox.height) / 2.0;
  const double img_w = static_cast<double>(image.cols - 1);
  const double img_h = static_cast<double>(image.rows - 1);
  // Center shape
  cv::Mat center_shape;
  LTS5::NormalizeShapeXY<double>(current_shape, face_bbox, &center_shape);
  // Align centered shape to mean_shape
  cv::Mat transform;
  LTS5::SimilarityTransformXYXY(mean_shape_, center_shape, &transform);
  const double* t_ptr = reinterpret_cast<const double*>(transform.data);
  const double* shape_ptr = reinterpret_cast<const double*>(current_shape.data);
  // Goes through every fern
  for (int nf = 0; nf < n_fern_; ++nf) {
    // Extract feature for this level
    std::vector<IndexedFeature>* idx = index_features_[nf];
    for (int f = 0; f < n_feat_fern_; ++f) {
      // Get corresponding indexed features
      const IndexedFeature& feat1 = idx->at(2 * f);
      const IndexedFeature& feat2 = idx->at(2 * f + 1);

      // Recover sampling position in the image
      double x = (t_ptr[0] * feat1.delta_x + t_ptr[1] * feat1.delta_y) * bbox_w;
      double y = (t_ptr[3] * feat1.delta_x + t_ptr[4] * feat1.delta_y) * bbox_h;
      x += shape_ptr[feat1.index];
      y += shape_ptr[feat1.index + 1];
      // Check boundaries
      x = std::max(0.0, std::min(x, img_w));
      y = std::max(0.0, std::min(y, img_h));
      // Sample
      double pix_value1 = image.at<unsigned char>(static_cast<int>(y),
                                                  static_cast<int>(x));
      // Recover sampling position in the image
      x = (t_ptr[0] * feat2.delta_x + t_ptr[1] * feat2.delta_y) * bbox_w;
      y = (t_ptr[3] * feat2.delta_x + t_ptr[4] * feat2.delta_y) * bbox_h;
      x += shape_ptr[feat2.index];
      y += shape_ptr[feat2.index + 1];
      // Check boundaries
      x = std::max(0.0, std::min(x, img_w));
      y = std::max(0.0, std::min(y, img_h));
      // Sample
      double pix_value2 = image.at<unsigned char>(static_cast<int>(y),
                                                  static_cast<int>(x));
      // Compute diff
      feat_ptr[f] = pix_value1 - pix_value2;
    }
    // Predict
    fern_[nf]->Predict(extracted_features_, prediction);
  }

  // Apply inverse transform
  const int n_pts = mean_shape_.cols / 2;
  Vec2* update_ptr = reinterpret_cast<Vec2*>(prediction->data);
  for (int p = 0; p < n_pts; ++p) {
    // in
    double dx = update_ptr[p].x_;
    double dy = update_ptr[p].y_;
    // out
    update_ptr[p].x_ = t_ptr[0] * dx + t_ptr[1] * dy;
    update_ptr[p].y_ = t_ptr[3] * dx + t_ptr[4] * dy;
  }
}

}  // namespace LTS5
