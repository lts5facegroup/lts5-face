//
//  test_ssift_descriptor.cpp
//  LTS5-Dev
//
//  Created by Christophe Ecabert on 06/05/15.
//  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
//

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <numeric>
#include <random>
#include <chrono>
#include <cuda_runtime.h>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/gpu/gpu.hpp"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/ssift.hpp"
#include "lts5/cuda/utils/ssift.hpp"
#include "lts5/utils/file_io.hpp"

#define N_TEST 1
#define N_ALLOWED_DIFFERENT_ELEM 10
static cv::Mat s_input_image;

/**
 *  @class  GPUSSiftTest
 *  @brief  Test unit for GPU SSift implementation
 *          Input/Output are CPU container (cv::Mat)
 *  @author Christophe Ecabert
 *  @date   18/05/16
 */
class GPUSSiftTest : public ::testing::TestWithParam<int> {
public:

  GPUSSiftTest(void) : n_pts_(20), key_points_(n_pts_, cv::KeyPoint()) {
    test_instance_ = GetParam();
  }

  void ComputeCPUSSift(void) {
    sift_params_.sift_root = false;
    LTS5::SSift::ComputeDescriptorOpt(s_input_image,
                                      key_points_,
                                      sift_params_,
                                      &cpu_sift_, test_instance_);
  }

  virtual void ComputeGPUSSift(void) {
    // Upload
    cv::gpu::GpuMat input_image(s_input_image);
    LTS5::CUDA::GPUMatrix<float> gpu_sift;
    int  err = LTS5::CUDA::GPUSSift::ComputeDescriptor(input_image,
                                                       key_points_,
                                                       sift_params_,
                                                       &gpu_sift);
    gpu_sift.Download(&gpu_sift_);
    ASSERT_TRUE(err == 0);
  }

  void Compare(bool* same, int* n_diff ) {
    int sz = cpu_sift_.cols * cpu_sift_.rows;
    *same = true;
    *n_diff = 0;
    for (int i = 0; i < sz; ++i) {
      if(std::abs(cpu_sift_.at<float>(i) - gpu_sift_.at<float>(i)) > 1e-4) {
        *same = false;
        *n_diff += 1;
        if (*n_diff < N_ALLOWED_DIFFERENT_ELEM) {
          std::cout << i;
          std::cout << " cpu : " << cpu_sift_.at<float>(i);
          std::cout << " gpu : " << gpu_sift_.at<float>(i) << std::endl;
        }
      }
    }
  }


  /** N sift pts */
  int n_pts_;
  /** Sample location */
  std::vector<cv::KeyPoint> key_points_;
  /** SSift param */
  LTS5::SSift::SSiftParameters sift_params_;
  /** CPU sift */
  cv::Mat cpu_sift_;
  /** GPU sift */
  cv::Mat gpu_sift_;
  /** Test instance */
  int test_instance_;

protected:

  void SetUp(void) {
    // code here will execute just before the test ensues

    // Generate random pts
    unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now()
                                          .time_since_epoch()
                                          .count());
    std::mt19937_64 engine(seed);
    std::uniform_int_distribution<int> dist_w(0, s_input_image.cols - 1);
    std::uniform_int_distribution<int> dist_h(0, s_input_image.rows - 1);
    for (int p = 0; p < n_pts_; ++p) {
      int u = dist_w(engine);
      int v = dist_h(engine);
      key_points_[p].pt.x = static_cast<float>(u);
      key_points_[p].pt.y = static_cast<float>(v);
      key_points_[p].size = 32;
    }
  }

  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
    cudaDeviceReset();
  }
};

TEST(DataTransfer, GPUMatrix) {
  // Generate data
  cv::Mat data(4, 6, CV_32FC1);
  cv::randn(data, 0.f, 2.f);
  // push to gpu
  LTS5::CUDA::GPUMatrix<float> gpu_data;
  gpu_data.Upload(data);
  // download
  cv::Mat download_data;
  gpu_data.Download(&download_data);

  // Compare matrix dimension
  EXPECT_TRUE(data.rows == download_data.rows);
  EXPECT_TRUE(data.cols == download_data.cols);

  // Compare matrix content
  std::vector<float> true_data(reinterpret_cast<float*>(data.data),
                               reinterpret_cast<float*>(data.data) + data.total());
  ASSERT_THAT(true_data,
              ::testing::ElementsAreArray(reinterpret_cast<float*>(download_data.data),
                                          download_data.total()));
}

TEST_P(GPUSSiftTest, ValidationTest) {
  // Compute ground truth
  this->ComputeCPUSSift();
  // Compute GPU
  this->ComputeGPUSSift();
  // Compare
  EXPECT_TRUE(cpu_sift_.rows == gpu_sift_.rows);
  EXPECT_TRUE(cpu_sift_.cols == gpu_sift_.cols);
  bool is_same;
  int n_diff;
  this->Compare(&is_same, &n_diff);
  EXPECT_LE(n_diff, N_ALLOWED_DIFFERENT_ELEM);
}

INSTANTIATE_TEST_CASE_P(GPU, GPUSSiftTest, ::testing::Range(0, N_TEST));

int main(int argc, const char * argv[]) {

  LTS5::CmdLineParser parser;
  parser.AddArgument("-i",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Image path");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Init google test
    ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
    // Get option
    std::string image_path;
    parser.HasArgument("-i", &image_path);

    // Load data
    s_input_image = cv::imread(image_path);
    if (!s_input_image.empty()) {
      // Run test
      err = ::RUN_ALL_TESTS();
    } else {
      err = -1;
      std::cout << "Unable to load image : " << image_path << std::endl;
    }

  } else {
    std::cout << "Unable to parse cmd line" << std::endl;
  }
  return err;
}
