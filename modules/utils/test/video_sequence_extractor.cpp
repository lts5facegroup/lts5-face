/**
 *  @file   video_sequence_extractor.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   30.09.16
 *  Copyright © 2016 Ecabert Christophe. All rights reserved.
 */

#include <iostream>

#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/utils/sys/file_system.hpp"

using StringList = std::vector<std::string>;
using IndexList = std::vector<std::pair<int, int>>;

/**
 *  @name ExtractFrameIndex
 *  @fn void ExtractFrameIndex(const StringList& region, IndexList* region_idx);
 *  @brief  Extract from string the corresponding frame index [start,stop]
 *  @param[in]  region      Text region
 *  @param[in]  fps         Video fps
 *  @param[out] region_idx  Extracted frame index (Start, stop)
 */
void ExtractFrameIndex(const StringList& region, const int fps, IndexList* region_idx);


int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("-v",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Video file to split");
  parser.AddArgument("-r",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Timestamp where to cut : \"mm:ss-mm:ss; ...\"");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Video output filename");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    std::string input_file;
    std::string split_region;
    std::string output_file;
    parser.HasArgument("-v", &input_file);
    parser.HasArgument("-r", &split_region);
    parser.HasArgument("-o", &output_file);
    
    
    // Open video
    cv::VideoCapture cap(input_file);
    if (cap.isOpened()) {
      // Get fps + dimension
      int fps = static_cast<int>(std::round(cap.get(cv::CAP_PROP_FPS)));
      if (!fps) {
        fps = 30;
      }
      int width = static_cast<int>(std::round(cap.get(cv::CAP_PROP_FRAME_WIDTH)));
      int height = static_cast<int>(std::round(cap.get(cv::CAP_PROP_FRAME_HEIGHT)));
      int length = static_cast<int>(std::round(cap.get(cv::CAP_PROP_FRAME_COUNT)));
      std::cout << "Video duration : " << length/fps << " sec" << std::endl;
      // Extract region index
      IndexList frame_idx;
      std::vector<std::string> split;
      LTS5::SplitString(split_region, ";", &split);
      
      // Process string region
      ExtractFrameIndex(split, fps, &frame_idx);
      
      // Loop
      std::string dir, file, ext;
      LTS5::ExtractDirectory(output_file, &dir, &file, &ext);
      auto* fs = LTS5::GetDefaultFileSystem();
      if (!fs->FileExist(dir).Good()) {
        fs->CreateDirRecursively(dir);
      }
      int cnt = 0;
      for (int i = 0; i < frame_idx.size(); ++i) {
        // Log
        std::cout << "Process region : " << split[i] << std::endl;
        // Define output
        std::string output_name = (dir + file + "_" + std::to_string(i) +
                                   "." + ext);
        cv::VideoWriter writer(output_name,
                               cv::VideoWriter::fourcc('m', 'p', '4', 'v'),
                               static_cast<double>(fps),
                               cv::Size(width, height));
        if (writer.isOpened()) {
          // Process
          const auto& idx = frame_idx[i];
          cv::Mat frame;
          while (cap.grab() && cnt < idx.second) {
            if (cnt >= idx.first) {
              // Read frame
              cap.retrieve(frame);
              // Write
              writer.write(frame);
            }
            cnt++;
          }
          // Close
          writer.release();
          if (cnt == idx.second) {
            std::cout << "\tSuccessful extraction" << std::endl;
          } else {
            err |= -1;
            std::cout << "\tERROR" << std::endl;
          }
        } else {
          err |= -1;
          std::cout << "Error, can not open file : " << output_name << std::endl;
        }
      }
    } else {
      err = -1;
      std::cout << "Can not open video file : " << input_file << std::endl;
    }
  } else {
    parser.PrintHelp();
    std::cout << "Unable to parse commandd line" << std::endl;
  }
}


/*
 *  @name ExtractFrameIndex
 *  @fn void ExtractFrameIndex(const StringList& region, IndexList* region_idx);
 *  @brief  Extract from string the corresponding frame index [start,stop]
 *  @param[in]  region      Text region
 *  @param[out] region_idx  Extracted frame index (Start, stop)
 */
void ExtractFrameIndex(const StringList& region,
                       const int fps,
                       IndexList* region_idx) {
  // Setup output
  region_idx->resize(region.size());
  // Loop
  for (int i = 0; i < region.size(); ++i) {
    std::vector<std::string> time;
    LTS5::SplitString(region[i], "-", &time);
    // Begin
    std::vector<std::string> number;
    LTS5::SplitString(time[0], ":", &number);
    int start_idx = (std::atoi(number[0].c_str()) * 60
                     + std::atoi(number[1].c_str())) * fps;
    
    // End
    number.clear();
    LTS5::SplitString(time[1], ":", &number);
    int stop_idx = (std::atoi(number[0].c_str()) * 60
                    + std::atoi(number[1].c_str())) * fps;
    
    // Put resut
    region_idx->at(i) = std::make_pair(start_idx, stop_idx);
  }
}










