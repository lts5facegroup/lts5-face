/**
 *  @file   test_logger.cpp
 *  @brief  Logger test unit
 *
 *  @author Christophe Ecabert
 *  @date   22/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <sstream>

#include "gtest/gtest.h"

#include "lts5/utils/logger.hpp"
#include "lts5/utils/string_util.hpp"

#ifndef WIN32
#define ERROR_STR " \033[31mERROR\033[0m "
#define WARNING_STR " \033[33mWARNING\033[0m "
#define INFO_STR " \033[32mINFO\033[0m "
#define DEBUG_STR " \033[37mDEBUG\033[0m "
#define DEBUG1_STR " \033[37mDEBUG1\033[0m "
#define DEBUG2_STR " \033[37mDEBUG2\033[0m "
#else
#define ERROR_STR " ERROR "
#define WARNING_STR " WARNING "
#define INFO_STR " INFO "
#define DEBUG_STR " DEBUG "
#define DEBUG1_STR " DEBUG1 "
#define DEBUG2_STR " DEBUG2 "
#endif

static std::ostringstream stream;
auto logger = LTS5::Logger::Instance(stream);

TEST(LoggerDebug, Logger) {
  // Reset stringstream
  stream.str("");
  // Log
  LTS5_LOG_DEBUG("This is a debug entry");
  // Check for output
  std::vector<std::string> log_part;
  LTS5::SplitString(stream.str(), ":", &log_part);
  // Check line number
  EXPECT_EQ(std::atoi(log_part[1].c_str()), __LINE__ - 5);
  // Check level indicator
  EXPECT_EQ(log_part[2], DEBUG_STR);
  // Check message
  EXPECT_EQ(log_part[3], " This is a debug entry\n");
}

TEST(LoggerInfo, Logger) {
  // Reset stringstream
  stream.str("");
  // Log
  LTS5_LOG_INFO("This is an info entry");
  // Check for output
  std::vector<std::string> log_part;
  LTS5::SplitString(stream.str(), ":", &log_part);
  // Check line number
  EXPECT_EQ(std::atoi(log_part[1].c_str()), __LINE__ - 5);
  // Check level indicator
  EXPECT_EQ(log_part[2], INFO_STR);
  // Check message
  EXPECT_EQ(log_part[3], " This is an info entry\n");
}

TEST(LoggerWarning, Logger) {
  // Reset stringstream
  stream.str("");
  // Log
  LTS5_LOG_WARNING("This is a warning entry");
  // Check for output
  std::vector<std::string> log_part;
  LTS5::SplitString(stream.str(), ":", &log_part);
  // Check line number
  EXPECT_EQ(std::atoi(log_part[1].c_str()), __LINE__ - 5);
  // Check level indicator
  EXPECT_EQ(log_part[2], WARNING_STR);
  // Check message
  EXPECT_EQ(log_part[3], " This is a warning entry\n");
}

TEST(LoggerError, Logger) {
  // Reset stringstream
  stream.str("");
  // Log
  LTS5_LOG_ERROR("This is an error entry with value " << 42);
  // Check for output
  std::vector<std::string> log_part;
  LTS5::SplitString(stream.str(), ":", &log_part);
  // Check line number
  EXPECT_EQ(std::atoi(log_part[1].c_str()), __LINE__ - 5);
  // Check level indicator
  EXPECT_EQ(log_part[2], ERROR_STR);
  // Check message
  EXPECT_EQ(log_part[3], " This is an error entry with value 42\n");
}

TEST(LoggerDebugLvl1, Logger) {
  // Reset stringstream
  stream.str("");
  LTS5::Logger::Instance().set_log_level(LTS5::Logger::Level::kDebug2);
  // Log
  LTS5_LOG_DEBUG1("This is a level 1 debug entry");
  // Check for output
  std::vector<std::string> log_part;
  LTS5::SplitString(stream.str(), ":", &log_part);
  // Check line number
  EXPECT_EQ(std::atoi(log_part[1].c_str()), __LINE__ - 5);
  // Check level indicator
  EXPECT_EQ(log_part[2], DEBUG1_STR);
  // Check message
  EXPECT_EQ(log_part[3], " \tThis is a level 1 debug entry\n");
}

TEST(LoggerDebugLvl2, Logger) {
  // Reset stringstream
  stream.str("");
  LTS5::Logger::Instance().set_log_level(LTS5::Logger::Level::kDebug2);
  // Log
  LTS5_LOG_DEBUG2("This is a level 2 debug entry");
  // Check for output
  std::vector<std::string> log_part;
  LTS5::SplitString(stream.str(), ":", &log_part);
  // Check line number
  EXPECT_EQ(std::atoi(log_part[1].c_str()), __LINE__ - 5);
  // Check level indicator
  EXPECT_EQ(log_part[2], DEBUG2_STR);
  // Check message
  EXPECT_EQ(log_part[3], " \t\tThis is a level 2 debug entry\n");
}

TEST(LoggerDisable, Logger) {
  // Reset stringstream
  stream.str("");
  LTS5::Logger::Instance().Disable();
  // Log
  LTS5_LOG_INFO("This should not be logged");
  // Check for output
  EXPECT_TRUE(stream.str().empty());
}

TEST(LoggerFilter, Logger) {
  // Reset stringstream
  stream.str("");
  LTS5::Logger::Instance().set_log_level(LTS5::Logger::Level::kDebug);
  LTS5::Logger::Instance().Enable();  // need to be re-enable (singleton)
  // Log
  LTS5_LOG_DEBUG1("This should not be logged");
  // Check for output
  EXPECT_TRUE(stream.str().empty());
  // Log
  LTS5::Logger::Instance().set_log_level(LTS5::Logger::Level::kDebug1);
  LTS5_LOG_DEBUG1("This is level 1");
  LTS5_LOG_DEBUG2("This should not be logged");
  // Check for output
  std::vector<std::string> log_part;
  LTS5::SplitString(stream.str(), ":", &log_part);
  // Check line number
  EXPECT_EQ(std::atoi(log_part[1].c_str()), __LINE__ - 6);
  // Check level indicator
  EXPECT_EQ(log_part[2], DEBUG1_STR);
  // Check message
  EXPECT_EQ(log_part[3], " \tThis is level 1\n");
}

int main(int argc, char* argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  // Run unit test
  return RUN_ALL_TESTS();
}