/**
 *  @file   test_fixed_point.cpp
 *  @brief  Fixed point algebra unit test
 *
 *  @author Christophe Ecabert
 *  @date   29/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <chrono>

#include "gtest/gtest.h"

#include "lts5/utils/math/fixed_point.hpp"

TEST(Constructor, FixedPoint) {
  using Fp = LTS5::FPoint<int, 16>;

  Fp fp0(0);
  EXPECT_EQ(static_cast<int>(fp0), 0);
  // Signed char
  Fp fp1(static_cast<signed char>(-128));
  EXPECT_EQ(static_cast<signed char>(fp1), -128);
  // Unsigned char
  Fp fp2(static_cast<unsigned char>(127));
  EXPECT_EQ(static_cast<unsigned char>(fp2), 127);
  // Short
  Fp fp3(static_cast<short>(-32768));
  EXPECT_EQ(static_cast<short>(fp3), -32768);
  // Unsigned short
  Fp fp4(static_cast<unsigned short>(32767));
  EXPECT_EQ(static_cast<unsigned short>(fp4), 32767);
  // Int
  Fp fp5(-32768);
  EXPECT_EQ(static_cast<int>(fp5), -32768);
  // Unsigned int
  Fp fp6(static_cast<unsigned short>(32767));
  EXPECT_EQ(static_cast<int>(fp6), 32767);
  // Float
  Fp fp7(1.5f);
  EXPECT_FLOAT_EQ(static_cast<float>(fp7), 1.5f);
  Fp fp8(-1.5f);
  EXPECT_FLOAT_EQ(static_cast<float>(fp8), -1.5f);
  // Double
  Fp fp9(1.5);
  EXPECT_DOUBLE_EQ(static_cast<double>(fp9), 1.5);
  Fp fp10(-1.5);
  EXPECT_DOUBLE_EQ(static_cast<double>(fp10), -1.5);
}

TEST(CopyConstructor, FixedPoint) {
  using Fp = LTS5::FPoint<int, 16>;

  Fp fp0(0);
  Fp fp0c = fp0;
  EXPECT_EQ(static_cast<int>(fp0c), 0);
  // Signed char
  Fp fp1(static_cast<signed char>(-128));
  Fp fp1c = fp1;
  EXPECT_EQ(static_cast<signed char>(fp1c), -128);
  // Unsigned char
  Fp fp2(static_cast<unsigned char>(127));
  Fp fp2c = fp2;
  EXPECT_EQ(static_cast<unsigned char>(fp2c), 127);
  // Short
  Fp fp3(static_cast<short>(-32768));
  Fp fp3c = fp3;
  EXPECT_EQ(static_cast<short>(fp3c), -32768);
  // Unsigned short
  Fp fp4(static_cast<unsigned short>(32767));
  Fp fp4c = fp4;
  EXPECT_EQ(static_cast<unsigned short>(fp4c), 32767);
  // Int
  Fp fp5(-32768);
  Fp fp5c = fp5;
  EXPECT_EQ(static_cast<int>(fp5c), -32768);
  // Unsigned int
  Fp fp6(static_cast<unsigned int>(32767));
  Fp fp6c = fp6;
  EXPECT_EQ(static_cast<unsigned int>(fp6c), 32767);
  // Float
  Fp fp7(1.5f);
  Fp fp7c = fp7;
  EXPECT_FLOAT_EQ(static_cast<float>(fp7c), 1.5f);
  Fp fp8(-1.5f);
  Fp fp8c = fp8;
  EXPECT_FLOAT_EQ(static_cast<float>(fp8c), -1.5f);
  // Double
  Fp fp9(1.5);
  Fp fp9c = fp9;
  EXPECT_DOUBLE_EQ(static_cast<double>(fp9c), 1.5);
  Fp fp10(-1.5);
  Fp fp10c = fp10;
  EXPECT_DOUBLE_EQ(static_cast<double>(fp10c), -1.5);
}

TEST(Comparison, FixedPoint) {
  using Fp = LTS5::FPoint<int, 16>;

  Fp fp1(static_cast<signed char>(-128));
  Fp fp2(static_cast<unsigned char>(127));
  Fp fp3(static_cast<signed short>(-32768));
  Fp fp4(static_cast<unsigned short>(32767));

  EXPECT_TRUE(fp1 < fp2);
  EXPECT_TRUE(fp2 > fp1);
  EXPECT_TRUE(fp3 <= fp4);
  EXPECT_TRUE(fp4 >= fp3);
}

TEST(Equality, FixedPoint) {
  using Fp = LTS5::FPoint<int, 16>;

  Fp fp0(0);
  Fp fp1(static_cast<signed char>(-128));
  Fp fp2(static_cast<unsigned char>(127));
  Fp fp3(static_cast<signed short>(-32768));
  Fp fp4(static_cast<unsigned short>(32767));
  Fp fp5(static_cast<signed int>(-32768));
  Fp fp6(static_cast<unsigned int>(32767));
  Fp fp7(-1.5f);
  Fp fp8(1.5f);
  Fp fp9(-1.5);
  Fp fp10(1.5);

  EXPECT_TRUE(fp3 == fp5);
  EXPECT_TRUE(fp4 == fp6);
  EXPECT_TRUE(fp7 == fp9);
  EXPECT_TRUE(fp8 == fp10);

  EXPECT_TRUE(fp1 != fp2);
  EXPECT_TRUE(fp3 != fp4);
  EXPECT_TRUE(fp5 != fp6);
  EXPECT_TRUE(fp7 != fp8);
  EXPECT_TRUE(fp9 != fp10);
}

TEST(UnaryMinus, FixedPoint) {
  using Fp = LTS5::FPoint<int, 16>;

  Fp fp1(static_cast<signed char>(-127));
  Fp fp2(static_cast<unsigned char>(127));
  Fp fp3(static_cast<signed short>(-32767));
  Fp fp4(static_cast<unsigned short>(32767));
  Fp fp5(static_cast<signed int>(-32767));
  Fp fp6(static_cast<unsigned int>(32767));
  Fp fp7(-1.5f);
  Fp fp8(1.5f);
  Fp fp9(-1.5);
  Fp fp10(1.5);

  EXPECT_TRUE(fp1 == -fp2);
  EXPECT_TRUE(-fp1 == fp2);
  EXPECT_TRUE(fp3 == -fp4);
  EXPECT_TRUE(-fp3 == fp4);
  EXPECT_TRUE(fp5 == -fp6);
  EXPECT_TRUE(-fp5 == fp6);
  EXPECT_TRUE(fp7 == -fp8);
  EXPECT_TRUE(-fp7 == fp8);
  EXPECT_TRUE(fp9 == -fp10);
  EXPECT_TRUE(-fp9 == fp10);
}

TEST(FunctionAbs, FixedPoint) {
  using Fp = LTS5::FPoint<int, 16>;

  // Signed char
  Fp fp1(static_cast<signed char>(-125));
  Fp fp1a = Fp::abs(fp1);
  EXPECT_EQ(static_cast<signed char>(fp1a), 125);
  // Unsigned char
  Fp fp2(static_cast<unsigned char>(125));
  Fp fp2a = Fp::abs(fp2);
  EXPECT_EQ(static_cast<signed char>(fp2a), 125);
  // Short
  Fp fp3(static_cast<short>(-32760));
  Fp fp3a = Fp::abs(fp3);
  EXPECT_EQ(static_cast<short>(fp3a), 32760);
  // Unsigned short
  Fp fp4(static_cast<unsigned short>(32760));
  Fp fp4a = Fp::abs(fp4);
  EXPECT_EQ(static_cast<unsigned short>(fp4a), 32760);
  // Int
  Fp fp5(-32760);
  Fp fp5a = Fp::abs(fp5);
  EXPECT_EQ(static_cast<int>(fp5a), 32760);
  // Unsigned int
  Fp fp6(static_cast<unsigned int>(32767));
  Fp fp6a = Fp::abs(fp6);
  EXPECT_EQ(static_cast<unsigned int>(fp6a), 32767);
  // Float
  Fp fp7(1.5f);
  Fp fp7a = Fp::abs(fp7);
  EXPECT_FLOAT_EQ(static_cast<float>(fp7a), 1.5f);
  Fp fp8(-1.5f);
  Fp fp8a = Fp::abs(fp8);
  EXPECT_FLOAT_EQ(static_cast<float>(fp8a), 1.5f);
  // Double
  Fp fp9(1.5);
  Fp fp9a = Fp::abs(fp9);
  EXPECT_DOUBLE_EQ(static_cast<double>(fp9a), 1.5);
  Fp fp10(-1.5);
  Fp fp10a = Fp::abs(fp10);
  EXPECT_DOUBLE_EQ(static_cast<double>(fp10a), 1.5);
}

TEST(FunctionCeil, FixedPoint) {
  using Fp = LTS5::FPoint<int, 16>;

  // Signed char
  Fp fp1(static_cast<signed char>(-125));
  Fp fp1c = Fp::ceil(fp1);
  EXPECT_EQ(static_cast<signed char>(fp1c), -125);
  // Unsigned char
  Fp fp2(static_cast<unsigned char>(125));
  Fp fp2c = Fp::ceil(fp2);
  EXPECT_EQ(static_cast<signed char>(fp2c), 125);
  // Short
  Fp fp3(static_cast<short>(-32760));
  Fp fp3c = Fp::ceil(fp3);
  EXPECT_EQ(static_cast<short>(fp3c), -32760);
  // Unsigned short
  Fp fp4(static_cast<unsigned short>(32760));
  Fp fp4c = Fp::ceil(fp4);
  EXPECT_EQ(static_cast<unsigned short>(fp4c), 32760);
  // Int
  Fp fp5(-32760);
  Fp fp5c = Fp::ceil(fp5);
  EXPECT_EQ(static_cast<int>(fp5c), -32760);
  // Unsigned int
  Fp fp6(static_cast<unsigned int>(32767));
  Fp fp6c = Fp::ceil(fp6);
  EXPECT_EQ(static_cast<unsigned int>(fp6c), 32767);
  // Float
  Fp fp7(1.5f);
  Fp fp7c = Fp::ceil(fp7);
  EXPECT_FLOAT_EQ(static_cast<float>(fp7c), 2.f);
  Fp fp8(-1.5f);
  Fp fp8c = Fp::ceil(fp8);
  EXPECT_FLOAT_EQ(static_cast<float>(fp8c), -1.f);
  // Double
  Fp fp9(1.5);
  Fp fp9c = Fp::ceil(fp9);
  EXPECT_DOUBLE_EQ(static_cast<double>(fp9c), 2.0);
  Fp fp10(-1.5);
  Fp fp10c = Fp::ceil(fp10);
  EXPECT_DOUBLE_EQ(static_cast<double>(fp10c), -1.0);
}

TEST(FunctionFloor, FixedPoint) {
  using Fp = LTS5::FPoint<int, 16>;

  // Signed char
  Fp fp1(static_cast<signed char>(-125));
  Fp fp1f = Fp::floor(fp1);
  EXPECT_EQ(static_cast<signed char>(fp1f), -125);
  // Unsigned char
  Fp fp2(static_cast<unsigned char>(125));
  Fp fp2f = Fp::floor(fp2);
  EXPECT_EQ(static_cast<signed char>(fp2f), 125);
  // Short
  Fp fp3(static_cast<short>(-32760));
  Fp fp3f = Fp::floor(fp3);
  EXPECT_EQ(static_cast<short>(fp3f), -32760);
  // Unsigned short
  Fp fp4(static_cast<unsigned short>(32760));
  Fp fp4f = Fp::floor(fp4);
  EXPECT_EQ(static_cast<unsigned short>(fp4f), 32760);
  // Int
  Fp fp5(-32760);
  Fp fp5f = Fp::floor(fp5);
  EXPECT_EQ(static_cast<int>(fp5f), -32760);
  // Unsigned int
  Fp fp6(static_cast<unsigned int>(32767));
  Fp fp6f = Fp::floor(fp6);
  EXPECT_EQ(static_cast<unsigned int>(fp6f), 32767);
  // Float
  Fp fp7(1.5f);
  Fp fp7f = Fp::floor(fp7);
  EXPECT_FLOAT_EQ(static_cast<float>(fp7f), 1.f);
  Fp fp8(-1.5f);
  Fp fp8f = Fp::floor(fp8);
  EXPECT_FLOAT_EQ(static_cast<float>(fp8f), -2.f);
  // Double
  Fp fp9(1.5);
  Fp fp9f = Fp::floor(fp9);
  EXPECT_DOUBLE_EQ(static_cast<double>(fp9f), 1.0);
  Fp fp10(-1.5);
  Fp fp10f = Fp::floor(fp10);
  EXPECT_DOUBLE_EQ(static_cast<double>(fp10f), -2.0);
}

TEST(FunctionFmod, FixedPoint) {
  using Fp = LTS5::FPoint<int, 16>;

  // Signed char
  Fp fp1x(static_cast<signed char>(-19));
  Fp fp1y(static_cast<signed char>(-6));
  Fp fp1m = Fp::fmod(fp1x, fp1y);
  EXPECT_EQ(static_cast<signed char>(fp1m), -1);
  // Unsigned char
  Fp fp2x(static_cast<unsigned char>(175));
  Fp fp2y(static_cast<unsigned char>(69));
  Fp fp2m = Fp::fmod(fp2x, fp2y);
  EXPECT_EQ(static_cast<signed char>(fp2m) , 37);
  // Short
  Fp fp3x(static_cast<short>(-10000));
  Fp fp3y(static_cast<short>(-97));
  Fp fp3m = Fp::fmod(fp3x, fp3y);
  EXPECT_EQ(static_cast<short>(fp3m) , -9);
  // Unsigned short
  Fp fp4x(static_cast<unsigned short>(32760));
  Fp fp4y(static_cast<unsigned short>(15));
  Fp fp4m = Fp::fmod(fp4x, fp4y);
  EXPECT_EQ(static_cast<unsigned short>(fp4m) , 0);
  // Int
  Fp fp5x(-32760);
  Fp fp5y(-115);
  Fp fp5m = Fp::fmod(fp5x, fp5y);
  EXPECT_EQ(static_cast<int>(fp5m) , -100);
  // Unsigned int
  Fp fp6x(static_cast<unsigned int>(32767));
  Fp fp6y(static_cast<unsigned int>(63));
  Fp fp6m = Fp::fmod(fp6x, fp6y);
  EXPECT_EQ(static_cast<unsigned int>(fp6m) , 7);
  // Float
  Fp fp7x(4.5f);
  Fp fp7y(1.5f);
  Fp fp7m = Fp::fmod(fp7x, fp7y);
  EXPECT_FLOAT_EQ(static_cast<float>(fp7m), 0.f);
  Fp fp8x(-4.5f);
  Fp fp8y(-1.f);
  Fp fp8m = Fp::fmod(fp8x, fp8y);
  EXPECT_FLOAT_EQ(static_cast<float>(fp8m), -0.5f);
  // Double
  Fp fp9x(4.5);
  Fp fp9y(1.5);
  Fp fp9m = Fp::fmod(fp9x, fp9y);
  EXPECT_DOUBLE_EQ(static_cast<double>(fp9m), 0.0);
  Fp fp10x(-4.5);
  Fp fp10y(-1.0);
  Fp fp10m = Fp::fmod(fp10x, fp10y);
  EXPECT_DOUBLE_EQ(static_cast<double>(fp10m), -0.5);
}

TEST(FunctionSin, FixedPoint) {
  using Fp = LTS5::FPoint<int, 14>;

  const float x1 = 0.0001f;
  Fp fp1(x1);
  const float fp1_sin = static_cast<float>(Fp::sin(fp1));
  const float std1_sin = std::sin(x1);
  float r_err = std::abs((fp1_sin - std1_sin) / std1_sin);
  EXPECT_NEAR(r_err, 0.f, 1e-2);

  const float x2 = -0.0001f;
  Fp fp2(x2);
  const float fp2_sin = static_cast<float>(Fp::sin(fp2));
  const float std2_sin = std::sin(x2);
  r_err = std::abs((fp2_sin - std2_sin) / std2_sin);
  EXPECT_NEAR(r_err, 0.f, 1e-2);

  const float x3 = static_cast<float>(LTS5::Constants<double>::PI_2 / 3.0);
  Fp fp3(x3);
  const float fp3_sin = static_cast<float>(Fp::sin(fp3));
  const float std3_sin = std::sin(x3);
  r_err = std::abs((fp3_sin - std3_sin) / std3_sin);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x4 = static_cast<float>(-LTS5::Constants<double>::PI_2 / 3.0);
  Fp fp4(x4);
  const float fp4_sin = static_cast<float>(Fp::sin(fp4));
  const float std4_sin = std::sin(x4);
  r_err = std::abs((fp4_sin - std4_sin) / std4_sin);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x5 = static_cast<float>(LTS5::Constants<double>::PI_2);
  Fp fp5(x5);
  const float fp5_sin = static_cast<float>(Fp::sin(fp5));
  const float std5_sin = std::sin(x5);
  r_err = std::abs((fp5_sin - std5_sin) / std5_sin);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x6 = static_cast<float>(-LTS5::Constants<double>::PI_2);
  Fp fp6(x6);
  const float fp6_sin = static_cast<float>(Fp::sin(fp6));
  const float std6_sin = std::sin(x6);
  r_err = std::abs((fp6_sin - std6_sin) / std6_sin);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x7 = static_cast<float>(3.0 * LTS5::Constants<double>::PI_2 / 2.0);
  Fp fp7(x7);
  const float fp7_sin = static_cast<float>(Fp::sin(fp7));
  const float std7_sin = std::sin(x7);
  r_err = std::abs((fp7_sin - std7_sin) / std7_sin);
  EXPECT_NEAR(r_err, 0.f, 1e-2);

  const float x8 = static_cast<float>(-3.0 * LTS5::Constants<double>::PI_2 / 2.0);
  Fp fp8(x8);
  const float fp8_sin = static_cast<float>(Fp::sin(fp8));
  const float std8_sin = std::sin(x8);
  r_err = std::abs((fp8_sin - std8_sin) / std8_sin);
  EXPECT_NEAR(r_err, 0.f, 1e-2);
}

TEST(FunctionCos, FixedPoint) {
  using Fp = LTS5::FPoint<int, 14>;

  const float x1 = 0.0001f;
  Fp fp1(x1);
  const float fp1_cos = static_cast<float>(Fp::cos(fp1));
  const float std1_cos = std::cos(x1);
  float r_err = std::abs((fp1_cos - std1_cos) / std1_cos);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x2 = -0.0001f;
  Fp fp2(x2);
  const float fp2_cos = static_cast<float>(Fp::cos(fp2));
  const float std2_cos = std::cos(x2);
  r_err = std::abs((fp2_cos - std2_cos) / std2_cos);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x3 = static_cast<float>(LTS5::Constants<double>::PI_2 / 3.0);
  Fp fp3(x3);
  const float fp3_cos = static_cast<float>(Fp::cos(fp3));
  const float std3_cos = std::cos(x3);
  r_err = std::abs((fp3_cos - std3_cos) / std3_cos);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x4 = static_cast<float>(-LTS5::Constants<double>::PI_2 / 3.0);
  Fp fp4(x4);
  const float fp4_cos = static_cast<float>(Fp::cos(fp4));
  const float std4_cos = std::cos(x4);
  r_err = std::abs((fp4_cos - std4_cos) / std4_cos);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x5 = static_cast<float>(LTS5::Constants<double>::PI_2+0.2);
  Fp fp5(x5);
  const float fp5_cos = static_cast<float>(Fp::cos(fp5));
  const float std5_cos = std::cos(x5);
  r_err = std::abs((fp5_cos - std5_cos) / std5_cos);
  EXPECT_NEAR(r_err, 0.f, 5e-2);

  const float x6 = static_cast<float>(-LTS5::Constants<double>::PI_2+0.2);
  Fp fp6(x6);
  const float fp6_cos = static_cast<float>(Fp::cos(fp6));
  const float std6_cos = std::cos(x6);
  r_err = std::abs((fp6_cos - std6_cos) / std6_cos);
  EXPECT_NEAR(r_err, 0.f, 5e-2);

  const float x7 = static_cast<float>(3.0 * LTS5::Constants<double>::PI_2 / 2.0);
  Fp fp7(x7);
  const float fp7_cos = static_cast<float>(Fp::cos(fp7));
  const float std7_cos = std::cos(x7);
  r_err = std::abs((fp7_cos - std7_cos) / std7_cos);
  EXPECT_NEAR(r_err, 0.f, 5e-2);

  const float x8 = static_cast<float>(-3.0 * LTS5::Constants<double>::PI_2 / 2.0);
  Fp fp8(x8);
  const float fp8_cos = static_cast<float>(Fp::cos(fp8));
  const float std8_cos = std::cos(x8);
  r_err = std::abs((fp8_cos - std8_cos) / std8_cos);
  EXPECT_NEAR(r_err, 0.f, 5e-2);
}

TEST(FunctionExp, FixedPoint) {
  using Fp = LTS5::FPoint<int, 16>;

  const float x1 = 2.5f;
  Fp fp1(x1);
  const float fp1e = static_cast<float>(Fp::exp(fp1));
  const float fp1t = std::exp(x1);
  float r_err = std::abs((fp1e - fp1t) / fp1t);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x2 = 10.5f;
  Fp fp2(x2);
  const float fp2e = static_cast<float>(Fp::exp(fp2));
  const float fp2t = std::exp(x2);
  r_err = std::abs((fp2e - fp2t) / fp2t);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x3 = -0.005125f;
  LTS5::FPoint<int, 4> fp3(x3);
  const float fp3e = static_cast<float>(LTS5::FPoint<int, 4>::exp(fp3));
  const float fp3t = std::exp(x3);
  r_err = std::abs((fp3e - fp3t) / fp3t);
  EXPECT_NEAR(r_err, 0.f, 5e-4);


  const float x4 = -10.5123f;
  LTS5::FPoint<int, 5> fp4(x4);
  const float fp4e = static_cast<float>(LTS5::FPoint<int, 5>::exp(fp4));
  const float fp4t = std::exp(x4);
  r_err = std::abs((fp4e - fp4t) / fp4t);
  EXPECT_NEAR(r_err, 0.f, 5e-4);
}

TEST(FunctionSqrt, FixedPoint) {

  using Fp = LTS5::FPoint<int, 14>;

  const float x1 = 256.f;
  Fp fp1(x1);
  const float fp1s = Fp::sqrt(fp1);
  const float fp1t = std::sqrt(x1);
  float r_err = std::abs((fp1s - fp1t) / fp1t);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x2 = 1024.12356f;
  Fp fp2(x2);
  const float fp2s = Fp::sqrt(fp2);
  const float fp2t = std::sqrt(x2);
  r_err = std::abs((fp2s - fp2t) / fp2t);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const float x3 = 3.12356f;
  Fp fp3(x3);
  const float fp3s = Fp::sqrt(fp3);
  const float fp3t = std::sqrt(x3);
  r_err = std::abs((fp3s - fp3t) / fp3t);
  EXPECT_NEAR(r_err, 0.f, 5e-4);

  const double x4 = 0.12356;
  Fp fp4(x4);
  const double fp4s = Fp::sqrt(fp4);
  const double fp4t = std::sqrt(x4);
  double r_errd = std::abs((fp4s - fp4t) / fp4t);
  EXPECT_NEAR(r_errd, 0.f, 5e-4);
}

TEST(TimingExp, DISABLED_FixedPoint) {
  using Fp = LTS5::FPoint<int, 14>;
  float x = 0.01f;
  auto start = std::chrono::high_resolution_clock::now();
  Fp fps;
  for (int i = 0; i < 1000; ++i) {
    Fp fp(x);
    fps = Fp::exp(fp);
    x += 0.01f;
  }
  auto end = std::chrono::high_resolution_clock::now();
  auto duration_fp = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();

  std::cout << duration_fp / 1000.0 << std::endl;

  x = 0.01f;
  start = std::chrono::high_resolution_clock::now();
  float sq;
  for (int i = 0; i < 1000; ++i) {
    float fp = x;
    sq = std::exp(fp);
    x += 0.01f;
  }
  end = std::chrono::high_resolution_clock::now();
  auto duration_std = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();

  std::cout << duration_std / 1000.0 << std::endl;

  EXPECT_LT(duration_fp, duration_std);
}

TEST(TimingSqrt, DISABLED_FixedPoint) {

  using Fp = LTS5::FPoint<int, 14>;

  auto start = std::chrono::high_resolution_clock::now();
  Fp fps;
  for (int i = 1; i < 1001; ++i) {
    Fp fp(static_cast<float>(i));
    fps = Fp::sqrt(fp);
  }
  auto end = std::chrono::high_resolution_clock::now();
  auto duration_fp = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();

  std::cout << duration_fp << std::endl;

  start = std::chrono::high_resolution_clock::now();
  float sq;
  for (int i = 1; i < 1001; ++i) {
    float fp = static_cast<float>(i);
    sq = std::sqrt(fp);
  }
  end = std::chrono::high_resolution_clock::now();
  auto duration_std = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();

  std::cout << duration_std << std::endl;

  EXPECT_LT(duration_fp, duration_std);
}

int main(int argc, char* argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  // Run unit test
  return RUN_ALL_TESTS();
}
