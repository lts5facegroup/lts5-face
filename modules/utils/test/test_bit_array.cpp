/**
 *  @file   test_bit_array.cpp
 *  @brief  Unit Test for BitArray class
 *
 *  @author Christophe Ecabert
 *  @date   11/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <random>
#include <chrono>

#include "gtest/gtest.h"

#include "lts5/utils/bit_array.hpp"

class BitArrayTest : public ::testing::TestWithParam<size_t> {

 protected:

  void SetUp(void) {
    bit_array_.SetSize(GetParam());
  }

  /** Bit array under test */
  LTS5::BitArray bit_array_;
};

/** Check dimension */
TEST_P(BitArrayTest, CheckDimension) {
  EXPECT_EQ(GetParam(), this->bit_array_.Size());
}

/** Initialization should be cleared */
TEST_P(BitArrayTest, InitZero) {
  for (size_t i = 0; i < GetParam(); ++i) {
    EXPECT_FALSE(this->bit_array_.Get(i));
  }
}

/** Set bits */
TEST_P(BitArrayTest, SetBit) {
  std::default_random_engine gen(std::chrono::system_clock::now()
                                         .time_since_epoch()
                                         .count());
  std::uniform_int_distribution<size_t> dist(0, GetParam() - 1);
  // Pick randomly some index to set
  for (int i = 0; i < GetParam(); ++i) {
    size_t n = dist(gen);
    this->bit_array_.Set(n);
    bool b = this->bit_array_.Get(n);
    EXPECT_TRUE(b);
  }
}

/** Reset bits */
TEST_P(BitArrayTest, ResetBit) {
  std::default_random_engine gen(std::chrono::system_clock::now()
                                         .time_since_epoch()
                                         .count());
  std::uniform_int_distribution<size_t> dist(0, GetParam() - 1);
  // Pick randomly some index to set
  for (int i = 0; i < GetParam(); ++i) {
    size_t n = dist(gen);
    this->bit_array_.Set(n);
    EXPECT_TRUE(this->bit_array_.Get(n));
    this->bit_array_.Reset(n);
    EXPECT_FALSE(this->bit_array_.Get(n));
  }
}

/** Flip bits */
TEST_P(BitArrayTest, FlipBit) {
  std::default_random_engine gen(std::chrono::system_clock::now()
                                         .time_since_epoch()
                                         .count());
  std::uniform_int_distribution<size_t> dist(0, GetParam() - 1);
  // Pick randomly some index to set
  for (int i = 0; i < GetParam(); ++i) {
    size_t n = dist(gen);
    this->bit_array_.Set(n);
    EXPECT_TRUE(this->bit_array_.Get(n));
    this->bit_array_.Flip(n);
    EXPECT_FALSE(this->bit_array_.Get(n));
    this->bit_array_.Flip(n);
    EXPECT_TRUE(this->bit_array_.Get(n));
  }
}

INSTANTIATE_TEST_CASE_P(BitArrayTestInst,
                        BitArrayTest,
                        ::testing::Values(5 , 13, 27, 54, 139));

int main(int argc, char* argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  // Run unit test
  return RUN_ALL_TESTS();
}
