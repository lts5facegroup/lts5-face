/**
 *  @file   test_colormap.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   20/06/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/utils/colormap.hpp"

template<typename T>
class ColorMapIntegralTest : public ::testing::Test {
public:
  ColorMapIntegralTest(void) : map_() {
  }
  using ColorMapType = typename LTS5::ColorMap<T>::ColorMapType;

protected:

  /**
   * @name  set_colormap_type
   * @fn  void set_colormap_type(const ColorMapType type)
   * @brief Set colormap style
   * @param[in] type  Type of colormap
   */
  void set_colormap_type(const ColorMapType type) {
    map_.set_colormap_type(type);
  }

  /**
   * @name  set_range
   * @fn  void set_range(const T min, const T max)
   * @brief Set colormap range
   * @param[in] min Minimum range value
   * @param[in] max Maximum range value
   */
  void set_range(const T min, const T max) {
    map_.set_range(min, max);
  }

  /** ColorMap */
  LTS5::ColorMap<T> map_;

  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }
};

template<typename T>
class ColorMapTest : public ColorMapIntegralTest<T> {
 public:
  ColorMapTest(void) : ColorMapIntegralTest<T>::ColorMapIntegralTest() {
  }
};

// Set type of test
typedef ::testing::Types<float, double> TestTypes;
TYPED_TEST_CASE(ColorMapTest, TestTypes);

// ----------------------------------------------------------------------------
// Default range [0 ; +1], custom type
// ----------------------------------------------------------------------------

/** JET Default test */
TYPED_TEST(ColorMapTest, JetDefaultMap) {
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  Color color;
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(0.0), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(0.25), &color);
  EXPECT_EQ(color, Color(0.f, 1.f, 0.99607843f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(0.75), &color);
  EXPECT_EQ(color, Color(0.99607843f,1.f,0.f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(1.0), &color);
  EXPECT_EQ(color, Color(1.f, 0.f, 0.f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(1.25), &color);
  EXPECT_EQ(color, Color(1.f, 0.f, 0.f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-0.25), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));
}

/** Grayscale Default test */
TYPED_TEST(ColorMapTest, GrayscaleDefaultMap) {
  using ColorMapType = typename LTS5::ColorMap<TypeParam>::ColorMapType;
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  // Setup
  Color color;
  this->map_.set_colormap_type(ColorMapType::kGrayscale);
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(0.0), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 0.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(0.25), &color);
  EXPECT_EQ(color, Color(0.24705882f, 0.24705882f, 0.24705882f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(0.75), &color);
  EXPECT_EQ(color, Color(0.74901960f, 0.74901960f, 0.74901960f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(1.0), &color);
  EXPECT_EQ(color, Color(1.f, 1.f, 1.f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(1.25), &color);
  EXPECT_EQ(color, Color(1.f, 1.f, 1.f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-0.25), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 0.f));
}

/** Winter Default test */
TYPED_TEST(ColorMapTest, WinterDefaultMap) {
  using ColorMapType = typename LTS5::ColorMap<TypeParam>::ColorMapType;
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  // Setup
  Color color;
  this->map_.set_colormap_type(ColorMapType::kWinter);
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(0.0), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(0.25), &color);
  EXPECT_EQ(color, Color(0.f, 0.24705882f, 1.f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(0.75), &color);
  EXPECT_EQ(color, Color(0.f, 0.74901960f, 1.f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(1.0), &color);
  EXPECT_EQ(color, Color(0.f, 1.f, 1.f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(1.25), &color);
  EXPECT_EQ(color, Color(0.f, 1.f, 1.f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-0.25), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));
}

/** Summer Default test */
TYPED_TEST(ColorMapTest, SummerDefaultMap) {
  using ColorMapType = typename LTS5::ColorMap<TypeParam>::ColorMapType;
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  // Setup
  Color color;
  this->map_.set_colormap_type(ColorMapType::kSummer);
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(0.0), &color);
  EXPECT_EQ(color, Color(0.f, 0.62352941f, 0.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(0.25), &color);
  EXPECT_EQ(color, Color(0.58823529f, 0.82352941f, 0.f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(0.75), &color);
  EXPECT_EQ(color, Color(0.87843137f, 0.86274509f, 0.42745098f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(1.0), &color);
  EXPECT_EQ(color, Color(0.97647058f, 0.86274509f, 0.66274509f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(1.25), &color);
  EXPECT_EQ(color, Color(0.97647058f, 0.86274509f, 0.66274509f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-0.25), &color);
  EXPECT_EQ(color, Color(0.f, 0.62352941f, 0.f));
}

// ----------------------------------------------------------------------------
// Custom range [-5 ; +10], custom type
// ----------------------------------------------------------------------------

/** JET Custom Range test */
TYPED_TEST(ColorMapTest, JetCustomRangeMap) {
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  Color color;
  // Setup up
  this->set_range(TypeParam(-5.0), TypeParam(10));
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(-5.0), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(-1.25), &color);
  EXPECT_EQ(color, Color(0.f, 1.f, 0.99607843f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(6.25), &color);
  EXPECT_EQ(color, Color(0.99607843f,1.f,0.f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(10.0), &color);
  EXPECT_EQ(color, Color(1.f, 0.f, 0.f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(12.5), &color);
  EXPECT_EQ(color, Color(1.f, 0.f, 0.f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-7.25), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));
}

/** Grayscale Custom Range test */
TYPED_TEST(ColorMapTest, GrayscaleCustomRangeMap) {
  using ColorMapType = typename LTS5::ColorMap<TypeParam>::ColorMapType;
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  // Setup
  Color color;
  this->set_colormap_type(ColorMapType::kGrayscale);
  this->set_range(TypeParam(-5.0), TypeParam(10));
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(-5.0), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 0.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(-1.25), &color);
  EXPECT_EQ(color, Color(0.24705882f, 0.24705882f, 0.24705882f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(6.25), &color);
  EXPECT_EQ(color, Color(0.74901960f, 0.74901960f, 0.74901960f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(10.0), &color);
  EXPECT_EQ(color, Color(1.f, 1.f, 1.f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(12.5), &color);
  EXPECT_EQ(color, Color(1.f, 1.f, 1.f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-7.25), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 0.f));
}

/** Winter CustomRange test */
TYPED_TEST(ColorMapTest, WinterCustomRangeMap) {
  using ColorMapType = typename LTS5::ColorMap<TypeParam>::ColorMapType;
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  // Setup
  Color color;
  this->set_colormap_type(ColorMapType::kWinter);
  this->set_range(TypeParam(-5.0), TypeParam(10));
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(-5.0), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(-1.25), &color);
  EXPECT_EQ(color, Color(0.f, 0.24705882f, 1.f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(6.25), &color);
  EXPECT_EQ(color, Color(0.f, 0.74901960f, 1.f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(10.0), &color);
  EXPECT_EQ(color, Color(0.f, 1.f, 1.f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(12.5), &color);
  EXPECT_EQ(color, Color(0.f, 1.f, 1.f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-7.25), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));
}

/** Summer CustomRange test */
TYPED_TEST(ColorMapTest, SummerCustomRangeMap) {
  using ColorMapType = typename LTS5::ColorMap<TypeParam>::ColorMapType;
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  // Setup
  Color color;
  this->map_.set_colormap_type(ColorMapType::kSummer);
  this->set_range(TypeParam(-5.0), TypeParam(10));
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(-5.0), &color);
  EXPECT_EQ(color, Color(0.f, 0.62352941f, 0.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(-1.25), &color);
  EXPECT_EQ(color, Color(0.58823529f, 0.82352941f, 0.f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(6.25), &color);
  EXPECT_EQ(color, Color(0.87843137f, 0.86274509f, 0.42745098f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(10.0), &color);
  EXPECT_EQ(color, Color(0.97647058f, 0.86274509f, 0.66274509f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(12.5), &color);
  EXPECT_EQ(color, Color(0.97647058f, 0.86274509f, 0.66274509f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-7.25), &color);
  EXPECT_EQ(color, Color(0.f, 0.62352941f, 0.f));
}

// ----------------------------------------------------------------------------
// Custom range [-5 ; +10], custom type, Integer Type
// ----------------------------------------------------------------------------
// Set type of test
typedef ::testing::Types<long long, int, short> IntegralTestTypes;
TYPED_TEST_CASE(ColorMapIntegralTest, IntegralTestTypes);

/** JET Custom Range Integral Type test */
TYPED_TEST(ColorMapIntegralTest, JetCustomRangeIntegralTypeMap) {
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  Color color;
  // Setup up
  this->set_range(TypeParam(-10), TypeParam(10));
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(-10), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(-5), &color);
  EXPECT_EQ(color, Color(0.f, 0.98823529f, 1.f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(5), &color);
  EXPECT_EQ(color, Color(0.99607843f, 1.f, 0.f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(10.0), &color);
  EXPECT_EQ(color, Color(1.f, 0.f, 0.f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(15), &color);
  EXPECT_EQ(color, Color(1.f, 0.f, 0.f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-15), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));
}

/** Grayscale Custom Range test */
TYPED_TEST(ColorMapIntegralTest, GrayscaleCustomRangeIntegralTypeMap) {
  using ColorMapType = typename LTS5::ColorMap<TypeParam>::ColorMapType;
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  // Setup
  Color color;
  this->set_colormap_type(ColorMapType::kGrayscale);
  this->set_range(TypeParam(-10), TypeParam(10));
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(-10), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 0.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(-5), &color);
  EXPECT_EQ(color, Color(0.24705882f, 0.24705882f, 0.24705882f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(5), &color);
  EXPECT_EQ(color, Color(0.74901960f, 0.74901960f, 0.74901960f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(10.0), &color);
  EXPECT_EQ(color, Color(1.f, 1.f, 1.f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(15), &color);
  EXPECT_EQ(color, Color(1.f, 1.f, 1.f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-15), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 0.f));
}

/** Winter CustomRange IntegralType test */
TYPED_TEST(ColorMapIntegralTest, WinterCustomRangeMap) {
  using ColorMapType = typename LTS5::ColorMap<TypeParam>::ColorMapType;
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  // Setup
  Color color;
  this->set_colormap_type(ColorMapType::kWinter);
  this->set_range(TypeParam(-10), TypeParam(10));
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(-10), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(-5), &color);
  EXPECT_EQ(color, Color(0.f, 0.24705882f, 1.f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(5), &color);
  EXPECT_EQ(color, Color(0.f, 0.74901960f, 1.f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(10), &color);
  EXPECT_EQ(color, Color(0.f, 1.f, 1.f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(15), &color);
  EXPECT_EQ(color, Color(0.f, 1.f, 1.f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-15), &color);
  EXPECT_EQ(color, Color(0.f, 0.f, 1.f));
}

/** Summer CustomRange test */
TYPED_TEST(ColorMapIntegralTest, SummerCustomRangeIntegralTypeMap) {
  using ColorMapType = typename LTS5::ColorMap<TypeParam>::ColorMapType;
  using Color = typename LTS5::ColorMap<TypeParam>::Color;
  // Setup
  Color color;
  this->map_.set_colormap_type(ColorMapType::kSummer);
  this->set_range(TypeParam(-10), TypeParam(10));
  /* Test 0.0 */
  this->map_.PickColor(TypeParam(-10), &color);
  EXPECT_EQ(color, Color(0.f, 0.62352941f, 0.f));

  /* Test 0.25 */
  this->map_.PickColor(TypeParam(-5), &color);
  EXPECT_EQ(color, Color(0.58039215f, 0.81960784f, 0.f));

  /* Test 0.75 */
  this->map_.PickColor(TypeParam(5), &color);
  EXPECT_EQ(color, Color(0.87843137f, 0.86274509f, 0.42745098f));

  /* Test 1.0 */
  this->map_.PickColor(TypeParam(10.0), &color);
  EXPECT_EQ(color, Color(0.97647058f, 0.86274509f, 0.66274509f));

  /* Test 1.25 */
  this->map_.PickColor(TypeParam(15), &color);
  EXPECT_EQ(color, Color(0.97647058f, 0.86274509f, 0.66274509f));
  /* Test -0.25 */
  this->map_.PickColor(TypeParam(-15), &color);
  EXPECT_EQ(color, Color(0.f, 0.62352941f, 0.f));
}



int main(int argc, const char * argv[]) {
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  return ::RUN_ALL_TESTS();
}
