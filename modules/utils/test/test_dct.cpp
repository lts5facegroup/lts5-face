/**
 *  @file   test_dct.cpp
 *  @brief  Testing target for DCT class methods
 *  @author Marina Zimmermann
 *  @date   29/07/16
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */

#include <math.h>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "opencv2/core/core.hpp"

#include "lts5/utils/dct.hpp"


/**
 * @class   DCTTest
 * @brief   Test unit for DCT class methods
 * @author  Marina Zimmermann
 * @date    29/07/16
 */
class DCTTest : public ::testing::Test {

public:


protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

  // put in any custom data members that you need

};

/** Define unit test, DCTOfConstantMat */
TEST(DCTTest, DCTOfConstantMat) {

  cv::Mat mat_100_(8, 8, CV_32FC1, 100);
  cv::Mat mat_100_dct_ = cv::Mat::zeros(8, 8, CV_32FC1);
  mat_100_dct_.at<float>(0,0) = 800;

  cv::Mat dct_image, full_dct, odd_dct, even_odd_dct;
  LTS5::ComputeDct(mat_100_, cv::Size(8, 8), &dct_image, &full_dct, &odd_dct, &even_odd_dct);

  for (int i = 0; i < dct_image.rows; ++i) {
    for (int j = 0; j < dct_image.cols; ++j) {
      EXPECT_THAT(roundf(dct_image.at<float>(i, j) * 1000) / 1000, ::testing::FloatEq(mat_100_dct_.at<float>(i, j)));
    }
  }
}

/** Define unit test, DCTOfNearConstantMat */
TEST(DCTTest, DCTOfNearConstantMat) {

  float mat_init[8][8] = {{51, 51, 50, 51, 51, 50, 51, 50},
                          {52, 52, 50, 50, 50, 51, 52, 51},
                          {51, 51, 51, 50, 50, 52, 51, 52},
                          {50, 51, 52, 50, 51, 52, 50, 52},
                          {50, 50, 52, 52, 50, 51, 52, 50},
                          {52, 52, 51, 50, 50, 50, 50, 51},
                          {50, 52, 51, 50, 51, 50, 52, 52},
                          {52, 51, 51, 51, 50, 50, 50, 51}};

  cv::Mat mat_near_const_ = cv::Mat(8, 8, CV_32FC1, mat_init).t();

  float mat_init_dct[8][8] = {{407, 0.352, 1.904, -0.661, -1, -0.229, 0.023, -0.110},
                              {0.058, -0.654, -0.116, 1.350, -0.335, 0.162, -0.173, -0.383},
                              {-0.518, 1.019, 1, 0.689, 1.171, 0.115, -1.707, 0.105},
                              {-0.592, 0.818, -0.598, -0.055, 0.102, 0.711, -1.529, 0.470},
                              {-0.5, 0.179, -2.174, -0.425, 0.5, 0.955, 0.630, 0.005},
                              {0.118, -1.074, -0.352, -0.599, -0.020, -1.902, 0.109, 0.568},
                              {-0.597, 1.190, 0.293, 0.254, 0.868, -0.108, 1, -0.470},
                              {0.086, -1.194, -1.006, -0.412, -0.502, 1.454, -0.603, 0.111}};


  cv::Mat mat_near_const_dct_ = cv::Mat(8, 8, CV_32FC1, mat_init_dct).t();

  cv::Mat dct_image, full_dct, odd_dct, even_odd_dct;
  LTS5::ComputeDct(mat_near_const_, cv::Size(8, 8), &dct_image, &full_dct, &odd_dct, &even_odd_dct);

  for (int i = 0; i < dct_image.rows; ++i) {
    for (int j = 0; j < dct_image.cols; ++j) {
      EXPECT_THAT(roundf(dct_image.at<float>(i, j) * 1000) / 1000, ::testing::FloatEq(mat_near_const_dct_.at<float>(i, j)));
    }
  }
}

/** Define unit test, ZigzagMat */
TEST(DCTTest, ZigzagMat) {

  float mat_init[3][3] = {{1, 2, 3},
                          {4, 5, 6},
                          {7, 8, 9}};

  cv::Mat mat_ind_ = cv::Mat(3, 3, CV_32FC1, mat_init);
  cv::Mat mat_ind_zigzag_ = (cv::Mat_<float>(9, 1) << 1, 2, 4, 7, 5, 3, 6, 8, 9);
  cv::Mat mat_ind_zigzag_odd_ = (cv::Mat_<float>(5, 1) << 4, 7, 3, 6, 9);
  cv::Mat mat_ind_zigzag_evenodd_ = (cv::Mat_<float>(8, 1) << 2, 4, 7, 5, 3, 6, 8, 9);


  cv::Mat full_dct, odd_dct, even_odd_dct;
  LTS5::zigzagMatrix(mat_ind_, cv::Size(3, 3), &full_dct, &odd_dct, &even_odd_dct);

  for (int i = 0; i < full_dct.rows; ++i) {
    for (int j = 0; j < full_dct.cols; ++j) {
      EXPECT_THAT(full_dct.at<float>(i, j), ::testing::FloatEq(mat_ind_zigzag_.at<float>(i, j)));
    }
  }

  for (int i = 0; i < odd_dct.rows; ++i) {
    for (int j = 0; j < odd_dct.cols; ++j) {
      EXPECT_THAT(odd_dct.at<float>(i, j), ::testing::FloatEq(mat_ind_zigzag_odd_.at<float>(i, j)));
    }
  }

  for (int i = 0; i < even_odd_dct.rows; ++i) {
    for (int j = 0; j < even_odd_dct.cols; ++j) {
      EXPECT_THAT(even_odd_dct.at<float>(i, j), ::testing::FloatEq(mat_ind_zigzag_evenodd_.at<float>(i, j)));
    }
  }
}


int main(int argc, const char * argv[]) {
    // Init test
    ::testing::InitGoogleTest(&argc, const_cast<char **>(argv));
    // Run test
    return RUN_ALL_TESTS();
}