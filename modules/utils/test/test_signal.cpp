/**
 *  @file   test_signal.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   01/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <thread>
#include <chrono>

#include "gtest/gtest.h"

#include "lts5/utils/signal.hpp"

/** Event type */
enum EventType{kRed, kGreen};
/** global variable for test */
int g_test_value;
float g_test_f_value;

/**
 *  @brief Signal definition with the following definition :
 *      Events are signal by EventType variable
 *      Callback has the following signature : void(int, float)
 */
using Signal = LTS5::Signal<EventType, void(int, float)>;

/** Method callbacks */
void observerCallback(const int value, const float f_val) {
  g_test_value = value;
  g_test_f_value = f_val;
}

/** Method callbacks - second */
void secondObserverCallback(const int value, const float f_val) {
  g_test_value += value;
  g_test_f_value += f_val;
}

/** Member callback */
struct observerHandler {
  void callback(const int value, const float f_val) {
    test_value = value;
    test_f_value = f_val;
  }
  void sum_callback(const int value, const float f_val) {
    test_value += value;
    test_f_value += f_val;
  }
  int test_value;
  float test_f_value;
};

TEST(TestSignal, AddRemoveObserver) {
  // Define signal
  Signal signal;
  // Register callback to signal
  signal.AddObserver(EventType::kRed, observerCallback);
  EXPECT_EQ(signal.get_number_observer(), 1);
  // Register a new one
  signal.AddObserver(EventType::kGreen, observerCallback);
  EXPECT_EQ(signal.get_number_observer(), 2);
  // Remove
  signal.RemoveObserver(EventType::kRed);
  EXPECT_EQ(signal.get_number_observer(), 1);
  signal.RemoveObserver(EventType::kRed);
  EXPECT_EQ(signal.get_number_observer(), 1);
}

TEST(TestSignal, MultipleObserver) {
  // Define signal
  Signal signal;
  // Register callback to signal
  signal.AddObserver(EventType::kRed, observerCallback);
  signal.AddObserver(EventType::kRed, secondObserverCallback);
  // Set initial value
  g_test_value = -1;
  g_test_f_value = -1.f;
  // Notify
  signal.Notify(EventType::kRed, 52, 7.25f);
  // Control
  EXPECT_EQ(g_test_value, 104);
  EXPECT_FLOAT_EQ(g_test_f_value, 14.5f);
}

TEST(TestSignal, MultipleMemberObserver) {
  // Define signal
  Signal signal;
  // Define callback handler
  observerHandler handler;
  // Register callback to signal
  signal.AddMemberObserver(EventType::kGreen,
                           &observerHandler::callback,
                           &handler,
                           std::placeholders::_1,
                           std::placeholders::_2);
  signal.AddMemberObserver(EventType::kGreen,
                           &observerHandler::sum_callback,
                           &handler,
                           std::placeholders::_1,
                           std::placeholders::_2);
  // Set initial value
  handler.test_value = -1;
  handler.test_f_value = -1.f;
  // Notify
  signal.Notify(EventType::kGreen, 12, 1.75f);
  // Control
  EXPECT_EQ(handler.test_value, 24);
  EXPECT_FLOAT_EQ(handler.test_f_value, 3.5f);
}

TEST(TestSignal, Callback) {
  // Define signal
  Signal signal;
  // Register callback to signal
  signal.AddObserver(EventType::kRed, observerCallback);
  // Set initial value
  g_test_value = -1;
  g_test_f_value = -1.f;
  // Notify
  signal.Notify(EventType::kRed, 102, 3.48f);
  // Control
  EXPECT_EQ(g_test_value, 102);
  EXPECT_FLOAT_EQ(g_test_f_value, 3.48f);
}

TEST(TestSignal, MemberCallback) {
  // Define signal
  Signal signal;
  // Define callback handler
  observerHandler handler;
  // Register callback to signal
  signal.AddMemberObserver(EventType::kGreen,
                           &observerHandler::callback,
                           &handler,
                           std::placeholders::_1,
                           std::placeholders::_2);
  // Set initial value
  handler.test_value = -1;
  handler.test_f_value = -1.f;
  // Notify
  signal.Notify(EventType::kGreen, 12, 1.75f);
  // Control
  EXPECT_EQ(handler.test_value, 12);
  EXPECT_FLOAT_EQ(handler.test_f_value, 1.75f);
}

TEST(TestSignal, WorkerThreadCallback) {
  // Define signal
  Signal signal;
  // Register callback to signal
  signal.AddObserver(EventType::kRed, observerCallback);
  // Set initial value
  g_test_value = -1;
  g_test_f_value = -1.f;
  // Spawn thread
  std::thread worker([&](void) -> void {
    // sleep for a moment
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    // notify
    signal.Notify(EventType::kRed, 98, 7.89f);
  });
  // Wait
  worker.join();
  // Control
  EXPECT_EQ(g_test_value, 98);
  EXPECT_FLOAT_EQ(g_test_f_value, 7.89f);
}

TEST(TestSignal, WorkerThreadMemberCallback) {
  // Define signal
  Signal signal;
  // Define callback handler
  observerHandler handler;
  // Register callback to signal
  signal.AddMemberObserver(EventType::kGreen,
                           &observerHandler::callback,
                           &handler, std::placeholders::_1,
                           std::placeholders::_2);
  // Set initial value
  handler.test_value = -1;
  handler.test_f_value = -1.f;
  // Spawn thread
  std::thread worker([&](void) -> void {
    // sleep for a moment
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    // notify
    signal.Notify(EventType::kGreen, 42, 11.36f);
  });
  // Wait
  worker.join();
  // Control
  EXPECT_EQ(handler.test_value, 42);
  EXPECT_FLOAT_EQ(handler.test_f_value, 11.36f);
}

int main(int argc, char* argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  // Run unit test
  return RUN_ALL_TESTS();
}
