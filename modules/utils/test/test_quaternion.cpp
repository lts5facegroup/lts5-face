//
//  test_ssift_descriptor.cpp
//  LTS5-Dev
//
//  Created by Christophe Ecabert on 06/05/15.
//  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
//

#include <iostream>
#include <chrono>
#include <random>

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "opencv2/core/core.hpp"

#include "lts5/utils/math/quaternion.hpp"
#include "lts5/utils/math/constant.hpp"

// List of type to test against
typedef ::testing::Types<float, double> TypeList;

/**
 *  @class  TestLinearAlgebra
 *  @brief  Class for testing multiple type
 *  @author Christophe Ecabert
 *  @date   20/10/2016
 *  @see    https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md
 */
template<typename T>
class TestQuaternion : public ::testing::Test {
 public:
  TestQuaternion(void) {}
};

TYPED_TEST_CASE(TestQuaternion, TypeList);

#pragma mark -
#pragma mark Dot

/** Addition */
TYPED_TEST(TestQuaternion, Addition) {
  // Random gen
  auto seed = std::chrono::high_resolution_clock::now()
          .time_since_epoch().count();
  std::mt19937_64 gen(seed);
  std::uniform_real_distribution<TypeParam> dist(0.0, 1.0);

  // Define data
  LTS5::Quaternion<TypeParam> q1;
  TypeParam* q1_ptr = &(q1.x_);
  LTS5::Quaternion<TypeParam> q2;
  TypeParam* q2_ptr = &(q2.x_);
  std::vector<TypeParam> q_true(4);
  for (int i = 0; i < 4; ++i) {
    // Draw variables
    const TypeParam v1 = dist(gen);
    const TypeParam v2 = dist(gen);
    // Compute ground truth
    q_true[i] = v1 + v2;
    // Set to qX
    q1_ptr[i] = v1;
    q2_ptr[i] = v2;
  }
  // Op
  auto q = q1 + q2;
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-12;
  EXPECT_LT(std::abs(q.x_ - q_true[0]), thr);
  EXPECT_LT(std::abs(q.y_ - q_true[1]), thr);
  EXPECT_LT(std::abs(q.z_ - q_true[2]), thr);
  EXPECT_LT(std::abs(q.w_ - q_true[3]), thr);
}

/** Subtraction */
TYPED_TEST(TestQuaternion, Subtraction) {
  // Random gen
  auto seed = std::chrono::high_resolution_clock::now()
          .time_since_epoch().count();
  std::mt19937_64 gen(seed);
  std::uniform_real_distribution<TypeParam> dist(0.0, 1.0);

  // Define data
  LTS5::Quaternion<TypeParam> q1;
  TypeParam* q1_ptr = &(q1.x_);
  LTS5::Quaternion<TypeParam> q2;
  TypeParam* q2_ptr = &(q2.x_);
  std::vector<TypeParam> q_true(4);
  for (int i = 0; i < 4; ++i) {
    // Draw variables
    const TypeParam v1 = dist(gen);
    const TypeParam v2 = dist(gen);
    // Compute ground truth
    q_true[i] = v1 - v2;
    // Set to qX
    q1_ptr[i] = v1;
    q2_ptr[i] = v2;
  }
  // Op
  auto q = q1 - q2;
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-12;
  EXPECT_LT(std::abs(q.x_ - q_true[0]), thr);
  EXPECT_LT(std::abs(q.y_ - q_true[1]), thr);
  EXPECT_LT(std::abs(q.z_ - q_true[2]), thr);
  EXPECT_LT(std::abs(q.w_ - q_true[3]), thr);
}

/** Scaling */
TYPED_TEST(TestQuaternion, Scaling) {
  // Random gen
  auto seed = std::chrono::high_resolution_clock::now()
          .time_since_epoch().count();
  std::mt19937_64 gen(seed);
  std::uniform_real_distribution<TypeParam> dist(0.0, 1.0);

  // Define data
  LTS5::Quaternion<TypeParam> q1;
  TypeParam* q1_ptr = &(q1.x_);
  std::vector<TypeParam> q_true(4);
  const TypeParam scale = dist(gen);
  for (int i = 0; i < 4; ++i) {
    // Draw variables
    const TypeParam v1 = dist(gen);

    // Compute ground truth
    q_true[i] = v1 * scale;
    // Set to qX
    q1_ptr[i] = v1;
  }
  // Op
  auto q = q1 * scale;
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-12;
  EXPECT_LT(std::abs(q.x_ - q_true[0]), thr);
  EXPECT_LT(std::abs(q.y_ - q_true[1]), thr);
  EXPECT_LT(std::abs(q.z_ - q_true[2]), thr);
  EXPECT_LT(std::abs(q.w_ - q_true[3]), thr);
}

/** Rotation */
TYPED_TEST(TestQuaternion, Rotation) {

  // Define data
  using T = TypeParam;
  LTS5::Vector3<T> axis(0.0, 1.0, 0.0);
  const T angle = LTS5::Constants<T>::PI * T(45.0/180.0);
  LTS5::Quaternion<TypeParam> q1(axis, angle);
  const T sq2 = std::sqrt(T(2.0)) / T(2.0);
  // Define ground truth
  const std::vector<T> yrot = {sq2, T(0.0), sq2,
                               T(0.0), T(1.0), T(0.0),
                               -sq2, T(0.0), sq2};
  // Convert to rot
  LTS5::Matrix3x3<T> rot;
  q1.ToRotationMatrix(&rot);
  std::vector<T> rotm(rot.Data(), rot.Data()  + 9);
  // Compare
  for (int i = 0; i < 9; ++i) {
    EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperFloatingPointEQ<T>,
                        rotm[i],
                        yrot[i]);

  }
}

/** Stacked Rotation */
TYPED_TEST(TestQuaternion, StackedRotation) {

  // Define data
  using T = TypeParam;
  LTS5::Vector3<T> axis(0.0, 1.0, 0.0);
  const T ang1 = LTS5::Constants<T>::PI * T(45.0/180.0);
  const T ang2 = LTS5::Constants<T>::PI * T(-90.0/180.0);
  LTS5::Quaternion<TypeParam> q1(axis, ang1);
  LTS5::Quaternion<TypeParam> q2(axis, ang2);
  auto q = q2 * q1;


  // Define ground truth
  const T sq2 = std::sqrt(T(2.0)) / T(2.0);
  std::vector<T> yrot = {sq2, T(0.0), -sq2,
                         T(0.0), T(1.0), T(0.0),
                         sq2, T(0.0), sq2};
  // Convert to rot
  LTS5::Matrix3x3<T> rot;
  q.ToRotationMatrix(&rot);
  std::vector<T> rotm(rot.Data(), rot.Data()  + 9);
  // Compare
  for (int i = 0; i < 9; ++i) {
    EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperFloatingPointEQ<T>,
                        rotm[i],
                        yrot[i]);

  }
  // Define new rotation
  q = q1 * q2;
  q.ToRotationMatrix(&rot);
  // Define ground truth
  yrot = {sq2, T(0.0), -sq2,
          T(0.0), T(1.0), T(0.0),
          sq2, T(0.0), sq2};
  // Compare
  for (int i = 0; i < 9; ++i) {
    EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperFloatingPointEQ<T>,
                        rotm[i],
                        yrot[i]);

  }
}


int main(int argc, const char * argv[]) {
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  return RUN_ALL_TESTS();
}


