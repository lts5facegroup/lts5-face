/**
 *  @file   test_linear_algebra_wrapper.cpp
 *  @brief  Linear Algebra test unit
 *
 *  @author Christophe Ecabert
 *  @date   06/05/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#include <cstdio>
#include <iostream>
#include <chrono>
#include <random>

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "opencv2/core/core.hpp"

#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "Eigen/Core"

// List of type to test against
typedef ::testing::Types<float, double> TypeList;

/**
 *  @class  TestLinearAlgebra
 *  @brief  Class for testing multiple type
 *  @author Christophe Ecabert
 *  @date   20/10/2016
 *  @see    https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md
 */
template<typename T>
class TestLinearAlgebra : public ::testing::Test {
 public:
  TestLinearAlgebra(void) {}
};

TYPED_TEST_CASE(TestLinearAlgebra, TypeList);

#pragma mark -
#pragma mark Dot

/** Dot product test case */
TYPED_TEST(TestLinearAlgebra, DotStride1) {
  // Define data
  std::vector<TypeParam> a = {5.0 , 8.0 , 3.0 , 1.0 , 6.0, 1.0};
  std::vector<TypeParam> b = {3.0 , 7.0 , 9.0 , 2.0 , 4.0, 9.0};
  cv::Mat vec_a = cv::Mat(6,1,cv::DataType<TypeParam>::type, a.data());
  cv::Mat vec_b = cv::Mat(6,1,cv::DataType<TypeParam>::type, b.data());

  // Compute dot prod
  TypeParam d = LTS5::LinearAlgebra<TypeParam>::Dot(vec_a, vec_b);
  EXPECT_FLOAT_EQ(d, 133.0);
}
TYPED_TEST(TestLinearAlgebra, DotStride2) {
  // Define data
  std::vector<TypeParam> a = {5.0 , 8.0 , 3.0 , 1.0 , 6.0, 1.0};
  std::vector<TypeParam> b = {3.0 , 7.0 , 9.0 , 2.0 , 4.0, 9.0};
  cv::Mat vec_a = cv::Mat(6,1,cv::DataType<TypeParam>::type, a.data());
  cv::Mat vec_b = cv::Mat(6,1,cv::DataType<TypeParam>::type, b.data());

  // Compute dot prod
  TypeParam d = LTS5::LinearAlgebra<TypeParam>::Dot(vec_a, 2, vec_b, 2);
  EXPECT_FLOAT_EQ(d, 66.0);
}
TYPED_TEST(TestLinearAlgebra, DotStride3) {
  // Define data
  std::vector<TypeParam> a = {5.0 , 8.0 , 3.0 , 1.0 , 6.0, 1.0};
  std::vector<TypeParam> b = {3.0 , 7.0 , 9.0 , 2.0 , 4.0, 9.0};
  cv::Mat vec_a = cv::Mat(6,1,cv::DataType<TypeParam>::type, a.data());
  cv::Mat vec_b = cv::Mat(6,1,cv::DataType<TypeParam>::type, b.data());

  // Compute dot prod
  TypeParam d = LTS5::LinearAlgebra<TypeParam>::Dot(vec_a, 3, vec_b, 3);
  EXPECT_FLOAT_EQ(d, 17.0);
}
TYPED_TEST(TestLinearAlgebra, DotStride4) {
  // Define data
  std::vector<TypeParam> a = {5.0 , 8.0 , 3.0 , 1.0 , 6.0, 1.0};
  std::vector<TypeParam> b = {3.0 , 7.0 , 9.0 , 2.0 , 4.0, 9.0};
  cv::Mat vec_a = cv::Mat(6,1,cv::DataType<TypeParam>::type, a.data());
  cv::Mat vec_b = cv::Mat(6,1,cv::DataType<TypeParam>::type, b.data());

  // Compute dot prod
  TypeParam d = LTS5::LinearAlgebra<TypeParam>::Dot(vec_a, 4, vec_b, 4);
  EXPECT_FLOAT_EQ(d, 39.0);
}
TYPED_TEST(TestLinearAlgebra, DotStride5) {
  // Define data
  std::vector<TypeParam> a = {5.0 , 8.0 , 3.0 , 1.0 , 6.0, 1.0};
  std::vector<TypeParam> b = {3.0 , 7.0 , 9.0 , 2.0 , 4.0, 9.0};
  cv::Mat vec_a = cv::Mat(6,1,cv::DataType<TypeParam>::type, a.data());
  cv::Mat vec_b = cv::Mat(6,1,cv::DataType<TypeParam>::type, b.data());

  // Compute dot prod
  TypeParam d = LTS5::LinearAlgebra<TypeParam>::Dot(vec_a, 5, vec_b, 5);
  EXPECT_FLOAT_EQ(d, 24.0);
}
TYPED_TEST(TestLinearAlgebra, DotStride6) {
  // Define data
  std::vector<TypeParam> a = {5.0 , 8.0 , 3.0 , 1.0 , 6.0, 1.0};
  std::vector<TypeParam> b = {3.0 , 7.0 , 9.0 , 2.0 , 4.0, 9.0};
  cv::Mat vec_a = cv::Mat(6,1,cv::DataType<TypeParam>::type, a.data());
  cv::Mat vec_b = cv::Mat(6,1,cv::DataType<TypeParam>::type, b.data());

  // Compute dot prod
  TypeParam d = LTS5::LinearAlgebra<TypeParam>::Dot(vec_a, 6, vec_b, 6);
  EXPECT_FLOAT_EQ(d, 15.0);
}

#pragma mark -
#pragma mark Gemv

/** Gemv */
TYPED_TEST(TestLinearAlgebra, GemvNoTrans) {
  using LA = LTS5::LinearAlgebra<TypeParam>;
  using TType = typename LTS5::LinearAlgebra<TypeParam>::TransposeType;
  // Define input to vector
  cv::Mat A(57, 13, cv::DataType<TypeParam>::type);
  cv::Mat x(13, 1, cv::DataType<TypeParam>::type);
  cv::Mat y(57, 1, cv::DataType<TypeParam>::type);
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  cv::randn(A, TypeParam(0.0), TypeParam(1.0));
  cv::randn(x, TypeParam(0.0), TypeParam(1.0));
  cv::randn(y, TypeParam(0.0), TypeParam(1.0));
  // Define random alpha
  auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  std::mt19937 gen(seed);
  std::normal_distribution<TypeParam> dist(TypeParam(0.0), TypeParam(1.0));
  TypeParam alpha = dist(gen);
  TypeParam beta = dist(gen);

  // Compute ground truth
  cv::Mat gt_y = (alpha * (A * x)) + (beta * y);
  // Call cblas
  LA::Gemv(A, TType::kNoTranspose, alpha, x, beta, &y);
  // Compare
  TypeParam diff = (TypeParam)cv::norm(gt_y, y) / gt_y.total();
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-8;
  EXPECT_LT(diff, thr);
}

/** Gemv */
TYPED_TEST(TestLinearAlgebra, GemvTrans) {
  using LA = LTS5::LinearAlgebra<TypeParam>;
  using TType = typename LTS5::LinearAlgebra<TypeParam>::TransposeType;
  // Define input to vector
  cv::Mat A(57, 13, cv::DataType<TypeParam>::type);
  cv::Mat x(57, 1, cv::DataType<TypeParam>::type);
  cv::Mat y(13, 1, cv::DataType<TypeParam>::type);
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  cv::randn(A, TypeParam(0.0), TypeParam(1.0));
  cv::randn(x, TypeParam(0.0), TypeParam(1.0));
  cv::randn(y, TypeParam(0.0), TypeParam(1.0));
  // Define random alpha
  auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  std::mt19937 gen(seed);
  std::normal_distribution<TypeParam> dist(TypeParam(0.0), TypeParam(1.0));
  TypeParam alpha = dist(gen);
  TypeParam beta = dist(gen);

  // Compute ground truth
  cv::Mat gt_y = (alpha * (A.t() * x)) + (beta * y);
  // Call cblas
  LA::Gemv(A, TType::kTranspose, alpha, x, beta, &y);
  // Compare
  TypeParam diff = (TypeParam)cv::norm(gt_y, y) / gt_y.total();
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-8;
  EXPECT_LT(diff, thr);
}

#pragma mark -
#pragma mark Gemm

/** Gemm */
TYPED_TEST(TestLinearAlgebra, Gemm_AB) {
  using LA = LTS5::LinearAlgebra<TypeParam>;
  using TType = typename LTS5::LinearAlgebra<TypeParam>::TransposeType;
  // Define input to vector
  cv::Mat A(57, 33, cv::DataType<TypeParam>::type);
  cv::Mat B(33, 17, cv::DataType<TypeParam>::type);
  cv::Mat C(57, 17, cv::DataType<TypeParam>::type);
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  cv::randn(A, TypeParam(0.0), TypeParam(1.0));
  cv::randn(B, TypeParam(0.0), TypeParam(1.0));
  cv::randn(C, TypeParam(0.0), TypeParam(1.0));
  // Define random alpha
  auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  std::mt19937 gen(seed);
  std::normal_distribution<TypeParam> dist(TypeParam(0.0), TypeParam(1.0));
  TypeParam alpha = dist(gen);
  TypeParam beta = dist(gen);

  // Compute ground truth
  cv::Mat gt_C = (alpha * (A * B)) + (beta * C);
  // Call cblas
  LA::Gemm(A, TType::kNoTranspose, alpha, B, TType::kNoTranspose, beta, &C);
  // Compare
  TypeParam diff = (TypeParam)cv::norm(gt_C, C) / gt_C.total();
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-8;
  EXPECT_LT(diff, thr);
}

/** Gemm */
TYPED_TEST(TestLinearAlgebra, Gemm_AtB) {
  using LA = LTS5::LinearAlgebra<TypeParam>;
  using TType = typename LTS5::LinearAlgebra<TypeParam>::TransposeType;
  // Define input to vector
  cv::Mat A(57, 33, cv::DataType<TypeParam>::type);
  cv::Mat B(57, 17, cv::DataType<TypeParam>::type);
  cv::Mat C(33, 17, cv::DataType<TypeParam>::type);
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  cv::randn(A, TypeParam(0.0), TypeParam(1.0));
  cv::randn(B, TypeParam(0.0), TypeParam(1.0));
  cv::randn(C, TypeParam(0.0), TypeParam(1.0));
  // Define random alpha
  auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  std::mt19937 gen(seed);
  std::normal_distribution<TypeParam> dist(TypeParam(0.0), TypeParam(1.0));
  TypeParam alpha = dist(gen);
  TypeParam beta = dist(gen);

  // Compute ground truth
  cv::Mat gt_C = (alpha * (A.t() * B)) + (beta * C);
  // Call cblas
  LA::Gemm(A, TType::kTranspose, alpha, B, TType::kNoTranspose, beta, &C);
  // Compare
  TypeParam diff = (TypeParam)cv::norm(gt_C, C) / gt_C.total();
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-8;
  EXPECT_LT(diff, thr);
}

/** Gemm */
TYPED_TEST(TestLinearAlgebra, Gemm_ABt) {
  using LA = LTS5::LinearAlgebra<TypeParam>;
  using TType = typename LTS5::LinearAlgebra<TypeParam>::TransposeType;
  // Define input to vector
  cv::Mat A(57, 33, cv::DataType<TypeParam>::type);
  cv::Mat B(17, 33, cv::DataType<TypeParam>::type);
  cv::Mat C(57, 17, cv::DataType<TypeParam>::type);
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  cv::randn(A, TypeParam(0.0), TypeParam(1.0));
  cv::randn(B, TypeParam(0.0), TypeParam(1.0));
  cv::randn(C, TypeParam(0.0), TypeParam(1.0));
  // Define random alpha
  auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  std::mt19937 gen(seed);
  std::normal_distribution<TypeParam> dist(TypeParam(0.0), TypeParam(1.0));
  TypeParam alpha = dist(gen);
  TypeParam beta = dist(gen);

  // Compute ground truth
  cv::Mat gt_C = (alpha * (A * B.t())) + (beta * C);
  // Call cblas
  LA::Gemm(A, TType::kNoTranspose, alpha, B, TType::kTranspose, beta, &C);
  // Compare
  TypeParam diff = (TypeParam)cv::norm(gt_C, C) / gt_C.total();
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-8;
  EXPECT_LT(diff, thr);
}

/** Gemm */
TYPED_TEST(TestLinearAlgebra, Gemm_AtBt) {
  using LA = LTS5::LinearAlgebra<TypeParam>;
  using TType = typename LTS5::LinearAlgebra<TypeParam>::TransposeType;
  // Define input to vector
  cv::Mat A(57, 33, cv::DataType<TypeParam>::type);
  cv::Mat B(17, 57, cv::DataType<TypeParam>::type);
  cv::Mat C(33, 17, cv::DataType<TypeParam>::type);
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  cv::randn(A, TypeParam(0.0), TypeParam(1.0));
  cv::randn(B, TypeParam(0.0), TypeParam(1.0));
  cv::randn(C, TypeParam(0.0), TypeParam(1.0));
  // Define random alpha
  auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  std::mt19937 gen(seed);
  std::normal_distribution<TypeParam> dist(TypeParam(0.0), TypeParam(1.0));
  TypeParam alpha = dist(gen);
  TypeParam beta = dist(gen);

  // Compute ground truth
  cv::Mat gt_C = (alpha * (A.t() * B.t())) + (beta * C);
  // Call cblas
  LA::Gemm(A, TType::kTranspose, alpha, B, TType::kTranspose, beta, &C);
  // Compare
  TypeParam diff = (TypeParam)cv::norm(gt_C, C) / gt_C.total();
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-8;
  EXPECT_LT(diff, thr);
}

/** Gemm */
TYPED_TEST(TestLinearAlgebra, Gemm_AB_ROI) {
  using LA = LTS5::LinearAlgebra<TypeParam>;
  using TType = typename LTS5::LinearAlgebra<TypeParam>::TransposeType;
  // Define input to vector
  cv::Mat A(57, 33, cv::DataType<TypeParam>::type);
  cv::Mat B(33, 17, cv::DataType<TypeParam>::type);
  cv::Mat C(57, 34, cv::DataType<TypeParam>::type);
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  cv::randn(A, TypeParam(0.0), TypeParam(1.0));
  cv::randn(B, TypeParam(0.0), TypeParam(1.0));
  cv::randn(C, TypeParam(0.0), TypeParam(1.0));
  C(cv::Rect(0, 0, 17, 57)) = TypeParam(1.0);
  // Define random alpha
  auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  std::mt19937 gen(seed);
  std::normal_distribution<TypeParam> dist(TypeParam(0.0), TypeParam(1.0));
  TypeParam alpha = dist(gen);
  TypeParam beta = dist(gen);

  // Compute ground truth
  cv::Mat gt_C(57,
               34,
               cv::DataType<TypeParam>::type,
               cv::Scalar_<TypeParam>(1.0));
  gt_C(cv::Rect(17, 0, 17, 57))  = (alpha * (A * B)) +
                                   (beta * C(cv::Rect(17, 0, 17, 57)));


  const TypeParam* a_ptr = A.ptr<const TypeParam>(0, 0);
  const TypeParam* b_ptr = B.ptr<const TypeParam>(0, 0);
  TypeParam* c_ptr = C.ptr<TypeParam>(0, 17);
  // Call cblas with ROI
  LA::Gemm(TType::kNoTranspose,
           A.rows,
           B.cols,
           A.cols,
           alpha,
           a_ptr,
           A.cols,
           TType::kNoTranspose,
           beta,
           b_ptr,
           B.cols,
           C.cols,
           c_ptr);
  // Compare
  TypeParam diff = (TypeParam)cv::norm(gt_C, C) / gt_C.total();
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-8;
  EXPECT_LT(diff, thr);
}

#pragma mark -
#pragma mark EigenValue

TYPED_TEST(TestLinearAlgebra, EigenValue) {
  std::vector<TypeParam> a = {611.0, 196.0, -192.0, 407.0, 196.0, 899.0, 113.0,
                              -192.0, -192.0, 113.0, 899.0, 196.0, 407.0,
                              -192.0, 196.0, 611.0};
  cv::Mat A = cv::Mat(4, 4, cv::DataType<TypeParam>::type, a.data());
  //Ground truth
  cv::Mat real_eigen_value, real_eigen_vector;
  cv::eigen(A, real_eigen_value, real_eigen_vector);
  //Wrapper computation
  typename LTS5::LinearAlgebra<TypeParam>::SymEigenDecomposition Decomp;
  Decomp.InitFullComputation(A);
  Decomp.Compute();
  cv::Mat eigenvalues;
  cv::Mat eigenvectors;
  Decomp.GetEigenvalues(&eigenvalues);
  Decomp.GetEigenvectors(&eigenvectors);
  // Compare
  TypeParam diff_eig_val = (TypeParam)cv::norm(real_eigen_value - eigenvalues);
  EXPECT_LT(diff_eig_val, 5e-4);

  TypeParam err_vector = TypeParam(0.0);
  for(int i = 0 ; i < real_eigen_vector.rows ; ++i) {
    cv::Mat row_gt = real_eigen_vector.row(i);
    row_gt /= (float)cv::norm(row_gt);
    cv::Mat row_lts5 = real_eigen_vector.row(i);
    row_lts5 /= (float)cv::norm(row_lts5);
    err_vector +=  (float)std::abs(row_gt.dot(row_lts5));
  }
  err_vector -= TypeParam(4.0);
  EXPECT_LT(err_vector, 1e-6);
}

#pragma mark -
#pragma mark SVD

TYPED_TEST(TestLinearAlgebra, SingularValueDecomposition) {
  // Generate random matrix
  cv::Mat A = cv::Mat(800, 533, cv::DataType<TypeParam>::type);
  cv::randn(A, 0.0, 2.0);
  // Init SVD
  using LA = typename LTS5::LinearAlgebra<TypeParam>;
  using SVDDecomp = typename LTS5::LinearAlgebra<TypeParam>::SvdDecomposition;
  SVDDecomp svd;
  svd.Init(A, LA::Lapack::SVDType::kFullUV);
  // Compute
  svd.Compute();
  EXPECT_TRUE(svd.p_.kInfo == 0);
  // Reconstruct
  cv::Mat U, S, Vt;
  svd.Get(&U, &S, &Vt);
  cv::Mat Arec = (U * S * Vt);
  // Compute diff
  TypeParam diff = (TypeParam)cv::norm(A - Arec) / A.total();
  TypeParam thrsh = sizeof(TypeParam) == 4 ? 1e-6 : 1e-14;
  EXPECT_LT(diff, thrsh);
}

TYPED_TEST(TestLinearAlgebra, SingularValueDecompositionEigen) {
  using LA = typename LTS5::LinearAlgebra<TypeParam>;
  using Matrix = typename LA::Matrix;
  using SVDDecomp = typename LA::SvdDecomposition;
  // Generate random matrix
  Matrix A = Matrix::Random(800, 533);
  // Init SVD
  SVDDecomp svd;
  svd.Init(A, LA::Lapack::SVDType::kFullUV);
  // Compute
  svd.Compute();
  EXPECT_TRUE(svd.p_.kInfo == 0);
  // Reconstruct
  Matrix U, S, Vt;
  svd.Get(&U, &S, &Vt);
  Matrix Arec = (U * S * Vt);
  // Compute diff
  TypeParam N = static_cast<TypeParam>(A.cols() * A.rows());
  TypeParam diff = (A - Arec).norm() / N;
  TypeParam thrsh = sizeof(TypeParam) == 4 ? 1e-6 : 1e-14;
  EXPECT_LT(diff, thrsh);
}

TYPED_TEST(TestLinearAlgebra, DivideConquerSingularValueDecomposition) {
  // Generate random matrix
  cv::Mat A = cv::Mat(800, 533, cv::DataType<TypeParam>::type);
  cv::randn(A, 0.0, 2.0);
  // Init SVD
  using LA = typename LTS5::LinearAlgebra<TypeParam>;
  using SVDDecomp = typename LTS5::LinearAlgebra<TypeParam>::DCSvdDecomposition;
  SVDDecomp svd;
  svd.Init(A, LA::Lapack::SVDType::kFullUV);
  // Compute
  svd.Compute();
  EXPECT_TRUE(svd.p_.kInfo == 0);
  // Reconstruct
  cv::Mat U, S, Vt;
  svd.Get(&U, &S, &Vt);
  cv::Mat Arec = (U * S * Vt);
  // Compute diff
  TypeParam diff = (TypeParam)cv::norm(A - Arec) / A.total();
  TypeParam thrsh = sizeof(TypeParam) == 4 ? 1e-6 : 1e-14;
  EXPECT_LT(diff, thrsh);
}

TYPED_TEST(TestLinearAlgebra, DivideConquerSingularValueDecompositionEigen) {
  using LA = typename LTS5::LinearAlgebra<TypeParam>;
  using Matrix = typename LA::Matrix;
  using SVDDecomp = typename LA::DCSvdDecomposition;
  // Generate random matrix
  Matrix A = Matrix::Random(800, 533);
  // Init SVD
  SVDDecomp svd;
  svd.Init(A, LA::Lapack::SVDType::kFullUV);
  // Compute
  svd.Compute();
  EXPECT_TRUE(svd.p_.kInfo == 0);
  // Reconstruct
  Matrix U, S, Vt;
  svd.Get(&U, &S, &Vt);
  Matrix Arec = (U * S * Vt);
  // Compute diff
  TypeParam N = static_cast<TypeParam>(A.cols() * A.rows());
  TypeParam diff = (A - Arec).norm() / N;
  TypeParam thrsh = sizeof(TypeParam) == 4 ? 1e-6 : 1e-14;
  EXPECT_LT(diff, thrsh);
}

#pragma mark -
#pragma mark Least Square

TYPED_TEST(TestLinearAlgebra, LeastSquareOverDetermined) {
  std::vector<TypeParam> a = {1.0, -4.0, 3.0, 2.0, 0.0, 5.0, 3.0, -2.0, 7.0,
                              8.0, -4.0, 3.0};
  std::vector<TypeParam> x = {7.0, 1.0, -6.0, -5.0, 9.0, -3.0};
  cv::Mat A = cv::Mat(4, 3, cv::DataType<TypeParam>::type, a.data());
  cv::Mat X = cv::Mat(3, 2, cv::DataType<TypeParam>::type, x.data());
  cv::Mat X1 = cv::Mat(4, 1, cv::DataType<TypeParam>::type, x.data());
  cv::Mat B = A * X;
  cv::Mat B1 = A.t() * X1;

  //Over determined system
  typename LTS5::LinearAlgebra<TypeParam>::LinearSolver solver;
  cv::Mat sol;
  solver.Solve(A, B, &sol);
  TypeParam diff = (TypeParam)cv::norm(X - sol);
  EXPECT_LE(diff, 1e-5);
}

TYPED_TEST(TestLinearAlgebra, LeastSquareUnderDetermined) {
  std::vector<TypeParam> a = {1.0, -4.0, 3.0, 2.0, 0.0, 5.0, 3.0, -2.0, 7.0,
                              8.0, -4.0, 3.0};
  std::vector<TypeParam> x = {7.0, 1.0, -6.0, -5.0, 9.0, -3.0};
  cv::Mat A = cv::Mat(4, 3, cv::DataType<TypeParam>::type, a.data());
  cv::Mat X = cv::Mat(3, 2, cv::DataType<TypeParam>::type, x.data());
  cv::Mat X1 = cv::Mat(4, 1, cv::DataType<TypeParam>::type, x.data());
  cv::Mat B = A * X;
  cv::Mat B1 = A.t() * X1;

  //Over determined system
  typename LTS5::LinearAlgebra<TypeParam>::LinearSolver solver;
  cv::Mat sol;
  solver.Solve(A.t(), B1, &sol);
  TypeParam diff = (TypeParam)cv::norm(A.t() * sol - B1);
  EXPECT_LE(diff, 1e-5);
}

TYPED_TEST(TestLinearAlgebra, ElemWiseVectorProduct) {
  // Input
  cv::Mat A(17, 1, cv::DataType<TypeParam>::type);
  cv::Mat x(17, 1, cv::DataType<TypeParam>::type);
  cv::randn(A, TypeParam(0.0), TypeParam(5.0));
  cv::randn(x, TypeParam(0.0), TypeParam(5.0));
  // Output
  cv::Mat ytrue, y;
  ytrue = A.mul(x);

  // Compute element-wise product
  LTS5::LinearAlgebra<TypeParam>::Sbmv(A,
                                       TypeParam(1.0),
                                       x,
                                       TypeParam(0.0),
                                       &y);
  // Compare
  TypeParam diff = (TypeParam)cv::norm(ytrue - y) / TypeParam(x.total());
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-6 : 1e-14;
  EXPECT_LE(diff, thr);
}

TYPED_TEST(TestLinearAlgebra, SquareLinearSystem) {
  using SqLinSolver = typename LTS5::LinearAlgebra<TypeParam>::SquareLinearSolver;
  cv::Mat A(78, 78, cv::DataType<TypeParam>::type);
  cv::Mat x(78, 1, cv::DataType<TypeParam>::type);
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  cv::randn(A, TypeParam(0.0), TypeParam(5.0));
  cv::randn(x, TypeParam(0.0), TypeParam(5.0));
  // Output
  cv::Mat y;
  y = A * x;
  // Solve system
  cv::Mat xhat;
  SqLinSolver solver;
  solver.Solve(A, y, &xhat);
  // Check value
  TypeParam diff = (TypeParam)cv::norm(xhat - x) / (TypeParam)x.total();
  TypeParam thr = sizeof(TypeParam) == 4 ? 1e-4 : 1e-12;
  EXPECT_LE(diff, thr);
}

TYPED_TEST(TestLinearAlgebra, QRDecomposition) {
  using T = TypeParam;
  using LA = LTS5::LinearAlgebra<T>;
  using QRDecomp = typename LA::QRDecomposition;
  // Generate data
  std::vector<int> mat_sizes = {5, 9};
  for (const auto sz : mat_sizes) {
    // Create random matrix
    cv::Mat A(sz, 6, cv::DataType<TypeParam>::type);
    cv::randn(A, T(0.0), T(1.0));
    { // Compute decomposition partial
      QRDecomp qr(A);
      // Get component
      cv::Mat Q, R;
      qr.GetComponents(false, &Q, &R);
      // Check if A = Q R
      cv::Mat Apred = Q * R;
      T diff = cv::norm(Apred - A);
      EXPECT_LE(diff, 1e-5);
    }
    { // Compute decomposition full
      QRDecomp qr(A);
      // Get componenet
      cv::Mat Q, R;
      qr.GetComponents(true, &Q, &R);
      // Check if A = Q R
      cv::Mat Apred = Q * R;
      T diff = cv::norm(Apred - A);
      EXPECT_LE(diff, 1e-5);
    }
    { // Pre-Multiplication
      QRDecomp qr(A);
      cv::Mat Q;
      qr.GetComponents(true, &Q, nullptr);
      // No transpose
      cv::Mat C1(7, A.rows, cv::DataType<T>::type);
      cv::randn(C1, T(0.0), T(1.0));
      cv::Mat C1i = C1.clone();
      int err = qr.MultiplyBy(QRDecomp::MultType::kPre, false, &C1);
      EXPECT_EQ(err, 0);
      cv::Mat C1true = C1i * Q;
      T diff1 = cv::norm(C1true - C1);
      EXPECT_LE(diff1, 1e-5);
      // With transpose
      cv::Mat C2(12, A.rows, cv::DataType<T>::type);
      cv::randn(C2, T(0.0), T(1.0));
      cv::Mat C2i = C2.clone();
      err = qr.MultiplyBy(QRDecomp::MultType::kPre, true, &C2);
      EXPECT_EQ(err, 0);
      cv::Mat C2true = C2i * Q.t();
      T diff2 = cv::norm(C2true - C2);
      EXPECT_LE(diff2, 1e-5);
    }
    { // Post-Multiplication
      QRDecomp qr(A);
      cv::Mat Q;
      qr.GetComponents(true, &Q, nullptr);
      // No transpose
      cv::Mat C1(A.rows, 7, cv::DataType<T>::type);
      cv::randn(C1, T(0.0), T(1.0));
      cv::Mat C1i = C1.clone();
      int err = qr.MultiplyBy(QRDecomp::MultType::kPost, false, &C1);
      EXPECT_EQ(err, 0);
      cv::Mat C1true = Q * C1i;
      T diff1 = cv::norm(C1true - C1);
      EXPECT_LE(diff1, 1e-5);
      // With transpose
      cv::Mat C2(A.rows, 12, cv::DataType<T>::type);
      cv::randn(C2, T(0.0), T(1.0));
      cv::Mat C2i = C2.clone();
      err = qr.MultiplyBy(QRDecomp::MultType::kPost, true, &C2);
      EXPECT_EQ(err, 0);
      cv::Mat C2true = Q.t() * C2i;
      T diff2 = cv::norm(C2true - C2);
      EXPECT_LE(diff2, 1e-5);
    }
  }
}

int main(int argc, const char * argv[]) {
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  return RUN_ALL_TESTS();
}


