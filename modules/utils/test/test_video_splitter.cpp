/**
 *  @file   test_video_splitter.cpp
 *  @brief  Test video splitting into smaller section
 *  @author Christophe Ecabert
 *  @date   03/09/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <utility>

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"

#include "lts5/utils/sys/file_system.hpp"

/**
 *  @name printUsage
 *  @fn void printUsage(int argc, const char * argv[])
 *  @brief  Print how to use executable
 *  @param[in]  argc  Number of arguments
 *  @param[in]  argv  Arguments list
 */
void printUsage(int argc, const char * argv[]);

/**
 *  @name   ReadNode
 *  @brief  Search a key in a given node and output its value if present
 *  @param[in]  node    Node to search in
 *  @param[in]  key     Key to search for
 *  @param[out] value   Value associated for the given key
 */
template<class T>
void ReadNode(const cv::FileNode& node, const std::string& key, T* value);

/**
 *  @name ParseConfiguration
 *  @fn int ParseConfiguration(const std::String& config_file,
                               int* fps,
                               std::vector<std::pair<int,int>>* region,
                               std::string* image_ext)
 *  @brief  Parse configuration
 *  @param[in]  config_file Configuration file
 *  @param[out] fps         Video FPS
 *  @param[out] region      List of region of interest
 *  @param[out] image_ext   Image extension desired
 */
int ParseConfiguration(const std::string& config_file,
                       int* fps,
                       std::vector<std::pair<int,int>>* region,
                       std::string* image_ext);

int main(int argc, const char * argv[]) {
  if (argc != 4) {
    printUsage(argc, argv);
    return -1;
  }
  // Ok process split config
  int fps;
  std::vector<std::pair<int, int>> video_region;
  std::string image_ext;
  int error = ParseConfiguration(argv[2], &fps, &video_region, &image_ext);
  if (!error) {
    // Ok, prepare output
    std::vector<std::string> paths;
    for (int i = 0; i < video_region.size(); ++i) {
      std::string num = std::to_string(i);
      std::string folder = (std::string(argv[3]) +
                            "segment_" +
                            std::string("0000",4 - num.length()) +
                            num);
      auto* fs = LTS5::GetDefaultFileSystem();
      if (!fs->FileExist(folder).Good()) {
        fs->CreateDirRecursively(folder);
      }
      paths.push_back(folder);
    }

    // Open video
    cv::VideoCapture cap(argv[1]);
    if (cap.isOpened()) {
      std::cout << "Split video : " << argv[1] << std::endl;
      // Process
      int cnt = 0;
      cv::Mat frame;
      while (cap.grab()) {
        // New frame coming
        auto cmp = [cnt](const std::pair<int, int>& pair) {
          return (cnt >= pair.first && cnt < pair.second);
        };
        auto pair_it = std::find_if(video_region.begin(),
                                    video_region.end(),
                                    cmp);
        if (pair_it != video_region.end()) {
          // Get frame
          cap.retrieve(frame);
          // Get sequence index
          int idx = (int)(pair_it - video_region.begin());
          std::string num = std::to_string(cnt);
          std::string image_name = paths[idx];
          image_name += "/frame_";
          image_name += std::string("00000000", 8 - num.length());
          image_name += num;
          image_name += image_ext;
          // write
          cv::imwrite(image_name, frame);
        }
        // Increase counter
        ++cnt;
      }
      // Release
      cap.release();
      std::cout << "Done" << std::endl;
    } else {
      std::cout << "Unable to load video : " << argv[1] << std::endl;
      error = -1;
    }
  }
    return error;
}


/*
 *  @name printUsage
 *  @fn void printUsage(int argc, const char * argv[])
 *  @brief  Print how to use executable
 *  @param[in]  argc  Number of arguments
 *  @param[in]  argv  Arguments list
 */
void printUsage(int argc, const char * argv[]) {
  std::cout << argv[0] << "<VideoIn> <SplitConfig> <OutputFolder>" << std::endl;
  std::cout << "<VideoIn>       Input video" << std::endl;
  std::cout << "<SplitConfig>   Section to split" << std::endl;
  std::cout << "<OutputFolder>  Output location" << std::endl;
}

/*
 *  @name   ReadNode
 *  @brief  Search a key in a given node and output its value if present
 *  @param[in]  node    Node to search in
 *  @param[in]  key     Key to search for
 *  @param[out] value   Value associated for the given key
 */
template<class T>
void ReadNode(const cv::FileNode& node, const std::string& key, T* value) {
  if (!node[key].empty()) {
    node[key] >> *value;
  }
}

/*
 *  @name ParseConfiguration
 *  @fn int ParseConfiguration(const std::String& config_file,
                               int* fps,
                               std::vector<std::pair<int,int>>* region,
                               std::string* image_ext)
 *  @brief  Parse configuration
 *  @param[in]  config_file Configuration file
 *  @param[out] fps         Video FPS
 *  @param[out] region      List of region of interest
 *  @param[out] image_ext   Image extension desired
 */
int ParseConfiguration(const std::string& config_file,
                       int* fps,
                       std::vector<std::pair<int,int>>* region,
                       std::string* image_ext) {
  int error = -1;
  cv::FileStorage storage(config_file, cv::FileStorage::READ);
  if (storage.isOpened()) {
    // Get root
    cv::FileNode root = storage.getFirstTopLevelNode();
    // Read config
    ReadNode(root, "fps", fps);
    // Read region
    std::string region_str;
    std::string delimiter = ";";
    std::string token;
    size_t p = 0;
    ReadNode(root, "split_region", &region_str);
    int begin = -1;
    int end = -1;
    while ((p = region_str.find(delimiter)) != std::string::npos) {
      token = region_str.substr(0, p);
      region_str.erase(0, p + delimiter.length());
      // split begin
      size_t mid = token.find("-");
      size_t t_pos = token.substr(0, mid).find(":");
      int min = std::atoi(token.substr(0, t_pos).c_str());
      int sec = std::atoi(token.substr(t_pos + 1, mid).c_str());
      // Define start
      begin = (min * 60 + sec) * *fps;
      // split end
      t_pos = token.substr(mid, token.length()).find(":");
      min = std::atoi(token.substr(mid + 1, mid + t_pos).c_str());
      sec = std::atoi(token.substr(mid + t_pos + 1, token.length()).c_str());
      end = (min * 60 + sec) * *fps;
      // Push into vector
      region->push_back(std::make_pair(begin, end));
    }
    // Extension
    ReadNode(root, "image_ext", image_ext);
    // Done
    storage.release();
    error = 0;
  }
  return error;
}









