/**
 *  @file   test_shape_estimator.cpp
 *  @brief  Testing target for shape_estimator_base class and derived classes
 *  @author Gabriel Cuendet
 *  @date   25/05/16
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <limits>
#include <string>
#include <vector>

#include "Eigen/SparseCore"

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/utils/math/sparse_linear_algebra_wrapper.hpp"

/**
 * @class   ShapeEstimatorTest
 * @brief   Test unit for Shape estimator classes methods
 * @author  Gabriel Cuendet
 * @date    25/05/16
 */
class SparseLinearAlgTest : public ::testing::Test {

 public:

  SparseLinearAlgTest(void) : Q_(9,9), D_(9,9) {
    // Result of the discrete graph laplacian on the toy mesh in test_halfedge_mesh.cpp
    std::vector<Eigen::Triplet<float> > D_vec{Eigen::Triplet<float>(0, 0, 0.5),
                                              Eigen::Triplet<float>(1, 1, 0.25),
                                              Eigen::Triplet<float>(2, 2, 0.2),
                                              Eigen::Triplet<float>(3, 3, 0.25),
                                              Eigen::Triplet<float>(4, 4, 0.5),
                                              Eigen::Triplet<float>(5, 5, 0.166667),
                                              Eigen::Triplet<float>(6, 6, 0.33333),
                                              Eigen::Triplet<float>(7, 7, 0.33333),
                                              Eigen::Triplet<float>(8, 8, 0.33333)};
    std::vector<Eigen::Triplet<float> > Q_vec{Eigen::Triplet<float>(0, 0, -2),
                                              Eigen::Triplet<float>(0, 1, 1),
                                              Eigen::Triplet<float>(0, 2, 1),
                                              Eigen::Triplet<float>(1, 0, 1),
                                              Eigen::Triplet<float>(1, 1, -4),
                                              Eigen::Triplet<float>(1, 2, 1),
                                              Eigen::Triplet<float>(1, 5, 1),
                                              Eigen::Triplet<float>(1, 6, 1),
                                              Eigen::Triplet<float>(2, 0, 1),
                                              Eigen::Triplet<float>(2, 1, 1),
                                              Eigen::Triplet<float>(2, 2, -5),
                                              Eigen::Triplet<float>(2, 3, 1),
                                              Eigen::Triplet<float>(2, 4, 1),
                                              Eigen::Triplet<float>(2, 5, 1),
                                              Eigen::Triplet<float>(3, 2, 1),
                                              Eigen::Triplet<float>(3, 3, -4),
                                              Eigen::Triplet<float>(3, 4, 1),
                                              Eigen::Triplet<float>(3, 5, 1),
                                              Eigen::Triplet<float>(3, 8, 1),
                                              Eigen::Triplet<float>(4, 2, 1),
                                              Eigen::Triplet<float>(4, 3, 1),
                                              Eigen::Triplet<float>(4, 4, -2),
                                              Eigen::Triplet<float>(5, 1, 1),
                                              Eigen::Triplet<float>(5, 2, 1),
                                              Eigen::Triplet<float>(5, 3, 1),
                                              Eigen::Triplet<float>(5, 5, -6),
                                              Eigen::Triplet<float>(5, 6, 1),
                                              Eigen::Triplet<float>(5, 7, 1),
                                              Eigen::Triplet<float>(5, 8, 1),
                                              Eigen::Triplet<float>(6, 1, 1),
                                              Eigen::Triplet<float>(6, 5, 1),
                                              Eigen::Triplet<float>(6, 6, -3),
                                              Eigen::Triplet<float>(6, 7, 1),
                                              Eigen::Triplet<float>(7, 5, 1),
                                              Eigen::Triplet<float>(7, 6, 1),
                                              Eigen::Triplet<float>(7, 7, -3),
                                              Eigen::Triplet<float>(7, 8, 1),
                                              Eigen::Triplet<float>(8, 3, 1),
                                              Eigen::Triplet<float>(8, 5, 1),
                                              Eigen::Triplet<float>(8, 7, 1),
                                              Eigen::Triplet<float>(8, 8, -3)};

    D_.setFromTriplets(D_vec.begin(), D_vec.end());
    Q_.setFromTriplets(Q_vec.begin(), Q_vec.end());

    //std::cout << "Matrix Q: " << Q_ << std::endl;
    // Q = [[-2,1,1,0,0,0,0,0,0];[1,-4,1,0,0,1,1,0,0];[1,1,-5,1,1,1,0,0,0];[0,0,1,-4,1,1,0,0,1];[0,0,1,1,-2,0,0,0,0];[0,1,1,1,0,-6,1,1,1];[0,1,0,0,0,1,-3,1,0];[0,0,0,0,0,1,1,-3,1];[0,0,0,1,0,1,0,1,-3]]
    //std::cout << "Matrix D: " << D_ << std::endl;
    // D = diag([0.5,0.25,0.2,0.25,0.5,0.1666667,0.33333,0.33333,0.33333])
    //std::cout << "-DQ: " << -D_*Q_ << std::endl;
  }

 protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

  /**< Stifness matrix **/
  Eigen::SparseMatrix<double> Q_;
  /**< Lumped mass matrix **/
  Eigen::SparseMatrix<double> D_;

};

/** Define unit test, NonSymShiftEigenVal */
TEST_F(SparseLinearAlgTest, NonSymShiftEigenVal) {
  // Eigendecomposition of -D^(-1)Q
  Eigen::SparseMatrix<double> A = -D_*Q_;

  LTS5::SparseLinearAlgebra::LuNonSymEigShift<double> decomp;
  LTS5::SparseLinearAlgebra::Arpack::non_sym_eigen_params<double>& params = decomp.get_params();
  params.nevp = 6;
  params.ncvp = A.rows() - 1;
  params.fs= 0.001;  // CAUTION: seems unstable if fs is too small

  Eigen::Matrix<double, Eigen::Dynamic, 1> eigvalues;
  decomp.Init(A);
  decomp.Compute();
  decomp.GetSortedEigenvalues(&eigvalues);
  //std::cout << eigvalues << std::endl;

  double tol = 1e-6;
  EXPECT_NEAR(eigvalues[0], 0.0, tol);
  EXPECT_NEAR(eigvalues[1], 0.450693482827754, tol);
  EXPECT_NEAR(eigvalues[2], 0.543563448265531, tol);
  EXPECT_NEAR(eigvalues[3], 0.999993999976000, tol);
  EXPECT_NEAR(eigvalues[4], 1.157053599525207, tol);
  EXPECT_NEAR(eigvalues[5], 1.278451037871239, tol);
}

/** Define unit test, NonSymShiftEigenVec */
TEST_F(SparseLinearAlgTest, NonSymShiftEigenVec) {
  // Eigendecomposition of -D^(-1)Q
  Eigen::SparseMatrix<double> A = -D_*Q_;

  LTS5::SparseLinearAlgebra::LuNonSymEigShift<double> decomp;
  LTS5::SparseLinearAlgebra::Arpack::non_sym_eigen_params<double>& params = decomp.get_params();
  params.nevp = 3;
  params.ncvp = A.rows() - 1;
  params.fs= 1e-3;  // CAUTION: seems unstable if fs is too small

  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> eigvec;
  decomp.Init(A);
  decomp.Compute();
  decomp.GetEigenvectors(&eigvec);
  std::cout << eigvec << std::endl;

  double tol = 1e-6;
  Eigen::RowVectorXd ground_truth(9);
  ground_truth << -1/3.0, -1/3.0, -1/3.0, -1/3.0, -1/3.0, -1/3.0, -1/3.0, -1/3.0, -1/3.0;

  EXPECT_NEAR(std::abs(ground_truth.dot(eigvec.row(0))), 1.0,
              std::numeric_limits<double>::epsilon());

  ground_truth << -0.427451556832019, -0.126747797169118, -0.342856054717383,
                  -0.126747797169114, -0.427451556832014, 0.166733381461153,
                  0.325080665999362, 0.495716805632314, 0.325080665999364;
  EXPECT_NEAR(std::abs(ground_truth.dot(eigvec.row(1))), 1.0, tol);

  ground_truth << 0.468519788681335, 0.427699113530142, 0.0,
                -0.427699113530143, -0.468519788681336, 0.0,
                0.312350245557015, 0.0, -0.312350245557013;
  EXPECT_NEAR(std::abs(ground_truth.dot(eigvec.row(2))), 1.0, tol);
}

/** Define unit test, NonSymShiftSortedEigenVec */
TEST_F(SparseLinearAlgTest, NonSymShiftSortedEigenVec) {
  // Eigendecomposition of -D^(-1)Q
  Eigen::SparseMatrix<double> A = -D_*Q_;

  LTS5::SparseLinearAlgebra::LuNonSymEigShift<double> decomp;
  LTS5::SparseLinearAlgebra::Arpack::non_sym_eigen_params<double>& params = decomp.get_params();
  params.nevp = 3;
  params.ncvp = A.rows() - 1;
  params.fs= 1.3;  // CAUTION: seems unstable if fs is too small

  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> eigvec;
  Eigen::Matrix<double, Eigen::Dynamic, 1> eigvalues;
  decomp.Init(A);
  decomp.Compute();
  decomp.GetEigenvalues(&eigvalues);
  //std::cout << eigvalues << std::endl;
  decomp.GetEigenvectors(&eigvec);
  // std::cout << eigvec << std::endl;

  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> sorted_eigvec;
  decomp.GetSortedEigenvectors(&sorted_eigvec);
  // std::cout << sorted_eigvec << std::endl;

  std::vector<int> sorted_idx(eigvalues.size());
  std::iota(sorted_idx.begin(), sorted_idx.end(), 0);

  auto eigval_sort = [&] (const int& a, const int& b) {
    return eigvalues[a]*eigvalues[a] < eigvalues[b]*eigvalues[b];
  };
  std::sort(sorted_idx.begin(), sorted_idx.end(), eigval_sort);

  EXPECT_EQ(sorted_eigvec.row(0), eigvec.row(sorted_idx[0]));
  EXPECT_EQ(sorted_eigvec.row(1), eigvec.row(sorted_idx[1]));
  EXPECT_EQ(sorted_eigvec.row(2), eigvec.row(sorted_idx[2]));
}

int main(int argc, const char * argv[]) {
  // Init test
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  // Run test
  return RUN_ALL_TESTS();
}
