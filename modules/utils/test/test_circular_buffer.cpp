/**
 *  @file   test_circular_buffer.cpp
 *  @brief  Circular buffer test unit
 *
 *  @author Christophe Ecabert
 *  @date   27/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>

#include "gtest/gtest.h"

#include "lts5/utils/circular_buffer.hpp"

/**
 * @class   CircularBufferTest
 * @brief   Testing unit for CircularBuffer class
 * @author  Christophe Ecabert
 * @date    27/05/16
 */
class CircularBufferTest : public ::testing::Test {
public:

  CircularBufferTest(void) : buffer_(5) {
  }

  /** Buffer */
  LTS5::CircularBuffer<int> buffer_;

protected:

  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }
};

/** Test constructor */
TEST_F(CircularBufferTest, TestConstructor) {
  // Test for empty buffer
  EXPECT_TRUE(buffer_.empty());
}

/** Test full */
TEST_F(CircularBufferTest, TestFull) {
  // Insert
  buffer_.push_back(1);
  buffer_.push_back(2);
  buffer_.push_back(3);
  buffer_.push_back(4);
  buffer_.push_back(5);
  // Test
  EXPECT_TRUE(buffer_.full());
}

/** Test Read */
TEST_F(CircularBufferTest, TestRead) {
  // Insert
  buffer_.push_back(1);
  buffer_.push_back(2);
  buffer_.push_back(3);
  // Read
  int v = buffer_.pop_back();
  EXPECT_EQ(v, 1);
  v = buffer_.pop_back();
  EXPECT_EQ(v, 2);
  v = buffer_.pop_back();
  EXPECT_EQ(v, 3);
}

/** Test iterator */
TEST_F(CircularBufferTest, TestIterator) {
  // Insert
  buffer_.push_back(1);
  buffer_.push_back(2);
  buffer_.push_back(3);
  buffer_.push_back(4);
  buffer_.push_back(5);
  // Read
  int v = buffer_.pop_back();
  v = buffer_.pop_back();
  v = buffer_.pop_back();
  EXPECT_EQ(buffer_.size(), 2);
  // Insert again
  buffer_.push_back(6);
  buffer_.push_back(7);
  // Get
  LTS5::CircularBuffer<int>::CircularIt first = buffer_.begin();
  LTS5::CircularBuffer<int>::CircularIt last = buffer_.end();
  int cnt = 0;
  int res[] = {4, 5, 6, 7};
  while (first != last) {
    EXPECT_EQ(*first, res[cnt]);
    ++first;
    ++cnt;
    ASSERT_LE(cnt, 4);
  }
}


int main(int argc, const char * argv[]) {
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  return ::RUN_ALL_TESTS();
}