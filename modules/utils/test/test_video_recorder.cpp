/**
 *  @file   test_video_recorder.cpp
 *  @brief  Record video from webcam and save video into specific file
 *  @author Christophe Ecabert
 *  @date   14/09/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>
#include <iostream>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

/**
 *  @name printUsage
 *  @fn void printUsage(int argc, const char * argv[])
 *  @brief  Print how to use executable
 *  @param[in]  argc  Number of arguments
 *  @param[in]  argv  Arguments list
 */
void printUsage(int argc, const char * argv[]);

int main(int argc, const char * argv[]) {
  if (argc != 2) {
    printUsage(argc, argv);
    return -1;
  }

  // Open camera
  cv::VideoCapture cap(0);
  if (!cap.isOpened()) {
    // Error,
    std::cout << "Unable to access webcam ..." << std::endl;
    return -1;
  }
  // Writer
  int frame_w = cap.get(CV_CAP_PROP_FRAME_WIDTH);
  int frame_h = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
  int frame_fps = cap.get(CV_CAP_PROP_FPS);
  frame_fps = frame_fps == 0 ? 10 : frame_fps;

  cv::VideoWriter writer(argv[1],
                         CV_FOURCC('D', 'I', 'V', 'X'),
                         frame_fps,
                         cv::Size(frame_w,frame_h));
  if (!writer.isOpened()) {
    std::cout << "Unable to access output file ..." << std::endl;
    return -1;
  }

  // Loop until quite
  bool run = true;
  bool is_recording = false;
  char key = ' ';
  cv::Mat frame;
  cv::namedWindow("Live feed");
  while (run) {

    // Grab frame
    cap >> frame;
    if (!frame.empty()) {
      // Show
      cv::imshow("Live feed", frame);

      // Record ?
      if (is_recording) {
        writer << frame;
      }
    }

    // User input
    key = static_cast<char>(cv::waitKey(5));
    if (key == 0x1B) {
      // Exit
      std::cout << "Quit application ... " << std::endl;
      run = false;
    } else if (key == 'r') {
      is_recording = !is_recording;
      std::cout << (is_recording ?
                    "Start recording ..." :
                    "Stop recording ...") << std::endl;
    }
  }

  // Clean up stuff !
  cap.release();
  writer.release();

  return 0;
}


/*
 *  @name printUsage
 *  @fn void printUsage(int argc, const char * argv[])
 *  @brief  Print how to use executable
 *  @param[in]  argc  Number of arguments
 *  @param[in]  argv  Arguments list
 */
void printUsage(int argc, const char * argv[]) {
  std::cout << argv[0] << " <OutputFilename>" << std::endl;
}