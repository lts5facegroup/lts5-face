//
//  test_ssift_descriptor.cpp
//  LTS5-Dev
//
//  Created by Christophe Ecabert on 06/05/15.
//  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <numeric>
#include <random>
#include <chrono>

#include "gtest/gtest.h"
#include "opencv2/highgui.hpp"

#include "lts5/utils/ssift.hpp"
#include "lts5/utils/cmd_parser.hpp"

static cv::Mat s_input_image;

/**
 *  @class  SSiftTest
 *  @brief  Test unit for SSift implementation
 *          Input/Output are CPU container (cv::Mat)
 *  @author Christophe Ecabert
 *  @date   06/05/15
 */
class SSiftTest : public ::testing::TestWithParam<int> {
 public:

  SSiftTest() : n_pts_(68), key_points_(n_pts_, cv::KeyPoint()) {
  }

  void ComputeReferenceSSift() {
    sift_params_.sift_root = false;
    LTS5::SSift::ComputeDescriptor(s_input_image,
                                   key_points_,
                                   sift_params_,
                                   &cpu_sift_);
  }

  void ComputeOptimizedSSift() {
    LTS5::SSift::ComputeDescriptorOpt(s_input_image,
                                      key_points_,
                                      sift_params_,
                                      &opt_sift_);
  }

  /** N sift pts */
  int n_pts_;
  /** Sample location */
  std::vector<cv::KeyPoint> key_points_;
  /** SSift param */
  LTS5::SSift::SSiftParameters sift_params_;
  /** CPU sift */
  cv::Mat cpu_sift_;
  /** GPU sift */
  cv::Mat opt_sift_;

 protected:

  void SetUp() override {
    // code here will execute just before the test ensues

    // Generate random pts
    unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now()
                                          .time_since_epoch()
                                          .count());
    std::mt19937_64 engine(seed);
    std::uniform_int_distribution<int> dist_w(0, s_input_image.cols - 1);
    std::uniform_int_distribution<int> dist_h(0, s_input_image.rows - 1);
    for (int p = 0; p < n_pts_; ++p) {
      int u = dist_w(engine);
      int v = dist_h(engine);
      key_points_[p].pt.x = static_cast<float>(u);
      key_points_[p].pt.y = static_cast<float>(v);
      key_points_[p].size = 32;
    }
  }
};

TEST_P(SSiftTest, Validation) {
  // Compute ref
  this->ComputeReferenceSSift();
  // New impl
  this->ComputeOptimizedSSift();
  // Error
  float diff = (float)cv::norm(cpu_sift_ - opt_sift_);
  // We tolerate that one element is different
  EXPECT_LE(diff, 1.f);
}

INSTANTIATE_TEST_CASE_P(SSift, SSiftTest, ::testing::Range(0, 50));




int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("-i",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Image path");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Init google test
    ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
    // Get option
    std::string image_path;
    parser.HasArgument("-i", &image_path);
    // Load data
    s_input_image = cv::imread(image_path);
    if (!s_input_image.empty()) {
      // Run test
      err = ::RUN_ALL_TESTS();
    } else {
      err = -1;
      std::cout << "Unable to load image : " << image_path << std::endl;
    }

  } else {
    std::cout << "Unable to parse cmd line" << std::endl;
  }
  return err;
}
