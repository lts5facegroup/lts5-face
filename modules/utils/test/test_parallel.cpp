/**
 *  @file   test_parallel.cpp
 *  @brief  Unit test for parallel processing
 *  @ingroup    utils
 *
 *  @author Christophe Ecabert
 *  @date   11/1/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <atomic>
#include <thread>
#include <vector>
#include <numeric>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "lts5/utils/sys/parallel.hpp"

TEST(TestParallel, Counter) {
  using Para = LTS5::Parallel;
  // Create counter
  std::atomic<int> cnt(0);
  // Define loop body
  auto body = [&cnt](const size_t& i) {
    // increase by one
    cnt.fetch_add(1);
  };
  // Increase counter in parallel
  Para::For(1024*32, body);

  // Check counter
  ASSERT_EQ(cnt.load(), 32 * 1024);
}

TEST(TestParallel, Container) {
  using Para = LTS5::Parallel;
  // Fill container from various thread
  std::vector<size_t> container(32 * 1024);
  std::vector<size_t> target(container.size());
  for (size_t i = 0; i < target.size(); ++i) {
    target[i] = 2 * i;
  }
  auto body = [&container](const size_t& i) {
    container[i] = 2 * i;
  };
  Para::For(container.size(), body);
  // Check
  ASSERT_THAT(container,
              testing::ElementsAreArray(target));
}

TEST(TestParallel, CriticalSection) {
  using Para = LTS5::Parallel;
  using Mutex = LTS5::Mutex;
  using Lock = LTS5::ScopedLock;

  // Create mutex
  Mutex m;
  // Define some data
  const size_t N = 32 * 1024;
  std::vector<int> data(N);
  std::iota(data.begin(), data.end(), 0);
  // Find maximum
  int max = -1;
  Para::For(N,
            [&](const size_t& k) {
              // Access elem, check if max in critical section
              const auto& value = data[k];
              {
                Lock lock(m);
                if (value > max) {
                  max = value;
                }
              }
            });
  // Check maximum founded
  ASSERT_EQ(max, N - 1);
}

int main(int argc, const char * argv[]) {
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  return RUN_ALL_TESTS();
}
