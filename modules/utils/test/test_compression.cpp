/**
 *  @file   test_compression.cpp
 *  @brief  Testing target for data compression using zlib
 *  @author Christophe Ecabert
 *  @date   06/11/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>
#include <iostream>
#include <random>
#include <chrono>
#include <algorithm>
#include <functional>
#include <type_traits>

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "opencv2/core/core.hpp"

#include "lts5/utils/file_io.hpp"

/**
 *  @class  CompressionTest
 *  @brief  Test compression
 *  @author Christophe Ecabert
 *  @date   06/11/15
 */
template<typename T>
class TestCompression : public ::testing::Test {
 public:

  TestCompression(void) {}

  int InMemoryCompression(void) {
    const int nbytes = ground_truth_.size() * sizeof(T);
    int err = LTS5::CompressMemoryBuffer((void*)ground_truth_.data(),
                                         nbytes,
                                         &compressed_data_);
    if (!err) {
      // Try to uncompress
      std::vector<unsigned char> uncompressed_data;
      err = LTS5::UncompressMemoryBuffer((const void*)compressed_data_.data(),
                                         (int)compressed_data_.size(),
                                         &uncompressed_data_);
    }
    return err;
  }

  void FileCompression(void) {
    // Format ground truth
    ground_truth_matrix_ = cv::Mat(dim_,
                                   dim_,
                                   cv::DataType<T>::type,
                                   ground_truth_.data());
    // Compress
    std::ofstream out("compressed.bin", std::ios::binary);
    if (out.is_open()) {
      LTS5::WriteCompressedMatToBin(out, ground_truth_matrix_);
      out.close();
      // Read
      std::ifstream in("compressed.bin", std::ios::binary);
      LTS5::ReadCompressedMatFromBin(in, &uncompressed_data_matrix_);
    }

  }

  void Compare(void) {
    size_t n_elem = uncompressed_data_.size() / sizeof(T);
    std::vector<T> data((T*)uncompressed_data_.data(),
                        (T*)uncompressed_data_.data() + n_elem);
    ASSERT_THAT(data, ::testing::ElementsAreArray(ground_truth_));
  }

  void CompareMatrix(void) {
    T diff = (T)cv::norm(ground_truth_matrix_ - uncompressed_data_matrix_);
    EXPECT_LT(diff, 1e-4);
  }

  /** dimension */
  int dim_;
  /** GroundTruth */
  std::vector<T> ground_truth_;
  /** Compressed data */
  std::vector<unsigned char> compressed_data_;
  /** Uncompressed data */
  std::vector<unsigned char> uncompressed_data_;

  /** GroundTruth matrix */
  cv::Mat ground_truth_matrix_;
  /** Compressed data matrix */
  cv::Mat compressed_data_matrix_;
  /** Uncompressed data matrix */
  cv::Mat uncompressed_data_matrix_;

  void SetUp(const int dim);

  /*void SetUp(const int dim) {

  }

  void SetUp(const int dim) {
    // Get dimension
    dim_ = dim;
    ground_truth_.resize(dim * dim, T(-1));

    // Generate data
    unsigned seed = static_cast<unsigned>(std::
    chrono::
    system_clock::
    now().time_since_epoch().count());
    std::mt19937_64 gen(seed);
    std::normal_distribution<T> distrib(T(0.0), T(5.0));
    auto functor = std::bind(distrib, gen);
    // Generate random vector
    std::generate_n(ground_truth_.begin(), ground_truth_.size(), functor);
  }*/
};

template<typename T>
void TestCompression<T>::SetUp(const int dim) {
  ASSERT_TRUE(false);
}

template<>
void TestCompression<short>::SetUp(const int dim) {
  // Get dimension
  dim_ = dim;
  ground_truth_.resize(dim * dim, -1);

  // Generate data
  unsigned seed = static_cast<unsigned>(std::
  chrono::
  system_clock::
  now().time_since_epoch().count());
  std::mt19937_64 gen(seed);
  std::uniform_int_distribution<short> distrib(-10, 10);
  auto functor = std::bind(distrib, gen);
  // Generate random vector
  std::generate_n(ground_truth_.begin(), ground_truth_.size(), functor);
}

template<>
void TestCompression<int>::SetUp(const int dim) {
  // Get dimension
  dim_ = dim;
  ground_truth_.resize(dim * dim, -1);

  // Generate data
  unsigned seed = static_cast<unsigned>(std::
  chrono::
  system_clock::
  now().time_since_epoch().count());
  std::mt19937_64 gen(seed);
  std::uniform_int_distribution<int> distrib(-10, 10);
  auto functor = std::bind(distrib, gen);
  // Generate random vector
  std::generate_n(ground_truth_.begin(), ground_truth_.size(), functor);
}

template<>
void TestCompression<double>::SetUp(const int dim) {
  // Get dimension
  dim_ = dim;
  ground_truth_.resize(dim * dim, -1);

  // Generate data
  unsigned seed = static_cast<unsigned>(std::
  chrono::
  system_clock::
  now().time_since_epoch().count());
  std::mt19937_64 gen(seed);
  std::normal_distribution<double> distrib(0.0, 5.0);
  auto functor = std::bind(distrib, gen);
  // Generate random vector
  std::generate_n(ground_truth_.begin(), ground_truth_.size(), functor);
}

template<>
void TestCompression<float>::SetUp(const int dim) {
  // Get dimension
  dim_ = dim;
  ground_truth_.resize(dim * dim, -1.f);

  // Generate data
  unsigned seed = static_cast<unsigned>(std::
  chrono::
  system_clock::
  now().time_since_epoch().count());
  std::mt19937_64 gen(seed);
  std::normal_distribution<float> distrib(0.f, 5.f);
  auto functor = std::bind(distrib, gen);
  // Generate random vector
  std::generate_n(ground_truth_.begin(), ground_truth_.size(), functor);
}

typedef ::testing::Types<short, int, float, double> ListType;
TYPED_TEST_CASE(TestCompression, ListType);

#pragma mark -
#pragma mark InMemory

/** Dot product test case */
TYPED_TEST(TestCompression, InMemory10) {
  // Setup
  this->SetUp(10);
  // Compress+Uncompress
  this->InMemoryCompression();
  // Compare
  this->Compare();
}

TYPED_TEST(TestCompression, InMemory32) {
  // Setup
  this->SetUp(32);
  // Compress+Uncompress
  this->InMemoryCompression();
  // Compare
  this->Compare();
}

TYPED_TEST(TestCompression, InMemory100) {
  // Setup
  this->SetUp(100);
  // Compress+Uncompress
  this->InMemoryCompression();
  // Compare
  this->Compare();
}

TYPED_TEST(TestCompression, InMemory320) {
  // Setup
  this->SetUp(320);
  // Compress+Uncompress
  this->InMemoryCompression();
  // Compare
  this->Compare();
}

#pragma mark -
#pragma mark InFile

TYPED_TEST(TestCompression, InFile10) {
  // Setup
  this->SetUp(320);
  // Compress+Uncompress
  this->FileCompression();
  // Compare
  this->CompareMatrix();
}

int main(int argc, const char * argv[]) {

  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
  return RUN_ALL_TESTS();
}

