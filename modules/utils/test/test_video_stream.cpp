//
//  test_video_stream.cpp
//  LTS5-Lib
//
//  Created by Christophe Ecabert on 12/11/15.
//  Copyright © 2015 Ecabert Christophe. All rights reserved.
//

#include <stdio.h>
#include <iostream>

#include "lts5/utils/webcam_video_stream.hpp"

int main(int argc, const char * argv[]) {

  int error = -1;
  LTS5::BaseVideoStream* stream = new LTS5::WebcamStream();
  bool ret = stream->Open(0);
  if (ret) {
    error = 0;

    // Grab till no more frame are available
    cv::Mat frame;
    char key;
    while (stream->Grab()) {
      // Retrieve
      stream->Retrieve(&frame);


      cv::imshow("view", frame);
      key = cv::waitKey(5);
      if (key == 'q') {
        break;
      }
    }


  }
  delete stream;
  return error;
}