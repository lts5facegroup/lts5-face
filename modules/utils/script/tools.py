# coding=utf-8
""" Utility function """
import sys
from argparse import ArgumentTypeError
__author__ = 'Christophe Ecabert'


def check_py_version(major=3, minor=None):
  """
  Check if python interpreter has the required version

  :param major:    Major version number
  :param minor:    Minor version number
  """
  minorv = 0 if minor is None else int(minor)
  if sys.version_info[0] != major or (sys.version_info[0] == major and
                                      sys.version_info[1] < minorv):
    raise Exception('This script uses Python %d.%d' % (major, minorv))


def init_logger(use_native_lts=True):
  """
  Initialize logger

  See: https://github.com/borntyping/python-colorlog

  :param use_native_lts: Indicate if native LTS5 logger should be used, default
                         is `True`.
  :return: Logger instance
  """
  if use_native_lts:
    from lts5.utils.logger import Logger
    # Init logger
    return Logger(stream=sys.stdout)
  # Standard python logger
  import os
  import colorlog
  import logging
  import inspect
  frmt = "%(log_color)s%(levelname)-8s%(reset)s | %(message)s"
  formatter = colorlog.ColoredFormatter(fmt=frmt,
                                        datefmt=None,
                                        reset=True,
                                        log_colors={'DEBUG': 'white',
                                                    'INFO': 'green',
                                                    'WARNING': 'yellow',
                                                    'ERROR': 'red',
                                                    'CRITICAL': 'bold_red'},
                                        secondary_log_colors={},
                                        style='%')
  handler = colorlog.StreamHandler()
  handler.setFormatter(formatter)
  # Query caller filename, https://stackoverflow.com/questions/13699283
  frame = inspect.stack()[1]
  module = inspect.getmodule(frame[0])
  script_name = os.path.basename(module.__file__).strip().split('.')[0]
  logger = colorlog.getLogger(str(script_name) + 'Logger')
  logger.addHandler(handler)
  logger.setLevel(logging.DEBUG)
  return logger


def search_folder(folder, ext, skip=None, relative=True):
  """
  Search recursively for files with specific extension in a given folder

  :param folder:      Root folder where to start to search
  :param ext:         List if extensions of interest
  :param skip:        List of element to avoid during the search
  :param relative:    Indicate if path are relative to `folder`
  :return:            List of files matching the extension
  """
  import os

  def _need_skip(fname, skip):
    if skip is not None:
      for sk in skip:
        if sk in fname:
          return True
    return False

  selected = []
  start = len(folder) + 1
  for root, _, files in os.walk(folder):
    for f in files:
      _, e = os.path.splitext(f)
      if len(e) and e in ext and not _need_skip(f, skip):
        n = start if relative else 0
        fname = os.path.join(root, f)[n:]
        selected.append(fname)
  # Sort
  selected.sort()
  return selected


def str2bool(v):
  """ https://stackoverflow.com/a/43357954 """
  if isinstance(v, bool):
    return v
  if v.lower() in ('yes', 'true', 't', 'y', '1'):
    return True
  elif v.lower() in ('no', 'false', 'f', 'n', '0'):
    return False
  else:
    raise ArgumentTypeError('Boolean value expected.')