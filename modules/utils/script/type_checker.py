# coding=utf-8
""" Decorator to check if a function has the correct type of parameters """
from functools import wraps
from lts5.utils.exception import InvalidNumberArgumentError
from lts5.utils.exception import InvalidArgumentError
from lts5.utils.exception import InvalidReturnTypeError
__author__ = 'Christophe Ecabert'


def _ordinal(num):
  """
  Returns the ordinal number of a given integer, as a string.
  eg. 1 -> 1st, 2 -> 2nd, 3 -> 3rd, etc.

  :param num: Number to convert
  :return:    Ordinal number as string
  """
  if 10 <= num % 100 < 20:
    return '{0}th'.format(num)
  suffix = {1: 'st', 2: 'nd', 3: 'rd'}.get(num % 10, 'th')
  return '{0}{1}'.format(num, suffix)


def accepts(*args_type):
  """
  A decorator to validate the parameter types of a given function.
  It is passed a tuple of types. eg. (<type 'tuple'>, <type 'int'>)

  Note: It doesn't do a deep check, for example checking through a
        tuple of types. The argument passed must only be types.

  :param args_type:   Type of arguments needed by the function
  :return:    Decorator
  """
  def _accept_decorator(func):
    """
    Check if the number of arguments to the validator function is the same
    as the arguments provided to the actual function to validate. We don't
    need to check if the function to validate has the right amount of
    arguments, as Python will do this automatically (also with a
    TypeError).
    :param func: Function to wrap
    :return: Wrapped function
    """
    @wraps(func)
    def _decorator_wrapper(*args, **kwargs):
      """ Decorator wrapper """
      if len(args) + len(kwargs) != len(args_type):
        # Number of arguments does not match
        raise InvalidNumberArgumentError(func.__name__)

      # We're using enumerate to get the index, so we can pass the
      # argument number with the incorrect type to InvalidArgumentError.
      arg = list(args) + list(kwargs.values())
      for k, (func_type, arg_type) in enumerate(zip(arg, args_type)):
        if not isinstance(func_type, arg_type) and \
           not issubclass(arg_type, func_type.__class__):
          # Type is wrong
          ordnum = _ordinal(k + 1)
          raise InvalidArgumentError(ordnum, func.__name__, arg_type)
      return func(*args, **kwargs)
    return _decorator_wrapper
  return _accept_decorator


def returns(*ret_type):
  """

  Validates the return type. Since there's only ever one return type, this
  makes life simpler. Along with the  accepts() decorator, this also only
  does a check for the top argument. For example you couldn't check
  (<type 'tuple'>, <type 'int'>, <type 'str'>). In that case you could only
  check if it was a tuple.

  :param ret_type:   Expected returned types
  :return:    Decorator
  """
  def _returns_decorator(func):
    """ Craete decorator for a given `func` """
    # No return type has been specified.
    if not ret_type:
      raise TypeError('Returned type must be specified')

    @wraps(func)
    def _decorator_wrapper(*args):
      """ Decorator wrapper """
      # More than one return type has been specified.
      if len(ret_type) > 1:
        raise TypeError('Only one return type can be specified')

      # Since the decorator receives a tuple of arguments
      # and the is only ever one object returned, we'll just
      # grab the first parameter.
      accepted_type = ret_type[0]

      # We'll execute the function, and take a look at the return type.
      rvalue = func(*args)
      if not isinstance(rvalue, accepted_type):
        raise InvalidReturnTypeError(func.__name__, accepted_type)

      return rvalue
    return _decorator_wrapper
  return _returns_decorator
