# coding=utf-8
""" Query data from Pescho database """

import argparse
import os
from shutil import copyfile
import MySQLdb
from MySQLdb import OperationalError



def _search_in_folder(folder, exts, pattern=None):
  """
  Search within a given `folder` for files terminating with possible
  extensions `exts`.

  :param folder:  Folder where to start to search
  :param exts:    List of extensions of interest
  :param pattern: Required pattern in the filename
  :return:    List of corresponding files
  """
  selected = []
  if pattern is not None:
    def _pred(fname, ext):
      return pattern in fname and ext in exts
  else:
    def _pred(fname, ext):  # pylint: disable=unused-argument
      return ext in exts
  # Recursively goes through the folder tree
  for root, _, files in os.walk(folder):
    for f in files:
      _, e = os.path.splitext(f)
      if _pred(f, e):
        selected.append(os.path.join(root, f))

  return selected


def _execute_scripts_from_file(filename, db_cursor):
  """
  Run sql file
  :param filename:    Path to the *.sql file
  :param db_cursor:   Database cursor
  """
  # Open and read the file as a single buffer
  with open(filename, 'r') as fd:
    sql_content = fd.read()

    # all SQL commands (split on ';')
    sql_commands = sql_content.split(';')

    # Execute every command from the input file
    for cmd in sql_commands:
      # This will skip and report errors
      # For example, if the tables do not yet exist, this will skip over
      # the DROP TABLE commands
      try:
        db_cursor.execute(cmd)
      except OperationalError as msg:
        print('Command skipped: {}'.format(msg))


def _create_database(db_file):
  """
  Reconstruct MySql databse from a dump file and create a cursor to access it.

  :param db_file: Database dump
  :return:    Database cursor
  """
  # Connect to MySql server
  db = MySQLdb.connect(host='localhost', user='root', passwd='')
  db_cursor = db.cursor()
  # Create db
  _execute_scripts_from_file(filename=db_file, db_cursor=db_cursor)
  return db_cursor


def _gather_data(db_cursor, in_folder, out_folder):
  """
  Copy data of interest into a specified folder

  :param db_cursor:  Database cursor to iterate over
  :param in_folder:   Folder where images are stored
  :param out_folder:  Destination folder where data will be copied
  """
  gender_type = {0: 'undef',
                 1: 'male',
                 2: 'female',
                 3: 'unknown'}

  def _check_file_exist(in_f, p_id, suffix, l_zero):
    prefix = '0' * l_zero
    fname = os.path.join(in_f,
                         str(p_id),
                         'etape1',
                         '{}{}_photo_1_{}.jpg'.format(prefix,
                                                      p_id,
                                                      suffix))
    return fname, os.path.exists(fname)

  def _has_images(in_f, p_id, l_zero):
    """ Check if image exist """
    f_exist = False
    f_name = ''
    p_exist = False
    p_name = ''
    for k in range(l_zero):
      if not f_exist:
        f_name, f_exist = _check_file_exist(in_f, p_id, 'front', k)
      if not p_exist:
        p_name, p_exist = _check_file_exist(in_f, p_id, 'profile', k)
      if f_exist and p_exist:
        # early stop if both files exists
        break
    return (f_name, p_name), f_exist and p_exist

  def _is_meta_data_valid(**kwargs):
    valid = True
    for key, value in kwargs.items():
      if key == 'weight' or key == 'height':
        valid &= value is not None
    return valid

  # Check if dest exist
  if not os.path.exists(out_folder):
    os.makedirs(out_folder)
  # Create label file
  with open(os.path.join(out_folder, 'labels.csv'), 'w') as f:
    # Write header
    hdr = 'ID,Gender,Age,Height,Weight,FrontalImagePath,ProfileImagePath\n'
    f.write(hdr)
    # Iterate over db entries
    cmd = "SELECT patient_id, weight, height, age, gender FROM interventions " \
          "ORDER BY patient_id"
    db_cursor.execute(cmd)
    n_row = db_cursor.rowcount
    for _ in range(n_row):
      # Get entry
      p_id, weight, height, age, gender = db_cursor.fetchone()
      # Check images
      img_path, img_valid = _has_images(in_f=in_folder,
                                        p_id=p_id,
                                        l_zero=4)
      meta_valid = _is_meta_data_valid(weight=weight,
                                       height=height,
                                       age=age,
                                       gender=gender)
      if img_valid and meta_valid is True:
        # Copy data
        rel_img_path = []
        for img in img_path:
          dest = os.path.join(out_folder, img[len(in_folder)+1:])
          if not os.path.exists(os.path.dirname(dest)):
            os.makedirs(os.path.dirname(dest))
          # Copy only if not there
          if not os.path.exists(dest):
            copyfile(img, dest)

          rel_img_path.append(dest[len(out_folder)+1:])
        # Write labels
        f.write('{},{},{},{},{},{},{}\n'.format(p_id,
                                                gender_type[gender],
                                                age,
                                                height,
                                                weight,
                                                rel_img_path[0],
                                                rel_img_path[1]))
      else:
        print('Drop patient {}, no images/meta-data founded'.format(p_id))

if __name__ == '__main__':

  desc = 'Extract Images and meta-data from the Pescho database.'
  parser = argparse.ArgumentParser(description=desc)

  # Image root folder
  parser.add_argument('-i',
                      type=str,
                      dest='img_folder',
                      help='Folder where images are stored',
                      required=True)
  # Database file
  parser.add_argument('-d',
                      type=str,
                      dest='db_file',
                      help='MySql database dump file (*.sql)',
                      required=True)
  # Output folder
  parser.add_argument('-o',
                      type=str,
                      dest='out_folder',
                      help='Location where to dump the data (folder)',
                      required=True)

  # Parse
  args = parser.parse_args()

  # Search for images
  # images = _search_in_folder(args.img_folder,
  #                            ['.png', '.jpg'],
  #                            'photo_1')
  # Create db
  cursor = _create_database(args.db_file)
  # Gather data
  _gather_data(db_cursor=cursor,
               in_folder=args.img_folder,
               out_folder=args.out_folder)
