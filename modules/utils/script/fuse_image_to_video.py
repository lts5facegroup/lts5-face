# coding=utf-8
""" Fuse image sequences into a video file """
import os
import argparse
import cv2
import numpy as np
import lts5.utils as utils

__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
# Define Logger
logger = utils.init_logger()


def scan_for_folder(folder):
  """
  Scan a given folder for child folders
  :param folder:   Root folder
  :return:        List of subsequent folders (can be empty if none)
  """
  root = folder if folder[-1] == '/' else folder + '/'
  folders = [f for f in os.listdir(root) if \
             os.path.isdir(os.path.join(root, f))]
  if not folders:
    return [root]
  list_f = [root + s for s in folders]
  list_f.sort()
  return list_f


def scan_for_image(folders, extension):
  """
  Search for images inside a list of folders
  :param folders: List of folders to scabn
  :param extension: extension to look for
  :return: List of file matching
  """
  images = []
  for folder in folders:
    list_f = []
    for path, _, files in os.walk(folder):
      for f in files:
        if f.endswith(extension):
          list_f.append(os.path.join(path, f))
    list_f.sort()
    images.append(list_f)
  return images


def generate_video(arguments):
  """
  Generate video
  :param arguments: Parsed arguments
  """
  # Have we one or multiple view ? -> Scan root and check if folder of not
  folders = scan_for_folder(folder=arguments.root)
  images = scan_for_image(folders=folders, extension='.jpg')
  if images:
    # Define output dimensions
    max_h = 0
    min_h = 1e6
    imgs = []
    for _, image in images:
      img = cv2.imread(image[0])
      imgs.append(img)
      max_h = img.shape[0] if img.shape[0] > max_h else max_h
      min_h = img.shape[0] if img.shape[0] < min_h else min_h
    scaling = float(min_h) / float(max_h)
    height = min_h
    width = 0
    for image in imgs:
      s = 1.0 if image.shape[1] == height else scaling
      width += int(image.shape[1] * s)
    del imgs
    # Open writer
    writer = 0
    fourcc = cv2.cv.CV_FOURCC(*'MJPG')  # mp4v
    writer = cv2.VideoWriter(filename=arguments.output,
                             fourcc=fourcc,
                             fps=30.0,
                             frameSize=(width, height),
                             isColor=False)
    if not writer.isOpened():
      logger.error('Can not create video : %s', arguments.output)
      exit(-1)
    # Load image
    for image in images:
      frames = []
      for img_path in image:
        print('Load frame : ' + img_path)
        img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        if img.shape[0] != height:
          img = cv2.resize(img,
                           (int(img.shape[1] * scaling),
                            int(img.shape[0] * scaling)))
        frames.append(img)
      # Dump
      if len(frames) > 1:
        # fuse
        for k, f in enumerate(frames):
          if k == 0:
            frame = f
          else:
            frame = np.concatenate((frame, f), axis=1)
        # write
        writer.write(frame)
      else:
        # Write without concern
        writer.write(frames[0])
    # Release writer
    if writer.isOpened():
      writer.release()

  else:
    logger.error('Unable to find images in : %s', arguments.root)


if __name__ == '__main__':
  # Parser
  parser = argparse.ArgumentParser(description='Transform image sequence '
                                               'into video')
  # Input
  parser.add_argument('-i',
                      type=str,
                      required=True,
                      dest='root',
                      help='Root folder where images are stored. Can have '
                           'dedicated folder for each views')
  # Output filname
  parser.add_argument('-o',
                      type=str,
                      required=True,
                      dest='output',
                      help='Video filename to be created')
  # Video format
  parser.add_argument('-s',
                      type=str,
                      required=False,
                      dest='scale',
                      help='Video resolution in format : \"WxH\"')
  # Parse
  args = parser.parse_args()
  # Generate video
  generate_video(arguments=args)
