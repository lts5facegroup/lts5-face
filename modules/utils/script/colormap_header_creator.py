""" Create colormap header file
colormap file definition provided by: Paul Bourke

Link:
- http://paulbourke.net/texture_colour/colourspace/
"""
import argparse
import re
__author__ = 'Christophe Ecabert'


def compare_color_tuple(item1, item2):
  """
  Compare tuple
  :param item1: Color A
  :param item2: Color B
  :return: 0 if equal, +- 1 otherwise
  """
  if item1 == item2:
    return 0
  elif item1 < item2:
    return -1
  return 1


class ColorMapCreator:
  """ Colormap header generator """

  def __init__(self):
    """ Constructor """
    self.data = []

  def load(self, input_file):
    """
    Load lookup table
    :param input_file:  File storing lookup table
    :return: 0 if no error occurs, -1 otherwise
    """
    with open(input_file, 'r') as f_in:
      for v in f_in:
        match = re.findall(r'([0-9]+)', v)
        match_size = len(match)
        if match_size % 4 == 0:
          for i in range(0, match_size, 4):
            self.data.append((float(match[i]),
                              map(float, match[i+1:i+4])))
        else:
          print('error missing data !')
          return -1
      # Sort
      self.data.sort(key=compare_color_tuple)
    return 0

  def save(self, output_name):
    """
    Dump into header file
    :param output_name:   Location
    """
    idx = output_name.rfind('.')
    if idx == -1:
      output_name += '.hpp'
      map_name = output_name
    else:
      ext = output_name[idx:]
      map_name = output_name[0:idx]
      if ext != '.hpp':
        output_name = output_name[:idx] + '.hpp'
    # Open
    with open(output_name, 'w') as f_out:
      # Define array name
      idx = map_name.rfind('/')
      map_name = map_name if idx == -1 else map_name[idx+1:]
      array_name = 'float ' + map_name + '_colormap[][3] = {\n'
      # Define length
      map_length = 'unsigned int ' + map_name + '_colormap_length = ' + \
                   str(len(self.data)) + ';\n'
      f_out.write(map_length)
      f_out.write(array_name)
      # Dump data
      for v in self.data:
        v_str = '  {' + str(v[1][0]/255.0) + ','
        v_str += str(v[1][1] / 255.0) + ','
        v_str += str(v[1][2] / 255.0) + '}'
        if v[0] != len(self.data) - 1:
          v_str += ',\n'
        f_out.write(v_str)
      f_out.write('};')


if __name__ == '__main__':
  desc = 'ColorMap header creator from file definition'
  parser = argparse.ArgumentParser(description=desc)
  parser.add_argument('-i',
                      nargs=1,
                      type=str,
                      dest='input',
                      help='Input file (.dat)',
                      required=True)
  parser.add_argument('-o',
                      nargs=1,
                      type=str,
                      dest='output',
                      help='Output filename',
                      required=True)
  args = parser.parse_args()
  # Create obj
  creator = ColorMapCreator()
  # Load
  creator.load(args.input[0])
  # Save
  creator.save(args.output[0])
