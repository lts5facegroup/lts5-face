# coding=utf-8
"""
Define various exception type

See:
- https://www.pythoncentral.io/validate-python-function-parameters-and-return-types-with-decorators/
"""
__author__ = 'Christophe Ecabert'


class InvalidArgumentError(ValueError):
  """
  Raised when the type of an argument to a function is not what it should be.
  """
  def __init__(self, argc, func, valid_type):
    """
    Constructor

    :param argc:    Argument number
    :param func:    Function name
    :param valid_type:    Accepted type
    """
    super().__init__()
    self._error = 'The {0} argument of {1}() is not a {2}'.format(argc,
                                                                  func,
                                                                  valid_type)

  def __str__(self):
    """
    Convert to string
    :return: Error message
    """
    return self._error


class InvalidNumberArgumentError(ValueError):
  """
  Raised when the number of arguments supplied to a function is incorrect.
  Note that this check is only performed from the number of arguments
  specified in the validate_accept() decorator. If the validate_accept()
  call is incorrect, it is possible to have a valid function where this
  will report a false validation.
  """
  def __init__(self, func):
    """
    Constructor

    :param func: Function name
    """
    super().__init__()
    self._error = 'Invalid number of arguments for {0}()'.format(func)

  def __str__(self):
    """
    Convert to string
    :return: Error message
    """
    return self._error


class InvalidReturnTypeError(ValueError):
  """
  As the name implies, the return value is the wrong type.
  """

  def __init__(self, func, rtype):
    """
    Constructor

    :param func:    Function's name
    :param rtype:   Returned type
    """
    super().__init__()
    self._error = 'Invalid return type {0} for {1}()'.format(rtype,
                                                             func)

  def __str__(self):
    return self._error
