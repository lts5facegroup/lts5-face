# coding=utf-8
""" Utility function for handling files """
import os
import tarfile
import zipfile
import shutil
import hashlib
from urllib.request import urlretrieve, urlopen
from urllib.error import HTTPError, URLError

__author__ = 'Christophe Ecabert'


def extract_archive(file_path, path='.', archive_format='auto'):
  """
  Extracts an archive if it matches tar, tar.gz, tar.bz, or zip formats.

  :param file_path:       Path to the archive file
  :param path:            Path to extract the archive file
  :param archive_format:  Archive format to try for extracting the file.
                          Options are 'auto', 'tar', 'zip', and None.
                          'tar' includes tar, tar.gz, and tar.bz files.
                          The default 'auto' is ['tar', 'zip'].
                          None or an empty list will return no matches found.
  :return:  True if a match was found and an archive extraction was completed,
        False otherwise.
  """
  if archive_format is None:
    return False
  if archive_format == 'auto':
    archive_format = ['tar', 'zip']
  if isinstance(archive_format, str):
    archive_format = [archive_format]

  for archive_type in archive_format:
    if archive_type == 'tar':
      open_fn = tarfile.open
      is_match_fn = tarfile.is_tarfile
    if archive_type == 'zip':
      open_fn = zipfile.ZipFile
      is_match_fn = zipfile.is_zipfile

    if is_match_fn(file_path):
      with open_fn(file_path) as archive:
        try:
          archive.extractall(path)
        except (tarfile.TarError, RuntimeError,
                KeyboardInterrupt):
          if os.path.exists(path):
            if os.path.isfile(path):
              os.remove(path)
            else:
              shutil.rmtree(path)
          raise
      return True
  return False


def hash_file(fpath, algorithm='sha256', chunk_size=65535):
  """
  Calculates a file sha256 or md5 hash.
  :param fpath:       Path to the file being validated
  :param algorithm:   Hash algorithm, one of 'auto', 'sha256', or 'md5'.
                      The default 'auto' detects the hash algorithm in use.
  :param chunk_size:  Bytes to read at a time, important for large files.
  :return:  The file hash
  """
  if (algorithm == 'sha256') or (algorithm == 'auto' and len(hash) == 64):
    hasher = hashlib.sha256()
  else:
    hasher = hashlib.md5()

  with open(fpath, 'rb') as fpath_file:
    for chunk in iter(lambda: fpath_file.read(chunk_size), b''):
      hasher.update(chunk)

  return hasher.hexdigest()


def validate_file(fpath, file_hash, algorithm='auto', chunk_size=65535):
  """
  Validates a file against a sha256 or md5 hash.
  :param fpath:       Path to the file being validated
  :param file_hash:   The expected hash string of the file. The sha256 and md5
                      hash algorithms are both supported.
  :param algorithm:   Hash algorithm, one of 'auto', 'sha256', or 'md5'.
                      The default 'auto' detects the hash algorithm in use.
  :param chunk_size:  Bytes to read at a time, important for large files.
  :return:  Whether the file is valid
  """
  if ((algorithm == 'sha256') or
      (algorithm == 'auto' and len(file_hash) == 64)):
    hasher = 'sha256'
  else:
    hasher = 'md5'

  if str(hash_file(fpath, hasher, chunk_size)) == str(file_hash):
    return True
  else:
    return False


def get_file(fname,
             origin,
             file_hash=None,
             cache_subdir='data',
             hash_algorithm='auto',
             extract=False,
             archive_format='auto',
             cache_dir=None):
  """ Downloads a file from a URL if it not already in the cache.

  By default the file at the url `origin` is downloaded to the cache_dir
  `~/.lts5`, placed in the cache_subdir `data`, and given the filename `fname`.
  The final location of a file `example.txt` would therefore be
  `~/.lts5/data/example.txt`. Files in tar, tar.gz, tar.bz, and zip formats can
  also be extracted. Passing a hash will verify the file after download. The
  command line programs `shasum` and `sha256sum` can compute the hash.

  :param fname: Name of the file. If an absolute path `/path/to/file.txt` is
            specified the file will be saved at that location.
  :param origin:  Original URL of the file.
  :param file_hash: The expected hash string of the file after download.
            The sha256 and md5 hash algorithms are both supported.
  :param cache_subdir:  Subdirectory under the LTS5 cache dir where the file is
            saved. If an absolute path `/path/to/folder` is
            specified the file will be saved at that location.
  :param hash_algorithm:  Select the hash algorithm to verify the file.
            options are 'md5', 'sha256', and 'auto'.
            The default 'auto' detects the hash algorithm in use.
  :param extract: True tries extracting the file as an Archive, like tar or zip.
  :param archive_format:  Archive format to try for extracting the file.
            Options are 'auto', 'tar', 'zip', and None.
            'tar' includes tar, tar.gz, and tar.bz files.
            The default 'auto' is ['tar', 'zip'].
            None or an empty list will return no matches found.
  :param cache_dir:  Location to store cached files, when None it
            defaults to the LTS5 Directory.
  :return:  Path to the downloaded file
  """
  if cache_dir is None:
    if 'LTS5_HOME' in os.environ:
      cache_dir = os.environ.get('LTS5_HOME')
    else:
      cache_dir = os.path.join(os.path.expanduser('~'), '.lts5')
  datadir_base = os.path.expanduser(cache_dir)
  if not os.access(datadir_base, os.W_OK):
    datadir_base = os.path.join('/tmp', '.lts5')
  datadir = os.path.join(datadir_base, cache_subdir)
  if not os.path.exists(datadir):
    os.makedirs(datadir)

  if extract:
    untar_fpath = os.path.join(datadir, fname)
    fpath = untar_fpath + '.tar.gz'
  else:
    fpath = os.path.join(datadir, fname)


  download = False
  if os.path.exists(fpath):
    # File found; verify integrity if a hash was provided.
    if file_hash is not None:
      if not validate_file(fpath, file_hash, algorithm=hash_algorithm):
        print('A local file was found, but it seems to be incomplete'
              ' or outdated because the {} file hash does not match '
              'the original value of {} so we will re-download the '
              'data.'.format(hash_algorithm, file_hash))
        download = True
  else:
    download = True

  if download:
    print('Downloading data from', origin)

    error_msg = 'URL fetch failure on {} : {} -- {}'
    try:
      try:
        urlretrieve(origin, fpath)
      except HTTPError as e:
        raise Exception(error_msg.format(origin, e.code, e.msg))
      except URLError as e:
        raise Exception(error_msg.format(origin, e.errno, e.reason))
    except (Exception, KeyboardInterrupt):
      if os.path.exists(fpath):
        os.remove(fpath)
      raise

  if extract:
    extract_archive(fpath, datadir, archive_format)

  return fpath
