# coding=utf-8
""" Interface for native logger """
import threading
import sys
import inspect
from lts5.utils.pyutils import Logger as lts_logger
# pylint: disable=invalid-name
Level = lts_logger.Level

__logger = None
__logger_lock = threading.Lock()


def _get_logger_instance(stream=None):
  """ Retrieve an instance of LTS logger """
  global __logger

  # Defined ?
  if __logger:
    return __logger
  # Nope, lock
  __logger_lock.acquire()
  try:
    if __logger:
      return __logger
    # Init logger
    log_stream = sys.stdout if stream is None else stream
    logger = lts_logger(stream=log_stream)
    # Define more option if needed here ...
    __logger = logger
    return __logger
  finally:
    # Release lock when done
    __logger_lock.release()


def _can_log(level, log_level):
  """ Check if the logging level is high enough to be logged """
  return level <= log_level


class Logger(object):
  """ Public interface for Logging tools """

  def __init__(self, stream=None):
    """
    Create a logging object
    :param stream:  IOStream where to dump the information
    """
    self.__log_level = Level.kDebug
    self.__logger = _get_logger_instance(stream)

  def _emit_msg(self, level, msg, *args, **kwargs):
    """
    Emit message to the logger
    :param level: Logging level
    :param msg:   Template message
    :param args:  Parameters of the template
    :param kwargs: Extra keyword arguments
    :keyword stacklevel: Offset in the stack level, default 0.
    """
    # Finalize message
    log_msg = msg % args if len(args) else msg
    # Get file + line
    stacklevel = kwargs.get('stacklevel', 0)
    stack = inspect.stack()[2 + stacklevel]
    # filename = os.path.basename(stack.filename)
    filename = stack.filename
    lineno = stack.lineno
    # Log entry
    self.__logger.log(level=level, filename=filename, line=lineno, msg=log_msg)

  def enable(self):
    """ Turn logging on """
    self.__logger.enable()

  def disable(self):
    """ Turn logging off """
    self.__logger.disable()

  @property
  def log_level(self):
    """ Get the actual logging level, default = Level.kDebug """
    return self.__log_level

  @log_level.setter
  def log_level(self, level):
    """ Set a logging level """
    self.__logger.level = level
    self.__log_level = level

  def debug(self, msg, *args, **kwargs):
    """
    Record a debug entry if the logging level is high enough
    :param msg:   Template message
    :param args:  Argument to add
    :param kwargs Extra keyword arguments
    :keyword stacklevel: Offset in the function stack.
    """
    if _can_log(level=Level.kDebug, log_level=self.__log_level):
      self._emit_msg(Level.kDebug, msg, *args, **kwargs)

  def info(self, msg, *args, **kwargs):
    """
    Record an info entry if the logging level is high enough
    :param msg:   Template message
    :param args:  Argument to add
    :param kwargs Extra keyword arguments
    :keyword stacklevel: Offset in the function stack.
    """
    if _can_log(level=Level.kInfo, log_level=self.__log_level):
      self._emit_msg(Level.kInfo, msg, *args, **kwargs)

  def warning(self, msg, *args, **kwargs):
    """
    Record a warning entry if the logging level is high enough
    :param msg:   Template message
    :param args:  Argument to add
    :param kwargs Extra keyword arguments
    :keyword stacklevel: Offset in the function stack.
    """
    if _can_log(level=Level.kWarning, log_level=self.__log_level):
      self._emit_msg(Level.kWarning, msg, *args, **kwargs)

  def error(self, msg, *args, **kwargs):
    """
    Record an error entry if the logging level is high enough
    :param msg:   Template message
    :param args:  Argument to add
    :param kwargs Extra keyword arguments
    :keyword stacklevel: Offset in the function stack.
    """
    if _can_log(level=Level.kError, log_level=self.__log_level):
      self._emit_msg(Level.kError, msg, *args, **kwargs)
