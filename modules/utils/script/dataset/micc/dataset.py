# coding=utf-8
"""
MICC Florence 3D face dataset

This dataset is being constructed specifically to support research on techniques
 that bridge the gap between 2D, appearance-based recognition techniques, and
 fully 3D approaches. It is designed to simulate, in a controlled fashion,
 realistic surveillance conditions and to probe the efficacy of exploiting 3D
 models in real scenarios.

See:
  https://www.micc.unifi.it/resources/datasets/florence-3d-faces/
"""
import os.path as _path
import re as _re
from lts5.utils.tools import search_folder
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.node import Video
from lts5.utils.dataset.base.iterator import VideoIterator

__author__ = 'Christophe Ecabert'


class MICCInfo(DatasetInfo):
  """ MICC Information class """

  def name(self):
    """
    Return a simple name for the dataset.
    :return: 'pld'
    """
    return 'micc'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['cooperative', 'indoor', 'outdoor']

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return False

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


def _filter_partition(path, partition):
  """
  Filter
  :param path:      Path to analyze
  :param partition: Partition to select
  :return:  True if selected, false otherwise
  """

  fname, _ = _path.splitext(_path.basename(path))
  p = fname.lower().split('-')[-1]  # name is <Letters>-<Partition>.<Ext> format
  return p == partition


class MICC(Dataset):
  """ MICC Dataset for face alignment """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(MICC, self).__init__(info=MICCInfo())
    # Build MICC dataset, gather sample
    files = search_folder(folder=folder,
                          ext=['.mpeg', '.mjpeg'],
                          relative=False)
    # Regex for extracting subject ID
    re_subj_id = _re.compile('.*subject_([0-9]*)')
    for part, sz in zip(self.infos.partitions(), [53, 53, 53]):
      # Scan for entries
      entries = [x for x in files if _filter_partition(x, part)]
      # Create nodes list
      videos = []
      for e in entries:
        # Extract subect ID
        node_id = int(re_subj_id.match(e).groups()[0])
        # Create node
        videos.append(Video(video_id=node_id, path=e))
      # Set attirbutes
      setattr(self, '_{}_videos'.format(part), videos)
      # Sanity check
      msg = 'Not all videos in {} partition have been found'.format(part)
      self._check_dimensions(sets=[len(videos)],
                             n=sz,
                             msg=msg)

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_videos'.format(partition))
    return VideoIterator(seq=seq, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_videos'.format(partition))
    return VideoIterator(seq=seq, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    return type(None)
