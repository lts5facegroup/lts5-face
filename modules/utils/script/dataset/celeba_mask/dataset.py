# coding=utf-8
""" Implementation of the interface for the sfa dataset """
import os.path as _path
from os import makedirs as _mkdir
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.node import Image
from lts5.utils.dataset.base.node import SegmentationMask
from lts5.utils.dataset.base.iterator import ListIterator
from lts5.utils.tools import search_folder as _search_f
__author__ = 'Christophe Ecabert'


class CelebAMaskHqInfo(DatasetInfo):
  """ Helen Face Parts Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'celeba_mask_hq'
        """
    return 'celeba_mask_hq'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train', 'validation', 'test']

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


class CelebAMaskHq(Dataset):
  """ CelebaAMaskHq Dataset for face parsing """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(CelebAMaskHq, self).__init__(info=CelebAMaskHqInfo())

    def _create_type(folder, ext, dtype):
      path = _search_f(folder=folder, ext=ext, relative=False)
      return [dtype(k, p) for k, p in enumerate(path)]

    def _parse_partition_file(partition, folder):
      """
      Parse a partition file
      :param partition: Partition to parse
      :param folder: Root folder
      :return: List of image in the partition, list of segmentation mask
      """
      images = []
      masks = []
      with open(_path.join(folder, '{}_list.txt'.format(partition)), 'rt') as f:
        for k, line in enumerate(f):
          line = line.strip()
          if len(line):
            im_path = _path.join(folder, 'CelebA-HQ-img', line)
            lbl_path = _path.join(folder, 'mask', line.replace('.jpg', '.png'))
            images.append(Image(k, im_path))
            masks.append(SegmentationMask(k, lbl_path))
      return images, masks

    # Build CelebAMaskHq dataset, gather sample + labels
    for part, sz in zip(self.infos.partitions(), [24183, 2993, 2824]):
      # Create image + labels for the partition
      images, labels = _parse_partition_file(partition=part, folder=folder)
      # Set attributes
      setattr(self, '_{}_images'.format(part), images)
      setattr(self, '_{}_labels'.format(part), labels)
      # Sanity check
      msg = 'Not all images in {} partition have been found'.format(part)
      self._check_dimensions(sets=[len(images)],
                             n=sz,
                             msg=msg)
      msg = 'Not all labels in {} partition have been found'.format(part)
      self._check_dimensions(sets=[len(labels)],
                             n=sz,
                             msg=msg)

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_labels'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    seq = getattr(self, '_{}_labels'.format(partition))
    return type(seq[0])


def _generate_masks(pargs):
  """
  Generate segmentation mask
  :param pargs: Parsed arguments
  """
  import numpy as np
  from imageio import imread, imwrite
  from lts5.utils.tools import init_logger
  # List of labels, skip background at position 0
  label_list = ['skin', 'nose', 'eye_g', 'l_eye', 'r_eye', 'l_brow', 'r_brow',
                'l_ear', 'r_ear', 'mouth', 'u_lip', 'l_lip', 'hair', 'hat',
                'ear_r', 'neck_l', 'neck', 'cloth']
  # properties
  logger = init_logger()
  folder_base = _path.join(pargs.root, 'CelebAMask-HQ-mask-anno')
  output = _path.join(pargs.root, 'mask')
  img_num = 30000

  if not _path.exists(output):
    _mkdir(output)

  for k in range(img_num):
    if k % 1000 == 0:
      logger.info('Generate mask for image {:05d}.jpg'.format(k))
    folder_num = k // 2000
    im_base = np.zeros((512, 512), dtype=np.uint8)
    for idx, label in enumerate(label_list):
      filename = _path.join(folder_base,
                            str(folder_num),
                            str(k).rjust(5, '0') + '_' + label + '.png')
      if _path.exists(filename):
        im = np.asarray(imread(filename))[:, :, 0]
        im_base[im != 0] = (idx + 1)  # Add 1 since background is skipped
    # Dump full mask
    filename_save = _path.join(output, str(k) + '.png')
    imwrite(filename_save, im_base)


def _generate_partitions(pargs):
  """
  Generate partitions: 'train', 'validaion' and 'test'
  :param pargs: Parsed arguments
  """
  import re
  # properties
  matcher = re.compile(r"([0-9]*)\s*([0-9]*)\s*([0-9]*.jpg)")
  mapping_file = _path.join(pargs.root, 'CelebA-HQ-to-CelebA-mapping.txt')
  # Create destination
  writers = {'train': open(_path.join(pargs.root, 'train_list.txt'), 'wt'),
             'val': open(_path.join(pargs.root, 'validation_list.txt'), 'wt'),
             'test': open(_path.join(pargs.root, 'test_list.txt'), 'wt')}

  with open(mapping_file, 'rt') as f_map:
    f_map.readline()  # Skip header
    for line in f_map:
      line = line.strip()
      if len(line):
        m = matcher.match(line).groups()
        im_number = int(m[0])
        im_filename = '{}.jpg'.format(im_number)
        idx = int(m[1])
        if idx < 162771:
          writers['train'].write('{}\n'.format(im_filename))
        elif 162770 < idx < 182636:
          writers['val'].write('{}\n'.format(im_filename))
        else:
          writers['test'].write('{}\n'.format(im_filename))
  # Close writers
  for k, v in writers.items():
    v.close()


if __name__ == '__main__':
  from argparse import ArgumentParser

  p = ArgumentParser('Utility tool to generate mask/partition for CelebA Mask '
                     'HQ')
  # Dataset location
  p.add_argument('--root',
                 type=str,
                 required=True,
                 help='Location of CelebA Mask HQ dataset')
  # Parse arguments
  args = p.parse_args()

  _generate_masks(pargs=args)
  _generate_partitions(pargs=args)