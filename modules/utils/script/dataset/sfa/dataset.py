# coding=utf-8
""" Implementation of the interface for the sfa dataset """
import os.path as _path
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.node import Image
from lts5.utils.dataset.base.node import BinaryMask
from lts5.utils.dataset.base.iterator import ListIterator
from lts5.utils.tools import search_folder as _search_f
__author__ = 'Christophe Ecabert'


class SFAInfo(DatasetInfo):
  """ sfa Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'sfa'
        """
    return 'sfa'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train']  # no specific partition define, therefore put all in train

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


class SFA(Dataset):
  """ Sfa Dataset for face alignment """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(SFA, self).__init__(info=SFAInfo())
    # Build SFA dataset, gather sample + labels

    def _create_type(folder, ext, dtype):
      path = _search_f(folder=folder, ext=ext, relative=False)
      return [dtype(k, p) for k, p in enumerate(path)]

    # Create list of `Node`
    fname = _path.join(folder, 'ORI')
    self._images = _create_type(folder=fname, ext=['.jpg'], dtype=Image)
    fname = _path.join(folder, 'GT')
    self._labels = _create_type(folder=fname, ext=['.jpg'], dtype=BinaryMask)


    # Sanity check
    self._check_dimensions(sets=[len(self._images), len(self._labels)],
                           n=1118,
                           msg='Not all samples have been found')

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._images, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._labels, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._images, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    return type(self._labels[0])
