# coding=utf-8
""" Implementation of the interface for the ibugvw dataset """
import re as _re
import os.path as _path
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.node import Video
from lts5.utils.dataset.base.node import Landmarks
from lts5.utils.dataset.base.iterator import VideoIterator, ListIterator
from lts5.utils.tools import search_folder as _search_f

__author__ = 'Christophe Ecabert'


class IBugVWInfo(DatasetInfo):
  """ ibugvw Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'ibugvw'
        """
    return 'ibugvw'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train']  # no specific partition define, therefore put all in train

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


class IBugVW(Dataset):
  """
  Ibug video in the wild Dataset for face alignment

  See: https://ibug.doc.ic.ac.uk/resources/300-VW/
  """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(IBugVW, self).__init__(info=IBugVWInfo())
    # Build IBugVW dataset, gather sample + labels
    self._videos = []
    self._landmarks = []
    # Scan for videos
    m_video = _re.compile(r"([0-9]*)/vid")
    files = _search_f(folder=folder, ext=['.avi'], relative=False)
    for path in files:
      # Video object
      res = m_video.search(path).groups()
      video_id = int(res[0])
      self._videos.append(Video(video_id=video_id, path=path))
      # Landmarks
      folder = _path.dirname(path)
      fname = _path.join(folder, 'annot')
      label_files = _search_f(folder=fname, ext=['.pts'], relative=False)
      if not len(label_files):
        raise FileNotFoundError('No annotations found in {}'.format(fname))
      # Treat labels
      for label_path in label_files:
        # Extract id
        label_id, _ = _path.splitext(_path.basename(label_path))
        label_id = int((video_id * 1e6) + int(label_id))
        self._landmarks.append(Landmarks(landmarks_id=label_id,
                                         path=label_path))
    # Sanity check
    self._check_dimensions(sets=[len(self._videos)],
                           n=114,
                           msg='Not all videos have been found')
    self._check_dimensions(sets=[len(self._landmarks)],
                           n=218597,
                           msg='Not all landmarks have been found')

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    return VideoIterator(seq=self._videos, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._landmarks, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    return VideoIterator(seq=self._videos, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    return type(self._landmarks[0])
