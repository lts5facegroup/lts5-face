# coding=utf-8
"""
AFLW dataset

See:
  https://www.tugraz.at/institute/icg/research/team-bischof/lrs/downloads/aflw/
"""
import os.path as _path
from sqlite3 import connect as _connect
from lts5.utils.dataset.base.dataset import Dataset, DatasetInfo, MetaData
from lts5.utils.dataset.base.types import Bbox, HeadPose
from lts5.utils.dataset.base.node import Image
from lts5.utils.dataset.base.iterator import ListIterator, MetaDataIterator

__author__ = 'Christophe Ecabert'


class AFLWInfo(DatasetInfo):
  """ AFLW Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'aflw'
        """
    return 'aflw'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train']

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return False

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return True


class AFLW(Dataset):
  """ AFLW Dataset for face alignment """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(AFLW, self).__init__(info=AFLWInfo())


    # Open DB
    db_path = _path.join(folder, 'data', 'aflw.sqlite')
    conn = _connect(db_path)
    cursor = conn.cursor()

    # Create QUERY for retriving: roll, pitch, yaw and faces position
    select_str = 'faceimages.filepath, faces.face_id, facepose.roll, facepose.'\
                 'pitch, facepose.yaw, facerect.x, facerect.y, facerect.w,'\
                 ' facerect.h'
    from_str = 'faceimages, faces, facepose, facerect'
    where_str = 'faces.face_id = facepose.face_id and faces.file_id ='\
                ' faceimages.file_id and faces.face_id = facerect.face_id'
    query_str = "SELECT {} FROM {} WHERE {}".format(select_str,
                                                    from_str,
                                                    where_str)

    # Build AFLW dataset, gather sample + labels
    for part, sz in zip(self.infos.partitions(), [24384]):
      images = []
      metas = []
      for row in cursor.execute(query_str):
        im_path, face_id, roll, pitch, yaw, x, y, w, h = row

        # Create Image node
        im_name = _path.join(folder, 'data', im_path)
        images.append(Image(image_id=face_id, path=im_name))

        # Create meta data
        bbox = Bbox(bbox_id=face_id, x=x, y=y, width=w, height=h)
        pose = HeadPose(pose_id=face_id, roll=roll, pitch=pitch, yaw=yaw)
        meta = MetaData(bbox=bbox, pose=pose)
        metas.append(meta)


      # Got all set attr
      setattr(self, '_{}_images'.format(part), images)
      setattr(self, '_{}_meta'.format(part), metas)
      # Sanity check
      msg = 'Not all images in {} partition have been found'.format(part)
      self._check_dimensions(sets=[len(images)],
                             n=sz,
                             msg=msg)
    # Close DB -> no need to keep it alive
    conn.close()

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_meta'.format(partition))
    return MetaDataIterator(seq=seq)

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    return None
