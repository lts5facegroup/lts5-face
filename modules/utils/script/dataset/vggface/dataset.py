# coding=utf-8
"""
VGGFace2 dataset

See:
  http://www.robots.ox.ac.uk/~vgg/data/vgg_face2/
"""
import os.path as _path
from os import listdir as _listdir
import cv2 as _cv
import re as _re
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.node import Folder
from lts5.utils.dataset.base.iterator import FolderIterator

__author__ = 'Christophe Ecabert'


class VGGFace2Info(DatasetInfo):
  """ VGGFace2 Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'widerface'
        """
    return 'vggface2'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train', 'test']

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return False

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


class VGGFace2(Dataset):
  """ VGGFace2 Dataset for face alignment """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(VGGFace2, self).__init__(info=VGGFace2Info())

    # Load image
    def _decode_fn(path):
      frame = _cv.imread(path)
      return _cv.cvtColor(frame, _cv.COLOR_BGR2RGB)

    # Search pattern -> extract nXXXXXX -> n + XXXXXX
    srch = _re.compile(r"(n)([0-9]*)")

    # Build VGGFace2 dataset, gather sample + labels
    for part, sz in zip(self.infos.partitions(), [8631, 500]):
      # Folder where data are stored
      subject_folder = _path.join(folder, 'data', part)
      # Create one folder node for every entries in `subject_folder`
      subjects = _listdir(subject_folder)
      folders = []
      for subj in subjects:
        m = srch.match(subj).groups()
        if m[0] == 'n' and m[1] != '':
          f = _path.join(subject_folder, subj)
          fid = int(m[1])
          folders.append(Folder(folder=f,
                                folder_id=fid,
                                ext='.jpg',
                                open_fn=_decode_fn))

      # Got all set attr
      setattr(self, '_{}_folders'.format(part), folders)
      # Sanity check
      msg = 'Not all folders in {} partition have been found'.format(part)
      self._check_dimensions(sets=[len(folders)],
                             n=sz,
                             msg=msg)

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_folders'.format(partition))
    return FolderIterator(seq=seq, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_folders'.format(partition))
    return FolderIterator(seq=seq, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    return None