# coding=utf-8
""" Iterator interface for various dataset.

Available specialisation:
  - ListIterator
  - FolderIterator
  - VideoIterator
  - MetadataIterator
"""
from abc import ABC
from abc import abstractmethod as _abstractmethod
from lts5.utils.dataset.base.node import Node, Video, Folder

__author__ = 'Christophe Ecabert'


class AbstractIterator(ABC):
  """
  Iterator interface

  Attributes
  ----------
  seq: list
    Underlying sequence of lts5.utils.dataset.base.node.Node or subclass
  """

  def __init__(self, seq: list, dtype=Node):
    """
    Constructor sequence
    :param seq: Sequence to iterate over
    :param dtype: Type of object stored in the sequence
    """
    assert all(isinstance(n, dtype) for n in seq), 'Only accept subclass of \
{}'.format(dtype)
    self.seq = seq

  @_abstractmethod
  def __iter__(self):
    """ Return the iterator """
    pass

  @_abstractmethod
  def __next__(self):
    """
    Retrieve the next element in the sequence
    :raise: StopIteration when reach the end of the sequence
    """
    pass


class ListIterator(AbstractIterator):
  """ Iterator over a list of lts5.utils.dataset.base.node.Node or subclass """

  def __init__(self, seq, attribute):
    """
    Constructor
    :param seq: Sequence on which to iterate
    :param attribute: Name of the node's attribute to iterate on
                      (i.e. `load`, `name`, ...)
    """
    super(ListIterator, self).__init__(seq=seq)
    self._index = 0
    self._attr = attribute

  def __iter__(self):
    """ Provide the iterator """
    return self

  def __next__(self):
    """ Retrieve next element in the sequence """
    if self._index >= len(self.seq):
      raise StopIteration
    # Can continue
    getter = getattr(self.seq[self._index], self._attr, False)
    if getter is False:
      raise ValueError('Attribute "{}" is not supported by `Node`'
                       .format(self._attr))
    # Getter is callable ?
    if hasattr(getter, '__call__'):
      # Yes, iterator function, i.e. `load`
      retval = getter()
    else:
      # Read property
      retval = getter
    self._index += 1
    return retval


class FolderIterator(AbstractIterator):
  """ Iterate over a list of lts5.utils.dataset.base.node.Folder """

  def __init__(self, seq, attribute):
    """
    Constructor
    :param seq: Sequence on which to iterate
    :param attribute: Name of the node's attribute to iterate on
                      (i.e. `load`, `name`, ...)
    """
    super(FolderIterator, self).__init__(seq=seq, dtype=Folder)
    self._index = 0  # Index of the current video read
    self._frame = 0  # Number of frames loaded for the current video
    self._attr = attribute

  def __iter__(self):
    """ Provide the iterator """
    return self

  def __next__(self):
    """ Retrieve next element in the sequence """
    self._check_stop_iteration()
    # Try to read from current index
    ret = False
    value = None
    while not ret:
      # Select proper getter
      getter = getattr(self.seq[self._index], self._attr, False)
      if getter is False:
        raise ValueError('Attribute "{}" is not supported by `Node`'
                         .format(self._attr))
      # Getter is callable ?
      if hasattr(getter, '__call__'):
        # Yes, iterator function, i.e. `load`
        ret, value = getter()
      else:
        # Read property
        value = getter
        ret = value is not None
      if not ret:
        # Fail, go to the next video
        self._index += 1
        self._check_stop_iteration()
    return value

  def _check_stop_iteration(self):
    """ Check if the iterator as reach the end of the sequence """
    if self._index >= len(self.seq):
      raise StopIteration


class VideoIterator(AbstractIterator):
  """ Iterator over a list of lts5.utils.dataset.base.node.Video object """

  def __init__(self, seq, attribute):
    """
    Constructor
    :param seq: Sequence on which to iterate
    :param attribute: Name of the node's attribute to iterate on
                      (i.e. `load`, `name`, ...)
    """
    super(VideoIterator, self).__init__(seq=seq, dtype=Video)
    self._index = 0  # Index of the current video read
    self._frame = 0  # Number of frames loaded for the current video
    self._attr = attribute

  def __iter__(self):
    """ Provide the iterator """
    return self

  def __next__(self):
    """ Retrieve next element in the sequence """
    self._check_stop_iteration()
    # Try to read from current index
    ret = False
    while not ret:
      # Select proper getter
      getter = getattr(self.seq[self._index], self._attr, False)
      if getter is False:
        raise ValueError('Attribute "{}" is not supported by `Node`'
                         .format(self._attr))
      # Getter is callable ?
      if hasattr(getter, '__call__'):
        # Yes, iterator function, i.e. `load`
        ret, img = getter()
      else:
        # Read property
        img = getter
        ret = img is not None
      if not ret:
        # Fail, go to the next video
        self._index += 1
        self._check_stop_iteration()
    return img

  def _check_stop_iteration(self):
    """ Check if the iterator as reach the end of the sequence """
    if self._index >= len(self.seq):
      raise StopIteration


class MetaDataIterator(AbstractIterator):
  """
  Iterator over a list of lts5.utils.dataset.base.dataset.MetaData object
  """

  def __init__(self, seq):
    """
    Constructor
    :param seq: Sequence on which to iterate
    """
    super(MetaDataIterator, self).__init__(seq=seq, dtype=dict)
    self._index = 0

  def __iter__(self):
    """ Provide the iterator """
    return self

  def __next__(self):
    """ Retrieve next element in the sequence """
    if self._index >= len(self.seq):
      raise StopIteration
    # Can continue
    retval = self.seq[self._index].load()
    self._index += 1
    return retval
