# coding=utf-8
"""
Commonly used objects for all datasets

Objects:
  - Bbox
  - HeadPose
"""
__author__ = 'Christophe Ecabert'


class Bbox:
  """
  Face bounding box for CelebA dataset stored as follow: x, y, width, height

  Attributes
  ----------
  bbox_id: int
    Bounding box's ID
  x: int
    X origin
  y: int
    Y origin
  width: int
    box width
  height: int
    box height
  """

  def __init__(self, bbox_id: int, x: int, y: int, width: int, height: int):
    """
    Constructor
    :param bbox_id: Bounding box's ID
    :param x:       X origin
    :param y:       Y origin
    :param width:   Width
    :param height:  Height
    """
    self._bbox_id = bbox_id
    self._x = x
    self._y = y
    self._width = width
    self._height = height

  @property
  def id(self):
    return self._bbox_id

  @property
  def x(self):
    return self._x

  @property
  def y(self):
    return self._y

  @property
  def width(self):
    return self._width

  @property
  def height(self):
    return self._height


class HeadPose:
  """ Head pose parametrization: roll, pitch, yaw """

  def __init__(self, pose_id, roll, pitch, yaw):
    """
    Constructor
    :param pose_id: Id
    :param roll:    Roll angle
    :param pitch:   Pitch angle
    :param yaw:     Yaw angle
    """
    self._pose_id = pose_id
    self._roll = roll
    self._pitch = pitch
    self._yaw = yaw

  @property
  def id(self):
    return self._pose_id

  @property
  def roll(self):
    return self._roll

  @property
  def pitch(self):
    return self._pitch

  @property
  def yaw(self):
    return self._yaw


