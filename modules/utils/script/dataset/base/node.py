# coding=utf-8
""" Interface for data abstraction across various datasets. """
import os as _os
from abc import ABC
from abc import abstractmethod as _abstractmethod
import numpy as _np
import cv2 as _cv
from scipy.io import loadmat
from lts5.utils import load_pts as _load_pts
from lts5.utils.tools import search_folder as _search_folder

__author__ = 'Christophe Ecabert'


class Node(ABC):
  """ Abstract Interface for any data points

  Attributes
  ----------
  node_id: int
    Unique identifier
  directory: str
    Location where the data node is stored
  name: str
    Name of the node, can be empty
  extension: str
    extension of the underlying type if applicable
   """

  def __init__(self, node_id, name='', directory='', ext=''):
    """
    Constructor
    :param node_id: Node ID, should be unique
    :param name:  Node's name
    :param directory: Location where the data represented by the node is stored
    :param ext: Extension of the underlying data
    """
    self._node_id = node_id
    self._directory = directory
    self._name = name
    self._extension = ext

  @_abstractmethod
  def load(self):
    """
    Load the underlying data into memory, should be implemented in the derived
    class
    :return:  tuple, (node id, node value)
    """
    pass

  def make_path(self):
    """
    Constructor path to the underlying data node
    :return: Absolute path to the data node
    """
    return _os.path.join(self._directory, self._name + self._extension)

  @property
  @_abstractmethod
  def id(self):
    """
    Provide's node's ID
    :return:  ID
    """
    pass

  @property
  @_abstractmethod
  def name(self):
    """
    Provide Node's name
    :return: `str` with node's name
    """
    pass


class Image(Node):
  """ Reference to an image file """

  def __init__(self, image_id, path):
    """
    Constructor
    :param image_id:  Image ID
    :param path:  Path to the location where the image is stored
    """
    # Dissect path into dir, name, ext
    abs_path = _os.path.abspath(path)
    directoy = _os.path.dirname(abs_path)
    filename, extension = _os.path.splitext(_os.path.basename(abs_path))
    super(Image, self).__init__(node_id=image_id,
                                name=filename,
                                directory=directoy,
                                ext=extension)

  def load(self):
    """
    Load the image into memory
    :return:  ndarray storing the image in uint8 RGB format
    """
    path = self.make_path()
    image = _cv.imread(path)
    return self.id, _cv.cvtColor(image, _cv.COLOR_BGR2RGB)   # RGB image

  @property
  def id(self):
    """
    Provide's node's ID
    :return:  ID
    """
    return self._node_id

  @property
  def name(self):
    """
    Provide Node's name
    :return: `str` with node's name
    """
    return self._name + self._extension


class Folder(Node):
  """ Node for files stored into a folder. For instance when images of a given
  class are stored into a folders. """

  def __init__(self, folder, folder_id, ext, open_fn):
    """
    Constructor
    :param folder:    Location where to look for image
    :param folder_id: Folder id
    :param ext:       Extension to search
    :param open_fn:   Function to use for opening the files.
                      Prototype: open_fn(str) -> Any
    """
    # Dissect path into dir, name, ext
    directoy = _os.path.abspath(folder)
    super(Folder, self).__init__(node_id=folder_id,
                                 directory=directoy,
                                 ext=ext)
    self._folder_name = _os.path.basename(folder)
    self._index = 0
    self._files = _search_folder(folder=directoy, ext=ext)
    self._open_fn = open_fn

  def load(self):
    """
    Read one image from the underlying image list
    :return:  tuple(err, frame)
    """
    if self._index < len(self._files):
      # Load image
      frame = self._open_fn(_os.path.join(self._directory,
                                          self._files[self._index]))
      self._index += 1
      return True, (self.id, frame)
    # No image left
    return False, (self.id, None)

  @property
  def id(self):
    """
    Provide's node's ID
    :return:  ID
    """
    return self._node_id

  @property
  def name(self):
    """
    Provide Node's name
    :return: `str` with node's name or empty if outside boundaries
    """
    if 0 < self._index <= len(self._files):
      return _os.path.join(self._folder_name, self._files[self._index - 1])
    return None


class SegmentationMask(Image):
  """ Multi-class segmentation mask, used in segmentatic segmentation """

  def __init__(self, mask_id, path):
    """
    Constructor
    :param mask_id: MAsk's ID
    :param path:    Path to the location where the mask is stored
    """
    super(SegmentationMask, self).__init__(mask_id, path)

  def load(self):
    """
    Load mask into memory
    :return: id, ndarray storing the image in uint8
    """
    path = self.make_path()
    image = _cv.imread(path, _cv.IMREAD_GRAYSCALE)
    return self.id, image

  @property
  def id(self):
    """
    Provide's node's ID
    :return:  ID
    """
    return self._node_id

  @property
  def name(self):
    """
    Provide Node's name
    :return: `str` with node's name
    """
    return self._name + self._extension


class BinaryMask(Image):
  """ Reference to a binary mask stored in an image """

  def __init__(self, mask_id, path, is_color=True, inverted=False):
    """
    Constructor
    :param mask_id: Mask's ID
    :param path:    Path to the location where the mask is stored
    :param is_color:  If `True` indicates that the mask is not yet binarized
    :param inverted:  If `True` indicateds that the mask is inverted.
                      i.e. labels are zero at the object location
    """
    super(BinaryMask, self).__init__(mask_id, path)
    self.is_color = is_color
    self._inverted = inverted

  def load(self):
    """
    Load the image into memory
    :return:  ndarray storing the image in uint8 RGB format
    """
    im = super(BinaryMask, self).load()
    if self.is_color:
      # Binarize image
      # Sum over each channels, if pixel != 0 mark it as 1, 0 otherwise
      m = _np.sum(im, axis=-1)
      if self._inverted:
        im = (m == 0).astype(_np.uint8)
      else:
        im = (m != 0).astype(_np.uint8)
    return self.id, im

  @property
  def id(self):
    """
    Provide's node's ID
    :return:  ID
    """
    return self._node_id

  @property
  def name(self):
    """
    Provide Node's name
    :return: `str` with node's name
    """
    return self._name + self._extension


class Landmarks(Node):
  """ Reference to a landmark file stored in ibug format """

  def __init__(self, landmarks_id, path):
    """
    Constructor
    :param landmarks_id:  Landmark's ID
    :param path:  Location of the file
    """
    # Dissect path into dir, name, ext
    abs_path = _os.path.abspath(path)
    directoy = _os.path.dirname(abs_path)
    filename, extension = _os.path.splitext(_os.path.basename(abs_path))
    super(Landmarks, self).__init__(node_id=landmarks_id,
                                    name=filename,
                                    directory=directoy,
                                    ext=extension)

  def load(self):
    """
    Parse the landmark file
    :return:  ndarray with landmarks, format [x0, x1, ... xN, y0, y1, ... yN]
    """
    path = self.make_path()
    lms = _load_pts(filename=path).reshape(2, -1).transpose()
    return self.id, lms

  @property
  def id(self):
    """
    Provide's node's ID
    :return:  ID
    """
    return self._node_id

  @property
  def name(self):
    """
    Provide Node's name
    :return: `str` with node's name
    """
    return self._name + self._extension


class Video(Node):
  """
  Reference to video file

  Attributes
  ----------
  cap: cv2.VideoCapture
    Video decoder from OpenCV
  """

  def __init__(self, video_id, path):
    """
    Constructor
    :param video_id:  Video's ID
    :param path:  Path to the video file
    """
    # Dissect path into dir, name, ext
    abs_path = _os.path.abspath(path)
    directoy = _os.path.dirname(abs_path)
    filename, extension = _os.path.splitext(_os.path.basename(abs_path))
    super(Video, self).__init__(node_id=video_id,
                                name=filename,
                                directory=directoy,
                                ext=extension)
    self._index = -1
    self._cap = _cv.VideoCapture()
    self._ret = False

  def __del__(self):
    """ Destructor, close the video reader if needed """
    if self._cap.isOpened():
      self._cap.release()

  def load(self):
    """
    Read on frame from the underlying video file
    :return:  tuple(err, frame)
    """
    # Lazy init
    if not self._cap.isOpened():
      path = self.make_path()
      self._cap.open(filename=path)
    # Read
    self._ret, frame = self._cap.read()
    if self._ret:
      self._index += 1
      frame = _cv.cvtColor(frame, _cv.COLOR_BGR2RGB)
    return self._ret, (self.id, frame)

  @property
  def id(self):
    """
    Provide's node's ID
    :return:  ID
    """
    return self._node_id

  @property
  def name(self):
    """
    Provide Node's name
    :return: `str` with node's name, If string is empty indicates that no frame
             have been read.
    """
    fname = None
    if self._index < 0:
      raise RuntimeError('One frame must be loaded before accessing name '
                         'property')
    if self._index >= 0 and self._ret:
      fname = self._name + '_{:08d}'.format(self._index) + self._extension
    return fname


class File(Node):
  """ Reference to a file """

  def __init__(self, file_id, path, read_fn):
    """
    Constructor
    :param file_id:  File's ID
    :param path:  Path to the text file
    :param read_fn: Callable function to read the data: read_fn(path) -> Data
    """
    # Dissect path into dir, name, ext
    abs_path = _os.path.abspath(path)
    directoy = _os.path.dirname(abs_path)
    filename, extension = _os.path.splitext(_os.path.basename(abs_path))
    super(File, self).__init__(node_id=file_id,
                               name=filename,
                               directory=directoy,
                               ext=extension)
    self.read_fn = read_fn

  def load(self):
    """ Load the whole text file into memory """
    path = self.make_path()
    return self.id, self.read_fn(path)

  @property
  def id(self):
    """
    Provide's node's ID
    :return:  ID
    """
    return self._node_id

  @property
  def name(self):
    """
    Provide Node's name
    :return: `str` with node's name
    """
    return self._name + self._extension


class TextFile(File):
  """ Reference to a text file """

  def __init__(self, text_id, path, binary=False):
    """
    Constructor
    :param text_id:  Text file's ID
    :param path:  Path to the text file
    :param binary: If `True` indicates the text file is binary.
    """
    # read function
    def _read_fn(path):
      mode = 'rb' if binary else 'rt'
      with open(path, mode) as f:
        return f.read()

    super(TextFile, self).__init__(file_id=text_id,
                                   path=path,
                                   read_fn=_read_fn)


class MatFile(File):
  """ Label of type Matlab file """

  def __init__(self, mat_id, path, variables=None):
    """
    Constructor
    :param mat_id:   Matlab file's ID
    :param path:  Path to the file
    :param variables: List of variable to extract from the matfile, if None
      return all of them.
    """

    # read function
    def _read_fn(path):
      mat = loadmat(path)
      vars = variables
      if vars is None:
        # Remove keys like '__header__', '__version__'
        vars = [k for k in mat.keys() if not k.startswith('__')]
      mat = [mat[v] for v in vars]
      if len(mat) == 1:
        mat = mat[0]
      return mat

    super(MatFile, self).__init__(file_id=mat_id,
                                  path=path,
                                  read_fn=_read_fn)


class Label(Node):
  """
  Reference to a categorical label

  Attributes
  ----------
  value:
    Value of the label
  """

  def __init__(self, label_id, value):
    """
    Constructor
    :param label_id:  Label's ID
    :param value:   Label's value
    """
    super(Label, self).__init__(node_id=label_id)
    self.value = value

  def load(self):
    """ Access the underlying label's value """
    return self.id, self.value

  @property
  def id(self):
    """
    Provide's node's ID
    :return:  ID
    """
    return self._node_id

  @property
  def name(self):
    """
    Provide Node's name
    :return: `str` with node's name, i.e. Node repr
    """
    return 'Value: {}'.format(self.value)


class BBox(Node):
  """
  Reference to a bounding box object

  Attributes
  ----------
  bbox: list
    List of bonding box for a given image
  """

  def __init__(self, bbox_id: int, bbox: list):
    """
    Constructor
    :param label_id:  Label's ID
    :param value:   Label's value
    """
    super(BBox, self).__init__(node_id=bbox_id)
    self.bbox = bbox

  def load(self):
    """ Access the underlying list of bounding boxes """
    return self.id, self.bbox

  @property
  def id(self):
    """
    Provide's node's ID
    :return:  ID
    """
    return self._node_id

  @property
  def name(self):
    """
    Provide Node's name
    :return: `str` with node's name, i.e. Node repr
    """
    return str(self.bbox)
