# coding=utf-8
""" Interface for various dataset """
from abc import ABC, abstractmethod
from lts5.utils.dataset.base.iterator import ListIterator

__author__ = 'Christophe Ecabert'


class MetaData(dict):
  """
    Convenience class that behaves exactly like dict(), but allows accessing
    the keys and values using the attribute syntax, i.e., "mydict.key = value".

    See:
      https://github.com/tkarras/progressive_growing_of_gans/blob/master/config.py#L12
    """
  def __getattr__(self, name):
    """
    Get attribute (i.e. mydict.name)
    :param name:  Attribute's name
    :return:    Attribute's value
    """
    return self[name]

  def __setattr__(self, name, value):
    """
    Set attribute's value.
    :param name:  Attribute's name to be set
    :param value: Value to set
    """
    self[name] = value

  def __delattr__(self, name):
    """
    Delete an attribute
    :param name:  Attribute's name to be deleted
    """
    del self[name]


class DatasetInfo(ABC):
  """ Class holding the description of a given dataset. Must be specialized """

  @abstractmethod
  def name(self):
    """
    Return a simple name for the dataset.
    :return: str
    """
    pass

  @abstractmethod
  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    pass

  @abstractmethod
  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    pass

  @abstractmethod
  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    pass

  @abstractmethod
  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    pass

  @abstractmethod
  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    pass


class Dataset(ABC):
  """
  Abstract Interface for dataset

  Attributes:
  info: lts5.utils.dataset.base.dataset.DatasetInfo
    Information about the dataset
  """

  def __init__(self, info):
    """
    Constructor
    :param info:  Dataset information, must be a derived class of
                  `lts5.utils.dataset.base.dataset.DatasetInfo`
    """
    assert isinstance(info, DatasetInfo), 'Dataset information must be a \
derive type of {}'.format(DatasetInfo)
    self._info = info

  @abstractmethod
  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    pass

  @abstractmethod
  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    pass

  @abstractmethod
  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    pass

  @abstractmethod
  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    pass

  @abstractmethod
  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    pass

  @property
  def infos(self):
    """
    Return an instance of `lts5.utils.dataset.base.dataset.DatasetInfo`
    specialized for the dataset
    :return:  Derived of `lts5.utils.dataset.base.dataset.DatasetInfo`
    """
    return self._info

  def _partition_is_valid(self, partition: str):
    """
    Check if a given partition is valid for the dataset
    :param partition: Partition's name
    """
    if partition not in self.infos.partitions():
      msg = 'Unsupported type of partition, only {} are available' \
        .format(self.infos.partitions())
      raise RuntimeError(msg)

  def _check_dimensions(self, sets: list, n: int, msg: str):
    """
    Check that elements in `sets` have the same values
    :param img:   List of actual sets dimensions
    :param n:     Number of expected samples
    :param msg:   Message to add to the exception
    """
    # https://stackoverflow.com/a/3844948/4546884
    if sets.count(n) != len(sets):
      m = msg + ', got {} instead of {}'.format(sets[0], n)
      raise FileNotFoundError(m)


class NoOpInfo(DatasetInfo):
  """ Dataset information for NoOp datset. Intend to be used for debugging """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'noop'
        """
    return 'noop'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train', 'validation', 'test']

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


class NoOp(Dataset):
  """ NoOp Dataset for face alignment """

  def __init__(self, samples: dict, labels: dict=None):
    """
    Constructor
    :param samples: Dict of list of nodes for each partition
    :param labels:  Dict of list of nodes for each partition
    """
    super(NoOp, self).__init__(NoOpInfo())

    def _set_attr(attr, data):
      for key, value in data.items():
        # Check partition
        self._partition_is_valid(partition=key)
        # set data
        setattr(self, '_{}_{}'.format(key, attr), value)

    # Set samples
    _set_attr(attr='samples', data=samples)
    # labels if any
    if labels is not None:
      _set_attr(attr='labels', data=labels)

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_samples'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """

    self._partition_is_valid(partition=partition)
    if hasattr(self, '_{}_labels'.format(partition)):
      seq = getattr(self, '_{}_labels'.format(partition))
      return ListIterator(seq=seq, attribute='load')
    return None

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_samples'.format(partition))
    return ListIterator(seq=seq, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    return None
