# coding=utf-8
""" Implementation of the interface for the LFW dataset """
import os.path as _path
import re as _re
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.node import Image
from lts5.utils.dataset.base.node import Label
from lts5.utils.dataset.base.iterator import ListIterator
from lts5.utils.tools import search_folder as _search_f

__author__ = 'Christophe Ecabert'


class LFWInfo(DatasetInfo):
  """ LFW Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'lfw'
        """
    return 'lfw'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    # no specific partition define for the moment, if interested in face
    # recognition go check http://vis-www.cs.umass.edu/lfw/ for protocols
    return ['train']

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


class LFW(Dataset):
  """ LFW Dataset for face alignment """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(LFW, self).__init__(info=LFWInfo())
    # Build LFW dataset, gather sample + labels
    subject = set()
    files = _search_f(folder=folder, ext=['.jpg'], relative=False)
    self._images = []
    self._labels = []
    m = _re.compile(r"([a-zA-Z_-]*)_([0-9]*)")
    for k, f in enumerate(files):
      res = m.search(_path.basename(f)).groups()
      label_id = int(res[1])
      label_value = res[0]
      subject.add(label_value)
      self._images.append(Image(image_id=k, path=f))
      self._labels.append(Label(label_id=label_id, value=label_value))
    # Sanity check
    self._check_dimensions(sets=[len(self._images)],
                           n=13233,
                           msg='Not all images have been found')
    self._check_dimensions(sets=[len(subject)],
                           n=5749,
                           msg='Not all subjects have been found')

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._images, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._labels, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._images, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    return type(self._labels[0])