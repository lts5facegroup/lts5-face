# coding=utf-8
""" Implementation of the interface for the CelebA dataset """
import os.path as _path
import re as _re
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.dataset import MetaData
from lts5.utils.dataset.base.node import Image
from lts5.utils.dataset.base.node import Label
from lts5.utils.dataset.base.iterator import ListIterator
from lts5.utils.dataset.base.iterator import MetaDataIterator
from lts5.utils.dataset.base.types import Bbox
from lts5.utils.tools import search_folder as _search_f

__author__ = 'Christophe Ecabert'


class CelebAInfo(DatasetInfo):
  """ CelebA Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'celeba'
        """
    return 'celeba'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train', 'validation', 'test']

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return True


# List of all attributes
attributes_str = ('5_o_Clock_Shadow',
                  'Arched_Eyebrows',
                  'Attractive',
                  'Bags_Under_Eyes',
                  'Bald',
                  'Bangs',
                  'Big_Lips',
                  'Big_Nose',
                  'Black_Hair',
                  'Blond_Hair',
                  'Blurry',
                  'Brown_Hair',
                  'Bushy_Eyebrows',
                  'Chubby',
                  'Double_Chin',
                  'Eyeglasses',
                  'Goatee',
                  'Gray_Hair',
                  'Heavy_Makeup',
                  'High_Cheekbones',
                  'Male',
                  'Mouth_Slightly_Open',
                  'Mustache',
                  'Narrow_Eyes',
                  'No_Beard',
                  'Oval_Face',
                  'Pale_Skin',
                  'Pointy_Nose',
                  'Receding_Hairline',
                  'Rosy_Cheeks',
                  'Sideburns',
                  'Smiling',
                  'Straight_Hair',
                  'Wavy_Hair',
                  'Wearing_Earrings',
                  'Wearing_Hat',
                  'Wearing_Lipstick',
                  'Wearing_Necklace',
                  'Wearing_Necktie',
                  'Young')

# Map between attribute's name and index
attribute_index = {a: k for k, a in enumerate(attributes_str)}


class Attribute:
  """
  Binary attribute, it either exists (``+1``) or it does not exist (``-1``)

  Attributes
  ----------
  attribute_id: int
    Attribute's ID
  attributes:  list
    List of binary attribute, len(attributes) must be equal to 40.
  """

  def __init__(self, attribute_id: int, attributes: list):
    """
    Constructor
    :param attribute_id:  Attribute's ID
    :param attributes:  List of attributes
    """
    self.attributes_id = attribute_id
    self.attributes = attributes
    if len(self.attributes) != 40:
      raise ValueError('Wrong number of attributes, got {} instead of {}' \
                       .format(len(attributes),
                               40))

  def __call__(self, attribute_names=None):
    """
    Returns the subset or all of the attributes in a list.
    :param attribute_names: List of attribute's name to select. If None return
                            all of them
    :return: List of attributes
    """
    if attribute_names is None:
      return self.attributes
    return [self.attributes[attribute_index[a]] for a in attribute_names]


class Landmark:
  """
  Face landmark for CelebA dataset stored as follow:
  Left eye x/y Right Eye x/y Nose x/y Left Mouth x/y Right Mouth x/y

  Attributes
  ----------
  landmark_id: int
    Landmark's ID
  left_eye: tuple
    Left eye position
  right_eye: tuple
    Right eye position
  nose: tuple
    Nose position
  left_mouth: tuple
    Left mouth position
  right_mouth: tuple
    Right mouth position
  """

  def __init__(self, landmark_id: int, position: list):
    """
    Constructor
    :param landmark_id: Landmark's ID
    :param position:    List of landmarks position
    """
    self.landmark_id = landmark_id
    self.left_eye = position[:2]
    self.right_eye = position[2:4]
    self.nose = position[4:6]
    self.left_mouth = position[6:8]
    self.right_mouth = position[8:]


_partition_converter = {'train': 'tr',
                        'validation': 'val',
                        'test': 'test'}


class CelebA(Dataset):
  """ CelebA Dataset for face alignment """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(CelebA, self).__init__(info=CelebAInfo())
    # Build CelebA dataset, gather sample + labels
    fname = _path.join(folder, 'images')
    subject = set()
    files = _search_f(folder=fname, ext=['.jpg'], relative=False)
    n_img = len(files)
    if n_img != 202599:
      raise ValueError('Not all images have been found, got {} instead of {}' \
                       .format(n_img, 202599))
    # Prepare data structure
    self._tr_images = []
    self._val_images = []
    self._test_images = []
    self._tr_labels = []
    self._val_labels = []
    self._test_labels = []
    self._tr_metadata = []
    self._val_metadata = []
    self._test_metadata = []
    # open files
    id_fname = _path.join(folder, 'identity_CelebA.txt')
    attr_fname = _path.join(folder, 'list_attr_celeba.txt')
    bbox_fname = _path.join(folder, 'list_bbox_celeba.txt')
    lms_fname = _path.join(folder, 'list_landmarks_celeba.txt')
    part_fname = _path.join(folder, 'list_eval_partition.csv')
    with open(id_fname, 'rt') as f_id, \
        open(attr_fname, 'rt') as f_attr, \
        open(bbox_fname, 'rt') as f_bbox, \
        open(lms_fname, 'rt') as f_lms, \
        open(part_fname, 'rt') as f_part:
      # Skip unrelevant part of each files
      # Attr
      f_attr.readline()
      f_attr.readline()
      # bbox
      f_bbox.readline()
      f_bbox.readline()
      # Landmarks
      f_lms.readline()
      f_lms.readline()
      # partitions
      f_part.readline()

      # Iterate over every files
      for img, ids, attr, bbox, lms, part in zip(files,
                                                 f_id,
                                                 f_attr,
                                                 f_bbox,
                                                 f_lms,
                                                 f_part):
        # Extract image name
        im_name = _path.basename(img)
        im_number = int(_path.splitext(im_name)[0])
        # Define labels (i.e. Identity)
        p = ids.strip().split(' ')
        subject_id = int(p[1])
        assert im_name == p[0], 'Image name does not match (Identity)'
        # Create images + label
        im = Image(image_id=im_number, path=img)
        lbl = Label(label_id=im_number, value=subject_id)
        # Create metadata
        # attr
        p = attr.strip().split()
        assert im_name == p[0], 'Image name does not match (Attribute)'
        attribute = Attribute(attribute_id=im_number,
                              attributes=list(map(int, p[1:])))
        # bbox
        p = bbox.strip().split()
        bb = Bbox(bbox_id=im_number,
                  x=int(p[1]),
                  y=int(p[2]),
                  width=int(p[3]),
                  height=int(p[4]))
        # lms
        p = lms.strip().split()
        assert im_name == p[0], 'Image name does not match (Landmarks)'
        lm = Landmark(landmark_id=im_number, position=list(map(int, p[1:])))
        # Pack metadata
        metadata = MetaData()
        metadata.attributes = attribute
        metadata.bbox = bb
        metadata.landmarks = lm
        # Partition split, train, val, test
        p = part.strip().split(',')
        assert im_name == p[0], 'Image name does not match (Partition)'
        idx = int(p[1])
        if idx == 0:
          # Train
          self._tr_images.append(im)
          self._tr_labels.append(lbl)
          self._tr_metadata.append(metadata)
        elif idx == 1:
          # Validation
          self._val_images.append(im)
          self._val_labels.append(lbl)
          self._val_metadata.append(metadata)
        else:
          # Test
          self._test_images.append(im)
          self._test_labels.append(lbl)
          self._test_metadata.append(metadata)
    # Sanity check
    self._check_dimensions(sets=[len(self._tr_images),
                                 len(self._tr_labels),
                                 len(self._tr_metadata)],
                           n=162770,
                           msg='Not all training samples have be found')
    self._check_dimensions(sets=[len(self._val_images),
                                 len(self._val_labels),
                                 len(self._val_metadata)],
                           n=19867,
                           msg='Not all validation samples have be found')
    self._check_dimensions(sets=[len(self._test_images),
                                 len(self._test_labels),
                                 len(self._test_metadata)],
                           n=19962,
                           msg='Not all validation samplse have be found')

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    if partition == 'train':
      return ListIterator(seq=self._tr_images, attribute='load')
    elif partition == 'validation':
      return ListIterator(seq=self._val_images, attribute='load')
    else:
      return ListIterator(seq=self._test_images, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    if partition == 'train':
      return ListIterator(seq=self._tr_labels, attribute='load')
    elif partition == 'validation':
      return ListIterator(seq=self._val_labels, attribute='load')
    else:
      return ListIterator(seq=self._test_labels, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    if partition == 'train':
      return MetaDataIterator(seq=self._tr_metadata)
    elif partition == 'validation':
      return MetaDataIterator(seq=self._val_metadata)
    else:
      return MetaDataIterator(seq=self._test_metadata)

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    if partition == 'train':
      return ListIterator(seq=self._tr_images, attribute='name')
    elif partition == 'validation':
      return ListIterator(seq=self._val_images, attribute='name')
    else:
      return ListIterator(seq=self._test_images, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    attr_name = _partition_converter[partition]
    attr = getattr(self, '_{}_labels'.format(attr_name))
    return type(attr[0])
