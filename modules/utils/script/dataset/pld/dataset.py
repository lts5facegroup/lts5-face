# coding=utf-8
"""
Part Label Database

This dataset contains labelings of 2927 face images into Hair/Skin/Background
labels. The images are a subset of the LFW funneled images.

See:
  http://vis-www.cs.umass.edu/lfw/part_labels/
"""
import os.path as _path
import numpy as _np
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.node import Image
from lts5.utils.dataset.base.iterator import ListIterator

__author__ = 'Christophe Ecabert'


class PLDInfo(DatasetInfo):
  """ PLD Information class """

  def name(self):
    """
    Return a simple name for the dataset.
    :return: 'pld'
    """
    return 'pld'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    # http://vis-www.cs.umass.edu/lfw/part_labels/
    return ['train', 'validation', 'test']

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


def _parse_partition(filename):
  """
  Parse partition file, return list of tuple(image_path, image_number)
  :param filename: Path to the partition file
  :return:  List of tuple
  """
  path = []
  with open(filename, 'rt') as f:
    for line in f:
      entry = line.strip().split()
      path.append(entry)
  # Sort by image filename
  path.sort(key=lambda x: x[0])
  return path


class _Mask(Image):
  """
  Node subclass of Image type in order to convert label to more suited labels.

  The labels are coded as follow:
  (255, 0, 0) == Hair
  (0, 255, 0) == Skin
  (0, 0, 255) == Background

  The triples are mapped to:

  Background == 0, Skin == 1, Hair == 2
  """

  def __init__(self, mask_id, path, with_hair=True):
    """
    Constructor
    :param mask_id: Mask ID
    :param path:    File's path
    :param with_hair: If `True` hair labels will be included, otherwise they
                      are ignored.
    """
    super(_Mask, self).__init__(mask_id, path)
    self._with_hair = with_hair

  def load(self):
    """
    Load image and convert into proper formatting
    :return:  Image
    """
    img = super(_Mask, self).load()
    mask = _np.argmax(img[:, :, ::-1], axis=-1)
    if not self._with_hair:
      # Remove hair
      mask[mask == 2] = 0  # Set hair to background
    return mask


class PLD(Dataset):
  """ PLD Dataset for face alignment """

  def __init__(self, folder, with_hair=True):
    """
    Constructor
    :param folder:  Location where the data are stored
    :param with_hair: If `True` hair labels will be included, otherwise they
                      are ignored.
    """
    super(PLD, self).__init__(info=PLDInfo())
    # Build PLD dataset, gather sample + labels
    for part, sz in zip(self.infos.partitions(), [1500, 500, 927]):
      # Parse partition file
      part_name = _path.join(folder, 'data', '{}_set.txt'.format(part))
      entries = _parse_partition(filename=part_name)
      # Define image + label
      images = []
      labels = []
      for k, entry in enumerate(entries):
        # image + label path
        im_path = '{0}/{1}/{1}_{2:04d}.jpg'.format('images',
                                                   entry[0],
                                                   int(entry[1]))
        im_path = _path.join(folder, 'data', im_path)
        lbl_path = im_path.replace('images', 'labels')
        lbl_path = lbl_path.replace('jpg', 'ppm')
        lbl_path = lbl_path.replace('/{}'.format(entry[0]), '', 1)  # rmv folder
        lbl_path = _path.join(folder, 'data', lbl_path)
        # Sanity check
        if not _path.exists(im_path) or not _path.exists(lbl_path):
          msg = 'Image/Labels does not exists ({}, {})'.format(im_path,
                                                               lbl_path)
          raise ValueError(msg)
        # Create node
        images.append(Image(image_id=k, path=im_path))
        labels.append(_Mask(mask_id=k, path=lbl_path, with_hair=with_hair))
      # Add to object
      setattr(self, '_{}_images'.format(part), images)
      setattr(self, '_{}_labels'.format(part), labels)
      # Sanity check
      msg = 'Not all images in {} partition have been found'.format(part)
      self._check_dimensions(sets=[len(images)],
                             n=sz,
                             msg=msg)
      msg = 'Not all labels in {} partition have been found'.format(part)
      self._check_dimensions(sets=[len(labels)],
                             n=sz,
                             msg=msg)

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    seq = getattr(self, '_{}_labels'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    attr = getattr(self, '_{}_labels'.format(partition))
    return type(attr[0])
