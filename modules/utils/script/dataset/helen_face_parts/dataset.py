# coding=utf-8
""" Implementation of the interface for the sfa dataset """
import os.path as _path
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.node import Image
from lts5.utils.dataset.base.node import SegmentationMask
from lts5.utils.dataset.base.iterator import ListIterator
from lts5.utils.tools import search_folder as _search_f
__author__ = 'Christophe Ecabert'


class HelenFacePartsInfo(DatasetInfo):
  """ Helen Face Parts Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'sfa'
        """
    return 'helen_face_parts'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train', 'validation', 'test']

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


class HelenFaceParts(Dataset):
  """ Helen Face parts Dataset for face parsing """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(HelenFaceParts, self).__init__(info=HelenFacePartsInfo())

    def _create_type(folder, ext, dtype):
      path = _search_f(folder=folder, ext=ext, relative=False)
      return [dtype(k, p) for k, p in enumerate(path)]

    # Build HelenFaceParts dataset, gather sample + labels
    for part, sz in zip(self.infos.partitions(), [2000, 230, 100]):
      images = _create_type(_path.join(folder, part),
                            ['.jpg'],
                            Image)
      labels = _create_type(_path.join(folder, part),
                            ['.png'],
                            SegmentationMask)
      # Set attributes
      setattr(self, '_{}_images'.format(part), images)
      setattr(self, '_{}_labels'.format(part), labels)
      # Sanity check
      msg = 'Not all images in {} partition have been found'.format(part)
      self._check_dimensions(sets=[len(images)],
                             n=sz,
                             msg=msg)
      msg = 'Not all labels in {} partition have been found'.format(part)
      self._check_dimensions(sets=[len(labels)],
                             n=sz,
                             msg=msg)

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_labels'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    seq = getattr(self, '_{}_labels'.format(partition))
    return type(seq[0])
