# coding=utf-8
"""
Figaro1k dataset

In order to carry out hair analysis in the wild, a database with unconstrained
view images containing various hair textures is needed. The scarcity of open and
 available databases pushed us to build and make publicly available Figaro1k, an
 extension of Figaro, an annotated novel multi-class (straight, wavy, curly,
 kinky, braids, dreadlocks and short; each containing 150 images) image database
 for hair analysis in the wild.

Being images different in size and aspect ratio, a normalization procedure has
 then been applied: the employed normalization factor is chosen so as to reduce
 the size of the maximum squared area inscribed in the hair region to 227×227
 pixels.

Patch-F1k is an auxiliary database used only for training hair detection at
 patch level. It is provided together with Figaro1k and contains 1,050 pure hair
 texture images and 1,050 pure non-hair texture images, for a total of 2,100
 images of size 227×227. Both hair and non-hair images are divided in the same
 way as Figaro1k, i.e. 840 for training and 210 for testing. In our experiments
 we use only the training set. In order to make the two datasets compatible,
 hair patches in the training set of Patch-F1k are directly extracted from 840
 images of Figaro1k training set (first row of Figure 5), while non-hair patches
 are partially extracted from Figaro1k (420 images), and partially from VOC2012
 (again 420), for a total of 840 non-hair images.

The 7 classes are distributed in this order:
  straight: frame00001-00150
  wavy: frame00151-00300
  curly: frame00301-00450
  kinky: frame00451-00600
  braids: frame00601-00750
  dreadlocks: frame00751-00900
  short-men: frame00091-01050

See:
  http://projects.i-ctm.eu/it/progetto/figaro-1k
"""
import os.path as _path
import numpy as _np
from enum import Enum, unique
from lts5.utils.dataset.base.dataset import Dataset, DatasetInfo, MetaData
from lts5.utils.dataset.base.node import Image, BinaryMask
from lts5.utils.dataset.base.iterator import ListIterator, MetaDataIterator
from lts5.utils.tools import search_folder as _search_f

__author__ = 'Christophe Ecabert'


@unique
class HairStyle(Enum):
  """
  Hair style category

  Attributes
  ----------
    - straight
    - wavy
    - curly
    - kinky
    - braids
    - dreadlocks
    - short-men
  """
  kStraight = 1
  kWavy = 2
  kCurly = 3
  kKinky = 4
  kBraids = 5
  kDreadlocks = 6
  kShortMen = 7


class Figaro1kInfo(DatasetInfo):
  """ Figaro1k Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'figaro1k'
        """
    return 'figaro1k'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train', 'test']

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return True


class Figaro1k(Dataset):
  """ Figaro1k Dataset """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(Figaro1k, self).__init__(info=Figaro1kInfo())

    def _get_style(name):
      """ Define hairstyle based on file name

      straight: frame00001-00150
      wavy: frame00151-00300
      curly: frame00301-00450
      kinky: frame00451-00600
      braids: frame00601-00750
      dreadlocks: frame00751-00900
      short-men: frame00091-01050
      """
      fname, _ = _path.splitext(_path.basename(name))
      nbr = int(fname[-9:-4])
      if 0 < nbr <= 150:
        return HairStyle.kStraight
      if 150 < nbr <= 300:
        return HairStyle.kWavy
      elif 300 < nbr <= 450:
        return HairStyle.kCurly
      elif 450 < nbr <= 600:
        return HairStyle.kKinky
      elif 600 < nbr <= 750:
        return HairStyle.kBraids
      elif 750 < nbr <= 900:
        return HairStyle.kDreadlocks
      else:
        return HairStyle.kShortMen

    # Build Figaro1k dataset, gather sample + labels
    _partition_converter = {'train': 'Training',
                            'test': 'Testing'}
    for part, sz in zip(self.infos.partitions(), [840, 210]):
      # Image + Label
      im_folder = _path.join(folder,
                             'Original',
                             _partition_converter[part])
      lbl_folder = _path.join(folder,
                              'GT',
                              _partition_converter[part])
      # Scan for files
      images = _search_f(im_folder, ext=['.jpg'], relative=False)
      labels = _search_f(lbl_folder, ext=['.pbm'], relative=False)

      im_node = []
      lbl_node = []
      metas = []
      for k, (im_path, lbl_path) in enumerate(zip(images, labels)):
        im_node.append(Image(image_id=k,
                             path=im_path))
        lbl_node.append(BinaryMask(mask_id=k,
                                   path=lbl_path,
                                   is_color=True))
        # Metadata
        meta = MetaData()
        meta.hairstyle = _get_style(name=im_path)
        metas.append(meta)

      # Reach end of file, set attributes
      setattr(self, '_{}_images'.format(part), im_node)
      setattr(self, '_{}_labels'.format(part), lbl_node)
      setattr(self, '_{}_metadata'.format(part), metas)

      # Sanity check
      self._check_dimensions(sets=[len(im_node), len(lbl_node), len(metas)],
                             n=sz,
                             msg='Not all samples in {} partition have been '
                                 'found'.format(part))

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_labels'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_metadata'.format(partition))
    return MetaDataIterator(seq=seq)

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    attr = getattr(self, '_{}_labels'.format(partition))
    return type(attr[0])
