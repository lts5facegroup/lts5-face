# coding=utf-8
""" Implementation of the interface for the fsd dataset """
import os.path as _path
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.node import Image
from lts5.utils.dataset.base.node import BinaryMask
from lts5.utils.dataset.base.iterator import ListIterator
from lts5.utils.tools import search_folder as _search_f
__author__ = 'Christophe Ecabert'


class FSDInfo(DatasetInfo):
  """ fsd Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'fsd'
        """
    return 'fsd'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train']  # no specific partition define, therefore put all in train

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


class FSD(Dataset):
  """ Sfa Dataset for face alignment """

  def __init__(self, folder, dtype='face'):
    """
    Constructor
    :param folder:  Location where the data are stored
    :param dtype:   Data type, either 'face' or 'skin'
    """
    super(FSD, self).__init__(info=FSDInfo())
    # Build FSD dataset, gather sample + labels
    self._check_dtype(dtype=dtype)

    # Some samples need to be skipped since they don't match the label. They
    # are sort of transposed
    to_skip = [1195, 1809, 1966, 2247, 3016]

    def _create_type(folder, ext, skip, dtype):
      path = _search_f(folder=folder, ext=ext, skip=skip, relative=False)
      return [dtype(k, p) for k, p in enumerate(path)]

    # Create list of `Node`
    fname = _path.join(folder, 'Original')
    im_skip = ['im{:05d}.jpg'.format(k) for k in to_skip]
    self._images = _create_type(folder=fname,
                                ext=['.jpg'],
                                skip=im_skip,
                                dtype=Image)
    subfolder = 'Face' if dtype == 'face' else 'Skin'
    suffix = '_f' if dtype == 'face' else '_s'
    fname = _path.join(folder, subfolder)
    lbl_skip = ['im{:05d}{}.png'.format(k, suffix) for k in to_skip]
    self._labels = _create_type(folder=fname,
                                ext=['.png'],
                                skip=lbl_skip,
                                dtype=lambda k, p: BinaryMask(mask_id=k,
                                                              path=p,
                                                              inverted=True))
    # Sanity check
    self._check_dimensions(sets=[len(self._images), len(self._labels)],
                           n=4000 - len(to_skip),
                           msg='Not all samples have been found')

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._images, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._labels, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def _check_dtype(self, dtype, choices=('face', 'skin')):
    """
    Check if data type is valid
    :param dtype:   User's data type
    :param choices: List of possible choices
    """
    if dtype not in choices:
      msg = '"{}" data type is not supported, possible values are {}'\
        .format(dtype, choices)
      raise ValueError(msg)

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._images, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    return type(self._labels[0])