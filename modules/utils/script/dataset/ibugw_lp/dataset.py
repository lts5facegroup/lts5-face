# coding=utf-8
""" Implementation of the interface for the ibugvw dataset """
import os.path as _path
import re as _re

from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.iterator import ListIterator
from lts5.utils.dataset.base.node import MatFile
from lts5.utils.dataset.base.node import Image
from lts5.utils.tools import search_folder as _search_f

__author__ = 'Christophe Ecabert'


class IBugWLPInfo(DatasetInfo):
  """ 300W-LP Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'ibugvw'
        """
    return 'ibugw_lp'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train']  # no specific partition define, therefore put all in train

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


class IBugW_LP(Dataset):
  """
  300W-LP Dataset for face alignment

  See: http://www.cbsr.ia.ac.cn/users/xiangyuzhu/projects/3DDFA/main.htm
  """

  def __init__(self, folder, lms_type='2d'):
    """
    Constructor
    :param folder:  Location where the data are stored
    :param lms_type: Type of landmarks to load, can be one of `2d`, `3d`, `both`
    """
    super(IBugW_LP, self).__init__(info=IBugWLPInfo())
    # Type of landmarks
    if lms_type not in ('2d', '3d', 'both'):
      raise ValueError('Type of landmarks must be one of {}'.format(('2d',
                                                                     '3d',
                                                                     'both')))
    self.lms_type = ['pts_' + lms_type] if lms_type != 'both' else None
    # Build IBugW-LP dataset, gather sample + labels
    afw_img = _search_f(folder=_path.join(folder, 'AFW'),
                        ext=['.jpg'],
                        relative=False)
    afw_lms = _search_f(folder=_path.join(folder, 'landmarks', 'AFW'),
                        ext=['.mat'],
                        relative=False)
    helen_img = _search_f(folder=_path.join(folder, 'HELEN'),
                          ext=['.jpg'],
                          relative=False)
    helen_lms = _search_f(folder=_path.join(folder, 'landmarks', 'HELEN'),
                          ext=['.mat'],
                          relative=False)
    ibug_img = _search_f(folder=_path.join(folder, 'IBUG'),
                         ext=['.jpg'],
                         relative=False)
    ibug_lms = _search_f(folder=_path.join(folder, 'landmarks', 'IBUG'),
                         ext=['.mat'],
                         relative=False)
    lfpw_img = _search_f(folder=_path.join(folder, 'LFPW'),
                         ext=['.jpg'],
                         relative=False)
    lfpw_lms = _search_f(folder=_path.join(folder, 'landmarks', 'LFPW'),
                         ext=['.mat'],
                         relative=False)
    images = afw_img + helen_img + ibug_img + lfpw_img
    landmarks = afw_lms + helen_lms + ibug_lms + lfpw_lms

    self._images = []
    self._landmarks = []
    for k, (im_path, lms_path) in enumerate(zip(images, landmarks)):
      self._images.append(Image(image_id=k, path=im_path))
      self._landmarks.append(MatFile(mat_id=k,
                                     path=lms_path,
                                     variables=self.lms_type))

    # Sanity check
    self._check_dimensions(sets=[len(self._images)],
                           n=61225,
                           msg='Not all images have been found')
    self._check_dimensions(sets=[len(self._landmarks)],
                           n=61225,
                           msg='Not all landmarks have been found')

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._images, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._landmarks, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    return ListIterator(seq=self._images, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    return type(self._landmarks[0])
