# coding=utf-8
""" Implementation of the interface for the Helen dataset """
import os.path as _path
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.node import Image
from lts5.utils.dataset.base.node import Landmarks
from lts5.utils.dataset.base.iterator import ListIterator
from lts5.utils.tools import search_folder as _search_f
__author__ = 'Christophe Ecabert'


class HelenInfo(DatasetInfo):
  """ Helen Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'helen'
        """
    return 'helen'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train', 'test']  # Train + test are available

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return False


class Helen(Dataset):
  """ Helen Dataset for face alignment """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(Helen, self).__init__(info=HelenInfo())
    # Build Helen dataset, gather sample + labels

    def _create_type(f, ext, dtype):
      path = _search_f(folder=f, ext=ext, relative=False)
      return [dtype(k, p) for k, p in enumerate(path)]

    # Create list of training `Node`
    fname = _path.join(folder, 'trainset')
    self._train_images = _create_type(f=fname, ext=['.jpg'], dtype=Image)
    self._train_landmarks = _create_type(f=fname, ext=['.pts'], dtype=Landmarks)
    # Create list of testing `Node`
    fname = _path.join(folder, 'testset')
    self._test_images = _create_type(f=fname, ext=['.jpg'], dtype=Image)
    self._test_landmarks = _create_type(f=fname, ext=['.pts'], dtype=Landmarks)
    # Sanity check
    self._check_dimensions(sets=[len(self._train_images),
                                 len(self._train_landmarks)],
                           n=2000,
                           msg='Not all training samples have been found')
    self._check_dimensions(sets=[len(self._test_images),
                                 len(self._test_landmarks)],
                           n=330,
                           msg='Not all testing samples have been found')

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    seq = self._train_images if partition == 'train' else self._test_images
    return ListIterator(seq=seq, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = self._train_landmarks if partition == 'train' else \
      self._test_landmarks
    return ListIterator(seq=seq, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    return None

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = self._train_images if partition == 'train' else self._test_images
    return ListIterator(seq=seq, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    attr = getattr(self, '_{}_landmarks'.format(partition))
    return type(attr[0])
