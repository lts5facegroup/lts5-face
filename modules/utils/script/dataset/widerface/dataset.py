# coding=utf-8
"""
WIDERFace dataset

WIDER FACE dataset is a face detection benchmark dataset, of which images are
 selected from the publicly available WIDER dataset. We choose 32,203 images
 and label 393,703 faces with a high degree of variability in scale, pose and
 occlusion as depicted in the sample images. WIDER FACE dataset is organized
 based on 61 event classes. For each event class, we randomly select
 40%/10%/50% data as training, validation and testing sets. We adopt the same
 evaluation metric employed in the PASCAL VOC dataset. Similar to MALF and
 Caltech datasets, we do not release bounding box ground truth for the test
 images. Users are required to submit final prediction files, which we shall
 proceed to evaluate.

See:
  http://shuoyang1213.me/WIDERFACE/index.html
"""
import os.path as _path
import numpy as _np
from enum import Enum, unique
from lts5.utils.dataset.base.dataset import Dataset
from lts5.utils.dataset.base.dataset import DatasetInfo
from lts5.utils.dataset.base.node import Image
from lts5.utils.dataset.base.node import BBox as BBoxNode
from lts5.utils.dataset.base.dataset import MetaData
from lts5.utils.dataset.base.iterator import ListIterator
from lts5.utils.dataset.base.iterator import MetaDataIterator

__author__ = 'Christophe Ecabert'


class WIDERFaceInfo(DatasetInfo):
  """ WIDERFace Information class """

  def name(self):
    """
        Return a simple name for the dataset.
        :return: 'widerface'
        """
    return 'widerface'

  def version(self):
    """
    Return the version number of the package `lts5.utils.version.get_version()`
    :return:  str
    """
    from lts5.utils.version import get_version
    return get_version()

  def partitions(self):
    """
    List all available partitions in the dataset. Possible values are 'train',
    'validation' and 'test'
    :return:  list of str
    """
    return ['train', 'validation', 'test']

  def files(self):
    """
    List all the auxiliary files needed by the dataset. If not use, return an
    empty list
    :return: list
    """
    return []

  def label(self):
    """
    Flag that indicates if the dataset has labels attached to the samples
    :return:  bool
    """
    return True

  def metadata(self):
    """
    Flat that indicates if the dataset has metadata attached to the samples
    :return: bool
    """
    return True


@unique
class Blur(Enum):
  """
  Image blur attributes

  Attributes
  ----------
  kClear: int
    No blur
  kNormal:  int
    Slight blur
  kHeavy: int
    Large blur

  """
  kClear = 0
  kNormal = 1
  kHeavy = 2


@unique
class Expression(Enum):
  """
  Facial expression amplitude

  Attributes
  ----------
  kNormal: int
    Normal facial expression
  kExaggerate: int
    Over-acted facial expression
  """
  kNormal = 0
  kExaggerate = 1


@unique
class Illumination(Enum):
  """
  Illumination type

  Attributes
  ----------
  kNormal: int
    Normal illumination
  kExtreme: int
    Extreme lighting condition
  """
  kNormal = 0
  kExtreme = 1


@unique
class Occlusion(Enum):
  """
  Amount of occlusion in the image

  Attributes
  ----------
  kNo: int
    No occlusion
  kPartial: int
    Partial face occlusions
  kHeavy: int
    Large facial occlusion
  """
  kNo = 0
  kPartial = 1
  kHeavy = 2


@unique
class Pose(Enum):
  """
  Head pose

  Attributes
  ----------
  kTypical: int
    Normal head pose
  kAtypical: int
    Atypical head pose
  """
  kTypical = 0
  kAtypical = 1


@unique
class Invalid(Enum):
  """
  Indicate if the image is valid or not

  Attributes
  ----------
  kFalse: int
    Indicates a valid image
  kTrue: int
    Indicates an invalid image
  """
  kFalse = 0
  kTrue = 1


class Bbox:
  """
  Face bounding box for CelebA dataset stored as follow: x, y, width, height

  Attributes
  ----------
  bbox_id: int
    Bounding box's ID
  x: int
    X origin
  y: int
    Y origin
  width: int
    box width
  height: int
    box height
  """

  def __init__(self, x: int, y: int, width: int, height: int):
    """
    Constructor
    :param x:       X origin
    :param y:       Y origin
    :param width:   Width
    :param height:  Height
    """
    self.x = x
    self.y = y
    self.width = width
    self.height = height


class WIDERFace(Dataset):
  """ WIDERFace Dataset for face alignment """

  def __init__(self, folder):
    """
    Constructor
    :param folder:  Location where the data are stored
    """
    super(WIDERFace, self).__init__(info=WIDERFaceInfo())
    # Build WIDERFace dataset, gather sample + labels
    for part, sz in zip(self.infos.partitions(), [12876, 3226, 16097]):
      # Folder where data are stored
      part_short = part if part != 'validation' else 'val'
      im_folder = _path.join(folder,
                             'data',
                             'images')
      split_folder = _path.join(folder,
                                'data',
                                'split')
      if part_short == 'test':
        # Special case for testing data
        fname = _path.join(split_folder,
                           'wider_face_{}_filelist.txt'.format(part_short))
        with open(fname, 'r') as f:
          images = []
          for line in f:
            # Each line is an entry, there is not labels/metadata for test
            line = line.strip()
            im_path = _path.join(im_folder, line)
            images.append(Image(image_id=k, path=im_path))
          # Reach end of file, set attributes
          setattr(self, '_{}_images'.format(part), images)
          # Sanity check
          msg = 'Not all images in {} partition have been found'.format(part)
          self._check_dimensions(sets=[len(images)],
                                 n=sz,
                                 msg=msg)

      else:
        # Train/validation partition
        fname = _path.join(split_folder,
                           'wider_face_{}_bbx_gt.txt'.format(part_short))
        # The format of txt ground truth.
        # File name
        # Number of bounding box
        # x1, y1, w, h, blur, expression, illumination, invalid, occlusion, pose
        images = []
        bboxes = []
        metadata = []
        with open(fname, 'r') as f:
          line = f.readline().strip()
          k = 0
          while line != '':
            im_path = _path.join(im_folder, line)
            n_bbox = int(f.readline().strip())
            # Iterate over all bounding box
            if n_bbox > 0:
              images.append(Image(image_id=k, path=im_path))
              # Process bbox + attributes
              bbox = []
              attr = []
              for k in range(n_bbox):
                p = list(map(int, f.readline().strip().split(' ')))
                # Box
                bbox.append(Bbox(x=p[0],
                                 y=p[1],
                                 width=p[2],
                                 height=p[3]))
                # Metadata
                meta = MetaData()
                meta.blur = Blur(p[4])
                meta.expression = Expression(p[5])
                meta.illumination = Illumination(p[6])
                meta.invalid = Invalid(p[7])
                meta.occlusion = Occlusion(p[8])
                meta.pose = Pose(p[9])
                attr.append(meta)
              bboxes.append(BBoxNode(bbox_id=k, bbox=bbox))
              metadata.append(meta)
            else:
              # Skip line since they've added a line of zeros when there is no
              # bbox
              f.readline()
            # Read next line
            k += 1
            line = f.readline().strip()
          # Reach end of file, set attributes
          setattr(self, '_{}_images'.format(part), images)
          setattr(self, '_{}_labels'.format(part), bboxes)
          setattr(self, '_{}_metadata'.format(part), metadata)
          # Sanity check
          msg = 'Not all images in {} partition have been found'.format(part)
          self._check_dimensions(sets=[len(images)],
                                 n=sz,
                                 msg=msg)
          msg = 'Not all labels in {} partition have been found'.format(part)
          self._check_dimensions(sets=[len(bboxes)],
                                 n=sz,
                                 msg=msg)
          msg = 'Not all metadata in {} partition have been found'.format(part)
          self._check_dimensions(sets=[len(metadata)],
                                 n=sz,
                                 msg=msg)

  def samples(self, partition=None, **kwargs):
    """
    Provide iterator on the samples for a given `partition` of the dataset.
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def annotations(self, partition=None, **kwargs):
    """
    Provide iterator on the annotation for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_labels'.format(partition))
    return ListIterator(seq=seq, attribute='load')

  def metadata(self, partition=None, **kwargs):
    """
    Provide iterator on the metadata for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_metadata'.format(partition))
    return MetaDataIterator(seq=seq)

  def names(self, partition=None, **kwargs):
    """
    Provide iterator on the Node's name  for a given `partition` of the dataset
    :param partition: Type of partition
    :param kwargs:  Extra arguments
    :return:  iterator or None if not applicable
    """
    self._partition_is_valid(partition=partition)
    seq = getattr(self, '_{}_images'.format(partition))
    return ListIterator(seq=seq, attribute='name')

  def label_type(self, partition):
    """
    Provide the type of labels of the dataset
    :param partition: Partition to look for
    :return:  Types, or None if no labels available
    """
    self._partition_is_valid(partition)
    attr = getattr(self, '_{}_labels'.format(partition))
    return type(attr[0])
