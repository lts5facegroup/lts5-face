# coding=utf-8
""" Various decorator implementation """
import functools
import inspect
import warnings
from lts5.utils.tools import init_logger

_logger = init_logger()


def deprecated(reason):
  """
  This is a decorator which can be used to mark functions
  as deprecated. It will result in a warning being emitted
  when the function is used.
  https://stackoverflow.com/a/40301488
  :param reason: Reason of the deprecation
  """

  if isinstance(reason, (type(b''), type(u''))):
    # The @deprecated is used with a 'reason'.
    #
    # .. code-block:: python
    #
    #    @deprecated("please, use another function")
    #    def old_function(x, y):
    #      pass

    def decorator(func1):
      if inspect.isclass(func1):
        fmt1 = "Call to deprecated class `{name}` ({reason})."
      else:
        fmt1 = "Call to deprecated function `{name}` ({reason})."

      @functools.wraps(func1)
      def new_func1(*args, **kwargs):
        msg = fmt1.format(name=func1.__name__, reason=reason)
        _logger.warning(msg, stacklevel=1)
        return func1(*args, **kwargs)
      return new_func1
    return decorator

  elif inspect.isclass(reason) or inspect.isfunction(reason):

    # The @deprecated is used without any 'reason'.
    #
    # .. code-block:: python
    #
    #    @deprecated
    #    def old_function(x, y):
    #      pass
    func2 = reason
    if inspect.isclass(func2):
      fmt2 = "Call to deprecated class `{name}`."
    else:
      fmt2 = "Call to deprecated function `{name}`."

    @functools.wraps(func2)
    def new_func2(*args, **kwargs):
      msg = fmt2.format(name=func2.__name__)
      _logger.warning(msg, stacklevel=1)
      return func2(*args, **kwargs)
    return new_func2
  else:
    raise TypeError(repr(type(reason)))
