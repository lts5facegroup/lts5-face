/**
 *  @file   image_transforms.cpp
 *  @brief  Useful transforms for cv::Mat images
 *
 *  @author Hua Gao
 *  @date   24/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#include <cstring>

#include "opencv2/core.hpp"

#include "lts5/utils/image_transforms.hpp"
#include "lts5/utils/sys/parallel.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/*
 *  @name   NormalizeScale
 *  @brief  Normalize shape regarding the scale
 *  @param[in]  reference_shape
 *  @param[in]  edge
 *  @param[in]  image               Input image
 *  @param[out] normalized_image    Normalized image used for features
 *                                  extraction
 *  @param[out] normalized_shape    Normalized shape
 *  @param[out] ant                 ?¿?
 *  @param[out] inverse_transform   Transformation applied for normalization
 */
void NormalizeScale(const cv::Mat& reference_shape,
                                const int& edge,
                                const cv::Mat& image,
                                cv::Mat* normalized_image,
                                cv::Mat* normalized_shape,
                                cv::Mat* ant,
                                cv::Mat* inverse_transform) {
  //Get transformation
  cv::Vec4d similarity_transform;
  ComputeSimilarityTransform(reference_shape,
                             *normalized_shape,
                             &similarity_transform);
  double scale = std::sqrt(similarity_transform[0] *
                           similarity_transform[0] +
                           similarity_transform[1] *
                           similarity_transform[1]);
  double dx = edge - image.cols * scale / 2;
  double dy = edge - image.rows * scale / 2;
  cv::Mat transform = (cv::Mat_<double>(2, 3) << scale, 0, dx,
                       0, scale, dy);
  //Get inverse transform
  cv::invertAffineTransform(transform, *inverse_transform);

  if (image.data) {
    cv::warpAffine(image, *normalized_image,
                   transform,
                   cv::Size(edge * 2, edge * 2),
                   cv::INTER_LINEAR,
                   cv::BORDER_REPLICATE);
  }

  *normalized_shape = LTS5::TransformShape(transform,*normalized_shape);
  if (ant->rows == reference_shape.rows) {
    *ant = LTS5::TransformShape(transform, *ant);
  }
}

/*
 *  @name   NormalizeScaleAndRotation
 *  @brief  Normalize shape regarding the scale and rotation
 *  @param[in]  reference_shape
 *  @param[in]  edge
 *  @param[in]  image               Input image
 *  @param[out] normalized_image    Normalized image used for features
 *                                  extraction
 *  @param[out] normalized_shape    Normalized shape
 *  @param[out] ant                 ?¿?
 *  @param[out] transform           Transformation applied for normalization
 */
void NormalizeScaleAndRotation(const cv::Mat& reference_shape,
                               const int& edge,
                               const cv::Mat& image,
                               cv::Mat* normalized_image,
                               cv::Mat* normalized_shape,
                               cv::Mat* ant,
                               cv::Mat* transform) {
  //Get transformation
  cv::Vec4d similarity_transform;
  ComputeSimilarityTransform(reference_shape,
                             *normalized_shape,
                             &similarity_transform);
  *transform = (cv::Mat_<double>(2, 3) << similarity_transform[0],
                -similarity_transform[1], similarity_transform[2],
                similarity_transform[1], similarity_transform[0],
                similarity_transform[3]);
  //Define inverse
  cv::Mat inverse_transform;
  cv::invertAffineTransform(*transform, inverse_transform);
  //Warp images
  if (image.data) {
    cv::warpAffine(image, *normalized_image,
                   *transform,
                   cv::Size(edge * 2, edge * 2),
                   cv::INTER_LINEAR | cv::WARP_INVERSE_MAP,
                   cv::BORDER_REPLICATE);
  }
  //normalized shape
  *normalized_shape = LTS5::TransformShape(inverse_transform,
                                           *normalized_shape);

  if (ant->rows == reference_shape.rows) {
    *ant = LTS5::TransformShape(inverse_transform, *ant);
  }
}
/*
 *  @name BilinearInterpolation
 *  @fn void BilinearInterpolation(const cv::Mat& image,
                                   const float x,
                                   const float y,
                                   float* value)
 *  @brief  Apply bilinear interpolation at location {x,y}
 *  @param[in]  image   Input image to interpolate from
 *  @param[in]  x       X Location
 *  @param[in]  y       Y Location
 *  @param[out] value   Interpolated value
 */
void BilinearInterpolation(const cv::Mat& image,
                           const float x,
                           const float y,
                           float* value) {
  // Get top left postion
  int X = static_cast<int>(x);
  int Y = static_cast<int>(y);
  // Get delta
  float dx = x - static_cast<float>(X);
  float dy = y - static_cast<float>(Y);
  // Interpolate
  float pix_value_i = static_cast<float>(image.at<uchar>(Y, X));
  float pix_value_f = static_cast<float>(image.at<uchar>(Y + 1, X));
  float t1 = dy * (pix_value_f - pix_value_i) + pix_value_i;
  pix_value_i = static_cast<float>(image.at<uchar>(Y, X + 1));
  pix_value_f = static_cast<float>(image.at<uchar>(Y + 1, X + 1));
  float t2 = dy * (pix_value_f - pix_value_i) + pix_value_i;
  // Return value
  *value = dx * (t2 - t1) + t1;
}

/*
 *  @name BilinearInterpolation
 *  @fn void BilinearInterpolation(const cv::Mat& image,
                                   const float x,
                                   const float y,
                                   unsigned char* value)
 *  @brief  Apply bilinear interpolation at location {x,y}
 *  @param[in]  image       Input image to interpolate from
 *  @param[in]  x           X Location
 *  @param[in]  y           Y Location
 *  @param[out] value       Interpolated value
 */
void BilinearInterpolation(const cv::Mat& image,
                           const float x,
                           const float y,
                           unsigned char* value) {
  // Get top left postion
  assert(image.channels() == 1);
  int X = static_cast<int>(x);
  int Y = static_cast<int>(y);
  // Get delta
  float dx = x - std::floor(x);
  float dy = y - std::floor(y);
  float dx1 = 1.f - dx;
  float dy1 = 1.f - dy;
  // Interpolate
  const uchar* p_c00 = image.data + (X + Y * image.cols);
  const uchar* p_c01 = p_c00 + 1;
  const uchar* p_c10 = p_c00 + image.cols;
  const uchar* p_c11 = p_c10 + 1;
  // Compute weight
  int weight[4];
  weight[0] = dx1 * dy1 * 256.f;
  weight[1] = dx * dy1 * 256.f;
  weight[2] = dx1 * dy * 256.f;
  weight[3] = dx * dy * 256.f;
  // Return value
  *value = ((*p_c00 * weight[0] +
             *p_c01 * weight[1] +
             *p_c10 * weight[2] +
             *p_c11 * weight[3]) >> 8);
}

/*
 *  @name PieceWiseWarping
 *  @fn void PieceWiseWarping(const std::vector<LTS5::Vector2<float>>& src_pts,
                           const std::vector<LTS5::Vector3<int>>& list_tri,
                           const std::vector<LTS5::Vector2<float>>& target_pts,
                           const cv::Mat& image_src,
                           const float scale,
                           const int target_width,
                           const int target_height,
                           const LTS5::WarpingColorSpace color_frmt,
                           const bool inv_y_axis,
                           const int background_value,
                           unsigned char* target_data_array)
 *  @brief  Piecewise warping of a triangulated mesh
 *  @param[in]  src_pts       List of point (source)
 *  @param[in]  list_tri      Source point triangulation
 *  @param[in]  target_pts    Target points
 *  @param[in]  image_src     Input image
 *  @param[in]  scale         Scaling factor for target points
 *  @param[in]  target_width  Target image width
 *  @param[in]  target_height Target image height
 *  @param[in]  color_frmt    Target color format
 *  @param[in]  inv_y_axis    Indice if y axis need to be inverted in the target image
 *  @param[in]  background_value  Background color, if <0 not used
 *  @param[out] target_image  Warpped image
 */
void PieceWiseWarping(const std::vector<LTS5::Vector2<float>>& src_pts,
                      const std::vector<LTS5::Vector3<int>>& list_tri,
                      const std::vector<LTS5::Vector2<float>>& target_pts,
                      const cv::Mat& image_src,
                      const float scale,
                      const int target_width,
                      const int target_height,
                      const LTS5::WarpingColorSpace color_frmt,
                      const bool inv_y_axis,
                      const int background_value,
                      unsigned char* target_data_array) {
  using Vec2 = LTS5::Vector2<float>;
  using Vec3 =  LTS5::Vector3<float>;

  // Set target memory to '0'
  if (background_value > 0) {
    size_t n_byte = static_cast<size_t>(target_height * target_width *
                                        static_cast<int>(color_frmt));
    std::memset(reinterpret_cast<void*>(target_data_array),
                128,
                n_byte);
  }
  int n_src_channel = image_src.channels();
  assert((n_src_channel == 1 && color_frmt == 1) ||
         (n_src_channel == 3 && color_frmt == 3) ||
         (n_src_channel == 1 && color_frmt == 3));

  // Define type of output
  int rtype = -1;
  if (n_src_channel == 1 && color_frmt == kGray ) {
    // Grayscale in/out
    rtype = 1;
  } else if (n_src_channel == 1 && ((color_frmt == kBGR) || (color_frmt == kRGB))) {
    // Inpupt gray, out = color -> replicate
    rtype = 2;
  } else if (n_src_channel == 3 && color_frmt == kBGR) {
    // In color ocv, out color ocv
    rtype = 3;
  } else if (n_src_channel == 3 && color_frmt == kRGB) {
    // In color ocv, out color gui
    rtype = 4;
  } else {
    // Not supported
    std::cout << "Color format not supported by warping function" << std::endl;
    return;
  }

  // Loop over every triangle
  const int stride = static_cast<int>(color_frmt);
  const int n_col = target_width;
  const int n_row = target_height;

  Parallel::For(list_tri.size(),
                [&](const size_t& tri) {
    auto tri_it = std::next(list_tri.begin(), tri);
    // Get source pts
    Vec2 p0_s = src_pts[tri_it->x_];
    Vec2 p1_s = src_pts[tri_it->y_];
    Vec2 p2_s = src_pts[tri_it->z_];
    // Define target point, apply scaling
    const Vec2 p0_t = target_pts[tri_it->x_] * scale;
    const Vec2 p1_t = target_pts[tri_it->y_] * scale;
    const Vec2 p2_t = target_pts[tri_it->z_] * scale;

    // Get affine transformation
    float srcs[] = {p0_s.x_, p0_s.y_, p1_s.x_, p1_s.y_, p2_s.x_, p2_s.y_};
    float dest[] = {p0_t.x_, p0_t.y_, p1_t.x_, p1_t.y_, p2_t.x_, p2_t.y_};
    // Find Affine transform from target to source
    cv::Mat M = cv::getAffineTransform(reinterpret_cast<cv::Point2f*>(&dest[0]),
                                       reinterpret_cast<cv::Point2f*>(&srcs[0]));
    cv::Mat Mf;
    M.convertTo(Mf, CV_32F);
    const float* m_ptr = reinterpret_cast<float*>(Mf.data);
    // Define bbox
    float xmin = -1.0, xmax = -1.0;
    float ymin = -1.0, ymax = -1.0;
    if (p0_t.x_ <= p1_t.x_ && p0_t.x_ <= p2_t.x_) {
      // p0_t.x == xmin
      xmin = p0_t.x_;
      // xmax ?
      xmax = p1_t.x_ > p2_t.x_ ? p1_t.x_ : p2_t.x_;
    } else if (p1_t.x_ <= p0_t.x_ && p1_t.x_ <= p2_t.x_) {
      // p1_t.x == xmin
      xmin = p1_t.x_;
      // xmax ?
      xmax = p0_t.x_ > p2_t.x_ ? p0_t.x_ : p2_t.x_;
    } else {
      // p2_t.x == xmin
      xmin = p2_t.x_;
      xmax = p1_t.x_ > p0_t.x_ ? p1_t.x_ : p0_t.x_;
    }
    if (p0_t.y_ <= p1_t.y_ && p0_t.y_ <= p2_t.y_) {
      // p0_t.y == ymin
      ymin = p0_t.y_;
      // xmax ?
      ymax = p1_t.y_ > p2_t.y_ ? p1_t.y_ : p2_t.y_;
    } else if (p1_t.y_ <= p0_t.y_ && p1_t.y_ <= p2_t.y_) {
      // p1_t.y == ymin
      ymin = p1_t.y_;
      // ymax ?
      ymax = p0_t.y_ > p2_t.y_ ? p0_t.y_ : p2_t.y_;
    } else {
      // p2_t.y == xmin
      ymin = p2_t.y_;
      ymax = p1_t.y_ > p0_t.y_ ? p1_t.y_ : p0_t.y_;
    }

    // Loop over patch define by bbox
    if ((int)xmin != (int)xmax) {
      xmin = std::ceil(xmin);
      xmax = std::floor(xmax);
    }
    if ((int)ymin != (int)ymax) {
      ymin = std::ceil(ymin);
      ymax = std::floor(ymax);
    }
    assert(xmin <= xmax && ymin <= ymax);

    // Local coordinate system
    Vec3 X = Vec3(p1_t.x_ - p0_t.x_, p1_t.y_ - p0_t.y_, 0.f);
    Vec3 Y = Vec3(p2_t.x_ - p0_t.x_, p2_t.y_ - p0_t.y_, 0.f);
    Vec3 p;
    float xp = 1.0f, yp = -1.0f;
    float xs = 1.f, ys = -1.0;   // Target pts in source image
    unsigned char color[3] = {0, 0 ,0};
    int idx = -1;
    int xi = -1, yi = -1;
    for (float x = xmin; x <= xmax; ++x) {
      for (float y = ymin; y <= ymax; ++y) {
        // Find x,y component in sys
        p.x_ = x - p0_t.x_;
        p.y_ = y - p0_t.y_;

        Vec3 XcrossP = X ^ p;
        Vec3 XcrossY = X ^ Y;
        if (XcrossP * XcrossY < 0) continue;

        Vec3 YcrossP = Y ^ p;
        Vec3 YcrossX = Y ^ X;
        if (YcrossP * YcrossX < 0) continue;

        float n = XcrossY.Norm();
        xp = YcrossP.Norm() / n;
        yp = XcrossP.Norm() / n;

        // Inside triangle ?
        if ((xp + yp) > 1.0f) continue;

        // Iniside
        xs = m_ptr[0] * x + m_ptr[1] * y + m_ptr[2];
        ys = m_ptr[3] * x + m_ptr[4] * y + m_ptr[5];
        // Interpolate
        LTS5::BilinearInterpolation(image_src, xs, ys, &color[0]);
        // Set value
        xi = static_cast<int>(x);
        yi = inv_y_axis ?
             n_row - static_cast<int>(y) :
             static_cast<int>(y);
        idx = ((yi * n_col) + xi) * stride;

        if (rtype == 1) {
          // Grayscale in/out
          target_data_array[idx] = color[0];
        } else if (rtype == 2) {
          // Inpupt gray, out = color -> replicate
          target_data_array[idx] = color[0];
          target_data_array[idx + 1] = color[0];
          target_data_array[idx + 2] = color[0];
        } else if (rtype == 3) {
          // In color ocv, out color ocv
          target_data_array[idx] = color[0];
          target_data_array[idx + 1] = color[1];
          target_data_array[idx + 2] = color[2];
        } else {
          target_data_array[idx] = color[2];
          target_data_array[idx + 1] = color[1];
          target_data_array[idx + 2] = color[0];
        }
      }
    }
  });
}



}
