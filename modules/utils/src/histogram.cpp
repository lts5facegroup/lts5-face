/**
 *  @file   histogram.cpp
 *  @brief  Simple histogram implementation
 *  @ingroup    utils
 *
 *  @author Christophe Ecabert
 *  @date   6/16/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#include <algorithm>
#include <random>
#include <iterator>
#include <chrono>

#include "lts5/utils/math/histogram.hpp"
#include "lts5/utils/bit_array.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name  Build
 * @fn    void Build(const cv::Mat& data, const int& bin_per_channel)
 * @brief Build histogram for a given set of data points
 * @param[in] data    2D array of data points of dimensions NxK to use to
 *                    build the histogram. Each row is considered as a sample
 *                    with K dimension features.
 * @param[in] range   Data range for each channel (i.e. K pairs <min, max>)
 * @param[in] bin_per_channel Number of bins per feature channel.
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int Histogram<T>::Build(const cv::Mat& data,
                        const std::vector<Range>& ranges,
                        const size_t& bin_per_channel) {
  int err = -1;
  // Clear current state if any
  bins_.clear();
  ranges_.clear();
  offsets_.clear();
  total_bins_ = 0;
  bin_per_channel_ = 0;
  count_ = 0;
  if (data.cols == ranges.size()) {
    // Initialize histogram data structure
    offsets_.resize(data.cols);
    ranges_ = ranges;
    bin_per_channel_ = bin_per_channel;
    total_bins_ = std::pow(bin_per_channel, data.cols);
    bins_.resize(this->total_bins_);
    // Compute offsets
    for (size_t k = 0; k < offsets_.size(); ++k) {
      offsets_[k] = std::pow(bin_per_channel, offsets_.size() - 1 - k);
    }
    // Build histogram
    for (int idx = 0; idx < data.rows; ++idx) {
      // Get beginning of sample
      auto bin_idx = this->GetBinIndex(data.ptr<T>(idx), data.cols);
      this->bins_[bin_idx].push_back(idx);
      count_ += 1;
    }
    err = 0;
  }
  return err;
}

/*
 * @name  GetBinIndex
 * @fn    int GetBinIndex(const T* sample_ptr)
 * @brief Compute the bin index of a given sample
 * @param[in] sample_ptr Pointer to the sample array.
 * @param[in] length      Array dimension
 * @tparam T Underlying data type
 * @return    Bin index
 */
template<typename T>
size_t Histogram<T>::GetBinIndex(const T* sample_ptr, int length) {
  size_t idx = 0;
  for (int k = 0; k < length; ++k) {
    const auto& r = ranges_[k];
    const auto& offset = offsets_[k];
    const auto& value = sample_ptr[k];
    T slope = static_cast<T>(bin_per_channel_) / (r.second - r.first);
    auto ix = static_cast<size_t>(std::floor(slope * (value - r.first)));
    ix = std::min(ix, bin_per_channel_ - 1);
    idx += ix * offset;
  }
  return idx;
}

#pragma mark -
#pragma mark Functions

bool isBinEntirelySampled(const BitArray& mask,
                          const size_t& start,
                          const size_t& end){
  bool flag = true;
  for (size_t k = start; k < end; ++k) {
    flag &= mask.Get(k);
    if (!flag) {
      break;
    }
  }
  return flag;
}

/*
 * @name    UniformHistogramSampling
 * @fn  void UniformHistogramSampling(const Histogram<T>& histogram,
                                      const int& n_sample,
                                      std::vector<size_t>* indices)
 * @brief Sample an histogram uniformly
 * @param[in] histogram Histogram to sample from
 * @param[in] n_sample Number of samples to draw
 * @param[out] indices Sampled indices
 * @tparam T    Data type
 */
template<typename T>
void UniformHistogramSampling(const Histogram<T>& histogram,
                              const size_t& n_sample,
                              std::vector<size_t>* indices) {
  // Ensure not too many samples are askesd
  auto ns = std::min(n_sample, histogram.count());
  // Histrogram bins
  const auto& bins = histogram.get_bins();
  // Indicates if data points is already sampled
  BitArray is_sampled(histogram.count());
  BitArray is_bin_empty(bins.size());
  std::mt19937 gen(std::chrono::system_clock::now().time_since_epoch().count());

  // Define the starting index of each bins
  std::vector<size_t> starting_indexes(bins.size());
  starting_indexes[0] = 0;
  for (size_t k = 1; k < bins.size(); ++k) {
    starting_indexes[k] = starting_indexes[k-1] + bins[k-1].size();
  }
  // Start sampling
  indices->resize(ns);
  size_t i = 0;
  while (i < ns) {
    // Iterating through every bin and picking one point at random, until the
    // required number of points are sampled.
    for (size_t k = 0; k < bins.size(); ++k) {
      // Get bin size, and check if we can sample it
      const auto& bin = bins[k];
      auto bin_sz = bin.size();
      if (bin_sz == 0 || is_bin_empty.Get(k)) {
        continue;
      }
      // Select randomly within this bin
      size_t random_index = 0;
      size_t pos = 0;
      std::uniform_int_distribution<unsigned> dist(0, bin_sz - 1);
      do {
        random_index = dist(gen);
        pos = starting_indexes[k] + random_index;
      } while (is_sampled.Get(pos));
      // Candidate is valid, update mask
      is_sampled.Set(pos);
      // Check if every sample in the current bin have been sampled
      auto binFull = isBinEntirelySampled(is_sampled,
                                          starting_indexes[k],
                                          starting_indexes[k] + bin.size());
      if (binFull) {
        is_bin_empty.Set(k);
      }
      // Get sampled index
      size_t index = *std::next(bin.begin(), random_index);
      indices->at(i) = index;
      i++;
      if (i == ns) {
        break;
      }
    }
  }
}

/*
 * @name    WeightedHistogramSampling
 * @fn  void WeightedHistogramSampling(const Histogram<T>& histogram,
                                      const int& n_sample,
                                      std::vector<size_t>* indices)
 * @brief Sample an histogram with the same distribution (i.e. will draw more
 *  samples from most populated bins)
 * @param[in] histogram Histogram to sample from
 * @param[in] n_sample Number of samples to draw
 * @param[out] indices Sampled indices
 * @tparam T    Data type
 */
template<typename T>
void WeightedHistogramSampling(const Histogram<T>& histogram,
                               const size_t& n_sample,
                               std::vector<size_t>* indices) {
  // Ensure not too many samples are askesd
  auto ns = std::min(n_sample, histogram.count());
  // Histrogram bins
  const auto& bins = histogram.get_bins();
  std::vector<size_t> bin_count;
  for (const auto& bin : bins) {
    bin_count.push_back(bin.size());
  }
  // Indicates if data points is already sampled
  BitArray is_sampled(histogram.count());
  BitArray is_bin_empty(bins.size());
  std::mt19937 gen(std::chrono::system_clock::now().time_since_epoch().count());

  // Define the starting index of each bins
  std::vector<size_t> starting_indexes(bins.size());
  starting_indexes[0] = 0;
  for (size_t k = 1; k < bins.size(); ++k) {
    starting_indexes[k] = starting_indexes[k-1] + bins[k-1].size();
  }
  // Start sampling
  indices->resize(ns);
  size_t i = 0;
  std::discrete_distribution<int> bin_dist(bin_count.begin(), bin_count.end());
  while (i < ns) {
    // Iterating through every bin and picking one point at random, until the
    // required number of points are sampled.
    // Randomly select bin index
    int rnd_bin_idx = bin_dist(gen);
    // Get bin size, and check if we can sample it
    const auto& bin = bins[rnd_bin_idx];
    auto bin_sz = bin.size();
    if (bin_sz == 0 || is_bin_empty.Get(rnd_bin_idx)) {
      continue;
    }
    // Select randomly within this bin
    size_t random_index = 0;
    size_t pos = 0;
    std::uniform_int_distribution<unsigned> dist(0, bin_sz - 1);
    do {
      random_index = dist(gen);
      pos = starting_indexes[rnd_bin_idx] + random_index;
    } while (is_sampled.Get(pos));
    // Candidate is valid, update mask
    is_sampled.Set(pos);
    // Check if every sample in the current bin have been sampled
    auto binFull = isBinEntirelySampled(is_sampled,
                                        starting_indexes[rnd_bin_idx],
                                        starting_indexes[rnd_bin_idx] +
                                        bin.size());
    if (binFull) {
      is_bin_empty.Set(rnd_bin_idx);
    }
    // Get sampled index
    size_t index = *std::next(bin.begin(), random_index);
    indices->at(i) = index;
    i++;
  }
}

#pragma mark -
#pragma mark Explicit Instanciation

/** Float */
template class Histogram<float>;
/** Double */
template class Histogram<double>;

/** Sampling - float */
template void UniformHistogramSampling<float>(const Histogram<float>&,
                                              const size_t&,
                                              std::vector<size_t>*);
template void WeightedHistogramSampling<float>(const Histogram<float>&,
                                               const size_t&,
                                               std::vector<size_t>*);
/** Sampling - float */
template void UniformHistogramSampling<double>(const Histogram<double>&,
                                               const size_t&,
                                               std::vector<size_t>*);
template void WeightedHistogramSampling<double>(const Histogram<double>&,
                                                const size_t&,
                                                std::vector<size_t>*);


}  // namespace LTS5
