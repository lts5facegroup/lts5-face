/**
 *  @file   linear_algebra_wrapper.cpp
 *  @brief  Common linear algebra wrapper between OpenCV and
 *          BLAS+Lapack library
 *
 *  @author Christophe Ecabert
 *  @date   05/01/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <cassert>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <algorithm>

#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/utils/math/linear_algebra_types.hpp"
#include "lts5/utils/logger.hpp"

#include "Eigen/Core"

// LAPACK shenanigans
#ifdef __APPLE__
// Float
#define LTS5_ssytrf(...) ssytrf_(__VA_ARGS__)
#define LTS5_ssytri(...) ssytri_(__VA_ARGS__)
#define LTS5_spotrf(...) spotrf_(__VA_ARGS__)
#define LTS5_spotri(...) spotri_(__VA_ARGS__)
#define LTS5_slamch(...) slamch_(__VA_ARGS__)
#define LTS5_ssyevx(...) ssyevx_(__VA_ARGS__)
#define LTS5_sgeev(...) sgeev_(__VA_ARGS__)
#define LTS5_sgesvd(...) sgesvd_(__VA_ARGS__)
#define LTS5_sgesdd(...) sgesdd_(__VA_ARGS__)
#define LTS5_sgels(...) sgels_(__VA_ARGS__)
#define LTS5_sgesv(...) sgesv_(__VA_ARGS__)
#define LTS5_sgeqrf(...) sgeqrf_(__VA_ARGS__)
#define LTS5_sorgqr(...) sorgqr_(__VA_ARGS__)
#define LTS5_sormqr(...) sormqr_(__VA_ARGS__)
// Double
#define LTS5_dsytrf(...) dsytrf_(__VA_ARGS__)
#define LTS5_dsytri(...) dsytri_(__VA_ARGS__)
#define LTS5_dpotrf(...) dpotrf_(__VA_ARGS__)
#define LTS5_dpotri(...) dpotri_(__VA_ARGS__)
#define LTS5_dlamch(...) dlamch_(__VA_ARGS__)
#define LTS5_dsyevx(...) dsyevx_(__VA_ARGS__)
#define LTS5_dgeev(...) dgeev_(__VA_ARGS__)
#define LTS5_dgesvd(...) dgesvd_(__VA_ARGS__)
#define LTS5_dgesdd(...) dgesdd_(__VA_ARGS__)
#define LTS5_dgels(...) dgels_(__VA_ARGS__)
#define LTS5_dgesv(...) dgesv_(__VA_ARGS__)
#define LTS5_dgeqrf(...) dgeqrf_(__VA_ARGS__)
#define LTS5_dorgqr(...) dorgqr_(__VA_ARGS__)
#define LTS5_dormqr(...) dormqr_(__VA_ARGS__)
#else
#define LTS5_ssytrf(...) LAPACK_ssytrf(__VA_ARGS__)
#define LTS5_ssytri(...) LAPACK_ssytri(__VA_ARGS__)
#define LTS5_spotrf(...) LAPACK_spotrf(__VA_ARGS__)
#define LTS5_spotri(...) LAPACK_spotri(__VA_ARGS__)
#define LTS5_slamch(...) LAPACK_slamch(__VA_ARGS__)
#define LTS5_ssyevx(...) LAPACK_ssyevx(__VA_ARGS__)
#define LTS5_sgeev(...) LAPACK_sgeev(__VA_ARGS__)
#define LTS5_sgesvd(...) LAPACK_sgesvd(__VA_ARGS__)
#define LTS5_sgesdd(...) LAPACK_sgesdd(__VA_ARGS__)
#define LTS5_sgels(...) LAPACK_sgels(__VA_ARGS__)
#define LTS5_sgesv(...) LAPACK_sgesv(__VA_ARGS__)
#define LTS5_sgeqrf(...) LAPACK_sgeqrf(__VA_ARGS__)
#define LTS5_sorgqr(...) LAPACK_sorgqr(__VA_ARGS__)
#define LTS5_sormqr(...) LAPACK_sormqr(__VA_ARGS__)
// Double
#define LTS5_dsytrf(...) LAPACK_dsytrf(__VA_ARGS__)
#define LTS5_dsytri(...) LAPACK_dsytri(__VA_ARGS__)
#define LTS5_dpotrf(...) LAPACK_dpotrf(__VA_ARGS__)
#define LTS5_dpotri(...) LAPACK_dpotri(__VA_ARGS__)
#define LTS5_dlamch(...) LAPACK_dlamch(__VA_ARGS__)
#define LTS5_dsyevx(...) LAPACK_dsyevx(__VA_ARGS__)
#define LTS5_dgeev(...) LAPACK_dgeev(__VA_ARGS__)
#define LTS5_dgesvd(...) LAPACK_dgesvd(__VA_ARGS__)
#define LTS5_dgesdd(...) LAPACK_dgesdd(__VA_ARGS__)
#define LTS5_dgels(...) LAPACK_dgels(__VA_ARGS__)
#define LTS5_dgesv(...) LAPACK_dgesv(__VA_ARGS__)
#define LTS5_dgeqrf(...) LAPACK_dgeqrf(__VA_ARGS__)
#define LTS5_dorgqr(...) LAPACK_dorgqr(__VA_ARGS__)
#define LTS5_dormqr(...) LAPACK_dormqr(__VA_ARGS__)
#endif


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark L2 Norm

/*
 *  @name L2Norm
 *  @fn T L2Norm(const cv::Mat& vector)
 *  @brief  Compute the L2 norm of a vector
 *  @param[in]  vector  Vector to compute the norm
 *  @return L2 Norm
 */
template<typename T>
T LinearAlgebra<T>::L2Norm(const cv::Mat& vector) {
  // stub
  return -1.0;
}

template<>
float LinearAlgebra<float>::L2Norm(const cv::Mat& vector) {
  // Only float
  assert(vector.channels() == 1 &&
         (vector.type() == cv::DataType<float>::type) &&
         (vector.rows == 1 || vector.cols == 1));
  // Dot product
  const int& N = std::max(vector.cols, vector.rows);
  return cblas_snrm2(N, reinterpret_cast<const float*>(vector.data), 1);
}

template<>
double LinearAlgebra<double>::L2Norm(const cv::Mat& vector) {
  // Only float
  assert(vector.channels() == 1 &&
         (vector.type() == cv::DataType<double>::type) &&
         (vector.rows == 1 || vector.cols == 1));
  // Dot product
  const int& N = std::max(vector.cols, vector.rows);
  return cblas_dnrm2(N, reinterpret_cast<const double*>(vector.data), 1);
}

#pragma mark -
#pragma mark Mean

/*
 *  @name Mean
 *  @fn T Mean(const cv::Mat& matrix, const int& direction)
 *  @brief  Compute average value of a matrix in a given direction
 *  @param[in]  matrix  Data matrix
 *  @param[in]  direction 0 = row, other = column
 *  @param[out] mean  Average values
 */
template<typename T>
void LinearAlgebra<T>::Mean(const cv::Mat& matrix,
                            const int& direction,
                            cv::Mat* mean) {
  // stub
}

template<>
void LinearAlgebra<float>::Mean(const cv::Mat& matrix,
                                const int& direction,
                                cv::Mat* mean) {
  // Float specialization
  assert(matrix.type() == CV_32FC1);
  // Select direction
  if (direction == 0) {
    // by row
    mean->create(matrix.rows, 1, matrix.type());
    cv::Mat one = cv::Mat(matrix.cols ,1 , matrix.type(), cv::Scalar(1.f));
    const float alpha = 1.0f / static_cast<float>(matrix.cols);
    cblas_sgemv(CBLAS_ORDER::CblasRowMajor,
                CBLAS_TRANSPOSE::CblasNoTrans,
                matrix.rows,
                matrix.cols,
                alpha,
                reinterpret_cast<const float*>(matrix.data),
                matrix.cols,
                reinterpret_cast<const float*>(one.data),
                1,
                0.0f,
                reinterpret_cast<float*>(mean->data),
                1);
  } else {
    // by column
    mean->create(1, matrix.cols, matrix.type());
    cv::Mat one = cv::Mat(matrix.rows, 1, matrix.type(), cv::Scalar(1.f));
    const float alpha = 1.0f / static_cast<float>(matrix.rows);
    cblas_sgemv(CBLAS_ORDER::CblasRowMajor,
                CBLAS_TRANSPOSE::CblasTrans,
                matrix.rows,
                matrix.cols,
                alpha,
                reinterpret_cast<const float*>(matrix.data),
                matrix.cols,
                reinterpret_cast<const float*>(one.data),
                1,
                0.0f,
                reinterpret_cast<float*>(mean->data),
                1);
  }
}

template<>
void LinearAlgebra<double>::Mean(const cv::Mat& matrix,
                                const int& direction,
                                cv::Mat* mean) {
  // Double specialization
  assert(matrix.type() == CV_64FC1);
  // Select direction
  if (direction == 0) {
    // by row
    mean->create(matrix.rows, 1, matrix.type());
    cv::Mat one = cv::Mat(matrix.cols ,1 , matrix.type(), cv::Scalar(1.0));
    const double alpha = 1.0 / static_cast<double>(matrix.cols);
    cblas_dgemv(CBLAS_ORDER::CblasRowMajor,
                CBLAS_TRANSPOSE::CblasNoTrans,
                matrix.rows,
                matrix.cols,
                alpha,
                reinterpret_cast<const double*>(matrix.data),
                matrix.cols,
                reinterpret_cast<const double*>(one.data),
                1,
                0.0f,
                reinterpret_cast<double*>(mean->data),
                1);
  } else {
    // by column
    mean->create(1, matrix.cols, matrix.type());
    cv::Mat one = cv::Mat(matrix.rows, 1, matrix.type(), cv::Scalar(1.f));
    const double alpha = 1.0 / static_cast<double>(matrix.rows);
    cblas_dgemv(CBLAS_ORDER::CblasRowMajor,
                CBLAS_TRANSPOSE::CblasTrans,
                matrix.rows,
                matrix.cols,
                alpha,
                reinterpret_cast<const double*>(matrix.data),
                matrix.cols,
                reinterpret_cast<const double*>(one.data),
                1,
                0.0f,
                reinterpret_cast<double*>(mean->data),
                1);
  }
}

#pragma mark -
#pragma mark Copy

/*
 *  @name Mean
 *  @fn void Mean(const cv::Mat& src, cv::Mat* dest)
 *  @brief  Copy \p src matrix into a \p dest matrix
 *  @param[in]  src  Source
 *  @param[in]  dest Destination
 */
template<typename T>
void LinearAlgebra<T>::Copy(const cv::Mat& src, cv::Mat* dest) {
  // stub
}

template<>
void LinearAlgebra<float>::Copy(const cv::Mat& src, cv::Mat* dest) {
  // Float specialization
  assert(src.type() == CV_32FC1);
  // Define output
  dest->create(src.rows, src.cols, src.type());
  // Copy
  const int& n = static_cast<int>(src.total());
  cblas_scopy(n,
              reinterpret_cast<const lapack_flt*>(src.data),
              1,
              reinterpret_cast<lapack_flt*>(dest->data),
              1);
}

template<>
void LinearAlgebra<double>::Copy(const cv::Mat& src, cv::Mat* dest) {
  // Float specialization
  assert(src.type() == CV_64FC1);
  // Define output
  dest->create(src.rows, src.cols, src.type());
  // Copy
  const int& n = static_cast<int>(src.total());
  cblas_dcopy(n,
              reinterpret_cast<const lapack_dbl*>(src.data),
              1,
              reinterpret_cast<lapack_dbl*>(dest->data),
              1);
}

#pragma mark -
#pragma mark Axpby

/*
 *  @name Axpby
 *  @fn void Axpby(const int& N, const T* A, const int& N, const T alpha,
                  const T beta, T* B)
 *  @brief  Compute : B := alpha * A + beta * B
 *  @param[in]        N       Array dimension
 *  @param[in]        A       Array A
 *  @param[in]        alpha   Alpha scaling factor
 *  @param[in]        beta    Beta scaling factor
 *  @param[in,out]    B       Array B
 */
template<typename T>
void LinearAlgebra<T>::Axpby(const int& N, const T* A, const T alpha,
                             const T beta, T* B) {
  // Stub
}

template<>
void LinearAlgebra<float>::Axpby(const int& N, const float* A, const float alpha,
                                 const float beta, float* B) {
#ifdef __APPLE__
  catlas_saxpby(N,
                alpha,
                A,
                1,
                beta,
                B,
                1);
#else
  cblas_saxpby(N,
               alpha,
               A,
               1,
               beta,
               B,
               1);
#endif
}

template<>
void LinearAlgebra<double>::Axpby(const int& N, const double* A, const double alpha,
                                 const double beta, double* B) {
#ifdef __APPLE__
  catlas_daxpby(N,
                alpha,
                A,
                1,
                beta,
                B,
                1);
#else
  cblas_daxpby(N,
               alpha,
               A,
               1,
               beta,
               B,
               1);
#endif
}

/*
 *  @name Axpby
 *  @fn void Axpby(const cv::Mat& A, const T alpha,
 *                 const T beta, cv::Mat* B)
 *  @brief  Compute : B := alpha * A + beta * B
 *  @param[in]      A         Vector/Matrix A
 *  @param[in]      alpha     Alpha scaling factor
 *  @param[in]      beta      Beta scaling factor
 *  @param[in,out]  vector_b  Vector/Matrix B
 */
template<typename T>
void LinearAlgebra<T>::Axpby(const cv::Mat& A,
                             const T alpha,
                             const T beta,
                             cv::Mat* B) {
  // Stub
}

template<>
void LinearAlgebra<float>::Axpby(const cv::Mat& A,
                                 const float alpha,
                                 const float beta,
                                 cv::Mat* B) {
  // Float implementation
  assert(A.type() == CV_32FC1);
  // Define output
  B->create(A.rows, A.cols, A.type());
  // Call
  const int& N = static_cast<int>(A.total());
#ifdef __APPLE__
  catlas_saxpby(N,
                alpha,
                reinterpret_cast<const lapack_flt*>(A.data),
                1,
                beta,
                reinterpret_cast<lapack_flt*>(B->data),
                1);
#else
  cblas_saxpby(N,
               alpha,
               reinterpret_cast<const lapack_flt*>(A.data),
               1,
               beta,
               reinterpret_cast<lapack_flt*>(B->data),
               1);
#endif
}

template<>
void LinearAlgebra<double>::Axpby(const cv::Mat& A,
                                  const double alpha,
                                  const double beta,
                                  cv::Mat* B) {
  // Double implementation
  assert(A.type() == CV_64FC1);
  // Define output
  B->create(A.rows, A.cols, A.type());
  // Call
  const int& N = static_cast<int>(A.total());
#ifdef __APPLE__
  catlas_daxpby(N,
                alpha,
                reinterpret_cast<const lapack_dbl*>(A.data),
                1,
                beta,
                reinterpret_cast<lapack_dbl*>(B->data),
                1);
#else
  cblas_daxpby(N,
               alpha,
               reinterpret_cast<const lapack_dbl*>(A.data),
               1,
               beta,
               reinterpret_cast<lapack_dbl*>(B->data),
               1);
#endif
}


#pragma mark -
#pragma mark Axpy

/*
 *  @name Axpy
 *  @fn void Axpy(const cv::Mat& A, const T alpha, cv::Mat* B)
 *  @brief  Compute : B := alpha * A + B
 *  @param[in]      A         Vector/Matrix A
 *  @param[in]      alpha     Alpha scaling factor
 *  @param[in,out]  B         Vector/Matrix B
 */
template<typename T>
void LinearAlgebra<T>::Axpy(const cv::Mat& A,
                            const T alpha,
                            cv::Mat* B) {
  // stub
}

template<>
void LinearAlgebra<float>::Axpy(const cv::Mat& A,
                                const float alpha,
                                cv::Mat* B) {
  // Float specialization
  assert(A.type() == CV_32FC1);
  // Assign output
  B->create(A.rows, A.cols, A.type());
  const int& N = static_cast<int>(A.total());
  cblas_saxpy(N,
              alpha,
              reinterpret_cast<const lapack_flt*>(A.data),
              1,
              reinterpret_cast<lapack_flt*>(B->data),
              1);
}

template<>
void LinearAlgebra<double>::Axpy(const cv::Mat& A,
                                const double alpha,
                                cv::Mat* B) {
  // Double specialization
  assert(A.type() == CV_64FC1);
  // Assign output
  B->create(A.rows, A.cols, A.type());
  const int& N = static_cast<int>(A.total());
  cblas_daxpy(N,
              alpha,
              reinterpret_cast<const lapack_dbl*>(A.data),
              1,
              reinterpret_cast<lapack_dbl*>(B->data),
              1);
}

#pragma mark -
#pragma mark Dot

/*
 *  @name   Dot
 *  @fn T Dot(const cv::Mat& vector_a, const cv::Mat& vector_b)
 *  @brief  Compute the dot product between two vectors
 *  @param[in]  vector_a  Vector A
 *  @param[in]  vector_b  Vector B
 *  @return Dot product
 */
template<typename T>
T LTS5_EXPORTS Dot(const cv::Mat& vector_a, const cv::Mat& vector_b) {
  // stub
  return -1.0;
}

template<>
float LinearAlgebra<float>::Dot(const cv::Mat& vector_a,
                                const cv::Mat& vector_b) {
  // Float specialization
  assert(vector_a.channels() == 1 && vector_b.channels() == 1 &&
         (vector_a.type() == CV_32FC1) &&
         (vector_b.type() == CV_32FC1));
  //Call cblas function
  const int& N = std::max(vector_a.rows,vector_a.cols);
  assert(N == std::max(vector_b.rows,vector_b.cols));
  return cblas_sdot(N,
                    reinterpret_cast<const float*>(vector_a.data),
                    1,
                    reinterpret_cast<const float*>(vector_b.data),
                    1);
}

template<>
double LinearAlgebra<double>::Dot(const cv::Mat& vector_a,
                                  const cv::Mat& vector_b) {
  // Float specialization
  assert(vector_a.channels() == 1 && vector_b.channels() == 1 &&
         (vector_a.type() == CV_64FC1) &&
         (vector_b.type() == CV_64FC1));
  //Call cblas function
  const int& N = std::max(vector_a.rows,vector_a.cols);
  assert(N == std::max(vector_b.rows,vector_b.cols));
  return cblas_ddot(N,
                    reinterpret_cast<const double*>(vector_a.data),
                    1,
                    reinterpret_cast<const double*>(vector_b.data),
                    1);
}

/*
 *  @name   Dot
 *  @fn T Dot(const int& N, const T* A, const int& inc_a, const T* B, const int& inc_b)
 *  @brief  Compute the dot product between two vectors
 *  @param[in]  A  Vector A
 *  @param[in]  inc_a     Stride within vector_A. For example, if inc_a
 is 7, every 7th element is used.
 *  @param[in]  B  Vector A
 *  @param[in]  inc_b     Stride within vector_B
 *  @return Dot product or -1.0 if error(s)
 */
template<typename T>
T LinearAlgebra<T>::Dot(const int& N, const T* A, const int& inc_a,
                        const T* B, const int& inc_b) {
  return -1.0;
}

template<>
float LinearAlgebra<float>::Dot(const int& N, const float* A, const int& inc_a,
                                const float* B, const int& inc_b) {
  return cblas_sdot(N, A, inc_a, B, inc_b);
}

template<>
double LinearAlgebra<double>::Dot(const int& N, const double* A, const int& inc_a,
                                const double* B, const int& inc_b) {
  return cblas_ddot(N, A, inc_a, B, inc_b);
}

/*
 *  @name   Dot
 *  @fn T Dot(const cv::Mat& vector_a,const int& inc_a,
        const cv::Mat& vector_b, const int& inc_b)
 *  @brief  Compute the dot product between two vectors
 *  @param[in]  vector_a  Vector A
 *  @param[in]  inc_a     Stride within vector_A. For example, if inc_a
                          is 7, every 7th element is used.
 *  @param[in]  vector_b  Vector B
 *  @param[in]  inc_b     Stride within vector_B
 *  @return Dot product or -1.0 if error(s)
 */
template<typename T>
T LinearAlgebra<T>::Dot(const cv::Mat& vector_a,const int& inc_a,
                        const cv::Mat& vector_b, const int& inc_b) {
  // stub
  return -1.0;
}

template<>
float LinearAlgebra<float>::Dot(const cv::Mat& vector_a,const int& inc_a,
                                const cv::Mat& vector_b, const int& inc_b) {
  // Float specialization
  //Only float/double
  assert(vector_a.channels() == 1 &&
         vector_b.channels() == 1 &&
         (vector_a.type() == CV_32FC1) &&
         (vector_b.type() == CV_32FC1));
  //Call cblas function
  int Na = static_cast<int>(std::ceil(std::max(vector_a.rows,
                                               vector_a.cols) /
                                      static_cast<float>(inc_a)));
  int Nb = static_cast<int>(std::ceil(std::max(vector_b.rows,
                                               vector_b.cols) /
                                      static_cast<float>(inc_b)));
  if (Na == Nb) {
    return cblas_sdot(Na,
                      reinterpret_cast<const float*>(vector_a.data),
                      inc_a,
                      reinterpret_cast<const float*>(vector_b.data),
                      inc_b);
  } else {
    return -1.f;
  }
}

template<>
double LinearAlgebra<double>::Dot(const cv::Mat& vector_a,const int& inc_a,
                                  const cv::Mat& vector_b, const int& inc_b) {
  // Float specialization
  //Only float/double
  assert(vector_a.channels() == 1 &&
         vector_b.channels() == 1 &&
         (vector_a.type() == CV_64FC1) &&
         (vector_b.type() == CV_64FC1));
  //Call cblas function
  int Na = static_cast<int>(std::ceil(std::max(vector_a.rows,
                                               vector_a.cols) /
                                      static_cast<double>(inc_a)));
  int Nb = static_cast<int>(std::ceil(std::max(vector_b.rows,
                                               vector_b.cols) /
                                      static_cast<double>(inc_b)));
  if (Na == Nb) {
    return cblas_ddot(Na,
                      reinterpret_cast<const double*>(vector_a.data),
                      inc_a,
                      reinterpret_cast<const double*>(vector_b.data),
                      inc_b);
  } else {
    return -1.0;
  }
}

#pragma mark -
#pragma mark Gemv

/*
 *  @name   Gemv
 *  @fn void Gemv(const cv::Mat& matrix_a,
                  const TransposeType& trans_a,
                  const T& alpha,
                  const cv::Mat& vector_x,
                  const T& beta,
                  cv::Mat* vector_y)
 *  @brief  Multiplies a matrix by a vector
 *          Y = a * AX + b Y
 *  @param[in]      matrix_a  Matrix A
 *  @param[in]      trans_a   Indicate if A is transpose or not
 *  @param[in]      alpha     Scaling factor alpha
 *  @param[in]      vector_x  Vector X
 *  @param[in]      beta      Scaling factor beta
 *  @param[in,out]  vector_y  Output vector
 */
template<typename T>
void LinearAlgebra<T>::Gemv(const cv::Mat& matrix_a,
                            const TransposeType& trans_a,
                            const T& alpha,
                            const cv::Mat& vector_x,
                            const T& beta,
                            cv::Mat* vector_y) {
  // stub
}

template<>
void LinearAlgebra<float>::Gemv(const cv::Mat& matrix_a,
                                const TransposeType& trans_a,
                                const float& alpha,
                                const cv::Mat& vector_x,
                                const float& beta,
                                cv::Mat* vector_y) {
  // Float specialization
  assert(matrix_a.channels() == 1 &&
         vector_x.channels() == 1 &&
         (matrix_a.type() == CV_32FC1));
  //Take care for output
  if(trans_a == TransposeType::kNoTranspose) {
    assert(matrix_a.cols == std::max(vector_x.cols, vector_x.rows));
    vector_y->create(matrix_a.rows, 1, matrix_a.type());
  } else {
    assert(matrix_a.rows == std::max(vector_x.cols, vector_x.rows));
    vector_y->create(matrix_a.cols, 1, matrix_a.type());
  }
  cblas_sgemv(CBLAS_ORDER::CblasRowMajor,
              static_cast<CBLAS_TRANSPOSE>(trans_a),
              matrix_a.rows,
              matrix_a.cols,
              alpha,
              reinterpret_cast<const float*>(matrix_a.data),
              matrix_a.cols,
              reinterpret_cast<const float*>(vector_x.data),
              1,
              beta,
              reinterpret_cast<float*>(vector_y->data),
              1);
}

template<>
void LinearAlgebra<double>::Gemv(const cv::Mat& matrix_a,
                                const TransposeType& trans_a,
                                const double& alpha,
                                const cv::Mat& vector_x,
                                const double& beta,
                                cv::Mat* vector_y) {
  // Float specialization
  assert(matrix_a.channels() == 1 &&
         vector_x.channels() == 1 &&
         (matrix_a.type() == CV_64FC1));
  //Take care for output
  if(trans_a == TransposeType::kNoTranspose) {
    vector_y->create(matrix_a.rows, 1, matrix_a.type());
  } else {
    vector_y->create(matrix_a.cols, 1, matrix_a.type());
  }
  cblas_dgemv(CBLAS_ORDER::CblasRowMajor,
              static_cast<CBLAS_TRANSPOSE>(trans_a),
              matrix_a.rows,
              matrix_a.cols,
              alpha,
              reinterpret_cast<const double*>(matrix_a.data),
              matrix_a.cols,
              reinterpret_cast<const double*>(vector_x.data),
              1,
              beta,
              reinterpret_cast<double*>(vector_y->data),
              1);
}

/*
 *  @name   Gemv
 *  @fn void  Gemv(const Matrix& matrix_a,
                   const TransposeType& trans_a,
                   const T& alpha,
                   const Matrix& vector_x,
                   const T& beta,
                   Matrix* vector_y)
 *  @brief  Multiplies a matrix by a vector
 *          Y = a * AX + b Y
 *  @param[in]      matrix_a  Matrix A
 *  @param[in]      trans_a   Indicate if A is transpose or not
 *  @param[in]      alpha     Scaling factor alpha
 *  @param[in]      vector_x  Vector X
 *  @param[in]      beta      Scaling factor beta
 *  @param[in,out]  vector_y  Output vector
 */
template<typename T>
void LinearAlgebra<T>::Gemv(const Matrix& matrix_a,
                            const TransposeType& trans_a,
                            const T& alpha,
                            const Matrix& vector_x,
                            const T& beta,
                            Matrix* vector_y) {
  // stub
}

template<>
void LinearAlgebra<float>::Gemv(const Matrix& matrix_a,
                                const TransposeType& trans_a,
                                const float& alpha,
                                const Matrix& vector_x,
                                const float& beta,
                                Matrix* vector_y) {
  //Take care for output
  if(trans_a == TransposeType::kTranspose) {
    vector_y->resize(matrix_a.rows(), 1);
  } else {
    vector_y->resize(matrix_a.cols(), 1);
  }
  //Float
  cblas_sgemv(CBLAS_ORDER::CblasColMajor,
              static_cast<CBLAS_TRANSPOSE>(trans_a),
              static_cast<int>(matrix_a.rows()),
              static_cast<int>(matrix_a.cols()),
              alpha,
              reinterpret_cast<const lapack_flt*>(matrix_a.data()),
              static_cast<int>(matrix_a.rows()),
              reinterpret_cast<const lapack_flt*>(vector_x.data()),
              1,
              beta,
              reinterpret_cast<lapack_flt*>(vector_y->data()),
              1);
}

template<>
void LinearAlgebra<double>::Gemv(const Matrix& matrix_a,
                                 const TransposeType& trans_a,
                                 const double& alpha,
                                 const Matrix& vector_x,
                                 const double& beta,
                                 Matrix* vector_y) {
  //Take care for output
  if(trans_a == TransposeType::kTranspose) {
    vector_y->resize(matrix_a.rows(), 1);
  } else {
    vector_y->resize(matrix_a.cols(), 1);
  }
  //Float
  cblas_dgemv(CBLAS_ORDER::CblasColMajor,
              static_cast<CBLAS_TRANSPOSE>(trans_a),
              static_cast<int>(matrix_a.rows()),
              static_cast<int>(matrix_a.cols()),
              alpha,
              reinterpret_cast<const lapack_dbl*>(matrix_a.data()),
              static_cast<int>(matrix_a.rows()),
              reinterpret_cast<const lapack_dbl*>(vector_x.data()),
              1,
              beta,
              reinterpret_cast<lapack_dbl*>(vector_y->data()),
              1);
}

#pragma mark -
#pragma mark Sbmv

/*
 * @name  Sbmv
 * @fn    static void Sbmv(const cv::Mat& A, const T alpha, const cv::Mat& x,
                 const T beta, cv::Mat* y)
 * @brief Perform matrix-vector operation :  y := alpha*A*x + beta*y
 *        where alpha and beta are scalars, x and y are n element vectors and
 *        A is an n by n symmetric band matrix, with k super-diagonals.
 *        At the moment support only diagonal matrix
 *        i.e. element-wise vector multiplication
 * @param[in] A       Vector of element on the matrix's diagonal
 * @param[in] alpha   Scaling factor
 * @param[in] x       Vector
 * @param[in] beta    Scaling factor
 * @param[in, out] y  Output
 */
template<typename T>
void LinearAlgebra<T>::Sbmv(const cv::Mat& A,
                            const T alpha,
                            const cv::Mat& x,
                            const T beta, cv::Mat* y) {
  // STub
}

template<>
void LinearAlgebra<float>::Sbmv(const cv::Mat& A,
                                const float alpha,
                                const cv::Mat& x,
                                const float beta,
                                cv::Mat* y) {
  // Sanity check
  assert((A.cols == 1 && A.rows > 1) || (A.cols > 1 && A.rows == 1));
  assert((x.cols == 1 && x.rows > 1) || (x.cols > 1 && x.rows == 1));
  assert(A.type() == CV_32FC1 && x.type() == CV_32FC1);
  // Define output
  y->create(x.rows, x.cols, x.type());
  // Compute
  const int& k = 0;  // only diagonal
  const int& N = std::max(A.cols, A.rows);
  const int& lda = 1;
  const int& inc = 1;
  cblas_ssbmv(CBLAS_ORDER::CblasRowMajor,
              CBLAS_UPLO::CblasUpper,
              N,
              k,
              alpha,
              reinterpret_cast<const float*>(A.data),
              lda,
              reinterpret_cast<const float*>(x.data),
              inc,
              beta,
              reinterpret_cast<float*>(y->data),
              inc);
}

template<>
void LinearAlgebra<double>::Sbmv(const cv::Mat& A,
                                 const double alpha,
                                 const cv::Mat& x,
                                 const double beta,
                                 cv::Mat* y) {
  // Sanity check
  assert((A.cols == 1 && A.rows > 1) || (A.cols > 1 && A.rows == 1));
  assert((x.cols == 1 && x.rows > 1) || (x.cols > 1 && x.rows == 1));
  assert(A.type() == CV_64FC1 && x.type() == CV_64FC1);
  // Define output
  y->create(x.rows, x.cols, x.type());
  // Compute
  const int& k = 0;  // only diagonal
  const int& N = std::max(A.cols, A.rows);
  const int& lda = 1;
  const int& inc = 1;
  cblas_dsbmv(CBLAS_ORDER::CblasRowMajor,
              CBLAS_UPLO::CblasUpper,
              N,
              k,
              alpha,
              reinterpret_cast<const double*>(A.data),
              lda,
              reinterpret_cast<const double*>(x.data),
              inc,
              beta,
              reinterpret_cast<double*>(y->data),
              inc);
}

#pragma mark -
#pragma mark Gemm

/*
 *  @name   Gemm
 *  @fn void Gemm(const cv::Mat& matrix_a,
                  const TransposeType& trans_a,
                  const T& alpha,
                  const cv::Mat& matrix_b,
                  const TransposeType& trans_b,
                  const T& beta,
                  cv::Mat* matrix_c)
 *  @brief  Compute the product between two matrices
 *          C = a * AB + b * C
 *  @param[in]      matrix_a  Matrix A
 *  @param[in]      trans_a   Transpose flag indicator for A
 *  @param[in]      alpha     Alpha coefficient
 *  @param[in]      matrix_b  Matrix B
 *  @param[in]      trans_b   Transpose flag indicator for B
 *  @param[in]      beta      Beta coefficient
 *  @param[in,out]  matrix_c  Resulting matrix
 */
template<typename T>
void LinearAlgebra<T>::Gemm(const cv::Mat& matrix_a,
                            const TransposeType& trans_a,
                            const T& alpha,
                            const cv::Mat& matrix_b,
                            const TransposeType& trans_b,
                            const T& beta,
                            cv::Mat* matrix_c) {
  // Stub
}

template<>
void LinearAlgebra<float>::Gemm(const cv::Mat& matrix_a,
                                const TransposeType& trans_a,
                                const float& alpha,
                                const cv::Mat& matrix_b,
                                const TransposeType& trans_b,
                                const float& beta,
                                cv::Mat* matrix_c) {
  assert(matrix_a.channels() == 1 && matrix_b.channels() == 1 &&
         (matrix_a.type() == CV_32FC1) &&
         (matrix_b.type() == CV_32FC1));
  //Define output matrix size
  const int& out_row = trans_a == TransposeType::kNoTranspose ?
                      matrix_a.rows :
                      matrix_a.cols;
  const int& out_col = trans_b == TransposeType::kNoTranspose ?
                      matrix_b.cols :
                      matrix_b.rows;
  const int& K = trans_a == TransposeType::kNoTranspose ?
                matrix_a.cols :
                matrix_a.rows;
  matrix_c->create(out_row,out_col,matrix_a.type());
  // multiplication
  cblas_sgemm(CBLAS_ORDER::CblasRowMajor,
              static_cast<CBLAS_TRANSPOSE>(trans_a),
              static_cast<CBLAS_TRANSPOSE>(trans_b),
              out_row,
              out_col,
              K,
              alpha,
              reinterpret_cast<const float*>(matrix_a.data),
              matrix_a.cols,
              reinterpret_cast<const float*>(matrix_b.data),
              matrix_b.cols,
              beta,
              reinterpret_cast<float*>(matrix_c->data),
              matrix_c->cols);
}

template<>
void LinearAlgebra<double>::Gemm(const cv::Mat& matrix_a,
                                const TransposeType& trans_a,
                                const double& alpha,
                                const cv::Mat& matrix_b,
                                const TransposeType& trans_b,
                                const double& beta,
                                cv::Mat* matrix_c) {
  assert(matrix_a.channels() == 1 && matrix_b.channels() == 1 &&
         (matrix_a.type() == CV_64FC1) &&
         (matrix_b.type() == CV_64FC1));
  //Define output matrix size
  const int& out_row = trans_a == TransposeType::kNoTranspose ?
                      matrix_a.rows :
                      matrix_a.cols;
  const int& out_col = trans_b == TransposeType::kNoTranspose ?
                      matrix_b.cols :
                      matrix_b.rows;
  const int& K = trans_a == TransposeType::kNoTranspose ?
                matrix_a.cols :
                matrix_a.rows;
  matrix_c->create(out_row,out_col,matrix_a.type());
  // multiplication
  cblas_dgemm(CBLAS_ORDER::CblasRowMajor,
              static_cast<CBLAS_TRANSPOSE>(trans_a),
              static_cast<CBLAS_TRANSPOSE>(trans_b),
              out_row,
              out_col,
              K,
              alpha,
              reinterpret_cast<const double*>(matrix_a.data),
              matrix_a.cols,
              reinterpret_cast<const double*>(matrix_b.data),
              matrix_b.cols,
              beta,
              reinterpret_cast<double*>(matrix_c->data),
              matrix_c->cols);
}

template<typename T>
void LinearAlgebra<T>::Gemm(const TransposeType& trans_a,
                            const int& m,
                            const int& n,
                            const int& k,
                            const T& alpha,
                            const T* a,
                            const int& lda,
                            const TransposeType& trans_b,
                            const T& beta,
                            const T* b,
                            const int& ldb,
                            const int& ldc,
                            T* matrix_c) {
  // Stub
}

template<>
void LinearAlgebra<float>::Gemm(const TransposeType& trans_a,
                                const int& m,
                                const int& n,
                                const int& k,
                                const float& alpha,
                                const float* a,
                                const int& lda,
                                const TransposeType& trans_b,
                                const float& beta,
                                const float* b,
                                const int& ldb,
                                const int& ldc,
                                float* c) {
  // multiplication
  cblas_sgemm(CBLAS_ORDER::CblasRowMajor,
              static_cast<CBLAS_TRANSPOSE>(trans_a),
              static_cast<CBLAS_TRANSPOSE>(trans_b),
              m,
              n,
              k,
              alpha,
              a,
              lda,
              b,
              ldb,
              beta,
              c,
              ldc);
}

template<>
void LinearAlgebra<double>::Gemm(const TransposeType& trans_a,
                                const int& m,
                                const int& n,
                                const int& k,
                                const double& alpha,
                                const double* a,
                                const int& lda,
                                const TransposeType& trans_b,
                                const double& beta,
                                const double* b,
                                const int& ldb,
                                const int& ldc,
                                double* c) {
  // multiplication
  cblas_dgemm(CBLAS_ORDER::CblasRowMajor,
              static_cast<CBLAS_TRANSPOSE>(trans_a),
              static_cast<CBLAS_TRANSPOSE>(trans_b),
              m,
              n,
              k,
              alpha,
              a,
              lda,
              b,
              ldb,
              beta,
              c,
              ldc);
}

/*
 *  @name   Gemm
 *  @fn void Gemm(const Matrix& matrix_a,
                 const TransposeType& trans_a,
                 const T& alpha,
                 const Matrix& matrix_b,
                 const TransposeType& trans_b,
                 const T& beta,
                 Matrix* matrix_c)
 *  @brief  Compute the product between two matrices
 *          C = a * AB + b * C
 *  @param[in]      matrix_a  Matrix A
 *  @param[in]      trans_a   Transpose flag indicator for A
 *  @param[in]      alpha     Alpha coefficient
 *  @param[in]      matrix_b  Matrix B
 *  @param[in]      trans_b   Transpose flag indicator for B
 *  @param[in]      beta      Beta coefficient
 *  @param[in,out]  matrix_c  Resulting matrix
 */
template<typename T>
void LinearAlgebra<T>::Gemm(const Matrix& matrix_a,
                            const TransposeType& trans_a,
                            const T& alpha,
                            const Matrix& matrix_b,
                            const TransposeType& trans_b,
                            const T& beta,
                            Matrix* matrix_c) {
  // Stub
}

template<>
void LinearAlgebra<float>::Gemm(const Matrix& matrix_a,
                                const TransposeType& trans_a,
                                const float& alpha,
                                const Matrix& matrix_b,
                                const TransposeType& trans_b,
                                const float& beta,
                                Matrix* matrix_c) {
  //Define output matrix size
  const int& out_row = (trans_a == kNoTranspose ?
                       static_cast<int>(matrix_a.rows()) :
                       static_cast<int>(matrix_a.cols()));
  const int& out_col = (trans_b == kNoTranspose ?
                       static_cast<int>(matrix_b.cols()) :
                       static_cast<int>(matrix_b.rows()));
  const int& K = (trans_a == kNoTranspose ?
                 static_cast<int>(matrix_a.cols()) :
                 static_cast<int>(matrix_a.rows()));
  matrix_c->resize(out_row, out_col);
  cblas_sgemm(CBLAS_ORDER::CblasColMajor,
              static_cast<CBLAS_TRANSPOSE>(trans_a),
              static_cast<CBLAS_TRANSPOSE>(trans_b),
              out_row,
              out_col,
              K,
              alpha,
              reinterpret_cast<const lapack_flt*>(matrix_a.data()),
              static_cast<int>(matrix_a.rows()),
              reinterpret_cast<const lapack_flt*>(matrix_b.data()),
              static_cast<int>(matrix_b.rows()),
              beta,
              reinterpret_cast<lapack_flt*>(matrix_c->data()),
              static_cast<int>(matrix_c->rows()));
}

template<>
void LinearAlgebra<double>::Gemm(const Matrix& matrix_a,
                                const TransposeType& trans_a,
                                const double& alpha,
                                const Matrix& matrix_b,
                                const TransposeType& trans_b,
                                const double& beta,
                                Matrix* matrix_c) {
  //Define output matrix size
  const int& out_row = (trans_a == kNoTranspose ?
                       static_cast<int>(matrix_a.rows()) :
                       static_cast<int>(matrix_a.cols()));
  const int& out_col = (trans_b == kNoTranspose ?
                       static_cast<int>(matrix_b.cols()) :
                       static_cast<int>(matrix_b.rows()));
  const int& K = (trans_a == kNoTranspose ?
                 static_cast<int>(matrix_a.cols()) :
                 static_cast<int>(matrix_a.rows()));
  matrix_c->resize(out_row, out_col);
  cblas_dgemm(CBLAS_ORDER::CblasColMajor,
              static_cast<CBLAS_TRANSPOSE>(trans_a),
              static_cast<CBLAS_TRANSPOSE>(trans_b),
              out_row,
              out_col,
              K,
              alpha,
              reinterpret_cast<const lapack_dbl*>(matrix_a.data()),
              static_cast<int>(matrix_a.rows()),
              reinterpret_cast<const lapack_dbl*>(matrix_b.data()),
              static_cast<int>(matrix_b.rows()),
              beta,
              reinterpret_cast<lapack_dbl*>(matrix_c->data()),
              static_cast<int>(matrix_c->rows()));
}

#pragma mark -
#pragma mark Ger

/*
 *  @name   Ger
 *  @fn void Ger(const cv::Mat& vec_x,
                  const cv::Mat& vec_y,
                  const T alpha,
                  cv::Mat* matrix_a)
 *  @brief  Compute the multiplication between two vector and add it to a
 *          matrix :
 *          A = a * x * y' + A
 *  @param[in]      vec_x
 *  @param[in]      vec_y
 *  @param[in]      alpha
 *  @param[in,out]  matrix_a
 */
template<typename T>
void LinearAlgebra<T>::Ger(const cv::Mat& vec_x, const cv::Mat& vec_y,
                           const T alpha,cv::Mat* matrix_a) {
  // Stub
}

template<>
void LinearAlgebra<float>::Ger(const cv::Mat& vec_x,
                               const cv::Mat& vec_y,
                               const float alpha,
                               cv::Mat* matrix_a) {
  assert((vec_x.type() == CV_32FC1) && (vec_y.type() == CV_32FC1) &&
         vec_x.channels() == 1 && vec_y.channels() == 1 &&
         (vec_x.cols == 1 || vec_x.rows == 1) &&
         (vec_y.cols == 1 || vec_y.rows == 1));
  int row = std::max(vec_x.cols,vec_x.rows);
  int col = std::max(vec_y.cols,vec_y.rows);
  matrix_a->create(row,col,vec_x.type());
  cblas_sger(CBLAS_ORDER::CblasRowMajor,
             matrix_a->rows,
             matrix_a->cols,
             alpha,
             reinterpret_cast<const float*>(vec_x.data),
             1,
             reinterpret_cast<const float*>(vec_y.data),
             1,
             reinterpret_cast<float*>(matrix_a->data),
             matrix_a->cols);
}

template<>
void LinearAlgebra<double>::Ger(const cv::Mat& vec_x,
                                const cv::Mat& vec_y,
                                const double alpha,
                                cv::Mat* matrix_a) {
  assert((vec_x.type() == CV_64FC1) && (vec_y.type() == CV_64FC1) &&
         vec_x.channels() == 1 && vec_y.channels() == 1 &&
         (vec_x.cols == 1 || vec_x.rows == 1) &&
         (vec_y.cols == 1 || vec_y.rows == 1));
  int row = std::max(vec_x.cols,vec_x.rows);
  int col = std::max(vec_y.cols,vec_y.rows);
  matrix_a->create(row,col,vec_x.type());
  cblas_dger(CBLAS_ORDER::CblasRowMajor,
             matrix_a->rows,
             matrix_a->cols,
             alpha,
             reinterpret_cast<const double*>(vec_x.data),
             1,
             reinterpret_cast<const double*>(vec_y.data),
             1,
             reinterpret_cast<double*>(matrix_a->data),
             matrix_a->cols);
}

#pragma mark -
#pragma mark Matrix inversion

/*
 *  @name InverseSymMatrix
 *  @fn int InverseMatrix(const Matrix& matrix_a, Matrix* inv_matrix_a)
 *  @brief  Compute the inverse of a real \p matrix_a using LU Decomposition
 *          method
 *  @param[in]  matrix_a      Real matrix
 *  @param[out] inv_matrix_a  Inverse of \p matrix_a
 *  @return 0 on sucess, error code otherwise
 */
template<typename T>
int LinearAlgebra<T>::InverseMatrix(const Matrix& matrix_a,
                                    Matrix* inv_matrix_a) {
  // Stub
  return -1;
}

template<>
int LinearAlgebra<float>::InverseMatrix(const Matrix& matrix_a,
                                        Matrix* inv_matrix_a) {
  auto M = static_cast<lapack_int>(matrix_a.rows());
  auto N = static_cast<lapack_int>(matrix_a.cols());
  assert(M == N);
  // Init output
  inv_matrix_a->resize(matrix_a.rows(), matrix_a.cols());
  // copy matrix_a into output
  *inv_matrix_a = matrix_a;
  // Compute the LU decomposition
  auto* ipiv = new lapack_int[N];
  lapack_int info = -1, lwork = -1;
  auto* work = new float[1];
  sgetrf_(&M,
          &N,
          reinterpret_cast<lapack_flt*>(inv_matrix_a->data()),
          &M,
          ipiv,
          &info);
  if (info == 0) {
    // Query workspace size
    sgetri_(&N,
            reinterpret_cast<lapack_flt*>(inv_matrix_a->data()),
            &N,
            ipiv,
            reinterpret_cast<lapack_flt*>(work),
            &lwork,
            &info);
    if (info == 0) {
      lwork = static_cast<lapack_int>(work[0]);
      delete[] work;
      work = new float[lwork];
      sgetri_(&N,
              reinterpret_cast<lapack_flt*>(inv_matrix_a->data()),
              &N,
              ipiv,
              reinterpret_cast<lapack_flt*>(work),
              &lwork,
              &info);
      delete[] work;
    } else {
      std::cout << "WARNING : Unable to query workspace size" << std::endl;
    }
  }
  return (int)info;
}

template<>
int LinearAlgebra<double>::InverseMatrix(const Matrix& matrix_a,
                                         Matrix* inv_matrix_a) {
  auto M = static_cast<lapack_int>(matrix_a.rows());
  auto N = static_cast<lapack_int>(matrix_a.cols());
  assert(M == N);
  // Init output
  inv_matrix_a->resize(matrix_a.rows(), matrix_a.cols());
  // copy matrix_a into output
  *inv_matrix_a = matrix_a;
  // Compute the LU decomposition
  auto* ipiv = new lapack_int[N];
  lapack_int info = -1, lwork = -1;
  auto* work = new double[1];
  dgetrf_(&M,
          &N,
          reinterpret_cast<lapack_dbl*>(inv_matrix_a->data()),
          &M,
          ipiv,
          &info);
  if (info == 0) {
    // Query workspace size
    dgetri_(&N,
            reinterpret_cast<lapack_dbl*>(inv_matrix_a->data()),
            &N,
            ipiv,
            reinterpret_cast<lapack_dbl*>(work),
            &lwork,
            &info);
    if (info == 0) {
      lwork = static_cast<lapack_int>(work[0]);
      delete[] work;
      work = new double[lwork];
      dgetri_(&N,
              reinterpret_cast<lapack_dbl*>(inv_matrix_a->data()),
              &N,
              ipiv,
              reinterpret_cast<lapack_dbl*>(work),
              &lwork,
              &info);
      delete[] work;
    } else {
      std::cout << "WARNING : Unable to query workspace size" << std::endl;
    }
  }
  return (int)info;
}

/**
 *  @name InverseSymMatrix
 *  @fn int InverseSymMatrix(const Matrix& matrix_a, Matrix* inv_matrix_a)
 *  @brief  Compute the inverse of a real symmetric \p matrix_a using the
 *          Bunch-Kaufman diagonal pivoting method
 *  @param[in]  matrix_a      Real symmetric matrix
 *                            in the process
 *  @param[out] inv_matrix_a  Inverse of \p matrix_a
 *  @return 0 on sucess, error code otherwise
 */
template<typename T>
int LinearAlgebra<T>::InverseSymMatrix(const Matrix& matrix_a,
                                       Matrix* inv_matrix_a) {
  //Stub
  return -1;
}

template<>
int LinearAlgebra<float>::InverseSymMatrix(const Matrix& matrix_a,
                                           Matrix* inv_matrix_a) {
  // Init output
  inv_matrix_a->resize(matrix_a.rows(), matrix_a.cols());
  // Compute the decomposition
  char uplo = 'U';
  auto N = static_cast<lapack_int>(matrix_a.rows());
  auto* ipiv = new lapack_int[N];
  auto* work = new float[1];
  lapack_int lwork = -1, info = -1;
  // Query workspace size
  LTS5_ssytrf(&uplo, &N,
              reinterpret_cast<lapack_flt*>(const_cast<lapack_flt*>(matrix_a.data())),
              &N, ipiv,
              reinterpret_cast<lapack_flt*>(work),
              &lwork, &info);
  if (info == 0) {
    // Query successfull, copy matrix_a into output
    *inv_matrix_a = matrix_a;
    lwork = static_cast<lapack_int>(work[0]);
    delete[] work;
    work = new float[lwork];
    // Compute decomposition
    LTS5_ssytrf(&uplo, &N,
                reinterpret_cast<lapack_flt*>(inv_matrix_a->data()),
                &N, ipiv,
                reinterpret_cast<lapack_flt*>(work),
                &lwork, &info);
    if (info == 0) {
      // Matrix inversion
      LTS5_ssytri(&uplo, &N,
                  reinterpret_cast<lapack_flt*>(inv_matrix_a->data()),
                  &N, ipiv,
                  reinterpret_cast<lapack_flt*>(work),
                  &info);
      // Copy Upper part to lower part if success
      if (info == 0) {
        for (int r = 0; r < N; ++r) {
          for (int c = r + 1 ; c < N ; ++c) {
            (*inv_matrix_a)(c, r) = (*inv_matrix_a)(r, c);
          }
        }
      }
    }
  } else {
    LTS5_LOG_ERROR("Unable to query workspace size");
  }
  // clean up
  delete[] work;
  delete[] ipiv;
  return info;
}

template<>
int LinearAlgebra<double>::InverseSymMatrix(const Matrix& matrix_a,
                                            Matrix* inv_matrix_a) {
  // Init output
  inv_matrix_a->resize(matrix_a.rows(), matrix_a.cols());
  // Compute the decomposition
  char uplo = 'U';
  auto N = static_cast<lapack_int>(matrix_a.rows());
  auto* ipiv = new lapack_int[N];
  auto* work = new double[1];
  lapack_int lwork = -1, info = -1;
  // Query workspace size
  LTS5_dsytrf(&uplo,
              &N,
              reinterpret_cast<lapack_dbl*>(const_cast<lapack_dbl *>(matrix_a.data())),
              &N,
              ipiv,
              reinterpret_cast<lapack_dbl*>(work),
              &lwork, &info);
  if (info == 0) {
    // Query successfull, copy matrix_a into output
    *inv_matrix_a = matrix_a;
    lwork = static_cast<lapack_int>(work[0]);
    delete[] work;
    work = new double[lwork];
    // Compute decomposition
    LTS5_dsytrf(&uplo,
                &N,
                reinterpret_cast<lapack_dbl*>(inv_matrix_a->data()),
                &N,
                ipiv,
                reinterpret_cast<lapack_dbl*>(work),
                &lwork,
                &info);
    if (info == 0) {
      // Matrix inversion
      LTS5_dsytri(&uplo,
                  &N,
                  reinterpret_cast<lapack_dbl*>(inv_matrix_a->data()),
                  &N,
                  ipiv,
                  reinterpret_cast<lapack_dbl*>(work),
                  &info);
      // Copy Upper part to lower part if success
      if (info == 0) {
        for (int r = 0; r < N; ++r) {
          for (int c = r + 1 ; c < N ; ++c) {
            (*inv_matrix_a)(c, r) = (*inv_matrix_a)(r, c);
          }
        }
      }
    }
  } else {
    LTS5_LOG_ERROR("Unable to query workspace size");
  }
  // clean up
  delete[] work;
  delete[] ipiv;
  return info;
}

/*
 *  @name InverseSymPosMatrix
 *  @fn int InverseSymPosMatrix(const Matrix& matrix_a, Matrix* inv_matrix_a)
 *  @brief  Compute the inverse of a real symmetric positive definite
 *          \p matrix_a using Cholesky Factorization.
 *  @param[in]  matrix_a      Real symmetric positive definite matrix
 *  @param[out] inv_matrix_a  Inverse of \p matrix_a
 *  @return 0 on sucess, error code otherwise
 */
template<typename T>
int LinearAlgebra<T>::InverseSymPosMatrix(const Matrix& matrix_a,
                                          Matrix* inv_matrix_a) {
  // Stub
  return -1;
}

template<>
int LinearAlgebra<float>::InverseSymPosMatrix(const Matrix& matrix_a,
                                              Matrix* inv_matrix_a) {
  // Init output
  inv_matrix_a->resize(matrix_a.rows(), matrix_a.cols());
  //copy matrix_a into output
  *inv_matrix_a = matrix_a;
  // Compute Cholesky decomposition
  char uplo = 'U';
  auto N = static_cast<lapack_int>(matrix_a.rows());
  auto info = -1;
  LTS5_spotrf(&uplo, &N,
              reinterpret_cast<lapack_flt*>(inv_matrix_a->data()),
              &N,
              &info);
  if (info == 0) {
    // Matrix inversion
    LTS5_spotri(&uplo,
                &N,
                reinterpret_cast<lapack_flt*>(inv_matrix_a->data()),
                &N,
                &info);
    // Copy Upper part to lower part if success
    if (info == 0) {
      for (int r = 0; r < N; ++r) {
        for (int c = r + 1 ; c < N ; ++c) {
          (*inv_matrix_a)(c, r) = (*inv_matrix_a)(r, c);
        }
      }
    }
  } else if (info > 0) {
    std::cout << "WARNING : Matrix A is not positive definite !" << std::endl;
  }
  return info;
}

template<>
int LinearAlgebra<double>::InverseSymPosMatrix(const Matrix& matrix_a,
                                               Matrix* inv_matrix_a) {
  // Init output
  inv_matrix_a->resize(matrix_a.rows(), matrix_a.cols());
  //copy matrix_a into output
  *inv_matrix_a = matrix_a;
  // Compute Cholesky decomposition
  char uplo = 'U';
  auto N = static_cast<lapack_int>(matrix_a.rows());
  lapack_int info = -1;
  LTS5_dpotrf(&uplo,
              &N,
              reinterpret_cast<lapack_dbl*>(inv_matrix_a->data()),
              &N,
              &info);
  if (info == 0) {
    // Matrix inversion
    LTS5_dpotri(&uplo,
            &N,
            reinterpret_cast<lapack_dbl*>(inv_matrix_a->data()),
            &N,
            &info);
    // Copy Upper part to lower part if success
    if (info == 0) {
      for (int r = 0; r < N; ++r) {
        for (int c = r + 1 ; c < N ; ++c) {
          (*inv_matrix_a)(c, r) = (*inv_matrix_a)(r, c);
        }
      }
    }
  } else if (info > 0) {
    LTS5_LOG_WARNING("Matrix A is not positive definite !");
  }
  return info;
}


#pragma mark -
#pragma mark Lapack wrapper (utils)

/*
 * @name    MachinePrecisionCall
 * @brief   Determines precision machine parameters.
 * @param   type    Type of precision
 * @return  value
 */
template<typename T>
T LinearAlgebra<T>::Lapack::MachinePrecisionCall(char* type) {
  // stub
  return T(-1.0);
}

template<>
float LinearAlgebra<float>::Lapack::MachinePrecisionCall(char* type) {
  return LTS5_slamch(type);
}

template<>
double LinearAlgebra<double>::Lapack::MachinePrecisionCall(char* type) {
  return LTS5_dlamch(type);
}

#pragma mark -
#pragma mark Lapack wrapper (symmetric eigen decomposition)

/*
 *  @name Print
 *  @fn void Print()
 *  @brief  Output the parameters values
 */
template<typename T>
void LinearAlgebra<T>::Lapack::sym_eigen_params::Print() {
  std::cout << "Parameters : " << std::endl;
  std::cout << "jobz : " << kJobz << std::endl;
  std::cout << "range : " << kRange << std::endl;
  std::cout << "uplo : " << kUplo << std::endl;
  std::cout << "N : " << kN << std::endl;
  std::cout << "A* : " << kA << std::endl;
  std::cout << "LDA : " << kLda << std::endl;
  std::cout << "VL : " << kVl << std::endl;
  std::cout << "VU : " << kVu << std::endl;
  std::cout << "IL : " << kIl << std::endl;
  std::cout << "IU : " << kIu << std::endl;
  std::cout << "abstol : " << kAbstol << std::endl;
  std::cout << "M : " << kM << std::endl;
  std::cout << "eig_val* : " << kEigValues << std::endl;
  std::cout << "eig_vect* : " << kEigVectors << std::endl;
  std::cout << "LDZ : " << kLdz << std::endl;
  std::cout << "Work* : " << kWork << std::endl;
  std::cout << "lwork : " << kLwork << std::endl;
  std::cout << "iwork* : " << kIwork << std::endl;
  std::cout << "ifail* : " << kIfail << std::endl;
  std::cout << "info : " << kInfo << std::endl;
}

/*
 *  @name   SymEigenDecompositionCall
 *  @fn inline void SymEigenDecompositionCall(sym_eigen_params& p)
 *  @brief  Interface for lapack function call - Undefined type
 *  @throw  std::runtime_error()
 */
template<typename T>
void LinearAlgebra<T>::Lapack::SymEigenDecompositionCall(sym_eigen_params& p) {
  throw std::runtime_error("Error Unsupported Type");
}

template<>
void LinearAlgebra<float>::Lapack::SymEigenDecompositionCall(sym_eigen_params& p) {
  LTS5_ssyevx(&p.kJobz,
              &p.kRange,
              &p.kUplo,
              &p.kN,
              reinterpret_cast<lapack_flt*>(p.kA),
              &p.kLda,
              reinterpret_cast<lapack_flt*>(&p.kVl),
              reinterpret_cast<lapack_flt*>(&p.kVu),
              &p.kIl,
              &p.kIu,
              reinterpret_cast<lapack_flt*>(&p.kAbstol),
              &p.kM,
              reinterpret_cast<lapack_flt*>(p.kEigValues),
              reinterpret_cast<lapack_flt*>(p.kEigVectors),
              &p.kLdz,
              reinterpret_cast<lapack_flt*>(p.kWork),
              &p.kLwork,
              p.kIwork,
              p.kIfail,
              &p.kInfo);
}

template<>
void LinearAlgebra<double>::Lapack::SymEigenDecompositionCall(sym_eigen_params& p) {
  LTS5_dsyevx(&p.kJobz,
              &p.kRange,
              &p.kUplo,
              &p.kN,
              reinterpret_cast<lapack_dbl*>(p.kA),
              &p.kLda,
              reinterpret_cast<lapack_dbl*>(&p.kVl),
              reinterpret_cast<lapack_dbl*>(&p.kVu),
              &p.kIl,
              &p.kIu,
              reinterpret_cast<lapack_dbl*>(&p.kAbstol),
              &p.kM,
              reinterpret_cast<lapack_dbl*>(p.kEigValues),
              reinterpret_cast<lapack_dbl*>(p.kEigVectors),
              &p.kLdz,
              reinterpret_cast<lapack_dbl*>(p.kWork),
              &p.kLwork,
              p.kIwork,
              p.kIfail,
              &p.kInfo);
}

#pragma mark -
#pragma mark Lapack wrapper (non symmetric eigen decomposition)

/*
 *  @name   NSymEigenDecompositionCall
 *  @fn inline void NSymEigenDecompositionCall(n_sym_eigen_params& p)
 *  @brief  Interface for lapack function call - Undefined type
 *  @throw  std::runtime_error()
 */
template<typename T>
void LinearAlgebra<T>::Lapack::NSymEigenDecompositionCall(n_sym_eigen_params& p) {
  throw std::runtime_error("Error Unsupported Type");
}

template<>
void LinearAlgebra<float>::Lapack::NSymEigenDecompositionCall(n_sym_eigen_params& p) {
  LTS5_sgeev(&p.kJobvl,
             &p.kJobvr,
             &p.kN,
             reinterpret_cast<lapack_flt*>(p.kA),
             &p.kLda,
             reinterpret_cast<lapack_flt*>(p.kWr),
             reinterpret_cast<lapack_flt*>(p.kWi),
             reinterpret_cast<lapack_flt*>(p.kVl),
             &p.kLdvl,
             reinterpret_cast<lapack_flt*>(p.kVr),
             &p.kLdvr,
             reinterpret_cast<lapack_flt*>(p.kWork),
             &p.kLwork,
             &p.kInfo);
}

template<>
void LinearAlgebra<double>::Lapack::NSymEigenDecompositionCall(n_sym_eigen_params& p) {
  LTS5_dgeev(&p.kJobvl,
             &p.kJobvr,
             &p.kN,
             reinterpret_cast<lapack_dbl*>(p.kA),
             &p.kLda,
             reinterpret_cast<lapack_dbl*>(p.kWr),
             reinterpret_cast<lapack_dbl*>(p.kWi),
             reinterpret_cast<lapack_dbl*>(p.kVl),
             &p.kLdvl,
             reinterpret_cast<lapack_dbl*>(p.kVr),
             &p.kLdvr,
             reinterpret_cast<lapack_dbl*>(p.kWork),
             &p.kLwork,
             &p.kInfo);
}

#pragma mark -
#pragma mark Lapack wrapper (svd decomposition)

/*
 *  @name Print
 *  @fn void Print()
 *  @brief  Output the parameters values
 */
template<typename T>
void LinearAlgebra<T>::Lapack::svd_params::Print() {
  std::cout << "SVD Parameters :" << std::endl;
  std::cout << "JobU : " << kJobu << std::endl;
  std::cout << "JoVt : " << kJobvt << std::endl;
  std::cout << "M : " << kM << std::endl;
  std::cout << "N : " << kN << std::endl;
  std::cout << "Min(M,N) : " << kMin << std::endl;
  std::cout << "Lda : " << kLda << std::endl;
  std::cout << "A : " << kA << std::endl;
  std::cout << "S : " << kS << std::endl;
  std::cout << "Ldu : " << kLdu << std::endl;
  std::cout << "U : " << kU << std::endl;
  std::cout << "Ldvt : " << kLdvt << std::endl;
  std::cout << "Vt : " << kVt << std::endl;
  std::cout << "LWork : " << kLwork << std::endl;
  std::cout << "Work : " << kWork << std::endl;
  std::cout << "Info : " << kInfo << std::endl;
  std::cout << "Tpye : " << kType << std::endl;
}

/*
 *  @name   SvdDecompositionCall
 *  @fn inline void SvdDecompositionCall(svd_params& p)
 *  @brief  Interface for lapack function call - Undefined type
 *  @throw  std::runtime_error()
 */
template<typename T>
void LinearAlgebra<T>::Lapack::SvdDecompositionCall(svd_params& p) {
  throw std::runtime_error("Error Unsupported Type");
}

template<>
void LinearAlgebra<float>::Lapack::SvdDecompositionCall(svd_params& p) {
  LTS5_sgesvd(&p.kJobu,
              &p.kJobvt,
              &p.kM,
              &p.kN,
              reinterpret_cast<lapack_flt*>(p.kA),
              &p.kLda,
              reinterpret_cast<lapack_flt*>(p.kS),
              reinterpret_cast<lapack_flt*>(p.kU),
              &p.kLdu,
              reinterpret_cast<lapack_flt*>(p.kVt),
              &p.kLdvt,
              reinterpret_cast<lapack_flt*>(p.kWork),
              &p.kLwork,
              &p.kInfo);
}

template<>
void LinearAlgebra<double>::Lapack::SvdDecompositionCall(svd_params& p) {
  LTS5_dgesvd(&p.kJobu,
              &p.kJobvt,
              &p.kM,
              &p.kN,
              reinterpret_cast<lapack_dbl*>(p.kA),
              &p.kLda,
              reinterpret_cast<lapack_dbl*>(p.kS),
              reinterpret_cast<lapack_dbl*>(p.kU),
              &p.kLdu,
              reinterpret_cast<lapack_dbl*>(p.kVt),
              &p.kLdvt,
              reinterpret_cast<lapack_dbl*>(p.kWork),
              &p.kLwork,
              &p.kInfo);
}

/*
 *  @name Print
 *  @fn void Print()
 *  @brief  Output the parameters values
 */
template<typename T>
void LinearAlgebra<T>::Lapack::dc_svd_params::Print() {
  std::cout << "SVD Parameters :" << std::endl;
  std::cout << "JobZ : " << kJobz << std::endl;
  std::cout << "M : " << kM << std::endl;
  std::cout << "N : " << kN << std::endl;
  std::cout << "Min(M,N) : " << kMin << std::endl;
  std::cout << "Lda : " << kLda << std::endl;
  std::cout << "A : " << kA << std::endl;
  std::cout << "S : " << kS << std::endl;
  std::cout << "Ldu : " << kLdu << std::endl;
  std::cout << "U : " << kU << std::endl;
  std::cout << "Ldvt : " << kLdvt << std::endl;
  std::cout << "Vt : " << kVt << std::endl;
  std::cout << "LWork : " << kLwork << std::endl;
  std::cout << "Work : " << kWork << std::endl;
  std::cout << "IWork : " << kIwork << std::endl;
  std::cout << "Info : " << kInfo << std::endl;
  std::cout << "Tpye : " << kType << std::endl;
}

/*
 *  @name   DCSvdDecompositionCall
 *  @fn inline void DCSvdDecompositionCall(dc_svd_params& p)
 *  @brief  Interface for lapack function call - Undefined type
 *  @throw  std::runtime_error()
 */
template<typename T>
void LinearAlgebra<T>::Lapack::DCSvdDecompositionCall(dc_svd_params &p) {
  throw std::runtime_error("Error Unsupported Type");
}

template<>
void LinearAlgebra<float>::Lapack::DCSvdDecompositionCall(dc_svd_params &p) {
  LTS5_sgesdd(&p.kJobz,
              &p.kM,
              &p.kN,
              reinterpret_cast<lapack_flt*>(p.kA),
              &p.kLda,
              reinterpret_cast<lapack_flt*>(p.kS),
              reinterpret_cast<lapack_flt*>(p.kU),
              &p.kLdu,
              reinterpret_cast<lapack_flt*>(p.kVt),
              &p.kLdvt,
              reinterpret_cast<lapack_flt*>(p.kWork),
              &p.kLwork,
              reinterpret_cast<lapack_int*>(p.kIwork),
              &p.kInfo);
}

template<>
void LinearAlgebra<double>::Lapack::DCSvdDecompositionCall(dc_svd_params &p) {
  LTS5_dgesdd(&p.kJobz,
              &p.kM,
              &p.kN,
              reinterpret_cast<lapack_dbl*>(p.kA),
              &p.kLda,
              reinterpret_cast<lapack_dbl*>(p.kS),
              reinterpret_cast<lapack_dbl*>(p.kU),
              &p.kLdu,
              reinterpret_cast<lapack_dbl*>(p.kVt),
              &p.kLdvt,
              reinterpret_cast<lapack_dbl*>(p.kWork),
              &p.kLwork,
              reinterpret_cast<lapack_int*>(p.kIwork),
              &p.kInfo);
}

#pragma mark -
#pragma mark Lapack wrapper (Linear solver)

/*
 *  @name   LinearSolverCall
 *  @fn inline void LinearSolverCall(lin_solver_params& p)
 *  @brief  Interface for lapack function call - Undefined type
 *  @throw  std::runtime_error()
 */
template<typename T>
void LinearAlgebra<T>::Lapack::LinearSolverCall(lin_solver_params& p) {
  throw std::runtime_error("Error Unsupported Type");
}

template<>
void LinearAlgebra<float>::Lapack::LinearSolverCall(lin_solver_params& p) {
  LTS5_sgels(&p.kTrans,
             &p.kM,
             &p.kN,
             &p.kNrhs,
             reinterpret_cast<lapack_flt*>(p.kA),
             &p.kLda,
             reinterpret_cast<lapack_flt*>(p.kB),
             &p.kLdb,
             reinterpret_cast<lapack_flt*>(p.kWork),
             &p.kLwork,
             &p.kInfo);
}

template<>
void LinearAlgebra<double>::Lapack::LinearSolverCall(lin_solver_params& p) {
  LTS5_dgels(&p.kTrans,
             &p.kM,
             &p.kN,
             &p.kNrhs,
             reinterpret_cast<lapack_dbl*>(p.kA),
             &p.kLda,
             reinterpret_cast<lapack_dbl*>(p.kB),
             &p.kLdb,
             reinterpret_cast<lapack_dbl*>(p.kWork),
             &p.kLwork,
             &p.kInfo);
}

#pragma mark -
#pragma mark Lapack wrapper (Square Linear solver)

/*
 *  @name   SquareLinearSolverCall
 *  @fn inline void SquareLinearSolverCall(lin_solver_params<T>& p)
 *  @brief  Interface for lapack function call - Undefined type
 *  @throw  std::runtime_error()
 */
template<typename T>
void LinearAlgebra<T>::Lapack::
SquareLinearSolverCall(square_lin_solver_params& p) {
  throw std::runtime_error("Error Unsupported Type");
}

template<>
void LinearAlgebra<float>::Lapack::
SquareLinearSolverCall(square_lin_solver_params& p) {
  LTS5_sgesv(&p.k_n,
             &p.k_nrhs,
             (lapack_flt*)p.k_a,
             &p.k_lda,
             (lapack_int*)p.k_ipiv,
             (lapack_flt*)p.k_b,
             &p.k_ldb,
             &p.k_info);
}

template<>
void LinearAlgebra<double>::Lapack::
SquareLinearSolverCall(square_lin_solver_params& p) {
  LTS5_dgesv(&p.k_n,
             &p.k_nrhs,
             (lapack_dbl*)p.k_a,
             &p.k_lda,
             (lapack_int*)p.k_ipiv,
             (lapack_dbl*)p.k_b,
             &p.k_ldb,
             &p.k_info);
}

#pragma mark -
#pragma mark Lapack wrapper (QR Decomposition)

template<typename T>
void LinearAlgebra<T>::Lapack::
QRDecompositionCall(qr_decomposition_params &p) {
  throw std::runtime_error("Error Unsupported Type");
}

template<>
void LinearAlgebra<float>::Lapack::
QRDecompositionCall(qr_decomposition_params &p) {
  LTS5_sgeqrf(&p.m, &p.n, p.a, &p.lda, p.tau, p.work, &p.lwork, &p.info);
}

template<>
void LinearAlgebra<double>::Lapack::
QRDecompositionCall(qr_decomposition_params &p) {
  LTS5_dgeqrf(&p.m, &p.n, p.a, &p.lda, p.tau, p.work, &p.lwork, &p.info);
}

template<typename T>
void LinearAlgebra<T>::Lapack::QRGenerateQCall(qr_q_gen_params& p) {
  throw std::runtime_error("Error Unsupported Type");
}

template<>
void LinearAlgebra<float>::Lapack::QRGenerateQCall(qr_q_gen_params& p) {
  LTS5_sorgqr(&p.m, &p.n, &p.k, p.a, &p.lda, p.tau, p.work, &p.lwork, &p.info);
}

template<>
void LinearAlgebra<double>::Lapack::QRGenerateQCall(qr_q_gen_params& p) {
  LTS5_dorgqr(&p.m, &p.n, &p.k, p.a, &p.lda, p.tau, p.work, &p.lwork, &p.info);
}

template<typename T>
void LinearAlgebra<T>::Lapack::QRQProductCall(qr_q_prod_params& p) {
  throw std::runtime_error("Error Unsupported Type");
}

template<>
void LinearAlgebra<float>::Lapack::QRQProductCall(qr_q_prod_params& p) {
  LTS5_sormqr(&p.side, &p.trans, &p.m, &p.n, &p.k, p.a, &p.lda, p.tau, p.c, &p.ldc,
          p.work, &p.lwork, &p.info);
}

template<>
void LinearAlgebra<double>::Lapack::QRQProductCall(qr_q_prod_params& p) {
  LTS5_dormqr(&p.side, &p.trans, &p.m, &p.n, &p.k, p.a, &p.lda, p.tau, p.c, &p.ldc,
          p.work, &p.lwork, &p.info);
}


#pragma mark -
#pragma mark Symmetric Eigen decomposition

/*
 *  @name   SymEigenDecomposition
 *  @fn SymEigenDecomposition()
 *  @brief Constructor
 */
template<typename T>
LinearAlgebra<T>::SymEigenDecomposition::SymEigenDecomposition() {
  eig_params_.kA = nullptr;
  eig_params_.kEigValues = nullptr;
  eig_params_.kEigVectors = nullptr;
  eig_params_.kWork = nullptr;
  eig_params_.kIwork = nullptr;
  eig_params_.kIfail = nullptr;
}

/*
 *  @name   ~SymEigenDecomposition
 *  @fn ~SymEigenDecomposition()
 *  @brief  Destructor
 */
template<typename T>
LinearAlgebra<T>::SymEigenDecomposition::~SymEigenDecomposition() {
  //Release memory
  if(eig_params_.kA != nullptr) {
    delete [] eig_params_.kA;
  }
  if(eig_params_.kEigValues != nullptr) {
    delete [] eig_params_.kEigValues;
  }
  if(eig_params_.kEigVectors != nullptr) {
    delete [] eig_params_.kEigVectors;
  }
  if(eig_params_.kWork != nullptr) {
    delete [] eig_params_.kWork;
  }
  if(eig_params_.kIwork != nullptr) {
    delete [] eig_params_.kIwork;
  }
  if(eig_params_.kIfail != nullptr) {
    delete [] eig_params_.kIfail;
  }
}

/**
 *  @name   InitOnlyEigenvalueComputation
 *  @fn void InitOnlyEigenvalueComputation(const cv::Mat& matrix_a)
 *  @brief  Initialize internal structure for only eigenvalues computation
 *  @param[in]  matrix_a  Symmetric matrix
 */
template<typename T>
void LinearAlgebra<T>::
SymEigenDecomposition::InitOnlyEigenvalueComputation(const cv::Mat& matrix_a) {
  assert(matrix_a.cols == matrix_a.rows && matrix_a.cols != 0);

  //Define type of job + range
  eig_params_.kJobz = 'N';     //Only eig value
  eig_params_.kRange = 'A';    //All of them
  eig_params_.kUplo = 'U';     //Upper triangle of matrix_a is stored

  //Store matrix in column manjor
  eig_params_.kN = matrix_a.rows;
  eig_params_.kA = new T[eig_params_.kN * eig_params_.kN];
  std::memcpy(static_cast<void*>(eig_params_.kA),
              static_cast<const void*>(matrix_a.data),
              eig_params_.kN * eig_params_.kN * sizeof(T));
  eig_params_.kLda = matrix_a.cols;

  //Init partial computation
  eig_params_.kVl = 0.0;   //Not referenced if RANGE = 'A' or 'I'.
  eig_params_.kVu = 0.0;   //Not referenced if RANGE = 'A' or 'I'.
  eig_params_.kIl = 0;         //Not referenced if RANGE = 'A' or 'V'.
  eig_params_.kIu = 0;         //Not referenced if RANGE = 'A' or 'V'.

  //Set tolerance
  char prec = 'S';
  eig_params_.kAbstol = Lapack::MachinePrecisionCall(&prec);
  //lie in an interval [a,b] of width less
  //than or equal to ABSTOL + EPS *   max( |a|,|b| )

  //Define number of eigval + vector
  eig_params_.kM = eig_params_.kN;
  eig_params_.kEigValues = new T[eig_params_.kN];
  eig_params_.kEigVectors = nullptr;  //Not referenced if JOB = 'N'.
  eig_params_.kLdz = 1;

  //Setup workspace
  eig_params_.kWork = new T;
  eig_params_.kIwork = new int[5*eig_params_.kN];
  eig_params_.kIfail = new int[eig_params_.kN];
  eig_params_.kLwork = -1;
  eig_params_.kInfo = 0;

  //Query workspace size
  Lapack::SymEigenDecompositionCall(eig_params_);

  //Setup workspace with correct size
  eig_params_.kLwork = static_cast<int>(eig_params_.kWork[0]);
  delete eig_params_.kWork;
  eig_params_.kWork = new T[eig_params_.kLwork];
  //Done
}

/*
 *  @name InitFullComputation
 *  @fn void InitFullComputation(const cv::Mat& matrix_a)
 *  @brief  Initialize internal structure for full eigenvalues and
 *          eigenvectors decomposition
 *  @param  matrix_a  Symmetric matrix
 */
template<typename T>
void LinearAlgebra<T>::
SymEigenDecomposition::InitFullComputation(const cv::Mat& matrix_a) {
  assert(matrix_a.cols == matrix_a.rows && matrix_a.cols != 0);

  //Define type of job + range
  eig_params_.kJobz = 'V';
  eig_params_.kRange = 'A';
  eig_params_.kUplo = 'U';

  //Store matrix in column manjor
  eig_params_.kN = matrix_a.rows;
  eig_params_.kA = new T[eig_params_.kN * eig_params_.kN];
  std::memcpy(static_cast<void*>(eig_params_.kA),
              static_cast<const void*>(matrix_a.data),
              eig_params_.kN * eig_params_.kN * sizeof(T));
  eig_params_.kLda = matrix_a.cols;

  //Init partial computation
  eig_params_.kVl = 0.0;   //Not referenced if RANGE = 'A' or 'I'.
  eig_params_.kVu = 0.0;   //Not referenced if RANGE = 'A' or 'I'.
  eig_params_.kIl = 0;         //Not referenced if RANGE = 'A' or 'V'.
  eig_params_.kIu = 0;         //Not referenced if RANGE = 'A' or 'V'.

  //Set tolerance
  char prec = 'S';
  eig_params_.kAbstol = Lapack::MachinePrecisionCall(&prec);
  //lie in an interval [a,b] of width less
  //than or equal to ABSTOL + EPS *   max( |a|,|b| )

  //Define number of eigval + vector
  eig_params_.kM = eig_params_.kN;
  eig_params_.kEigValues = new T[eig_params_.kN];
  eig_params_.kEigVectors = new T[eig_params_.kN * eig_params_.kN];
  eig_params_.kLdz = eig_params_.kN;

  //Setup workspace
  eig_params_.kWork = new T;
  eig_params_.kIwork = new int[5*eig_params_.kN];
  eig_params_.kIfail = new int[eig_params_.kN];
  eig_params_.kLwork = -1;
  eig_params_.kInfo = 0;

  //Query workspace size
  Lapack::SymEigenDecompositionCall(eig_params_);

  //Setup workspace with correct size
  eig_params_.kLwork = static_cast<int>(eig_params_.kWork[0]);
  delete eig_params_.kWork;
  eig_params_.kWork = new T[eig_params_.kLwork];
  //Done
}

/*
 *  @name InitPartialComputation
 *  @fn void InitPartialComputation(const cv::Mat& matrix_a,
                                    const int& number_of_vectors)
 *  @brief  Initialize internal structure for partial eigenvalues and
 *          eigenvectors decomposition
 *  @param[in]  matrix_a          Symmetric matrix
 *  @param[in]  number_of_vectors How many eigenvectors to compute
 */
template<typename T>
void LinearAlgebra<T>::
SymEigenDecomposition::InitPartialComputation(const cv::Mat& matrix_a,
                                              const int& number_of_vectors) {
  assert(matrix_a.cols == matrix_a.rows && matrix_a.cols != 0);

  //Define type of job + range
  eig_params_.kJobz = 'V';
  eig_params_.kRange = 'I';  //the IL-th through IU-th eigenvalues will be found.
  eig_params_.kUplo = 'U';

  //Store matrix in column manjor
  eig_params_.kN = matrix_a.rows;
  eig_params_.kA = new T[eig_params_.kN * eig_params_.kN];
  std::memcpy((void*)eig_params_.kA,
              (const void*)matrix_a.data,
              eig_params_.kN * eig_params_.kN * sizeof(T));
  eig_params_.kLda = matrix_a.cols;

  //Init partial computation
  eig_params_.kVl = 0.0;   //Not referenced if RANGE = 'A' or 'I'.
  eig_params_.kVu = 0.0;   //Not referenced if RANGE = 'A' or 'I'.
  eig_params_.kIl = 1;
  eig_params_.kIu = number_of_vectors;

  //Set tolerance
  char prec = 'S';
  eig_params_.kAbstol = Lapack::MachinePrecisionCall(&prec);
  //lie in an interval [a,b] of width less
  //than or equal to ABSTOL + EPS *   max( |a|,|b| )

  //Define number of eigval + vector
  eig_params_.kM = eig_params_.kIu - eig_params_.kIl + 1;
  eig_params_.kEigValues = new T[eig_params_.kN];
  eig_params_.kEigVectors = new T[eig_params_.kM* eig_params_.kN];
  eig_params_.kLdz = eig_params_.kN;

  //Setup workspace
  eig_params_.kWork = new T;
  eig_params_.kIwork = new int[5*eig_params_.kN];
  eig_params_.kIfail = new int[eig_params_.kN];
  eig_params_.kLwork = -1;
  eig_params_.kInfo = 0;

  //Query workspace size
  Lapack::SymEigenDecompositionCall(eig_params_);

  //Setup workspace with correct size
  eig_params_.kLwork = static_cast<int>(eig_params_.kWork[0]);
  delete eig_params_.kWork;
  eig_params_.kWork = new T[eig_params_.kLwork];
  //Done
}

/*
 *  @name Compute
 *  @fn inline void Compute()
 *  @brief  Call lapack library for decompostion
 */
template<typename T>
void LinearAlgebra<T>::
SymEigenDecomposition::Compute() {
  Lapack::SymEigenDecompositionCall(eig_params_);
}

/*
 *  @name   GetEigenvalues
 *  @fn inline int GetEigenvalues(cv::Mat* eigenvalues)
 *  @brief  Provide access to eigen values
 *  @param[out] eigenvalues in decreasing order
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int LinearAlgebra<T>::
SymEigenDecomposition::GetEigenvalues(cv::Mat* eigenvalues) {
  //Computation ok ?
  int error = -1;
  if (eig_params_.kInfo == 0) {
    //Ok
    eigenvalues->create(eig_params_.kM, 1, cv::DataType<T>::type);
    for(int i = 0 ; i < eig_params_.kM ; ++i) {
      eigenvalues->at<T>(i) = eig_params_.kEigValues[eig_params_.kM -
                                                       i - 1];
    }
    error = 0;
  }
  return error;
}

/*
 *  @name   GetEigenvectors
 *  @fn inline int GetEigenvectors(cv::Mat* eigenvectors)
 *  @brief  Provide access to eigen vectors
 *  @param[out] eigenvectors Eigen vector in decreasing order by row
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int LinearAlgebra<T>::
SymEigenDecomposition::GetEigenvectors(cv::Mat* eigenvectors) {
  int error = -1;
  if (eig_params_.kInfo == 0 && (eig_params_.kJobz == 'V')) {
    //Ok
    //TODO: (Chirstophe) Handle when only a range is computated !
    if(eig_params_.kRange == 'A') {
      eigenvectors->create(eig_params_.kN, eig_params_.kM,
                           cv::DataType<T>::type);
      T* dest_ptr = (T*)eigenvectors->data;
      const T* src_ptr = &eig_params_.kEigVectors[(eig_params_.kM - 1) *
                                                    eig_params_.kLdz];
      for(int i = 0 ; i < eig_params_.kM ; ++i) {
        //Copy eigenvector into decreasing order
        std::memcpy((void*)dest_ptr,(const void*)src_ptr,eig_params_.kN *
                                                         sizeof(T));
        //Change ptr
        dest_ptr += eig_params_.kN;
        src_ptr -= eig_params_.kLdz;
      }
      error = 0;
    } else {
      //Partial computation
      std::cout << "NOT IMPLEMENTED YET !!!" << std::endl;
    }
  }
  return error;
}

#pragma mark -
#pragma mark Non Symmetric Eigen decomposition

/*
 *  @name   ~NSymEigenDecompositon
 *  @fn ~NSymEigenDecompositon()
 *  @brief  Destructor
 */
template<typename T>
LinearAlgebra<T>::NSymEigenDecompositon::~NSymEigenDecompositon() {
  if (eig_params_.kWr) {
    delete[] eig_params_.kWr;
    eig_params_.kWr = nullptr;
  }
  if (eig_params_.kWi) {
    delete[] eig_params_.kWi;
    eig_params_.kWi = nullptr;
  }
  if (eig_params_.kVl) {
    delete[] eig_params_.kVl;
    eig_params_.kVl = nullptr;
  }
  if (eig_params_.kVr) {
    delete[] eig_params_.kVr;
    eig_params_.kVr = nullptr;
  }
  if (eig_params_.kWork) {
    delete[] eig_params_.kWork;
    eig_params_.kWork = nullptr;
  }
}

/*
 *  @name InitFullComputation
 *  @fn void InitFullComputation(const cv::Mat& matrix_a)
 *  @brief  Initialize internal structure for full eigenvalues and
 *          eigenvectors decomposition
 *  @param  matrix_a  Symmetric matrix
 */
template<typename T>
void LinearAlgebra<T>::
NSymEigenDecompositon::InitFullComputation(const cv::Mat& matrix_a) {
  // Allocate memory
  assert(matrix_a.cols == matrix_a.rows);
  // Matrix order
  eig_params_.kN = matrix_a.cols;
  // Lda
  eig_params_.kLda = matrix_a.rows;
  // Define data ptr
  eig_params_.kA = (T*)matrix_a.data;
  // Eigenvalue
  eig_params_.kWr = new T[eig_params_.kN];
  eig_params_.kWi = new T[eig_params_.kN];
  // Eigenvectors
  eig_params_.kLdvl = eig_params_.kN;
  eig_params_.kLdvr = eig_params_.kN;
  eig_params_.kVl = new T[eig_params_.kLdvl * eig_params_.kN];
  eig_params_.kVr = new T[eig_params_.kLdvr * eig_params_.kN];
  // Workspace
  eig_params_.kLwork = T(4.0) * eig_params_.kN;
  eig_params_.kWork = new T[eig_params_.kLwork];
}

/*
 *  @name Compute
 *  @fn inline void Compute()
 *  @brief  Call lapack library for decompostion
 */
template<typename T>
void LinearAlgebra<T>::
NSymEigenDecompositon::Compute() {
  Lapack::NSymEigenDecompositionCall(eig_params_);
}

/*
 *  @name   GetEigenvalues
 *  @fn inline int GetEigenvalues(cv::Mat* real_eigenvalues, cv::Mat* img_eigenvalues)
 *  @brief  Provide access to eigen values
 *  @param[out] real_eigenvalues real part
 *  @param[out] img_eigenvalues imaginary part
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int LinearAlgebra<T>::
NSymEigenDecompositon::GetEigenvalues(cv::Mat* real_eigenvalues,
                                      cv::Mat* img_eigenvalues) {
  // Init mat
  if (!eig_params_.kInfo) {
    real_eigenvalues->create(eig_params_.kN, 1, cv::DataType<T>::type);
    img_eigenvalues->create(eig_params_.kN, 1, cv::DataType<T>::type);
    // Copy value
    memcpy(static_cast<void*>(real_eigenvalues->data),
           static_cast<const void *>(eig_params_.kWr),
           eig_params_.kN * sizeof(T));
    memcpy(static_cast<void*>(img_eigenvalues->data),
           static_cast<const void *>(eig_params_.kWi),
           eig_params_.kN * sizeof(T));
  }
  return eig_params_.kInfo;
}

/*
 *  @name   GetEigenvectors
 *  @fn inline int GetEigenvectors(cv::Mat* eigenvectors)
 *  @brief  Provide access to eigen vectors
 *  @param[out] eigenvectors Eigen vector in decreasing order by row
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int LinearAlgebra<T>::
NSymEigenDecompositon::GetEigenvectors(cv::Mat* eigenvectors) {
  if (!eig_params_.kInfo) {
    eigenvectors->create(eig_params_.kN,
                         eig_params_.kLdvr,
                         cv::DataType<T>::type);
    memcpy(static_cast<void*>(eigenvectors->data),
           static_cast<const void*>(eig_params_.kVr),
           eig_params_.kN * eig_params_.kLdvr * sizeof(T));
  }
  return eig_params_.kInfo;
}

#pragma mark -
#pragma mark SVD decomposition

/*
 *  @name   ~SvdDecomposition
 *  @fn ~SvdDecomposition()
 *  @brief  Destructor
 */
template<typename T>
LinearAlgebra<T>::SvdDecomposition::~SvdDecomposition() {
  if (p_.kA) {
    delete[] p_.kA;
    p_.kA = nullptr;
  }
  if (p_.kS) {
    delete[] p_.kS;
    p_.kS = nullptr;
  }
  if (p_.kU) {
    delete[] p_.kU;
    p_.kU = nullptr;
  }
  if (p_.kVt) {
    delete[] p_.kVt;
    p_.kVt = nullptr;
  }
  if (p_.kWork) {
    delete[] p_.kWork;
    p_.kWork = nullptr;
  }
}

/*
 *  @name Init
 *  @fn void Init(const cv::Mat& matrix, const Lapack::SVDType type)
 *  @brief  Initialize internal structure for svd decomposition
 *  @param[in]  matrix  Matrix to decompose
 *  @param[in]  type    Type of decompostion
 */
template<typename T>
void LinearAlgebra<T>::
SvdDecomposition::Init(const cv::Mat& matrix,
                       const typename Lapack::SVDType& type) {
  p_.kType = type;
  // Dimension
  p_.kM = matrix.rows;
  p_.kN = matrix.cols;
  p_.kLda = p_.kM;
  // Data
  p_.kA = new T[p_.kM * p_.kN];
  cv::Mat colMat = matrix.t();
  std::copy(reinterpret_cast<T*>(colMat.data),
            reinterpret_cast<T*>(colMat.data) + colMat.total(),
            p_.kA);
  // Singular values
  p_.kMin = std::min(p_.kM, p_.kN);
  p_.kS = new T[p_.kMin];
  // Singular vector - dimension
  p_.kLdu = p_.kM;
  // Singular vector buffer
  if(p_.kType == Lapack::SVDType::kFullUV) {
    p_.kJobu = 'A';
    p_.kJobvt = 'A';
    p_.kLdvt = p_.kN;
    p_.kU = new T[p_.kLdu * p_.kM];
    p_.kVt = new T[p_.kLdvt * p_.kN];
  } else {
    p_.kJobu = 'S';
    p_.kJobvt = 'S';
    p_.kLdvt = p_.kMin;
    p_.kU = new T[p_.kLdu * p_.kMin];
    p_.kVt = new T[p_.kLdvt * p_.kMin];
  }
  // Workspace
  p_.kLwork = -1;
  p_.kWork = new T;
  // Define workspace size
  Lapack::SvdDecompositionCall(p_);
  // Setup
  p_.kLwork = (int)p_.kWork[0];
  delete p_.kWork;
  p_.kWork = new T[p_.kLwork];
}

/*
 *  @name Init
 *  @fn void Init(const Matrix& matrix, const Lapack::SVDType type)
 *  @brief  Initialize internal structure for svd decomposition
 *  @param[in]  matrix  Matrix to decompose
 *  @param[in]  type    Type of decompostion
 */
template<typename T>
void LinearAlgebra<T>::
SvdDecomposition::Init(const Matrix& matrix,
                       const typename Lapack::SVDType& type) {
  p_.kType = type;
  // Dimension
  p_.kM = static_cast<int>(matrix.rows());
  p_.kN = static_cast<int>(matrix.cols());
  p_.kLda = p_.kM;
  // Data
  p_.kA = new T[p_.kM * p_.kN];
  std::copy(matrix.data(),
            matrix.data() + (p_.kM * p_.kN),
            p_.kA);
  // Singular values
  p_.kMin = std::min(p_.kM, p_.kN);
  p_.kS = new T[p_.kMin];
  // Singular vector - dimension
  p_.kLdu = p_.kM;
  // Singular vector buffer
  if(p_.kType == Lapack::SVDType::kFullUV) {
    p_.kJobu = 'A';
    p_.kJobvt = 'A';
    p_.kLdvt = p_.kN;
    p_.kU = new T[p_.kLdu * p_.kM];
    p_.kVt = new T[p_.kLdvt * p_.kN];
  } else {
    p_.kJobu = 'S';
    p_.kJobvt = 'S';
    p_.kLdvt = p_.kMin;
    p_.kU = new T[p_.kLdu * p_.kMin];
    p_.kVt = new T[p_.kLdvt * p_.kMin];
  }
  // Workspace
  p_.kLwork = -1;
  p_.kWork = new T[1];
  // Define workspace size
  Lapack::SvdDecompositionCall(p_);
  // Setup
  p_.kLwork = (int)p_.kWork[0];
  delete[] p_.kWork;
  p_.kWork = new T[p_.kLwork];
}

/*
 * @name    Update
 * @fn  void Update(const cv::Mat& matrix)
 * @brief   Update data on which to compute decompisition
 * @param[in] matrix    New data to decompose (must have the same dim)
 */
template<typename T>
void LinearAlgebra<T>::
SvdDecomposition::Update(const cv::Mat& matrix) {
  // Sanity check
  assert(matrix.rows == p_.kM && matrix.cols == p_.kN);
  // Update data
  cv::Mat colMat = matrix.t();
  std::copy(reinterpret_cast<T*>(colMat.data),
            reinterpret_cast<T*>(colMat.data) + colMat.total(),
            p_.kA);
}

/*
 * @name    Update
 * @fn  void Update(const Matrix& matrix)
 * @brief   Update data on which to compute decompisition
 * @param[in] matrix    New data to decompose (must have the same dim)
 */
template<typename T>
void LinearAlgebra<T>::
SvdDecomposition::Update(const Matrix& matrix) {
  // Sanity check
  assert(matrix.rows() == p_.kM && matrix.cols() == p_.kN);
  // Update data
  std::copy(matrix.data(),
            matrix.data() + (p_.kM * p_.kN),
            p_.kA);
}

/*
 *  @name Compute
 *  @fn inline void Compute()
 *  @brief  Call lapack library for decompostion
 */
template<typename T>
void LinearAlgebra<T>::SvdDecomposition::Compute() {
  // Call
  Lapack::SvdDecompositionCall(p_);
}


/*
 * @name    Get
 * @fn  void Get(cv::Mat* U, cv::Mat* S, cv::Mat* Vt)
 * @brief   Provide SVD decomposition
 * @param[out] U    U
 * @param[out] S    Singular value
 * @param[out] Vt   V transpose
 */
template<typename T>
void LinearAlgebra<T>::SvdDecomposition::Get(cv::Mat* U,
                                             cv::Mat* S,
                                             cv::Mat* Vt) {
  // OpencV Friendly
  int ucol = p_.kType == Lapack::SVDType::kFullUV ? p_.kM : p_.kMin;
  if (U != nullptr) {
    *U = cv::Mat(ucol, p_.kLdu, cv::DataType<T>::type, p_.kU).t();
  }
  if (Vt != nullptr) {
    *Vt = cv::Mat(p_.kN, p_.kLdvt, cv::DataType<T>::type, p_.kVt).t();
  }
  if (S != nullptr) {
    *S = cv::Mat::zeros(ucol, p_.kLdvt, cv::DataType<T>::type);
    T* sptr = reinterpret_cast<T*>(S->data);
    for (int i = 0; i < p_.kMin; ++i) {
      sptr[i * S->step1() + i] = p_.kS[i];
    }
  }
}

template<typename T>
void LinearAlgebra<T>::SvdDecomposition::Get(Matrix* U, Matrix* S, Matrix* Vt) {
  using MapType = Eigen::Map<Matrix>;
  int ucol = p_.kType == Lapack::SVDType::kFullUV ? p_.kM : p_.kMin;
  if (U) {
    *U = MapType(p_.kU, p_.kLdu, ucol);
  }
  if (Vt) {
    *Vt = MapType(p_.kVt, p_.kLdvt, p_.kN);
  }
  if (S) {
    S->setZero(ucol, p_.kLdvt);
    T* sptr = S->data();
    for (int i = 0; i < p_.kMin; ++i) {
      sptr[i * ucol + i] = p_.kS[i];
    }
  }
}

/*
 * @name Print
 * @fn  void Print()
 * @brief Print parameters
 */
template<typename T>
void LinearAlgebra<T>::SvdDecomposition::Print() {
  p_.Print();
}

/*
 *  @name   ~DCSvdDecomposition
 *  @fn ~DCSvdDecomposition()
 *  @brief  Destructor
 */
template<typename T>
LinearAlgebra<T>::DCSvdDecomposition::~DCSvdDecomposition() {
  if (p_.kA) {
    delete[] p_.kA;
    p_.kA = nullptr;
  }
  if (p_.kS) {
    delete[] p_.kS;
    p_.kS = nullptr;
  }
  if (p_.kU) {
    delete[] p_.kU;
    p_.kU = nullptr;
  }
  if (p_.kVt) {
    delete[] p_.kVt;
    p_.kVt = nullptr;
  }
  if (p_.kWork) {
    delete[] p_.kWork;
    p_.kWork = nullptr;
  }
  if (p_.kIwork) {
    delete[] p_.kIwork;
    p_.kIwork = nullptr;
  }
}

/*
 *  @name Init
 *  @fn void Init(const cv::Mat& matrix, const Lapack::SVDType& type)
 *  @brief  Initialize internal structure for svd decomposition
 *  @param[in]  matrix  Matrix to decompose
 *  @param[in]  type    Type of decompostion
 */
template<typename T>
void LinearAlgebra<T>::DCSvdDecomposition::Init(const cv::Mat& matrix,
                                                const typename Lapack::SVDType& type) {
  p_.kType = type;
  // Dimension
  p_.kM = matrix.rows;
  p_.kN = matrix.cols;
  p_.kLda = p_.kM;
  // Data
  p_.kA = new T[p_.kM * p_.kN];
  cv::Mat colMat = matrix.t();
  std::copy(reinterpret_cast<T*>(colMat.data),
            reinterpret_cast<T*>(colMat.data) + colMat.total(),
            p_.kA);
  // Singular values
  p_.kMin = std::min(p_.kM, p_.kN);
  p_.kS = new T[p_.kMin];
  // Singular vector - dimension
  p_.kLdu = p_.kM;
  // Singular vector buffer
  if(p_.kType == Lapack::SVDType::kFullUV) {
    p_.kJobz = 'A';
    p_.kLdvt = p_.kN;
    p_.kU = new T[p_.kLdu * p_.kM];
  } else {
    p_.kJobz = 'S';
    p_.kLdvt = p_.kMin;
    p_.kU = new T[p_.kLdu * p_.kMin];
  }
  p_.kVt = new T[p_.kLdvt * p_.kN];
  // IWork
  p_.kIwork = new lapack_int[8 * p_.kMin];
  // Workspace
  p_.kLwork = -1;
  p_.kWork = new T;
  // Define workspace size
  Lapack::DCSvdDecompositionCall(p_);
  // Setup
  p_.kLwork = (int)p_.kWork[0];
  delete p_.kWork;
  p_.kWork = new T[p_.kLwork];
}

/*
 *  @name Init
 *  @fn void Init(const Matrix& matrix, const Lapack::SVDType& type)
 *  @brief  Initialize internal structure for svd decomposition
 *  @param[in]  matrix  Matrix to decompose
 *  @param[in]  type    Type of decompostion
 */
template<typename T>
void LinearAlgebra<T>::DCSvdDecomposition::Init(const Matrix& matrix,
                                                const typename Lapack::SVDType& type) {
  p_.kType = type;
  // Dimension
  p_.kM = static_cast<int>(matrix.rows());
  p_.kN = static_cast<int>(matrix.cols());
  p_.kLda = p_.kM;
  // Data
  p_.kA = new T[p_.kM * p_.kN];
  std::copy(matrix.data(),
            matrix.data() + (matrix.cols() * matrix.rows()),
            p_.kA);
  // Singular values
  p_.kMin = std::min(p_.kM, p_.kN);
  p_.kS = new T[p_.kMin];
  // Singular vector - dimension
  p_.kLdu = p_.kM;
  // Singular vector buffer
  if(p_.kType == Lapack::SVDType::kFullUV) {
    p_.kJobz = 'A';
    p_.kLdvt = p_.kN;
    p_.kU = new T[p_.kLdu * p_.kM];
  } else {
    p_.kJobz = 'S';
    p_.kLdvt = p_.kMin;
    p_.kU = new T[p_.kLdu * p_.kMin];
  }
  p_.kVt = new T[p_.kLdvt * p_.kN];
  // IWork
  p_.kIwork = new lapack_int[8 * p_.kMin];
  // Workspace
  p_.kLwork = -1;
  p_.kWork = new T;
  // Define workspace size
  Lapack::DCSvdDecompositionCall(p_);
  // Setup
  p_.kLwork = (int)p_.kWork[0];
  delete p_.kWork;
  p_.kWork = new T[p_.kLwork];
}

/*
 * @name    Update
 * @fn  void Update(const cv::Mat& matrix)
 * @brief   Update data on which to compute decompisition
 * @param[in] matrix    New data to decompose (must have the same dim)
 */
template<typename T>
void LinearAlgebra<T>::
DCSvdDecomposition::Update(const cv::Mat& matrix) {
  // Sanity check
  assert(matrix.rows == p_.kM && matrix.cols == p_.kN);
  // Update data
  cv::Mat colMat = matrix.t();
  std::copy(reinterpret_cast<T*>(colMat.data),
            reinterpret_cast<T*>(colMat.data) + colMat.total(),
            p_.kA);
}

/*
 * @name    Update
 * @fn  void Update(const Matrix& matrix)
 * @brief   Update data on which to compute decompisition
 * @param[in] matrix    New data to decompose (must have the same dim)
 */
template<typename T>
void LinearAlgebra<T>::
DCSvdDecomposition::Update(const Matrix& matrix) {
  // Sanity check
  assert(matrix.rows() == p_.kM && matrix.cols() == p_.kN);
  // Update data
  std::copy(matrix.data(),
            matrix.data() + (p_.kM * p_.kN),
            p_.kA);
}

/*
 *  @name Compute
 *  @fn inline void Compute()
 *  @brief  Call lapack library for decompostion
 */
template<typename T>
void LinearAlgebra<T>::DCSvdDecomposition::Compute() {
  // Call
  Lapack::DCSvdDecompositionCall(p_);
}

template<typename T>
void LinearAlgebra<T>::DCSvdDecomposition::Get(cv::Mat* U,
                                               cv::Mat* S,
                                               cv::Mat* Vt) {
  // OpencV Friendly
  int ucol = p_.kType == Lapack::SVDType::kFullUV ? p_.kM : p_.kMin;
  if (U != nullptr) {
    *U = cv::Mat(ucol, p_.kLdu, cv::DataType<T>::type, p_.kU).t();
  }
  if (Vt != nullptr) {
    *Vt = cv::Mat(p_.kN, p_.kLdvt, cv::DataType<T>::type, p_.kVt).t();
  }
  if (S != nullptr) {
    *S = cv::Mat::zeros(ucol, p_.kLdvt, cv::DataType<T>::type);
    T *sptr = reinterpret_cast<T *>(S->data);
    for (int i = 0; i < p_.kMin; ++i) {
      sptr[i * S->step1() + i] = p_.kS[i];
    }
  }
}

/*
 * @name    Get
 * @fn  void Get(Matrix* U, Matrix* S, Matrix* Vt)
 * @brief   Provide SVD decomposition
 * @param[out] U    U
 * @param[out] S    Singular value
 * @param[out] Vt   V transpose
 */
template<typename T>
void LinearAlgebra<T>::DCSvdDecomposition::Get(Matrix* U,
                                               Matrix* S,
                                               Matrix* Vt) {
  using MapType = Eigen::Map<Matrix>;
  int ucol = p_.kType == Lapack::SVDType::kFullUV ? p_.kM : p_.kMin;
  if (U) {
    *U = MapType(p_.kU, p_.kLdu, ucol);
  }
  if (Vt) {
    *Vt = MapType(p_.kVt, p_.kLdvt, p_.kN);
  }
  if (S) {
    S->setZero(ucol, p_.kLdvt);
    T* sptr = S->data();
    for (int i = 0; i < p_.kMin; ++i) {
      sptr[i * ucol + i] = p_.kS[i];
    }
  }
}

/*
 * @name Print
 * @fn  void Print()
 * @brief Print parameters
 */
template<typename T>
void LinearAlgebra<T>::DCSvdDecomposition::Print() {
  p_.Print();
}

#pragma mark -
#pragma mark Linear Solver

/*
 *  @name   LinearSolver
 *  @fn LinearSolver()
 *  @brief  Constructor
 */
template<typename T>
LinearAlgebra<T>::LinearSolver::LinearSolver() {
  ls_params_.kA = nullptr;
  ls_params_.kB = nullptr;
  ls_params_.kWork = nullptr;
}

/*
 *  @name   ~LinearSolver
 *  @fn ~LinearSolver()
 *  @brief  Destructor
 */
template<typename T>
LinearAlgebra<T>::LinearSolver::~LinearSolver() {
  if (ls_params_.kA) {
    delete[] ls_params_.kA;
  }
  if (ls_params_.kB) {
    delete[] ls_params_.kB;
  }
  if (ls_params_.kWork) {
    delete[] ls_params_.kWork;
  }
}

/*
 *  @name Solve
 *  @fn void Solve(const cv::Mat& matrix_a,
                    const cv::Mat& matrix_b,
                    cv::Mat* matrix_x)
 *  @brief  Solve AX = B
 *  @param[in]  matrix_a  Matrix A
 *  @param[in]  matrix_b  Matrix B, stored in column
 *  @param[out] matrix_x  Solution
 */
template<typename T>
void LinearAlgebra<T>::LinearSolver::Solve(const cv::Mat& matrix_a,
                                           const cv::Mat& matrix_b,
                                           cv::Mat* matrix_x) {
  //Init
  ls_params_.kTrans = 'N';
  //Convert to column major memory layout for A
  ls_params_.kM = matrix_a.rows; //Column-major system
  ls_params_.kN = matrix_a.cols;
  ls_params_.kNrhs = matrix_b.cols;
  ls_params_.kA = new T[ls_params_.kM *
                                 ls_params_.kN];
  cv::Mat tmp_mat_a = cv::Mat(matrix_a.cols,
                              matrix_a.rows,
                              cv::DataType<T>::type,
                              ls_params_.kA );
  tmp_mat_a = matrix_a.t();
  ls_params_.kLda = ls_params_.kM;
  //Convert to column major memory layout for B
  ls_params_.kLdb = std::max(ls_params_.kM,
                                      ls_params_.kN);
  ls_params_.kB = new T[ls_params_.kLdb *
                                 ls_params_.kNrhs];
  cv::Mat tmp_mat_b = cv::Mat(matrix_b.cols,
                              matrix_b.rows,
                              cv::DataType<T>::type,
                              ls_params_.kB);
  tmp_mat_b = matrix_b.t();
  //Workspace
  ls_params_.kWork = new T;
  ls_params_.kLwork = -1;
  //Query workspace size
  Lapack::LinearSolverCall(ls_params_);
  //Setup workspace with correct size
  ls_params_.kLwork = static_cast<int>(ls_params_.kWork[0]);
  delete ls_params_.kWork;
  ls_params_.kWork = new T[ls_params_.kLwork];
  ls_params_.kInfo = 0;
  //Done, Solve the problem
  Lapack::LinearSolverCall(ls_params_);
  //Retrive solution
  if (ls_params_.kInfo == 0) {
    matrix_x->create(ls_params_.kNrhs,matrix_a.cols,cv::DataType<T>::type);
    T* src_ptr = ls_params_.kB;
    T* dest_ptr = reinterpret_cast<T*>(matrix_x->data);
    for(int i = 0 ; i < ls_params_.kNrhs; ++i) {
      std::memcpy((void*)dest_ptr,(const void*)src_ptr, ls_params_.kN * sizeof(T));
      src_ptr += ls_params_.kLdb;
      dest_ptr += ls_params_.kN;
    }
    //Transpose back to columnwise structure
    *matrix_x = matrix_x->t();
  } else {
    *matrix_x = cv::Mat();
  }
}

#pragma mark -
#pragma mark Square Linear Solver

/*
 *  @name   SquareLinearSolver
 *  @fn SquareLinearSolver()
 *  @brief  Constructor
 */
template<typename T>
LinearAlgebra<T>::SquareLinearSolver::SquareLinearSolver() {
  p_.k_a = nullptr;
  p_.k_ipiv = nullptr;
  p_.k_b = nullptr;
}

/*
 *  @name   ~SquareLinearSolver
 *  @fn ~SquareLinearSolver()
 *  @brief  Destructor
 */
template<typename T>
LinearAlgebra<T>::SquareLinearSolver::~SquareLinearSolver() {
  if (p_.k_a) {
    delete[] p_.k_a;
    p_.k_a = nullptr;
  }
  if (p_.k_b) {
    delete[] p_.k_b;
    p_.k_b = nullptr;
  }
  if (p_.k_ipiv) {
    delete[] p_.k_ipiv;
    p_.k_ipiv = nullptr;
  }
}

/*
 *  @name Solve
 *  @fn void Solve(const cv::Mat& matrix_a,
                    const cv::Mat& matrix_b,
                    cv::Mat* matrix_x)
 *  @brief  Solve AX = B
 *  @param[in]  matrix_a  Matrix A
 *  @param[in]  matrix_b  Matrix B, stored in column
 *  @param[out] matrix_x  Solution
 */
template<typename T>
void LinearAlgebra<T>::SquareLinearSolver::Solve(const cv::Mat& matrix_a,
                                                 const cv::Mat& matrix_b,
                                                 cv::Mat* matrix_x) {
  assert(matrix_a.rows == matrix_a.cols);
  // Init
  if (p_.k_n != matrix_a.rows ||
          p_.k_lda != matrix_a.rows ||
          p_.k_nrhs != matrix_b.cols) {
    // Release ressources if necessary
    if (p_.k_a) { delete p_.k_a; }
    if (p_.k_b) { delete p_.k_b; }
    if (p_.k_ipiv) { delete p_.k_ipiv; }
    // Define dimensions
    p_.k_n = matrix_a.rows; //Column-major system
    p_.k_lda = matrix_a.rows;
    p_.k_nrhs = matrix_b.cols;
    p_.k_ldb = p_.k_n;
    // Init arrays
    p_.k_a = new T[p_.k_n * p_.k_n];
    p_.k_b = new T[p_.k_ldb * p_.k_nrhs];
    p_.k_ipiv = new int[p_.k_n];
  }

  //Convert to column major memory layout for A
  cv::Mat tmp_a = cv::Mat(matrix_a.cols,
                          matrix_a.rows,
                          cv::DataType<T>::type,
                          p_.k_a );
  tmp_a = matrix_a.t();
  //Convert to column major memory layout for B
  cv::Mat tmp_b = cv::Mat(matrix_b.cols,
                          matrix_b.rows,
                          cv::DataType<T>::type,
                          p_.k_b);
  tmp_b = matrix_b.t();
  //Done, Solve the problem
  p_.k_info = 0;
  Lapack::SquareLinearSolverCall(p_);
  // Retrieve results
  if (p_.k_info == 0) {
    cv::Mat buff(p_.k_nrhs, p_.k_ldb, cv::DataType<T>::type, p_.k_b);
    *matrix_x = buff.t();
  } else {
    LTS5_LOG_INFO("Solver can not find a solution to the provided system");
    if (p_.k_info > 0) {
      std::cout << "Diag elem: ";
      std::cout << matrix_a.at<T>(p_.k_info, p_.k_info) << std::endl;
      std::cout << matrix_a << std::endl;
    }
    *matrix_x = cv::Mat();
  }
}

#pragma mark -
#pragma mark QR Decomposition

template<typename T>
LinearAlgebra<T>::QRDecomposition::QRDecomposition(const cv::Mat& A) {
  int err = this->Init(A);
  if (!err) {
    err |= this->Compute();
  }
  if (err) {
    throw std::runtime_error("Error while computing QR decomposition");
  }
}

template<typename T>
int LinearAlgebra<T>::QRDecomposition::Init(const cv::Mat &A) {
  qr_p_.Clear();
  // Init storage for A
  qr_p_.m = A.rows;
  qr_p_.n = A.cols;
  qr_p_.lda = A.rows;
  qr_p_.a = new T[A.rows * A.cols];
  cv::Mat a_tmp(A.cols, A.rows, cv::DataType<T>::type, (void*)qr_p_.a);
  a_tmp = A.t();
  // Init tau array
  qr_p_.tau = new T[std::min(A.rows, A.cols)];
  // Query work space size via lwork = -1:
  qr_p_.lwork = -1;
  qr_p_.work = new T[1];
  qr_p_.info = 0;
  // Query size
  Lapack::QRDecompositionCall(qr_p_);
  // Initialize work array
  qr_p_.lwork = static_cast<int>(qr_p_.work[0]);
  delete[] qr_p_.work;
  qr_p_.work = new T[qr_p_.lwork];
  // Done
  return qr_p_.info == 0 ? 0 : -1;
}

template<typename T>
int LinearAlgebra<T>::QRDecomposition::Compute() {
  Lapack::QRDecompositionCall(qr_p_);
  return qr_p_.info == 0 ? 0 : -1;
}

template<typename T>
int LinearAlgebra<T>::QRDecomposition::
MultiplyBy(MultType type, bool transpose, cv::Mat* C) {
  // Clear config
  q_prod_p_.Clear();
  // Add sanity check for input dimensions!
  int n_inner_rows = type == kPre ? qr_p_.m : C->rows;
  int n_inner_cols = type == kPre ? C->cols : qr_p_.m;
  if (n_inner_rows != n_inner_cols) {
    LTS5_LOG_ERROR("Dimensions between `C` and `Q` does not match!");
    return -1;
  }
  // Init struct
  q_prod_p_.side = type == kPre ? 'R' : 'L';
  q_prod_p_.trans = transpose ? 'T' : 'N';
  q_prod_p_.m = C->rows;
  q_prod_p_.n = C->cols;
  // Tau
  q_prod_p_.k = std::min(qr_p_.m, qr_p_.n);
  q_prod_p_.tau = qr_p_.tau;
  // A matrix
  q_prod_p_.a = qr_p_.a;
  q_prod_p_.lda = qr_p_.lda;
  // C matrix
  q_prod_p_.ldc = C->rows;
  q_prod_p_.c = new T[C->rows * C->cols];
  cv::Mat c_tmp(C->cols, C->rows,
                cv::DataType<T>::type, (void*)q_prod_p_.c);
  c_tmp = C->t();
  // Query work space
  q_prod_p_.lwork = -1;
  q_prod_p_.work = new T[1];
  Lapack::QRQProductCall(q_prod_p_);
  // Init workspace
  q_prod_p_.lwork = static_cast<int>(q_prod_p_.work[0]);
  delete[] q_prod_p_.work;
  q_prod_p_.work = new T[q_prod_p_.lwork];
  // Clear error
  q_prod_p_.info = 0;
  // Run computation
  Lapack::QRQProductCall(q_prod_p_);
  if (q_prod_p_.info == 0) {
    // Convert C back to row-major layout
    cv::Mat c_tmp(C->cols, C->rows,
                  cv::DataType<T>::type,
                  (void *) q_prod_p_.c);
    *C = c_tmp.t();
  }
  return q_prod_p_.info == 0 ? 0 : -1;
}


template<typename T>
int LinearAlgebra<T>::QRDecomposition::GetComponents(bool full,
                                                     cv::Mat* Q,
                                                     cv::Mat* R) {
  int err = 0;
  if (R != nullptr) {
    int n_row = full ? qr_p_.m : std::min(qr_p_.m, qr_p_.n);
    R->create(n_row, qr_p_.n, cv::DataType<T>::type);
    R->setTo(T(0.0));
    // Fill matrix
    for (int i = 0; i < n_row; ++i) {
      for (int j = i; j < qr_p_.n; ++j) {
        // Flat index
        int flat_idx = j * qr_p_.lda + i;
        R->at<T>(i, j) = qr_p_.a[flat_idx];   // Upper triangle
      }
    }
  }
  if (Q != nullptr) {
    q_gen_p_.Clear();
    // Init storage for A
    q_gen_p_.m = qr_p_.m;
    q_gen_p_.n = full ? qr_p_.m : std::min(qr_p_.m, qr_p_.n);
    // Assign larger array to be sure that QR decomposition can fit in.
    int sz = std::max(q_gen_p_.m * q_gen_p_.n, qr_p_.m * qr_p_.n);
    q_gen_p_.a = new T[sz];
    std::copy(qr_p_.a, qr_p_.a + (qr_p_.m * qr_p_.n), q_gen_p_.a);
    q_gen_p_.lda = qr_p_.lda;
    // Tau
    q_gen_p_.k = std::min(qr_p_.m, qr_p_.n);
    q_gen_p_.tau = new T[q_gen_p_.k];
    std::copy(qr_p_.tau, qr_p_.tau + q_gen_p_.k, q_gen_p_.tau);
    // Query workspace size
    q_gen_p_.lwork = -1;
    q_gen_p_.work = new T[1];
    Lapack::QRGenerateQCall(q_gen_p_);
    // Init Work space
    q_gen_p_.lwork = static_cast<int>(q_gen_p_.work[0]);
    delete[] q_gen_p_.work;
    q_gen_p_.work = new T[q_gen_p_.lwork];
    // Clear error
    q_gen_p_.info = 0;
    // Call
    Lapack::QRGenerateQCall(q_gen_p_);
    // Q is stored in q_gen_p_.a in column major format
    cv::Mat q_tmp(q_gen_p_.n,
                  q_gen_p_.m,
                  cv::DataType<T>::type,
                  (void *) q_gen_p_.a);
    *Q = q_tmp.t();   // Row major format
    // Done
    err = q_gen_p_.info == 0 ? 0 : -1;
  }
  // Done
  return err;
}

#pragma mark -
#pragma mark Declaration
/** Float algebra */
template class LinearAlgebra<float>;
/** Double algebra */
template class LinearAlgebra<double>;

}  // namepsace LTS5
