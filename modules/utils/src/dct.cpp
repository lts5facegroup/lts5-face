/**
 *  @file   dct.cpp
 *  @brief  DCT methods implementation
 *
 *  @author Marina Zimmermann
 *  @date   24/07/16
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */

#include <cassert>

#include "lts5/utils/dct.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @name zigzagMatrix
 *  @fn     void zigzagMatrix(const cv::Mat& image,
                            const cv::Size normalized_size,
                            cv::Mat* full_vect,
                            cv::Mat* odd_vect,
                            cv::Mat* even_odd_vect)
 *  @brief  Reads a matrix in zigzag order and returns vectors containing
 *          coefficients from the full, odd or even and odd columns
 *  @param[in]  matrix              Input matrix
 *  @param[in]  normalized_size     Size to normalize image to
 *  @param[out] full_vect           Full output in zigzag order (with DC)
 *  @param[out] odd_vect            Odd output in zigzag order (no DC)
 *  @param[out] even_odd_vect       Even and odd output in zigzag order (no DC)
 */
void zigzagMatrix(const cv::Mat& matrix,
                  const cv::Size normalized_size,
                  cv::Mat* full_vect,
                  cv::Mat* odd_vect,
                  cv::Mat* even_odd_vect) {
  const int dct_dtype = CV_32FC1;
  int normalized_width = normalized_size.width;
  int normalized_height = normalized_size.height;
  full_vect->create(1, normalized_width*normalized_height, dct_dtype);
  int n_odd_value = int(std::ceil(normalized_width / 2.0)) * normalized_height - 1;
  odd_vect->create(1, n_odd_value, dct_dtype);
  even_odd_vect->create(1, normalized_width*normalized_height - 1, dct_dtype);

  int rows = matrix.rows;
  int cols = matrix.cols;
  int row = 0;
  int col = 0;
  int diag = 0;

  int tot_ind = rows * cols;
  int full_vect_ind = 0;
  int odd_vect_ind = 0;
  int even_odd_vect_ind = 0;

  for (int ind = 0; ind < tot_ind; ind++) {
    // Odd columns (since the definition starts counting columns at 1)
    if (col % 2 == 0) {
      full_vect->at<float>(0, full_vect_ind) = matrix.at<float>(row, col);
      full_vect_ind++;

      // Except for DC component
      if (ind != 0) {
        assert(odd_vect_ind < odd_vect->cols && "Write outside matrix");
        odd_vect->at<float>(0, odd_vect_ind) = matrix.at<float>(row, col);
        even_odd_vect->at<float>(0, even_odd_vect_ind) = matrix.at<float>(row, col);
        odd_vect_ind++;
        even_odd_vect_ind++;
      }
    }
    // Even columns
    else {
      full_vect->at<float>(0, full_vect_ind) = matrix.at<float>(row, col);
      even_odd_vect->at<float>(0, even_odd_vect_ind) = matrix.at<float>(row, col);
      full_vect_ind++;
      even_odd_vect_ind++;
    }

    if (diag % 2 == 0) {
      if (col >= cols - 1) {
        row++;
        col = cols - 1;
        diag++;
      }
      else if (row <= 0) {
        row = 0;
        col++;
        diag++;
      }
      else {
        row--;
        col++;
      }
    }
    else if (diag % 2 == 1) {
      if (row >= rows - 1) {
        row = rows - 1;
        col++;
        diag++;
      }
      else if (col == 0) {
        row++;
        col = 0;
        diag++;
      }
      else {
        row++;
        col--;
      }
    }
  }
}


/**
 *  @name ComputeDct
 *  @fn     void ComputeDct(const cv::Mat& image,
                            const cv::Size normalized_size,
                            cv::Mat* full_dct,
                            cv::Mat* odd_dct,
                            cv::Mat* even_odd_dct)
 *  @brief  Computes the DCT and returns the coefficients in zigzag order
 *  @param[in]  image              Input image
 *  @param[in]  normalized_size    Size to normalize image to
 *  @param[out] dct_image          Full DCT output
 *  @param[out] full_dct           Full DCT output in zigzag order (with DC)
 *  @param[out] odd_dct            Odd DCT output in zigzag order (no DC)
 *  @param[out] even_odd_dct       Even and odd DCT output in zigzag order (no DC)
 */
void ComputeDct(const cv::Mat& image,
                const cv::Size normalized_size,
                cv::Mat* dct_image,
                cv::Mat* full_dct,
                cv::Mat* odd_dct,
                cv::Mat* even_odd_dct) {
  const int dct_dtype = CV_32FC1;

  cv::Mat temp_image;
  cv::Mat working_image(normalized_size, dct_dtype);

  dct_image->create(normalized_size, dct_dtype);
  image.convertTo(temp_image, dct_dtype);

  cv::resize(temp_image, working_image, normalized_size);
  cv::dct(working_image, *dct_image);

  // Extract coefficients in zigzag order
  zigzagMatrix(*dct_image, normalized_size, full_dct, odd_dct, even_odd_dct);

}
}