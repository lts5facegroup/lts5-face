/**
 *  @file   file_io.cpp
 *  @brief  Input/Output file data transfert
 *
 *  @author Christophe Ecabert
 *  @date   22/04/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <iostream>
#include <sstream>
#include <vector>
#include <dirent.h>
#include <zlib.h>
#include <cstring>
#include <algorithm>

#include "opencv2/core.hpp"
#include "tinydir.h"

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/binary_operations.hpp"
#include "lts5/utils/logger.hpp"

#include "Eigen/Dense"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name   ReadMatFromChar
 *  @brief  Load matrix from text file
 *  @param[in]  input_stream    Stream to data file location
 *  @param[out] matrix          Matrix with data loaded
 *  @return -1 if error, 0 otherwise
 */
int ReadMatFromChar(std::istream& input_stream, cv::Mat* matrix) {
  //Everything ok ?
  int error = -1;

  if (input_stream.good()) {
    //Start reading data
    int mat_type = -1;
    int mat_rows = -1;
    int mat_cols = -1;

    input_stream >> mat_type >> mat_rows >> mat_cols;
    if (mat_rows != -1 && mat_cols != -1) {
      //Init matrix
      matrix->create(mat_rows,mat_cols,mat_type);
      double value = 0.0;
      //Fill
      for (int row = 0 ; row < mat_rows; ++row) {
        for (int col = 0; col < mat_cols; ++col) {
          if(!(input_stream >> value)) {
            error = -1;
            break;
          }
          error = 0.0;
          MatrixSetValue(row, col, value, matrix);
        }
        if (error == -1){ break; }
      }
    }
  }
  return error;
}

/*
 *  @name   SaveMatToChar
 *  @brief  Write matrix to text file
 *  @param[in]  output_stream   Output stream to filen
 *  @param[out] matrix          Matrix to write
 *  @return -1 if error, 0 otherwise
 */
int WriteMatToChar(std::ostream& output_stream,const cv::Mat& matrix) {
  int error = -1;
  if (output_stream.good() && !matrix.empty()) {
    //Ok, write matrix header
    output_stream << matrix.type() << std::endl;
    output_stream << matrix.rows << std::endl;
    output_stream << matrix.cols << std::endl;
    //Set precision
    std::streamsize stream_precision =
    output_stream.precision(std::numeric_limits<double>::digits10);

    //Flush data
    for (int row = 0; row < matrix.rows; ++row) {
      for (int col = 0; col < matrix.cols; ++col) {
        output_stream << GetMatrixValue(row, col, matrix) << " ";
      }
      output_stream << std::endl;
    }
    output_stream.precision(stream_precision);
    error = 0;
  }
  return error;
}

/*
 *  @name   ReadMatFromBin
 *  @brief  Load matrix from binary stream
 *  @param[in]  input_stream    Stream to data file location
 *  @param[out] matrix          Matrix with data loaded.
 *  @return -1 if error, 0 otherwise
 */
int ReadMatFromBin(std::istream& input_stream, cv::Mat* matrix) {
  int err = -1;
  if (input_stream.good()) {
    // read the header first (3 x int)
    int matrix_type = 0;
    input_stream.read(reinterpret_cast<char *>(&matrix_type),
                      sizeof(matrix_type));
    int rows = 0;
    input_stream.read(reinterpret_cast<char *>(&rows), sizeof(rows));
    int cols = 0;
    input_stream.read(reinterpret_cast<char *>(&cols), sizeof(cols));
    matrix->create(rows, cols, matrix_type);
    // and then the data
    input_stream.read(matrix->ptr<char>(), matrix->elemSize() * rows * cols);
    err = input_stream.good() ? 0 : -1;
  }
  return err;
}

/*
 *  @name   ReadEigenFromBin
 *  @fn int ReadEigenFromBin(std::istream& input_stream,
                             Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* matrix)
 *  @brief  Load eigen matrix from binary stream
 *  @ingroup utils
 *  @param[in]  input_stream    Stream to data file location
 *  @param[out] matrix          Matrix with data loaded.
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int ReadEigenFromBin(std::istream& input_stream,
                     Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* matrix) {
  int err = -1;
  if (input_stream.good()) {
    // Get dimension
    int type, rows, cols;
    input_stream.read(reinterpret_cast<char*>(&type), sizeof(type));
    input_stream.read(reinterpret_cast<char*>(&rows), sizeof(rows));
    input_stream.read(reinterpret_cast<char*>(&cols), sizeof(cols));
    if (type == cv::DataType<T>::type) {
      // Init matrix
      matrix->resize(rows, cols);
      // Load data
      input_stream.read(reinterpret_cast<char*>(matrix->data()),
                        rows * cols * sizeof(T));
      // Sanity check
      err = input_stream.good() ? 0 : -1;
    } else {
      // Wrong type
      err = -2;
    }
  }
  return err;
}

template
int ReadEigenFromBin<float>(std::istream& input_stream,
                            Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>* matrix);
template
int ReadEigenFromBin<double>(std::istream& input_stream,
                             Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>* matrix);

/*
 *  @name   ReadMatFromBinTyped
 *  @fn int ReadMatFromBinTyped(std::istream& input_stream, cv::Mat* matrix)
 *  @brief  Load matrix from binary stream into a matrix of a given type. Type
 *          conversion may occurs in the loading chain.
 *  @ingroup utils
 *  @param[in]  input_stream    Stream to data file location
 *  @param[out] matrix          Matrix with data loaded.
 *  @return -1 if error, 0 otherwise
 *  @tparam T Output matrix data  type wanted
 */
template<typename T>
int ReadMatFromBinTyped(std::istream& input_stream, cv::Mat* matrix) {
  // read the header first (3 x int)
  int matrix_type;
  input_stream.read(reinterpret_cast<char *>(&matrix_type),
                    sizeof(matrix_type));
  int rows;
  input_stream.read(reinterpret_cast<char *>(&rows), sizeof(rows));
  int cols;
  input_stream.read(reinterpret_cast<char *>(&cols), sizeof(cols));
  // Check type
  if (matrix_type == cv::DataType<T>::type) {
    matrix->create(rows, cols, matrix_type);
    // and then the data
    input_stream.read(matrix->ptr<char>(), matrix->elemSize() * rows * cols);
  } else {
    // Need conversion
    cv::Mat buffer(rows, cols, matrix_type);
    input_stream.read(buffer.ptr<char>(), buffer.elemSize() * rows * cols);
    buffer.convertTo(*matrix, cv::DataType<T>::type);
  }
  return input_stream.good() ? 0 : -1;
}

// Signed char
template
int ReadMatFromBinTyped<int8_t>(std::istream& input_stream, cv::Mat* matrix);
// Unsigned char
template
int ReadMatFromBinTyped<uint8_t>(std::istream& input_stream, cv::Mat* matrix);
// Signed short
template
int ReadMatFromBinTyped<int16_t>(std::istream& input_stream, cv::Mat* matrix);
// Unsigned short
template
int ReadMatFromBinTyped<uint16_t>(std::istream& input_stream, cv::Mat* matrix);
// Signed int
template
int ReadMatFromBinTyped<int>(std::istream& input_stream, cv::Mat* matrix);
// Float
template
int ReadMatFromBinTyped<float>(std::istream& input_stream, cv::Mat* matrix);
// Double
template
int ReadMatFromBinTyped<double>(std::istream& input_stream, cv::Mat* matrix);

/*
 *  @name   WriteMatToBin
 *  @brief  Write matrix to binary output stream
 *  @param[in]  output_stream   Output stream to filen
 *  @param[in]  matrix          Matrix to write
 *  @return -1 if error, 0 otherwise
 */
int WriteMatToBin(std::ostream& output_stream, const cv::Mat& matrix) {
  assert(matrix.channels() == 1);

  int error = -1;
  if (output_stream.good() && !matrix.empty()) {
    //Ok, write matrix header
    int matrix_type = matrix.type();
    output_stream.write(reinterpret_cast<const char *>(&matrix_type),
                        sizeof(matrix_type));
    int matrix_rows = matrix.rows;
    output_stream.write(reinterpret_cast<const char *>(&matrix_rows),
                        sizeof(matrix_rows));
    int matrix_cols = matrix.cols;
    output_stream.write(reinterpret_cast<const char *>(&matrix_cols),
                        sizeof(matrix_cols));
    output_stream.write(matrix.ptr<const char>(),
                        matrix.elemSize() * matrix_rows * matrix_cols);
    error = output_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name   WriteMatToBin
 *  @fn int WriteMatToBin(std::ostream& output_stream,
                const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& matrix)
 *  @brief  Write eigen matrix to binary output stream
 *  @ingroup utils
 *  @param[in]  output_stream   Output stream to filen
 *  @param[in] matrix          Matrix to write
 *  @return -1 if error, 0 otherwise
 *  @tparam T data type
 */
template<typename T>
int WriteEigenToBin(std::ostream& output_stream,
                    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& matrix) {
  int err = -1;
  if (output_stream.good()) {
    int type = cv::DataType<T>::type;
    int rows = static_cast<int>(matrix.rows());
    int cols = static_cast<int>(matrix.cols());
    // Dump dimension + type to ensure proper type when reading back.
    output_stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    output_stream.write(reinterpret_cast<const char*>(&rows), sizeof(rows));
    output_stream.write(reinterpret_cast<const char*>(&cols), sizeof(cols));
    // Dump data
    output_stream.write(reinterpret_cast<const char*>(matrix.data()),
                        rows * cols * sizeof(T));
    // sanity check
    err = output_stream.good() ? 0 : -1;
  }
  return err;
}

template
int WriteEigenToBin<float>(std::ostream& output_stream,
                           const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>& matrix);
template
int WriteEigenToBin<int>(std::ostream& output_stream,
                           const Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic>& matrix);
template
int WriteEigenToBin<double>(std::ostream& output_stream,
                            const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>& matrix);

/*
 *  @name   ReadMatFromHtkBin
 *  @brief  Load matrix from HTK data binary stream
 *  @param[in]  input_stream    Stream to data file location
 *  @param[out] matrix          Matrix with data loaded.
 *  @return -1 if error, 0 otherwise
 */
int ReadMatFromHtkBin(std::istream& input_stream, cv::Mat* matrix) {
  // read the header first (2 x long, 2 x short)
  int nsamples_temp;
  int nsamples;
  input_stream.read(reinterpret_cast<char *>(&nsamples_temp),
                    sizeof(nsamples_temp));
  nsamples = LongSwap(nsamples_temp);

  int sampPeriod_temp;
  int sampPeriod;
  input_stream.read(reinterpret_cast<char *>(&sampPeriod_temp),
                    sizeof(sampPeriod_temp));
  sampPeriod = LongSwap(sampPeriod_temp);

  short sampSize_temp;
  short sampSize;
  input_stream.read(reinterpret_cast<char *>(&sampSize_temp),
                    sizeof(sampSize_temp));
  sampSize = ShortSwap(sampSize_temp);

  short parmKind_temp;
  short parmKind;
  input_stream.read(reinterpret_cast<char *>(&parmKind_temp),
                    sizeof(parmKind_temp));
  parmKind = ShortSwap(parmKind_temp);

  int matrix_type = CV_32FC1;
  int rows = nsamples;
  int cols = sampSize / 4;

  matrix->create(rows, cols, matrix_type);
  // and then the data
  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      float temp_feat;
      input_stream.read(reinterpret_cast<char *>(&temp_feat),
                        sizeof(temp_feat));
      matrix->at<float>(row, col) = FloatSwap(temp_feat);
    }
  }

  return 0;
}

/*
 *  @name   WriteMatToHtkBin
 *  @brief  Write matrix to binary HTK data output stream
 *  @param[in]  output_stream   Output stream to HTK data file
 *  @param[in]  matrix          Matrix to write
 *  @return -1 if error, 0 otherwise
 */
int WriteMatToHtkBin(std::ostream& output_stream, const cv::Mat& matrix) {
  assert(matrix.channels() == 1);

  int error = -1;
  if (output_stream.good() && !matrix.empty()) {
    //Ok, write matrix header
    int nsamples = LongSwap(matrix.rows);
    output_stream.write(reinterpret_cast<char *>(&nsamples), sizeof(nsamples));

    int sampPeriod = LongSwap(333333); // TODO: make this depend on actual sampPeriod
    output_stream.write(reinterpret_cast<char *>(&sampPeriod), sizeof(sampPeriod));

    //short sampSize = ShortSwap(44 * 4);
    short sampSize = ShortSwap(matrix.cols * 4);
    output_stream.write(reinterpret_cast<char *>(&sampSize), sizeof(sampSize));

    short parmKind = ShortSwap(9);
    output_stream.write(reinterpret_cast<char *>(&parmKind), sizeof(parmKind));

    if (matrix.type() != CV_32F) {
      cv::Mat temp_matrix;
      matrix.convertTo(temp_matrix, CV_32F);
      for (int row = 0; row < temp_matrix.rows; row++) {
        for (int col = 0; col < temp_matrix.cols; col++) {
          float temp_feat = FloatSwap(temp_matrix.at<float>(row, col));
          output_stream.write(reinterpret_cast<char *>(&temp_feat),
                              sizeof(temp_feat));
        }
      }
    }

    else {
      for (int row = 0; row < matrix.rows; row++) {
        for (int col = 0; col < matrix.cols; col++) {
          float temp_feat = FloatSwap(matrix.at<float>(row, col));
          output_stream.write(reinterpret_cast<char *>(&temp_feat),
                              sizeof(temp_feat));
        }
      }
    }
    error = 0;
  }
  return error;
}

/*
 *  @name   LoadMatFromChar
 *  @brief  Load matrix from text file
 *  @param[in]  filename    Input filename
 *  @param[out] matrix      Matrix where data will be loaded
 *  @return -1 if error, 0 otherwise
 */
int LoadMatFromChar(const std::string& filename, cv::Mat* matrix) {
  //Open stream
  int error = -1;
  std::ifstream input_file(filename.c_str(),std::ifstream::in);
  if (input_file.is_open())
  {
    error = LTS5::ReadMatFromChar(input_file, matrix);
    input_file.close();
  }
  return error;
}

/*
 *  @name   LoadMatFromBin
 *  @brief  Load matrix from binary file
 *  @param[in]  filename    Input filename
 *  @param[out] matrix      Matrix where data will be loaded. Should be
 allocated correctly already! Otherwise call
 ReadMatFromBin(std::istream& input_stream)
 *  @return -1 if error, 0 otherwise
 */
int LoadMatFromBin(const std::string& filename, cv::Mat* matrix) {
  //Open stream
  int error = -1;
  std::ifstream input_file(filename.c_str(),
                           std::ifstream::in|std::fstream::binary);
  if (input_file.is_open()) {
    error = LTS5::ReadMatFromBin(input_file, matrix);
    input_file.close();
  }
  return error;
}

/*
 *  @name   LoadMatFromBinTyped
 *  @fn int LoadMatFromBinTyped(const std::string& filename, cv::Mat* matrix)
 *  @brief  Load matrix from binary file into a given typed matrix.
 *  @ingroup utils
 *  @param[in]  filename    Input filename
 *  @param[out] matrix      Matrix where data will be loaded
 *  @return -1 if error, 0 otherwise
 *  @tparam Output matrix data type
 */
template<typename T>
int LoadMatFromBinTyped(const std::string& filename, cv::Mat* matrix) {
  //Open stream
  int error = -1;
  std::ifstream input_file(filename.c_str(),
                           std::ifstream::in|std::fstream::binary);
  if (input_file.is_open()) {
    error = LTS5::ReadMatFromBinTyped<T>(input_file, matrix);
    input_file.close();
  }
  return error;
}

// Signed char
template
int LoadMatFromBinTyped<int8_t>(const std::string& filename, cv::Mat* matrix);
// Unsigned char
template
int LoadMatFromBinTyped<uint8_t>(const std::string& filename, cv::Mat* matrix);
// Signed short
template
int LoadMatFromBinTyped<int16_t>(const std::string& filename, cv::Mat* matrix);
// Unsigned short
template
int LoadMatFromBinTyped<uint16_t>(const std::string& filename, cv::Mat* matrix);
// Signed int
template
int LoadMatFromBinTyped<int32_t>(const std::string& filename, cv::Mat* matrix);
// Signed float
template
int LoadMatFromBinTyped<float>(const std::string& filename, cv::Mat* matrix);
// Unsigned double
template
int LoadMatFromBinTyped<double>(const std::string& filename, cv::Mat* matrix);

/*
 *  @name   SaveMatToChar
 *  @brief  Save matrix to text file
 *  @param[in]  filename    Input filename
 *  @param[in]  matrix      Matrix where data will be loaded
 *  @return -1 if error, 0 otherwise
 */
int SaveMatToChar(const std::string& filename,const cv::Mat& matrix) {
  //Open stream
  int error = -1;
  std::ofstream out_file(filename.c_str(),std::ofstream::out);
  if (out_file.is_open()) {
    error = LTS5::WriteMatToChar(out_file, matrix);
    out_file.close();
  }
  return error;
}

/*
 *  @name   SaveMatToBin
 *  @brief  Save matrix to binary file
 *  @param[in]  filename    Input filename
 *  @param[in]  matrix      Matrix where data will be loaded
 *  @return -1 if error, 0 otherwise
 */
int SaveMatToBin(const std::string& filename,const cv::Mat& matrix) {
  //Open stream
  int error = -1;
  std::ofstream out_file(filename.c_str(),
                         std::ofstream::out|std::ofstream::binary);
  if (out_file.is_open()) {
    error = LTS5::WriteMatToBin(out_file, matrix);
    out_file.close();
  }
  return error;
}

/*
 *  @name   MatrixSetValue
 *  @brief  Set the value at position [row,col] according to the matrix depth
 *  @param[in]  row     Row index
 *  @param[in]  col     Col index
 *  @param[in]  value   Value
 *  @param[out] matrix  Destination matrix
 */
void MatrixSetValue(const int& row, const int& col, const double value,
                    cv::Mat* matrix) {
  assert(!matrix->empty());
  assert(matrix->channels() == 1);
  assert(row >= 0);
  assert(row < matrix->rows);
  assert(col >= 0);
  assert(col < matrix->cols);
  assert(matrix->depth() >= 0);
  assert(matrix->depth() <= 6);

  switch (matrix->depth()) {
    case CV_8U:
      matrix->at<uchar>(row, col) = static_cast<uchar>(value);
      break;
    case CV_8S:
      matrix->at<schar>(row, col) = static_cast<schar>(value);
      break;
    case CV_16U:
      matrix->at<ushort>(row, col) = static_cast<ushort>(value);
      break;
    case CV_16S:
      matrix->at<short>(row, col) = static_cast<short>(value);
      break;
    case CV_32S:
      matrix->at<int>(row, col) = static_cast<int>(value);
      break;
    case CV_32F:
      matrix->at<float>(row, col) = static_cast<float>(value);
      break;
    case CV_64F:
      matrix->at<double>(row, col) = static_cast<double>(value);
      break;
  }
}

/*
 *  @name   GetMatrixValue
 *  @brief  Get the value at position [row,col], cast the value according to
 *          the matrix depth
 *  @param[in]  row     Row index
 *  @param[in]  col     Col index
 *  @param[int] matrix  Matrix
 *  @return value
 */
double GetMatrixValue(const int& row, const int& col,
                      const cv::Mat& matrix) {
  assert(!matrix.empty());
  assert(matrix.channels() == 1);
  assert(row >= 0);
  assert(row < matrix.rows);
  assert(col >= 0);
  assert(col < matrix.cols);
  assert(matrix.depth() >= 0);
  assert(matrix.depth() <= 6);

  switch (matrix.type()) {
    case CV_8U:
      return matrix.at<uchar>(row, col);
    case CV_8S:
      return matrix.at<schar>(row, col);
    case CV_16U:
      return matrix.at<ushort>(row, col);
    case CV_16S:
      return matrix.at<short>(row, col);
    case CV_32S:
      return matrix.at<int>(row, col);
    case CV_32F:
      return matrix.at<float>(row, col);
    case CV_64F:
      return matrix.at<double>(row, col);
  }
  //Should not arrive here
  return -1.0;
}

/*
 *  @name   ReadMatFromCSV
 *  @brief  Load matrix of a given type from .csv file
 *  @param[in]  filename    File holding matrix data
 *  @param[out] matrix      Destination matrix
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int ReadMatFromCSV(const std::string& filename,
                   cv::Mat* matrix) {
  int error = -1;
  if (filename.rfind(".csv") != std::string::npos) {
    //Correct type -> can read file
    std::vector<std::vector<T>> data;
    std::ifstream input_stream(filename.c_str());
    while (input_stream.good()) {
      // Access data
      std::string line;
      if(!getline(input_stream, line).good()) {
        break;
      }
      // Line has been correctly read
      std::istringstream string_stream(line);
      std::vector<T> record;

      while(string_stream.good()) {
        std::string element;
        if (!getline(string_stream, element, ',').good()) {
          break;
        }
        // ok got element
        std::istringstream convert_stream(element);
        T value;
        if (!(convert_stream >> value)) {
          // Error clear vector
          data.clear();
          break;
        }
        // Conversion ok
        record.push_back(value);
      }
      // Add data
      data.push_back(record);
    }

    // Have reach the end of file ?
    if (input_stream.eof()) {
      // Yes -> Init matrix + fill
      int row = static_cast<int>(data.size());
      int col = static_cast<int>(data[0].size());
      int data_type = cv::DataType<T>::type;
      matrix->create(row, col, data_type);
      for (int r = 0 ; r < row; ++r) {
        std::vector<T> row_data = data[r];
        for (int c = 0; c < col; ++c) {
          matrix->at<T>(r,c) = row_data[c];
        }
      }
      // Done
      error = 0;
    }

  }
  return error;
}
template int LTS5_EXPORTS ReadMatFromCSV<int>(const std::string& filename,
                                              cv::Mat* matrix);
template int LTS5_EXPORTS ReadMatFromCSV<float>(const std::string& filename,
                                                cv::Mat* matrix);
template int LTS5_EXPORTS ReadMatFromCSV<double>(const std::string& filename,
                                                 cv::Mat* matrix);
/*
 *  @name   LoadPts
 *  @brief
 *  @param[in]  filename
 *  @param[in]  indices
 *  @param[out] data
 *  @return value
 */
void LoadPts(const std::string& filename,
             const std::unordered_set<int>& indices,
             cv::Mat& data) {
  std::ifstream is(filename.c_str());
  if (!is.good()) {
    return;
  }
  int nPoints = -1;
  int nIndices = -1;
  cv::MatIterator_<double> it;
  // First check pts format: ... n_points: [n_points] { ... }
  std::string line;
  std::stringstream ss;
  int idx=0;
  while (true) {
    std::getline(is, line);
    if (is.eof()) {
      break;
    }
    if (line.length() == 0) {
      continue;
    }
    ss.clear();
    ss.str(line);
    if (nPoints == -1) {
      std::string dummy;
      ss >> dummy;
      if (dummy == "n_points:") {
        ss >> nPoints;
      } else {
        continue;
      }
      assert(nPoints > 0);

      if(indices.empty()){
        assert(nPoints > 0);
        data.create(nPoints, 2, CV_64FC1);
      }
      else {
        nIndices = (int)indices.size();
        assert(nPoints > nIndices);
        data.create(nIndices, 2, CV_64FC1);
      }

      it = data.begin<double>();
    } else {
      if (line[0] == '{' || line[0] == '}') {
        continue;
      }

      if(indices.find(idx) != indices.end() || indices.empty()){
        double values[2];
        ss >> values[0] >> values[1];
        for (int i = 0; i < 2; i++) {
          *it++ = values[i];
        }
      }

      idx++;

      if (it == data.end<double>()) {
        break;
      }
    }
  }
  if (nPoints == -1) { // Then check pts format: 2 [n_points] ...
    is.clear();
    is.seekg(0, std::ios::beg);
    std::getline(is, line);
    if (line == "2") {
      std::getline(is, line);
      ss.clear();
      ss.str(line);
      std::stringstream ss(line);
      ss >> nPoints;
      assert(nPoints > 0);
      data.create(nPoints, 2, CV_64FC1);
      it = data.begin<double>();
      std::getline(is, line);
      ss.clear();
      ss.str(line);
      while (it != data.end<double>()) {
        // @TODO: read only a selection of points...
        ss >> *it++;
        assert(ss.good());
      }
    }
  }
  assert(it == data.end<double>());
  is.close();
  data = data.t();

  if(indices.empty()){
    data = data.reshape(data.channels(), 2 * nPoints);
  }
  else {
    data = data.reshape(data.channels(), 2 * nIndices);
  }
}

/*
 *  @name   ScanInFolder
 *  @brief  Scan in the give folder and return all files with given extensions
 *  @param[in]  directory  path where the files are located
 *  @param[in]  sorted     if true, sort the output
 *  @return vector of file names in string
 */
std::vector<std::string> ScanInFolder(const std::string& directory,
                                            std::vector<std::string>& exts,
                                            bool sorted) {
  std::vector<std::string> filePaths;

  tinydir_dir dir;
  tinydir_open(&dir, directory.c_str());

  while (dir.has_next)
  {
    tinydir_file file;
    tinydir_readfile(&dir, &file);

    if(file.is_reg && !file.is_dir)
    {
      std::string filename = std::string(file.name);
      std::string path = std::string(file.path);
      std::string ext = std::string(file.extension);

      for(size_t i=0; i<exts.size(); i++)
      {
        if(ext == exts[i])
        {
          filePaths.push_back(path);
          break;
        }
      }
    }

    tinydir_next(&dir);
  }

  tinydir_close(&dir);

  if(sorted)
    std::sort(filePaths.begin(), filePaths.end());

  return filePaths;
}

/*
 *  @name   ScanImagesInFolder
 *  @brief  Scan in the give folder and return all image files (unsorted)
 *  @param[in]  directory  path where the images are located
 *  @return vector of file names in string
 */
std::vector<std::string> ScanImagesInFolder(const std::string& directory) {

  std::vector<std::string> exts{"bmp", "jpg", "jpeg", "png", "pgm"};
  std::vector<std::string> imagePaths = ScanInFolder(directory, exts, false);

  return imagePaths;
}

/*
 *  @name   ScanImagesInFolder
 *  @brief  Scan in the give folder and return all image files
 *  @param[in]  directory  path where the images are located
 *  @param[in]  sorted     if true, sort the output
 *  @return vector of file names in string
 */
std::vector<std::string> ScanImagesInFolder(const std::string& directory,
                                            bool sorted) {

  std::vector<std::string> exts{"bmp", "jpg", "jpeg", "png", "pgm"};
  std::vector<std::string> imagePaths = ScanInFolder(directory, exts, sorted);
  return imagePaths;
}

/*
 *  @name   ScanVideosInFolder
 *  @brief  Scan in the give folder and return all video files (unsorted)
 *  @param[in]  directory  path where the videos are located
 *  @return vector of file names in string
 */
std::vector<std::string> ScanVideosInFolder(const std::string& directory) {

  std::vector<std::string> exts{"mp4", "avi"};
  std::vector<std::string> videoPaths = ScanInFolder(directory, exts, false);

  return videoPaths;
}

/*
 *  @name   ScanVideosInFolder
 *  @brief  Scan in the give folder and return all video files
 *  @param[in]  directory  path where the videos are located
 *  @param[in]  sorted     if true, sort the output
 *  @return vector of file names in string
 */
std::vector<std::string> ScanVideosInFolder(const std::string& directory,
                                            bool sorted) {

  std::vector<std::string> exts{"mp4", "avi"};
  std::vector<std::string> videoPaths = ScanInFolder(directory, exts, sorted);

  return videoPaths;
}

/*
 *  @name   ScanFoldersInFolder
 *  @fn std::vector<std::string> ScanFoldersInFolder(const std::string& directory)
 *  @brief  Scan in the give folder and return all folders inside (unsorted)
 *  @ingroup utils
 *  @param[in]  directory  path where the videos are located
 *  @return vector of folders names in string
 */
std::vector<std::string> ScanFoldersInFolder(const std::string& directory) {

  std::vector<std::string> folders;
  tinydir_dir dir;
  tinydir_open(&dir, directory.c_str());
  while (dir.has_next) {
    tinydir_file file;
    tinydir_readfile(&dir, &file);
    if(file.is_dir) {
      std::string name = std::string(file.name);
      std::string path = std::string(file.path);
      if (name != "." && name != "..") {
        folders.push_back(path);
      }
    }
    tinydir_next(&dir);
  }
  tinydir_close(&dir);
  return folders;
}

/*
 *  @name   LoadPts
 *  @brief
 *  @param[in]  filename
 *  @param[out] data
 *  @return value
 */
void LoadPts(const std::string& filename, cv::Mat& data) {
  std::ifstream is(filename.c_str());
  if (!is.good()) {
    return;
  }
  int nPoints = -1;
  cv::MatIterator_<double> it;
  // First check pts format: ... n_points: [n_points] { ... }
  std::string line;
  std::stringstream ss;
  int idx=0;
  while (true) {
    std::getline(is, line);
    if (is.eof()) {
      break;
    }
    if (line.length() == 0) {
      continue;
    }
    ss.clear();
    ss.str(line);
    if (nPoints == -1) {
      std::string dummy;
      ss >> dummy;
      if (dummy == "n_points:") {
        ss >> nPoints;
      } else {
        continue;
      }

      assert(nPoints > 0);
      data.create(nPoints, 2, CV_64FC1);

      it = data.begin<double>();
    } else {
      if (line[0] == '{' || line[0] == '}') {
        continue;
      }

      double values[2];
      ss >> values[0] >> values[1];
      for (int i = 0; i < 2; i++) {
        *it++ = values[i];
      }

      idx++;

      if (it == data.end<double>()) {
        break;
      }
    }
  }
  if (nPoints == -1) { // Then check pts format: 2 [n_points] ...
    is.clear();
    is.seekg(0, std::ios::beg);
    std::getline(is, line);
    if (line == "2") {
      std::getline(is, line);
      ss.clear();
      ss.str(line);
      std::stringstream ss(line);
      ss >> nPoints;
      assert(nPoints > 0);
      data.create(nPoints, 2, CV_64FC1);
      it = data.begin<double>();
      std::getline(is, line);
      ss.clear();
      ss.str(line);
      while (it != data.end<double>()) {
        // @TODO: read only a selection of points...
        ss >> *it++;
        assert(ss.good());
      }
    }
  }
  assert(it == data.end<double>());
  is.close();
  data = data.t();

  data = data.reshape(data.channels(), 2 * nPoints);
}


/*
 *  @name   SavePts
 *  @brief
 *  @param[in]  filename
 *  @param[in]  data
 *  @return value
 */
void SavePts(const std::string& filename, const cv::Mat& data) {
  std::ofstream os(filename.c_str());
  if (!os.good()) {
    return;
  }
  int nPoints = data.rows / 2;
  os << "n_points: " << nPoints << std::endl << "{" << std::endl;
  for (int i = 0; i < nPoints; i++) {
    os << data.at<double>(i, 0) << " " << data.at<double>(i + nPoints, 0) <<
    std::endl;
  }
  os << "}" << std::endl;
  os.close();
}

/*
 *  @name ScanStreamForObject
 *  @brief  Scan a given stream to find a certain type of object
 *  @param[in]  input_stream  Input binary stream
 *  @param[in]  object_tyle   Type of object to search for
 *  @return     0 if found, -1 otherwise (error / not found)
 */
int ScanStreamForObject(std::istream& input_stream,
                        const HeaderObjectType& object_type) {
  int error = -1;
  HeaderObjectType obj_type = static_cast<HeaderObjectType>(0);
  int obj_size = 0;
  while (input_stream.good() && obj_type != object_type) {
    input_stream.read(reinterpret_cast<char*>(&obj_type), sizeof(obj_type));
    input_stream.read(reinterpret_cast<char*>(&obj_size), sizeof(obj_size));
    if (obj_type != object_type) {
      input_stream.seekg(obj_size,std::ios_base::cur);
    }
  }
  if (obj_type == object_type) {
    error = 0;
  }
  return error;
}

/*
 *  @name   PrintStreamContent
 *  @brief  Print the content of a binary file
 *  @param[in]  input_stream  Binary stream to a file
 *  @return -1 if error, 0 otherwise
 */
int PrintStreamContent(std::istream& input_stream)  {
  int error = -1;
  if (input_stream.good()) {
    // Stream OK
    HeaderObjectType obj_type = static_cast<HeaderObjectType>(0);
    int obj_size = -1;
    while (input_stream.good()) {
      // Read obj type + size
      input_stream.read(reinterpret_cast<char*>(&obj_type), sizeof(obj_type));
      input_stream.read(reinterpret_cast<char*>(&obj_size), sizeof(obj_size));
      if (!input_stream.good()) {
        break;
      }
      switch (obj_type) {
        // SDM
        case HeaderObjectType::kSdm :
          std::cout << "Sdm" << std::endl;
          break;
        // SVM tracker quality assessment
        case HeaderObjectType::kSvmTrackerAssessment :
          std::cout << "Svm tracker quality assessment" << std::endl;
          break;
        // SVM tracker quality assessment training
        case HeaderObjectType::kSvmTrackerAssessmentTrainer :
          std::cout << "Svm tracker quality assessment training" << std::endl;
          break;
        // Const tracker quality assessment
        case HeaderObjectType::kConstTrackerAssessment :
          std::cout << "Const tracker quality assessment" << std::endl;
          break;
        // Const tracker quality assessment training
        case HeaderObjectType::kConstTrackerAssessmentTrainer :
          std::cout << "Const tracker quality assessment training" << std::endl;
          break;
        // PDM
        case HeaderObjectType::kPointDistributionModel :
          std::cout << "Point distribution model" << std::endl;
          break;
        // Binary linear SVM
        case HeaderObjectType::kBinaryLinearSvm :
          std::cout << "Linear binary SVM" << std::endl;
          break;
        // Emotion detector
        case HeaderObjectType::kExpressionDetector :
          std::cout << "Emotion detector" << std::endl;
          break;
        // Emotion analysis
        case HeaderObjectType::kExpressionAnalysis :
          std::cout << "Emotion analysis" << std::endl;
          break;
        // Random Fern
        case HeaderObjectType::kRandomFern :
          std::cout << "Random Fern" << std::endl;
          break;
        // Cascaded Random Fern
        case HeaderObjectType::kCascadedRandomFern :
          std::cout << "Cascaded Random Fern" << std::endl;
          break;
        // CPR Face tracker
        case HeaderObjectType::kCPRTracker :
          std::cout << "CPR Face tracker" << std::endl;
          break;
        // AU Detector
        case HeaderObjectType::kAUDetector :
          std::cout << "AU Detector" << std::endl;
          break;
        // AU Analysis
        case HeaderObjectType::kAUAnalysis :
          std::cout << "AU Analysis" << std::endl;
          break;
        // LBF face tracker
        case HeaderObjectType::kLBFTracker :
          std::cout << "LBF face tracker" << std::endl;
          break;
        // Binary Sparse Linear SVM
        case  HeaderObjectType::kBinarySparseLinearSvm :
          std::cout << "Binary Sparse Linear SVM" << std::endl;
          break;
        // LBF Tracker assessment
        case HeaderObjectType::kLBFTrackerAssessment :
          std::cout << "LBF Tracker assessment" << std::endl;
          break;
        // PCA Model
        case HeaderObjectType::kPCAModel :
          std::cout << "PCA Model" << std::endl;
          break;
        // 3D Statistical model
        case HeaderObjectType::kShapeModel :
          std::cout << "3D Statistical Model" << std::endl;
          break;
        // Statistical color model
        case HeaderObjectType::kColorModel :
          std::cout << "Statistical Color Model" << std::endl;
          break;
        // Statistical texture model
        case HeaderObjectType::kTextureModel :
          std::cout << "Statistical Texture Model" << std::endl;
          break;
        // Morphable model
        case HeaderObjectType::kMorphableModel :
          std::cout << "Morphable Model" << std::endl;
          break;
        // Color Morphable model
        case HeaderObjectType::kColorMorphableModel :
          std::cout << "Color Morphable Model" << std::endl;
          break;
        // Texture Morphable model
        case HeaderObjectType::kTextureMorphableModel :
          std::cout << "Texture Morphable Model" << std::endl;
          break;

        default:
          std::cout << "Unknown object type" << std::endl;
          error = -1;
          break;
      }
      input_stream.seekg(obj_size, std::ios_base::cur);
    }

  }
  return error;
}

/*
 *  Get a list with the name of the whole file into a specific directory
 *  @param[out] files   Returned value as a vector string
 *  @param[in]  dir     Full name the directory to check
 *  @param[in]  subdir  Subfolder name, initialise with ""
 *  @param[in]  ext     Extension file type, DAFAULT = ".jpg"
 */
void GetFileListing(std::vector<std::string> &files,
                    const std::string &dir,
                    const std::string &subdir,
                    const std::string &ext) {
  DIR *d;
  std::string aFolderName = std::string(dir);

  if (subdir.empty() == false) {
    // Add sub folder to the name
    if (aFolderName[aFolderName.length() - 1 ] != '/') {
      aFolderName += "/";
    }
    if (subdir[subdir.length() - 1] != '/') {
      aFolderName += subdir + "/";
    } else {
      aFolderName += subdir;
    }
  }

  if ((d = opendir(aFolderName.c_str())) == NULL) return;
  //if (dir.at(dir.length() - 1) != '/') dir += "/";// maybe without the "-1"

  std::string CapitalExt(ext.size(),'0');
  std::transform(ext.begin(),ext.end(),CapitalExt.begin(),toupper);

  struct dirent *dent;

  while ((dent = readdir(d)) != NULL) {
    if (std::string(dent->d_name) != "." &&
        std::string(dent->d_name) != "..") {
      // Check if it is a subfolder of a file
      if (dent->d_type == DT_DIR) {
        // Subfolder
        // std::cout << "subfolder" << std::endl;
        // Get files into the subfolder
        // Change folder
        GetFileListing(files, aFolderName,std::string(dent->d_name),ext);
      } else {
        // Get the name
        std::string filename = aFolderName + std::string(dent->d_name);

        // Check if there is an extension in the detected string
        unsigned long foundLower = filename.find(ext);
        unsigned long foundUpper = filename.find(CapitalExt);

        if (foundLower != std::string::npos ||
            foundUpper != std::string::npos) {
          // Extension found -> Add string to the list file
          files.push_back(filename);
        }
      }
    }
  }
  closedir(d);
}

/*
 *  @name   ReadNode
 *  @brief  Search a key in a given node and output its value if present
 *  @param[in]  node    Node to search in
 *  @param[in]  key     Key to search for
 *  @param[out] value   Value associated for the given key
 */
template<typename T>
void ReadNode(const cv::FileNode& node, const std::string& key, T* value) {
  if (!node[key].empty()) {
    node[key] >> *value;
  }
}
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, signed char* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, unsigned char* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, int* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, std::string* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, short* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, unsigned short* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, float* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, double* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, std::vector<signed char>* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, std::vector<unsigned char>* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, std::vector<int>* value);
//template
//void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, std::vector<std::string>* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, std::vector<short>* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, std::vector<unsigned short>* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, std::vector<float>* value);
template
void LTS5_EXPORTS ReadNode(const cv::FileNode& node, const std::string& key, std::vector<double>* value);

/*
 *  @name CompressMemoryBuffer
 *  @fn void CompressMemoryBuffer(void* input_buffer,
                                   const int input_size,
                                   std::vector<unsigned char>* compressed_buffer)
 *  @brief  Compress a given buffer
 *  @see  http://stackoverflow.com/questions/4538586/how-to-compress-a-buffer-with-zlib
 *  @param[in]  input_buffer      Input data to compress
 *  @param[in]  input_size        Input buffer size in bytes
 *  @param[out] compressed_buffer Compressed buffer
 *  @return -1 if error, 0 otherwise
 */
int CompressMemoryBuffer(const void* input_buffer,
                          const int input_size,
                          std::vector<unsigned char>* compressed_buffer) {
  int error = -1;
  // Internal buffer
  const int BUFF_SIZE = 16384;  // 16kb
  std::vector<unsigned char> buffer;
  unsigned char buff[BUFF_SIZE];
  // Compress stream
  z_stream strm;
  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;
  strm.opaque = Z_NULL;
  strm.next_in = reinterpret_cast<unsigned char*>(const_cast<void*>(input_buffer));
  strm.avail_in = input_size;
  strm.next_out = buff;
  strm.avail_out = BUFF_SIZE;
  // Init stream
  error = deflateInit(&strm, Z_BEST_COMPRESSION);
  int flush = -1;
  int n_byte = -1;
  if (error == Z_OK) {
    // Start compression
    do {
      flush = strm.avail_in == 0 ? Z_FINISH : Z_NO_FLUSH;
      // Run deflate on input buffer until output is full
      do {
        // Nothing in the output buffer
        strm.avail_out = BUFF_SIZE;
        strm.next_out = buff;
        // Deflate
        error = deflate(&strm, flush);    /* no bad return value */
        assert(error != Z_STREAM_ERROR);  /* state not clobbered */
        // how many byte have been processed ?
        n_byte = BUFF_SIZE - strm.avail_out;
        // Copy into internal buffer
        buffer.insert(buffer.end(), buff, buff + n_byte);
      } while (strm.avail_out == 0);
      assert(strm.avail_in == 0);     /* all input will be used */
      /* done when last data in file processed */
    } while (flush != Z_FINISH);
    assert(error == Z_STREAM_END);        /* stream will be complete */
  }
  /* clean up and return */
  (void)deflateEnd(&strm);
  // Swap buffer
  compressed_buffer->swap(buffer);
  // Return
  return Z_OK;
}

/*
 *  @name UncompressMemoryBuffer
 *  @fn void UncompressMemoryBuffer(const void* input_buffer,
                                   const int input_size,
                                   std::vector<unsigned char>* uncompressed_buffer)
 *  @brief  Uncompress a given buffer
 *  @see  http://www.zlib.net/zpipe.c
 *  @param[in]  input_buffer        Input data to uncompress
 *  @param[in]  input_size          Input buffer size in bytes
 *  @param[out] uncompressed_buffer Uncompressed buffer
 *  @return -1 if error, 0 otherwise
 */
int UncompressMemoryBuffer(const void* input_buffer,
                           const int input_size,
                           std::vector<unsigned char>* uncompressed_buffer) {
  int error = -1;
  // Internal buffer
  const int BUFF_SIZE = 16384;  // 16kb
  std::vector<unsigned char> buffer;
  unsigned char buff[BUFF_SIZE];
  /* allocate inflate state */
  z_stream strm;
  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;
  strm.opaque = Z_NULL;
  error = inflateInit(&strm);
  int n_byte = -1;
  if (error == Z_OK) {
    /* decompress until deflate stream ends or end of file */
    do {
      strm.avail_in = input_size;
      strm.next_in = reinterpret_cast<unsigned char*>(const_cast<void*>(input_buffer));
      /* run inflate() on input until output buffer not full */
      do {
        strm.avail_out = BUFF_SIZE;
        strm.next_out = buff;
        // Decompress
        error = inflate(&strm, Z_NO_FLUSH);
        assert(error != Z_STREAM_ERROR);  /* state not clobbered */
        // Handle error
        switch (error) {
          case Z_NEED_DICT:
            error = Z_DATA_ERROR;     /* and fall through */
          case Z_DATA_ERROR:
          case Z_MEM_ERROR:
            (void)inflateEnd(&strm);
            return error;
        }
        // Copy data to buffer
        n_byte = BUFF_SIZE - strm.avail_out;
        buffer.insert(buffer.end(), buff, buff + n_byte);
      } while (strm.avail_out == 0);
      /* done when inflate() says it's done */
    } while (error != Z_STREAM_END);
  }
  /* Done copy buffer */
  uncompressed_buffer->swap(buffer);
  /* clean up and return */
  (void)inflateEnd(&strm);
  return error == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

/*
 *  @name   WriteCompressedMatToBin
 *  @fn int WriteCompressedMatToBin(std::ostream& output_stream,const cv::Mat& matrix)
 *  @brief  Write compressed matrix to binary output stream
 *  @param[in]  output_stream   Output stream to filen
 *  @param[in]  matrix          Matrix to write
 *  @return -1 if error, number of bytes written otherwise
 */
int WriteCompressedMatToBin(std::ostream& output_stream,
                            const cv::Mat& matrix) {
  int error = -1;
  if (output_stream.good()) {
    // Write matrix dimension/type
    int dummy = matrix.type();
    output_stream.write(reinterpret_cast<const char*>(&dummy),
                        sizeof(dummy));
    output_stream.write(reinterpret_cast<const char*>(&matrix.rows),
                        sizeof(matrix.rows));
    output_stream.write(reinterpret_cast<const char*>(&matrix.cols),
                        sizeof(matrix.cols));
    // Compress data
    std::vector<unsigned char> data;
    int in_size = static_cast<int>(matrix.total() * matrix.elemSize());
    error = LTS5::CompressMemoryBuffer(reinterpret_cast<const void*>(matrix.data),
                                       in_size, &data);
    // Write size
    dummy = static_cast<int>(data.size());
    output_stream.write(reinterpret_cast<const char*>(&dummy),
                        sizeof(dummy));
    // Write data
    output_stream.write(reinterpret_cast<const char*>(data.data()),
                        dummy);
    // Sanity check
    error |= output_stream.good() ? 0 : -1;
  }
  return error;
}

/*
 *  @name   ReadCompressedMatFromBin
 *  @fn int ReadCompressedMatFromBin(std::istream& input_stream, cv::Mat* matrix)
 *  @brief  Load compressed matrix from binary stream
 *  @param[in]  input_stream    Stream to data file location
 *  @param[out] matrix          Matrix with data loaded.
 *  @return -1 if error, 0 otherwise
 */
int ReadCompressedMatFromBin(std::istream& input_stream,
                             cv::Mat* matrix)  {
  int error = -1;
  if (input_stream.good()) {
    int mat_type = -1;
    input_stream.read(reinterpret_cast<char*>(&mat_type), sizeof(mat_type));
    int row = -1;
    input_stream.read(reinterpret_cast<char*>(&row), sizeof(row));
    int col = -1;
    input_stream.read(reinterpret_cast<char*>(&col), sizeof(col));
    int comp_size = -1;
    input_stream.read(reinterpret_cast<char*>(&comp_size), sizeof(comp_size));
    error = input_stream.good() ? 0 : -1;
    // Read compressed data
    std::vector<unsigned char> compressed_data(comp_size, 0);
    input_stream.read(reinterpret_cast<char*>(compressed_data.data()),
                      comp_size);
    // Uncompress data
    matrix->create(row, col, mat_type);
    std::vector<unsigned char> uncompressed;
    error |= UncompressMemoryBuffer(reinterpret_cast<void*>(compressed_data.data()),
                                    comp_size,
                                    &uncompressed);
    std::memcpy(reinterpret_cast<void*>(matrix->data),
                reinterpret_cast<const void*>(uncompressed.data()),
                uncompressed.size());
  }
  return error;
}

}  // namespace LTS5
