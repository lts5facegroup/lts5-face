/**
 *  @file   colormap.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   20/06/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/utils/colormap.hpp"
#include "lts5/utils/colormap/jet.hpp"
#include "lts5/utils/colormap/gray.hpp"
#include "lts5/utils/colormap/winter.hpp"
#include "lts5/utils/colormap/summer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  ColorMap
 * @fn  ColorMap()
 * @brief Constructor
 */
template<typename T>
ColorMap<T>::ColorMap() : map_(jet_colormap),
                          map_length_(jet_colormap_length),
                          min_(0.0),
                          max_(1.0),
                          range_(1.0) {
}

/**
 * @name  ColorMap
 * @fn  ColorMap(const ColorMapType type, const T min, const T max)
 * @brief Constructor
 * @param[in] type  Type of colormap
 * @param[in] min   Minimum range value
 * @param[in] max   Maximum range value
 */
template<typename T>
ColorMap<T>::ColorMap(const ColorMapType& type,
        const T& min,
        const T& max) :
        min_(min),
        max_(max),
        range_(max_ - min_) {
  this->set_colormap_type(type);
}

#pragma mark -
#pragma mark Usage

/*
 * @name  PickColor
 * @fn  void PickColor(const T& value, Color* rgb)
 * @brief Pick color for a given value
 * @param[in]   value   Value to select color from
 * @param[out]  rgb     Corresponding color
 */
template<typename T>
void ColorMap<T>::PickColor(const T& value, Color* rgb) const {
  if (value < min_) {
    rgb->r_ = static_cast<T>(map_[0][0]);
    rgb->g_ = static_cast<T>(map_[0][1]);
    rgb->b_ = static_cast<T>(map_[0][2]);
  } else if (value > max_) {
    int end = static_cast<int>(map_length_ - T(1));
    rgb->r_ = static_cast<T>(map_[end][0]);
    rgb->g_ = static_cast<T>(map_[end][1]);
    rgb->b_ = static_cast<T>(map_[end][2]);
  } else {
    T v = T(map_length_ - 1) * (value - min_) / range_;
    int idx = std::round(v);
    rgb->r_ = static_cast<T>(map_[idx][0]);
    rgb->g_ = static_cast<T>(map_[idx][1]);
    rgb->b_ = static_cast<T>(map_[idx][2]);
  }
}

#pragma mark -
#pragma mark Accessors

/*
 * @name  set_colormap_type
 * @fn  void set_colormap_type(const ColorMapType type)
 * @brief Set colormap style
 * @param[in] type  Type of colormap
 */
template<typename T>
void ColorMap<T>::set_colormap_type(const ColorMapType& type) {
  switch (type) {
    default:
      std::cout << "Error, Unsupported type, set to defaut Jet" << std::endl;
    case kJet: {
      map_ = &jet_colormap[0];
      map_length_ = static_cast<T>(jet_colormap_length);
    }
      break;

    case kGrayscale: {
      map_ = &gray_colormap[0];
      map_length_ = static_cast<T>(gray_colormap_length);
    }
      break;

    case kWinter: {
      map_ = &winter_colormap[0];
      map_length_ = static_cast<T>(winter_colormap_length);
    }
      break;

    case kSummer: {
      map_ = &summer_colormap[0];
      map_length_ = static_cast<T>(summer_colormap_length);
    }
      break;
  }
}

/*
 * @name  set_range
 * @fn  void set_range(const T& min, const T& max)
 * @brief Set colormap range
 * @param[in] min Minimum range value
 * @param[in] max Maximum range value
 */
template<typename T>
void ColorMap<T>::set_range(const T& min, const T& max) {
  min_ = min;
  max_ = max;
  range_ = max_ - min_;
}

#pragma mark -
#pragma mark Declaration

/** Float */
template class ColorMap<float>;
/** Double */
template class ColorMap<double>;
/** Long long 64bits */
template class ColorMap<long long>;
/** Int 32bits */
template class ColorMap<int>;
/** short 16bits */
template class ColorMap<short>;

}  // namepsace LTS5
