/**
 *  @file   bit_array.hpp
 *  @brief  Bit array container
 *
 *  @author Christophe Ecabert
 *  @date   11/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/utils/bit_array.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/**
 * @name  BitArray
 * @fn    BitArray(const std::size_t& size)
 * @brief Constructor
 * @param[in] size Bit array size
 */
BitArray::BitArray(const std::size_t& size) : bit_array_(((size - 1) / 8) + 1, 0),
                                              size_(size) {
}

/*
 * @name  SetSize
 * @fn    void SetSize(const std::size_t& size)
 * @brief Set the size (i.e. number of bits) to store in the container
 *        Content will be erased
 * @param[in] size    Set array dimension
 */
void BitArray::SetSize(const std::size_t& size) {
  size_ = size;
  bit_array_.resize(((size - 1) / 8) + 1, 0);
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Size
 * @fn    std::size_t Size() const
 * @brief Provide bit array dimension
 * @return    Size
 */
std::size_t BitArray::Size() const {
  return size_;
}

/*
 * @name  Get
 * @fn    bool Get(const std::size_t n_bit)
 * @brief Get the value of a given bit
 * @param[in] n_bit Bit number to get
 * @return True if set, false otherwise
 */
bool BitArray::Get(const std::size_t& n_bit) const {
  return ((bit_array_[n_bit / 8] >> (n_bit % 8)) & 0x01u) == 0x01u;
}

/*
 * @name  Set
 * @fn    void Set(const std::size_t& n_bit)
 * @brief Set a given bit to 1.
 * @param[in] n_bit Bit number to set
 */
void BitArray::Set(const std::size_t& n_bit) {
  bit_array_[n_bit / 8] |= (0x01u << (n_bit % 8));
}

/*
 * @name  Reset
 * @fn    void Reset(const std::size_t n_bit)
 * @brief Clear a given bit to 0.
 * @param[in] n_bit Bit number to reset
 */
void BitArray::Reset(const std::size_t& n_bit) {
  bit_array_[n_bit / 8] &= ~(0x01u << (n_bit % 8));
}

/*
 * @name  Flip
 * @fn    void Flip(const std::size_t n_bit)
 * @brief Flip a given bit.
 * @param[in] n_bit Bit number to flip
 */
void BitArray::Flip(const std::size_t& n_bit) {
  bit_array_[n_bit / 8] ^= (0x01u << (n_bit % 8));
}
}  // namepsace LTS5