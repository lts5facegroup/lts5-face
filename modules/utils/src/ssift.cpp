/**
https://github.com/vlfeat/vlfeat/blob/master/vl/sift.c
The SIFT descriptor is a three dimensional histogram of the
position and orientation of the gradient.  There are NBP bins for
each spatial dimension and NBO bins for the orientation dimension,
for a total of NBP x NBP x NBO bins.

The support of each spatial bin has an extension of SBP = 3sigma
pixels, where sigma is the scale of the keypoint.  Thus all the
bins together have a support SBP x NBP pixels wide. Since
weighting and interpolation of pixel is used, the support extends
by another half bin. Therefore, the support is a square window of
SBP x (NBP + 1) pixels. Finally, since the patch can be
arbitrarily rotated, we need to consider a window 2W += sqrt(2) x
SBP x (NBP + 1) pixels wide.
*/


#include <numeric>
#include <iostream>

#ifdef __APPLE__
  #include <dispatch/dispatch.h>
#endif

#include <numeric>

#include "opencv2/core.hpp"
#include "opencv2/core/hal/hal.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/utils/ssift.hpp"
#include "lts5/utils/sys/parallel.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name   ComputeDescriptor
 *  @brief  Extract SSfit descriptor at location provided by key_points
 *  @param[in]  image           Input images
 *  @param[in]  key_points      Location of sift patches
 *  @param[in]  sift_param      Parameters for sift extraction
 *  @param[out] sift_features   Extracted descriptor
 */
void SSift::ComputeDescriptor(const cv::Mat& image,
                              const std::vector<cv::KeyPoint>& key_points,
                              const SSiftParameters& sift_param,
                              cv::Mat* sift_features) {
  //Put input image into correct format
  cv::Mat working_image;
  if (image.channels() == 3 || image.channels() == 4) {
    cv::cvtColor(image, working_image, cv::COLOR_BGR2GRAY);
    working_image.convertTo(working_image, cv::DataType<float>::type);
  } else if (image.type() != CV_32FC1){
    // Single channel, but not float
    image.convertTo(working_image, cv::DataType<float>::type);
  } else {
    working_image = image.clone();
  }
  //Apply filter
  float sigma_gaussian = sqrtf(std::max(sift_param.sift_sigma *
                                        sift_param.sift_sigma -
                                        sift_param.sift_init_sigma *
                                        sift_param.sift_init_sigma, 0.01f));
  cv::GaussianBlur(working_image, working_image, cv::Size(), sigma_gaussian,
                   sigma_gaussian);

  //Define output + histo + scale
  sift_features->create((int)key_points.size(), sift_param.sift_dim, CV_32FC1);
  float bins_per_rad = sift_param.sift_descr_hist_bins / 360.0f;
  float exp_scale = -1.f / (sift_param.sift_descr_width *
                            sift_param.sift_descr_width *
                            0.5f);
  int histogram_length = (sift_param.sift_descr_width + 2) *
                          (sift_param.sift_descr_width + 2) *
                          (sift_param.sift_descr_hist_bins + 2);

  Parallel::For(key_points.size(),
                [&](const size_t& p) {
    float* dst = sift_features->ptr<float>((int)p);
    float hist_width = sift_param.sift_descr_scl_fctr *
                       key_points[p].size *
                       sift_param.sift_size_scl * 0.5f;

    int radius = cvRound(hist_width * LTS5::math::constants::kSqrt2 *
                         (sift_param.sift_descr_width + 1) * 0.5f);
    int i, j, k, len = (radius * 2 + 1) * (radius * 2 + 1);

    cv::AutoBuffer<float> buf(len * 6 + histogram_length);
    float *X = buf;
    float *Y = X + len;
    float *Mag = Y;
    float *Ori = Mag + len;
    float *W = Ori + len;
    float *RBin = W + len;
    float *CBin = RBin + len;
    float *hist = CBin + len;

    for (int i = 0; i < histogram_length; ++i) {
      hist[i] = 0.f;
    }

    for (i = -radius, k = 0; i <= radius; i++) {
      for (j = -radius; j <= radius; j++) {
        float c_rot = (float)j / hist_width;
        float r_rot = (float)i / hist_width;
        float rbin = r_rot + sift_param.sift_descr_width / 2 - 0.5f;
        float cbin = c_rot + sift_param.sift_descr_width / 2 - 0.5f;
        int r = key_points[p].pt.y + i, c = key_points[p].pt.x + j;

        if (rbin > -1 && rbin < sift_param.sift_descr_width && cbin > -1 &&
            cbin < sift_param.sift_descr_width && r > 0 &&
            r < working_image.rows - 1 && c > 0 &&
            c < working_image.cols - 1) {
          float dx = (float)(working_image.at<float>(r, c + 1) -
                             working_image.at<float>(r, c - 1));
          float dy = (float)(working_image.at<float>(r - 1, c) -
                             working_image.at<float>(r + 1, c));
          X[k] = dx;
          Y[k] = dy;
          RBin[k] = rbin;
          CBin[k] = cbin;
          W[k] = (c_rot * c_rot + r_rot * r_rot) * exp_scale;
          k++;
        }
      }
    }
    len = k;
    //cv::fastAtan2(Y, X, Ori, len, true);
    cv::hal::fastAtan2(Y, X, Ori, len, true);
    //cv::magnitude(X, Y, Mag, len);
    cv::hal::magnitude(X, Y, Mag, len);
    //cv::exp(W, W, len);
    cv::hal::exp(W, W, len);

    for (k = 0; k < len; k++) {
      float rbin = RBin[k], cbin = CBin[k];
      float obin = Ori[k] * bins_per_rad;
      float mag = Mag[k] * W[k];

      int r0 = cvFloor(rbin);
      int c0 = cvFloor(cbin);
      int o0 = cvFloor(obin);
      rbin -= r0;
      cbin -= c0;
      obin -= o0;

      if (o0 < 0) {
        o0 += sift_param.sift_descr_hist_bins;
      }
      if (o0 >= sift_param.sift_descr_hist_bins) {
        o0 -= sift_param.sift_descr_hist_bins;
      }

      // Histogram update using tri-linear interpolation
      float v_r1 = mag * rbin, v_r0 = mag - v_r1;
      float v_rc11 = v_r1 * cbin, v_rc10 = v_r1 - v_rc11;
      float v_rc01 = v_r0 * cbin, v_rc00 = v_r0 - v_rc01;
      float v_rco111 = v_rc11 * obin, v_rco110 = v_rc11 - v_rco111;
      float v_rco101 = v_rc10 * obin, v_rco100 = v_rc10 - v_rco101;
      float v_rco011 = v_rc01 * obin, v_rco010 = v_rc01 - v_rco011;
      float v_rco001 = v_rc00 * obin, v_rco000 = v_rc00 - v_rco001;

      int idx = ((r0 + 1) * (sift_param.sift_descr_width + 2) + c0 + 1) *
                (sift_param.sift_descr_hist_bins + 2) + o0;
      hist[idx] += v_rco000;
      hist[idx + 1] += v_rco001;
      hist[idx + (sift_param.sift_descr_hist_bins + 2)] += v_rco010;
      hist[idx + (sift_param.sift_descr_hist_bins + 3)] += v_rco011;
      hist[idx + (sift_param.sift_descr_width + 2) *
                 (sift_param.sift_descr_hist_bins + 2)] += v_rco100;
      hist[idx + (sift_param.sift_descr_width + 2) *
                 (sift_param.sift_descr_hist_bins + 2) + 1] += v_rco101;
      hist[idx + (sift_param.sift_descr_width + 3) *
                 (sift_param.sift_descr_hist_bins + 2)] += v_rco110;
      hist[idx + (sift_param.sift_descr_width + 3) *
                 (sift_param.sift_descr_hist_bins + 2) + 1] += v_rco111;
    }

    // Finalize histogram, since the orientation histograms are circular
    for (i = 0; i < sift_param.sift_descr_width; i++) {
      for (j = 0; j < sift_param.sift_descr_width; j++) {
        int idx = ((i + 1) * (sift_param.sift_descr_width + 2) + (j + 1)) *
                  (sift_param.sift_descr_hist_bins + 2);
        hist[idx] += hist[idx + sift_param.sift_descr_hist_bins];
        hist[idx + 1] += hist[idx + sift_param.sift_descr_hist_bins + 1];
        for (k = 0; k < sift_param.sift_descr_hist_bins; k++) {
          dst[(i * sift_param.sift_descr_width + j) *
              sift_param.sift_descr_hist_bins + k] = hist[idx + k];
        }
      }
    }

    // Copy histogram to the descriptor
    // Apply hysteresis thresholding and scale the result
    float nrm2 = 0;
    len = sift_param.sift_descr_width *
          sift_param.sift_descr_width *
          sift_param.sift_descr_hist_bins;
    for (k = 0; k < len; k++) {
      nrm2 += dst[k] * dst[k];
    }
    float thr = std::sqrt(nrm2) * sift_param.sift_descr_mag_thr;
    for (i = 0, nrm2 = 0; i < k; i++) {
      float val = std::min(dst[i], thr);
      dst[i] = val;
      nrm2 += val * val;
    }
    nrm2 = sift_param.sift_int_descr_fctr / std::max(std::sqrt(nrm2),
                                                     FLT_EPSILON);

    for (k = 0; k < len; k++) {
      dst[k] = cv::saturate_cast<uchar>(dst[k] * nrm2);
    }

    // RootSift
    // R. Arandjelovic, A. Zisserman, Three things everyone should know
    // to improve object retrieval, CVPR'12
    // rootsift = sqrt(sift / sum(sift))
    if (sift_param.sift_root) {
      float sum = std::accumulate(dst, dst + len, FLT_EPSILON);
      for (int k = 0; k < len; k++) {
        dst[k] = std::sqrt(dst[k] / sum);
      }
    }
  });
}

static void createGaussianKernels( cv::Mat& kx,
                                   cv::Mat& ky,
                                   int type,
                                   cv::Size ksize,
                                   double sigma1,
                                   double sigma2 ) {
  int depth = CV_MAT_DEPTH(type);
  if( sigma2 <= 0 ) {
    sigma2 = sigma1;
  }
  // automatic detection of kernel size from sigma
  if( ksize.width <= 0 && sigma1 > 0 )
    ksize.width = cvRound(sigma1*(depth == CV_8U ? 3 : 4)*2 + 1)|1;
  if( ksize.height <= 0 && sigma2 > 0 )
    ksize.height = cvRound(sigma2*(depth == CV_8U ? 3 : 4)*2 + 1)|1;

  CV_Assert( ksize.width > 0 && ksize.width % 2 == 1 &&
             ksize.height > 0 && ksize.height % 2 == 1 );

  sigma1 = std::max( sigma1, 0.);
  sigma2 = std::max( sigma2, 0.);

  kx = cv::getGaussianKernel( ksize.width, sigma1, std::max(depth, CV_32F) );
  if( ksize.height == ksize.width && std::abs(sigma1 - sigma2) < DBL_EPSILON )
    ky = kx;
  else
    ky = cv::getGaussianKernel( ksize.height, sigma2, std::max(depth, CV_32F) );
}

/*
 *  @name   ComputeDescriptorOpt
 *  @brief  Extract SSfit descriptor at location provided by key_points
 *  @param[in]  image           Input images
 *  @param[in]  key_points      Location of sift patches
 *  @param[in]  sift_param      Parameters for sift extraction
 *  @param[out] sift_features   Extracted descriptor
 */
void SSift::ComputeDescriptorOpt(const cv::Mat& image,
                                 const std::vector<cv::KeyPoint>& key_points,
                                 const SSift::SSiftParameters& sift_param,
                                 cv::Mat* sift_features) {
  // Setup output
  sift_features->create((int)key_points.size(), sift_param.sift_dim, CV_32F);
  //Work for patch
  auto body = [&](const size_t& p) {
    //Create gaussian kernel
    float sigma_gaussian = sqrtf(std::max(sift_param.sift_sigma *
                                          sift_param.sift_sigma -
                                          sift_param.sift_init_sigma *
                                          sift_param.sift_init_sigma, 0.01f));
    cv::Mat kx, ky;
    createGaussianKernels(kx,
                          ky,
                          CV_32FC1,
                          cv::Size(),
                          sigma_gaussian,
                          sigma_gaussian);
    int f_size = std::max(kx.rows, kx.cols) * std::max(ky.rows, ky.cols);

    //Define output + histo + scale + radius
    float bins_per_rad = sift_param.sift_descr_hist_bins / 360.0f;
    float exp_scale = -1.f / (sift_param.sift_descr_width *
                              sift_param.sift_descr_width *
                              0.5f);
    int histogram_length = ((sift_param.sift_descr_width + 2) *
                            (sift_param.sift_descr_width + 2) *
                            (sift_param.sift_descr_hist_bins + 2));

    float hist_width = (sift_param.sift_descr_scl_fctr *
                        key_points[p].size *
                        sift_param.sift_size_scl * 0.5f);

    int radius = cvRound(hist_width * LTS5::math::constants::kSqrt2 *
                         (sift_param.sift_descr_width + 1) * 0.5f);

    //Define patch size according to filter size
    cv::Rect patch_roi;
    patch_roi.width = f_size + 2*(radius + 1) - 1;
    patch_roi.height = f_size + 2*(radius + 1) - 1;
    patch_roi.x = key_points[p].pt.x - patch_roi.width/2;
    patch_roi.y = key_points[p].pt.y - patch_roi.height/2;
    int patch_roi_center_x = patch_roi.width/2;
    int patch_roi_center_y = patch_roi.height/2;

    //Check for boundaries
    int left = patch_roi.x;
    int right = patch_roi.x + patch_roi.width;
    int top = patch_roi.y;
    int bottom = patch_roi.y + patch_roi.height;

    left = left < 0 ? 0 : left > image.cols ? image.cols : left;
    right = right < 0 ? 0 : right > image.cols ? image.cols : right;
    top = top < 0 ? 0 : top > image.rows ? image.rows : top;
    bottom = bottom < 0 ? 0 : bottom > image.rows ? image.rows : bottom;
    //Adjust center according new reference system
    patch_roi_center_x = key_points[p].pt.x - left;
    patch_roi_center_y = key_points[p].pt.y - top;
    //Define new boundaries
    patch_roi.x = left;
    patch_roi.width = right - left;
    patch_roi.y = top;
    patch_roi.height = bottom - top;
    //Extract image patch + convert to float type
    cv::Mat working_patch;
    if (image.channels() != 1) {
      cv::cvtColor(image(patch_roi), working_patch, cv::COLOR_BGR2GRAY);
      working_patch.convertTo(working_patch, cv::DataType<float>::type);
    } else {
      image(patch_roi).convertTo(working_patch, cv::DataType<float>::type);
    }
    //Filter the image
    cv::sepFilter2D(working_patch, working_patch, CV_32FC1, kx, ky);
    //Compute Sift descriptor
    auto* dst = sift_features->ptr<float>((int)p);
    int len = (radius * 2 + 1) * (radius * 2 + 1);
    cv::AutoBuffer<float> buf(len * 6 + histogram_length);
    float *X = buf;
    float *Y = X + len;
    float *Mag = Y;
    float *Ori = Mag + len;
    float *W = Ori + len;
    float *RBin = W + len;
    float *CBin = RBin + len;
    float *hist = CBin + len;

    for (int i = 0; i < histogram_length; ++i) {
      hist[i] = 0.f;
    }
    int k = 0;
    for (int i = -radius; i <= radius; i++) {
      for (int j = -radius; j <= radius; j++) {
        float c_rot = (float)j / hist_width;
        float r_rot = (float)i / hist_width;
        float rbin = r_rot + sift_param.sift_descr_width / 2 - 0.5f;
        float cbin = c_rot + sift_param.sift_descr_width / 2 - 0.5f;
        int r = patch_roi_center_y + i, c = patch_roi_center_x + j;

        if (rbin > -1 && rbin < sift_param.sift_descr_width && cbin > -1 &&
            cbin < sift_param.sift_descr_width && r > 0 &&
            r < working_patch.rows - 1 && c > 0 &&
            c < working_patch.cols - 1) {
          auto dx = (float)(working_patch.at<float>(r, c + 1) -
                             working_patch.at<float>(r, c - 1));
          auto dy = (float)(working_patch.at<float>(r - 1, c) -
                             working_patch.at<float>(r + 1, c));
          X[k] = dx;
          Y[k] = dy;
          RBin[k] = rbin;
          CBin[k] = cbin;
          W[k] = (c_rot * c_rot + r_rot * r_rot) * exp_scale;
          k++;
        }
      }
    }

    len = k;
    //cv::fastAtan2(Y, X, Ori, len, true);
    cv::hal::fastAtan2(Y, X, Ori, len, true);
    //cv::magnitude(X, Y, Mag, len);
    cv::hal::magnitude(X, Y, Mag, len);
    //cv::exp(W, W, len);
    cv::hal::exp(W, W, len);
    for (k = 0; k < len; k++) {
      float rbin = RBin[k], cbin = CBin[k];
      float obin = Ori[k] * bins_per_rad;
      float mag = Mag[k] * W[k];

      int r0 = cvFloor(rbin);
      int c0 = cvFloor(cbin);
      int o0 = cvFloor(obin);
      rbin -= r0;
      cbin -= c0;
      obin -= o0;

      if (o0 < 0) {
        o0 += sift_param.sift_descr_hist_bins;
      }
      if (o0 >= sift_param.sift_descr_hist_bins) {
        o0 -= sift_param.sift_descr_hist_bins;
      }

      // Histogram update using tri-linear interpolation
      float v_r1 = mag * rbin, v_r0 = mag - v_r1;
      float v_rc11 = v_r1 * cbin, v_rc10 = v_r1 - v_rc11;
      float v_rc01 = v_r0 * cbin, v_rc00 = v_r0 - v_rc01;
      float v_rco111 = v_rc11 * obin, v_rco110 = v_rc11 - v_rco111;
      float v_rco101 = v_rc10 * obin, v_rco100 = v_rc10 - v_rco101;
      float v_rco011 = v_rc01 * obin, v_rco010 = v_rc01 - v_rco011;
      float v_rco001 = v_rc00 * obin, v_rco000 = v_rc00 - v_rco001;

      int idx = ((r0 + 1) * (sift_param.sift_descr_width + 2) + c0 + 1) *
                (sift_param.sift_descr_hist_bins + 2) + o0;

      hist[idx] += v_rco000;
      hist[idx + 1] += v_rco001;
      hist[idx + (sift_param.sift_descr_hist_bins + 2)] += v_rco010;
      hist[idx + (sift_param.sift_descr_hist_bins + 3)] += v_rco011;
      hist[idx + (sift_param.sift_descr_width + 2) *
                 (sift_param.sift_descr_hist_bins + 2)] += v_rco100;
      hist[idx + (sift_param.sift_descr_width + 2) *
                 (sift_param.sift_descr_hist_bins + 2) + 1] += v_rco101;
      hist[idx + (sift_param.sift_descr_width + 3) *
                 (sift_param.sift_descr_hist_bins + 2)] += v_rco110;
      hist[idx + (sift_param.sift_descr_width + 3) *
                 (sift_param.sift_descr_hist_bins + 2) + 1] += v_rco111;
    }
    // Finalize histogram, since the orientation histograms are circular
    for (int i = 0; i < sift_param.sift_descr_width; i++) {
      for (int j = 0; j < sift_param.sift_descr_width; j++) {
        int idx = ((i + 1) * (sift_param.sift_descr_width + 2) + (j + 1)) *
                  (sift_param.sift_descr_hist_bins + 2);
        hist[idx] += hist[idx + sift_param.sift_descr_hist_bins];
        hist[idx + 1] += hist[idx + sift_param.sift_descr_hist_bins + 1];
        for (k = 0; k < sift_param.sift_descr_hist_bins; k++) {
          dst[(i * sift_param.sift_descr_width + j) *
              sift_param.sift_descr_hist_bins + k] = hist[idx + k];
        }
      }
    }
    // Copy histogram to the descriptor
    // Apply hysteresis thresholding and scale the result
    float nrm2 = 0;
    len = sift_param.sift_descr_width *
          sift_param.sift_descr_width *
          sift_param.sift_descr_hist_bins;
    for (k = 0; k < len; k++) {
      nrm2 += dst[k] * dst[k];
    }
    float thr = std::sqrt(nrm2) * sift_param.sift_descr_mag_thr;
    nrm2 = 0.f;
    for (int i = 0; i < k; i++) {
      float val = std::min(dst[i], thr);
      dst[i] = val;
      nrm2 += val * val;
    }
    nrm2 = sift_param.sift_int_descr_fctr / std::max(std::sqrt(nrm2),
                                                     FLT_EPSILON);
    for (k = 0; k < len; k++) {
      dst[k] = cv::saturate_cast<uchar>(dst[k] * nrm2);
    }
    // RootSift
    // R. Arandjelovic, A. Zisserman, Three things everyone should know
    // to improve object retrieval, CVPR'12
    // rootsift = sqrt(sift / sum(sift))
    if (sift_param.sift_root) {
      float sum = std::accumulate(dst, dst + len, FLT_EPSILON);
      for (k = 0; k < len; k++) {
        dst[k] = std::sqrt(dst[k] / sum);
      }
    }
  };
  // Call loop
  Parallel::For(key_points.size(), body);
}
}  // namespace LTS5
