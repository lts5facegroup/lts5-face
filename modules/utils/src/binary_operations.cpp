/**
 *  @file   binary_operations.cpp
 *  @brief  Operations to swap between big and small endian
 *
 *  @author Marina Zimmermann
 *  @date   03/10/16
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5
{

/**
 *  @name   ShortSwap
 *  @fn short ShortSwap(short s)
 *  @brief  Swap a short int
 *  @param[in]  s   Short int
 *  @return swapped short
 */
short ShortSwap (short s)
{
  unsigned char b1, b2;

  b1 = s & 0xFF;
  b2 = (s >> 0x8) & 0xFF;

  return (b1 << 0x8) + b2;
}

/**
 *  @name   LongSwap
 *  @fn int LongSwap(int s)
 *  @brief  Swap a long int
 *  @param[in]  i   Long int
 *  @return swapped int
 */
int LongSwap (int i)
{
  unsigned char b1, b2, b3, b4;

  b1 = i & 0xFF;
  b2 = (i >> 0x8) & 0xFF;
  b3 = (i >> 0x10) & 0xFF;
  b4 = (i >> 0x18) & 0xFF;

  return ((int)b1 << 0x18) + ((int)b2 << 0x10) + ((int)b3 << 0x8) + b4;
}

/**
 *  @name   FloatSwap
 *  @fn int FloatSwap(float f)
 *  @brief  Swap a float
 *  @param[in]  f   Float
 *  @return swapped float
 */
float FloatSwap (float f)
{
  union
  {
    float f;
    unsigned char b[4];
  } dat1, dat2;

  dat1.f = f;
  dat2.b[0] = dat1.b[3];
  dat2.b[1] = dat1.b[2];
  dat2.b[2] = dat1.b[1];
  dat2.b[3] = dat1.b[0];
  return dat2.f;
}

} // LTS5 namespace

