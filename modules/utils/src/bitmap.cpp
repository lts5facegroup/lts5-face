/**
 *  @file   bitmap.cpp
 *  @brief  Image interface helper
 *
 *  @author Christophe Ecabert
 *  @date   15/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <cstdlib>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/utils/bitmap.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

  /*
   *  @name Bitmap
   *  @fn Bitmap(const int width,
                 const int height,
                 Format format,
                 const unsigned char* data)
   *  @brief  Create bitmap image of a given size and format
   *  @param[in]  width   Image width
   *  @param[in]  height  Image height
   *  @param[in]  format  Image format
   *  @param[in]  data    Image raw data
   */
  Bitmap::Bitmap(const int width,
                 const int height,
                 Format format,
                 const unsigned char* data) : data_(nullptr) {
    this->Set(width, height, format, data);
  }

  /*
   *  @name   ~Bitmap
   *  @fn ~Bitmap()
   *  @brief  Destrcutor
   */
  Bitmap::~Bitmap() {
    if (data_) {
      free(data_);
    }
  }

  /*
   *  @name Bitmap
   *  @fn Bitmap(const Bitmap& other)
   *  @brief  Copy constructor
   *  @param[in]  other Bitmap to copy
   */
  Bitmap::Bitmap(const Bitmap& other) : data_(nullptr) {
    this->Set(other.width_, other.height_, other.format_, other.data_);
  }

  /*
   *  @name operator=
   *  @fn Bitmap& operator=(const Bitmap& rhs)
   *  @brief  Assignment constructor
   *  @param[in]  rhs Right hand sign member
   *  @return Assigned bitmap
   */
  Bitmap& Bitmap::operator=(const Bitmap& rhs) {
    this->Set(rhs.width_, rhs.height_, rhs.format_, rhs.data_);
    return *this;
  }

  /*
   *  @name LoadFromFile
   *  @fn
   *  @brief  Helper function to load bitmap from file
   *  @param[in]  filename  Path to the image file
   *  @return Created Bitmap
   */
  Bitmap Bitmap::LoadFromFile(const std::string& filename) {
    int width = -1, height = -1, channels = -1;
    cv::Mat mat = cv::imread(filename);
    if (!mat.empty()) {
      if (mat.channels() == 3) {
        cv::cvtColor(mat, mat, cv::COLOR_BGR2RGBA);
      } else if (mat.channels() == 1) {
        cv::cvtColor(mat, mat, cv::COLOR_GRAY2BGRA);
      }
      width = mat.cols;
      height = mat.rows;
      channels = mat.channels();
      return Bitmap(width, height, static_cast<Format>(channels), mat.data);
    }
    return Bitmap();
  }

  /*
   *  @name Set
   *  @fn void Set(const int width,
                   const int height,
                   Format format,
                   const unsigned char* data)
   *  @brief  Common constructor
   *  @param[in]  width   Image width
   *  @param[in]  height  Image height
   *  @param[in]  format  Image format
   *  @param[in]  data    Image raw data
   */
  void Bitmap::Set(const int width,
                   const int height,
                   Format format,
                   const unsigned char* data) {
    assert(width > 0 && height > 0 && (format > 0 && format < 5));
    // Define image property
    width_ = width;
    height_ = height;
    format_ = format;
    // Allocate buffer properly
    size_t n_size = width_ * height_ * format_;
    if (data_) {
      // Need to realloc
      data_ = static_cast<unsigned char*>(realloc(reinterpret_cast<void*>(data_),
                                                  n_size));
    } else {
      // New allocation
      data_ = static_cast<unsigned char*>(malloc(n_size));
    }
    // Copy data
    if (data_) {
      memcpy(reinterpret_cast<void*>(data_),
             reinterpret_cast<const void*>(data),
             n_size);
    }
  }
}  // namespace LTS5





