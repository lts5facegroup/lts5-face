/**
 *  @file   timestamp.cpp
 *  @brief  TimeStamp helper class
 *
 *  @author Christophe Ecabert
 *  @date   02/08/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/utils/timestamp.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma Initialization

/*
 * @name  TimeStamp
 * @fn    TimeStamp(void)
 * @brief Constructor
 */
template<typename DurationT, class ClockT>
TimeStamp<DurationT, ClockT>::TimeStamp() {
  t_ = ClockT::now();
}

#pragma mark -
#pragma Usage

/*
 * @name  ConvertToString
 * @fn    void ConvertToString(std::string* str)
 * @brief Convert timestamp into readable string
 * @param str Converted string
 */
template<typename DurationT, class ClockT>
void TimeStamp<DurationT, ClockT>::ConvertToString(std::string* str) {
  std::time_t tt = ClockT::to_time_t(t_);
  *str = std::ctime(&tt);
};

#pragma mark -
#pragma Operator

/*
 * @name  operator-
 * @fn DurationT operator-(const TimeStamp& rhs) const
 * @brief Compute time difference in a given unit
 * @param rhs Second timestamp
 * @return Time elapsed between the two timestamps
 */
template<typename DurationT, class ClockT>
DurationT TimeStamp<DurationT, ClockT>::operator-(const TimeStamp& rhs) const {
  return std::chrono::duration_cast<DurationT>(rhs.t_ - t_);
}

/*
 * @name  operator>
 * @fn    bool operator>(const TimeStamp& rhs)
 * @brief Compare if the timestamp is greater than \p rhs
 * @param t1  rhs Second timestamp
 * @return True if the current timestamp is greater than \p rhs, false
 *         otherwise
 */
template<typename DurationT, class ClockT>
bool TimeStamp<DurationT, ClockT>::operator>(const TimeStamp& rhs) const {
  return t_ > rhs.t_;
}

/*
 * @name  operator<
 * @fn    bool operator<(const TimeStamp& rhs)
 * @brief Compare if the timestamp is lower than \p rhs
 * @param t1  rhs Second timestamp
 * @return True if the current timestamp is lower than \p rhs, false
 *         otherwise
 */
template<typename DurationT, class ClockT>
bool TimeStamp<DurationT, ClockT>::operator<(const TimeStamp& rhs) const {
  return t_ < rhs.t_;
}

#pragma mark -
#pragma Declaration

/** TimeStamp - Milliseconds/system clock */
template class TimeStamp<std::chrono::milliseconds, std::chrono::system_clock>;
/** TimeStamp - Milliseconds/system clock */
template class TimeStamp<std::chrono::seconds, std::chrono::system_clock>;

}  // namespace LTS5