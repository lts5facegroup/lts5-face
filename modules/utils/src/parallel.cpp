/**
 *  @file   parallel.cpp
 *  @brief  Multi-threading abstraction
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   10/31/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#if defined(USE_TBB)
#include <mutex>
#include "tbb/parallel_for.h"
#elif defined(__APPLE__)
#include <dispatch/dispatch.h>
#include <mutex>
#elif defined(USE_OPENMP)
#include <omp.h>
#else
#include <thread>
#include <mutex>
#endif

#include "lts5/utils/sys/parallel.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @enum
 * @brief Possible threading framework
 */
enum ThreadingType {
  /** TBB */
  kTBB,
  /** OpenMP */
  kOpenMP,
  /** Apple - GCD */
  kApple,
  /** STL */
  kSTL
};

/**
 * @struct  LockType
 * @brief   Automatic lock type selection
 * @tparam T    Type of threading framework
 */
template<ThreadingType T>
struct LockType;

template<ThreadingType T>
class MutexImplBase;

#if defined(USE_TBB)
/** TBB Specialization */
template<> struct LockType<kTBB> {
  using type = std::mutex;
};

template <> class MutexImplBase<kTBB> {
 public:
  /** Destructor */
  virtual ~MutexImplBase() = default;
  /** Acquire lock */
  void Lock() {
    lock_.lock();
  }
  /** Release lock */
  void Unlock() {
    lock_.unlock();
  }

 private:
  using lock_t = typename LockType<kTBB>::type;
  lock_t lock_;
};

using MutexImplType = MutexImplBase<kTBB>;

#elif defined(USE_OPENMP)
/** OpenMP Specialization */
template<> struct LockType<kOpenMP> {
  using type = omp_lock_t;
};

template <> class MutexImplBase<kOpenMP> {
 public:
  /** Constructor */
  MutexImplBase() {
    omp_init_lock(&lock_);
  }
  /** Destructor */
  virtual ~MutexImplBase() {
    omp_destroy_lock(&lock_);
  }
  /** Acquire lock */
  void Lock() {
    omp_set_lock(&lock_);
  }
  /** Release lock */
  void Unlock() {
    omp_unset_lock(&lock_);
  }

 private:
  using lock_t = typename LockType<kOpenMP>::type;
  lock_t lock_;
};

using MutexImplType = MutexImplBase<kOpenMP>;

#elif defined(__APPLE__)
/** Processing queue */
struct Queue {
  /** Constructor */
  Queue() {
    obj = dispatch_queue_create("lts5.processing.queu",
                                DISPATCH_QUEUE_CONCURRENT);
  }
  /** Destructor */
  ~Queue() {
    dispatch_release(obj);
  }
  
  /** Queue object */
  dispatch_queue_t obj;
};
// Queue object
static Queue processing_queue;
  
// TODO(christophe): For sor reason using `dispatch_semaphore_t` as a lock is
// super slow, therefore use std::mutex instead
template<> struct LockType<kApple> {
  using type = std::mutex; // dispatch_semaphore_t;
};
  
template <> class MutexImplBase<kApple> {
 public:
  /** Constructor */
  MutexImplBase() {
    // lock_ = dispatch_semaphore_create(1);
  }
  /** Destructor */
  virtual ~MutexImplBase() {
    // dispatch_release(lock_);
  }
  /** Acquire lock */
  void Lock() {
    //dispatch_semaphore_wait(lock_, DISPATCH_TIME_FOREVER);
    lock_.lock();
  }
  /** Release lock */
  void Unlock() {
    // dispatch_semaphore_signal(lock_);
    lock_.unlock();
  }
 private:
  using lock_t = typename LockType<kApple>::type;
  lock_t lock_;
};
using MutexImplType = MutexImplBase<kApple>;

#else
/** STL Specialization */
template<> struct LockType<kSTL> {
  using type = std::mutex;
};

template <> class MutexImplBase<kSTL> {
 public:
  /** Constructor */
  MutexImplBase() = default;
  /** Destructor */
  virtual ~MutexImplBase() = default;
  /** Acquire lock */
  void Lock() {
    lock_.lock();
  }
  /** Release lock */
  void Unlock() {
    lock_.unlock();
  }

 private:
  using lock_t = typename LockType<kSTL>::type;
  lock_t lock_;
};

using MutexImplType = MutexImplBase<kSTL>;
#endif

class MutexImpl : public MutexImplType {
 public:
  /** Constructor */
  MutexImpl() = default;

  ~MutexImpl() override = default;
};

#pragma mark -
#pragma mark Mutex abstraction

/*
 * @name  Mutex
 * @fn    Mutex()
 * @brief Constructor
 */
Mutex::Mutex() : lock_(new MutexImpl()) {}

/*
 * @name  ~Mutex
 * @fn    ~Mutex()
 * @brief Destructor
 */
Mutex::~Mutex() {
  delete lock_;
}

/*
 * @name  Lock
 * @fn    void Lock()
 * @brief Acquire lock
 */
void Mutex::Lock() {
  lock_->Lock();
}

/*
 * @name  Unlock
 * @fn    void Unlock()
 * @brief Release lock
 */
void Mutex::Unlock() {
  lock_->Unlock();
}

#pragma mark -
#pragma mark Multi-threading function
  
/*
 * @name  For
 * @fn static void For(const size_t& n, std::function<void(const size_t&)>&& body)
 * @brief Execute `body` in parallel fashion
 * @param[in] n   Number of time body needs to be run
 * @param[in,out] body    Body of for loop
 */
void Parallel::For(const size_t& n,
                   std::function<void(const size_t&)>&& body) {
  // Switch to old behaviours
#if defined(USE_TBB)
  // Use TBB as multithreading framework
  tbb::parallel_for(tbb::blocked_range<size_t>(0, n),
                    [&](const tbb::blocked_range<size_t>& range) {
                      for (auto k = range.begin(); k != range.end(); ++k) {
                        body(k);
                      }
                    });
#elif defined(USE_OPENMP)
#pragma omp parallel for
  for (size_t k = 0; k < n; ++k) {
    body(k);
  }
#elif defined(__APPLE__)
  dispatch_apply(n,
                 processing_queue.obj, ^(size_t k) {
    body(k);
  });
#else
  // Use standard for loop
  for (size_t k = 0; k < n; ++k) {
    body(k);
  }
#endif
}
}  // namespace LTS5
