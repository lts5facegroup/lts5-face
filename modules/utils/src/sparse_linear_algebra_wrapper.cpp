/**
 *  @file   sparse_linear_algebra_wrapper.cpp
 *  @brief
 *
 *  @author Gabriel Cuendet
 *  @date   12.09.16
 *  Copyright © 2016 Gabriel Cuendet. All rights reserved.
 */

#include <algorithm>
#include <vector>

#ifdef HAS_ARPACKPP
  #include "arlnsmat.h"
  #include "arlsnsym.h"
#endif

#include "lts5/utils/math/sparse_linear_algebra_wrapper.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  SparseLinearAlgebra
 *  @brief      Subspace for sparse linear algebra function
 */
namespace SparseLinearAlgebra {

/*
 * @name LuNonSymEigShift
 * @fn LuNonSymEigShift()
 * @brief Constructor
 */
template<typename T>
LuNonSymEigShift<T>::LuNonSymEigShift() : BWM_(nullptr), prob_(nullptr) {}

/*
 *  @name   ~LuNonSymEigShift
 *  @fn ~LuNonSymEigShift()
 *  @brief  Destructor
 */
template<typename T>
LuNonSymEigShift<T>::~LuNonSymEigShift() {
#ifdef HAS_ARPACKPP
  if (BWM_ != nullptr) {
    delete BWM_;
  }
  if (prob_ != nullptr) {
    delete prob_;
  }
#endif
}

/*
 * @name  get_params
 * @fn  const Arpack::non_sym_eigen_params<T>& get_params() const;
 * @brief Getter for the eigenvalue decomposition parameters
 * @return Const reference to the parameters
 */
template<typename T>
const Arpack::non_sym_eigen_params<T>& LuNonSymEigShift<T>::get_params() const {
  return eig_params_;
}

/*
 * @name  get_params
 * @fn  Arpack::non_sym_eigen_params<T>& get_params();
 * @brief Getter for the eigenvalue decomposition parameters
 * @return Reference to the parameters
 */
template<typename T>
Arpack::non_sym_eigen_params<T>& LuNonSymEigShift<T>::get_params() {
  return eig_params_;
}

/*
 * @name Init
 * @fn void Init(const Eigen::SparseMatrix<T>& A)
 * @brief Initialize the computation of the wigenvalues/vectors of A
 * @param A the matrix to decompose into eigenvectors
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int LuNonSymEigShift<T>::Init(const Eigen::SparseMatrix<T>& A) {
  int err = -1;
#ifdef HAS_ARPACKPP
  eig_params_.n = A.cols();
  eig_params_.nnz = A.nonZeros();
  eig_params_.irow = const_cast<int*>(A.innerIndexPtr());
  eig_params_.pcol = const_cast<int*>(A.outerIndexPtr());
  eig_params_.A = const_cast<T*>(A.valuePtr());

  if (BWM_) {
    delete BWM_;
  }

  BWM_ = new ARluNonSymMatrix<T, T>(eig_params_.n, eig_params_.nnz,
                                    eig_params_.A, eig_params_.irow,
                                    eig_params_.pcol);
  if (prob_) {
    delete prob_;
  }
  if (eig_params_.enable_shift_inverse) {
    prob_ = new ARluNonSymStdEig<T>(eig_params_.nevp, *BWM_, eig_params_.fs,
                                    eig_params_.whichp, eig_params_.ncvp);
  } else {
    prob_ = new ARluNonSymStdEig<T>(eig_params_.nevp, *BWM_,
                                    eig_params_.whichp, eig_params_.ncvp);
  }
  err = 0;
#endif
  return err;
}

/*
 * @name  ChangeFs
 * @fn  void ChangeFs(const T& fs)
 * @brief Change the frequency fs
 * @param[in]  new fs frequency
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int LuNonSymEigShift<T>::ChangeFs(const T& fs) {
  int err = -1;
#ifdef HAS_ARPACKPP
  eig_params_.fs = fs;
  if (prob_) {
    prob_->ChangeShift(fs);
    err = 0;
  }
#endif
  return err;
}

/*
 *  @name Compute
 *  @fn inline void Compute(const Eigen::SparseMatrix<T>& A)
 *  @brief  Call arpack library for decompostion
 *  @param[in] A matrix to decompose
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int LuNonSymEigShift<T>::Compute() {
  int err = -1;
#ifdef HAS_ARPACKPP
  // Finding eigenvalues and eigenvectors.
  // Use ARPACKPP wrapper to arpack
  int n_converged = prob_->FindEigenvectors();
  if (n_converged != eig_params_.nevp) {
    LTS5_LOG_ERROR("Not all values converged!");
  } else {
    err = 0;
  }
#endif
  return err;
}

/*
 *  @name   GetEigenvalues
 *  @fn inline int GetEigenvalues(EVector* eigenvalues)
 *  @brief  Provide access to eigen values
 *  @param[out] eigenvalues in no particular order
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int LuNonSymEigShift<T>::GetEigenvalues(EVector* eigenvalues) {
  int error = -1;
#ifdef HAS_ARPACKPP
  if (prob_->EigenvaluesFound()) {
    int nconv = prob_->ConvergedEigenvalues();
    eigenvalues->resize(nconv, 1);
    error = 0;
    for (int i = 0; i < nconv; ++i) {
      (*eigenvalues)[i] = prob_->EigenvalueReal(i);
    }
  }
#endif
  return error;
}

/*
 *  @name   GetSortedEigenvalues
 *  @fn inline int GetSortedEigenvalues(EVector* eigenvalues)
 *  @brief  Provide access to sorted eigen values
 *  @param[out] eigenvalues in increasing order of magnitude (ABS VALUE)
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int LuNonSymEigShift<T>::GetSortedEigenvalues(EVector* eigenvalues) {
  int error = GetEigenvalues(eigenvalues);
  // Sort them!
  if (!error) {
    auto eigval_sort = [&] (const T& a, const T& b) {
      return a*a < b*b;
    };
    std::sort(eigenvalues->data(), eigenvalues->data() + eigenvalues->size(),
              eigval_sort);
  }
  return error;
}

/*
 *  @name   GetEigenvectors
 *  @fn inline int GetEigenvectors(EMatrix* eigenvectors)
 *  @brief  Provide access to eigen vectors
 *  @param[out] eigenvectors Eigen vector in no particular order by row
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int LuNonSymEigShift<T>::
GetEigenvectors(EMatrix*  eigenvectors) {
  int error = -1;
#ifdef HAS_ARPACKPP
  if (prob_->EigenvectorsFound()) {
    int nconv = prob_->ConvergedEigenvalues();
    int ndim = prob_->GetN();
    eigenvectors->resize(nconv, ndim);
    error = 0;
    for (int eigvec = 0; eigvec < nconv; ++eigvec) {
      for (int val = 0; val < ndim; ++val) {
        (*eigenvectors)(eigvec, val) = prob_->EigenvectorReal(eigvec, val);
      }
    }
  }
#endif
  return error;
}

/*
 *  @name   GetSortedEigenvectors
 *  @fn inline int GetSortedEigenvectors(cv::Mat* eigenvectors)
 *  @brief  Provide access to eigen vectors
 *  @param[out] eigenvectors Eigen vector in increasing order of magnitude by row
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int LuNonSymEigShift<T>::GetSortedEigenvectors(EMatrix*  eigenvectors) {
  EVector eigenvalues;
  int error = -1;
#ifdef HAS_ARPACKPP
  GetEigenvalues(&eigenvalues);

  // Sort them, then get eigenvectors, then apply the same reordering!
  std::vector<int> sorted_idx(eigenvalues.size());
  std::iota(sorted_idx.begin(), sorted_idx.end(), 0);

  Eigen::Array<T, Eigen::Dynamic, 1> squared_eigval(eigenvalues);
  squared_eigval *= squared_eigval;

  auto eigval_sort = [&] (const int& a, const int& b) {
    return squared_eigval[a] < squared_eigval[b];
  };
  std::sort(sorted_idx.begin(), sorted_idx.end(), eigval_sort);

  if (prob_->EigenvectorsFound()) {
    int nconv = prob_->ConvergedEigenvalues();
    int ndim = prob_->GetN();
    eigenvectors->resize(nconv, ndim);
    error = 0;
    for (int i = 0; i < nconv; ++i) {
      for (int val = 0; val < ndim; ++val) {
        (*eigenvectors)(i, val) = prob_->EigenvectorReal(sorted_idx[i], val);
      }
    }
  }
#endif
  return error;
}

#pragma mark -
#pragma mark Declaration

/** Float LuNonSymEigShift */
template class LuNonSymEigShift<float>;
/** Double LuNonSymEigShift */
template class LuNonSymEigShift<double>;

}  // namespace SparseLinearAlgebra
}  // namespace LTS5
