/**
 *  @file   shape_transforms.cpp
 *  @brief  Useful transforms for cv::Mat shapes
 *
 *  @author Gabriel Cuendet
 *  @date   24/04/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#include <cstring>

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include "lts5/utils/shape_transforms.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name   ImageAbsToRectAbs
 *  @brief  Convert shape coordinates from absolute image coordinates to
 *          absolute rectangle coordinates
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 *  @return
 */
void ImageAbsToRectAbs(const cv::Rect& rectangle, cv::Mat* shape) {
  assert(shape->type() == CV_64F);
  assert(shape->rows == 1 || shape->cols == 1);
  const int k_points = std::max(shape->rows, shape->cols) / 2;
  for (int i = 0; i < k_points; i++) {
    shape->at<double>(i) -= rectangle.x;
    shape->at<double>(i + k_points) -= rectangle.y;
  }
}

/*
 *  @name   ImageAbsToRectAbs
 *  @brief  Convert shape coordinates from absolute image coordinates to
 *          absolute rectangle coordinates
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 *  @return
 */
cv::Mat ImageAbsToRectAbs(const cv::Rect& rectangle, const cv::Mat& shape) {
  assert(shape.type() == CV_64F);
  assert(shape.rows == 1 || shape.cols == 1);
  const int k_points = std::max(shape.rows, shape.cols) / 2;

  cv::Mat new_shape = shape.clone();

  for (int i = 0; i < k_points; i++) {
    new_shape.at<double>(i) -= rectangle.x;
    new_shape.at<double>(i + k_points) -= rectangle.y;
  }
  return new_shape;
}

/*
 *  @name   ImageAbsToRectRel
 *  @brief  Convert shape coordinates from absolute image coordinates to
 *          relative rectangle coordinates
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 *  @return
 */
void ImageAbsToRectRel(const cv::Rect& rectangle, cv::Mat* shape) {
  assert(shape->type() == CV_64F);
  assert(shape->rows == 1 || shape->cols == 1);
  const int k_points = std::max(shape->rows, shape->cols) / 2;

  ImageAbsToRectAbs(rectangle, shape);

  for (int i = 0; i < k_points; i++) {
    shape->at<double>(i) /= rectangle.width;
    shape->at<double>(i + k_points) /= rectangle.height;
  }
}

/*
 *  @name   ImageAbsToRectRel
 *  @brief  Convert shape coordinates from absolute image coordinates to
 *          relative rectangle coordinates
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 *  @return
 */
cv::Mat ImageAbsToRectRel(const cv::Rect& rectangle, const cv::Mat& shape) {
  assert(shape.type() == CV_64F);
  assert(shape.rows == 1 || shape.cols == 1);
  const int k_points = std::max(shape.rows, shape.cols) / 2;
  cv::Mat new_shape = shape.clone();

  ImageAbsToRectAbs(rectangle, &new_shape);

  for (int i = 0; i < k_points; i++) {
    new_shape.at<double>(i) /= rectangle.width;
    new_shape.at<double>(i + k_points) /= rectangle.height;
  }
  return new_shape;
}

/*
 *  @name   RectAbsToImageAbs
 *  @brief  Convert shape coordinates from absolute rectangle coordinates to
 *          absolute image coordinates
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 *  @return
 */
void RectAbsToImageAbs(const cv::Rect& rectangle, cv::Mat* shape) {
  assert(shape->type() == CV_64F);
  assert(shape->rows == 1 || shape->cols == 1);
  const int k_points = std::max(shape->rows, shape->cols) / 2;

  for (int i = 0; i < k_points; i++) {
    shape->at<double>(i) += rectangle.x;
    shape->at<double>(i + k_points) += rectangle.y;
  }
}

/*
 *  @name   RectAbsToImageAbs
 *  @brief  Convert shape coordinates from absolute rectangle coordinates to
 *          absolute image coordinates
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 *  @return
 */
cv::Mat RectAbsToImageAbs(const cv::Rect& rectangle, const cv::Mat& shape) {
  assert(shape.type() == CV_64F);
  assert(shape.rows == 1 || shape.cols == 1);
  const int k_points = std::max(shape.rows, shape.cols) / 2;

  cv::Mat new_shape = shape.clone();

  for (int i = 0; i < k_points; i++) {
    new_shape.at<double>(i) += rectangle.x;
    new_shape.at<double>(i + k_points) += rectangle.y;
  }
  return new_shape;
}

/*
 *  @name   RectRelToImageAbs
 *  @brief  Convert shape coordinates from relative rectangle coordinates to
 *          absolute image coordinates
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 *  @return
 */
void RectRelToImageAbs(const cv::Rect& rectangle, cv::Mat* shape) {
  assert(shape->type() == CV_64F);
  assert(shape->rows == 1 || shape->cols == 1);
  const int k_points = std::max(shape->rows, shape->cols) / 2;

  for (int i = 0; i < k_points; i++) {
    shape->at<double>(i) *= rectangle.width;
    shape->at<double>(i + k_points) *= rectangle.height;
  }
  RectAbsToImageAbs(rectangle, shape);
}

/*
 *  @name   RectRelToImageAbs
 *  @brief  Convert shape coordinates from relative rectangle coordinates to
 *          absolute image coordinates
 *  @param[in]  rectangle   reference rectangle
 *  @param[in]  shape       shape to convert (will be NOT be modified!)
 *  @return The modified shape
 */
cv::Mat RectRelToImageAbs(const cv::Rect& rectangle, const cv::Mat& shape) {
  // Equivalent to Util::calcInitShape()
  assert(shape.type() == CV_64F);
  assert(shape.rows == 1 || shape.cols == 1);
  const int k_points = std::max(shape.rows, shape.cols) / 2;

  cv::Mat new_shape = shape.clone();

  for (int i = 0; i < k_points; i++) {
    new_shape.at<double>(i) *= rectangle.width;
    new_shape.at<double>(i + k_points) *= rectangle.height;
  }
  RectAbsToImageAbs(rectangle, &new_shape);
  return new_shape;
}

/*
 *  @name   TransformPoint
 *  @brief  Transform a 2D point according to 2x3 matrix
 *  @param[in]      transform   transform matrix
 *  @param[in,out]  point       point to transform (will be modified!)
 *  @return
 */
void TransformPoint(const cv::Mat& transform, cv::Point2d* point) {
  double a = transform.at<double>(0, 0);
  double b = transform.at<double>(1, 0);
  double tx = transform.at<double>(0, 2);
  double ty = transform.at<double>(1, 2);
  double x = point->x;
  double y = point->y;
  point->x = a * x - b * y + tx;
  point->y = b * x + a * y + ty;
}

/*
 *  @name   TransformPoint
 *  @brief  Transform a 2D point according to 2x3 matrix
 *  @param[in]  transform   transform matrix
 *  @param[in]  point       point to transform (will be NOT be modified!)
 *  @return The transformed point
 */
cv::Point2d TransformPoint(const cv::Mat& transform, const cv::Point2d& point) {

  double a = transform.at<double>(0, 0);
  double b = transform.at<double>(1, 0);
  double tx = transform.at<double>(0, 2);
  double ty = transform.at<double>(1, 2);
  return cv::Point2d(a * point.x - b * point.y + tx,
                     b * point.x + a * point.y + ty);
}

/*
 *  @name   transformShape
 *  @brief  Transform a shape
 *  @param[in]  transform   transform matrix
 *  @param[in]  shape       shape to transform (will be modified!)
 *  @return
 */
void TransformShape(const cv::Mat& transform, cv::Mat* shape) {
  assert(shape->type() == CV_64F);
  assert(shape->rows == 1 || shape->cols == 1);
  const int k_points = std::max(shape->rows, shape->cols) / 2;

  for (int p = 0; p < k_points; p++) {
    cv::Point2d pt0;
    pt0.x = shape->at<double>(p);
    pt0.y = shape->at<double>(p + k_points);
    cv::Point2d pt1 = LTS5::TransformPoint(transform, pt0);
    shape->at<double>(p) = pt1.x;
    shape->at<double>(p + k_points) = pt1.y;
  }
}
/*
 *  @name   transformShape
 *  @brief  Transform a shape
 *  @param[in]  transform   transform matrix
 *  @param[in]  shape       shape to transform (will NOT be modified!)
 *  @return Transformed shape
 */
cv::Mat TransformShape(const cv::Mat& transform, const cv::Mat& shape) {
  assert(shape.type() == CV_64F);
  assert(shape.rows == 1 || shape.cols == 1);
  const int k_points = std::max(shape.rows, shape.cols) / 2;

  cv::Mat transformed_shape = cv::Mat(shape.rows,
                                      shape.cols,
                                      shape.type());
  cv::Point2d source_pt;
  cv::Point2d dest_pt;
  for (int p = 0; p < k_points; p++) {
    source_pt.x = shape.at<double>(p);
    source_pt.y = shape.at<double>(p + k_points);
    dest_pt = TransformPoint(transform, source_pt);
    transformed_shape.at<double>(p) = dest_pt.x;
    transformed_shape.at<double>(p + k_points) = dest_pt.y;
  }
  return transformed_shape;
}

/*
 *  @name   GetCenterPoint
 *  @brief  Get the center of a shape
 *  @param[in]  shape   The shape from which to compute the center
 *  @return the center of the shape as a cv::Point2d
 */
cv::Point2d GetCenterPoint(const cv::Mat& shape) {
  assert(shape.type() == CV_64F);
  assert(shape.rows == 1 || shape.cols == 1);
  const int k_points = std::max(shape.rows, shape.cols) / 2;

  cv::Point2d center(0.0, 0.0);
  for (int i = 0; i < k_points; i++) {
    center.x += shape.at<double>(i);
    center.y += shape.at<double>(i + k_points);
  }
  center.x /= k_points;
  center.y /= k_points;
  return center;
}

/*
 *  @name   ComputeRigidTransform
 *  @brief  Compute a rigid transform between two shapes
 *  @param[in]  source  The source shape
 *  @param[in]  dest    The destination shape
 *  @return Elements of the transform matrix [a, b, tx, ty] such that T*src=dst
 */
cv::Vec<double, 4> ComputeRigidTransform(const cv::Mat& source,
                                         const cv::Mat& dest) {
  assert((source.type() == CV_64F) && (dest.type() == CV_64F) &&
         (source.rows == dest.rows) && (source.cols == dest.cols) &&
         (source.cols == 1));
  const int k_points = source.rows / 2;
  cv::Mat H(4, 4, CV_64F, cv::Scalar(0));
  cv::Mat g(4, 1, CV_64F, cv::Scalar(0));
  cv::Mat p(4, 1, CV_64F);
  cv::MatConstIterator_<double> ptr1x = source.begin<double>();
  cv::MatConstIterator_<double> ptr1y = source.begin<double>() + k_points;
  cv::MatConstIterator_<double> ptr2x = dest.begin<double>();
  cv::MatConstIterator_<double> ptr2y = dest.begin<double>() + k_points;
  for (int i = 0; i < k_points; i++, ++ptr1x, ++ptr1y, ++ptr2x, ++ptr2y) {
    H.at<double>(0, 0) += (*ptr1x)*(*ptr1x) + (*ptr1y)*(*ptr1y);
    H.at<double>(0, 2) += *ptr1x; H.at<double>(0, 3) += *ptr1y;
    g.at<double>(0, 0) += (*ptr1x) * (*ptr2x) + (*ptr1y) * (*ptr2y);
    g.at<double>(1, 0) += (*ptr1x) * (*ptr2y) - (*ptr1y) * (*ptr2x);
    g.at<double>(2, 0) += *ptr2x; g.at<double>(3, 0) += *ptr2y;
  }
  H.at<double>(1, 1) = H.at<double>(0, 0);
  H.at<double>(1, 2) = H.at<double>(2, 1) = -1.0 * (H.at<double>(3, 0) = H.at<double>(0, 3));
  H.at<double>(1, 3) = H.at<double>(3, 1) = H.at<double>(2, 0) = H.at<double>(0, 2);
  H.at<double>(2, 2) = H.at<double>(3, 3) = k_points;
  cv::solve(H, g, p, cv::DECOMP_CHOLESKY);
  double a = p.at<double>(0, 0);
  double b = p.at<double>(1, 0);
  double tx = p.at<double>(2, 0);
  double ty = p.at<double>(3, 0);
  return cv::Vec<double, 4>(a, b, tx, ty);
}

/*
 *  @name   ComputeSimilarityTransform
 *  @brief  Compute the similarity transform between two shapes
 *  @param[in]  source            Source shape
 *  @param[in]  dest              Target shape
 *  @param[out] transformation    Linear Transformation [a, b, tx, ty] between
 *                                the two shapes such that T*source = dest.
 */
void ComputeSimilarityTransform(const cv::Mat& source,
                                const cv::Mat& dest,
                                cv::Vec4d* transformation) {
  assert((source.type() == CV_64F) && (dest.type() == CV_64F) &&
  (source.rows == dest.rows) && (source.cols == dest.cols) &&
  (source.cols == 1));

  int i;
  int n = source.rows / 2;
  cv::Mat H(4, 4, CV_64F, cv::Scalar(0));
  cv::Mat g(4, 1, CV_64F, cv::Scalar(0));
  cv::Mat p(4, 1, CV_64F);
  cv::MatConstIterator_<double> ptr1x = source.begin<double>();
  cv::MatConstIterator_<double> ptr1y = source.begin<double>() + n;
  cv::MatConstIterator_<double> ptr2x = dest.begin<double>();
  cv::MatConstIterator_<double> ptr2y = dest.begin<double>() + n;
  for (i = 0; i < n; i++, ++ptr1x, ++ptr1y, ++ptr2x, ++ptr2y) {
    H.at<double>(0, 0) += (*ptr1x)*(*ptr1x) + (*ptr1y)*(*ptr1y);
    H.at<double>(0, 2) += *ptr1x; H.at<double>(0, 3) += *ptr1y;
    g.at<double>(0, 0) += (*ptr1x) * (*ptr2x) + (*ptr1y) * (*ptr2y);
    g.at<double>(1, 0) += (*ptr1x) * (*ptr2y) - (*ptr1y) * (*ptr2x);
    g.at<double>(2, 0) += *ptr2x; g.at<double>(3, 0) += *ptr2y;
  }
  H.at<double>(1, 1) = H.at<double>(0, 0);
  H.at<double>(1, 2) = H.at<double>(2, 1) = -1.0 * (H.at<double>(3, 0) = H.at<double>(0, 3));
  H.at<double>(1, 3) = H.at<double>(3, 1) = H.at<double>(2, 0) = H.at<double>(0, 2);
  H.at<double>(2, 2) = H.at<double>(3, 3) = n;
  cv::solve(H, g, p, cv::DECOMP_CHOLESKY);
  double a = p.at<double>(0, 0);
  double b = p.at<double>(1, 0);
  double tx = p.at<double>(2, 0);
  double ty = p.at<double>(3, 0);
  (*transformation)[0] = a;
  (*transformation)[1] = b;
  (*transformation)[2] = tx;
  (*transformation)[3] = ty;
}

/*
 *  @name ComputeSimilarityTransform
 *  @fn void ComputeSimilarityTransform(const cv::Mat& source,
                                 const cv::Mat& dest,
                                 cv::Mat* transform);
 *  @brief  Compute rigid transform between two shapes (x1,x2,...y1,y2,..)
 *  @param[in]  source      Reference shape
 *  @param[in]  dest        Target shape (sR * src + T)
 *  @param[out] transform   Rigid transformation
 */
void LTS5_EXPORTS ComputeSimilarityTransform(const cv::Mat& source,
                                             const cv::Mat& dest,
                                             cv::Mat* transform) {
  assert(((source.cols == 1) && (source.rows > 4)) ||
         ((source.rows == 1) && (source.cols > 4)));
  assert(((dest.cols == 1) && (dest.rows > 4)) ||
         ((dest.rows == 1) && (dest.cols > 4)));
  assert(((source.rows == dest.rows) && (source.cols == dest.cols)) ||
         ((source.rows == dest.cols) && (source.cols == dest.rows)));
  // Copied from OpenCV 2.3.1, Tuned by Christophe Ecabert
  double sa[16], sb[4];
  memset(sa, 0, sizeof(sa));
  memset(sb, 0, sizeof(sb));

  int n_points = std::max(source.cols, source.rows) / 2;
  const double* src_ptr = reinterpret_cast<const double*>(source.data);
  const double* dst_ptr = reinterpret_cast<const double*>(dest.data);
  double a_x, a_y;
  double b_x, b_y;
  for (int i = 0; i < n_points; ++i) {
    a_x = *src_ptr;
    a_y = *(src_ptr + n_points);
    b_x = *dst_ptr;
    b_y = *(dst_ptr + n_points);
    ++src_ptr;
    ++dst_ptr;

    sa[0] += a_x * a_x + a_y * a_y;
    sa[2] += a_x;
    sa[3] += a_y;

    sa[5] += a_x * a_x + a_y * a_y;
    sa[6] += -a_y;
    sa[7] += a_x;

    sa[8] += a_x;
    sa[9] += -a_y;
    sa[10] += 1;

    sa[12] += a_y;
    sa[13] += a_x;
    sa[15] += 1;

    sb[0] += a_x * b_x + a_y * b_y;
    sb[1] += a_x * b_y - a_y * b_x;
    sb[2] += b_x;
    sb[3] += b_y;
  }
  // Solve linear least square
  cv::Mat A = cv::Mat(4, 4, CV_64FC1, sa);
  cv::Mat B = cv::Mat(4, 1, CV_64FC1, sb);
  cv::Mat M = cv::Mat(4, 1, CV_64FC1);
  LinearAlgebra<double>::LinearSolver solver;
  solver.Solve(A, B, &M);

  transform->create(2, 3, CV_64FC1);
  double* t_ptr = reinterpret_cast<double*>(transform->data);
  const double* m_ptr = reinterpret_cast<const double*>(M.data);
  t_ptr[0] = m_ptr[0];
  t_ptr[1] = -m_ptr[1];
  t_ptr[2] = m_ptr[2];
  t_ptr[3] = m_ptr[1];
  t_ptr[4] = m_ptr[0];
  t_ptr[5] = m_ptr[3];
}

/*
 *  @name SimilarityTransformXYXY
 *  @fn void SimilarityTransformXYXY(const cv::Mat& source,
                                     const cv::Mat& dest,
                                     cv::Mat* transform);
 *  @brief  Compute rigid transform between two shape ordered (x1,y1,x2,y2,..)
 *  @param[in]  source      Reference shape
 *  @param[in]  dest        Target shape (sR * src + T)
 *  @param[out] transfrom   Rigid transformation
 */
void SimilarityTransformXYXY(const cv::Mat& source,
                             const cv::Mat& dest,
                             cv::Mat* transform) {
  // Sanity check (only debug)
  assert(((source.cols == 1) && (source.rows > 4)) ||
         ((source.rows == 1) && (source.cols > 4)));
  assert(((dest.cols == 1) && (dest.rows > 4)) ||
         ((dest.rows == 1) && (dest.cols > 4)));
  assert((source.rows == dest.rows) && (source.cols == dest.cols));

  // Copied from OpenCV 2.3.1, Tuned by Christophe Ecabert
  double sa[16], sb[4];
  std::memset(sa, 0, 128);
  std::memset(sb, 0, 32);

  const int n = std::max(source.rows, source.cols) / 2;
  const double* src_ptr = reinterpret_cast<double*>(source.data);
  const double* dst_ptr = reinterpret_cast<double*>(dest.data);
  double src_x, src_y;
  double dst_x, dst_y;
  for (int i = 0; i < n; ++i) {
    src_x = *src_ptr++;
    src_y = *src_ptr++;
    dst_x = *dst_ptr++;
    dst_y = *dst_ptr++;

    sa[0] += src_x * src_x + src_y * src_y;
    sa[2] += src_x;
    sa[3] += src_y;

    sa[5] += src_x * src_x + src_y * src_y;
    sa[6] += -src_y;
    sa[7] += src_x;

    sa[8] += src_x;
    sa[9] += -src_y;
    sa[10] += 1;

    sa[12] += src_y;
    sa[13] += src_x;
    sa[15] += 1;

    sb[0] += src_x * dst_x + src_y * dst_y;
    sb[1] += src_x * dst_y - src_y * dst_x;
    sb[2] += dst_x;
    sb[3] += dst_y;
  }
  // Solve linear least square
  cv::Mat A = cv::Mat(4, 4, CV_64FC1, sa);
  cv::Mat B = cv::Mat(4, 1, CV_64FC1, sb);
  cv::Mat M = cv::Mat(4, 1, CV_64FC1);
  LinearAlgebra<double>::LinearSolver solver;
  solver.Solve(A, B, &M);

  transform->create(2, 3, CV_64FC1);
  double* t_ptr = reinterpret_cast<double*>(transform->data);
  const double* m_ptr = reinterpret_cast<const double*>(M.data);
  t_ptr[0] = m_ptr[0];
  t_ptr[1] = -m_ptr[1];
  t_ptr[2] = m_ptr[2];
  t_ptr[3] = m_ptr[1];
  t_ptr[4] = m_ptr[0];
  t_ptr[5] = m_ptr[3];
}

/*
 *  @name       Rot2Euler
 *  @brief      Converting from rotation matrix to rotation angles
 *  @param[in]    R        Rotation matrix
 *  @param[out]   pitch    Head rotation angle in pitch
 *  @param[out]   yaw      Head rotation angle in yaw
 *  @param[out]   roll     Head rotation angle in roll
 */
template<typename T>
void Rot2Euler(const cv::Mat &R, T* pitch, T* yaw, T* roll) {
  assert((R.rows == 3) && (R.cols == 3));
  T q[4];
  q[0] = sqrt(1+R.at<T>(0,0)+R.at<T>(1,1)+R.at<T>(2,2))/2;
  q[1] = (R.at<T>(2,1) - R.at<T>(1,2)) / (4*q[0]) ;
  q[2] = (R.at<T>(0,2) - R.at<T>(2,0)) / (4*q[0]) ;
  q[3] = (R.at<T>(1,0) - R.at<T>(0,1)) / (4*q[0]) ;
  *yaw = asin(2*(q[0]*q[2] + q[1]*q[3]));
  *pitch= atan2(2*(q[0]*q[1]-q[2]*q[3]),
                q[0]*q[0]-q[1]*q[1]-q[2]*q[2]+q[3]*q[3]);
  *roll = atan2(2*(q[0]*q[3]-q[1]*q[2]),
                q[0]*q[0]+q[1]*q[1]-q[2]*q[2]-q[3]*q[3]);
  return;
}

template void LTS5_EXPORTS Rot2Euler<float>(const cv::Mat &R,
                                            float* pitch,
                                            float* yaw,
                                            float* roll);
template void LTS5_EXPORTS Rot2Euler<double>(const cv::Mat &R,
                                             double* pitch,
                                             double* yaw,
                                             double* roll);

/*
 *  @name     AddOrthRow
 *  @brief    Add the third orthogonal row vector to the rotation matrix
 *  @param[in,out]   R   Rotation matrix
 */
void AddOrthRow(cv::Mat *R) {
  assert((R->rows == 3) && (R->cols == 3));
  R->at<double>(2,0) = R->at<double>(0,1)*R->at<double>(1,2) - R->at<double>(0,2)*R->at<double>(1,1);
  R->at<double>(2,1) = R->at<double>(0,2)*R->at<double>(1,0) - R->at<double>(0,0)*R->at<double>(1,2);
  R->at<double>(2,2) = R->at<double>(0,0)*R->at<double>(1,1) - R->at<double>(0,1)*R->at<double>(1,0);
}

/*
 *  @name   Euler2Rot
 *  @brief  Converting from Euler angles Rx * Ry * Rz rotation to Rotation
 *          matrix
 *          Full indicates if a 3x3 or a 2x3 matrix is needed (in 2x3 case
 *          we don't care about resulting z component)
 *  @param[in]   pitch  Head rotation angle in pitch
 *  @param[in]   yaw    Head rotation angle in yaw
 *  @param[in]   roll   Head rotation angle in roll
 *  @param[in]   full   Full rotation matrix
 *  @param[out]  R      Rotation matrix
 */
void Euler2Rot(const double pitch,
               const double yaw,
               const double roll,
               cv::Mat* R,
               bool full) {
  if(full) { // defining the matrices based on needed size
    if((R->rows != 3) || (R->cols != 3)) {
      R->create(3,3,CV_64F);
    }
  } else {
    if((R->rows != 2) || (R->cols != 3)) {
      R->create(2,3,CV_64F);
    }
  }
  // used for the rotation matrix calculations
  double sina = sin(pitch), sinb = sin(yaw), sinc = sin(roll);
  double cosa = cos(pitch), cosb = cos(yaw), cosc = cos(roll);
  R->at<double>(0,0) = cosb * cosc;
  R->at<double>(0,1) = -cosb * sinc;
  R->at<double>(0,2) = sinb;
  R->at<double>(1,0) = cosa * sinc + sina * sinb * cosc;
  R->at<double>(1,1) = cosa * cosc - sina * sinb * sinc;
  R->at<double>(1,2) = -sina * cosb;

  // if we need a full matrix, construct it using the existing bit (as there
  // is enough info in first two rows)
  if(full) {
    AddOrthRow(R);
  }

  return;
}

/*
 *  @name         AlignShapes3D2D
 *  @brief        Align a 3D shape to a 2D shape
 *  @param[in]    s2D       2D shape
 *  @param[in]    s3D       3D shape
 *  @param[out]   scale     scaling factor
 *  @param[out]   pitch     Head rotation angle in pitch
 *  @param[out]   yaw       Head rotation angle in yaw
 *  @param[out]   roll      Head rotation angle in roll
 *  @param[out]   x         Translation in x
 *  @param[out]   y         Translation in y
 */
void AlignShapes3D2D(const cv::Mat &s2D,
                     const cv::Mat &s3D,
                     double* scale,
                     double* pitch,
                     double* yaw,
                     double* roll,
                     double* x,
                     double* y) {
  assert((s2D.cols == 1) && (s3D.rows == 3*(s2D.rows/2)) && (s3D.cols == 1));

  int n = s2D.rows/2;
  //double t2[2],t3[3];

  cv::Mat X = s2D.reshape(1, 2).t();
  cv::Mat S = s3D.reshape(1, 3).t();
  cv::Mat mean_x;
  cv::Mat mean_s;
  cv::reduce(X, mean_x, 0, cv::REDUCE_AVG);
  cv::reduce(S, mean_s, 0, cv::REDUCE_AVG);
  double* x_ptr = reinterpret_cast<double*>(X.data);
  double* s_ptr = reinterpret_cast<double*>(S.data);
  const double* mean_x_ptr = reinterpret_cast<const double*>(mean_x.data);
  const double* mean_s_ptr = reinterpret_cast<const double*>(mean_s.data);
  for (int p = 0; p < n; ++p) {
    // X
    *x_ptr = *x_ptr - mean_x_ptr[0];
    ++x_ptr;
    *x_ptr = *x_ptr - mean_x_ptr[1];
    ++x_ptr;
    // S
    *s_ptr = *s_ptr - mean_s_ptr[0];
    ++s_ptr;
    *s_ptr = *s_ptr - mean_s_ptr[1];
    ++s_ptr;
    *s_ptr = *s_ptr - mean_s_ptr[2];
    ++s_ptr;
  }

  /*for(i = 0; i < 2; i++){
    cv::Mat v = X.col(i);  // X is (66,2) sized matrix containing x,y coordinates
    t2[i] = sum(v)[0]/n;    // t2[0]:mean of x coordinates  t2[1]: mean of y coordinates
    v-=t2[i];               // v:normalized X
  }
  for(i = 0; i < 3; i++) {
    cv::Mat v = S.col(i);
    t3[i] = sum(v)[0]/n;
    v-=t3[i];
  }*/
  cv::Mat St = S.t();
  cv::Mat M = ((St * S).inv(cv::DECOMP_CHOLESKY)) * St * X;  //M:3x2
  cv::Mat Mt = M.t();
  cv::Mat MtM = Mt * M;                                     //MtM:2x2
  cv::SVD svd(MtM,cv::SVD::MODIFY_A);
  svd.w.at<double>(0,0) = 1.0 / sqrt(svd.w.at<double>(0,0));
  svd.w.at<double>(1,0) = 1.0 / sqrt(svd.w.at<double>(1,0));

  cv::Mat T(3,3,CV_64F);
  T(cv::Rect(0,0,3,2)) = svd.u * cv::Mat::diag(svd.w) * svd.vt * Mt;
  *scale = 0.5 * sum(T(cv::Rect(0,0,3,2)).mul(Mt))[0];

  AddOrthRow(&T);

  Rot2Euler(T,pitch,yaw,roll);

  T *= *scale;
  //*x = t2[0] - (T.at<double>(0,0)*t3[0] + T.at<double>(0,1)*t3[1] + T.at<double>(0,2)*t3[2]);
  //*y = t2[1] - (T.at<double>(1,0)*t3[0] + T.at<double>(1,1)*t3[1] + T.at<double>(1,2)*t3[2]);
  *x = mean_x_ptr[0] - (T.at<double>(0,0)* mean_s_ptr[0] +
                        T.at<double>(0,1) * mean_s_ptr[1] +
                        T.at<double>(0,2) * mean_s_ptr[2]);
  *y = mean_x_ptr[1] - (T.at<double>(1,0)* mean_s_ptr[0] +
                        T.at<double>(1,1) * mean_s_ptr[1] +
                        T.at<double>(1,2)*mean_s_ptr[2]);
}

/*void Align3Dto2DShapes(double& scale,double& pitch,double& yaw,double& roll,
                       double& x,double& y,cv::Mat &s2D,cv::Mat &s3D)
{
  assert((s2D.cols == 1) && (s3D.rows == 3*(s2D.rows/2)) && (s3D.cols == 1));
  int i,n = s2D.rows/2; double t2[2],t3[3];
  cv::Mat s2D_cpy = s2D.clone(),s3D_cpy = s3D.clone();
  cv::Mat X = (s2D_cpy.reshape(1,2)).t(),S = (s3D_cpy.reshape(1,3)).t();
  for(i = 0; i < 2; i++){cv::Mat v = X.col(i); t2[i] = sum(v)[0]/n; v-=t2[i];}
  for(i = 0; i < 3; i++){cv::Mat v = S.col(i); t3[i] = sum(v)[0]/n; v-=t3[i];}
  cv::Mat M = ((S.t()*S).inv(cv::DECOMP_CHOLESKY))*S.t()*X;
  cv::Mat MtM = M.t()*M; cv::SVD svd(MtM,cv::SVD::MODIFY_A);
  svd.w.at<double>(0,0) = 1.0/sqrt(svd.w.at<double>(0,0));
  svd.w.at<double>(1,0) = 1.0/sqrt(svd.w.at<double>(1,0));
  cv::Mat T(3,3,CV_64F);
  T(cv::Rect(0,0,3,2)) = svd.u*cv::Mat::diag(svd.w)*svd.vt*M.t();
  scale = 0.5*sum(T(cv::Rect(0,0,3,2)).mul(M.t()))[0];
  AddOrthRow(&T);
  Rot2Euler(T,&pitch,&yaw,&roll);
  T *= scale;
  x = t2[0] - (T.at<double>(0,0)*t3[0] + T.at<double>(0,1)*t3[1] + T.at<double>(0,2)*t3[2]);
  y = t2[1] - (T.at<double>(1,0)*t3[0] + T.at<double>(1,1)*t3[1] + T.at<double>(1,2)*t3[2]);
  return;
}*/

/*
 *  @name FlipShape
 *  @brief  Horizontally flips a shape
 *  @param[in]  shape       Shape to flip
 *  @param[in]  image_size  Size of the corresponding image
 */
cv::Mat FlipShape(const cv::Mat& shape, const cv::Size& image_size) {
  assert(shape.type() == CV_64F);
  assert(shape.rows == 1 || shape.cols == 1);
  const int k_points = std::max(shape.rows, shape.cols) / 2;

  cv::Mat output = shape.clone();
  std::vector<int> symlist;
  switch (k_points) {
    case 29: {
      int list[] = {1, 0, 3, 2, 6, 7, 4, 5, 9, 8, 11, 10, 14, 15, 12, 13, 17,
                    16, 19, 18, 20, 21, 23, 22, 24, 25, 26, 27, 28};
      symlist.insert(symlist.end(), list, list + k_points);
      break;
    } case 68: {
      int list[] = {16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
                    26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 27, 28, 29, 30, 35,
                    34, 33, 32, 31, 45, 44, 43, 42, 47, 46, 39, 38, 37, 36, 41,
                    40, 54, 53, 52, 51, 50, 49, 48, 59, 58, 57, 56, 55, 64, 63,
                    62, 61, 60, 67, 66, 65};
      symlist.insert(symlist.end(), list, list + k_points);
      break;
    }
    default:
      std::cout << "Horizontal flip is not defined for shape containing " <<
      k_points << " landmarks!" << std::endl;
      return output;
  }
  // Flip images & annotations
  for (int p = 0; p < k_points; p++) {
    output.at<double>(p) = image_size.width - 1 -
                           shape.at<double>(symlist[p]);
    output.at<double>(p + k_points) = shape.at<double>(symlist[p] + k_points);
  }

  return output;
}

/*
 *  @name ComputeMeanShape
 *  @brief Computes the mean shape of a vector of shapes
 *  @param[in]   shapes     Vector of cv::Mat to average into mean shape
 *  @param[out]  mean_shape Resulting mean shape
 */
void ComputeMeanShape(const std::vector<cv::Mat>& shapes,
                      cv::Mat* mean_shape) {
  const int n_shapes = static_cast<int>(shapes.size());
  assert(n_shapes > 0);

  // Initialize mean shape
  const int rows = shapes[0].rows;
  const int cols = shapes[0].cols;
  const int type = shapes[0].type();
  mean_shape->create(rows, cols, type);
  //*mean_shape = cv::Mat::zeros(rows, cols, type);

  for (int i = 0; i < n_shapes; ++i) {
    *mean_shape += shapes[i];
  }

  *mean_shape /= n_shapes;
}

/*
 *  @name ComputeRelativeMeanShape
 *  @brief Computes the mean shape of a vector of shapes, relatively to a vector
 *         of rectangles (calls ImageAbsToRectRel)
 *  @param[in]   shapes     Vector of cv::Mat to average into mean shape
 *  @param[in]   rects      Vector of cv::Rect of the same length as shapes
 *  @param[out]  mean_shape Resulting mean shape
 */
void ComputeRelativeMeanShape(const std::vector<cv::Mat>& shapes,
                              const std::vector<cv::Rect>& rects,
                              cv::Mat* mean_shape) {
  const int n_shapes = static_cast<int>(shapes.size());
#ifdef DEBUG
  const int n_rects = static_cast<int>(rects.size());
  assert(n_shapes == n_rects && n_shapes > 0);
#endif

  // Initialize mean shape
  const int rows = shapes[0].rows;
  const int cols = shapes[0].cols;
  const int type = shapes[0].type();
  mean_shape->create(rows, cols, type);
  *mean_shape = cv::Mat::zeros(rows, cols, type);

  for (int i = 0; i < n_shapes; ++i) {
    assert(rects[i].width > 0.001 && rects[i].height > 0.001);
    *mean_shape += LTS5::ImageAbsToRectRel(rects[i], shapes[i]);
  }
  *mean_shape /= n_shapes;
}

/*
 *  @name   KeypointsFromShape
 *  @brief  Convert a shape into a vector of cv::KeyPoint
 *  @param[in]      shape       Shape
 *  @param[in]      diameter    Diameter of the keypoint (opencv requirement)
 *  @return The vector of cv::KeyPoint
 */
std::vector<cv::KeyPoint> KeypointsFromShape(const cv::Mat& shape,
                                             const float& diameter) {
  assert(shape.type() == CV_64F);
  assert(shape.rows == 1 || shape.cols == 1);
  const int k_points = std::max(shape.rows, shape.cols) / 2;

  std::vector<cv::KeyPoint> keypoints;

  for (int p = 0; p < k_points; p++) {
    double x = shape.at<double>(p);
    double y = shape.at<double>(p + k_points);
    cv::KeyPoint kp(cvRound(x), cvRound(y), diameter);
    keypoints.push_back(kp);
  }
  return keypoints;
}

/*
 *  @name   KeypointsFromShape
 *  @brief  Convert a shape into a vector of cv::KeyPoint
 *  @param[in]      shape       Shape
 *  @param[in]      diameter    Diameter of the keypoint (opencv requirement)
 *  @param[in]      landmarks   Set of landmarks to include
 *  @return The vector of cv::KeyPoint
 */
std::vector<cv::KeyPoint> KeypointsFromShape(const cv::Mat& shape,
                                             const float& diameter,
                                             const std::unordered_set<int>&
                                             landmarks) {
  assert(shape.type() == CV_64F);
  assert(shape.rows == 1 || shape.cols == 1);
  const int k_points = std::max(shape.rows, shape.cols) / 2;
  std::vector<cv::KeyPoint> keypoints;

  for (int p = 0; p < k_points; p++) {
    if (landmarks.find(p) != landmarks.end()){
      double x = shape.at<double>(p);
      double y = shape.at<double>(p + k_points);
      cv::KeyPoint kp(cvRound(x), cvRound(y), diameter);
      keypoints.push_back(kp);
    }
  }
  return keypoints;
}
/*
 *  @name   ExtractInnerShape
 *  @brief  From a Sdm shape, extract the inner shape
 *  @param[in]  shape   Shape from sdm tracker (i.e. 68 pts)
 *  @param[out] inner_shape Inner shape of sdm shape
 */
void ExtractInnerShape(const cv::Mat& shape, cv::Mat* inner_shape) {
  assert((((shape.rows == 136) && (shape.cols == 1)) ||
         ((shape.rows == 1) && (shape.cols == 136)) ||
         ((shape.rows == 132) && (shape.cols == 1)) ||
         ((shape.rows == 1) && (shape.cols == 132))) &&
         (shape.type() == CV_64FC1));
  // Define dimension
  int row = shape.rows > 1 ? 98 : 1;
  int col = shape.cols > 1 ? 98 : 1;
  inner_shape->create(row, col, shape.type());

  // Extract inner shape
  int n_points = std::max(shape.cols, shape.rows) / 2;
  int in_shift = 49;
  int in_idx = 0;
  for (int i = 0; i < n_points; ++i) {
    if (i < 17) {
      continue;
    } else {
      if (n_points == 68 && (i == 60 || i == 64)) {
        continue;
      }
    }
    inner_shape->at<double>(in_idx) = shape.at<double>(i);
    inner_shape->at<double>(in_idx + in_shift) = shape.at<double>(i + n_points);
    in_idx++;
  }
}
/*
 *  @name Convert68ShapeTo66
 *  @brief  Convert a 68 points SDM shape to 66 points CLM shape
 *  @param[in]
 *  @param[in]
 */
void Convert68ShapeTo66(const cv::Mat& shape_68, cv::Mat* shape_66) {
  assert((((shape_68.rows == 136) && (shape_68.cols == 1)) ||
         ((shape_68.rows == 1) && (shape_68.cols == 136))) &&
         (shape_68.type() == CV_64FC1));
  // Define dimensions
  int row = shape_68.rows > 1 ? 132 : 1;
  int col = shape_68.cols > 1 ? 132 : 1;
  shape_66->create(row, col, shape_68.type());
  // Extract pts
  int n_points = std::max(shape_68.cols, shape_68.rows) / 2;
  int out_idx = 0;
  for (int i = 0; i < n_points; ++i) {
    if ( i == 60 || i == 64) {
      continue;
    }
    shape_66->at<double>(out_idx) = shape_68.at<double>(i);
    shape_66->at<double>(out_idx + 66) = shape_68.at<double>(i + n_points);
    out_idx++;
  }
}

/*
 *  @name TriangulatePoints
 *  @fn void TriangulatePoints(const std::vector<LTS5::Vector2<T>>& pts,
                               const int width, const int height,
                               std::vector<LTS5::Vector3<int>>* tri)
 *  @brief  Perform Delauny triangulation on a given set of points
 *  @param[in]  pts     Points to triangulate
 *  @param[in]  width   Region width
 *  @param[in]  height  Region height
 *  @param[out] tri     List of corresponding triangle
 */
template<typename T>
void TriangulatePoints(const std::vector<LTS5::Vector2<T>>& pts,
                       const int width, const int height,
                       std::vector<LTS5::Vector3<int>>* tri) {
  // Create subdivision
  cv::Subdiv2D subdiv(cv::Rect(0,0,width,height));
  for (int p = 0; p < pts.size(); ++p) {
    const LTS5::Vector2<T>& p_in = pts[p];
    subdiv.insert(cv::Point2f(p_in.x_, p_in.y_));
  }
  // Recover triangle list
  std::vector<cv::Vec6f> triList;
  subdiv.getTriangleList(triList);
  // Loop over list to find corresponding index
  int n_tri = static_cast<int>(triList.size());
  LTS5::Vector2<float>* tri_ptr = (LTS5::Vector2<float>*)(triList.data());
  int tri_idx = -1;
  std::vector<LTS5::Vector2<float>>::const_iterator beg = pts.begin();
  std::vector<LTS5::Vector2<float>>::const_iterator end = pts.end();
  int idx0 = -1, idx1 = -1, idx2 = -1;
  float w = static_cast<float>(width);
  float h = static_cast<float>(height);
  tri->clear();
  for (int t = 0; t < n_tri; ++t) {
    // If outside image region, remove it
    tri_idx = t * 3;
    const LTS5::Vector2<float>& p0 = tri_ptr[tri_idx];
    const LTS5::Vector2<float>& p1 = tri_ptr[tri_idx + 1];
    const LTS5::Vector2<float>& p2 = tri_ptr[tri_idx + 2];
    if ((p0.x_ < 0.0 || p0.y_ < 0.0) || (p1.x_ < 0.0 || p1.y_ < 0.0) ||
        (p2.x_ < 0.0 || p2.y_ < 0.0) ||
        (p0.x_ > w || p0.y_ > h) || (p1.x_ > w || p1.y_ > h) ||
        (p2.x_ > w || p2.y_ > h)) {
      // Outside image -> drop
      continue;
    }
    // Is inside, find index
    auto it_p0 = std::find(beg, end, p0);
    auto it_p1 = std::find(beg, end, p1);
    auto it_p2 = std::find(beg, end, p2);
    if (it_p0 != end && it_p1 != end && it_p2 != end) {
      // Triangle found
      idx0 = static_cast<int>(std::distance(beg, it_p0));
      idx1 = static_cast<int>(std::distance(beg, it_p1));
      idx2 = static_cast<int>(std::distance(beg, it_p2));
      // Check if normal point in the correct direction (outside)
      const Vector3<float> pt0 = Vector3<float>(pts[idx0].x_, pts[idx0].y_, 0.f);
      const Vector3<float> pt1 = Vector3<float>(pts[idx1].x_, pts[idx1].y_, 0.f);
      const Vector3<float> pt2 = Vector3<float>(pts[idx2].x_, pts[idx2].y_, 0.f);
      auto n = (pt1 - pt0) ^ (pt2 - pt0);
      n.Normalize();
      if (n.z_ < 0.f) {
        int tmp = idx1;
        idx1 = idx2;
        idx2 = tmp;
      }
      tri->push_back(LTS5::Vector3<int>(idx0, idx1, idx2));
    }
  }
}

template void LTS5_EXPORTS TriangulatePoints(const std::vector<Vector2<float>>& pts,
                                             const int width, const int height,
                                             std::vector<Vector3<int>>* tri);

/**
 *  @name DrawTriangulation
 *  @fn void DrawTriangulation(const cv::Mat& image,
                               const std::vector<LTS5::Vector2<T>>& pts,
                               const std::vector<LTS5::Triangle>& tri,
                               cv::Mat* canvas)
 *  @brief  Draw a given triangulation on an image
 *  @param[in]  image   Image to draw on
 *  @param[in]  pts     List of points
 *  @param[in]  tri     List of triangles
 *  @param[out] canvas  Image with triangulation draw on it
 */
template<typename T>
void DrawTriangulation(const cv::Mat& image,
                       const std::vector<LTS5::Vector2<T>>& pts,
                       const std::vector<LTS5::Vector3<int>>& list_tri,
                       const Vector3<unsigned char>& color,
                       cv::Mat* canvas) {
  // Color image ?
  if (image.channels() != 3) {
    cv::cvtColor(image, *canvas, cv::COLOR_GRAY2BGR);
  } else {
    *canvas = image.clone();
  }

  // Loop over each triangle
  for (int t = 0; t < list_tri.size(); ++t) {
    // Access triangle
    const LTS5::Vector3<int>& tri = list_tri[t];
    // Access triangle's pts
    const LTS5::Vector2<T>& p0 = pts[tri.x_];
    const LTS5::Vector2<T>& p1 = pts[tri.y_];
    const LTS5::Vector2<T>& p2 = pts[tri.z_];
    // Draw lines
    cv::line(*canvas,
             cv::Point2f(p0.x_,p0.y_),
             cv::Point2f(p1.x_, p1.y_),
             CV_RGB(color.r_, color.g_, color.b_), 1, cv::LINE_AA);
    cv::line(*canvas,
             cv::Point2f(p0.x_,p0.y_),
             cv::Point2f(p2.x_, p2.y_),
             CV_RGB(color.r_, color.g_, color.b_), 1, cv::LINE_AA);
    cv::line(*canvas,
             cv::Point2f(p1.x_,p1.y_),
             cv::Point2f(p2.x_, p2.y_),
             CV_RGB(color.r_, color.g_, color.b_), 1, cv::LINE_AA);
  }
}

template LTS5_EXPORTS void DrawTriangulation(const cv::Mat& image,
                                     const std::vector<LTS5::Vector2<float>>& pts,
                                     const std::vector<LTS5::Vector3<int>>& list_tri,
                                     const Vector3<unsigned char>& color,
                                     cv::Mat* canvas);

/*
 *  @name NormalizeShape
 *  @fn static void NormalizeShapeXY(const cv::Mat& shape,
                                     const cv::Rect& bbox,
                                     cv::Mat* norm_shape)
 *  @brief  Normalize a given shape
 *  @param[in]  shape   Shape to normalize
 *  @param[in]  bbox    Face bounding box where shape stands
 *  @param[out] norm_shape  Normalized shape
 */
template<typename T>
void NormalizeShapeXY(const cv::Mat& shape,
                      const cv::Rect& bbox,
                      cv::Mat* norm_shape) {
  using Vec2 = LTS5::Vector2<double>;
  norm_shape->create(shape.rows, shape.cols, shape.type());
  const int n_pts = std::max(shape.rows, shape.cols) / 2;
  const Vec2* shape_ptr = reinterpret_cast<Vec2*>(shape.data);
  Vec2* norm_shape_ptr = reinterpret_cast<Vec2*>(norm_shape->data);
  const double c_x = (2.0 * static_cast<double>(bbox.x) +
                      static_cast<double>(bbox.width)) / 2.0;
  const double c_y = (2.0 * static_cast<double>(bbox.y) +
                      static_cast<double>(bbox.height)) / 2.0;
  const double bbox_w = bbox.width / 2.0;
  const double bbox_h = bbox.height / 2.0;
  for (int p = 0; p < n_pts; ++p) {
    norm_shape_ptr[p].x_ = (shape_ptr[p].x_ - c_x) / bbox_w;
    norm_shape_ptr[p].y_ = (shape_ptr[p].y_ - c_y) / bbox_h;
  }
}

template void LTS5_EXPORTS NormalizeShapeXY<float>(const cv::Mat& shape,
                                                   const cv::Rect& bbox,
                                                   cv::Mat* norm_shape);
template void LTS5_EXPORTS NormalizeShapeXY<double>(const cv::Mat& shape,
                                                    const cv::Rect& bbox,
                                                    cv::Mat* norm_shape);

/**
 *  @name PlaceShapeShape
 *  @fn static void PlaceShapeShape(const cv::Mat& shape,
                                   const cv::Rect& bbox,
                                   cv::Mat* placed_shape)
 *  @brief  Place shape at specific location according a given bounding box
 *  @param[in]  shape   Shape to normalize
 *  @param[in]  bbox    Face bounding box where shape stands
 *  @param[out] placed_shape  Placed shape
 */
template<typename T>
void PlaceShapeXY(const cv::Mat& shape,
                  const cv::Rect& bbox,
                  cv::Mat* placed_shape) {
  using Vec2 = LTS5::Vector2<double>;
  placed_shape->create(shape.rows, shape.cols, shape.type());
  const int n_pts = std::max(shape.rows, shape.cols) / 2;
  const Vec2* shape_ptr = reinterpret_cast<Vec2*>(shape.data);
  Vec2* place_shape_ptr = reinterpret_cast<Vec2*>(placed_shape->data);
  const double c_x = (2.0 * static_cast<double>(bbox.x) +
                      static_cast<double>(bbox.width)) / 2.0;
  const double c_y = (2.0 * static_cast<double>(bbox.y) +
                      static_cast<double>(bbox.height)) / 2.0;
  const double bbox_w = bbox.width / 2.0;
  const double bbox_h = bbox.height / 2.0;
  for (int p = 0; p < n_pts; ++p) {
    place_shape_ptr[p].x_ = (shape_ptr[p].x_ * bbox_w) + c_x;
    place_shape_ptr[p].y_ = (shape_ptr[p].y_ * bbox_h) + c_y;
  }
}
template void LTS5_EXPORTS PlaceShapeXY<float>(const cv::Mat& shape,
                                               const cv::Rect& bbox,
                                               cv::Mat* placed_shape);
template void LTS5_EXPORTS PlaceShapeXY<double>(const cv::Mat& shape,
                                                const cv::Rect& bbox,
                                                cv::Mat* placed_shape);


}  // namespace LTS5
