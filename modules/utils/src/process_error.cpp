/**
 *  @file   process_error.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 13/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include "lts5/utils/process_error.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name   ProcessError
 *  @brief  Constructor
 */
//ProcessError::ProcessError(){};

/*
 *  @name   ProcessError
 *  @brief  Constructor
 *  @param  error_code    Error code
 *  @param  message        Error message
 *  @param  function_name   Function name that created this error
 */
ProcessError::ProcessError(const ProcessErrorEnum& error_code,
                           const std::string& message,
                           const std::string& function_name) noexcept {
  message_ = function_name + " gives error : " + std::to_string(error_code) +
            " with the following message : " + message;
}

/*
 *  @name   ~ProcessError
 *  @brief  Destructor
 */
//ProcessError::~ProcessError() noexcept {}

/*
 *  @name   what
 *  @brief  Return description and context of the error
 */
const char* ProcessError::what() const noexcept {
  return message_.c_str();
}
}
