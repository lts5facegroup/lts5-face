/**
 *  @file   constants.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   19/12/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/utils/math/constant.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Pi */
template <typename T>
constexpr T Constants<T>::PI;
/** Pi / 2 */
template <typename T>
constexpr T Constants<T>::PI_2;
/** Pi / 3 */
template <typename T>
constexpr T Constants<T>::PI_3;
/** Pi / 4 */
template <typename T>
constexpr T Constants<T>::PI_4;
/** Pi / 6 */
template <typename T>
constexpr T Constants<T>::PI_6;

}  // namepsace LTS5