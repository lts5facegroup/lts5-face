/**
 *  @file   status.cpp
 *  @brief  Indicator of success or failure of functions. Strip and adapted from
 *         Tensorflow
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   6/13/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <cassert>

#include "lts5/utils/status.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   Status
 *  @fn     Status(const Type& type, const std::string& msg)
 *  @brief  Constructor
 *  @param[in] type Type of error/status
 *  @param[in] msg  Message explaining what went wrong (error description)
 */
Status::Status(const Type& type,
               const std::string& msg) : err_(type), msg_(msg) {
  assert(type != Type::kOk);
}

/*
 *  @name   ToString
 *  @fn     std::string ToString(void) const
 *  @brief  Convert status error code into human readable string. Usefull for
 *          debugging
 */
std::string Status::ToString(void) const {
  std::string str;
  if (err_ == Type::kOk) {
    str = "Ok";
  } else {
    switch (err_) {
      case Type::kUnknown: str = "Unknown: ";
        break;
      case Type::kInvalidArgument: str = "Invalid argument: ";
        break;
      case Type::kNotFound: str = "Not found: ";
        break;
      case Type::kAlreadyExists: str = "Already exists: ";
        break;
      case Type::kOutOfRange: str = "Out of range: ";
        break;
      case Type::kUnimplemented: str = "Unimplemented: ";
        break;
      case Type::kInternalError: str = "Internal error: ";
        break;
      default:  str="Unknown code (" + std::to_string((int)err_) + "): ";
        break;
    }
    str += msg_;
  }
  return str;
}

/*
 *  @name   operator<<
 *  @fn     friend std::ostream& operator<<(std::ostream& os, const Status& status)
 *  @brief  Dump status into an output stream
 *  @param[in] os Output stream in which to write
 *  @param[in] status Status to dump
 *  @return Output stream
 */
std::ostream& operator<<(std::ostream& os, const Status& status) {
  os << status.ToString();
  return os;
}

}  // namespace LTS5
