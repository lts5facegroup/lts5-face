/**
 *  @file   profiling.cpp
 *  @brief  Utility class to profile execution times
 *
 *  @author Marco Lourenço
 *  @date   11/03/16
 *  Copyright (c) 2016 Marco Lourenço. All rights reserved.
 */

#include <iostream>
#include <stdlib.h>
#include <fstream>

#include "lts5/utils/profiling.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name Profiling
 *  @fn Profiling(void)
 *  @brief  Constructor
 */
Profiling::Profiling(void) {
  tik_timer_ = 0.0;
  tok_timer_ = 0.0;
  timer_array_index_ = 0;
  number_of_timers_ = 0;
  number_of_passes_ = 0;
}

/*
 *  @name ~Profiling
 *  @fn ~Profiling(void)
 *  @brief  Destructor
 */
Profiling::~Profiling(void) {
}

#pragma mark -
#pragma mark Process

/*
 *  @name StartTimer
 *  @fn StartTimer(void)
 *  @brief start counting time
 */
void Profiling::StartTimer() {
  struct timeval tp;
  gettimeofday(&tp, NULL);
  tik_timer_ = ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

/*
 *  @name StopTimer
 *  @fn StopTimer(void)
 *  @brief stop counting time
 *  @return the number of milliseconds that passed since startTimer()
 */
double Profiling::StopTimer() {
  struct timeval tp;
  gettimeofday(&tp, NULL);
  tok_timer_ = ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
  return (tok_timer_ - tik_timer_);
}

/*
 *  @name InitCSVFile
 *  @fn InitCSVFile(std::string filename)
 *  @brief init headers in CSV file
 *  @param[in]  filename               the name of the file to init
 */
void Profiling::InitCSVFile(std::string filename)
{
  std::ofstream csv_out_file;
  csv_out_file.open(filename, std::ofstream::out | std::ofstream::app);
  if(!csv_out_file.is_open()) {
    printf("Error: File Not Found\n");
  } else {
    // TODO: (Christophe) Would be nice to have that as parameters instead of Hard-coded.
    csv_out_file << "frame_capture_module_->CaptureFrame();face_extractor_->Process();"
    "face_extractor_->get_face_landmarks();face_extractor_->get_pose_estimation();"
    "face_extractor_->get_face_region_origin();face_extractor_->get_contour();"
    "model_fitter_->FitModel();model_face_mesh_->Update();model_fitter_->get_projection_matrix();"
    "model_fitter_->get_projection_matrix();model_fitter_->get_projection_matrix();"
    "texture_mapper_->Process();frame_capture_module_->get_frame_name();"
    "mesh_exporter_->set_mesh();mesh_exporter_->Process()" << std::endl;
    csv_out_file.close();
  }
}

/*
 *  @name ClearCSVFile
 *  @fn ClearCSVFile(std::string filename)
 *  @brief remove all content from CSV file
 *  @param[in]  filename               the name of the file from which to delete
 */
void Profiling::ClearCSVFile(std::string filename) {
  std::ofstream csv_out_file;
  // empty file
  csv_out_file.open(filename, std::ofstream::out | std::ofstream::trunc);
  csv_out_file.close();
}

/*
 *  @name ExportTimersToCSV
 *  @fn ExportTimersToCSV(std::string filename)
 *  @brief export all recorded timers to CSV file
 *  @param[in]  filename		 the name of the file created
 */
void Profiling::ExportTimersToCSV(std::string filename)
{
  std::ofstream csv_out_file;
  csv_out_file.open(filename, std::ofstream::out | std::ofstream::app);
  if(!csv_out_file.is_open()) {
    printf("Error: File Not Found\n");
  } else {
    for(int i = 0;i<timer_array_index_;i++) {
      csv_out_file << save_timer_array_[i] << ";";
    }
    csv_out_file << std::endl;
    timer_array_index_ = 0;
    csv_out_file.close();
  }
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name SetTimerArray
 *  @fn SetTimerArray(int index)
 *  @brief set a value in save_timer_array_ at pos index
 *  @param[in]  value		 the value to set
 *  @param[in]  index      index in array
 */
void Profiling::SetTimerArray(double value, int index) {
  save_timer_array_[index] = value;
}

}  // namespace LTS5
