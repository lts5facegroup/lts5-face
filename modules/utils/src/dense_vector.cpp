/**
 *  @file   dense_vector.cpp
 *  @brief  Dense vector data structure.
 *          Based on OpenNL library
 *
 *  @author Christophe Ecabert
 *  @date   12/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <cstring>
#include <assert.h>

#include "lts5/utils/math/dense_vector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name DenseVector
 *  @fn DenseVector(int dimension)
 *  @brief      Constructor
 *  @param[in]  dimension Vector dimension
 */
template<typename T>
DenseVector<T>::DenseVector(int dimension) {
  // Define dimension
  dimension_ = dimension;
  // Init array
  coeff_ = new T[dimension_];
  // Fill with zeros
  for (int i = 0; i < dimension_; ++i) {
    coeff_[i] = 0.0;
  }
}

/*
 *  @name DenseVector
 *  @fn DenseVector(const DenseVector& other)
 *  @brief  Copy constructor
 *  @param[in]  other Vector to copy
 */
template <typename T>
DenseVector<T>::DenseVector(const DenseVector& other) {
  // Setup dimension
  dimension_ = other.dimension();
  // Setup array
  if (coeff_ != nullptr) {
    delete [] coeff_;
  }
  coeff_ = new T[dimension_];
  // Copy data
  std::memcpy(reinterpret_cast<void*>(coeff_),
              reinterpret_cast<const void*>(other.data()),
              dimension_ * sizeof(T));
}

/*
 *  @name ~DenseVector
 *  @fn ~DenseVector(void)
 *  @brief  Destructor
 */
template <typename T>
DenseVector<T>::~DenseVector(void) {
  dimension_ = -1;
  delete [] coeff_;
}

#pragma mark -
#pragma mark Operator
/*
 *  @name DenseVector
 *  @fn DenseVector& operator=(const DenseVector& other)
 *  @brief  Assignment operator
 *  @param[in]  other Vector to assign from
 *  @return Assign vector
 */
template <typename T>
DenseVector<T>& DenseVector<T>::operator=(const DenseVector& other) {
  // Check if other == this
  if (this != &other) {
    // Can assign
    delete [] coeff_;
    // Setup dimension
    dimension_ = other.dimension();
    // Setup array
    coeff_ = new T[dimension_];
    // Copy data
    std::memcpy(reinterpret_cast<void*>(coeff_),
                reinterpret_cast<const void*>(other.data()),
                dimension_ * sizeof(T));
  }
  return *this;
}

/*
 *  @name operator[]
 *  @fn const T& operator[](int i) const
 *  @brief  Provide access to element i
 *  @param[in]  i Element of interest
 *  @return     Value of the ith element
 */
template <typename T>
const T& DenseVector<T>::operator[](int i) const {
  return coeff_[i];
}

/*
 *  @name operator[]
 *  @fn T& operator[](int i)
 *  @brief  Provide access to element i
 *  @param[in]  i Element of interest
 *  @return     Value of the ith element
 */
template <typename T>
T& DenseVector<T>::operator[](int i) {
  return coeff_[i];
}

#pragma mark -
#pragma mark Static Methods

/*
 *  @name axpy
 *  @fn static void axpy(const T a, const DenseVector& x, DenseVector* y)
 *  @brief  Blas function : y = y + a * x
 *  @param[in]  a   Scaling factor
 *  @param[in]  x   Vector
 *  @param[out] y   Resulting vector
 */
template<typename T>
void DenseVector<T>::axpy(const T a, const DenseVector& x, DenseVector* y) {
  assert(x.dimension() == y->dimension());
  for (int i = 0; i < x.dimension(); ++i) {
    (*y)[i] += a * x[i];
  }
}

/*
 *  @name scal
 *  @fn static void scal(const T a, DenseVector* x)
 *  @brief  Blas function : x = a * x
 *  @param[in]      a   Scaling factor
 *  @param[in,out]  x   Vector
 */
template<typename T>
void DenseVector<T>::scal(const T a, DenseVector* x) {
  for (int i = 0; i < x->dimension(); ++i) {
    (*x)[i] *= a;
  }
}

/*
 *  @name copy
 *  @fn static void copy(const DenseVector& x, DenseVector* y)
 *  @brief  Blas function : y <- x
 *  @param[in]   x   Input vector
 *  @param[out]  y   Copy
 */
template<typename T>
void DenseVector<T>::copy(const DenseVector& x, DenseVector* y) {
  assert(x.dimension() == y->dimension());
  for (int i = 0; i < x.dimension(); ++i) {
    (*y)[i] = x[i];
  }
}

/*
 *  @name dot
 *  @fn static T dot(const DenseVector& x,const DenseVector& y)
 *  @brief  Blas function : return xt * y
 *  @param[in]   x   Vector
 *  @param[out]  y   Vector
 */
template<typename T>
T DenseVector<T>::dot(const DenseVector& x,const DenseVector& y) {
  assert(x.dimension() == y.dimension());
  T res = 0;
  for (int i = 0 ; i < x.dimension(); ++i) {
    res += x[i] * y[i];
  }
  return res;
}

#pragma mark -
#pragma mark Explicit Definition
  
template class LTS5_EXPORTS DenseVector<float>;
template class LTS5_EXPORTS DenseVector<double>;

}  // namespace LTS5