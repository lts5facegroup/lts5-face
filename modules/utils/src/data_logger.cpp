/**
 *  @file   data_logger.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 24/03/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <algorithm>

#include "lts5/utils/data_logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name   FileDataLogger
 *  @brief  Constructor
 */
FileDataLoggerPolicy::FileDataLoggerPolicy() : file_stream_( new std::ofstream ) {}

/*
 *  @name   ~FileDataLogger
 *  @brief  Destructor
 */
FileDataLoggerPolicy::~FileDataLoggerPolicy() {
  if (file_stream_->is_open()) {
    this->CloseStream();
  }
}
/*
 *  @name   OpenStream
 *  @brief  Open a stream where data will be pushed, i.e. file/ram/socket
 *  @param[in]  stream_name name
 */
void FileDataLoggerPolicy::OpenStream(const std::string& stream_name) {
  // Open file
  file_stream_->open(stream_name.c_str(),
                     std::ios_base::out|std::ios_base::binary);
  if (!file_stream_->is_open()) {
    // Error
    throw std::runtime_error("FileDataLogger : Unable to open file stream : " + stream_name);
  }
}

/*
 *  @name   CloseStream
 *  @brief  Close the stream opened earlier
 */
void FileDataLoggerPolicy::CloseStream() {
  if (file_stream_) {
    file_stream_->close();
  }
}

/*
 *  @name   Write
 *  @brief  Write data into stream
 *  @param[in]  msg    Message to write
 */
void FileDataLoggerPolicy::Write(const std::string& msg) {
  // Write msg
  file_stream_->write(msg.c_str(), msg.length());
  // Flush stream
  file_stream_->flush();
}


/*
 *  @name   DataLogger
 *  @brief  Constructor
 *  @param  Log name
 */
template<class T,typename DataLoggerPolicy>
DataLogger<T,DataLoggerPolicy>::DataLogger(const std::string& log_name) {
  // Clear map/vector
  data_.clear();
  data_map_.clear();
  // Create new policy
  data_logger_policy_ = new DataLoggerPolicy;
  if (!data_logger_policy_) {
    throw std::runtime_error("DataLogger : Unable to create logger instance");
  }
  // Open stream
  data_logger_policy_->OpenStream(log_name);
}

/*
 *  @name   ~DataLogger
 *  @brief  Destructor
 */
template<class T,typename DataLoggerPolicy>
DataLogger<T,DataLoggerPolicy>::~DataLogger() {
  if (data_logger_policy_) {
    // Close stream
    data_logger_policy_->CloseStream();
    delete data_logger_policy_;
  }
}

/*
 *  @name   AddEntry
 *  @brief  Add value to the log
 *  @param  key
 *  @param  value
 */
template<class T,typename DataLoggerPolicy>
void DataLogger<T,DataLoggerPolicy>::AddEntry(const std::string& key,
                                              const T value) {
  // Find key with lambda function
  auto entry_it = std::find_if(data_map_.begin(),
                               data_map_.end(),
                               [key](const std::pair<std::string, int>& p)
                              {return p.first == key;});
  if(entry_it == data_map_.end()) {
    // Not in the map -> add it
    int index = (int)data_map_.size();
    data_map_.push_back(std::make_pair(key, index));
    // Push_back empty vector
    data_.push_back(std::deque<T>());
    // Push_back value
    data_[index].push_back(value);
  } else {
    int index = entry_it->second;
    typename std::vector<std::deque<T>>::iterator data_it = std::next(data_.begin(),index);
    // Add value
    data_it->push_back(value);
  }
}

/*
 *  @name   DumpData
 *  @brief  Dump data into stream
 */
template<class T,typename DataLoggerPolicy>
void  DataLogger<T,DataLoggerPolicy>::DumpData(void) {
  // Goes through the map
  auto key_it = data_map_.begin();
  typename std::vector<std::deque<T>>::const_iterator data_it;

  // Print number of Trace
  logger_stream_ << data_map_.size() << std::endl;
  data_logger_policy_->Write(logger_stream_.str());
  logger_stream_.str("");
  for (; key_it != data_map_.end(); ++key_it) {
    // Get key + index
    int index = key_it->second;
    std::string key = key_it->first;

    // Move data iterator
    data_it = std::next(data_.begin(),index);

    // Prepare data for printing
    logger_stream_.str("");
    logger_stream_ << key << std::endl;
    typename std::deque<T>::const_iterator aValIt = data_it->begin();
    for (; aValIt != data_it->end(); ++aValIt) {
      logger_stream_ << *aValIt << " ";
    }
    logger_stream_ << std::endl;
    // Print
    data_logger_policy_->Write(logger_stream_.str());
    // Clear logger
    logger_stream_.str("");
  }
}

/*
 *  @name   ClearBuffer
 *  @brief  Empty the internal buffer
 */
template<class T,typename DataLoggerPolicy>
void DataLogger<T,DataLoggerPolicy>::ClearBuffer(void) {
  // Clear map + vector
  data_map_.clear();
  data_.clear();
}

template class LTS5_EXPORTS DataLogger<double,FileDataLoggerPolicy>;
template class LTS5_EXPORTS DataLogger<float,FileDataLoggerPolicy>;
template class LTS5_EXPORTS DataLogger<int,FileDataLoggerPolicy>;
template class LTS5_EXPORTS DataLogger<short,FileDataLoggerPolicy>;
template class LTS5_EXPORTS DataLogger<char,FileDataLoggerPolicy>;
}
