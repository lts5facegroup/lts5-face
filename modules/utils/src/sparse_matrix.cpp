/**
 *  @file   sparse_matrix.cpp
 *  @brief  Sparse matrix data structure.
 *          Based on OpenNL library
 *
 *  @author Christophe Ecabert
 *  @date   12/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>

#include "lts5/utils/math/sparse_matrix.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Type definition

/*
 *  @name AddCoeff
 *  @fn void AddCoeff(int index, CoeffType value)
 *  @brief  Add a coefficient inside structure :
 *          a_{index} = a_{index} + value
 *  @param[in]  index Position where to add value
 *  @param[in]  value Value to add
 */
template<typename T>
void SparseMatrix<T>::Row::AddCoeff(int index, T value) {
  // Search index into structure
  typename superclass::iterator it = superclass::begin();
  for (; it != superclass::end(); ++it) {
    if (it->index_ == index) {
      it->value_ += value;
      return;
    }
  }
  // Index not found -> new element
  superclass::push_back(Coeff(index, value));
}

/*
 *  @name SetCoeff
 *  @fn void SetCoeff(int index, CoeffType value, bool is_new)
 *  @brief  Set a coefficient inside structure :
 *          a_{index} = value
 *  @param[in]  index   Position where to add value
 *  @param[in]  value   Value to set
 *  @param[in]  is_new  Set to true if new element in matrix
 */
template<typename T>
void SparseMatrix<T>::Row::SetCoeff(int index, T value, bool is_new) {
  // Coefficient already in the matrix ?
  if (!is_new) {
    // Search it,
    typename superclass::iterator it = superclass::begin();
    for (; it != superclass::end(); ++it) {
      if (it->index_ == index) {
        it->value_ = value;
        return;
      }
    }
  }
  // Coefficient doesn't exist yet if we reach this point
  superclass::push_back(Coeff(index, value));
}

/*
 *  @name GetCoeff
 *  @fn CoeffType GetCoeff(int index) const
 *  @brief  Provide coefficient's value at position index, default value = 0
 *          return a_{index} (0 by default)
 *  @param[in]  index Position
 *  @return     Value at index
 */
template<typename T>
T SparseMatrix<T>::Row::GetCoeff(int index) const {
  // Search for coefficient into superclass structure
  typename superclass::const_iterator it = superclass::begin();
  for (; it != superclass::end(); ++it) {
    if (it->index_ == index) {
      return it->value_;
    }
  }
  // Coefficient doesn't exist if we reach this point
  return 0;
}

#pragma mark -
#pragma mark Initialization

/**
 *  @name SparseMatrix
 *  @fn explicit SparseMatrix(int dimension)
 *  @brief  Constructor
 *  @param[in]  dimension Matrix dimension (square)
 */
template<typename T>
SparseMatrix<T>::SparseMatrix(int dimension) {
  assert(dimension > 0);
  // Define dimension
  dimension_ = dimension;
  // Setup rows
  row_ = new Row[dimension_];
}

/*
 *  @name SparseMatrix
 *  @fn SparseMatrix(int rows, int columns)
 *  @brief  Constructor for rectangle matrix (Warning support only square
 *          matrix at the moment)
 *  @param[in]  rows    Number of rows in the matrix
 *  @param[in]  coumns  Number of columns in the matrix
 */
template<typename T>
SparseMatrix<T>::SparseMatrix(int rows, int columns) {
  assert(rows == columns && rows > 0);
  // Setup dimension
  dimension_ = rows;
  // Setup rows structure
  row_ = new Row[dimension_];
}

/*
 *  @name ~SparseMatrix
 *  @fn ~SparseMatrix(void)
 *  @brief  Destructor
 */
template<typename T>
SparseMatrix<T>::~SparseMatrix(void) {
  delete [] row_;
  row_ = nullptr;
}

/*
 *  @name nnz
 *  @fn int nnz(void)
 *  @brief  Give the number of non-zero elements inside the matrix
 *  @return Number of non-zero element
 */
template<typename T>
int SparseMatrix<T>::nnz(void) const {
  int nnz = 0;
  for (int i = 0; i < dimension_; ++i) {
    nnz += this->row(i).size();
  }
  return nnz;
}

#pragma mark -
#pragma mark Operator
/*
 *  @name operator*
 *  @fn DenseVector<T>& operator*(const DenseVector<T>& rhs)
 *  @brief  Matrix - vector multiplication y = Mx
 *  @param[in]  x Input vector
 *  @return     return product vector (e.g. y)
 */
template<typename T>
const DenseVector<T> SparseMatrix<T>::operator*(const DenseVector<T>& x) {
  // Check if rhs is correct size
  assert(x.dimension() == dimension_);
  // Init return vector
  DenseVector<T> y = DenseVector<T>(dimension_);
  // Perform mutliplcation
  for (int i = 0; i < dimension_; ++i) {
    y[i] = 0;
    const typename SparseMatrix<T>::Row& R = this->row(i);
    int j = 0;
    for (int jj = 0; jj < R.size(); ++jj) {
      j = R[jj].index_;
      y[i] += R[jj].value_ * x[j];
    }
  }
  return y;
}

/*
 *  @name operator*
 *  @fn DenseVector<T>& operator*(const DenseVector<T>& rhs)
 *  @brief  Matrix - vector multiplication y = Mx
 *  @param[in]  x Input vector
 *  @return     return product vector (e.g. y)
 */
template<typename T>
const DenseVector<T> SparseMatrix<T>::operator*(const DenseVector<T>& x) const {
  // Check if rhs is correct size
  assert(x.dimension() == dimension_);
  // Init return vector
  DenseVector<T> y = DenseVector<T>(dimension_);
  // Perform mutliplcation
  for (int i = 0; i < dimension_; ++i) {
    y[i] = 0;
    const typename SparseMatrix<T>::Row& R = this->row(i);
    int j = 0;
    for (int jj = 0; jj < R.size(); ++jj) {
      j = R[jj].index_;
      y[i] += R[jj].value_ * x[j];
    }
  }
  return y;
}

#pragma mark -
#pragma mark Explicit Definition
  
template class LTS5_EXPORTS SparseMatrix<float>;
template class LTS5_EXPORTS SparseMatrix<double>;

}  // namespace LTS5