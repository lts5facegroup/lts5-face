/**
 *  @file   webcam_video_stream.cpp
 *  @brief  Utility class to handle webcam video stream
 *
 *  @author Christophe Ecabert
 *  @date   16/11/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#include <stdio.h>

#include "lts5/utils/webcam_video_stream.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name WebcamStream
 *  @fn WebcamStream(void)
 *  @brief  Constructor
 */
WebcamStream::WebcamStream(void) {
  // Open default capture stream
  cap_ = new cv::VideoCapture();
}

/*
 *  @name WebcamStream
 *  @fn explicit WebcamStream(const int idx)
 *  @brief  Constructor
 *  @param[in]  idx Index of the camera to open
 */
WebcamStream::WebcamStream(const int idx) {
  // Open specific capture stream
  cap_ = new cv::VideoCapture(idx);
}

/*
 *  @name Open
 *  @fn bool Open(const int idx)
 *  @brief Open a specific camera stream
 *  @return false if error, true otherwise
 */
bool WebcamStream::Open(const int idx) {
  return cap_->open(idx);
}

/*
 *  @name ~WebcamStream
 *  @fn ~WebcamStream(void)
 *  @brief  Destructor
 */
WebcamStream::~WebcamStream(void) {
  if (cap_) {
    cap_->release();
    delete cap_;
  }
}

/*
 *  @name IsOpen
 *  @fn bool IsOpen(void)
 *  @brief  Indicate if the stream has been properly initialized
 *  @return false if error, true otherwise
 */
bool WebcamStream::IsOpen(void) {
  return (cap_ != nullptr ? cap_->isOpened() : false);
}

#pragma mark -
#pragma mark Process

/*
 *  @name Grab
 *  @fn bool Grab(void)
 *  @brief  Grab a frame from the current stream. Trigger the acquisition
 *          (fast methods)
 *  @return True on success, false otherwise
 */
bool WebcamStream::Grab(void) {
  return cap_->grab();
}

/*
 *  @name Retrieve
 *  @fn void Retrieve(cv::Mat* frame)
 *  @brief  This method decode and retrieved the last grabbed frame
 *  @param[out] frame   Acquired frame
 */
void WebcamStream::Retrieve(cv::Mat* frame) {
  cap_->retrieve(*frame);
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name GetProperty
 *  @fn double GetProperty(const int property_id) const
 *  @brief  Get the value of a given properties
 *  @param[in]  property_id   Id of the property to look for.
 *  @return Property value
 */
double WebcamStream::GetProperty(const int property_id) const {
  return cap_->get(property_id);
}

/*
 *  @name SetProperty
 *  @fn void SetProperty(const int property_id, const double value)
 *  @brief  Set the value of a specific property
 *  @param[in]  property_id   Id of the property to look for.
 *  @param[in]  value         Value of the property
 */
void WebcamStream::SetProperty(const int property_id, const double value) {
  cap_->set(property_id, value);
}

}  // namespace LTS5
