/**
 *  @file   tee_stream.cpp
 *  @brief  Mechanism to duplicate stream and dump information into various
 *          streams. I.e. file + console similar to `tee` on linux
 *  @ingroup utils
 *  @see    https://stackoverflow.com/a/1761027/4546884
 *
 *  @author Christophe Ecabert
 *  @date   9/12/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <functional>
#include <algorithm>

#include "lts5/utils/tee_stream.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name    InsertBuffer
 * @fn  void InsertBuffer(std::streambuf* streambuf)
 * @brief Add a streambuf where to mirror the data.
 * @param[in] streambuf Stream to add
 */
void TeeStreambuf::InsertBuffer(std::streambuf *streambuf) {
  streams_.push_back(streambuf);
}

/*
 *  @name   overflow
 *  @fn     virtual int_type overflow(int_type c) override
 *  @brief  C.f. C++ standard section 27.5.2.4.5
 */
TeeStreambuf::int_type TeeStreambuf::overflow(int_type c) {
  if (!traits_type::eq_int_type(c, traits_type::eof())) {
    // Dump into streams, not eof
    bool is_eof = false;
    for (auto* s : streams_) {
      auto r = s->sputc(traits_type::to_char_type(c));
      is_eof |= traits_type::eq_int_type(r, traits_type::eof());
    }
    return is_eof ? traits_type::eof() : c;
  } else {
    return traits_type::not_eof(c);
  }
}

/*
 *  @brief  Synchronizes the buffer arrays with the controlled sequences.
 *  @return  -1 on failure.
 *
 *  Each derived class provides its own appropriate behavior,
 *  including the definition of @a failure.
 *  @note  Base class version does nothing, returns zero.
*/
int TeeStreambuf::sync() {
  int err = 0;
  for (auto* s : streams_) {
    err |= s->pubsync();
  }
  return err;
}

/*
 * @name  TeeStream
 * @fn    TeeStream()
 * @brief Constructor
 */
TeeStream::TeeStream() : std::ostream(nullptr) {
  this->init(&streambuf_);
}

/*
 * @name  LinkStream
 * @fn    void LinkStream(std::ostream& stream)
 * @brief Link this stream with another one (Aggregation)
 * @param[in] stream Stream to link with
 */
void TeeStream::LinkStream(std::ostream& stream) {
  stream.flush();
  streambuf_.InsertBuffer(stream.rdbuf());
}

}  // namespace LTS5