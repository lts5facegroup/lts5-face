/**
 *  @file   lts5/utils.hpp
 *
 *  @author Christophe Ecabert
 *  @date   22/04/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include "lts5/utils/utils.hpp"