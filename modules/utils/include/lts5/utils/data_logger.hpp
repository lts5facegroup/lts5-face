/**
 *  @file   data_logger.hpp
 *  @brief  Logger for data
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date 24/03/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_DATA_LOGGER__
#define __LTS5_DATA_LOGGER__

#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include <deque>
#include <sstream>
#include <memory>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  DataLoggerInterface
 *  @brief  Public interface for data logging
 *  @author Christophe Ecabert
 *  @date   24/03/15
 *  @ingroup utils
 */
class DataLoggerInterface {
 public :

  /**
   *  @name ~DataLoggerInterface
   *  @fn virtual ~DataLoggerInterface(void)
   *  @brief  Destructor
   */
  virtual ~DataLoggerInterface(void) {};
  /**
   *  @name   OpenStream
   *  @fn virtual void OpenStream(const std::string& stream_name) = 0
   *  @brief  Open a stream where data will be pushed, i.e. file/ram/socket
   *  @param[in]  stream_name     Stream name
   */
  virtual void OpenStream(const std::string& stream_name) = 0;

  /**
   *  @name   CloseStream
   *  @fn virtual void CloseStream() = 0
   *  @brief  Close the stream opened earlier
   */
  virtual void CloseStream() = 0;

  /**
   *  @name   Write
   *  @fn virtual void Write(const std::string& msg) = 0
   *  @brief  Write data into stream
   *  @param[in]  msg    Message to write
   */
  virtual void Write(const std::string& msg) = 0;
};


/**
 *  @class  FileDataLoggerPolicy
 *  @brief  Data logger for file output
 *  @author Christophe Ecabert
 *  @date   24/03/15
 *  @ingroup utils
 */
class LTS5_EXPORTS FileDataLoggerPolicy : public DataLoggerInterface {
 private :
  /** File output stream */
  std::unique_ptr<std::ofstream> file_stream_;

 public :
  /**
   *  @name   FileDataLogger
   *  @fn FileDataLoggerPolicy()
   *  @brief  Constructor
   */
  FileDataLoggerPolicy();

  /**
   *  @name   ~FileDataLogger
   *  @fn ~FileDataLoggerPolicy()
   *  @brief  Destructor
   */
  ~FileDataLoggerPolicy();

  /**
   *  @name   OpenStream
   *  @fn void OpenStream(const std::string& stream_name)
   *  @brief  Open a stream where data will be pushed, i.e. file/ram/socket
   *  @param[in]  stream_name     Stream name
   */
  void OpenStream(const std::string& stream_name);

  /**
   *  @name   CloseStream
   *  @fn void CloseStream()
   *  @brief  Close the stream opened earlier
   */
  void CloseStream();

  /**
   *  @name   Write
   *  @fn void Write(const std::string& msg)
   *  @brief  Write data into stream
   *  @param[in]  msg    Message to write
   */
  void Write(const std::string& msg);
};

/**
 *  @class  DataLogger
 *  @brief  Data logger
 *  @author Christophe Ecabert
 *  @date   24/03/15
 *  @ingroup utils
 */
template<class T,typename DataLoggerPolicy>
class LTS5_EXPORTS DataLogger {
 private :
  /** Policy, how data will be written */
  DataLoggerPolicy* data_logger_policy_;
  /** Convert data to string stream */
  std::stringstream logger_stream_;
  /** Mapping between entry name and indexes */
  std::vector<std::pair<std::string,int>> data_map_;
  /** Traces */
  std::vector<std::deque<T>> data_;

 public :
  /**
   *  @name   DataLogger
   *  @fn DataLogger(const std::string& log_name)
   *  @brief  Constructor
   *  @param[in]  log_name    Log's name
   */
  explicit DataLogger(const std::string& log_name);

  /**
   *  @name   ~DataLogger
   *  @fn ~DataLogger()
   *  @brief  Destructor
   */
  ~DataLogger();

  /**
   *  @name   AddEntry
   *  @fn void AddEntry(const std::string& key,const T value)
   *  @brief  Add value to the log
   *  @param[in]  key
   *  @param[in]  value
   */
  void AddEntry(const std::string& key,const T value);

  /**
   *  @name   DumpData
   *  @fn void DumpData(void)
   *  @brief  Dump data into stream
   */
  void DumpData(void);

  /**
   *  @name   ClearBuffer
   *  @fn void ClearBuffer(void)
   *  @brief  Empty the internal buffer
   */
  void ClearBuffer(void);
};
}


#endif /* __LTS5_DATA_LOGGER__ */
