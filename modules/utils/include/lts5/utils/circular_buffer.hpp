/**
 *  @file   circular_buffer.hpp
 *  @brief  Circular buffer implementation
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   27/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __CIRCULAR_BUFFER__
#define __CIRCULAR_BUFFER__

#include <deque>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/circular_iterator.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   CircularBuffer
 * @brief   Templated circular buffer implementation
 *          Buff must follow standard stl container rule.
 * @author  Christophe Ecabert
 * @date    27/05/16
 * @ingroup utils
 */
template<typename T, typename Buff=std::deque<T>>
class LTS5_EXPORTS CircularBuffer {

 public:

#pragma mark -
#pragma mark Type definition

  /** Container iterator type */
  using BufferIt = typename Buff::iterator;
  /** Container const iterator type */
  using ConstBufferIt = typename Buff::const_iterator;
  /** Output iterator type */
  using CircularIt = CircularIterator<T, BufferIt>;
  /** Output iterator type */
  using ConstCircularIt = CircularIterator<T, ConstBufferIt>;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  CircularBuffer
   * @fn  explicit CircularBuffer(const size_t capacity)
   * @brief Constructor
   * @param[in] capacity  Buffer capacity
   */
  explicit CircularBuffer(const size_t capacity) : capacity_(capacity),
                                                   n_element_(0) {
    // Create buffer
    buffer_ = Buff(capacity_);
    // Init iterator
    input_ = CircularIt(buffer_.begin(), buffer_.end());
    output_ = CircularIt(buffer_.begin(), buffer_.end());
  }

  /**
   * @name  CircularBuffer
   * @fn  CircularBuffer(const CircularBuffer& other) = delete
   * @brief Copy Constructor
   * @param[in] other  Object to copy from
   */
  CircularBuffer(const CircularBuffer& other) = delete;

  /**
   * @name  operator=
   * @fn  CircularBuffer& operator=(const CircularBuffer& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs  Object to assign from
   * @return  Assigned object
   */
  CircularBuffer& operator=(const CircularBuffer& rhs) = delete;

  /**
   * @name  ~CircularBuffer
   * @fn  ~CircularBuffer(void)
   * @brief Destructor
   */
  ~CircularBuffer(void) {}

#pragma mark -
#pragma mark Usage

  /**
   * @name  empty
   * @fn  bool empty(void) const
   * @brief   Indicates if buffer is empty
   * @return  True if empty, false otherwise
   */
  bool empty(void) const {
    return n_element_ == 0;
  }

  /**
   * @name  full
   * @fn  bool full(void) const
   * @brief Indicate if buffer is full
   * @return  True if complete, false otherwise
   */
  bool full(void) const {
    return n_element_ == capacity_;
  }

  /**
   * @name  size
   * @fn  std::size_t size(void) const
   * @brief   Current buffer size
   * @return  size
  */
  std::size_t size(void) const {
    return n_element_;
  }

  /**
   * @name  capacity
   * @fn  std::size_t capacity(void) const
   * @brief   Buffer capacity
   * @return  capacity
  */
  std::size_t capacity(void) const {
    return capacity_;
  }

  /**
   * @name  push_back
   * @fn  void push_back(const T& v)
   * @brief Add new element in the buffer
   * @param[in] v Value to insert in the buffer
   */
  void push_back(const T& v) {
    if (n_element_ < capacity_) {
      *input_ = v;
      n_element_ += 1;
      ++input_;
    }
  }

  /**
   * @name  push_back
   * @fn  void push_back(const T& v)
   * @brief Add new element in the buffer
   * @param[in] v Value to insert in the buffer
   */
  T& pop_back(void) {
    T& v = *output_;
    if (n_element_ > 1) {
      n_element_ -= 1;
      ++output_;
    }
    return v;
  }

  /**
   * @name begin
   * @fn  ConstCircularIt begin(void) const
   * @brief Provide first iterator
   * @return  Begin iterator
   */
  CircularIt begin(void) const {
    return output_;
  }

  /**
   * @name end
   * @fn  ConstCircularIt end(void) const
   * @brief Provide last iterator
   * @return  End iterator
   */
  CircularIt end(void) const {
    return input_;
  }

#pragma mark -
#pragma mark Private
 private:
  /** Buffer */
  Buff buffer_;
  /** Buffer capacity */
  std::size_t capacity_;
  /** Number of element inside buffer (Should be : in - out) */
  std::size_t n_element_;
  /** Input iterator */
  CircularIt input_;
  /** Output iterator */
  CircularIt output_;
};
}  // namepsace LTS5
#endif //__CIRCULAR_BUFFER__
