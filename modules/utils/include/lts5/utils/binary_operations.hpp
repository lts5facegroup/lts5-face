/**
 *  @file   binary_operations.hpp
 *  @brief  Operations to swap between big and small endian
 *  @ingroup utils
 *
 *  @author Marina Zimmermann
 *  @date   03/10/16
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */

#ifndef __LTS5_binary_operations_hpp__
#define __LTS5_binary_operations_hpp__

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @name   ShortSwap
 *  @fn short ShortSwap(short s)
 *  @brief  Swap a short int
 *  @param[in]  s   Short int
 *  @return swapped short
 *  @ingroup utils
 */
short ShortSwap (short s);

/**
 *  @name   LongSwap
 *  @fn int LongSwap (int i)
 *  @brief  Swap a long int
 *  @param[in]  i   Long int
 *  @return swapped int
 *  @ingroup utils
 */
int LongSwap (int i);

/**
 *  @name   FloatSwap
 *  @fn int FloatSwap(float f)
 *  @brief  Swap a float
 *  @param[in]  f   Float
 *  @return swapped float
 *  @ingroup utils
 */
float FloatSwap (float f);



} // LTS5 namespace

#endif //__LTS5_binary_operations_hpp__
