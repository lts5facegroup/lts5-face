/**
 *  @file   "lts5/utils/file_io.hpp"
 *  @brief  Input/Output file data transfert
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   22/04/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_FILE_IO__
#define __LTS5_FILE_IO__

#include <string>
#include <fstream>
#include <unordered_set>
#include <ostream>
#include <istream>

#include "opencv2/core.hpp"

#include "lts5/utils/object_type.hpp"
#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"

namespace Eigen {
template<typename T, int, int, int, int, int>
class Matrix;
}

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5
{
/**
 *  @name   ReadMatFromChar
 *  @fn int ReadMatFromChar(std::istream& input_stream, cv::Mat* matrix)
 *  @brief  Load matrix from input stream
 *  @ingroup utils
 *  @param[in]  input_stream    Stream to data file location
 *  @param[out] matrix          Matrix with data loaded
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS ReadMatFromChar(std::istream& input_stream, cv::Mat* matrix);

/**
 *  @name   SaveMatToChar
 *  @fn int WriteMatToChar(std::ostream& output_stream,const cv::Mat& matrix)
 *  @brief  Write matrix to output stream
 *  @ingroup utils
 *  @param[in]  output_stream   Output stream to filen
 *  @param[out] matrix          Matrix to write
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS WriteMatToChar(std::ostream& output_stream,
                                const cv::Mat& matrix);

/**
 *  @name   ReadMatFromBin
 *  @fn int ReadMatFromBin(std::istream& input_stream, cv::Mat* matrix)
 *  @brief  Load matrix from binary stream
 *  @ingroup utils
 *  @param[in]  input_stream    Stream to data file location
 *  @param[out] matrix          Matrix with data loaded.
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS ReadMatFromBin(std::istream& input_stream, cv::Mat* matrix);

/**
 *  @name   ReadEigenFromBin
 *  @fn int ReadEigenFromBin(std::istream& input_stream,
                             Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>* matrix)
 *  @brief  Load eigen matrix from binary stream
 *  @ingroup utils
 *  @param[in]  input_stream    Stream to data file location
 *  @param[out] matrix          Matrix with data loaded.
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int LTS5_EXPORTS ReadEigenFromBin(std::istream& input_stream,
                     Eigen::Matrix<T, -1, -1, 0, -1, -1>* matrix);

/**
 *  @name   ReadMatFromBinTyped
 *  @fn int ReadMatFromBinTyped(std::istream& input_stream, cv::Mat* matrix)
 *  @brief  Load matrix from binary stream into a matrix of a given type. Type
 *          conversion may occurs in the loading chain.
 *  @ingroup utils
 *  @param[in]  input_stream    Stream to data file location
 *  @param[out] matrix          Matrix with data loaded.
 *  @return -1 if error, 0 otherwise
 *  @tparam T Output matrix data  type wanted
 */
template<typename T>
int LTS5_EXPORTS ReadMatFromBinTyped(std::istream& input_stream,
                                     cv::Mat* matrix);

/**
 *  @name   WriteMatToBin
 *  @fn int WriteMatToBin(std::ostream& output_stream,const cv::Mat& matrix)
 *  @brief  Write matrix to binary output stream
 *  @ingroup utils
 *  @param[in]  output_stream   Output stream to filen
 *  @param[in] matrix          Matrix to write
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS WriteMatToBin(std::ostream& output_stream,
                               const cv::Mat& matrix);

/**
 *  @name   WriteMatToBin
 *  @fn int WriteMatToBin(std::ostream& output_stream,
                const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& matrix)
 *  @brief  Write eigen matrix to binary output stream
 *  @ingroup utils
 *  @param[in]  output_stream   Output stream to filen
 *  @param[in] matrix          Matrix to write
 *  @return -1 if error, 0 otherwise
 *  @tparam T data type
 */
template<typename T>
int LTS5_EXPORTS WriteEigenToBin(std::ostream& output_stream,
                const Eigen::Matrix<T, -1, -1, 0, -1, -1>& matrix);

/**
 *  @name   ReadMatFromHtkBin
 *  @fn int ReadMatFromHtkBin(std::istream& input_stream, cv::Mat* matrix)
 *  @brief  Load matrix from HTK data binary stream
 *  @ingroup utils
 *  @param[in]  input_stream    Stream to HTK data file location
 *  @param[out] matrix          Matrix with data loaded.
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS ReadMatFromHtkBin(std::istream& input_stream, cv::Mat* matrix);


/**
 *  @name   WriteMatToHtkBin
 *  @fn int WriteMatToHtkBin(std::ostream& output_stream,const cv::Mat& matrix)
 *  @brief  Write matrix to HTK data binary output stream
 *  @ingroup utils
 *  @param[in]  output_stream   Output HTK data stream to file
 *  @param[in] matrix           Matrix to write
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS WriteMatToHtkBin(std::ostream& output_stream,
                               const cv::Mat& matrix);

/**
 *  @name   LoadMatFromChar
 *  @fn int LoadMatFromChar(const std::string& filename, cv::Mat* matrix)
 *  @brief  Load matrix from text file
 *  @ingroup utils
 *  @param[in]  filename    Input filename
 *  @param[out] matrix      Matrix where data will be loaded
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS LoadMatFromChar(const std::string& filename, cv::Mat* matrix);

/**
 *  @name   LoadMatFromBin
 *  @fn int LoadMatFromBin(const std::string& filename, cv::Mat* matrix)
 *  @brief  Load matrix from binary file
 *  @ingroup utils
 *  @param[in]  filename    Input filename
 *  @param[out] matrix      Matrix where data will be loaded
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS LoadMatFromBin(const std::string& filename, cv::Mat* matrix);

/**
 *  @name   LoadMatFromBinTyped
 *  @fn int LoadMatFromBinTyped(const std::string& filename, cv::Mat* matrix)
 *  @brief  Load matrix from binary file into a given typed matrix.
 *  @ingroup utils
 *  @param[in]  filename    Input filename
 *  @param[out] matrix      Matrix where data will be loaded
 *  @return -1 if error, 0 otherwise
 *  @tparam Output matrix data type
 */
template<typename T>
int LTS5_EXPORTS LoadMatFromBinTyped(const std::string& filename,
                                     cv::Mat* matrix);

/**
 *  @name   SaveMatToChar
 *  @fn int SaveMatToChar(const std::string& filename,const cv::Mat& matrix)
 *  @brief  Save matrix to text file
 *  @ingroup utils
 *  @param[in]  filename    Input filename
 *  @param[in]  matrix      Matrix where data will be loaded
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS SaveMatToChar(const std::string& filename,
                               const cv::Mat& matrix);

/**
 *  @name   SaveMatToBin
 *  @fn int SaveMatToBin(const std::string& filename,const cv::Mat& matrix)
 *  @brief  Save matrix to binary file
 *  @ingroup utils
 *  @param[in]  filename    Input filename
 *  @param[in]  matrix      Matrix where data will be loaded
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS SaveMatToBin(const std::string& filename,
                              const cv::Mat& matrix);

/**
 *  @name   MatrixSetValue
 *  @fn void MatrixSetValue(const int& row, const int& col,
                            const double value,
                            cv::Mat* matrix)
 *  @brief  Set the value at position [row,col] according to the matrix depth
 *  @ingroup utils
 *  @param[in]  row     Row index
 *  @param[in]  col     Col index
 *  @param[in]  value   Value
 *  @param[out] matrix  Destination matrix
 */
void LTS5_EXPORTS MatrixSetValue(const int& row, const int& col,
                                 const double value,
                                 cv::Mat* matrix);

/**
 *  @name   GetMatrixValue
 *  @fn double GetMatrixValue(const int& row, const int& col, const cv::Mat& matrix)
 *  @brief  Get the value at position [row,col], cast the value according
 *          to the matrix depth
 *  @ingroup utils
 *  @param[in]  row     Row index
 *  @param[in]  col     Col index
 *  @param[in] matrix  Matrix
 *  @return value
 */
double LTS5_EXPORTS GetMatrixValue(const int& row,
                                   const int& col,
                                   const cv::Mat& matrix);

/**
 *  @name   ReadMatFromCSV
 *  @fn int ReadMatFromCSV(const std::string& filename,
                           cv::Mat* matrix)
 *  @brief  Load matrix of a given type from .csv file
 *  @ingroup utils
 *  @param[in]  filename    File holding matrix data
 *  @param[out] matrix      Destination matrix
 *  @return -1 if error, 0 otherwise
 */
template<typename T>
int LTS5_EXPORTS ReadMatFromCSV(const std::string& filename,
                                cv::Mat* matrix);


/**
 *  @name   ScanInFolder
 *  @fn std::vector<std::string> ScanInFolder(const std::string& directory,
                                              std::vector<std::string>& exts,
                                              bool sorted)
 *  @brief  Scan in the give folder and return all files with given extensions
 *  @ingroup utils
 *  @param[in]  directory  path where the files are located
 *  @param[in]  exts       vector of possible file extensions
 *  @param[in]  sorted     if true, sort the output
 *  @return vector of file names in string
 */
std::vector<std::string> LTS5_EXPORTS ScanInFolder(const std::string& directory,
                                                         std::vector<std::string>& exts,
                                                         bool sorted);

/**
 *  @name   ScanImagesInFolder
 *  @fn std::vector<std::string> ScanImagesInFolder(const std::string& directory)
 *  @brief  Scan in the give folder and return all image files (unsorted)
 *  @ingroup utils
 *  @param[in]  directory  path where the images are located
 *  @return vector of file names in string
 */
std::vector<std::string> LTS5_EXPORTS ScanImagesInFolder(const std::string& directory);

/**
 *  @name   ScanImagesInFolder
 *  @fn std::vector<std::string> ScanImagesInFolder(const std::string& directory,
                                                    bool sorted)
 *  @brief  Scan in the give folder and return all image files
 *  @ingroup utils
 *  @param[in]  directory  path where the images are located
 *  @param[in]  sorted     if true, sort the output
 *  @return vector of file names in string
 */
std::vector<std::string> LTS5_EXPORTS ScanImagesInFolder(const std::string& directory,
                                                         bool sorted);

/**
 *  @name   ScanVideosInFolder
 *  @fn std::vector<std::string> ScanVideosInFolder(const std::string& directory)
 *  @brief  Scan in the give folder and return all video files (unsorted)
 *  @ingroup utils
 *  @param[in]  directory  path where the videos are located
 *  @return vector of file names in string
 */
std::vector<std::string> LTS5_EXPORTS ScanVideosInFolder(const std::string& directory);

/**
 *  @name   ScanVideosInFolder
 *  @fn std::vector<std::string> ScanVideosInFolder(const std::string& directory,
                                                    bool sorted)
 *  @brief  Scan in the give folder and return all video files
 *  @ingroup utils
 *  @param[in]  directory  path where the videos are located
 *  @param[in]  sorted     if true, sort the output
 *  @return vector of file names in string
 */
std::vector<std::string> LTS5_EXPORTS ScanVideosInFolder(const std::string& directory,
                                                         bool sorted);

/**
 *  @name   ScanFoldersInFolder
 *  @fn std::vector<std::string> ScanFoldersInFolder(const std::string& directory)
 *  @brief  Scan in the give folder and return all folders inside (unsorted)
 *  @ingroup utils
 *  @param[in]  directory  path where the videos are located
 *  @return vector of folders names in string
 */
std::vector<std::string> LTS5_EXPORTS ScanFoldersInFolder(const std::string& directory);

/**
 *  @name   LoadPts
 *  @fn void LoadPts(const std::string& filename,
                     const std::unordered_set<int>& indices,
                     cv::Mat& data)
 *  @brief  Load annotation file
 *  @ingroup utils
 *  @param[in]  filename  Path to the annotation file
 *  @param[in]  indices   List of index to load
 *  @param[out] data      Where annotation will be loaded
 */
void LTS5_EXPORTS LoadPts(const std::string& filename,
                          const std::unordered_set<int>& indices,
                          cv::Mat& data);

/**
 *  @name   LoadPts
 *  @fn void LoadPts(const std::string& filename, cv::Mat& data)
 *  @brief  Load annotation file
 *  @ingroup utils
 *  @param[in]  filename  Path to the annotation file
 *  @param[out] data      Where annotation will be loaded
 */
void LTS5_EXPORTS LoadPts(const std::string& filename, cv::Mat& data);


/**
 *  @name   SavePts
 *  @fn void SavePts(const std::string& filename, const cv::Mat& data)
 *  @brief  Save annotations into file
 *  @ingroup utils
 *  @param[in]  filename  Path to the annotation file
 *  @param[in]  data      Annotation to be saved
 */
void LTS5_EXPORTS SavePts(const std::string& filename, const cv::Mat& data);

/**
 *  @name ScanStreamForObject
 *  @fn int ScanStreamForObject(std::istream& input_stream,
                                const HeaderObjectType& object_type)
 *  @brief  Scan a given stream to find a certain type of object
 *  @ingroup utils
 *  @param[in]  input_stream  Input binary stream
 *  @param[in]  object_type   Type of object to search for
 *  @return     0 if found, -1 otherwise (error / not found)
 */
int LTS5_EXPORTS ScanStreamForObject(std::istream& input_stream,
                                     const HeaderObjectType& object_type);

/**
 *  @name   PrintStreamContent
 *  @fn int PrintStreamContent(std::istream& input_stream)
 *  @brief  Print the content of a binary file
 *  @ingroup utils
 *  @param[in]  input_stream  Binary stream to a file
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS PrintStreamContent(std::istream& input_stream);

/**
 *  @name GetFileListing
 *  @fn void GetFileListing(std::vector<std::string> &files,
                           const std::string &dir,
                           const std::string &subdir = "",
                           const std::string &ext = ".jpg")
 *  @brief  Get a list with the name of the whole file into a specific
 *          directory
 *  @ingroup utils
 *  @param[out] files   Returned value as a vector string
 *  @param[in]  dir     Full name the directory to check
 *  @param[in]  subdir  Subfolder name, initialise with ""
 *  @param[in]  ext     Extension file type, DAFAULT = ".jpg"
 */
void LTS5_EXPORTS GetFileListing(std::vector<std::string> &files,
                                 const std::string &dir,
                                 const std::string &subdir = "",
                                 const std::string &ext = ".jpg");

/**
 *  @name   ReadNode
 *  @brief  Search a key in a given node and output its value if present
 *  @ingroup utils
 *  @param[in]  node    Node to search in
 *  @param[in]  key     Key to search for
 *  @param[out] value   Value associated for the given key
 */
template<class T>
void LTS5_EXPORTS ReadNode(const cv::FileNode& node,
                           const std::string& key,
                           T* value);

/**
 *  @name CompressMemoryBuffer
 *  @fn void CompressMemoryBuffer(const void* input_buffer,
                                   const int input_size,
                                   std::vector<unsigned char>* compressed_buffer)
 *  @brief  Compress a given buffer
 *  @ingroup utils
 *  @see  http://stackoverflow.com/questions/4538586/how-to-compress-a-buffer-with-zlib
 *  @param[in]  input_buffer      Input data to compress
 *  @param[in]  input_size        Input buffer size in bytes
 *  @param[out] compressed_buffer Compressed buffer
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS CompressMemoryBuffer(const void* input_buffer,
                                      const int input_size,
                                      std::vector<unsigned char>* compressed_buffer);

/**
 *  @name UncompressMemoryBuffer
 *  @fn void UncompressMemoryBuffer(const void* input_buffer,
                                    const int input_size,
                                    std::vector<unsigned char>* uncompressed_buffer)
 *  @brief  Uncompress a given buffer
 *  @ingroup utils
 *  @see  http://www.zlib.net/zpipe.c
 *  @param[in]  input_buffer        Input data to uncompress
 *  @param[in]  input_size          Input buffer size in bytes
 *  @param[out] uncompressed_buffer Uncompressed buffer
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS UncompressMemoryBuffer(const void* input_buffer,
                                        const int input_size,
                                        std::vector<unsigned char>* uncompressed_buffer);

/**
 *  @name   WriteCompressedMatToBin
 *  @fn int WriteCompressedMatToBin(std::ostream& output_stream,const cv::Mat& matrix)
 *  @brief  Write compressed matrix to binary output stream
 *  @ingroup utils
 *  @param[in]  output_stream   Output stream to filen
 *  @param[in]  matrix          Matrix to write
 *  @return -1 if error, number of bytes written otherwise
 */
int LTS5_EXPORTS WriteCompressedMatToBin(std::ostream& output_stream,
                                         const cv::Mat& matrix);

/**
 *  @name   ReadCompressedMatFromBin
 *  @fn int ReadCompressedMatFromBin(std::istream& input_stream, cv::Mat* matrix)
 *  @brief  Load compressed matrix from binary stream
 *  @ingroup utils
 *  @param[in]  input_stream    Stream to data file location
 *  @param[out] matrix          Matrix with data loaded.
 *  @return -1 if error, 0 otherwise
 */
int LTS5_EXPORTS ReadCompressedMatFromBin(std::istream& input_stream,
                                          cv::Mat* matrix);
}  // namespace LTS5
#endif
