/**
 *  @file   signal.hpp
 *  @brief  Observer pattern implementation
 *          Based on :
 *          http://simmesimme.github.io/tutorials/2015/09/20/signal-slot
 *          https://juanchopanzacpp.wordpress.com/2013/02/24/simple-observer-pattern-implementation-c11/
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   01/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_SIGNAL__
#define __LTS5_SIGNAL__

#include <map>
#include <functional>
#include <vector>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   Signal
 * @brief   Observer pattern implementation
 * @author  Christophe Ecabert
 * @date    01/11/2016
 * @ingroup utils
 */
template<typename Event, typename Sig>
class LTS5_EXPORTS Signal{};

/**
 * @class   Signal<Event, R(Args...)>
 * @brief   Observer pattern implementation
 *          Specialization where complete callback signature can be passed
 * @author  Christophe Ecabert
 * @date    01/11/2016
 * @ingroup utils
 */
template<typename Event, typename R, typename ... Args>
class LTS5_EXPORTS Signal<Event, R(Args...)> {
 public:

#pragma mark -
#pragma mark Usage

  /**
   * @name  AddMemberObserver
   * @fn    void AddMemberObserver(const Event& event, Fcn&& fcn, Obj&& obj)
   * @brief Add an observer to the signal which is a member of an instance
   * @param[in] event   Event linked to the observer member
   * @param[in] fcn     Member function to invoke when notification is fired
   * @param[in] obj     Instance object that holds \p fcn
   */
  template<typename Fcn, typename ...Obj>
  void AddMemberObserver(const Event& event, Fcn&& fcn, Obj&& ...obj) {
    observers_[event].push_back(std::bind(fcn, obj...));
  }

  /**
   * @name  AddObserver
   * @fn    void AddObserver(const Event& event, std::function<void(Args...)> const& fcn)
   * @brief Add an observer to the signal
   * @param event       Event linked to the observer member
   * @param fcn         Function to invoke when notification is fired
   */
  void AddObserver(const Event& event, std::function<R(Args...)> const& fcn) {
    observers_[event].push_back(fcn);
  }

  /**
   * @name  RemoveObserver
   * @fn    void RemoveObserver(const Event& event)
   * @brief Remove a specific observer's event
   * @param event   Event to remove
   */
  void RemoveObserver(const Event& event) {
    observers_.erase(event);
  }

  /**
   * @name  Notify
   * @fn    void Notify(const Event& event, const Args... args)
   * @brief Notify observer that a specific event happened
   * @param event   Type of event
   * @param args    Argument to pass to the observer(s)
   */
  void Notify(const Event& event, const Args... args) {
    for (auto& obs : observers_.at(event)) {
      obs(args...);
    }
  }

  /**
   * @name  get_number_observer
   * @fn    const size_t& get_number_observer(void) const
   * @brief Indicate how many observer are currently registered
   * @return    Number of observer
   */
  size_t get_number_observer(void) const {
    return observers_.size();
  }

#pragma mark -
#pragma mark Private
 private:
  /** Callback storage */
  std::map<Event, std::vector<std::function<R(Args...)>>> observers_;

};

}  // namepsace LTS5

#endif //__LTS5_SIGNAL__
