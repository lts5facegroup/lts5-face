//
//  time_lapse.hpp
//  lts5-face
//
//  Created by Nikos Athanasiou
//  sources: https://github.com/picanumber/bureaucrat/blob/master/time_lapse.h
//

#ifndef __LTS5_time_lapse_hpp__
#define __LTS5_time_lapse_hpp__

#include <chrono>

#define fw(what) std::forward<decltype(what)>(what)

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @struct Measure
 * @brief Class to measure the execution time of a callable
 * @see https://github.com/picanumber/bureaucrat
 * @ingroup utils
 */
template <
      typename TimeT = std::chrono::milliseconds, class ClockT = std::chrono::system_clock
>
struct Measure
{
  /**
   * @ fn    Execution
   * @ brief Returns the quantity (count) of the elapsed time as TimeT units
   */
  template<typename F, typename ...Args>
  static typename TimeT::rep Execution(F&& func, Args&&... args) {
    auto start = ClockT::now();
    fw(func)(std::forward<Args>(args)...);
    auto duration = std::chrono::duration_cast<TimeT>(ClockT::now() - start);
    return duration.count();
  }

  /**
   * @fn    Duration
   * @brief Returns the duration (in chrono's type system) of the elapsed time
   */
  template<typename F, typename... Args>
  static TimeT Duration(F&& func, Args&&... args) {
    auto start = ClockT::now();
    fw(func)(std::forward<Args>(args)...);
    return std::chrono::duration_cast<TimeT>(ClockT::now() - start);
  }
};
}  // namespace LTS5

#undef fw
#endif /* __LTS5_time_lapse_hpp__ */
