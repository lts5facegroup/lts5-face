/**
 *  @file   circular_iterator.hpp
 *  @brief  Circular iterator for circular buffer
 *          See : http://stackoverflow.com/questions/2616643/is-there-a-standard-cyclic-iterator-in-c
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   27/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __CIRCULAR_ITERATOR__
#define __CIRCULAR_ITERATOR__

#include <iterator>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   CircularIterator
 * @brief   Circular Iterator implementation
 * @author  Christophe Ecabert
 * @date    27/05/16
 * @ingroup utils
 */
template<typename T, typename Iterator>
class LTS5_EXPORTS CircularIterator : public std::iterator<std::bidirectional_iterator_tag, T, ptrdiff_t> {

 public:

#pragma mark -
#pragma mark Type definition

  /** Reference */
  using reference = typename std::iterator<std::bidirectional_iterator_tag, T, ptrdiff_t>::reference;
  /** Pointer */
  using pointer = typename std::iterator<std::bidirectional_iterator_tag, T, ptrdiff_t>::pointer;
  /** Difference type */
  using difference_type = typename std::iterator<std::bidirectional_iterator_tag, T, ptrdiff_t>::difference_type;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  CircularIterator
   * @fn  CircularIterator(void)
   * @brief Constructor
   */
  CircularIterator(void) {}

  /**
   * @name  CircularIterator
   * @fn  CircularIterator(const Iterator& first, const Iterator& last)
   * @brief Constructor
   * @param[in] first   Starting iterator
   * @param[in] last    Ending iterator
   */
  CircularIterator(const Iterator& first, const Iterator& last) : cursor_(first),
                                                                  begin_(first), end_(last) {
  }

#pragma mark -
#pragma mark Operator

  /**
   * @name  operator==
   * @fn  bool operator==(const CircularIterator& c) const
   * @brief Comparison operator
   * @param[in] c Element to compare to
   * @return  true if egal, false otherwise
   */
  bool operator==(const CircularIterator& c) const {
    return cursor_ == c.cursor_;
  }

  /**
   * @name  operator!=
   * @fn  bool operator!=(const CircularIterator& c) const
   * @brief not egal operator
   * @param[in] c Element to compare to
   * @return  true if different, false otherwise
   */
  bool operator!=(const CircularIterator& c) const {
    return cursor_ != c.cursor_;
  }

  /**
   * @name  operator*
   * @fn  reference operator*(void) const
   * @brief reference operator
   * @return  reference
   */
  reference operator*(void) const {
    return *cursor_;
  }

  /**
   * @name  operator++
   * @fn  CircularIterator& operator++(void)
   * @brief pre-increment operator
   * @return  Iterator
   */
  CircularIterator& operator++(void) {
    ++cursor_;
    if (cursor_ == end_) {
      cursor_ = begin_;
    }
    return *this;
  }

  /**
   * @name  operator++(int)
   * @fn  CircularIterator& operator++(int)
   * @brief post-increment operator
   * @return  Iterator
   */
  CircularIterator operator++(int) {
    CircularIterator it = *this;
    ++*this;
    return it;
  }

  /**
   * @name  operator--
   * @fn  CircularIterator& operator--(void)
   * @brief pre-decrement operator
   * @return  Iterator
   */
  CircularIterator& operator--(void) {
    if (cursor_ == begin_) {
      cursor_ = end_;
    }
    --cursor_;
    return *this;
  }

  /**
   * @name  operator--(int)
   * @fn  CircularIterator& operator--(int)
   * @brief post-decrement operator
   * @return  Iterator
   */
  CircularIterator operator--(int) {
    CircularIterator it = *this;
    --*this;
    return it;
  }

#pragma mark -
#pragma mark Private
 private:
  /** Actual position */
  Iterator cursor_;
  /** Container begining */
  Iterator begin_;
  /** Container end */
  Iterator end_;
};

}  // namepsace LTS5

#endif //__CIRCULAR_ITERATOR__
