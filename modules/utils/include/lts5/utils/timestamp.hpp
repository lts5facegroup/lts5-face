/**
 *  @file   timestamp.hpp
 *  @brief  TimeStamp helper class
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   02/08/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TIMESTAMP__
#define __LTS5_TIMESTAMP__

#include <chrono>
#include <string>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class  TimeStamp
 * @brief   TimeStamp helper class
 * @author  Christophe Ecabert
 * @date    20/08/2016
 * @ingroup utils
 */
template<typename DurationT = std::chrono::milliseconds,
         class ClockT = std::chrono::system_clock>
class LTS5_EXPORTS TimeStamp {

 public:

#pragma mark -
#pragma Initialization

  /**
   * @name  TimeStamp
   * @fn    TimeStamp()
   * @brief Constructor
   */
  TimeStamp();

  /**
   * @name  TimeStamp
   * @fn    TimeStamp(const TimeStamp& other) = default
   * @brief Copy Constructor
   * @param[in] other   Object to copy from
   */
  TimeStamp(const TimeStamp& other) = default;

  /**
   * @name  operator=
   * @fn    TimeStamp& operator=(const TimeStamp& rhs) = default
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  TimeStamp& operator=(const TimeStamp& rhs) = default;

  /**
   * @name  ~TimeStamp
   * @fn    ~TimeStamp() = default
   * @brief Destructor
   */
  ~TimeStamp() = default;

#pragma mark -
#pragma Usage

  /**
   * @name  ConvertToString
   * @fn    void ConvertToString(std::string* str)
   * @brief Convert timestamp into readable string
   * @param[out] str Converted string
   */
  void ConvertToString(std::string* str);

#pragma mark -
#pragma Operator

  /**
   * @name  operator-
   * @fn DurationT operator-(const TimeStamp& rhs) const
   * @brief Compute time difference in a given unit
   * @param[in] rhs Second timestamp
   * @return Time elapsed between the two timestamps
   */
  DurationT operator-(const TimeStamp& rhs) const;

  /**
   * @name  operator>
   * @fn    bool operator>(const TimeStamp& rhs)
   * @brief Compare if the timestamp is greater than \p rhs
   * @param[in] rhs Second timestamp
   * @return True if the current timestamp is greater than \p rhs, false
   *         otherwise
   */
  bool operator>(const TimeStamp& rhs) const;

  /**
   * @name  operator<
   * @fn    bool operator<(const TimeStamp& rhs)
   * @brief Compare if the timestamp is lower than \p rhs
   * @param[in] rhs Second timestamp
   * @return True if the current timestamp is lower than \p rhs, false
   *         otherwise
   */
  bool operator<(const TimeStamp& rhs) const;

#pragma mark -
#pragma Private

 private:
  /** Time point */
  typename ClockT::time_point t_;
};

}  // namepsace LTS5
#endif //__LTS5_TIMESTAMP__
