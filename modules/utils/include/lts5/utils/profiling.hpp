/**
 *  @file   profiling.hpp
 *  @brief  Utility class to profile execution times
 *  @ingroup utils
 *
 *  @author Marco Lourenço
 *  @date   11/03/16
 *  Copyright (c) 2016 Marco Lourenço. All rights reserved.
 */

#ifndef __LTS5_PROFILING__
#define __LTS5_PROFILING__

#include <stdio.h>
#include <string>
#include <sys/time.h>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  Profiling
 *  @brief  Utility class to profile execution times
 *  @author Marco Lourenço
 *  @date   11/03/16
 *  @ingroup utils
 */
class LTS5_EXPORTS Profiling {

 public :

  /** Timer data buffer index */
  int timer_array_index_;

#pragma mark -
#pragma mark Initialization

  /**
   *  @name Profiling
   *  @fn Profiling(void)
   *  @brief  Constructor
   */
  Profiling(void);

  /**
   *  @name ~Profiling
   *  @fn ~Profiling(void)
   *  @brief  Destructor
   */
  ~Profiling(void);

#pragma mark -
#pragma mark Process

  /**
   *  @name StartTimer
   *  @fn StartTimer(void)
   *  @brief start counting time
   */
  void StartTimer();

  /**
   *  @name InitCSVFile
   *  @fn InitCSVFile(std::string filename)
   *  @brief init headers in CSV file
   *  @param[in]  filename               the name of the file to init
   */
  void InitCSVFile(std::string filename);

  /**
   *  @name ClearCSVFile
   *  @fn ClearCSVFile(std::string filename)
   *  @brief remove all content from CSV file
   *  @param[in]  filename               the name of the file from which to delete
   */
  void ClearCSVFile(std::string filename);

  /**
   *  @name ExportTimersToCSV
   *  @fn ExportTimersToCSV(std::string filename)
   *  @brief export all recorded timers to CSV file
   *  @param[in]  filename		 the name of the file created
   */
  void ExportTimersToCSV(std::string filename);

  /**
   *  @name StopTimer
   *  @fn StopTimer(void)
   *  @brief stop counting time
   *  @return the number of seconds that passed since startTimer()
   */
  double StopTimer();

  /**
   *  @name InitArray
   *  @fn InitArray(void)
   *  @brief init the array with number_of_timers_* number_of_passes_
   */
  void InitArray();

#pragma mark -
#pragma mark Accessors

  /**
   *  @name SetTimerArray
   *  @fn SetTimerArray(int index)
   *  @brief set a value in save_timer_array_ at pos index
   *  @param[in]  value		 the value to set
   *  @param[in]  index    index in array
   */
  void SetTimerArray(double value, int index);

#pragma mark -
#pragma mark Private

 private :

  /** ADD COMMENT HERE */
  double tik_timer_;
  /** ADD COMMENT HERE */
  double tok_timer_;
  /** Number of timer to save per run */
  int number_of_timers_;
  /** number of times the loop runs */
  int number_of_passes_;
  /** Time data buffer */
  double save_timer_array_[100];

};

}  // namespace LTS5
#endif /* __LTS5_PROFILING__ */
