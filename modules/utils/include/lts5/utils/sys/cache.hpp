/**
 *  @file   cache.hpp
 *  @brief  Caching mechanism
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   15/02/21
 *  Copyright (c) 2021 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_UTILS_SYS_CACHE__
#define __LTS5_UTILS_SYS_CACHE__

#include <functional>
#include <memory>

#include "thread-safe-lru/scalable-cache.h"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/hash.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<class... Args>
class LTS5_EXPORTS ThreadSafeTupleKey {
 public:
  /**
   * @name  ThreadSafeTupleKey
   * @brief Constructor
   * @param[in] args    List of arguments
   */
  ThreadSafeTupleKey(const Args... args) : storage_(new Storage(args...)) {
  }

  /**
   * @name  ThreadSafeTupleKey
   * @brief Empty constructor
   */
  ThreadSafeTupleKey() = default;

  /**
   * @name  hash
   * @brief Get hash value for the underlying key
   * @return    Hash value
   */
  size_t hash() const {
    return storage_->hash();
  }

  std::tuple<Args...> data() const {
    return storage_->data_;
  }

  size_t Size() const {
    return storage_->Size();
  }

  /**
   * @name  operator==
   * @brief Compare two tuple key for equality.
   * @param[in] other   Key to compare against
   * @return    True if both keys are the same, false otherwise
   */
  bool operator==(const ThreadSafeTupleKey<Args...>& other) const {
    //return this->hash() == other.hash();
    return storage_->Size() && other.storage_->Size() && this->data() == other.data();
  }

  /**
   * @struct HashCompare
   * @brief Comparator for two Hashed key.
   */
  struct HashCompare {
    /**
     * @name    equal
     * @brief   Compare two hashed key.
     * @param[in] a First key
     * @param[in] b Second key
     * @return  True if both keys are the same, false otherwise.
     */
    bool equal(const ThreadSafeTupleKey<Args...>& a,
               const ThreadSafeTupleKey<Args...>& b) const {
      return a == b;
    }

    /**
     * @name    hash
     * @brief   Compute hash value
     * @param[in] k Key
     * @return  Hash value
     */
    size_t hash(const ThreadSafeTupleKey<Args...>& k) const {
      return k.hash();
    }
  };


 private:
  /** Tuple's argument without reference */
  using TupleNoRef = std::tuple<typename std::remove_reference<Args>::type...>;

  /**
   * @struct    Container for tuple key
   * @brief     Thread safe key for tuple, based on `ThreadSafeStringKey` in
   *    file "string-key.h"
   */
  struct Storage {
    /**
     * @name
     * @brief Constructor
     * @param[in] args  List of input arguments
     */
    Storage(const Args... args) : data_(std::forward<Args>(args)...),
                                  size_(sizeof...(args)),
                                  hash_(0) {}

    /**
     * @name  ~Storage
     * @brief Destructor
     */
    ~Storage() = default;

    /**
     * @name    hash
     * @brief   Compute hash or used cached one.
     * @return  Hash value
     */
    size_t hash() const {
      // See string-key.h
      size_t h = hash_.load(std::memory_order_relaxed);
      if (h == 0) {
        // Compute hash value and store it
        h = std::hash<TupleNoRef>()(data_);
        hash_.store(h, std::memory_order_relaxed);
      }
      return h;
    }

    /**
     * @name    Size
     * @brief   Size of the tuple
     * @return  Tuple's size
     */
    size_t Size() const {
      return size_;
    }

    /** Tuple */
    TupleNoRef data_;
    /** Size of tuple */
    size_t size_;
    /** Hash */
    mutable std::atomic<size_t> hash_;
  };

  /** Container for tuple key */
  std::shared_ptr<Storage> storage_;
};

/**
 * @class   Cache
 * @brief   Caching mechanism to avoid periodic computation. Undefined!
 * @author  Christophe Ecabert
 * @date    15/02/2021
 * @ingroup utils
 * @tparam T    Default argument
 */
template<class T>
class Cache{};

/**
 * @class   Cache
 * @brief   Caching mechanism to avoid periodic computation.
 * @author  Christophe Ecabert
 * @date    15/02/2021
 * @ingroup utils
 * @tparam R    Functor's return type
 * @tparam Args Functor's input arguments
 */
template<class R, class... Args>
class LTS5_EXPORTS Cache<R(Args...)> {
 private:
  /** Map key */
  using Key = ThreadSafeTupleKey<Args...>;
  /** Hash comparator */
  using HashComp = typename ThreadSafeTupleKey<Args...>::HashCompare;
  /** Cache */
  using ThreadSafeCache = tstarling::ThreadSafeScalableCache<Key, R, HashComp>;

 public:
  /**
   * @name Cache
   * @fn Cache(size_t size, std::function<R(Args...)> fcn)
   * @brief Constructor
   * @param[in] size    Size of the cache
   * @param[in] fcn     Functor doing computation of the cached values
   */
  Cache(size_t size, std::function<R(Args...)> fcn) : cache_(size),
                                                      fcn_(fcn) {}

  /**
   * @name  ~Cache
   * @fn    ~Cache() = default;
   * @brief Destructor
   */
  ~Cache() = default;

  /**
   * @name  GetOrComputeValue
   * @fn    R GetOrComputeValue(const Args... args)
   * @brief Compute the desired value or used the cached one.
   * @param[in] args    List of input arguments
   * @return    Computed/Cached value
   */
  R GetOrComputeValue(const Args... args) {
    using CstAccessor = typename ThreadSafeCache::ConstAccessor;
    // Value in cache ?
    CstAccessor accessor;
    Key key(args...);
    if (cache_.find(accessor, key)) {
      // Found it
      return *accessor;
    } else {
      // Compute value and insert in cache
      R value = fcn_(args...);
      cache_.insert(key, value);
      return value;
    }
  }

  /**
   * @name  operator()
   * @fn    R operator()(const Args... args)
   * @brief Utility function calling `GetOrComputeValue` function.
   * @param[in] args    List of input arguments
   * @return    Computed/Cached value
   */
  R operator()(const Args... args) {
    return this->GetOrComputeValue(args...);
  }

 private:
  /** Cache */
  ThreadSafeCache cache_;
  /** Functor */
  std::function<R(Args...)> fcn_;
};


}  // namespace LTS5
#endif // __LTS5_UTILS_SYS_CACHE__
