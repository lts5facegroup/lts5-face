/**
 *  @file   refcount.hpp
 *  @brief  Mechanism for reference counting. Taken from tensorflow repo
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   1/22/20
 */

/* Copyright 2015 The TensorFlow Authors. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef __LTS5_REFCOUNT__
#define __LTS5_REFCOUNT__

#include <atomic>
#include <memory>
#include <cassert>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   RefCounted
 * @brief   Reference counting mechanism
 * @date    22/01/2020
 * @ingroup utils
 */
class LTS5_EXPORTS RefCounted {
 public:
  /**
   * @name  RefCounted
   * @fn    RefCounted()
   * @brief Constructor, initial reference count is one.
   */
  RefCounted();

  /**
   * @name  RefCounted
   * @fn    RefCounted(const RefCounted& other)
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  RefCounted(const RefCounted& other);

  /**
   * @name  operator=
   * @fn    RefCounted& operator=(const RefCounted& rhs)
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  RefCounted& operator=(const RefCounted& rhs);

  /**
   * @name  Ref
   * @fn    void Ref() const
   * @brief Increments reference count by one.
   */
  void Ref() const;

  /**
   * @name  Unref
   * @fn    bool Unref() const
   * @brief Decrements reference count by one.  If the count remains positive,
   *        returns false.  When the count reaches zero, returns true and
   *        deletes this, in which case the caller must not access the object
   *        afterward.
   * @return True if counter reach zero, false otherwise
   */
  bool Unref() const;

  /**
   * @name  RefCountIsOne
   * @fn    bool RefCountIsOne() const
   * @brief     Return whether the reference count is one. If the reference
   *            count is used in the conventional way, a reference count of 1
   *            implies that the current thread owns the reference and no other
   *            thread shares it. This call performs the test for a reference
   *            count of one, and performs the memory barrier needed for the
   *            owning thread to act on the object, knowing that it has
   *            exclusive access to the object
   * @return    Return whether the reference count is one.
   */
  bool RefCountIsOne() const;

 protected:
  /**
   * @name  ~RefCounted
   * @fn    virtual ~RefCounted()
   * @brief     Make destructor protected so that RefCounted objects cannot be
   *            instantiated directly. Only subclasses can be instantiated.
   */
  virtual ~RefCounted();

  /**
   * @name  Initialize
   * @fn    void Initialize()
   * @brief Initialize a new reference counter and set it to 1.
   */
  void Initialize();

 private:
  /** Reference counter */
  std::atomic_int_fast32_t* ref_;
};

// Inlined routines, since these are performance critical
inline RefCounted::RefCounted() : ref_(new std::atomic_int_fast32_t(1)) {}

inline RefCounted::RefCounted(const RefCounted& other) : ref_(other.ref_) {
  this->Ref();
}

inline RefCounted& RefCounted::operator=(const RefCounted& rhs) {
  if (this != &rhs) {
    if (this->Unref()) {  // Release current
      delete this->ref_;
    }
    this->ref_ = rhs.ref_;  // Copy other + increase counter
    this->Ref();
  }
  return *this;
}

inline RefCounted::~RefCounted() {
  if (ref_->load() == 0) {
    delete ref_;
  }
}

/*
 * @name  Initialize
 * @fn    void Initialize()
 * @brief Initialize a new reference counter and set it to 1.
 */
inline void RefCounted::Initialize() {
  if (ref_->load(std::memory_order_acquire) == 0) {
    delete this->ref_;
  }
  this->ref_ = new std::atomic_int_fast32_t(1);
}

inline void RefCounted::Ref() const {
  assert(ref_->load() >= 1);
  ref_->fetch_add(1, std::memory_order_relaxed);
}

inline bool RefCounted::Unref() const {
  assert(ref_->load() > 0);
  // If ref_==1, this object is owned only by the caller. Bypass a locked op
  // in that case.
  if (RefCountIsOne() || ref_->fetch_sub(1) == 1) {
    // Make DCHECK in ~RefCounted happy
    ref_->store(0);
    return true;
  } else {
    return false;
  }
}

inline bool RefCounted::RefCountIsOne() const {
  return (ref_->load(std::memory_order_acquire) == 1);
}

}  // namespace LTS5
#endif  // __LTS5_REFCOUNT__
