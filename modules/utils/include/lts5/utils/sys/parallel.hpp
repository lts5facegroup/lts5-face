/**
 *  @file   parallel.hpp
 *  @brief  Multi-threading abstraction
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   10/31/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <functional>

#include "lts5/utils/library_export.hpp"

#ifndef __LTS5_PARALLEL__
#define __LTS5_PARALLEL__

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Mutex implementation */
class MutexImpl;

/**
 * @class   Mutex
 * @brief   Locking mechanism for thread safety
 * @author  Christophe Ecabert
 * @date    1.11.18
 * @ingroup utils
 */
class LTS5_EXPORTS Mutex {
 public:

  /**
   * @name  Mutex
   * @fn    Mutex()
   * @brief Constructor
   */
  Mutex();

  /**
   * @name  Mutex
   * @fn    Mutex(const Mutex& other) = delete
   * @brief Copy Constructor
   * @param[in] other   Object to copy from
   */
  Mutex(const Mutex& other) = delete;

  /**
   * @name  operator=
   * @fn    Mutex& operator=(const Mutex& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs   Object to assign from
   * @return Newly assigned object
   */
  Mutex& operator=(const Mutex& rhs) = delete;

  /**
   * @name  ~Mutex
   * @fn    ~Mutex()
   * @brief Destructor
   */
  ~Mutex();

  /**
   * @name  Lock
   * @fn    void Lock()
   * @brief Acquire lock
   */
  void Lock();

  /**
   * @name  Unlock
   * @fn    void Unlock()
   * @brief Release lock
   */
  void Unlock();

 private:
  /** Lock */
  MutexImpl* lock_;
};

/**
 * @class   ScopedLock
 * @brief   RAII object handling locking/unlocking mutex
 * @author  Christophe Ecabert
 * @date    1.11.18
 * @ingroup utils
 */
class LTS5_EXPORTS ScopedLock {
 public:

  /**
   * @name  ScopedLock
   * @fn    explicit ScopedLock(Mutex& m)
   * @brief Constructor
   * @param[in] m   Mutex to lock
   */
  explicit ScopedLock(Mutex& m) : m_(m) {
    m.Lock();
  }

  /**
   * @name  ~ScopedLock
   * @fn    ~ScopedLock()
   * @brief Destructor, automatically release lock
   */
  ~ScopedLock() {
    m_.Unlock();
  }

 private:
  /** Mutex */
  Mutex& m_;
};

/**
 * @class   Parallel
 * @brief   Multithreading abstraction
 * @author  Christophe Ecabert
 * @date    31.10.18
 * @ingroup utils
 */
class LTS5_EXPORTS Parallel {
 public:

  /**
   * @name  For
   * @fn static void For(const size_t& n, std::function<void(const size_t&)>&& body)
   * @brief Execute `body` in parallel fashion
   * @param[in] n   Number of time body needs to be run
   * @param[in,out] body    Body of for loop
   */
  static void For(const size_t& n, std::function<void(const size_t&)>&& body);
};

}  // namespace LTS5
#endif  // __LTS5_PARALLEL__
