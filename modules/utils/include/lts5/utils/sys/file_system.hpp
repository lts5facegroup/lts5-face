/**
 *  @file   file_system.hpp
 *  @brief  Interface to access file system (check if file/dir exist, ...).
 *          Inspired by Tensorflow
 *  @ingroup    utils
 *
 *  @author Christophe Ecabert
 *  @date   6/13/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_FILE_SYSTEM__
#define __LTS5_FILE_SYSTEM__

#include <vector>
#include <string>
#include <limits>
#include <unordered_set>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/status.hpp"

#ifdef IS_WINDOWS
#undef CopyFile
#endif

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @struct  FileProperty
 *  @brief  Hold file properties
 *  @author Christophe Ecabert
 *  @date   6/13/18
 *  @ingroup utils
 */
struct LTS5_EXPORTS FileProperty {
  /** File size, or size_t::max() if not supported */
  size_t size = std::numeric_limits<size_t>::max();
  /** Directory flags */
  bool is_dir = false;

  /**
   *  @name   FileProperty
   *  @fn     FileProperty() = default
   *  @brief  Constructor
   */
  FileProperty() = default;

  /**
   *  @name   FileProperty
   *  @fn     FileProperty() = default
   *  @brief  Constructor
   */
  FileProperty(const size_t& sz, const bool is_dir) : size(sz),
                                                      is_dir(is_dir) {}

  /**
   *  @name   ~FileProperty
   *  @fn     ~FileProperty() = default
   *  @brief  Destructor
   */
  ~FileProperty() = default;
};

/**
 *  @class  FileSystem
 *  @brief  A generic interface for accessing a file system
 *  @author Christophe Ecabert
 *  @date   02.03.18
 *  @ingroup utils
 */
class LTS5_EXPORTS FileSystem {
 public:

  /** Default buffer size for copy, 128k */
  static constexpr size_t kBufferSize = 128 * (1 << 10);

  /**
   *  @name   ~FileSystem
   *  @fn     virtual ~FileSystem() = default
   *  @brief  Destructor
   */
  virtual ~FileSystem() = default;

  /**
   *  @name   NormalizePath
   *  @fn     virtual std::string NormalizePath(const std::string& path)
   *  @brief  Convert a given path into a standardized representation
   *  @param[in] path Path to normalize
   *  @return Normalized path
   */
  virtual std::string NormalizePath(const std::string& path);

  /**
   *  @name   FileExists
   *  @fn     virtual Status FileExist(const std::string& filename) = 0
   *  @brief  Check if a given path exists
   *  @param[in] filename Path to check existence
   *  @return kGood of kNotFound
   */
  virtual Status FileExist(const std::string& filename) = 0;

  /**
   *  @name   FilesExists
   *  @fn     virtual bool FilesExists(const std::vector<std::string>& filenames,
                                       std::vector<Status>* status)
   *  @brief  Check if a list of files exists.
   *  @param[in] filenames  List of files to check the existence
   *  @param[out] status    List of status for each files. Optionnal can be
   *                        nullptr
   *  @return True if all files exists, false otherwise
   */
  virtual bool FilesExists(const std::vector<std::string>& filenames,
                           std::vector<Status>* status);

  /**
   *  @name   ListDir
   *  @fn     virtual Status ListDir(const std::string& dir,
   *                                 std::vector<std::string>* files) = 0
   *  @brief  List the content of a given directory `dir`.
   *  @param[in] dir  Directory to scan
   *  @param[out] files List of files/dirs in `dir`
   *  @return kGood or Error code
   */
  virtual Status ListDir(const std::string& dir,
                         std::vector<std::string>* files) = 0;

  /**
   *  @name   ListDirRecursively
   *  @fn     virtual Status ListDirRecursively(const std::string& dir,
   *                                 std::vector<std::string>* files)
   *  @brief  List the content of a given directory `dir` and follow the
   *          arborescence in order to list all files
   *  @param[in] dir  Directory to scan
   *  @param[out] files List of files/dirs in `dir`
   *  @return kGood or Error code
   */
  virtual Status ListDirRecursively(const std::string& dir,
                                    std::vector<std::string>* files);

  /**
   *  @name   FileProp
   *  @fn     virtual Status FileProp(const std::string& filename,
   *                                  FileProperty* prop) = 0
   *  @brief  Gather file properties for a given file
   *  @param[in] filename Path to the file
   *  @param[out] prop    File's properties
   *  @return kGood, kNotFound or error code.
   */
  virtual Status FileProp(const std::string& filename, FileProperty* prop) = 0;

  /**
   *  @name   DeleteFile
   *  @fn     virtual Status DeleteFile(const std::string& filename) = 0
   *  @brief  Delete a give file
   *  @param[in] filename File to delete
   *  @return Status of the operation
   */
  virtual Status DeleteFile(const std::string& filename) = 0;

  /**
   *  @name   CreateDir
   *  @fn     virtual Status CreateDir(const std::string& dir) = 0
   *  @brief  Create a directory
   *  @param[in] dir  path to the directory to create
   *  @return Status of the operation
   */
  virtual Status CreateDir(const std::string& dir) = 0;

  /**
   *  @name   IsDirectory
   *  @fn     virtual Status IsDirectory(const std::string& dir)
   *  @brief  Check if a given path is a directory
   *  @param[in] dir  Path to check if it is a directory
   *  @return Status of the operation
   */
  virtual Status IsDirectory(const std::string& dir);

  /**
   *  @name   CreateDirRecursively
   *  @fn     virtual Status CreateDirRecursively(const std::string& dir)
   *  @brief  Create a directory arborescence
   *  @param[in] dir  path to the directory to create
   *  @return Status of the operation
   */
  virtual Status CreateDirRecursively(const std::string& dir);

  /**
   *  @name   DeleteDir
   *  @fn     virtual Status DeleteDir(const std::string& dir) = 0
   *  @brief  Delete a directory
   *  @param[in] dir  path to the directory to delete
   *  @return Status of the operation
   */
  virtual Status DeleteDir(const std::string& dir) = 0;

  /**
   *  @name   DeleteDirRecursively
   *  @fn     virtual Status DeleteDirRecursively(const std::string& dir)
   *  @brief  Delete a directory arborescence
   *  @param[in] dir  path to the directory to delete
   *  @return Status of the operation
   */
  virtual Status DeleteDirRecursively(const std::string& dir);

  /**
   *  @name   RenameFile
   *  @fn     virtual Status RenameFile(const std::string& src,
   *                                    const std::string& dst) = 0;
   *  @brief  Change the name of file given in `src` into `dst`
   *  @param[in] src  Old file's name
   *  @param[in] dst  New file's name
   *  @return Status of the operation
   */
  virtual Status RenameFile(const std::string& src, const std::string& dst) = 0;

  /**
   *  @name   QueryFileSize
   *  @fn     virtual Status QueryFileSize(const std::string& filename, size_t* size) = 0
   *  @brief  Gather file size in bytes
   *  @param[in] filename File to query size
   *  @param[out] size    File's size
   *  @return Status of the operation
   */
  virtual Status QueryFileSize(const std::string& filename, size_t* size) = 0;

  /**
   *  @name   CopyFile
   *  @fn     virtual Status CopyFile(const std::string& src, const std::string& dst)
   *  @brief  Copy a file to a new location
   *  @param[in] src  File to copy
   *  @param[in] dst  Location where to copy it (dir + fname).
   *  @return Status of the operation
   */
  virtual Status CopyFile(const std::string& src, const std::string& dst);
};

/**
 *  @class  FileSystemProxy
 *  @brief  Proxy for registering/creating filesystems
 *  @author Christophe Ecabert
 *  @date   6/13/18
 *  @ingroup utils
 */
class LTS5_EXPORTS FileSystemProxy {
 public:
  /**
   *  @name   FileSystemProxy
   *  @fn     FileSystemProxy()
   *  @brief  Constructor
   */
  FileSystemProxy();

  /**
   *  @name   ~FileSystemProxy
   *  @fn     virtual ~FileSystemProxy() = default
   *  @brief  Destructor
   */
  virtual ~FileSystemProxy() = default;

  /**
   *  @name   Create
   *  @fn     virtual FileSystem* Create() const = 0
   *  @brief  Create an instance of the FileSystem attached to this proxy
   *  @return FileSystem instance
   */
  virtual FileSystem* Create() const = 0;

  /**
   *  @name   Name
   *  @fn     virtual const char* Name() const = 0
   *  @brief  Name of the attached file system.
   *  @return FileSystem's name
   */
  virtual const char* Name() const = 0;
};

/**
 *  @class  FileSystemFactory
 *  @brief  Mechanism to store file systems implementations
 *  @author Christophe Ecabert
 *  @date   6/13/18
 *  @ingroup utils
 */
class LTS5_EXPORTS FileSystemFactory {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   *  @name Get
   *  @fn static FileSystemFactory& Get()
   *  @brief  Provide access to single instance of FileSystemFactory
   */
  static FileSystemFactory& Get();

  /**
   *  @name   ~FileSystemFactory
   *  @fn     ~FileSystemFactory()
   *  @brief  Destructor
   */
  ~FileSystemFactory();

  /**
   *  @name   FileSystemFactory
   *  @fn     FileSystemFactory(const FileSystemFactory& other) = delete
   *  @brief  Copy constructor
   */
  FileSystemFactory(const FileSystemFactory& other) = delete;

  /**
   *  @name   operator=
   *  @fn     FileSystemFactory& operator=(const FileSystemFactory& rhs) = delete
   *  @brief  Copy assignment
   *  @param[in] rhs  Object to assign from
   *  @return Newly assigned object
   */
  FileSystemFactory& operator=(const FileSystemFactory& rhs) = delete;

#pragma mark -
#pragma mark Usage

  /**
   *  @name   Register
   *  @fn     void Register(const FileSystemProxy* proxy)
   *  @brief  Register a new file system into the factory
   *  @param[in]  proxy File system's proxy (from macro REGISTER_FILE_SYSTEM)
   */
  void Register(const FileSystemProxy* proxy);

  /**
   *  @name   Retrieve
   *  @fn     FileSystem* Retrieve(const std::string& name) const
   *  @brief  Retrieve file system corresponding to a given name or nullptr
   *  @return File system instance or nullptr.
   */
  FileSystem* Retrieve(const std::string& name) const;

#pragma mark -
#pragma mark Private
 private:

  /**
   *  @name   FileSystemFactory
   *  @fn     FileSystemFactory() = default
   *  @brief  Constructor
   */
  FileSystemFactory() = default;


  /** Registered FileSystem */
  std::unordered_set<const FileSystemProxy*> proxies_;
};

/**
 * @name    GetDefaultFileSystem
 * @fn  FileSystem* GetDefaultFileSystem()
 * @brief   Provide default file system (Posix/Windows)
 * @return  FileSystem or nullptr
 */
FileSystem* LTS5_EXPORTS GetDefaultFileSystem();

/**
 *  @def  REGISTER_FILE_SYSTEM
 *  @brief  Register a given file_system
 */
#define REGISTER_FILE_SYSTEM(name, file_system)                           \
  static file_system file_system##Instance;                               \
  class file_system##Proxy : public FileSystemProxy {                     \
  public:                                                                 \
  file_system##Proxy() : FileSystemProxy() {}                             \
  ~file_system##Proxy() override = default;                               \
  FileSystem* Create() const override { return &file_system##Instance; }  \
  const char* Name() const override { return name; }                      \
  };                                                                      \
static file_system##Proxy file_system##Registrator;

}  // namespace LTS5
#endif //__LTS5_FILE_SYSTEM__
