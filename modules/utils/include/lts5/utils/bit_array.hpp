/**
 *  @file   bit_array.hpp
 *  @brief  Bit array container
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   11/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __BIT_ARRAY__
#define __BIT_ARRAY__

#include <vector>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   BitArray
 * @brief   Bit array container
 * @author  Christophe Ecabert
 * @date    11/11/2016
 * @ingroup utils
 */
class LTS5_EXPORTS BitArray {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  BitArray
   * @fn    BitArray() = default
   * @brief Constructor
   */
  BitArray() = default;

  /**
   * @name  BitArray
   * @fn    BitArray(const std::size_t& size)
   * @brief Constructor
   * @param[in] size Bit array size
   */
  BitArray(const std::size_t& size);

  /**
   * @name  BitArray
   * @fn    BitArray(const BitArray& other) = default
   * @brief Copy constructor
   * @param[in] other Object to copy
   */
  BitArray(const BitArray& other) = default;

  /**
   * @name  operator=
   * @fn    BitArray operator=(const BitArray& rhs) = default
   * @brief Assignement operator
   * @param[in] rhs Object to assign from
   * @return Newly assigned object
   */
  BitArray& operator=(const BitArray& rhs) = default;

  /**
   * @name  ~BitArray
   * @fn    ~BitArray() = default
   * @brief Destructor
   */
  ~BitArray() = default;

  /**
   * @name  SetSize
   * @fn    void SetSize(const std::size_t& size)
   * @brief Set the size (i.e. number of bits) to store in the container.
   *        Content will be erased
   * @param[in] size    Set array dimension
   */
  void SetSize(const std::size_t& size);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Size
   * @fn    std::size_t Size() const
   * @brief Provide bit array dimension
   * @return    Size
   */
  std::size_t Size() const;

  /**
   * @name  Get
   * @fn    bool Get(const std::size_t& n_bit) const
   * @brief Get the value of a given bit
   * @param[in] n_bit Bit number to get
   * @return True if set, false otherwise
   */
  bool Get(const std::size_t& n_bit) const;

  /**
   * @name  Set
   * @fn    void Set(const std::size_t& n_bit)
   * @brief Set a given bit to 1.
   * @param[in] n_bit Bit number to set
   */
  void Set(const std::size_t& n_bit);

  /**
   * @name  Reset
   * @fn    void Reset(const std::size_t& n_bit)
   * @brief Clear a given bit to 0.
   * @param[in] n_bit Bit number to reset
   */
  void Reset(const std::size_t& n_bit);

  /**
   * @name  Flip
   * @fn    void Flip(const std::size_t& n_bit)
   * @brief Flip a given bit.
   * @param[in] n_bit Bit number to flip
   */
  void Flip(const std::size_t& n_bit);

#pragma mark -
#pragma mark Private

 private:
  /** Data container */
  std::vector<unsigned char> bit_array_;
  /** Size */
  std::size_t size_;
};
}  // namepsace LTS5
#endif //__BIT_ARRAY__
