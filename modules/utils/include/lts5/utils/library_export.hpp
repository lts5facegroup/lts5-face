/**
 *  @file   library_export.hpp
 *  @brief  Define macros to handle platform specific symbols exportation
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   15/09/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_LIBRARY_EXPORT__
#define __LTS5_LIBRARY_EXPORT__

#if (defined WIN32 || defined _WIN32 || defined WINCE)
  #define FUNC_NAME __FUNCTION__
  #if defined LTS5_API_EXPORTS
    #define LTS5_EXPORTS __declspec(dllexport)

    #if _MSC_VER && !__INTEL_COMPILER
      /* disable unknown pragma warnings */
      #pragma warning (disable : 4068 )
    #endif
  #else
    #define LTS5_EXPORTS __declspec(dllimport)
  #endif
#else
  #define LTS5_EXPORTS
  #define FUNC_NAME __PRETTY_FUNCTION__
#endif

/** Deprecation Macro */
#if defined(_MSC_VER)
#define LTS5_LEGACY(func) __declspec(deprecated) func
#elif defined(__GNUC__) && !defined(__INTEL_COMPILER)
#define LTS5_LEGACY(func) func __attribute__((deprecated))
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define LTS5_LEGACY(func) func
#endif

/** Deprecation Macro with message */
#if defined(_MSC_VER)
#define LTS5_LEGACY_WITH_MSG(func) __declspec(deprecated(msg)) func
#elif defined(__GNUC__) && !defined(__INTEL_COMPILER)
#define LTS5_LEGACY_WITH_MSG(func, msg) func __attribute__((deprecated(msg)))
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define LTS5_LEGACY_WITH_MSG(func, msg) func
#endif

#endif /* __LTS5_LIBRARY_EXPORT__ */
