/**
 *  @file   ordered_field_operator.hpp
 *  @brief  Ordered Field Operator
 *          Needed : +=, -=, *=, /=, <, ==
 *          Automatically generated : +, -, *, /, >, >=, <=, !=
 *  @ingroup utils
 *  @see    http://www.drdobbs.com/cpp/fixed-point-arithmetic-types-for-c/184401992?pgno=1
 *
 *  @author Christophe Ecabert
 *  @date   29/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __ORDER_FIELD_OPERATOR__
#define __ORDER_FIELD_OPERATOR__

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   OrderedFieldOperator
 * @brief   Automatic operator generator from user supplied one.
 * @author  Christophe Ecabert
 * @date    29/11/2016
 * @see     http://www.drdobbs.com/cpp/fixed-point-arithmetic-types-for-c/184401992?pgno=1
 * @tparam T    Base type
 * @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS OrderedFieldOperator {
 public:

  /**
   * @name  operator+
   * @fn    friend T operator+(const T& lhs, const T& rhs)
   * @brief Addition
   * @param lhs Left Hand sign
   * @param rhs Righ Hand sign
   * @return    Addition of both term
   */
  friend T operator+(const T& lhs, const T& rhs) {
    T tmp(lhs);
    tmp += rhs;
    return tmp;
  }

  /**
   * @name  operator-
   * @fn    friend T operator-(const T& lhs, const T& rhs)
   * @brief Subtraction
   * @param lhs Left Hand sign
   * @param rhs Righ Hand sign
   * @return    Subtraction of both term
   */
  friend T operator-(const T& lhs, const T& rhs) {
    T tmp(lhs);
    tmp -= rhs;
    return tmp;
  }

  /**
   * @name  operator*
   * @fn    friend T operator*(const T& lhs, const T& rhs)
   * @brief Multiplication
   * @param lhs Left Hand sign
   * @param rhs Righ Hand sign
   * @return    Multiplication of both term
   */
  friend T operator*(const T& lhs, const T& rhs) {
    T tmp(lhs);
    tmp *= rhs;
    return tmp;
  }

  /**
   * @name  operator/
   * @fn    friend T operator/(const T& lhs, const T& rhs)
   * @brief Division
   * @param lhs Left Hand sign
   * @param rhs Righ Hand sign
   * @return    Division of both term
   */
  friend T operator/(const T& lhs, const T& rhs) {
    T tmp(lhs);
    tmp /= rhs;
    return tmp;
  }

  /**
   * @name  operator>=
   * @fn    friend bool operator>=(const T& lhs, const T& rhs)
   * @brief Greater or equal
   * @param lhs     Left hand sign
   * @param rhs     Right hand sign
   * @return    True if lhs >= rhs, false otherwise
   */
  friend bool operator>=(const T& lhs, const T& rhs) {
    return !(lhs < rhs);
  }

  /**
   * @name  operator>
   * @fn    friend bool operator>(const T& lhs, const T& rhs)
   * @brief Greater than
   * @param lhs     Left hand sign
   * @param rhs     Right hand sign
   * @return    True if lhs > rhs, false otherwise
   */
  friend bool operator>(const T& lhs, const T& rhs) {
    return rhs < lhs;
  }

  /**
   * @name  operator<=
   * @fn    friend bool operator<=(const T& lhs, const T& rhs)
   * @brief Less or equal
   * @param lhs     Left hand sign
   * @param rhs     Right hand sign
   * @return    True if lhs <= rhs, false otherwise
   */
  friend bool operator<=(const T& lhs, const T& rhs) {
    return ((lhs < rhs) || (lhs == rhs));
  }

  /**
   * @name  operator!=
   * @fn    friend bool operator!=(const T& lhs, const T& rhs)
   * @brief Inequality
   * @param lhs     Left hand sign
   * @param rhs     Right hand sign
   * @return    True if lhs != rhs, false otherwise
   */
  friend bool operator!=(const T& lhs, const T& rhs) {
    return !(lhs == rhs);
  }
};
}  // namepsace LTS5
#endif //__ORDER_FIELD_OPERATOR__
