/**
 *  @file   safe_queue.hpp
 *  @brief  Thread safe non-blocking queue
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   26/08/15
 *  @see  http://stackoverflow.com/questions/15278343/c11-thread-safe-queue
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_SAFE_QUEUE__
#define __LTS5_SAFE_QUEUE__

#include <queue>
#include <mutex>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/base_safe_queue.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  SafeQueue
 *  @brief  Thread safe blocking queue
 *  @author Christophe Ecabert
 *  @date   26/08/15
 *  @ingroup utils
 */
template<class T>
class LTS5_EXPORTS SafeQueue : public BaseSafeQueue<T> {

 public:

  /**
   *  @name SafeQueue
   *  @fn SafeQueue(void)
   *  @brief  Constructor
   */
  SafeQueue(void) : BaseSafeQueue<T>() {}

  /**
   *  @name ~SafeQueue
   *  @fn ~SafeQueue(void)
   *  @brief  Destructor
   */
  ~SafeQueue(void) {}

  /**
   *  @name Push
   *  @fn void Push(const T& object)
   *  @brief  Add new object to the queue
   *  @param[in]  object  Object to add to the queue
   */
  void Push(const T& object) {
    // Lock
    std::lock_guard<std::mutex> lock(this->mutex_);
    // Add to the queue
    this->queue_.push(object);
    // Check capacity
    if ((this->capacity_ != -1) && (this->queue_.size() > this->capacity_)) {
      this->queue_.pop();
    }
  }

  /**
   *  @name Back
   *  @fn T Back(void)
   *  @brief  Remove object from the queue
   *  @param[in]  with_pop  Indique if object need to be removed from the
   *                        queue (i.e. pop())
   *  @param[out] object    Object retrieve from the queue, can be null
   */
  void Back(const bool with_pop, T* object) {
    // Create unique lock (~ similar to mutex_.lock() do stuff mutex_.unlock())
    std::unique_lock<std::mutex> lock(this->mutex_);
    if(!this->queue_.empty()) {
      // Somthing is inside, can pop out object
      *object = this->queue_.back();
      // Remove the object from the queue if asked
      if (with_pop) {
        this->queue_.pop();
      }
    } else {
      *object = T();
    }
  }
};

}  // namespace LTS5

#endif
