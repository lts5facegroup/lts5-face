/**
 *  @file   base_safe_queue.hpp
 *  @brief  Thread safe non-blocking queue
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   26/08/15
 *  @see  http://stackoverflow.com/questions/15278343/c11-thread-safe-queue
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_BASE_SAFE_QUEUE__
#define __LTS5_BASE_SAFE_QUEUE__

#include <queue>
#include <mutex>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BaseSafeQueue
 *  @brief  Thread safe queue interface
 *  @author Christophe Ecabert
 *  @date   26/08/15
 *  @ingroup utils
 */
template<class T>
class LTS5_EXPORTS BaseSafeQueue {

 public:

  /**
   *  @name BaseSafeQueue
   *  @fn virtual BaseSafeQueue(void)
   *  @brief  Constructor
   */
  BaseSafeQueue(void) : queue_(), mutex_(), capacity_(-1) {}

  /**
   *  @name ~BaseSafeQueue
   *  @fn virtual ~BaseSafeQueue(void)
   *  @brief  Destructor
   */
  virtual ~BaseSafeQueue(void) {}

  /**
   *  @name Push
   *  @fn virtual void Push(const T& object) = 0
   *  @brief  Add new object to the queue
   *  @param[in]  object  Object to add to the queue
   */
  virtual void Push(const T& object) = 0;

  /**
   *  @name Back
   *  @fn T Back(void)
   *  @brief  Remove object from the queue
   *  @param[in]  with_pop  Indique if object need to be removed from the
   *                        queue (i.e. pop())
   *  @param[out] object    Object retrieve from the queue, can be null
   */
  virtual void Back(const bool with_pop, T* object) = 0;

  /**
   * @name  Size
   * @fn    virtual size_t Size(void) const
   * @brief Provide current size of the queue
   * @return    Actual queue size
   */
  virtual size_t Size(void) const {
    return queue_.size();
  }

  /**
   * @name  set_capacity
   * @fn  virtual void set_capacity(const int capacity)
   * @brief Set a new capacity
   * @param[in] capacity  New capacity
   */
  virtual void set_capacity(const int capacity) {
    capacity_ = capacity;
  }


 protected:
  /** Object queue */
  std::queue<T> queue_;
  /** Mutex for thread synchronization */
  mutable std::mutex mutex_;
  /** Capacity */
  int capacity_;
};

}  // namespace LTS5

#endif
