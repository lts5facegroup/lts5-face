/**
 *  @file   shift_operator.hpp
 *  @brief  Extend operator >>, << from >>=, <<=
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   30/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __SHIFT_OPERATOR__
#define __SHIFT_OPERATOR__

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   ShiftOperator
 * @brief   Extend class to add shift operator
 * @author  Christophe Ecabert
 * @date    30/11/2016
 * @see http://www.drdobbs.com/cpp/fixed-point-arithmetic-types-for-c/184401992?pgno=1
 * @tparam T    Class to extend
 * @tparam U    Shift type
 * @ingroup utils
 */
template<typename T, typename U>
class LTS5_EXPORTS ShiftOperator {
 public:

  /**
   * @name  operator>>
   * @fn    friend T operator>>(const T& x, U shift)
   * @brief Right shift operator
   * @param x       Value to shift to the right
   * @param shift   Amount of shift
   * @return        Right shifted value
   */
  friend T operator>>(const T& x, U shift) {
    T tmp(x);
    tmp >>= shift;
    return tmp;
  }

  /**
   * @name  operator<<
   * @fn    friend T operator<<(const T& x, U shift)
   * @brief Left shift operator
   * @param x       Value to shift to the left
   * @param shift   Amount of shift
   * @return        Left shifted value
   */
  friend T operator<<(const T& x, U shift) {
    T tmp(x);
    tmp <<= shift;
    return tmp;
  }
};
}  // namepsace LTS5
#endif //__SHIFT_OPERATOR__
