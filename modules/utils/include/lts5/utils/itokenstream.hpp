/**
 *  @file   itokenstream.hpp
 *  @brief  Stream to parse comma, space separated strings
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   04/09/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_ITOKENSTREAM__
#define __LTS5_ITOKENSTREAM__

#include  <sstream>

#include "lts5/utils/library_export.hpp"
/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  ITokenStream
 *  @brief  Stream to parse comma, space separated strings
 *  @author Christophe Ecabert
 *  @date   04/09/15
 *  @ingroup utils
 */
class LTS5_EXPORTS ITokenStream : public std::istringstream {

 public :

  /**
   *  @name ITokenStream
   *  @fn ITokenStream(void)
   *  @brief  Constructor
   */
  ITokenStream(void) : std::istringstream() {}

  /**
   *  @name ITokenStream
   *  @fn explicit ITokenStream(const std::string& string)
   *  @brief  Constructor
   */
  explicit ITokenStream(const std::string& string) : std::istringstream(string) {}

  /**
   *  @name ITokenStream
   *  @fn explicit ITokenStream(const char* string)
   *  @brief  Constructor
   */
  explicit ITokenStream(const char* string) : std::istringstream(string) {}

  /**
   *  @name ~ITokenStream
   *  @fn virtual ~ITokenStream(void)
   *  @brief  Destructor
   */
  virtual ~ITokenStream(void) {}

  /**
   *  @name operator>>
   *  @fn ITokenStream& operator>>(T& v)
   *  @brief  Operator to write data into input stream
   *  @param[in]  v Input value
   *  @return Current stream
   */
  template<typename T>
  ITokenStream& operator>>(T& v) {
    // Call base operator
    std::istringstream::operator>>(v);
    // Add extra functionnality
    if (this->peek() == ',' || this->peek() == ' ') {
      this->ignore();
    }
    return *this;
  }

};
}  // namespace LTS5
#endif
