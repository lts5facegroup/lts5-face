/**
 *  @file   iterator.hpp
 *  @brief  Various iterator implementation:
 *      - zip: https://gist.github.com/mortehu/373069390c75b02f98b655e3f7dbef9a
 *      - enumerate
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   17/12/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_UTILS_ITERATOR__
#define __LTS5_UTILS_ITERATOR__

#include <iterator>
#include <tuple>
#include <utility>

#include "lts5/utils/library_export.hpp"

#pragma mark -
#pragma mark Zip Iterator
/**
 * @class   ZipHelper
 * @brief   Utility class for zip iterator
 * @date    17/12/2020
 * @ingroup utils
 * @tparam ...T  Zipped type
 */
template <typename... T>
class LTS5_EXPORTS ZipHelper {
 public:
  class ZipIterator : public std::iterator<std::forward_iterator_tag,
          std::tuple<decltype(*std::declval<T>().begin())...>> {
   private:
    std::tuple<decltype(std::declval<T>().begin())...> iters_;

    template <std::size_t... I>
    auto deref(std::index_sequence<I...>) const {
      return typename ZipIterator::value_type{*std::get<I>(iters_)...};
    }

    template <std::size_t... I>
    void increment(std::index_sequence<I...>) {
      auto l = {(++std::get<I>(iters_), 0)...};
    }
   public:
    explicit ZipIterator(decltype(iters_) iters) : iters_{std::move(iters)} {}

    ZipIterator& operator++() {
      increment(std::index_sequence_for<T...>{});
      return *this;
    }

    ZipIterator operator++(int) {
      auto saved{*this};
      increment(std::index_sequence_for<T...>{});
      return saved;
    }

    bool operator!=(const ZipIterator& other) const {
      return iters_ != other.iters_;
    }

    auto operator*() const { return deref(std::index_sequence_for<T...>{}); }
  };

  /**
   * @name  ZipHelper
   * @fn    ZipHelper(T&... seqs)
   * @brief Constructor
   * @param[in] seqs List of sequences to combine
   */
  ZipHelper(T&... seqs) : begin_{std::make_tuple(seqs.begin()...)},
                          end_{std::make_tuple(seqs.end()...)} {
  }

  /**
   * @name  begin
   * @fn    ZipIterator begin() const
   * @brief Beginning of the iterator
   * @return    Iterator
   */
  ZipIterator begin() const { return begin_; }

  /**
   * @name  end
   * @fn    ZipIterator end() const
   * @brief End of the iterator
   * @return    Iterator
   */
  ZipIterator end() const { return end_; }

 private:
  /** Begining of iterator */
  ZipIterator begin_;
  /** End of iterator */
  ZipIterator end_;
};

// Sequences must be the same length.
/**
 * @name    Zip
 * @fn  auto Zip(T&&... seqs)
 * @brief Combine sequences into single iterator. Sequences MUST have the same
 *  length
 * @param[in] ...seqs    List of sequences to combine
 * @tparam T    List of sequence's types
 * @return Zip iterator
 */
template <typename... T>
auto LTS5_EXPORTS Zip(T&&... seqs) {
  return ZipHelper<T...>{seqs...};
}



#endif //__LTS5_UTILS_ITERATOR__
