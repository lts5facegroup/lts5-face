/**
 *  @file   image_transforms.hpp
 *  @brief  Useful transforms for cv::Mat images
 *  @ingroup utils
 *
 *  @author Hua Gao
 *  @date   24/04/15
 *  Copyright (c) 2015 Hua Gao. All rights reserved.
 */

#ifndef __LTS5_IMAGE_TRANSFORMS__
#define __LTS5_IMAGE_TRANSFORMS__

#include <stdio.h>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/shape_transforms.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @enum   WarpingColorSpace
 *  @brief  List possible color space
 *  @ingroup utils
 */
enum WarpingColorSpace {
  /** Default opencv color */
  kBGR = 3,
  /** Default opencv gray */
  kGray = 1,
  /** GUI color */
  kRGB = 3
};

/**
 *  @name   NormalizeScale
 *  @fn void NormalizeScale(const cv::Mat& reference_shape,
                            const int& edge,
                            const cv::Mat& image,
                            cv::Mat* normalized_image,
                            cv::Mat* normalized_shape,
                            cv::Mat* ant,
                            cv::Mat* inverse_transform)
 *  @brief  Normalize shape regarding the scale
 *  @ingroup utils
 *  @param[in]  reference_shape
 *  @param[in]  edge
 *  @param[in]  image               Input image
 *  @param[out] normalized_image    Normalized image used for features
 *                                  extraction
 *  @param[out] normalized_shape    Normalized shape
 *  @param[out] ant                 ?¿?
 *  @param[out] inverse_transform   Transformation applied for
 *                                  normalization
 */
void LTS5_EXPORTS NormalizeScale(const cv::Mat& reference_shape,
                                 const int& edge,
                                 const cv::Mat& image,
                                 cv::Mat* normalized_image,
                                 cv::Mat* normalized_shape,
                                 cv::Mat* ant,
                                 cv::Mat* inverse_transform);

/**
 *  @name   NormalizeScaleAndRotation
 *  @fn void NormalizeScaleAndRotation(const cv::Mat& reference_shape,
                                        const int& edge,
                                        const cv::Mat& image,
                                        cv::Mat* normalized_image,
                                        cv::Mat* normalized_shape,
                                        cv::Mat* ant,
                                        cv::Mat* inverse_transform)
 *  @brief  Normalize shape regarding the scale and rotation
 *  @ingroup utils
 *  @param[in]  reference_shape
 *  @param[in]  edge
 *  @param[in]  image               Input image
 *  @param[out] normalized_image    Normalized image used for features
 *                                  extraction
 *  @param[out] normalized_shape    Normalized shape
 *  @param[out] ant                 ?¿?
 *  @param[out] inverse_transform   Transformation applied for
 *                                  normalization
 */
void LTS5_EXPORTS NormalizeScaleAndRotation(const cv::Mat& reference_shape,
                                            const int& edge,
                                            const cv::Mat& image,
                                            cv::Mat* normalized_image,
                                            cv::Mat* normalized_shape,
                                            cv::Mat* ant,
                                            cv::Mat* inverse_transform);

/**
 *  @name BilinearInterpolation
 *  @fn void BilinearInterpolation(const cv::Mat& image,
                                   const float x,
                                   const float y,
                                   float* value)
 *  @brief  Apply bilinear interpolation at location {x,y}
 *  @ingroup utils
 *  @param[in]  image   Input image to interpolate from
 *  @param[in]  x       X Location
 *  @param[in]  y       Y Location
 *  @param[out] value   Interpolated value
 */
void LTS5_EXPORTS BilinearInterpolation(const cv::Mat& image,
                                        const float x,
                                        const float y,
                                        float* value);

/**
 *  @name BilinearInterpolation
 *  @fn void BilinearInterpolation(const cv::Mat& image,
                                   const float x,
                                   const float y,
                                   unsigned char* value)
 *  @brief  Apply bilinear interpolation at location {x,y}
 *  @ingroup utils
 *  @param[in]  image       Input image to interpolate from
 *  @param[in]  x           X Location
 *  @param[in]  y           Y Location
 *  @param[out] value       Interpolated value
 */
void LTS5_EXPORTS BilinearInterpolation(const cv::Mat& image,
                                        const float x,
                                        const float y,
                                        unsigned char* value);

/**
 *  @name PieceWiseWarping
 *  @fn void PieceWiseWarping(const std::vector<LTS5::Vector2<float>>& src_pts,
                           const std::vector<LTS5::Vector3<int>>& list_tri,
                           const std::vector<LTS5::Vector2<float>>& target_pts,
                           const cv::Mat& image_src,
                           const float scale,
                           const int target_width,
                           const int target_height,
                           const LTS5::WarpingColorSpace color_frmt,
                           const bool inv_y_axis,
                           const int background_value,
                           unsigned char* target_data_array)
 *  @brief  Piecewise warping of a triangulated mesh
 *  @ingroup utils
 *  @param[in]  src_pts       List of point (source)
 *  @param[in]  list_tri      Source point triangulation
 *  @param[in]  target_pts    Target points
 *  @param[in]  image_src     Input image
 *  @param[in]  scale         Scaling factor for target points
 *  @param[in]  target_width  Target image width
 *  @param[in]  target_height Target image height
 *  @param[in]  color_frmt    Target color format
 *  @param[in]  inv_y_axis    Indice if y axis need to be inverted in the target image
 *  @param[in]  background_value  Background color, if <0 not used
 *  @param[out] target_data_array  Warpped image
 */
void LTS5_EXPORTS PieceWiseWarping(const std::vector<LTS5::Vector2<float>>& src_pts,
                                   const std::vector<LTS5::Vector3<int>>& list_tri,
                                   const std::vector<LTS5::Vector2<float>>& target_pts,
                                   const cv::Mat& image_src,
                                   const float scale,
                                   const int target_width,
                                   const int target_height,
                                   const LTS5::WarpingColorSpace color_frmt,
                                   const bool inv_y_axis,
                                   const int background_value,
                                   unsigned char* target_data_array);

}
#endif /* __LTS5_IMAGE_TRANSFORMS__ */
