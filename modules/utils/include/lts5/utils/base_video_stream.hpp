/**
 *  @file   base_video_stream.hpp
 *  @brief  Utility class to handle video stream from Camera/Webcam/Video file
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   16/11/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_BASE_VIDEO_STREAM__
#define __LTS5_BASE_VIDEO_STREAM__

#include "opencv2/core/core.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BaseVideoStream
 *  @brief  Interface for Video stream object
 *  @author Christophe Ecabert
 *  @date   16/11/15
 *  @ingroup utils
 */
class BaseVideoStream {

 public :
  /**
   *  @name ~BaseVideoStream
   *  @fn virtual ~BaseVideoStream(void)
   *  @brief  Destructor
   */
  virtual ~BaseVideoStream(void) {}

  /**
   *  @name Open
   *  @fn virtual bool Open(const int idx) = 0
   *  @brief Open a specific camera stream
   *  @return false if error, true otherwise
   */
  virtual bool Open(const int idx) = 0;

  /**
   *  @name IsOpen
   *  @fn virtual bool IsOpen(void) = 0
   *  @brief  Indicate if the stream has been properly initialized
   *  @return false if error, true otherwise
   */
  virtual bool IsOpen(void) = 0;

  /**
   *  @name Grab
   *  @fn virtual bool Grab(void) = 0
   *  @brief  Grab a frame from the current stream. Trigger the acquisition
   *          (fast methods)
   *  @return True on success, false otherwise
   */
  virtual bool Grab(void) = 0;

  /**
   *  @name Retrieve
   *  @fn virtual void Retrieve(cv::Mat* frame) = 0
   *  @brief  This method decode and retrieved the last grabbed frame
   *  @param[out] frame   Acquired frame
   */
  virtual void Retrieve(cv::Mat* frame) = 0;

  /**
   *  @name GetProperty
   *  @fn virtual double GetProperty(const int property_id) const = 0
   *  @brief  Get the value of a given properties
   *  @param[in]  property_id   Id of the property to look for.
   *  @return Property value
   */
  virtual double GetProperty(const int property_id) const = 0;

  /**
   *  @name SetProperty
   *  @fn virtual void SetProperty(const int property_id, const double value) = 0
   *  @brief  Set the value of a specific property
   *  @param[in]  property_id   Id of the property to look for.
   *  @param[in]  value         Value of the property
   */
  virtual void SetProperty(const int property_id, const double value) = 0;
};

}  // namespace LTS5
#endif /* __LTS5_BASE_VIDEO_STREAM__ */
