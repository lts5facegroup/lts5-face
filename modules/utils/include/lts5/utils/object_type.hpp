/**
 *  @file   object_type.hpp
 *  @brief  Global type definition
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   02/06/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_OBJECT_TYPE__
#define __LTS5_OBJECT_TYPE__

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

  /**
   *  @enum   HeaderObjectType
   *  @brief  List of possible object type used in header's configuration
   *          file
   *  @ingroup utils
   */
  enum class HeaderObjectType : int {
    /** Sdm */
    kSdm = 0x001,
    /** Face tracker quality assessment, svm based */
    kSvmTrackerAssessment = 0x002,
    /** Face tracker quality assessment trainer */
    kSvmTrackerAssessmentTrainer = 0x003,
    /** Face tracker quality assessment, const based */
    kConstTrackerAssessment = 0x004,
    /** Face tracker quality assessment, trainer */
    kConstTrackerAssessmentTrainer = 0x005,
    /** Point distribution model */
    kPointDistributionModel = 0x006,
    /** Binary Linear SVM */
    kBinaryLinearSvm = 0x007,
    /** Emotion Detector */
    kExpressionDetector = 0x008,
    /** Emotion Analysis */
    kExpressionAnalysis = 0x009,
    /** Random Fern */
    kRandomFern = 0x00A,
    /** Cascaded Random Fern */
    kCascadedRandomFern = 0x00B,
    /** CPR face tracker */
    kCPRTracker = 0x00C,
    /** Intraface face tracker */
    kIntrafaceTracker = 0x00D,
    /** AU Detector */
    kAUDetector = 0x00E,
    /** AU Analysis */
    kAUAnalysis = 0x00F,
    /** LBF face tracker */
    kLBFTracker = 0x010,
    /** Binary Sparse Linear SVM */
    kBinarySparseLinearSvm = 0x011,
    /** LBF Tracker assessment */
    kLBFTrackerAssessment = 0x012,
    /** PCA Model */
    kPCAModel = 0x013,
    /** 3D statistical Shape Model */
    kShapeModel = 0x14,
    /** 3D statistical Multi Shape Model */
    kMultiShapeModel = 0x15,
    /** Statistical Color Model */
    kColorModel = 0x016,
    /** Statistical Texture Model */
    kTextureModel = 0x017,
    /** Morphable Model */
    kMorphableModel = 0x018,
    /** Color Morphable Model */
    kColorMorphableModel = 0x019,
    /** Texture Morphable Model */
    kTextureMorphableModel = 0x01A,
    /** Multi shape model with segment */
    kMultiShapeSegmentModel = 0x01B,
    /** UnstructuredPointDomain */
    kUnstructuredPointDomain = 0x01C,
    /** TriangleMesh */
    kTriangleMesh = 0x01D,
    /** DiscreteKLBasis */
    kDiscreteKLBasis = 0x01E
  };
}
#endif
