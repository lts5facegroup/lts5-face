/**
 *  @file   unit_step_operator.hpp
 *  @brief  Add post increment/decrment from a given pre increment/decrement
 *          operator
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   30/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __UNIT_STEP_OPERATOR__
#define __UNIT_STEP_OPERATOR__

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   UnitStepOperator
 * @brief   Extend class to add post increment/decrement
 * @author  Christophe Ecabert
 * @date    30/11/2016
 * @see     http://www.drdobbs.com/cpp/fixed-point-arithmetic-types-for-c/184401992?pgno=1
 * @tparam T    Class to extend
 * @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS UnitStepOperator {
 public:

  /**
   * @name  operator++
   * @fn    friend T operator++(T& x, int)
   * @brief Post-fix increment
   * @param x   Value to increment
   * @return    Value before increment
   */
  friend T operator++(T& x, int) {
    T current(x);   // Copy current value
    ++x;            // prefix-increment this instance
    return current; // Return value before increment
  }

  /**
   * @name  operator--
   * @fn    friend T operator--(T& x, int)
   * @brief Post-fix decrement
   * @param x   Value to decrement
   * @return    Value before decrement
   */
  friend T operator--(T& x, int) {
    T current(x);   // Copy current value
    --x;            // prefix-decrement this instance
    return current; // Return value before decrement
  }
};
}  // namepsace LTS5
#endif //__UNIT_STEP_OPERATOR__
