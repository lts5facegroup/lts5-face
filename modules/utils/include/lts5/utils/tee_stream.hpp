/**
 *  @file   tee_stream.hpp
 *  @brief  Mechanism to duplicate stream and dump information into various
 *          streams. I.e. file + console similar to `tee` on linux
 *  @ingroup utils
 *  @see    https://stackoverflow.com/a/1761027/4546884
 *
 *  @author Christophe Ecabert
 *  @date   9/12/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_TEE_STREAM__
#define __LTS5_TEE_STREAM__

#include <iostream>
#include <vector>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class     TeeStreambuf
 * @brief     Mirroring stream buffers
 * @author    Christophe Ecabert
 * @date      12.09.18
 * @see       https://stackoverflow.com/a/1761027/4546884
 * @ingroup   utils
 */
class LTS5_EXPORTS TeeStreambuf : public std::basic_streambuf<char, std::char_traits<char>> {
 private:
  /** Base class type */
  using base_t = std::basic_streambuf<char, std::char_traits<char>>;

 public:

  /** Integer type */
  using int_type = typename base_t::int_type;
  /** Traits type */
  using traits_type = typename base_t::traits_type;

  /**
   * @name ~TeeStreambuf
   * @fn  ~TeeStreambuf() override = default
   * @brief Destructor
   */
  ~TeeStreambuf() override = default;

  /**
   * @name    InsertBuffer
   * @fn  void InsertBuffer(base_t* streambuf)
   * @brief Add a streambuf where to mirror the data.
   * @param[in] streambuf Stream to add
   */
  void InsertBuffer(base_t* streambuf);

  /**
   *  @name   overflow
   *  @fn     virtual int_type overflow(int_type c) override
   *  @brief  C.f. C++ standard section 27.5.2.4.5
   */
  int_type overflow(int_type c/*=traits_type::eof()*/) override;

  /**
   *  @brief  Synchronizes the buffer arrays with the controlled sequences.
   *  @return  -1 on failure.
   *
   *  Each derived class provides its own appropriate behavior,
   *  including the definition of @a failure.
   *  @note  Base class version does nothing, returns zero.
  */
  int sync() override;

 private:
  /** Mirror streams */
  std::vector<base_t*> streams_;
};

/**
 *  @class  TeeStream
 *  @brief  Mirroring stream
 *  @author Christophe Ecabert
 *  @date   12.09.18
 *  @see    https://stackoverflow.com/a/1761027/4546884
 *  @ingroup utils
 */
class LTS5_EXPORTS TeeStream : public std::ostream {
 public:

  /**
   * @name  TeeStream
   * @fn    TeeStream()
   * @brief Constructor
   */
  TeeStream();

  /**
   * @name  LinkStream
   * @fn    void LinkStream(std::ostream& stream)
   * @brief Link this stream with another one (Aggregation)
   * @param[in] stream Stream to link with
   */
  void LinkStream(std::ostream& stream);

  /**
   * @name  ~TeeStream
   * @fn    ~TeeStream() override = default
   * @brief Destructor
   */
  ~TeeStream() override = default;

 private:
  /** Stream buffer */
  TeeStreambuf streambuf_;
};

}  // namespace LTS5
#endif  // __LTS5_TEE_STREAM__
