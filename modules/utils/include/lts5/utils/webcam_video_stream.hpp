/**
 *  @file   webcam_video_stream.hpp
 *  @brief  Utility class to handle webcam video stream
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   16/11/15
 *  Copyright (c) 2015 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_WEBCAM_VIDEO_STREAM__
#define __LTS5_WEBCAM_VIDEO_STREAM__

#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/base_video_stream.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  WebcamStream
 *  @brief  Helper class to handle webcam stream
 *  @author Christophe Ecabert
 *  @date   16/11/15
 *  @ingroup utils
 */
class LTS5_EXPORTS WebcamStream : public BaseVideoStream {

#pragma mark -
#pragma mark Initialization
 public :
  /**
   *  @name WebcamStream
   *  @fn WebcamStream(void)
   *  @brief  Constructor
   */
  WebcamStream(void);

  /**
   *  @name WebcamStream
   *  @fn explicit WebcamStream(const int idx)
   *  @brief  Constructor
   *  @param[in]  idx Index of the camera to open
   */
  explicit WebcamStream(const int idx);

  /**
   *  @name Open
   *  @fn bool Open(const int idx)
   *  @brief Open a specific camera stream
   *  @return false if error, true otherwise
   */
  bool Open(const int idx);

  /**
   *  @name ~WebcamStream
   *  @fn ~WebcamStream(void)
   *  @brief  Destructor
   */
  ~WebcamStream(void);

  /**
   *  @name IsOpen
   *  @fn bool IsOpen(void)
   *  @brief  Indicate if the stream has been properly initialized
   *  @return false if error, true otherwise
   */
  bool IsOpen(void);

#pragma mark -
#pragma mark Process

  /**
   *  @name Grab
   *  @fn bool Grab(void)
   *  @brief  Grab a frame from the current stream. Trigger the acquisition
   *          (fast methods)
   *  @return True on success, false otherwise
   */
  bool Grab(void);

  /**
   *  @name Retrieve
   *  @fn void Retrieve(cv::Mat* frame)
   *  @brief  This method decode and retrieved the last grabbed frame
   *  @param[out] frame   Acquired frame
   */
  void Retrieve(cv::Mat* frame);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name GetProperty
   *  @fn double GetProperty(const int property_id) const
   *  @brief  Get the value of a given properties
   *  @param[in]  property_id   Id of the property to look for.
   *  @return Property value
   */
  double GetProperty(const int property_id) const;

  /**
   *  @name SetProperty
   *  @fn void SetProperty(const int property_id, const double value)
   *  @brief  Set the value of a specific property
   *  @param[in]  property_id   Id of the property to look for.
   *  @param[in]  value         Value of the property
   */
  void SetProperty(const int property_id, const double value);

#pragma mark -
#pragma mark Private
 private :

  /**
   *  @name WebcamStream
   *  @fn WebcamStream(const WebcamStream& other)
   *  @brief  Copy constructor, not allowed
   */
  WebcamStream(const WebcamStream& other);

  /** Video Capture */
  cv::VideoCapture* cap_;

};

}  // namespace LTS5


#endif /* __LTS5_WEBCAM_VIDEO_STREAM__ */
