/**
 *  @file   string_util.hpp
 *  @brief  Utility functions for string manipulation
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   28/04/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __STRING_UTIL__
#define __STRING_UTIL__

#include <string>
#include <vector>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @name  SplitString
 * @fn  void SplitString(const std::string& input,
                         const std::string& sep,
                         std::vector<std::string>* parts)
 * @brief Split a string into parts for a given separator
 * @ingroup utils
 * @param[in]   input   Input string to fraction
 * @param[in]   sep     String separator
 * @param[out]  parts   Separated string
 */
void LTS5_EXPORTS SplitString(const std::string& input,
                              const std::string& sep,
                              std::vector<std::string>* parts);

/**
 *  @name ExtractDirectory
 *  @fn void ExtractDirectory(const std::string& path,
                              std::string* dir,
                              std::string* file,
                              std::string* ext)
 *  @brief  Split path into directory + extension
 *  @ingroup utils
 *  @param[in]  path  Path where to extract data
 *  @param[out] dir   Extracted directory
 *  @param[out] file  Extracted filename
 *  @param[out] ext   Extracted extension
 */
void LTS5_EXPORTS ExtractDirectory(const std::string& path,
                                   std::string* dir,
                                   std::string* file,
                                   std::string* ext);

/**
 *  @namespace  Path
 *  @brief      Path processing
 */
namespace Path {

namespace internal {
std::string LTS5_EXPORTS JoinImpl(std::initializer_list<std::string> parts);
}

/**
 *  @name   Join
 *  @fn     std::string Join(const T&... parts)
 *  @tparam T Pack strings to merge together
 *  @brief  Merge filename component into a single valid path. Similar to
 *          python `os.path.join` method
 *  @param[in] parts  Packed parameters to aggregate together
 *  @return Appended path
 */
template<typename... T>
std::string LTS5_EXPORTS Join(const T&... parts) {
  return internal::JoinImpl({parts...});
}

/**
 *  @name   IsAbsolute
 *  @fn     bool IsAbsolute(const std::string& path)
 *  @brief  Check if a given `path`is absolute or not (i.e. start with '/').
 *  @param[in] path Path to check
 *  @return True if absolute, false otherwise.
 */
bool LTS5_EXPORTS IsAbsolute(const std::string& path);

/**
 *  @name   Dirname
 *  @fn     std::string Dirname(const std::string& path)
 *  @brief  Return the part in front of the last '/' in `path`. If there is no
 *          '/' give an empty string.
 *          Similar to `os.path.dirname`
 *  @param[in] path Path to file
 *  @return Complete file's directory or empty string.
 */
std::string LTS5_EXPORTS Dirname(const std::string& path);


/**
 *  @name   Basename
 *  @fn     std::string Basename(const std::string& path)
 *  @brief  Extract file name from path (i.e. part after last '/'). If there
 *          is no '/' it returns the same as the input.
 *          Similar to `os.path.basename`
 *  @param[in] path Path to file
 *  @return File name
 */
std::string LTS5_EXPORTS Basename(const std::string& path);

/**
 *  @name   Extension
 *  @fn     std::string Extension(const std::string& path)
 *  @brief  Extract file's extension (i.e part after the last '.'). If there
 *          is not '.' return an empty string
 *  @param[in] path Path to file
 *  @return File's extension or empty string
 */
std::string LTS5_EXPORTS Extension(const std::string& path);

/**
 *  @name   Clean
 *  @fn     std::string Clean(const std::string& path)
 *  @brief  Remove duplicate, add ./
 *  @param[in] path Path to clean
 *  @return cleaned path
 */
std::string LTS5_EXPORTS Clean(const std::string& path);

/**
 *  @name SplitComponent
 *  @fn void SplitComponent(const std::string& path, std::string* dir,
                            std::string* file, std::string* ext)
 *  @brief  Split path into directory + file + extension
 *  @param[in]  path  Path where to extract data
 *  @param[out] dir   Extracted directory, skipped if nullptr
 *  @param[out] file  Extracted filename, skipped if nullptr
 *  @param[out] ext   Extracted extension, skipped if nullptr
 */
void LTS5_EXPORTS SplitComponent(const std::string& path,
                                 std::string* dir,
                                 std::string* file,
                                 std::string* ext);

}  // namepsace Path
}  // namepsace LTS5
#endif //__STRING_UTIL__
