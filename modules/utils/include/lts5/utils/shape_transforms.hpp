/**
 *  @file   shape_transforms.hpp
 *  @brief  Useful transforms for cv::Mat shapes
 *  @ingroup utils
 *
 *  @author Gabriel Cuendet
 *  @date   24/04/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_SHAPE_TRANSFORMS__
#define __LTS5_SHAPE_TRANSFORMS__

#include <stdio.h>
#include <unordered_set>
#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @name   ImageAbsToRectAbs
 *  @fn void ImageAbsToRectAbs(const cv::Rect& rectangle, cv::Mat* shape)
 *  @brief  Convert shape coordinates from absolute image coordinates to
 *          absolute rectangle coordinates
 *  @ingroup utils
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 */
void LTS5_EXPORTS ImageAbsToRectAbs(const cv::Rect& rectangle, cv::Mat* shape);

/**
 *  @name   ImageAbsToRectAbs
 *  @fn cv::Mat ImageAbsToRectAbs(const cv::Rect& rectangle, const cv::Mat& shape)
 *  @brief  Convert shape coordinates from absolute image coordinates to
 *          absolute rectangle coordinates
 *  @ingroup utils
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 *  @return Converted shape
 */
cv::Mat LTS5_EXPORTS ImageAbsToRectAbs(const cv::Rect& rectangle,
                                       const cv::Mat& shape);

/**
 *  @name   ImageAbsToRectRel
 *  @fn void ImageAbsToRectRel(const cv::Rect& rectangle, cv::Mat* shape)
 *  @brief  Convert shape coordinates from absolute image coordinates to
 *          relative rectangle coordinates
 *  @ingroup utils
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 */
void LTS5_EXPORTS ImageAbsToRectRel(const cv::Rect& rectangle,
                                    cv::Mat* shape);

/**
 *  @name   ImageAbsToRectRel
 *  @fn cv::Mat ImageAbsToRectRel(const cv::Rect& rectangle, const cv::Mat& shape)
 *  @brief  Convert shape coordinates from absolute image coordinates to
 *          relative rectangle coordinates
 *  @ingroup utils
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 *  @return Converted shape
 */
cv::Mat LTS5_EXPORTS ImageAbsToRectRel(const cv::Rect& rectangle,
                                       const cv::Mat& shape);

/**
 *  @name   RectAbsToImageAbs
 *  @fn void RectAbsToImageAbs(const cv::Rect& rectangle, cv::Mat* shape)
 *  @brief  Convert shape coordinates from absolute rectangle coordinates to
 *          absolute image coordinates
 *  @ingroup utils
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 */
void LTS5_EXPORTS RectAbsToImageAbs(const cv::Rect& rectangle,
                                    cv::Mat* shape);

/**
 *  @name   RectAbsToImageAbs
 *  @fn cv::Mat RectAbsToImageAbs(const cv::Rect& rectangle, const cv::Mat& shape)
 *  @brief  Convert shape coordinates from absolute rectangle coordinates to
 *          absolute image coordinates
 *  @ingroup utils
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 *  @return Converted shape
 */
cv::Mat LTS5_EXPORTS RectAbsToImageAbs(const cv::Rect& rectangle,
                                       const cv::Mat& shape);

/**
 *  @name   RectRelToImageAbs
 *  @fn void RectRelToImageAbs(const cv::Rect& rectangle, cv::Mat* shape)
 *  @brief  Convert shape coordinates from relative rectangle coordinates to
 *          absolute image coordinates
 *  @ingroup utils
 *  @param[in]      rectangle   reference rectangle
 *  @param[in,out]  shape       shape to convert (will be modified!)
 */
void LTS5_EXPORTS RectRelToImageAbs(const cv::Rect& rectangle,
                                       cv::Mat* shape);

/**
 *  @name   RectRelToImageAbs
 *  @fn cv::Mat RectRelToImageAbs(const cv::Rect& rectangle, const cv::Mat& shape)
 *  @brief  Convert shape coordinates from relative rectangle coordinates to
 *          absolute image coordinates
 *  @ingroup utils
 *  @param[in]  rectangle   reference rectangle
 *  @param[in]  shape       shape to convert (will be NOT be modified!)
 *  @return The modified shape
 */
cv::Mat LTS5_EXPORTS RectRelToImageAbs(const cv::Rect& rectangle,
                                       const cv::Mat& shape);

/**
 *  @name   TransformPoint
 *  @fn void TransformPoint(const cv::Mat& transform, cv::Point2d* point)
 *  @brief  Transform a 2D point according to 2x3 matrix
 *  @ingroup utils
 *  @param[in]      transform   transform matrix
 *  @param[in,out]  point       point to transform (will be modified!)
 */
void LTS5_EXPORTS TransformPoint(const cv::Mat& transform, cv::Point2d* point);

/**
 *  @name   TransformPoint
 *  @fn cv::Point2d TransformPoint(const cv::Mat& transform, const cv::Point2d& point)
 *  @brief  Transform a 2D point according to 2x3 matrix
 *  @ingroup utils
 *  @param[in]  transform   transform matrix
 *  @param[in]  point       point to transform (will be NOT be modified!)
 *  @return The transformed point
 */
cv::Point2d LTS5_EXPORTS TransformPoint(const cv::Mat& transform,
                                        const cv::Point2d& point);

/**
 *  @name   transformShape
 *  @fn void TransformShape(const cv::Mat& transform, cv::Mat* shape)
 *  @brief  Transform a shape
 *  @ingroup utils
 *  @param[in]  transform   transform matrix
 *  @param[in]  shape       shape to transform (will be modified!)
 */
void LTS5_EXPORTS TransformShape(const cv::Mat& transform,
                                 cv::Mat* shape);

/**
 *  @name   transformShape
 *  @fn cv::Mat TransformShape(const cv::Mat& transform, const cv::Mat& shape)
 *  @brief  Transform a shape
 *  @ingroup utils
 *  @param[in]  transform   transform matrix
 *  @param[in]  shape       shape to transform (will NOT be modified!)
 *  @return Transformed shape
 */
cv::Mat LTS5_EXPORTS TransformShape(const cv::Mat& transform,
                                    const cv::Mat& shape);

/**
 *  @name   GetCenterPoint
 *  @fn cv::Point2d GetCenterPoint(const cv::Mat& shape)
 *  @brief  Get the center of a shape
 *  @ingroup utils
 *  @param[in]  shape   The shape from which to compute the center
 *  @return the center of the shape as a cv::Point2d
 */
cv::Point2d LTS5_EXPORTS GetCenterPoint(const cv::Mat& shape);

/**
 *  @name   ComputeRigidTransform
 *  @fn cv::Vec<double, 4> ComputeRigidTransform(const cv::Mat& source,
                                                  const cv::Mat& dest)
 *  @brief  Compute a rigid transform between to shapes
 *  @ingroup utils
 *  @param[in]  source  The source shape
 *  @param[in]  dest    The destination shape
 *  @return Elements of the transform matrix [a, b, tx, ty] such that T*src=dst
 */
cv::Vec<double, 4> LTS5_EXPORTS ComputeRigidTransform(const cv::Mat& source,
                                                      const cv::Mat& dest);


/**
 *  @name   ComputeSimilarityTransform
 *  @fn void ComputeSimilarityTransform(const cv::Mat& source,
                                        const cv::Mat& dest,
                                        cv::Vec4d* transformation)
 *  @brief  Compute the similarity transform between two shapes
 *  @ingroup utils
 *  @param[in]  source            Source shape
 *  @param[in]  dest              Target shape
 *  @param[out] transformation    Linear Transformation [a, b, tx, ty] between
 *                                the two shapes such that T*source = dest.
 */
void LTS5_EXPORTS ComputeSimilarityTransform(const cv::Mat& source,
                                             const cv::Mat& dest,
                                             cv::Vec4d* transformation);

/**
 *  @name ComputeSimilarityTransform
 *  @fn void ComputeSimilarityTransform(const cv::Mat& source,
                                 const cv::Mat& dest,
                                 cv::Mat* transform);
 *  @brief  Compute rigid transform between two shapes (x1,x2,...y1,y2,..)
 *  @ingroup utils
 *  @param[in]  source      Reference shape
 *  @param[in]  dest        Target shape (sR * src + T)
 *  @param[out] transform   Rigid transformation
 */
void LTS5_EXPORTS ComputeSimilarityTransform(const cv::Mat& source,
                                             const cv::Mat& dest,
                                             cv::Mat* transform);

/**
 *  @name SimilarityTransformXYXY
 *  @fn void SimilarityTransformXYXY(const cv::Mat& source,
                                     const cv::Mat& dest,
                                     cv::Mat* transform);
 *  @brief  Compute rigid transform between two shape ordered (x1,y1,x2,y2,..)
 *  @ingroup utils
 *  @param[in]  source      Reference shape
 *  @param[in]  dest        Target shape (sR * src + T)
 *  @param[out] transform   Rigid transformation
 */
void LTS5_EXPORTS SimilarityTransformXYXY(const cv::Mat& source,
                                          const cv::Mat& dest,
                                          cv::Mat* transform);

/**
 *  @name   Euler2Rot
 *  @fn void Euler2Rot(const double pitch,
                        const double yaw,
                        const double roll,
                        cv::Mat* R,
                        bool full = true)
 *  @brief  Converting from Euler angles Rx * Ry * Rz rotation to Rotation matrix
 *   Full indicates if a 3x3 or a 2x3 matrix is needed (in 2x3 case we don't care about resulting z component)
 *  @ingroup utils
 *  @param[in]   pitch  Head rotation angle in pitch
 *  @param[in]   yaw    Head rotation angle in yaw
 *  @param[in]   roll   Head rotation angle in roll
 *  @param[in]   full   Full rotation matrix
 *  @param[out]  R      Rotation matrix
 */
void LTS5_EXPORTS Euler2Rot(const double pitch,
                            const double yaw,
                            const double roll,
                            cv::Mat* R,
                            bool full = true);


/**
 *  @name       Rot2Euler
 *  @fn void Rot2Euler(const cv::Mat &R,double* pitch, double* yaw, double* roll)
 *  @brief      Converting from rotation matrix to rotation angles
 *  @ingroup utils
 *  @param[in]    R        Rotation matrix
 *  @param[out]   pitch    Head rotation angle in pitch
 *  @param[out]   yaw      Head rotation angle in yaw
 *  @param[out]   roll     Head rotation angle in roll
 */
template<typename T>
void LTS5_EXPORTS Rot2Euler(const cv::Mat &R, T* pitch, T* yaw, T* roll);

/**
 *  @name     AddOrthRow
 *  @fn void AddOrthRow(cv::Mat* R)
 *  @brief    Add the third orthogonal row vector to the rotation matrix
 *  @ingroup utils
 *  @param[in,out]   R   Rotation matrix
 */
void LTS5_EXPORTS AddOrthRow(cv::Mat* R);

/**
 *  @name         AlignShapes3D2D
 *  @fn void AlignShapes3D2D(const cv::Mat &s2D,
                              const cv::Mat &s3D,
                              double* scale,
                              double* pitch,
                              double* yaw,
                              double* roll,
                              double* x,
                              double* y)
 *  @brief        Align a 3D shape to a 2D shape
 *  @ingroup utils
 *  @param[in]    s2D       2D shape
 *  @param[in]    s3D       3D shape
 *  @param[out]   scale     scaling factor
 *  @param[out]   pitch     Head rotation angle in pitch
 *  @param[out]   yaw       Head rotation angle in yaw
 *  @param[out]   roll      Head rotation angle in roll
 *  @param[out]   x         Translation in x
 *  @param[out]   y         Translation in y
 */
void LTS5_EXPORTS AlignShapes3D2D(const cv::Mat &s2D,
                                  const cv::Mat &s3D,
                                  double* scale,
                                  double* pitch,
                                  double* yaw,
                                  double* roll,
                                  double* x,
                                  double* y);

//void Align3Dto2DShapes(double& scale,double& pitch,double& yaw,double& roll,
//                       double& x,double& y,cv::Mat &s2D,cv::Mat &s3D);

/**
 *  @name FlipShape
 *  @fn cv::Mat FlipShape(const cv::Mat& shape, const cv::Size& image_size)
 *  @brief  Horizontally flips a shape
 *  @ingroup utils
 *  @param[in]  shape       Shape to flip
 *  @param[in]  image_size  Image size
 */
cv::Mat LTS5_EXPORTS FlipShape(const cv::Mat& shape,
                               const cv::Size& image_size);

/**
 *  @name ComputeMeanShape
 *  @fn void ComputeMeanShape(const std::vector<cv::Mat>& shapes, cv::Mat* mean_shape)
 *  @brief Computes the mean shape of a vector of shapes
 *  @ingroup utils
 *  @param[in]   shapes     Vector of cv::Mat to average into mean shape
 *  @param[out]  mean_shape Resulting mean shape
 */
void LTS5_EXPORTS ComputeMeanShape(const std::vector<cv::Mat>& shapes,
                                   cv::Mat* mean_shape);

/**
 *  @name ComputeRelativeMeanShape
 *  @fn void ComputeRelativeMeanShape(const std::vector<cv::Mat>& shapes,
                                          const std::vector<cv::Rect>& rects,
                                          cv::Mat* mean_shape)
 *  @brief Computes the mean shape of a vector of shapes, relatively to a vector
 *         of rectangles (calls ImageAbsToRectRel)
 *  @ingroup utils
 *  @param[in]   shapes     Vector of cv::Mat to average into mean shape
 *  @param[in]   rects      Vector of cv::Rect of the same length as shapes
 *  @param[out]  mean_shape Resulting mean shape
 */
void LTS5_EXPORTS ComputeRelativeMeanShape(const std::vector<cv::Mat>& shapes,
                                           const std::vector<cv::Rect>& rects,
                                           cv::Mat* mean_shape);

/**
 *  @name   KeypointsFromShape
 *  @fn std::vector<cv::KeyPoint> KeypointsFromShape(const cv::Mat& shape,
                                                     const float& diameter)
 *  @brief  Convert a shape into a vector of cv::KeyPoint
 *  @ingroup utils
 *  @param[in]      shape       Shape
 *  @param[in]      diameter    Diameter of the keypoint (opencv requirement)
 *  @return The vector of cv::KeyPoint
 */
std::vector<cv::KeyPoint> LTS5_EXPORTS KeypointsFromShape(const cv::Mat& shape,
                                                          const float& diameter);

/**
 *  @name   KeypointsFromShape
 *  @fn std::vector<cv::KeyPoint> KeypointsFromShape(const cv::Mat& shape,
                                                     const float& diameter,
                                                     const std::unordered_set<int>&
                                                     landmarks)
 *  @brief  Convert a shape into a vector of cv::KeyPoint
 *  @ingroup utils
 *  @param[in]      shape       Shape
 *  @param[in]      diameter    Diameter of the keypoint (opencv requirement)
 *  @param[in]      landmarks   Set of landmarks to include
 *  @return The vector of cv::KeyPoint
 */
std::vector<cv::KeyPoint> LTS5_EXPORTS KeypointsFromShape(const cv::Mat& shape,
                                                          const float& diameter,
                                                          const std::unordered_set<int>&
                                                          landmarks);

/**
 *  @name   ExtractInnerShape
 *  @fn void ExtractInnerShape(const cv::Mat& shape, cv::Mat* inner_shape)
 *  @brief  From a Sdm shape, extract the inner shape
 *  @ingroup utils
 *  @param[in]  shape   Shape from sdm tracker (i.e. 68 / 66 pts)
 *  @param[out] inner_shape Inner shape of sdm shape
 */
void LTS5_EXPORTS ExtractInnerShape(const cv::Mat& shape, cv::Mat* inner_shape);

/**
 *  @name Convert68ShapeTo66
 *  @fn void Convert68ShapeTo66(const cv::Mat& shape_68, cv::Mat* shape_66)
 *  @brief  Convert a 68 points SDM shape to 66 points CLM shape
 *  @ingroup utils
 *  @param[in]  shape_68  68 points shape
 *  @param[out] shape_66  66 ponts shape
 */
void LTS5_EXPORTS Convert68ShapeTo66(const cv::Mat& shape_68, cv::Mat* shape_66);

/**
 *  @name TriangulatePoints
 *  @fn void TriangulatePoints(const std::vector<LTS5::Vector2<T>>& pts,
                               const int width, const int height,
                               std::vector<LTS5::Vector3<int>>* tri)
 *  @brief  Perform Delauny triangulation on a given set of points
 *  @ingroup utils
 *  @param[in]  pts P   oints to triangulate
 *  @param[in]  width   Region width
 *  @param[in]  height  Region height
 *  @param[out] tri     List of corresponding triangle
 */
template<typename T>
void LTS5_EXPORTS TriangulatePoints(const std::vector<LTS5::Vector2<T>>& pts,
                                    const int width, const int height,
                                    std::vector<LTS5::Vector3<int>>* tri);

/**
 *  @name DrawTriangulation
 *  @fn void DrawTriangulation(const cv::Mat& image,
                               const std::vector<LTS5::Vector2<T>>& pts,
                               const std::vector<LTS5::Vector3<int>>& list_tri,
                               cv::Mat* canvas)
 *  @brief  Draw a given triangulation on an image
 *  @ingroup utils
 *  @param[in]  image     Image to draw on
 *  @param[in]  pts       List of points
 *  @param[in]  list_tri  List of triangles
 *  @param[in]  color     Color of the triangle to draww
 *  @param[out] canvas    Image with triangulation draw on it
 */
template<typename T>
void LTS5_EXPORTS DrawTriangulation(const cv::Mat& image,
                                    const std::vector<LTS5::Vector2<T>>& pts,
                                    const std::vector<LTS5::Vector3<int>>& list_tri,
                                    const Vector3<unsigned char>& color,
                                    cv::Mat* canvas);

/**
 *  @name NormalizeShape
 *  @fn void NormalizeShapeXY(const cv::Mat& shape,
                               const cv::Rect& bbox,
                               cv::Mat* norm_shape)
 *  @brief  Normalize a given shape
 *  @ingroup utils
 *  @param[in]  shape   Shape to normalize
 *  @param[in]  bbox    Face bounding box where shape stands
 *  @param[out] norm_shape  Normalized shape
 */
template<typename T>
void LTS5_EXPORTS NormalizeShapeXY(const cv::Mat& shape,
                                   const cv::Rect& bbox,
                                   cv::Mat* norm_shape);

/**
 *  @name PlaceShapeShape
 *  @fn void PlaceShapeXY(const cv::Mat& shape,
                           const cv::Rect& bbox,
                           cv::Mat* placed_shape)
 *  @brief  Place shape at specific location according a given bounding box
 *  @ingroup utils
 *  @param[in]  shape   Shape to normalize
 *  @param[in]  bbox    Face bounding box where shape stands
 *  @param[out] placed_shape  Placed shape
 */
template<typename T>
void LTS5_EXPORTS PlaceShapeXY(const cv::Mat& shape,
                               const cv::Rect& bbox,
                               cv::Mat* placed_shape);
}
#endif /* __LTS5_SHAPE_TRANSFORMS__ */
