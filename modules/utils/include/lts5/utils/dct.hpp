/**
 *  @file   dct.hpp
 *  @brief  DCT methods implementation
 *  @ingroup utils
 *
 *  @author Marina Zimmermann
 *  @date   24/07/16
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */

#ifndef __LTS5_DCT_HPP__
#define __LTS5_DCT_HPP__

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @name zigzagMatrix
 *  @fn     void zigzagMatrix(const cv::Mat& matrix,
                               const cv::Size normalized_size,
                               cv::Mat* full_vect,
                               cv::Mat* odd_vect,
                               cv::Mat* even_odd_vect)
 *  @brief  Reads a matrix in zigzag order and returns vectors containing
 *          coefficients from the full, odd or even and odd columns
 *  @ingroup utils
 *  @param[in]  matrix              Input matrix
 *  @param[in]  normalized_size     Size to normalize image to
 *  @param[out] full_vect           Full output in zigzag order (with DC)
 *  @param[out] odd_vect            Odd output in zigzag order (no DC)
 *  @param[out] even_odd_vect       Even and odd output in zigzag order (no DC)
 */
void LTS5_EXPORTS zigzagMatrix(const cv::Mat& matrix,
                               const cv::Size normalized_size,
                               cv::Mat* full_vect,
                               cv::Mat* odd_vect,
                               cv::Mat* even_odd_vect);

/**
 *  @name ComputeDct
 *  @fn     void ComputeDct(const cv::Mat& image,
                            const cv::Size normalized_size,
                            cv::Mat* full_dct,
                            cv::Mat* odd_dct,
                            cv::Mat* even_odd_dct)
 *  @brief  Computes the DCT and returns the coefficients in zigzag order
 *  @ingroup utils
 *  @param[in]  image              Input image
 *  @param[in]  normalized_size    Size to normalize image to
 *  @param[out] dct_image          Full DCT output
 *  @param[out] full_dct           Full DCT output in zigzag order (with DC)
 *  @param[out] odd_dct            Odd DCT output in zigzag order (no DC)
 *  @param[out] even_odd_dct       Even and odd DCT output in zigzag order (no DC)
 */
void LTS5_EXPORTS ComputeDct(const cv::Mat& image,
                             const cv::Size normalized_size,
                             cv::Mat* dct_image,
                             cv::Mat* full_dct,
                             cv::Mat* odd_dct,
                             cv::Mat* even_odd_dct);
} // namespace LTS5

#endif //__LTS5_DCT_HPP__
