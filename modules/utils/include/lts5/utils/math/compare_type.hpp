/**
 *  @file   compare_type.hpp
 *  @brief  Union to compare float/double type
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   20/06/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __COMPARE_TYPE__
#define __COMPARE_TYPE__

#include <stdint.h>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   CompareType
 * @brief   Union to compare float/double type
 * @author  Christophe Ecabert
 * @date    20/06/16
 * @see https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
 * @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS CompareType {
 public:
  /**
   * @name  CompareType
   * @fn  CompareType()
   * @brief Constructor
   */
  CompareType() : value_(0) {}

  /**
   * @name  CompareType
   * @fn  explicit CompareType(const T value)
   * @brief Constructor
   * @param[in] value Number value
   */
  explicit CompareType(const T value) : value_(value) {}

  /**
   * @name IsNegative
   * @fn  bool IsNegative() const
   * @brief Check if number is negative
   * @return  True if negative, false otherwise
   */
  bool IsNegative() const {
    return bits_ < 0;
  }

  union {
    /** Type value */
    T value_;
    /** integer value */
    int32_t bits_;
  };

  /**
   * How many ULP's (Units in the Last Place) we want to tolerate when
   * comparing two numbers.  The larger the value, the more error we
   * allow.  A 0 value means that two numbers must be exactly the same
   * to be considered equal.
   *
   * The maximum error of a single floating-point operation is 0.5
   * units in the last place.  On Intel CPU's, all floating-point
   * calculations are done with 80-bit precision, while double has 64
   * bits.  Therefore, 4 should be enough for ordinary use.
   */
  static const int32_t max_ulps_ = 4;

  /**
   * @name operator==
   * @fn  bool operator==(const CompareType<T>& rhs)
   * @brief Equality operator
   * @param[in] rhs  Element to compare with
   * @return  True if equal, false otherwise
   */
  bool operator==(const CompareType<T>& rhs) {
    return this->value_ == rhs.value_;
  }

  /**
   * @name  operator==
   * @fn  bool operator==(const CompareType<T>& rhs) const
   * @brief Equality operator
   * @param[in] rhs  Element to compare with
   * @return  True if equal, false otherwise
   */
  bool operator==(const CompareType<T>& rhs) const {
    return this->value_ == rhs.value_;
  }

  /**
   * @name operator!=
   * @fn  bool operator!=(const CompareType<T>& rhs)
   * @brief Inequality operator
   * @param[in] rhs  Element to compare with
   * @return  False if equal, true otherwise
   */
  bool operator!=(const CompareType<T>& rhs) {
    return this->value_ != rhs.value_;
  }

  /**
   * @name operator!=
   * @fn  bool operator!=(const CompareType<T>& rhs)
   * @brief Inequality operator
   * @param[in] rhs  Element to compare with
   * @return  False if equal, true otherwise
   */
  bool operator!=(const CompareType<T>& rhs) const {
    return this->value_ != rhs.value_;
  }
};

/**
 * @class   CompareType<float>
 * @brief   Union to compare float/double type
 * @author  Christophe Ecabert
 * @date    20/06/16
 * @see https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
 */
template<>
class LTS5_EXPORTS CompareType<float> {
public:
  /**
   * @name  CompareType
   * @fn  CompareType()
   * @brief Constructor
   */
  CompareType() : value_(0) {}

  /**
   * @name  CompareType
   * @fn  explicit CompareType(const float value)
   * @brief Constructor
   * @param[in] value Number value
   */
  explicit CompareType(const float value) : value_(value) {}

  /**
   * @name IsNegative
   * @fn  bool IsNegative() const
   * @brief Check if number is negative
   * @return  True if negative, false otherwise
   */
  bool IsNegative() const {
    return bits_ < 0;
  }

  union {
    /** Type value */
    float value_;
    /** integer value */
    int32_t bits_;
  };

  /**
   * How many ULP's (Units in the Last Place) we want to tolerate when
   * comparing two numbers.  The larger the value, the more error we
   * allow.  A 0 value means that two numbers must be exactly the same
   * to be considered equal.
   *
   * The maximum error of a single floating-point operation is 0.5
   * units in the last place.  On Intel CPU's, all floating-point
   * calculations are done with 80-bit precision, while double has 64
   * bits.  Therefore, 4 should be enough for ordinary use.
   */
  static const int32_t max_ulps_ = 4;

  /**
   * @name operator==
   * @fn  bool operator==(const CompareType<float>& rhs)
   * @brief Equality operator
   * @param[in] rhs  Element to compare with
   * @return  True if equal, false otherwise
   */
  bool operator==(const CompareType<float>& rhs) {
    // Different signs means they do not match.
    if (this->IsNegative() != rhs.IsNegative()) {
      // Check for equality to make sure +0==-0
      return this->value_ == rhs.value_;
    }
    // Find the difference in ULPs.
    return std::abs(this->bits_ - rhs.bits_) <= max_ulps_;
  }

  /**
   * @name  operator==
   * @fn  bool operator==(const CompareType<float>& rhs) const
   * @brief Equality operator
   * @param[in] rhs  Element to compare with
   * @return  True if equal, false otherwise
   */
  bool operator==(const CompareType<float>& rhs) const {
    // Different signs means they do not match.
    if (this->IsNegative() != rhs.IsNegative()) {
      // Check for equality to make sure +0==-0
      return this->value_ == rhs.value_;
    }
    // Find the difference in ULPs.
    return std::abs(this->bits_ - rhs.bits_) <= max_ulps_;
  }

  /**
   * @name operator!=
   * @fn  bool operator!=(const CompareType<float>& rhs)
   * @brief Inequality operator
   * @param[in] rhs  Element to compare with
   * @return  False if equal, true otherwise
   */
  bool operator!=(const CompareType<float>& rhs) {
    // Different signs means they do not match.
    if (this->IsNegative() != rhs.IsNegative()) {
      // Check for equality to make sure +0==-0
      return this->value_ != rhs.value_;
    }
    // Find the difference in ULPs.
    return std::abs(this->bits_ - rhs.bits_) > max_ulps_;
  }

  /**
   * @name operator!=
   * @fn  bool operator!=(const CompareType<float>& rhs)
   * @brief Inequality operator
   * @param[in] rhs  Element to compare with
   * @return  False if equal, true otherwise
   */
  bool operator!=(const CompareType<float>& rhs) const {
    // Different signs means they do not match.
    if (this->IsNegative() != rhs.IsNegative()) {
      // Check for equality to make sure +0==-0
      return this->value_ != rhs.value_;
    }
    // Find the difference in ULPs.
    return std::abs(this->bits_ - rhs.bits_) > max_ulps_;
  }
};

/**
 * @class   CompareType<double>
 * @brief   Union to compare float/double type
 * @author  Christophe Ecabert
 * @date    20/06/16
 * @see https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
 */
template<>
class LTS5_EXPORTS CompareType<double> {
public:
  /**
   * @name  CompareType
   * @fn  CompareType()
   * @brief Constructor
   */
  CompareType() : value_(0) {}

  /**
   * @name  CompareType
   * @fn  explicit CompareType(const double value)
   * @brief Constructor
   * @param[in] value Number value
   */
  explicit CompareType(const double value) : value_(value) {}

  /**
   * @name IsNegative
   * @fn  bool IsNegative() const
   * @brief Check if number is negative
   * @return  True if negative, false otherwise
   */
  bool IsNegative() const {
    return bits_ < 0;
  }

  union {
    /** Type value */
    double value_;
    /** integer value */
    int64_t bits_;
  };

  /**
   * How many ULP's (Units in the Last Place) we want to tolerate when
   * comparing two numbers.  The larger the value, the more error we
   * allow.  A 0 value means that two numbers must be exactly the same
   * to be considered equal.
   *
   * The maximum error of a single floating-point operation is 0.5
   * units in the last place.  On Intel CPU's, all floating-point
   * calculations are done with 80-bit precision, while double has 64
   * bits.  Therefore, 4 should be enough for ordinary use.
   */
  static const int64_t max_ulps_ = 4;

  /**
   * @name operator==
   * @fn  bool operator==(const CompareType<double>& rhs)
   * @brief Equality operator
   * @param[in] rhs  Element to compare with
   * @return  True if equal, false otherwise
   */
  bool operator==(const CompareType<double>& rhs) {
    // Different signs means they do not match.
    if (this->IsNegative() != rhs.IsNegative()) {
      // Check for equality to make sure +0==-0
      return this->value_ == rhs.value_;
    }
    // Find the difference in ULPs.
    return std::abs(this->bits_ - rhs.bits_) <= max_ulps_;
  }

  /**
   * @name  operator==
   * @fn  bool operator==(const CompareType<double>& rhs) const
   * @brief Equality operator
   * @param[in] rhs  Element to compare with
   * @return  True if equal, false otherwise
   */
  bool operator==(const CompareType<double>& rhs) const {
    // Different signs means they do not match.
    if (this->IsNegative() != rhs.IsNegative()) {
      // Check for equality to make sure +0==-0
      return this->value_ == rhs.value_;
    }
    // Find the difference in ULPs.
    return std::abs(this->bits_ - rhs.bits_) <= max_ulps_;
  }

  /**
   * @name operator!=
   * @fn  bool operator!=(const CompareType<double>& rhs)
   * @brief Inequality operator
   * @param[in] rhs  Element to compare with
   * @return  False if equal, true otherwise
   */
  bool operator!=(const CompareType<double>& rhs) {
    // Different signs means they do not match.
    if (this->IsNegative() != rhs.IsNegative()) {
      // Check for equality to make sure +0==-0
      return this->value_ != rhs.value_;
    }
    // Find the difference in ULPs.
    return std::abs(this->bits_ - rhs.bits_) > max_ulps_;
  }

  /**
   * @name operator!=
   * @fn  bool operator!=(const CompareType<double>& rhs)
   * @brief Inequality operator
   * @param[in] rhs  Element to compare with
   * @return  False if equal, true otherwise
   */
  bool operator!=(const CompareType<double>& rhs) const {
    // Different signs means they do not match.
    if (this->IsNegative() != rhs.IsNegative()) {
      // Check for equality to make sure +0==-0
      return this->value_ != rhs.value_;
    }
    // Find the difference in ULPs.
    return std::abs(this->bits_ - rhs.bits_) > max_ulps_;
  }
};
}  // namepsace LTS5
#endif //__COMPARE_TYPE__
