/**
 *  @file   "lts5/utils/math/matrix.hpp"
 *  @brief  3x3 matrix representation
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   01/12/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_MATRIX__
#define __LTS5_MATRIX__

#include <cstring>
#include <algorithm>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/math/compare_type.hpp"
#include "lts5/utils/math/constant.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  Matrix1x1
 *  @brief  1x1 Matrix implementation
 *  @author Christophe Ecabert
 *  @date   01/12/15
 *  @ingroup utils
 */
template<class T>
class LTS5_EXPORTS Matrix1x1 {
 public :

#pragma mark -
#pragma mark Initialization

  /**
   *  @name Matrix1x1
   *  @fn Matrix1x1()
   *  @brief  Constructor
   */
  Matrix1x1() : m_(T(0.0)) {}

  /**
   *  @name Matrix1x1
   *  @fn explicit Matrix1x1(const T* data)
   *  @brief  Construcotr
   *  @param[in]  data  Value to use to fill Matrix
   */
  explicit Matrix1x1(const T* data) {
    m_ = data[0];
  }

  /**
   *  @name operator=
   *  @fn Matrix1x1& operator=(const Matrix1x1& rhs)
   *  @brief  Assignment operator
   *  @param[in]  rhs  Object to assign from
   *  @return Newly assigned object
   */
  Matrix1x1& operator=(const Matrix1x1& rhs) {
    if (this != &rhs) {
      m_ = rhs.m_;
    }
    return *this;
  }

  /**
   *  @name ~Matrix1x1
   *  @fn ~Matrix1x1()
   *  @brief  Destructor
   */
  ~Matrix1x1() = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Identity
   * @fn    void Identity()
   * @brief Set matrix to identity
   */
  Matrix1x1& Identity() {
    m_ = T(1.0);
    return *this;
  }

  /**
   * @name  Zeros
   * @fn    void Zeros()
   * @brief Set matrix to zeros
   */
  Matrix1x1& Zeros() {
    m_ = T(0.0);
    return *this;
  }

  /**
   * @name  Ones
   * @fn    void Ones()
   * @brief Set matrix to ones
   */
  Matrix1x1& Ones() {
    m_ = T(1.0);
    return *this;
  }

  /**
   *  @name Data
   *  @fn const T* Data() const
   *  @brief  Provide access to raw pointer data
   *  @return Pointer to raw data
   */
  const T* Data() const {
    return &m_;
  }

  /**
   *  @name Data
   *  @fn T* Data()
   *  @brief  Provide access to raw pointer data
   *  @return Pointer to raw data
   */
  T* Data() {
    return &m_;
  }

  /**
   * @name  Norm
   * @fn    T Norm() const
   * @brief Compute the Frobenius norm of the matrix
   * @return    Norm
   */
  T Norm() const {
    return std::abs(m_);
  }

  /**
   *  @name Determinent
   *  @fn T Determinent() const
   *  @brief  Compute matrix determinent
   *  @return Determinent of the matrix
   */
  T Determinent() const {
    return m_;
  }

  /**
   *  @name Trace
   *  @fn T Trace() const
   *  @brief  Compute trace of the matrix
   *  @return Trace of the matrix
   */
  T Trace() const {
    return m_;
  }

  /**
   *  @name Transpose
   *  @fn Matrix1x1<T> Transpose() const
   *  @brief  Compute the matrix transpose
   *  @return Transposed matrix
   */
  Matrix1x1<T> Transpose() const {
    Matrix1x1<T> M;
    M[0] = m_;
    return M;
  }

  /**
   *  @name Inverse
   *  @fn Matrix1x1<T> Inverse() const
   *  @brief  Compute the inverse of this matrix
   *  @return Inverse matrix
   */
  Matrix1x1<T> Inverse() const {
    Matrix1x1<T> iM;
    T det = this->Determinent();
    if (det != 0.0) {
      det = 1.0 / det;
      iM[0] = m_ * det;
    }
    return iM;
  }

  /**
   *  @name SetData
   *  @fn void SetData(const T* data)
   *  @brief  Set all data at once
   *  @param[in]  data  Data to set
   */
  void SetData(const T* data) {
    m_ = data[0];
  }

  /**
   *  @name operator<<
   *  @fn friend std::ostream& operator<<(std::ostream& out, const Matrix1x1<T>& rhs)
   *  @brief  Print matrix into a given steram
   *  @param[in]  out   Output stream
   *  @param[in]  rhs   Matrix to print
   *  @return Output stream
   */
  friend std::ostream& operator<<(std::ostream& out, const Matrix1x1<T>& rhs) {
    out << "[" << rhs[0] << "]";
    return out;
  }

#pragma mark -
#pragma mark Operator

  /**
   *  @name operator()(
   *  @fn T& operator()(const int row, const int col)
   *  @brief  Row/Col matrix accessor
   *  @param[in]  row Index of the row
   *  @param[in]  col Index of the column
   *  @return Reference to the selected location
   */
  T& operator()(const int row, const int col) {
    assert(row == 0 && col == 0 && "Index must be 0 for Matrix1x1");
    return m_;
  }

  /**
   *  @name operator()(
   *  @fn const T& operator()(const int row, const int col) const
   *  @brief  Row/Col matrix accessor
   *  @param[in]  row Index of the row
   *  @param[in]  col Index of the column
   *  @return Reference to the selected location
   */
  const T& operator()(const int row, const int col) const {
    assert(row == 0 && col == 0 && "Index must be 0 for Matrix1x1");
    return m_;
  }

  /**
   *  @name operator*
   *  @fn Matrix1x1<T> operator*(const Matrix1x1<T>& rhs) const
   *  @brief  Multiply by a #LTS5::Matrix1x1
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix product
   */
  Matrix1x1<T> operator*(const Matrix1x1<T>& rhs) const {
    Matrix1x1<T> M;
    M[0] = m_ * rhs[0];
    return M;
  }

  /**
   *  @name operator*
   *  @fn cv::Mat operator*(const cv::Mat& rhs) const
   *  @brief  Multiply by an opencv matrix
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix product
   */
  cv::Mat operator*(const cv::Mat& rhs) const {
    assert(rhs.rows == 1 && rhs.type() == cv::DataType<T>::type);
    cv::Mat M(1, rhs.cols, cv::DataType<T>::type);
    for (int c = 0; c < rhs.cols; ++c) {
      M.at<T>(0, c) = m_ * rhs.at<T>(0, c);
    }
    return M;
  }

  /**
   *  @name operator*
   *  @fn Vector1<T> operator*(const Vector1<T>& rhs) const
   *  @brief  Multiply by a #LTS5::Vector1
   *  @param[in]  rhs Right hand sign vector
   *  @return Matrix-Vector product
   */
  Vector1<T> operator*(const Vector1<T>& rhs) const {
    Vector2<T> v;
    v.x_ = m_ * rhs.x_;
    return v;
  }

  /**
   *  @name operator*
   *  @fn Matrix1x1<T> operator*(const T s) const
   *  @brief  Multiply by a scalar
   *  @param[in]  s Scalar to multiply by
   *  @return Scaled Matrix
   */
  Matrix1x1<T> operator*(const T s) const {
    Matrix1x1<T> M;
    M[0] = m_ * s;
    return M;
  }

  /**
   * @name  ElementWiseProduct
   * @fn    Matrix1x1<T> ElementWiseProduct(const Matrix1x1<T>& rhs) const
   * @brief Multiply two matrices element-wise
   * @param[in] rhs
   * @return Element-wise product
   */
  Matrix1x1<T> ElementWiseProduct(const Matrix1x1<T>& rhs) const {
    Matrix1x1<T> m;
    m[0] = m_ * rhs[0];
    return m;
  }

  /**
   *  @name operator+
   *  @fn Matrix1x1<T> operator+(const Matrix1x1<T>& rhs) const
   *  @brief  Add another #LTS5::Matrix1x1
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix addition
   */
  Matrix1x1<T> operator+(const Matrix1x1<T>& rhs) const {
    Matrix1x1<T> M;
    M[0] = m_ + rhs[0];
    return M;
  }

  /**
   * @name  operator+=
   * @fn    Matrix1x1& operator+=(const Matrix1x1<T>& rhs)
   * @brief Inplace matrix addition
   * @param[in] rhs Matrix to add
   * @return  Matrix
   */
  Matrix1x1<T>& operator+=(const Matrix1x1<T>& rhs) {
    m_ += rhs[0];
    return *this;
  }

  /**
   *  @name operator-
   *  @fn Matrix1x1<T> operator-(const Matrix1x1<T>& rhs) const
   *  @brief  Subtract another #LTS5::Matrix1x1
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix subtraction
   */
  Matrix1x1<T> operator-(const Matrix1x1<T>& rhs) const {
    Matrix1x1<T> M;
    M[0] = m_ - rhs[0];
    return M;
  }

  /**
   *  @name operator=
   *  @fn Matrix1x1<T>& operator=(const T s)
   *  @brief  Assignment operator
   *  @param[in]  s Scalar to assign to this matrix
   *  @return
   */
  Matrix1x1<T>& operator=(const T s) {
    m_ = s;
    return *this;
  }

  /**
   *  @name operator[]
   *  @fn T& operator[](const int idx)
   *  @brief  Operator to linearly access element stored in row major flavor
   *  @param[in]  idx Index to reach
   *  @return Reference to the selected element
   */
  T& operator[](const int idx) {
    return m_;
  }

  /**
   *  @name operator[]
   *  @fn const T& operator[](const int idx) const
   *  @brief  Operator to linearly access element stored in row major flavor
   *  @param[in]  idx Index to reach
   *  @return Reference to the selected element
   */
  const T& operator[](const int idx) const {
    return m_;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix1x1<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator==(const Matrix1x1<T>& rhs) {
    return CompareType<T>(m_) == CompareType<T>(rhs.m_);
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix1x1<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator==(const Matrix1x1<T>& rhs) const {
    return CompareType<T>(m_) == CompareType<T>(rhs.m_);
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix1x1<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator!=(const Matrix1x1<T>& rhs) {
    return CompareType<T>(m_) != CompareType<T>(rhs.m_);
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Matrix1x1<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator!=(const Matrix1x1<T>& rhs) const {
    return CompareType<T>(m_) != CompareType<T>(rhs.m_);
  }

#pragma mark -
#pragma mark Private
 private :
  /** Data */
  T m_;
};

/**
 *  @class  Matrix2x2
 *  @brief  2x2 Matrix implementation
 *  @author Christophe Ecabert
 *  @date   01/12/15
 *  @ingroup utils
 */
template<class T>
class LTS5_EXPORTS Matrix2x2 {
 public :

#pragma mark -
#pragma mark Initialization

  /**
   *  @name Matrix2x2
   *  @fn Matrix2x2()
   *  @brief  Constructor
   */
  Matrix2x2() : m_{T(0.0)} {}

  /**
   *  @name Matrix2x2
   *  @fn explicit Matrix2x2(const T* data)
   *  @brief  Construcotr
   *  @param[in]  data  Value to use to fill Matrix
   */
  explicit Matrix2x2(const T* data) {
    memcpy(reinterpret_cast<void*>(&m_[0]),
           reinterpret_cast<const void*>(data),
           4 * sizeof(T));
  }

  /**
   *  @name operator=
   *  @fn Matrix2x2& operator=(const Matrix2x2& rhs)
   *  @brief  Assignment operator
   *  @param[in]  rhs  Object to assign from
   *  @return Newly assigned object
   */
  Matrix2x2& operator=(const Matrix2x2& rhs) {
    if (this != &rhs) {
      memcpy(reinterpret_cast<void*>(&m_[0]),
             reinterpret_cast<const void*>(&rhs.m_[0]),
             4 * sizeof(T));
    }
    return *this;
  }

  /**
   *  @name ~Matrix2x2
   *  @fn ~Matrix2x2()
   *  @brief  Destructor
   */
  ~Matrix2x2() = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Identity
   * @fn    void Identity()
   * @brief Set matrix to identity
   */
  Matrix2x2& Identity() {
    m_[0] = m_[3] = T(1.0);
    m_[1] = m_[2] = T(0.0);
    return *this;
  }

  /**
   * @name  Zeros
   * @fn    void Zeros()
   * @brief Set matrix to zeros
   */
  Matrix2x2& Zeros() {
    for (int i = 0; i < 4; ++i) { m_[i] = T(0.0); }
    return *this;
  }

  /**
   * @name  Ones
   * @fn    void Ones()
   * @brief Set matrix to ones
   */
  Matrix2x2& Ones() {
    for (int i = 0; i < 4; ++i) { m_[i] = T(1.0); }
    return *this;
  }

  /**
   *  @name Data
   *  @fn const T* Data() const
   *  @brief  Provide access to raw pointer data
   *  @return Pointer to raw data
   */
  const T* Data() const {
    return &(m_[0]);
  }

  /**
   *  @name Data
   *  @fn T* Data()
   *  @brief  Provide access to raw pointer data
   *  @return Pointer to raw data
   */
  T* Data() {
    return &(m_[0]);
  }

  /**
   * @name  Norm
   * @fn    T Norm() const
   * @brief Compute the Frobenius norm of the matrix
   * @return    Norm
   */
  T Norm() const {
    T nrm = T(0.0);
#pragma unroll
    for (size_t i = 0; i < 4; ++i) {
      nrm += m_[i] * m_[i];
    }
    return std::sqrt(nrm);
  }

  /**
   *  @name Determinent
   *  @fn T Determinent() const
   *  @brief  Compute matrix determinent
   *  @return Determinent of the matrix
   */
  T Determinent() const {
    T det = (m_[0] * m_[3]) - (m_[2] * m_[1]);
    return det;
  }

  /**
   *  @name Trace
   *  @fn T Trace() const
   *  @brief  Compute trace of the matrix
   *  @return Trace of the matrix
   */
  T Trace() const {
    return m_[0] + m_[3];
  }

  /**
   *  @name Transpose
   *  @fn Matrix2x2<T> Transpose() const
   *  @brief  Compute the matrix transpose
   *  @return Transposed matrix
   */
  Matrix2x2<T> Transpose() const {
    Matrix2x2<T> M;
    M[0] = m_[0]; M[1] = m_[2];
    M[2] = m_[1]; M[3] = m_[3];
    return M;
  }

  /**
   *  @name Inverse
   *  @fn Matrix2x2<T> Inverse() const
   *  @brief  Compute the inverse of this matrix
   *  @return Inverse matrix
   */
  Matrix2x2<T> Inverse() const {
    Matrix2x2<T> iM;
    T det = this->Determinent();
    if (det != 0.0) {
      det = 1.0 / det;
      iM[0] = m_[3] * det;
      iM[1] = -m_[1] * det;
      iM[2] = -m_[2] * det;
      iM[3] = m_[0] * det;
    }
    return iM;
  }

  /**
   *  @name SetData
   *  @fn void SetData(const T* data)
   *  @brief  Set all data at once
   *  @param[in]  data  Data to set
   */
  void SetData(const T* data) {
    std::copy(data, data + 4, &m_[0]);
  }

  /**
   *  @name operator<<
   *  @fn friend std::ostream& operator<<(std::ostream& out, const Matrix2x2<T>& rhs)
   *  @brief  Print matrix into a given steram
   *  @param[in]  out   Output stream
   *  @param[in]  rhs   Matrix to print
   *  @return Output stream
   */
  friend std::ostream& operator<<(std::ostream& out, const Matrix2x2<T>& rhs) {
    out << "[" << rhs[0] << ", " << rhs[1] << std::endl;
    out << rhs[2] << ", " << rhs[3] << "]";
    return out;
  }

#pragma mark -
#pragma mark Operator

  /**
   *  @name operator()(
   *  @fn T& operator()(const int row, const int col)
   *  @brief  Row/Col matrix accessor
   *  @param[in]  row Index of the row
   *  @param[in]  col Index of the column
   *  @return Reference to the selected location
   */
  T& operator()(const int row, const int col) {
    return m_[row * 2 + col];
  }

  /**
   *  @name operator()(
   *  @fn const T& operator()(const int row, const int col) const
   *  @brief  Row/Col matrix accessor
   *  @param[in]  row Index of the row
   *  @param[in]  col Index of the column
   *  @return Reference to the selected location
   */
  const T& operator()(const int row, const int col) const {
    return m_[row * 2 + col];
  }

  /**
   *  @name operator*
   *  @fn Matrix2x2<T> operator*(const Matrix2x2<T>& rhs) const
   *  @brief  Multiply by a #LTS5::Matrix2x2
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix product
   */
  Matrix2x2<T> operator*(const Matrix2x2<T>& rhs) const {
    Matrix2x2<T> M;
    const T* m1 = rhs.Data();
    M[0] = m_[0] * m1[0] + m_[1] * m1[2];
    M[1] = m_[0] * m1[1] + m_[1] * m1[3];
    M[2] = m_[2] * m1[0] + m_[3] * m1[2];
    M[3] = m_[2] * m1[1] + m_[3] * m1[3];
    return M;
  }

  /**
   *  @name operator*
   *  @fn cv::Mat operator*(const cv::Mat& rhs) const
   *  @brief  Multiply by an opencv matrix
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix product
   */
  cv::Mat operator*(const cv::Mat& rhs) const {
    assert(rhs.rows == 2 && rhs.type() == cv::DataType<T>::type);
    cv::Mat M(2, rhs.cols, cv::DataType<T>::type);
    for (int c = 0; c < rhs.cols; ++c) {
      const T x = rhs.at<T>(0, c);
      const T y = rhs.at<T>(1, c);
      M.at<T>(0, c) = m_[0] * x + m_[1] * y;
      M.at<T>(1, c) = m_[2] * x + m_[3] * y;
    }
    return M;
  }

  /**
   *  @name operator*
   *  @fn Vector2<T> operator*(const Vector2<T>& rhs) const
   *  @brief  Multiply by a #LTS5::Vector2
   *  @param[in]  rhs Right hand sign vector
   *  @return Matrix-Vector product
   */
  Vector2<T> operator*(const Vector2<T>& rhs) const {
    Vector2<T> v;
    v.x_ = m_[0] * rhs.x_ + m_[1] * rhs.y_;
    v.y_ = m_[2] * rhs.x_ + m_[3] * rhs.y_;
    return v;
  }

  /**
   *  @name operator*
   *  @fn Matrix2x2<T> operator*(const T s) const
   *  @brief  Multiply by a scalar
   *  @param[in]  s Scalar to multiply by
   *  @return Scaled Matrix
   */
  Matrix2x2<T> operator*(const T s) const {
    Matrix2x2<T> M;
    M[0] = m_[0] * s;
    M[1] = m_[1] * s;
    M[2] = m_[2] * s;
    M[3] = m_[3] * s;
    return M;
  }

  /**
   * @name  ElementWiseProduct
   * @fn    Matrix2x2<T> ElementWiseProduct(const Matrix2x2<T>& rhs) const
   * @brief Multiply two matrices element-wise
   * @param[in] rhs
   * @return Element-wise product
   */
  Matrix2x2<T> ElementWiseProduct(const Matrix2x2<T>& rhs) const {
    Matrix2x2<T> m;
    for (int i = 0; i < 4; ++i) {
      m[i] = m_[i] * rhs[i];
    }
    return m;
  }

  /**
   *  @name operator+
   *  @fn Matrix2x2<T> operator+(const Matrix2x2<T>& rhs) const
   *  @brief  Add another #LTS5::Matrix2x2
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix addition
   */
  Matrix2x2<T> operator+(const Matrix2x2<T>& rhs) const {
    Matrix2x2<T> M;
    M[0] = m_[0] + rhs[0];
    M[1] = m_[1] + rhs[1];
    M[2] = m_[2] + rhs[2];
    M[3] = m_[3] + rhs[3];
    return M;
  }

  /**
   * @name  operator+=
   * @fn    Matrix2x2& operator+=(const Matrix2x2<T>& rhs)
   * @brief Inplace matrix addition
   * @param[in] rhs Matrix to add
   * @return  Matrix
   */
  Matrix2x2<T>& operator+=(const Matrix2x2<T>& rhs) {
    for (int i = 0; i < 4; ++i) {
      this->m_[i] += rhs[i];
    }
    return *this;
  }

  /**
   *  @name operator-
   *  @fn Matrix2x2<T> operator-(const Matrix2x2<T>& rhs) const
   *  @brief  Subtract another #LTS5::Matrix2x2
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix subtraction
   */
  Matrix2x2<T> operator-(const Matrix2x2<T>& rhs) const {
    Matrix2x2<T> M;
    M[0] = m_[0] - rhs[0];
    M[1] = m_[1] - rhs[1];
    M[2] = m_[2] - rhs[2];
    M[3] = m_[3] - rhs[3];
    return M;
  }

  /**
   *  @name operator=
   *  @fn Matrix2x2<T>& operator=(const T s)
   *  @brief  Assignment operator
   *  @param[in]  s Scalar to assign to this matrix
   *  @return
   */
  Matrix2x2<T>& operator=(const T s) {
    for (int i = 0 ; i < 4 ; ++i) {
      m_[i] = s;
    }
    return *this;
  }

  /**
   *  @name operator[]
   *  @fn T& operator[](const int idx)
   *  @brief  Operator to linearly access element stored in row major flavor
   *  @param[in]  idx Index to reach
   *  @return Reference to the selected element
   */
  T& operator[](const int idx) {
    return m_[idx];
  }

  /**
   *  @name operator[]
   *  @fn const T& operator[](const int idx) const
   *  @brief  Operator to linearly access element stored in row major flavor
   *  @param[in]  idx Index to reach
   *  @return Reference to the selected element
   */
  const T& operator[](const int idx) const {
    return m_[idx];
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix2x2<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator==(const Matrix2x2<T>& rhs) {
    for (int i = 0; i < 4; ++i) {
      if (CompareType<T>(m_[i]) != CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix2x2<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator==(const Matrix2x2<T>& rhs) const {
    for (int i = 0; i < 4; ++i) {
      if (CompareType<T>(m_[i]) != CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix2x2<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator!=(const Matrix2x2<T>& rhs) {
    for (int i = 0; i < 4; ++i) {
      if (CompareType<T>(m_[i]) == CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Matrix2x2<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator!=(const Matrix2x2<T>& rhs) const {
    for (int i = 0; i < 4; ++i) {
      if (CompareType<T>(m_[i]) == CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }

#pragma mark -
#pragma mark Private
 private :
  /** Data */
  T m_[4];
};

/**
 *  @class  Matrix3x3
 *  @brief  3x3 Matrix implementation
 *  @author Christophe Ecabert
 *  @date   01/12/15
 *  @ingroup utils
 */
template<class T>
class LTS5_EXPORTS Matrix3x3 {
 public :

#pragma mark -
#pragma mark Initialization

  /**
   *  @name Matrix3x3
   *  @fn Matrix3x3()
   *  @brief  Constructor
   */
  Matrix3x3() : m_{T(0.0)} {}

  /**
   * @name  Matrix3x3
   * @fn    Matrix3x3(const T& gamma, const T& theta, const T& phi)
   * @brief Construct a rotation matrix from three euler angles. The matrix as
   *        the for of \f$ R = R_{z}(\gamma) R_{y}(\theta) R_{x}(\phi) \f$
   * @param[in] gamma   Rotation around `z` axis
   * @param[in] theta   Rotation around `y` axis
   * @param[in] phi     Rotation around `x` axis
   * @see https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
   */
  Matrix3x3(const T& gamma, const T& theta, const T& phi) {
    const T cg = std::cos(gamma);
    const T sg = std::sin(gamma);
    const T ct = std::cos(theta);
    const T st = std::sin(theta);
    const T cp = std::cos(phi);
    const T sp = std::sin(phi);
    // Fill rotation matrix
    m_[0] = ct * cg;
    m_[1] = (sp * st * cg) - (cp * sg);
    m_[2] = (sp * sg) + (cp * st * cg);
    m_[3] = ct * sg;
    m_[4] = (sp * st * sg) + (cp * cg);
    m_[5] = (cp * st * sg) - (sp * cg);
    m_[6] = -st;
    m_[7] = sp * ct;
    m_[8] = cp * ct;
  }

  /**
   *  @name Matrix3x3
   *  @fn explicit Matrix3x3(const T* data)
   *  @brief  Construcotr
   *  @param[in]  data  Value to use to fill Matrix
   */
  explicit Matrix3x3(const T* data) {
    memcpy(reinterpret_cast<void*>(&m_[0]),
           reinterpret_cast<const void*>(data),
           9 * sizeof(T));
  }

  /**
   *  @name operator=
   *  @fn Matrix3x3& operator=(const Matrix3x3& rhs)
   *  @brief  Assignment operator
   *  @param[in]  rhs  Object to assign from
   *  @return Newly assigned object
   */
  Matrix3x3& operator=(const Matrix3x3& rhs) {
    if (this != &rhs) {
      memcpy(reinterpret_cast<void*>(&m_[0]),
             reinterpret_cast<const void*>(&rhs.m_[0]),
             9 * sizeof(T));
    }
    return *this;
  }

  /**
   *  @name ~Matrix3x3
   *  @fn ~Matrix3x3()
   *  @brief  Destructor
   */
  ~Matrix3x3() = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Identity
   * @fn    void Identity()
   * @brief Set matrix to identity
   */
  Matrix3x3& Identity() {
    m_[0] = m_[4] = m_[8] = T(1.0);
    m_[1] = m_[2] = m_[3] = m_[5] = m_[6] = m_[7];
    return *this;
  }

  /**
   * @name  Zeros
   * @fn    void Zeros()
   * @brief Set matrix to zeros
   */
  Matrix3x3& Zeros() {
    for (int i = 0; i < 9; ++i) { m_[i] = T(0.0); }
    return *this;
  }

  /**
   * @name  Ones
   * @fn    void Ones()
   * @brief Set matrix to ones
   */
  Matrix3x3& Ones() {
    for (int i = 0; i < 9; ++i) { m_[i] = T(1.0); }
    return *this;
  }

  /**
   *  @name Data
   *  @fn const T* Data() const
   *  @brief  Provide access to raw pointer data
   *  @return Pointer to raw data
   */
  const T* Data() const {
    return &(m_[0]);
  }

  /**
   *  @name Data
   *  @fn T* Data()
   *  @brief  Provide access to raw pointer data
   *  @return Pointer to raw data
   */
  T* Data() {
    return &(m_[0]);
  }

  /**
   * @name  Norm
   * @fn    T Norm() const
   * @brief Compute the Frobenius norm of the matrix
   * @return    Norm
   */
  T Norm() const {
    T nrm = T(0.0);
    #pragma unroll
    for (size_t i = 0; i < 9; ++i) {
      nrm += m_[i] * m_[i];
    }
    return std::sqrt(nrm);
  }

  /**
   *  @name Determinent
   *  @fn T Determinent() const
   *  @brief  Compute matrix determinent
   *  @return Determinent of the matrix
   */
  T Determinent() const {
    T det = m_[0] * (m_[4] * m_[8] - m_[7] * m_[5]);
    det -=  m_[1] * (m_[3] * m_[8] - m_[6] * m_[5]);
    det += m_[2] * (m_[3] * m_[7] - m_[6] * m_[4]);
    return det;
  }

  /**
   *  @name Trace
   *  @fn T Trace() const
   *  @brief  Compute trace of the matrix
   *  @return Trace of the matrix
   */
  T Trace() const {
    return m_[0] + m_[4] + m_[8];
  }

  /**
   *  @name Transpose
   *  @fn Matrix3x3<T> Transpose() const
   *  @brief  Compute the matrix transpose
   *  @return Transposed matrix
   */
  Matrix3x3<T> Transpose() const {
    Matrix3x3<T> M;
    M[0] = m_[0]; M[1] = m_[3]; M[2] = m_[6];
    M[3] = m_[1]; M[4] = m_[4]; M[5] = m_[7];
    M[6] = m_[2]; M[7] = m_[5]; M[8] = m_[8];
    return M;
  }

  /**
   *  @name Inverse
   *  @fn Matrix3x3<T> Inverse() const
   *  @brief  Compute the inverse of this matrix
   *  @return Inverse matrix
   */
  Matrix3x3<T> Inverse() const {
    Matrix3x3<T> iM;
    T det = this->Determinent();
    if (det != 0.0) {
      det = 1.0 / det;
      iM[0] = (m_[4] * m_[8] - m_[7] * m_[5]) * det;
      iM[1] = (m_[2] * m_[7] - m_[8] * m_[1]) * det;
      iM[2] = (m_[1] * m_[5] - m_[4] * m_[2]) * det;
      iM[3] = (m_[5] * m_[6] - m_[8] * m_[3]) * det;
      iM[4] = (m_[0] * m_[8] - m_[6] * m_[2]) * det;
      iM[5] = (m_[2] * m_[3] - m_[5] * m_[0]) * det;
      iM[6] = (m_[3] * m_[7] - m_[6] * m_[4]) * det;
      iM[7] = (m_[1] * m_[6] - m_[7] * m_[0]) * det;
      iM[8] = (m_[0] * m_[4] - m_[3] * m_[1]) * det;
    }
    return iM;
  }

  /**
   * @name  ToEuler
   * @fn    void ToEuler(T* gamma, T* theta, T* phi) const
   * @brief Convert the rotation matrix to euler's angles
   * @param[out] gamma  Rotation around `z` axis
   * @param[out] theta  Rotation around `y` axis
   * @param[out] phi    Rotation around `x` axis
   */
  void ToEuler(T* gamma, T* theta, T* phi) const {
    const T ct = std::sqrt((m_[0] * m_[0]) + (m_[3] * m_[3]));
    *gamma = std::atan2(m_[3], m_[0]);
    *theta = std::atan2(-m_[6], ct);
    *phi = std::atan2(m_[7], m_[8]);
  }

  /**
   *  @name SetData
   *  @fn void SetData(const T* data)
   *  @brief  Set all data at once
   *  @param[in]  data  Data to set
   */
  void SetData(const T* data) {
    std::copy(data, data + 9, &m_[0]);
  }

  /**
   *  @name operator<<
   *  @fn friend std::ostream& operator<<(std::ostream& out, const Matrix3x3<T>& rhs)
   *  @brief  Print matrix into a given steram
   *  @param[in]  out   Output stream
   *  @param[in]  rhs   Matrix to print
   *  @return Output stream
   */
  friend std::ostream& operator<<(std::ostream& out, const Matrix3x3<T>& rhs) {
    out << "[" << rhs[0] << ", " << rhs[1] << ", " << rhs[2] << std::endl;
    out << rhs[3] << ", " << rhs[4] << ", " << rhs[5] << std::endl;
    out << rhs[6] << ", " << rhs[7] << ", " << rhs[8] << "]";
    return out;
  }

#pragma mark -
#pragma mark Operator

  /**
   *  @name operator()(
   *  @fn T& operator()(const int row, const int col)
   *  @brief  Row/Col matrix accessor
   *  @param[in]  row Index of the row
   *  @param[in]  col Index of the column
   *  @return Reference to the selected location
   */
  T& operator()(const int row, const int col) {
    return m_[row * 3 + col];
  }

  /**
   *  @name operator()(
   *  @fn const T& operator()(const int row, const int col) const
   *  @brief  Row/Col matrix accessor
   *  @param[in]  row Index of the row
   *  @param[in]  col Index of the column
   *  @return Reference to the selected location
   */
  const T& operator()(const int row, const int col) const {
    return m_[row * 3 + col];
  }

  /**
   *  @name operator*
   *  @fn Matrix3x3<T> operator*(const Matrix3x3<T>& rhs) const
   *  @brief  Multiply by a #LTS5::Matrix3x3
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix product
   */
  Matrix3x3<T> operator*(const Matrix3x3<T>& rhs) const {
    Matrix3x3<T> M;
    const T* m1 = rhs.Data();
    M[0] = m_[0] * m1[0] + m_[1] * m1[3] + m_[2] * m1[6];
    M[1] = m_[0] * m1[1] + m_[1] * m1[4] + m_[2] * m1[7];
    M[2] = m_[0] * m1[2] + m_[1] * m1[5] + m_[2] * m1[8];
    M[3] = m_[3] * m1[0] + m_[4] * m1[3] + m_[5] * m1[6];
    M[4] = m_[3] * m1[1] + m_[4] * m1[4] + m_[5] * m1[7];
    M[5] = m_[3] * m1[2] + m_[4] * m1[5] + m_[5] * m1[8];
    M[6] = m_[6] * m1[0] + m_[7] * m1[3] + m_[8] * m1[6];
    M[7] = m_[6] * m1[1] + m_[7] * m1[4] + m_[8] * m1[7];
    M[8] = m_[6] * m1[2] + m_[7] * m1[5] + m_[8] * m1[8];
    return M;
  }

  /**
   *  @name operator*
   *  @fn cv::Mat operator*(const cv::Mat& rhs) const
   *  @brief  Multiply by an opencv matrix
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix product
   */
  cv::Mat operator*(const cv::Mat& rhs) const {
    assert(rhs.rows == 3 && rhs.type() == cv::DataType<T>::type);
    cv::Mat M(3, rhs.cols, cv::DataType<T>::type);
    for (int c = 0; c < rhs.cols; ++c) {
      const T x = rhs.at<T>(0, c);
      const T y = rhs.at<T>(1, c);
      const T z = rhs.at<T>(2, c);
      M.at<T>(0, c) = m_[0] * x + m_[1] * y + m_[2] * z;
      M.at<T>(1, c) = m_[3] * x + m_[4] * y + m_[5] * z;
      M.at<T>(2, c) = m_[6] * x + m_[7] * y + m_[8] * z;
    }
    return M;
  }

  /**
   *  @name operator*
   *  @fn Vector3<T> operator*(const Vector3<T>& rhs) const
   *  @brief  Multiply by a #LTS5::Vector3
   *  @param[in]  rhs Right hand sign vector
   *  @return Matrix-Vector product
   */
  Vector3<T> operator*(const Vector3<T>& rhs) const {
    Vector3<T> v;
    v.x_ = m_[0] * rhs.x_ + m_[1] * rhs.y_ + m_[2] * rhs.z_;
    v.y_ = m_[3] * rhs.x_ + m_[4] * rhs.y_ + m_[5] * rhs.z_;
    v.z_ = m_[6] * rhs.x_ + m_[7] * rhs.y_ + m_[8] * rhs.z_;
    return v;
  }

  /**
   *  @name operator*
   *  @fn Matrix3x3<T> operator*(const T s) const
   *  @brief  Multiply by a scalar
   *  @param[in]  s Scalar to multiply by
   *  @return Scaled Matrix
   */
  Matrix3x3<T> operator*(const T s) const {
    Matrix3x3<T> M;
    M[0] = m_[0] * s;
    M[1] = m_[1] * s;
    M[2] = m_[2] * s;
    M[3] = m_[3] * s;
    M[4] = m_[4] * s;
    M[5] = m_[5] * s;
    M[6] = m_[6] * s;
    M[7] = m_[7] * s;
    M[8] = m_[8] * s;
    return M;
  }

  /**
   * @name  ElementWiseProduct
   * @fn    Matrix3x3<T> ElementWiseProduct(const Matrix3x3<T>& rhs) const
   * @brief Multiply two matrices element-wise
   * @param[in] rhs Right hand side
   * @return Element-wise product
   */
  Matrix3x3<T> ElementWiseProduct(const Matrix3x3<T>& rhs) const {
    Matrix3x3<T> m;
    for (int i = 0; i < 9; ++i) {
      m[i] = m_[i] * rhs[i];
    }
    return m;
  }

  /**
   *  @name operator+
   *  @fn Matrix3x3<T> operator+(const Matrix3x3<T>& rhs) const
   *  @brief  Add another #LTS5::Matrix3x3
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix addition
   */
  Matrix3x3<T> operator+(const Matrix3x3<T>& rhs) const {
    Matrix3x3<T> M;
    M[0] = m_[0] + rhs[0];
    M[1] = m_[1] + rhs[1];
    M[2] = m_[2] + rhs[2];
    M[3] = m_[3] + rhs[3];
    M[4] = m_[4] + rhs[4];
    M[5] = m_[5] + rhs[5];
    M[6] = m_[6] + rhs[6];
    M[7] = m_[7] + rhs[7];
    M[8] = m_[8] + rhs[8];
    return M;
  }

  /**
   * @name  operator+=
   * @fn    Matrix3x3& operator+=(const Matrix3x3<T>& rhs)
   * @brief Inplace matrix addition
   * @param[in] rhs Matrix to add
   * @return  Matrix
   */
  Matrix3x3<T>& operator+=(const Matrix3x3<T>& rhs) {
    for (int i = 0; i < 9; ++i) {
      this->m_[i] += rhs[i];
    }
    return *this;
  }

  /**
   *  @name operator-
   *  @fn Matrix3x3<T> operator-(const Matrix3x3<T>& rhs) const
   *  @brief  Subtract another #LTS5::Matrix3x3
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix subtraction
   */
  Matrix3x3<T> operator-(const Matrix3x3<T>& rhs) const {
    Matrix3x3<T> M;
    M[0] = m_[0] - rhs[0];
    M[1] = m_[1] - rhs[1];
    M[2] = m_[2] - rhs[2];
    M[3] = m_[3] - rhs[3];
    M[4] = m_[4] - rhs[4];
    M[5] = m_[5] - rhs[5];
    M[6] = m_[6] - rhs[6];
    M[7] = m_[7] - rhs[7];
    M[8] = m_[8] - rhs[8];
    return M;
  }

  /**
   *  @name operator=
   *  @fn Matrix3x3<T>& operator=(const T s)
   *  @brief  Assignment operator
   *  @param[in]  s Scalar to assign to this matrix
   *  @return
   */
  Matrix3x3<T>& operator=(const T s) {
    for (int i = 0 ; i < 9 ; ++i) {
      m_[i] = s;
    }
    return *this;
  }

  /**
   *  @name operator[]
   *  @fn T& operator[](const int idx)
   *  @brief  Operator to linearly access element stored in row major flavor
   *  @param[in]  idx Index to reach
   *  @return Reference to the selected element
   */
  T& operator[](const int idx) {
    return m_[idx];
  }

  /**
   *  @name operator[]
   *  @fn const T& operator[](const int idx) const
   *  @brief  Operator to linearly access element stored in row major flavor
   *  @param[in]  idx Index to reach
   *  @return Reference to the selected element
   */
  const T& operator[](const int idx) const {
    return m_[idx];
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix3x3<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator==(const Matrix3x3<T>& rhs) {
    for (int i = 0; i < 9; ++i) {
      if (CompareType<T>(m_[i]) != CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix3x3<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator==(const Matrix3x3<T>& rhs) const {
    for (int i = 0; i < 9; ++i) {
      if (CompareType<T>(m_[i]) != CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix3x3<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator!=(const Matrix3x3<T>& rhs) {
    for (int i = 0; i < 9; ++i) {
      if (CompareType<T>(m_[i]) == CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Matrix3x3<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator!=(const Matrix3x3<T>& rhs) const {
    for (int i = 0; i < 9; ++i) {
      if (CompareType<T>(m_[i]) == CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }

#pragma mark -
#pragma mark Private
 private :
  /** Data */
  T m_[9];
};

/**
 *  @class  Matrix4x4
 *  @brief  4x4 matrix class
 *  @author Christophe Ecabert
 *  @date   31/07/16
 *  @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS Matrix4x4 {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   *  @name Matrix4x4
   *  @fn Matrix4x4()
   *  @brief  Construcotr
   */
  Matrix4x4() : m_{T(0)} {}

  /**
   *  @name Matrix4x4
   *  @fn Matrix4x4(const T* data)
   *  @brief  Construcotr
   *  @param[in]  data  Value to use to fill Matrix
   */
  explicit Matrix4x4(const T* data) {
    memcpy(reinterpret_cast<void*>(&m_[0]),
           reinterpret_cast<const void*>(data),
           16 * sizeof(T));
  }

  /**
   * @name  Matrix4x4
   * @fn    Matrix4x4(const T& gamma, const T& theta, const T& phi)
   * @brief Construct a rotation matrix from three euler angles. The matrix as
   *        the for of \f$ R = R_{z}(\gamma) R_{y}(\theta) R_{x}(\phi) \f$
   * @param[in] gamma   Rotation around `z` axis
   * @param[in] theta   Rotation around `y` axis
   * @param[in] phi     Rotation around `x` axis
   * @see https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
   */
  Matrix4x4(const T& gamma, const T& theta, const T& phi) {
    const T cg = std::cos(gamma);
    const T sg = std::sin(gamma);
    const T ct = std::cos(theta);
    const T st = std::sin(theta);
    const T cp = std::cos(phi);
    const T sp = std::sin(phi);
    // Fill rotation matrix
    m_[0] = ct * cg;
    m_[1] = (sp * st * cg) - (cp * sg);
    m_[2] = (sp * sg) + (cp * st * cg);
    m_[3] = T(0.0);
    m_[4] = ct * sg;
    m_[5] = (sp * st * sg) + (cp * cg);
    m_[6] = (cp * st * sg) - (sp * cg);
    m_[7] = T(0.0);
    m_[8] = -st;
    m_[9] = sp * ct;
    m_[10] = cp * ct;
    m_[11] = T(0.0);
    m_[12] = T(0.0);
    m_[13] = T(0.0);
    m_[14] = T(0.0);
    m_[15] = T(1.0);
  }

  /**
   *  @name Matrix4x4
   *  @fn Matrix4x4(const Matrix4x4& other)
   *  @brief  Copy Construcotr
   *  @param[in]  other  Object to copy from
   */
  Matrix4x4(const Matrix4x4& other) {
    memcpy(reinterpret_cast<void*>(&m_[0]),
           reinterpret_cast<const void*>(&other.m_[0]),
           16 * sizeof(T));
  }

  /**
   * @name Ortho
   * @fn    static Matrix4x4 Ortho(const T& left,
                                   const T& right,
                                   const T& bottom,
                                   const T& top)
   * @brief Constructor orthogonal projection matrix
   * @param[in] left    Left screen position
   * @param[in] right   Right screen position
   * @param[in] bottom  Bottom screen position
   * @param[in] top     Top screen position
   * @return    Orthographic projection matrix
   */
  static Matrix4x4 Ortho(const T& left,
                         const T& right,
                         const T& bottom,
                         const T& top) {
    Matrix4x4 m = Matrix4x4().Identity();
    m(0, 0) = T(2.0) / ( right - left);
    m(1, 1) = T(2.0) / ( top - bottom);
    m(2, 2) = -T(1.0);
    m(0, 3) = - (right + left) / (right - left);
    m(1, 3) = - (top + bottom) / (top - bottom);
    return m;
  }

  /**
   * @name  Perspective
   * @fn    static Matrix4x4 Perspective(const T& fov, const T& aspect,
   *                                     const T& near, const T& far)
   * @brief Constructor perspective projection matrix
   * @param[in] fov         Field of view in degree
   * @param[in] aspect      Window aspect ratio
   * @param[in] near        Near plane
   * @param[in] far         Far plane
   * @return Perspective projection matrix
   */
  static Matrix4x4 Perspective(const T& fov,
                               const T& aspect,
                               const T& near,
                               const T& far) {
    assert(std::abs(aspect - std::numeric_limits<T>::epsilon()) > T(0.0));
    Matrix4x4 m;
    const T half_fov = LTS5::Constants<T>::Deg2Rad * fov * T(0.5);
    const T tanHalfFovy = std::tan(half_fov);
    m(0, 0) = T(1.0) / (aspect * tanHalfFovy);
    m(1, 1) = T(1.0) / (tanHalfFovy);
    m(2, 2) = - (far + near) / (far - near);
    m(3, 2) = T(-1.0);
    m(2, 3) = - (T(2.0) * far * near) / (far - near);
    return m;
  }

  /**
   *  @name operator=
   *  @fn Matrix4x4& operator=(const Matrix4x4& rhs)
   *  @brief  Assignment operator
   *  @param[in]  rhs  Object to assign from
   *  @return Newly assigned object
   */
  Matrix4x4& operator=(const Matrix4x4& rhs) {
    if (this != &rhs) {
      memcpy(reinterpret_cast<void*>(&m_[0]),
             reinterpret_cast<const void*>(&rhs.m_[0]),
             16 * sizeof(T));
    }
    return *this;
  }

  /**
   *  @name ~Matrix4x4
   *  @fn ~Matrix4x4()
   *  @brief  Destrucotr
   */
  ~Matrix4x4() = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Identity
   * @fn    void Identity()
   * @brief Set matrix to identity
   */
  Matrix4x4& Identity() {
    m_[0] = m_[5] = m_[10] = m_[15] = T(1.0);
    m_[1] = m_[2] = m_[3] = m_[4] = m_[6] = m_[7] = m_[8] = m_[9] = T(0.0);
    m_[11] = m_[12] = m_[13] = m_[14] = T(0.0);
    return *this;
  }

  /**
   * @name  Zeros
   * @fn    void Zeros()
   * @brief Set matrix to zeros
   */
  Matrix4x4& Zeros() {
    for (int i = 0; i < 16; ++i) { m_[i] = T(0.0); }
    return *this;
  }

  /**
   * @name  Ones
   * @fn    void Ones()
   * @brief Set matrix to ones
   */
  Matrix4x4& Ones() {
    for (int i = 0; i < 16; ++i) { m_[i] = T(1.0); }
    return *this;
  }

  /**
   *  @name Data
   *  @fn const T* Data() const
   *  @brief  Provide access to raw pointer data
   *  @return Pointer to raw data
   */
  const T* Data() const {
    return &(m_[0]);
  }

  /**
   *  @name Data
   *  @fn T* Data()
   *  @brief  Provide access to raw pointer data
   *  @return Pointer to raw data
   */
  T* Data() {
    return &(m_[0]);
  }

  /**
   * @name  Norm
   * @fn    T Norm() const
   * @brief Compute the Frobenius norm of the matrix
   * @return    Norm
   */
  T Norm() const {
    T nrm = T(0.0);
    #pragma unroll
    for (size_t i = 0; i < 16; ++i) {
      nrm += m_[i] * m_[i];
    }
    return std::sqrt(nrm);
  }

  /**
   *  @name Determinent
   *  @fn T Determinent() const
   *  brief Compute matrix determinent
   *  @return Matrix determinent
   */
  T Determinent() const {
    T det = ((m_[0]*m_[5]*m_[10]*m_[15]) +
             (m_[0]*m_[6]*m_[11]*m_[13]) +
             (m_[0]*m_[7]*m_[9]*m_[14]) +
             (m_[1]*m_[4]*m_[11]*m_[14]) +
             (m_[1]*m_[6]*m_[8]*m_[15]) +
             (m_[1]*m_[7]*m_[10]*m_[12]) +
             (m_[2]*m_[4]*m_[9]*m_[15]) +
             (m_[2]*m_[5]*m_[11]*m_[12]) +
             (m_[2]*m_[7]*m_[8]*m_[13]) +
             (m_[3]*m_[4]*m_[10]*m_[13]) +
             (m_[3]*m_[5]*m_[8]*m_[14]) +
             (m_[3]*m_[6]*m_[9]*m_[12]) -
             (m_[0]*m_[5]*m_[11]*m_[14]) -
             (m_[0]*m_[6]*m_[9]*m_[15]) -
             (m_[0]*m_[7]*m_[10]*m_[13]) -
             (m_[1]*m_[4]*m_[10]*m_[15]) -
             (m_[1]*m_[6]*m_[11]*m_[12]) -
             (m_[1]*m_[7]*m_[8]*m_[14]) -
             (m_[2]*m_[4]*m_[11]*m_[13]) -
             (m_[2]*m_[5]*m_[8]*m_[15]) -
             (m_[2]*m_[7]*m_[9]*m_[12]) -
             (m_[3]*m_[4]*m_[9]*m_[14]) -
             (m_[3]*m_[5]*m_[10]*m_[12]) -
             (m_[3]*m_[6]*m_[8]*m_[13]));
    return det;
  }

  /**
   *  @name Trace
   *  @fn T Trace() const
   *  brief Compute matrix trace
   *  @return Matrix trace
   */
  T Trace() const {
    return m_[0] + m_[5] + m_[10] + m_[15];
  }

  /**
   *  @name Transpose
   *  @fn Matrix4x4& Transpose() const
   *  @brief  Compute the matrix transpose
   *  @return Transposed matrix
   */
  Matrix4x4 Transpose() const {
    Matrix4x4 M;
    M[0] = m_[0];
    M[1] = m_[4];
    M[2] = m_[8];
    M[3] = m_[12];
    M[4] = m_[1];
    M[5] = m_[5];
    M[6] = m_[9];
    M[7] = m_[13];
    M[8] = m_[2];
    M[9] = m_[6];
    M[10] = m_[10];
    M[11] = m_[14];
    M[12] = m_[3];
    M[13] = m_[7];
    M[14] = m_[11];
    M[15] = m_[15];
    return M;
  }

  /**
   *  @name Inverse
   *  @fn Matrix4x4 Inverse() const
   *  @brief  Compute the inverse of this matrix
   *  @return Inverse matrix
   */
  Matrix4x4 Inverse() const {
    Matrix4x4 iM;
    const T det = this->Determinent();
    if (det != T(0)) {
      const T idet = T(1) / det;
      iM[0] = (m_[5]*m_[10]*m_[15] + m_[6]*m_[11]*m_[13] + m_[7]*m_[9]*m_[14] -
               m_[5]*m_[11]*m_[14] - m_[6]*m_[9]*m_[15] - m_[7]*m_[10]*m_[13]) * idet;
      iM[4] = (m_[4]*m_[11]*m_[14] + m_[6]*m_[8]*m_[15] + m_[7]*m_[10]*m_[12] -
               m_[4]*m_[10]*m_[15] - m_[6]*m_[11]*m_[12] - m_[7]*m_[8]*m_[14]) * idet;
      iM[8] = (m_[4]*m_[9]*m_[15] + m_[5]*m_[11]*m_[12] + m_[7]*m_[8]*m_[13] -
               m_[4]*m_[11]*m_[13] - m_[5]*m_[8]*m_[15] - m_[7]*m_[9]*m_[12]) * idet;
      iM[12] = (m_[4]*m_[10]*m_[13] + m_[5]*m_[8]*m_[14] + m_[6]*m_[9]*m_[12] -
                m_[4]*m_[9]*m_[14] - m_[5]*m_[10]*m_[12] - m_[6]*m_[8]*m_[13]) * idet;
      iM[1] = (m_[1]*m_[11]*m_[14] + m_[2]*m_[9]*m_[15] + m_[3]*m_[10]*m_[13] -
               m_[1]*m_[10]*m_[15] - m_[2]*m_[11]*m_[13] - m_[3]*m_[9]*m_[14]) * idet;
      iM[5] = (m_[0]*m_[10]*m_[15] + m_[2]*m_[11]*m_[12] + m_[3]*m_[8]*m_[14] -
               m_[0]*m_[11]*m_[14] - m_[2]*m_[8]*m_[15] - m_[3]*m_[10]*m_[12]) * idet;
      iM[9] = (m_[0]*m_[11]*m_[13] + m_[1]*m_[8]*m_[15] + m_[3]*m_[9]*m_[12] -
               m_[0]*m_[9]*m_[15] - m_[1]*m_[11]*m_[12] - m_[3]*m_[8]*m_[13]) * idet;
      iM[13] = (m_[0]*m_[9]*m_[14] + m_[1]*m_[10]*m_[12] + m_[2]*m_[8]*m_[13] -
                m_[0]*m_[10]*m_[13] - m_[1]*m_[8]*m_[14] - m_[2]*m_[9]*m_[12]) * idet;
      iM[2] = (m_[1]*m_[6]*m_[15] + m_[2]*m_[7]*m_[13] + m_[3]*m_[5]*m_[14] -
               m_[1]*m_[7]*m_[14] - m_[2]*m_[5]*m_[15] - m_[3]*m_[6]*m_[13]) * idet;
      iM[6] = (m_[0]*m_[7]*m_[14] + m_[2]*m_[4]*m_[15] + m_[3]*m_[6]*m_[12] -
               m_[0]*m_[6]*m_[15] - m_[2]*m_[7]*m_[12] - m_[3]*m_[4]*m_[14]) * idet;
      iM[10] = (m_[0]*m_[5]*m_[15] + m_[1]*m_[7]*m_[12] + m_[3]*m_[4]*m_[13] -
                m_[0]*m_[7]*m_[13] - m_[1]*m_[4]*m_[15] - m_[3]*m_[5]*m_[12]) * idet;
      iM[14] = (m_[0]*m_[6]*m_[13] + m_[1]*m_[4]*m_[14] + m_[2]*m_[5]*m_[12] -
                m_[0]*m_[5]*m_[14] - m_[1]*m_[6]*m_[12] - m_[2]*m_[4]*m_[13]) * idet;
      iM[3] = (m_[1]*m_[7]*m_[10] + m_[2]*m_[5]*m_[11] + m_[3]*m_[6]*m_[9] -
               m_[1]*m_[6]*m_[11] - m_[2]*m_[7]*m_[9] - m_[3]*m_[5]*m_[10]) * idet;
      iM[7] = (m_[0]*m_[6]*m_[11] + m_[2]*m_[7]*m_[8] + m_[3]*m_[4]*m_[10] -
               m_[0]*m_[7]*m_[10] - m_[2]*m_[4]*m_[11] - m_[3]*m_[6]*m_[8]) * idet;
      iM[11] = (m_[0]*m_[7]*m_[9] + m_[1]*m_[4]*m_[11] + m_[3]*m_[5]*m_[8] -
                m_[0]*m_[5]*m_[11] - m_[1]*m_[7]*m_[8] - m_[3]*m_[4]*m_[9]) * idet;
      iM[15] = (m_[0]*m_[5]*m_[10] + m_[1]*m_[6]*m_[8] + m_[2]*m_[4]*m_[9] -
                m_[0]*m_[6]*m_[9] - m_[1]*m_[4]*m_[10] - m_[2]*m_[5]*m_[8]) * idet;
    }
    return iM;
  }

  /**
   * @name  ToEuler
   * @fn    void ToEuler(T* gamma, T* theta, T* phi) const
   * @brief Convert the rotation matrix to euler's angles
   * @param[out] gamma  Rotation around `z` axis
   * @param[out] theta  Rotation around `y` axis
   * @param[out] phi    Rotation around `x` axis
   */
  void ToEuler(T* gamma, T* theta, T* phi) const {
    assert(m_[12] == 0.0 && m_[13] == 0.0 && m_[14] == 0.0 && m_[15] == 1.0);
    const T ct = std::sqrt((m_[0] * m_[0]) + (m_[4] * m_[4]));
    *gamma = std::atan2(m_[4], m_[0]);
    *theta = std::atan2(-m_[8], ct);
    *phi = std::atan2(m_[9], m_[10]);
  }

  /**
   *  @name SetData
   *  @fn void SetData(const T* data)
   *  @brief  Set all data at once
   *  @param[in]  data  Data to set
   */
  void SetData(const T* data) {
    std::copy(data, data + 16, &m_[0]);
  }

#pragma mark -
#pragma mark Operator

  /**
   *  @name operator()(
   *  @fn T& operator()(const int row, const int col)
   *  @brief  Row/Col matrix accessor
   *  @param[in]  row Index of the row
   *  @param[in]  col Index of the column
   *  @return Reference to the selected location
   */
  T& operator()(const int row, const int col) {
    return m_[row * 4 + col];
  }

  /**
   *  @name operator()(
   *  @fn const T& operator()(const int row, const int col) const
   *  @brief  Row/Col matrix accessor
   *  @param[in]  row Index of the row
   *  @param[in]  col Index of the column
   *  @return Reference to the selected location
   */
  const T& operator()(const int row, const int col) const {
    return m_[row * 4 + col];
  }

  /**
   *  @name operator*
   *  @fn Matrix4x4 operator*(const Matrix4x4& rhs) const
   *  @brief  Matrix multiplication
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix product
   */
  Matrix4x4 operator*(const Matrix4x4& rhs) const {
    Matrix4x4 M;
    M[0] = m_[0] * rhs[0] + m_[1] * rhs[4] + m_[2] * rhs[8] + m_[3] * rhs[12];
    M[1] = m_[0] * rhs[1] + m_[1] * rhs[5] + m_[2] * rhs[9] + m_[3] * rhs[13];
    M[2] = m_[0] * rhs[2] + m_[1] * rhs[6] + m_[2] * rhs[10] + m_[3] * rhs[14];
    M[3] = m_[0] * rhs[3] + m_[1] * rhs[7] + m_[2] * rhs[11] + m_[3] * rhs[15];
    M[4] = m_[4] * rhs[0] + m_[5] * rhs[4] + m_[6] * rhs[8] + m_[7] * rhs[12];
    M[5] = m_[4] * rhs[1] + m_[5] * rhs[5] + m_[6] * rhs[9] + m_[7] * rhs[13];
    M[6] = m_[4] * rhs[2] + m_[5] * rhs[6] + m_[6] * rhs[10] + m_[7] * rhs[14];
    M[7] = m_[4] * rhs[3] + m_[5] * rhs[7] + m_[6] * rhs[11] + m_[7] * rhs[15];
    M[8] = m_[8] * rhs[0] + m_[9] * rhs[4] + m_[10] * rhs[8] + m_[11] * rhs[12];
    M[9] = m_[8] * rhs[1] + m_[9] * rhs[5] + m_[10] * rhs[9] + m_[11] * rhs[13];
    M[10] = m_[8] * rhs[2] + m_[9] * rhs[6] + m_[10] * rhs[10] + m_[11] * rhs[14];
    M[11] = m_[8] * rhs[3] + m_[9] * rhs[7] + m_[10] * rhs[11] + m_[11] * rhs[15];
    M[12] = m_[12] * rhs[0] + m_[13] * rhs[4] + m_[14] * rhs[8] + m_[15] * rhs[12];
    M[13] = m_[12] * rhs[1] + m_[13] * rhs[5] + m_[14] * rhs[9] + m_[15] * rhs[13];
    M[14] = m_[12] * rhs[2] + m_[13] * rhs[6] + m_[14] * rhs[10] + m_[15] * rhs[14];
    M[15] = m_[12] * rhs[3] + m_[13] * rhs[7] + m_[14] * rhs[11] + m_[15] * rhs[15];
    return M;
  }

  /**
   *  @name operator*
   *  @fn Vector4<T> operator*(const Vector4<T>& rhs) const
   *  @brief  Multiply by a Vector3
   *  @param[in]  rhs Right hand sign vector
   *  @return Matrix-Vector product
   */
  Vector4<T> operator*(const Vector4<T>& rhs) const {
    Vector4<T> v;
    v.x_ = m_[0] * rhs.x_ + m_[1] * rhs.y_ + m_[2] * rhs.z_ + m_[3] * rhs.w_;
    v.y_ = m_[4] * rhs.x_ + m_[5] * rhs.y_ + m_[6] * rhs.z_ + m_[7] * rhs.w_;
    v.z_ = m_[8] * rhs.x_ + m_[9] * rhs.y_ + m_[10] * rhs.z_ + m_[11] * rhs.w_;
    v.w_ = m_[12] * rhs.x_ + m_[13] * rhs.y_ + m_[14] * rhs.z_ + m_[15] * rhs.w_;
    return v;
  }

  /**
   *  @name operator*
   *  @fn Vector3<T> operator*(const Vector4<T>& rhs) const
   *  @brief  Multiply by a Vector3, assuming homogenous vector
   *            (i.e. rhs.w_ = 1)
   *  @param[in]  rhs Right hand sign vector
   *  @return Matrix-Vector product
   */
  Vector3<T> operator*(const Vector3<T>& rhs) const {
    Vector3<T> v;
    v.x_ = m_[0] * rhs.x_ + m_[1] * rhs.y_ + m_[2] * rhs.z_ + m_[3];
    v.y_ = m_[4] * rhs.x_ + m_[5] * rhs.y_ + m_[6] * rhs.z_ + m_[7];
    v.z_ = m_[8] * rhs.x_ + m_[9] * rhs.y_ + m_[10] * rhs.z_ + m_[11];
    return v;
  }

  /**
   *  @name operator*
   *  @fn Matrix4x4 operator*(const T s) const
   *  @brief  Multiply by a scalar
   *  @param[in]  s Scalar to multiply by
   *  @return Scaled Matrix
   */
  Matrix4x4<T> operator*(const T s) const {
    Matrix4x4 M;
    #pragma unroll
    for (int i = 0; i < 16; ++i) {
      M[i] = m_[i] * s;
    }
    return M;
  }

  /**
   * @name  ElementWiseProduct
   * @fn    Matrix3x3<T> ElementWiseProduct(const Matrix3x3<T>& rhs) const
   * @brief Multiply two matrices element-wise
   * @param[in] rhs Right hand side
   * @return Element-wise product
   */
  Matrix4x4<T> ElementWiseProduct(const Matrix4x4<T>& rhs) const {
    Matrix4x4<T> m;
    for (int i = 0; i < 16; ++i) {
      m[i] = m_[i] * rhs[i];
    }
    return m;
  }

  /**
   *  @name operator+
   *  @fn Matrix4x4 operator+(const Matrix4x4& rhs) const
   *  @brief  Add another Matrix4x4
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix addition
   */
  Matrix4x4 operator+(const Matrix4x4& rhs) const {
    Matrix4x4 M;
    #pragma unroll
    for (int i = 0; i < 16; ++i) {
      M[i] = m_[i] + rhs[i];
    }
    return M;
  }

  /**
   * @name  operator+=
   * @fn    Matrix4x4<T>& operator+=(const Matrix4x4<T>& rhs)
   * @brief Inplace matrix addition
   * @param[in] rhs Matrix to add
   * @return  Matrix
   */
  Matrix4x4<T>& operator+=(const Matrix4x4<T>& rhs) {
    for (int i = 0; i < 16; ++i) {
      this->m_[i] += rhs[i];
    }
    return *this;
  }

  /**
   *  @name operator-
   *  @fn Matrix4x4 operator-(const Matrix4x4& rhs) const
   *  @brief  Subtract another Matrix4x4
   *  @param[in]  rhs Right hand sign matrix
   *  @return Matrix subtraction
   */
  Matrix4x4 operator-(const Matrix4x4& rhs) const {
    Matrix4x4 M;
    #pragma unroll
    for (int i = 0; i < 16; ++i) {
      M[i] = m_[i] - rhs[i];
    }
    return M;
  }

  /**
   *  @name operator=
   *  @fn Matrix4x4& operator=(const T s)
   *  @brief  Assignment operator
   *  @param[in]  s Scalar to assign to this matrix
   *  @return
   */
  Matrix4x4& operator=(const T s) {
    #pragma unroll
    for (int i = 0 ; i < 16 ; ++i) {
      m_[i] = s;
    }
    return *this;
  }

  /**
   *  @name operator[]
   *  @fn T& operator[](const int idx)
   *  @brief  Operator to linearly access element stored in row major flavor
   *  @param[in]  idx Index to reach
   *  @return Reference to the selected element
   */
  T& operator[](const int idx) {
    return m_[idx];
  }

  /**
   *  @name operator[]
   *  @fn const T& operator[](const int idx) const
   *  @brief  Operator to linearly access element stored in row major flavor
   *  @param[in]  idx Index to reach
   *  @return Reference to the selected element
   */
  const T& operator[](const int idx) const {
    return m_[idx];
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix4x4<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator==(const Matrix4x4<T>& rhs) {
    for (int i = 0; i < 16; ++i) {
      if (CompareType<T>(m_[i]) != CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix4x4<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator==(const Matrix4x4<T>& rhs) const {
    for (int i = 0; i < 16; ++i) {
      if (CompareType<T>(m_[i]) != CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Matrix4x4<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator!=(const Matrix4x4<T>& rhs) {
    for (int i = 0; i < 16; ++i) {
      if (CompareType<T>(m_[i]) == CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Matrix4x4<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if matrix are same
   */
  bool operator!=(const Matrix4x4<T>& rhs) const {
    for (int i = 0; i < 16; ++i) {
      if (CompareType<T>(m_[i]) == CompareType<T>(rhs.m_[i])) {
        return false;
      }
    }
    return true;
  }


  /**
   *  @name operator<<
   *  @fn friend std::ostream& operator<<(std::ostream& out, const Matrix4x4& m)
   *  @brief  Print matrix into a given steram
   *  @param[in]  out   Output stream
   *  @param[in]  m     Matrix to print
   *  @return Output stream
   */
  friend std::ostream& operator<<(std::ostream& out, const Matrix4x4& m) {
    out << "[";
    out << m[0] << ", " << m[1] << ", " << m[2] << ", " << m[3] << std::endl;
    out << m[4] << ", " << m[5] << ", " << m[6] << ", " << m[7] << std::endl;
    out << m[8] << ", " << m[9] << ", " << m[10] << ", " << m[11] << std::endl;
    out << m[12] << ", " << m[13] << ", " << m[14] << ", " << m[15] << "]";
    return out;
  }

#pragma mark -
#pragma mark Private

 private:
  /** Data array - RownMajor layout */
  T m_[16];
};

/**
 * @struct  Matrix
 * @brief   Matrix type selector at compilation time
 * @author  Christophe Ecabert
 * @date    01/12/2020
 * @ingroup utils
 */
template<int NDim, typename T>
struct Matrix {};

template<typename T>
struct Matrix<1, T> {
  using Type = Matrix1x1<T>;
};

template<typename T>
struct Matrix<2, T> {
  using Type = Matrix2x2<T>;
};

template<typename T>
struct Matrix<3, T> {
  using Type = Matrix3x3<T>;
};

template<typename T>
struct Matrix<4, T> {
  using Type = Matrix4x4<T>;
};

}  // namespace LTS5
#endif /* __LTS5_MATRIX__ */
