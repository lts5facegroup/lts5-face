/**
 *  @file   sparse_matrix.hpp
 *  @brief  Sparse matrix data structure.
 *          Based on OpenNL library
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   12/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_sparse_matrix_hpp__
#define __LTS5_sparse_matrix_hpp__

#include <vector>
#include <assert.h>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/dense_vector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  SparseMatrix
 *  @brief  Sparse matrix data structure
 *          Based on OpenNL Library
 *  @author Christophe Ecabert
 *  @date   12/10/15
 *  @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS SparseMatrix {

 public :

#pragma mark -
#pragma mark Types definition

  /**
   *  @struct Coeff
   *  @brief  Data structure for one node holding position and value
   */
  struct LTS5_EXPORTS Coeff {
    /**
     *  @name Coeff
     *  @fn Coeff(void)
     *  @brief  Default constructor
     */
    Coeff(void){}

    /**
     *  @name Coeff
     *  @fn Coeff(int index, T value)
     *  @brief  Constructor
     *  @param[in]  index   Index of the coefficient inside structure
     *  @param[in]  value   Value of the coefficient
     */
    Coeff(int index, T value) : index_(index), value_(value) {}

    /** Coefficient's index inside data structure */
    int index_;
    /** Coefficient's value */
    T value_;
  };

  /**
   *  @class Row
   *  @brief  A row/column of a SparseMatrix. The row/column is compressed,
   *          and stored in the form of a list of (value,index) pair.
   *          Based on OpenNL Library
   *  @author Christophe Ecabert
   *  @date   12/10/15
   */
  class LTS5_EXPORTS Row : public std::vector<Coeff> {
   public :
    /** Definition of SuperClass name */
    typedef typename std::vector<Coeff> superclass;

    /**
     *  @name AddCoeff
     *  @fn void AddCoeff(int index, CoeffType value)
     *  @brief  Add a coefficient inside structure :
     *          a_{index} = a_{index} + value
     *  @param[in]  index Position where to add value
     *  @param[in]  value Value to add
     */
    void AddCoeff(int index, T value);

    /**
     *  @name SetCoeff
     *  @fn void SetCoeff(int index, CoeffType value, bool is_new)
     *  @brief  Set a coefficient inside structure :
     *          a_{index} = value
     *  @param[in]  index   Position where to add value
     *  @param[in]  value   Value to set
     *  @param[in]  is_new  Set to true if new element in matrix
     */
    void SetCoeff(int index, T value, bool is_new);

    /**
     *  @name GetCoeff
     *  @fn T GetCoeff(int index) const
     *  @brief  Provide coefficient's value at position index, default value = 0
     *          return a_{index} (0 by default)
     *  @param[in]  index Position
     *  @return     Value at index
     */
    T GetCoeff(int index) const;
  };

#pragma mark -
#pragma mark Initialization

  /**
   *  @name SparseMatrix
   *  @fn explicit SparseMatrix(int dimension)
   *  @brief  Constructor
   *  @param[in]  dimension Matrix dimension (square)
   */
  explicit SparseMatrix(int dimension);

  /**
   *  @name SparseMatrix
   *  @fn SparseMatrix(int rows, int columns)
   *  @brief  Constructor for rectangle matrix (Warning support only square
   *          matrix at the moment)
   *  @param[in]  rows    Number of rows in the matrix
   *  @param[in]  columns Number of columns in the matrix
   */
  SparseMatrix(int rows, int columns);

  /**
   *  @name ~SparseMatrix
   *  @fn ~SparseMatrix(void)
   *  @brief  Destructor
   */
  ~SparseMatrix(void);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name dimension
   *  @fn int dimension(void) const
   *  @brief  Provide matrix dimension
   *  @return Size of the matrix
   */
  int dimension(void) const {
    return dimension_;
  }

  /**
   *  @name row_dimension
   *  @fn int row_dimension(void) const
   *  @brief  Provide number of rows
   *  @return Rows in the matrix
   */
  int row_dimension(void) const {
    return dimension();
  }

  /**
   *  @name column_dimension
   *  @fn int column_dimension(void) const
   *  @brief  Provide number of columns
   *  @return Columns in the matrix
   */
  int column_dimension(void) const {
    return dimension();
  }

  /**
   *  @name row
   *  @fn const Row& row(int index) const
   *  @brief  Provide access to a given row of the matrix
   *  @param[in]  index Row of interest
   *  @return     Reference to the selected row
   */
  const Row& row(int index) const {
    assert(index < dimension_);
    return row_[index];
  }

  /**
   *  @name row
   *  @fn Row& row(int index)
   *  @brief  Provide access to a given row of the matrix
   *  @param[in]  index Row of interest
   *  @return     Reference to the selected row
   */
  Row& row(int index) {
    assert(index < dimension_);
    return row_[index];
  }

  /**
   *  @name get_coeff
   *  @fn CoeffType get_coeff(int row, int col) const
   *  @brief  Provide the value at position (row, col)
   *  @param[in]  row   Selected row
   *  @param[in]  col   Selected column
   *  @return Coefficient's value
   */
  T get_coeff(int row, int col) const {
    assert(row < dimension_ && col < dimension_);
    return this->row(row).GetCoeff(col);
  }

  /**
   *  @name add_coeff
   *  @fn void add_coeff(int row, int col, T value)
   *  @brief  Add one coefficient into the matrix (row,col).
   *          a_row,col = a_row,col + value
   *  @param[in]  row   Row index
   *  @param[in]  col   Column index
   *  @param[in]  value Coefficient's value
   */
  void add_coeff(int row, int col, T value) {
    assert(row < dimension_ && col < dimension_);
    this->row(row).AddCoeff(col, value);
  }

  /**
   *  @name add_coeff
   *  @fn void add_coeff(int row, int col, T value, bool is_new)
   *  @brief  Add one coefficient into the matrix (row,col).
   *          a_row,col = value
   *  @param[in]  row     Row index
   *  @param[in]  col     Column index
   *  @param[in]  value   Coefficient's value
   *  @param[in]  is_new  Indicate a new value to add into matrix
   */
  void add_coeff(int row, int col, T value, bool is_new) {
    assert(row < dimension_ && col < dimension_);
    this->row(row).SetCoeff(col, value, is_new);
  }

  /**
   *  @name nnz
   *  @fn int nnz(void) const
   *  @brief  Give the number of non-zero elements inside the matrix
   *  @return Number of non-zero element
   */
  int nnz(void) const;

#pragma mark -
#pragma mark Operator

  /**
   *  @name operator*
   *  @fn DenseVector<T>& operator*(const DenseVector<T>& x)
   *  @brief  Matrix - vector multiplication y = Mx
   *  @param[in]  x Input vector
   *  @return     return product vector (e.g. y)
   */
  const DenseVector<T> operator*(const DenseVector<T>& x);

  /**
   *  @name operator*
   *  @fn DenseVector<T>& operator*(const DenseVector<T>& x) const
   *  @brief  Matrix - vector multiplication y = Mx
   *  @param[in]  x Input vector
   *  @return     return product vector (e.g. y)
   */
  const DenseVector<T> operator*(const DenseVector<T>& x) const;



#pragma mark -
#pragma mark Private
 private :
  /** Matrix dimension */
  int dimension_;
  /** Matrix rows */
  Row* row_;
};

}  // namespace LTS5

#endif /* __LTS5_sparse_matrix_hpp__ */
