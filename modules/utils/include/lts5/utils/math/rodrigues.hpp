/**
 *  @file   lts5/utils/math/rodrigues.hpp
 *  @brief  3D Rotation representation using axis-angle approach.
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   2/20/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_RODRIGUES__
#define __LTS5_RODRIGUES__

#include <limits>
#include <cmath>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/matrix.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class       Rodrigues
 * @brief       Rotation parameterisation using Rodrigues formula
 * @author      Christophe Ecabert
 * @date        20/02/19
 * @tparam T    Data type
 * @ingroup     utils
 */
template<typename T>
class LTS5_EXPORTS Rodrigues {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  Rodrigues
   * @fn    Rodrigues()
   * @brief Constructor
   */
  Rodrigues() : x_(0), y_(0), z_(0) {}

  /**
   * @name  Rodrigues
   * @fn    Rodrigues(const T& x, const T& y, const T& z)
   * @brief Constructor
   * @param[in] x   X component
   * @param[in] y   Y component
   * @param[in] z   Z component
   */
  Rodrigues(const T& x, const T& y, const T& z) : x_(x), y_(y), z_(z) {}

  /**
   * @name  Rodrigues
   * @fn    Rodrigues(const Vector3<T>& axis, const T angle)
   * @brief Constructor for a rotation of a specific \p angle around a
   *        given \p axis.
   * @param[in] axis    Rotation axis
   * @param[in] angle   Amount of rotation, in radians
   */
  Rodrigues(const Vector3<T>& axis, const T angle) {
    T s = T(1.0) / axis.Norm();
    // Construct rodrigues representation
    x_ = axis.x_ * s * angle;
    y_ = axis.y_ * s * angle;
    z_ = axis.z_ * s * angle;
  }

  /**
   * @name  Rodrigues
   * @fn    Rodrigues(const Rodrigues& other) = default
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  Rodrigues(const Rodrigues& other) = default;

  /**
   * @name  operator=
   * @fn    Rodrigues& operator=(const Rodrigues& rhs) = default
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  Rodrigues& operator=(const Rodrigues& rhs) = default;

  /**
   *  @name ~Rodrigues
   *  @fn ~Rodrigues() = default
   *  @brief  Destructor
   */
  ~Rodrigues() = default;

  /**
   * @name  ToEuler
   * @fn    void ToEuler(T* gamma, T* theta, T* phi) const
   * @brief Convert rodrigues representation to Euler's angle.
   *        Assuming R = Rz*Ry*Rz.
   * @param[out] gamma  Rotation around `z` axis
   * @param[out] theta  Rotation around `y` axis
   * @param[out] phi    Rotation around `x` axis
   */
  void ToEuler(T* gamma, T* theta, T* phi) const {
    T ang = std::sqrt((x_ * x_) + (y_ * y_) + (z_ * z_));
    if (ang < std::numeric_limits<T>::epsilon()) {
      // Identity
      *gamma = T(0.0);
      *theta = T(0.0);
      *phi = T(0.0);
    } else {
      // Convert to angles
      T ct = std::cos(ang);
      T st = std::sin(ang);
      T c1 = T(1.0) - ct;
      T i_ang = ang ? T(1.0) / ang : T(0.0);
      T rx = x_ * i_ang;
      T ry = y_ * i_ang;
      T rz = z_ * i_ang;
      // Define part of Rmat
      T m0 = ct + (c1 * rx * rx);
      T m3 = (c1 * ry * rx) + (st * rz);
      T m6 = (c1 * rz * rx) - (st * ry);
      T m7 = (c1 * rz * ry) + (st * rx);
      T m8 = ct + (c1 * rz * rz);
      // Compute euler's angles
      *gamma = std::atan2(m3, m0);
      *theta = std::atan2(-m6, std::sqrt((m0 * m0) + (m3 * m3)));
      *phi = std::atan2(m7, m8);
    }
  }

  /**
   *  @name ToRotationMatrix
   *  @fn void ToRotationMatrix(Matrix3x3<T>* matrix) const
   *  @brief  Convert the rodrigues representation to a 3x3 rotation matrix.
   *  @param[out] matrix    Rotation matrix initialized from quaternion
   */
  void ToRotationMatrix(Matrix3x3<T>* matrix) const {
    auto x = static_cast<double>(x_);
    auto y = static_cast<double>(y_);
    auto z = static_cast<double>(z_);
    auto ang = std::sqrt((x * x) + (y * y) + (z * z));
    if (ang < std::numeric_limits<double>::epsilon()) {
      // Identity
      matrix->Identity();
    } else {
      // Convert to angles
      auto ct = std::cos(ang);
      auto st = std::sin(ang);
      auto c1 = 1.0 - ct;
      auto i_ang = ang ? 1.0 / ang : 0.0;
      auto vx = x * i_ang;
      auto vy = y * i_ang;
      auto vz = z * i_ang;
      // Define part of Rmat
      T* m = matrix->Data();
      m[0] = static_cast<T>(ct + (c1 * vx * vx));
      m[1] = static_cast<T>((c1 * vx * vy) - (st * vz));
      m[2] = static_cast<T>((c1 * vx * vz) + (st * vy));
      m[3] = static_cast<T>((c1 * vy * vx) + (st * vz));
      m[4] = static_cast<T>(ct + (c1 * vy * vy));
      m[5] = static_cast<T>((c1 * vy * vz) - (st * vx));
      m[6] = static_cast<T>((c1 * vz * vx) - (st * vy));
      m[7] = static_cast<T>((c1 * vz * vy) + (st * vx));
      m[8] = static_cast<T>(ct + (c1 * vz * vz));
    }
  }

  /**
   *  @name ToRotationMatrix
   *  @fn void ToRotationMatrix(Matrix4x4<T>* matrix) const
   *  @brief  Convert the rodrigues representation to a 4x4 rotation matrix.
   *  @param[out] matrix    Rotation matrix initialized from quaternion
   */
  void ToRotationMatrix(Matrix4x4<T>* matrix) const {
    auto x = static_cast<double>(x_);
    auto y = static_cast<double>(y_);
    auto z = static_cast<double>(z_);
    auto ang = std::sqrt((x * x) + (y * y) + (z * z));
    if (ang < std::numeric_limits<double>::epsilon()) {
      // Identity
      matrix->Identity();
    } else {
      // Convert to angles
      auto ct = std::cos(ang);
      auto st = std::sin(ang);
      auto c1 = 1.0 - ct;
      auto i_ang = ang ? 1.0 / ang : 0.0;
      auto vx = x * i_ang;
      auto vy = y * i_ang;
      auto vz = z * i_ang;
      // Define part of Rmat
      T* m = matrix->Data();
      m[0] = static_cast<T>(ct + (c1 * vx * vx));
      m[1] = static_cast<T>((c1 * vx * vy) - (st * vz));
      m[2] = static_cast<T>((c1 * vx * vz) + (st * vy));
      m[3] = 0.0;
      m[4] = static_cast<T>((c1 * vy * vx) + (st * vz));
      m[5] = static_cast<T>(ct + (c1 * vy * vy));
      m[6] = static_cast<T>((c1 * vy * vz) - (st * vx));
      m[7] = 0.0;
      m[8] = static_cast<T>((c1 * vz * vx) - (st * vy));
      m[9] = static_cast<T>((c1 * vz * vy) + (st * vx));
      m[10] = static_cast<T>(ct + (c1 * vz * vz));
      m[11] = 0.0;
      m[12] = 0.0;
      m[13] = 0.0;
      m[14] = 0.0;
      m[15] = 1.0;
    }
  }

  /**
   * @name  FromEuler
   * @fn    void FromEuler(const T& gamma, const T& theta, const T& phi)
   * @brief Construct the rodrigues representation from Euler's angles
   * @param[in] gamma   Rotation around `z` axis
   * @param[in] theta   Rotation around `y` axis
   * @param[in] phi     Rotation around `x` axis
   */
  void FromEuler(const T& gamma, const T& theta, const T& phi) {
    Matrix3x3<T> rmat(gamma, theta, phi);
    this->FromRotationMatrix(rmat);
  }

  /**
   *  @name FromRotationMatrix
   *  @fn void FromRotationMatrix(const Matrix3x3<T>& matrix)
   *  @brief  Initialize the rodrigues representation from a 3x3 rotation
   *          matrix.
   *  @param[in] matrix Rotation matrix to convert
   */
  void FromRotationMatrix(const Matrix3x3<T>& matrix) {
    // Taken from opencv
    Vector3<T> r;
    r.x_ = matrix(2, 1) - matrix(1, 2);
    r.y_ = matrix(0, 2) - matrix(2, 0);
    r.z_ = matrix(1, 0) - matrix(0, 1);
    T st = std::sqrt((r.x_*r.x_ + r.y_*r.y_ + r.z_*r.z_) * T(0.25));
    T ct = (matrix(0, 0) + matrix(1, 1) + matrix(2, 2) - 1) * T(0.5);
    ct = ct > 1. ? 1. : ct < -1. ? -1. : ct;
    T theta = std::acos(ct);

    if( st < 1e-5 ) {
      if( ct > T(0.0)) {
        r = Vector3<T>(0, 0, 0);
      } else {
        T t = (matrix(0, 0) + T(1.0)) * T(0.5);
        r.x_ = std::sqrt(std::max(t, T(0.0)));
        t = (matrix(1, 1) + T(1.0)) * T(0.5);
        r.y_ = std::sqrt(std::max(t, T(0.0))) * (matrix(0, 1) < T(0.0) ? T(-1.0) : T(1.0));
        t = (matrix(2, 2) + T(1.0)) * T(0.5);
        r.z_ = std::sqrt(std::max(t, T(0.0))) * (matrix(0, 2) < T(0.0) ? T(-1.0) : T(1.0));
        if (std::abs(r.x_) < std::abs(r.y_) && std::abs(r.x_) < std::abs(r.z_) &&
            (matrix(1, 2) > T(0.0)) != (r.y_ * r.z_ > T(0.0))) {
          r.z_ = -r.z_;
        }
        theta /= r.Norm();
        r *= theta;
      }
    } else {
      T vth = T(1.0) / (T(2.0) * st);
      vth *= theta;
      r *= vth;
    }
    x_ = r.x_;
    y_ = r.y_;
    z_ = r.z_;
  }

  /**
   *  @name FromRotationMatrix
   *  @fn void FromRotationMatrix(const Matrix4x4<T>& matrix)
   *  @brief  Initialize the rodrigues representation from a 3x3 rotation
   *          matrix.
   *  @param[in] matrix Rotation matrix to convert
   */
  void FromRotationMatrix(const Matrix4x4<T>& matrix) {
    // Taken from opencv
    Vector3<T> r;
    r.x_ = matrix(2, 1) - matrix(1, 2);
    r.y_ = matrix(0, 2) - matrix(2, 0);
    r.z_ = matrix(1, 0) - matrix(0, 1);
    T st = std::sqrt((r.x_*r.x_ + r.y_*r.y_ + r.z_*r.z_) * T(0.25));
    T ct = (matrix(0, 0) + matrix(1, 1) + matrix(2, 2) - 1) * T(0.5);
    ct = ct > 1. ? 1. : ct < -1. ? -1. : ct;
    T theta = std::acos(ct);
    if( st < 1e-5 ) {
      if( ct > T(0.0)) {
        r = Vector3<T>(0, 0, 0);
      } else {
        T t = (matrix(0, 0) + T(1.0)) * T(0.5);
        r.x_ = std::sqrt(std::max(t, T(0.0)));
        t = (matrix(1, 1) + T(1.0)) * T(0.5);
        r.y_ = std::sqrt(std::max(t, T(0.0))) * (matrix(0, 1) < T(0.0) ? T(-1.0) : T(1.0));
        t = (matrix(2, 2) + T(1.0)) * T(0.5);
        r.z_ = std::sqrt(std::max(t, T(0.0))) * (matrix(0, 2) < T(0.0) ? T(-1.0) : T(1.0));
        if (std::abs(r.x_) < std::abs(r.y_) && std::abs(r.x_) < std::abs(r.z_) &&
            (matrix(1, 2) > T(0.0)) != (r.y_ * r.z_ > T(0.0))) {
          r.z_ = -r.z_;
        }
        theta /= r.Norm();
        r *= theta;
      }
    } else {
      T vth = T(1.0) / (T(2.0) * st);
      vth *= theta;
      r *= vth;
    }
    x_ = r.x_;
    y_ = r.y_;
    z_ = r.z_;
  }

#pragma mark -
#pragma mark Usage

#pragma mark -
#pragma mark Members

  /** X Component */
  T x_;
  /** Y Component */
  T y_;
  /** Z Component */
  T z_;
};

}  // namespace LTS5
#endif  // __LTS5_RODRIGUES__
