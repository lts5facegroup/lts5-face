/**
 *  @file   vector.hpp
 *  @brief  Vector 2d/3d representation
 *          Based on OpenNL library
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   13/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_vector_hpp__
#define __LTS5_vector_hpp__

#include <cmath>
#include <iostream>
#include <limits>
#include <cassert>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/compare_type.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  Vector1
 *  @brief  Represent 2D vecotr
 *  @author Christophe Ecabert
 *  @date   14/10/15
 *  @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS Vector1 {

 public :
  /**
   *  @name Vector1
   *  @fn Vector1()
   *  @brief  Constructor
   */
  Vector1() : x_(0) {}

  /**
   *  @name Vector1
   *  @fn explicit Vector1(const T x)
   *  @brief  Constructor
   */
  explicit Vector1(const T x) : x_(x) {}

  /**
   *  @name Norm
   *  @fn T Norm() const
   *  @brief  Compute norm of the vector
   *  @return Norm of the vector
   */
  T Norm() const {
    return std::sqrt(x_ * x_);
  }

  /**
   *  @name SquaredNorm
   *  @fn T SquaredNorm() const
   *  @brief  Compute squared norm of the vector
   *  @return Squared norm of the vector
   */
  T SquaredNorm() const {
    return (x_ * x_);
  }

  /**
   *  @name Normalize
   *  @fn void Normalize()
   *  @brief  Normalize vector
   */
  void Normalize() {
    T norm = this->Norm();
    if (norm != 0.0) {
      x_ /= norm;
    }
  }

  /**
   * @name  Normalized
   * @fn    Vector1 Normalized() const
   * @brief Compute normalized vector
   * @return    Normalized vector
   */
  Vector1 Normalized() const {
    T nrm = this->Norm();
    return Vector1(x_ / nrm);
  }

  /**
   *  @name operator=
   *  @fn Vector1& operator=(const Vector1<T>& rhs)
   *  @brief  Assignment operator
   *  @param[in] rhs Right hand sign
   */
  Vector1& operator=(const Vector1<T>& rhs) {
    if (this != &rhs) {
      this->x_ = rhs.x_;
    }
    return *this;
  }

  /**
   * @name operator-
   * @fn    Vector1& operator-()
   * @brief Unary operator minus.
   * @return Return -this
   */
  Vector1& operator-() {
    this->x_ = -this->x_;
    return *this;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Vector1<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are same
   */
  bool operator==(const Vector1<T>& rhs) {
    return CompareType<T>(x_) == CompareType<T>(rhs.x_);
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Vector1<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are same
   */
  bool operator==(const Vector1<T>& rhs) const {
    return CompareType<T>(x_) == CompareType<T>(rhs.x_);
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Vector1<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are same
   */
  bool operator!=(const Vector1<T>& rhs) {
    return CompareType<T>(x_) != CompareType<T>(rhs.x_);
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Vector1<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are same
   */
  bool operator!=(const Vector1<T>& rhs) const {
    return CompareType<T>(x_) != CompareType<T>(rhs.x_);
  }

  /**
   *  @name operator-=
   *  @fn Vector1& operator-=(const T& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector1& operator-=(const T& rhs) {
    this->x_ -= rhs;
    return *this;
  }

  /**
   *  @name operator-=
   *  @fn Vector1& operator-=(const Vector1& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector1& operator-=(const Vector1& rhs) {
    this->x_ -= rhs.x_;
    return *this;
  }

  /**
   *  @name operator+=
   *  @fn Vector1& operator+=(const T& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector1& operator+=(const T& rhs) {
    this->x_ += rhs;
    return *this;
  }

  /**
     *  @name operator*=
     *  @fn Vector1& operator*=(const T value)
     *  @param[in]  value Right Hand sign
     *  @return Vector
     */
  Vector1& operator*=(const T value) {
    this->x_ *= value;
    return *this;
  }

  /**
   *  @name operator/=
   *  @fn Vector1& operator/=(const T value)
   *  @param[in]  value Right Hand sign
   *  @return Vector
   */
  Vector1& operator/=(const T value) {
    if (value != T(0.0)) {
      this->x_ /= value;
    } else {
      this->x_ = std::numeric_limits<T>::quiet_NaN();
    }
    return *this;
  }

  /**
   *  @name operator+=
   *  @fn Vector1& operator+=(const Vector1& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector1& operator+=(const Vector1& rhs) {
    this->x_ += rhs.x_;
    return *this;
  }

  /**
   * @name  operator[]
   * @brief Subscript operator
   * @return    Value at position
   */
  const T& operator[](int index) const {
    assert(index == 0 && "Index must be equal to 0 for single element vector");
    return x_;
  }

  /**
   * @name  operator[]
   * @brief Subscript operator
   * @return    Value at position
   */
  T& operator[](int index) {
    assert(index == 0 && "Index must be equal to 0 for single element vector");
    return x_;
  }

  /**
   *  @name operator<<
   *  @fn friend std::ostream& operator<<(std::ostream& out, const Vector1<T>& v)
   *  @param[in]  out Output strream
   *  @param[in]  v   Vector to write
   *  @return output stream
   */
  friend std::ostream& operator<<(std::ostream& out, const Vector1<T>& v) {
    return out << "[" << v.x_ << "]" ;
  }

  /**
   *  @name operator>>
   *  @fn friend std::istream& operator>>(std::istream& in, Vector1<T>& v)
   *  @param[in]  in Input stream
   *  @param[in]  v   Vector to fill
   *  @return input stream
   */
  friend std::istream& operator>>(std::istream& in, Vector1<T>& v) {
    in >> v.x_;
    return in;
  }

  /** Data type */
  using DType = T;

  /** X coordinate */
  T x_;
};

/**
 *  @class  Vector2
 *  @brief  Represent 2D vecotr
 *  @author Christophe Ecabert
 *  @date   14/10/15
 *  @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS Vector2 {

 public :
  /**
   *  @name Vector2
   *  @fn Vector2()
   *  @brief  Constructor
   */
  Vector2() : x_(0), y_(0) {}

  /**
   *  @name Vector2
   *  @fn Vector2(const T x,const T y)
   *  @brief  Constructor
   */
  Vector2(const T x,const T y) : x_(x), y_(y) {}

  /**
   *  @name Norm
   *  @fn T Norm() const
   *  @brief  Compute norm of the vector
   *  @return Norm of the vector
   */
  T Norm() const {
    return std::sqrt(x_ * x_ + y_* y_);
  }

  /**
   *  @name SquaredNorm
   *  @fn T SquaredNorm() const
   *  @brief  Compute squared norm of the vector
   *  @return Squared norm of the vector
   */
  T SquaredNorm() const {
    return (x_ * x_) + (y_* y_);
  }

  /**
   *  @name Normalize
   *  @fn void Normalize()
   *  @brief  Normalize vector
   */
  void Normalize() {
    T norm = this->Norm();
    if (norm != 0.0) {
      x_ /= norm; y_ /= norm;
    }
  }

  /**
   * @name  Normalized
   * @fn    Vector2 Normalized() const
   * @brief Compute normalized vector
   * @return    Normalized vector
   */
  Vector2 Normalized() const {
    T nrm = this->Norm();
    return Vector2(x_ / nrm, y_ / nrm);
  }

  /**
   *  @name operator=
   *  @fn Vector2& operator=(const Vector2<T>& rhs)
   *  @brief  Assignment operator
   *  @param[in] rhs Right hand sign
   */
  Vector2& operator=(const Vector2<T>& rhs) {
    if (this != &rhs) {
      this->x_ = rhs.x_;
      this->y_ = rhs.y_;
    }
    return *this;
  }

  /**
   * @name operator-
   * @fn    Vector2& operator-()
   * @brief Unary operator minus.
   * @return Return -this
   */
  Vector2& operator-() {
    this->x_ = -this->x_;
    this->y_ = -this->y_;
    return *this;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Vector2<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are same
   */
  bool operator==(const Vector2<T>& rhs) {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 2; ++i) {
      if (CompareType<T>(ptr[i]) != CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Vector2<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are same
   */
  bool operator==(const Vector2<T>& rhs) const {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 2; ++i) {
      if (CompareType<T>(ptr[i]) != CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Vector2<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are same
   */
  bool operator!=(const Vector2<T>& rhs) {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 2; ++i) {
      if (CompareType<T>(ptr[i]) == CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Vector2<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are same
   */
  bool operator!=(const Vector2<T>& rhs) const {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 2; ++i) {
      if (CompareType<T>(ptr[i]) == CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator-=
   *  @fn Vector2& operator-=(const T& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector2& operator-=(const T& rhs) {
    this->x_ -= rhs;
    this->y_ -= rhs;
    return *this;
  }

  /**
   *  @name operator-=
   *  @fn Vector2& operator-=(const Vector2& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector2& operator-=(const Vector2& rhs) {
    this->x_ -= rhs.x_;
    this->y_ -= rhs.y_;
    return *this;
  }

  /**
   *  @name operator+=
   *  @fn Vector2& operator+=(const T& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector2& operator+=(const T& rhs) {
    this->x_ += rhs;
    this->y_ += rhs;
    return *this;
  }

  /**
     *  @name operator*=
     *  @fn Vector2& operator*=(const T value)
     *  @param[in]  value Right Hand sign
     *  @return Vector
     */
  Vector2& operator*=(const T value) {
    this->x_ *= value;
    this->y_ *= value;
    return *this;
  }

  /**
   *  @name operator/=
   *  @fn Vector2& operator/=(const T value)
   *  @param[in]  value Right Hand sign
   *  @return Vector
   */
  Vector2& operator/=(const T value) {
    if (value != T(0.0)) {
      this->x_ /= value;
      this->y_ /= value;
    } else {
      this->x_ = std::numeric_limits<T>::quiet_NaN();
      this->y_ = std::numeric_limits<T>::quiet_NaN();
    }
    return *this;
  }

  /**
   *  @name operator+=
   *  @fn Vector2& operator+=(const Vector2& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector2& operator+=(const Vector2& rhs) {
    this->x_ += rhs.x_;
    this->y_ += rhs.y_;
    return *this;
  }

  /**
   * @name  operator[]
   * @brief Subscript operator
   * @return    Value at position
   */
  const T& operator[](int index) const {
    assert(index < 2 && "Index must be smaller than 2");
    if (index == 0) {
      return x_;
    } else {
      return y_;
    }
  }

  /**
   * @name  operator[]
   * @brief Subscript operator
   * @return    Value at position
   */
  T& operator[](int index) {
    assert(index < 2 && "Index must be smaller than 2");
    if (index == 0) {
      return x_;
    } else {
      return y_;
    }
  }

  /**
   *  @name operator<<
   *  @fn friend std::ostream& operator<<(std::ostream& out, const Vector2<T>& v)
   *  @param[in]  out Output strream
   *  @param[in]  v   Vector to write
   *  @return output stream
   */
  friend std::ostream& operator<<(std::ostream& out, const Vector2<T>& v) {
    return out << "[" << v.x_ << ", " << v.y_ << "]" ;
  }

  /**
   *  @name operator>>
   *  @fn friend std::istream& operator>>(std::istream& in, Vector2<T>& v)
   *  @param[in]  in Input stream
   *  @param[in]  v   Vector to fill
   *  @return input stream
   */
  friend std::istream& operator>>(std::istream& in, Vector2<T>& v) {
    in >> v.x_ >> v.y_;
    return in;
  }

  /** Data type */
  using DType = T;

  union {
    /** X coordinate */
    T x_;
    /** Red channel */
    T r_;
  };
  union {
    /** Y coordinate */
    T y_;
    /** Green channel */
    T g_;
  };
};

/**
 *  @class  Vector3
 *  @brief  Represent 3D vecotr
 *  @author Christophe Ecabert
 *  @date   14/10/15
 *  @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS Vector3 {

  public :
  /**
   *  @name Vector3
   *  @fn Vector3()
   *  @brief  Constructor
   */
  Vector3() : x_(0), y_(0), z_(0) {}

  /**
   *  @name Vector3
   *  @fn Vector3(const T x,const T y, const T z)
   *  @brief  Constructor
   */
  Vector3(const T x,const T y, const T z) : x_(x), y_(y), z_(z) {}

  /**
   *  @name Norm
   *  @fn T Norm() const
   *  @brief  Compute norm of the vector
   *  @return Norm of the vector
   */
  T Norm() const {
    return std::sqrt(x_ * x_ + y_* y_ + z_  * z_);
  }

  /**
   *  @name SquaredNorm
   *  @fn T SquaredNorm() const
   *  @brief  Compute squared norm of the vector
   *  @return Squared norm of the vector
   */
  T SquaredNorm() const {
    return (x_ * x_) + (y_* y_) + (z_ * z_);
  }

  /**
   *  @name Normalize
   *  @fn void Normalize()
   *  @brief  Normalize vector
   */
  void Normalize() {
    T norm = this->Norm();
    if (norm != 0.0) {
      x_ /= norm; y_ /= norm; z_ /= norm;
    }
  }

  /**
   * @name  Normalized
   * @fn    Vector3 Normalized() const
   * @brief Compute normalized vector
   * @return    Normalized vector
   */
  Vector3 Normalized() const {
    T nrm = this->Norm();
    return Vector3(x_ / nrm, y_ / nrm, z_ / nrm);
  }

  /**
   *  @name operator=
   *  @fn Vector3& operator=(const Vector3<T>& rhs)
   *  @brief  Assignment operator
   *  @param[in] rhs Right hand sign
   */
  Vector3& operator=(const Vector3<T>& rhs) {
    if (this != &rhs) {
      this->x_ = rhs.x_;
      this->y_ = rhs.y_;
      this->z_ = rhs.z_;
    }
    return *this;
  }

  /**
   * @name operator-
   * @fn    Vector3& operator-()
   * @brief Unary operator minus.
   * @return Return -this
   */
  Vector3& operator-() {
    this->x_ = -this->x_;
    this->y_ = -this->y_;
    this->z_ = -this->z_;
    return *this;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Vector3<T>& rhs)
   *  @param[in] rhs Right Hand sign
   *  @return True if vector are same
   */
  bool operator==(const Vector3<T>& rhs) {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 3; ++i) {
      if (CompareType<T>(ptr[i]) != CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Vector3<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are same
   *  @see https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
   */
  bool operator==(const Vector3<T>& rhs) const {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 3; ++i) {
      if (CompareType<T>(ptr[i]) != CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Vector3<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are different
   */
  bool operator!=(const Vector3<T>& rhs) {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 3; ++i) {
      if (CompareType<T>(ptr[i]) == CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Vector3<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are different
   */
  bool operator!=(const Vector3<T>& rhs) const {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 3; ++i) {
      if (CompareType<T>(ptr[i]) == CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator-=
   *  @fn Vector3& operator-=(const T& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector3& operator-=(const T& rhs) {
    this->x_ -= rhs;
    this->y_ -= rhs;
    this->z_ -= rhs;
    return *this;
  }

  /**
   *  @name operator-=
   *  @fn Vector3& operator-=(const Vector3& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector3& operator-=(const Vector3& rhs) {
    this->x_ -= rhs.x_;
    this->y_ -= rhs.y_;
    this->z_ -= rhs.z_;
    return *this;
  }

  /**
   *  @name operator+=
   *  @fn Vector3& operator+=(const T& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector3& operator+=(const T& rhs) {
    this->x_ += rhs;
    this->y_ += rhs;
    this->z_ += rhs;
    return *this;
  }

  /**
   *  @name operator+=
   *  @fn Vector3& operator+=(const Vector3& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector3& operator+=(const Vector3& rhs) {
    this->x_ += rhs.x_;
    this->y_ += rhs.y_;
    this->z_ += rhs.z_;
    return *this;
  }

  /**
   *  @name operator*=
   *  @fn Vector3& operator*=(const T value)
   *  @param[in]  value Right Hand sign
   *  @return Vector
   */
  Vector3& operator*=(const T value) {
    this->x_ *= value;
    this->y_ *= value;
    this->z_ *= value;
    return *this;
  }

  /**
   *  @name operator/=
   *  @fn Vector3& operator/=(const T value)
   *  @param[in]  value Right Hand sign
   *  @return Vector
   */
  Vector3& operator/=(const T value) {
    if (value != T(0.0)) {
      this->x_ /= value;
      this->y_ /= value;
      this->z_ /= value;
    } else {
      this->x_ = std::numeric_limits<T>::quiet_NaN();
      this->y_ = std::numeric_limits<T>::quiet_NaN();
      this->z_ = std::numeric_limits<T>::quiet_NaN();
    }
    return *this;
  }

  /**
   * @name  operator[]
   * @brief Subscript operator
   * @return    Value at position
   */
  const T& operator[](int index) const {
    assert(index < 3 && "Index must be smaller than 3");
    if (index == 0) {
      return x_;
    } else if (index == 1){
      return y_;
    } else {
      return z_;
    }
  }

  /**
   * @name  operator[]
   * @brief Subscript operator
   * @return    Value at position
   */
  T& operator[](int index) {
    assert(index < 3 && "Index must be smaller than 3");
    if (index == 0) {
      return x_;
    } else if (index == 1){
      return y_;
    } else {
      return z_;
    }
  }

  /**
   *  @name operator<<
   *  @fn friend std::ostream& operator<<(std::ostream& out, const Vector3<T>& v)
   *  @param[in]  out Output strream
   *  @param[in]  v   Vector to write
   *  @return output stream
   */
  friend std::ostream& operator<<(std::ostream& out, const Vector3<T>& v) {
    return out << "[" << v.x_ << ", " << v.y_ << ", " << v.z_ << "]" ;
  }

  /**
   *  @name operator>>
   *  @fn friend std::istream& operator>>(std::istream& in, Vector3<T>& v)
   *  @param[in]  in Input stream
   *  @param[in]  v   Vector to fill
   *  @return input stream
   */
  friend std::istream& operator>>(std::istream& in, Vector3<T>& v) {
    in >> v.x_ >> v.y_ >> v.z_;
    return in;
  }

  /**
   *  @name ScalarTriple
   *  @fn static T ScalarTriple(const Vector3& u,const Vector3& v,const Vector3& w)
   *  @brief  Compute scalar triple between three vector : val = (u x v).W
   *  @param[in]  u   First vector
   *  @param[in]  v   Second vector
   *  @param[in]  w   Third vector
   *  @return Scalar triple value
   */
  static T ScalarTriple(const Vector3& u,
                        const Vector3& v,
                        const Vector3& w) {
    return (u ^ v) * w;
  }

  /** Data type */
  using DType = T;

  union {
    /** X coordinate */
    T x_;
    /** Red channel */
    T r_;
  };
  union {
    /** Y coordinate */
    T y_;
    /** Green channel */
    T g_;
  };
  union {
    /** Z coordinate */
    T z_;
    /** Blue channel */
    T b_;
  };

};

/**
 *  @class  Vector4
 *  @brief  Represent 3D vecotr
 *  @author Christophe Ecabert
 *  @date   14/10/15
 *  @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS Vector4 {

public :
  /**
   *  @name Vector4
   *  @fn Vector4()
   *  @brief  Constructor
   */
  Vector4() : x_(0), y_(0), z_(0), w_(0) {}

  /**
   *  @name Vector4
   *  @fn Vector4(const T x,const T y, const T z,  const T w)
   *  @brief  Constructor
   */
  Vector4(const T x,const T y, const T z, const T w) : x_(x), y_(y), z_(z), w_(w) {}

  /**
   *  @name Norm
   *  @fn T Norm() const
   *  @brief  Compute norm of the vector
   *  @return Norm of the vector
   */
  T Norm() const {
    return std::sqrt(x_ * x_ + y_* y_ + z_  * z_ + w_ * w_);
  }

  /**
   *  @name SquaredNorm
   *  @fn T SquaredNorm() const
   *  @brief  Compute squared norm of the vector
   *  @return Squared norm of the vector
   */
  T SquaredNorm() const {
    return (x_ * x_) + (y_* y_) + (z_ * z_) + (w_ * w_);
  }

  /**
   *  @name Normalize
   *  @fn void Normalize()
   *  @brief  Normalize vector
   */
  void Normalize() {
    T norm = this->Norm();
    if (norm != 0.0) {
      x_ /= norm; y_ /= norm; z_ /= norm; w_ /= norm;
    }
  }

  /**
   * @name  Normalized
   * @fn    Vector3 Normalized() const
   * @brief Compute normalized vector
   * @return    Normalized vector
   */
  Vector4 Normalized() const {
    T nrm = this->Norm();
    return Vector4(x_ / nrm, y_ / nrm, z_ / nrm, w_ / nrm);
  }

  /**
   *  @name operator=
   *  @fn Vector4& operator=(const Vector4<T>& rhs)
   *  @brief  Assignment operator
   *  @param[in] rhs Right hand sign
   */
  Vector4& operator=(const Vector4<T>& rhs) {
    if (this != &rhs) {
      this->x_ = rhs.x_;
      this->y_ = rhs.y_;
      this->z_ = rhs.z_;
      this->w_ = rhs.w_;
    }
    return *this;
  }

  /**
   * @name operator-
   * @fn    Vector4& operator-()
   * @brief Unary operator minus.
   * @return Return -this
   */
  Vector4& operator-() {
    this->x_ = -this->x_;
    this->y_ = -this->y_;
    this->z_ = -this->z_;
    this->w_ = -this->w_;
    return *this;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Vector4<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are same
   */
  bool operator==(const Vector4<T>& rhs) {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 4; ++i) {
      if (CompareType<T>(ptr[i]) != CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Vector4<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are same
   */
  bool operator==(const Vector4<T>& rhs) const {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 4; ++i) {
      if (CompareType<T>(ptr[i]) != CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Vector4<T>& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are different
   */
  bool operator!=(const Vector4<T>& rhs) {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 4; ++i) {
      if (CompareType<T>(ptr[i]) == CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Vector4<T>& rhs) const
   *  @param[in]  rhs Right Hand sign
   *  @return True if vector are different
   */
  bool operator!=(const Vector4<T>& rhs) const {
    const T* rhs_ptr = &rhs.x_;
    const T* ptr = &x_;
    for (int i = 0; i < 4; ++i) {
      if (CompareType<T>(ptr[i]) == CompareType<T>(rhs_ptr[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   *  @name operator-=
   *  @fn Vector4& operator-=(const T& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector4& operator-=(const T& rhs) {
    this->x_ -= rhs;
    this->y_ -= rhs;
    this->z_ -= rhs;
    this->w_ -= rhs;
    return *this;
  }

  /**
   *  @name operator-=
   *  @fn Vector4& operator-=(const Vector4& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector4& operator-=(const Vector4& rhs) {
    this->x_ -= rhs.x_;
    this->y_ -= rhs.y_;
    this->z_ -= rhs.z_;
    this->w_ -= rhs.w_;
    return *this;
  }

  /**
   *  @name operator+=
   *  @fn Vector4& operator+=(const T& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector4& operator+=(const T& rhs) {
    this->x_ += rhs;
    this->y_ += rhs;
    this->z_ += rhs;
    this->w_ += rhs;
    return *this;
  }

  /**
   *  @name operator+=
   *  @fn Vector4& operator+=(const Vector4& rhs)
   *  @param[in]  rhs Right Hand sign
   *  @return Vector
   */
  Vector4& operator+=(const Vector4& rhs) {
    this->x_ += rhs.x_;
    this->y_ += rhs.y_;
    this->z_ += rhs.z_;
    this->w_ += rhs.w_;
    return *this;
  }

  /**
   *  @name operator*=
   *  @fn Vector4& operator*=(const T value)
   *  @param[in]  value Right Hand sign
   *  @return Vector
   */
  Vector4& operator*=(const T value) {
    this->x_ *= value;
    this->y_ *= value;
    this->z_ *= value;
    this->w_ *= value;
    return *this;
  }

  /**
   *  @name operator/=
   *  @fn Vector4& operator/=(const T value)
   *  @param[in]  value Right Hand sign
   *  @return Vector
   */
  Vector4& operator/=(const T value) {
    if (value != T(0.0)) {
      this->x_ /= value;
      this->y_ /= value;
      this->z_ /= value;
      this->w_ /= value;
    } else {
      this->x_ = std::numeric_limits<T>::quiet_NaN();
      this->y_ = std::numeric_limits<T>::quiet_NaN();
      this->z_ = std::numeric_limits<T>::quiet_NaN();
      this->w_ = std::numeric_limits<T>::quiet_NaN();
    }
    return *this;
  }

  /**
   * @name  operator[]
   * @brief Subscript operator
   * @return    Value at position
   */
  const T& operator[](int index) const {
    assert(index < 4 && "Index must be smaller than 4");
    if (index == 0) {
      return x_;
    } else if (index == 1) {
      return y_;
    } else if (index == 2) {
      return z_;
    } else {
      return w_;
    }
  }

  /**
   * @name  operator[]
   * @brief Subscript operator
   * @return    Value at position
   */
  T& operator[](int index) {
    assert(index < 4 && "Index must be smaller than 4");
    if (index == 0) {
      return x_;
    } else if (index == 1) {
      return y_;
    } else if (index == 2) {
      return z_;
    } else {
      return w_;
    }
  }

  /**
   *  @name operator<<
   *  @fn friend std::ostream& operator<<(std::ostream& out, const Vector4<T>& v)
   *  @param[in]  out Output strream
   *  @param[in]  v   Vector to write
   *  @return output stream
   */
  friend std::ostream& operator<<(std::ostream& out, const Vector4<T>& v) {
    return out << "[" << v.x_ << ", " << v.y_ << ", " << v.z_ << ", " << v.w_ << "]";
  }

  /**
   *  @name operator>>
   *  @fn friend std::istream& operator>>(std::istream& in, Vector4<T>& v)
   *  @param[in]  in Input stream
   *  @param[in]  v   Vector to fill
   *  @return input stream
   */
  friend std::istream& operator>>(std::istream& in, Vector4<T>& v) {
    in >> v.x_ >> v.y_ >> v.z_ >> v.w_;
    return in;
  }

  /** Data type */
  using DType = T;

  union {
    /** X coordinate */
    T x_;
    /** Red channel */
    T r_;
  };
  union {
    /** Y coordinate */
    T y_;
    /** Green channel */
    T g_;
  };
  union {
    /** Z coordinate */
    T z_;
    /** Blue channel */
    T b_;
  };
  union {
    /** W coordinate */
    T w_;
    /** Alpha channel */
    T a_;
  };
};

/**
 * @class  Point
 * @brief   Nd point abstraction
 * @author  Christophe Ecabert
 * @date    30/11/2020
 * @ingroup utils
 */
template<int Dim, typename T>
struct Point {};

template<typename T>
struct Point<1, T> {
  using Type = Vector1<T>;
};

template<typename T>
struct Point<2, T> {
  using Type = Vector2<T>;
};

template<typename T>
struct Point<3, T> {
  using Type = Vector3<T>;
};

template<typename T>
struct Point<4, T> {
  using Type = Vector4<T>;
};


/** Dot Product */
template<typename T>
T LTS5_EXPORTS operator*(const Vector1<T>& v1, const Vector1<T>& v2) {
  return v1.x_ * v2.x_;
}

template<typename T>
T LTS5_EXPORTS operator*(const Vector2<T>& v1, const Vector2<T>& v2) {
  return v1.x_ * v2.x_ + v1.y_ * v2.y_;
}

template<typename T>
T LTS5_EXPORTS operator*(const Vector3<T>& v1, const Vector3<T>& v2) {
  return v1.x_ * v2.x_ + v1.y_ * v2.y_ + v1.z_ * v2.z_;
}

template<typename T>
T LTS5_EXPORTS operator*(const Vector4<T>& v1, const Vector4<T>& v2) {
  return v1.x_ * v2.x_ + v1.y_ * v2.y_ + v1.z_ * v2.z_ + v1.w_ * v2.w_;
}

/** Scalar Product */
template<typename T>
Vector1<T> LTS5_EXPORTS operator*(const Vector1<T>& v1, const T value) {
  return Vector1<T>(v1.x_ * value);
}

template<typename T>
Vector2<T> LTS5_EXPORTS operator*(const Vector2<T>& v1, const T value) {
  return Vector2<T>(v1.x_ * value, v1.y_ * value);
}

template<typename T>
Vector3<T> LTS5_EXPORTS operator*(const Vector3<T>& v1, const T value) {
  return Vector3<T>(v1.x_ * value, v1.y_ * value, v1.z_ * value);
}

template<typename T>
Vector4<T> LTS5_EXPORTS operator*(const Vector4<T>& v1, const T value) {
  return Vector4<T>(v1.x_ * value, v1.y_ * value, v1.z_ * value, v1.w_ * value);
}

template<typename T>
Vector1<T> LTS5_EXPORTS operator*(const T value, const Vector1<T>& v1) {
  return Vector1<T>(v1.x_ * value);
}

template<typename T>
Vector2<T> LTS5_EXPORTS operator*(const T value, const Vector2<T>& v1) {
  return Vector2<T>(v1.x_ * value, v1.y_ * value);
}

template<typename T>
Vector3<T> LTS5_EXPORTS operator*(const T value, const Vector3<T>& v1) {
  return Vector3<T>(v1.x_ * value, v1.y_ * value, v1.z_ * value);
}

template<typename T>
Vector4<T> LTS5_EXPORTS operator*(const T value, const Vector4<T>& v1) {
  return Vector4<T>(v1.x_ * value, v1.y_ * value, v1.z_ * value, v1.w_ * value);
}

/** Division Product */
template<typename T>
Vector1<T> LTS5_EXPORTS operator/(const Vector1<T>& v1, const T value) {
  return Vector1<T>(v1.x_ / value);
}

template<typename T>
Vector2<T> LTS5_EXPORTS operator/(const Vector2<T>& v1, const T value) {
  return Vector2<T>(v1.x_ / value, v1.y_ / value);
}

template<typename T>
Vector3<T> LTS5_EXPORTS operator/(const Vector3<T>& v1, const T value) {
  return Vector3<T>(v1.x_ / value, v1.y_ / value, v1.z_ / value);
}

template<typename T>
Vector4<T> LTS5_EXPORTS operator/(const Vector4<T>& v1, const T value) {
  return Vector4<T>(v1.x_ / value, v1.y_ / value, v1.z_ / value, v1.w_ / value);
}

/* cross product */
template<typename T>
Vector3<T> LTS5_EXPORTS operator^(const Vector3<T>& v1, const Vector3<T>& v2) {
  return Vector3<T>(v1.y_ * v2.z_ - v2.y_ * v1.z_,
                    v1.z_ * v2.x_ - v2.z_ * v1.x_,
                    v1.x_ * v2.y_ - v2.x_ * v1.y_);
}

/** Addition */
template<typename T>
Vector1<T> LTS5_EXPORTS operator+(const Vector1<T>& v1, const Vector1<T>& v2) {
  return Vector1<T>(v1.x_ + v2.x_);
}

template<typename T>
Vector2<T> LTS5_EXPORTS operator+(const Vector2<T>& v1, const Vector2<T>& v2) {
  return Vector2<T>(v1.x_ + v2.x_,
                    v1.y_ + v2.y_);
}

template<typename T>
Vector3<T> LTS5_EXPORTS operator+(const Vector3<T>& v1, const Vector3<T>& v2) {
  return Vector3<T>(v1.x_ + v2.x_,
                    v1.y_ + v2.y_,
                    v1.z_ + v2.z_);
}

template<typename T>
Vector4<T> LTS5_EXPORTS operator+(const Vector4<T>& v1, const Vector4<T>& v2) {
  return Vector4<T>(v1.x_ + v2.x_,
                    v1.y_ + v2.y_,
                    v1.z_ + v2.z_,
                    v1.w_ + v2.w_);
}

/** Sustraction */
template<typename T>
Vector1<T> LTS5_EXPORTS operator-(const Vector1<T>& v1, const Vector1<T>& v2) {
  return Vector1<T>(v1.x_ - v2.x_);
}

template<typename T>
Vector2<T> LTS5_EXPORTS operator-(const Vector2<T>& v1, const Vector2<T>& v2) {
  return Vector2<T>(v1.x_ - v2.x_,
                    v1.y_ - v2.y_);
}

template<typename T>
Vector3<T> LTS5_EXPORTS operator-(const Vector3<T>& v1, const Vector3<T>& v2) {
  return Vector3<T>(v1.x_ - v2.x_,
                    v1.y_ - v2.y_,
                    v1.z_ - v2.z_);
}

template<typename T>
Vector4<T> LTS5_EXPORTS operator-(const Vector4<T>& v1, const Vector4<T>& v2) {
  return Vector4<T>(v1.x_ - v2.x_,
                    v1.y_ - v2.y_,
                    v1.z_ - v2.z_,
                    v1.w_ - v2.w_);
}

}  // namespace LTS5



#endif /* __LTS5_vector_hpp__ */
