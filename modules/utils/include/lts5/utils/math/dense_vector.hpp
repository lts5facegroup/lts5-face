/**
 *  @file   dense_vector.hpp
 *  @brief  Dense vector data structure.
 *          Based on OpenNL library
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   12/10/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_dense_vector_hpp__
#define __LTS5_dense_vector_hpp__

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  DenseVector
 *  @brief  Represent dense vector data structure.
 *          Based on OpenNL Library
 *  @author Christophe Ecabert
 *  @date   12/10/15
 *  @ingroup utils
 */
template <typename T>
class LTS5_EXPORTS DenseVector {

#pragma mark -
#pragma mark Initialization
 public :

  /**
   *  @name DenseVector
   *  @fn DenseVector(int dimension)
   *  @brief      Constructor
   *  @param[in]  dimension Vector dimension
   */
  explicit DenseVector(int dimension);

  /**
   *  @name DenseVector
   *  @fn DenseVector(const DenseVector& other)
   *  @brief  Copy constructor
   *  @param[in]  other Vector to copy
   */
  DenseVector(const DenseVector& other);

  /**
   *  @name ~DenseVector
   *  @fn ~DenseVector(void)
   *  @brief  Destructor
   */
  ~DenseVector(void);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name dimension
   *  @fn int dimension(void) const
   *  @brief  Provide size of the vector
   *  @return Vector's size
   */
  int dimension(void) const {
    return dimension_;
  }

  /**
   *  @name data
   *  @fn T* data(void) const
   *  @brief  Provide direct access to vector coefficients
   *  @return Pointer to first element
   */
  T* data(void) const {
    return coeff_;
  }

  /**
   *  @name data
   *  @fn T* data(void)
   *  @brief  Provide direct access to vector coefficients
   *  @return Pointer to first element
   */
  T* data(void) {
    return coeff_;
  }

#pragma mark -
#pragma mark Operator
  /**
   *  @name DenseVector
   *  @fn DenseVector& operator=(const DenseVector& other)
   *  @brief  Assignment operator
   *  @param[in]  other Vector to assign from
   *  @return Assign vector
   */
  DenseVector& operator=(const DenseVector& other);

  /**
   *  @name operator[]
   *  @fn const T& operator[](int i) const
   *  @brief  Provide access to element i
   *  @param[in]  i Element of interest
   *  @return     Value of the ith element
   */
  const T& operator[](int i) const;

  /**
   *  @name operator[]
   *  @fn T& operator[](int i)
   *  @brief  Provide access to element i
   *  @param[in]  i Element of interest
   *  @return     Value of the ith element
   */
  T& operator[](int i);

#pragma mark -
#pragma mark Static Methods

  /**
   *  @name axpy
   *  @fn static void axpy(const T a, const DenseVector& x, DenseVector* y)
   *  @brief  Blas function : y = y + a * x
   *  @param[in]  a   Scaling factor
   *  @param[in]  x   Vector
   *  @param[out] y   Resulting vector
   */
  static void axpy(const T a, const DenseVector& x, DenseVector* y);

  /**
   *  @name scal
   *  @fn static void scal(const T a, DenseVector* x)
   *  @brief  Blas function : x = a * x
   *  @param[in]      a   Scaling factor
   *  @param[in,out]  x   Vector
   */
  static void scal(const T a, DenseVector* x);

  /**
   *  @name copy
   *  @fn static void copy(const DenseVector& x, DenseVector* y)
   *  @brief  Blas function : y <- x
   *  @param[in]   x   Input vector
   *  @param[out]  y   Copy
   */
  static void copy(const DenseVector& x, DenseVector* y);

  /**
   *  @name dot
   *  @fn static T dot(const DenseVector& x,const DenseVector& y)
   *  @brief  Blas function : return xt * y
   *  @param[in]   x   Vector
   *  @param[out]  y   Vector
   */
  static T dot(const DenseVector& x,const DenseVector& y);

#pragma mark -
#pragma mark Private

 private :
  /** Vector dimension */
  int dimension_;
  /** Array of coefficients */
  T* coeff_;

};

}  // namespace LTS5

#endif /* __LTS5_dense_vector_hpp__ */
