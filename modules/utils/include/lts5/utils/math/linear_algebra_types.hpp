/**
 *  @file   linear_algebra_types.hpp
 *  @brief  Type definition for BLAS+Lapack library
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   04/05/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_linear_algebra_type__
#define __LTS5_linear_algebra_type__

#if defined(__APPLE__)
  #include <Accelerate/Accelerate.h>
  //Lapack type
  typedef __CLPK_integer  lapack_int;
  typedef __CLPK_real lapack_flt;
  typedef __CLPK_doublereal lapack_dbl;
#else
  #include <cblas.h>
  #include <lapack.h>
  //Lapack
  typedef float lapack_flt;
  typedef double lapack_dbl;
#endif
#endif
