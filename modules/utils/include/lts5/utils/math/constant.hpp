/**
 *  @file   constant.hpp
 *  @brief  Math constants
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   13/12/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __CONSTANT__
#define __CONSTANT__

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   Constants
 * @brief   Math constant definition
 * @author  Christophe Ecabert
 * @date    13/12/2016
 * @tparam T    Type of constant
 * @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS Constants {
 public:
  /** Pi */
  static constexpr T PI = T(3.14159265358979323846);
  /** Pi / 2 */
  static constexpr T PI_2 = T(1.57079632679489661923);
  /** Pi / 3 */
  static constexpr T PI_3 = T(1.04719755119659774615);
  /** Pi / 4 */
  static constexpr T PI_4 = T(0.78539816339744830961);
  /** Pi / 6 */
  static constexpr T PI_6 = T(0.52359877559829887307);
  /** Rad to Deg */
  static constexpr T Rad2Deg = T(180.0) / Constants<T>::PI;
  /** Deg to Rad */
  static constexpr T Deg2Rad = Constants<T>::PI / T(180.0);
  /** Square root of 2 */
  static constexpr T Sqrt2 = T(1.4142135623730950488);
};
}  // namepsace LTS5
#endif //__CONSTANT__
