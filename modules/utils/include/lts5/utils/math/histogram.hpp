/**
 *  @file   lts5/utils/math/histogram.hpp
 *  @brief  Simple histogram implementation
 *  @ingroup    utils
 *
 *  @author Christophe Ecabert
 *  @date   6/16/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_HISTOGRAM__
#define __LTS5_HISTOGRAM__

#include <vector>
#include <list>
#include <utility>

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   Histogram
 * @brief   Simple histogram / binning mechanism implementation
 * @author  Christophe Ecabert
 * @date    16/06/2020
 * @ingroup utils
 * @tparam T Data type
 */
template<typename T>
class LTS5_EXPORTS Histogram {
 public:
  /** Sample range */
  using Range = std::pair<T, T>;
  /** Bin */
  using Bin = std::list<size_t>;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  Histogram
   * @fn    Histogram() = default
   * @brief Constructor
   */
  Histogram() = default;

  /**
   * @name  Histogram
   * @fn    Histogram(const Histogram& other) = default
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  Histogram(const Histogram& other) = default;

  /**
   * @name  operator=
   * @fn    Histogram& operator=(const Histogram& rhs) = default
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  Histogram& operator=(const Histogram& rhs) = default;

  /**
   * @name  ~Histogram
   * @fn    ~Histogram() = default
   * @brief Destructor
   */
  ~Histogram() = default;

  /**
   * @name  Build
   * @fn    void Build(const cv::Mat& data,
                        const std::vector<Range>& ranges,
                        const size_t& bin_per_channel)
   * @brief Build histogram for a given set of data points
   * @param[in] data    2D array of data points of dimensions NxK to use to
   *                    build the histogram. Each row is considered as a sample
   *                    with K dimension features.
   * @param[in] ranges   Data range for each channel (i.e. K pairs <min, max>)
   * @param[in] bin_per_channel Number of bins per feature channel.
   * @return -1 if error, 0 otherwise
   */
  int Build(const cv::Mat& data,
            const std::vector<Range>& ranges,
            const size_t& bin_per_channel);

#pragma mark -
#pragma mark Accessors

  /**
   * @name get_bins
   * @fn const std::vector<Bin>& get_bins() const
   * @brief Return the underlying histogram.
   * @return Histogram data structure
   */
  const std::vector<Bin>& get_bins() const {
    return bins_;
  }

  /**
   * @name  get_ranges
   * @fn    const std::vector<Range>& get_ranges() const
   * @brief Provide the data range used to build the histogram
   * @return    List of range pair <xmin, xmax>
   */
  const std::vector<Range>& get_ranges() const {
    return ranges_;
  }

  /**
   * @name  count
   * @fn    const size_t& count() const
   * @brief Total number of sample in the histogram
   * @return    Number
   */
  const size_t& count() const {
    return count_;
  }

#pragma mark -
#pragma mark Private

 private:
  /**
   * @name  GetBinIndex
   * @fn    int GetBinIndex(const T* sample_ptr)
   * @brief Compute the bin index of a given sample
   * @param[in] sample_ptr Pointer to the sample array.
   * @param[in] length      Array dimension
   * @tparam T Underlying data type
   * @return    Bin index
   */
  size_t GetBinIndex(const T* sample_ptr, int length);

  /** Histogram bins */
  std::vector<Bin> bins_;
  /** Data value span */
  std::vector<Range> ranges_;
  /** Bins offset used while computing bin index */
  std::vector<size_t> offsets_;
  /** Total number of bins */
  size_t total_bins_;
  /** Number of bins per channel */
  size_t bin_per_channel_;
  /** Number of sample in the histogram */
  size_t count_;
};

/**
 * @name    UniformHistogramSampling
 * @fn  void UniformHistogramSampling(const Histogram<T>& histogram,
                                      const int& n_sample,
                                      std::vector<size_t>* indices)
 * @brief Sample an histogram uniformly
 * @param[in] histogram Histogram to sample from
 * @param[in] n_sample Number of samples to draw
 * @param[out] indices Sampled indices
 * @tparam T    Data type
 */
template<typename T>
void LTS5_EXPORTS UniformHistogramSampling(const Histogram<T>& histogram,
                                           const size_t& n_sample,
                                           std::vector<size_t>* indices);

/**
 * @name    WeightedHistogramSampling
 * @fn  void WeightedHistogramSampling(const Histogram<T>& histogram,
                                      const int& n_sample,
                                      std::vector<size_t>* indices)
 * @brief Sample an histogram with the same distribution (i.e. will draw more
 *  samples from most populated bins)
 * @param[in] histogram Histogram to sample from
 * @param[in] n_sample Number of samples to draw
 * @param[out] indices Sampled indices
 * @tparam T    Data type
 */
template<typename T>
void LTS5_EXPORTS WeightedHistogramSampling(const Histogram<T>& histogram,
                                            const size_t& n_sample,
                                            std::vector<size_t>* indices);

}  // namespace LTS5
#endif  // __LTS5_HISTOGRAM__
