/**
 *  @file   fixed_point.hpp
 *  @brief  Basic fixed point algebra
 *          Based on
 *          http://www.drdobbs.com/cpp/optimizing-math-intensive-applications-w/207000448
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   29/11/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __FIXED_POINT__
#define __FIXED_POINT__

#include <limits>
#include <cmath>
#include <type_traits>
#include <cerrno>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/constant.hpp"
#include "lts5/utils/ordered_field_operator.hpp"
#include "lts5/utils/unit_step_operator.hpp"
#include "lts5/utils/shift_operator.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   FPoint
 * @brief   Basic fixed point algebra
 * @author  Christophe Ecabert
 * @date    29/11/2016
 * @tparam B    Base type, must be an integer type. If signed the fixed point
 *              will behave signed to
 * @tparam I    Integer part bit count
 * @tparam F    Fractional part bit count
 * @ingroup utils
 */
template<typename B,
         unsigned char I,
         unsigned char F = std::numeric_limits<B>::digits - I>
class LTS5_EXPORTS FPoint : public OrderedFieldOperator<FPoint<B, I, F>>,
                            public UnitStepOperator<FPoint<B, I, F>>,
                            public ShiftOperator<FPoint<B, I, F>, size_t> {
 public :


#pragma mark -
#pragma mark Sanity check

  // Only integer types qualify for base type. If this line triggers an error,
  // the base type is not an integer type.
  static_assert(std::is_integral<B>::value,
                "Base type is not an integral type");

  // Make sure that the bit counts are ok. If this line triggers an error, the
  // sum of the bit counts for the fractional and integer parts do not match
  // the bit count provided by the base type. The sign bit does not count.
  static_assert(I + F == std::numeric_limits<B>::digits,
                "Bit count does not match");

#pragma mark -
#pragma mark Type definition

  /**
   * @struct    Pow2
   * @brief     Compute power of 2 at the compilation time
   * @see       http://stackoverflow.com/questions/27270541/
   * @tparam P  Power needed
   * @tparam T  Dummy type to make gcc happy
   */
  template<int P, typename T = void>
  struct Pow2 {
    /** Power of two value */
    static const long long value = 2 * Pow2<P-1, T>::value;
  };

  /**
   * @struct    Pow2
   * @brief     Compute power of 2 at the compilation time
   * @tparam T  Dummy type to make compiler happy
   */
  template<typename T>
  struct Pow2<0, T> {
    /** Power of two value */
    static const long long value = 1;
  };

  /** Base type */
  typedef B base_type;

  /** Integer part bit count */
  static const unsigned char integer_bit_count = I;

  /** Fractional part bit count */
  static const unsigned char fractional_bit_count = F;

  /**
   * Grant the numeric_limits specialization for this fixed_point class
   * access to private members.
   */
  friend class std::numeric_limits<FPoint<B, I, F>>;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  FPoint
   * @fn    FPoint(void)
   * @brief Empty constructor
   */
  FPoint(void) {}

  /**
   * @name  FPoint
   * @fn    template<typename T> FPoint(const T value)
   * @brief Converting constructor
   * @tparam T      Type to convert to fixed point
   * @param[in] value   Value to convert to fixed point
   */
  template<typename T>
  FPoint(const T value) : value_((B)value << F) {
    assert(std::is_integral<T>::value);
  }

  /**
   * @name  FPoint
   * @fn    FPoint(const float value)
   * @brief Converting constructor
   * @param[in] value   Value to convert to fixed point
   */
  FPoint(const float value) : value_((B)(value * Pow2<F>::value +
                                         (value >= 0 ? .5 : -.5))) {}

  /**
   * @name  FPoint
   * @fn    FPoint(const double value)
   * @brief Converting constructor
   * @param[in] value   Value to convert to fixed point
   */
  FPoint(const double value) : value_((B)(value * Pow2<F>::value +
                                          (value >= 0 ? .5 : -.5))) {}

  /**
   * @name  FPoint
   * @fn    FPoint(const FPoint& other)
   * @brief Copy constructor
   * @param[in] other   Object to copy from
   */
  FPoint(const FPoint& other) : value_(other.value_) {}

  /**
   * @name  FPoint
   * @fn    FPoint(const FPoint<B, I2, F2>& other)
   * @tparam F2 New fraction part size
   * @tparam I2 New integer part size
   * @brief Copy constructor
   * @param[in] other   Object to copy from
   */
  template<unsigned char I2, unsigned char F2>
  FPoint(const FPoint<B, I2, F2>& other) : value_(other.value_) {
    if (I-I2 > 0) {
      this->value_ >>= (I - I2);
    }
    if (I2-I > 0) {
      this->value_ <<= (I2 - I);
    }
  }

  /**
   * @name  operator=
   * @fn    FPoint& operator=(const FPoint& rhs)
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  FPoint& operator=(const FPoint& rhs) {
    if (this != &rhs) {
      this->value_ = rhs.value_;
    }
    return *this;
  }

  /**
   * @name  operator=
   * @fn    FPoint& operator=(const FPoint<B, I2, F2>& rhs)
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  template<unsigned char I2, unsigned char F2>
  FPoint& operator=(const FPoint<B, I2, F2>& rhs) {
    if (I-I2 > 0) {
      this->value_ = (rhs.value_ >> (I - I2));
    }
    if (I2-I > 0) {
      this->value_ = (rhs.value_ << (I2 - I));
    }
    return *this;
  }

#pragma mark -
#pragma mark Operator

  /**
   * @name  operator<
   * @fn    bool operator<(const FPoint& rhs) const
   * @brief Less than operator
   * @param[in] rhs Right Hand sign
   * @return    True if less, false otherwise
   */
  bool operator<(const FPoint& rhs) const {
    return this->value_ < rhs.value_;
  }

  /**
   * @name  operator==
   * @fn    bool operator==(const FPoint& rhs) const
   * @brief Equality operator
   * @param[in] rhs Right Hand sign
   * @return    True if equal, false otherwise
   */
  bool operator==(const FPoint& rhs) const {
    return this->value_ == rhs.value_;
  }

  /**
   * @name  operator!()
   * @fn    bool operator!() const
   * @brief Negation operator
   * @return    True if equal to zero, false otherwise
   */
  bool operator!() const {
    return this->value_ == 0;
  }

  /**
   * @name  operator-()
   * @fn    FPoint operator-() const
   * @brief Unary minux operator
   * @return    The negative value
   */
  FPoint operator-() const {
    FPoint res;
    res.value_ = -this->value_;
    return res;
  }

  /**
   * @name  operator++
   * @fn    FPoint& operator++()
   * @brief Prefix increment
   * @return    Incremented value
   */
  FPoint& operator++() {
    this->value_ += Pow2<F>::value;
    return *this;
  }

  /**
   * @name  operator--
   * @fn    FPoint& operator--()
   * @brief Prefix decrement
   * @return    decremented value
   */
  FPoint& operator--() {
    this->value_ -= Pow2<F>::value;
    return *this;
  }

  /**
   * @name  operator+=
   * @fn    FPoint& operator+=(const FPoint& rhs)
   * @brief Addition operator
   * @param[in] rhs Right hand sign
   * @return    Summed number
   */
  FPoint& operator+=(const FPoint& rhs) {
    this->value_ += rhs.value_;
    return *this;
  }

  /**
   * @name  operator-=
   * @fn    FPoint& operator-=(const FPoint& rhs)
   * @brief Subtraction operator
   * @param[in] rhs Right hand sign
   * @return    Subtract number
   */
  FPoint& operator-=(const FPoint& rhs) {
    this->value_ -= rhs.value_;
    return *this;
  }

  /**
   * @name  operator*=
   * @fn    FPoint& operator*=(const FPoint& rhs)
   * @brief Multiplication
   * @param[in] rhs Right hand sign
   * @return    Multiplied fixed point
   */
  FPoint& operator*=(const FPoint& rhs) {
    using CastType = typename FPoint::template promote_type<B>::type;
    this->value_ = (static_cast<CastType>(this->value_) * rhs.value_ + Pow2<F-1>::value) >> F;
    return *this;
  }

  /**
   * @name  operator/=
   * @fn    FPoint& operator/=(const FPoint& rhs)
   * @brief Division
   * @param[in] rhs Right hand sign
   * @return    Divided fixed point
   */
  FPoint& operator/=(const FPoint& rhs) {
    using CastType = typename FPoint::template promote_type<B>::type;
    this->value_ = (static_cast<CastType>(this->value_) << F) / rhs.value_;
    return *this;
  }

  /**
   * @name  operator>>=
   * @fn    FPoint& operator>>=(const size_t shift)
   * @brief Shift right
   * @param[in] shift   Amount of shift
   * @return    Shifted number
   */
  FPoint& operator>>=(const size_t shift) {
    this->value_ >>= shift;
    return *this;
  }

  /**
   * @name  operator<<=
   * @fn    FPoint& operator<<=(const size_t shift)
   * @brief Left right
   * @param[in] shift   Amount of shift
   * @return    Shifted number
   */
  FPoint& operator<<=(const size_t shift) {
    this->value_ <<= shift;
    return *this;
  }

  /**
   * @name  char
   * @fn    operator char() const
   * @brief Convert to char
   * @return    Converted value
   */
  operator char() const {
    return static_cast<char>(this->value_ >> F);
  }

  /**
   * @name  signed char
   * @fn    operator signed char() const
   * @brief Convert to signed char
   * @return    Converted value
   */
  operator signed char() const {
    return static_cast<signed char>(this->value_ >> F);
  }

  /**
   * @name  unsigned char
   * @fn    operator unsigned char() const
   * @brief Convert to unsigned char
   * @return    Converted value
   */
  operator unsigned char() const {
    return static_cast<unsigned char>(this->value_ >> F);
  }

  /**
   * @name  short
   * @fn    operator short() const
   * @brief Convert to short
   * @return    Converted value
   */
  operator short() const {
    return static_cast<short>(this->value_ >> F);
  }

  /**
   * @name  unsigned short
   * @fn    operator unsigned short() const
   * @brief Convert to unsigned short
   * @return    Converted value
   */
  operator unsigned short() const {
    return static_cast<unsigned short>(this->value_ >> F);
  }

  /**
   * @name  int
   * @fn    operator int() const
   * @brief Convert to int
   * @return    Converted value
   */
  operator int() const {
    return static_cast<int>(this->value_ >> F);
  }

  /**
   * @name  unsigned int
   * @fn    operator unsigned int() const
   * @brief Convert to unsigned int
   * @return    Converted value
   */
  operator unsigned int() const {
    return static_cast<unsigned int>(this->value_ >> F);
  }

  /**
   * @name  long
   * @fn    operator long() const
   * @brief Convert to long
   * @return    Converted value
   */
  operator long() const {
    return static_cast<long>(this->value_ >> F);
  }

  /**
   * @name  unsigned long
   * @fn    operator unsigned long() const
   * @brief Convert to unsigned long
   * @return    Converted value
   */
  operator unsigned long() const {
    return static_cast<unsigned long>(this->value_ >> F);
  }

  /**
   * @name  long long
   * @fn    operator long long() const
   * @brief Convert to long long
   * @return    Converted value
   */
  operator long long() const {
    return static_cast<long long>(this->value_ >> F);
  }

  /**
   * @name  unsigned long long
   * @fn    operator unsigned long long() const
   * @brief Convert to unsigned long long
   * @return    Converted value
   */
  operator unsigned long long() const {
    return static_cast<unsigned long long>(this->value_ >> F);
  }

  /**
   * @name  bool
   * @fn    operator bool() const
   * @brief Convert to bool
   * @return    Converted value
   */
  operator bool() const {
    return static_cast<bool>(this->value_);
  }

  /**
   * @name  float
   * @fn    operator float() const
   * @brief Convert to float
   * @return    Converted value
   */
  operator float() const {
    return static_cast<float>(this->value_) / Pow2<F>::value;
  }

  /**
   * @name  double
   * @fn    operator double() const
   * @brief Convert to double
   * @return    Converted value
   */
  operator double() const {
    return static_cast<double>(this->value_) / Pow2<F>::value;
  }

#pragma mark -
#pragma mark Operation

  /**
   * @name  abs
   * @fn    static FPoint abs(const FPoint& x)
   * @brief Compute the absolute value
   * @param[in] x Value to compute abs
   * @return    Absolute value
   */
  static FPoint abs(const FPoint& x) {
    return (x < FPoint(0) ? -x : x);
  }

  /**
   * @name  ceil
   * @fn    static FPoint ceil(const FPoint& x)
   * @brief Compute the smallest integral value not less than the \p x.
   * @param[in] x   Value to ceil
   * @return    ceil
   */
  static FPoint ceil(const FPoint& x) {
    FPoint res;
    res.value_ = x.value_ & ~(Pow2<F>::value - 1);
    return res + FPoint(x.value_ & (Pow2<F>::value-1) ? 1 : 0);
  }

  /**
   * @name  floor
   * @fn    static FPoint floor(const FPoint& x)
   * @brief Computes the largest integral value not greater than \p x
   * @param[in] x   Value to floor
   * @return    Floored value
   */
  static FPoint floor(const FPoint& x) {
    FPoint res;
    res.value_ = (x.value_ & ~(Pow2<F>::value - 1));
    return res;
  }

  /**
   * @name  fmod
   * @fn    static FPoint fmod(const FPoint& x, const FPoint& y)
   * @brief Computes the fixed point remainder of \p x / \p y.
   * @param[in] x   X
   * @param[in] y   Y
   * @return    modulo(x, y)
   */
  static FPoint fmod(const FPoint& x, const FPoint& y) {
    FPoint res;
    res.value_ = x.value_ % y.value_;
    return res;
  }

  /**
   * @name  sin
   * @fn    static FPoint sin(const FPoint& x)
   * @brief Calculates the sine.
   *        The algorithm uses a MacLaurin series expansion.
   * @param[in] x   X
   * @return    sine of \p x
   */
  static FPoint sin(const FPoint& x) {
    const FPoint pi_2 = FPoint(2.0 * Constants<double>::PI);
    const FPoint pi = FPoint(Constants<double>::PI);
    // Bring into [-Pi..Pi]
    FPoint x_ = fmod(x, pi_2);
    if (x_ > pi) {
      x_ -= pi_2;
    } else if (x_ < -pi) {
      x_ += pi_2;
    }
    // Compute sine with MacLaurin expension
    FPoint xx = x_ * x_;
    FPoint y = -xx * FPoint(0.00019841269841269841);// 1.0 / (2 * 3 * 4 * 5 * 6 * 7)
    y += FPoint(0.00833333333333333333); // 1. / (2 * 3 * 4 * 5)
    y *= xx;
    y -= FPoint(0.16666666666666666666); //1. / (2 * 3)
    y *= xx;
    y += FPoint(1);
    y *= x_;
    return y;
  }

  /**
   * @name  cos
   * @fn    static FPoint cos(const FPoint& x)
   * @brief Calculates the cosine.
   *        The algorithm uses a MacLaurin series expansion.
   * @param[in] x   X
   * @return    cosine of \p x
   */
  static FPoint cos(const FPoint& x) {
    const FPoint pi_2 = FPoint(2.0 * Constants<double>::PI);
    const FPoint pi = FPoint(Constants<double>::PI);
    // Bring into [-Pi..Pi]
    FPoint x_ = fmod(x, pi_2);
    if (x_ > pi) {
      x_ -= pi_2;
    } else if (x_ < -pi) {
      x_ += pi_2;
    }
    // Compute cosine with MacLaurin expension
    FPoint xx = x_ * x_;
    FPoint y = -xx * FPoint(0.00138888888888888888); // 1. / (2 * 3 * 4 * 5 * 6)
    y += FPoint(0.04166666666666666666);  // 1. / (2 * 3 * 4)
    y *= xx;
    y -= FPoint(0.5); //1. / (2)
    y *= xx;
    y += FPoint(1);
    return y;
  }

  /**
   * @name  exp
   * @fn    static FPoint exp(const FPoint& x)
   * @brief Computes the exponential function of x. The algorithm uses
   *        the identity e^(a+b) = e^a * e^b
   * @param[in] x   Exponent
   * @return    e^x
   */
  static FPoint exp(const FPoint& x) {
    // a[x] = exp( (1/2) ^ x ), x: [0 ... 31]
    static const FPoint a[] = {1.64872127070012814684865078781,
                               1.28402541668774148407342056806,
                               1.13314845306682631682900722781,
                               1.06449445891785942956339059464,
                               1.03174340749910267093874781528,
                               1.01574770858668574745853507208,
                               1.00784309720644797769345355976,
                               1.00391388933834757344360960390,
                               1.00195503359100281204651889805,
                               1.00097703949241653524284529261,
                               1.00048840047869447312617362381,
                               1.00024417042974785493700523392,
                               1.00012207776338377107650351967,
                               1.00006103701893304542177912060,
                               1.00003051804379102429545128481,
                               1.00001525890547841394814004262,
                               1.00000762942363515447174318433,
                               1.00000381470454159186605078771,
                               1.00000190735045180306002872525,
                               1.00000095367477115374544678825,
                               1.00000047683727188998079165439,
                               1.00000023841860752327418915867,
                               1.00000011920929665620888994533,
                               1.00000005960464655174749969329,
                               1.00000002980232283178452676169,
                               1.00000001490116130486995926397,
                               1.00000000745058062467940380956,
                               1.00000000372529030540080797502,
                               1.00000000186264515096568050830,
                               1.00000000093132257504915938475,
                               1.00000000046566128741615947508};
    static const FPoint e(2.718281828459045);
    FPoint y(1);
    for (int i = F-1; i >= 0; --i) {
      if (x.value_ & (0x01 << i)) {
        y *= a[F - i - 1];
      }
    }
    int x_int = static_cast<int>(FPoint::floor(x));
    if (x_int < 0) {
      for (int i = 1; i <= -x_int ; ++i) {
        y /= e;
      }
    } else {
      for (int i = 1; i <= x_int ; ++i) {
        y *= e;
      }
    }
    return y;
  }

  /**
   * @name  sqrt
   * @fn    static FPoint sqrt(const FPoint& x)
   * @brief Computes the nonnegative square root of its argument. A domain
   *        error results if the argument is negative.
   *        The algorithm seems to have originated in a book on programming
   *        abaci by Mr C. Woo.
   * @param[in] x   Value to take the square root
   * @return    sqrt(x) if x > 0, 0 otherwise
   */
  static FPoint sqrt(const FPoint& x) {
    // Check boundary
    if (x < FPoint(0)) {
      errno = EDOM;
      return 0;
    }
    typedef typename FPoint::template promote_type<B>::type p_type;
    p_type op = static_cast<p_type>(x.value_) << F;
    p_type res = 0;
    p_type one = (p_type)1 << (std::numeric_limits<p_type>::digits - 1);

    while(one > op) {
      one >>= 2;
    }

    while (one != 0) {
      if (op >= res + one) {
        op = op - (res + one);
        res = res + (one << 1);
      }
      res >>= 1;
      one >>= 2;
    }

    FPoint root;
    root.value_ = static_cast<B>(res);
    return root;
  }

#pragma mark -
#pragma mark Private
 private:

  /** Actual number */
  B value_;

  /**
   * Multiplication and division of fixed_point numbers need type
   * promotion.
   *
   * When two 8 bit numbers are multiplied, a 16 bit result is produced.
   * When two 16 bit numbers are multiplied, a 32 bit result is produced.
   * When two 32 bit numbers are multiplied, a 64 bit result is produced.
   * Since the fixed_point class internally relies on integer
   * multiplication, we need type promotion. After the multiplication we
   * need to adjust the position of the decimal point by shifting the
   * temporary result to the right an appropriate number of bits.
   * However, if the temporary multiplication result is not kept in a big
   * enough variable, overflow errors will occur and lead to wrong
   * results. A similar promotion needs to be done to the divisor in the
   * case of division, but here the divisor needs to be shifted to the
   * left an appropriate number of bits.
   *
   * Unfortunately the integral_promotion class of the boost type_traits
   * library could not be used, since it does not provide a promotion
   * from int/unsigned int (32 bit) to long long/unsigned long long
   * (64 bit). However, this promotion is often needed, because it is
   * quite common to use a 32 bit base type for the fixed_point type.
   */
  template<typename T, typename U = void>
  struct promote_type {
  };

  // Promote signed char to signed short
  template<typename U>
  struct promote_type<signed char, U> {
    typedef signed short type;
  };

  // Promote unsigned char to unsigned short
  template<typename U>
  struct promote_type<unsigned char, U> {
    typedef unsigned short type;
  };

  // Promote signed short to signed int
  template<typename U>
  struct promote_type<signed short, U> {
    typedef signed int type;
  };

  // Promote unsigned short to unsigned int
  template<typename U>
  struct promote_type<unsigned short, U> {
    typedef unsigned int type;
  };

  // Promote signed int to signed long long
  template<typename U>
  struct promote_type<signed int, U> {
    typedef signed long long type;
  };

  // Promote unsigned int to unsigned int
  template<typename U>
  struct promote_type<unsigned int, U> {
    typedef unsigned long long type;
  };
};
}  // namepsace LTS5

#pragma mark -
#pragma mark Extension : numeric_limits<FPoint<B, I, F>>
/**
 * @namespace   std
 * @brief       std lib namespace, where we are allowed to extend
 *              numeric_limits<T>
 */
namespace std {

/**
 * @class       numeric_limits
 * @brief       Extend numeric_limits for FPoint<B, I, F> type
 * @author      Christophe Ecabert
 * @date        30/11/16
 * @tparam B    Base type
 * @tparam I    Integer bit number
 * @tparam F    Fraction bit number
 */
template<typename B, unsigned char I, unsigned char F>
class numeric_limits<LTS5::FPoint<B, I, F>> {
 public:
  // Type for this specialization
  typedef LTS5::FPoint<B, I, F> fp_type;

  /**
   * Tests whether a type allows denormalized values.
   * An enumeration value of type const float_denorm_style, indicating
   * whether the type allows denormalized values. The fixed_point class does
   * not have denormalized values.
   * The member is always set to denorm_absent.
   */
  static const float_denorm_style has_denorm = denorm_absent;

  /**
   * Tests whether loss of accuracy is detected as a denormalization loss
   * rather than as an inexact result.
   *
   * The fixed_point class does not have denormalized values.
   *
   * The member is always set to false.
   */
  static const bool has_denorm_loss = false;

  /**
   * Tests whether a type has a representation for positive infinity.
   *
   * The fixed_point class does not have a representation for positive
   * infinity.
   *
   * The member is always set to false.
   */
  static const bool has_infinity = false;

  /**
   * Tests whether a type has a representation for a quiet not a number
   * (NAN), which is nonsignaling.
   *
   * The fixed_point class does not have a quiet NAN.
   *
   * The member is always set to false.
   */
  static const bool has_quiet_NaN = false;

  /**
   * Tests whether a type has a representation for signaling not a number
   * (NAN).
   *
   * The fixed_point class does not have a signaling NAN.
   *
   * The member is always set to false.
   */
  static const bool has_signaling_NaN = false;

  /**
   * Tests if the set of values that a type may represent is finite.
   *
   * The fixed_point type has a bounded set of representable values.
   *
   * The member is always set to true.
   */
  static const bool is_bounded = true;

  /**
   * Tests if the calculations done on a type are free of rounding errors.
   *
   * The fixed_point type is considered exact.
   *
   * The member is always set to true.
   */
  static const bool is_exact = true;

  /**
   * Tests if a type conforms to IEC 559 standards.
   *
   * The fixed_point type does not conform to IEC 559 standards.
   *
   * The member is always set to false.
   */
  static const bool is_iec559 = false;

  /**
   * Tests if a type has an integer representation.
   *
   * The fixed_point type behaves like a real number and thus has not
   * integer representation.
   *
   * The member is always set to false.
   */
  static const bool is_integer = false;

  /**
   * Tests if a type has a modulo representation.
   *
   * A modulo representation is a representation where all results are
   * reduced modulo some value. The fixed_point class does not have a
   * modulo representation.
   *
   * The member is always set to false.
   */
  static const bool is_modulo = false;

  /**
   * Tests if a type has a signed representation.
   *
   * The member stores true for a type that has a signed representation,
   * which is the case for all fixed_point types with a signed base type.
   * Otherwise it stores false.
   */
  static const bool is_signed =
          std::numeric_limits<typename fp_type::base_type>::is_signed;

  /**
   * Tests if a type has an explicit specialization defined in the template
   * class numeric_limits.
   *
   * The fixed_point class has an explicit specialization.
   *
   * The member is always set to true.
   */
  static const bool is_specialized = true;

  /**
   * Tests whether a type can determine that a value is too small to
   * represent as a normalized value before rounding it.
   *
   * Types that can detect tinyness were included as an option with IEC 559
   * floating-point representations and its implementation can affect some
   * results.
   *
   * The member is always set to false.
   */
  static const bool tinyness_before = false;

  /** Tests whether trapping that reports on arithmetic exceptions is
   * implemented for a type.
   *
   * The member is always set to false.
   */
  static const bool traps = false;

  /**
   * Returns a value that describes the various methods that an
   * implementation can choose for rounding a real value to an integer
   * value.
   *
   * The member is always set to round_toward_zero.
   */
  static const float_round_style round_style = round_toward_zero;

  /**
   * Returns the number of radix digits that the type can represent without
   * loss of precision.
   *
   * The member stores the number of radix digits that the type can represent
   * without change.
   *
   * The member is set to the template parameter I (number of integer bits).
   */
  static const int digits = I;

  /** Returns the number of decimal digits that the type can represent without
   * loss of precision.
   *
   * The member is set to the number of decimal digits that the type can
   * represent.
   */
  static const int digits10 = (int)(digits * 301. / 1000. + .5);

  static const int max_exponent = 0;
  static const int max_exponent10 = 0;
  static const int min_exponent = 0;
  static const int min_exponent10 = 0;
  static const int radix = 0;

  /**
   * @name      min
   * @fn        static fp_type min(void)
   * @brief     The minimum value of this type.
   * @return    The minimum value representable with this type.
   */
  static fp_type min(void) {
    fp_type minimum;
    minimum.value_ = std::numeric_limits<typename fp_type::base_type>::min();
    return minimum;
  }

  /**
   * @name      max
   * @fn        static fp_type max(void)
   * @brief     The maximum value of this type.
   * @return    The maximum value representable with this type.
   */
  static fp_type max(void) {
    fp_type maximum;
    maximum.value_ = std::numeric_limits<typename fp_type::base_type>::max();
    return maximum;
  }

  /**
   * @name    epsilon
   * @fn      static fp_type epsilon(void)
   * @brief   The function returns the difference between 1 and the smallest
              value
   * @return  The smallest effective increment from 1.0.
   */
  static fp_type epsilon(void) {
    fp_type epsilon;
    epsilon.value_ = 1;
    return epsilon;
  }

  /**
   * @name  round_error
   * @fn    static fp_type round_error(void)
   * @brief The maximum rounding error for the fixed_point type is 0.5.
   * @return  Always returns 0.5
   */
  static fp_type round_error(void) {
    return static_cast<fp_type>(0.5);
  }

  /**
   * @name  denorm_min
   * @fn    static fp_type denorm_min(void)
   * @return Always return 0.
   */
  static fp_type denorm_min(void) {
    return static_cast<fp_type>(0);
  }

  /**
   * @name  infinity
   * @fn    static fp_type infinity(void)
   * @return Always return 0.
   */
  static fp_type infinity(void) {
    return static_cast<fp_type>(0);
  }

  /**
   * @name  quiet_NaN
   * @fn    static fp_type quiet_NaN(void)
   * @return Always return 0.
   */
  static fp_type quiet_NaN(void) {
    return static_cast<fp_type>(0);
  }

  /**
   * @name  signaling_NaN
   * @fn    static fp_type signaling_NaN(void)
   * @return Always return 0.
   */
  static fp_type signaling_NaN(void) {
    return static_cast<fp_type>(0);
  }
};
}   // namepsace STD

#endif //__FIXED_POINT__
