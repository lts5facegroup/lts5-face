/**
 *  @file   linear_algebra_wrapper.hpp
 *  @brief  Common linear algebra wrapper between OpenCV and
 *          BLAS+Lapack library
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   30/04/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_LINEAR_ALGEBRA_WRAPPER__
#define __LTS5_LINEAR_ALGEBRA_WRAPPER__

#include "opencv2/core/core.hpp"
#include "lts5/utils/library_export.hpp"

namespace Eigen {
template<typename T, int, int, int, int, int>
class Matrix;
}

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   LinearAlgebra
 * @brief   Common linear algebra wrapper between OpenCV and
 *          BLAS+Lapack library
 * @author  Christophe Ecabert
 * @date    30/04/15
 * @ingroup utils
 */
template<typename T>
class LTS5_EXPORTS LinearAlgebra {

 public:

  /** Matrix type, -1 == Dynamic */
  using Matrix = Eigen::Matrix<T, -1, -1, 0, -1, -1>;
  /** Vector type */
  using Vector = Eigen::Matrix<T, -1, 1, 0, -1, 1>;

  /**
   * @enum  TransposeType
   * @brief List of operation on matrix
   */
  enum TransposeType {
    /** No transpose */
    kNoTranspose = 111,
    /** Transpose */
    kTranspose = 112,
    /** Conjugate transpose */
    kConjTranspose = 113,
    /** Conjugate transpose */
    kConjNoTranspose = 114
  };

  /**
   *  @name L2Norm
   *  @fn T L2Norm(const cv::Mat& vector)
   *  @brief  Compute the L2 norm of a vector
   *  @param[in]  vector  Vector to compute the norm
   *  @return L2 Norm
   */
  static T L2Norm(const cv::Mat& vector);

  /**
   *  @name Mean
   *  @fn static void Mean(const cv::Mat& matrix,
                            const int& direction, cv::Mat* mean)
   *  @brief  Compute average value of a matrix in a given direction
   *  @param[in]  matrix  Data matrix
   *  @param[in]  direction 0 = row, other = column
   *  @param[out] mean  Average values
   */
  static void Mean(const cv::Mat& matrix, const int& direction, cv::Mat* mean);

  /**
   *  @name Copy
   *  @fn static void Copy(const cv::Mat& src, cv::Mat* dest)
   *  @brief  Copy \p src matrix into a \p dest matrix
   *  @param[in]  src  Source
   *  @param[in]  dest Destination
   */
  static void Copy(const cv::Mat& src, cv::Mat* dest);

  /**
   *  @name Axpy
   *  @fn void Axpy(const cv::Mat& A, const T alpha, cv::Mat* B)
   *  @brief  Compute : B := alpha * A + B
   *  @param[in]      A         Vector/Matrix A
   *  @param[in]      alpha     Alpha scaling factor
   *  @param[in,out]  B         Vector/Matrix B
   */
  static void Axpy(const cv::Mat& A,
                   const T alpha,
                   cv::Mat* B);

  /**
   *  @name Axpby
   *  @fn void Axpby(const int& N, const T* A, const T alpha, const T beta, T* B)
   *  @brief  Compute : B := alpha * A + beta * B
   *  @param[in]        N       Array dimension
   *  @param[in]        A       Array A
   *  @param[in]        alpha   Alpha scaling factor
   *  @param[in]        beta    Beta scaling factor
   *  @param[in,out]    B       Array B
   */
  static void Axpby(const int& N,
                    const T* A,
                    const T alpha,
                    const T beta,
                    T* B);

  /**
   *  @name Axpby
   *  @fn void Axpby(const cv::Mat& A, const T alpha,
   *                 const T beta, cv::Mat* B)
   *  @brief  Compute : B := alpha * A + beta * B
   *  @param[in]      A         Vector/Matrix A
   *  @param[in]      alpha     Alpha scaling factor
   *  @param[in]      beta      Beta scaling factor
   *  @param[in,out]  B  Vector/Matrix B
   */
  static void Axpby(const cv::Mat& A,
                    const T alpha,
                    const T beta,
                    cv::Mat* B);


  /**
   *  @name   Dot
   *  @fn T Dot(const cv::Mat& vector_a, const cv::Mat& vector_b)
   *  @brief  Compute the dot product between two vectors
   *  @param[in]  vector_a  Vector A
   *  @param[in]  vector_b  Vector B
   *  @return Dot product
   */
  static T Dot(const cv::Mat& vector_a, const cv::Mat& vector_b);

  /**
   *  @name   Dot
   *  @fn T Dot(const int& N,
                const T* A, const int& inc_a, const T* B, const int& inc_b)
   *  @brief  Compute the dot product between two vectors
   *  @param[in]  N   Vector dimensions
   *  @param[in]  A   Vector A
   *  @param[in]  inc_a     Stride within vector_A. For example, if inc_a
   is 7, every 7th element is used.
   *  @param[in]  B   Vector A
   *  @param[in]  inc_b     Stride within vector_B
   *  @return Dot product or -1.0 if error(s)
   */
  static T Dot(const int& N,
               const T* A, const int& inc_a,
               const T* B, const int& inc_b);

  /**
   *  @name   Dot
   *  @fn T Dot(const cv::Mat& vector_a,const int& inc_a,
          const cv::Mat& vector_b, const int& inc_b)
   *  @brief  Compute the dot product between two vectors
   *  @param[in]  vector_a  Vector A
   *  @param[in]  inc_a     Stride within vector_A. For example, if inc_a
                            is 7, every 7th element is used.
   *  @param[in]  vector_b  Vector B
   *  @param[in]  inc_b     Stride within vector_B
   *  @return Dot product or -1.0 if error(s)
   */
  static T Dot(const cv::Mat& vector_a,const int& inc_a,
               const cv::Mat& vector_b, const int& inc_b);

  /**
   *  @name   Gemv
   *  @fn void Gemv(const cv::Mat& matrix_a,
                    const TransposeType& trans_a,
                    const T& alpha,
                    const cv::Mat& vector_x,
                    const T& beta,
                    cv::Mat* vector_y)
   *  @brief  Multiplies a matrix by a vector
   *          Y = a * AX + b Y
   *  @param[in]      matrix_a  Matrix A
   *  @param[in]      trans_a   Indicate if A is transpose or not
   *  @param[in]      alpha     Scaling factor alpha
   *  @param[in]      vector_x  Vector X
   *  @param[in]      beta      Scaling factor beta
   *  @param[in,out]  vector_y  Output vector
   */
  static void Gemv(const cv::Mat& matrix_a,
                         const TransposeType& trans_a,
                         const T& alpha,
                         const cv::Mat& vector_x,
                         const T& beta,
                         cv::Mat* vector_y);

  /**
   *  @name   Gemv
   *  @fn void  Gemv(const Matrix& matrix_a,
                     const TransposeType& trans_a,
                     const T& alpha,
                     const Matrix& vector_x,
                     const T& beta,
                     Matrix* vector_y)
   *  @brief  Multiplies a matrix by a vector
   *          Y = a * AX + b Y
   *  @param[in]      matrix_a  Matrix A
   *  @param[in]      trans_a   Indicate if A is transpose or not
   *  @param[in]      alpha     Scaling factor alpha
   *  @param[in]      vector_x  Vector X
   *  @param[in]      beta      Scaling factor beta
   *  @param[in,out]  vector_y  Output vector
   */
  static void Gemv(const Matrix& matrix_a,
                   const TransposeType& trans_a,
                   const T& alpha,
                   const Matrix& vector_x,
                   const T& beta,
                   Matrix* vector_y);

  /**
   * @name  Sbmv
   * @fn    static void Sbmv(const cv::Mat& A, const T alpha, const cv::Mat& x,
                   const T beta, cv::Mat* y)
   * @brief Perform matrix-vector operation :  y := alpha*A*x + beta*y
   *        where alpha and beta are scalars, x and y are n element vectors and
   *        A is an n by n symmetric band matrix, with k super-diagonals.
   *        At the moment support only diagonal matrix
   *        i.e. element-wise vector multiplication
   * @param[in] A       Vector of element on the matrix's diagonal
   * @param[in] alpha   Scaling factor
   * @param[in] x       Vector
   * @param[in] beta    Scaling factor
   * @param[in, out] y  Output
   */
  static void Sbmv(const cv::Mat& A,
                   const T alpha,
                   const cv::Mat& x,
                   const T beta, cv::Mat* y);

  /**
   *  @name   Gemm
   *  @fn void Gemm(const cv::Mat& matrix_a,
                    const TransposeType& trans_a,
                    const T& alpha,
                    const cv::Mat& matrix_b,
                    const TransposeType& trans_b,
                    const T& beta,
                    cv::Mat* matrix_c)
   *  @brief  Compute the product between two matrices
   *          C = a * AB + b * C
   *  @param[in]      matrix_a  Matrix A
   *  @param[in]      trans_a   Transpose flag indicator for A
   *  @param[in]      alpha     Alpha coefficient
   *  @param[in]      matrix_b  Matrix B
   *  @param[in]      trans_b   Transpose flag indicator for B
   *  @param[in]      beta      Beta coefficient
   *  @param[in,out]  matrix_c  Resulting matrix
   */
  static void Gemm(const cv::Mat& matrix_a,
                   const TransposeType& trans_a,
                   const T& alpha,
                   const cv::Mat& matrix_b,
                   const TransposeType& trans_b,
                   const T& beta,
                   cv::Mat* matrix_c);

  /**
   *  @name   Gemm
   *  @fn void Gemm(const TransposeType& trans_a,
                   const int& m,
                   const int& n,
                   const int& k,
                   const T& alpha,
                   const T* a,
                   const int& lda,
                   const TransposeType& trans_b,
                   const T& beta,
                   const T* b,
                   const int& ldb,
                   const int& ldc,
                   T* c)
   *  @brief  Compute the product between two matrices
   *          C = a * op(A) op(B) + b * C
   *  @param[in]     trans_a    Transpose flag indicator for A
   *  @param[in]     m          Number of rows in op(A)
   *  @param[in]     n          Number of cols in op(B)
   *  @param[in]     k          Number of cols in op(A)
   *  @param[in]     alpha      Alpha coefficient
   *  @param[in]     a          Matrix A (row major)
   *  @param[in]     lda        Leading direction in A
   *  @param[in]     trans_b    Transpose flag indicator for B
   *  @param[in]     beta       Beta coefficient
   *  @param[in]     b          Matrix B (row major)
   *  @param[in]     ldb        Leading direction in B
   *  @param[in]     ldc        Leading direction in C
   *  @param[in,out] c          Resulting matrix, matrix should already be
   *                            initialized with proper dimension (Not checked)
   */
  static void Gemm(const TransposeType& trans_a,
                   const int& m,
                   const int& n,
                   const int& k,
                   const T& alpha,
                   const T* a,
                   const int& lda,
                   const TransposeType& trans_b,
                   const T& beta,
                   const T* b,
                   const int& ldb,
                   const int& ldc,
                   T* c);

  /**
   *  @name   Gemm
   *  @fn void Gemm(const Matrix& matrix_a,
                   const TransposeType& trans_a,
                   const T& alpha,
                   const Matrix& matrix_b,
                   const TransposeType& trans_b,
                   const T& beta,
                   Matrix* matrix_c)
   *  @brief  Compute the product between two matrices
   *          C = a * AB + b * C
   *  @param[in]      matrix_a  Matrix A
   *  @param[in]      trans_a   Transpose flag indicator for A
   *  @param[in]      alpha     Alpha coefficient
   *  @param[in]      matrix_b  Matrix B
   *  @param[in]      trans_b   Transpose flag indicator for B
   *  @param[in]      beta      Beta coefficient
   *  @param[in,out]  matrix_c  Resulting matrix
   */
  static void Gemm(const Matrix& matrix_a,
                   const TransposeType& trans_a,
                   const T& alpha,
                   const Matrix& matrix_b,
                   const TransposeType& trans_b,
                   const T& beta,
                   Matrix* matrix_c);

  /**
   *  @name   Ger
   *  @fn void Ger(const cv::Mat& vec_x,
                    const cv::Mat& vec_y,
                    const T alpha,
                    cv::Mat* matrix_a)
   *  @brief  Compute the multiplication between two vector and add it to a
   *          matrix :
   *          A = a * x * y' + A
   *  @param[in]      vec_x
   *  @param[in]      vec_y
   *  @param[in]      alpha
   *  @param[in,out]  matrix_a
   */
  static void Ger(const cv::Mat& vec_x,
                  const cv::Mat& vec_y,
                  const T alpha,cv::Mat* matrix_a);

  /**
   *  @name InverseSymMatrix
   *  @fn int InverseMatrix(const Matrix& matrix, Matrix* inv_matrix)
   *  @brief  Compute the inverse of a real \p matrix using LU Decomposition
   *          method
   *  @param[in]  matrix      Real matrix
   *  @param[out] inv_matrix  Inverse of \p matrix_a
   *  @return 0 on sucess, error code otherwise
   */
  static int InverseMatrix(const Matrix& matrix, Matrix* inv_matrix);

  /**
   *  @name InverseSymMatrix
   *  @fn int InverseSymMatrix(const Matrix& matrix_a, Matrix* inv_matrix_a)
   *  @brief  Compute the inverse of a real symmetric \p matrix_a using the
   *          Bunch-Kaufman diagonal pivoting method
   *  @param[in]  matrix_a      Real symmetric matrix
   *                            in the process
   *  @param[out] inv_matrix_a  Inverse of \p matrix_a
   *  @return 0 on sucess, error code otherwise
   */
  static int InverseSymMatrix(const Matrix& matrix_a, Matrix* inv_matrix_a);

  /**
   *  @name InverseSymPosMatrix
   *  @fn int InverseSymPosMatrix(const Matrix& matrix_a, Matrix* inv_matrix_a)
   *  @brief  Compute the inverse of a real symmetric positive definite
   *          \p matrix_a using Cholesky Factorization.
   *  @param[in]  matrix_a      Real symmetric positive definite matrix
   *  @param[out] inv_matrix_a  Inverse of \p matrix_a
   *  @return 0 on sucess, error code otherwise
   */
  static int InverseSymPosMatrix(const Matrix& matrix_a, Matrix* inv_matrix_a);

  /**
   *  @class  Lapack
   *  @brief  Subspace for lapack call
   */
  class LTS5_EXPORTS Lapack {
   public:

    /**
     * @name    MachinePrecisionCall
     * @brief   Determines precision machine parameters.
     * @param   type    Type of precision
     * @return  value
     */
    static T MachinePrecisionCall(char* type);

    /**
     *  @struct   sym_eigen_params
     *  @brief  Parameters for symmetric eigen values/vectors decomposition
     */
    struct sym_eigen_params {
      /** Type of computation, eig_val + eig_vect | eig_val */
      char kJobz;
      /** Range of computation */
      char kRange;
      /** Input type */
      char kUplo;
      /** Order of the matrix */
      int kN;
      /** In : Symmetric matrix */
      T* kA;
      /** Leading direction of A */
      int kLda;
      /** the lower and upper  bounds  of  the interval  to be searched
       for eigenvalues (If range='V') */
      T kVl;
      T kVu;
      /** If RANGE='I', the indices (in ascending order)  of the   smallest
       and  largest  eigenvalues  to  be returned.*/
      int kIl;
      int kIu;
      /** The absolute error tolerance for the  eigenvalues */
      T kAbstol;
      /** Number of eigenvalue found */
      int kM;
      /** First m eigenvalues */
      T* kEigValues;
      /** First m eigenvectors */
      T* kEigVectors;
      /** Leading direction of k_eig_vectors */
      int kLdz;
      /** Internal workspace */
      T* kWork;
      /** Size of the workspace */
      int kLwork;
      int* kIwork;
      /** Index of eigenvector that fail to converge */
      int* kIfail;
      /** Computation information */
      int kInfo;

      /**
       *  @name   sym_eigen_params
       *  @fn sym_eigen_params()
       *  @brief  Constructor
       */
      sym_eigen_params() : kA(nullptr), kVl(0.0), kVu(0.0),
                            kEigValues(nullptr), kEigVectors(nullptr),
                            kWork(nullptr),kIwork(nullptr),kIfail(nullptr){
      }

      /**
       *  @name Print
       *  @fn void Print()
       *  @brief  Output the parameters values
       */
      void Print();
    };

    /**
     *  @name   SymEigenDecompositionCall
     *  @fn inline void SymEigenDecompositionCall(sym_eigen_params& p)
     *  @brief  Interface for lapack function call - Undefined type
     *  @throw  std::runtime_error()
     */
    static void SymEigenDecompositionCall(sym_eigen_params& p);

    /**
     *  @struct   n_sym_eigen_params
     *  @brief  Parameters for non symmetric eigen values/vectors decomposition
     */
    struct n_sym_eigen_params {
      /** Left eigenvector not computed */
      char kJobvl = 'N';
      /** Right eigenvector COMPUTED */
      char kJobvr = 'V';
      /** Order of the matrix */
      int kN;
      /** Ptr to matrix data as COLUMN-MAJOR */
      T* kA;
      /** Leading direction of A */
      int kLda;
      /** Eigenvalue, real part (n) */
      T* kWr;
      /** Eigenvalue, img part  (n) */
      T* kWi;
      /** Leading direction of left eigenvector (not used here !) */
      int kLdvl;
      /** Left eigenvectors (LDVL,N) */
      T* kVl;
      /** Leading direction of right eigenvector */
      int kLdvr;
      /** Left eigenvectors (LDVR,N) */
      T* kVr;
      /** Define work array dimension */
      int kLwork;
      /** Working array (LWORK >= 4*N) */
      T* kWork;
      /** Information about computation */
      int kInfo;

      /**
       *  @name n_sym_eigen_params
       *  @fn n_sym_eigen_params()
       *  @brief  Constructor
       */
      n_sym_eigen_params() {
        kWr = nullptr;
        kWi = nullptr;
        kVl = nullptr;
        kVr = nullptr;
        kWork = nullptr;
      }
    };

    /**
     *  @name   NSymEigenDecompositionCall
     *  @fn inline void NSymEigenDecompositionCall(n_sym_eigen_params& p)
     *  @brief  Interface for lapack function call - Undefined type
     *  @throw  std::runtime_error()
     */
    static void NSymEigenDecompositionCall(n_sym_eigen_params& p);

    /**
     *  @enum SVDType
     *  @brief  Type of SVD computation
     */
    enum SVDType {
      /** Full computation */
      kFullUV,
      /** ? */
      kStd
    };

    /**
     *  @struct svd_params
     *  @brief  SVD wrapper parameters
     */
    struct svd_params {
      /** Compute all/part left singular vectors */
      char kJobu;
      /** Compute all/part right singular vectors */
      char kJobvt;
      /** Number of rows */
      int kM;
      /** Numer of cols */
      int kN;
      /** Min(M,N) */
      int kMin;
      /** Leading direction of A matrix */
      int kLda;
      /** Matrix A */
      T* kA = 0;
      /** Singular values */
      T* kS = 0;
      /** Leading direction of U */
      int kLdu;
      /** Left singular vectors */
      T* kU = 0;
      /** Leading direction of Vt */
      int kLdvt;
      /** Vt data */
      T* kVt = 0;
      /** Work space query */
      int kLwork = -1;
      /** Work space */
      T* kWork = 0;
      /** Info */
      int kInfo = -1;
      /** Type of decomposition */
      SVDType kType;

      /**
       * @name  svd_params
       * @fn    svd_params()
       * @brief Constructor
       */
      svd_params() {
        kS = nullptr;
        kU = nullptr;
        kVt = nullptr;
        kWork = nullptr;
      }

      /**
       * @name  Print
       * @fn    void Print()
       * @brief Print SVD call configuration
       */
      void Print();
    };

    /**
     *  @name   SvdDecompositionCall
     *  @fn inline void SvdDecompositionCall(svd_params& p)
     *  @brief  Interface for lapack function call - Undefined type
     *  @throw  std::runtime_error()
     */
    static void SvdDecompositionCall(svd_params& p);

    /**
     * @struct dc_svd_params
     * @brief   Singular Value Decomposition parameters using Divide and Conquer
     *          algorithm
     */
    struct dc_svd_params {
      /** Type of computation, A, S */
      char kJobz;
      /** Number of rows */
      int kM;
       /** Number of columns */
      int  	kN;
      /** Min(M,N) */
      int kMin;
      /** A, data to decompose */
      T* kA;
      /** Leading direction of A, col major */
      int  	kLda;
      /** Array storing singular value on exit */
      T* kS;
      /** Array with left ortho */
      T* kU;
      /** Leading direction of U */
      int kLdu;
      /** Array with ritht transpose ortho */
      T* kVt;
      /** Leading direction of Vt */
      int kLdvt;
      /** workspace */
      T* kWork;
      /** Workspace dimension */
      int kLwork;
      /** index workspace */
      int* kIwork;
      /** Information */
      int kInfo;
      /** Type of decomposition */
      SVDType kType;

      /**
       * @name  dc_svd_params
       * @fn    dc_svd_params()
       * @brief Constructor
       */
      dc_svd_params() : kA(nullptr),
                            kS(nullptr),
                            kU(nullptr),
                            kVt(nullptr),
                            kWork(nullptr),
                            kIwork(nullptr),
                            kInfo(-1) {
      }

      /**
       * @name  ~dc_svd_params
       * @fn    ~dc_svd_params()
       * @brief Destructor
       */
      ~dc_svd_params() {
        delete[] kA;
        delete[] kU;
        delete[] kVt;
        delete[] kWork;
        delete[] kIwork;
      }

      /**
       *  @name Print
       *  @fn void Print()
       *  @brief  Output the parameters values
       */
      void Print();
    };

    /**
     *  @name   DCSvdDecompositionCall
     *  @fn inline void DCSvdDecompositionCall(dc_svd_params& p)
     *  @brief  Interface for lapack function call - Undefined type
     *  @throw  std::runtime_error()
     */
    static void DCSvdDecompositionCall(dc_svd_params& p);

    /**
     *  @struct lin_solver_params
     *  @brief  Structure including all parameters for linear solver Ax = B
     */
    struct lin_solver_params {
      /** Indicate if matrix A is transposed */
      char kTrans;
      /** Number of rows in matrix A */
      int kM;
      /** Number of cols in matrix A */
      int kN;
      /** Number of columns of the matrices B and X */
      int kNrhs;
      /** Matrix A data */
      T* kA;
      /** Leading direction of A */
      int kLda;
      /** Matrix B data */
      T* kB;
      /** Leading direction of B */
      int kLdb;
      /** Workspace */
      T* kWork;
      /** Workspace size */
      int kLwork;
      /** Info */
      int kInfo;
    };

    /**
     *  @name   LinearSolverCall
     *  @fn inline void LinearSolverCall(lin_solver_params& p)
     *  @brief  Interface for lapack function call - Undefined type
     *  @throw  std::runtime_error()
     */
    static void LinearSolverCall(lin_solver_params& p);

    /**
     * @struct  square_lin_solver_params
     * @brief   Structure including all parameters for linear solver Ax = B.
     *          A matrix must be square
     */
    struct square_lin_solver_params {
      /** Number of linear equation */
      int k_n;
      /** Number of right hand side, i.e. number of column in matrix B */
      int k_nrhs;
      /** Matrix A [N x N] */
      T* k_a;
      /** Leading direction in A */
      int k_lda;
      /** List of pivot indexes (i.e. permutation) [N x 1] */
      int* k_ipiv;
      /** Right hand side - Matrix B */
      T* k_b;
      /** Leading direction in B */
      int k_ldb;
      /** Info - output */
      int k_info;
    };

    /**
     *  @name   SquareLinearSolverCall
     *  @fn inline void SquareLinearSolverCall(lin_solver_params<T>& p)
     *  @brief  Interface for lapack function call - Undefined type
     *  @throw  std::runtime_error()
     */
    static void SquareLinearSolverCall(square_lin_solver_params& p);


    /**
     * @struct  qr_decomposition_params
     * @brief   Structure including all parameters for QR decomposition
     */
    struct qr_decomposition_params {
      /** Number of row in A */
      int m;
      /** Number of col in A */
      int n;
      /** Array storing matrix A */
      T* a;
      /** Leading dimension in A (i.e column major layout) */
      int lda;
      /** Scalar reflector array of size min(m, n) */
      T* tau;
      /** Workspace */
      T* work;
      /** Workspace dimension */
      int lwork;
      /** Info */
      int info;

      qr_decomposition_params() : m(0),
                                  n(0),
                                  a(nullptr),
                                  lda(0),
                                  tau(nullptr),
                                  work(nullptr),
                                  lwork(0),
                                  info(0) {}

      ~qr_decomposition_params() {
        this->Clear();
      }

      void Clear() {
        if (a != nullptr) {
          delete[] a;
          delete[] tau;
          delete[] work;
        }
      }
    };

    /**
     *  @name   QRDecompositionCall
     *  @fn static void QRDecompositionCall(qr_decomposition_params& p)
     *  @brief  Interface for lapack function call.
     */
    static void QRDecompositionCall(qr_decomposition_params& p);

    /**
     * @struct  qr_q_gen_params
     * @brief   Structure including all parameters to generate Q matrix from
     *  QR decomposition output.
     */
    struct qr_q_gen_params {
      /** Number of row in Q */
      int m;
      /** Number of col in Q */
      int n;
      /** Number of elementary reflector */
      int k;
      /** Output of QR decomp */
      T* a;
      /** Leading direction of A */
      int lda;
      /** Array of scalar factor of elementary reflector */
      T* tau;
      /** Workspace */
      T* work;
      /** Workspace size */
      int lwork;
      /** Info */
      int info;

      qr_q_gen_params() : m(0), n(0), k(0), a(nullptr), lda(0), tau(nullptr),
                          work(nullptr), lwork(0), info(0) {}

      ~qr_q_gen_params() {
        this->Clear();
      }
      void Clear() {
        if (a != nullptr) {
          delete[] a;
          delete[] tau;
          delete[] work;
        }
      }
    };

    /**
     *  @name   QRGenerateQCall
     *  @fn static void QRGenerateQCall(qr_q_gen_params& p)
     *  @brief  Interface for lapack function call.
     */
    static void QRGenerateQCall(qr_q_gen_params& p);

    /**
     * @struct  qr_q_prod_params
     * @brief   Structure including all parameters to multiply Q matrix from
     *  QR decomposition output with another matrix C.
     */
    struct qr_q_prod_params {
      /** Side, kind of product */
      char side;
      /** Transpose flag */
      char trans;
      /**  The number of rows of the matrix C */
      int m;
      /** The number of columns of the matrix C */
      int n;
      /** The number of elementary reflectors  */
      int k;
      /** matrix A */
      T* a;
      /** Leading direction of A */
      int lda;
      /** Array of scalar reflector */
      T* tau;
      /** Input / Output matrix */
      T* c;
      /** Leading direction of C */
      int ldc;
      /** Workspace */
      T* work;
      /** Workspace size */
      int lwork;
      /** info */
      int info;

      qr_q_prod_params() : side('L'),
                           trans('N'),
                           m(0),
                           n(0),
                           k(0),
                           a(nullptr),
                           lda(0),
                           tau(nullptr),
                           c(nullptr),
                           ldc(0),
                           work(nullptr),
                           lwork(0),
                           info(0) {}

      ~qr_q_prod_params() {
        this->Clear();
      }

      void Clear() {
        if (a != nullptr) {
          delete[] c;
          delete[] work;
        }
      }
    };

    /**
     *  @name   QRQProductCall
     *  @fn static void QRQProductCall(qr_q_prod_params& p)
     *  @brief  Interface for lapack function call.
     */
    static void QRQProductCall(qr_q_prod_params& p);
  };

  /**
   *  @struct SymEigenDecompositon
   *  @brief  Symmetric eigenvalue decomposition class
   */
  struct LTS5_EXPORTS SymEigenDecomposition {
    /** Internal decomposition parameters */
    typename Lapack::sym_eigen_params eig_params_;

    /**
     *  @name   SymEigenDecomposition
     *  @fn SymEigenDecomposition()
     *  @brief Constructor
     */
    SymEigenDecomposition();

    /**
     *  @name   ~SymEigenDecomposition
     *  @fn ~SymEigenDecomposition()
     *  @brief  Destructor
     */
    ~SymEigenDecomposition();

    /**
     *  @name   InitOnlyEigenvalueComputation
     *  @fn void InitOnlyEigenvalueComputation(const cv::Mat& matrix_a)
     *  @brief  Initialize internal structure for only eigenvalues computation
     *  @param[in]  matrix_a  Symmetric matrix
     */
    void InitOnlyEigenvalueComputation(const cv::Mat& matrix_a);

    /**
     *  @name InitFullComputation
     *  @fn void InitFullComputation(const cv::Mat& matrix_a)
     *  @brief  Initialize internal structure for full eigenvalues and
     *          eigenvectors decomposition
     *  @param  matrix_a  Symmetric matrix
     */
    void InitFullComputation(const cv::Mat& matrix_a);

    /**
     *  @name InitPartialComputation
     *  @fn void InitPartialComputation(const cv::Mat& matrix_a,
                                        const int& number_of_vectors)
     *  @brief  Initialize internal structure for partial eigenvalues and
     *          eigenvectors decomposition
     *  @param[in]  matrix_a          Symmetric matrix
     *  @param[in]  number_of_vectors How many eigenvectors to compute
     */
    void InitPartialComputation(const cv::Mat& matrix_a,
                                const int& number_of_vectors);

    /**
     *  @name Compute
     *  @fn inline void Compute()
     *  @brief  Call lapack library for decompostion
     */
    void Compute();

    /**
     *  @name   GetEigenvalues
     *  @fn inline int GetEigenvalues(cv::Mat* eigenvalues)
     *  @brief  Provide access to eigen values
     *  @param[out] eigenvalues in decreasing order
     *  @return -1 if error, 0 otherwise
     */
    int GetEigenvalues(cv::Mat* eigenvalues);

    /**
     *  @name   GetEigenvectors
     *  @fn inline int GetEigenvectors(cv::Mat* eigenvectors)
     *  @brief  Provide access to eigen vectors
     *  @param[out] eigenvectors Eigen vector in decreasing order by row
     *  @return -1 if error, 0 otherwise
     */
    int GetEigenvectors(cv::Mat* eigenvectors);
  };

  /**
   *  @struct NSymEigenDecompositon
   *  @brief  Non Symmetric eigenvalue decomposition class
   */
  struct LTS5_EXPORTS NSymEigenDecompositon {
    /** Internal decomposition parameters */
    typename Lapack::n_sym_eigen_params eig_params_;

    /**
     *  @name   NSymEigenDecompositon
     *  @fn NSymEigenDecompositon()
     *  @brief Constructor
     */
    NSymEigenDecompositon() = default;

    /**
     *  @name   ~NSymEigenDecompositon
     *  @fn ~NSymEigenDecompositon()
     *  @brief  Destructor
     */
    ~NSymEigenDecompositon();

    /**
     *  @name InitFullComputation
     *  @fn void InitFullComputation(const cv::Mat& matrix_a)
     *  @brief  Initialize internal structure for full eigenvalues and
     *          eigenvectors decomposition
     *  @param  matrix_a  Symmetric matrix
     */
    void InitFullComputation(const cv::Mat& matrix_a);

    /**
     *  @name Compute
     *  @fn inline void Compute()
     *  @brief  Call lapack library for decompostion
     */
    void Compute();

    /**
     *  @name   GetEigenvalues
     *  @fn inline int GetEigenvalues(cv::Mat* real_eigenvalues, cv::Mat* img_eigenvalues)
     *  @brief  Provide access to eigen values
     *  @param[out] real_eigenvalues real part
     *  @param[out] img_eigenvalues imaginary part
     *  @return -1 if error, 0 otherwise
     */
    int GetEigenvalues(cv::Mat* real_eigenvalues, cv::Mat* img_eigenvalues);

    /**
     *  @name   GetEigenvectors
     *  @fn inline int GetEigenvectors(cv::Mat* eigenvectors)
     *  @brief  Provide access to eigen vectors
     *  @param[out] eigenvectors Eigen vector in decreasing order by row
     *  @return -1 if error, 0 otherwise
     */
    int GetEigenvectors(cv::Mat* eigenvectors);
  };

  /**
   *  @struct SvdDecomposition
   *  @brief  Svd decomposition class
   */
  struct LTS5_EXPORTS SvdDecomposition {
    /** Internal decomposition parameters */
    typename Lapack::svd_params p_;

    /**
     *  @name   SvdDecomposition
     *  @fn SvdDecomposition()
     *  @brief Constructor
     */
    SvdDecomposition() = default;

    /**
     *  @name   ~SvdDecomposition
     *  @fn ~SvdDecomposition()
     *  @brief  Destructor
     */
    ~SvdDecomposition();

    /**
     *  @name Init
     *  @fn void Init(const cv::Mat& matrix, const typename Lapack::SVDType& type)
     *  @brief  Initialize internal structure for svd decomposition
     *  @param[in]  matrix  Matrix to decompose
     *  @param[in]  type    Type of decompostion
     */
    void Init(const cv::Mat& matrix, const typename Lapack::SVDType& type);

    /**
     *  @name Init
     *  @fn void Init(const Matrix& matrix, const typename Lapack::SVDType& type)
     *  @brief  Initialize internal structure for svd decomposition
     *  @param[in]  matrix  Matrix to decompose
     *  @param[in]  type    Type of decompostion
     */
    void Init(const Matrix& matrix, const typename Lapack::SVDType& type);

    /**
     * @name    Update
     * @fn  void Update(const cv::Mat& matrix)
     * @brief   Update data on which to compute decompisition
     * @param[in] matrix    New data to decompose (must have the same dim)
     */
    void Update(const cv::Mat& matrix);

    /**
     * @name    Update
     * @fn  void Update(const Matrix& matrix)
     * @brief   Update data on which to compute decompisition
     * @param[in] matrix    New data to decompose (must have the same dim)
     */
    void Update(const Matrix& matrix);

    /**
     *  @name Compute
     *  @fn inline void Compute()
     *  @brief  Call lapack library for decompostion
     */
    void Compute();

    /**
     * @name    Get
     * @fn  void Get(cv::Mat* U, cv::Mat* S, cv::Mat* Vt)
     * @brief   Provide SVD decomposition
     * @param[out] U    U
     * @param[out] S    Singular value
     * @param[out] Vt   V transpose
     */
    void Get(cv::Mat* U, cv::Mat* S, cv::Mat* Vt);

    /**
     * @name    Get
     * @fn  void Get(Matrix* U, Matrix* S, Matrix* Vt)
     * @brief   Provide SVD decomposition
     * @param[out] U    U
     * @param[out] S    Singular value
     * @param[out] Vt   V transpose
     */
    void Get(Matrix* U, Matrix* S, Matrix* Vt);

    /**
     * @name Print
     * @fn  void Print()
     * @brief Print parameters
     */
    void Print();
  };

  /**
   *  @struct DCSvdDecomposition
   *  @brief  Svd decomposition class (Divide & Conquer algorithm)
   */
  struct LTS5_EXPORTS DCSvdDecomposition {
    /** Parameters */
    typename Lapack::dc_svd_params p_;

    /**
     *  @name   DCSvdDecomposition
     *  @fn DCSvdDecomposition()
     *  @brief Constructor
     */
    DCSvdDecomposition() = default;

    /**
     *  @name   ~DCSvdDecomposition
     *  @fn ~DCSvdDecomposition()
     *  @brief  Destructor
     */
    ~DCSvdDecomposition();

    /**
     *  @name Init
     *  @fn void Init(const cv::Mat& matrix,
                      const typename Lapack::SVDType& type)
     *  @brief  Initialize internal structure for svd decomposition
     *  @param[in]  matrix  Matrix to decompose
     *  @param[in]  type    Type of decompostion
     */
    void Init(const cv::Mat& matrix, const typename Lapack::SVDType& type);

    /**
     *  @name Init
     *  @fn void Init(const Matrix& matrix,
                      const typename Lapack::SVDType& type)
     *  @brief  Initialize internal structure for svd decomposition
     *  @param[in]  matrix  Matrix to decompose
     *  @param[in]  type    Type of decompostion
     */
    void Init(const Matrix& matrix, const typename Lapack::SVDType& type);

    /**
     * @name    Update
     * @fn  void Update(const cv::Mat& matrix)
     * @brief   Update data on which to compute decompisition
     * @param[in] matrix    New data to decompose (must have the same dim)
     */
    void Update(const cv::Mat& matrix);

    /**
     * @name    Update
     * @fn  void Update(const Matrix& matrix)
     * @brief   Update data on which to compute decompisition
     * @param[in] matrix    New data to decompose (must have the same dim)
     */
    void Update(const Matrix& matrix);

    /**
     *  @name Compute
     *  @fn inline void Compute()
     *  @brief  Call lapack library for decompostion
     */
    void Compute();

    /**
     * @name    Get
     * @fn  void Get(cv::Mat* U, cv::Mat* S, cv::Mat* Vt)
     * @brief   Provide SVD decomposition
     * @param[out] U    U
     * @param[out] S    Singular value
     * @param[out] Vt   V transpose
     */
    void Get(cv::Mat* U, cv::Mat* S, cv::Mat* Vt);

    /**
     * @name    Get
     * @fn  void Get(Matrix* U, Matrix* S, Matrix* Vt)
     * @brief   Provide SVD decomposition
     * @param[out] U    U
     * @param[out] S    Singular value
     * @param[out] Vt   V transpose
     */
    void Get(Matrix* U, Matrix* S, Matrix* Vt);

    /**
     * @name Print
     * @fn  void Print()
     * @brief Print parameters
     */
    void Print();
  };

  /**
   *  @struct LinearSolver
   *  @brief  Linear solver class
   */
  struct LTS5_EXPORTS LinearSolver {
    typename Lapack::lin_solver_params ls_params_;

    /**
     *  @name   LinearSolver
     *  @fn LinearSolver()
     *  @brief  Constructor
     */
    LinearSolver();

    /**
     *  @name   ~LinearSolver
     *  @fn ~LinearSolver()
     *  @brief  Destructor
     */
    ~LinearSolver();

    /**
     *  @name Solve
     *  @fn void Solve(const cv::Mat& matrix_a,
                        const cv::Mat& matrix_b,
                        cv::Mat* matrix_x)
     *  @brief  Solve AX = B
     *  @param[in]  matrix_a  Matrix A
     *  @param[in]  matrix_b  Matrix B, stored in column
     *  @param[out] matrix_x  Solution
     */
    void Solve(const cv::Mat& matrix_a,
               const cv::Mat& matrix_b,
               cv::Mat* matrix_x);
  };

  /**
   *  @struct SquareLinearSolver
   *  @brief  Linear solver class for square system. Solve Ax = B using LU
   *          decomposition on A
   */
  struct LTS5_EXPORTS SquareLinearSolver {
    /** Solver configuration */
    typename Lapack::square_lin_solver_params p_;

    /**
     *  @name   SquareLinearSolver
     *  @fn SquareLinearSolver()
     *  @brief  Constructor
     */
    SquareLinearSolver();

    /**
     *  @name   ~SquareLinearSolver
     *  @fn ~SquareLinearSolver()
     *  @brief  Destructor
     */
    ~SquareLinearSolver();

    /**
     *  @name Solve
     *  @fn void Solve(const cv::Mat& matrix_a,
                        const cv::Mat& matrix_b,
                        cv::Mat* matrix_x)
     *  @brief  Solve AX = B
     *  @param[in]  matrix_a  Matrix A
     *  @param[in]  matrix_b  Matrix B, stored in column
     *  @param[out] matrix_x  Solution
     */
    void Solve(const cv::Mat& matrix_a,
               const cv::Mat& matrix_b,
               cv::Mat* matrix_x);
  };

  /**
   *  @class  QRDecomposition
   *  @brief  Computes a QR factorization of a real M-by-N matrix A:
   *          A = Q * ( R ),
   *                  ( 0 )
   */
  class LTS5_EXPORTS QRDecomposition {
   public:

    /**
     * @enum    MultType
     * @brief Type of multiplication
     */
    enum MultType {
      /** Pre-multiply: C * Q */
      kPre,
      /** Post-multiply:  Q * C */
      kPost
    };
    /**
     * @name    QRDecomposition
     * @fn  QRDecomposition() = default
     * @brief Constructor
     */
    QRDecomposition() = default;

    /**
     * @name    QRDecomposition
     * @fn  explicit QRDecomposition(const cv::Mat& A)
     * @brief Constructor and run computation directly
     */
    explicit QRDecomposition(const cv::Mat& A);

    /**
     * @name    Init
     * @fn  int Init(const cv::Mat& A)
     * @brief Initialize decomposition
     * @param[in] A
     * @return  -1 if error, 0 otherwise
     */
    int Init(const cv::Mat& A);

    /**
     * @name    Compute
     * @fn  int Compute()
     * @brief Run decomposition
     * @return -1 if error, 0 otherwise
     */
    int Compute();

    /**
     * @name    MultiplyBy
     * @fn  int MultiplyBy(MultType type, bool transpose, cv::Mat* C)
     * @brief   Perform matrix multiplication
     *  kPre: C := C Q or C := C Q'
     *  kPost C := Q C or C := Q' C
     * @param[in] type  Type of multiplication to perform.
     * @param[in] transpose If `true` matrix Q will be transposed.
     * @param[in, out] C    Matrix to pre-multiply
     * @return  -1 if error, 0 otherwise
     */
    int MultiplyBy(MultType type, bool transpose, cv::Mat* C);

    /**
     * @name    GetComponents
     * @fn  int GetComponents(cv::Mat* Q, cv::Mat* R)
     * @brief Compute full Q and R matrix. NOTE internal container will be
     *  overwritten by the computation of Q.
     * @param[in] full  If true, output full Q matrix of size MxM.
     * @param[out] Q    Orthogonal Q matrix (optional)
     * @param[out] R    Triangular R matrix (optional)
     * @return -1 if error, 0 otherwise
     */
    int GetComponents(bool full, cv::Mat* Q, cv::Mat* R);

   private:
    /** Parameters */
    typename Lapack::qr_decomposition_params qr_p_;
    typename Lapack::qr_q_gen_params q_gen_p_;
    typename Lapack::qr_q_prod_params q_prod_p_;
  };

};

} // namespace LTS5
#endif
