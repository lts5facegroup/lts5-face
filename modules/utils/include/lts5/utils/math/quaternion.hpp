/**
 *  @file   quaternion.hpp
 *  @brief  Quaternion representation
 *          Based on Irrlicht 3D Engine
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   01/12/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_quaternion__
#define __LTS5_quaternion__

#include <iostream>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/matrix.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  Quaternion
 *  @brief  Quaternion representation based on Irrlicht 3D engine
 *  @author Christophe Ecabert
 *  @date   01/12/15
 *  @see    http://irrlicht.sourceforge.net/docu/
 *  @ingroup utils
 */
template <class T>
class LTS5_EXPORTS Quaternion {
 public :

#pragma mark -
#pragma mark Initialization

  /**
   *  @name Quaternion
   *  @fn Quaternion()
   *  @brief  Constructor
   */
  Quaternion() : x_(0.0), y_(0.0), z_(0.0), w_(1.0) {}

  /**
   *  @name Quaternion
   *  @fn Quaternion(const T x, const T y, const T z, const T w)
   *  @brief  Constructor
   *  @param[in]  x   X Imaginary part
   *  @param[in]  y   Y Imaginary part
   *  @param[in]  z   Z Imaginary part
   *  @param[in]  w   W Real part
   */
  Quaternion(const T x, const T y, const T z, const T w) :
    x_(x), y_(y), z_(z), w_(w) {
  }

  /**
   *  @name Quaternion
   *  @fn Quaternion(const T x, const T y, const T z)
   *  @brief  Constructor with null real part.
   *  @param[in]  x   X Imaginary part
   *  @param[in]  y   Y Imaginary part
   *  @param[in]  z   Z Imaginary part
   */
  Quaternion(const T x, const T y, const T z) :
    x_(x), y_(y), z_(z), w_(0.0) {}

  /**
   * @name  Quaternion
   * @fn    Quaternion(const Vector3<T>& axis, const T angle)
   * @brief Constructor for a rotation of a specific \p angle around a
   *        given \p axis.
   * @param[in] axis    Rotation axis
   * @param[in] angle   Amount of rotation, in radians
   */
  Quaternion(const Vector3<T>& axis, const T angle) {
    const T half_angle = angle / T(2.0);
    w_ = std::cos(half_angle);
    const T axis_n = axis.Norm();
    const T scale = std::sin(half_angle) / axis_n;
    x_ = axis.x_ * scale;
    y_ = axis.y_ * scale;
    z_ = axis.z_ * scale;
  }

  /**
   *  @name Quaternion
   *  @fn Quaternion(const Quaternion<T>& other)
   *  @brief  Constructor
   *  @param[in]  other Other quaternion to copy from
   */
  Quaternion(const Quaternion<T>& other) {
    x_ = other.x_;
    y_ = other.y_;
    z_ = other.z_;
    w_ = other.w_;
  }

  /**
   *  @name ~Quaternion
   *  @fn ~Quaternion() = default
   *  @brief  Destructor
   */
  ~Quaternion() = default;

#pragma mark -
#pragma mark Operator

  /**
   *  @name operator=
   *  @fn Quaternion<T>& operator=(const Quaternion<T>& other)
   *  @brief  Assignment operator
   *  @param[in]  other Quaternion to assign from
   *  @return New quaternion
   */
  Quaternion<T>& operator=(const Quaternion<T>& other) {
    if (*this != other) {
      x_ = other.x_;
      y_ = other.y_;
      z_ = other.z_;
      w_ = other.w_;
    }
    return *this;
  }

  /**
   *  @name operator==
   *  @fn bool operator==(const Quaternion<T>& other) const
   *  @brief  Equal operator
   *  @param[in]  other Quaternion to compare for equality
   *  @return True if equal, false otherwise
   */
  bool operator==(const Quaternion<T>& other) const {
    return (x_ == other.x_ && y_ == other.y_ &&
            z_ == other.z_ && w_ == other.w_);
  }

  /**
   *  @name operator!=
   *  @fn bool operator!=(const Quaternion<T>& other) const
   *  @brief  Not equal operator
   *  @param[in]  other Quaternion to compare for inequality
   *  @return True if not equal, false otherwise
   */
  bool operator!=(const Quaternion<T>& other) const {
    return !(*this == other);
  }

  /**
   *  @name operator+
   *  @fn Quaternion<T> operator+(const Quaternion<T>& rhs) const
   *  @brief  Addition operator
   *  @param[in]  rhs Quaternion to add to the current one
   *  @return Addition result
   */
  Quaternion<T> operator+(const Quaternion<T>& rhs) const {
    return Quaternion<T>(x_ + rhs.x_,
                         y_ + rhs.y_,
                         z_ + rhs.z_,
                         w_ + rhs.w_);
  }

  /**
   *  @name operator-
   *  @fn Quaternion<T> operator-(const Quaternion<T>& rhs) const
   *  @brief  Subtraction operator
   *  @param[in]  rhs Quaternion to subtract to the current one
   *  @return Subtraction result
   */
  Quaternion<T> operator-(const Quaternion<T>& rhs) const {
    return Quaternion<T>(x_ - rhs.x_,
                         y_ - rhs.y_,
                         z_ - rhs.z_,
                         w_ - rhs.w_);
  }

  /**
   *  @name operator*
   *  @fn Quaternion<T> operator*(const Quaternion<T>& rhs) const
   *  @brief  Multiplicator operator
   *  @param[in]  rhs Quaternion to multiply with
   *  @return Multiplication result
   */
  Quaternion<T> operator*(const Quaternion<T>& rhs) const {
    Quaternion<T> prod;
    prod.x_ = (x_ * rhs.w_) + (w_ * rhs.x_) + (z_ * rhs.y_) - (y_ * rhs.z_);
    prod.y_ = (y_ * rhs.w_) + (w_ * rhs.y_) + (x_ * rhs.z_) - (z_ * rhs.x_);
    prod.z_ = (z_ * rhs.w_) + (w_ * rhs.z_) + (y_ * rhs.x_) - (x_ * rhs.y_);
    prod.w_ = (w_ * rhs.w_) - (x_ * rhs.x_) - (y_ * rhs.y_) - (z_ * rhs.z_);
    return prod;
  }

  /**
   *  @name operator*
   *  @fn Quaternion<T> operator*(T s) const
   *  @brief  Multiplicator operator
   *  @param[in]  s Scaling factor
   *  @return Multiplication result
   */
  Quaternion<T> operator*(T s) const {
    return Quaternion<T>(x_ * s, y_ * s, z_ * s, w_ * s);
  }

  /**
   *  @name operator*=
   *  @fn Quaternion<T> operator*=(T s)
   *  @brief  Multiplicator operator
   *  @param[in]  s Scaling factor
   *  @return Multiplication result
   */
  Quaternion<T> operator*=(T s) {
    x_ *= s;
    y_ *= s;
    z_ *= s;
    w_ *= s;
    return *this;
  }

  /**
   *  @name operator<<
   *  @fn friend std::ostream& operator<<(std::ostream& out, const Quaternion<T>& q)
   *  @brief  Print quaternion into a given steram
   *  @param[in]  out Output stream
   *  @param[in]  q   Quaternion to print
   *  @return Output stream
   */
  friend std::ostream& operator<<(std::ostream& out, const Quaternion<T>& q) {
    out << "(" << q.x_ << " " << q.y_ << " " << q.z_ << " " <<  q.w_ << ")";
    return out;
  }

#pragma mark -
#pragma mark Usage

  /**
   *  @name Norm
   *  @fn T Norm() const
   *  @brief  Compute the norm of the quaternion
   *  @return Norm of the quaternion
   */
  T Norm() const {
    return std::sqrt(x_ * x_ + y_ * y_ + z_ * z_ + w_ * w_);
  }

  /**
   *  @name Normalize
   *  @fn void Normalize()
   *  @brief  Normalize the quaternion to unit norm
   */
  void Normalize() {
    const T n2 = x_ * x_ + y_ * y_ + z_ * z_ + w_ * w_;
    if (n2 != 1.0) {
      *this *= (1.0 / std::sqrt(n2));
    }
  }

  /**
   *  @name Conjugate
   *  @fn Quaternion<T> Conjugate() const
   *  @brief  Compute the conjugate of the quaternion
   *  @return Conjugate quaternion
   */
  Quaternion<T> Conjugate() const {
    return Quaternion<T>(-x_, -y_, -z_, w_);
  }

  /**
   *  @name Inverse
   *  @fn Quaternion<T> Inverse() const
   *  @brief  Compute the inverse of the quaternion
   *  @return Inverse quaternion
   */
  Quaternion<T> Inverse() const {
    const T n = T(1.0) / (x_ * x_ + y_ * y_ + z_ * z_ + w_ * w_);
    return Quaternion<T>(-x_ * n, -y_ * n, -z_ * n, w_ * n);
  }

  /**
   *  @name Dot
   *  @fn T Dot(const Quaternion<T>& rhs) const
   *  @brief  Compute the dot product
   *  @param[in]  rhs Second quaternion to use to compute dot product
   *  @return Dot product between the two quaternion
   */
  T Dot(const Quaternion<T>& rhs) const {
    return (x_ * rhs.x_) + (y_ * rhs.y_) + (z_ * rhs.z_) + (w_ * rhs.w_);
  }

  /**
   *  @name SetIdentity
   *  @fn void SetIdentity()
   *  @brief  Set quaternion to identity
   */
  void SetIdentity() {
    x_ = 0.0;
    y_ = 0.0;
    z_ = 0.0;
    w_ = 1.0;
  }

  /**
   * @name  ToEuler
   * @fn    void ToEuler(T* gamma, T* theta, T* phi) const
   * @brief Convert quaternion to Euler's angle.
   * @param[out] gamma  Rotation around `z` axis
   * @param[out] theta  Rotation around `y` axis
   * @param[out] phi    Rotation around `x` axis
   */
  void ToEuler(T* gamma, T* theta, T* phi) const {
    const T xx = x_ * x_;
    const T yy = y_ * y_;
    const T zz = z_ * z_;
    const T ww = w_ * w_;
    const T m0 = xx - yy - zz + ww;
    const T m3 = 2.0 * (x_ * y_ + z_ * w_);
    const T m6 = 2.0 * (x_ * z_ - y_ * w_);
    const T m7 = 2.0 * (x_ * w_ + y_ * z_);
    const T m8 = zz - xx - yy + ww;
    // Compute euler's angles
    *gamma = std::atan2(m3, m0);
    *theta = std::atan2(-m6, std::sqrt((m0 * m0) + (m3 * m3)));
    *phi = std::atan2(m7, m8);
  }

  /**
   *  @name ToRotationMatrix
   *  @fn void ToRotationMatrix(Matrix3x3<T>* matrix) const
   *  @brief  Convert the quaternion to a 3x3 rotation matrix. The quaternion
   *          is required to be normalized, otherwise the result is undefined.
   *  @param[out] matrix    Rotation matrix initialized from quaternion
   */
  void ToRotationMatrix(Matrix3x3<T>* matrix) const {
    T xx = x_ * x_;
    T yy = y_ * y_;
    T zz = z_ * z_;
    T ww = w_ * w_;
    T* d = matrix->Data();
    d[0] = xx - yy - zz + ww;
    d[1] = 2.0 * (x_ * y_ - z_ * w_);
    d[2] = 2.0 * (y_ * w_ + x_ * z_);
    d[3] = 2.0 * (x_ * y_ + z_ * w_);
    d[4] = yy - xx -zz + ww;
    d[5] = 2.0 * (y_ * z_ - x_ * w_);
    d[6] = 2.0 * (x_ * z_ - y_ * w_);
    d[7] = 2.0 * (x_ * w_ + y_ * z_);
    d[8] = zz - xx - yy + ww;
  }

  /**
   *  @name ToRotationMatrix
   *  @fn void ToRotationMatrix(Matrix3x3<T>* matrix) const
   *  @brief  Convert the quaternion to a 3x3 rotation matrix. The quaternion
   *          is required to be normalized, otherwise the result is undefined.
   *  @param[out] matrix    Rotation matrix initialized from quaternion
   */
  void ToRotationMatrix(Matrix4x4<T>* matrix) const {
    T xx = x_ * x_;
    T yy = y_ * y_;
    T zz = z_ * z_;
    T ww = w_ * w_;
    T* d = matrix->Data();
    // First row
    d[0] = xx - yy - zz + ww;
    d[1] = 2.0 * (x_ * y_ - z_ * w_);
    d[2] = 2.0 * (y_ * w_ + x_ * z_);
    d[3] = T(0.0);
    // Second row
    d[4] = 2.0 * (x_ * y_ + z_ * w_);
    d[5] = yy - xx -zz + ww;
    d[6] = 2.0 * (y_ * z_ - x_ * w_);
    d[7] = T(0.0);
    // Third row
    d[8] = 2.0 * (x_ * z_ - y_ * w_);
    d[9] = 2.0 * (x_ * w_ + y_ * z_);
    d[10] = zz - xx - yy + ww;
    d[11] = T(0.0);
    // Fourth row
    d[12] = T(0.0);
    d[13] = T(0.0);
    d[14] = T(0.0);
    d[15] = T(1.0);
  }

  /**
   * @name  FromEuler
   * @fn    void FromEuler(const T& gamma, const T& theta, const T& phi)
   * @brief Construct the quaternion from Euler's angles
   * @param[in] gamma   Rotation around `z` axis
   * @param[in] theta   Rotation around `y` axis
   * @param[in] phi     Rotation around `x` axis
   */
  void FromEuler(const T& gamma, const T& theta, const T& phi) {
    const T cg = std::cos(gamma * T(0.5));
    const T sg = std::sin(gamma * T(0.5));
    const T ct = std::cos(theta * T(0.5));
    const T st = std::sin(theta * T(0.5));
    const T cp = std::cos(phi * T(0.5));
    const T sp = std::sin(phi * T(0.5));
    // Set quaternion
    w_ = (cp * ct * cg) + (sp * st * sg);
    x_ = (sp * ct * cg) - (cp * st * sg);
    y_ = (cp * st * cg) + (sp * ct * sg);
    z_ = (cp * ct * sg) - (sp * st * cg);
  }

  /**
   *  @name FromRotationMatrix
   *  @fn void FromRotationMatrix(const Matrix3x3<T>& matrix)
   *  @brief  Initialize the quaternion from a 3x3 rotation matrix.
   *  @param[in] matrix Rotation matrix to convert to quaternion
   */
  void FromRotationMatrix(const Matrix3x3<T>& matrix) {
    T tr = matrix[0] + matrix[4] + matrix[8];
    if (tr > T(0.0)) {
      float s = std::sqrt(tr + T(1.0)) * T(2.0); //
      w_ = T(0.25) * s;
      x_ = (matrix[7] - matrix[5]) / s;
      y_ = (matrix[2] - matrix[6]) / s;
      z_ = (matrix[3] - matrix[1]) / s;
    } else if ((matrix[0] > matrix[4]) && (matrix[0] > matrix[8])) {
      float s = std::sqrt(T(1.0) + matrix[0] - matrix[4] - matrix[8]) * T(2.0); // s=4*qx
      w_ = (matrix[7] - matrix[5]) / s;
      x_ = T(0.25) * s;
      y_ = (matrix[1] + matrix[3]) / s;
      z_ = (matrix[2] + matrix[6]) / s;
    } else if (matrix[4] > matrix[8]) {
      float s = std::sqrt(T(1.0) + matrix[4] - matrix[0] - matrix[8]) * T(2.0); // s=4*qy
      w_ = (matrix[2] - matrix[6]) / s;
      x_ = (matrix[1] + matrix[3]) / s;
      y_ = T(0.25) * s;
      z_ = (matrix[5] + matrix[7]) / s;
    } else {
      float s = std::sqrt(T(1.0) + matrix[8] - matrix[0] - matrix[4]) * T(2.0); // s=4*qz
      w_ = (matrix[3] - matrix[1]) / s;
      x_ = (matrix[2] + matrix[6]) / s;
      y_ = (matrix[5] + matrix[7]) / s;
      z_ = T(0.25) * s;
    }
  }

  /**
   *  @name FromRotationMatrix
   *  @fn void FromRotationMatrix(const Matrix4x4<T>& matrix)
   *  @brief  Initialize the quaternion from a 4x4 rotation matrix.
   *  @param[in] matrix Rotation matrix to convert to quaternion
   */
  void FromRotationMatrix(const Matrix4x4<T>& matrix) {
    T tr = matrix[0] + matrix[5] + matrix[10];
    if (tr > T(0.0)) {
      float s = std::sqrt(tr + T(1.0)) * T(2.0); //
      w_ = T(0.25) * s;
      x_ = (matrix[9] - matrix[6]) / s;
      y_ = (matrix[2] - matrix[8]) / s;
      z_ = (matrix[4] - matrix[1]) / s;
    } else if ((matrix[0] > matrix[5]) && (matrix[0] > matrix[10])) {
      float s = std::sqrt(T(1.0) + matrix[0] - matrix[5] - matrix[10]) * T(2.0); // s=4*qx
      w_ = (matrix[9] - matrix[6]) / s;
      x_ = T(0.25) * s;
      y_ = (matrix[1] + matrix[4]) / s;
      z_ = (matrix[2] + matrix[8]) / s;
    } else if (matrix[5] > matrix[10]) {
      float s = std::sqrt(T(1.0) + matrix[5] - matrix[0] - matrix[10]) * T(2.0); // s=4*qy
      w_ = (matrix[2] - matrix[8]) / s;
      x_ = (matrix[1] + matrix[4]) / s;
      y_ = T(0.25) * s;
      z_ = (matrix[6] + matrix[9]) / s;
    } else {
      float s = std::sqrt(T(1.0) + matrix[10] - matrix[0] - matrix[5]) * T(2.0); // s=4*qz
      w_ = (matrix[4] - matrix[1]) / s;
      x_ = (matrix[2] + matrix[8]) / s;
      y_ = (matrix[6] + matrix[9]) / s;
      z_ = T(0.25) * s;
    }
  }

#pragma mark -
#pragma mark Members

  /** x, imaginary part */
  T x_;
  /** y, imaginary part */
  T y_;
  /** z, imaginary part */
  T z_;
  /** w, real part */
  T w_;
};

}  // namespace LTS5


#endif /* __LTS5_quaternion__ */
