/**
 *  @file   sparse_linear_algebra_wrapper.hpp
 *  @brief  Common sparse linear algebra wrapper between Eigen and ARPACKPP/ARPACK library
 *  @ingroup utils
 *
 *  @author Gabriel Cuendet
 *  @date   08/09/16
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef __LTS5_sparse_linear_algebra_wrapper__
#define __LTS5_sparse_linear_algebra_wrapper__

#include <limits>
#include <numeric>
#include <vector>

#include "Eigen/Dense"
#include "Eigen/Sparse"

#include "lts5/utils/library_export.hpp"

template<typename T, typename U> class ARluNonSymMatrix;
template<typename T> class ARluNonSymStdEig;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  SparseLinearAlgebra
 *  @brief      Subspace for sparse linear algebra function
 */
namespace SparseLinearAlgebra {

/**
 *  @namespace  Arpack
 *  @brief      Subspace for arpack/arpackpp call
 */
namespace Arpack {
  /**
   *  @struct   non_sym_eigen_params
   *  @brief  Parameters for non symmetric eigen values/vectors decomposition
   *  @ingroup utils
   */
  template<class T>
  struct LTS5_EXPORTS non_sym_eigen_params {
    /**< Dimension of the problem */
    int n;
    /**< Number of nonzero elements in A */
    int nnz;
    /**< pointer to an array that stores the row indices of the nonzeros in A*/
    int* irow;
    /**< pointer to an array of pointers to the beginning of each column of A in vector A. */
    int* pcol;
    /**< pointer to an array that stores the nonzero elements of A */
    T* A;
    /**< number of eigenpairs to compute */
    int nevp;
    /**< Which end of the spectrum to compute */
    std::string whichp;
    /**< number of arnoldi vectors to compute */
    int ncvp;
    /**< central frequency of the band */
    T fs;
    /**< Stopping criterion (relative accuracy of Ritz values) */
    T tolp;
    /**< Maximum number of iterations */
    int maxitp; // By default (i.e. if equal to 0, then set to 100*nev)
    /**< */
    //T residp;
    /**< Wether or not to use Shift-inverse mode */
    bool enable_shift_inverse;

    /**
     *  @name   non_sym_eigen_params
     *  @fn non_sym_eigen_params()
     *  @brief  Constructor
     */
    non_sym_eigen_params() : irow(nullptr), pcol(nullptr), A(nullptr),
    whichp("LM"), fs(std::numeric_limits<T>::epsilon()), tolp(T(0.0)),
    maxitp(0), enable_shift_inverse(true) {}

    /**
     *  @name Print
     *  @fn void Print()
     *  @brief  Output the parameters values
     */
    void Print() {

    }
  };
}

/**
 *  @class LuNonSymEigShift
 *  @brief  Standard (as opposed to generalized) eigenvalue decomposition class
 *          for REAL NON-SYMMETRIC SPARSE matrices using SHIFT AND INVERT mode
 *          and SuperLU to solve some linear systems involved in inv(A-sigma*I)
 *  @ingroup utils
 */
template<class T>
class LTS5_EXPORTS LuNonSymEigShift {
 public:
  /** Matrix type */
  using EMatrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;
  /** Vector type */
  using EVector = Eigen::Matrix<T, Eigen::Dynamic, 1>;

  /**
 * @name  has_arpackpp
 * @fn  static bool has_arpackpp()
 * @brief Check if arpackpp is properly installed/linked
 * @return wether arpackpp is properly installed/linked
 */
  static bool has_arpackpp() {
#ifdef HAS_ARPACKPP
    return true;
#else
    return false;
#endif
  }

  /**
   * @name LuNonSymEigShift
   * @fn LuNonSymEigShift()
   * @brief Constructor
   */
  LuNonSymEigShift();

  /**
   *  @name   ~LuNonSymEigShift
   *  @fn ~LuNonSymEigShift()
   *  @brief  Destructor
   */
  ~LuNonSymEigShift();

  /**
   * @name  get_params
   * @fn  const Arpack::non_sym_eigen_params<T>& get_params() const;
   * @brief Getter for the eigenvalue decomposition parameters
   * @return Const reference to the parameters
   */
  const Arpack::non_sym_eigen_params<T>& get_params() const;

  /**
   * @name  get_params
   * @fn  Arpack::non_sym_eigen_params<T>& get_params();
   * @brief Getter for the eigenvalue decomposition parameters
   * @return Reference to the parameters
   */
  Arpack::non_sym_eigen_params<T>& get_params();

  /**
   * @name Init
   * @fn void Init(const Eigen::SparseMatrix<T>& A)
   * @brief Initialize the computation of the wigenvalues/vectors of A
   * @param A the matrix to decompose into eigenvectors
   * @return 0 if success, -1 otherwise
   */
  int Init(const Eigen::SparseMatrix<T>& A);

  /**
   * @name  ChangeFs
   * @fn  int ChangeFs(const T& fs)
   * @brief Change the frequency fs
   * @param[in] fs  new fs frequency
   * @return 0 if success, -1 otherwise
   */
  int ChangeFs(const T& fs);

  /**
   *  @name Compute
   *  @fn int Compute()
   *  @brief  Call arpack library for decompostion
   *  @return 0 if success, -1 otherwise
   */
  int Compute();

  /**
   *  @name   GetEigenvalues
   *  @fn inline int GetEigenvalues(EVector* eigenvalues)
   *  @brief  Provide access to eigen values
   *  @param[out] eigenvalues in no particular order
   *  @return -1 if error, 0 otherwise
   */
  int GetEigenvalues(EVector* eigenvalues);

  /**
   *  @name   GetSortedEigenvalues
   *  @fn inline int GetSortedEigenvalues(EVector* eigenvalues)
   *  @brief  Provide access to sorted eigen values
   *  @param[out] eigenvalues in increasing order
   *  @return -1 if error, 0 otherwise
   */
  int GetSortedEigenvalues(EVector* eigenvalues);

  /**
   *  @name   GetEigenvectors
   *  @fn inline int GetEigenvectors(EMatrix* eigenvectors)
   *  @brief  Provide access to eigen vectors
   *  @param[out] eigenvectors Eigen vector in no particular order by row
   *  @return -1 if error, 0 otherwise
   */
  int GetEigenvectors(EMatrix*  eigenvectors);

  /**
   *  @name   GetSortedEigenvectors
   *  @fn inline int GetSortedEigenvectors(EMatrix* eigenvectors)
   *  @brief  Provide access to eigen vectors
   *  @param[out] eigenvectors Eigen vector in increasing order by row
   *  @return -1 if error, 0 otherwise
   */
  int GetSortedEigenvectors(EMatrix*  eigenvectors);

 private:
  /**< Internal decomposition parameters **/
  Arpack::non_sym_eigen_params<T> eig_params_;
  /**< Matrix to decompose **/
  ARluNonSymMatrix<T, T>* BWM_;
  /**< Arpackpp solver wrapper */
  ARluNonSymStdEig<T>* prob_;

};

}  // namespace SparseLinearAlgebra
}  // namespace LTS5
#endif //__LTS5_sparse_linear_algebra_wrapper__
