/**
 *  @file   bitmap.hpp
 *  @brief  Image interface helper
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   15/01/16
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_BITMAP__
#define __LTS5_BITMAP__

#include <string>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  Bitmap
 *  @brief  Image interface helper, based on :
 *          Thomas Dalling - http://tomdalling.com/
 *  @author Christophe Ecabert
 *  @date   15/01/16
 *  @ingroup utils
 */
class LTS5_EXPORTS Bitmap {
 public :

  /**
   *  @enum Format
   *  @brief  Image format
   */
  enum Format {
    /** Grayscale */
    kGray = 1,
    /** Grayscale + alpha channel */
    kGrayAlpha = 2,
    /** BGR Color */
    kRGB = 3,
    /** BGR Color + alpha channel */
    kRGBA = 4
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name Bitmap
   * @fn Bitmap()
   * @brief Default constructor
   */
  Bitmap() = default;

  /**
   *  @name Bitmap
   *  @fn Bitmap(const int width,
                 const int height,
                 Format format,
                 const unsigned char* data)
   *  @brief  Create bitmap image of a given size and format
   *  @param[in]  width   Image width
   *  @param[in]  height  Image height
   *  @param[in]  format  Image format
   *  @param[in]  data    Image raw data
   */
  Bitmap(const int width,
         const int height,
         Format format,
         const unsigned char* data);

  /**
   *  @name   ~Bitmap
   *  @fn ~Bitmap()
   *  @brief  Destrcutor
   */
  ~Bitmap();

  /**
   *  @name Bitmap
   *  @fn Bitmap(const Bitmap& other)
   *  @brief  Copy constructor
   *  @param[in]  other Bitmap to copy
   */
  Bitmap(const Bitmap& other);

  /**
   *  @name operator=
   *  @fn Bitmap& operator=(const Bitmap& rhs)
   *  @brief  Assignment constructor
   *  @param[in]  rhs Right hand sign member
   *  @return Assigned bitmap
   */
  Bitmap& operator=(const Bitmap& rhs);

  /**
   *  @name LoadFromFile
   *  @fn static Bitmap LoadFromFile(const std::string& filename)
   *  @brief  Helper function to load bitmap from file
   *  @param[in]  filename  Path to the image file
   *  @return Created Bitmap
   */
  static Bitmap LoadFromFile(const std::string& filename);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name get_width
   *  @fn int get_width() const
   *  @brief  Provide image width
   *  @return Image width
   */
  int get_width() const {
    return width_;
  }

  /**
   *  @name get_height
   *  @fn int get_height() const
   *  @brief  Provide image height
   *  @return Image height
   */
  int get_height() const {
    return height_;
  }

  /**
   *  @name get_format
   *  @fn Format get_format() const
   *  @brief  Provide image format
   *  @return Image format
   */
  Format get_format() const {
    return format_;
  }

  /**
   *  @name data
   *  @fn unsigned char* data() const
   *  @brief  Return pointer to raw image data buffer
   *  @return Image data buffer
   */
  unsigned char* data() const {
    return data_;
  }

#pragma mark -
#pragma mark Private
 private :

  /**
   *  @name Set
   *  @fn void Set(const int width,
                   const int height,
                   Format format,
                   const unsigned char* data)
   *  @brief  Common constructor
   *  @param[in]  width   Image width
   *  @param[in]  height  Image height
   *  @param[in]  format  Image format
   *  @param[in]  data    Image raw data
   */
  void Set(const int width,
           const int height,
           Format format,
           const unsigned char* data);



  /** Image width */
  int width_;
  /** Image height */
  int height_;
  /** Image format */
  Format format_;
  /** Image data */
  unsigned char* data_;
};
}  // namespace LTS5
#endif /* __LTS5_BITMAP__ */
