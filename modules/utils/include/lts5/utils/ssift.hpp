/**
 *  @file   "lts5/utils/ssift.hpp"
 *  @brief  Compute SIFT descriptor
 *  @ingroup utils
 *
 *  @author Hua Gao
 *  @date   22/04/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_SSIFT__
#define __LTS5_SSIFT__

#include "lts5/utils/library_export.hpp"

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
*  @namespace  math
*  @brief      Math related namespace
*/
namespace math {
/**
*  @namespace  constants
*  @brief      Constant definition space
*/
namespace constants {
/** Square root of 2 constant */
const float kSqrt2 = 1.4142135623730951;
}
}
/**
 *  @class  SSift
 *  @brief  Simple SIFT descriptor w/o scale space and orientation
 *  @author Unknown
 *  @date   22/04/15
 *  @ingroup utils
 */
class LTS5_EXPORTS SSift {

public:
  /**
   *  @struct SSiftParameters
   *  @brief  Sift extraction parameters
   */
  struct SSiftParameters {
    /** Sift size */
    int sift_dim;
    /** Indicate if it is root*/
    bool sift_root;
    /** Number of bins in the histogram */
    int sift_descr_hist_bins;
    /** Size of the descriptor */
    int sift_descr_width;
    /** ?? */
    float sift_descr_scl_fctr;
    /** Magnitude threshold */
    float sift_descr_mag_thr;
    /** ??? */
    float sift_int_descr_fctr;
    /** Gaussian filter standard deviation */
    float sift_sigma;
    /** Initial standard deviation */
    float sift_init_sigma;
    /** ??? */
    float sift_size_scl;

    /**
     *  @name   SSiftParameters
     *  @fn SSiftParameters(void)
     *  @brief  Constructor
     */
    SSiftParameters(void) {
      sift_dim = 128;
      sift_root = true;
      sift_descr_hist_bins = 8;
      sift_descr_width = 4;
      sift_descr_scl_fctr = 3.0f;
      sift_descr_mag_thr = 0.2f;
      sift_int_descr_fctr = 512.f;
      sift_sigma = 1.6f;
      sift_init_sigma = 0.5f;
      sift_size_scl = LTS5::math::constants::kSqrt2/ 15;
    }

    void operator=(const SSiftParameters& rhs) {
      this->sift_dim = rhs.sift_dim;
      this->sift_root = rhs.sift_root;
      this->sift_descr_hist_bins = rhs.sift_descr_hist_bins;
      this->sift_descr_width = rhs.sift_descr_width;
      this->sift_descr_scl_fctr = rhs.sift_descr_scl_fctr;
      this->sift_descr_mag_thr = rhs.sift_descr_mag_thr;
      this->sift_int_descr_fctr = rhs.sift_int_descr_fctr;
      this->sift_sigma = rhs.sift_sigma;
      this->sift_init_sigma = rhs.sift_init_sigma;
      this->sift_size_scl = rhs.sift_size_scl;
    }

    /**
     *  @name   ComputeHeaderSize
     *  @fn int ComputeObjectSize(void) const
     *  @brief  Compute the size of the header in byte
     *  @return Length in bytes
     */
    int ComputeObjectSize(void) const {
      int size = 4 * sizeof(int);
      size += 6 * sizeof(float);
      return size;
    }

    /**
     *  @name   ReadParameterHeader
     *  @fn int ReadParameterHeader(std::istream& input_stream)
     *  @brief  Initialize parameters structures from input stream
     *  @param[in]  input_stream  Stream from where to read data
     *  @return -1 If error, 0 otherwise
     */
    int ReadParameterHeader(std::istream& input_stream) {
      int error = -1;
      int dummy = -1;
      if (input_stream.good()) {
        //Sift dim
        input_stream.read(reinterpret_cast<char*>(&sift_dim), sizeof(sift_dim));
        //Root
        input_stream.read(reinterpret_cast<char*>(&dummy), sizeof(dummy));
        sift_root = (dummy == 1);
        //#bins hist
        input_stream.read(reinterpret_cast<char*>(&sift_descr_hist_bins),
                          sizeof(sift_descr_hist_bins));
        //Descr size
        input_stream.read(reinterpret_cast<char*>(&sift_descr_width),
                          sizeof(sift_descr_width));
        //Descr scl fctr
        input_stream.read(reinterpret_cast<char*>(&sift_descr_scl_fctr),
                          sizeof(sift_descr_scl_fctr));
        //sift_descr_mag_thr
        input_stream.read(reinterpret_cast<char*>(&sift_descr_mag_thr),
                          sizeof(sift_descr_mag_thr));
        //sift_int_descr_fctr
        input_stream.read(reinterpret_cast<char*>(&sift_int_descr_fctr),
                          sizeof(sift_int_descr_fctr));
        //sift_sigma
        input_stream.read(reinterpret_cast<char*>(&sift_sigma),
                          sizeof(sift_sigma));
        //sift_init_sigma
        input_stream.read(reinterpret_cast<char*>(&sift_init_sigma),
                          sizeof(sift_init_sigma));
        //sift_size_scl
        input_stream.read(reinterpret_cast<char*>(&sift_size_scl),
                          sizeof(sift_size_scl));
        // Done sanity check
        error = input_stream.good() ? 0 : -1;
      }
      return error;
    }
    /**
     *  @name WriteParameterHeader
     *  @fn int WriteParameterHeader(std::ostream& output_stream) const
     *  @brief  Dump tracker parameters into a given stream.
     *  @param[in]  output_stream Stream where data will be written
     *  @return -1 If error, 0 otherwise
     */
    int WriteParameterHeader(std::ostream& output_stream) const {
      int error = -1;
      if (output_stream.good()) {
        int dummy = 0;
        // Sift dim
        output_stream.write(reinterpret_cast<const char*>(&sift_dim),
                            sizeof(int));
        // Root
        dummy = sift_root ? 1 : 0;
        output_stream.write(reinterpret_cast<const char*>(&dummy),
                            sizeof(dummy));
        // #bins hist
        output_stream.write(reinterpret_cast<const char*>(&sift_descr_hist_bins),
                            sizeof(sift_descr_hist_bins));
        // Descr size
        output_stream.write(reinterpret_cast<const char*>(&sift_descr_width),
                            sizeof(sift_descr_width));
        // Descr scl fctr
        output_stream.write(reinterpret_cast<const char*>(&sift_descr_scl_fctr),
                            sizeof(sift_descr_scl_fctr));
        // sift_descr_mag_thr
        output_stream.write(reinterpret_cast<const char*>(&sift_descr_mag_thr),
                            sizeof(sift_descr_mag_thr));
        // sift_int_descr_fctr
        output_stream.write(reinterpret_cast<const char*>(&sift_int_descr_fctr),
                            sizeof(sift_int_descr_fctr));
        // sift_sigma
        output_stream.write(reinterpret_cast<const char*>(&sift_sigma),
                            sizeof(sift_sigma));
        // sift_init_sigma
        output_stream.write(reinterpret_cast<const char*>(&sift_init_sigma),
                            sizeof(sift_init_sigma));
        // sift_size_scl
        output_stream.write(reinterpret_cast<const char*>(&sift_size_scl),
                            sizeof(sift_size_scl));
        // Done, sanity check
        error = output_stream.good() ? 0 : -1;
      }
      return error;
    }

  };

  /**
   *  @name   ComputeDescriptor
   *  @fn static void ComputeDescriptor(const cv::Mat& image,
                                  const std::vector<cv::KeyPoint>& key_points,
                                  const SSiftParameters& sift_param,
                                  cv::Mat* sift_features)
   *  @brief  Extract SSfit descriptor at location provided by key_points
   *  @param[in]  image           Input images
   *  @param[in]  key_points      Location of sift patches
   *  @param[in]  sift_param      Parameters for sift extraction
   *  @param[out] sift_features   Extracted descriptor
   */
  static void ComputeDescriptor(const cv::Mat& image,
                                const std::vector<cv::KeyPoint>& key_points,
                                const SSiftParameters& sift_param,
                                cv::Mat* sift_features);

  /**
   *  @name   ComputeDescriptorOpt
   *  @fn static void ComputeDescriptorOpt(const cv::Mat& image,
                                  const std::vector<cv::KeyPoint>& key_points,
                                  const SSiftParameters& sift_param,
                                  cv::Mat* sift_features)
   *  @brief  Extract SSfit descriptor at location provided by key_points
   *  @param[in]  image           Input images
   *  @param[in]  key_points      Location of sift patches
   *  @param[in]  sift_param      Parameters for sift extraction
   *  @param[out] sift_features   Extracted descriptor
   */
  static void ComputeDescriptorOpt(const cv::Mat& image,
                                   const std::vector<cv::KeyPoint>& key_points,
                                   const SSiftParameters& sift_param,
                                   cv::Mat* sift_features);
};
}
#endif  /* __LTS5_SSIFT__ */
