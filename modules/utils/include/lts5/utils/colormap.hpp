/**
 *  @file   colormap.hpp
 *  @brief  Colormap
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   20/06/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __COLORMAP__
#define __COLORMAP__

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   ColorMap
 * @brief   Color map
 * @author  Christophe Ecabert
 * @date    20/06/16
 * @see http://paulbourke.net/texture_colour/colourspace/
 * @ingroup utils
 */
template <typename T>
class LTS5_EXPORTS ColorMap {
 public:

#pragma mark -
#pragma mark Type Definition

  /**
   * @enum  ColorMapType
   * @brief List of type of color map
   */
  enum ColorMapType {
    /** Jet */
    kJet,
    /** Grayscale */
    kGrayscale,
    /** Winter */
    kWinter,
    /** Summer */
    kSummer
  };

  /** Color type */
  using Color = LTS5::Vector3<T>;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  ColorMap
   * @fn  ColorMap()
   * @brief Constructor
   */
  ColorMap();

  /**
   * @name  ColorMap
   * @fn  ColorMap(const ColorMapType& type, const T& min, const T& max)
   * @brief Constructor
   * @param[in] type  Type of colormap
   * @param[in] min   Minimum range value
   * @param[in] max   Maximum range value
   */
  ColorMap(const ColorMapType& type, const T& min, const T& max);

  /**
   * @name  ColorMap
   * @fn  ColorMap(const ColorMap& other)
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  ColorMap(const ColorMap& other) = delete;

  /**
   * @name  operator=
   * @fn  ColorMap& operator=(const ColorMap& rhs)
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return  Assign object
   */
  ColorMap& operator=(const ColorMap& rhs) = delete;

  /**
   * @name ~ColorMap
   * @fn  ~ColorMap() = default
   * @brief Destructor
   */
  ~ColorMap() = default;

#pragma mark -
#pragma mark Usage

  /**
   * @name  PickColor
   * @fn  void PickColor(const T& value, Color* rgb) const
   * @brief Pick color for a given value
   * @param[in]   value   Value to select color from
   * @param[out]  rgb     Corresponding color
   */
  void PickColor(const T& value, Color* rgb) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  set_colormap_type
   * @fn  void set_colormap_type(const ColorMapType& type)
   * @brief Set colormap style
   * @param[in] type  Type of colormap
   */
  void set_colormap_type(const ColorMapType& type);

  /**
   * @name  set_range
   * @fn  void set_range(const T& min, const T& max)
   * @brief Set colormap range
   * @param[in] min Minimum range value
   * @param[in] max Maximum range value
   */
  void set_range(const T& min, const T& max);

#pragma mark -
#pragma mark Private
 private:
  /** Lookup table pointer */
  float(*map_)[3];
  /** Lookup table length */
  T map_length_;
  /** Minimum range */
  T min_;
  /** Maximum range */
  T max_;
  /** Range */
  T range_;
};

}  // namepsace LTS5
#endif //__COLORMAP__
