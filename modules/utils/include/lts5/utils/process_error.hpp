/**
 *  @file   process_error.hpp
 *  @brief  Exception error
 *
 *  @author Christophe Ecabert
 *  @date 13/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_PROCESS_ERROR__
#define __LTS5_PROCESS_ERROR__

#include <iostream>
#include <exception>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  ProcessError
 *  @brief  Class used to handle errors inside process pipeline
 *  @author Christophe Ecabert
 *  @date   13.03.2014
 *  @ingroup utils
 */
class LTS5_EXPORTS ProcessError : public std::exception {
 public :

  /**
   *  @enum   ProcessErrorEnum
   *  @brief  List of possible error handled in the process pipeline
   */
  enum ProcessErrorEnum {
    /** Success */
    kSuccess = 0,
    /** Generic error */
    kErr = -1,
    /** Error while opening file */
    kErrOpeningFile = -2,
    /** Error while reading header */
    kErrReadingDataHeader = -3,
    /** Error while reading data */
    kErrReadingData = -4
  };

  /**
   *  @name   ProcessError
   *  @fn ProcessError()
   *  @brief  Constructor
   */
  ProcessError() noexcept = default;

  /**
   *  @name   ProcessError
   *  @fn ProcessError(const ProcessErrorEnum& error_code,
                       const std::string& message,
                       const std::string& function_name)
   *  @brief  Constructor
   *  @param[in]  error_code      Error code
   *  @param[in]  message         Error message
   *  @param[in]  function_name   Function name that created this error
   */
  ProcessError(const ProcessErrorEnum& error_code,
               const std::string& message,
               const std::string& function_name) noexcept;

  /**
   *  @name   ~ProcessError
   *  @fn virtual ~ProcessError() throw()
   *  @brief  Destructor
   */
  ~ProcessError() noexcept override = default;

  /**
   *  @name   what
   *  @fn virtual const char *what() const throw()
   *  @brief  Return description and context of the error
   */
  const char *what() const noexcept override;

 protected :

  std::string message_;       // !< Error message
};
}
#endif /* __LTS5_PROCESS_ERROR__ */
