/**
 *  @file   threaded_ressource_manager.hpp
 *  @brief  Singleton based Ressources Manager.
 *          Ensure that each thread as a unique ressources by keeping a map of
 *          every allocated ressources for each thread
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   16/06/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __THREADED_RESSOURCE_MANAGER__
#define __THREADED_RESSOURCE_MANAGER__

#include <thread>
#include <unordered_map>
#include <mutex>
#include <iostream>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class       ThreadedRessourceManager
 * @brief       Create singleton that handle per thread unique ressource. Hold
 *              key/value map based on thread ID - ressources
 * @author      Christophe Ecabert
 * @date        16/06/2017
 * @tparam T    Object to create singleton for
 * @ingroup     utils
 */
template<typename T>
class LTS5_EXPORTS ThreadedRessourceManager {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @name  Instance
   * @fn    static ThreadedRessourceManager<T>& Instance()
   * @brief     Provide single instance of ressource manager
   * @return    Thread safe ressource manager
   */
  static ThreadedRessourceManager<T>& Instance() {
    static ThreadedRessourceManager<T> manager;
    return manager;
  }

  /**
   * @name  ~ThreadedRessourceManager
   * @fn    ~ThreadedRessourceManager()
   * @brief Destructor
   */
  ~ThreadedRessourceManager() {
    for (auto& r : ressources_) {
      delete r.second;
      r.second = nullptr;
    }
  }

  /**
   * @name  ThreadedRessourceManager
   * @fn    ThreadedRessourceManager(const ThreadedRessourceManager<T>& other)
   * @brief Copy constructor
   * @param[in] other   Object to copy from
   */
  ThreadedRessourceManager(const ThreadedRessourceManager<T>& other) = delete;

  /**
   * @name  operator=
   * @fn    ThreadedRessourceManager<T>& operator=(const ThreadedRessourceManager<T>& rhs)
   * @brief Assignment operator
   * @param[in] rhs   Object to assign from
   * @return Newly assigned object
   */
  ThreadedRessourceManager<T>& operator=(const ThreadedRessourceManager<T>& rhs) = delete;

#pragma mark -
#pragma mark Private

  /**
   * @name  Get
   * @fn    T& Get()
   * @brief Provide ressource based on thread Id of the caller. If no
   *        ressource founded for this tread, create a new one and add it the
   *        global map
   * @return    Ressources specific for this thread
   */
  T* Get() {
    // Already one for this thread ?
    auto id = std::this_thread::get_id();
    auto res = ressources_.find(id);
    if (res == ressources_.end()) {
      // Add new entry to the map, first lock
      std::lock_guard<std::mutex> lock(this->mutex_);
      T* obj = new T();
      return (ressources_.insert({id, obj}).first)->second;
    } else {
      return res->second;
    }
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  ThreadedRessourceManager
   * @fn    ThreadedRessourceManager() = default
   * @brief Constructor
   */
  ThreadedRessourceManager() = default;

  /** Map of ressources per thread */
  std::unordered_map<std::thread::id, T*> ressources_;
  /** Mutex for thread safety */
  std::mutex mutex_;

};
}  // namepsace LTS5
#endif //__THREADED_RESSOURCE_MANAGER__
