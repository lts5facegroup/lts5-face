/**
 *  @file   hash.hpp
 *  @brief  Hash function for non standard type (i.e. std::pair, std::tuple)
 *          extracted from boost::hash<T>
 *  @ingroup utils
 *  @see https://www.nullptr.me/2018/01/15/hashing-stdpair-and-stdtuple/
 *
 *  @author Christophe Ecabert
 *  @date   8/8/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_HASH__
#define __LTS5_HASH__

#include <functional>
#include <utility>
#include <tuple>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @name    HashCombine
 * @fn inline void HashCombine(const T& value, size_t& seed)
 * @brief Combine hash function
 * @see https://stackoverflow.com/questions/5889238
 * @param[in] value   Value to be hased
 * @param[in, out] seed Seed used for hashing.
 * @tparam T    Data type
 */
template<typename T>
inline void HashCombine(const T& value, size_t& seed) {
  std::hash<T> hasher;
  seed ^= hasher(value) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}
}  // namespace LTS5


namespace std {
/**
 * @struct  std::hash
 * @brief   Hash  specialization for std::pair
 * @author  Christophe Ecabert
 * @date    08.08.18
 * @see https://stackoverflow.com/a/7222201/916549
 * @ingroup utils
 * @tparam T    First value type
 * @tparam U    Second value type
 */
template<typename T, typename U>
struct hash<std::pair<T, U>> {

  /**
   * @name  operator()
   * @fn    inline size_t operator()(const std::pair<T, U>& value) const
   * @brief Hash functor for std::pair.
   * @param[in] value
   * @return hashed value
   */
  inline size_t operator()(const std::pair<T, U> &value) const {
    size_t seed = 0;
    LTS5::HashCombine(value.first, seed);
    LTS5::HashCombine(value.second, seed);
    return seed;
  }
};

/**
 * @struct  std::hash
 * @brief   Hash  specialization for std::tuple
 * @author  Christophe Ecabert
 * @date    08.08.18
 * @see https://www.nullptr.me/2018/01/15/hashing-stdpair-and-stdtuple/
 * @ingroup utils
 * @tparam Args    Tuple's type
 */
template<class... Args>
struct hash<std::tuple<Args...>> {
 private:

  /**
   * @name  HashCombineTuple
   * @brief Termination condition, Idx == sizeof...(Types)
   * @param[in] tup Tuple to be hased
   * @param[in, out] seed   Seed
   * @tparam Idx    Index of a type of the tuple
   * @tparam Types  List of tuple's type
   */
  template<size_t Idx, typename ...Types>
  inline typename enable_if<Idx == sizeof...(Types), void>::type
  HashCombineTuple(const tuple<Types...>& tup, size_t& seed) const {
    // no-op
  }

  /**
   * @name
   * @brief Hash function called recursively while Idx < sizeof...(Types)
   * @param[in] tup Tuple to be hased
   * @param[in, out] seed   Seed
   * @tparam Idx    Index of a type of the tuple
   * @tparam Types  List of tuple's type
   */
  template<size_t Idx, typename ...Types>
  inline typename enable_if<Idx < sizeof...(Types), void>::type
  HashCombineTuple(const tuple<Types...>& tup, size_t& seed) const {
    // Hash the value at position `Idx`
    LTS5::HashCombine(get<Idx>(tup), seed);
    // Call the next element `Idx + 1`
    HashCombineTuple<Idx + 1>(tup, seed);
  }

 public:

  /**
   * @name operator()
   * @brief Hash functor for std::tuple type
   * @param[in] value Tuple to be hased
   * @return    Hased value
   */
  size_t operator()(std::tuple<Args...> value) const {
    size_t seed = 0;
    HashCombineTuple<0>(value, seed);
    return seed;
  }
};
}  // namespace std
#endif //__LTS5_HASH__
