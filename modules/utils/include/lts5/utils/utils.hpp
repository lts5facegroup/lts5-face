/**
 *  @file   lts5/utils/utils.hpp
 *  @brief  Utility module
 *
 *  @author Christophe Ecabert
 *  @date   22/04/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_UTILS__
#define __LTS5_UTILS__

/** Binary operations */
#include "lts5/utils/binary_operations.hpp"
/** Object type */
#include "lts5/utils/object_type.hpp"
/** file Input/Output */
#include "lts5/utils/file_io.hpp"
/** Command Line Parser **/
#include "lts5/utils/cmd_parser.hpp"
/** SIFT Feature extractor */
#include "lts5/utils/ssift.hpp"
/** Shape coordinates transform*/
#include "lts5/utils/shape_transforms.hpp"
/** Image coordinates transform*/
#include "lts5/utils/image_transforms.hpp"
/** Linear algebra wrapper for OpenCV */
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
/** Library exportation macro */
#include "lts5/utils/library_export.hpp"
/** Dense vector data structure */
#include "lts5/utils/math/dense_vector.hpp"
/** Sparse matrix data structure */
#include "lts5/utils/math/sparse_matrix.hpp"
/** Float/Double comparator */
#include "lts5/utils/math/compare_type.hpp"
/** Base video stream */
#include "lts5/utils/base_video_stream.hpp"
/** Time-lapse */
#include "lts5/utils/time_lapse.hpp"
/** Circular Iterator */
#include "lts5/utils/circular_iterator.hpp"
/** Circular Buffer */
#include "lts5/utils/circular_buffer.hpp"
/** Data logger */
#include "lts5/utils/data_logger.hpp"
/** Process error */
#include "lts5/utils/process_error.hpp"
/**Timestamp */
#include "lts5/utils/timestamp.hpp"


#endif
