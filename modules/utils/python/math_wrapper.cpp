/**
 *  @file   math_converter.cpp
 *  @brief  Automatically convert python iterable object into Vector<T>{2,3,4}
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   6/4/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */


#include <algorithm>
#include <iostream>
#include <cmath>

#include "pybind11/operators.h"
#include "pybind11/stl.h"

#include "math_converter.hpp"
#include "lts5/python/ocv_converter.hpp"

#include "lts5/utils/math/quaternion.hpp"
#include "lts5/utils/math/rodrigues.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/math/histogram.hpp"

namespace py = pybind11;
using namespace pybind11::literals; // to bring in the `_a` literal

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

#pragma mark -
#pragma mark Module

/**
 *  @struct MathConverterTest
 *  @brief  Unit test for math wrapper validation
 *  @author Christophe Ecabert
 *  @date   30.07.18
 *  @ingroup python
 *  @tparam T Data type
 */
template<typename T>
struct MathConverterTest {
  using Vec2 = LTS5::Vector2<T>;
  using Vec3 = LTS5::Vector3<T>;
  using Vec4 = LTS5::Vector4<T>;
  using Mat3 = LTS5::Matrix3x3<T>;
  using Mat4 = LTS5::Matrix4x4<T>;

  /**
   * @name  Print
   * @fn static void Print(const Vec2& vec)
   * @brief Dump vector into `std::cout`
   * @param[in] vec Vector
   */
  static std::string Print(const Vec2& vec) {
    std::ostringstream stream;
    stream << vec;
    return stream.str();
  }

  /**
   * @name  Print
   * @fn static void Print(const Vec3& vec)
   * @brief Dump vector into `std::cout`
   * @param[in] vec Vector
   */
  static std::string Print(const Vec3& vec) {
    std::ostringstream stream;
    stream << vec;
    return stream.str();
  }

  /**
   * @name  Print
   * @fn static void Print(const Vec4& vec)
   * @brief Dump vector into `std::cout`
   * @param[in] vec Vector
   */
  static std::string Print(const Vec4& vec) {
    std::ostringstream stream;
    stream << vec;
    return stream.str();
  }

  /**
   * @name  Print
   * @fn static void Print(const Mat3& mat)
   * @brief Dump matrix into `std::cout`
   * @param[in] mat Matrix
   */
  static std::string Print(const Mat3& mat) {
    std::ostringstream stream;
    stream << mat;
    return stream.str();
  }

  /**
   * @name  Print
   * @fn static void Print(const Mat4& mat)
   * @brief Dump matrix into `std::cout`
   * @param[in] mat Matrix
   */
  static std::string Print(const Mat4& mat)  {
    std::ostringstream stream;
    stream << mat;
    return stream.str();
  }

  /**
   * @name  CreateVector2
   * @fn    static Vec2 CreateVector2()
   * @brief Create an LTS5::Vector2 vector filled with [0,1]
   * @return    Vector
   */
  static Vec2 CreateVector2() {
    Vec2 vec(0.0, 1.0);
    return vec;
  }

  /**
   * @name  CreateVector3
   * @fn    static Vec3 CreateVector3()
   * @brief Create an LTS5::Vector3 vector filled with [0,2]
   * @return    Vector
   */
  static Vec3 CreateVector3() {
    Vec3 vec(0.0, 1.0, 2.0);
    return vec;
  }

  /**
   * @name  CreateVector4
   * @fn    static Vec4 CreateVector4()
   * @brief Create an LTS5::Vector4 vector filled with [0,3]
   * @return    Vector
   */
  static Vec4 CreateVector4() {
    Vec4 vec(0.0, 1.0, 2.0, 3.0);
    return vec;
  }

  /**
   * @name  CreateMatrix3
   * @fn    static Mat3 CreateMatrix3()
   * @brief Create an LTS5::Matrix3x3 matrix filled with [0,8]
   * @return    Matrix
   */
  static Mat3 CreateMatrix3() {
    Mat3 matrix;
    std::iota(matrix.Data(), matrix.Data() + 9, 0);
    return matrix;
  }

  /**
   * @name  CreateMatrix4
   * @fn    static Mat4 CreateMatrix4()
   * @brief Create an LTS5::Matrix4x4 matrix filled with [0,15]
   * @return    Matrix
   */
  static Mat4 CreateMatrix4() {
    Mat4 matrix;
    std::iota(matrix.Data(), matrix.Data() + 16, 0);
    return matrix;
  }
};

/*
 * @name    AddMathTestCase
 * @fn  void AddMathTestCase(py::module& m)
 * @brief Add unittest for math wrapper
 * @param[in,out] m Python module to add function to
 */
void AddMathTestCase(py::module& m) {

  using print_vec2f_func = std::string(*)(const LTS5::Vector2<float>&);
  using print_vec3f_func = std::string(*)(const LTS5::Vector3<float>&);
  using print_vec4f_func = std::string(*)(const LTS5::Vector4<float>&);
  using print_vec2d_func = std::string(*)(const LTS5::Vector2<double>&);
  using print_vec3d_func = std::string(*)(const LTS5::Vector3<double>&);
  using print_vec4d_func = std::string(*)(const LTS5::Vector4<double>&);
  using print_mat3f_func = std::string(*)(const LTS5::Matrix3x3<float>&);
  using print_mat4f_func = std::string(*)(const LTS5::Matrix4x4<float>&);
  using print_mat3d_func = std::string(*)(const LTS5::Matrix3x3<double>&);
  using print_mat4d_func = std::string(*)(const LTS5::Matrix4x4<double>&);

  // Print - vector
  m.def("math_converter_print_vec2f",
        (print_vec2f_func)&MathConverterTest<float>::Print,
        py::arg("vec"),
        R"doc(Dump a given vector 2x1 into `std::cout`)doc");
  m.def("math_converter_print_vec3f",
        (print_vec3f_func)&MathConverterTest<float>::Print,
        py::arg("vec"),
        R"doc(Dump a given vector 3x1 into `std::cout`)doc");
  m.def("math_converter_print_vec4f",
        (print_vec4f_func)&MathConverterTest<float>::Print,
        py::arg("vec"),
        R"doc(Dump a given vector 4x1 into `std::cout`)doc");
  m.def("math_converter_print_vec2d",
        (print_vec2d_func)&MathConverterTest<double>::Print,
        py::arg("vec"),
        R"doc(Dump a given vector 2x1 into `std::cout`)doc");
  m.def("math_converter_print_vec3d",
        (print_vec3d_func)&MathConverterTest<double>::Print,
        py::arg("vec"),
        R"doc(Dump a given vector 3x1 into `std::cout`)doc");
  m.def("math_converter_print_vec4d",
        (print_vec4d_func)&MathConverterTest<double>::Print,
        py::arg("vec"),
        R"doc(Dump a given vector 4x1 into `std::cout`)doc");
  // Print - matrix
  m.def("math_converter_print_mat3f",
        (print_mat3f_func)&MathConverterTest<float>::Print,
        py::arg("matrix"),
        R"doc(Dump a given 3x3 matrix into `std::cout`)doc");
  m.def("math_converter_print_mat4f",
        (print_mat4f_func)&MathConverterTest<float>::Print,
        py::arg("matrix"),
        R"doc(Dump a given 4x4 matrix into `std::cout`)doc");
  m.def("math_converter_print_mat3d",
        (print_mat3d_func)&MathConverterTest<double>::Print,
        py::arg("matrix"),
        R"doc(Dump a given 3x3 matrix into `std::cout`)doc");
  m.def("math_converter_print_mat4d",
        (print_mat4d_func)&MathConverterTest<double>::Print,
        py::arg("matrix"),
        R"doc(Dump a given 4x4 matrix into `std::cout`)doc");

  // Create - Vector
  m.def("math_converter_create_vec2f",
        &MathConverterTest<float>::CreateVector2,
        R"doc(Create a 2x1 vector filled with float from [0, 1])doc");
  m.def("math_converter_create_vec3f",
        &MathConverterTest<float>::CreateVector3,
        R"doc(Create a 3x1 vector filled with float from [0, 2])doc");
  m.def("math_converter_create_vec4f",
        &MathConverterTest<float>::CreateVector4,
        R"doc(Create a 4x1 vector filled with float from [0, 3])doc");
  m.def("math_converter_create_vec2d",
        &MathConverterTest<double>::CreateVector2,
        R"doc(Create a 2x1 vector filled with double from [0, 1])doc");
  m.def("math_converter_create_vec3d",
        &MathConverterTest<double>::CreateVector3,
        R"doc(Create a 3x1 vector filled with double from [0, 2])doc");
  m.def("math_converter_create_vec4d",
        &MathConverterTest<double>::CreateVector4,
        R"doc(Create a 4x1 vector filled with double from [0, 3])doc");
  // Create - matrix
  m.def("math_converter_create_mat3f",
        &MathConverterTest<float>::CreateMatrix3,
        R"doc(Create a 3x3 matrix filled with float from [0, 8])doc");
  m.def("math_converter_create_mat4f",
        &MathConverterTest<float>::CreateMatrix4,
        R"doc(Create a 4x4 matrix filled with float from [0, 15])doc");
  m.def("math_converter_create_mat3d",
        &MathConverterTest<double>::CreateMatrix3,
        R"doc(Create a 3x3 matrix filled with double from [0, 8])doc");
  m.def("math_converter_create_mat4d",
        &MathConverterTest<double>::CreateMatrix4,
        R"doc(Create a 4x4 matrix filled with double from [0, 15])doc");
}

template<typename T>
void ExposeQuaternion(py::module& m, const std::string& name) {
  using Quat = LTS5::Quaternion<T>;
  using Mat4 = LTS5::Matrix4x4<T>;
  using Vec3 = LTS5::Vector3<T>;

  // Function pointer
  using from_rmat_func = void(Quat::*)(const Mat4&);

  // Define class
  py::class_<Quat>(m,
                   name.c_str(),
                   R"doc(Quaternion object representing 3D rotation)doc")
          .def(py::init<>(), R"doc(Construct an identity quaternion)doc")
          .def(py::init<T, T, T>(),
               py::arg("x"), py::arg("y"),py::arg("z"),
               R"doc(Quaternion with a given imaginary part)doc")
          .def(py::init<T, T, T, T>(),
               py::arg("x"), py::arg("y"), py::arg("z"), py::arg("w"),
               R"doc(Quaternion from values)doc")
          .def(py::init<Vec3, T>(),
               py::arg("axis"), py::arg("angle"),
               R"doc(Quaternion from an axis of rotation and an angle)doc")
          .def(py::init<Quat const&>(),
               py::arg("other"),
               R"doc(Copy constructor)doc")
  // Methods
          .def("norm",
               &Quat::Norm,
               R"doc(Compute the norm of the quaternion)doc")
          .def("normalize",
               &Quat::Normalize,
               R"doc(Normalize the quaternion to unit norm)doc")
          .def("conjugate",
               &Quat::Conjugate,
               R"doc(Compute the conjugate of the quaternion)doc")
          .def("inverse",
               &Quat::Inverse,
               R"doc(Compute the inverse of the quaternion)doc")
          .def("dot",
               &Quat::Dot,
               py::arg("other"),
               R"doc(Compute the dot product between two quaternion)doc")
          .def("clear",
               &Quat::SetIdentity,
               R"doc()doc")
          .def("to_euler",
               [](const Quat& q){
                 T gamma = T(0.0), theta = T(0.0), phi = T(0.0);
                 q.ToEuler(&gamma, &theta, &phi);
                 return py::make_tuple(gamma, theta, phi);
               },
               R"doc(Convert quaternion to Euler's angle (gamma, theta, phi))doc")
          .def("from_euler",
               &Quat::FromEuler,
               py::arg("gamma"), py::arg("theta"), py::arg("phi"),
               R"doc(quaternion from Euler's angles)doc")
          .def("to_rotation_matrix",
               [](const Quat& q) {
                 Mat4 mat;
                 q.ToRotationMatrix(&mat);
                 return mat;
               },
               R"doc(Convert quaternion to a 4x4 rotation matrix)doc")
          .def("from_rotation_matrix",
               (from_rmat_func)&Quat::FromRotationMatrix,
               py::arg("matrix"),
               R"doc(Construct the quaternion from a rotation matrix)doc")
          .def_readwrite("x",
                         &Quat::x_,
                         R"doc(Imaginary part)doc")
          .def_readwrite("y",
                         &Quat::y_,
                         R"doc(Imaginary part)doc")
          .def_readwrite("z",
                         &Quat::z_,
                         R"doc(Imaginary part)doc")
          .def_readwrite("w",
                         &Quat::w_,
                         R"doc(Real part)doc")
   // Operator
          .def(py::self == py::self,
               R"doc(Equality operator)doc")
          .def(py::self != py::self,
               R"doc(Inequality operator)doc")
          .def(py::self + py::self,
               R"doc(Add operator)doc")
          .def(py::self - py::self,
               R"doc(Sub operator)doc")
          .def(py::self * py::self,
               R"doc(Product between two quaternion)doc")
          .def(py::self * float(),
               R"doc(Scaling operation)doc")
          .def(py::self *= float(),
               R"doc(Scaling operation - inplace)doc")
          .def("__repr__",
               [](const Quat& q) {
                 return "[" + std::to_string(q.x_) + ", " +
                         std::to_string(q.y_) + ", " +
                         std::to_string(q.z_) + ", " +
                         std::to_string(q.w_) + "]";
               },
               R"doc("Print quaterion content (x, y, z, w)")doc");
}

template<typename T>
void ExposeRodrigues(py::module& m, const std::string& name) {
  using Rod = LTS5::Rodrigues<T>;
  using Mat4 = LTS5::Matrix4x4<T>;
  using Vec3 = LTS5::Vector3<T>;
  // Function pointer
  using from_rmat_func = void(Rod::*)(const Mat4&);

  // Define class
  py::class_<Rod>(m,
                  name.c_str(),
                  R"doc(Rodrigues object representing 3D rotation)doc")
          .def(py::init<>(), R"doc(Construct an identity rodrigues object)doc")
          .def(py::init<T, T, T>(),
               py::arg("x"), py::arg("y"),py::arg("z"),
               R"doc(Rodrigues with given components)doc")
          .def(py::init<Vec3, T>(),
               py::arg("axis"), py::arg("angle"),
               R"doc(Rodrigues from an axis of rotation and an angle (rad))doc")
          .def(py::init<Rod const&>(),
               py::arg("other"),
               R"doc(Copy constructor)doc")
  // Methods
          .def("to_euler",
               [](const Rod& q){
                 T gamma = T(0.0), theta = T(0.0), phi = T(0.0);
                 q.ToEuler(&gamma, &theta, &phi);
                 return py::make_tuple(gamma, theta, phi);
               },
               R"doc(Convert Rodrigues to Euler's angle (gamma, theta, phi))doc")
          .def("from_euler",
               &Rod::FromEuler,
               py::arg("gamma"), py::arg("theta"), py::arg("phi"),
               R"doc(Rodrigues from Euler's angles)doc")
          .def("to_rotation_matrix",
               [](const Rod& q) {
                 Mat4 mat;
                 q.ToRotationMatrix(&mat);
                 return mat;
               },
               R"doc(Convert Rodrigues to a 4x4 rotation matrix)doc")
          .def("from_rotation_matrix",
               (from_rmat_func)&Rod::FromRotationMatrix,
               py::arg("matrix"),
               R"doc(Construct the Rodrigues object from a rotation matrix)doc")
          .def_readwrite("x",
                         &Rod::x_,
                         R"doc(X Component)doc")
          .def_readwrite("y",
                         &Rod::y_,
                         R"doc(Y Component)doc")
          .def_readwrite("z",
                         &Rod::z_,
                         R"doc(Z Component)doc")
          .def("__repr__",
               [](const Rod& r) {
                 return "[" + std::to_string(r.x_) + ", " +
                        std::to_string(r.y_) + ", " +
                        std::to_string(r.z_) + "]";
               },
               R"doc("Print Rodrigues content (x, y, z)")doc");
}

template<typename T>
void ExposeHistogram(py::module& m, const std::string& name) {
  using Hist_t = Histogram<T>;

  py::class_<Hist_t> hist(m,
                          name.c_str(),
                          R"doc(Basic histogram class using linear buffer.
 Each channel is divided into N bins of the size. The bin size is defined by the
 list of provided data range)doc");
  hist.def(py::init<>(), R"doc(Histogram constructor)doc")
      .def("build",
           &Hist_t::Build,
           "data"_a,
           "ranges"_a,
           "bin_per_channel"_a,
           R"doc(Build histogram for a given set of data points
> data: 2D array of data points of dimensions NxK to use to  build the
  histogram. Each row is considered as a sample with K dimension features.
> ranges: Data range for each channel (i.e. K pairs <min, max>)
> bin_per_channel: Number of bins per feature channel.)doc" )
      .def_property_readonly("bins",
                             &Hist_t::get_bins,
                             R"doc(Histogram data structure)doc")
      .def_property_readonly("ranges",
                             &Hist_t::get_ranges,
                             R"doc(Provide the data range used to build
 the histogram)doc")
      .def_property_readonly("count",
                             &Hist_t::count,
                             R"doc(Total number of sample in the
 histogram)doc");

  // Expose sampling
  m.def("uniform_histogram_sampling",
        [](const Hist_t& hist, const size_t& n_sample) {
          std::vector<size_t> indices;
          UniformHistogramSampling<T>(hist, n_sample, &indices);
          return indices;
        },
        "histogram"_a,
        "n_sample"_a,
        R"doc(Sample an histogram uniformly.
> histogram: Histogram to sample from
> n_sample: Number of samples to draw)doc");

  m.def("weighted_histogram_sampling",
        [](const Hist_t& hist, const size_t& n_sample) {
          std::vector<size_t> indices;
          WeightedHistogramSampling<T>(hist, n_sample, &indices);
          return indices;
        },
        "histogram"_a,
        "n_sample"_a,
        R"doc(Sample an histogram with the same distribution (i.e. will
draw more samples from most populated bins).
> histogram: Histogram to sample from
> n_sample: Number of samples to draw)doc");

}

/*
 * @name    AddMath
 * @fn  void AddMath(py::module& m)
 * @brief Expose math type to python
 * @param[in,out] m Python module to add function to
 */
void AddMath(py::module& m) {
  // Expose Quaternion
  ExposeQuaternion<float>(m, "QuaternionFloat");
  ExposeQuaternion<double>(m, "QuaternionDouble");
  ExposeRodrigues<float>(m, "RodriguesFloat");
  ExposeRodrigues<double>(m, "RodriguesDouble");
  ExposeHistogram<float>(m, "HistogramFloat");
  ExposeHistogram<double>(m, "HistogramDouble");
}

}  // namespace Python
}  // namespace LTS5
