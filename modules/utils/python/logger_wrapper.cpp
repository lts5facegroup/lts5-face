/**
 *  @file   logger_wrapper_impl.cpp
 *  @brief  Logger wrapper for pyhton binding - implementation detail
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   8/6/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#include <memory>
#include <cmath>
#include <string>
#include <vector>
#include <iostream>
#include <streambuf>
#include "lts5/utils/tee_stream.hpp"
#include "lts5/python/py_streambuf.hpp"

#include "logger_wrapper.hpp"


namespace py = pybind11;
using namespace py::literals;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {


/*
 * @name  LoggerWrapper
 * @fn    LoggerWrapper(py::object& obj)
 * @brief Constructor
 * @param[in] stream  Where to dump the message
 */
LoggerWrapper::
LoggerWrapper(py::object& obj) : obj_(obj),
                                 stream_(obj,
                                         PyStreambuf::default_buffer_size) {
  // Check if py::object is writable
  if (py::getattr(obj, "write", py::none()).is_none()) {
    throw std::invalid_argument("Python object does not have 'write' "
                                "attribute");
  }
  Logger::Instance(stream_);
}

/*
 * @name  Log
 * @fn    void Log(const Level level,
                   const char* file,
                   const int line,
                   const std::string& msg)
 * @brief Method to invoke for logging data in a given stream.
 * @param[in] level   Level of logging
 * @param[in] file    File from where the log has been emitted
 * @param[in] line    Line number where log has been emitted
 * @param[in] msg     Message to log
 */
void LoggerWrapper::Log(const Level& level,
                        const char* file,
                        const int& line,
                        const std::string& data) {
  Logger::Instance().Log(level,
                         file,
                         line,
                         LTS5::Logger::LogData<LTS5::Logger::None>() << data);
}

/*
 * @name    AddLogger
 * @fn  void AddLogger(pybind11::module& m)
 * @brief   Expose logger to python
 * @param[in,out] m Python module
 */
void AddLogger(pybind11::module& m) {
  using Level = LTS5::Logger::Level;
  using Logger = LoggerWrapper;
  // Define class + ctor
  py::class_<Logger> logger (m,
                             "Logger",
                             R"doc(Logging tools)doc");
  logger.def(py::init<py::object&>(),
             py::arg("stream"),
             R"doc(Create a single instance of logger object)doc");
  // Export nested enum
  py::enum_<Level>(logger, "Level", py::arithmetic())
          .value("kError", Level::kError)
          .value("kWarning", Level::kWarning)
          .value("kInfo", Level::kInfo)
          .value("kDebug", Level::kDebug)
          .value("kDebug1", Level::kDebug1)
          .value("kDebug2", Level::kDebug2).export_values();
  // Expose the remaining part of the class
  logger.def("log",
             &Logger::Log,
             py::arg("level") = Level::kDebug,
             py::arg("filename") = "",
             py::arg("line") = 0,
             py::arg("msg"),
             R"doc(Method to invoke for logging data)doc");
  logger.def("enable",
             &Logger::Enable,
             R"doc(Enable logging tool)doc");
  logger.def("disable",
             &Logger::Disable,
             R"doc(Disable logging tool)doc");
  logger.def_property("level",
                      &Logger::get_log_level,
                      &Logger::set_log_level,
                      R"doc(Get / Set the level of logging)doc");
}

/**
 * @class TeeStreamWrapper
 * @brief Thin wrapper over TeeStream to ensure the ostream object linked to the
 *        TeeStreambuf (i.e. derived from python objects) remain alive for the
 *        entire lifetime of this object.
 * @author Christophe Ecabert
 * @date 01/07/2020
 * @ingroup utils
 */
class TeeStreamWrapper : public TeeStream {
 public:
  /**
   * @name  TeeStreamWrapper
   * @fn    TeeStreamWrapper() = default
   * @brief Constructor
   */
  TeeStreamWrapper() = default;

  /**
   * @name
   * @fn
   * @brief Link TeeStream to a new output stream
   * @param[in] stream Python stream to be added
   */
  void LinkStream(py::object& stream) {
    // Check if py::object is writable
    if (py::getattr(stream, "write", py::none()).is_none()) {
      throw std::invalid_argument("Python object does not have 'write' "
                                  "attribute");
    }
    linked_streams_.push_back(new OStreamWrapper(stream,
                                                 PyStreambuf::default_buffer_size));
    TeeStream::LinkStream(*linked_streams_.back());
  }

 private:
  /** List of linked stream */
  std::vector<std::ostream*> linked_streams_;
};

/**
 * @name    AddTeeStream
 * @fn  void AddTeeStream(pybind11::module& m)
 * @brief   Expose TeeStream to python, object allowing duplicating message to
 *          various stream (i.e. cout + files) similar to linux tee command
 * @param[in,out] m Python module
 */
void AddTeeStream(pybind11::module& m) {
  using TeeStream_t = TeeStreamWrapper;

  // Ctor
  py::class_<TeeStream_t> tee(m,
                              "TeeStream",
                              R"doc(Mirroring stream similar to tee linux
 command)doc");
  tee.def(py::init<>(),
          R"doc(Create a TeeStream object)doc");

  // Method
  tee.def("link_stream",
          &TeeStream_t::LinkStream,
          "stream"_a,
          R"doc(Link this stream with another one (Aggregation).
 > stream: Stream to link with)doc")
     .def("write",
          [](TeeStream_t& stream, py::str& str) {
            std::string msg = str;
            stream.write(msg.c_str(), msg.size());
            stream.flush();
          },
          "message"_a,
          R"doc(Write a message into the underlying streams linked to the
 TeeStream.
 > message: Message to write.)doc")
     .def("flush",
          [](TeeStream_t& stream) { stream.flush(); },
          R"doc(Synchronizes the associated stream buffer with its
 controlled output sequence.)doc");

}

}  // namespace Python
}  // namespace LTS5
