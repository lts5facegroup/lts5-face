/**
 *  @file   colormap_wrapper.cpp
 *  @brief  Expose ColorMap class to python
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   11/13/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include "colormap_wrapper.hpp"
#include "math_converter.hpp"

#include "lts5/utils/colormap.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    ExposeColorMap
 * @fn  void ExposeColorMap(py::module& m, const std::string& name)
 * @brief   Expose ColorMap to python
 * @param[in] m     Python module where to add `ColorMap`
 * @param[in] name  Name of the exported class
 * @tparam T    Data type
 */
template<typename T>
void ExposeColorMap(py::module& m, const std::string& name) {
  using CMap = ColorMap<T>;
  using CMapType = typename CMap::ColorMapType;

  // Define ColorMap
  py::class_<CMap> cmap(m,
                        name.c_str(),
                        R"doc(Color map: Transform scalar value into color using
 a given color scheme)doc");

  cmap.def(py::init<>(),
           R"doc(Default constructor)doc")
      .def(py::init<CMapType, T, T>(),
           py::arg("type"),
           py::arg("min"),
           py::arg("max"),
           R"doc(Create a colormap of a specific type with a given range [min,
max].
> type: Type of color map
> min: Minimum value of the possible range
> max: Maximum value of the possible range)doc")
      .def("pick_color",
           [](const CMap& cm, const T value) {
             typename CMap::Color color;
             cm.PickColor(value, &color);
             return color;
           },
           py::arg("value"),
           R"doc(Pick color for a given value
> value: Value to select color for)doc")
      .def("set_colormap_type",
           &CMap::set_colormap_type,
           py::arg("type"),
           R"doc(Set colormap style
> type: Type of colormap)doc")
      .def("set_range",
           &CMap::set_range,
           py::arg("min"),
           py::arg("max"),
           R"doc(Set colormap range
> min: Minimum range value
> max: Maximum range value)doc");


  // Enum
  // See: https://pybind11.readthedocs.io/en/stable/classes.html#enumerations-and-internal-types
  py::enum_<CMapType>(cmap, "ColorMapType")
          .value("kGrayscale", CMapType::kGrayscale)
          .value("kJet", CMapType::kJet)
          .value("kSummer", CMapType::kSummer)
          .value("kWinter", CMapType::kWinter)
          .export_values();
}


/*
 * @name    AddColorMap
 * @fn      void AddColorMap(py::module& m)
 * @brief Expose ColorMap to python
 * @param[in] m Python module where to add `ColorMap`
 */
void AddColorMap(py::module& m) {
  ExposeColorMap<float>(m, "ColorMapFloat");
  ExposeColorMap<double>(m, "ColorMapDouble");
}

}  // namespace Python
}  // namespace LTS5
