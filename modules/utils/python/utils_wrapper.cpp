/**
 *  @file   face_tracker_wrapper.cpp
 *  @brief  Convert numpy array into opencv matrix
 *  @see https://github.com/yati-sagade/opencv-ndarray-conversion/
 *  @ingroup python
 *
 *  @author Gabriel Cuendet / Christophe Ecabert
 *  @date   05/07/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <memory>
#include <cmath>  // https://stackoverflow.com/questions/28683358 mingw64

#include "pybind11/pybind11.h"

#include "lts5/python/ocv_converter.hpp"
#include "lts5/python/py_streambuf.hpp"

#include "file_io_wrapper.hpp"
#include "math_wrapper.hpp"
#include "logger_wrapper.hpp"
#include "colormap_wrapper.hpp"


namespace py = pybind11;

// Create module
PYBIND11_MODULE(pyutils, m) {

  // Init
  // LTS5::Python::InitThread();
  LTS5::Python::InitNumpyArray();

  // Doc
  m.doc() = R"doc(LTS5 Utility modules)doc";

  // IOStream test case
  LTS5::Python::AddIOStreamTestCase(m);
  // OCVMatConverter test case
  LTS5::Python::AddOCVMatConverterTestCase(m);
  // MathConverter test case
  LTS5::Python::AddMathTestCase(m);
  
  // Add FileIO
  LTS5::Python::AddFileIO(m);
  // Add Math
  LTS5::Python::AddMath(m);
  // Add logger
  LTS5::Python::AddLogger(m);
  LTS5::Python::AddTeeStream(m);
  // ColorMap
  LTS5::Python::AddColorMap(m);
}
