/**
 *  @file   math_converter.hpp
 *  @brief  Automatically convert python iterable object into Vector<T>{2,3,4}
 *  @ingroup python
 *
 *  @author Christophe Ecabert
 *  @date   6/4/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_PYTHON_MATH_CONVERTER__
#define __LTS5_PYTHON_MATH_CONVERTER__

#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"
#include "pybind11/pybind11.h"
#include "pybind11/numpy.h"

#include "lts5/utils/math/matrix.hpp"

namespace py = pybind11;

namespace pybind11 {
namespace detail {

#define VECTOR_TYPE_CASTER(type, dim)                                         \
template<>                                                                    \
struct type_caster<LTS5::Vector##dim<type>> {                                 \
 public:                                                                      \
  using Type = LTS5::Vector##dim<type>;                                       \
  using DType = typename LTS5::Vector##dim<type>::DType;                      \
  PYBIND11_TYPE_CASTER(Type, _("LTS5::Vector##dim##<type>"));                 \
  bool load(handle src, bool convert) {                                       \
    auto sz = len(src);                                                       \
    if(getattr(src, "__iter__").is_none() && sz != dim) {                     \
      return false;                                                           \
    }                                                                         \
    auto iter = py::iter(src);                                                \
    DType* ptr = &value.x_;                                                   \
    for (size_t i = 0; i < dim; ++i, ++iter, ++ptr) {                         \
      *ptr = iter->cast<type>();                                              \
    }                                                                         \
    return true;                                                              \
  }                                                                           \
  static handle cast(const Type& src,                                         \
                     return_value_policy policy,                              \
                     handle parent) {                                         \
    py::list data;                                                            \
    const DType* ptr = &(src.x_);                                             \
    for (size_t i = 0; i < dim; ++i, ++ptr) {                                 \
      data.append(*ptr);                                                      \
    }                                                                         \
    return data.release();                                                    \
  }                                                                           \
}

#define MATRIX_TYPE_CASTER(type, dim)                                         \
template<>                                                                    \
struct type_caster<LTS5::Matrix##dim##x##dim<type>> {                         \
 public:                                                                      \
  using Type = LTS5::Matrix##dim##x##dim<type>;                               \
  PYBIND11_TYPE_CASTER(Type, _("LTS5::Matrix##dim##x##dim<type>"));           \
  bool load(handle src, bool convert) {                                       \
    if (!py::array_t<type>::check_(src)) {                                    \
      return false;                                                           \
    }                                                                         \
    auto buf = py::array_t<type>::ensure(src);                                \
    if (buf.ndim() != 2 && buf.size() != (dim * dim)) {                       \
      return false;                                                           \
    }                                                                         \
    value = Type(buf.data());                                                 \
    return true;                                                              \
  }                                                                           \
  static handle cast(const Type& src,                                         \
                     return_value_policy policy,                              \
                     handle parent) {                                         \
    py::array_t<type> buf({dim, dim},                                         \
                          {dim * sizeof(type), sizeof(type)},                 \
                          src.Data());                                        \
    return buf.release();                                                     \
  }                                                                           \
}

VECTOR_TYPE_CASTER(int, 2);
VECTOR_TYPE_CASTER(int, 3);
VECTOR_TYPE_CASTER(int, 4);
VECTOR_TYPE_CASTER(float, 2);
VECTOR_TYPE_CASTER(float, 3);
VECTOR_TYPE_CASTER(float, 4);
VECTOR_TYPE_CASTER(double, 2);
VECTOR_TYPE_CASTER(double, 3);
VECTOR_TYPE_CASTER(double, 4);
MATRIX_TYPE_CASTER(float, 2);
MATRIX_TYPE_CASTER(float, 3);
MATRIX_TYPE_CASTER(float, 4);
MATRIX_TYPE_CASTER(double, 2);
MATRIX_TYPE_CASTER(double, 3);
MATRIX_TYPE_CASTER(double, 4);

#undef VECTOR_TYPE_CASTER
#undef MATRIX_TYPE_CASTER

}}  // namespace pybind11::detail
#endif //__LTS5_PYTHON_MATH_CONVERTER__
