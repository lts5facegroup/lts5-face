/**
 *  @file   math_wrapper.hpp
 *  @brief  Python wrapper for math module
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   6/17/20
 *  Copyright (c) 2020 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_PYTHON_MATH_WRAPPER__
#define __LTS5_PYTHON_MATH_WRAPPER__

#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"
#include "pybind11/pybind11.h"

namespace py = pybind11;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

#pragma mark -
#pragma mark Module definition

/**
 * @name    AddMathTestCase
 * @fn  void AddMathTestCase(py::module& m)
 * @brief Add unittest for math wrapper
 * @param[in,out] m Python module to add function to
 */
void AddMathTestCase(py::module& m);

/**
 * @name    AddMath
 * @fn  void AddMath(py::module& m)
 * @brief Expose math type to python
 * @param[in,out] m Python module to add function to
 */
void AddMath(py::module& m);


}  // namespace Python
}  // namespace LTS5

#endif  // __LTS5_PYTHON_MATH_WRAPPER__
