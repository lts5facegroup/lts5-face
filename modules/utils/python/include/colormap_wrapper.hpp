/**
 *  @file   colormap_wrapper.hpp
 *  @brief  Expose ColorMap class to python
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   11/13/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_COLORMAP_WRAPPER__
#define __LTS5_COLORMAP_WRAPPER__

#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"
#include "pybind11/pybind11.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

namespace py = pybind11;


#pragma mark -
#pragma mark Module definition

/**
 * @name    AddColorMap
 * @fn      void AddColorMap(py::module& m)
 * @brief Expose ColorMap to python
 * @param[in] m Python module where to add `ColorMap`
 */
void AddColorMap(py::module& m);


}  // namespace Python
}  // namespace LTS5

#endif  // __LTS5_COLORMAP_WRAPPER__
