/**
 *  @file   file_io_wrapper.hpp
 *  @brief  Wrapper for pyhton binding
 *  @ingroup    utils
 *
 *  @author Christophe Ecabert
 *  @date   05/10/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_FILE_IO_WRAPPER__
#define __LTS5_FILE_IO_WRAPPER__

#include "pybind11/pybind11.h"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/file_io.hpp"


namespace py = pybind11;

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {
  
#pragma mark -
#pragma mark Declaration

/**
 * @name    LoadMatFromBin
 * @fn  py::tuple LoadMatFromBin(const std::string& filename)
 * @brief Load matrix from binary file
 * @param[in] filename Path to binary file
 * @return  Return tuple [err_code, matrix] where err_code is -1 if error
 *          occurs, 0 otherwise and matrix holds data
 */
py::tuple LoadMatFromBin(const std::string& filename) {
  cv::Mat data;
  int err = LTS5::LoadMatFromBin(filename, &data);
  return py::make_tuple(err, data);
}

/**
 * @name    LoadMatFromBin
 * @fn  py::tuple ReadMatFromBin(std::istream& stream)
 * @brief Load matrix from binary stream
 * @param[in] stream Binary stream to read from
 * @return  Return tuple [err_code, matrix] where err_code is -1 if error
 *          occurs, 0 otherwise and matrix holds data
 */
py::tuple ReadMatFromBin(std::istream& stream) {
  cv::Mat data;
  int err = LTS5::ReadMatFromBin(stream, &data);
  return py::make_tuple(err, data);
}

/**
 *  @name   LoadPts
 *  @fn cv::Mat LoadPts(const std::string& filename)
 *  @brief  Load annotation file
 *  @ingroup utils
 *  @param[in]  filename  Path to the annotation file
 *  @return Return tuple [err_code, pts] where err_code is -1 if error
 *          occurs, 0 otherwise and pts holds data points
 */
cv::Mat LoadPts(const std::string& filename) {
  cv::Mat data;
  LTS5::LoadPts(filename, data);
  return data;
}
  

/**
 *  @name   AddFileIO
 *  @fn     void AddFileIO(py::module& m)
 *  @brief  Export FileIO operation to python
 *  @param[in] m  Python module in which to add the function
 */
void AddFileIO(pybind11::module& m)  {
  // Load / Save points
  m.def("load_pts",
        &LoadPts,
        py::arg("filename"),
        R"doc(Load annotation file *.pts)doc");
  m.def("save_pts",
        &LTS5::SavePts,
        py::arg("filename"), py::arg("pts"),
        R"doc(Save annotations into file)doc");

  // Load / Save matrix
  m.def("load_matrix",
        &LoadMatFromBin,
        py::arg("filename"),
        R"doc(Load matrix from binary file)doc");
  m.def("read_matrix",
        &ReadMatFromBin,
        py::arg("stream"),
        R"doc(Load matrix from binary steam)doc");

  m.def("save_matrix",
        &LTS5::SaveMatToBin,
        py::arg("filename"), py::arg("matrix"),
        R"doc(Save a matrix into a binary file)doc");
  m.def("write_matrix",
        &LTS5::WriteMatToBin,
        py::arg("stream"), py::arg("matrix"),
        R"doc(Save a matrix into a binary stream)doc");
}

}  // namepsace Python
}  // namepsace LTS5
#endif //__LTS5_FILE_IO_WRAPPER__
