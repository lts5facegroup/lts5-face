/**
 *  @file   logger_wrapper.hpp
 *  @brief  Logger wrapper for pyhton binding
 *  @ingroup    utils
 *
 *  @author Christophe Ecabert
 *  @date   8/6/18
 *  Copyright (c) 2018 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_LOGGER_WRAPPER__
#define __LTS5_LOGGER_WRAPPER__

#include "pybind11/pybind11.h"

#include "lts5/utils/logger.hpp"
#include "lts5/python/py_streambuf.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

class LoggerWrapper {
 public:

  using Level = LTS5::Logger::Level;

  /**
   * @name  LoggerWrapper
   * @fn    LoggerWrapper(py::object& obj)
   * @brief Constructor
   * @param[in] obj  IO object were to dump the log
   */
  explicit LoggerWrapper(py::object& obj);

  /**
   * @name  ~LoggerWrapper
   * @fn    ~LoggerWrapper() = default
   * @brief Destructor
   */
  ~LoggerWrapper() = default;

  /**
   * @name  Log
   * @fn    void Log(const Level& level,
                     const char* file,
                     const int& line,
                     const std::string& msg)
   * @brief Method to invoke for logging data in a given stream.
   * @param[in] level   Level of logging
   * @param[in] file    File from where the log has been emitted
   * @param[in] line    Line number where log has been emitted
   * @param[in] msg     Message to log
   */
  void Log(const Level& level,
           const char* file,
           const int& line,
           const std::string& data);

  /**
   * @name  Enable
   * @fn    void Enable()
   * @brief Enable logging tool
   */
  void Enable() {
    Logger::Instance().Enable();
  }

  /**
   * @name  Disable
   * @fn    void Disable()
   * @brief Disable logging tool
   */
  void Disable() {
    Logger::Instance().Disable();
  }

  /**
   * @name  set_log_level
   * @fn    void set_log_level(const Level& level)
   * @brief Define the level of log to record
   * @param[in] level   Desired level of logging
   */
  void set_log_level(const Level& level) {
    Logger::Instance().set_log_level(level);
  }

  /**
   * @name  get_log_level
   * @fn    const Level& get_log_level() const
   * @brief Provide current level of logging
   * @return    Logging level
   */
  const Level& get_log_level() const {
    return Logger::Instance().get_log_level();
  }

 private:
  /** Python object reference */
  py::object obj_;
  /** Stream */
  OStreamWrapper stream_;
};

/**
 * @name    AddLogger
 * @fn  void AddLogger(pybind11::module& m)
 * @brief   Expose logger to python
 * @param[in,out] m Python module
 */
void AddLogger(pybind11::module& m);

/**
 * @name    AddTeeStream
 * @fn  void AddTeeStream(pybind11::module& m)
 * @brief   Expose TeeStream to python, object allowing duplicating message to
 *          various stream (i.e. cout + files) similar to linux tee command
 * @param[in,out] m Python module
 */
void AddTeeStream(pybind11::module& m);

}  // namespace Python
}  // namespace LTS5
#endif //__LTS5_LOGGER_WRAPPER__
