/**
 *  @file   partsbased_face_detector.hpp
 *  @brief  PartsBased Detector wrapper
 *  @ingroup face_detector
 *
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_PARTSBASED_FACE_DETECTOR__
#define __LTS5_PARTSBASED_FACE_DETECTOR__

#include <string>
#include <vector>

#include "opencv2/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_detector/base_face_detector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/** Forward type */
struct PartDetector;

/**
 *  @class  PartsBasedFaceDetector
 *  @brief  PartsBased Detector wrapper
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  @ingroup face_detector
 */
class LTS5_EXPORTS PartsBasedFaceDetector : public BaseFaceDetector {
 public:
  /**
   *  @name   PartsBasedFaceDetector
   *  @fn explicit PartsBasedFaceDetector(const std::string& filepath)
   *  @brief  Constructor
   *  @param[in]  filepath  Path of the model file (either .xml or .mat)
   */
  explicit PartsBasedFaceDetector(const std::string& filepath);

  /**
   *  @name   ~PartsBasedFaceDetector
   *  @fn ~PartsBasedFaceDetector() override
   *  @brief  Destructor
   */
  ~PartsBasedFaceDetector() override;

# pragma mark -
# pragma mark Public methods

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image, std::vector<cv::Rect>* detections) override
   *  @brief  Dectection method, given a still image, provides the face bounding
   *          boxes
   *  @param[in]  image       Input image
   *  @param[out] detections  vector of rectangles
   *  @return -1 if error, the number of detections otherwise
   */
  int Detect(const cv::Mat& image, std::vector<cv::Rect>* detections) override;

  /**
   *  @name   DetectSingle
   *  @fn int DetectSingle(const cv::Mat& image, cv::Rect* detection) override
   *  @brief  Dectection method, given a still image, provides a single face
   *          bounding box
   *  @param[in]  image       Input image
   *  @param[out] detection   rectangle of the detection
   *  @return -1 if error, 0 if no detection, 1 if successful
   */
  int DetectSingle(const cv::Mat& image, cv::Rect* detection) override;

  /**
   *  @name   empty
   *  @fn bool empty() const override
   *  @brief  Checks whether the classifier has been loaded
   *  @return true if loaded, false if not
   */
  bool empty() const override;

  /**
   *  @name set_parameters
   *  @fn void set_parameters(const BaseFaceDetector::BaseFaceDetectorParameter& param) override
   *  @brief  Set a new set of parameters
   *  @param[in]  param New parameters
   */
  void set_parameters(const BaseFaceDetector::BaseFaceDetectorParameter& param) override;

 private:
  /** PartsBasedDetector instance */
  PartDetector* detector_;
  /** Wether the detector has been loaded or not */
  bool empty_ = true;
};
}  // namespace LTS5

#endif  /* __LTS5_PARTSBASED_FACE_DETECTOR__ */
