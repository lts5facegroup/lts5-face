/**
 *  @file   lts5/face_detector/face_detector.hpp
 *  @brief  Face detector module
 *
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_FACE_DETECTOR__
#define __LTS5_FACE_DETECTOR__

/** Base class for face detector */
#include "lts5/face_detector/base_face_detector.hpp"
/** PartsBased wrapper for face detector */
#include "lts5/face_detector/partsbased_face_detector.hpp"
/** OpenCV Cascade wrapper for face detector */
#include "lts5/face_detector/cascade_face_detector.hpp"

#endif
