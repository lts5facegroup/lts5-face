/**
 *  @file   cascade_face_detector.hpp
 *  @brief  OpenCV Cascade Detector wrapper
 *  @ingroup face_detector
 *
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_CASCADE_FACE_DETECTOR__
#define __LTS5_CASCADE_FACE_DETECTOR__

#include <string>
#include <limits>
#include <typeinfo>

#include "opencv2/core/core.hpp"
#include "opencv2/objdetect/objdetect.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/face_detector/base_face_detector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  CascadeFaceDetector
 *  @brief  OpenCV Cascade Detector wrapper
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  @ingroup face_detector
 */
class LTS5_EXPORTS CascadeFaceDetector : public BaseFaceDetector {
 public:
  /**
   *  @struct CascadeDetectorParameters
   *  @brief  User configuration for OpenCV Cascade Detector
   */
  struct CascadeDetectorParameters : public BaseFaceDetectorParameter {

    /** Wether or not to downsample the image before detecting in order to
     * speed up computation */
    bool downsample_image = false;
    /** Parameter specifying how much the image size is reduced at each image
     scale */
    double scale_factor = 1.1;
    /** Parameter specifying how many neighbors each candidate rectangle should
     have to retain it */
    int min_neighbors = 2;
    /** Parameter with the same meaning for an old cascade as in the function
     cvHaarDetectObjects. It is not used for a new cascade */
    int flags;
    /** Minimum possible object size. Objects smaller than that are ignored */
    cv::Size min_size;
    /** Maximum possible object size. Objects larger than that are ignored */
    cv::Size max_size;

    /**
     *  @name   CascadeDetectorParameters
     *  @brief  Constructor
     */
    CascadeDetectorParameters(void) {
      downsample_image = false;
      scale_factor = 1.1;
      min_neighbors = 2;
      flags = 0;
      min_size = cv::Size(30, 30);
      max_size = cv::Size(INT_MAX, INT_MAX);
    }
  };

  /**
   *  @name   CascadeFaceDetector
   *  @fn explicit CascadeFaceDetector(const std::string& filepath)
   *  @brief  Constructor
   *  @param[in]  filepath  Path to the model file (.xml)
   */
  explicit CascadeFaceDetector(const std::string& filepath);

  /**
   *  @name   ~CascadeFaceDetector
   *  @fn ~CascadeFaceDetector()
   *  @brief  Destructor
   */
  ~CascadeFaceDetector();

# pragma mark -
# pragma mark Public methods

  /**
   *  @name   Detect
   *  @fn int Detect(const cv::Mat& image, std::vector<cv::Rect>* detections)
   *  @brief  Dectection method, given a still image, provides the face bounding
   *          boxes
   *  @param[in]  image       Input image
   *  @param[out] detections  vector of rectangles
   *  @return -1 if error, the number of detections otherwise
   */
  int Detect(const cv::Mat& image, std::vector<cv::Rect>* detections);

  /**
   *  @name   DetectSingle
   *  @fn int DetectSingle(const cv::Mat& image, cv::Rect* detection)
   *  @brief  Dectection method, given a still image, provides a single face
   *          bounding box
   *  @param[in]  image       Input image
   *  @param[out] detection   rectangle of the detection
   *  @return -1 if error, 0 if no detection, 1 if successful
   */
  int DetectSingle(const cv::Mat& image, cv::Rect* detection);

  /**
   *  @name   empty
   *  @fn bool empty(void) const
   *  @brief  Checks whether the classifier has been loaded
   *  @return true if loaded, false if not
   */
  bool empty(void) const;

  /**
   *  @name set_parameters
   *  @fn void set_parameters(const BaseFaceDetector::BaseFaceDetectorParameter& param)
   *  @brief  Set a new set of parameters
   *  @param[in]  param New parameters
   */
  void set_parameters(const BaseFaceDetector::BaseFaceDetectorParameter& param);

 private:
  /** OpenCV Viola-Jones face detector */
  cv::CascadeClassifier* detector_;
  /** Detector parameters */
  CascadeDetectorParameters parameters_;
};
}  // namespace LTS5
#endif /* __LTS5_CASCADE_FACE_DETECTOR__ */
