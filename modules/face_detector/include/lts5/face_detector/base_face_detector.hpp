/**
 *  @file   base_face_detector.hpp
 *  @brief  Base Face detector definition
 *  @ingroup face_detector
 *
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_BASE_FACE_DETECTOR__
#define __LTS5_BASE_FACE_DETECTOR__

#include <vector>
#include <iostream>
#include "opencv2/core/core.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  BaseFaceDetector
 *  @brief  Abstract class to define basic face detector interface
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  @ingroup face_detector
 */
class BaseFaceDetector {
 public:

  /**
   *  @struct BaseFaceDetectorParameter
   *  @brief  Base class for face detector parameter. This class is used to
   *          Properly setup parameters through derived classes
   */
  struct BaseFaceDetectorParameter {
    /**
     *  @name ~BaseDetectorParameter
     *  @fn virtual ~BaseFaceDetectorParameter(void)
     *  @brief  Destructor
     */
    virtual ~BaseFaceDetectorParameter(void) {}
  };

  /**
   *  @name   ~BaseFaceDetector
   *  @fn virtual ~BaseFaceDetector()
   *  @brief  Destructor
   */
  virtual ~BaseFaceDetector() {}

  /**
   *  @name   Detect
   *  @fn virtual int Detect(const cv::Mat& image,
                             std::vector<cv::Rect>* detections) = 0
   *  @brief  Dectection method, given a still image, provides the face bounding
   *          boxes
   *  @param[in]  image       Input image
   *  @param[out] detections  vector of rectangles
   *  @return -1 if error, the number of detections otherwise
   */
  virtual int Detect(const cv::Mat& image,
                     std::vector<cv::Rect>* detections) = 0;

  /**
   *  @name   DetectSingle
   *  @fn virtual int DetectSingle(const cv::Mat& image, cv::Rect* detection) = 0
   *  @brief  Dectection method, given a still image, provides a single face
   *          bounding box
   *  @param[in]  image       Input image
   *  @param[out] detection   rectangle of the detection
   *  @return -1 if error, 0 if no detection, 1 if successful
   */
  virtual int DetectSingle(const cv::Mat& image, cv::Rect* detection) = 0;

  /**
   *  @name   empty
   *  @fn virtual bool empty(void) const = 0
   *  @brief  Checks whether the classifier has been loaded
   *  @return true if loaded, false if not
   */
  virtual bool empty(void) const = 0;

  /**
   *  @name set_parameters
   *  @fn virtual void set_parameters(const BaseFaceDetectorParameter& param)
   *  @brief  Setup new parameters for face detector class
   *  @param[in]  param New parameters
   */
  virtual void set_parameters(const BaseFaceDetectorParameter& param) = 0;
};
}  // namespace LTS5
#endif /* __LTS5_BASE_FACE_DETECTOR__ */
