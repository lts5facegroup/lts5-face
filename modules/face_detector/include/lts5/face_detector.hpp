/**
 *  @file   lts5/face_detector.hpp
 *
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#include "lts5/face_detector/face_detector.hpp"