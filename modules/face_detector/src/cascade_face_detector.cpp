/**
 *  @file   cascade_face_detector.cpp
 *  @brief  OpenCV Cascade Detector wrapper
 *
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#include "opencv2/highgui.hpp"

#include "opencv2/imgproc/imgproc.hpp"
#include "lts5/face_detector/cascade_face_detector.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/*
 *  @name   CascadeFaceDetector
 *  @brief  Constructor
 *  @param[in]  filepath  Path to the model file (.xml)
 */
CascadeFaceDetector::CascadeFaceDetector(const std::string& filepath) {
  detector_ = new cv::CascadeClassifier(filepath);
}

/*
 *  @name   ~PartsBasedFaceDetector
 *  @brief  Destructor
 */
CascadeFaceDetector::~CascadeFaceDetector() {
  // Clean up
  if (!detector_) {
    delete detector_;
  }
}

# pragma mark -
# pragma mark Public methods

/*
 *  @name   Detect
 *  @brief  Dectection method, given a still image, provides the face bounding
 *          boxes
 *  @param[in]  image       Input image
 *  @param[out] detections  vector of rectangles
 *  @return -1 if error, the number of detections otherwise
 */
int CascadeFaceDetector::Detect(const cv::Mat& image,
                                std::vector<cv::Rect>* detections) {
  detections->clear();
  int n_detections = -1;
  cv::Mat working_img = image;

  float scale = 1.f;
  if (parameters_.downsample_image) {
    // Downsample image to speedup computation
    float aspect_ratio = (float) image.rows / (float) image.cols;
    int img_height = static_cast<int>(320.f * aspect_ratio);
    cv::Mat downsample_input;
    cv::resize(image, downsample_input, cv::Size(320, img_height));
    scale = static_cast<float>(image.cols) /
            static_cast<float>(downsample_input.cols);
    parameters_.min_size = cv::Size((200.0 / scale), (200.0 / scale));
    working_img = downsample_input;
  }

  detector_->detectMultiScale(working_img,
                              *detections,
                              parameters_.scale_factor,
                              parameters_.min_neighbors,
                              parameters_.flags,
                              parameters_.min_size,
                              parameters_.max_size);

  if (parameters_.downsample_image) {
    for (auto rect_it = detections->begin(); rect_it != detections->end(); ++rect_it) {
      // Convert back to original image coordinate
      rect_it->x = static_cast<int>(rect_it->x * scale);
      rect_it->y = static_cast<int>(rect_it->y * scale);
      rect_it->width = static_cast<int>(rect_it->width * scale);
      rect_it->height = static_cast<int>(rect_it->height * scale);
    }
  }

  n_detections = static_cast<int>((*detections).size());
  return n_detections;
}

/*
 *  @name   DetectSingle
 *  @brief  Dectection method, given a still image, provides a single face
 *          bounding box
 *  @param[in]  image       Input image
 *  @param[out] detection   rectangle of the detection
 *  @return -1 if error, 0 if no detection, 1 if successful
 */
int CascadeFaceDetector::DetectSingle(const cv::Mat& image,
                                      cv::Rect* detection) {
  // @TODO: (Gabriel) Think about a way to select the "best" detection amongst several
  return -1;
}

/*
 *  @name   empty
 *  @brief  Checks whether the classifier has been loaded
 *  @return true if loaded, false if not
 */
bool CascadeFaceDetector::empty(void) const {
  return detector_->empty();
}

/*
 *  @name set_parameters
 *  @fn void set_parameters(const BaseFaceDetectorParameter& param)
 *  @brief  Set a new set of parameters
 *  @param[in]  param New parameters
 */
void CascadeFaceDetector::set_parameters(const BaseFaceDetector::BaseFaceDetectorParameter& param) {
  // Cast to BaseDetectorParameter ptr
  typedef CascadeDetectorParameters Derived;

  const Derived& p_ptr = dynamic_cast<const Derived&>(param);
  // Setup new param
  parameters_ = p_ptr;
}

}  // namespace LTS5
