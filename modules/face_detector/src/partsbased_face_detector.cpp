/**
 *  @file   partsbased_face_detector.cpp
 *  @brief  PartsBased Detector wrapper
 *
 *  @author Gabriel Cuendet
 *  @date   07/07/15
 *  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
 */

#include <string>
#include <vector>

#include "PartsBasedDetector.hpp"
#include "Candidate.hpp"
#include "FileStorageModel.hpp"
#include "MatlabIOModel.hpp"
#include "Visualize.hpp"
#include "types.hpp"
#include "nms.hpp"
#include "Rect3.hpp"
#include "DistanceTransform.hpp"

#include "lts5/face_detector/partsbased_face_detector.hpp"

/*
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma Encapsulation

struct PartDetector {
  /** Instance */
  PartsBasedDetector<double> *ptr;

  /** CTor */
  PartDetector() : ptr(nullptr) {
    ptr = new PartsBasedDetector<double>();
  }

  ~PartDetector() {
    delete ptr;
  }
};

#pragma mark -
#pragma Implementation

/*
 *  @name   PartsBasedFaceDetector
 *  @brief  Constructor
 *  @param[in]  filepath  Path of the model file (either .xml or .mat)
 */
PartsBasedFaceDetector::PartsBasedFaceDetector(const std::string& filepath) {
  Model* model;
  // determine the type of model to read
  auto pos = filepath.rfind('.');
  std::string ext;
  if (pos != std::string::npos) {
    ext = filepath.substr(pos);
  }
  if (ext == ".xml" || ext == ".yaml") {
    model = new FileStorageModel();
  }
  else if (ext == ".mat") {
    model = new MatlabIOModel();
  }
  else {
    printf("Unsupported model format: %s\n", ext.c_str());
  }

  bool ok = model->deserialize(filepath);
  if (!ok) {
    printf("Error deserializing file\n");
  }

  // create the PartsBasedDetector and distribute the model parameters
  detector_ = new PartDetector();
  detector_->ptr->distributeModel(*model);
  empty_ = false;
  delete model;
}
/*
 *  @name   ~PartsBasedFaceDetector
 *  @fn ~PartsBasedFaceDetector()
 *  @brief  Destructor
 */
PartsBasedFaceDetector::~PartsBasedFaceDetector() {
  if (detector_) {
    delete detector_;
  }
}

# pragma mark -
# pragma mark Public methods

/*
 *  @name   Detect
 *  @brief  Dectection method, given a still image, provides the face bounding
 *          boxes
 *  @param[in]  image       Input image
 *  @param[out] detections  vector of rectangles
 *  @return -1 if error, the number of detections otherwise
 */
int PartsBasedFaceDetector::Detect(const cv::Mat& image,
                                   std::vector<cv::Rect>* detections) {
  detections->clear();
  int n_detections = -1;
  // detect potential candidates in the image
  cv::Mat_<float> depth;
  std::vector<Candidate> candidates;
  detector_->ptr->detect(image, depth, candidates);
  n_detections = static_cast<int>(candidates.size());

  // Convert candidates (vector of parts = vector of Rect) to boundingBox Rect
  for (int i=0; i < n_detections; ++i) {
    detections->push_back(candidates[i].boundingBox());
  }

  return n_detections;
}

/*
 *  @name   DetectSingle
 *  @brief  Dectection method, given a still image, provides a single face
 *          bounding box
 *  @param[in]  image       Input image
 *  @param[out] detection   rectangle of the detection
 *  @return -1 if error, 0 if no detection, 1 if successful
 */
int PartsBasedFaceDetector::DetectSingle(const cv::Mat& image,
                                         cv::Rect* detection) {
  int n_detections = -1;
  // detect potential candidates in the image
  cv::Mat_<float> depth;
  std::vector<Candidate> candidates;
  detector_->ptr->detect(image, depth, candidates);
  n_detections = static_cast<int>(candidates.size());

  if (n_detections > 1) {
    Candidate::sort(candidates);
  }

  if (n_detections > 0) {
    *detection = candidates[0].boundingBox();
  } else {
    *detection = cv::Rect(0, 0, 0, 0);
  }

  return n_detections;
}

/*
 *  @name   empty
 *  @brief  Checks whether the classifier has been loaded
 *  @return true if loaded, false if not
 */
bool PartsBasedFaceDetector::empty() const {
  return empty_;
}

/*
 *  @name set_parameters
 *  @fn void set_parameters(const BaseFaceDetector::BaseFaceDetectorParameter& param)
 *  @brief  Set a new set of parameters
 *  @param[in]  param New parameters
 */
void PartsBasedFaceDetector::set_parameters(const BaseFaceDetector::BaseFaceDetectorParameter& param) {
  std::cout << "Cannot set parameters... No parameters!" << std::endl;
}

}  // namespace LTS5
