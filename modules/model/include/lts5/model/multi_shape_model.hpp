/**
 *  @file   multi_shape_model.hpp
 *  @brief  3D statistical shape model with more than one variation. For
 *          instance Id + Expr
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   14/11/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_MULTI_SHAPE_MODEL__
#define __LTS5_MULTI_SHAPE_MODEL__

#include "lts5/utils/library_export.hpp"
#include "lts5/model/shape_model.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

template<typename T>
class LTS5_EXPORTS MultiShapeModel : public ShapeModel<T> {
public:

#pragma mark -
#pragma mark Type Definition

  /** Mesh type */
  using Mesh = typename PCAModel<T>::Mesh;
  /** Triangle type */
  using Triangle = typename PCAModel<T>::Mesh::Triangle;

#pragma mark -
#pragma mark Expose overlaoded function

  // Expose base class functions
  using ShapeModel<T>::Save;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  MultiShapeModel
   * @fn    MultiShapeModel()
   * @brief Constructor
   */
  MultiShapeModel() = default;

  /**
   * @name  MultiShapeModel
   * @fn    MultiShapeModel(const cv::Mat& mean,
                            const cv::Mat& variation,
                            const cv::Mat& prior,
                            const int& n_channels,
                            const std::vector<Triangle>& tri,
                            const std::vector<int>& landmarks,
                            const std::vector<T>& cog,
                            const std::vector<int>& dims)
   * @brief Specialized constructor
   * @param[in] mean        Mean value
   * @param[in] variation   Variation matrix (Eigen vectors in column)
   * @param[in] prior       Prior (Eigen values)
   * @param[in] n_channels  Number of channels in the flattened mean.
   * @param[in] tri         Triangulation
   * @param[in] landmarks   Facial landmark's indexes
   * @param[in] cog         Center of gravity of the shape model (offset)
   * @param[in] dims        Each shape dimensions. Must sum to the number of
   *                        principal components
   */
  MultiShapeModel(const cv::Mat& mean,
                  const cv::Mat& variation,
                  const cv::Mat& prior,
                  const int& n_channels,
                  const std::vector<Triangle>& tri,
                  const std::vector<int>& landmarks,
                  const std::vector<T>& cog,
                  const std::vector<int>& dims);

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream) override
   * @brief Load from a given binary \p stream
   * @param[in] stream Binary stream to load model from
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream) override;

  /**
   * @name  Save
   * @fn    int Save(std::ostream& stream) const override
   * @brief Save to a given binary \p stream
   * @param[in] stream Binary stream to save model from
   * @return    -1 if error, 0 otherwise
   */
  int Save(std::ostream& stream) const override;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize() const override
   * @brief Compute object size in byte
   * @return    Object's size
   */
  int ComputeObjectSize() const override;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Project
   * @fn    void Project(const Mesh& instance,
                         const cv::Mat& data,
                         const bool p_scaled,
                         cv::Mat* p) const
   * @brief Estimate parameters for a given instance
   * @param[in] instance    Instance for which parameters need to be estimated
   * @param[in] data        Extra data need for sampling (i.e. texture map)
   * @param[in] p_scaled    True indicate that coefficient will relative to
   *                        eigenvalues
   * @param[out] p          Estimated parameters
   */
  void Project(const Mesh& instance,
               const cv::Mat& data,
               const bool p_scaled,
               cv::Mat* p) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_dims
   * @fn    const std::vector<int>& get_dims() const
   * @brief Give each shape dimensions
   * @return    List of shape's dimensions
   */
  const std::vector<int>& get_dims() const {
    return dim_;
  }

  /**
   * @name  get_triangles
   * @fn    const std::vector<Triangle>& get_triangles() const
   * @brief Provide surface triangulation
   * @return    List of triangle
   */
  const std::vector<Triangle>& get_triangles() const {
    return this->tri_;
  }

  /**
   * @name  get_type
   * @fn    HeaderObjectType get_type() const override
   * @brief Indicate dynamically the underlying type of the object
   * @return    Object type
   */
  HeaderObjectType get_type() const override {
    return HeaderObjectType::kMultiShapeModel;
  }

#pragma mark -
#pragma mark Private
 private:
  /** Models dimensions */
  std::vector<int> dim_;
};


template<typename T>
class LTS5_EXPORTS MultiShapeSegmentModel : public MultiShapeModel<T> {
 public:

#pragma mark -
#pragma mark Type Definition

  /** Mesh type */
  using Mesh = typename PCAModel<T>::Mesh;
  /** Triangle type */
  using Triangle = typename PCAModel<T>::Mesh::Triangle;

#pragma mark -
#pragma mark Expose overlaoded function

  // Expose base class functions
  using ShapeModel<T>::Save;
  using ShapeModel<T>::Load;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  MultiShapeSegmentModel
   * @fn    MultiShapeSegmentModel()
   * @brief Constructor
   */
  MultiShapeSegmentModel() = default;

  /**
   * @name  MultiShapeSegmentModel
   * @fn    MultiShapeSegmentModel(const cv::Mat& mean,
                            const cv::Mat& variation,
                            const cv::Mat& prior,
                            const int& n_channels,
                            const cv::Mat& mixing,
                            const int& n_segment,
                            const std::vector<Triangle>& tri,
                            const std::vector<int>& landmarks,
                            const std::vector<T>& cog,
                            const std::vector<int>& dims)
   * @brief Specialized constructor
   * @param[in] mean        Mean value
   * @param[in] variation   Variation matrix (Eigen vectors in column)
   * @param[in] prior       Prior (Eigen values)
   * @param[in] n_channels  Number of channels in the flattened mean.
   * @param[in] mixing      Mixing matrix for each segment. Must have the same
   *                        number of rows as the number of segement defined.
   * @param[in] n_segment   Number of segment defined
   * @param[in] tri         Triangulation
   * @param[in] landmarks   Facial landmark's indexes
   * @param[in] cog         Center of gravity of the shape model (offset)
   * @param[in] dims        Each shape dimensions. Must sum to the number of
   *                        principal components
   */
  MultiShapeSegmentModel(const cv::Mat& mean,
                         const cv::Mat& variation,
                         const cv::Mat& prior,
                         const int& n_channels,
                         const cv::Mat& mixing,
                         const int& n_segment,
                         const std::vector<Triangle>& tri,
                         const std::vector<int>& landmarks,
                         const std::vector<T>& cog,
                         const std::vector<int>& dims);

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream) override
   * @brief Load from a given binary \p stream
   * @param[in] stream Binary stream to load model from
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream) override;

  /**
   * @name  Save
   * @fn    int Save(std::ostream& stream) const override
   * @brief Save to a given binary \p stream
   * @param[in] stream Binary stream to save model from
   * @return    -1 if error, 0 otherwise
   */
  int Save(std::ostream& stream) const override;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize() const override
   * @brief Compute object size in byte
   * @return    Object's size
   */
  int ComputeObjectSize() const override;

#pragma mark -
#pragma mark Usage


#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_n_segment
   * @fn    int get_n_segment() const
   * @brief Indicate the number of segment in the model
   * @return    Number of segment defined in the model
   */
  int get_n_segment() const {
    return n_segment_;
  }

  /**
   * @name  get_mixing
   * @fn    const cv::Mat& get_mixing() const
   * @brief Provide mixing matrix for blending each segment together
   * @return    Mixing matrix
   */
  const cv::Mat& get_mixing() const {
    return mixing_;
  }

  /**
   * @name  get_type
   * @fn    HeaderObjectType get_type() const override
   * @brief Indicate dynamically the underlying type of the object
   * @return    Object type
   */
  HeaderObjectType get_type() const override {
    return HeaderObjectType::kMultiShapeSegmentModel;
  }

#pragma mark -
#pragma mark Private
 private:
  /**
   * @name  LoadMixingMatrix
   * @brief Load mixing matrix into the object
   * @param[in] stream Binary stream storing data
   * @return -1 if error, 0 otherwise
   */
  int LoadMixingMatrix(std::istream& stream);

  /** Number of segment in the model */
  int n_segment_;
  /** Mixing matrix */
  cv::Mat mixing_;
};





/**
 * @class   MultiShapeModelProxy
 * @brief   Registration mechanism for PCAModel type
 * @author  Christophe Ecabert
 * @date    14/11/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS MultiShapeModelProxy : public PCAModelProxy<T> {
 public:

  /**
   * @name  MultiShapeModelProxy
   * @fn    MultiShapeModelProxy()
   * @brief Constructor
   */
  MultiShapeModelProxy() = default;

  /**
   * @name  ~MultiShapeModelProxy()
   * @fn    ~MultiShapeModelProxy() = default
   * @brief Destructor
   */
  ~MultiShapeModelProxy() = default;

  /**
   * @name  Create
   * @fn    PCAModel<T>* Create() const
   * @brief Create a PCAModel instance for a given type
   * @return Instance of a given class
   */
  PCAModel<T>* Create() const;

  /**
   * @name  Type
   * @fn    HeaderObjectType Type() const
   * @brief Provide the type represented by this proxy
   * @return    Object type
   */
  HeaderObjectType Type() const;

  /**
   * @name  Name
   * @fn    const char* Name() const
   * @brief Provide the name represented by this proxy
   * @return    Object name
   */
  const char* Name() const;
};

/**
 * @class   MultiShapeSegmentModelProxy
 * @brief   Registration mechanism for PCAModel type
 * @author  Christophe Ecabert
 * @date    14/11/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS MultiShapeSegmentModelProxy : public PCAModelProxy<T> {
 public:

  /**
   * @name  MultiShapeSegmentModelProxy
   * @fn    MultiShapeSegmentModelProxy()
   * @brief Constructor
   */
  MultiShapeSegmentModelProxy() = default;

  /**
   * @name  ~MultiShapeSegmentModelProxy()
   * @fn    ~MultiShapeSegmentModelProxy() = default
   * @brief Destructor
   */
  ~MultiShapeSegmentModelProxy() = default;

  /**
   * @name  Create
   * @fn    PCAModel<T>* Create() const
   * @brief Create a PCAModel instance for a given type
   * @return Instance of a given class
   */
  PCAModel<T>* Create() const;

  /**
   * @name  Type
   * @fn    HeaderObjectType Type() const
   * @brief Provide the type represented by this proxy
   * @return    Object type
   */
  HeaderObjectType Type() const;

  /**
   * @name  Name
   * @fn    const char* Name() const
   * @brief Provide the name represented by this proxy
   * @return    Object name
   */
  const char* Name() const;
};



}  // namepsace LTS5
#endif //__LTS5_MULTI_SHAPE_MODEL__
