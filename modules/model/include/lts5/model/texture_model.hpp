/**
 *  @file   texture_model.hpp
 *  @brief  Statistical texture model. Use texture map instead of per-vertex
 *          color
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_TEXTURE_MODEL__
#define __LTS5_TEXTURE_MODEL__

#include "lts5/utils/library_export.hpp"
#include "lts5/model/pca_model.hpp"
#include "lts5/geometry/mesh.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   TextureModel
 * @brief   Statistical texture model. Use texture map instead of per-vertex
 *          color
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS TextureModel : public PCAModel<T> {
 public:

#pragma mark -
#pragma mark Type Definition

  /** Mesh type */
  using Mesh = typename PCAModel<T>::Mesh;
  /** Texture coordinate type */
  using TCoord = typename Mesh::TCoord;
  /** Instance Type */
  using InstanceType = typename PCAModel<T>::InstanceType;
  /** ImagePoint type */
  using ImagePoint = typename PCAModel<T>::ImagePoint;

#pragma mark -
#pragma mark Initialization

/**
   * @name  TextureModel
   * @fn    TextureModel() = default
   * @brief Constructor
   */
  TextureModel() = default;

  /**
   * @name  TextureModel
   * @fn    TextureModel(const TextureModel& other) = delete
   * @brief Copy Constructor
   * @param[in] other   Object to copy from
   */
  TextureModel(const TextureModel& other) = delete;

  /**
   * @name  operator=
   * @fn    TextureModel& operator=(const TextureModel& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  TextureModel& operator=(const TextureModel& rhs) = delete;

  /**
   * @name  TextureModel
   * @fn    TextureModel(TextureModel&& other) = delete
   * @brief Move Constructor
   * @param[in] other   Object to move from
   */
  TextureModel(TextureModel&& other) = delete;

  /**
   * @name  operator=
   * @fn    TextureModel& operator=(TextureModel&& rhs) = delete
   * @brief Move assignment operator
   * @param[in] rhs Object to move assign from
   * @return    Newly moved assign object
   */
  TextureModel& operator=(TextureModel&& rhs) = delete;

  /**
   * @name  Load
   * @fn    int Load(const std::string& filename)
   * @brief Load from a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream)
   * @brief Load from a given binary \p stream
   * @param[in] stream Binary stream to load model from
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream);

  /**
   * @name  Save
   * @fn    int Save(const std::string& filename) const
   * @brief Save to a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  int Save(const std::string& filename) const;

  /**
   * @name  Save
   * @fn    int Save(std::ostream& stream) const
   * @brief Save to a given binary \p stream
   * @param[in] stream Binary stream to save model from
   * @return    -1 if error, 0 otherwise
   */
  int Save(std::ostream& stream) const;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize() const
   * @brief Compute object size in byte
   * @return    Object's size
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Project
   * @fn    void Project(const Mesh& instance,
                         const cv::Mat& data,
                         const bool& p_scaled,
                         cv::Mat* p) const
   * @brief Estimate parameters for a given instance
   * @param[in] instance    Instance for which parameters need to be estimated
   * @param[in] data        Extra data need for sampling (i.e. texture map)
   * @param[in] p_scaled    True indicate that coefficient will relative to
   *                        eigenvalues
   * @param[out] p          Estimated parameters
   */
  void Project(const Mesh& instance,
               const cv::Mat& data,
               const bool& p_scaled,
               cv::Mat* p) const;

  /**
   * @name  Sample
   * @fn    void Sample(const Mesh& instance,
                        const cv::Mat& data,
                        const std::vector<ImagePoint>& pts,
                        const InstanceType& type,
                        cv::Mat* sample) const
   * @param[in] instance    Mesh instance
   * @param[in] data        Extra data need for sampling (i.e. texture map)
   * @param[in] pts         List of point where to sample
   * @param[in] type        Type of sampling
   * @param[out] sample     Sampled elements
   */
  void Sample(const Mesh& instance,
              const cv::Mat& data,
              const std::vector<ImagePoint>& pts,
              const InstanceType& type,
              cv::Mat* sample) const;

  /**
   * @name  Generate
   * @fn    void Generate(const cv::Mat& p, const bool& p_scaled,
                            Mesh* instance) const
   * @brief Generate an instance given a set of coefficients \p p.
   * @param[in] p   Nodel's coefficients
   * @param[in] p_scaled    True indicate that coefficient are relative to
   *                        eigenvalues
   * @param[out] instance   Generated instance
   */
  void Generate(const cv::Mat& p, const bool& p_scaled, Mesh* instance) const;

  /**
   * @name  Generate
   * @fn    void Generate(Mesh* instance) const
   * @brief Generate a random instance
   * @param[out] instance   Randomly generated instance
   */
  void Generate(Mesh* instance) const;

  /**
   * @name  Generate
   * @fn    void Generate(cv::Mat* p, Mesh* instance) const
   * @brief Generate a random instance
   * @param[out] p  Parameters used for instance generation (scaled)
   * @param[out] instance   Randomly generated instance
   */
  void Generate(cv::Mat* p, Mesh* instance) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_type
   * @fn    HeaderObjectType get_type() const override
   * @brief Indicate dynamically the underlying type of the object
   * @return    Object type
   */
  HeaderObjectType get_type() const override {
    return HeaderObjectType::kTextureModel;
  }

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  LoadTCoord
   * @fn    int LoadTCoord(std::istream& stream)
   * @brief Load texture coordinates from a given \p stream.
   * @param[in] stream Binary stream to read tcoords from.
   * @return  -1 if error, 0 otherwise
   */
  int LoadTCoord(std::istream& stream);

  /** TCoords */
  std::vector<TCoord> tcoords_;
  /** Number of Texture Coordinates */
  size_t n_tcoord_;
};

/**
 * @class   TextureModelProxy
 * @brief   Registration mechanism for PCAModel type
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class TextureModelProxy : public PCAModelProxy<T> {
 public:

  /**
   * @name  TextureModelProxy
   * @fn    TextureModelProxy()
   * @brief Constructor
   */
  TextureModelProxy() = default;

  /**
   * @name  ~TextureModelProxy()
   * @fn    ~TextureModelProxy() = default
   * @brief Destructor
   */
  ~TextureModelProxy() = default;

  /**
   * @name  Create
   * @fn    PCAModel<T>* Create() const
   * @brief Create a PCAModel instance for a given type
   * @return Instance of a given class
   */
  PCAModel<T>* Create() const;

  /**
   * @name  Type
   * @fn    HeaderObjectType Type() const
   * @brief Provide the type represented by this proxy
   * @return    Object type
   */
  HeaderObjectType Type() const;

  /**
   * @name  Name
   * @fn    const char* Name() const
   * @brief Provide the name represented by this proxy
   * @return    Object name
   */
  const char* Name() const;
};


}  // namepsace LTS5
#endif //__LTS5_TEXTURE_MODEL__