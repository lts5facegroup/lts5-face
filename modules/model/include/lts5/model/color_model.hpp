/**
 *  @file   color_model.hpp
 *  @brief  Statistical per vertex color model
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_COLOR_MODEL__
#define __LTS5_COLOR_MODEL__

#include "lts5/utils/library_export.hpp"
#include "lts5/model/pca_model.hpp"
#include "lts5/geometry/mesh.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   ColorModel
 * @brief   Statistical per vertex color model
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T Data type
 */
template<typename T>
class LTS5_EXPORTS ColorModel : public PCAModel<T> {
 public:

#pragma mark -
#pragma mark Type Definition

  /** Property */
  using Property = typename PCAModel<T>::Property;
  /** Mesh Type */
  using Mesh = typename PCAModel<T>::Mesh;
  /** Instance Type */
  using InstanceType = typename PCAModel<T>::InstanceType;
  /** ImagePoint type */
  using ImagePoint = typename PCAModel<T>::ImagePoint;


#pragma mark -
#pragma mark Initialization
  // Overloaded virtual function
  // see https://stackoverflow.com/questions/17453609
  using PCAModel<T>::Generate;

  /**
   * @name  ColorModel
   * @fn    ColorModel() = default
   * @brief Constructor
   */
  ColorModel() = default;

  /**
   * @name  ColorModel
   * @fn    ColorModel(const cv::Mat& mean,
                       const cv::Mat& variation,
                       const cv::Mat& prior,
                       const int& n_channels)
   * @brief Specialized constructor
   * @param[in] mean        Mean value
   * @param[in] variation   Variation matrix (Eigen vectors in column)
   * @param[in] prior       Prior (Eigen values)
   * @param[in] n_channels  Number of channels in the flattened mean.
   */
  ColorModel(const cv::Mat& mean,
             const cv::Mat& variation,
             const cv::Mat& prior,
             const int& n_channels);

  /**
   * @name  ColorModel
   * @fn    ColorModel(const ColorModel& other) = delete
   * @brief Copy Constructor
   * @param[in] other   Object to copy from
   */
  ColorModel(const ColorModel& other) = delete;

  /**
   * @name  operator=
   * @fn    ColorModel& operator=(const ColorModel& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  ColorModel& operator=(const ColorModel& rhs) = delete;

  /**
   * @name  ColorModel
   * @fn    ColorModel(ColorModel&& other) = delete
   * @brief Move Constructor
   * @param[in] other   Object to move from
   */
  ColorModel(ColorModel&& other) = delete;

  /**
   * @name  operator=
   * @fn    ColorModel& operator=(ColorModel&& rhs) = delete
   * @brief Move assignment operator
   * @param[in] rhs Object to move assign from
   * @return    Newly moved assign object
   */
  ColorModel& operator=(ColorModel&& rhs) = delete;

  /**
   * @name  Load
   * @fn    int Load(const std::string& filename) override
   * @brief Load from a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  int Load(const std::string& filename) override;

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream) override
   * @brief Load from a given binary \p stream
   * @param[in] stream Binary stream to load model from
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream) override;

  /**
   * @name  Save
   * @fn    int Save(const std::string& filename) const override
   * @brief Save to a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  int Save(const std::string& filename) const override;

  /**
   * @name  Save
   * @fn    int Save(std::ostream& stream) const override
   * @brief Save to a given binary \p stream
   * @param[in] stream Binary stream to save model from
   * @return    -1 if error, 0 otherwise
   */
  int Save(std::ostream& stream) const override;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize() const override
   * @brief Compute object size in byte
   * @return    Object's size
   */
  int ComputeObjectSize() const override;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Project
   * @fn    virtual void Project(const Mesh& instance,
                                 const cv::Mat& data,
                                 const bool& p_scaled,
                                 cv::Mat* p) const override
   * @brief Estimate parameters for a given instance
   * @param[in] instance    Instance for which parameters need to be estimated
   * @param[in] data        Extra data need for sampling (i.e. texture map)
   * @param[in] p_scaled    True indicate that coefficient will relative to
   *                        eigenvalues
   * @param[out] p          Estimated parameters
   */
  void Project(const Mesh& instance,
               const cv::Mat& data,
               const bool& p_scaled,
               cv::Mat* p) const override;

  /**
   * @name  Sample
   * @fn    void virtual Sample(const Mesh& instance,
                                const cv::Mat& data,
                                const std::vector<ImagePoint>& pts,
                                const InstanceType& type,
                                cv::Mat* sample) const override
   * @param[in] instance    Mesh instance
   * @param[in] data        Extra data need for sampling (i.e. texture map)
   * @param[in] pts         List of point where to sample
   * @param[in] type        Type of sampling
   * @param[out] sample     Sampled elements
   */
  void Sample(const Mesh& instance,
              const cv::Mat& data,
              const std::vector<ImagePoint>& pts,
              const InstanceType& type,
              cv::Mat* sample) const override;

  /**
   * @name  Generate
   * @fn    void Generate(const cv::Mat& p,
                          const bool& p_scaled,
                          Mesh* instance) const override
   * @brief Generate an instance given a set of coefficients \p p.
   * @param[in] p   Nodel's coefficients
   * @param[in] p_scaled    True indicate that coefficient are relative to
   *                        eigenvalues
   * @param[out] instance   Generated instance
   */
  void Generate(const cv::Mat& p, 
                const bool& p_scaled, 
                Mesh* instance) const override;

  /**
   * @name  Generate
   * @fn    void Generate(Mesh* instance) const override
   * @brief Generate a random instance
   * @param[out] instance   Randomly generated instance
   */
  void Generate(Mesh* instance) const override;

  /**
   * @name  Generate
   * @fn    void Generate(cv::Mat* p, Mesh* instance) const override
   * @brief Generate a random instance
   * @param[out] p  Parameters used for instance generation (scaled)
   * @param[out] instance   Randomly generated instance
   */
  void Generate(cv::Mat* p, Mesh* instance) const override;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_n_texel
   * @fn    size_t get_n_texel() const
   * @brief Provide number of texel in the color model
   * @return number of texels
   */
  size_t get_n_texel() const {
    return n_texel_;
  }

  /**
   * @name  get_property
   * @fn    virtual size_t get_property(const Property prop) const 
            override
   * @brief Access property by a given index
   * @param[in] prop    Property index
   * @return    Property value
   */
  size_t get_property(const Property prop) const override {
    size_t p = std::numeric_limits<size_t>::max();
    if (prop == Property::kTexel) {
      p = n_texel_;
    }
    return p;
  }

  /**
   * @name  get_type
   * @fn    HeaderObjectType get_type() const override
   * @brief Indicate dynamically the underlying type of the object
   * @return    Object type
   */
  HeaderObjectType get_type() const override {
    return HeaderObjectType::kColorModel;
  }

#pragma mark -
#pragma mark Private
 private:

  /** Number of Texel */
  size_t n_texel_;
};

/**
 * @class   ColorModelProxy
 * @brief   Registration mechanism for PCAModel type
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class ColorModelProxy : public PCAModelProxy<T> {
 public:

  /**
   * @name  ColorModelProxy
   * @fn    ColorModelProxy()
   * @brief Constructor
   */
  ColorModelProxy() = default;

  /**
   * @name  ~ColorModelProxy()
   * @fn    ~ColorModelProxy() = default
   * @brief Destructor
   */
  ~ColorModelProxy() = default;

  /**
   * @name  Create
   * @fn    PCAModel<T>* Create() const
   * @brief Create a PCAModel instance for a given type
   * @return Instance of a given class
   */
  PCAModel<T>* Create() const;

  /**
   * @name  Type
   * @fn    HeaderObjectType Type() const
   * @brief Provide the type represented by this proxy
   * @return    Object type
   */
  HeaderObjectType Type() const;

  /**
   * @name  Name
   * @fn    const char* Name() const
   * @brief Provide the name represented by this proxy
   * @return    Object name
   */
  const char* Name() const;
};

}  // namepsace LTS5
#endif //__LTS5_COLOR_MODEL__
