/**
 *  @file   bilinear_face_model_converter.hpp
 *  @brief  Convert Bilinear Face model (Ho-SVD) into a blendshape model.
 *  @ingroup model
 *
 *  @author Christophe Ecabert
 *  @date   03.10.16
 *  Copyright (c) 2016 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_BILINEAR_FACE_MODEL_CONVERTER__
#define __LTS5_BILINEAR_FACE_MODEL_CONVERTER__

#include "opencv2/core/core.hpp"
#include "vmmlib/tensor3.hpp"
#include "vmmlib/matrix.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BilinearFaceModelConverter
 *  @brief  Convert bilinear face model to blendshape model
 *  @author Christophe Ecabert
 *  @date   03/10/16
 *  @ingroup model
 */
class LTS5_EXPORTS BilinearFaceModelConverter {

 public:

#pragma mark -
#pragma mark Type definitions

  /** Vertex dimension in core tensor (Number of Vertex * 3) */
  static constexpr int VertexSize = 24780;
  /** Identity dimension in core tensor (Compressed) */
  static constexpr int CIdentitySize = 50;
  /** Expression dimension in core tensor (Compressed) */
  static constexpr int CExpressionSize = 25;
  /** Expression dimension in core tensor (UnCompressed) */
  static constexpr int ExpressionSize = 47;

  /**
   *  @enum   ContainerType
   *  @brief  Type of container
   */
  enum ContainerType {
    /** OpenCV */
    kOpenCV,
    /** VMML */
    kVmml
  };


  /** Core tensor type */
  using CoreTensor = vmml::tensor3<VertexSize, CIdentitySize, CExpressionSize>;
  /** Blendshape tensor type */
  using BlendshapeTensor = vmml::tensor3<VertexSize, CIdentitySize, ExpressionSize>;
  /** Basis matrix type */
  using ExprBasis = vmml::matrix<ExpressionSize, CExpressionSize>;

#pragma mark -
#pragma mark Initialization

  /**
   *  @name   BilinearFaceModelConverter
   *  @fn     BilinearFaceModelConverter(void)
   *  @brief  Constructor
   */
  BilinearFaceModelConverter(void);

  /**
   *  @name   ~BilinearFaceModelConverter
   *  @fn     ~BilinearFaceModelConverter(void)
   *  @brief  Destructor
   */
  ~BilinearFaceModelConverter(void);

  /**
   *  @name   BilinearFaceModelConverter
   *  @fn     BilinearFaceModelConverter(const BilinearFaceModelConverter& other)
   *  @brief  Copy Constructor
   *  @param[in]  other Object to copy from
   */
  BilinearFaceModelConverter(const BilinearFaceModelConverter& other) = delete;

  /**
   *  @name operator=
   *  @fn BilinearFaceModelConverter& operator=(const BilinearFaceModelConverter& rhs)
   *  @brief  Assignement operator
   *  @param[in]  rhs Object to assign from
   *  @return Newly assigned operator
   */
  BilinearFaceModelConverter& operator=(const BilinearFaceModelConverter& rhs) = delete;

  /**
   *  @name LoadModel
   *  @fn int LoadModel(const std::string& path)
   *  @brief  Load data from bilinear model file
   *  @param[in]  path          Path to model file
   *  @return -1 if error, 0 otherwise
   */
  int LoadModel(const std::string& path);

  /**
   *  @name LoadBasis
   *  @fn int LoadBasis(const std::string& path)
   *  @brief  Load basis data from tensor decomposition
   *  @param[in]  path          Path to model file
   *  @return -1 if error, 0 otherwise
   */
  int LoadBasis(const std::string& path);

#pragma mark -
#pragma mark Usage

  /**
   *  @name   ConvertTo
   *  @fn     void ConvertTo(ContainerType type)
   *  @brief  Convert face model to vmml format
   *  @param[in]  type  Type of conversion
   *                    OpenCV -> VMML, VMML -> OpenCV
   */
  void ConvertTo(ContainerType type);

  /**
   *  @name   GenerateBlendshapeModel
   *  @fn     void GenerateBlendshapeModel(void)
   *  @brief  Generate blendshape model from bilinear face model
   */
  void GenerateBlendshapeModel(void);

  /**
   *  @name   Save
   *  @fn     int Save(const std::string& output_name)
   *  @brief  Save blendshape model to file
   *  @param[in]  output_name Model filename to save as
   *  @return -1 if error, 0 otherwise
   */
  int Save(const std::string& output_name);

#pragma mark -
#pragma mark Private

 private:
  /** Core face model */
  cv::Mat core_;
  /** Meanshape */
  cv::Mat meanshape_;
  /** Neutral expression */
  cv::Mat neutral_exp_;
  /** Average identity coefficient */
  cv::Mat average_id_;
  /** Core tensor - vmmlib format */
  CoreTensor tensor_;
  /** Expression basis */
  ExprBasis expr_basis_;
  /** Blendshape tensor */
  BlendshapeTensor blendshape_tensor_;

};
}  // namespace LTS5
#endif /* __LTS5_BILINEAR_FACE_MODEL_CONVERTER__ */
