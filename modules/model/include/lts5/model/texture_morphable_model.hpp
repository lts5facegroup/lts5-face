/**
 *  @file   texture_morphable_model.hpp
 *  @brief  Morphable model with texture map as color scheme
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_TEXTURE_MORPHABLE_MODEL__
#define __LTS5_TEXTURE_MORPHABLE_MODEL__

#include "lts5/utils/library_export.hpp"
#include "lts5/model/morphable_model.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   TextureMorphableModel
 * @brief   Morphable model with texture map as color scheme
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS TextureMorphableModel : public MorphableModel<T> {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  TextureMorphableModel
   * @fn    TextureMorphableModel() = default
   * @brief Destructor
   */
  TextureMorphableModel() = default;

  /**
   * @name  ~TextureMorphableModel
   * @fn    ~TextureMorphableModel() = default
   * @brief Destructor
   */
  ~TextureMorphableModel() = default;

  /**
   * @name  Load
   * @fn    int Load(const std::string& filename)
   * @brief Load from a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream)
   * @brief Load from a given binary \p stream
   * @param[in] stream Binary stream to load model from
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream);

  /**
   * @name  Save
   * @fn    int Save(const std::string& filename) const
   * @brief Save to a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  int Save(const std::string& filename) const;

  /**
   * @name  Save
   * @fn    int Save(std::ostream& stream) const
   * @brief Save to a given binary \p stream
   * @param[in] stream Binary stream to save model from
   * @return    -1 if error, 0 otherwise
   */
  int Save(std::ostream& stream) const;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize() const
   * @brief Compute object size in byte
   * @return    Object's size
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Usage

#pragma mark -
#pragma mark Accessors

#pragma mark -
#pragma mark Private

};

/**
 * @class   TextureMorphableModelProxy
 * @brief   Registration mechanism for Morphable model type
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class TextureMorphableModelProxy : public MorphableModelProxy<T> {
 public:

  /**
   * @name  TextureMorphableModelProxy
   * @fn    TextureMorphableModelProxy()
   * @brief Constructor
   */
  TextureMorphableModelProxy() = default;

  /**
   * @name  ~TextureMorphableModelProxy()
   * @fn    ~TextureMorphableModelProxy() = default
   * @brief Destructor
   */
  ~TextureMorphableModelProxy() = default;

  /**
   * @name  Create
   * @fn    MorphableModel<T>* Create() const
   * @brief Create a MorphableModel instance for a given type
   * @return Instance of a given class
   */
  MorphableModel<T>* Create() const;

  /**
   * @name  Type
   * @fn    HeaderObjectType Type() const
   * @brief Provide the type represented by this proxy
   * @return    Object type
   */
  HeaderObjectType Type() const;

  /**
   * @name  Name
   * @fn    const char* Name() const
   * @brief Provide the name represented by this proxy
   * @return    Object name
   */
  const char* Name() const;
};


}  // namepsace LTS5
#endif //__LTS5_TEXTURE_MORPHABLE_MODEL__
