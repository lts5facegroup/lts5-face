/**
 *  @file   color_morphable_model.hpp
 *  @brief  Morphable model with per-vertex color scheme such as BFM
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_COLOR_MORPHABLE_MODEL__
#define __LTS5_COLOR_MORPHABLE_MODEL__

#include "lts5/utils/library_export.hpp"
#include "lts5/model/morphable_model.hpp"
#include "lts5/model/color_model.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   ColorMorphableModel
 * @brief   Morphable model with per-vertex color scheme such as BFM
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS ColorMorphableModel : public MorphableModel<T> {
 public:

#pragma mark -
#pragma mark Type definition

  /** Mesh Type */
  using Mesh = typename MorphableModel<T>::Mesh;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  ColorMorphableModel
   * @fn    ColorMorphableModel() = default
   * @brief Destructor
   */
  ColorMorphableModel() = default;

  /**
   * @name  ColorMorphableModel
   * @fn    ColorMorphableModel(PCAModel<T>* shape,
                                ColorModel<T>* color)
   * @brief Constructor
   * @param[in] shape Statistical shape model to use, take ownership
   * @param[in] color Statistical ColorModel to use, take ownership
   */
  ColorMorphableModel(PCAModel<T>* shape,
                      ColorModel<T>* color);



  /**
   * @name  ~ColorMorphableModel
   * @fn    ~ColorMorphableModel() = default
   * @brief Destructor
   */
  ~ColorMorphableModel() = default;

  /**
   * @name  Load
   * @fn    int Load(const std::string& filename)
   * @brief Load from a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  int Load(const std::string& filename);

  /**
   * @name  Load
   * @fn    int Load(std::istream& stream)
   * @brief Load from a given binary \p stream
   * @param[in] stream Binary stream to load model from
   * @return    -1 if error, 0 otherwise
   */
  int Load(std::istream& stream);

  /**
   * @name  Save
   * @fn    int Save(const std::string& filename) const
   * @brief Save to a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  int Save(const std::string& filename) const;

  /**
   * @name  Save
   * @fn    int Save(std::ostream& stream) const
   * @brief Save to a given binary \p stream
   * @param[in] stream Binary stream to save model from
   * @return    -1 if error, 0 otherwise
   */
  int Save(std::ostream& stream) const;

  /**
   * @name  ComputeObjectSize
   * @fn    int ComputeObjectSize() const
   * @brief Compute object size in byte
   * @return    Object's size
   */
  int ComputeObjectSize() const;

#pragma mark -
#pragma mark Usage

  /**
   * @name  AddIllumination
   * @fn    void AddIllumination(const Matrix4x4<T>& normal_matrix, const cv::Mat& ip,
                                 Mesh* instance) const
   * @brief Add illumination contribution to the generated texture using
   *        spherical harmonics approximation. Should be implemented by derived
   *        model
   * @param[in] normal_matrix   Transformation matrix to go from model space into
   *                            camera space: Transform = (Model^-1)^T
   * @param[in] ip  Illumination parameters
   * @param[in,out] instance    Mesh instance where to add illumination
   */
  void AddIllumination(const Matrix4x4<T>& normal_matrix,
                       const cv::Mat& ip,
                       Mesh* instance) const;

#pragma mark -
#pragma mark Accessors

#pragma mark -
#pragma mark Private

};

/**
 * @class   ColorMorphableModelProxy
 * @brief   Registration mechanism for Morphable model type
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class ColorMorphableModelProxy : public MorphableModelProxy<T> {
 public:

  /**
   * @name  ColorMorphableModelProxy
   * @fn    ColorMorphableModelProxy()
   * @brief Constructor
   */
  ColorMorphableModelProxy() = default;

  /**
   * @name  ~ColorMorphableModelProxy()
   * @fn    ~ColorMorphableModelProxy() = default
   * @brief Destructor
   */
  ~ColorMorphableModelProxy() = default;

  /**
   * @name  Create
   * @fn    MorphableModel<T>* Create() const
   * @brief Create a MorphableModel instance for a given type
   * @return Instance of a given class
   */
  MorphableModel<T>* Create() const;

  /**
   * @name  Type
   * @fn    HeaderObjectType Type() const
   * @brief Provide the type represented by this proxy
   * @return    Object type
   */
  HeaderObjectType Type() const;

  /**
   * @name  Name
   * @fn    const char* Name() const
   * @brief Provide the name represented by this proxy
   * @return    Object name
   */
  const char* Name() const;
};


}  // namepsace LTS5
#endif //__LTS5_COLOR_MORPHABLE_MODEL__
