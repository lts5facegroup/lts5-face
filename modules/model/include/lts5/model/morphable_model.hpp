/**
 *  @file   lts5/model/morphable_model.hpp
 *  @brief  Morphable model composed of two component, shape_model and
 *          texture_model
 *  @ingroup model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_MORPHABLE_MODEL__
#define __LTS5_MORPHABLE_MODEL__

#include <memory>

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/matrix.hpp"
#include "lts5/model/pca_model.hpp"
#include "lts5/model/shape_model.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   MorphableModel
 * @brief   Morphable model composed of two component, shape_model and
 *          texture_model
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS MorphableModel {
 public:

#pragma mark -
#pragma mark Type Definition

  /** Mesh Type */
  using Mesh = typename PCAModel<T>::Mesh;
  /** Instance Type */
  using InstanceType = typename PCAModel<T>::InstanceType;
  /** ImagePoint Type */
  using ImagePoint = typename PCAModel<T>::ImagePoint;


#pragma mark -
#pragma mark Initialization

  /**
   * @name  MorphableModel
   * @fn    MorphableModel()
   * @brief Constructor
   */
  MorphableModel() = default;

  /**
   * @name  MorphableModel
   * @fn    MorphableModel(PCAModel<T>* shape, PCAModel<T>* color)
   * @brief Constructor
   * @param[in] shape Statistical shape model to use, take ownership.
   * @param[in] color Statistical color model to use, take ownership.
   */
  MorphableModel(PCAModel<T>* shape, PCAModel<T>* color);

  /**
   * @name  ~MorphableModel
   * @fn    virtual ~MorphableModel() = default
   * @brief Destructor
   */
  virtual ~MorphableModel() = default;

  /**
   * @name  Load
   * @fn    virtual int Load(const std::string& filename)
   * @brief Load from a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  virtual int Load(const std::string& filename);

  /**
   * @name  Load
   * @fn    virtual int Load(std::istream& stream)
   * @brief Load from a given binary \p stream
   * @param[in] stream Binary stream to load model from
   * @return    -1 if error, 0 otherwise
   */
  virtual int Load(std::istream& stream);

  /**
   * @name  Save
   * @fn    virtual int Save(const std::string& filename) const
   * @brief Save to a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  virtual int Save(const std::string& filename) const;

  /**
   * @name  Save
   * @fn    virtual int Save(std::ostream& stream) const
   * @brief Save to a given binary \p stream
   * @param[in] stream Binary stream to save model from
   * @return    -1 if error, 0 otherwise
   */
  virtual int Save(std::ostream& stream) const;

  /**
   * @name  ComputeObjectSize
   * @fn    virtual int ComputeObjectSize() const
   * @brief Compute object size in byte
   * @return    Object's size
   */
  virtual int ComputeObjectSize() const;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Project
   * @fn    void Project(const Mesh& instance,
                         const cv::Mat& data,
                         const bool& p_scaled,
                         const InstanceType& type,
                         cv::Mat* p) const
   * @brief Estimate parameters for a given instance (Shape or texture)
   * @param[in] instance    Instance for which parameters need to be estimated
   * @param[in] data        Extra data needed for projection (i.e. texture map)
   * @param[in] type        Type of instance
   * @param[in] p_scaled    True indicate that coefficient will relative to
 *                        eigenvalues
   * @param[out] p          Estimated parameters
   */
  void Project(const Mesh& instance,
               const cv::Mat& data,
               const InstanceType& type,
               const bool& p_scaled,
               cv::Mat* p) const;

  /**
   * @name  Sample
   * @fn    void Sample(const Mesh& instance,
                        const cv::Mat& data,
                        const std::vector<ImagePoint>& pts,
                        const InstanceType& type,
                        cv::Mat* sample) const
   * @param[in] instance    Mesh instance
   * @param[in] data        Extra data need for sampling (i.e. texture map)
   * @param[in] pts         List of point where to sample
   * @param[in] type        Type of sampling
   * @param[out] sample     Sampled elements
   */
  void Sample(const Mesh& instance,
              const cv::Mat& data,
              const std::vector<ImagePoint>& pts,
              const InstanceType& type,
              cv::Mat* sample) const;

  /**
   * @name  Generate
   * @fn    virtual void Generate(const cv::Mat& sp,
                        const cv::Mat& tp,
                        const bool& p_scaled,
                        Mesh* instance) const
   * @brief Generate an instance given a set of coefficients \p sp and \p tp.
   * @param[in] sp   Shape coefficients
   * @param[in] tp   Texture coefficients
   * @param[in] p_scaled    True indicate that coefficient are relative to
   *                        eigenvalues
   * @param[out] instance   Generated instance
   */
  virtual void Generate(const cv::Mat& sp,
                        const cv::Mat& tp,
                        const bool& p_scaled,
                        Mesh* instance) const;

  /**
   * @name  Generate
   * @fn    virtual void Generate(Mesh* instance) const
   * @brief Generate a random instance
   * @param[out] instance   Randomly generated instance
   */
  virtual void Generate(Mesh* instance) const;

  /**
   * @name  Generate
   * @fn    virtual void Generate(cv::Mat* sp, cv::Mat* tp, Mesh* instance) const
   * @brief Generate a random instance
   * @param[out] sp Shape parameters used while generating object
   * @param[out] tp Texture parameters used while generating object
   * @param[out] instance   Randomly generated instance
   */
  virtual void Generate(cv::Mat* sp, cv::Mat* tp, Mesh* instance) const;

  /**
   * @name  AddIllumination
   * @fn    virtual void AddIllumination(const Matrix4x4<T>& normal_matrix,
                                         const cv::Mat& ip,
                                         Mesh* instance) const
   * @brief Add illumination contribution to the generated texture using
   *        spherical harmonics approximation. Should be implemented by derived
   *        model
   * @param[in] normal_matrix   Transformation matrix to go from model space into
   *                            camera space: Transform = (Model^-1)^T
   * @param[in] ip  Illumination parameters
   * @param[in,out] instance    Mesh instance where to add illumination
   */
  virtual void AddIllumination(const Matrix4x4<T>& normal_matrix,
                               const cv::Mat& ip,
                               Mesh* instance) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_n_vertex
   * @fn    size_t get_n_vertex() const
   * @brief Provide number of vertices in the shape model
   * @return number of vertices
   */
  size_t get_n_vertex() const {
    return n_vertex_;
  }

  /**
   * @name  get_n_triangle
   * @fn    size_t get_n_triangle() const
   * @brief Provide number of triangles in the shape model
   * @return number of triangles
   */
  size_t get_n_triangle() const {
    return n_tri_;
  }

  /**
   * @name  get_n_texel
   * @fn    size_t get_n_texel() const
   * @brief Provide number of texel in the texture model
   * @return number of texel
   */
  size_t get_n_texel() const {
    return n_texel_;
  }

  /**
   * @name  get_active_shape_component
   * @fn    int get_active_shape_component() const
   * @brief Provide the number of principal component in the shape model
   * @return    Active component
   */
  int get_active_shape_component() const {
    return shape_->get_n_principle_component();
  }

  /**
   * @name  get_active_texture_component
   * @fn    int get_active_texture_component() const
   * @brief Provide the number of principal component in the texture model
   * @return    Active component
   */
  int get_active_texture_component() const {
    return texture_->get_n_principle_component();
  }

  /**
   * @name  get_active_illumination_component
   * @fn    int get_active_illumination_component() const
   * @brief Provide the number of coefficient used to model the illumination
   * @return    number of illumination coefficients
   */
  int get_active_illumination_component() const {
    return this->get_n_channels() * 9;
  }

  /**
   * @name  get_n_channels
   * @fn    int get_n_channels() const
   * @brief Indicate how many channels are used in texture model
   * @return    Amount of channels
   */
  int get_n_channels() const {
    return texture_->get_n_channels();
  }

  /**
   * @name  get_shape_prior
   * @fn    const cv::Mat& get_shape_prior() const
   * @brief Provide shape's prior
   * @return Shape's eigenvalues
   */
  const cv::Mat& get_shape_prior() const {
    return shape_->get_prior();
  }

  /**
   * @name  get_shape_inverse_prior
   * @fn    const cv::Mat& get_shape_inverse_prior() const
   * @brief Provide shape's inverse prior (i.e. 1 / sigma)
   * @return Shape's inverse eigenvalues
   */
  const cv::Mat& get_shape_inverse_prior() const {
    return shape_->get_inverse_prior();
  }

  /**
   * @name  get_texture_prior
   * @fn    const cv::Mat& get_texture_prior() const
   * @brief Provide texture's prior
   * @return Texture's eigenvalues
   */
  const cv::Mat& get_texture_prior() const {
    return texture_->get_prior();
  }

  /**
   * @name  get_texture_inverse_prior
   * @fn    const cv::Mat& get_texture_inverse_prior() const
   * @brief Provide texture's inverse prior (i.e. 1 / sigma)
   * @return Texture's inverse eigenvalues
   */
  const cv::Mat& get_texture_inverse_prior() const {
    return texture_->get_inverse_prior();
  }

  /**
   * @name  get_shape_principal_component
   * @fn    const cv::Mat& get_shape_principal_component() const
   * @brief Provide shape's principal_component
   * @return Shape's eigenvectors
   */
  const cv::Mat& get_shape_principal_component() const {
    return shape_->get_variation();
  }

  /**
   * @name  get_texture_principal_component
   * @fn    const cv::Mat& get_texture_principal_component() const
   * @brief Provide texture's principal_component
   * @return Texture's eigenvectors
   */
  const cv::Mat& get_texture_principal_component() const {
    return shape_->get_variation();
  }

  /**
   * @name  get_mean_shape
   * @fn    const cv::Mat& get_mean_shape() const
   * @brief Provide shape's mean value
   * @return Shape's mean value
   */
  const cv::Mat& get_mean_shape() const {
    return shape_->get_mean();
  }

  /**
   * @name  get_mean_texture
   * @fn    const cv::Mat& get_mean_texture() const
   * @brief Provide texture's mean value
   * @return Texture's mean value
   */
  const cv::Mat& get_mean_texture() const {
    return texture_->get_mean();
  }

  /**
   * @name  get_landmark_index
   * @fn    const std::vector<int>& get_landmark_index() const
   * @brief Provide list of index corresponding to face landmarks
   * @return    Landmark's index
   */
  const std::vector<int>& get_landmark_index() const {
    return reinterpret_cast<const ShapeModel<T>*>(shape_)->get_landmark_index();
  }

  /**
   * @name  get_mean_landmarks
   * @fn    const cv::Mat& get_mean_landmarks() const
   * @brief Provide mean landmarks
   * @return    Mean landmarks
   */
  const cv::Mat& get_mean_landmarks() const {
    return mean_landmarks_;
  }

  /**
   * @name  get_mean_landmarks
   * @fn    const cv::Mat& get_mean_landmarks() const
   * @brief Provide mean landmarks
   * @return    Mean landmarks
   */
  const cv::Mat& get_normalized_mean_landmarks() const {
    return normalized_mean_landmarks_;
  }

  /**
   * @name  get_shape_model
   * @fn    const PCAModel<T>* get_shape_model(void) const
   * @brief Expose underlying shape model. Keep owner ship of the pointer
   * @return    Pointer to an instance of PCAModel<T> or nullptr if not init.
   */
  PCAModel<T>* get_shape_model() {
    return shape_;
  }

  /**
   * @name  get_texture_model
   * @fn    const PCAModel<T>* get_texture_model(void) const
   * @brief Expose underlying texture model. Keep owner ship of the pointer
   * @return    Pointer to an instance of PCAModel<T> or nullptr if not init.
   */
  PCAModel<T>* get_texture_model() {
    return texture_;
  }


#pragma mark -
#pragma mark Protected

 protected:
  /** Shape model */
  PCAModel<T>* shape_;
  /** Texture model */
  PCAModel<T>* texture_;
  /** Mean landmarks */
  cv::Mat mean_landmarks_;
  /** Normalized Mean landmarks */
  cv::Mat normalized_mean_landmarks_;
  /** Number of Vertex */
  size_t n_vertex_;
  /** Number of Triangle */
  size_t n_tri_;
  /** Number of Texel */
  size_t n_texel_;

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  InitLandmarksVariable
   * @fn    void InitLandmarksVariable()
   * @brief Initialize variable related to landmarks
   */
  void InitLandmarksVariable();

  /**
   * @name  LoadShapeModel
   * @fn    int LoadShapeModel(std::istream& stream, HeaderObjectType type)
   * @brief Load shape model
   * @param[in] stream  Binary stream from which model need to be loaded
   * @param[in] type    Shape model's type
   * @return -1 if error, 0 otherwise
   */
  int LoadShapeModel(std::istream& stream, HeaderObjectType type);

  /**
   * @name  LoadTextureModel
   * @fn    int LoadTextureModel(std::istream& stream, HeaderObjectType type)
   * @brief Load texture model
   * @param[in] stream  Binary stream from which model need to be loaded
   * @param[in] type    Texture model's type
   * @return -1 if error, 0 otherwise
   */
  int LoadTextureModel(std::istream& stream, HeaderObjectType type);
};

/**
 * @class   MorphableModelProxy
 * @brief   Registration mechanism for Morphable model type
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class MorphableModelProxy {
 public:

  /**
   * @name  MorphableModelProxy
   * @fn    MorphableModelProxy()
   * @brief Constructor
   */
  MorphableModelProxy();

  /**
   * @name  ~MorphableModelProxy()
   * @fn    virtual ~MorphableModelProxy() = default
   * @brief Destructor
   */
  virtual ~MorphableModelProxy() = default;

  /**
   * @name  Create
   * @fn    virtual MorphableModel<T>* Create() const = 0
   * @brief Create a MorphableModel instance for a given type
   * @return Instance of a given class
   */
  virtual MorphableModel<T>* Create() const = 0;

  /**
   * @name  Type
   * @fn    virtual HeaderObjectType Type() const = 0
   * @brief Provide the type represented by this proxy
   * @return    Object type
   */
  virtual HeaderObjectType Type() const = 0;

  /**
   * @name  Name
   * @fn    virtual const char* Name() const = 0
   * @brief Provide the name represented by this proxy
   * @return    Object name
   */
  virtual const char* Name() const = 0;
};


}  // namepsace LTS5
#endif //__LTS5_MORPHABLE_MODEL__
