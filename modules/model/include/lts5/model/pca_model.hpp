/**
 *  @file   pca_model.hpp
 *  @brief  Statistical model based on Principle Component Analysis
 *  @ingroup model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_PCA_MODEL__
#define __LTS5_PCA_MODEL__

#include <iostream>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/object_type.hpp"
#include "lts5/geometry/mesh.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   PCAModel
 * @author  Christophe Ecabert
 * @brief   Interface for PCA model, used for shape/texture modelisation
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type.
 */
template<typename T>
class LTS5_EXPORTS PCAModel {
 public:
#pragma mark -
#pragma mark Type Definition

  /** Mesh type */
  using Mesh = LTS5::Mesh<T, Vector3<T>>;

  /**
   * @enum  Property
   * @brief List of properties that can be queried from PCA model
   */
  enum class Property : size_t {
    /** Vertex */
    kVertex,
    /** Texel */
    kTexel,
    /** Triangle */
    kTriangle
  };

  /**
   * @enum  InstanceType
   * @brief Type of instance
   */
  enum class InstanceType : char {
    /** Shape Instance */
    kShape,
    /** Mean shape */
    kMeanShape,
    /** Shape's normal */
    kShapeNormal,
    /** Shape variation Instance */
    kShapeVariation,
    /** Shape scaled variation Instance */
    kShapeScaledVariation,
    /** Texture instance */
    kTex,
    /** Mean texture */
    kMeanTex,
    /** Textire scaled variation Instance */
    kTexScaledVariation,
    /** Textire variation Instance */
    kTexVariation
  };

  /**
   * @struct    BCoord
   * @brief     Barycentric coordinate
   */
  struct BCoord {
    /** Triangle index */
    size_t t_idx;
    /** Vertex A index */
    size_t va_idx;
    /** Vertex B index */
    size_t vb_idx;
    /** Vertex C index */
    size_t vc_idx;
    /** Barycentric coordinate - alpha */
    T alpha;
    /** Barycentric coordinate - beta */
    T beta;
    /** Barycentric coordinate - beta */
    T gamma;
  };

  /**
   * @struct    ImagePoint
   * @brief     Coordinate in image space including corresponding barycentric
   *            coordinates
   */
  struct ImagePoint {
    /** X position */
    int x;
    /** Y position */
    int y;
    /** Correspondind barycentric coordinate */
    BCoord bcoord;
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name  PCAModel
   * @fn    PCAModel() = default
   * @brief Constructor, empty model
   */
  PCAModel() = default;

  /**
   * @name  PCAModel
   * @fn    PCAModel(const cv::Mat& mean,
                     const cv::Mat& variation,
                     const cv::Mat& prior,
                     const int& n_channels)
   * @brief Specialized constructor
   * @param[in] mean        Mean value
   * @param[in] variation   Variation matrix (Eigen vectors in column)
   * @param[in] prior       Prior (Eigen values)
   * @param[in] n_channels  Number of channels in the flattened mean.
   */
  PCAModel(const cv::Mat& mean,
           const cv::Mat& variation,
           const cv::Mat& prior,
           const int& n_channels);

  /**
   * @name  ~PCAModel
   * @fn    virtual ~PCAModel() = default
   * @brief Destructor
   */
  virtual ~PCAModel() = default;

  /**
   * @name  Load
   * @fn    virtual int Load(const std::string& filename)
   * @brief Load from a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  virtual int Load(const std::string& filename);

  /**
   * @name  Load
   * @fn    virtual int Load(std::istream& stream)
   * @brief Load from a given binary \p stream
   * @param[in] stream Binary stream to load model from
   * @return    -1 if error, 0 otherwise
   */
  virtual int Load(std::istream& stream);

  /**
   * @name  Save
   * @fn    virtual int Save(const std::string& filename) const
   * @brief Save to a given \p filename
   * @param[in] filename    Path to the model file
   * @return    -1 if error, 0 otherwise
   */
  virtual int Save(const std::string& filename) const;

  /**
   * @name  Save
   * @fn    virtual int Save(std::ostream& stream) const
   * @brief Save to a given binary \p stream
   * @param[in] stream Binary stream to save model from
   * @return    -1 if error, 0 otherwise
   */
  virtual int Save(std::ostream& stream) const;

  /**
   * @name  ComputeObjectSize
   * @fn    virtual int ComputeObjectSize() const
   * @brief Compute object size in byte
   * @return    Object's size
   */
  virtual int ComputeObjectSize() const;

#pragma mark -
#pragma mark Usage


  /**
   * @name  Project
   * @fn    virtual void Project(const Mesh& instance,
                                 const cv::Mat& data,
                                 const bool& p_scaled,
                                 cv::Mat* p) const = 0
   * @brief Estimate parameters for a given instance
   * @param[in] instance    Instance for which parameters need to be estimated
   * @param[in] data        Extra data need for sampling (i.e. texture map)
   * @param[in] p_scaled    True indicate that coefficient will relative to
   *                        eigenvalues
   * @param[out] p          Estimated parameters
   */
  virtual void Project(const Mesh& instance,
                       const cv::Mat& data,
                       const bool& p_scaled,
                       cv::Mat* p) const = 0;

  /**
   * @name  Sample
   * @fn    void virtual Sample(const Mesh& instance,
                                const cv::Mat& data,
                                const std::vector<ImagePoint>& pts,
                                const InstanceType& type,
                                cv::Mat* sample) const = 0
   * @param[in] instance    Mesh instance
   * @param[in] data        Extra data need for sampling (i.e. texture map)
   * @param[in] pts         List of point where to sample
   * @param[in] type        Type of sampling
   * @param[out] sample     Sampled elements
   */
  virtual void Sample(const Mesh& instance,
                      const cv::Mat& data,
                      const std::vector<ImagePoint>& pts,
                      const InstanceType& type,
                      cv::Mat* sample) const = 0;

  /**
   * @name  Generate
   * @fn    virtual void Generate(const cv::Mat& p,
                                  const bool& p_scaled,
                                  T* instance) const
   * @brief Generate an instance given a set of coefficients \p p.
   * @param[in] p   Nodel's coefficients
   * @param[in] p_scaled    True indicate that coefficient are relative to
   *                        eigenvalues
   * @param[out] instance   Generated instance
   */
  void Generate(const cv::Mat& p,
                const bool& p_scaled,
                T* instance) const;

  /**
   * @name  Generate
   * @fn    void Generate(const cv::Mat& p,
                          const bool& p_scaled,
                          Mesh* instance) const
   * @brief Generate an instance given a set of coefficients \p p.
   * @param[in] p           Nodel's coefficients
   * @param[in] p_scaled    True indicate that coefficient are relative to
   *                        eigenvalues
   * @param[out] instance   Generated instance
   */
  virtual void Generate(const cv::Mat& p,
                        const bool& p_scaled,
                        Mesh* instance) const = 0;

  /**
   * @name  Generate
   * @fn    virtual void Generate(T* instance) const
   * @brief Generate a random instance
   * @param[out] instance   Randomly generated instance
   */
  void Generate(T* instance) const;

  /**
   * @name  Generate
   * @fn    virtual void Generate(Mesh* instance) const = 0
   * @brief Generate a random instance
   * @param[out] instance   Randomly generated instance
   */
  virtual void Generate(Mesh* instance) const = 0;

  /**
   * @name  Generate
   * @fn    virtual void Generate(cv::Mat* p, Mesh* instance) const = 0
   * @brief Generate a random instance
   * @param[out] p          Parameters used for instance generation (scaled)
   * @param[out] instance   Randomly generated instance
   */
  virtual void Generate(cv::Mat* p, Mesh* instance) const = 0;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_variation
   * @fn    const cv::Mat& get_variation() const
   * @brief Provide principale component
   * @return    Principale component
   */
  const cv::Mat& get_variation() const {
    return variation_;
  }

  /**
   * @name  get_scaled_variation
   * @fn    const cv::Mat& get_scaled_variation() const
   * @brief Provide principale component scaled by eigenvalues
   * @return    Scaled Principale Component
   */
  const cv::Mat& get_scaled_variation() const {
    return scaled_variation_;
  }

  /**
   * @name  get_prior
   * @fn    const cv::Mat& get_prior() const
   * @brief Provide prior (eigenvalues)
   * @return    Prior
   */
  const cv::Mat& get_prior() const {
    return prior_;
  }

  /**
   * @name  get_inverse_prior
   * @fn    const cv::Mat& get_inverse_prior() const
   * @brief Provide prior inverse ( 1/ eigenvalues)
   * @return    Inverse Prior
   */
  const cv::Mat& get_inverse_prior() const {
    return inv_prior_;
  }

  /**
   * @name  get_mean
   * @fn    const cv::Mat& get_mean() const
   * @brief Provide mean value
   * @return    Mean
   */
  const cv::Mat& get_mean() const {
    return mean_;
  }

  /**
   * @name  get_n_channels
   * @fn    int get_n_channels() const
   * @brief Indicate how many channels are used
   * @return    Amount of channels
   */
  int get_n_channels() const {
    return n_channels_;
  }

  /**
   * @name  get_n_principle_component
   * @fn    int get_n_principle_component() const
   * @brief Indicate how many principle components are used
   * @return    Amount of principle component
   */
  int get_n_principle_component() const {
    return n_principle_component_;
  }

  /**
   * @name  get_n_element
   * @fn    int get_n_element() const
   * @brief Indicate how many elements (feature) are used
   * @return    Amount of element in the PCA
   */
  int get_n_element() const {
    return n_element_;
  }

  /**
   * @name  get_property
   * @fn    virtual size_t get_property(const Property prop) const
   * @brief Access property by a given index
   * @param[in] prop    Property index
   * @return    Property value
   */
  virtual size_t get_property(const Property prop) const {
    return std::numeric_limits<size_t>::max();
  }

  /**
   * @name  get_type
   * @fn    virtual HeaderObjectType get_type() const = 0
   * @brief Indicate dynamically the underlying type of the object
   * @return    Object type
   */
  virtual HeaderObjectType get_type() const = 0;

#pragma mark -
#pragma mark Protected
 protected:
  /** Mean */
  cv::Mat mean_;
  /** Variation */
  cv::Mat variation_;
  /** Scaled Variation - var * prior */
  cv::Mat scaled_variation_;
  /** Prior */
  cv::Mat prior_;
  /** Inverse Prior */
  cv::Mat inv_prior_;
  /** Channels */
  int n_channels_;
  /** Number of principal components */
  int n_principle_component_;
  /** Number of element */
  int n_element_;
};

/**
 * @class   PCAModelProxy
 * @brief   Registration mechanism for PCAModel type
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class PCAModelProxy {
 public:

  /**
   * @name  PCAModelProxy
   * @fn    PCAModelProxy()
   * @brief Constructor
   */
  PCAModelProxy();

  /**
   * @name  ~PCAModelProxy()
   * @fn    virtual ~PCAModelProxy() = default
   * @brief Destructor
   */
  virtual ~PCAModelProxy() = default;

  /**
   * @name  Create
   * @fn    virtual PCAModel<T>* Create() const = 0
   * @brief Create a PCAModel instance for a given type
   * @return Instance of a given class
   */
  virtual PCAModel<T>* Create() const = 0;

  /**
   * @name  Type
   * @fn    virtual HeaderObjectType Type() const = 0
   * @brief Provide the type represented by this proxy
   * @return    Object type
   */
  virtual HeaderObjectType Type() const = 0;

  /**
   * @name  Name
   * @fn    virtual const char* Name() const = 0
   * @brief Provide the name represented by this proxy
   * @return    Object name
   */
  virtual const char* Name() const = 0;
};

}  // namepsace LTS5

#endif //__LTS5_PCA_MODEL__
