/**
 *  @file   lts5/model/model.hpp
 *  @brief  Include all model's headers
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   11/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_MODEL__
#define __LTS5_MODEL__

/** Bilinear face model */
#include "lts5/model/bilinear_face_model.hpp"
/** PCA Model */
#include "lts5/model/pca_model.hpp"
/** PCA Model Factory */
#include "lts5/model/pca_model_factory.hpp"
/** Shape Model */
#include "lts5/model/shape_model.hpp"
/** Color Model */
#include "lts5/model/color_model.hpp"
/** Texture Model */
#include "lts5/model/texture_model.hpp"
/** Morphable Model */
#include "lts5/model/morphable_model.hpp"
/** Morphable Model Factory */
#include "lts5/model/morphable_model_factory.hpp"
/** Color Morphable Model */
#include "lts5/model/color_morphable_model.hpp"
/** Texture Morphable Model */
#include "lts5/model/texture_morphable_model.hpp"
/** Orthographic Projection */
#include "lts5/model/orthographic_projection.hpp"
/** Weak Projection */
#include "lts5/model/weak_projection.hpp"
/** Perspective Projection */
#include "lts5/model/perspective_projection.hpp"

#endif //__LTS5_MODEL__
