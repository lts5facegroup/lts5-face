/**
 *  @file   pca_model_factory.hpp
 *  @brief  Manager for PCAModel based instance
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_PCA_MODEL_FACTORY__
#define __LTS5_PCA_MODEL_FACTORY__

#include "lts5/utils/library_export.hpp"
#include "lts5/model/pca_model.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   PCAModelFactory
 * @brief   Manager for PCAModel based instance
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS PCAModelFactory {
 public:

#pragma mark -
#pragma mark Type Definition

  /** Model Type */
  using Model = PCAModel<T>;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  Get
   * @fn    static PCAModelFactory<T>& Get()
   * @brief Factory's accessors
   * @return    Single instance of PCAModelFactory
   */
  static PCAModelFactory<T>& Get();

  /**
   * @name  ~PCAModelFactory
   * @fn    ~PCAModelFactory() = default
   * @brief Destructor
   */
  ~PCAModelFactory() = default;

  /**
   * @name  PCAModelFactory
   * @fn    PCAModelFactory(const PCAModelFactory& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  PCAModelFactory(const PCAModelFactory& other) = delete;

  /**
   * @name  operator=
   * @fn    PCAModelFactory& operator=(const PCAModelFactory& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  PCAModelFactory& operator=(const PCAModelFactory& rhs) = delete;

  /**
   * @name  PCAModelFactory
   * @fn    PCAModelFactory(const PCAModelFactory& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  PCAModelFactory(PCAModelFactory&& other) = delete;

  /**
   * @name  operator=
   * @fn    PCAModelFactory& operator=(PCAModelFactory&& rhs) = delete
   * @brief Move assignment operator
   * @param[in] rhs Object to move assign from
   * @return    Newly moved-assign object
   */
  PCAModelFactory& operator=(PCAModelFactory&& rhs) = delete;

#pragma mark -
#pragma mark Usage

  /**
   * @name  CreateByName
   * @fn    Model* CreateByName(const std::string& name) const
   * @brief Create an object that match the provided \p name
   * @param[in] name Object name
   * @return    Instance corresponding to name or nullptr if name is unknown
   */
  Model* CreateByName(const std::string& name) const;

  /**
   * @name  CreateByType
   * @fn    Model* CreateByType(const HeaderObjectType type) const
   * @brief Create an object that match the provided \p type
   * @param[in] type Object type
   * @return    Instance corresponding to the type or nullptr if type is unknown
   */
  Model* CreateByType(const HeaderObjectType& type) const;

  /**
   * @name  Register
   * @fn    void Register(const PCAModelProxy<T>* proxy)
   * @brief Register a new \p proxy with the factory
   * @param[in] proxy Proxy to register
   */
  void Register(const PCAModelProxy<T>* proxy);

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  PCAModelFactory
   * @fn    PCAModelFactory() = default
   * @brief Constructor
   */
  PCAModelFactory() = default;

  /** Proxies */
  std::vector<const PCAModelProxy<T>*> proxies_;
};

}  // namepsace LTS5
#endif //__LTS5_PCA_MODEL_FACTORY__
