/**
 *  @file   bilinear_face_model_trainer.hpp
 *  @brief  Train a bilinear face model from a set of blendshape
 *  @ingroup model
 *
 *  @author Christophe Ecabert
 *  @date 13/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_BILINEAR_FACE_MODEL_TRAINER__
#define __LTS5_BILINEAR_FACE_MODEL_TRAINER__

#include <stdio.h>
#include <string>
#include <vector>


#include "opencv2/core/core.hpp"
#include "vmmlib/tensor3.hpp"

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#define N_FOLD 5


#define SIZE_VERTEX   8260*3
//#define SIZE_VERTEX   5*3
#define SIZE_IDENTITY   150
#define SIZE_EXPRESSION 47
#define RANK_VERTEX     SIZE_VERTEX
#define RANK_IDENTITY   50
#define RANK_EXPRESSION 25

#define FOLD_SIZE SIZE_IDENTITY / N_FOLD
#define SIZE_IDENTIY_FOLD SIZE_IDENTITY - FOLD_SIZE

/**
 *  @class  BilinearFaceModelTrainer
 *  @brief  Train bilinear face model
 *  @author Christophe Ecabert
 *  @date   03/02/2015
 *  @ingroup model
 */
class LTS5_EXPORTS BilinearFaceModelTrainer {

  //Define types
  typedef vmml::tensor3<SIZE_VERTEX, SIZE_IDENTIY_FOLD, SIZE_EXPRESSION,float> t3_t;

private :
  /** Tensor holding training data */
  t3_t* train_tensor_;
  /** Overall meanshape */
  cv::Mat mean_shape_;
  /** Indicate whether of not the tensor has be initialized */
  bool  is_initialized_;


public :
  /**
   *  @name BilinearFaceModelTrainer
   *  @fn BilinearFaceModelTrainer(void)
   *  @brief  Constructor
   */
  BilinearFaceModelTrainer(void);

  /**
   *  @name BilinearFaceModelTrainer
   *  @fn BilinearFaceModelTrainer(const std::vector<std::string>& blendshape_filename,
                                   const std::vector<int>& shape_index)
   *  @brief  Constructor
   *  @param[in]  blendshape_filename  List of blendashapes
   *  @param[in]  shape_index List of blendshape to use to fill tensor
   */
  BilinearFaceModelTrainer(const std::vector<std::string>& blendshape_filename,
                           const std::vector<int>& shape_index);

  /**
   *  @name ~BilinearFaceModelTrainer
   *  @fn ~BilinearFaceModelTrainer(void)
   *  @brief  Constructor
   */
  ~BilinearFaceModelTrainer(void);

  /**
   *  @name   LoadTensorData
   *  @fn int LoadTensorData(void)
   *  @brief  Fill the tensor with information from all blendshape's
   *          files (*.bs) in the following way :
   *          Tensor = Row x Col x Tube   =>  Vertices x Identity x Expression
   *  @param[in]  blendshape_filename  List of blendashapes
   *  @param[in]  shape_index List of blendshape to use to fill tensor
   *  @return Error code (i.e. open file fails)
   */
  int LoadTensorData(const std::vector<std::string>& blendshape_filename,
                     const std::vector<int>& shape_index);

  /**
   *  @name   TrainBilinearFaceModel
   *  @fn void TrainBilinearFaceModel(const std::string& output_filename)
   *  @brief  Apply tucker decomposition on training data
   *          D = T x1 U_1 x2 U_2 x3 U_3
   *          U_1 = Vertices
   *          U_2 = Identity
   *          U_3 = Expression
   *  @param[in]  output_filename   Name under which Core tensor
   *          (aka Face model) will be saved (*.bin)
   */
  void TrainBilinearFaceModel(const std::string& output_filename);

  /**
   *  @name   ConvertModelToOCVMat
   *  @brief  Convert a model (tensor3) into opencv multidimensionnal matrix
   *          Write mean shape for neutral expression
   *          Write vector weight for neutral expression
   *  @param  model_name            File holding bilinear face model
   *  @param  blendshape_filename   List of blendshapes
   *  @param  shape_index           List of shape to use
   *  @return 0 if no error
   */
  template<size_t R1, size_t R2, size_t R3, size_t I2, size_t I3>
  static int ConvertModelToOCVMat(const std::string& model_name,
                                  const std::vector<std::string>& blendshape_filename,
                                  const std::vector<int>& shape_index);

};

}  // namespace LTS5
#endif /* __LTS5_BILINEAR_FACE_MODEL_TRAINER__ */
