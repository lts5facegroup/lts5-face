/**
 *  @file   morphable_model_factory.hpp
 *  @brief  Manager for MorphableModel based instance
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_MORPHABLE_MODEL_FACTORY__
#define __LTS5_MORPHABLE_MODEL_FACTORY__

#include "lts5/utils/library_export.hpp"
#include "lts5/model/morphable_model.hpp"
#include "lts5/utils/object_type.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   MorphableModelFactory
 * @brief   Manager for MorphableModel based instance
 * @author  Christophe Ecabert
 * @date    15/08/2017
 * @ingroup model
 * @tparam T    Data type
 */
template<typename T>
class LTS5_EXPORTS MorphableModelFactory {
 public:

#pragma mark -
#pragma mark Type Definition

  /** Model Type */
  using Model = MorphableModel<T>;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  Get
   * @fn    static MorphableModelFactory<T>& Get(void)
   * @brief Factory's accessors
   * @return    Single instance of MorphableModelFactory
   */
  static MorphableModelFactory<T>& Get(void);

  /**
   * @name  ~MorphableModelFactory
   * @fn    ~MorphableModelFactory(void) = default
   * @brief Destructor
   */
  ~MorphableModelFactory(void) = default;

  /**
   * @name  MorphableModelFactory
   * @fn    MorphableModelFactory(const MorphableModelFactory& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  MorphableModelFactory(const MorphableModelFactory& other) = delete;

  /**
   * @name  operator=
   * @fn    MorphableModelFactory& operator=(const MorphableModelFactory& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return    Newly assigned object
   */
  MorphableModelFactory& operator=(const MorphableModelFactory& rhs) = delete;

  /**
   * @name  MorphableModelFactory
   * @fn    MorphableModelFactory(const MorphableModelFactory& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  MorphableModelFactory(MorphableModelFactory&& other) = delete;

  /**
   * @name  operator=
   * @fn    MorphableModelFactory& operator=(MorphableModelFactory&& rhs) = delete
   * @brief Move assignment operator
   * @param[in] rhs Object to move assign from
   * @return    Newly moved-assign object
   */
  MorphableModelFactory& operator=(MorphableModelFactory&& rhs) = delete;

#pragma mark -
#pragma mark Usage

  /**
   * @name  CreateByName
   * @fn    Model* CreateByName(const std::string& name) const
   * @brief Create an object that match the provided \p name
   * @param[in] name Object name
   * @return    Instance corresponding to name or nullptr if name is unknown
   */
  Model* CreateByName(const std::string& name) const;

  /**
   * @name  CreateByType
   * @fn    Model* CreateByType(const HeaderObjectType type) const
   * @brief Create an object that match the provided \p type
   * @param[in] type Object type
   * @return    Instance corresponding to the type or nullptr if type is unknown
   */
  Model* CreateByType(const HeaderObjectType type) const;

  /**
   * @name  Register
   * @fn    void Register(const MorphableModelProxy<T>* proxy)
   * @brief Register a new \p proxy with the factory
   * @param[in] proxy Proxy to register
   */
  void Register(const MorphableModelProxy<T>* proxy);

#pragma mark -
#pragma mark Private
 private:

  /**
   * @name  MorphableModelFactory
   * @fn    MorphableModelFactory(void) = default
   * @brief Constructor
   */
  MorphableModelFactory(void) = default;

  /** Proxies */
  std::vector<const MorphableModelProxy<T>*> proxies_;
};

}  // namepsace LTS5
#endif //__LTS5_MORPHABLE_MODEL_FACTORY__
