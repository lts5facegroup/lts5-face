/**
 *  @file   lts5/model.hpp
 *  @brief  Face model collection
 *  @ingroup model
 *
 *  @author Christophe Ecabert
 *  @date   11/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/model/model.hpp"
