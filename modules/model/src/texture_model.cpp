/**
 *  @file   texture_model.cpp
 *  @brief  Statistical texture model. Use texture map instead of per-vertex
 *          color
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/model/texture_model.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  Load
 * @fn    int Load(const std::string& filename)
 * @brief Load from a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TextureModel<T>::Load(const std::string& filename) {
  int err = -1;
  std::ifstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Load(stream);
  }
  return err;
}

/*
 * @name  Load
 * @fn    int Load(std::istream& stream)
 * @brief Load from a given binary \p stream
 * @param[in] stream Binary stream to load model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TextureModel<T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    int s = ScanStreamForObject(stream, HeaderObjectType::kTextureModel);
    if (s == 0) {
      // Load PCA
      err = PCAModel<T>::Load(stream);
      // Read tcoords
      err |= this->LoadTCoord(stream);
    }
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(const std::string& filename) const
 * @brief Save to a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TextureModel<T>::Save(const std::string& filename) const {
  int err = -1;
  std::ofstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Save(stream);
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(std::istream& stream) const
 * @brief Save to a given binary \p stream
 * @param[in] stream Binary stream to save model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TextureModel<T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Header
    int type = static_cast<int>(HeaderObjectType::kTextureModel);
    int sz = this->ComputeObjectSize();
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // PCAModel
    err = PCAModel<T>::Save(stream);
    // TCoords
    stream.write(reinterpret_cast<const char*>(n_tcoord_), sizeof(n_tcoord_));
    for (size_t t = 0; t < n_tcoord_; ++t) {
      stream.write(reinterpret_cast<const char*>(&tcoords_[t].x_),
                   sizeof(TCoord));
    }
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize(void) const
 * @brief Compute object size in byte
 * @return    Object's size
 */
template<typename T>
int TextureModel<T>::ComputeObjectSize(void) const {
  int sz = 2 * sizeof(int);
  sz += PCAModel<T>::ComputeObjectSize();
  sz += sizeof(n_tcoord_);
  sz += tcoords_.size() * sizeof(TCoord);
  return sz;
}

#pragma mark -
#pragma mark Usage


/*
 * @name  Project
 * @fn    void Project(const Mesh& instance,
                       const cv::Mat& data,
                       const bool& p_scaled,
                       cv::Mat* p) const
 * @brief Estimate parameters for a given instance
 * @param[in] instance    Instance for which parameters need to be estimated
 * @param[in] data        Extra data need for sampling (i.e. texture map)
 * @param[in] p_scaled    True indicate that coefficient will relative to
 *                        eigenvalues
 * @param[out] p          Estimated parameters
 */
template<typename T>
void TextureModel<T>::Project(const Mesh& instance,
                              const cv::Mat& data,
                              const bool& p_scaled,
                              cv::Mat* p) const {
  LTS5_LOG_ERROR("Not implemented yet !");
}

/*
 * @name  Sample
 * @fn    void Sample(const Mesh& instance,
                      const cv::Mat& data,
                      const std::vector<ImagePoint>& pts,
                      const InstanceType& type,
                      cv::Mat* sample) const
 * @param[in] instance    Mesh instance
 * @param[in] data        Extra data need for sampling (i.e. texture map)
 * @param[in] pts         List of point where to sample
 * @param[in] type        Type of sampling
 * @param[out] sample     Sampled elements
 */
template<typename T>
void TextureModel<T>::Sample(const Mesh& instance,
                             const cv::Mat& data,
                             const std::vector<ImagePoint>& pts,
                             const InstanceType& type,
                             cv::Mat* sample) const {
  LTS5_LOG_ERROR("Not implemented yet !");
}

/*
 * @name  Generate
 * @fn    void Generate(const cv::Mat& p,
                        const bool& p_scaled,
                        Mesh* instance)
 * @brief Generate an instance given a set of coefficients \p p.
 * @param[in] p   Nodel's coefficients
 * @param[out] instance   Generated instance
 */
template<typename T>
void TextureModel<T>::Generate(const cv::Mat& p,
                               const bool& p_scaled,
                               Mesh* instance) const {
  // Init tcoords
  auto& tcoord = instance->get_tex_coord();
  if (tcoord.size() != n_tcoord_) {
    tcoord.resize(n_tcoord_);
    std::copy(tcoords_.begin(), tcoords_.end(), tcoord.begin());
  }
  // Generate texture from model


  LTS5_LOG_ERROR("Not implemented yet !");
}

/*
 * @name  Generate
 * @fn    void Generate(Mesh* instance)
 * @brief Generate a random instance
 * @param[out] instance   Randomly generated instance
 */
template<typename T>
void TextureModel<T>::Generate(Mesh* instance) const {
  // Init tcoords
  auto& tc = instance->get_tex_coord();
  if (tc.size() != n_tcoord_) {
    tc.resize(n_tcoord_);
    std::copy(tcoords_.begin(), tcoords_.end(), tc.begin());
  }
  // Generate texture from model
  LTS5_LOG_ERROR("Not implemented yet !");
}

/*
 * @name  Generate
 * @fn    void Generate(cv::Mat* p, Mesh* instance)
 * @brief Generate a random instance
 * @param[out] p  Parameters used for instance generation
 * @param[out] instance   Randomly generated instance
 */
template<typename T>
void TextureModel<T>::Generate(cv::Mat* p, Mesh* instance) const {
  // Init tcoords
  auto& tc = instance->get_tex_coord();
  if (tc.size() != n_tcoord_) {
    tc.resize(n_tcoord_);
    std::copy(tcoords_.begin(), tcoords_.end(), tc.begin());
  }
  // Generate texture from model
  LTS5_LOG_ERROR("Not implemented yet !");
}

#pragma mark -
#pragma mark Private

/**
 * @name  LoadTCoord
 * @fn    int LoadTCoord(std::istream& stream)
 * @brief Load texture coordinates from a given \p stream.
 * @param[in] stream Binary stream to read tcoords from.
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
int TextureModel<T>::LoadTCoord(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Get number of tri
    n_tcoord_ = 0;
    stream.read(reinterpret_cast<char*>(&n_tcoord_), sizeof(n_tcoord_));
    if (n_tcoord_ > 0) {
      tcoords_.resize(n_tcoord_);
      for (size_t i = 0; i < n_tcoord_; ++i) {
        stream.read(reinterpret_cast<char*>(&tcoords_[i].x_), sizeof(TCoord));
      }
      // Sanity check
      err = stream.good() ? 0 : -1;
    }
  }
  return err;
}

#pragma mark -
#pragma mark Proxy

/*
 * @name  Create
 * @fn    PCAModel<T>* Create(void) const
 * @brief Create a PCAModel instance for a given type
 * @return Instance of a given class
 */
template<typename T>
PCAModel<T>* TextureModelProxy<T>::Create(void) const {
  return new TextureModel<T>();
}

/*
 * @name  Type
 * @fn    HeaderObjectType Type(void) const
 * @brief Provide the type represented by this proxy
 * @return    Object type
 */
template<typename T>
HeaderObjectType TextureModelProxy<T>::Type(void) const {
  return HeaderObjectType::kTextureModel;
}

/*
 * @name  Name
 * @fn    const char* Name(void) const
 * @brief Provide the name represented by this proxy
 * @return    Object name
 */
template<typename T>
const char* TextureModelProxy<T>::Name(void) const {
  return "TextureModel";
}


// Explicit registration
TextureModelProxy<float> tex_model_proxyf;
TextureModelProxy<double> tex_model_proxyd;

#pragma mark -
#pragma mark Explicit Instantiation

/** Float */
template class TextureModel<float>;
/** Double */
template class TextureModel<double>;

}  // namepsace LTS5