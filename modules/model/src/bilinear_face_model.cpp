/**
 *  @file   bilinear_face_model.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 13/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <assert.h>
#include <fstream>

#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/utils/sys/parallel.hpp"
#include "lts5/model/bilinear_face_model.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name   BilinearFaceModel
 *  @brief  Create a bilinear face model object.
 *  @param  core_filename   File containing the data to load into
 *                          the core tensor.
 */
BilinearFaceModel::BilinearFaceModel(const std::string& core_filename) {
  this->Load(core_filename);
}

/*
 *  @name   Load
 *  @fn void Load(const std::string& core_filename)
 *  @brief  Load face model from config file
 *  @param[in]  core_filename Model configuration file
 *  @return -1 if error, 0 otherwise
 */
int BilinearFaceModel::Load(const std::string& core_filename) {
  // Load model
  int err = -1;
  int pos = (int)core_filename.find(".raw");
  if (pos != std::string::npos) {
    // Extension is correct
    FILE* model = fopen(core_filename.c_str(), "rb");
    if (model) {
      // Open
      // Read dimension
      int length = 0;
      fread((void*)&model_dims_[0], sizeof(int), 1, model);
      fread((void*)&model_dims_[1], sizeof(int), 1, model);
      fread((void*)&model_dims_[2], sizeof(int), 1, model);
      rank_vertices_ = model_dims_[0];
      rank_identity_ = model_dims_[1];
      rank_expression_ = model_dims_[2];

      // Init model matrix
      length = rank_vertices_ * rank_identity_ * rank_expression_;
      face_core_model_ = cv::Mat::zeros(3, model_dims_, CV_32FC1);
      // Read model
      fread((void*)face_core_model_.data, sizeof(float), length, model);
      // Close file
      fclose(model);
      err = 0;


      bool is_valid = cv::checkRange(face_core_model_);
      if (!is_valid) {
        std::cout << "Core tensor corrupted ..." << std::endl;
      }
    }
  }
  return err;
}

/*
 *  @name   ttv_multiply_second_mode
 *  @brief  Multiply bilinear model (tensor) by a vector in the second
 *          direction for specific vertices
 *  @param[in] vector                   Vector to multiply by
 *  @param[in]  index_selection         List of vertices to compute
 *  @param[in]  destination_row_offset  Row offset in the destination matrix
 *  @param[out] result                  Matrix holding results
 */
void BilinearFaceModel::TtvMultiplySecondMode(const cv::Mat& vector,
                                              const std::vector<int>* index_selection,
                                              const int destination_row_offset,
                                              cv::Mat* result) {
  // Define range of computation
  size_t range = index_selection == nullptr ? rank_vertices_/3 :
                                              index_selection->size();

  Parallel::For(range,
                [&](const size_t& idx) {
                  // Get pts index
                  int i = index_selection == nullptr ? (int)idx*3 :
                          (*index_selection)[idx]*3;
                  int vertex_idx = 0;
                  size_t address_shift;
                  size_t address_step = face_core_model_.step.p[2];
                  int idx_res = (int)idx * 3;
                  const float* core_ptr_start;
                  const float* vector_ptr_start = (const float*) vector.data;
                  float* result_ptr;
                  const int N = std::max(vector.cols, vector.rows);
                  // Goes through the x,y,z element of the vertex
                  int idx_result_precomp = 0;
                  for(int v = 0 ; v < 3 ; ++v) {
                    vertex_idx = i + v;
                    address_shift = vertex_idx * face_core_model_.step.p[0];
                    idx_res = ((int)idx + destination_row_offset)*3 + v;
                    idx_result_precomp = idx_res * result->cols;
                    result_ptr = &(((float*)result->data)[idx_result_precomp]);
                    for (int k = 0 ; k < rank_expression_ ; ++k)
                    {
                      // Select source address
                      core_ptr_start = (const float*)(face_core_model_.data +
                                                      address_shift +
                                                      k * address_step);
                      // Dot product
                      using LA = LinearAlgebra<float>;
                      *result_ptr = LA::Dot(N,
                                            core_ptr_start,
                                            (const int) rank_expression_,
                                            vector_ptr_start,
                                            (const int)1);
                      // Go to next element
                      result_ptr++;
                    }
                  }
                });
}

/*
 *  @name   ttv_multiply_second_mode
 *  @brief  Multiply bilinear model (tensor) by a vector in the third direction for specific vertices
 *  @param  vector                      Vector to multiply by
 *  @param  index_selection             List of vertices to compute
 *  @param  destination_row_offset      Row offset in the destination matrix
 *  @param  result                      Matrix holding results
 */
void BilinearFaceModel::TtvMultiplyThirdMode(const cv::Mat& vector,
                                             const std::vector<int>* index_selection,
                                             const int destination_row_offset,
                                             cv::Mat* result) {
  // Define range of computation
  size_t range = index_selection == nullptr ? rank_vertices_/3 :
                                              index_selection->size();
  Parallel::For(range,
          [&](const size_t& idx) {
            // Get pts index
            int i = index_selection == nullptr ? (
                    int)idx*3 :
                    (*index_selection)[idx]*3;
            int vertex_idx = 0;
            size_t address_shift;
            size_t address_step = face_core_model_.step.p[1];
            int idx_res = (int)idx * 3;
            const float* core_ptr_start;
            const float* vector_ptr_start = (const float*) vector.data;
            float* result_ptr;
            const int N = std::max(vector.cols, vector.rows);

            // Goes through the x,y,z element of the vertex
            int index_res_precomp = 0;
            for(int v = 0 ; v < 3 ; ++v) {
              vertex_idx = i + v;
              address_shift = vertex_idx * face_core_model_.step.p[0];
              idx_res = ((int)idx + destination_row_offset)*3 + v;
              index_res_precomp = idx_res * result->cols;
              result_ptr = &(((float*)result->data)[index_res_precomp]);
              for (int j = 0; j < rank_identity_; ++j) {
                // Select source address
                core_ptr_start = (const float*) (face_core_model_.data +
                                                 address_shift +
                                                 j * address_step);
                // Dot product
                using LA = LinearAlgebra<float>;
                *result_ptr = LA::Dot(N,
                                      core_ptr_start,
                                      (const int)1,
                                      vector_ptr_start,
                                      (const int)1);
                // Go to next element
                result_ptr++;
              }
            }
          });
}

/*
 *  @name   modelTimesWeightVector
 *  @brief  Multiply the model (tensor) by weights vector along a specific
 *          direction for specific vertex selection, if index_selection == NULL
 *          the multiplication is on the whole tensor
 *  @param[in]  dimension                   Direction selection
 *  @param[in]  vector                      Vector to multiply by.
 *  @param[in]  index_selection             List of vertex to compute
 *  @param[in]  destination_row_offset      Row offset in the resulting matrix, default = 0
 *  @param[out] result                      Matrix holding the result of the multiplication
 */
void BilinearFaceModel::ModelTimesWeightVector(const BilinearFaceModel::ModelDimensionEnum dimension,
                                               const cv::Mat& vector,
                                               const std::vector<int>* index_selection,
                                               cv::Mat* result,
                                               const int destination_row_offset) {
  switch (dimension)
  {
    case kId :      TtvMultiplySecondMode(vector,
                                          index_selection,
                                          destination_row_offset,
                                          result);
      break;

    case kExpr :    TtvMultiplyThirdMode(vector,
                                         index_selection,
                                         destination_row_offset,
                                         result);
      break;

    default:    LTS5_LOG_ERROR("Not supported");
      break;
  }
}
}
