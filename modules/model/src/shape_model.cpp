/**
 *  @file   shape_model.cpp
 *  @brief  3D statistical shape model
 *  @ingroup model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/model/shape_model.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  ShapeModel
 * @fn    ShapeModel(const cv::Mat& mean,
                     const cv::Mat& variation,
                     const cv::Mat& prior,
                     const int& n_channels,
                     const std::vector<Triangle>& tri,
                     const std::vector<int>& landmarks,
                     const std::vector<T>& cog)
 * @brief Specialized constructor
 * @param[in] mean        Mean value
 * @param[in] variation   Variation matrix (Eigen vectors in column)
 * @param[in] prior       Prior (Eigen values)
 * @param[in] n_channels  Number of channels in the flattened mean.
 * @param[in] tri         Triangulation
 * @param[in] landmarks   Facial landmark's indexes
 * @param[in] cog         Center of gravity of the shape model (offset)
 */
template<typename T>
ShapeModel<T>::ShapeModel(const cv::Mat& mean,
                          const cv::Mat& variation,
                          const cv::Mat& prior,
                          const int& n_channels,
                          const std::vector<Triangle>& tri,
                          const std::vector<int>& landmarks,
                          const std::vector<T>& cog) :
        PCAModel<T>::PCAModel(mean, variation, prior, n_channels),
        tri_(tri),
        landmarks_(landmarks),
        cog_(cog),
        n_landmarks_(landmarks.size()),
        n_tri_(tri.size()) {
  n_vertex_ = static_cast<size_t>(this->mean_.rows / this->n_channels_);
}


/*
 * @name  Load
 * @fn    int Load(const std::string& filename)
 * @brief Load from a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ShapeModel<T>::Load(const std::string& filename) {
  int err = -1;
  std::ifstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Load(stream);
  }
  return err;
}

/*
 * @name  Load
 * @fn    int Load(std::istream& stream)
 * @brief Load from a given binary \p stream
 * @param[in] stream Binary stream to load model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ShapeModel<T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    int s = ScanStreamForObject(stream, HeaderObjectType::kShapeModel);
    if (s == 0) {
      // Load PCA Model
      err = PCAModel<T>::Load(stream);
      // Load tri
      err |= this->LoadTriangle(stream);
      // Load indices
      err |= this->LoadLandmarks(stream);
      // Load cog
      int n = 0;
      stream.read(reinterpret_cast<char*>(&n), sizeof(n));
      cog_.resize(n);
      stream.read(reinterpret_cast<char*>(cog_.data()), n * sizeof(T));
      // Init var
      n_vertex_ = static_cast<size_t>(this->mean_.rows / this->n_channels_);
      err |= stream.good() ? 0 : -1;
    }
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(const std::string& filename) const
 * @brief Save to a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ShapeModel<T>::Save(const std::string& filename) const {
  int err = -1;
  std::ofstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Save(stream);
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(std::istream& stream) const
 * @brief Save to a given binary \p stream
 * @param[in] stream Binary stream to save model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ShapeModel<T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Dump size + type
    int sz = this->ComputeObjectSize();
    int type = static_cast<int>(HeaderObjectType::kShapeModel);
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Dump PCA model
    err = PCAModel<T>::Save(stream);
    // Dump tri
    stream.write(reinterpret_cast<const char*>(&n_tri_), sizeof(n_tri_));
    stream.write(reinterpret_cast<const char*>(tri_.data()),
                 tri_.size() * sizeof(Triangle));
    // Dump facial landmarks indices
    stream.write(reinterpret_cast<const char*>(&n_landmarks_),
                 sizeof(n_landmarks_));
    stream.write(reinterpret_cast<const char*>(landmarks_.data()),
                 landmarks_.size() * sizeof(landmarks_[0]));
    // Dump cog
    sz = static_cast<int>(this->cog_.size());
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    stream.write(reinterpret_cast<const char*>(cog_.data()),
                 cog_.size() * sizeof(T));
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize(void) const
 * @brief Compute object size in byte
 * @return    Object's size
 */
template<typename T>
int ShapeModel<T>::ComputeObjectSize(void) const {
  int sz = 2 * sizeof(int);
  sz += PCAModel<T>::ComputeObjectSize();
  sz += sizeof(n_tri_);
  sz += tri_.size() * sizeof(Triangle);
  sz += 4;
  sz += landmarks_.size() * sizeof(landmarks_[0]);
  return sz;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Project
 * @fn    void Project(const Mesh& instance,
                       const cv::Mat& data,
                       const bool& p_scaled,
                       cv::Mat* p) const
 * @brief Estimate parameters for a given instance
 * @param[in] instance    Instance for which parameters need to be estimated
 * @param[in] data        Extra data need for sampling (i.e. texture map)
 * @param[in] p_scaled    True indicate that coefficient will relative to
 *                        eigenvalues
 * @param[out] p          Estimated parameters
 */
template<typename T>
void ShapeModel<T>::Project(const Mesh& instance,
                            const cv::Mat& data,
                            const bool& p_scaled,
                            cv::Mat* p) const {
  using LA = LinearAlgebra<T>;
  using TType = typename LinearAlgebra<T>::TransposeType;
  // Remove mean
  const auto& vertex = instance.get_vertex();
  static cv::Mat vbuff, buff;
  vbuff = cv::Mat(static_cast<int>(vertex.size() * this->n_channels_),
                  1,
                  cv::DataType<T>::type,
                  (void*)vertex.data()).clone();
  // Estimate parameters
  LA::Axpy(this->mean_, T(-1.0), &vbuff);
  if (p_scaled) {
    LA::Gemv(this->variation_, TType::kTranspose, T(1.0), vbuff, T(0.0), &buff);
    LA::Sbmv(this->inv_prior_, T(1.0), buff, T(0.0), p);
  } else {
    LA::Gemv(this->variation_, TType::kTranspose, T(1.0), vbuff, T(0.0), p);
  }
}

/*
 * @name  Sample
 * @fn    void Sample(const Mesh& instance,
                      const cv::Mat& data,
                      const std::vector<ImagePoint>& pts,
                      const InstanceType& type,
                      cv::Mat* sample) const
 * @param[in] instance    Mesh instance
 * @param[in] data        Extra data need for sampling (i.e. texture map)
 * @param[in] pts         List of point where to sample
 * @param[in] type        Type of sampling
 * @param[out] sample     Sampled elements
 */
template<typename T>
void ShapeModel<T>::Sample(const Mesh& instance,
                           const cv::Mat& data,
                           const std::vector<ImagePoint>& pts,
                           const InstanceType& type,
                           cv::Mat* sample) const {
  int n = static_cast<int>(pts.size());
  int c = this->n_channels_;
  // Adapt sampling based on type
  switch (type) {
    case InstanceType::kShape: {
      sample->create(n * c, 1, cv::DataType<T>::type);
      const auto& vertex = instance.get_vertex();
      for (int i = 0; i < n; ++i) {
        int idx = i * c;
        const auto& p = pts[i];
        const auto& va = vertex[p.bcoord.va_idx];
        const auto& vb = vertex[p.bcoord.vb_idx];
        const auto& vc = vertex[p.bcoord.vc_idx];
        auto s = p.bcoord.alpha * va;
        s += p.bcoord.beta * vb;
        s += p.bcoord.gamma * vc;
        sample->at<T>(idx) = s.x_;
        sample->at<T>(idx + 1) = s.y_;
        sample->at<T>(idx + 2) = s.z_;
      }
    }
      break;

    case InstanceType::kShapeVariation:
    case InstanceType::kShapeScaledVariation: {
      int n_pc = this->n_principle_component_;
      sample->create(n * c, n_pc, cv::DataType<T>::type);
      const cv::Mat& var = (type == InstanceType::kShapeVariation ?
                            this->variation_ :
                            this->scaled_variation_);
      for (int i = 0; i < n; ++i) {
        const auto& p = pts[i];
        const auto& pa = var(cv::Rect(0, p.bcoord.va_idx * c, n_pc, c));
        const auto& pb = var(cv::Rect(0, p.bcoord.vb_idx * c, n_pc, c));
        const auto& pc = var(cv::Rect(0, p.bcoord.vc_idx * c, n_pc, c));
        cv::Mat ps = p.bcoord.alpha * pa;
        ps += p.bcoord.beta * pb;
        ps += p.bcoord.gamma * pc;
        ps.copyTo((*sample)(cv::Rect(0, i * c, n_pc, c)));
      }
    }
      break;

    case InstanceType::kMeanShape: {
      sample->create(n * c, 1, cv::DataType<T>::type);
      const cv::Mat& var = this->mean_;
      for (int i = 0; i < n; ++i) {
        const auto& p = pts[i];
        const auto& pa = var(cv::Rect(0, p.bcoord.va_idx * c, 1, c));
        const auto& pb = var(cv::Rect(0, p.bcoord.vb_idx * c, 1, c));
        const auto& pc = var(cv::Rect(0, p.bcoord.vc_idx * c, 1, c));
        cv::Mat ps = p.bcoord.alpha * pa;
        ps += p.bcoord.beta * pb;
        ps += p.bcoord.gamma * pc;
        ps.copyTo((*sample)(cv::Rect(0, i * c, 1, c)));
      }
    }
      break;

    case InstanceType::kShapeNormal: {
      sample->create(n * c, 1, cv::DataType<T>::type);
      const auto& normal = instance.get_normal();
      for (int i = 0; i < n; ++i) {
        int idx = i * c;
        const auto& p = pts[i];
        const auto& pa = normal[p.bcoord.va_idx];
        const auto& pb = normal[p.bcoord.vb_idx];
        const auto& pc = normal[p.bcoord.vc_idx];
        auto ps = p.bcoord.alpha * pa;
        ps += p.bcoord.beta * pb;
        ps += p.bcoord.gamma * pc;
        sample->at<T>(idx) = ps.x_;
        sample->at<T>(idx + 1) = ps.y_;
        sample->at<T>(idx + 2) = ps.z_;
      }
    }
      break;

    default:
      LTS5_LOG_ERROR("Sampler can not handle this type of instance");
  }
}

/*
 * @name  Generate
 * @fn    void Generate(const cv::Mat& p,
                        const bool& p_scaled,
                        Mesh* instance) const
 * @brief Generate an instance given a set of coefficients \p p.
 * @param[in] p   Nodel's coefficients
 * @param[in] p_scaled    True indicate that coefficient are relative to
 *                        eigenvalues
 * @param[out] instance   Generated instance
 */
template<typename T>
void ShapeModel<T>::Generate(const cv::Mat& p,
                             const bool& p_scaled,
                             Mesh* instance) const {
  auto& tri = instance->get_triangle();
  if (tri.size() != tri_.size()) {
    tri.resize(tri_.size());
    std::copy(tri_.begin(), tri_.end(), tri.begin());
  }
  // Vertex init ?
  auto& vertex = instance->get_vertex();
  if (vertex.size() != n_vertex_) {
    vertex.resize(n_vertex_);
  }
  // Create instance
  PCAModel<T>::Generate(p, p_scaled, &vertex[0].x_);
}

/*
 * @name  Generate
 * @fn    void Generate(Mesh* instance) const
 * @brief Generate a random instance
 * @param[out] instance   Randomly generated instance
 */
template<typename T>
void ShapeModel<T>::Generate(Mesh* instance) const {
  auto& tri = instance->get_triangle();
  if (tri.empty()) {
    tri.resize(tri_.size());
    std::copy(tri_.begin(), tri_.end(), tri.begin());
  }
  // Vertex init ?
  auto& vertex = instance->get_vertex();
  if (vertex.size() != n_vertex_) {
    vertex.resize(n_vertex_);
  }
  // Create instance
  PCAModel<T>::Generate(&vertex[0].x_);
}

/*
 * @name  Generate
 * @fn    void Generate(cv::Mat* p, Mesh* instance)
 * @brief Generate a random instance
 * @param[out] p  Parameters used for instance generation
 * @param[out] instance   Randomly generated instance
 */
template<typename T>
void ShapeModel<T>::Generate(cv::Mat* p,
                             Mesh* instance) const {
  auto& tri = instance->get_triangle();
  if (tri.empty()) {
    tri.resize(tri_.size());
    std::copy(tri_.begin(), tri_.end(), tri.begin());
  }
  // Vertex init ?
  auto& vertex = instance->get_vertex();
  if (vertex.size() != n_vertex_) {
    vertex.resize(n_vertex_);
  }
  // Init p
  p->create(this->n_principle_component_, 1, cv::DataType<T>::type);
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  cv::randn(*p, T(0.0), T(1.0));
  // Create instance
  PCAModel<T>::Generate(*p, true, &vertex[0].x_);
}

#pragma mark -
#pragma mark Private

/*
 * @name  LoadTriangle
 * @fn    int LoadTriangle(std::istream stream)
 * @brief Load a collection of triangles
 * @param[in] stream Binary stream to load from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ShapeModel<T>::LoadTriangle(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Get number of tri
    n_tri_ = 0;
    stream.read(reinterpret_cast<char*>(&n_tri_), sizeof(n_tri_));
    if (n_tri_ > 0) {
      tri_.resize(n_tri_);
      for (size_t i = 0; i < n_tri_; ++i) {
        stream.read(reinterpret_cast<char*>(&tri_[i].x_), sizeof(Triangle));
      }
      // Sanity check
      err = stream.good() ? 0 : -1;
    }
  }
  return err;
}

/*
 * @name  LoadLandmarks
 * @fn    int LoadLandmarks(std::istream& stream)
 * @brief Load a collection of facial landmarks indices
 * @param[in] stream Binary stream to load from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ShapeModel<T>::LoadLandmarks(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Get number of tri
    n_landmarks_ = 0;
    stream.read(reinterpret_cast<char*>(&n_landmarks_), sizeof(n_landmarks_));
    if (n_landmarks_ > 0) {
      landmarks_.resize(static_cast<size_t>(n_landmarks_));
      stream.read(reinterpret_cast<char*>(landmarks_.data()),
                  n_landmarks_ * sizeof(landmarks_[0]));
      // Sanity check
      err = stream.good() ? 0 : -1;
    }
  }
  return err;
}

#pragma mark -
#pragma mark Proxy

/*
 * @name  Create
 * @fn    PCAModel<T>* Create(void) const
 * @brief Create a PCAModel instance for a given type
 * @return Instance of a given class
 */
template<typename T>
PCAModel<T>* ShapeModelProxy<T>::Create(void) const {
  return new ShapeModel<T>();
}

/*
 * @name  Type
 * @fn    HeaderObjectType Type(void) const
 * @brief Provide the type represented by this proxy
 * @return    Object type
 */
template<typename T>
HeaderObjectType ShapeModelProxy<T>::Type(void) const {
  return HeaderObjectType::kShapeModel;
}

/*
 * @name  Name
 * @fn    const char* Name(void) const
 * @brief Provide the name represented by this proxy
 * @return    Object name
 */
template<typename T>
const char* ShapeModelProxy<T>::Name(void) const {
  return "ShapeModel";
}

// Explicit registration
ShapeModelProxy<float> shape_model_proxyf;
// Explicit registration
ShapeModelProxy<double> shape_model_proxyd;

#pragma mark -
#pragma mark Explicit instantiation

/** Float */
template class ShapeModel<float>;
/** Double */
template class ShapeModel<double>;

}  // namepsace LTS5
