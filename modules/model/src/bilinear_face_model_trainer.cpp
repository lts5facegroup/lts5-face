/**
 *  @file   bilinear_face_model_trainer.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 13/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <chrono>

#include "vmmlib/tucker3_tensor.hpp"
#include "vmmlib/t3_converter.hpp"
#include "opencv2/core/core_c.h"

#include "lts5/utils/process_error.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/sys/file_system.hpp"
#include "lts5/model/bilinear_face_model_trainer.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name BilinearFaceModelTrainer
 *  @fn BilinearFaceModelTrainer()
 *  @brief  Constructor
 */
BilinearFaceModelTrainer::BilinearFaceModelTrainer() {
  // Init stuff
  mean_shape_ = cv::Mat::zeros(SIZE_VERTEX, 1, CV_32FC1);
  train_tensor_ = new t3_t;
  is_initialized_ = false;
}

/*
 *  @name BilinearFaceModelTrainer
 *  @fn BilinearFaceModelTrainer(const std::string& blendshape_root_folder,
                                 const std::string& extension_type,
                                 const int n_folds)
 *  @brief  Constructor
 *  @param[in]  blendshape_filename  List of blendashapes
 *  @param[in]  shape_index List of blendshape to use to fill tensor
 */
BilinearFaceModelTrainer::
BilinearFaceModelTrainer(const std::vector<std::string>& blendshape_filename,
                         const std::vector<int>& shape_index) {
  // Init stuff
  mean_shape_ = cv::Mat::zeros(SIZE_VERTEX, 1, CV_32FC1);
  train_tensor_ = new t3_t;
  is_initialized_ = false;
  // FIll tensor
  if(!this->LoadTensorData(blendshape_filename, shape_index)) {
    throw ProcessError(ProcessError::ProcessErrorEnum::kErrOpeningFile,
                       "Unable to load data into tensor",
                       FUNC_NAME);
  }
}

/*
 *  @name ~BilinearFaceModelTrainer
 *  @fn ~BilinearFaceModelTrainer()
 *  @brief  Constructor
 */
BilinearFaceModelTrainer::~BilinearFaceModelTrainer() {
  if (train_tensor_) {
    delete train_tensor_;
    train_tensor_ = nullptr;
  }
}

/*
 *  @name   LoadTensorData
 *  @fn int LoadTensorData()
 *  @brief  Fill the tensor with information from all blendshape's
 *          files (*.bs) in the following way :
 *          Tensor = Row x Col x Tube   =>  Vertices x Identity x Expression
 *  @param[in]  blendshape_filename  List of blendashapes
 *  @param[in]  shape_index List of blendshape to use to fill tensor
 *  @return Error code (i.e. open file fails)
 */
int BilinearFaceModelTrainer::LoadTensorData(const std::vector<std::string>& blendshape_filename,
                                             const std::vector<int>& shape_index) {
  FILE* file_id = nullptr;
  int n_vertices = 0;
  int n_expr = 0;
  int n_face = 0;
  int n_id = static_cast<int>(shape_index.size());
  int error = 0;
  size_t n_byte = 0;
  std::string fname;

  // Start
  LTS5_LOG_INFO("\tStart to load training data into tensor...");
  if (blendshape_filename.empty() || shape_index.empty()) {
    LTS5_LOG_ERROR("\tNo blendshape/index are provided");
    return -1;
  }
  // Some data are present
  cv::Mat shape = cv::Mat(SIZE_VERTEX, 1, CV_32FC1);
  cv::Mat mean_shape = cv::Mat::zeros(SIZE_VERTEX, 1, CV_32FC1);
  // Start to loop
  vmml::vector<SIZE_VERTEX>* raw_vector = new vmml::vector<SIZE_VERTEX>;
  for (int i = 0; i < n_id; ++i) {
    // Open blendshape
    fname = blendshape_filename[shape_index[i]];
    LTS5_LOG_DEBUG("\tRead file : " << fname);
    file_id = fopen(fname.c_str(), "rb");
    if (file_id) {
      // Ok
      // Get n_vertex and n_expr
      fread(reinterpret_cast<void*>(&n_expr), sizeof(int), 1, file_id);
      fread(reinterpret_cast<void*>(&n_vertices), sizeof(int), 1, file_id);
      fread(reinterpret_cast<void*>(&n_face), sizeof(int), 1, file_id);



      n_vertices = SIZE_VERTEX/3;


      // Read some possible value ?
      if (n_vertices != 0 && n_expr != 0) {
        // Ok, allocate memory
        for (int k = 0; k <= n_expr; ++k) {
          // Read data
          n_byte = fread(reinterpret_cast<void*>(raw_vector->array),
                         sizeof(float),
                         n_vertices * 3,
                         file_id);
          if (n_byte == SIZE_VERTEX) {
            // Fill tensor
            train_tensor_->set_column(i, k, *raw_vector);
            // compute meanshpe as well
            memcpy(reinterpret_cast<void*>(shape.data),
                   reinterpret_cast<const void*>(&raw_vector->array[0]),
                   SIZE_VERTEX * sizeof(float));
            mean_shape += shape;
          } else {
            fclose(file_id);
            error = -1;
            break;
          }
        }
      } else {
        fclose(file_id);
        error = -1;
        break;
      }
    } else {
      // Error
      error = -1;
      break;
    }
  }
  delete raw_vector;

  double tensor_norm = train_tensor_->frobenius_norm();
  LTS5_LOG_INFO("\t Tensor norm : " << tensor_norm);

  if (error != 0) {
    LTS5_LOG_ERROR("\tUnable to load : " << fname);
  } else {
    LTS5_LOG_DEBUG("\tData loaded");
    is_initialized_ = true;
    mean_shape_ = mean_shape / (SIZE_EXPRESSION * SIZE_IDENTIY_FOLD);
  }
  return error;
}

/*
 *  @name   TrainBilinearFaceModel
 *  @fn void TrainBilinearFaceModel(const std::string& output_filename)
 *  @brief  Apply tucker decomposition on training data
 *          D = T x1 U_1 x2 U_2 x3 U_3
 *          U_1 = Vertices
 *          U_2 = Identity
 *          U_3 = Expression
 *  @param[in]  output_filename   Name under which Core tensor
 *          (aka Face model) will be saved (*.bin)
 */
void BilinearFaceModelTrainer::TrainBilinearFaceModel(const std::string& output_filename) {
  // Define some stuff
  typedef vmml::tucker3_tensor<RANK_VERTEX,
            RANK_IDENTITY,
            RANK_EXPRESSION,
            SIZE_VERTEX,
            SIZE_IDENTIY_FOLD,
            SIZE_EXPRESSION,
            float,
            float> t3_tucker_core_t;
  typedef vmml::t3_hooi<RANK_VERTEX,
            RANK_IDENTITY,
            RANK_EXPRESSION,
            SIZE_VERTEX,
            SIZE_IDENTIY_FOLD,
            SIZE_EXPRESSION,
            float> hooi_t;
  typedef vmml::tensor3<SIZE_VERTEX,
            RANK_IDENTITY,
            RANK_EXPRESSION> t3_core_t;
  typedef vmml::t3_converter<RANK_VERTEX,
            RANK_IDENTITY,
            RANK_EXPRESSION> t3_convert_t;

  // Check for root dir
  std::string dir;
  std::string model_name;
  size_t pos = output_filename.rfind('/');
  if (pos != std::string::npos) {
    dir = output_filename.substr(0, pos + 1);
    auto* fs = GetDefaultFileSystem();
    if (!fs->FileExist(dir).Good()) {
      fs->CreateDirRecursively(dir);
    }
    model_name = output_filename.substr(pos + 1, output_filename.length());
  } else {
    model_name = output_filename;
  }
  // Compute decomposition
  auto start = std::chrono::system_clock::now();
  LTS5_LOG_INFO("\tStart decomposition... will take a while");
  auto* tucker_tensor = new t3_tucker_core_t;
  tucker_tensor->tucker_als(*train_tensor_, hooi_t::init_hosvd(), 5, 1e-5);
  auto stop = std::chrono::system_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::minutes>(stop - start);
  LTS5_LOG_INFO("\tTucker decomposition take : " << duration.count() << " min");

  LTS5_LOG_INFO("\tCompute core tensor");
  auto* core_tensor = new t3_core_t;
  vmml::t3_ttm::multiply_frontal_fwd(tucker_tensor->_core_comp,
                                     *(tucker_tensor->_u1_comp),
                                     *core_tensor);
  // Backups, data
  LTS5_LOG_INFO("\tWrite model into file : ./" << output_filename << ".raw");
  LTS5_LOG_INFO("\tWrite model information into file : ./" << output_filename << ".dat");
  t3_convert_t::write_to_raw(*core_tensor, "", output_filename + ".raw");
  t3_convert_t::write_datfile("", output_filename + ".dat");
  tucker_tensor->_u1_comp->write_to_raw(dir,"U1_comp");
  tucker_tensor->_u2_comp->write_to_raw(dir,"U2_comp");
  tucker_tensor->_u3_comp->write_to_raw(dir,"U3_comp");
  //Convert to OCV mutlidimensionnal matrix
  LTS5_LOG_INFO("\tConvert core tensor into opencv n-dims matrix...");
  int size[] = {RANK_VERTEX,RANK_IDENTITY,RANK_EXPRESSION};
  int length = RANK_VERTEX * RANK_IDENTITY * RANK_EXPRESSION;
  cv::Mat core_cv = cv::Mat::zeros(3, size, CV_32FC1);
  for (int k = 0; k < RANK_EXPRESSION; ++k) {
    for (int row = 0; row < RANK_VERTEX; ++row) {
      for (int col = 0; col < RANK_IDENTITY; ++col) {
        core_cv.at<float>(row, col, k) = core_tensor->at(row, col, k);
      }
    }
  }
  //Get neutral expression weights
  auto* expr_vector = new vmml::vector<RANK_EXPRESSION>;
  tucker_tensor->_u3_comp->get_row(0, *expr_vector);
  //Compute average identity weights
  cv::Mat id_weight = cv::Mat(RANK_IDENTITY,
                              SIZE_IDENTIY_FOLD,
                              CV_32FC1,
                              &tucker_tensor->_u2_comp->array[0]);
  cv::Mat avg_id_weight = cv::Mat(RANK_IDENTITY, 1, CV_32FC1);
  cv::reduce(id_weight, avg_id_weight, 1, CV_REDUCE_AVG);

  // Dump into file
  std::string fname = output_filename + "_ocv.raw";
  FILE* file_id = fopen(fname.c_str(), "wb");
  if (file_id) {
    // Ok, wrote core tensor
    fwrite(reinterpret_cast<const void*>(&size[0]),
           sizeof(size[0]),
           1,
           file_id);
    fwrite(reinterpret_cast<const void*>(&size[1]),
           sizeof(size[0]),
           1,
           file_id);
    fwrite(reinterpret_cast<const void*>(&size[2]),
           sizeof(size[0]),
           1,
           file_id);
    fwrite(reinterpret_cast<const void*>(core_cv.data),
           sizeof(float),
           length,
           file_id);
    //Write neutral mean shape
    fwrite(reinterpret_cast<const void*>(&mean_shape_.rows),
           sizeof(mean_shape_.rows),
           1,
           file_id);
    fwrite(reinterpret_cast<const void*>(mean_shape_.data),
           sizeof(float),
           mean_shape_.rows,
           file_id);
    //Write neutral expression weight
    int expr_length = static_cast<int>(expr_vector->size());
    fwrite(reinterpret_cast<const void*>(&expr_length),
           sizeof(expr_length),
           1,
           file_id);
    fwrite(reinterpret_cast<const void*>(&expr_vector->array[0]),
           sizeof(float),
           expr_length,
           file_id);
    //Write avg identity weights
    fwrite(reinterpret_cast<const void*>(&avg_id_weight.rows),
           sizeof(avg_id_weight.rows),
           1,
           file_id);
    fwrite(reinterpret_cast<const void*>(avg_id_weight.data),
           sizeof(float),
           avg_id_weight.rows,
           file_id);
    fflush(file_id);
    fclose(file_id);
  } else {
    //Error writing
    LTS5_LOG_ERROR("Error writting OCV model...");
  }

  // Error
  double err = tucker_tensor->error(*train_tensor_);
  LTS5_LOG_INFO("\tModel Build with error : " << err);

  // Clear
  delete tucker_tensor;
  delete core_tensor;
  delete expr_vector;
}

}  // namespace LTS5
