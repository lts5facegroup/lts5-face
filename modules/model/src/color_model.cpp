/**
 *  @file   color_model.cpp
 *  @brief
 *  @ingroup
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/model/color_model.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name  ColorModel
 * @fn    ColorModel(const cv::Mat& mean, const cv::Mat& variation,
 *                   const cv::Mat& prior, const int n_channel)
 * @brief Specialized constructor
 * @param[in] mean        Mean value
 * @param[in] variation   Variation matrix (Eigen vectors in column)
 * @param[in] prior       Prior (Eigen values)
 * @param[in] n_channels  Number of channels in the flattened mean.
 */
template<typename T>
ColorModel<T>::ColorModel(const cv::Mat& mean,
                          const cv::Mat& variation,
                          const cv::Mat& prior,
                          const int& n_channels) : PCAModel<T>::PCAModel(mean,
                                                                         variation,
                                                                         prior,
                                                                         n_channels) {
  n_texel_ = static_cast<size_t>(this->mean_.rows / this->n_channels_);
}

/*
 * @name  Load
 * @fn    int Load(const std::string& filename)
 * @brief Load from a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ColorModel<T>::Load(const std::string& filename) {
  int err = -1;
  std::ifstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Load(stream);
  }
  return err;
}

/*
 * @name  Load
 * @fn    int Load(std::istream& stream)
 * @brief Load from a given binary \p stream
 * @param[in] stream Binary stream to load model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ColorModel<T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    int s = ScanStreamForObject(stream, HeaderObjectType::kColorModel);
    if (s == 0) {
      // Load PCA Model
      err = PCAModel<T>::Load(stream);
      // Init n_texel_
      n_texel_ = static_cast<size_t>(this->mean_.rows / this->n_channels_);
      // Sanity check
      err |= stream.good() ? 0 : -1;
    }
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(const std::string& filename) const
 * @brief Save to a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ColorModel<T>::Save(const std::string& filename) const {
  int err = -1;
  std::ofstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Save(stream);
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(std::istream& stream) const
 * @brief Save to a given binary \p stream
 * @param[in] stream Binary stream to save model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ColorModel<T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Type + Size
    int type = static_cast<int>(HeaderObjectType::kColorModel);
    int sz = this->ComputeObjectSize();
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Dump PCAModel
    err = PCAModel<T>::Save(stream);
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize(void) const
 * @brief Compute object size in byte
 * @return    Object's size
 */
template<typename T>
int ColorModel<T>::ComputeObjectSize(void) const {
  int sz = 2 * sizeof(int);
  sz += PCAModel<T>::ComputeObjectSize();
  return sz;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Project
 * @fn    virtual void Project(const Mesh& instance,
                               const cv::Mat& data,
                               const bool& p_scaled,
                               cv::Mat* p) const = 0
 * @brief Estimate parameters for a given instance
 * @param[in] instance    Instance for which parameters need to be estimated
 * @param[in] data        Extra data need for sampling (i.e. texture map)
 * @param[in] p_scaled    True indicate that coefficient will relative to
 *                        eigenvalues
 * @param[out] p          Estimated parameters
 */
template<typename T>
void ColorModel<T>::Project(const Mesh& instance,
                            const cv::Mat& data,
                            const bool& p_scaled,
                            cv::Mat* p) const {
  using LA = LinearAlgebra<T>;
  using TType = typename LinearAlgebra<T>::TransposeType;
  // Remove mean
  const auto& color = instance.get_vertex_color();
  static cv::Mat tbuff, buff;
  tbuff = cv::Mat(static_cast<int>(color.size() * this->n_channels_),
                  1,
                  cv::DataType<T>::type,
                  (void*)color.data()).clone();
  // Estimate parameters
  LA::Axpy(this->mean_, T(-1.0), &tbuff);
  if (p_scaled) {
    LA::Gemv(this->variation_, TType::kTranspose, T(1.0), tbuff, T(0.0), &buff);
    LA::Sbmv(this->inv_prior_, T(1.0), buff, T(0.0), p);
  } else {
    LA::Gemv(this->variation_, TType::kTranspose, T(1.0), tbuff, T(0.0), p);
  }
}

/*
 * @name  Sample
 * @fn    void virtual Sample(const Mesh& instance,
                              const cv::Mat& data,
                              const std::vector<ImagePoint>& pts,
                              const InstanceType& type,
                              cv::Mat* sample) const
 * @param[in] instance    Mesh instance
 * @param[in] data        Extra data need for sampling (i.e. texture map)
 * @param[in] pts         List of point where to sample
 * @param[in] type        Type of sampling
 * @param[out] sample     Sampled elements
 */
template<typename T>
void ColorModel<T>::Sample(const Mesh& instance,
                           const cv::Mat& data,
                           const std::vector<ImagePoint>& pts,
                           const InstanceType& type,
                           cv::Mat* sample) const {
  int n = static_cast<int>(pts.size());
  const int c = this->n_channels_;
  if (type == InstanceType::kTex) {
    sample->create(n * c, 1, cv::DataType<T>::type);
    const auto& color = instance.get_vertex_color();
    for (int i = 0; i < n; ++i) {
      int idx = i * c;
      const auto& p = pts[i];
      const auto& va = color[p.bcoord.va_idx];
      const auto& vb = color[p.bcoord.vb_idx];
      const auto& vc = color[p.bcoord.vc_idx];
      auto s = p.bcoord.alpha * va;
      s += p.bcoord.beta * vb;
      s += p.bcoord.gamma * vc;
      sample->at<T>(idx) = s.r_;
      sample->at<T>(idx + 1) = s.g_;
      sample->at<T>(idx + 2) = s.b_;
    }
  } else if (type == InstanceType::kTexVariation ||
             type == InstanceType::kTexScaledVariation) {
    const int n_pc = this->n_principle_component_;
    sample->create(n * c, n_pc, cv::DataType<T>::type);
    const cv::Mat& var = type == InstanceType::kTexVariation ?
                         this->variation_ :
                         this->scaled_variation_;
    for (int i = 0; i < n; ++i) {
      const auto& p = pts[i];
      const auto& pa = var(cv::Rect(0, p.bcoord.va_idx * c, n_pc, c));
      const auto& pb = var(cv::Rect(0, p.bcoord.vb_idx * c, n_pc, c));
      const auto& pc = var(cv::Rect(0, p.bcoord.vc_idx * c, n_pc, c));
      cv::Mat ps = p.bcoord.alpha * pa;
      ps += p.bcoord.beta * pb;
      ps += p.bcoord.gamma * pc;
      ps.copyTo((*sample)(cv::Rect(0, i * c, n_pc, c)));
    }
  } else if (type == InstanceType::kMeanTex) {
    sample->create(n * c, 1, cv::DataType<T>::type);
    const cv::Mat& var = this->mean_;
    for (int i = 0; i < n; ++i) {
      const auto& p = pts[i];
      const auto& pa = var(cv::Rect(0, p.bcoord.va_idx * c, 1, c));
      const auto& pb = var(cv::Rect(0, p.bcoord.vb_idx * c, 1, c));
      const auto& pc = var(cv::Rect(0, p.bcoord.vc_idx * c, 1, c));
      cv::Mat ps = p.bcoord.alpha * pa;
      ps += p.bcoord.beta * pb;
      ps += p.bcoord.gamma * pc;
      ps.copyTo((*sample)(cv::Rect(0, i * c, 1, c)));
    }
  } else {
    LTS5_LOG_ERROR("Sampler can not handle this type of instance");
  }
}

/*
 * @name  Generate
 * @fn    void Generate(const cv::Mat& p,
                        const bool& p_scaled,
                        Mesh* instance)
 * @brief Generate an instance given a set of coefficients \p p.
 * @param[in] p   Nodel's coefficients
 * @param[out] instance   Generated instance
 */
template<typename T>
void ColorModel<T>::Generate(const cv::Mat& p,
                             const bool& p_scaled,
                             Mesh* instance) const {
  // Texel init ?
  auto& texel = instance->get_vertex_color();
  if (texel.size() != n_texel_) {
    texel.resize(n_texel_);
  }
  // Generate instance
  PCAModel<T>::Generate(p, p_scaled, &texel[0].r_);
}

/*
 * @name  Generate
 * @fn    void Generate(Mesh* instance) const
 * @brief Generate a random instance
 * @param[out] instance   Randomly generated instance
 */
template<typename T>
void ColorModel<T>::Generate(Mesh* instance) const {
  // Texel init ?
  auto& texel = instance->get_vertex_color();
  if (texel.size() != n_texel_) {
    texel.resize(n_texel_);
  }
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  static cv::Mat p(this->variation_.cols, 1, cv::DataType<T>::type);
  cv::randn(p, T(0.0), T(1.0));
  // Generate instance
  PCAModel<T>::Generate(p, true, &texel[0].r_);
}

/*
 * @name  Generate
 * @fn    void Generate(cv::Mat* p, Mesh* instance) const
 * @brief Generate a random instance
 * @param[out] p  Parameters used for instance generation (scaled)
 * @param[out] instance   Randomly generated instance
 */
template<typename T>
void ColorModel<T>::Generate(cv::Mat* p, Mesh* instance) const {
  // Texel init ?
  auto& texel = instance->get_vertex_color();
  if (texel.size() != n_texel_) {
    texel.resize(n_texel_);
  }
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  p->create(this->n_principle_component_, 1, cv::DataType<T>::type);
  cv::randn(*p, T(0.0), T(0.5));
  // Generate instance
  PCAModel<T>::Generate(*p, true, &texel[0].r_);
}

#pragma mark -
#pragma mark Proxy

/*
 * @name  Create
 * @fn    PCAModel<T>* Create(void) const
 * @brief Create a PCAModel instance for a given type
 * @return Instance of a given class
 */
template<typename T>
PCAModel<T>* ColorModelProxy<T>::Create(void) const {
  return new ColorModel<T>();
}

/*
 * @name  Type
 * @fn    HeaderObjectType Type(void) const
 * @brief Provide the type represented by this proxy
 * @return    Object type
 */
template<typename T>
HeaderObjectType ColorModelProxy<T>::Type(void) const {
  return HeaderObjectType::kColorModel;
}

/*
 * @name  Name
 * @fn    const char* Name(void) const
 * @brief Provide the name represented by this proxy
 * @return    Object name
 */
template<typename T>
const char* ColorModelProxy<T>::Name(void) const {
  return "ColorModel";
}

// Explicit registration
ColorModelProxy<float> color_model_proxyf;
ColorModelProxy<double> color_model_proxyd;

#pragma mark -
#pragma mark Explicit instantiation

/** Float */
template class ColorModel<float>;
/** Double */
template class ColorModel<double>;

}  // namepsace LTS5
