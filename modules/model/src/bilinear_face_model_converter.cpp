/**
 *  @file   bilinear_face_model_converter.cpp
 *  Code
 *
 *  Created by Christophe Ecabert on 03/10/16.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>

#include "vmmlib/t3_ttm.hpp"

#include "lts5/model/bilinear_face_model_converter.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 *  @name   BilinearFaceModelConverter
 *  @fn     BilinearFaceModelConverter(void)
 *  @brief  Constructor
 */
BilinearFaceModelConverter::BilinearFaceModelConverter(void) {

}

/*
 *  @name   ~BilinearFaceModelConverter
 *  @fn     ~BilinearFaceModelConverter(void)
 *  @brief  Destructor
 */
BilinearFaceModelConverter::~BilinearFaceModelConverter(void) {

}

/*
 *  @name LoadModel
 *  @fn int LoadModel(const std::string& path)
 *  @brief  Load data from bilinear model file
 *  @param[in]  path          Path to model file
 *  @return -1 if error, 0 otherwise
 */
int BilinearFaceModelConverter::LoadModel(const std::string& path) {
  int err = -1;
  std::ifstream in(path.c_str(),
                   std::ios_base::in|std::ios_base::binary);
  if (in.is_open()) {
    // Query dimension
    int dim[3];
    in.read(reinterpret_cast<char*>(&dim[0]), 3 * sizeof(dim[0]));
    // Read core
    core_.create(3, dim, CV_32FC1);
    int length = dim[0] * dim[1] * dim[2] * sizeof(float);
    in.read(reinterpret_cast<char*>(core_.data), length);
    // Read mean shape
    int meanshape_dim = 0;
    in.read(reinterpret_cast<char*>(&meanshape_dim), sizeof(meanshape_dim));
    meanshape_.create(meanshape_dim, 1, CV_32FC1);
    in.read(reinterpret_cast<char*>(meanshape_.data),
            meanshape_dim * sizeof(float));
    // Read neutral expression (obsolete in this case)
    int neutral_dim = 0;
    in.read(reinterpret_cast<char*>(&neutral_dim), sizeof(neutral_dim));
    neutral_exp_.create(neutral_dim, 1, CV_32FC1);
    in.read(reinterpret_cast<char*>(neutral_exp_.data),
            neutral_dim * sizeof(float));
    // Average identitiy
    int identity_dim = 0;
    in.read(reinterpret_cast<char*>(&identity_dim), sizeof(identity_dim));
    average_id_.create(identity_dim, 1, CV_32FC1);
    in.read(reinterpret_cast<char*>(average_id_.data),
            identity_dim * sizeof(float));
    // Done
    err = in.good() ? 0 : -1;
    in.close();
  }
  return err;
}

/*
 *  @name LoadBasis
 *  @fn int LoadBasis(const std::string& path)
 *  @brief  Load basis data from tensor decomposition
 *  @param[in]  path          Path to model file
 *  @return -1 if error, 0 otherwise
 */
int BilinearFaceModelConverter::LoadBasis(const std::string& path) {
  expr_basis_.read_from_raw("", path);
  return 0;
}

#pragma mark -
#pragma mark Usage

/*
 *  @name   ConvertToVmmlFormat
 *  @fn     void ConvertToVmmlFormat(void)
 *  @brief  Convert face model to vmml format
 */
void BilinearFaceModelConverter::ConvertTo(ContainerType type) {
  auto dim = core_.size;
  // Loop over vertex
  for (int v = 0; v < dim[0]; ++v) {
    // Loop over id
    for (int i = 0; i < dim[1]; ++i) {
      // Loop over expr
      for (int e = 0; e < dim[2]; ++e) {
        if (type == kVmml) {
          tensor_.at(v, i, e) = core_.at<float>(v, i, e);
        } else {
          //core_.at<float>(v, i, e) = blendshape_tensor_.at(v, i, e);
          if (e == 0) {
            core_.at<float>(v, i, e) = blendshape_tensor_.at(v, i, e);
          } else {
            core_.at<float>(v, i, e) = (blendshape_tensor_.at(v, i, e) -
                                        blendshape_tensor_.at(v, i, 0));
          }
        }
      }
    }
  }
}

/*
 *  @name   GenerateBlendshapeModel
 *  @fn     void GenerateBlendshapeModel(void)
 *  @brief  Generate blendshape model from bilinear face model
 */
void BilinearFaceModelConverter::GenerateBlendshapeModel(void) {
  // Multiply core tensor by expression basis
  vmml::t3_ttm::multiply_lateral_fwd(tensor_, expr_basis_, blendshape_tensor_);
  // Convert to OpenCV
  int dim[3] = {VertexSize, CIdentitySize, ExpressionSize};
  core_.create(3, dim, CV_32FC1);
  this->ConvertTo(ContainerType::kOpenCV);
  // Update expression vector
  neutral_exp_ = 0.f;
  neutral_exp_.at<float>(0) = 1.f;
}

/*
 *  @name   Save
 *  @fn     int Save(const std::string& output_name)
 *  @brief  Save blendshape model to file
 *  @param[in]  output_name Model filename to save as
 *  @return -1 if error, 0 otherwise
 */
int BilinearFaceModelConverter::Save(const std::string& output_name) {
  int err = -1;
  std::ofstream out(output_name.c_str(), std::ios_base::binary);
  if (out.is_open()) {
    // Write model
    auto dim = core_.size;
    int length = dim[0] * dim[1] * dim[2];
    out.write(reinterpret_cast<const char*>(&dim.p[0]), 3 * sizeof(dim.p[0]));
    out.write(reinterpret_cast<const char*>(core_.data), length * sizeof(float));
    // Meanshape
    out.write(reinterpret_cast<const char*>(&meanshape_.rows),
              sizeof(meanshape_.rows));
    out.write(reinterpret_cast<const char*>(meanshape_.data),
              meanshape_.rows * sizeof(float));
    // Neutral expression
    out.write(reinterpret_cast<const char*>(&neutral_exp_.rows),
              sizeof(neutral_exp_.rows));
    out.write(reinterpret_cast<const char*>(neutral_exp_.data),
              neutral_exp_.rows * sizeof(float));
    // Average ID
    out.write(reinterpret_cast<const char*>(&average_id_.rows),
              sizeof(average_id_.rows));
    out.write(reinterpret_cast<const char*>(average_id_.data),
              average_id_.rows * sizeof(float));
    // Done
    err = out.good() ? 0 : -1;
    out.close();
  }
  return err;
}
}  // namespace LTS5
