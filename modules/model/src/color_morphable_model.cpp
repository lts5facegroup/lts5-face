/**
 *  @file   color_morphable_model.cpp
 *  @brief  Morphable model with per-vertex color scheme such as BFM
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/model/color_morphable_model.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/geometry/spherical_harmonic.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  MorphableModel
 * @fn    MorphableModel(PCAModel<T>* shape,
                         PCAModel<T>* color,
                         bool own_shape,
                         bool own_color)
 * @brief Constructor
 * @param[in] shape Statistical shape model to use.
 * @param[in] color Statistical ColorModel to use.
 */
template<typename T>
ColorMorphableModel<T>::ColorMorphableModel(PCAModel<T>* shape,
                                            ColorModel<T>* color) :
        MorphableModel<T>::MorphableModel(shape, color) {
}

/*
 * @name  Load
 * @fn    int Load(const std::string& filename)
 * @brief Load from a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ColorMorphableModel<T>::Load(const std::string& filename) {
  int err = -1;
  std::ifstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Load(stream);
  }
  return err;
}

/*
 * @name  Load
 * @fn    int Load(std::istream& stream)
 * @brief Load from a given binary \p stream
 * @param[in] stream Binary stream to load model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ColorMorphableModel<T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    int s = ScanStreamForObject(stream, HeaderObjectType::kColorMorphableModel);
    if (s == 0) {
      // Load
      err = MorphableModel<T>::Load(stream);
    }
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(const std::string& filename) const
 * @brief Save to a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ColorMorphableModel<T>::Save(const std::string& filename) const {
  int err = -1;
  std::ofstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Save(stream);
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(std::istream& stream) const
 * @brief Save to a given binary \p stream
 * @param[in] stream Binary stream to save model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int ColorMorphableModel<T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Dump type + size
    int type = static_cast<int>(HeaderObjectType::kColorMorphableModel);
    int sz = this->ComputeObjectSize();
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Dump
    err = MorphableModel<T>::Save(stream);
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  AddIllumination
 * @fn    void AddIllumination(const Matrix4x4<T>& normal_matrix, const cv::Mat& ip,
                               Mesh* instance) const
 * @brief Add illumination contribution to the generated texture using
 *        spherical harmonics approximation. Should be implemented by derived
 *        model
 * @param[in] normal_matrix   Transformation matrix to go from model space into
 *                            camera space: Transform = (Model^-1)^T
 * @param[in] ip  Illumination parameters
 * @param[in,out] instance    Mesh instance where to add illumination
 */
template<typename T>
void ColorMorphableModel<T>::AddIllumination(const Matrix4x4<T>& normal_matrix,
                                             const cv::Mat& ip,
                                             Mesh* instance) const {
  // Spherical Harmonics generator
  LTS5::SphericalHarmonic<T, 3> sh;

  const auto& normal = instance->get_normal();
  auto& color = instance->get_vertex_color();
  cv::Mat factors;
  for (size_t i = 0; i < color.size(); ++i) {
    // Compute basis + lighting coefficients
    const auto& n = normal_matrix * normal[i];
    auto& c = color[i];
    sh.Generate(n);
    sh(ip, &factors);
    // Applying lighting factor
    c.r_ *= factors.at<T>(0);
    c.g_ *= factors.at<T>(1);
    c.b_ *= factors.at<T>(2);
  }
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize() const
 * @brief Compute object size in byte
 * @return    Object's size
 */
template<typename T>
int ColorMorphableModel<T>::ComputeObjectSize() const {
  int sz = 2 * sizeof(int);
  sz += MorphableModel<T>::ComputeObjectSize();
  return sz;
}

#pragma mark -
#pragma mark Proxy

/*
 * @name  Create
 * @fn    MorphableModel<T>* Create() const
 * @brief Create a MorphableModel instance for a given type
 * @return Instance of a given class
 */
template<typename T>
MorphableModel<T>* ColorMorphableModelProxy<T>::Create() const {
  return new  ColorMorphableModel<T>();
}

/*
 * @name  Type
 * @fn    HeaderObjectType Type() const
 * @brief Provide the type represented by this proxy
 * @return    Object type
 */
template<typename T>
HeaderObjectType ColorMorphableModelProxy<T>::Type() const {
  return HeaderObjectType::kColorMorphableModel;
}

/*
 * @name  Name
 * @fn    const char* Name() const
 * @brief Provide the name represented by this proxy
 * @return    Object name
 */
template<typename T>
const char* ColorMorphableModelProxy<T>::Name() const {
  return "ColorMorphableModel";
}

// Explicit registration
ColorMorphableModelProxy<float> color_morphable_modelf;
ColorMorphableModelProxy<double> color_morphable_modeld;

#pragma mark -
#pragma mark Explicit instantiation

/** Float */
template class ColorMorphableModel<float>;
/** Double */
template class ColorMorphableModel<double>;

}  // namepsace LTS5
