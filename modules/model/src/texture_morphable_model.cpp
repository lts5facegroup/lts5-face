/**
 *  @file   texture_morphable_model.hpp
 *  @brief  Morphable model with texture map as color scheme
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/model/texture_morphable_model.hpp"
#include "lts5/utils/file_io.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  Load
 * @fn    int Load(const std::string& filename)
 * @brief Load from a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TextureMorphableModel<T>::Load(const std::string& filename) {
  int err = -1;
  std::ifstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Load(stream);
  }
  return err;
}

/*
 * @name  Load
 * @fn    int Load(std::istream& stream)
 * @brief Load from a given binary \p stream
 * @param[in] stream Binary stream to load model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TextureMorphableModel<T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    int s = ScanStreamForObject(stream, HeaderObjectType::kTextureMorphableModel);
    if (s == 0) {
      // Load
      err = MorphableModel<T>::Load(stream);
    }
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(const std::string& filename) const
 * @brief Save to a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TextureMorphableModel<T>::Save(const std::string& filename) const {
  int err = -1;
  std::ofstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Save(stream);
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(std::istream& stream) const
 * @brief Save to a given binary \p stream
 * @param[in] stream Binary stream to save model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int TextureMorphableModel<T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Dump type + size
    int type = static_cast<int>(HeaderObjectType::kTextureMorphableModel);
    int sz = this->ComputeObjectSize();
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Dump
    err = MorphableModel<T>::Save(stream);
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize(void) const
 * @brief Compute object size in byte
 * @return    Object's size
 */
template<typename T>
int TextureMorphableModel<T>::ComputeObjectSize(void) const {
  int sz = 2 * sizeof(int);
  sz += MorphableModel<T>::ComputeObjectSize();
  return sz;
}

#pragma mark -
#pragma mark Proxy

/*
 * @name  Create
 * @fn    MorphableModel<T>* Create(void) const
 * @brief Create a MorphableModel instance for a given type
 * @return Instance of a given class
 */
template<typename T>
MorphableModel<T>* TextureMorphableModelProxy<T>::Create(void) const {
  return new  TextureMorphableModel<T>();
}

/*
 * @name  Type
 * @fn    HeaderObjectType Type(void) const
 * @brief Provide the type represented by this proxy
 * @return    Object type
 */
template<typename T>
HeaderObjectType TextureMorphableModelProxy<T>::Type(void) const {
  return HeaderObjectType::kTextureMorphableModel;
}

/*
 * @name  Name
 * @fn    const char* Name(void) const
 * @brief Provide the name represented by this proxy
 * @return    Object name
 */
template<typename T>
const char* TextureMorphableModelProxy<T>::Name(void) const {
  return "TextureMorphableModel";
}

// Explicit registration
TextureMorphableModelProxy<float> texture_morphable_modelf;
TextureMorphableModelProxy<double> texture_morphable_modeld;

#pragma mark -
#pragma mark Explicit instantiation

/** Float */
template class TextureMorphableModel<float>;
/** Double */
template class TextureMorphableModel<double>;

}  // namepsace LTS5
