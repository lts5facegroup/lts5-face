/**
 *  @file   morphable_model_factory.cpp
 *  @brief
 *  @ingroup
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/model/morphable_model_factory.hpp"

#include <vector>

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  Get
 * @fn    static MorphableModelFactory<T>& Get(void)
 * @brief Factory's accessors
 * @return    Single instance of MorphableModelFactory
 */
template<typename T>
MorphableModelFactory<T>& MorphableModelFactory<T>::Get(void) {
  static MorphableModelFactory<T> factory;
  return factory;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  CreateByName
 * @fn    Model* CreateByName(const std::string& name) const
 * @brief Create an object that match the provided \p name
 * @param[in] name Object name
 * @return    Instance corresponding to name or nullptr if name is unknown
 */
template<typename T>
typename MorphableModelFactory<T>::Model*
MorphableModelFactory<T>::CreateByName(const std::string& name) const {
  Model* model = nullptr;
  const char* cname = name.c_str();
  for (auto& p : proxies_) {
    if (strcmp(cname, p->Name()) == 0) {
      model = p->Create();
      break;
    }
  }
  return model;
}

/*
 * @name  CreateByType
 * @fn    Model* CreateByType(const HeaderObjectType type) const
 * @brief Create an object that match the provided \p type
 * @param[in] type Object type
 * @return    Instance corresponding to the type or nullptr if type is unknown
 */
template<typename T>
typename MorphableModelFactory<T>::Model*
MorphableModelFactory<T>::CreateByType(const HeaderObjectType type) const {
  Model* model = nullptr;
  for (auto& p : proxies_) {
    if (type == p->Type()) {
      model = p->Create();
      break;
    }
  }
  return model;
}

/*
 * @name  Register
 * @fn    void Register(const MorphableModelProxy<T>* proxy)
 * @brief Register a new \p proxy with the factory
 * @param[in] proxy Proxy to register
 */
template<typename T>
void MorphableModelFactory<T>::Register(const MorphableModelProxy<T>* proxy) {
  proxies_.push_back(proxy);
}

#pragma mark -
#pragma mark Explicit instantiation

/** Float */
template class MorphableModelFactory<float>;
/** Double */
template class MorphableModelFactory<double>;

}  // namepsace LTS5
