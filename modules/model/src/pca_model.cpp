/**
 *  @file   pca_model.cpp
 *  @brief  Statistical model based on Principle Component Analysis
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <fstream>

#include "lts5/model/pca_model.hpp"
#include "lts5/model/pca_model_factory.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  PCAModel
 * @fn    PCAModel(const cv::Mat& mean, const cv::Mat& variation,
 *                 const cv::Mat& prior, const int& n_channel)
 * @brief Specialized constructor
 * @param[in] mean        Mean value
 * @param[in] variation   Variation matrix (Eigen vectors in column)
 * @param[in] prior       Prior (Eigen values)
 * @param[in] n_channels  Number of channels in the flattened mean.
 */
template<typename T>
PCAModel<T>::PCAModel(const cv::Mat& mean,
                      const cv::Mat& variation,
                      const cv::Mat& prior,
                      const int& n_channel) : mean_(mean.clone()),
                                              variation_(variation.clone()),
                                              prior_(prior.clone()),
                                              n_channels_(n_channel),
                                              n_principle_component_(variation.cols),
                                              n_element_(variation.rows){
  // Compute scaled variation + inv prior
  inv_prior_ = T(1.0) / prior_;
  scaled_variation_ = variation_ * cv::Mat::diag(prior_);
}

/*
 * @name  Load
 * @fn    virtual int Load(const std::string& filename)
 * @brief Load from a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int PCAModel<T>::Load(const std::string& filename) {
  int err = -1;
  std::ifstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Load(stream);
  }
  return err;
}

/*
 * @name  Load
 * @fn    virtual int Load(std::istream& stream)
 * @brief Load from a given binary \p stream
 * @param[in] stream Binary stream to load model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int PCAModel<T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    int s = ScanStreamForObject(stream, HeaderObjectType::kPCAModel);
    if (s == 0) {
      // Load
      err = ReadMatFromBinTyped<T>(stream, &mean_);
      err |= ReadMatFromBinTyped<T>(stream, &variation_);
      err |= ReadMatFromBinTyped<T>(stream, &prior_);
      // Channels
      stream.read(reinterpret_cast<char*>(&n_channels_), sizeof(n_channels_));
      // Init vars
      n_principle_component_ = variation_.cols;
      n_element_ = variation_.rows;
      inv_prior_ = T(1.0) / prior_;
      scaled_variation_ = variation_ * cv::Mat::diag(prior_);
      // Sanity check
      err |= stream.good() ? 0 : -1;
    }
  }
  return err;
}

/*
 * @name  Save
 * @fn    virtual int Save(const std::string& filename) const
 * @brief Save to a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int PCAModel<T>::Save(const std::string& filename) const {
  int err = -1;
  std::ofstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Save(stream);
  }
  return err;
};

/*
 * @name  Save
 * @fn    virtual int Save(std::istream& stream) const
 * @brief Save to a given binary \p stream
 * @param[in] stream Binary stream to save model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int PCAModel<T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Compute size
    int sz = this->ComputeObjectSize();
    int type = static_cast<int>(HeaderObjectType::kPCAModel);
    // Dump
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Mean, var, prior
    err = WriteMatToBin(stream, mean_);
    err |= WriteMatToBin(stream, variation_);
    err |= WriteMatToBin(stream, prior_);
    // Channels
    stream.write(reinterpret_cast<const char*>(&n_channels_),
                 sizeof(n_channels_));
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    virtual int ComputeObjectSize() const
 * @brief Compute object size in byte
 * @return    Object's size
 */
template<typename T>
int PCAModel<T>::ComputeObjectSize() const {
  int sz = 9 * sizeof(int);
  sz += mean_.total() * mean_.elemSize();
  sz += variation_.total() * variation_.elemSize();
  sz += prior_.total() * prior_.elemSize();
  sz += sizeof(n_channels_);
  return sz;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Generate
 * @fn    virtual void Generate(const cv::Mat& p,
                                const bool& p_scaled,
                                T* instance) const
 * @brief Generate an instance given a set of coefficients \p p.
 * @param[in] p   Nodel's coefficients
 * @param[in] p_scaled    True indicate that coefficient are relative to
 *                        eigenvalues
 * @param[out] instance   Generated instance
 */
template<typename T>
void PCAModel<T>::Generate(const cv::Mat& p,
                           const bool& p_scaled,
                           T* instance) const {
  using LA = typename LTS5::LinearAlgebra<T>;
  using TType = typename LTS5::LinearAlgebra<T>::TransposeType;
  // Init containter access
  cv::Mat out(mean_.rows,
              mean_.cols,
              cv::DataType<T>::type,
              (void*)instance);
  mean_.copyTo(out);
  // Generate instance
  const cv::Mat& var = p_scaled ? scaled_variation_ : variation_;
  LA::Gemv(var,
           TType::kNoTranspose,
           T(1.0),
           p,
           T(1.0),
           &out);
}

/*
 * @name  Generate
 * @fn    virtual void Generate(T* instance) const
 * @brief Generate a random instance
 * @param[out] instance   Randomly generated instance
 */
template<typename T>
void PCAModel<T>::Generate(T* instance) const {
  using LA = typename LTS5::LinearAlgebra<T>;
  using TType = typename LTS5::LinearAlgebra<T>::TransposeType;
  // Init containter access
  cv::Mat out(mean_.rows,
              mean_.cols,
              cv::DataType<T>::type,
              (void*)instance);
  mean_.copyTo(out);
  // Create random parameters
  cv::theRNG().state = static_cast<uint64_t>(cv::getTickCount());
  static cv::Mat p(variation_.cols, 1, cv::DataType<T>::type);
  cv::randn(p, T(0.0), T(1.0));
  // Generate instance
  LA::Gemv(scaled_variation_,
           TType::kNoTranspose,
           T(1.0),
           p,
           T(1.0),
           &out);
}

#pragma mark -
#pragma mark Proxy

/*
 * @name  PCAModelProxy
 * @fn    PCAModelProxy()
 * @brief Constructor
 */
template<typename T>
PCAModelProxy<T>::PCAModelProxy() {
  PCAModelFactory<T>::Get().Register(this);
}

#pragma mark -
#pragma mark Explicit instantiation

/** Float */
template class PCAModel<float>;
/** Double */
template class PCAModel<double>;

/** Float */
template class PCAModelProxy<float>;
/** Double */
template class PCAModelProxy<double>;

}  // namepsace LTS5