/**
 *  @file   multi_shape_model.cpp
 *  @brief  3D statistical shape model with more than one variation. For
 *          instance Id + Expr
 *  @ingroup    model
 *
 *  @author Christophe Ecabert
 *  @date   14/11/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/model/multi_shape_model.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  MultiShapeModel
 * @fn    MultiShapeModel(const cv::Mat& mean,
                         const cv::Mat& variation,
                         const cv::Mat& prior,
                         const int& n_channels,
                         const std::vector<Triangle>& tri,
                         const std::vector<int>& landmarks,
                         const std::vector<T>& cog
                         const std::vector<int>& dims)
 * @brief Specialized constructor
 * @param[in] mean        Mean value
 * @param[in] variation   Variation matrix (Eigen vectors in column)
 * @param[in] prior       Prior (Eigen values)
 * @param[in] n_channels  Number of channels in the flattened mean.
 * @param[in] tri         Triangulation
 * @param[in] landmarks   Facial landmark's indexes
 * @param[in] cog         Center of gravity of the shape model (offset)
 * @param[in] dims        Each shape dimensions. Must sum to the number of
 *                        principal components
 */
template<typename T>
MultiShapeModel<T>::MultiShapeModel(const cv::Mat& mean,
                                    const cv::Mat& variation,
                                    const cv::Mat& prior,
                                    const int& n_channels,
                                    const std::vector<Triangle>& tri,
                                    const std::vector<int>& landmarks,
                                    const std::vector<T>& cog,
                                    const std::vector<int>& dims) :
        ShapeModel<T>::ShapeModel(mean,
                variation,
                prior,
                n_channels,
                tri,
                landmarks,
                cog),
        dim_(dims) {
}

/*
 * @name  Load
 * @fn    int Load(std::istream& stream)
 * @brief Load from a given binary \p stream
 * @param[in] stream Binary stream to load model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MultiShapeModel<T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Load shape model
    int s = ScanStreamForObject(stream, HeaderObjectType::kMultiShapeModel);
    if (s == 0) {
      // Load PCA Model
      err = PCAModel<T>::Load(stream);
      // Load tri
      err |= this->LoadTriangle(stream);
      // Load indices
      err |= this->LoadLandmarks(stream);
      // Load dimensions
      int ndim = 0;
      stream.read(reinterpret_cast<char*>(&ndim), sizeof(ndim));
      this->dim_.resize(size_t(ndim));
      stream.read(reinterpret_cast<char*>(dim_.data()), ndim * sizeof(dim_[0]));
      // Load cog
      int n = 0;
      stream.read(reinterpret_cast<char*>(&n), sizeof(n));
      this->cog_.resize(n);
      stream.read(reinterpret_cast<char*>(this->cog_.data()), n * sizeof(T));
      err |= stream.good() ? 0 : -1;
      // Init var
      this->n_vertex_ = static_cast<size_t>(this->mean_.rows /
                                            this->n_channels_);
    }
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(std::ostream& stream) const
 * @brief Save to a given binary \p stream
 * @param[in] stream Binary stream to save model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MultiShapeModel<T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Dump size + type
    int sz = this->ComputeObjectSize();
    int type = static_cast<int>(HeaderObjectType::kMultiShapeModel);
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Dump PCA model
    err = PCAModel<T>::Save(stream);
    // Dump tri
    stream.write(reinterpret_cast<const char*>(&this->n_tri_),
                 sizeof(this->n_tri_));
    stream.write(reinterpret_cast<const char*>(this->tri_.data()),
                 this->tri_.size() * sizeof(Triangle));
    // Dump facial landmarks indices
    stream.write(reinterpret_cast<const char*>(&this->n_landmarks_),
                 sizeof(this->n_landmarks_));
    stream.write(reinterpret_cast<const char*>(this->landmarks_.data()),
                 this->landmarks_.size() * sizeof(this->landmarks_[0]));
    // Dump dimensions
    sz = static_cast<int>(dim_.size());
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    stream.write(reinterpret_cast<const char*>(dim_.data()),
                 dim_.size() * sizeof(dim_[0]));
    // Dump cog
    sz = static_cast<int>(this->cog_.size());
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    stream.write(reinterpret_cast<const char*>(this->cog_.data()),
                 this->cog_.size() * sizeof(T));
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize() const
 * @brief Compute object size in byte
 * @return    Object's size
 */
template<typename T>
int MultiShapeModel<T>::ComputeObjectSize() const {
  int sz = 2 * sizeof(int);
  sz += PCAModel<T>::ComputeObjectSize();
  sz += sizeof(this->n_tri_);
  sz += this->tri_.size() * sizeof(Triangle);
  sz += 4;
  sz += this->landmarks_.size() * sizeof(this->landmarks_[0]);
  sz += sizeof(dim_[0]) * (dim_.size() + 1);
  sz += sizeof(int) + (this->cog_.size() + sizeof(this->cog_[0]));
  return sz;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Project
 * @fn    virtual void Project(const Mesh& instance,
                               const cv::Mat& data,
                               const bool p_scaled,
                               cv::Mat* p) const
 * @brief Estimate parameters for a given instance
 * @param[in] instance    Instance for which parameters need to be estimated
 * @param[in] data        Extra data need for sampling (i.e. texture map)
 * @param[in] p_scaled    True indicate that coefficient will relative to
 *                        eigenvalues
 * @param[out] p          Estimated parameters
 */
template<typename T>
void MultiShapeModel<T>::Project(const Mesh& instance,
                                 const cv::Mat& data,
                                 const bool p_scaled,
                                 cv::Mat* p) const {
  std::cout << "Not implemented yet !" << std::endl;


  p->create(this->n_principle_component_, 1, cv::DataType<T>::type);
  p->setTo(0.0);
}

#pragma mark -
#pragma mark MultiShapeSegmentModel

/*
 * @name  MultiShapeSegmentModel
 * @fn    MultiShapeSegmentModel(const cv::Mat& mean,
                          const cv::Mat& variation,
                          const cv::Mat& prior,
                          const int& n_channels,
                          const cv::Mat& mixing,
                          const int& n_segment,
                          const std::vector<Triangle>& tri,
                          const std::vector<int>& landmarks,
                          const std::vector<T>& cog,
                          const std::vector<int>& dims)
 * @brief Specialized constructor
 * @param[in] mean        Mean value
 * @param[in] variation   Variation matrix (Eigen vectors in column)
 * @param[in] prior       Prior (Eigen values)
 * @param[in] n_channels  Number of channels in the flattened mean.
 * @param[in] mixing      Mixing matrix for each segment. Must have the same
 *                        number of rows as the number of segement defined.
 * @param[in] n_segment   Number of segment defined
 * @param[in] tri         Triangulation
 * @param[in] landmarks   Facial landmark's indexes
 * @param[in] cog         Center of gravity of the shape model (offset)
 * @param[in] dims        Each shape dimensions. Must sum to the number of
 *                        principal components
 */
template<typename T>
MultiShapeSegmentModel<T>::
MultiShapeSegmentModel(const cv::Mat& mean,
                       const cv::Mat& variation,
                       const cv::Mat& prior,
                       const int& n_channels,
                       const cv::Mat& mixing,
                       const int& n_segment,
                       const std::vector<Triangle>& tri,
                       const std::vector<int>& landmarks,
                       const std::vector<T>& cog,
                       const std::vector<int>& dims) :
        MultiShapeModel<T>(mean,
                           variation,
                           prior,
                           n_channels,
                           tri,
                           landmarks,
                           cog,
                           dims),
        n_segment_(n_segment),
        mixing_(mixing) {
}

/**
 * @name  Load
 * @fn    int Load(std::istream& stream) override
 * @brief Load from a given binary \p stream
 * @param[in] stream Binary stream to load model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MultiShapeSegmentModel<T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Load shape model
    int s = ScanStreamForObject(stream,
                                HeaderObjectType::kMultiShapeSegmentModel);
    if (s == 0) {
      // Load MultiShape model
      err = MultiShapeModel<T>::Load(stream);
      // Load mixing matrix
      err |= this->LoadMixingMatrix(stream);
      err |= stream.good() ? 0 : -1;
    }
  }
  return err;
}

/**
 * @name  LoadMixingMatrix
 * @brief Load mixing matrix into the object
 * @param[in] stream Binary stream storing data
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int MultiShapeSegmentModel<T>::LoadMixingMatrix(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    // Number of segment
    stream.read(reinterpret_cast<char *>(&n_segment_),
                sizeof(this->n_segment_));
    // Mixing matrix
    err = ReadMatFromBinTyped<T>(stream, &mixing_);
  }
  return err;
}

/*
 * @name  Save
 * @fn    int Save(std::ostream& stream) const override
 * @brief Save to a given binary \p stream
 * @param[in] stream Binary stream to save model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MultiShapeSegmentModel<T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good()) {
    // Dump size + type
    int sz = this->ComputeObjectSize();
    int type = static_cast<int>(HeaderObjectType::kMultiShapeSegmentModel);
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Dump PCA model
    err = MultiShapeModel<T>::Save(stream);
    // Dump #Segment
    stream.write(reinterpret_cast<const char*>(&n_segment_),
                 sizeof(n_segment_));
    // Dump mixing matrix
    err |= WriteMatToBin(stream, mixing_);
    // Sanity check
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    int ComputeObjectSize() const override
 * @brief Compute object size in byte
 * @return    Object's size
 */
template<typename T>
int MultiShapeSegmentModel<T>::ComputeObjectSize() const {
  int sz = 2 * sizeof(int);
  sz += MultiShapeModel<T>::ComputeObjectSize();
  sz += sizeof(n_segment_);
  sz += 3 * sizeof(int) + mixing_.total() * mixing_.elemSize();
  return sz;
}














#pragma mark -
#pragma mark Proxy

/*
 * @name  Create
 * @fn    PCAModel<T>* Create() const
 * @brief Create a PCAModel instance for a given type
 * @return Instance of a given class
 */
template<typename T>
PCAModel<T>* MultiShapeModelProxy<T>::Create() const {
  return new MultiShapeModel<T>();
}

/*
 * @name  Type
 * @fn    HeaderObjectType Type() const
 * @brief Provide the type represented by this proxy
 * @return    Object type
 */
template<typename T>
HeaderObjectType MultiShapeModelProxy<T>::Type() const {
  return HeaderObjectType::kMultiShapeModel;
}

/*
 * @name  Name
 * @fn    const char* Name() const
 * @brief Provide the name represented by this proxy
 * @return    Object name
 */
template<typename T>
const char* MultiShapeModelProxy<T>::Name() const {
  return "MultiShapeModel";
}

/*
 * @name  Create
 * @fn    PCAModel<T>* Create() const
 * @brief Create a PCAModel instance for a given type
 * @return Instance of a given class
 */
template<typename T>
PCAModel<T>* MultiShapeSegmentModelProxy<T>::Create() const {
  return new MultiShapeSegmentModel<T>();
}

/*
 * @name  Type
 * @fn    HeaderObjectType Type() const
 * @brief Provide the type represented by this proxy
 * @return    Object type
 */
template<typename T>
HeaderObjectType MultiShapeSegmentModelProxy<T>::Type() const {
  return HeaderObjectType::kMultiShapeSegmentModel;
}

/*
 * @name  Name
 * @fn    const char* Name() const
 * @brief Provide the name represented by this proxy
 * @return    Object name
 */
template<typename T>
const char* MultiShapeSegmentModelProxy<T>::Name() const {
  return "MultiShapeSegmentModel";
}


#pragma mark -
#pragma mark Explicit instantiation

// Explicit registration
MultiShapeModelProxy<float> multi_shape_model_proxyf;
MultiShapeSegmentModelProxy<float> multi_shape_segment_model_proxyf;
// Explicit registration
MultiShapeModelProxy<double> multi_shape_model_proxyd;
MultiShapeSegmentModelProxy<double> multi_shape_segment_model_proxyd;

/** Float */
template class MultiShapeModel<float>;
template class MultiShapeSegmentModel<float>;
/** Double */
template class MultiShapeModel<double>;
template class MultiShapeSegmentModel<double>;

}  // namepsace LTS5