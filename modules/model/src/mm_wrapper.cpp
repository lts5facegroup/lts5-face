/**
 *  @file   morphable_model.cpp
 *  @brief
 *  @ingroup
 *
 *  @author Christophe Ecabert
 *  @date   15/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "lts5/model/pca_model_factory.hpp"
#include "lts5/model/morphable_model.hpp"
#include "lts5/model/morphable_model_factory.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/logger.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {


#pragma mark -
#pragma mark Initialization

/*
 * @name  MorphableModel
 * @fn    MorphableModel(PCAModel<T>* shape,
                         PCAModel<T>* color,
                         bool own_shape,
                         bool own_color)
 * @brief Constructor
 * @param[in] shape Statistical shape model to use.
 * @param[in] color Statistical color model to use.
 */
template<typename T>
MorphableModel<T>::MorphableModel(PCAModel<T>* shape, PCAModel<T>* color)
        : shape_(shape),
          texture_(color),
          n_vertex_(shape->get_n_element() / get_n_channels()),
          n_tri_(shape_->get_property(PCAModel<T>::Property::kTriangle)),
          n_texel_(texture_->get_n_element() / texture_->get_n_channels()) {
  this->InitLandmarksVariable();
}

/*
 * @name  Load
 * @fn    virtual int Load(const std::string& filename)
 * @brief Load from a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MorphableModel<T>::Load(const std::string& filename) {
  int err = -1;
  std::ifstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Load(stream);
  }
  return err;
}

/*
 * @name  Load
 * @fn    virtual int Load(std::istream& stream)
 * @brief Load from a given binary \p stream
 * @param[in] stream Binary stream to load model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MorphableModel<T>::Load(std::istream& stream) {
  int err = -1;
  if (stream.good()) {
    int s = ScanStreamForObject(stream, HeaderObjectType::kMorphableModel);
    if (s == 0) {
      // Read shape/texture type
      int type = -1;
      stream.read(reinterpret_cast<char*>(&type), sizeof(type));
      auto shp_type = static_cast<HeaderObjectType>(type);
      stream.read(reinterpret_cast<char*>(&type), sizeof(type));
      auto tex_type = static_cast<HeaderObjectType>(type);
      // Create shape model
      err = this->LoadShapeModel(stream, shp_type);
      // Create texture model
      err |= this->LoadTextureModel(stream, tex_type);
    }
  }
  return err;
}

/*
 * @name  Save
 * @fn    virtual int Save(const std::string& filename) const
 * @brief Save to a given \p filename
 * @param[in] filename    Path to the model file
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MorphableModel<T>::Save(const std::string& filename) const {
  int err = -1;
  std::ofstream stream(filename.c_str(), std::ios_base::binary);
  if (stream.is_open()) {
    err = this->Save(stream);
  }
  return err;
}

/*
 * @name  Save
 * @fn    virtual int Save(std::istream& stream) const
 * @brief Save to a given binary \p stream
 * @param[in] stream Binary stream to save model from
 * @return    -1 if error, 0 otherwise
 */
template<typename T>
int MorphableModel<T>::Save(std::ostream& stream) const {
  int err = -1;
  if (stream.good() && shape_ && texture_) {
    // Dump type + size
    int type = static_cast<int>(HeaderObjectType::kMorphableModel);
    int sz = this->ComputeObjectSize();
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    stream.write(reinterpret_cast<const char*>(&sz), sizeof(sz));
    // Dump shape model type
    type = static_cast<int>(shape_->get_type());
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    // Dump texture model type
    type = static_cast<int>(texture_->get_type());
    stream.write(reinterpret_cast<const char*>(&type), sizeof(type));
    // Dump shape model
    err = shape_->Save(stream);
    err |= texture_->Save(stream);
    // Sanity
    err |= stream.good() ? 0 : -1;
  }
  return err;
}

/*
 * @name  ComputeObjectSize
 * @fn    virtual int ComputeObjectSize() const
 * @brief Compute object size in byte
 * @return    Object's size
 */
template<typename T>
int MorphableModel<T>::ComputeObjectSize() const {
  int sz = (4 + 2) * sizeof(int);
  sz += shape_->ComputeObjectSize();
  sz += texture_->ComputeObjectSize();
  return sz;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Project
 * @fn    void Project(const Mesh& instance,
                       const cv::Mat& data,
                       const InstanceType& type,
                       cv::Mat* p) const
 * @brief Estimate parameters for a given instance (Shape or texture)
 * @param[in] instance    Instance for which parameters need to be estimated
 * @param[in] data        Extra data needed for projection (i.e. texture map)
 * @param[in] type        Type of instance
 * @param[in] p_scaled    True indicate that coefficient will relative to
 *                        eigenvalues
 * @param[out] p          Estimated parameters
 */
template<typename T>
void MorphableModel<T>::Project(const Mesh& instance,
                                const cv::Mat& data,
                                const InstanceType& type,
                                const bool& p_scaled,
                                cv::Mat* p) const {
  switch (type) {
    case InstanceType::kShape : {
      if (shape_) {
        shape_->Project(instance, data, p_scaled, p);
      }
    }
      break;
    case InstanceType::kTex : {
      if (texture_) {
        texture_->Project(instance, data, p_scaled, p);
      }
    }
      break;
    default:
      LTS5_LOG_ERROR("Unsupported instance type");
  }
}

/*
 * @name  Sample
 * @fn    void Sample(const Mesh& instance,
                      const cv::Mat& data,
                      const std::vector<ImagePoint>& pts,
                      const InstanceType type,
                      cv::Mat* sample) const
 * @param[in] instance    Mesh instance
 * @param[in] data        Extra data need for sampling (i.e. texture map)
 * @param[in] pts         List of point where to sample
 * @param[in] type        Type of sampling
 * @param[out] sample     Sampled elements
 */
template<typename T>
void MorphableModel<T>::Sample(const Mesh& instance,
                               const cv::Mat& data,
                               const std::vector<ImagePoint>& pts,
                               const InstanceType& type,
                               cv::Mat* sample) const {
  switch (type) {
    // Shape
    case InstanceType::kShape:
    case InstanceType::kMeanShape:
    case InstanceType::kShapeNormal:
    case InstanceType::kShapeVariation:
    case InstanceType::kShapeScaledVariation: shape_->Sample(instance,
                                                             data,
                                                             pts,
                                                             type,
                                                             sample);
      break;
    // Tex
    case InstanceType::kTex:
    case InstanceType::kMeanTex:
    case InstanceType::kTexVariation:
    case InstanceType::kTexScaledVariation: texture_->Sample(instance,
                                                             data,
                                                             pts,
                                                             type,
                                                             sample);
      break;
  }
}

/*
 * @name  Generate
 * @fn    virtual void Generate(const cv::Mat& sp,
                      const cv::Mat& tp,
                      const bool p_scaled,
                      Mesh* instance) const
 * @brief Generate an instance given a set of coefficients \p sp and \p tp.
 * @param[in] sp   Shape coefficients
 * @param[in] tp   Texture coefficients
 * @param[in] p_scaled    True indicate that coefficient are relative to
 *                        eigenvalues
 * @param[out] instance   Generated instance
 */
template<typename T>
void MorphableModel<T>::Generate(const cv::Mat& sp,
                                 const cv::Mat& tp,
                                 const bool& p_scaled,
                                 Mesh* instance) const {
  // Generate shape
  if (shape_) {
    // update shape + vertex normal
    shape_->Generate(sp, p_scaled, instance);
    instance->ComputeVertexNormal();
  }
  // Generate texture
  if (texture_) {
    texture_->Generate(tp, p_scaled, instance);
  }
}

/*
 * @name  Generate
 * @fn    virtual void Generate(Mesh* instance) const
 * @brief Generate a random instance
 * @param[out] instance   Randomly generated instance
 */
template<typename T>
void MorphableModel<T>::Generate(Mesh* instance) const {
  // Generate shape
  if (shape_) {
    shape_->Generate(instance);
  }
  // Generate texture
  if (texture_) {
    texture_->Generate(instance);
  }
}

/*
 * @name  Generate
 * @fn    virtual void Generate(cv::Mat* sp, cv::Mat* tp, Mesh* instance) const
 * @brief Generate a random instance
 * @param[out] sp Shape parameters used while generating object
 * @param[out] tp Texture parameters used while generating object
 * @param[out] instance   Randomly generated instance
 */
template<typename T>
void MorphableModel<T>::Generate(cv::Mat* sp,
                                 cv::Mat* tp,
                                 Mesh* instance) const {
  // Generate shape
  if (shape_) {
    shape_->Generate(sp, instance);
  }
  // Generate texture
  if (texture_) {
    texture_->Generate(tp, instance);
  }
}

/*
 * @name  AddIllumination
 * @fn    virtual void AddIllumination(const Matrix4x4<T>& normal_matrix,
                                       const cv::Mat& ip,
                                       Mesh* instance) const
 * @brief Add illumination contribution to the generated texture using
 *        spherical harmonics approximation. Should be implemented by derived
 *        model
 * @param[in] normal_matrix   Transformation matrix to go from model space into
 *                            camera space: Transform = (Model^-1)^T
 * @param[in] ip  Illumination parameters
 * @param[in,out] instance    Mesh instance where to add illumination
 */
template<typename T>
void MorphableModel<T>::AddIllumination(const Matrix4x4<T>& normal_matrix,
                                        const cv::Mat& ip,
                                        Mesh* instance) const {
  LTS5_LOG_ERROR("Fail Safe, should be implemented by derived class");
}

#pragma mark -
#pragma mark Private

/*
 * @name  InitLandmarksVariable
 * @fn    void InitLandmarksVariable()
 * @brief Initialize variable related to landmarks
 */
template<typename T>
void MorphableModel<T>::InitLandmarksVariable() {
  const auto& idx = this->get_landmark_index();
  const int n_idx = static_cast<int>(idx.size());
  const cv::Mat& mean = this->get_mean_shape();
  mean_landmarks_.create(3 * n_idx, 1, cv::DataType<T>::type);
  normalized_mean_landmarks_.create(3 * n_idx, 1, cv::DataType<T>::type);

  T minx = std::numeric_limits<T>::max();
  T maxx = -minx;
  T miny = std::numeric_limits<T>::max();
  T maxy = -minx;
  T minz = std::numeric_limits<T>::max();
  T maxz = -minx;
  for (int i = 0; i < n_idx; ++i) {
    const int in = idx[i] * 3;
    const int out = i * 3;
    T x = mean.at<T>(in);
    T y = mean.at<T>(in + 1);
    T z = mean.at<T>(in + 2);
    mean_landmarks_.at<T>(out) = x;
    mean_landmarks_.at<T>(out + 1) = y;
    mean_landmarks_.at<T>(out + 2) = z;

    minx = x < minx ? x : minx;
    maxx = x > maxx ? x : maxx;
    miny = y < miny ? y : miny;
    maxy = y > maxy ? y : maxy;
    minz = z < minz ? z : minz;
    maxz = z > maxz ? z : maxz;
  }
  // Compute normalized mean landmarks
  const T wx = maxx - minx;
  const T wy = maxy - miny;
  const T wz = maxz - minz;
  for (int i = 0; i < n_idx; ++i) {
    const int is = i * 3;
    T x = mean_landmarks_.at<T>(is);
    T y = mean_landmarks_.at<T>(is + 1);
    T z = mean_landmarks_.at<T>(is + 2);
    normalized_mean_landmarks_.at<T>(is) = (x - minx) / wx;
    normalized_mean_landmarks_.at<T>(is + 1) = (y - miny) / wy;
    normalized_mean_landmarks_.at<T>(is + 2) = (z - minz) / wz;
  }
}

/*
 * @name  LoadShapeModel
 * @fn    int LoadShapeModel(std::istream& stream, HeaderObjectType type)
 * @brief Load shape model
 * @param[in] stream  Binary stream from which model need to be loaded
 * @param[in] type    Shape model's type
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int MorphableModel<T>::LoadShapeModel(std::istream& stream,
                                      HeaderObjectType type) {
  int err = -1;
  if (stream.good()) {
    // Init shape model instance
    if (shape_) {
      LTS5_LOG_WARNING("Shape model already assigned, leaking it");
    }
    shape_ = PCAModelFactory<T>::Get().CreateByType(type);
    if (shape_) {
      // Load
      err = shape_->Load(stream);
      n_vertex_ = static_cast<size_t>(shape_->get_n_element() / 3);
      n_tri_ = shape_->get_property(PCAModel<T>::Property::kTriangle);
      // Get mean landmarks
      this->InitLandmarksVariable();
    }
  }
  return err;
}

/*
 * @name  LoadTextureModel
 * @fn    int LoadTextureModel(std::istream& stream, HeaderObjectType type)
 * @brief Load texture model
 * @param[in] stream  Binary stream from which model need to be loaded
 * @param[in] type    Texture model's type
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int MorphableModel<T>::LoadTextureModel(std::istream& stream,
                                        HeaderObjectType type) {
  int err = -1;
  if (stream.good()) {
    // Init texture model instance
    if (texture_) {
      LTS5_LOG_WARNING("Texture model already assigned, leaking it");
    }
    texture_ = PCAModelFactory<T>::Get().CreateByType(type);
    if (texture_) {
      // Load
      err = texture_->Load(stream);
      n_texel_ = texture_->get_property(PCAModel<T>::Property::kTexel);
    }
  }
  return err;
}

#pragma mark -
#pragma mark Proxy

/*
 * @name  MorphableModelProxy
 * @fn    MorphableModelProxy()
 * @brief Constructor
 */
template<typename T>
MorphableModelProxy<T>::MorphableModelProxy() {
  MorphableModelFactory<T>::Get().Register(this);
}

#pragma mark -
#pragma mark Explicit instantiation

/** Float */
template class MorphableModel<float>;
/** Double */
template class MorphableModel<double>;

/** Float */
template class MorphableModelProxy<float>;
/** Double */
template class MorphableModelProxy<double>;

}  // namepsace LTS5
