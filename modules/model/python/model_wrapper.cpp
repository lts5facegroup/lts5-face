/**
 *  @file   model_wrapper.cpp
 *  @brief  Python wrapper for the model module
 *  @ingroup model
 *
 *  @author Christophe Ecabert
 *  @date   4/26/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <memory>
#include <cmath>  // https://stackoverflow.com/questions/28683358 mingw64

#include "pybind11/pybind11.h"

#include "lts5/python/ocv_converter.hpp"

#include "include/pca_model_wrapper.hpp"




namespace py = pybind11;

// Create module
PYBIND11_MODULE(pymodel, m) {


  // Init
  LTS5::Python::InitNumpyArray();

  // Doc
  m.doc() = R"doc(LTS5 Model modules)doc";

  // Expose class
  LTS5::Python::AddPCAModel(m);
  LTS5::Python::AddColorModel(m);
  LTS5::Python::AddShapeModel(m);
  LTS5::Python::AddMultiShapeModel(m);
  LTS5::Python::AddMorphableModel(m);
}