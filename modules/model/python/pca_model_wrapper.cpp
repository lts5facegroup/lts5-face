/**
 *  @file   pca_model_wrapper.cpp
 *  @brief  Python wrapper for PCAModel class
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   4/26/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#include <cmath>   // make mingw happy, otherwise get error "error: '::hypot' has not been declared"
#include "pybind11/stl.h"

#include "pca_model_wrapper.hpp"
#include "math_converter.hpp"

#include "lts5/python/ocv_converter.hpp"

#include "lts5/model/pca_model.hpp"
#include "lts5/model/color_model.hpp"
#include "lts5/model/shape_model.hpp"
#include "lts5/model/multi_shape_model.hpp"
#include "lts5/model/morphable_model.hpp"
#include "lts5/model/color_morphable_model.hpp"

namespace py = pybind11;
using namespace pybind11::literals; // to bring in the `_a` literal

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/*
 * @name    ExposePCAModel
 * @fn      void ExposePCAModel(py::module& m, const std::string& name)
 * @brief Expose the PCAModel class to a python module
 * @param[in,out] m Python module
 * @param[in] name  Class name
 * @tparam T    Data type
 */
template<typename T>
void ExposePCAModel(py::module& m, const std::string& name) {
  using Model = PCAModel<T>;
  py::class_<Model>(m,
                    name.c_str(),
                    R"doc(Statistical Model based on PCA)doc")
          .def_property_readonly("mean",
                                 &Model::get_mean,
                                 R"doc(Access mean value)doc")
          .def_property_readonly("variation",
                                 &Model::get_variation,
                                 R"doc(Access variation value (i.e. eigenvectors)doc")
          .def_property_readonly("prior",
                                 &Model::get_prior,
                                 R"doc(Access prior value (i.e. eigenvalues)doc");
}


/*
 * @name    ExposeColorModel
 * @fn      void ExposeColorModel(py::module& m, const std::string& name)
 * @brief Expose the ColorModel class to a python module
 * @param[in,out] m Python module
 * @param[in] name  Class name
 * @tparam T    Data type
 */
template<typename T>
void ExposeColorModel(py::module& m, const std::string& name) {
  using Base = PCAModel<T>;
  using CModel = ColorModel<T>;

  // Overloaded function
  using load_fn = int(CModel::*)(const std::string&);
  using save_fn = int(CModel::*)(const std::string&) const;

  // Expose class, with inheritance
  py::class_<CModel, Base>(m,
                           name.c_str(),
                           R"doc(Per-vertex statistical color model)doc")
  // Constructor
          .def(py::init<>(),
               R"doc(Create an empty model)doc")
          .def(py::init<const cv::Mat&, const cv::Mat&, const cv::Mat&, const int&>(),
               py::arg("mean"),
               py::arg("variation"),
               py::arg("prior"),
               py::arg("n_channels"),
               R"doc(Specialized constructor:
> mean: Average value
> variation: Eigenvectors stored in columns
> prior: Eigenvalues
> n_channels: Number of channels in the flatten mean)doc")
          .def("load",
               (load_fn)&CModel::Load,
               py::arg("filename"),
               R"doc(Load from a given file
> filename: Path to the model file)doc")
          .def("save",
               (save_fn)&CModel::Save,
               py::arg("filename"),
               R"doc(Save to a file
> filename: Path to the model file)doc")
// Methods
          .def("generate",
               [](const CModel& model, const cv::Mat& p, const bool& scaled) {
                  int size = model.get_n_element();
                  cv::Mat sample(size, 1, cv::DataType<T>::type);
                  model.Generate(p, scaled, (T*)sample.data);
                  return sample;
               },
               "param"_a,
               "scaled"_a = true,
               R"doc(Generate an instance given a set of coefficients.

> parma: Set of parameters
> scaled: True indicate that coefficient are relative to eigenvalues)doc")
// Properties
          .def_property_readonly("n_texel",
                                 &CModel::get_n_texel,
                                 R"doc(Provide number of texel in the color model)doc")
          .def("__repr__",
               [](const CModel& model) {
                  auto& mean = model.get_mean();
                  auto& var = model.get_variation();
                  auto& p = model.get_prior();
                 std::ostringstream s;
                 s << "Color model properties: ";
                 s << " Mean: [" << mean.rows << "x" << mean.cols << "]";
                 s << " Variation: [" << var.rows << "x" << var.cols << "]";
                 s << " Prior: [" << p.rows << "x" << p.cols << "]";
                 s << " #PCs: " << model.get_n_principle_component();
                 s << " #Channels: " << model.get_n_channels();
                 s << " #Elements: " << model.get_n_element();
                 s << " #Texels: " << model.get_n_texel();
                 return s.str();
               },
          R"doc(Print color model summary)doc");
}

/*
 * @name    ExposeShapeModel
 * @fn      void ExposeShapeModel(py::module& m, const std::string& name)
 * @brief Expose the ShapeModel class to a python module
 * @param[in,out] m Python module
 * @param[in] name  Class name
 * @tparam T    Data type
 */
template<typename T>
void ExposeShapeModel(py::module& m, const std::string& name) {
  using Base = PCAModel<T>;
  using SModel = ShapeModel<T>;
  using Mesh = typename SModel::Mesh;

  // Overloaded function
  using load_fn = int (SModel::*)(const std::string &);
  using save_fn = int (SModel::*)(const std::string &) const;

  // Expose class, with inheritance
  py::class_<SModel, Base>(m,
                           name.c_str(),
                           R"doc(Statistical shape model)doc")
          // Constructor
          .def(py::init<>(),
               R"doc(Create an empty model)doc")
          .def(py::init([](const cv::Mat& mean,
                           const cv::Mat& variation,
                           const cv::Mat& prior,
                           const int& n_channel,
                           const cv::Mat& triangle,
                           const cv::Mat& landmarks,
                           const cv::Mat& cog) {
                 using Tri = typename SModel::Triangle;

                 if (triangle.cols != 3 &&
                     triangle.type() != cv::DataType<int>::type) {
                   throw std::runtime_error("Triangle array must be of type 'int32' and cols == 3");
                 }
                 if (landmarks.type() != cv::DataType<int>::type) {
                   throw std::runtime_error("Landmarks array must be of type int32");
                 }

                 auto* tri_ptr = reinterpret_cast<Tri*>(triangle.data);
                 auto* lms_ptr = reinterpret_cast<int*>(landmarks.data);
                 int n_lms = std::max(landmarks.cols, landmarks.rows);
                 auto* cog_ptr = reinterpret_cast<T*>(cog.data);
                 int n_cog = cog.rows * cog.cols;
                 std::vector<Tri> tris(tri_ptr, tri_ptr + triangle.rows);
                 std::vector<int> lms(lms_ptr, lms_ptr + n_lms);
                 std::vector<T> c(cog_ptr, cog_ptr + n_cog);
                 // Convert
                 return std::unique_ptr<SModel>(new SModel(mean,
                                                           variation,
                                                           prior,
                                                           n_channel,
                                                           tris,
                                                           lms,
                                                           c));
               }),
               py::arg("mean"),
               py::arg("variation"),
               py::arg("prior"),
               py::arg("n_channels"),
               py::arg("triangles"),
               py::arg("landmarks"),
               py::arg("cog"),
               R"doc(Specialized constructor:
> mean: Average value
> variation: Eigenvectors stored in columns
> prior: Eigenvalues
> n_channels: Number of channels in the flatten mean
> triangles: Triangulation stored as an array [Nt x 3]
> landmarks: Landmark's indexes stored as an array [Nlms x 1]
> cog: Center of gravity (array))doc")
          .def("load",
               (load_fn)&SModel::Load,
               py::arg("filename"),
               R"doc(Load from a given file
> filename: Path to the model file)doc")
          .def("save",
               (save_fn)&SModel::Save,
               py::arg("filename"),
               R"doc(Save to a file
> filename: Path to the model file)doc")
          .def_property_readonly("n_vertex",
                                 &SModel::get_n_vertex,
                                 R"doc(Provide number of texel in the color model)doc")
          .def("generate",
               [](const SModel& model, const cv::Mat& p, const bool& scaled) {
                 Mesh m;
                 model.Generate(p, scaled, &m);
                 return m;
               },
               py::arg("param"),
               py::arg("scaled") = true,
               R"doc(Generate a shape instance from coefficients
> param: Shape parameters
> scaled: If `True` indicates parameters are relative to eigenvalues)doc")
          .def("__repr__",
               [](const SModel& model) {
                 auto& mean = model.get_mean();
                 auto& var = model.get_variation();
                 auto& p = model.get_prior();
                 std::ostringstream s;
                 s << "Shape model properties: ";
                 s << " Mean: [" << mean.rows << "x" << mean.cols << "]";
                 s << " Variation: [" << var.rows << "x" << var.cols << "]";
                 s << " Prior: [" << p.rows << "x" << p.cols << "]";
                 s << " #PCs: " << model.get_n_principle_component();
                 s << " #Channels: " << model.get_n_channels();
                 s << " #Elements: " << model.get_n_element();
                 s << " #Vertex: " << model.get_n_vertex();
                 s << " #Triangles: " << model.get_n_triangle();
                 s << " #Landmarks: " << model.get_landmark_index().size();
                 return s.str();
               },
               R"doc(Print shape model summary)doc");
}

/*
 * @name    ExposeMultiShapeModel
 * @fn      void ExposeMultiShapeModel(py::module& m, const std::string& name)
 * @brief Expose the MultiShapeModel class to a python module
 * @param[in,out] m Python module
 * @param[in] name  Class name
 * @tparam T    Data type
 */
template<typename T>
void ExposeMultiShapeModel(py::module& m, const std::string& name) {
  using SModel = ShapeModel<T>;
  using MSModel = MultiShapeModel<T>;

  using save_fn = int(MSModel::*)(const std::string&) const;


  // Expose class, with inheritance
  py::class_<MSModel, SModel>(m,
                              name.c_str(),
                              R"doc(Statistical multi-shape model)doc")
          // Constructor
          .def(py::init<>(),
               R"doc(Create an empty model)doc")
          .def(py::init([](const cv::Mat& mean,
                           const cv::Mat& variation,
                           const cv::Mat& prior,
                           const int& n_channel,
                           const cv::Mat& triangle,
                           const cv::Mat& landmarks,
                           const cv::Mat& cog,
                           const cv::Mat& dims) {
                 using Tri = typename MSModel::Triangle;

                 if (triangle.cols != 3 &&
                     triangle.type() != cv::DataType<int>::type) {
                   throw std::runtime_error("Triangle array must be of type 'int32' and cols == 3");
                 }
                 if (landmarks.type() != cv::DataType<int>::type) {
                   throw std::runtime_error("Landmarks array must be of type int32");
                 }
                 if (dims.type() != cv::DataType<int>::type) {
                   throw std::runtime_error("Dims array must be of type int32");
                 }

                 auto* tri_ptr = reinterpret_cast<Tri*>(triangle.data);
                 auto* lms_ptr = reinterpret_cast<int*>(landmarks.data);
                 int n_lms = landmarks.cols * landmarks.rows;
                 auto* cog_ptr = reinterpret_cast<T*>(cog.data);
                 int n_cog = cog.rows * cog.cols;
                 auto* dim_ptr = reinterpret_cast<int*>(dims.data);
                 int n_dims = dims.rows * dims.cols;
                 std::vector<Tri> tris(tri_ptr, tri_ptr + triangle.rows);
                 std::vector<int> lms(lms_ptr, lms_ptr + n_lms);
                 std::vector<T> c(cog_ptr, cog_ptr + n_cog);
                 std::vector<int> d(dim_ptr, dim_ptr + n_dims);
                 // Convert
                 return std::unique_ptr<MSModel>(new MSModel(mean,
                                                             variation,
                                                             prior,
                                                             n_channel,
                                                             tris,
                                                             lms,
                                                             c,
                                                             d));
               }),
               py::arg("mean"),
               py::arg("variation"),
               py::arg("prior"),
               py::arg("n_channels"),
               py::arg("triangles"),
               py::arg("landmarks"),
               py::arg("cog"),
               py::arg("dims"),
               R"doc(Specialized constructor:
> mean: Average value
> variation: Eigenvectors stored in columns
> prior: Eigenvalues
> n_channels: Number of channels in the flatten mean
> triangles: Triangulation stored as an array [Nt x 3]
> landmarks: Landmark's indexes stored as an array [Nlms x 1]
> cog: Center of gravity (array)
> dims: Shapes dimensions (array))doc")
          .def("load",
               &MSModel::Load,
               py::arg("filename"),
               R"doc(Load from a given file
> filename: Path to the model file)doc")
          .def("save",
               (save_fn)&MSModel::Save,
               py::arg("filename"),
               R"doc(Save to a file
> filename: Path to the model file)doc")
          .def("__repr__",
               [](const MSModel& model) {
                 auto& mean = model.get_mean();
                 auto& var = model.get_variation();
                 auto& p = model.get_prior();
                 std::ostringstream s;
                 s << "Multi-shape model properties: ";
                 s << " Mean: [" << mean.rows << "x" << mean.cols << "]";
                 s << " Variation: [" << var.rows << "x" << var.cols << "]";
                 s << " Prior: [" << p.rows << "x" << p.cols << "]";
                 s << " #PCs: " << model.get_n_principle_component();
                 s << " #Channels: " << model.get_n_channels();
                 s << " #Elements: " << model.get_n_element();
                 s << " #Vertex: " << model.get_n_vertex();
                 s << " #Triangles: " << model.get_n_triangle();
                 s << " #Landmarks: " << model.get_landmark_index().size();
                 s << " #Dims: " << model.get_dims().size();
                 return s.str();
               },
               R"doc(Print shape model summary)doc")
          .def_property_readonly("dims",
                                 &MSModel::get_dims,
                                 R"doc(Return list of each submodel coefficient's dimensions)doc")
          .def_property_readonly("tri",
                                 &MSModel::get_triangles,
                                 R"doc(Provide surface triangulation)doc")
          ;







}

template<typename T>
void ExposeMultiShapeSegmentModel(py::module& m, const std::string& name) {
  using MSModel = MultiShapeModel<T>;
  using MSSegModel = MultiShapeSegmentModel<T>;

  using save_fn = int(MSSegModel::*)(const std::string&) const;
  using load_fn = int(MSSegModel::*)(const std::string&);

  // Expose class, with inheritance
  py::class_<MSSegModel, MSModel>(m,
                                  name.c_str(),
                                  R"doc(Statistical multi-shape segment
 model)doc")
          // Constructor
          .def(py::init<>(),
               R"doc(Create an empty model)doc")
          .def(py::init([](const cv::Mat& mean,
                           const cv::Mat& variation,
                           const cv::Mat& prior,
                           const int& n_channel,
                           const cv::Mat& mixing,
                           const int& n_segment,
                           const cv::Mat& triangle,
                           const cv::Mat& landmarks,
                           const cv::Mat& cog,
                           const cv::Mat& dims) {
            using Tri = typename MSSegModel::Triangle;

            if (triangle.cols != 3 &&
                triangle.type() != cv::DataType<int>::type) {
              throw std::runtime_error("Triangle array must be of type 'int32' and cols == 3");
            }
            if (landmarks.type() != cv::DataType<int>::type) {
              throw std::runtime_error("Landmarks array must be of type int32");
            }
            if (dims.type() != cv::DataType<int>::type) {
              throw std::runtime_error("Dims array must be of type int32");
            }

            auto* tri_ptr = reinterpret_cast<Tri*>(triangle.data);
            auto* lms_ptr = reinterpret_cast<int*>(landmarks.data);
            int n_lms = landmarks.cols * landmarks.rows;
            auto* cog_ptr = reinterpret_cast<T*>(cog.data);
            int n_cog = cog.rows * cog.cols;
            auto* dim_ptr = reinterpret_cast<int*>(dims.data);
            int n_dims = dims.rows * dims.cols;
            std::vector<Tri> tris(tri_ptr, tri_ptr + triangle.rows);
            std::vector<int> lms(lms_ptr, lms_ptr + n_lms);
            std::vector<T> c(cog_ptr, cog_ptr + n_cog);
            std::vector<int> d(dim_ptr, dim_ptr + n_dims);
            // Convert
            return std::unique_ptr<MSSegModel>(new MSSegModel(mean,
                                                              variation,
                                                              prior,
                                                              n_channel,
                                                              mixing,
                                                              n_segment,
                                                              tris,
                                                              lms,
                                                              c,
                                                              d));
          }),
               "mean"_a,
               "variation"_a,
               "prior"_a,
               "n_channels"_a,
               "mixing"_a,
               "n_segment"_a,
               "triangles"_a,
               "landmarks"_a,
               "cog"_a,
               "dims"_a,
               R"doc(Specialized constructor:
> mean: Average value
> variation: Eigenvectors stored in columns
> prior: Eigenvalues
> n_channels: Number of channels in the flatten mean
> mixing: Mixing matrix for each segment. Must have the same number of rows as
 the number of segement defined.
> n_segment: Number of segment defined
> triangles: Triangulation stored as an array [Nt x 3]
> landmarks: Landmark's indexes stored as an array [Nlms x 1]
> cog: Center of gravity (array)
> dims: Shapes dimensions (array))doc")
          .def("load",
               (load_fn)&MSSegModel::Load,
               "filename"_a,
               R"doc(Load from a given file
> filename: Path to the model file)doc")
          .def("save",
               (save_fn)&MSSegModel::Save,
               "filename"_a,
               R"doc(Save to a file
> filename: Path to the model file)doc")
          .def("__repr__",
               [](const MSSegModel& model) {
                 auto& mean = model.get_mean();
                 auto& var = model.get_variation();
                 auto& p = model.get_prior();
                 auto& mix = model.get_mixing();
                 std::ostringstream s;
                 s << "Multi-shape model properties: ";
                 s << " Mean: [" << mean.rows << "x" << mean.cols << "]";
                 s << " Variation: [" << var.rows << "x" << var.cols << "]";
                 s << " Prior: [" << p.rows << "x" << p.cols << "]";
                 s << " #PCs: " << model.get_n_principle_component();
                 s << " #Channels: " << model.get_n_channels();
                 s << " #Elements: " << model.get_n_element();
                 s << " #Vertex: " << model.get_n_vertex();
                 s << " #Triangles: " << model.get_n_triangle();
                 s << " #Landmarks: " << model.get_landmark_index().size();
                 s << " #Dims: " << model.get_dims().size();
                 s << " #Segment: " << model.get_n_segment();
                 s << " Mixing: [" << mix.rows << "x" << mix.cols << "]";
                 return s.str();
               },
               R"doc(Print shape model summary)doc")
          .def_property_readonly("dims",
                                 &MSSegModel::get_dims,
                                 R"doc(Return list of each submodel coefficient'
s dimensions)doc")
          .def_property_readonly("n_segment",
                                 &MSSegModel::get_n_segment,
                                 R"doc(Indicate the number of segment in
 the model)doc")
          .def_property_readonly("mixing",
                                 &MSSegModel::get_mixing,
                                 R"doc(Mixing matrix for blending each
 segment together)doc")
               ;
}

/*
 * @name    ExposeMorphableModel
 * @fn      void ExposeMorphableModel(py::module& m, const std::string& name)
 * @brief Expose the MorphableModel class to a python module
 * @param[in,out] m Python module
 * @param[in] name  Class name
 * @tparam T    Data type
 */
template<typename T>
void ExposeMorphableModel(py::module& m, const std::string& name) {
  using Model = MorphableModel<T>;
  py::class_<Model>(m,
                    name.c_str(),
                    R"doc(Morphable Model base class)doc")
          .def_property_readonly("shape",
                                 &Model::get_shape_model,
                                 R"doc(Shape model)doc")
          .def_property_readonly("color",
                                 &Model::get_texture_model,
                                 R"doc(Texture model)doc");
}

/*
 * @name    ExposeColorMorphableModel
 * @fn      void ExposeColorMorphableModel(py::module& m, const std::string& name)
 * @brief Expose the ColorMorphableModel class to a python module
 * @param[in,out] m Python module
 * @param[in] name  Class name
 * @tparam T    Data type
 */
template<typename T>
void ExposeColorMorphableModel(py::module& m, const std::string& name) {
  using Base = MorphableModel<T>;
  using Model = ColorMorphableModel<T>;

  // Overloaded function
  using load_fn = int (Model::*)(const std::string &);
  using save_fn = int (Model::*)(const std::string &) const;

  py::class_<Model, Base>(m,
                          name.c_str(),
                          R"doc(Per-vertex Color Morphable Model)doc")
          .def(py::init(),
               R"doc(Create an empty model)doc")
          .def(py::init([](PCAModel<T>& shp, ColorModel<T>& color) {
                 return std::unique_ptr<Model>(new Model(&shp, &color));
               }),
               py::arg("shape"),
               py::arg("color"),
               R"doc(Specialized constructor:
> shape: Statistical shape model
> color: Per-vertex statistical color model)doc")
          .def("load",
               (load_fn)&Model::Load,
               py::arg("filename"),
               R"doc(Load from a given file
> filename: Path to the model file)doc")
          .def("save",
               (save_fn)&Model::Save,
               py::arg("filename"),
               R"doc(Save to a file
> filename: Path to the model file)doc");
}

/*
 * @name    AddPCAModel
 * @fn  void AddPCAModel(pybind11::module& m)
 * @brief Expose PCAModel base class to python
 * @param[in,out] m Python module where to add the PCAModel
 */
void AddPCAModel(py::module& m) {
  ExposePCAModel<float>(m, "PCAModelFloat");
  ExposePCAModel<double>(m, "PCAModelDouble");
}

/*
 * @name    AddColorModel
 * @fn  void AddColorModel(pybind11::module& m)
 * @brief Expose AddColorModel to python
 * @param[in,out] m Python module where to add the AddColorModel
 */
void AddColorModel(py::module& m) {
  ExposeColorModel<float>(m, "ColorModelFloat");
  ExposeColorModel<double>(m, "ColorModelDouble");
}

/*
 * @name    AddShapeModel
 * @fn  void AddShapeModel(pybind11::module& m)
 * @brief Expose ShapeModel to python
 * @param[in,out] m Python module where to add the ShapeModel
 */
void AddShapeModel(py::module& m) {
  ExposeShapeModel<float>(m, "ShapeModelFloat");
  ExposeShapeModel<double>(m, "ShapeModelDouble");
}

/*
 * @name    AddMultiShapeModel
 * @fn  void AddMultiShapeModel(pybind11::module& m)
 * @brief Expose MultiShapeModel to python
 * @param[in,out] m Python module where to add the MultiShapeModel
 */
void AddMultiShapeModel(pybind11::module& m) {
  ExposeMultiShapeModel<float>(m, "MultiShapeModelFloat");
  ExposeMultiShapeSegmentModel<float>(m, "MultiShapeSegmentModelFloat");
  ExposeMultiShapeModel<double>(m, "MultiShapeModelDouble");
  ExposeMultiShapeSegmentModel<double>(m, "MultiShapeSegmentModelDouble");
}

/*
 * @name    AddMorphableModel
 * @fn  void AddMorphableModel(pybind11::module& m)
 * @brief Expose MorphableModel base class to python
 * @param[in,out] m Python module where to add the PCAModel
 */
void AddMorphableModel(pybind11::module& m) {
  ExposeMorphableModel<float>(m, "MorphableModelFloat");
  ExposeMorphableModel<double>(m, "MorphableModelDouble");
  ExposeColorMorphableModel<float>(m, "ColorMorphableModelFloat");
  ExposeColorMorphableModel<double>(m, "ColorMorphableModelDouble");
}


}  // namespace Python
}  // namespace LTS5