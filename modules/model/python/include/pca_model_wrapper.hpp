/**
 *  @file   pca_model_wrapper.hpp
 *  @brief  Python wrapper for PCAModel class
 *  @ingroup utils
 *
 *  @author Christophe Ecabert
 *  @date   4/26/19
 *  Copyright (c) 2019 Christophe Ecabert. All rights reserved.
 */

#ifndef __LTS5_PCA_MODEL_WRAPPER__
#define __LTS5_PCA_MODEL_WRAPPER__

#include "pybind11/pybind11.h"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @namespace  Python
 *  @brief      Python wrapper space
 */
namespace Python {

/**
 * @name    AddPCAModel
 * @fn void AddPCAModel(pybind11::module& m)
 * @brief Expose PCAModel to python (Base class)
 * @param[in,out] m Python module where to add the PCAModel
 * @ingroup utils
 */
void AddPCAModel(pybind11::module& m);

/**
 * @name    AddColorModel
 * @fn  void AddColorModel(pybind11::module& m)
 * @brief Expose ColorModel to python
 * @param[in,out] m Python module where to add the ColorModel
 * @ingroup utils
 */
void AddColorModel(pybind11::module& m);

/**
 * @name    AddShapeModel
 * @fn  void AddShapeModel(pybind11::module& m)
 * @brief Expose ShapeModel to python
 * @param[in,out] m Python module where to add the ShapeModel
 * @ingroup utils
 */
void AddShapeModel(pybind11::module& m);

/**
 * @name    AddMultiShapeModel
 * @fn  void AddMultiShapeModel(pybind11::module& m)
 * @brief Expose MultiShapeModel to python
 * @param[in,out] m Python module where to add the MultiShapeModel
 * @ingroup utils
 */
void AddMultiShapeModel(pybind11::module& m);

/**
 * @name    AddMorphableModel
 * @fn  void AddMorphableModel(pybind11::module& m)
 * @brief Expose MorphableModel base class to python
 * @param[in,out] m Python module where to add the PCAModel
 * @ingroup utils
 */
void AddMorphableModel(pybind11::module& m);


}  // namespace Python
}  // namespace LTS5

#endif  // __LTS5_PCA_MODEL_WRAPPER__
