#!/bin/sh

if [ "$#" -lt 1 ]; then
    echo "Wrong number of parameters"
    echo "$0 <deformation_transfer> <FaceWarehouse> <output>"
    echo "deformation_transfer: Folder where deformation transfer binary are stored"
    echo "FaceWarehouse: Folder where the FaceWarehouse dataset is stored"
    echo "output: Folder where to place the generated surfaces"
    exit
fi

DT_EXEC="$1"
FH_FOLDER="$2"
OUTPUT="$3"
TRI_FILE="fw_quad_to_tri.tri"
TRI_RM="bfm_rm_tri.txt"
DENSE_CORRES="dense_fw_to_bfm.cons"
BFM_REF="bfm_ref.obj"

# Run
python ../script/transfer_fh_to_bfm.py --fh_folder $FH_FOLDER --dt_exec $DT_EXEC --tri_file $TRI_FILE --tri_rm $TRI_RM --dense_correspondence $DENSE_CORRES --bfm_mean $BFM_REF --out_folder $OUTPUT