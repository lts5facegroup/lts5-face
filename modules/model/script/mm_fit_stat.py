# coding=utf-8
""" plot morphable model fit evolution """
import argparse
import numpy as np
import matplotlib.pyplot as plt
import lts5.utils as utils
__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
# Define Logger
logger = utils.init_logger()


if __name__ == '__main__':
  # parser
  parser = argparse.ArgumentParser(description='Plot statistics for '
                                               'Morphable Model fitter')
  # Statistics file
  parser.add_argument('-i',
                      type=str,
                      required=True,
                      dest='stat',
                      help='Location where MM fit statistics are stored')
  # Parse
  args = parser.parse_args()

  # Load data
  err, stats = utils.LoadMatFromBin(args.stat)
  if err == 0:
    # Normalize data
    peak_trace = np.max(stats, axis=0)
    stats_n = stats / peak_trace[:, np.newaxis].transpose()
    # Plot
    plt.figure()
    plt.plot(stats_n)
    plt.xlabel('#iteration')
    plt.ylabel('Normalized error')
    plt.title('Cost vs Iteration')

    # Shrink current axis by 20%
    box = plt.gca().get_position()
    plt.gca().set_position([box.x0, box.y0, box.width * 0.8, box.height])

    plt.legend(['Cost', 'ImageTerm', 'ShapeP', 'TexP', 'Landmarks'],
               loc='center left',
               bbox_to_anchor=(1, 0.5))
    plt.show(block=True)

  else:
    logger.error('Can not open file %s', args.stat)
