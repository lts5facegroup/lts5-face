# coding=utf-8
"""
Generate Morphable model from:
  - BFM, Basel Face model 2009
    https://faces.dmi.unibas.ch/bfm/main.php?nav=1-0&id=basel_face_model
  - Exp_pca.bin, expression basis from FaceWarehouse from Guo et al.
    https://github.com/Juyong/3DFace
    (Download and unzip the Coarse_Dataset.zip, 'Exp_Pca.bin' is in there.

"""
from os.path import join, exists
from os import makedirs as mkdir
from argparse import ArgumentParser
from scipy.io import loadmat
from struct import unpack
import numpy as np
from lts5.utils import init_logger
from lts5.geometry import MeshFloat as Mesh
from lts5.model import ColorModelFloat as ColorModel
from lts5.model import MultiShapeModelFloat as MultiShapeModel
from lts5.model import MultiShapeSegmentModelFloat as MultiShapeSegmentModel
from lts5.model import ColorMorphableModelFloat as ColorMorphableModel

__author__ = 'Christophe Ecabert'

# Init logger
logger = init_logger()


def load_bfm(folder):
  """
  Open BGFM09 model from a given folder
  :param folder:  Folder where '01_MorphableModel.mat' is located
  :return: dict
  """
  logger.info('Search for Basel Face Model in: {}'.format(folder))
  model_name = join(folder, '01_MorphableModel.mat')
  try:
    # Load bfm model
    bfm = loadmat(model_name)
    # Load symmetry index
    sym_idx = np.loadtxt(join(folder,
                              '13_symmetry_indices',
                              'face05_symlist.txt'),
                         delimiter=' ')

    return bfm, sym_idx
  except IOError:
    logger.error('Unable to load `{}`'.format(model_name))
    return None, None


def load_expression(folder):
  """
  Load expression model from a givem folder
  :param folder:  Location where `Exp_pca.bin` is located
  :return:  eigen_value, eigen_vector
  """
  logger.info('Search for Expression Model in: {}'.format(folder))
  with open(join(folder, 'Coarse_Dataset', 'Exp_Pca.bin'), 'rb') as f:
    # File format
    # 1x int: dimension
    # Nx float: mean expression
    # Kx float: basis expression
    # bytes = f.read()
    n_vertex = 53215
    n_basis = unpack('@i', f.read(4))[0]
    exp_mu = np.frombuffer(f.read(12 * n_vertex), dtype=np.float32)
    exp_pc = np.frombuffer(f.read(12 * n_vertex * n_basis), dtype=np.float32)
    exp_pc = exp_pc.reshape(n_basis, -1).T
    exp_ev = np.loadtxt(join(folder, 'std_exp.txt')).astype(np.float32)
  # Return: Principal component, Eigen values
  return exp_pc, exp_ev


def load_other_infos(folder,
                     triangle,
                     full_model=False):
  """
  Load vertex selection for expression and identity since both surface do not
  have the same size (i.e. 53215 != 53490
  :param folder:  Location where selection is stored (`BFM_front_idx.mat` &
                  `BFM_exp_idx.mat`)
  :param triangle: BFM triangulation
  :param full_model:  If `True` load full model (i.e. with ears & neck)
  :return:  List of index for expression,
            list of index for identity (0 indexed)
            triangle
            landmarks index
  """
  # other info contains triangles, region used for computing photometric loss,
  # region used for skin texture regularization, and 68 landmarks index etc.
  other_info = loadmat(join(folder, 'facemodel_info.mat'))
  tri = other_info['tri'].astype(np.int32) - 1
  lms_idx = other_info['keypoints'].astype(np.int32) - 1

  idx_exp = loadmat(join(folder, 'BFM_front_idx.mat'))
  idx_exp = idx_exp['idx'].astype(np.int32) - 1  # starts from 0 (to 53215)
  idx_exp = np.squeeze(idx_exp, -1)

  if full_model:
    # Need to convert landmarks (indexed from cropped region back to full model
    # indexes
    lms_idx = idx_exp[lms_idx[0, :]].reshape(1, -1)
    # Override index for expression basis to select all of them
    idx_exp = np.arange(0, 53215)

  idx_shape = loadmat(join(folder, 'BFM_exp_idx.mat'))
  idx_shape = idx_shape['trimIndex'].astype(np.int32) - 1  # from 0 (to 53490)
  idx_shape = np.squeeze(idx_shape[idx_exp], -1)

  if full_model:
    # Triangulation must be overwrite since `other_info['tri']` is only valid
    # for cropped model
    tmap = {i: k for k, i in enumerate(idx_shape)}
    tris = []
    for tr in triangle:
      converted_tri = []
      for t in tr:
        tn = tmap.get(t, -1)
        if tn >= 0:
          converted_tri.append(tn)
        else:
          break
      if len(converted_tri) == 3:
        tris.append(converted_tri)
    tri = np.asarray(tris)
  # Done
  return idx_exp, idx_shape, tri, lms_idx


def convert_symmetry_index(symmetry, index):
  """
  Convert symmetry index
  :param symmetry:  Array, symmetry index
  :param index:     Selected vertices
  :return:  Converted list
  """
  idx_map = {value: key for key, value in enumerate(index)}
  # Convert list
  sym_idx = []
  for a_old, b_old in symmetry:
    a_new = idx_map.get(a_old, -1)
    b_new = idx_map.get(b_old, -1)
    if a_new != -1 and b_new != -1:
      sym_idx.append([a_new, b_new])
  return np.asarray(sym_idx, dtype=np.int32)


def save_symmetry_index(folder, symmetry, full_model=False, units='mm'):
  """
  Dump new symmetry index into a text file
  :param folder:  Location where to dump the data
  :param symmetry: List of symmetry indices
  :param full_model: Indicate non cropped model
  """
  fname = 'bfm_deng_face05_symlist{}_{}' \
          '.txt'.format('_full' if full_model else '', units)
  with open(join(folder, fname), 'wt') as f:
    for a, b in symmetry:
      f.write('{} {}\n'.format(a, b))


_unit_converter = {'mm': 1e3,
                   'cm': 1e4,
                   'dm': 1e5}

def convert_model(pargs):
  """
  Convert to BFM+Exp models into Morphable model
  :param pargs: Parsed arguemnts
  """

  # Output location
  if not exists(pargs.output):
    mkdir(pargs.output)

  if not pargs.units in ['mm', 'cm', 'dm']:
    raise ValueError('Unknown units, got {} instead of '
                     '{}'.format(pargs.units, ['mm', 'cm', 'dm'] ))

  # Load BFM
  bfm, sym_idx = load_bfm(pargs.bfm_folder)
  if bfm is None:
    return -1

  # BFM - PC / mean rescaling to mm
  scale = _unit_converter[pargs.units]
  id_mu = bfm['shapeMU'] / scale
  id_ev = bfm['shapeEV'] / scale
  id_pc = bfm['shapePC']
  # Convert RGB texture [0, 1]
  color_mu = bfm['texMU'] / 255.0
  color_ev = bfm['texEV'] / 255.0
  color_pc = bfm['texPC']
  # BFM - Triangle
  bfm_tri = bfm['tl'].astype(np.int32) - 1
  tmp = np.copy(bfm_tri[:, 1])
  bfm_tri[:, 1] = bfm_tri[:, 2]
  bfm_tri[:, 2] = tmp
  # Load Expression basis
  exp_pc, exp_ev = load_expression(pargs.exp_folder)

  for m_type in (False, True):

    # Load vertex selection
    idx_exp, idx_shp, tri, lms_idx = load_other_infos(pargs.exp_folder,
                                                      bfm_tri,
                                                      full_model=m_type)

    # Convert symmetry indices
    sym_idx_new = convert_symmetry_index(sym_idx, idx_shp)
    save_symmetry_index(pargs.output, sym_idx_new, m_type, pargs.units)

    # Define reduced basis
    _id_pc = np.reshape(id_pc[:, :80], [-1, 3, 80])
    _id_pc = _id_pc[idx_shp, :, :]
    _id_pc = np.reshape(_id_pc, [-1, 80])
    _id_ev = id_ev[:80, :]

    _color_pc = np.reshape(color_pc[:, :80], [-1, 3, 80])
    _color_pc = _color_pc[idx_shp, :, :]
    _color_pc = np.reshape(_color_pc, [-1, 80])
    _color_ev = color_ev[:80, :]

    _exp_pc = np.reshape(exp_pc[:, :64], [-1, 3, 64])
    _exp_pc = _exp_pc[idx_exp, :, :]
    _exp_pc = np.reshape(_exp_pc, [-1, 64])
    _exp_ev = exp_ev[:64] / scale   # to {mm, cm, dm}

    _id_mu = np.reshape(id_mu, [-1, 3])
    _id_mu = _id_mu[idx_shp, :]
    shp_cog = np.mean(_id_mu, axis=0)
    _id_mu = np.reshape(_id_mu, [-1, 1])

    _color_mu = np.reshape(color_mu, [-1, 3])
    _color_mu = _color_mu[idx_shp, :]
    _color_mu = np.reshape(_color_mu, [-1, 1])

    m = Mesh()
    m.vertex = _id_mu.reshape(-1, 3) - shp_cog
    m.vertex_color = _color_mu
    m.tri = tri
    fname = 'bfm_deng{}_{}.ply'.format('_full' if m_type else '', pargs.units)
    m.save(fname)

    # Shape model
    shape_pc = np.hstack((_id_pc, _exp_pc))
    shape_ev = np.vstack((_id_ev, _exp_ev.reshape(-1, 1)))
    dims = np.asarray([_id_pc.shape[1], _exp_pc.shape[1]], dtype=np.int32)
    shape = MultiShapeModel(mean=_id_mu,
                            variation=shape_pc,
                            prior=shape_ev,
                            n_channels=3,
                            triangles=tri,
                            landmarks=lms_idx,
                            cog=shp_cog,
                            dims=dims)
    # Color model
    tex = ColorModel(mean=_color_mu,
                     variation=_color_pc,
                     prior=_color_ev,
                     n_channels=3)
    # Morphable model
    color_mm = ColorMorphableModel(shape=shape, color=tex)
    fname = 'basel_face_exp{}_{}_deng_paper_model.bin'.format('_full' if m_type
                                                              else '',
                                                              pargs.units)
    fname = join(pargs.output, fname)
    logger.info('Save model into: {}'.format(fname))
    color_mm.save(filename=fname)

    # Segment model
    # -----------------------------------------------------------------------
    n_pts, nb_seg = bfm['segMB'].shape
    nb_seg = nb_seg // n_pts
    segMM = (bfm['segMM'].astype(np.float32)[idx_shp, :])[:, idx_shp]
    col_idx = []
    for k in range(nb_seg):
      col_idx.append(idx_shp + int(k * n_pts))
    col_idx = np.concatenate(col_idx)

    segMB = (bfm['segMB'].astype(np.float32)[idx_shp, :])[:, col_idx]
    mixing = np.zeros((nb_seg, idx_shp.size), dtype=np.float32)
    for k in range(nb_seg):
      mixing[k, :] = segMB.diagonal(k * idx_shp.size)

    shape_seg = MultiShapeSegmentModel(mean=_id_mu,
                                       variation=shape_pc,
                                       prior=shape_ev,
                                       n_channels=3,
                                       mixing=mixing,
                                       n_segment=nb_seg,
                                       triangles=tri,
                                       landmarks=lms_idx,
                                       cog=shp_cog,
                                       dims=dims)

    # Morphable model
    color_mm = ColorMorphableModel(shape=shape_seg, color=tex)
    fname = 'basel_face_exp{}_{}_deng_paper_segment' \
            '_model.bin'.format('_full' if m_type else '', pargs.units)
    fname = join(pargs.output, fname)
    logger.info('Save model into: {}'.format(fname))
    color_mm.save(filename=fname)


if __name__ == '__main__':
  # Parser
  p = ArgumentParser()

  # bfm folder
  p.add_argument('--bfm_folder',
                 required=True,
                 type=str,
                 help='Location where BFM09 model is stored')
  # expression folder
  p.add_argument('--exp_folder',
                 type=str,
                 required=True,
                 help='Location where the expression model is stored (Deng et '
                      'al.)')
  p.add_argument('--units',
                 type=str,
                 required=False,
                 default='mm',
                 help='Type of unit to use with the model, default = `mm`.'
                      ' Possible values are `mm` or `cm` or `dm`')
  # Outputs
  p.add_argument('--output',
                 type=str,
                 required=True,
                 help='Location where to place the generated model')


  args = p.parse_args()
  e_code = convert_model(pargs=args)
  exit(e_code)
