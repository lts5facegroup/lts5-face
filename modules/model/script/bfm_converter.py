# coding=utf-8
""" Convert MATLAB BFM model to binary file """
import os
import io
import argparse
import struct
import scipy.io
import numpy as np
import matplotlib.pyplot as plt
from lts5.geometry import MeshFloat as Mesh
import lts5.utils as utils
from lts5.model import ColorModelFloat as ColorModel
from lts5.model import ShapeModelFloat as ShapeModel
from lts5.model import MultiShapeModelFloat as MultiShapeModel
from lts5.model import ColorMorphableModelFloat as ColorMorphableModel
__author__ = 'Christophe Ecabert'

# Sanity check
utils.check_py_version(3, 0)
# Init logger
logger = utils.init_logger()

# ----------------------------------------------------------------------------
# Define data type conversion from numpy.dtype
data_type_from_numpy = {np.uint8: 0,
                        np.int8: 1,
                        np.uint16: 2,
                        np.int16: 3,
                        np.int32: 4,
                        np.float32: 5,
                        np.float64: 6}

# ----------------------------------------------------------------------------
# LANDMARKS ANNOTATIONS
# idx = [21622, 22162, 22050, 21819, 43295, 45277, 46664, 47604, 48124, 48709,
#        49646, 51186, 53420, 33165, 33011, 32996, 33363, 39186, 39702, 40057,
#        40290, 40484, 41168, 41336, 41544, 41868, 42389, 8286, 8302, 8314,
#        8320, 6268, 7300, 8332, 9106, 10267, 1959, 3888, 4920, 6217, 4803,
#        3900, 10472, 11628, 12789, 14472, 12801, 11769, 5521, 6024, 7183,
#        8344, 9376, 10668, 11067, 9793, 9019, 8374, 7729, 6697, 6037, 7191,
#        8352, 9255, 10808, 9013, 8368, 7594]
#
# idx = [47076, 47599, 48119, 48704, 49159, 39186, 39702, 40057, 40290,
#        40484, 41168, 41336, 41544, 41868, 42389, 8286, 8302, 8314, 8320,
#        6268, 7300, 8332, 9106, 10267, 1959, 3888, 4920, 6217, 4803,
#        3900, 10472, 11628, 12789, 14472, 12801, 11769, 5521, 6024, 7183,
#        8344, 9376, 10668, 11067, 9793, 9019, 8374, 7729, 6697, 6037, 7191,
#        8352, 9255, 10808, 9013, 8368, 7594]
# idx = [47076, 47599, 48119, 48704, 49159, 39456, 39702, 40057, 40290, 40484,
#        41117, 41280, 41483, 41735, 42044, 8286, 8302, 8314, 8320,
#        6268, 7300, 8332, 9106, 10267, 1959, 3888, 4920, 6217, 4803, 3900,
#        10472, 11628, 12789, 14472, 12801, 11769, 5521, 6024, 7183, 8344,
#        9376, 10668, 11067, 9793, 9019, 8374, 7729, 6697, 6037, 7193, 8354,
#        9257, 10808, 9012, 8367, 7722]


idx = [21622, 22162, 22050, 21819, 43295, 45277, 47076, 47599, 48119, 48704,
       49159, 51186, 53420, 33165, 33011, 32996, 33363, 39456, 39702, 40057,
       40290, 40484, 41117, 41280, 41483, 41735, 42044, 8286, 8302, 8314,
       8320, 6268, 7300, 8332, 9106, 10267, 1959, 3888, 4920, 6217, 4803,
       3900, 10472, 11628, 12789, 14472, 12801, 11769, 5521, 6024, 7183,
       8344, 9376, 10668, 11067, 9793, 9019, 8374, 7729, 6697, 6037, 7193,
       8354, 9257, 10808, 9012, 8367, 7722]


inner_idx = [42389, 41868, 41544, 41336, 41168, 40484, 40290, 40057, 39702,
             39186, 8286, 8302, 8314, 8320, 10267, 9106, 8332, 7300, 6268,
             14472, 12789, 11628, 10472, 11769, 12801, 6217, 4920, 3888,
             1959, 3900, 4803, 11067, 10668, 9376, 8344, 7183, 6024, 5521,
             6697, 7729, 8374, 9019, 9793, 10300, 9256, 8353, 7192, 6170,
             7592, 8366, 9011]
lms = np.asarray(idx, dtype=np.int32).reshape(-1, 1)


def save_matrices_to_bin(filename, matrices):
  """
  Save a given numpy arrays into a binary file
  :param filename:    File's name or file object
  :param matrices:    List of data to save onto hard drive
  :return:
  """
  assert isinstance(matrices, list)
  if isinstance(filename, str):
    f = open(filename, 'wb')
  elif isinstance(filename, io.IOBase):
    f = filename
  else:
    print('Unsupported file type')
    raise ValueError('Unsupported filename (str, file descriptor')
  for mat in matrices:
    t = data_type_from_numpy.get(mat.dtype.type)
    if t is not None:
      # Write header
      t = struct.pack('@i', t)
      row = struct.pack('@i', mat.shape[0])
      col = struct.pack('@i', mat.shape[1])
      f.write(t)
      f.write(row)
      f.write(col)
      # Write data
      f.write(mat.tobytes())
    else:
      raise TypeError('Unsupported matrix data type')


def parse_index_file(filename):
  """
  Parse a given text file and extract the indices.
  :param filename: Path to text file
  :return:  List of indices
  """
  local_idx = []
  with open(filename, 'r') as f:
    for line in f:
      line = line.strip()
      if line[0] != '#':
        local_idx.append(int(line))
  return local_idx


# ----------------------------------------------------------------------------
def search_folder(folder, ext, pattern=None):
  """
  Scan recursively a given folder for files with a specific extension
  :param folder:  Folder to scan
  :param ext:     Extension to look for
  :param pattern:  Pattern required to be present in filename
  :return:        Matching files
  """
  select = []
  for root, _, files in os.walk(folder):
    for f in files:
      if f.endswith(ext):
        if pattern is None:
          select.append(os.path.join(root, f))
        else:
          name = os.path.join(root, f)
          if pattern in name:
            select.append(name)
  select.sort()
  return select


class VarMaskGenerator:
  """ Compute per-vertex aldebo variance from snythetic data """

  def __init__(self, mean, basis, p_var):
    """
    Constructor

    :param mean:  Mean value of the underlying statistical model
    :param basis: Basis embedding the variance (scaled eigenvectors)
    :param p_var: Parameter's variance
    """
    self._mean = mean
    self._basis = basis
    self._p_var = p_var
    self._vertex_std = None

  def generate(self, n_sample):
    """
    Compute per-vertex standard deviation.
    :param n_sample:  Number of samples to generate for the estimation
    """
    # Generate samples at +/- 5 std for each basis
    n_var = self._basis.shape[1]
    n_model = 2 * n_var
    # Generate random parameters
    p = np.zeros((n_var, n_sample + n_model))
    np.fill_diagonal(p, 5.0)
    np.fill_diagonal(p[:, n_var:], -5.0)
    p[:, n_model:] = np.random.normal(0.0, self._p_var, size=(n_var, n_sample))
    # generate data
    s = self._mean + self._basis @ p
    # Compute per-vertex std
    std = np.std(s, axis=1).reshape((-1, 3))
    self._vertex_std = std.astype(self._basis.dtype)

  def visualize(self, cmap_name='viridis'):
    """
    Convert std into color coded array for visualization purpose
    :param cmap_name: Name of the color map to use
    :return:  Color buffer
    """
    cm = plt.get_cmap(cmap_name)
    std = np.mean(self._vertex_std, axis=1)
    std /= std.max()
    color = cm(std)[:, :3]
    return color.astype(self._vertex_std.dtype)

  def save(self, f):
    """
    Dump the estimated standard deviation into a given IO stream
    :param f: Binary io stream where to dump the data
    """
    if self._vertex_std is not None:
      save_matrices_to_bin(f, [self._vertex_std])

  def get_vertex_std(self):
    return self._vertex_std


def learn_expression_model(folder, n_vertex, retain=0.99):
  """
  Learn expression basis (PCA)
  :param folder:  Folder where mesh are stored
  :param n_vertex: Number of vertex in mesh
  :param retain:  Amount of variance to retain
  :return:  Eigenvectors, eigenvalues
  """
  eigval = np.empty((0, 0))
  eigvec = np.empty((0, 0))
  # Scan for mesh
  folder = folder if folder[-1] == '/' else folder + '/'
  exps = search_folder(folder, 'obj')
  if exps:
    if not os.path.exists('exp_data.npy'):
      logger.info('Compute expression normalization')
      # Fill data
      m = Mesh()
      data = np.zeros((n_vertex, len(exps)))
      for i, exp in enumerate(exps):
        # Log
        logger.debug('Process expression: %d', i)
        # Load
        err = m.load(exp)
        cog = np.mean(m.vertex, axis=0)
        trsfrm = np.eye(4, 4, dtype=np.float32)
        trsfrm[0:3, -1] = -cog
        m.transform(trsfrm)
        vertex = m.vertex.reshape(-1, 1)
        assert err == 0 and vertex.shape[0] == n_vertex
        # Compute covariance by accumulation
        data[:, i] = vertex[:, 0].astype(np.float64)
      # Compute mean expression
      mean_exp = np.mean(data, axis=1).reshape((-1, 1))
      m.vertex = mean_exp.astype(np.float32)
      m.save('mean_exp.obj')
      # Zero mean data
      data -= mean_exp
      # Save matrix
      np.save('exp_data.npy', data)
    else:
      logger.warning('Load expression normalization')
      data = np.load('exp_data.npy').astype(np.float64)

    # Compute PCA through SVD due to data dimension
    if not os.path.exists('smat.npy'):
      logger.info('Compute expression variation ...')
      # https://arxiv.org/pdf/1404.1100.pdf, Appendix B
      # Construct the matrix Y
      data /= (data.shape[1] - 1.0) ** 0.5
      # Compute SVD
      u_mat, s_mat, vt_mat = np.linalg.svd(data, full_matrices=False)
      # Calculate the variances / eigenvalues
      # eval are the singular value squared. Now since statistical shape model
      # uses the square root of the eval, so don't square it.
      eigval = s_mat
      # Column of V are the eigenvectors
      np.save('umat.npy', u_mat)
      np.save('smat.npy', s_mat)
      np.save('vmat.npy', vt_mat)
      np.save('eigval.npy', eigval)
    else:
      logger.warning('Load expression variation ...')
      u_mat = np.load('umat.npy')
      # s_mat = np.load('smat.npy')
      # vt_mat = np.load('vmat.npy')
      eigval = np.load('eigval.npy')

    # Compute energy + selected basis
    amount_var = np.cumsum(eigval ** 2.0) / np.sum(eigval ** 2.0)
    sel = np.where(amount_var <= retain)[0]
    # Keep only selected eigenvalues + eigenvectors
    eigval = eigval[sel].reshape(-1, 1).astype(np.float32)
    eigvec = u_mat[:, sel].astype(np.float32)
  else:
    logger.error('No expression shape founded in: %s', folder)
    exit(-1)
  return eigvec, eigval


def gram_schmidt(umat, idx=0):
  """
  Perform Modified Gram-Schmidt orthogonalisation
  :param umat:
  :param idx:
  :return:
  """
  n, k = umat.shape
  U = np.zeros((n, k), dtype=np.float32)
  if idx == 0:
    U[:, 0] = umat[:, 0] / (np.dot(umat[:, 0], umat[:, 0])) ** 0.5
    start = 1
  else:
    U[:, :idx] = umat[:, :idx]
    start = idx
  for i in range(start, k):
    U[:, i] = umat[:, i]
    for j in range(i):
      U[:, i] = U[:, i] - (
            np.dot(U[:, j], U[:, i]) / np.dot(U[:, j], U[:, j])) * U[:, j]
    U[:, i] = U[:, i] / (np.dot(U[:, i], U[:, i]) ** 0.5)
  return U


def adjust_prior(variation, prior, basis, idx=0):
  n, k = variation.shape
  p = np.zeros((k, 1), dtype=np.float32)
  p[:idx, 0] = prior[:idx, 0]
  vmat = (variation @ np.diagflat(prior))[:, idx:]
  p[idx:, 0] = np.diag(vmat.T @ basis[:, idx:]) / (
        np.diag(basis[:, idx:].T @ basis[:, idx:]) ** 0.5)
  return p


def convert(model, dest):
  r"""
  Convert BFM into binary file
  :param model: Path to matlab's BFM
  :param dest:    Location where to dump converted model
  :param obj_id:  ID, used internally by LTS5 framework, default is 0x013
  """
  # Load
  try:
    bfm = scipy.io.loadmat(model)
  except IOError:
    logger.error('Unable to open file : %s', model)
    return -1

  # Model loaded, create output folder if not already existing
  out = dest if dest[-1] == '/' else dest + '/'
  if not os.path.exists(out):
    os.makedirs(out)
  # Start conversion
  logger.info('Start conversion')
  # Tri
  tri = bfm['tl'].astype(np.int32) - 1
  tmp = np.copy(tri[:, 1])
  tri[:, 1] = tri[:, 2]
  tri[:, 2] = tmp
  tri_org = np.copy(tri)

  # Remove triangle from inside the mouth cavity
  script_folder = os.path.dirname(__file__)
  fname = '{}/../ressources/bfm_mouth_region.txt'.format(script_folder)
  tri_mouth = parse_index_file(fname)
  fname = '{}/../ressources/bfm_deng_region.txt'.format(script_folder)
  tri_deng = parse_index_file(fname)

  to_rmv = list(set(tri_mouth + tri_deng))
  tri_moved = np.copy(tri[tri_deng, :])
  tri = np.delete(tri, to_rmv, axis=0)
  logger.info('Remaining triangles: %d', tri.shape[0])
  tri = np.vstack([tri, tri_moved])

  # Rescale shape
  bfm['shapeMU'] /= 1000.0  # Convert micrometer to millimeter
  bfm['shapeEV'] /= 1000.0

  # Index of selected vertex
  ms = bfm['shapeMU'].reshape(-1, 3)
  v_idx = np.unique(tri)
  shp_cog = np.mean(ms[v_idx, :], axis=0)

  # Convert RGB texture [0, 1]
  bfm['texMU'] /= 255.0
  bfm['texEV'] /= 255.0

  # Keep basis explaining 99% of variance
  # Tewari et al, 2017 ICCV
  # Shape
  bfm['shapeEV'] = bfm['shapeEV'][:80]
  bfm['shapePC'] = bfm['shapePC'][:, :80]
  # Tex
  bfm['texEV'] = bfm['texEV'][:80]
  bfm['texPC'] = bfm['texPC'][:, :80]

  # Generate variance mask
  # logger.info('Generate per-vertex texture variation mask')
  # basis = bfm['texPC'] @ np.diagflat(bfm['texEV'])
  # var_gen = VarMaskGenerator(mean=bfm['texMU'], basis=basis, p_var=1.0)
  # var_gen.generate(n_sample=300)  # Similar to [Egger, 2018]

  m = Mesh()
  m.vertex = bfm['shapeMU'].reshape(-1, 3) - shp_cog
  m.vertex_color = bfm['texMU']
  m.tri = tri_org
  m.save('bfm.ply')

  # Generate Expression model
  if args.exp_folder is not None:
    logger.info('Generate Expression Model ...')
    exp_pc, exp_ev = learn_expression_model(args.exp_folder,
                                            bfm['shapeMU'].shape[0])

    plt.figure(1)
    plt.plot(bfm['shapeEV'][:exp_ev.shape[0]])
    plt.plot(exp_ev)
    plt.legend(['Id', 'Exp'])
    plt.show(block=False)
    # Create object
    shape_pc = np.hstack((bfm['shapePC'], exp_pc))
    shape_ev = np.vstack((bfm['shapeEV'], exp_ev))
    # dims = [bfm['shapePC'].shape[1], exp_pc.shape[1]]
    dims = np.asarray([bfm['shapePC'].shape[1], exp_pc.shape[1]],
                      dtype=np.int32)
    # bshape = PCAModel(bfm['shapeMU'], shape_pc, shape_ev, 3)
    # shape = MultiShapeModel(bshape, tri, lms, dims, shp_cog, 0x015)
    shape = MultiShapeModel(mean=bfm['shapeMU'],
                            variation=shape_pc,
                            prior=shape_ev,
                            n_channels=3,
                            triangles=tri,
                            landmarks=lms,
                            cog=shp_cog,
                            dims=dims)


    # Ortho model
    ortho_pc = gram_schmidt(shape_pc, idx=bfm['shapePC'].shape[1])
    ortho_ev = adjust_prior(shape_pc, shape_ev, ortho_pc)

    ortho_shape = MultiShapeModel(mean=bfm['shapeMU'],
                                  variation=ortho_pc,
                                  prior=ortho_ev,
                                  n_channels=3,
                                  triangles=tri,
                                  landmarks=lms,
                                  cog=shp_cog,
                                  dims=dims)
  else:
    # Create objects
    # bshape = PCAModel(bfm['shapeMU'], bfm['shapePC'], bfm['shapeEV'], 3)
    # shape = ShapeModel(bshape, tri, lms, 0x014)
    shape = ShapeModel(mean=bfm['shapeMU'],
                       variation=bfm['shapePC'],
                       prior=bfm['shapeEV'],
                       n_channels=3,
                       triangles=tri,
                       landmarks=lms,
                       cog=shp_cog)

  # btex = PCAModel(bfm['texMU'], bfm['texPC'], bfm['texEV'], 3)
  # tex = ColorModel(btex)
  tex = ColorModel(mean=bfm['texMU'],
                   variation=bfm['texPC'],
                   prior=bfm['texEV'], 
                   n_channels=3)

  # bmm = MorphableModel(shape, tex)
  # color_mm = ColorMorphableModel(bmm)
  color_mm = ColorMorphableModel(shape=shape, color=tex)

  # Open file
  logger.info('Dump everything into file')
  fname = 'basel_face' if args.exp_folder is None else 'basel_face_exp'
  f = open(out + fname + '_model.bin', 'wb')
  color_mm.save(filename=out + fname + '_model.bin')
  # Save std mask
  # var_gen.save(f)
  # Done
  #f.close()

  ortho_color_mm = ColorMorphableModel(shape=ortho_shape, color=tex)
  ortho_color_mm.save(filename=out + fname + '_ortho_model.bin')


  return 0


if __name__ == '__main__':
  # parser
  parser = argparse.ArgumentParser(description='Convert BFM model from '
                                               'matlab to binary for LTS5 '
                                               'library')
  # Path to model
  parser.add_argument('-m',
                      type=str,
                      required=True,
                      dest='model',
                      help='Location where BFM is stored (*.mat)')
  # Path where expression are stored
  parser.add_argument('-e',
                      type=str,
                      required=False,
                      dest='exp_folder',
                      help='Root location where expression shape are stored ('
                           '*.obj)')
  # Output
  parser.add_argument('-o',
                      type=str,
                      required=True,
                      dest='output',
                      help='Location where to place the converted model')
  # Parse
  args = parser.parse_args()
  # Convert
  e = convert(args.model, args.output)
  if e != 0:
    logger.error('Error while converting Basel Face Model')
  else:
    logger.info('Complete')
