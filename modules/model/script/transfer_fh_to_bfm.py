# coding=utf-8
""" Transfer facial expression from FaceWarehouse dataset to Basel Face Model"""
from argparse import ArgumentParser
from os import remove as _rm
from os import listdir as _ls
from os import walk as _walk
from os import makedirs as _mkdir
from os import rename as _rename
import os.path as _path
import re
import subprocess
import shlex
import numpy as np
from lts5.utils import init_logger
from lts5.geometry import MeshFloat as Mesh
__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
logger = init_logger()


# ----------------------------------------------------------------------------
def normalize_mesh(filename, tri_file, output_folder):
  """
  Normalize mesh for DeformationTransfer compability
  :param filename:    Mesh to normalize (*.obj)
  :param tri_file:    FaceWarehouse triangulation file (*.tri)
  :param output_folder:   Where to stored the converted mesh
  :return: -1 if error, 0 otherwise
  """
  # Already converted ?
  errno = 0
  fname = _path.basename(filename).split('.')[0]
  f_lts = _path.join(output_folder, fname + '_lts.obj')
  f_dtrans = _path.join(output_folder, fname + '_conv.obj')
  if not _path.exists(f_dtrans):
    # Load mesh + tri
    mesh = Mesh()
    errno = mesh.load(filename)
    errno |= mesh.load(tri_file)
    if not errno:
      # Compute normal
      mesh.compute_vertex_normal()
      # Save

      if not _path.exists(f_dtrans):
        # Normalize to DeformationTransfer format
        mesh.save(f_lts)
        with open(f_lts, 'r') as fin, open(f_dtrans, 'w') as fout:
          for line in fin:
            if line[:2] == 'f ':
              parts = line.strip().split(' ')
              nl = 'f %s//%s %s//%s %s//%s\n' % (parts[1], parts[1],
                                                 parts[2], parts[2],
                                                 parts[3], parts[3])
              fout.write(nl)
            else:
              fout.write(line)
          # Delete intermediate file
          _rm(f_lts)
      else:
        logger.info('File already normalized: %s', f_dtrans)
    else:
      logger.error('Unable to load file: %s', filename)
  return errno, f_dtrans


def search_folder(root_folder, ext, pattern=None):
  """
  Scan recursively a given folder for files with a specific extension
  :param root_folder:  Folder to scan
  :param ext:     Extension to look for
  :param pattern:  Pattern required to be present in filename
  :return:        Matching files
  """
  select = []
  for root, _, files in _walk(root_folder):
    for cfile in files:
      if cfile.endswith(ext):
        if pattern is None:
          select.append(_path.join(root, cfile))
        else:
          name = _path.join(root, cfile)
          if pattern in name:
            select.append(name)
  select.sort()
  return select


def generate_bfm_shape(filename, trilist):
  """
  Create a BFM shape

  :param filename: Mesh to load
  :param trilist: List of triangle to remove
  """
  # Load tri to remove
  tris = []
  tri_str = []
  vert_idx = set()
  with open(trilist, 'r') as f_tri:
    for line in f_tri:
      parts = line.strip().split(' ')
      tri_str.append(parts)
      for part in parts:
        vert_idx.add(int(part) - 1)
        tris.append(int(part) - 1)
  vert_idx = sorted(list(vert_idx))
  tris = np.asarray(tris, dtype=np.int32).reshape(-1, 3)
  # Remove them from mean shape
  file_folder = _path.split(_path.abspath(filename))[0]
  fname = _path.basename(filename).split('.')[0]
  name = _path.join(file_folder, fname + '_conv.obj')
  with open(filename, 'r') as fin, open(name, 'w') as fout:
    for line in fin:
      if line[:2] == 'f ':
        l = line[2:].strip().split(' ')
        if l not in tri_str:
          part = [int(x) for x in l]
          nl = 'f %d//%d %d//%d %d//%d\n' % (part[0], part[0],
                                             part[1], part[1],
                                             part[2], part[2])
          fout.write(nl)
        else:
          logger.debug('Remove triangle: %s', ' '.join(l))
      else:
        fout.write(line)
  return tris, vert_idx, name


def load_idx(filename):
  """
  Load index file (i.e. annotation)
  :param filename:  Path to the index file
  :return: List of index
  """
  idx = []
  with open(filename, 'r') as f_idx:
    for line in f_idx:
      if line:
        idx.append(int(line.strip()))
  return idx


def cleanup_folder(location):
  """
  Delete *.obj file within a given `folder`
  :param location:  Folder to cleanup
  """
  objs = search_folder(location, 'obj')
  _rm(_path.join(location, 'out.tricorrs'))
  for obj in objs:
    fname = _path.basename(obj)
    if 'bfm' not in fname:
      _rm(obj)


if __name__ == '__main__':
  # parser
  p = ArgumentParser(description='Transfer expression from '
                                 'FaceWarehouse to Basel Face'
                                 ' Model')
  # FaceWarehouse root
  p.add_argument('--fh_folder',
                 type=str,
                 required=True,
                 help='FaceWarehouse dataset root folder')
  # Transfer tools
  p.add_argument('--dt_exec',
                 type=str,
                 required=True,
                 help='Location where executable  for DeformationTransfer '
                      'are located')
  # Triangulation file
  p.add_argument('--tri_file',
                 type=str,
                 required=True,
                 help='FaceWarehouse triangulation file (quad to tri)')
  # Triangle to remove
  p.add_argument('--tri_rm',
                 type=str,
                 required=True,
                 help='List of triangle to remove from BFM mean (*.txt')
  p.add_argument('--dense_correspondence',
                 type=str,
                 required=False,
                 help='Dense correspondence between BFM and FW meshes' )
  # Basel Face model meanshape
  p.add_argument('--bfm_mean',
                 type=str,
                 required=True,
                 help='Basel Face Model meanshape (*.obj)')
  # Output
  p.add_argument('--out_folder',
                 type=str,
                 required=True,
                 help='Where to place the transfered mesh')
  args = p.parse_args()

  # Define 68 pts correspondence config file if no correspondence is given
  if args.dense_correspondence is None:
    logger.info('Generate anchors file')
    bfm_idx = [21622, 22162, 22050, 21819, 43295, 45277, 46664, 47604, 48124,
               48709, 49646, 51186, 53420, 33165, 33011, 32996, 33363, 39186,
               39702, 40057, 40290, 40484, 41168, 41336, 41544, 41868, 42389,
               8286, 8302, 8314, 8320, 6268, 7300, 8332, 9106, 10267, 1959,
               3888, 4920, 6217, 4803, 3900, 10472, 11368, 12787, 14472, 12804,
               11772, 5521, 6024, 7183, 8344, 9376, 10668, 11067, 9793, 9019,
               8374, 7729, 6697, 6170, 7192, 8353, 9256, 10300, 9011, 8366,
               7592]
    fw_idx = [1091, 1054, 1148, 1322, 497, 511, 3865, 477, 9053, 1902, 6754,
              9175, 6764, 1917, 2744, 8043, 2514, 10825, 10857, 725, 4300, 721,
              7193, 2147, 7195, 2139, 2128, 9396, 8943, 3419, 8972, 3549, 355,
              3563, 1780, 6464, 10892, 619, 613, 4209, 738, 4333, 2179, 7232,
              6834, 9220, 7247, 6813, 3185, 10328, 10314, 6081, 6094, 8845,
              1594, 6610, 6119, 6074, 3226, 10298, 10366, 219, 1641, 6174, 6198,
              1648, 1638, 222]
    # Dump corresponding pts between FW & BFM
    with open('../ressources/fw_to_bfm.cons', 'w') as f:
      f.write('%d\n' % len(bfm_idx))
      for fw, bfm in zip(fw_idx, bfm_idx):
        f.write('%d, %d\n' % (fw, bfm))

  bfm_nose_idx = load_idx('../ressources/bfm_nose_idx.txt')
  # Generate target BFM meanshpe
  logger.info('Generate Target Mesh')
  tri, vertex_idx, bfm_name = generate_bfm_shape(args.bfm_mean, args.tri_rm)
  # Load meahshape
  bfm_mesh = Mesh(bfm_name)
  bfm_vertex = bfm_mesh.vertex
  # Gather all subjects
  logger.info('Gather all subject in FaceWarehouse dataset')
  subjects = [x for x in _ls(args.fh_folder) if
              _path.isdir(_path.join(args.fh_folder, x))]
  subjects.sort(key=lambda x: int(x.split('_')[-1]))
  # Iterate all
  for sub in subjects:
    logger.info('Transfer data for %s', sub)
    # Scan for expressions
    folder = _path.join(args.fh_folder, sub)
    exps = search_folder(root_folder=folder,
                         ext='obj',
                         pattern='shape')
    exps.sort(key=lambda val: int(re.search(r"_([0-9]*).obj", val).groups()[0]))
    # Check if destination exists
    out_f = _path.join(args.out_folder, sub)
    if not _path.exists(out_f):
      _mkdir(out_f)
    # Normalize all expressions
    exps_norm = []
    for e in exps:
      err, n = normalize_mesh(filename=e,
                              tri_file=args.tri_file,
                              output_folder=out_f)
      if err == 0 and n != '':
        exps_norm.append(n)
      else:
        logger.error('Can not normalize file %s', e)
    # Find Neutral expression - should be at location one
    exp_neu = exps_norm[0]

    # Compute correspondance
    # -------------------------------------------------------
    pts = _path.abspath('../ressources/fw_to_bfm.cons')
    if args.dense_correspondence:
      pts = _path.abspath(args.dense_correspondence)
    w = '[1:10:100]' if args.dense_correspondence is None else '[1:0.1:2.0]'
    cmd = '%s/corres_resolve %s %s %s %s' % (args.dt_exec,
                                             exp_neu,
                                             bfm_name,
                                             pts,
                                             w)
    wdir = _path.split(exp_neu)[0]
    skip = True
    if not _path.exists(_path.join(wdir, 'out.tricorrs')):
      skip = False
      cmd_exec = shlex.split(cmd)
      process = subprocess.Popen(cmd_exec,
                                 cwd=wdir,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
      cmd_out, cmd_err = process.communicate()
    if skip is False and process.returncode != 0:
      logger.error('corres_resolve fail to run correctly : %s',
                   str(process.returncode))
      logger.debug('stdout : %s', cmd_out.decode('utf-8'))
      logger.debug('stderr : %s', cmd_err.decode('utf-8'))
    else:
      # Output data in wdir
      #   out.obj: Mesh source fitted on target
      #   out.tricorrs:   Correspondence computed

      # Transfer expressions
      # -------------------------------------------------------
      meshes = ' '.join(exps_norm[1:])
      corres = _path.join(wdir, 'out.tricorrs')
      cmd = '%s/dtrans %s %s %s %s' % (args.dt_exec,
                                       exp_neu,
                                       bfm_name,
                                       corres,
                                       meshes)
      cmd_exec = shlex.split(cmd)
      process = subprocess.Popen(cmd_exec,
                                 cwd=wdir,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
      cmd_out, cmd_err = process.communicate()
      if process.returncode != 0:
        logger.error('corres_resolve fail to run correctly : %s',
                     str(process.returncode))
        logger.debug('stdout : %s', cmd_out.decode('utf-8'))
        logger.debug('stderr : %s', cmd_err.decode('utf-8'))
      else:
        # Rename generated mesh
        t_exps = search_folder(wdir, 'obj', 'out')
        rep = re.compile(r"_([0-9]*).obj")
        exp_mesh = Mesh()
        for te in t_exps:
          m = rep.search(te)
          if m is not None:
            shape_id = int(m.groups()[0])
            fold = 'out_%d.obj' % shape_id
            fnew = 'bfm_%d.obj' % (shape_id + 1)
            te_new = te.replace(fold, fnew)
            # change name
            _rename(te, te_new)
            logger.info('Finalize expression %s', fnew)
            # Align
            e = exp_mesh.load(te_new)
            exp_vertex = exp_mesh.vertex
            exp_nose = exp_vertex[bfm_nose_idx, :]
            bfm_nose = bfm_vertex[bfm_nose_idx, :]
            shift = np.mean(bfm_nose, axis=0)
            shift -= np.mean(exp_nose, axis=0)
            exp_vertex = np.add(exp_vertex, shift)
            # Add missing vertex
            exp_vertex[vertex_idx, :] = bfm_vertex[vertex_idx, :]
            exp_mesh.vertex = exp_vertex
            # Remove normal
            exp_mesh.normal = np.empty((0, 0), dtype=np.float32)
            # Add missing triangle
            exp_tri = exp_mesh.tri
            exp_tri = np.vstack((exp_tri, tri))
            exp_mesh.tri = exp_tri
            # Save
            exp_mesh.save(te_new)
        # Clean up folder
        cleanup_folder(wdir)
    # Clear p
    del process
