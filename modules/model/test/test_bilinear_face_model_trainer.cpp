//
//  test_bilinear_face_model_trainer.cpp
//  LTS5-Lib
//
//  Created by Christophe Ecabert on 17/08/15.
//  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>

#include "lts5/model/bilinear_face_model_trainer.hpp"
#include "lts5/classifier/crossvalidation.hpp"
#include "lts5/utils/file_io.hpp"

int main(int argc, const char * argv[]) {
  if (argc < 5)
  {
    std::cout << argv[0] << "<BlendshapeRoot> <ExtensionType> <ModelOutputName> <ConfigOutName> (<StartingFold>)" << std::endl;
    return -1;
  }
  // Define starting fold
  int start_fold = 0;
  if (argc != 5) {
    start_fold = std::atoi(argv[5]);
    start_fold = start_fold >= N_FOLD ? N_FOLD - 1 : start_fold;
  }

  // Scan folder for blendshape
  std::vector<std::string> blendshape_files;
  LTS5::GetFileListing(blendshape_files,argv[1], "",argv[2]);
  std::sort(blendshape_files.begin(), blendshape_files.end());
  // Generate folds, or use existing
  std::vector<std::vector<int>> train_sample;
  std::vector<std::vector<int>> test_sample;
  std::string conf = std::string(argv[4]) + ".txt";
  std::ifstream in_stream(conf);
  if (!in_stream.is_open()) {
    // Generate cross validation
    std::cout << "Generate new cross-validation fold..." << std::endl;
    size_t n_shape = blendshape_files.size();
    LTS5::BinaryCrossValidation<cv::Mat>::GenerateKFold(n_shape,
                                                        N_FOLD,
                                                        &train_sample,
                                                        &test_sample);
    std::ofstream out_stream(conf);
    for (int k = 0; k < N_FOLD; ++k) {
      auto train_it = train_sample[k].begin();
      auto test_it = test_sample[k].begin();

      out_stream << "FOLD " << k << std::endl;
      out_stream << "TRAIN" << std::endl;
      for (; train_it != train_sample[k].end(); ++train_it) {
        out_stream << *train_it << " ";
      }
      out_stream << std::endl;
      out_stream << "TEST" << std::endl;
      for (; test_it != test_sample[k].end(); ++test_it) {
        out_stream << *test_it << " ";
      }
      out_stream << std::endl;
    }
    out_stream.close();
  } else {
    // Load existing one
    std::cout << "Load existing cross-validation fold..." << std::endl;
    std::vector<int> train;
    std::vector<int> test;
    std::string line;
    std::stringstream string_stream;
    int idx = -1;
    for (int f = 0; f < N_FOLD; ++f) {
      train.clear();
      test.clear();
      // Read line
      std::getline(in_stream, line);
      // Read train
      std::getline(in_stream, line);
      // Read train idx
      std::getline(in_stream, line);
      // Extract index
      line = line.substr(0, line.length() - 1);
      string_stream.str(line);
      while (string_stream.good()) {
        string_stream >> idx;
        train.push_back(idx);
      }
      // Read test
      std::getline(in_stream, line);
      // Read test idx
      std::getline(in_stream, line);
      // Extract index
      line = line.substr(0, line.length() - 1);
      string_stream.clear();
      string_stream.str(line);
      while (string_stream.good()) {
        string_stream >> idx;
        test.push_back(idx);
      }
      string_stream.clear();
      // Push back
      train_sample.push_back(train);
      test_sample.push_back(test);
    }
    in_stream.close();
  }

  // Train
  LTS5::BilinearFaceModelTrainer* train = new LTS5::BilinearFaceModelTrainer();
  for (int k = start_fold; k < N_FOLD; ++k) {
    std::cout << "Train fold : " << k << std::endl;
    std::cout << "---------------------------------" << std::endl;

    std::string name = std::string(argv[3]);
    size_t pos = name.rfind("/");
    std::string output_name;
    if (pos != std::string::npos) {
      output_name = name.substr(0, pos) + "/FOLD_" + std::to_string(k) + name.substr(pos, name.length());
    } else {
      output_name = "FOLD_" + std::to_string(k) + "/" + name;
    }

    // Fill tensor
    train->LoadTensorData(blendshape_files, train_sample[k]);
    // Train
    train->TrainBilinearFaceModel(output_name);
  }
  std::cout << "DONE" << std::endl;
  delete train;

  return 0;
}