/**
 *  @file   test_bilinear_face_model_converter.cpp
 *  LTS5-Lib
 *
 *  Created by Christophe Ecabert on 03/10/16.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#include <stdio.h>
#include <iostream>
#include <fstream>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/model/bilinear_face_model_converter.hpp"

/** Container type */
using ContainerType = LTS5::BilinearFaceModelConverter::ContainerType;

/**
 *  @name main
 *  @brief  Program entry point
 */
int main(int argc, const char * argv[]) {

  LTS5::CmdLineParser parser;
  parser.AddArgument("-m",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Path to bilinear face model");
  parser.AddArgument("-b",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Path to expression basis");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Output model filename");
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Ok, get parameters
    std::string face_model_path;
    std::string expr_basis_path;
    std::string output_path;
    parser.HasArgument("-m", &face_model_path);
    parser.HasArgument("-b", &expr_basis_path);
    parser.HasArgument("-o", &output_path);

    // Check output path + name
    std::string dir, file, ext;
    LTS5::ExtractDirectory(output_path, &dir, &file, &ext);
    if (ext != "raw") {
      ext = "raw";
    }

    // Convert
    LTS5::BilinearFaceModelConverter* converter;
    converter = new LTS5::BilinearFaceModelConverter();
    // Load
    err = converter->LoadModel(face_model_path);
    err |= converter->LoadBasis(expr_basis_path);
    if (!err) {
      // Get core tensor in vmmlib format
      converter->ConvertTo(ContainerType::kVmml);
      // Generate model
      converter->GenerateBlendshapeModel();
      // Save
      std::string out = dir + file + "." + ext;
      err = converter->Save(out);
      std::string msg = !err ? "Success" : "Error";
      std::cout << msg << std::endl;
    } else {
      std::cout << "Error, unable to open file : " << face_model_path;
      std::cout << " / " << expr_basis_path << std::endl;
    }
    // Clean up
    delete converter;
  } else {
    std::cout << "Unable to parse command line" << std::endl;
    parser.PrintHelp();
  }
  return err;
}




