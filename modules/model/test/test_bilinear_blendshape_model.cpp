/**
 *  @file   test_bilinear_blendshape_model.cpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   03/10/16
 *  Copyright © 2016 Ecabert Christophe. All rights reserved.
 */

#include <iostream>
#include <fstream>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/face_reconstruction/bilinear_face_model.hpp"

/**
 *  @name LoadAverageId
 *  @fn int LoadAverageId(const std::string& path,
                               cv::Mat* matrix)
 *  @brief  Load average ID from model
 *  @param[in]  path    Path to model
 *  @param[out] matrix  Average id
 *  @return -1 if error, 0 otherwise
 */
int LoadAverageId(const std::string& path,
                       cv::Mat* matrix);

/**
 *  @name main
 *  @brief  Program entry point
 */
int main(int argc, const char * argv[]) {
  
  LTS5::CmdLineParser parser;
  parser.AddArgument("-m",
                     LTS5::CmdLineParser::kNeeded,
                     "Tensor blendshape model");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::kNeeded,
                     "Output mesh name");
  
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Param
    std::string model_path;
    std::string mesh_name;
    parser.HasArgument("-m", &model_path);
    parser.HasArgument("-o", &mesh_name);
    
    // Load model
    LTS5::BilinearFaceModel* model = new LTS5::BilinearFaceModel();
    err = model->Load(model_path);
    if (!err) {
      // Ok load ground truth
      cv::Mat average_id;
      err = LoadAverageId(model_path, &average_id);
      if (!err) {
        // Get dim
        int n_vertex = model->get_number_vertex();
        int n_expr = model->get_expression_length();
        // Generate blendshape for a given ID
        cv::Mat blendshapes(n_vertex * 3, n_expr, CV_32FC1);
        model->ModelTimesWeightVector(LTS5::BilinearFaceModel::ModelDimensionEnum::kId,
                                      average_id,
                                      nullptr,
                                      &blendshapes);
        
        // Save matrix
        LTS5::SaveMatToBin("3DRec/bshape.bin", blendshapes);
        
        
        
        
        
        
        
        
        
        
        
        
        
      } else {
        std::cout << "Error, unable to load ground truth" << std::endl;
      }
    } else {
      std::cout << "Error, unable to load model file : ";
      std::cout << model_path << std::endl;
    }
  } else {
    std::cout << "Unable to parse command line" << std::endl;
    parser.PrintHelp();
  }
  return err;
}

/*
 *  @name LoadAverageId
 *  @fn int LoadAverageId(const std::string& path,
                         cv::Mat* matrix)
 *  @brief  Load average ID from model
 *  @param[in]  path    Path to model
 *  @param[out] matrix  Average id
 *  @return -1 if error, 0 otherwise
 */
int LoadAverageId(const std::string& path,
                       cv::Mat* matrix) {
  int err = -1;
  std::ifstream in(path.c_str(), std::ios_base::binary);
  if (in.is_open()) {
    // Read model dim + skip it
    int dim[3];
    in.read(reinterpret_cast<char*>(&dim[0]), 3 * sizeof(dim[0]));
    int skip_length = dim[0] * dim[1] * dim[2] * sizeof(float);
    in.seekg(skip_length, std::ios_base::cur);
    // Skip meanshape
    int meanshape_dim;
    in.read(reinterpret_cast<char*>(&meanshape_dim), sizeof(meanshape_dim));
    in.seekg(meanshape_dim * sizeof(float), std::ios_base::cur);
    // Neutral meanshape
    int neutral_dim;
    in.read(reinterpret_cast<char*>(&neutral_dim), sizeof(neutral_dim));
    in.seekg(neutral_dim * sizeof(float), std::ios_base::cur);
    // Average ID
    int id_dim;
    in.read(reinterpret_cast<char*>(&id_dim), sizeof(id_dim));
    matrix->create(id_dim, 1, CV_32FC1);
    in.read(reinterpret_cast<char*>(matrix->data), id_dim * sizeof(float));
    
    // Done
    err = 0;
  }
  return err;
}
