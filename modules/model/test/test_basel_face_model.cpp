/**
 *  @file   test_basel_face_model.cpp
 *  @brief  Generate random sample of BFM's instance
 *  @ingroup
 *
 *  @author Christophe Ecabert
 *  @date   14/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include "opencv2/calib3d.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/sys/file_system.hpp"
#include "lts5/model/morphable_model_factory.hpp"

int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("-m",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Path to Basel Face Model file");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Output location");
  int err = parser.ParseCmdLine(argc, argv);
  if (err == 0) {
    // Get args
    std::string bfm_model, output;
    parser.HasArgument("-m", &bfm_model);
    parser.HasArgument("-o", &output);
    auto* fs = LTS5::GetDefaultFileSystem();
    if (!fs->FileExist(output).Good()) {
      fs->CreateDirRecursively(output);
    }
    // Load model
    std::string name = "ColorMorphableModel";
    LTS5::MorphableModel<float>* bfm = nullptr;
    bfm = LTS5::MorphableModelFactory<float>::Get().CreateByName(name);
    if (bfm != nullptr) {
      err = bfm->Load(bfm_model);
      if (err == 0) {
        // Generate sample
        LTS5::Mesh<float, LTS5::Vector3<float>> instance;
        bfm->Generate(&instance);
        // Save
        std::string fname = output.back() == '/' ?
                            output + "rand_bfm.ply" :
                            output + "/rand_bfm.ply";
        err = instance.Save(fname);
        if (err != 0) {
          LTS5_LOG_ERROR("Error, can not save mesh into : " << fname);
        }
      } else {
        LTS5_LOG_ERROR("Error can not load BFM from : " << bfm_model);
      }
      // Clean up
      delete bfm;
    }
  } else {
    LTS5_LOG_ERROR("Unable to parse command line !");
  }
  return err;
}
