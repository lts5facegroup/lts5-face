/**
 *  @file   camera.cpp
 *  @brief  Abstract interface for PointGrey camera
 *
 *  @author Christophe Ecabert
 *  @date   15/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
*/

#include <cstring>
#include <iostream>
#include <ctime>

#include "flycapture/C/FlyCapture2_C.h"
#include "opencv2/video.hpp"

#include "lts5/pointgrey/camera.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  PGCamera
 * @fn  PGCamera()
 * @brief Constructor
 */
PGCamera::PGCamera() : external_trigger_(false),
                           ctx_(nullptr),
                           is_capturing_(false),
                           is_writing_(false),
                           cam_idx_(0),
                           frame_idx_(0){
  // Default export
  export_type_ = kImage;
  export_frmt_ = "frame_%s_%02i_%08i.jpg";
  frame_rate_ = 30.f;
}

/*
 * @name  ~PGCamera
 * @fn  ~PGCamera()
 * @brief Destructor
 */
PGCamera::~PGCamera() {
  if (is_capturing_) {
    this->StopCapture();
  }
  // Shutdow camera
  if (ctx_) {
    // Wait till image are written
    while (is_writing_);
    // Shutdown video writer if needed
    if (video_writer_.isOpened()) {
      video_writer_.release();
    }
    // Disconnect
    fc2Error err = fc2Disconnect(ctx_);
    if (err != FC2_ERROR_OK) {
      this->PrintErrorContent(err);
    }
    // Release context
    err = fc2DestroyContext(ctx_);
    if (err != FC2_ERROR_OK) {
      this->PrintErrorContent(err);
    }
  }
}

/*
 * @name  Init
 * @fn  int Init(const bool external_trig)
 * @brief Init the first camera founded
 * @param[in] external_trig External trigger flag, TRUE if used.
 */
int PGCamera::Init(const bool external_trig) {
  external_trigger_ = external_trig;
  // Get context
  int ret = -1;
  unsigned int n_available_cam = 0;
  fc2Context ctx;
  fc2Error err = fc2CreateContext(&ctx);
  if (err == FC2_ERROR_OK) {
    // Query number of camera
    err = fc2GetNumOfCameras(ctx, &n_available_cam);
    if (err == FC2_ERROR_OK) {
      if (n_available_cam > 0) {
        // Take first available camera
        ret = this->Init(0, external_trig);
      } else {
        std::cout << "Insufficient number of cameras..." << std::endl;
      }
    }
  }
  // Destroy context
  err = fc2DestroyContext(ctx);
  // Print error if any !
  this->PrintErrorContent(err);
  return ret;
}

/*
 * @name  Init
 * @fn  int Init(unsigned int camera_idx, const bool external_trig)
 * @brief Init the camera corresponding to a given index
 * @param[in] camera_idx      Index of the camera to intialize
 * @param[in] external_trig   External trigger flag, TRUE if used.
 */
int PGCamera::Init(unsigned int camera_idx, const bool external_trig) {
  int ret = -1;
  // Create context if none exist
  fc2Error err = FC2_ERROR_OK;
  if (!ctx_) {
    err = fc2CreateContext(&ctx_);
  }
  if (err == FC2_ERROR_OK) {
    // Context created
    fc2PGRGuid guid;
    external_trigger_ = external_trig;
    cam_idx_ = camera_idx;
    // Get camera at index
    err = fc2GetCameraFromIndex(ctx_, camera_idx, &guid);
    if (err == FC2_ERROR_OK) {
      // Connect
      err = fc2Connect(ctx_, &guid);
      if (err == FC2_ERROR_OK) {
        // Camera connected, get camera information
        this->Info();
        // Query resolution
        fc2CameraInfo info;
        err = fc2GetCameraInfo(ctx_, &info);
        if (err == FC2_ERROR_OK) {
          // Extract frame resolution
          sscanf(info.sensorResolution,
                 "%dx%d",
                 &frame_size_.width,
                 &frame_size_.height);
          // Setup trigger if needed
          if (external_trigger_) {
            // Check for external trigger support
            fc2TriggerModeInfo trig_info;
            err = fc2GetTriggerModeInfo(ctx_, &trig_info);
            if (err == FC2_ERROR_OK) {
              if (trig_info.present) {
                // Supported
                fc2TriggerMode trig_mode;
                err = fc2GetTriggerMode(ctx_, &trig_mode);
                if (err == FC2_ERROR_OK) {
                  // Set external trigger
                  trig_mode.mode = 0;
                  trig_mode.onOff = true;
                  trig_mode.polarity = 1;
                  trig_mode.parameter = 0;
                  trig_mode.source = 0;
                  err = fc2SetTriggerMode(ctx_, &trig_mode);
                  ret = err == FC2_ERROR_OK ? 0 : -1;
                }
              } else {
                std::cout<<"Camera does not support external trigger!"<<std::endl;
                ret = -1;
              }
            }
          }
        }
      }
    }
  }
  this->PrintErrorContent(err);
  return ret;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  StartCapture
 * @fn  int StartCapture()
 * @brief Start capturing frame
 */
int PGCamera::StartCapture() {
  int ret = -1;
  if (ctx_) {
    fc2Error err = fc2StartCaptureCallback(ctx_,
                                           PGCamera::ImageCallback,
                                           &frame_queue_);
    if (err == FC2_ERROR_ISOCH_BANDWIDTH_EXCEEDED) {
      std::cout << "Bandwidth exceeded" << std::endl;
      this->PrintErrorContent(err);
    } else if (err != FC2_ERROR_OK) {
      this->PrintErrorContent(err);
    } else {
      ret = 0;
    }
  }
  return ret;
}

/*
 * @name  StopCapture
 * @fn  int StopCapture()
 * @brief Stop capturing frame
 */
int PGCamera::StopCapture() {
  int ret = -1;
  // Disable trigger if external
  fc2Error err = FC2_ERROR_OK;
  if (external_trigger_) {
    fc2TriggerMode trig_mode;
    trig_mode.onOff = false;
    if (ctx_) {
      err = fc2GetTriggerMode(ctx_, &trig_mode);
      if (err != FC2_ERROR_OK) {
        this->PrintErrorContent(err);
      } else {
        ret = 0;
      }
    }
  }
  // Stop capture
  if (ctx_) {
    // Remove callback
    fc2SetCallback(ctx_, nullptr, nullptr);
    // Stop capture
    err = fc2StopCapture(ctx_);
    ret = err == FC2_ERROR_OK ? 0 : -1;
    this->PrintErrorContent(err);
  }
  return ret;
}

/*
 * @name  Read
 * @fn  int Read(cv::Mat* image)
 * @brief Get on frame from the connected camera
 * @param[out]  image Frame captured by this camera
 * @return -1 if error, 0 otherwise
 */
int PGCamera::Read(const bool remove_frame, cv::Mat* image) {
  int ret = -1;
  frame_queue_.Back(remove_frame, image);
  if(!image->empty()) {
    frame_size_.width = image->cols;
    frame_size_.height = image->rows;
    ret = 0;
  }
  return ret;
}

/*
 * @name  Write
 * @fn  int Write()
 * @brief Write captured frames into image/video
 * @param[in] output_name   Name of the exported file
 * @param[in] type          Type of exportation
 * @return -1 if error, 0 otherwise
 */
int PGCamera::Write(const std::string& output_name, const ExportType& type) {
  // Setup output
  int err = this->SetExportOption(output_name, type);
  if (!err) {
    err = this->Write();
  }
  return err;
}

/*
 * @name  Write
 * @fn  int Write()
 * @brief Write captured frames into image/video
 * @return -1 if error, 0 otherwise
 */
int PGCamera::Write() {
  int err = -1;
  // Empty the whole queue
  cv::Mat frame;
  do {
    is_writing_ = true;
    frame_queue_.Back(true, &frame);
    if (frame.empty()) {
      break;
    }
    if (export_type_ == kImage) {
      std::string ts;
      this->GetTimeStamp(&ts);
      // Define output name
      char name[256];
      sprintf(name, export_frmt_.c_str(), ts.c_str(), cam_idx_, frame_idx_);
      cv::imwrite(std::string(name), frame);
    } else {
      // already open ?
      if (!video_writer_.isOpened()) {
        err = -1;
        break;
      }
      // Write
      video_writer_.write(frame);
    }
    frame_idx_++;
    err = 0;
  } while(!frame.empty() );
  is_writing_ = false;
  // Done
  return err;
}

/*
 * @name  Info
 * @fn  void Info()
 * @brief Print camera info
 */
void PGCamera::Info() {
  if (ctx_) {
    fc2CameraInfo info;
    fc2Error error = fc2GetCameraInfo(ctx_, &info);
    if (error == FC2_ERROR_OK) {
      std::cout << "*** CAMERA INFORMATION ***" << std::endl;
      std::cout << "Serial number -" << info.serialNumber << std::endl;
      std::cout << "Camera model - " << info.modelName << std::endl;
      std::cout << "Camera vendor - " << info.vendorName << std::endl;
      std::cout << "Sensor - " << info.sensorInfo << std::endl;
      std::cout << "Resolution - " << info.sensorResolution << std::endl;
      std::cout << "Firmware version - " << info.firmwareVersion << std::endl;
      std::cout << "Firmware build time - " << info.firmwareBuildTime << std::endl;
    } else {
      this->PrintErrorContent(error);
    }
  }
}

#pragma mark -
#pragma mark Accessors

/*
 * @name  ImageCallback
 * @fn  static void ImageCallback(FlyCapture2::Image* image, void* user_data)
 * @brief Callback invoke when new image is available
 * @param[in]     image       New image from camera
 * @param[in,out] user_data   User data provided
 */
void PGCamera::ImageCallback(fc2Image* image,
                             void* user_data) {
  // Get queue
  SafeQueue<cv::Mat>* queue = reinterpret_cast<SafeQueue<cv::Mat>*>(user_data);
  // Convert to opencv format,push into queue
  unsigned int row_byte = (image->receivedDataSize /
                           image->rows);
  cv::Mat frame(image->rows,
                image->cols,
                CV_8UC1,
                image->pData,
                row_byte);
  queue->Push(frame);
}

/*
 * @name  SetProperty
 * @fn  int SetProperty(const std::vector<fc2Property>& property)
 * @brief Set properties to the camera
 * @param[in] property  List of properties to set
 * @return  -1 if error, 0 otherwise
 */
int PGCamera::SetProperty(const std::vector<fc2Property>& property) {
  int ret = -1;
  if (ctx_) {
    fc2Error err = FC2_ERROR_OK;
    for (int i = 0; i < property.size(); ++i) {
      if (property[i].type == fc2PropertyType::FC2_FRAME_RATE) {
        frame_rate_ = property[i].absValue;
      }
      err = fc2SetProperty(ctx_, const_cast<fc2Property*>(&property[i]));
      if (err != FC2_ERROR_OK) {
        this->PrintErrorContent(err);
        break;
      }
    }
    ret = err == FC2_ERROR_OK ? 0 : -1;
  }
  return ret;
}

/*
 * @name  SetExportOption
 * @fn  int SetExportOption(const std::string& output_name,
 *                            const ExportType& type)
 * @brief Setup export information
 * @param[in] output_name   Name of the exported file
 * @param[in] type          Type of exportation
 * @return  -1 if error, 0 otherwise
 */
int PGCamera::SetExportOption(const std::string& output_name,
                               const ExportType& type) {
  int err = -1;
  export_type_ = type;
  size_t pos = output_name.rfind(".");
  // Define output format
  if (pos != std::string::npos) {
    export_frmt_ = (output_name.substr(0, pos) +
                    (export_type_ == kImage ?
                     "_%s_%02i_%08i" :
                     "_%s_%d") +
                    output_name.substr(pos, output_name.length()));
  } else {
    // Default *.jpg
    export_frmt_ = output_name + (export_type_ == kImage ?
                                  "_%s_%02i_%08i.jpg" :
                                  "_%s_%d.avi");
  }
  if (export_type_ == kVideo) {
    std::string ts;
    this->GetTimeStamp(&ts);
    // Open exporter
    char name[256];
    sprintf(name, export_frmt_.c_str(), ts.c_str(), cam_idx_);
    if (!video_writer_.isOpened()) {
      video_writer_.open(name,
                         cv::VideoWriter::fourcc('M', 'J', 'P', 'G'),
                         frame_rate_,
                         frame_size_,
                         false);
      if(!video_writer_.isOpened()) {
        std::cout << "Unable to open : " << name << std::endl;
      }
      err = video_writer_.isOpened() ? 0 : -1;
    }
  } else {
    err = 0;
  }
  return err;
}

#pragma mark -
#pragma mark Static

/*
 * @name  DefaultIRPropertyFactory
 * @fn  static std::vector<FlyCapture2::Property> DefaultIRPropertyFactory()
 * @brief Create default property structure for IR Camera
 * @return  List of properties to apply to camera
 */
std::vector<fc2Property> PGCamera::DefaultIRPropertyFactory() {
  std::vector<fc2Property> props;
  // Auto-exposure
  fc2Property prop;
  memset(&prop, 0, sizeof(prop));
  prop.type = fc2PropertyType::FC2_AUTO_EXPOSURE ;
  prop.onOff = false;
  props.push_back(prop);
  // Shutter
  memset(&prop, 0, sizeof(prop));
  prop.type = fc2PropertyType::FC2_SHUTTER;
  prop.absControl = true;
  prop.absValue = 10.f;
  prop.onOff = false;
  props.push_back(prop);
  // Gain
  memset(&prop, 0, sizeof(prop));
  prop.type = fc2PropertyType::FC2_GAIN;
  prop.absControl = true;
  prop.absValue = 1.f;
  prop.onOff = false;
  props.push_back(prop);
  // FrameRate
  memset(&prop, 0, sizeof(prop));
  prop.type = fc2PropertyType::FC2_FRAME_RATE;
  prop.absControl = true;
  prop.absValue = 30.f;
  prop.onOff = true;
  props.push_back(prop);
  // Done
  return props;
}

/*
 * @name  PrintErrorContent
 * @fn  static void PrintErrorContent(const Error& error)
 * @brief Print Error Trace
 * @param[in] error PointGrey error generated by method
 */
void PGCamera::PrintErrorContent(const fc2Error& error) {
  std::string msg;
  switch(error) {
    case FC2_ERROR_UNDEFINED : msg = "Undefined error";
      break;
    case FC2_ERROR_FAILED : msg = "General failure";
      break;
    case FC2_ERROR_NOT_IMPLEMENTED : msg = "Function has not been implemented";
      break;
    case FC2_ERROR_FAILED_BUS_MASTER_CONNECTION :
      msg = "Could not connect to Bus Master";
      break;
    case FC2_ERROR_NOT_CONNECTED : msg = "Camera has not been connected";
      break;
    case FC2_ERROR_INIT_FAILED : msg = "Initialization failed";
      break;
    case FC2_ERROR_NOT_INTITIALIZED : msg = "Camera has not been initialized";
      break;
    case FC2_ERROR_INVALID_PARAMETER :
      msg = "Invalid parameter passed to function";
      break;
    case FC2_ERROR_INVALID_SETTINGS : msg = "Setting set to camera is invalid";
      break;
    case FC2_ERROR_INVALID_BUS_MANAGER : msg = "Invalid Bus Manager object";
      break;
    case FC2_ERROR_MEMORY_ALLOCATION_FAILED : msg = "Could not allocate memory";
      break;
    case FC2_ERROR_LOW_LEVEL_FAILURE : msg = "Low level error";
      break;
    case FC2_ERROR_NOT_FOUND : msg = "Device not found";
      break;
    case FC2_ERROR_FAILED_GUID : msg = "GUID failure";
      break;
    case FC2_ERROR_INVALID_PACKET_SIZE :
      msg = "Packet size set to camera is invalid";
      break;
    case FC2_ERROR_INVALID_MODE :
      msg = "Invalid mode has been passed to function";
      break;
    case FC2_ERROR_NOT_IN_FORMAT7 : msg = "Error due to not being in Format7";
      break;
    case FC2_ERROR_NOT_SUPPORTED : msg = "This feature is unsupported";
      break;
    case FC2_ERROR_TIMEOUT : msg = "Timeout error";
      break;
    case FC2_ERROR_BUS_MASTER_FAILED : msg = "Bus Master Failure";
      break;
    case FC2_ERROR_INVALID_GENERATION : msg = "Generation Count Mismatch";
      break;
    case FC2_ERROR_LUT_FAILED : msg = "Look Up Table failure";
      break;
    case FC2_ERROR_IIDC_FAILED : msg = "IIDC failure";
      break;
    case FC2_ERROR_STROBE_FAILED : msg = "Strobe failure";
      break;
    case FC2_ERROR_TRIGGER_FAILED : msg = "Trigger failure";
      break;
    case FC2_ERROR_PROPERTY_FAILED : msg = "Property failure";
      break;
    case FC2_ERROR_PROPERTY_NOT_PRESENT : msg = "Property is not present";
      break;
    case FC2_ERROR_REGISTER_FAILED : msg = "Register access failed";
      break;
    case FC2_ERROR_READ_REGISTER_FAILED : msg = "Register read failed";
      break;
    case FC2_ERROR_WRITE_REGISTER_FAILED : msg = "Register write failed";
      break;
    case FC2_ERROR_ISOCH_FAILED : msg = "Isochronous failure";
      break;
    case FC2_ERROR_ISOCH_ALREADY_STARTED :
      msg = "Isochronous transfer has already been started";
      break;
    case FC2_ERROR_ISOCH_NOT_STARTED :
      msg = "Isochronous transfer has not been started";
      break;
    case FC2_ERROR_ISOCH_START_FAILED : msg = "Isochronous start failed";
      break;
    case FC2_ERROR_ISOCH_RETRIEVE_BUFFER_FAILED :
      msg = "Isochronous retrieve buffer failed";
      break;
    case FC2_ERROR_ISOCH_STOP_FAILED : msg = "Isochronous stop failed";
      break;
    case FC2_ERROR_ISOCH_SYNC_FAILED :
      msg = "Isochronous image synchronization failed";
      break;
    case FC2_ERROR_ISOCH_BANDWIDTH_EXCEEDED :
      msg = "Isochronous bandwidth exceeded";
      break;
    case FC2_ERROR_IMAGE_CONVERSION_FAILED : msg = "Image conversion failed";
      break;
    case FC2_ERROR_IMAGE_LIBRARY_FAILURE : msg = "Image library failure";
      break;
    case FC2_ERROR_BUFFER_TOO_SMALL : msg = "Buffer is too small";
      break;
    case FC2_ERROR_IMAGE_CONSISTENCY_ERROR :
      msg = "There is an image consistency error";
      break;
    case FC2_ERROR_INCOMPATIBLE_DRIVER :
      msg = "The installed driver is not compatible with the library";
      break;
    default:
      break;
  }
  std::cout << msg << std::endl;
}

#pragma mark -
#pragma mark Private

/*
 * @name  GetTimeStamp
 * @fn  void GetTimeStamp(std::string* timestamp)
 * @brief Get formatted timestamp
 * @param[out]  timestamp   Current timestamp
 */
void PGCamera::GetTimeStamp(std::string* timestamp) {
  time_t rawtime;
  struct tm* info;
  char buffer[20];
  // Get time info
  time(&rawtime);
  info = localtime(&rawtime);
  // Convert to string
  strftime(buffer, 20, "%Y_%m_%d_%H_%M_%S", info); //Year-Month-Day-Hour-Min-Sec
  *timestamp = std::string(buffer);
}



}  // namepsace LTS5