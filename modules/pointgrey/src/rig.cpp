/**
 *  @file   rig.cpp
 *  @brief  PointGrey multicamera rig interface
 *
 *  @author Christophe Ecabert
 *  @date   15/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <chrono>
#include <iostream>

#include "flycapture/C/FlyCapture2_C.h"

#include "lts5/pointgrey/rig.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name  PGCameraRig
 * @fn  PGCameraRig(void)
 * @brief Constructor
 */
PGCameraRig::PGCameraRig(void) {
}

/*
 * @name  ~PGCameraRig
 * @fn  ~PGCameraRig(void)
 * @brief Destructor
 */
PGCameraRig::~PGCameraRig(void) {
  this->Release();
}

/*
 * @name  Init
 * @fn  int Init(const int n_camera, const bool external_trigger)
 * @brief
 * @param[in]
 * @param[in]
 * @return  -1 if error, 0 otherwise
 */
int PGCameraRig::Init(const int n_camera,
                      const bool external_trigger) {
  int err = -1;
  // Detect number of available camera
  fc2Context ctx;
  fc2Error fc_err = FC2_ERROR_OK;
  unsigned int n_available_camera = 0;
  fc_err = fc2CreateContext(&ctx);
  if (fc_err == FC2_ERROR_OK) {
    fc2GetNumOfCameras(ctx, &n_available_camera);
    if(n_camera <= n_available_camera) {
      // Generate default property
      auto props = PGCamera::DefaultIRPropertyFactory();
      for (int i = 0; i < n_camera; ++i) {
        // Create camera
        cams_.push_back(new PGCamera());
        err = cams_[i]->Init(i, external_trigger);
        err |= cams_[i]->SetProperty(props);
        if(err) {
          break;
        }
      }
      if(!err) {
        frame_buffer_ = std::vector<cv::Mat>(n_camera, cv::Mat());
        worker_data_ = std::vector<WriterUserData>(n_camera, WriterUserData());
      }
    } else {
      std::cout << "Not enough cameras" << std::endl;
    }
  } else {
    std::cout << "Unable to query cameras" << std::endl;
  }
  // Release ctx
  fc2DestroyContext(ctx);
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Start
 * @fn  int Start(void)
 * @brief Start capturing
 * @return  -1 if error, 0 otherwise
 */
int PGCameraRig::Start(void) {
  int err = cams_.size() == 0 ? -1 : 0;
  for (int i = 0; i < cams_.size(); ++i) {
    err |= cams_[i]->StartCapture();
  }
  return err;
}

/*
 * @name  Stop
 * @fn  int Stop(void)
 * @brief Stop capturing
 * @return  -1 if error, 0 otherwise
 */
int PGCameraRig::Stop(void) {
  int err = cams_.size() == 0 ? -1 : 0;
  for (int i = 0; i < cams_.size(); ++i) {
    err |= cams_[i]->StopCapture();
  }
  return err;
}

/*
 * @name  Read
 * @fn  int Read(cv::Mat* image)
 * @brief Get on frame from the connected camera
 * @param[in]   remove_frame  Indicate if frame need to be removed from the
 *                            queue
 * @param[out]  images        Frames captured by the cameras
 * @return -1 if error, 0 otherwise
 */
int PGCameraRig::Read(const bool remove_frame,
                      std::vector<cv::Mat>* images) {
  int err = cams_.size() == 0 ? -1 : 0;
  images->resize(cams_.size());
  for (int i = 0; i < cams_.size(); ++i) {
    err |= cams_[i]->Read(remove_frame, &(images->at(i)));
  }
  return err;
}

#pragma mark -
#pragma mark Static

/*
 * @name  WriteCallback
 * @fn  static void WriteCallback(const WriterUserData* data)
 * @param[in] data  User defined data for writer callback
 */
void PGCameraRig::WriteCallback(const PGCameraRig::WriterUserData* data) {
  // Start to run
  while(data->write) {
    // Write frame
    int ret = data->camera->Write();
    if (ret) {
      // No data -> sleep
      std::this_thread::sleep_for(std::chrono::milliseconds(data->sleep_time));
    }
  }
}

#pragma mark -
#pragma mark Accessors

/*
 * @name  SetProperty
 * @fn  int SetProperty(const int cam_idx,
 *                      const std::vector<PGCamera::Property>& property)
 * @brief Set properties to the camera
 * @param[in] cam_idx   Index of the camera where to set the property
 * @param[in] property  List of properties to set
 * @return  -1 if error, 0 otherwise
 */
int PGCameraRig::SetProperty(const int cam_idx,
                             const std::vector<PGCamera::Property>& property) {
  int ret = -1;
  if (cam_idx >= 0 && cam_idx < cams_.size()) {
    ret = cams_[cam_idx]->SetProperty(property);
  }
  return ret;
}

/*
 * @name  SetExportOption
 * @fn  void SetExportOption(const std::string& output_name,
 *                            const ExportType& type)
 * @brief Setup export information
 * @param[in] output_name   Name of the exported file
 * @param[in] type          Type of exportation
 */
int PGCameraRig::SetExportOption(const std::string& output_name,
                                 const PGCamera::ExportType& type) {
  // Setup export information
  int err = cams_.size() == 0 ? -1 : 0;
  err = worker_.size() == 0 ? err : -1;
  if(!err) {
    for (int i = 0; i < cams_.size(); ++i) {
      err |= cams_[i]->SetExportOption(output_name, type);
      // Setup writer thread
      worker_data_[i].camera = cams_[i];
      worker_data_[i].write = true;
      worker_data_[i].sleep_time = 30;  // Sleep for 30ms

      // Create thread
      worker_.push_back(std::thread(PGCameraRig::WriteCallback,
                                    &worker_data_[i]));
    }
  }
  return err;
}

#pragma mark -
#pragma mark Private

/**
 * @name  Release
 * @fn  void Release(void)
 * @brief Release ressource own by this object
 */
void PGCameraRig::Release(void) {
  // Stop worker
  for (int i = 0; i < worker_.size(); ++i) {
    // Shutdown
    worker_data_[i].write = false;
    // Wait till finish
    worker_[i].join();
  }
  // clear
  worker_.clear();
  worker_data_.clear();
  // Stop Camera
  for (int i = 0; i < cams_.size(); ++i) {
    delete cams_[i];
  }
  cams_.clear();
  // Shutdown buffer
  frame_buffer_.clear();
}
}  // namepsace LTS5
