//
//  pg_multi_camera.cpp
//  LTS5-Lib
//
//  Created by Christophe Ecabert on 17/02/16.
//  Copyright © 2015 Ecabert Christophe. All rights reserved.
//

/**
 *  @file   pg_multi_camera.cpp
 *  @brief  Example on how to use multi-camera interface for PointGrey camera
 *
 *  @author Christophe Ecabert
 *  @date   15/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
*/

#include <stdio.h>
#include <iostream>


#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/pointgrey/rig.hpp"

int main(int argc, const char * argv[]) {

  LTS5::CmdLineParser parser;
  parser.AddArgument("-n",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Number of camera");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Location where to save data (*.jpg/*.avi)");
  // Parse
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Retrive args
    std::string n_cam_str;
    std::string output_loc_str;
    parser.HasArgument("-n", &n_cam_str);
    parser.HasArgument("-o", &output_loc_str);

    // Init camera
    int n_cam = std::atoi(n_cam_str.c_str());
    LTS5::PGCameraRig* cam = new LTS5::PGCameraRig();
    err = cam->Init(n_cam, true);
    if (!err) {
      bool release_frame = output_loc_str.empty();
      if (!output_loc_str.empty()) {
        // Define type of output image/video
        size_t pos = output_loc_str.rfind(".");
        auto export_type = LTS5::PGCamera::ExportType::kVideo;
        if (pos != std::string::npos) {
          std::string ext = output_loc_str.substr(pos);
          if (ext == ".jpg" || ext == ".png") {
            export_type = LTS5::PGCamera::ExportType::kImage;
          }
        }
        // Setup export
        err = cam->SetExportOption(output_loc_str, export_type);
      }
      if (!err) {
        // Setup GUI
        std::vector<std::string> win_name;
        for (int i = 0; i < n_cam; ++i) {
          win_name.push_back("Cam" + std::to_string(i));
          cv::namedWindow(win_name[i]);
        }
        // Start Camera
        err = cam->Start();
        if (!err) {
          // Buffer
          std::vector<cv::Mat> frames(n_cam);
          char key = ' ';
          // Loop till user stop
          while(true) {
            int ret = cam->Read(release_frame, &frames);
            if (!ret) {
              // Display
              for (int i = 0; i < n_cam; ++i) {
                if (!frames[i].empty()) {
                  cv::imshow(win_name[i], frames[i]);
                }
              }
            }
            key = static_cast<char>(cv::waitKey(5));
            if (key == 'q' || key == 27) {
              break;
            }
          }
          cam->Stop();
        } else {
          std::cout << "Error, can not start camera(s)" << std::endl;
        }
      } else {
        std::cout << "Error, unable to set export type/location" << std::endl;
      }
    } else {
      std::cout << "Error, unable to initialize cameras" << std::endl;
    }
    // Clean up
    delete cam;
  } else {
    std::cout << "Unable to parse command line" << std::endl;
  }
  return err;
}