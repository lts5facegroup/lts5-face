//
//  test_pg_camera.cpp
//  LTS5-Lib
//
//  Created by Christophe Ecabert on 15/02/16.
//  Copyright © 2015 Ecabert Christophe. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <thread>


#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/pointgrey/camera.hpp"

int main(int argc, const char * argv[]) {

  int error = 0;
  // Default camera settings
  std::vector<LTS5::PGCamera::Property> props;
  props = LTS5::PGCamera::DefaultIRPropertyFactory();
  // Init camera
  LTS5::PGCamera* cam = new LTS5::PGCamera();
  error = cam->Init(true);
  error |= cam->SetProperty(props);
  error |= cam->SetExportOption("test_pg/test_frame.jpg",
                                LTS5::PGCamera::kImage);
  // Use it
  if (!error) {
    // Start capture
    error = cam->StartCapture();
    if (error) {
      std::cout << "Unable to start capturing" << std::endl;
      return error;
    }
    char key =' ';
    cv::Mat frame;
    std::string win_name = "pointgrey-cam";
    cv::namedWindow(win_name);
    int read_status;

    while(true) {
      // Read frame
      read_status = cam->Read(false, &frame);
      if (!read_status) {
        // Display
        cv::imshow(win_name, frame);
        // Write
        cam->Write();
      }
      // Quit ?
      key = cv::waitKey(5);
      if ((key == 27) || (key == 'q')) {
        break;
      }
    }
  }
  // Stop
  cam->StopCapture();
  // Clean up
  delete cam;
  return error;
}