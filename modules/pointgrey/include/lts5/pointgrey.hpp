/**
 *  @file   lts5/pointgrey.hpp
 *  @brief
 *
 *  @author Christophe Ecabert
 *  @date   15/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "lts5/pointgrey/pointgrey.hpp"
