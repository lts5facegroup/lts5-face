/**
 *  @file   rig.hpp
 *  @brief  PointGrey multicamera rig interface
 *  @ingroup pointgrey
 *
 *  @author Christophe Ecabert
 *  @date   15/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_RIG__
#define __LTS5_RIG__

#include <vector>
#include <thread>

#include "lts5/utils/library_export.hpp"
#include "lts5/pointgrey/camera.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   PGCameraRig
 * @brief   Abstract interface for PointGrey multi-camera setup
 * @author  Christophe Ecabert
 * @date    17/02/2016
 * @ingroup pointgrey
 */
class LTS5_EXPORTS PGCameraRig {
 public:
#pragma mark -
#pragma mark Initialization

  /**
   * @struct  WriterUserData
   * @brief   User defined data for Writer callback
   */
  struct WriterUserData {
    /** Camera from where to pick frames */
    PGCamera* camera;
    /** Thread life cycle */
    bool write;
    /** Sleep time - ms */
    int sleep_time;

    /**
     * @name  WriterUserData
     * @fn  WriterUserData()
     * @brief Constructor
     */
    WriterUserData() : camera(nullptr), write(true), sleep_time(30) {
    }
  };

  /**
   * @name  PGCameraRig
   * @fn  PGCameraRig()
   * @brief Constructor
   */
  PGCameraRig();

  /**
   * @name  ~PGCameraRig
   * @fn  ~PGCameraRig()
   * @brief Destructor
   */
  ~PGCameraRig();

  /**
   * @name  Init
   * @fn  int Init(const int n_camera, const bool external_trigger)
   * @brief Init camera rig
   * @param[in] n_camera            Number of camera needed
   * @param[in] external_trigger    Indicate if they are externally triggered
   * @return  -1 if error, 0 otherwise
   */
  int Init(const int n_camera, const bool external_trigger);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Start
   * @fn  int Start()
   * @brief Start capturing
   * @return  -1 if error, 0 otherwise
   */
  int Start();

  /**
   * @name  Stop
   * @fn  int Stop()
   * @brief Stop capturing
   * @return  -1 if error, 0 otherwise
   */
  int Stop();

  /**
   * @name  Read
   * @fn  int Read(cv::Mat* image)
   * @brief Get on frame from the connected camera
   * @param[in]   remove_frame  Indicate if frame need to be removed from the
   *                            queue
   * @param[out]  images        Frames captured by the cameras
   * @return -1 if error, 0 otherwise
   */
  int Read(const bool remove_frame, std::vector<cv::Mat>* images);

#pragma mark -
#pragma mark Static

  /**
   * @name  WriteCallback
   * @fn  static void WriteCallback(const WriterUserData* data)
   * @param[in] data  User defined data for writer callback
   */
  static void WriteCallback(const WriterUserData* data);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  SetProperty
   * @fn  int SetProperty(const int cam_idx,
   *                      const std::vector<PGCamera::Property>& property)
   * @brief Set properties to the camera
   * @param[in] cam_idx   Index of the camera where to set the property
   * @param[in] property  List of properties to set
   * @return  -1 if error, 0 otherwise
   */
  int SetProperty(const int cam_idx,
                  const std::vector<PGCamera::Property>& property);

  /**
   * @name  SetExportOption
   * @fn  int SetExportOption(const std::string& output_name,
   *                            const ExportType& type)
   * @brief Setup export information
   * @param[in] output_name   Name of the exported file
   * @param[in] type          Type of exportation
   * @return  -1 if error, 0 otherwise
   */
  int SetExportOption(const std::string& output_name,
                       const PGCamera::ExportType& type);

#pragma mark -
#pragma mark Private

 private:

  /**
   * @name  Release
   * @fn  void Release()
   * @brief Release ressource own by this object
   */
  void Release();

  /** List of camera on the rig */
  std::vector<PGCamera*> cams_;
  /** Frames buffers where each camera's frame are stored */
  std::vector<cv::Mat> frame_buffer_;
  /** Worker thread - Writer */
  std::vector<std::thread> worker_;
  /** Worker data - reference to object */
  std::vector<WriterUserData> worker_data_;
};

}  // namepsace LTS5


#endif //__LTS5_RIG__
