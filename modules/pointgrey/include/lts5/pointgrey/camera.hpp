/**
 *  @file   lts5/pointgrey/camera.hpp
 *  @brief  Abstract interface for PointGrey camera
 *  @ingroup pointgrey
 *
 *  @author Christophe Ecabert
 *  @date   15/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_CAMERA__
#define __LTS5_CAMERA__

#include <vector>
#include <queue>

#include "flycapture/C/FlyCapture2Defs_C.h"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"


#include "lts5/utils/library_export.hpp"
#include "lts5/utils/safe_queue.hpp"
#include "lts5/utils/safe_blocking_queue.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   PGCamera
 * @brief   Abstract interface for PointGrey camera
 * @author  Christophe Ecabert
 * @date    15/02/2016
 * @ingroup pointgrey
 */
class LTS5_EXPORTS PGCamera {
 public:

#pragma mark -
#pragma mark Initialization

  /** Camera property */
  using Property = fc2Property;

  /**
   * @enum  ExportType
   * @brief Type of image exportation supported by the camera
   */
  enum ExportType {
    /** Image export - dump frame into image file */
    kImage,
    /** Video export - dump frame into video file */
    kVideo
  };

  /**
   * @name  PGCamera
   * @fn  PGCamera(void)
   * @brief Constructor
   */
  PGCamera(void);

  /**
   * @name  ~PGCamera
   * @fn  ~PGCamera(void)
   * @brief Destructor
   */
  ~PGCamera(void);

  /**
   * @name  Init
   * @fn  int Init(const bool external_trig)
   * @brief Init the first camera founded
   * @param[in] external_trig External trigger flag, TRUE if used.
   */
  int Init(const bool external_trig);

  /**
   * @name  Init
   * @fn  int Init(unsigned int camera_idx, const bool external_trig)
   * @brief Init the camera corresponding to a given index
   * @param[in] camera_idx      Index of the camera to intialize
   * @param[in] external_trig   External trigger flag, TRUE if used.
   */
  int Init(unsigned int camera_idx, const bool external_trig);

#pragma mark -
#pragma mark Usage

  /**
   * @name  StartCapture
   * @fn  int StartCapture(void)
   * @brief Start capturing frame
   */
  int StartCapture(void);

  /**
   * @name  StopCapture
   * @fn  int StopCapture(void)
   * @brief Stop capturing frame
   */
  int StopCapture(void);

  /**
   * @name  Read
   * @fn  int Read(const bool remove_frame, cv::Mat* image)
   * @brief Get on frame from the connected camera
   * @param[in]   remove_frame  If true frame will be removed from the queue,
   *                            otherwise it'll stay in the queue. This allow
   *                            to call Write() after Read();
   * @param[out]  image         Frame captured by this camera
   * @return -1 if error, 0 otherwise
   */
  int Read(const bool remove_frame, cv::Mat* image);

  /**
   * @name  Write
   * @fn  int Write(void)
   * @brief Write captured frames into image/video
   * @return -1 if error, 0 otherwise
   */
  int Write(void);

  /**
   * @name  Write
   * @fn  int Write(const std::string& output_name, const ExportType& type)
   * @brief Write captured frames into image/video
   * @param[in] output_name   Name of the exported file
   * @param[in] type          Type of exportation
   * @return -1 if error, 0 otherwise
   */
  int Write(const std::string& output_name, const ExportType& type);

  /**
   * @name  Info
   * @fn  void Info(void)
   * @brief Print camera info
   */
  void Info(void);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  SetProperty
   * @fn  int SetProperty(const std::vector<fc2Property>& property)
   * @brief Set properties to the camera
   * @param[in] property  List of properties to set
   * @return  -1 if error, 0 otherwise
   */
  int SetProperty(const std::vector<fc2Property>& property);

  /**
   * @name  SetExportOption
   * @fn  int SetExportOption(const std::string& output_name,
   *                            const ExportType& type)
   * @brief Setup export information
   * @param[in] output_name   Name of the exported file
   * @param[in] type          Type of exportation
   * @return  -1 if error, 0 otherwise
   */
  int SetExportOption(const std::string& output_name, const ExportType& type);

#pragma mark -
#pragma mark Static methods

  /**
   * @name  ImageCallback
   * @fn  static void ImageCallback(fc2Image* image, void* user_data)
   * @brief Callback invoke when new image is available
   * @param[in]     image       New image from camera
   * @param[in,out] user_data   User data provided
   */
  static void ImageCallback(fc2Image* image, void* user_data);

  /**
   * @name  DefaultIRPropertyFactory
   * @fn  static std::vector<fc2Property> DefaultIRPropertyFactory(void)
   * @brief Create default property structure for IR Camera
   * @return  List of properties to apply to camera
   */
  static std::vector<fc2Property> DefaultIRPropertyFactory(void);

  /**
   * @name  PrintErrorContent
   * @fn  static void PrintErrorContent(const fc2Error& error)
   * @brief Print Error Trace
   * @param[in] error PointGrey error generated by method
   */
  static void PrintErrorContent(const fc2Error& error);

#pragma mark -
#pragma mark Private

 private:

  /**
   * @name  GetTimeStamp
   * @fn  void GetTimeStamp(std::string* timestamp)
   * @brief Get formatted timestamp
   * @param[out]  timestamp   Current timestamp
   */
  void GetTimeStamp(std::string* timestamp);


  /** External trigger flag */
  bool external_trigger_;
  /** FlyCapture context */
  fc2Context ctx_;
  /** Indicate if camera is currently capturing frames */
  bool is_capturing_;
  /** Indicate if currently writing data */
  bool is_writing_;
  /** Camera index */
  int cam_idx_;
  /** Frame counter */
  unsigned int frame_idx_;
  /** Frame queue */
  SafeQueue<cv::Mat> frame_queue_;
  /** Frame dimension */
  cv::Size frame_size_;
  /** Frame rate */
  float frame_rate_;
  /** Export name */
  std::string export_frmt_;
  /** Export type */
  ExportType export_type_;
  /** Video writer */
  cv::VideoWriter video_writer_;
};
}  // namepsace LTS5

#endif //__LTS5_CAMERA__
