/**
 *  @file   lts5/pointgrey/pointgrey.hpp
 *  @brief  Point grey camera wrapper
 *  @author Christophe Ecabert
 *  @date   15/02/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_POINTGREY__
#define __LTS5_POINTGREY__

/** PointGrey camera interface */
#include "lts5/pointgrey/camera.hpp"
/** PointGrey multi-camera interface */
#include "lts5/pointgrey/rig.hpp"

#endif //__LTS5_BASE_POINTGREY__
