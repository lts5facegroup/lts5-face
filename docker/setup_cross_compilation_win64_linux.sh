#!/bin/bash
# Root, from where script is called
DIR=`pwd`
# Install directory
INSTALL_DIR=$DIR/Library/Win64

# Cross-Compiler
# --------------------------------------------
# Toolchain's executable location
TOOLCHAIN_DIR=$DIR/toolchain-w64-linux/bin
# Toolchain's prefix
TOOLCHAIN=x86_64-w64-mingw32
# Toolchain's lib location
TOOLCHAIN_LIB_DIR=$TOOLCHAIN_DIR/../x86_64-w64-mingw32/lib

# Eigen3
# --------------------------------------------
# Download
wget -N http://bitbucket.org/eigen/eigen/get/3.3.1.tar.gz
tar xf 3.3.1.tar.gz && #rm 3.3.1.tar.gz
# Install
cd eigen-eigen-f562a193118d && mkdir -p build && cd build
cmake -DCMAKE_TOOLCHAIN_FILE=$DIR/toolchain-x86_64-w64-mingw32-linux-host.cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR ..
make install

# OpenBLAS
# --------------------------------------------
# Download
cd $DIR && git clone https://github.com/xianyi/OpenBLAS.git
cd OpenBLAS
# Build
make TARGET=HASWELL DYNAMIC_ARCH=1 HOSTCC=gcc CC=$TOOLCHAIN_DIR/$TOOLCHAIN-gcc FC=$TOOLCHAIN_DIR/$TOOLCHAIN-gfortran AR=$TOOLCHAIN_DIR/$TOOLCHAIN-ar BINARY=64 USE_THREAD=0
# Install
make TARGET=HASWELL DYNAMIC_ARCH=1 HOSTCC=gcc CC=$TOOLCHAIN_DIR/$TOOLCHAIN-gcc FC=$TOOLCHAIN_DIR/$TOOLCHAIN-gfortran AR=$TOOLCHAIN_DIR/$TOOLCHAIN-ar BINARY=64 USE_THREAD=0 PREFIX=$INSTALL_DIR install

# Boost
# --------------------------------------------
cd $DIR && wget -N http://downloads.sourceforge.net/project/boost/boost/1.60.0/boost_1_60_0.tar.gz
tar xf boost_1_60_0.tar.gz #&& rm -rf boost_1_53_0.tar.gz
cd boost_1_60_0
# Define toolchain
echo "using gcc : mingw64 : x86_64-w64-mingw32-g++ ;" > user-config.jam
# Configure
./bootstrap.sh --with-libraries=filesystem,system,signals,thread
# BUild
export PATH=$TOOLCHAIN_DIR/:$PATH
./b2 --user-config=user-config.jam --layout=versioned --toolset=gcc-mingw64 address-model=64 target-os=windows variant=release link=shared runtime-link=shared threading=multi threadapi=win32 install --prefix=$INSTALL_DIR

# GLEW
# --------------------------------------------
# Download
cd $DIR && wget -N http://downloads.sourceforge.net/project/glew/glew/1.13.0/glew-1.13.0.tgz
tar xf glew-1.13.0.tgz && #rm -rf glew-1.13.0.tgz
cd glew-1.13.0
# Build
make SYSTEM=linux-mingw64 CC=$TOOLCHAIN_DIR/$TOOLCHAIN-gcc LD=$TOOLCHAIN_DIR/$TOOLCHAIN-ld LDFLAGS.EXTRA=-L$TOOLCHAIN_LIB_DIR GLEW_DEST=$INSTALL_DIR
# Install
make SYSTEM=linux-mingw64 CC=$TOOLCHAIN_DIR/$TOOLCHAIN-gcc LD=$TOOLCHAIN_DIR/$TOOLCHAIN-ld LDFLAGS.EXTRA=-L$TOOLCHAIN_LIB_DIR GLEW_DEST=$INSTALL_DIR install

#GLFW
#--------------------------------------------
#Download
cd $DIR && git clone https://github.com/glfw/glfw.git
cd glfw && mkdir -p build && cd build
# Config
cmake -DCMAKE_TOOLCHAIN_FILE=../../toolchain-x86_64-w64-mingw32-linux-host.cmake -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR ..
# Build 
make -j8 && make install

#OpenCV
#--------------------------------------------
#Download
cd $DIR && git clone https://github.com/opencv/opencv.git
cd opencv && git checkout 2.4 && mkdir -p build && cd build
# Configure
cmake -DCMAKE_TOOLCHAIN_FILE=../../toolchain-x86_64-w64-mingw32-linux-host.cmake -DBUILD_EXAMPLES=OFF -DBUILD_PERF_TESTS=OFF -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR ..
# Build
make -j8
# Install
make install
