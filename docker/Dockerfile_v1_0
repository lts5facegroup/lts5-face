FROM ubuntu:14.04
MAINTAINER Christophe Ecabert (christophe.ecabert@epfl.ch)
ARG OpenCV_VERSION=3.3.0
# Update ubuntu package manager
RUN apt-get update && apt-get -y install build-essential gfortran Git wget libboost-all-dev libeigen3-dev xorg-dev libglew-dev libgtk2.0-dev \
pkg-config libavcodec-dev libavformat-dev libswscale-dev openssl libssl-dev && rm -rf /var/lib/apt/lists/*
# CMake
RUN wget -N https://cmake.org/files/v3.9/cmake-3.9.4.tar.gz && tar xf cmake-3.9.4.tar.gz && cd cmake-3.9.4 && ./bootstrap && make && make install && cd && rm -rf /cmake-3.9.4.tar.gz && rm -rf /cmake-3.9.4
# Python
RUN wget https://repo.continuum.io/archive/Anaconda2-4.2.0-Linux-x86_64.sh && bash Anaconda2-4.2.0-Linux-x86_64.sh -b && rm Anaconda2-4.2.0-Linux-x86_64.sh
# Install OpenBLAS
RUN git clone https://github.com/xianyi/OpenBLAS.git && cd OpenBLAS && make USE_THREAD=0 && make PREFIX=/usr/local/ install && cd && rm -rf /OpenBLAS
# Install GLFW3
RUN git clone https://github.com/glfw/glfw.git && mkdir glfw/build && cd glfw/build && cmake -DBUILD_SHARED_LIBS=ON .. && make -j4 && make install && cd && rm -rf /glfw
# Get OpenCV Contrib
RUN git clone https://github.com/opencv/opencv_contrib.git && cd opencv_contrib && git checkout $OpenCV_VERSION && cd
# Install OpenCV
RUN git clone https://github.com/opencv/opencv.git && cd opencv && git checkout $OpenCV_VERSION && mkdir -p build && cd build && \
cmake -DBUILD_EXAMPLES=OFF -DBUILD_PERF_TESTS=OFF -DBUILD_TESTS=OFF -DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules .. && make -j4 && make install && cd && rm -rf /opencv
# Python, anaconda as default
# ---------------------------------
ENV PATH=/root/anaconda2/bin:$PATH
# Update doxygen since it seems not to be the last version
RUN apt-get update ; apt-get -y install flex graphviz bison ; rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/doxygen/doxygen ; cd doxygen ; mkdir build ; cd build; cmake ..; make -j4; make install; cd; rm -rf /doxygen