import argparse
import owncloud
import os

__author__ = 'Christophe Ecabert'
install_deps_path = '/root/Library/Win64'
install_path = '/root/Install'
arch = 'win64'


class CheckType:
    def __init__(self):
        self.value = self.NONE

    # No control performed
    NONE = 0
    # Check all file
    ALL = 1
    # Check for specific file
    FILTER = 2


# Scan folder
def scan_folder(folder, extension, abs=False):
    """
    List all file of a specific extension within a given folder

    :param folder:      Folder to scan
    :param extension:   Extension to look for
    :param abs:         True for absolute path, false for relative
    :return:            List if file detected
    """
    picked_file = []
    folder_sz = len(folder) + 1
    for root, dirs, files in os.walk(folder):
        # Loop over files
        for f in files:
            # Correct extension ?
            if f.endswith(extension):
                f_name = root + '/' + f
                if abs:
                    picked_file.append(f_name)
                else:
                    picked_file.append(f_name[folder_sz:])
    return picked_file


def connect_to_service(username, pwd):
    """
    Connect to ownCloud service

    :param username:    Username (address)
    :param pwd:         Password
    :return:            Instance of ownCloud service or None if error
    """

    # Define client
    print 'Connecting to client ...'
    oc = owncloud.Client('https://drive.switch.ch')
    # Try to connect
    try:
        oc.login(username, pwd)
    except owncloud.HTTPResponseError as e:
        print e
        return None
    # return client connected
    return oc


def shutdown_service(service):
    """
    Close ownCloud sercice

    :param service: Instance of ownCloud service
    """
    if service:
        print 'Shutting down client'
        service.logout()
    else:
        print 'Error, unable to shuttdown client'


def gather_deployment_structure(service, dst_folder):
    """
    Scan a remote folder and return a list of files

    :param service:     Instance of ownCloud service
    :param dst_folder:  Remote path to scan
    :return:            List of files contained in dst_folder
    """
    folder_tree = {}
    if service:
        sz = len(dst_folder) + 1
        files = service.list(dst_folder, 20)
        for f in files:
            if f.file_type == 'dir':
                f_name = dst_folder + '/' + str(f.get_path())[sz:]
                if f_name not in folder_tree:
                    folder_tree[f_name] = True


    else:
        print 'Error, client not properly setup'
    return folder_tree


def copy_with_structure(service,
                        dst_folder,
                        src_folder,
                        file_list,
                        folder_structure,
                        check_exist=CheckType.NONE,
                        check_filter=None):
    """
    Copy a list of file to a remote destination

    :param service:     Instance of ownCloud service
    :param dst_folder:  Remote destination
    :param src_folder:  Host folder
    :param file_list:   List of file to copy
    :param folder_structure: Structure of remote folder
    :param check_exist: True for checking if remote file already exist
                        (upload skipped if it is the case)
    :return:
    """
    if service:
        # Get content
        files = []
        if check_exist != CheckType.NONE:
            files = service.list(dst_folder, 10)
        # Loop over file
        src_sz = len(src_folder) + 1
        for f in file_list:
            f_path = f[src_sz:]
            part = f_path.split('/')
            if len(part) != 1:
                # Already build ?
                i = 0
                key = '/' + dst_folder
                if key[-1] == '/':
                    key = key[:-1]
                while i < len(part) - 1:
                    key += '/' + part[i]
                    if key not in folder_structure:
                        # Add key
                        folder_structure[key] = True
                        # build struct
                        try:
                            service.mkdir(key)
                        except owncloud.HTTPResponseError as e:
                            print '--\t Failed to create folder ' + str(f) + \
                                  ', reason :' + str(e)
                    i += 1
            # copy file
            try:
                if check_exist == CheckType.ALL:
                    print '-- deploy ' + str(f)
                    if not filter(lambda x: x.path == '/' + dst_folder + f_path,
                                  files):
                        service.put_file(dst_folder + f_path, f)
                    else:
                        print '\t Skipped'
                elif check_exist == CheckType.FILTER:
                    print '-- deploy ' + str(f)
                    # File exist ?
                    if filter(lambda x: x.path == '/' + dst_folder + f_path,
                              files):
                        # Yes
                        if check_filter not in dst_folder + f_path:
                            service.put_file(dst_folder + f_path, f)
                        else:
                            print '\t Skipped'
                    else:
                        service.put_file(dst_folder + f_path, f)
                else:
                    print '-- deploy ' + str(f)
                    service.put_file(dst_folder + f_path, f)
            except owncloud.HTTPResponseError as e:
                print '-- deployment of ' + str(f) + ' fail, reason : ' + str(e)
    else:
        print 'Error, client not properly setup'


def deploy_file(service, dst_folder, file):
    """
    Upload a given file to a specifc remote folder

    :param service:     Instance of ownCloud service
    :param dst_folder:  Remote folder
    :param file:        File to upload
    :return:
    """
    print '-- deploy ' + str(file)
    try:
        service.put_file(dst_folder, file)
    except owncloud.HTTPResponseError as e:
        print '--\t deployment of ' + str(file) + ' failed, reason : ' + str(e)

if __name__ == '__main__':
    # Parse argument
    parser = argparse.ArgumentParser(description='Deploy LTS5-Win64 to WebDav')
    parser.add_argument('-e','--email', help='User login')
    parser.add_argument('-p','--pwd', help='User password')
    parser.add_argument('-t','--tchain', help='Toolchain folder')
    args = parser.parse_args()
    # Connect to server
    client = connect_to_service(args.email, args.pwd)
    if client:
        # Connection open, create structure
        deploy_folder = [f for f in client.list('') if
                         f.get_path() == '/LTS5']
        if not deploy_folder:
            try:
                print 'Build deployement folder tree'
                client.mkdir('LTS5')
                client.mkdir('LTS5/toolchain')
                client.mkdir('LTS5/toolchain/' + arch)
                client.mkdir('LTS5/lts5/')
                client.mkdir('LTS5/lts5/' + arch)
                client.mkdir('LTS5/lts5/' + arch + '/bin')
                client.mkdir('LTS5/lts5/' + arch + '/include')
                client.mkdir('LTS5/lts5/' + arch + '/lib')
                client.mkdir('LTS5/lts5/' + arch + '/share')
                client.mkdir('LTS5/3rdparty')
                client.mkdir('LTS5/3rdparty/' + arch)
                client.mkdir('LTS5/3rdparty/' + arch + '/bin')
                client.mkdir('LTS5/3rdparty/' + arch + '/lib')
                client.mkdir('LTS5/3rdparty/' + arch + '/include')

                # Toolchain dependencies
                print 'Deploy toolchain'
                tc_lib = scan_folder(args.tchain + '/lib', '.dll', abs=True)
                tc_lib += scan_folder(args.tchain + '/bin', '.dll', abs=True)
                for f in tc_lib:
                    deploy_file(client, 'LTS5/toolchain/' + arch + '/', f)

            except owncloud.HTTPResponseError as e:
                raise e
        # Gather deploy folder
        print 'Gather folder structure ...'
        deploy_folder_tree = gather_deployment_structure(client, '/LTS5')

        # Copy LTS5 dependencies binary
        # ----------------------------------------------------------
        print 'Deploy dependencies (*.dll)'
        # Boost + glew + glfw + OpenBLAS + OpenCV
        lib = scan_folder(install_deps_path + '/lib', '.dll', abs=True)
        lib += scan_folder(install_deps_path + '/bin', '.dll', abs=True)
        lib += scan_folder(install_deps_path + '/x64/mingw/bin',
                           '.dll',
                           abs=True)
        for f in lib:
            deploy_file(client, 'LTS5/3rdparty/' + arch + '/bin/', f)

        # Copy LTS5 dependencies symbol
        # ----------------------------------------------------------
        print 'Deploy symbol helper (*.dll.a)'
        lib = scan_folder(install_deps_path + '/lib', 'dll.a', abs=True)
        lib += scan_folder(install_deps_path + '/x64/mingw/lib',
                           'dll.a',
                           abs=True)
        for f in lib:
            deploy_file(client, 'LTS5/3rdparty/' + arch + '/lib/', f)

        # Copy LTS5 dependencies includes
        # ----------------------------------------------------------
        print 'Deploy header (*.h/*.hpp)'
        lib = scan_folder(install_deps_path + '/include',
                          extension='',
                          abs=True)
        print '#header = ' + str(len(lib))
        copy_with_structure(client,
                            'LTS5/3rdparty/' + arch + '/include/',
                            install_deps_path + '/include',
                            lib,
                            deploy_folder_tree,
                            check_exist=CheckType.ALL)

        # Copy LTS5 header
        # ----------------------------------------------------------
        print 'Deploy LTS5 header (*.h/*.hpp)'
        lib = scan_folder(install_path + '/include/lts5',
                          extension='',
                          abs=True)
        print '#header = ' + str(len(lib))
        copy_with_structure(client,
                            'LTS5/lts5/' + arch + '/include/',
                            install_path + '/include/lts5',
                            lib,
                            deploy_folder_tree,
                            check_exist=CheckType.FILTER,
                            check_filter="3rdParty")

        # Copy LTS5 binaries
        # ----------------------------------------------------------
        print 'Deploy LTS5 binaries (*.dll)'
        lib = scan_folder(install_path + '/bin', '.dll', abs=True)
        copy_with_structure(client,
                            'LTS5/lts5/' + arch + '/bin/',
                            install_path + '/bin',
                            lib,
                            deploy_folder_tree)

        # Copy LTS5 symbol
        # ----------------------------------------------------------
        print 'Deploy LTS5 symbol helper (*.dll.a)'
        lib = scan_folder(install_path + '/lib', '.dll.a', abs=True)
        copy_with_structure(client,
                            'LTS5/lts5/' + arch + '/lib/',
                            install_path + '/lib',
                            lib,
                            deploy_folder_tree)

        # Copy LTS5 executable
        # ----------------------------------------------------------
        print 'Deploy LTS5 executable (*.exe)'
        lib = scan_folder('bin', '.exe', abs=True)
        for f in lib:
            deploy_file(client, 'LTS5/lts5/' + arch + '/bin/', f)

        # Copy LTS5 cmake tools
        # ----------------------------------------------------------
        print 'Deploy LTS5 cmake tool (*.cmake)'
        lib = scan_folder(install_path + '/share', '', abs=True)
        copy_with_structure(client,
                            'LTS5/lts5/' + arch + '/share/',
                            install_path + '/share',
                            lib,
                            deploy_folder_tree)

    # Close connection
    shutdown_service(client)
