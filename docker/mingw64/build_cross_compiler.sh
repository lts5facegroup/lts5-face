#!/bin/bash

# Define usefull variable
DIR=`pwd`
HOST=`gcc -dumpmachine`
TARGET=x86_64-w64-mingw32

# Support link
GMP_ADDRESS="https://gmplib.org/download/gmp/gmp-6.1.2.tar.lz"
MPFR_ADDRESS="https://www.mpfr.org/mpfr-current/mpfr-4.0.1.tar.gz"
MPC_ADDRESS="https://ftp.gnu.org/gnu/mpc/mpc-1.1.0.tar.gz"
PPL_ADDRESS="http://www.bugseng.com/products/ppl/download/ftp/releases/1.2/ppl-1.2.tar.gz"
CLOOG_ADDRESS="https://www.bastoul.net/cloog/pages/download/count.php3?url=./cloog-0.18.4.tar.gz"
# Cross-compiler
BINUTILS_ADDRESS="https://ftp.gnu.org/gnu/binutils/binutils-2.31.1.tar.gz"
MINGWW64_ADDRESS="https://sourceforge.net/projects/mingw-w64/files/mingw-w64/mingw-w64-release/mingw-w64-v6.0.0.tar.bz2/download"
GCC_ADDRESS="ftp://ftp.mpi-sb.mpg.de/pub/gnu/mirror/gcc.gnu.org/pub/gcc/releases/gcc-5.4.0/gcc-5.4.0.tar.bz2"

# Source folders where data will be downloaded
SOURCE_DIR="${DIR}/src"
# Build folders
BUILD_DIR="${DIR}/build"
BUILD_DIR_SUPPORT="${BUILD_DIR}/support"
# Installation folder - output
PREFIX_SUPPORT="${DIR}/install/support"
PREFIX="${DIR}/install/toolchain"

# Create folder if not exist
mkdir -p ${SOURCE_DIR}
mkdir -p ${BUILD_DIR}
mkdir -p ${BUILD_DIR_SUPPORT}
mkdir -p ${PREFIX_SUPPORT}
mkdir -p ${PREFIX}

# Support Function
# ------------------------------------------------------------------------------------

build_support_gmp() {
	# Dowload GMP
	mkdir -p ${SOURCE_DIR}/gmp
	wget -nc ${GMP_ADDRESS} -O ${SOURCE_DIR}/gmp.tar.lz 
	tar --lzip -xvf ${SOURCE_DIR}/gmp.tar.lz --strip-components=1 -C ${SOURCE_DIR}/gmp
	# Build GMP
	mkdir -p ${BUILD_DIR_SUPPORT}/gmp && cd ${BUILD_DIR_SUPPORT}/gmp
	${SOURCE_DIR}/gmp/configure --prefix=${PREFIX_SUPPORT} --host=$HOST --build=$HOST --enable-cxx --disable-shared CPPFLAGS=-fexceptions
	# make install
	make install -j8
}

build_support_mpfr() {
	# Dowload - MPFR
	mkdir -p ${SOURCE_DIR}/mpfr
	wget -nc ${MPFR_ADDRESS} -O ${SOURCE_DIR}/mpfr.tar.gz
	tar -xvf ${SOURCE_DIR}/mpfr.tar.gz --strip-components=1 -C ${SOURCE_DIR}/mpfr
	# Build MPFR
	mkdir -p ${BUILD_DIR_SUPPORT}/mpfr && cd ${BUILD_DIR_SUPPORT}/mpfr
	${SOURCE_DIR}/mpfr/configure --prefix=${PREFIX_SUPPORT} --host=$HOST --build=$HOST --with-gmp=$PREFIX_SUPPORT --disable-shared
	make install -j8
}

build_support_mpc() {
	# Dowload - MPC
	mkdir -p ${SOURCE_DIR}/mpc
	wget -nc ${MPC_ADDRESS} -O ${SOURCE_DIR}/mpc.tar.gz
	tar -xvf ${SOURCE_DIR}/mpc.tar.gz --strip-components=1 -C ${SOURCE_DIR}/mpc
	# Build MPFR
	mkdir -p ${BUILD_DIR_SUPPORT}/mpc && cd ${BUILD_DIR_SUPPORT}/mpc
	${SOURCE_DIR}/mpc/configure --prefix=${PREFIX_SUPPORT} --host=$HOST --build=$HOST --with-gmp=${PREFIX_SUPPORT} --with-mpfr=${PREFIX_SUPPORT} --disable-shared
	make install -j8
}

build_support_ppl() {
	# Dowload - PPL
	mkdir -p ${SOURCE_DIR}/ppl
	wget -nc ${PPL_ADDRESS} -O ${SOURCE_DIR}/ppl.tar.gz
	tar -xvf ${SOURCE_DIR}/ppl.tar.gz --strip-components=1 -C ${SOURCE_DIR}/ppl
	# Build PPL
	mkdir -p ${BUILD_DIR_SUPPORT}/ppl && cd ${BUILD_DIR_SUPPORT}/ppl
	${SOURCE_DIR}/ppl/configure --prefix=${PREFIX_SUPPORT} --host=$HOST --build=$HOST --with-gmp=${PREFIX_SUPPORT} --disable-shared
	make install -j8
}

build_support_cloog() {
	# Dowload - Cloog
	mkdir -p ${SOURCE_DIR}/cloog
	wget -nc ${CLOOG_ADDRESS} -O ${SOURCE_DIR}/cloog.tar.gz
	tar -xvf ${SOURCE_DIR}/cloog.tar.gz --strip-components=1 -C ${SOURCE_DIR}/cloog
	# Build Cloog
	mkdir -p ${BUILD_DIR_SUPPORT}/cloog && cd ${BUILD_DIR_SUPPORT}/cloog
	${SOURCE_DIR}/cloog/configure --prefix=${PREFIX_SUPPORT} --host=$HOST --build=$HOST --with-gmp-prefix=${PREFIX_SUPPORT} --with-ppl=${PREFIX_SUPPORT} --with-host-libstdcxx="-lstdc++ -lsupc++" --disable-shared 
	make install -j8
}

# Cross compiler
# ------------------------------------------------------------------------------------

build_binutils() {
	# Download binutils
	mkdir -p ${SOURCE_DIR}/binutils
	wget -nc ${BINUTILS_ADDRESS} -O ${SOURCE_DIR}/binutils.tar.gz
	tar -xvf ${SOURCE_DIR}/binutils.tar.gz --strip-components=1 -C ${SOURCE_DIR}/binutils
	# Build Binutils
	mkdir -p ${BUILD_DIR}/binutils && cd ${BUILD_DIR}/binutils
	${SOURCE_DIR}/binutils/configure --prefix=${PREFIX} --with-sysroot=${PREFIX} --host=$HOST --build=$HOST --target=${TARGET} --enable-targets=${TARGET} --disable-multilib --enable-64-bit-bfd #--disable-nls
	make -j8 && make install
	# Update path
	export PATH="$PREFIX/bin:$PATH"
}

build_mingw_w64_headers() {
	export PATH="$PREFIX/bin:$PATH"
	# Download
	mkdir -p ${SOURCE_DIR}/mingw-w64
	wget -nc ${MINGWW64_ADDRESS} -O ${SOURCE_DIR}/mingw-w64.tar.bz2
	tar -xvf ${SOURCE_DIR}/mingw-w64.tar.bz2 --strip-components=1 -C ${SOURCE_DIR}/mingw-w64
	# Build  
	mkdir -p ${BUILD_DIR}/mingw-w64-headers && cd ${BUILD_DIR}/mingw-w64-headers
	${SOURCE_DIR}/mingw-w64/mingw-w64-headers/configure --prefix=${PREFIX}/${TARGET} --host=${TARGET} --build=${BUILD} --target=${TARGET} --enable-sdk=all 
	make -j8 && make install
}

setup_symlink() {
	ln -s ${PREFIX}/${TARGET} ${PREFIX}/mingw
}

build_core_gcc() {
	export PATH="$PREFIX/bin:$PATH"
	# Download
	mkdir -p ${SOURCE_DIR}/gcc
	wget -nc ${GCC_ADDRESS} -O ${SOURCE_DIR}/gcc.tar.bz2
	tar -xvf ${SOURCE_DIR}/gcc.tar.bz2 --strip-components=1 -C ${SOURCE_DIR}/gcc
	# Build 
	mkdir -p ${BUILD_DIR}/gcc && cd ${BUILD_DIR}/gcc
	# --with-gnu-as --with-gnu-ld --enable-libstdcxx-time --enable-checking=release
	${SOURCE_DIR}/gcc/configure --prefix=$PREFIX --with-sysroot=$PREFIX --host=$BUILD --build=$BUILD --target=$TARGET --disable-multilib --enable-64bit --enable-shared --enable-static \
	--enable-fully-dynamic-string --enable-libssp --enable-lto --enable-languages=c,c++,fortran \
	--with-host-libstdcxx="-static -lstdc++ -lm" --enable-threads=posix --enable-libgomp --with-native-system-header-dir=/$TARGET/include --with-gmp=${PREFIX_SUPPORT} --with-mpfr=${PREFIX_SUPPORT} \
	--with-mpc=${PREFIX_SUPPORT} --with-cloog=${PREFIX_SUPPORT} --with-ppl=${PREFIX_SUPPORT}
	make all-gcc -j8 && make install-gcc
}

build_mingw_w64_crt() {
	export PATH="$PREFIX/bin:$PATH"
	# Build
	mkdir -p ${BUILD_DIR}/mingw-w64-crt && cd ${BUILD_DIR}/mingw-w64-crt
	${SOURCE_DIR}/mingw-w64/mingw-w64-crt/configure --prefix=${PREFIX}/${TARGET} --with-sysroot=${PREFIX} --disable-lib32 --enable-lib64 --host=${TARGET} --build=${BUILD} --target=${TARGET}
	make -j8 && make install
}

build_win_pthread() {
	export PATH="$PREFIX/bin:$PATH"
	mkdir -p ${BUILD_DIR}/winpthreads && cd ${BUILD_DIR}/winpthreads
	${SOURCE_DIR}/mingw-w64/mingw-w64-libraries/winpthreads/configure --prefix=${PREFIX}/${TARGET} --with-sysroot=${PREFIX} --build=${BUILD} --host=${TARGET} --enable-shared --enable-static
	make -j8 && make install
}

build_lib_gcc() {
	export PATH="$PREFIX/bin:$PATH"
	# Build
	cd ${BUILD_DIR}/gcc
	make all-target-libgcc -j8 && make install-target-libgcc
}

finalize_gcc() {
	export PATH="$PREFIX/bin:$PATH"
	cd ${BUILD_DIR}/gcc
	make -j8
	make install
}

# Test
# ------------------------------------------------------------------------------------
TEST_OMP_STR="#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
int main (int argc, char *argv[]) {
int nthreads, tid;
/* Fork a team of threads giving them their own copies of variables */
#pragma omp parallel private(nthreads, tid)
 {
 /* Obtain thread number */
 tid = omp_get_thread_num();
 printf(\"Hello World from thread = %d\n\", tid);
 /* Only master thread does this */
 if (tid == 0) 
 {
	nthreads = omp_get_num_threads();
	printf(\"Number of threads = %d\n\", nthreads);
 }
 } /* All threads join master thread and disband */
}"

TEST_STL_STR="#include <thread>
#include <iostream>
#include <vector>
void hello(){
    std::cout << \"Hello from thread \" << std::this_thread::get_id() << std::endl;
}
int main(){
    std::vector<std::thread> threads;
    for(int i = 0; i < 5; ++i){
        threads.push_back(std::thread(hello));
    }
    for(auto& thread : threads){
        thread.join();
    }
    return 0;
}"

TEST_HELLO_WORLD_STR="#include <iostream>
int main() {
    std::cout << \"Hello, World!\" << std::endl;
    return 0;
}"

build_test() {
	export PATH="$PREFIX/bin:$PATH"
	mkdir -p ${BUILD_DIR}/test && cd ${BUILD_DIR}/test
	# Hello world
	echo "$TEST_HELLO_WORLD_STR" > hello_world.cpp
	${PREFIX}/bin/${TARGET}-g++ -std=c++11 hello_world.cpp -o hello_world.exe
	# Build STL thread test
	echo "$TEST_STL_STR" > hello_thread.cpp
	${PREFIX}/bin/${TARGET}-g++ -std=c++11 hello_thread.cpp -o hello_thread.exe
	# Build openmp test
	echo "$TEST_OMP_STR" > hello_openmp.cpp
	${PREFIX}/bin/${TARGET}-g++ -std=c++11 -fopenmp hello_openmp.cpp -o hello_openmp.exe
}


# Build 
# ------------------------------------------------------------------------------------
# Support 
# build_support_gmp
# build_support_mpfr
# build_support_mpc
# build_support_ppl
# build_support_cloog

# Cross-compiler
# build_binutils
# build_mingw_w64_headers
# setup_symlink
# build_core_gcc
# build_mingw_w64_crt
# build_lib_gcc
# build_win_pthread
# finalize_gcc

# Test
build_test