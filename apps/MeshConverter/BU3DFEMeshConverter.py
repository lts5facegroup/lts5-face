# coding=utf-8
import argparse
import colorlog
import logging
import os
import shutil
import shlex
import subprocess
__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
# Define Logger
# See : https://github.com/borntyping/python-colorlog
formatter = colorlog.ColoredFormatter(
    "%(log_color)s%(levelname)-8s%(reset)s | %(message)s",
    datefmt=None,
    reset=True,
    log_colors={'DEBUG': 'white',
                'INFO': 'green',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red'},
    secondary_log_colors={},
    style='%'
)
handler = colorlog.StreamHandler()
handler.setFormatter(formatter)
script_name = os.path.basename(__file__).strip().split('.')[0]
script_path =  os.path.abspath(__file__)
script_folder = os.path.split(script_path)[0] + '/'
logger = colorlog.getLogger(script_name + 'Logger')
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


# ----------------------------------------------------------------------------
def scan_folder(folder, ext, filt=None):
    r"""
    Scan recursively oa given folder for a specific extension. Filter can be 
    provided to only select a subset of the matching element
    :param folder:  Root folder to scan
    :param ext:     Extension to look for
    :param filt:    Optional filtering key
    :return:        List of files detected
    """
    pick = []
    for root, dirs, files in os.walk(folder):
        for f in files:
            if f.endswith(ext):
                if filt is None or filt in f:
                    pick.append(os.path.join(root[len(folder):], f))
    pick.sort()
    return pick


def copy_landmarks_index(src, dest):
    indexes = []
    # Read
    with open(src, 'r') as f:
        for line in f:
            parts = line.strip().split('\t')
            if len(parts) != 4:
                parts = [x for x in parts if x != '']
            assert len(parts) == 4
            indexes.append(parts)
    # Write
    with open(dest, 'w') as f:
        for idx in indexes:
            f.write(' '.join(idx) + '\n')


def convert_mesh(root, meshes, textures, cmd, landmarks, output):
    # Check if output exist
    if not os.path.exists(output):
        os.makedirs(output)
    # Iterate over all mesh
    for mesh, tex, lms in zip(meshes, textures, landmarks):
        folder, f = os.path.split(mesh)
        logger.info('Convert mesh %s' % f)
        # Check if destination exist
        if not os.path.exists(output + folder):
            os.makedirs(output + folder)
        # Copy texture
        logger.info('Copy texture ' + tex)
        if not os.path.exists(output + tex):
            shutil.copy2(root + tex, output + tex)
        # Copy landmarks index
        copy_landmarks_index(root + lms, output + lms)
        # Define conversion command
        cmd_str = cmd.replace('IN', root + mesh)
        cmd_str = cmd_str.replace('OUT', output + mesh)
        # Call conversion
        logger.info('Conversion of ' + mesh)
        cmd_exec = shlex.split(cmd_str)
        s = subprocess.Popen(cmd_exec,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        cmd_out, cmd_err = s.communicate()
        if s.returncode or len(cmd_err) != 0:
            logger.error('Reconstruction fail to run correctly '
                         + str(s.returncode))
            logger.error('stdout : ' + str(cmd_out))
            logger.error('stderr : ' + str(cmd_err))
        # Clear
        del s


if __name__ == '__main__':
    # Parser
    parser = argparse.ArgumentParser(description='Convert mesh from BU3D-FE '
                                                 'dataset into a more common '
                                                 'format (.ply)')
    # Root folder
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='root',
                        help='BU3D-FE root folder')
    # Blender executable location
    parser.add_argument('-b',
                        type=str,
                        required=True,
                        dest='blender_exec',
                        help='blender executable location')
    # Blender executable location
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='output',
                        help='Output location')
    args = parser.parse_args()

    input = args.root if args.root[-1] == '/' else args.root + '/'
    output = args.output if args.output[-1] == '/' else args.output + '/'

    # Scan mesh
    meshes = scan_folder(input, 'wrl', 'F3D')
    logger.debug('#Mesh %d' % len(meshes))
    # Scan images
    tex = scan_folder(input, 'bmp', 'F3D')
    logger.debug('#Tex %d' % len(tex))
    # Scan for landmarks
    lms = scan_folder(input, 'bnd', 'F3D')
    logger.debug('#Landmarks %d' % len(lms))

    # Convertion command
    blender_cmd = args.blender_exec
    blender_cmd += ' --background --python '
    blender_cmd += script_folder + 'blender_mesh_converter.py -- '
    blender_cmd += '--input IN --output OUT'
    convert_mesh(input, meshes, tex, blender_cmd, lms, output)
