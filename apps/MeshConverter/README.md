# Mesh Converter

*Mesh Converter* is a utility tool to convert mesh file from a given format into a new one (*i.e. .obj -> .ply*). There are two methods available:

- Python
- C++

`Python` script will call in background blender to do the conversion, therefore blender will be needed.

`C++`executable will under the hood it rely on the well known [assimp](https://github.com/assimp/assimp) library, it is completely indenpendant. For an exhausitve list of supported format, go check their website.

## Usage

### Python 

Script is called as follow:

```
python BU3DFEMeshConverter.py -i <root> -b <blender_exec> -o <output_folder>
```

Where:

- `root` is the dataset's location (*i.e. BU3D-FE*)
- `blender_exec`is the path to blender executable
- `output_folder`is the folder where processed data will be stored.

### C++

Executable can be called as follow:

```shell
./mesh_converter -i <input_mesh> -o <output_mesh>
```

