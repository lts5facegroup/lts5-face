/**
 *  @file   mesh_converter.cpp
 *  @brief  Convert a mesh into another format
 *  @ingroup    apps
 *
 *  @author Christophe Ecabert
 *  @date   18/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <iostream>

#include "assimp/Importer.hpp"
#include "assimp/Exporter.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/string_util.hpp"

/**
 * @name    FindMatchingExtension
 * @fn  std::string FindMatchingExtension(const Assimp::Exporter& exporter,
                                          const std::string& filename)
 * @brief   Find correct export property for a given filename
 * @param[] exporter    Assimp exported instance
 * @param[] filename    Desired output filename
 * @return Property or empty if nothing match
 */
std::string FindMatchingExtension(const Assimp::Exporter& exporter,
                                  const std::string& filename) {
  std::string prop, file, dir, ext;
  // Split
  LTS5::ExtractDirectory(filename, &dir, &file, &ext);

  // Loop over all supported format
  for (size_t i = 0; i < exporter.GetExportFormatCount(); ++i) {
    auto frmt = exporter.GetExportFormatDescription(i);
    if (strcmp(frmt->fileExtension, ext.c_str()) == 0) {
      // match
      prop = std::string(frmt->id);
      break;
    }
  }
  return prop;
}

int main(int argc, const char* argv[]) {

  LTS5::CmdLineParser parser;
  parser.AddArgument("-i",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Path to the mesh file to convert");
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Output filename");
  int err = parser.ParseCmdLine(argc, argv);
  if (err == 0) {
    // Retrieve arguments
    std::string in_mesh_path;
    std::string out_mesh_path;
    parser.HasArgument("-i", &in_mesh_path);
    parser.HasArgument("-o", &out_mesh_path);

    // Load mesh
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(in_mesh_path.c_str(),
                                             aiProcess_Triangulate);
    if (scene != nullptr && scene->mNumMeshes > 0) {
      // Scene loaded
      Assimp::Exporter exporter;
      // Define export property
      auto prop = FindMatchingExtension(exporter, out_mesh_path);
      if (!prop.empty()) {
        // Export
        auto e = exporter.Export(scene, prop, out_mesh_path);
        if (e != AI_SUCCESS) {
          LTS5_LOG_ERROR(exporter.GetErrorString());
        } else {
          LTS5_LOG_INFO("Exported with Success");
        }
      } else {
        LTS5_LOG_ERROR("No matching format");
      }
    } else {
      std::string msg = scene != nullptr ?
                        importer.GetErrorString() :
                        "No mesh loaded";
      LTS5_LOG_ERROR(msg);
    }
  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
  return err;
}