# coding=utf-8
import sys
import bpy
from math import radians
from mathutils import Matrix
__author__ = 'Christophe Ecabert'

# Get Parameters
# Locate parameters for script, will be placed after '--' in the command line
# See: https://blender.stackexchange.com/questions/6817
argv = sys.argv
try:
    index = argv.index("--") + 1
except ValueError:
    index = len(argv)
argv = argv[index:]

# Check parameters are valid
if len(argv) > 0:
    # Got some, check if they are valid
    if '--input' in argv and '--output' in argv and len(argv) == 4:
        input_mesh = argv[argv.index('--input') + 1]
        output_mesh = argv[argv.index('--output') + 1]
        output_mesh = output_mesh.split('.')[0] + '.ply'

        # Load input
        bpy.ops.import_scene.x3d(filepath=input_mesh)

        # Iterate over scene
        obj = bpy.data.objects.get('ShapeIndexedFaceSet')
        if obj is not None:
            # Mesh correctly loaded,
            # Set as active mesh
            bpy.context.scene.objects.active = obj
            # Convert to triangle, ensure there is only triangle
            n_tri = 0
            n_other = 0
            for p in obj.data.polygons:
                if len(p.vertices) == 3:
                    n_tri += 1
                else:
                    n_other += 1
            print('#Tri : ' + str(n_tri))
            print('#Other : ' + str(n_other))

            # Enable EDIT mode
            bpy.ops.object.mode_set(mode='EDIT')
            # Convert QUAD to TRI
            bpy.ops.mesh.quads_convert_to_tris()
            # Rotate
            rx = Matrix.Rotation(radians(90.0), 4, 'X')
            obj.matrix_world *= rx
            ry = Matrix.Rotation(radians(180.0), 4, 'Y')
            obj.matrix_world *= ry
            obj.rotation_euler = obj.matrix_world.to_euler()
            # Disable EDIT Mode
            bpy.ops.object.mode_set(mode='OBJECT')

            n_tri = 0
            n_other = 0
            for p in obj.data.polygons:
                if len(p.vertices) == 3:
                    n_tri += 1
                else:
                    n_other += 1
            print('#Tri : ' + str(n_tri))
            print('#Other : ' + str(n_other))

            # try to export it
            bpy.ops.export_mesh.ply(filepath=output_mesh, check_existing=False)
        else:
            print('No mesh loaded')
            exit(-1)
    else:
        print('Sould be call as : \"blender --background --python '
              'blender_mesh_converter.py -- --input <MeshIn> --output '
              '<MeshOut>\"')
        exit(-1)
else:
    print('Error, no parameters was given !')
    exit(-1)