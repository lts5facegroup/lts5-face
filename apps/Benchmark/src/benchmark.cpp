/**
 *  @file   benchmark.cpp
 *  @brief  Face tracker benchmark application
 *
 *  @author Gabriel Cuendet, Guillaume Jaume
 *  @date   08.05.15, 01.11.16
 *  Copyright (c) 2015 Gabriel Cuendet. All rights reserved.
 */

#include <fstream>

#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"
#include "lts5/utils/time_lapse.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/lbf_tracker.hpp"

#include "benchmark.hpp"

/*
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 *  used to benchmark SDM and LBF in term of accuracy/time/CED
 */
namespace LTS5 {

template<class T>
void Benchmark::StratifiedKFolds(const std::vector<T>& samples,
                                             const int& n_folds,
                                             bool truly_random,
                                             std::vector<std::vector<T> >* folds) {
  const int n_pos = static_cast<int>(samples.size());

  folds->clear();
  folds->reserve(n_folds);

  // Check out http://stackoverflow.com/questions/11965732
  std::vector<int> pos_indices, neg_indices;  // no default init
  pos_indices.reserve(n_pos);
  iota_n(std::back_inserter(pos_indices), n_pos, 0);  // fill them with 0, 1, ..., n_pos

  cv::RNG rng(truly_random ? cv::getTickCount() : -1);

  random_shuffle(pos_indices.begin(), pos_indices.end(), rng);

  double pos_step = double(n_pos) / double(n_folds);
  for (int i = 0; i < n_folds; ++i) {
    int fold_start = cvRound(i * pos_step);
    int fold_end = cvRound((i+1) * pos_step);
    for (int j = fold_start; j < fold_end; ++j) {
      folds[i].push_back(samples[pos_indices[j]]);
    }
  }
}

template<class OutputIterator, class Size, class Assignable>
void Benchmark::iota_n(OutputIterator first, Size n, Assignable value)
{
  std::generate_n(first, n, [&value]() {
    return value++;
  });
}


#pragma mark -
#pragma mark Initialization
/*
 *  @name   FaceModule
 *  @brief  Constructor
 */
Benchmark::Benchmark(void) : face_tracker_(nullptr),
                             face_detector_(nullptr) {
}

/*
 *  @name   ~FaceModule
 *  @brief  Destructor
 */
Benchmark::~Benchmark(void) {
  if (face_tracker_) {
    delete face_tracker_;
    face_tracker_ = nullptr;
  }
  if (face_detector_) {
    delete face_detector_;
    face_detector_ = nullptr;
  }
}

/*
 *  @name Load
 *  @brief  Initialize tracker benchmark
 *  @param[in]  type              Type of detector
 *  @param[in]  tracker_model     Path to the tracker model
 *  @param[in]  f_detect_model    Path to face detector model
 *  @return -1 if error, 0 otherwise.
 */
int Benchmark::Load(const HeaderObjectType& type,
                    const std::string& tracker_model,
                    const std::string& f_detect_model) {
  int error = 0;
  if (face_tracker_) {
    delete face_tracker_;
  }
  if (face_detector_) {
    delete face_detector_;
  }

  face_detector_ = new cv::CascadeClassifier();
  face_detector_->load(f_detect_model);
  error = face_detector_->empty() ? -1 : 0;

  switch(type) {
    case HeaderObjectType::kSdm :
      face_tracker_ = new SdmTracker();
      std::cout << "Load SDM Tracker ..." << std::endl;
      tracker_type_str_ = "_sdm";
      error |= face_tracker_->Load(tracker_model);
      break;

    case HeaderObjectType::kLBFTracker :
      face_tracker_ = new LBFTracker();
      std::cout << "Load LBF Tracker ..." << std::endl;
      tracker_type_str_ = "_lbf";
      error |= face_tracker_->Load(tracker_model);
      break;

    default: std::cout << "Unsupported tracker type" << std::endl;
      error = -1;
      break;
  }

  // Load CED parameters
  ced_param_.n_steps = 1000;
  ced_param_.delta_error = 0.0005;

  return error;
};

#pragma mark -
#pragma mark Processing

/*
 *  @name   Run
 *  @brief  Run the benchmark process
 *  @return True on success
 */
bool Benchmark::Run(void) {
  bool success = true;

  if (images_dir_.empty()) {
    std::cerr << "The image directory is not set... Please specify";
    std::cout << " an image directory" << std::endl;
    return false;
  }

  std::vector<std::string> images_files;
  images_files = LTS5::ScanImagesInFolder(images_dir_);
  std::sort(images_files.begin(), images_files.end());
  const int k_total_images = static_cast<int>(images_files.size());
  std::cout << k_total_images << " images found in the folder" << std::endl;

  for (int i=0; i<k_total_images; i++) {
    // Define annotation filename
    // -----------------------------------------------------------------
    std::string img_filename = images_files[i];
    size_t pos = img_filename.rfind('.');
    std::string pts_filename = img_filename.substr(0, pos) + ".pts";
    // Open images
    // -----------------------------------------------------------------
    cv::Mat img = cv::imread(img_filename);
    if (img.channels() != 1) {
      cv::cvtColor(img, working_img_, CV_BGR2GRAY);
    } else {
      working_img_ = img;
    }
    // Load annotation
    // -----------------------------------------------------------------
    LTS5::LoadPts(pts_filename, annotation_);
    // Run face detection
    // -----------------------------------------------------------------
    std::vector<cv::Rect> face_detections;
    face_detector_->detectMultiScale(working_img_,
                                     face_detections,
                                     1.1,
                                     2,
                                     0,
                                     cv::Size(30, 30));
    cv::Rect face_rect = PruneFaceDetections(annotation_, face_detections);
    if (face_rect.width == 0){
      std::cerr << "Face detector failed on image " << img_filename << std::endl;
      continue;
    }

    // Run tracker
    // -----------------------------------------------------------------
    auto detect = [&](void) -> void {
      face_tracker_->Detect(working_img_, face_rect, &face_landmarks_);
    };
    auto t_ms = LTS5::Measure<>::Duration(detect);
    timing_.push_back(t_ms.count());
    std::cout << "Time [ms] : " << t_ms.count() << std::endl;


    // Compute RMSE
    // -----------------------------------------------------------------
    double val = Rmse();
    rmse_.push_back(val);
    rmse_per_landmark_.push_back(RmsePerLandmark());
    std::cout << "with RMSE : " << val << std::endl;
  }

  // Compute AUCalpha metric
  // -----------------------------------------------------------------
  int N = static_cast<int>(rmse_.size());
  cumul_error_dist_ = CumulErrorDist(N ,
                                     ced_param_.n_steps,
                                     ced_param_.delta_error );

  // Compute Mean metrics
  mean_rmse_ = std::accumulate(rmse_.begin(),
                               rmse_.end(),
                               0.0) / rmse_.size();
  mean_timing_ = std::accumulate(timing_.begin(),
                                 timing_.end(),
                                 0.0) / timing_.size();


  // Display interesting results on console
  std::cout << "Mean RMSE on " << rmse_.size();
  std::cout << " images is " << mean_rmse_ << std::endl;
  std::cout << "Average processing time : " << mean_timing_ << " ms";
  std::cout << std::endl;
  // Done
  return success;
};

/*
 *  @name   Iod
 *  @brief  Returns the inter-occular distance, for normalization
 *  @return Inter occular distance
 */
double Benchmark::Iod(const cv::Mat& shape) {
  const int nPoints = shape.rows / 2;
  std::vector<int> indices(4);
  switch (nPoints) {
    case 11: // Self selected 11 points for all datasets
      // Use c++11 list_initializer<T>
      indices.assign({0, 1, 2, 3});
      break;
    case 16: // Idiap
      indices.assign({2, 3, 4, 5});
      break;
    case 20: // BioID
      indices.assign({9, 10, 11, 12});
      break;
    case 29: // LFPW29
      indices.assign({8, 10, 11, 9});
      break;
    case 49: // CMU68_inner_shape
      indices.assign({19, 22, 25, 28});
          break;
    case 58: // CVHCI
      indices.assign({21, 25, 17, 13});
      break;
    case 66: // CMU66
      indices.assign({36, 39, 42, 45});
      break;
    case 68: // CMU68
      indices.assign({36, 39, 42, 45});
      break;
    case 76: // MUCT
      indices.assign({27, 29, 34, 32});
      break;
    default:
      std::stringstream ss;
      ss << "IOD not defined for " << nPoints << " points annotation";
      throw std::runtime_error(ss.str());
  }
  double dx1, dx2, dy1, dy2;
  dx1 = shape.at<double>(indices[2], 0) - shape.at<double>(indices[0], 0);
  dx2 = shape.at<double>(indices[3], 0) - shape.at<double>(indices[1], 0);
  dy1 = shape.at<double>(indices[2] + nPoints, 0) - shape.at<double>(indices[0] + nPoints, 0);
  dy2 = shape.at<double>(indices[3] + nPoints, 0) - shape.at<double>(indices[1] + nPoints, 0);
  return sqrt(dx1 * dx1 + dy1 * dy1) / 2 + sqrt(dx2 * dx2 + dy2 * dy2) / 2;
};


/*
 * @name Rmse
 * @brief compute the Rmse
 * @return
 */
double Benchmark::Rmse() {
  double error = 0.0;
  int nPoints = annotation_.rows / 2;
  std::vector<int> points;
  if (nPoints == 29) { // Evaluation is only on 17 points for LFPW29
    points.assign({0, 1, 2, 3, 8, 9, 10, 11, 16, 17, 18, 19, 20, 22, 23, 24, 27});
  } else {
    for (int j = 0; j < nPoints; j++) {
      points.push_back(j);
    }
  }
  for (int j = 0; j < points.size(); j++) {
    double x1, x2, y1, y2;
    x1 = face_landmarks_.at<double>(points[j], 0);
    y1 = face_landmarks_.at<double>(points[j] + nPoints, 0);
    x2 = annotation_.at<double>(points[j], 0);
    y2 = annotation_.at<double>(points[j] + nPoints, 0);
    error += (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
  }
  error /= points.size();
  // Interocular distance
  double d = Iod(annotation_);
  // Normalized RMSE
  error /= (d * d);
  return sqrt(error);
};


/*
 * @name RmsePerLandmak
 * @brief Return the root mean square error per landmarks
 * @return vector of normalized error by the inter pupils dist
 */
std::vector<double> Benchmark::RmsePerLandmark() {

  int nbr_points = annotation_.rows / 2;
  std::vector<double> error_per_landmark(nbr_points,0.);
  double pupils_dist = Iod(annotation_);

  cv::Mat annot = annotation_;

  for (int j = 0; j < nbr_points ; ++j) {
    double x1, x2, y1, y2, error;
    x1 = face_landmarks_.at<double>(j, 0);
    y1 = face_landmarks_.at<double>(j + nbr_points, 0);
    x2 = annotation_.at<double>(j, 0);
    y2 = annotation_.at<double>(j + nbr_points, 0);
    error = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
    error /= (pupils_dist*pupils_dist);
    error_per_landmark[j] = sqrt(error);
  }

  return error_per_landmark;
}

/*
 * @name CumulErrorDist
 * @brief use the Rmse vector (rmse of each image) to compute CED
 * @return return the cumulative error distribution
 */
std::vector<std::vector<double>> Benchmark::CumulErrorDist(int N,
                                                           int number_of_steps,
                                                           double delta_error) {

  std::vector<std::vector<double> > cumul_error_dist(number_of_steps ,
                                                     std::vector<double>(2));
  double error_tol= 0;

  if (N == 0) {
    std::cerr<<"The RMSE per image has not been calcultated yet or the dir "
            "is empty"<<std::endl;
  }

  int number_of_images_below_thresh = 0;

  for (int j = 0 ; j < number_of_steps ; ++j) {
    for (int jj = 0 ; jj<N ; ++jj) {
      if (rmse_[jj]-error_tol < 0) {
        number_of_images_below_thresh += 1;
      }
    }
    cumul_error_dist[j][0] = error_tol;
    cumul_error_dist[j][1] = number_of_images_below_thresh;
    error_tol += delta_error;
    number_of_images_below_thresh = 0;
  }
  return cumul_error_dist;
}

/**
 * @name AUCalpha
 * @param[in] alpha limit of the integration in CED plot
 * @return AUC for the val alpha
 */
double Benchmark::AUCalpha() {
  // implemented in Matlab directly from the CED curve
  return 0;
}


/*
 *  @name       PruneFaceDetections
 *  @brief      Amongst (potentially) several detections in an image, returns
 *              the one which center is closest to the annotation center
 *  @param[in]  annotation      Annotation
 *  @param[in]  detections      Vector of Rect returned by the face detector
 *  @return     Detection which center is closest to the annotation center or an
 *              empty Rect
 */
cv::Rect Benchmark::PruneFaceDetections(const cv::Mat& annotation,
                                         const std::vector<cv::Rect>&
                                         detections) {
  cv::Rect return_rect;
  std::vector<cv::Rect> candidates_rect;// Rects that have shape center inside
  for (int j = 0; j < detections.size(); j++) {
    cv::Point2d center = LTS5::GetCenterPoint(annotation);
    if (center.x > detections[j].x &&
        center.x < detections[j].x + detections[j].width &&
        center.y > detections[j].y &&
        center.y < detections[j].y + detections[j].height) {
      candidates_rect.push_back(detections[j]);
    }
  }

  if (candidates_rect.size() == 1) {
    return_rect = candidates_rect[0];
  }
  else if (candidates_rect.size() > 1) {
    int minIdx = 0;
    double minDist = DBL_MAX;
    for (int j = 0; j < candidates_rect.size(); j++) {
      cv::Point2d rectangle_center(candidates_rect[j].x +
                                   candidates_rect[j].width / 2.0,
                                   candidates_rect[j].y +
                                   candidates_rect[j].height / 2.0);
      cv::Point2d pt1 = LTS5::GetCenterPoint(annotation);

      double v = (rectangle_center.x - pt1.x) * (rectangle_center.x - pt1.x);
      v += (rectangle_center.y - pt1.y) * (rectangle_center.y - pt1.y);
      double dist = sqrt(v);

      if (dist < minDist) {
        minIdx = j;
        minDist = dist;
      }
    }
    return_rect = candidates_rect[minIdx];
  }
  return return_rect;
};


/*
 * @name Write
 * @name write down important data for benchmark post-processing
 * @param[in] outTxt file to write on
 */
int Benchmark::Write(const std::string &filename) const {
  int err = -1;
  std::ofstream stream(filename.c_str(), std::ios_base::out);
  if (stream.is_open()) {
    // Write General Parameters
    stream << tracker_type_str_ << std::endl;
    stream << rmse_.size() << std::endl;
    // Write RMSE/RMSE_PER_LANDMARK
    stream << "#RMSE" << std::endl;
    for (size_t i = 0; i < rmse_.size(); ++i) {
      stream << rmse_[i] << std::endl;
      for (size_t k = 0; k < rmse_per_landmark_[i].size(); ++k) {
        stream << rmse_per_landmark_[i][k] << " ";
      }
      stream << std::endl;
    }
    // Write CED
    stream << "#CED" << std::endl;
    for (size_t j=0 ; j < ced_param_.n_steps ; ++j) {
      stream << cumul_error_dist_[j][0] << " ";
    }
    stream << std::endl;
    for (size_t j = 0 ; j < ced_param_.n_steps ; ++j) {
      stream << cumul_error_dist_[j][1] << " ";
    }
    stream << std::endl;
    // Write Time
    stream << "#TIME" << std::endl;
    for (size_t j = 0 ; j < timing_.size() ; ++j) {
      stream << timing_[j] << " ";
    }
    stream << std::endl;
    // Sanity check
    err = stream.good() ? 0 : -1;
    // Done
    stream.close();
  } else {
    std::cout << "Error can not open : " << filename << std::endl;
  }
  return err;
}


} // namepsace LTS5
