/**
 *  @file   benchmark.cpp
 *  @brief  Face tracker benchmark application
 *
 *  @author Gabriel Cuendet, Guillaume Jaume
 *  @date   08.05.15, 01.11.16
 *  Copyright (c) 2015 Gabriel Cuendet. All rights reserved.
 */

#include <iostream>

#include "lts5/utils/cmd_parser.hpp"

#include "benchmark.hpp"

/**
 * @name
 * @brief   Check if a given type is known
 * @param type_str  Type in string format
 * @param type      Converted type
 * @return  -1 if error, 0 otherwise
 */
int TypeIsKnown(const std::string& type_str, LTS5::HeaderObjectType* type);


int main(int argc, const char * argv[]) {
  using State = LTS5::CmdLineParser::ArgState;
  LTS5::CmdLineParser parser;
  parser.AddArgument("-i", State::kNeeded, "Input folder");
  parser.AddArgument("-type", State::kNeeded, "Type of tracker : SDM, LBF");
  parser.AddArgument("-t", State::kNeeded, "Path to tracker model");
  parser.AddArgument("-f", State::kNeeded, "Path to face detector model");
  parser.AddArgument("-o", State::kNeeded, "Output filename (result)");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Get parameters
    std::string input, type_str, tracker_model, f_detect_model, output_file;
    parser.HasArgument("-i", &input);
    parser.HasArgument("-type", &type_str);
    parser.HasArgument("-t", &tracker_model);
    parser.HasArgument("-f", &f_detect_model);
    parser.HasArgument("-o", &output_file);

    // Type is known ?
    LTS5::HeaderObjectType type;
    err = TypeIsKnown(type_str, &type);
    if (!err) {
      LTS5::Benchmark test;
      err = test.Load(type, tracker_model, f_detect_model);
      if (!err) {
        test.set_images_dir(input);
        test.Run();
        err = test.Write(output_file);
      } else {
        std::cout << "Error when loading the model" << std::endl;
      }
    } else {
      std::cout << "Error, unknown tracker type" << std::endl;
    }
  } else {
    std::cout << "Unable to parse command line" << std::endl;
    parser.PrintHelp();
  }
  return err;
}

/*
 * @name
 * @brief   Check if a given type is known
 * @param type_str  Type in string format
 * @param type      Converted type
 * @return  -1 if error, 0 otherwise
 */
int TypeIsKnown(const std::string& type_str, LTS5::HeaderObjectType* type) {
  int err = -1;
  if (type_str == "SDM") {
    err = 0;
    *type = LTS5::HeaderObjectType::kSdm;
  } else if (type_str == "LBF") {
    err = 0;
    *type = LTS5::HeaderObjectType::kLBFTracker;
  }
  return err;
}