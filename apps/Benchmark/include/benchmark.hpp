/**
 *  @file   benchmark.cpp
 *  @brief  Face tracker benchmark application
 *
 *  @author Gabriel Cuendet, Guillaume Jaume
 *  @date   08.05.15, 01.11.16
 *  Copyright (c) 2015 Gabriel Cuendet. All rights reserved.
 */

#ifndef __LTS5_Dev__benchmark__
#define __LTS5_Dev__benchmark__

#include <string>
#include <cstdio>
#include <algorithm>

#include "opencv2/core/core.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/object_type.hpp"
#include "lts5/face_tracker/base_face_tracker.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  Benchmark
 *  @brief  Face tracker benchmark application
 *  @author Gabriel Cuendet, Guillaume Jaume
 *  @date   08/05/2015
 */
class Benchmark {
 public :

  template<class T>
  static void StratifiedKFolds(const std::vector<T>& samples,
                               const int& n_folds,
                               bool truly_random,
                               std::vector<std::vector<T> >* folds);

  template<class OutputIterator, class Size, class Assignable>
  static void iota_n(OutputIterator first, Size n, Assignable value);


  /**
   * @struct CEDParam
   * @brief Cumulative Error Distribution computation
   */
  struct CEDParam {
    /** number of steps in the CED */
    int n_steps;
    /** delta x step in the CED */
    double delta_error;
  };


#pragma mark -
#pragma mark Initialization

  /**
   *  @name   FaceModule
   *  @brief  Constructor
   */
  Benchmark(void);


  /**
   *  @name   ~FaceModule
   *  @brief  Destructor
   */
  ~Benchmark(void);

  /**
   *  @name Load
   *  @brief  Initialize tracker benchmark
   *  @param[in]  type              Type of detector
   *  @param[in]  tracker_model     Path to the tracker model
   *  @param[in]  f_detect_model    Path to face detector model
   *  @return -1 if error, 0 otherwise.
   */
  int Load(const HeaderObjectType& type,
           const std::string& tracker_model,
           const std::string& f_detect_model);

#pragma mark -
#pragma mark Processing


  /**
   *  @name   Run
   *  @brief  Run the benchmark process
   *  @return True on success
   */
  bool Run(void);

#pragma mark -
#pragma mark Accessors


  /**
   *  @name   images_dir
   *  @brief  Get the path to the image directory containing images and annotations
   *  @return images_dir_   Path
   */
  std::string images_dir(void) const {
    return images_dir_;
  }


  /**
   *  @name   mean_rmse
   *  @brief  Get the mean rmse
   *  @return mean_rmse_
   */
  double mean_rmse(void) const {
    return mean_rmse_;
  }


  /**
   *  @name   set_images_dir
   *  @brief  Set the path to the image directory containing images and annotations
   *  @param[in]  images_dir   Path
   */
  void set_images_dir(const std::string& images_dir) {
    images_dir_ = images_dir;
  }


  /**
   *  @name   face_landmarks
   *  @brief  Give set of landmarks
   *  @return Face landmarks
   */
  cv::Mat face_landmarks(void) const {return face_landmarks_;}


  /**
   *  @name   annotation
   *  @brief  Returns landmarks annotations
   *  @return Annotation
   */
  cv::Mat annotation(void) const {return annotation_;}


  /**
   *  @name   Iod
   *  @brief  Returns the inter-occular distance, for normalization
   *  @return Inter occular distance
   */
  double Iod(const cv::Mat& shape);


  /**
   *  @name   Rmse
   *  @brief  Returns the root mean square error between landmarks and annotation
   *  @return RMS Error
   */
  double Rmse();


  /**
   * @name RmsePerLandmak
   * @brief Return the root mean square error per landmarks
   * @return vector of normalized error by the inter pupils dist
   */
  std::vector<double> RmsePerLandmark();


  /**
   * @name CumulErrorDist
   * @brief use the Rmse vector (rmse of each image) to compute CED
   * @return return the cumulative error distribution
   */
  std::vector<std::vector<double>> CumulErrorDist(int N, int number_of_steps, double delta_error);

  /**
   * @name AUCalpha
   * @return AUC for the val alpha specified in auc_param
   */
  double AUCalpha();

/*
 *  @name       PruneFaceDetections
 *  @brief      Amongst (potentially) several detections in an image, returns
 *              the one which center is closest to the annotation center
 *  @param[in]  annotation      Annotation
 *  @param[in]  detections      Vector of Rect returned by the face detector
 *  @return     Detection which center is closest to the annotation center or an
 *              empty Rect
 */
  cv::Rect PruneFaceDetections(const cv::Mat& annotation,
                               const std::vector<cv::Rect>&
                               detections);

  /**
   * @name Write
   * @brief write benchmark results in separated files for analysis
   */
  int Write(const std::string& filename) const;

private :

  /** Viola-Jones face detector */
  cv::CascadeClassifier* face_detector_;
  /** Face tracker */
  BaseFaceTracker<cv::Mat, cv::Mat>* face_tracker_;
  /** Face Landmarks provided by tracker */
  cv::Mat face_landmarks_;
  /** Face landmarks annotation */
  cv::Mat annotation_;
  /** Working directory */
  std::string images_dir_;
  /** Working image */
  cv::Mat working_img_;

  /** Mean error for all the images */
  double mean_rmse_;
  /** Mean error of all images  */
  std::vector<double> rmse_;
  /** Mean error for all images for all landmarks */
  std::vector<std::vector<double> > rmse_per_landmark_;
  /** Cumulative Error Distance */
  std::vector<std::vector<double> > cumul_error_dist_;
  /** Strcuture of AUC/CED parameters */
  struct CEDParam ced_param_;
  /** Timing */
  std::vector<double> timing_;
  /** Mean process time */
  double mean_timing_;
  /** Tracker type in string format */
  std::string tracker_type_str_;
};
}
#endif /* defined(__LTS5_Dev__benchmark__) */
