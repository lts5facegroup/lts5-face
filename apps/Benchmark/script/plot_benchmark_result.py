import argparse
import os
import matplotlib.pyplot as plt
import numpy as np
import itertools
__author__ = 'Christophe Ecabert'


def scan_folder(folder, expr):
    """
        List all files in a given folder with specific expression
    :param folder:      Folder to scan
    :param expr:        List of expression to look for
    :return:            List of folder
    """
    picked_file = []
    for root, dirs, files in os.walk(folder):
        # Loop over files
        for f in files:
            # has expr in it ?
            for e in expr:
                if e in f:
                    filename = root + '/' + f
                    picked_file.append(filename)
    picked_file.sort()
    return picked_file


def parse_result_file(list_result_files):
    ced_data = []
    rmse_data = [[], [], []]
    time_data = [[], [], []]
    labels = []
    for idx, res in enumerate(list_result_files):
        # Parse file
        with open(res, 'r') as f:
            # recover tracker name
            name = f.readline().strip()
            name = name[1:]
            cnt = 0
            num_str = ''
            while name + num_str in labels:
                num_str = str(cnt)
                cnt += 1
            name += num_str
            labels.append(name)
            # recover sample
            n_sample = float(f.readline().strip())
            # seek for line '#CED'
            while True:
                line = f.readline().strip()
                if not line:
                    break
                else:
                    if line == '#RMSE':
                        line = f.readline().strip()
                        rmse = 0
                        cnt = 0
                        pos = 0
                        while line != '#CED':
                            rmse += float(line)
                            cnt += 1
                            f.readline()
                            pos = f.tell()
                            line = f.readline().strip()
                        # rewind
                        f.seek(pos)
                        # Compute average
                        rmse /= cnt
                        # Add
                        rmse_data[0].append(name)
                        rmse_data[1].append(idx)
                        rmse_data[2].append(rmse)
                    elif line == '#TIME':
                        y_time = map(float, f.readline().strip().split(' '))
                        time = 0
                        for t in y_time:
                            time += t
                        time /= len(y_time)
                        time_data[0].append(name)
                        time_data[1].append(idx)
                        time_data[2].append(time)
                    elif line == '#CED':
                        x_data = map(float, f.readline().strip().split(' '))
                        y_data = map(float, f.readline().strip().split(' '))
                        # Normalize
                        y_data = map(lambda y: y/n_sample, y_data)
                        ced_data.append([name, x_data, y_data])
    return rmse_data, time_data, ced_data


def generate_rmse_plot(list_data, filename):
    # define output
    f, ext = os.path.splitext(filename)
    output = f + '_rmse' + ext
    # Create pyplot
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    n = 0
    plt.figure(1)
    ax = plt.subplot(111)
    for name, x, y in itertools.izip(list_data[0], list_data[1], list_data[2]):
        ax.bar(x, y, width=0.5, align='center', label=name, color=colors[n])
        n += 1
    # add legend
    plt.legend(list_data[0]) #,loc='lower right')
    # Remove tick on X
    plt.tick_params(
        axis='x',           # changes apply to the x-axis
        which='both',       # both major and minor ticks are affected
        bottom='off',       # ticks along the bottom edge are off
        top='off',          # ticks along the top edge are off
        labelbottom='off')  # labels along the bottom edge are off
    # Y/X label
    plt.title('Average Root Mean Square Error')
    plt.xlabel('Tracker')
    plt.ylabel('Average RMSE')
    # save image
    plt.savefig(output)


def generate_time_plot(list_data, filename):
    # define output
    f, ext = os.path.splitext(filename)
    output = f + '_time' + ext
    # Create pyplot
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    n = 0
    plt.figure(1)
    ax = plt.subplot(111)
    for name, x, y in itertools.izip(list_data[0], list_data[1], list_data[2]):
        ax.bar(x, y, width=0.5, align='center', label=name, color=colors[n])
        n += 1
    # add legend
    plt.legend(list_data[0]) #,loc='lower right')
    # Remove tick on X
    plt.tick_params(
        axis='x',           # changes apply to the x-axis
        which='both',       # both major and minor ticks are affected
        bottom='off',       # ticks along the bottom edge are off
        top='off',          # ticks along the top edge are off
        labelbottom='off')  # labels along the bottom edge are off
    # Y/X label
    plt.title('Average Alignment time')
    plt.xlabel('Tracker')
    plt.ylabel('Time [ms]')
    # save image
    plt.savefig(output)


def generate_ced_plot(list_data, filename):
    # define output
    f, ext = os.path.splitext(filename)
    output = f + '_ced' + ext
    # Create pyplot
    xmin = 0
    xmax = 0
    plt.figure(2)
    for data in list_data:
        x_data = data[1]
        y_data = data[2]
        plt.plot(x_data, y_data, '-',label=data[0])
        xmin = min(x_data)
        xmax = max(x_data)
    # change tick x
    plt.xticks(np.arange(xmin, xmax, 0.05))
    # add grid
    plt.grid(True)
    # add legend
    plt.legend(loc='lower right')
    # Y/X label
    plt.title('Cumulative Error Distribution')
    plt.xlabel('Error')
    plt.ylabel('Percentage of testset')
    # save image
    plt.savefig(output)


def main():
    parser = argparse.ArgumentParser(description='Generate plot from benchmark'
                                                 ' result files')
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='input',
                        help='Folder where result files are stored')
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='output',
                        help='Plot filename')

    args = parser.parse_args()
    # Scan input folder
    result = scan_folder(args.input, ['txt'])
    # Parse result
    rmse, time, ced = parse_result_file(result)
    # Plot
    generate_rmse_plot(rmse, filename=args.output)
    generate_time_plot(time, filename=args.output)
    generate_ced_plot(ced, filename=args.output)

if __name__ == '__main__':
    main()

