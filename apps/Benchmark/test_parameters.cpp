//
//  test_parameters.cpp
//  LTS5-Dev
//
//  Created by Gabriel Cuendet on 10.06.15.
//  Copyright (c) 2015 Cuendet Gabriel. All rights reserved.
//

#include <stdio.h>
#include "benchmark.hpp"
#include "lts5/face_tracker/sdm_trainer.hpp"
#include "lts5/face_tracker.hpp"

int main(int argc, const char * argv[])
{
  if (argc < 2) {
    std::cout << "Usage : " << argv[0] <<
    " images_dir_train images_dir_test temp_folder "  << std::endl;
    return -1;
  }

  std::vector<double> montecarlo_offsetX_std_scalings = {0.13958975, 0.27917949,
    0.41876924, 0.55835898, 0.69794873, 0.83753847, 0.97712822, 1.11671797,
    1.25630771, 1.39589746};
  std::vector<double> montecarlo_offsetY_std_scalings = {0.1384336, 0.27686719,
    0.41530079, 0.55373438, 0.69216798, 0.83060158, 0.96903517, 1.10746877,
    1.24590237, 1.38433596};
  std::vector<double> montecarlo_scale_std_scalings = {0.11370536, 0.22741072,
    0.34111609, 0.45482145, 0.56852681, 0.68223217, 0.79593753, 0.9096429,
    1.02334826, 1.13705362};

  std::vector<double> stds = {0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1};

  int offset_runs = static_cast<int>(montecarlo_offsetX_std_scalings.size());
  int scale_runs = static_cast<int>(montecarlo_scale_std_scalings.size());

  std::vector<double> results;

  std::string detector_filename = "models/haarcascade_frontalface_alt2.xml";
  std::string csv_filename = static_cast<std::string>(argv[3]) + "results.csv";
  std::ofstream out(csv_filename);
  out << "montecarlo XY std, montecarlo SCALE std, x factor, y factor, scale factor, mean rmse" << std::endl;

  std::unordered_set<int> indices;
  bool flip = false;
  LTS5::HeaderObjectType assessment_type = LTS5::HeaderObjectType::kConstTrackerAssessment;

  for (int xy_run = 0; xy_run < offset_runs; ++xy_run) {
    for (int s_run = 0; s_run < scale_runs; ++s_run) {
      // Train a new SDM
      LTS5::SdmTrainer* trainer = new LTS5::SdmTrainer(detector_filename);
      LTS5::SdmTracker::SdmTrackerParameters tracker_params;
      tracker_params.train_computation_mod = LTS5::kCula;
      tracker_params.montecarlo_offsetX_std_scaling = montecarlo_offsetX_std_scalings[xy_run];
      tracker_params.montecarlo_offsetY_std_scaling = montecarlo_offsetY_std_scalings[xy_run];
      tracker_params.montecarlo_scale_std_scaling = montecarlo_scale_std_scalings[s_run];
      trainer->set_training_parameters(tracker_params);
      // trainer->Train(argv[1]);
      trainer->Train(argv[1], flip, indices, assessment_type);
      std::string sdm_filename = static_cast<std::string>(argv[3]) + "model_run_" + std::to_string(scale_runs*xy_run + s_run) + ".bin";
      trainer->Save(sdm_filename);

      std::cout << "Benchmarking..." << std::endl;

      LTS5::Benchmark test;


      if (test.Load(LTS5::HeaderObjectType::kSdm,
                    sdm_filename,
                    detector_filename) != 0) {
        std::cerr << "Error when loading the model" << std::endl;
        return -1;
      }
      test.set_images_dir(argv[2]);
      test.Run();
      results.push_back(test.mean_rmse());

      out << stds[xy_run] << ","
          << stds[s_run] << ","
          << montecarlo_offsetX_std_scalings[xy_run] << ","
          << montecarlo_offsetY_std_scalings[xy_run] << ","
          << montecarlo_scale_std_scalings[s_run] << ","
          << test.mean_rmse() << std::endl;

      delete trainer;
    }
  }

  out.close();
  return 0;
}