/**
 *  @file FaceSynthesisPipelineWrapper.mm
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 06/08/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __FaceSynthesisPipelineWrapper_h__
#define __FaceSynthesisPipelineWrapper_h__

#import <Foundation/Foundation.h>

// Forward declaration
@class PipelineParameters;

/**
 *  @enum   FaceMeshSelection
 *  @brief  Possible mesh selection
 */
typedef enum MeshSelection {
  /** Reconstruct face shape */
  kModel,
  /** Synthetic input face shape */
  kSynthetic,
  /** Mean shape */
  kMeanShape,
  kDebug
} MeshSelection;

/**
 *  @enum   ProjectionModel
 *  @brief  Possible projection model
 */
typedef enum ProjectionModel {
  /** Pinhole model */
  kFullPerspective,
  /** Weak perspective */
  kWeakPerspective
} ProjectionModel;

@interface FaceSynthesisPipelineWrapper : NSObject {
  // Public members
}

#pragma mark -
#pragma mark Initialization

/**
 *  @name initWithParameters
 *  @fn +(void) FaceSynthesisPipelineWrapper
 *  @brief  Constructor
 *  @param[in]
 */
-(instancetype) initWithParameters : (PipelineParameters*) pipeline_configuration;

/**
 *  @name nativeRelease
 *  @fn -(void) nativeRelease
 *  @brief  Destructor
 */
-(void) nativeRelease;

#pragma mark -
#pragma mark Process

/**
 *  @name GenerateSyntheticData
 *  @fn -(void) GenerateSyntheticData
 *  @brief  Generate a new synthetic input
 */
-(void) GenerateSyntheticData;

/**
 *  @name Process
 *  @fn -(void) Process
 *  @brief  Launch pipeline to reconstruct 3D face
 */
-(void) Process;

/**
 *  @name ResetTrackingState
 *  @fn void ResetTrackingState(void)
 *  @brief  Reinitialize tracking state
 */
-(void) ResetTrackingState;

/**
 *  @name GenerateSyntheticSequenceData
 *  @fn -(void) GenerateSyntheticSequenceData
 *  @brief  Refit model and update view for sequence
 */
-(void) GenerateSyntheticSequenceData;


#pragma mark -
#pragma mark Accessors

/**
 *  @name   setNumberCamera
 *  @fn -(void) setNumberCamera : (const NSInteger) number_camera
 *  @brief  Select how many cameras are used for reconstruction
 *  @param[in] number_camera      Number of camera
 */
-(void) setNumberCamera : (const NSInteger) number_camera;

/**
 *  @name   getNumberCamera
 *  @fn -(NSInteger) getNumberCamera
 *  @brief  Indicate how many camera are used to reconstruct
 *  @param[in] number_camera      Number of camera
 */
-(NSInteger) getNumberCamera;

/**
 *  @name   setFrameToProcess
 *  @fn -(void) setFrameToProcess : (const NSInteger) frame_index
 *  @brief  Indicate from which frame to process
 *  @param[in]  frame_index   Frame's index
 */
-(void) setFrameToProcess : (const NSInteger) frame_index;

/**
 *  @name   getFrameToProcess
 *  @fn -(NSInteger) getFrameToProcess
 *  @brief  Provide from which frame to process
 *  @return Frame index
 */
-(NSInteger) getFrameToProcess;

/**
 *  @name   get_mesh
 *  @fn -(void*) getMesh : (MeshSelection) selection
 *  @brief  Return buffer holding one specific mesh
 *  @param  selection      Mesh selection
 */
-(void*) getMesh : (MeshSelection) selection;

/**
 *  @name   getCurrentFrameIndex
 *  @fn -(NSInteger) getCurrentFrameIndex
 *  @brief  Provide the index of the current frame being processed
 *  @return Frame index
 */
-(NSInteger) getCurrentFrameIndex;

/**
 *  @name   getTotalNumberOfFrame
 *  @fn -(NSInteger) getTotalNumberOfFrame
 *  @brief  Gives the number of image to process
 *  @return Number of frame
 */
-(NSInteger) getTotalNumberOfFrame;

/**
 *  @name getSyntheticIdentity
 *  @fn -(NSInteger) getSyntheticIdentity
 *  @brief  Provide the index of current ID.
 */
-(NSInteger) getSyntheticIdentity;

/**
 *  @name setSyntheticIdentity
 *  @fn -(void) setSyntheticIdentity:(NSInteger) identity
 *  @param[in]    identity  Selected identity
 */
-(void) setSyntheticIdentity:(NSInteger) identity;

/**
 *  @name getSyntheticExpression
 *  @fn -(NSInteger) getSyntheticExpression
 *  @brief  Provide the index of current EXPR.
 */
-(NSInteger) getSyntheticExpression;

/**
 *  @name setSyntheticExpression
 *  @fn -(void) setSyntheticExpression:(NSInteger) expression
 *  @param[in]    expression  Selected expression
 */
-(void) setSyntheticExpression:(NSInteger) expression;

/**
 *  @name getIdentityRegularization
 *  @fn -(float) getIdentityRegularization
 *  @brief  Provide current regularization factor for Identity
 *  @return Regularization factor
 */
-(float) getIdentityRegularization;

/**
 *  @name setIdentityRegularization
 *  @fn -(void) setIdentityRegularization: (float) reg_id
 *  @brief  Set regularization factor for Identity
 *  @param[in]  reg_id  Regularization factor
 */
-(void) setIdentityRegularization: (float) reg_id;

/**
 *  @name getExpressionRegularization
 *  @fn -(float) getExpressionRegularization
 *  @brief  Provide current regularization factor for Expression
 *  @return Regularization factor
 */
-(float) getExpressionRegularization;

/**
 *  @name setExpressionRegularization
 *  @fn -(void) setExpressionRegularization: (float) reg_expr
 *  @brief  Set regularization factor for Expression
 *  @param[in]  reg_expr  Regularization factor
 */
-(void) setExpressionRegularization: (float) reg_expr;

/**
 *  @name getSyntheticGamma
 *  @fn -(float) getSyntheticGamma
 *  @brief  Provide the current synthetic gamma angle
 *  @return Gamma angle
 */
-(float) getSyntheticGamma;

/**
 *  @name setSyntheticGamma
 *  @fn -(void) setSyntheticGamma : (float)gamma_angle
 *  @brief  Set the current synthetic gamma angle
 *  @param[in]  gamma_angle   Gamma angle
 */
-(void) setSyntheticGamma : (float)gamma_angle;

/**
 *  @name getSyntheticTheta
 *  @fn -(float) getSyntheticTheta
 *  @brief  Provide the current synthetic Theta angle
 *  @return Theta angle
 */
-(float) getSyntheticTheta;

/**
 *  @name setSyntheticTheta
 *  @fn -(void) setSyntheticTheta : (float)theta_angle
 *  @brief  Set the current synthetic Theta angle
 *  @param[in]  theta_angle   Theta angle
 */
-(void) setSyntheticTheta : (float)theta_angle;

/**
 *  @name getSyntheticPhi
 *  @fn -(float) getSyntheticPhi
 *  @brief  Provide the current synthetic Phi angle
 *  @return Phi angle
 */
-(float) getSyntheticPhi;

/**
 *  @name setSyntheticPhi
 *  @fn -(void) setSyntheticPhi : (float)phi_angle
 *  @brief  Set the current synthetic Phi angle
 *  @param[in]  phi_angle   Phi angle
 */
-(void) setSyntheticPhi : (float)phi_angle;

/**
 *  @name getSyntheticDeltaX
 *  @fn -(float) getSyntheticDeltaX
 *  @brief  Provide the current synthetic delta X
 *  @return Delta X
 */
-(float) getSyntheticDeltaX;

/**
 *  @name setSyntheticDeltaX
 *  @fn -(void) setSyntheticDeltaX : (float)delta_x
 *  @brief  Set the current synthetic delta X
 *  @param[in] delta_x   Delta X
 */
-(void) setSyntheticDeltaX : (float)delta_x;

/**
 *  @name getSyntheticDeltaY
 *  @fn -(float) getSyntheticDeltaY
 *  @brief  Provide the current synthetic delta Y
 *  @return Delta Y
 */
-(float) getSyntheticDeltaY;

/**
 *  @name setSyntheticDeltaY
 *  @fn -(void) setSyntheticDeltaY : (float)delta_y
 *  @brief  Set the current synthetic delta Y
 *  @param[in] delta_y   Delta Y
 */
-(void) setSyntheticDeltaY : (float)delta_y;

/**
 *  @name getSyntheticDeltaZ
 *  @fn -(float) getSyntheticDeltaZ
 *  @brief  Provide the current synthetic delta Z
 *  @return Delta Z
 */
-(float) getSyntheticDeltaZ;

/**
 *  @name setSyntheticDeltaZ
 *  @fn -(void) setSyntheticDeltaZ : (float)delta_z
 *  @brief  Set the current synthetic delta Z
 *  @param[in] delta_z   Delta Z
 */
-(void) setSyntheticDeltaZ : (float)delta_z;

/**
 *  @name getSyntheticScale
 *  @fn -(float) getSyntheticScale
 *  @brief  Provide the current synthetic scale
 *  @return Scale
 */
-(float) getSyntheticScale;

/**
 *  @name getSyntheticScale
 *  @fn -(void) setSyntheticScale:(float)scale
 *  @brief  Set the current synthetic scale
 *  @param[in]  scale Scale
 */
-(void) setSyntheticScale:(float)scale;

/**
 *  @name getSyntheticNoiseSTD
 *  @fn -(float) getSyntheticNoiseSTD
 *  @brief  Provide the current synthetic noise std
 *  @return Gaussian noise standard deviation
 */
-(float) getSyntheticNoiseSTD;

/**
 *  @name setSyntheticNoiseSTD
 *  @fn -(void) setSyntheticNoiseSTD:(float)noise_std;
 *  @brief  Set the current synthetic noise std
 *  @param[in] noise_std  Gaussian noise standard deviation
 */
-(void) setSyntheticNoiseSTD:(float)noise_std;

/**
 *  @name getGamma
 *  @fn -(float) getGamma
 *  @brief  Provide the current gamma angle
 *  @return Gamma angle
 */
-(float) getGamma;

/**
 *  @name getTheta
 *  @fn -(float) getTheta
 *  @brief  Provide the current Theta angle
 *  @return Theta angle
 */
-(float) getTheta;

/**
 *  @name getPhi
 *  @fn -(float) getPhi
 *  @brief  Provide the current Phi angle
 *  @return Phi angle
 */
-(float) getPhi;

/**
 *  @name getDeltaX
 *  @fn -(float) getDeltaX
 *  @brief  Provide the current delta X
 *  @return Delta X
 */
-(float) getDeltaX;

/**
 *  @name getDeltaY
 *  @fn -(float) getDeltaY
 *  @brief  Provide the current delta Y
 *  @return Delta Y
 */
-(float) getDeltaY;

/**
 *  @name getDeltaZ
 *  @fn -(float) getDeltaZ
 *  @brief  Provide the current delta Z
 *  @return Delta Z
 */
-(float) getDeltaZ;

/**
 *  @name getScale
 *  @fn -(float) getScale
 *  @brief  Provide the current scale
 *  @return Scale
 */
-(float) getScale;

/**
 *  @name setIsSyntheticData
 *  @fn -(void) setIsSyntheticData: (BOOL) is_synthetic
 *  @brief  Define the type of data used during reconstruction (synth/real)
 *  @param[in]  is_synthetic  True if synthetic, false otherwise
 */
-(void) setIsSyntheticData: (BOOL) is_synthetic;

/**
 *  @name isSyntheticData
 *  @fn -(BOOL) isSyntheticData
 *  @brief  Get the type of data used during reconstruction (synth/real)
 *  @return True if synthetic, false otherwise
 */
-(BOOL) isSyntheticData;

/**
 *  @name getProjectionModel
 *  @fn -(ProjectionModel) getProjectionModel
 *  @brief  Provide the type of projection used to reconstruct mesh
 *  @return Projection model enumarate
 */
-(ProjectionModel) getProjectionModel;

@end

#endif
