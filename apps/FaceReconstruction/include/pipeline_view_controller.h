/**
 *  @file PipelineViewController.h
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 11/08/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */


#import <Cocoa/Cocoa.h>

@class FaceSynthesisPipelineWrapper;
@class OGLView;

@interface PipelineViewController : NSViewController<NSTextFieldDelegate> {
  /** Box holding shape related controls */
  __weak IBOutlet NSBox* shape_container;
  /** Rigid transform related controls */
  __weak IBOutlet NSBox* transformation_container;

  // Synthetic shape controls
  /** View holding elements related to synthetic shape */
  __weak IBOutlet NSView* shape_selector;
  /** Identity selection */
  __weak IBOutlet NSTextField* identity_selector;
  /** Expression selection indicator */
  __weak IBOutlet NSTextField* expression_selected_viewer;
  /** Camera selector */
  __weak IBOutlet NSTextField* camera_selector;
  /** Expression selector */
  __weak IBOutlet NSSlider* expression_selector;
  
  // Real shape controls
  __weak IBOutlet NSView* real_shape_control;
  __weak IBOutlet NSTextField*  total_frame_viewer;
  __weak IBOutlet NSTextField*  frame_to_process_selector;
  __weak IBOutlet NSButton* process_all_selector;

  // Synthetic rigid transform + fitter param
  /** View holding synthetic rigid transform */
  __weak IBOutlet NSView* synthetic_static_transform_selector;
  /** Gamma slider */
  __weak IBOutlet NSSlider* gamma_slider;
  /** Thtea slider */
  __weak IBOutlet NSSlider* theta_slider;
  /** Phi slider */
  __weak IBOutlet NSSlider* phi_slider;
  /** Gamma indicator */
  __weak IBOutlet NSTextField* gamma_viewer;
  /** Theta indicator */
  __weak IBOutlet NSTextField* theta_viewer;
  /** Phi indicator */
  __weak IBOutlet NSTextField* phi_viewer;
  /** delta x indicator */
  __weak IBOutlet NSTextField* tx_viewer;
  /** delta y indicator */
  __weak IBOutlet NSTextField* ty_viewer;
  /** delta z indicator */
  __weak IBOutlet NSTextField* tz_viewer;
  /** scale selector */
  __weak IBOutlet NSTextField* s_selector;
  /** Amount of noise selector */
  __weak IBOutlet NSTextField* noise_selector;
  /** Id regularization selector */
  __weak IBOutlet NSTextField* etaId_selector;
  /** Expr regularization selector */
  __weak IBOutlet NSTextField* etaExpr_selector;

  // Real transform
  __weak IBOutlet NSView* real_data_transform_selector;
  /** Id reg for real data */
  __weak IBOutlet NSTextField *eta_real_id_selector;
  /** Expr reg for real data */
  __weak IBOutlet NSTextField *eta_real_expr_selector;

  // Recovered parameters
  /** Recovered gamma angle */
  __weak IBOutlet NSTextField* computed_gamma_viewer;
  /** Recovered theta angle */
  __weak IBOutlet NSTextField* computed_theta_viewer;
  /** Recovered phi angle */
  __weak IBOutlet NSTextField* computed_phi_viewer;
  /** Recovered scale */
  __weak IBOutlet NSTextField* computed_scale_viewer;
  /** Recovered delta x */
  __weak IBOutlet NSTextField* computed_delta_x_viewer;
  /** Recovered delta y */
  __weak IBOutlet NSTextField* computed_delta_y_viewer;
  /** Recovered delta z */
  __weak IBOutlet NSTextField* computed_delta_z_viewer;

  // VTK Stuff
  /** Synthetic 3D view */
  __weak IBOutlet OGLView* synthetic_view;
  /** Reconstructed 3D view */
  __weak IBOutlet OGLView* model_view;
}


// Properties

// Methods
#pragma mark -
#pragma mark IBAction 

/**
 *  @name slider_changed
 *  @fn - (IBAction)slider_changed:(NSSlider*)sender
 *  @brief  Callback function for slider changes
 *  @param[in]  sender  Slider begin updated
 */
- (IBAction)slider_changed:(NSSlider*)sender;

/**
 *  @name swicth_synthetic_real_data
 *  @fn - (IBAction)swicth_synthetic_real_data:(NSButton *)sender
 *  @brief  Callback when switching between Synthetic/Real data
 *  @param[in]  sender  Button begin switched
 */
- (IBAction)swicth_synthetic_real_data:(NSButton *)sender;

/**
 *  @name clear_transformation
 *  @fn - (IBAction)clear_transformation:(NSButton *)sender
 *  @brief  Callback where transformation is initialiazed to zero
 *  @param[in]  sender
 */
- (IBAction)clear_transformation:(NSButton *)sender;

/**
 *  @name process
 *  @fn - (IBAction)process:(NSButton *)sender
 *  @brief  Callback where processing is launched
 *  @param[in]  sender
 */
- (IBAction)process:(NSButton *)sender;

/**
 *  @name select_config_file
 *  @fn - (IBAction)select_config_file:(NSMenuItem *)sender
 *  @brief  Callback to select configuration file from menu
 *  @param[in]  sender  Entry being clicked
 */
- (IBAction)select_config_file:(NSMenuItem *)sender;

@end
