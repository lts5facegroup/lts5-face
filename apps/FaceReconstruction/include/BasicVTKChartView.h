/**
 *  @file   BasicVTKChartView.h
 *  Code
 *
 *  Created by Christophe Ecabert on 02/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#import <Cocoa/Cocoa.h>

#include "vtkCocoaGLView.h"
#include "vtkRenderer.h"
#include "vtkContextView.h"
#include "vtkChartXY.h"

/**
 *  @class  BasicVTKChartView
 *  @brief  Object representing basic elements of the vtkView for Cocoa interface for chart
 *          Based on : "vtk_root"/Examples/GUI/Cocoa
 *  @author -
 *  @date   02/03/14.
 */
@interface BasicVTKChartView : vtkCocoaGLView
{
  @private
  /** vtkRender used to display objects */
  vtkRenderer* renderer;
  /** vtkContext view used for chart printing */
  vtkContextView* contextView;
}

/**
 *  @name   initializeVTKChartSupport
 *  @brief  Initialize elements need by VTK framework to render object inside Cocoa view
 */
- (void)initializeVTKChartSupport;

/**
 *  @name   cleanUpVTKChartSupport
 *  @brief  Clean up elements used by the view
 */
- (void)cleanUpVTKChartSupport;

/**
 *  @name   getRenderer
 *  @brief  Return current renderer
 */
- (vtkRenderer*)getRenderer;

/**
 *  @name   setRenderer
 *  @brief  Set a new renderer
 *  @param  theRenderer     New renderer
 */
- (void)setRenderer:(vtkRenderer*)theRenderer;

/**
 *  @name   setChart
 *  @brief  Add chart to the view
 *  @param  theChart    Chart to add
 */
-(void) setChart : (vtkChartXY*)theChart;

/**
 *  @name   removeChart
 *  @brief  Remove chart inside view
 */
-(void) removeChart : (vtkChartXY*) aChart;

@end
