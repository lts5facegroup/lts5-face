/**
 *  @file   renderer_wrapper.h
 *  @brief  Wrapper from OpenGL Renderer
 *          HAs one LTS5::Mesh as input and holds OGLCallback instance for
 *          rendering
 *
 *  @author Christophe Ecabert
 *  @date   30/08/16
 *    Copyright © 2016 Ecabert Christophe. All rights reserved.
 */

#import <Foundation/Foundation.h>
#import "callback_wrapper.h"

/**
 *  @class  OGLRendererWrapper
 *  @brief  Wrapper from OpenGL Renderer
 *  @author Christophe Ecabert
 *  @date   30/08/16
 */
@interface OGLRendererWrapper : NSObject

#pragma mark -
#pragma mark Initialization

/**
 *  @name initWithWidth
 *  @brief  Constructor
 */
-(id) initWithWidth:(NSInteger) width andHeight:(NSInteger) height;

#pragma mark -
#pragma mark Usage

/**
 *  @name nativeRelease
 *  @brief  Destructor
 */
-(void) nativeRelease;

/**
 *  @name updateMesh
 *  @brief  Update mesh, push data onto GPU
 */
-(void) updateMesh:(void*)mesh;

#pragma mark -
#pragma mark Accessors

/**
 *  @name get_callback
 *  @brief  Provide callback to invoke when event occurs
 */
-(OGLCallbacksWrapper*) get_callbacks;

@end
