/**
 *  @file   ogl_view.h
 *  @brief  Custom Cocoa OpenGL view
 *
 *  @author Christophe Ecabert
 *  @date   31/07/16
 *  Copyright © 2016 Christophe Ecabert. All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import "callback_wrapper.h"

@interface OGLView : NSOpenGLView

#pragma mark -
#pragma mark Initialization

/**
 *  @name nativeRelease
 *  @brief  Release native renderer
 */
-(void) nativeRelease;

#pragma mark -
#pragma mark Usage

/**
 *  @name updateMesh
 *  @brief  Update mesh, push data onto GPU
 */
-(void) updateMesh:(void*)mesh;

/**
 *  @name addCallbacks
 *  @brief  Add callback for this view
 *  @param[in]  callback  Wrapper for OGLCallbacks interface
 */
-(void) addCallbacks:(OGLCallbacksWrapper*) callback;

@end
