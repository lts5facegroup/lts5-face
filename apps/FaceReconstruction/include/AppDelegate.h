/**
 *  @file AppDelegate.h
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 06/08/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */


#import <Cocoa/Cocoa.h>

#import "pipeline_view_controller.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@end

