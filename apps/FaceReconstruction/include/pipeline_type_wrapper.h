/**
 *  @file PipelineTypeWrapper.h
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 06/08/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_PipelineTypeWrapper_h__
#define __LTS5_PipelineTypeWrapper_h__

#import <Foundation/Foundation.h>

#pragma mark -
#pragma mark Pipeline parameters
/**
 *  @interface PipelineParameters
 *  @brief  Objective-C wrapper for FaceSynthesisPipeline::FaceSynthesisPipelineParameters
 *  @author Christophe Ecabert
 *  @date   07/08/15
 */
@interface PipelineParameters : NSObject {
  @public
  /** Config file for the whole pipeline */
  NSString* pipeline_xml_config_file;
  /** List of vertex to use to compute error when working with synthetic data */
  NSString* error_vertex_file;
  /** Working directory */
  NSString* working_directory;
}

@end

#pragma mark -
#pragma mark Processing parameters

/**
 *  @interface  PipelineProcessConfiguration
 *  @brief  Objective-C wrapper for FaceSynthesisPipeline::PipelineProcessConfiguration
 *  @author Christophe Ecabert
 *  @date   07/08/15
 */
@interface PipelineProcessConfiguration : NSObject {
  @public
  /** Use real or fake data */
  BOOL has_synthetic_data;
  /** Whether or not to draw landmarks */
  BOOL draw_landmarks;
}

@end


#endif
