/**
 *  @file   VTKMeshView.h
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 11/08/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */


#import <Foundation/Foundation.h>
#import "vtkCocoaGLView.h"

// Forward declaration
//@class BasicVTKView;

/**
 *  @class  VTKMeshView
 *  @brief  Object representing basic elements of the vtkView for Cocoa interface
 *          Based on : "vtk_root"/Examples/GUI/Cocoa
 *  @author -
 *  @date   02/03/14.
 *  @updated 11/08/15
 */
@interface VTKMeshView : vtkCocoaGLView {
}

#pragma mark -
#pragma mark Initialization

/**
 *  @name initWithMeshData
 *  @fn -(instancetype) initWithMeshData : (void*) mesh
 *  @brief  Constructor
 *  @param[in]  mesh  3D mesh to display (vtkPolyData*)
 */
-(instancetype) initWithMeshData : (void*) mesh;

/**
 *  @name nativeRelease
 *  @fn -(void) nativeRelease
 *  @brief  Destructor
 */
-(void) nativeRelease;

#pragma mark -
#pragma mark Process

/**
 *  @name update
 *  @fn -(void) update
 *  @brief  Update VTK Pipeline
 */
-(void) update;

/**
 *  @name RotateX
 *  @fn -(void) RotateX:(double)theta Y:(double) phi Z:(double) gamma
 *  @brief  Apply rotation on object
 *  @param[in]  gamma   Rotation along Z axis
 *  @param[in]  theta   Rotation along X axis
 *  @param[in]  phi     Rotation along Y axis
 */
-(void) RotateGamma:(double)gamma Theta:(double) theta Phi:(double) phi;

#pragma mark -
#pragma mark Accessors

/**
 *  @name setDataMesh
 *  @fn -(void) setDataMesh : (void*) data_mesh
 *  @brief  Set the mesh to render
 *  @param[in]  mesh 3D mesh to display (vtkPolyData*)
 */
-(void) setDataMesh : (void*) mesh;

-(void) setAmbientX: (double) x Y:(double) y Z:(double) z;

-(void) setLighting: (BOOL)state;

-(void) setLookUpTable:(void*)table;

/**
 *  @name   setRenderer
 *  @fn - (void)setRenderer:(void*)renderer
 *  @brief  Set a new renderer
 *  @param  new_renderer     New renderer
 */
- (void)setRenderer:(void*)new_renderer;

/**
 *  @name   getRenderer
 *  @fn - (void*)getRenderer
 *  @brief  Return current renderer (vtkRenderer) as void*
 *  @return (void) Pointer to current renderer
 */
- (void*)getRenderer;

/**
 *  @name setTextureMap
 *  @fn -(void) setTextureMap : (void*) texture_map;
 *  @brief  Add a texture map
 *  @param[in]  texture_map vtk texture map
 */
-(void) setTextureMap : (void*) texture_map;



@end
