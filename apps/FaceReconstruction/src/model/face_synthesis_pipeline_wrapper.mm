/**
 *  @file FaceSynthesisPipelineWrapper.mm
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 06/08/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#import <Foundation/Foundation.h>
#import "pipeline_type_wrapper.h"
#import "face_synthesis_pipeline_wrapper.h"

#import "lts5/face_reconstruction/face_synthesis_pipeline.hpp"
#import "lts5/face_reconstruction/data_generator.hpp"

@interface FaceSynthesisPipelineWrapper() {
  /** Reconstruction pipeline */
  LTS5::FaceSynthesisPipeline* face_pipeline;
  /** Pipeline parameters */
  LTS5::FaceSynthesisPipeline::FaceSynthesisPipelineParameters params;
  /** Processing configuration */
  LTS5::FaceSynthesisPipeline::PipelineProcessConfiguration process_configuration;
  /** Data generator */
  LTS5::DataGenerator* generator_;
  /** Data generator parameters */
  LTS5::DataGenerator::SyntheticDataParameter synthetic_parameter_;
}
@end

@implementation FaceSynthesisPipelineWrapper

#pragma mark -
#pragma mark Initialization

/*
 *  @name initWithParameters
 *  @fn +(void) FaceSynthesisPipelineWrapper
 *  @brief  Constructor
 *  @param[in]
 */
-(instancetype) initWithParameters : (PipelineParameters*) pipeline_configuration {
  // Do some stuff here
  self = [super init];
  if (self) {
    // Ok, init data structure
    params.pipeline_xml_config_file = std::string([pipeline_configuration->pipeline_xml_config_file UTF8String]);
    params.working_directory = std::string([pipeline_configuration->working_directory UTF8String]);
    // Create pipeline
    try {
      self->face_pipeline = new LTS5::FaceSynthesisPipeline(self->params);
      // Indicate it's using synthetic data, default behavior
      self->process_configuration.has_synthetic_data = true;
      // Create data generator
      generator_ = new LTS5::DataGenerator(params.pipeline_xml_config_file);
      // Default param
      synthetic_parameter_.gamma = 0.f;
      synthetic_parameter_.theta = 0.f;
      synthetic_parameter_.phi = 0.f;
      synthetic_parameter_.delta_x = 0.f;
      synthetic_parameter_.delta_y = 0.f;
      synthetic_parameter_.delta_z = -300.f;
      synthetic_parameter_.scale = 100.f;
      synthetic_parameter_.noise_std = 6.f;
      
      synthetic_parameter_.add_noise = (synthetic_parameter_.noise_std > 0.f);
    } catch (const LTS5::ProcessError& e) {
      // Rethrow exception to Objective-C Side
      NSString* msg = [[NSString alloc]initWithUTF8String:e.what()];
      @throw [NSException exceptionWithName:@"Unable to instantiate Native pipeline" reason:msg userInfo:nil];
    }
  }
  return self;
}

/*
 *  @name nativeRelease
 *  @fn -(void) nativeRelease
 *  @brief  Destructor
 */
-(void) nativeRelease {
  // Call destructor
  if (self->face_pipeline) {
    delete face_pipeline;
    face_pipeline = nullptr;
  }
  if (generator_) {
    delete generator_;
    generator_ = nullptr;
  }
}

#pragma mark -
#pragma mark Process

/*
 *  @name GenerateSyntheticData
 *  @fn -(void) GenerateSyntheticData
 *  @brief  Generate a new synthetic input
 */
-(void) GenerateSyntheticData {
  generator_->GenerateData(synthetic_parameter_);
  process_configuration.landmark = generator_->get_synthetic_landmarks();
}

/*
 *  @name Process
 *  @fn -(void) Process
 *  @brief  Launch pipeline to reconstruct 3D face
 */
-(void) Process {
  self->face_pipeline->Process(process_configuration);
}

/*
 *  @name ResetTrackingState
 *  @fn void ResetTrackingState(void)
 *  @brief  Reinitialize tracking state
 */
-(void) ResetTrackingState {
  self->face_pipeline->ResetTrackingState();
}

/*
 *  @name GenerateSyntheticSequenceData
 *  @fn -(void) GenerateSyntheticSequenceData
 *  @brief  Refit model and update view for sequence
 */
-(void) GenerateSyntheticSequenceData {
  //self->face_pipeline->UpdateSyntheticSequenceData();
}



#pragma mark -
#pragma mark Accessors

/**
 *  @name   setNumberCamera
 *  @fn -(void) setNumberCamera : (const NSInteger) number_camera
 *  @brief  Select how many cameras are used for reconstruction
 *  @param[in] number_camera      Number of camera
 */
-(void) setNumberCamera : (const NSInteger) number_camera {
  //self->face_pipeline->set_number_camera((int)number_camera);
}

/**
 *  @name   getNumberCamera
 *  @fn -(NSInteger) getNumberCamera
 *  @brief  Indicate how many camera are used to reconstruct
 *  @return  Number of camera
 */
-(NSInteger) getNumberCamera {
  return (NSInteger)self->face_pipeline->get_number_of_view();
}

/*
 *  @name   setFrameToProcess
 *  @fn -(void) setFrameToProcess : (const NSInteger) frame_index
 *  @brief  Indicate from which frame to process
 *  @param[in]  frame_index   Frame's index
 */
-(void) setFrameToProcess : (const NSInteger) frame_index {
  self->face_pipeline->set_frame_to_process((int)frame_index);
}

/*
 *  @name   getFrameToProcess
 *  @fn -(NSInteger) getFrameToProcess
 *  @brief  Provide from which frame to process
 *  @return Frame index
 */
-(NSInteger) getFrameToProcess {
  return self->face_pipeline->get_frame_to_process();
}

/*
 *  @name   getMesh
 *  @fn -(void*) getMesh : (MeshSelection*) selection
 *  @brief  Return buffer holding one specific mesh
 *  @param  selection      Mesh selection
 */
-(void*) getMesh : (MeshSelection) selection {
  switch (selection) {
    case kSynthetic:
      return (void*)generator_->get_mesh();
      
    case kModel:
      return (void*)face_pipeline->get_mesh();
      
    default:
      return nil;
  }
}

/*
 *  @name   getCurrentFrameIndex
 *  @fn -(NSInteger) getCurrentFrameIndex
 *  @brief  Provide the index of the current frame being processed
 *  @return Frame index
 */
-(NSInteger) getCurrentFrameIndex {
  return (NSInteger)self->face_pipeline->get_current_frame_index();
}

/*
 *  @name   getTotalNumberOfFrame
 *  @fn -(NSInteger) getTotalNumberOfFrame
 *  @brief  Gives the number of image to process
 *  @return Number of frame
 */
-(NSInteger) getTotalNumberOfFrame {
  return (NSInteger)self->face_pipeline->get_total_number_of_frame();
}

/*
 *  @name getSyntheticIdentity
 *  @fn -(NSInteger) getSyntheticIdentity
 *  @brief  Provide the index of current ID.
 */
-(NSInteger) getSyntheticIdentity {
  return synthetic_parameter_.id;
}

/*
 *  @name setSyntheticIdentity
 *  @fn -(void) setSyntheticIdentity:(NSInteger) identity
 *  @param[in]    identity  Selected identity
 */
-(void) setSyntheticIdentity:(NSInteger) identity {
  synthetic_parameter_.id = (int)identity;
}

/*
 *  @name getSyntheticExpression
 *  @fn -(NSInteger) getSyntheticExpression
 *  @brief  Provide the index of current EXPR.
 */
-(NSInteger) getSyntheticExpression {
  return synthetic_parameter_.expr;
}

/*
 *  @name setSyntheticExpression
 *  @fn -(void) setSyntheticExpression:(NSInteger) expression
 *  @param[in]    expression  Selected expression
 */
-(void) setSyntheticExpression:(NSInteger) expression {
  synthetic_parameter_.expr = (int)expression;
}

/*
 *  @name getIdentityRegularization
 *  @fn -(float) getIdentityRegularization
 *  @brief  Provide current regularization factor for Identity
 *  @return Regularization factor
 */
-(float) getIdentityRegularization {
  return self->face_pipeline->fit_parameter_.eta_identity;
}

/*
 *  @name setIdentityRegularization
 *  @fn -(void) setIdentityRegularization: (float) reg_id
 *  @brief  Set regularization factor for Identity
 *  @param[in]  reg_id  Regularization factor
 */
-(void) setIdentityRegularization: (float) reg_id {
  self->face_pipeline->fit_parameter_.eta_identity = reg_id;
}

/*
 *  @name getExpressionRegularization
 *  @fn -(float) getExpressionRegularization
 *  @brief  Provide current regularization factor for Expression
 *  @return Regularization factor
 */
-(float) getExpressionRegularization {
  return self->face_pipeline->fit_parameter_.eta_expression;
}

/*
 *  @name setExpressionRegularization
 *  @fn -(void) setExpressionRegularization: (float) reg_expr
 *  @brief  Set regularization factor for Expression
 *  @param[in]  reg_expr  Regularization factor
 */
-(void) setExpressionRegularization: (float) reg_expr {
  self->face_pipeline->fit_parameter_.eta_expression = reg_expr;
}

/*
 *  @name getSyntheticGamma
 *  @fn -(float) getSyntheticGamma
 *  @brief  Provide the current synthetic gamma angle
 *  @return Gamma angle
 */
-(float) getSyntheticGamma {
  return synthetic_parameter_.gamma;
}

/*
 *  @name setSyntheticGamma
 *  @fn -(void) setSyntheticGamma : (float)gamma_angle
 *  @brief  Set the current synthetic gamma angle
 *  @param[in]  gamma_angle   Gamma angle
 */
-(void) setSyntheticGamma : (float)gamma_angle {
  synthetic_parameter_.gamma = gamma_angle;
}

/*
 *  @name getSyntheticTheta
 *  @fn -(float) getSyntheticTheta
 *  @brief  Provide the current synthetic Theta angle
 *  @return Theta angle
 */
-(float) getSyntheticTheta {
  return synthetic_parameter_.theta;
}

/*
 *  @name setSyntheticTheta
 *  @fn -(void) setSyntheticTheta : (float)theta_angle
 *  @brief  Set the current synthetic Theta angle
 *  @param[in]  theta_angle   Theta angle
 */
-(void) setSyntheticTheta : (float)theta_angle {
  synthetic_parameter_.theta = theta_angle;
}

/*
 *  @name getSyntheticPhi
 *  @fn -(float) getSyntheticPhi
 *  @brief  Provide the current synthetic Phi angle
 *  @return Phi angle
 */
-(float) getSyntheticPhi {
  return synthetic_parameter_.phi;
}

/**
 *  @name setSyntheticPhi
 *  @fn -(void) setSyntheticPhi : (float)phi_angle
 *  @brief  Set the current synthetic Phi angle
 *  @param[in]  phi_angle   Phi angle
 */
-(void) setSyntheticPhi : (float)phi_angle {
  synthetic_parameter_.phi = phi_angle;
}

/*
 *  @name getSyntheticDeltaX
 *  @fn -(float) getSyntheticDeltaX
 *  @brief  Provide the current synthetic delta X
 *  @return Delta X
 */
-(float) getSyntheticDeltaX {
  return synthetic_parameter_.delta_x;
}

/**
 *  @name setSyntheticDeltaX
 *  @fn -(void) setSyntheticDeltaX : (float)delta_x
 *  @brief  Set the current synthetic delta X
 *  @param[in] delta_x   Delta X
 */
-(void) setSyntheticDeltaX : (float)delta_x {
  synthetic_parameter_.delta_x = delta_x;
}

/*
 *  @name getSyntheticDeltaY
 *  @fn -(float) getSyntheticDeltaY
 *  @brief  Provide the current synthetic delta Y
 *  @return Delta Y
 */
-(float) getSyntheticDeltaY {
  return synthetic_parameter_.delta_y;
}

/*
 *  @name setSyntheticDeltaY
 *  @fn -(void) setSyntheticDeltaY : (float)delta_y
 *  @brief  Set the current synthetic delta Y
 *  @param[in] delta_y   Delta Y
 */
-(void) setSyntheticDeltaY : (float)delta_y {
  synthetic_parameter_.delta_y = delta_y;
}

/*
 *  @name getSyntheticDeltaZ
 *  @fn -(float) getSyntheticDeltaZ
 *  @brief  Provide the current synthetic delta Z
 *  @return Delta Z
 */
-(float) getSyntheticDeltaZ {
  return synthetic_parameter_.delta_z;
}

/*
 *  @name setSyntheticDeltaZ
 *  @fn -(void) setSyntheticDeltaZ : (float)delta_z
 *  @brief  Set the current synthetic delta Z
 *  @param[in] delta_z   Delta Z
 */
-(void) setSyntheticDeltaZ : (float)delta_z {
  synthetic_parameter_.delta_z = delta_z;
}

/*
 *  @name getSyntheticScale
 *  @fn -(float) getSyntheticScale
 *  @brief  Provide the current synthetic scale
 *  @return Scale
 */
-(float) getSyntheticScale {
  return synthetic_parameter_.scale;
}

/*
 *  @name getSyntheticScale
 *  @fn -(void) setSyntheticScale:(float)scale
 *  @brief  Set the current synthetic scale
 *  @param[in]  scale Scale
 */
-(void) setSyntheticScale:(float)scale {
  synthetic_parameter_.scale = scale;
}

/*
 *  @name getSyntheticNoiseSTD
 *  @fn -(float) getSyntheticNoiseSTD
 *  @brief  Provide the current synthetic noise std
 *  @return Gaussian noise standard deviation
 */
-(float) getSyntheticNoiseSTD {
  return synthetic_parameter_.noise_std;
}

/*
 *  @name setSyntheticNoiseSTD
 *  @fn -(void) setSyntheticNoiseSTD:(float)noise_std;
 *  @brief  Set the current synthetic noise std
 *  @param[in] noise_std  Gaussian noise standard deviation
 */
-(void) setSyntheticNoiseSTD:(float)noise_std {
  synthetic_parameter_.noise_std = noise_std;
  synthetic_parameter_.add_noise = (noise_std > 0.f);
}

/*
 *  @name getGamma
 *  @fn -(float) getGamma
 *  @brief  Provide the current gamma angle
 *  @return Gamma angle
 */
-(float) getGamma {
  return self->face_pipeline->fit_result_.gamma;
}

/*
 *  @name getTheta
 *  @fn -(float) getTheta
 *  @brief  Provide the current Theta angle
 *  @return Theta angle
 */
-(float) getTheta {
  return self->face_pipeline->fit_result_.theta;
}

/*
 *  @name getPhi
 *  @fn -(float) getPhi
 *  @brief  Provide the current Phi angle
 *  @return Phi angle
 */
-(float) getPhi {
  return self->face_pipeline->fit_result_.phi;
}

/*
 *  @name getDeltaX
 *  @fn -(float) getDeltaX
 *  @brief  Provide the current delta X
 *  @return Delta X
 */
-(float) getDeltaX {
  return self->face_pipeline->fit_result_.delta_x;
}

/*
 *  @name getDeltaY
 *  @fn -(float) getDeltaY
 *  @brief  Provide the current delta Y
 *  @return Delta Y
 */
-(float) getDeltaY {
  return self->face_pipeline->fit_result_.delta_y;
}

/*
 *  @name getDeltaZ
 *  @fn -(float) getDeltaZ
 *  @brief  Provide the current delta Z
 *  @return Delta Z
 */
-(float) getDeltaZ {
  return self->face_pipeline->fit_result_.delta_z;
}

/*
 *  @name getScale
 *  @fn -(float) getScale
 *  @brief  Provide the current scale
 *  @return Scale
 */
-(float) getScale {
  return self->face_pipeline->fit_result_.scale;
}

/*
 *  @name setIsSyntheticData
 *  @fn -(void) setIsSyntheticData: (BOOL) is_synthetic
 *  @brief  Define the type of data used during reconstruction (synth/real)
 *  @param[in]  is_synthetic  True if synthetic, false otherwise
 */
-(void) setIsSyntheticData: (BOOL) is_synthetic {
  process_configuration.has_synthetic_data = (bool)is_synthetic;
}

/*
 *  @name isSyntheticData
 *  @fn -(BOOL) isSyntheticData
 *  @brief  Get the type of data used during reconstruction (synth/real)
 *  @return True if synthetic, false otherwise
 */
-(BOOL) isSyntheticData  {
  return (BOOL) process_configuration.has_synthetic_data;
}

/*
 *  @name getProjectionModel
 *  @fn -(ProjectionModel) getProjectionModel
 *  @brief  Provide the type of projection used to reconstruct mesh
 *  @return Projection model enumarate
 */
-(ProjectionModel) getProjectionModel {
  return (ProjectionModel) self->face_pipeline->get_projection_model();
}

@end
