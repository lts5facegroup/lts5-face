//
//  PipelineTypeWrapper.m
//  LTS5-Apps
//
//  Created by Christophe Ecabert on 07/08/15.
//  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "pipeline_type_wrapper.h"


#pragma mark -
#pragma mark Pipeline parameters

@implementation PipelineParameters
@end

#pragma mark -
#pragma mark Processing parameters

@implementation PipelineProcessConfiguration
@end
