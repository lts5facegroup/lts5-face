/**
 *  @file   BasicVTKChartView.mm
 *  Code
 *
 *  Created by Christophe Ecabert on 02/03/14.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#import "BasicVTKChartView.h"

#import "vtkRenderer.h"
#import "vtkRenderWindow.h"
#import "vtkRenderWindowInteractor.h"
#import "vtkCocoaRenderWindowInteractor.h"
#import "vtkCocoaRenderWindow.h"
#import "vtkContextScene.h"

@implementation BasicVTKChartView

/**
 *  @name   initWithFrame
 *  @brief  Initialize new view
 *  @param  frame       Frame where to place the view
 *  @return New View
 */
- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self)
    {
        //  nothing to do... add something if you need to
    }
    return self;
}

/**
 *  @name   dealloc
 *  @brief  Clean up function, release memory
 */
- (void)dealloc
{
    [self cleanUpVTKChartSupport];
#if !__has_feature(objc_arc)
    [super dealloc];
#endif
}

/**
 *  @name   drawRect
 *  @brief  We are going to over ride the super class here to do some last minute
 *          setups. We need to do this because if we initialize in the constructor or
 *          even later, in say an NSDocument's windowControllerDidLoadNib, then
 *          we will get a warning about "Invalid Drawable" because the OpenGL Context
 *          is trying to be set and rendered into an NSView that most likely is not
 *          on screen yet. This is a way to defer that initialization until the NSWindow
 *          that contains our NSView subclass is actually on screen and ready to be drawn.
 */
- (void)drawRect:(NSRect)theRect
{
    //  Check for a valid vtkWindowInteractor and then initialize it. Technically we
    //  do not need to do this, but what happens is that the window that contains
    //  this object will not immediately render it so you end up with a big empty
    //  space in your gui where this NSView subclass should be. This may or may
    //  not be what is wanted. If you allow this code then what you end up with is the
    //  typical empty black OpenGL view which seems more 'correct' or at least is
    //  more soothing to the eye.
    vtkRenderWindowInteractor* theRenWinInt = [self getInteractor];
    if (theRenWinInt && (theRenWinInt->GetInitialized() == NO))
    {
        theRenWinInt->Initialize();
    }
    
    // Let the vtkCocoaGLVIew do its regular drawing
    [super drawRect:theRect];
}

/**
 *  @name   initializeVTKChartSupport
 *  @brief  Initialize elements need by VTK framework to render object inside Cocoa view
 */
- (void)initializeVTKChartSupport {
    //  The usual vtk object creation
    contextView = vtkContextView::New();
    
    vtkRenderer* renderer = contextView->GetRenderer();
    vtkRenderWindow* render_window = contextView->GetRenderWindow();
    vtkRenderWindowInteractor* render_window_interactor = contextView->GetInteractor();
    
    //  The cast should never fail, but we do it anyway, as
    //  it's more correct to do so.
    vtkCocoaRenderWindow* cocoa_window = vtkCocoaRenderWindow::SafeDownCast(render_window);
    
    if (renderer && cocoa_window && render_window_interactor) {
        //  This is special to our usage of vtk.  To prevent vtk
        //  from creating an NSWindow and NSView automatically (its
        //  default behaviour) we tell vtk that they exist already.
        //  The APIs names are a bit misleading, due to the cross
        //  platform nature of vtk, but this usage is correct.
        cocoa_window->SetRootWindow((__bridge void*)[self window]);
        cocoa_window->SetWindowId((__bridge void*)self);
        
        //  The usual vtk connections
        cocoa_window->AddRenderer(renderer);
        render_window_interactor->SetRenderWindow(cocoa_window);
        
        //  This is special to our usage of vtk.  vtkCocoaGLView
        //  keeps track of the renderWindow, and has a get
        //  accessor if you ever need it.
        [self setVTKRenderWindow:cocoa_window];
        
        //  Likewise, BasicVTKView keeps track of the renderer
        [self setRenderer:renderer];
    }
}

/**
 *  @name   cleanUpVTKChartSupport
 *  @brief  Clean up elements used by the view
 */
- (void)cleanUpVTKChartSupport {
    // Get object
    if(contextView) {
        contextView->Delete();
        contextView = NULL;
    }
    
    // Indicate to the view that those elements are not any more present
    [self setRenderer:NULL];
    [self setVTKRenderWindow:NULL];
    
    //  There is no setter accessor for the render window
    //  interactor, that's ok.
}

/**
 *  @name   getRenderer
 *  @brief  Return current renderer
 */
- (vtkRenderer*)getRenderer {
    return renderer;
}

/**
 *  @name   setRenderer
 *  @brief  Set a new renderer
 *  @param  theRenderer     New renderer
 */
- (void)setRenderer: (vtkRenderer*) theRenderer {
    renderer = theRenderer;
}

/**
 *  @name   setChart
 *  @brief  Add chart to the view
 *  @param  theChart    Chart to add
 */
-(void) setChart : (vtkChartXY*)theChart {
    contextView->GetScene()->AddItem(theChart);
}

/**
 *  @name   removeChart
 *  @brief  Remove chart inside view
 */
-(void) removeChart : (vtkChartXY*) aChart {
    contextView->GetScene()->RemoveItem(aChart);
}

@end
