/**
 *  @file   VTKMeshView.mm
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 11/08/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#import "VTKMeshView.h"

#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkCocoaRenderWindowInteractor.h"
#include "vtkCocoaRenderWindow.h"
#include "vtkLight.h"
#include "vtkProperty.h"
#include "vtkLookupTable.h"
#include "vtkScalarBarActor.h"

@interface VTKMeshView() {
  /** Mapper for vtk mesh */
  vtkPolyDataMapper* data_mapper;
  //vtkPainterPolyDataMapper* data_mapper;
  /** Actor, i.e. object */
  vtkActor* data_actor;
  /** OpenGL Renderer */
  vtkRenderer* data_renderer;
  /** 3D Mesh */
  vtkPolyData* data_mesh;
  /** Color bar actor */
  vtkScalarBarActor* data_bar_actor;
  /** Flag to indicate if vtk pipeline is initialized */
  BOOL pipeline_init;
}

@end

@implementation VTKMeshView

#pragma mark -
#pragma mark Initialization

/*
 *  @name   initWithFrame
 *  @brief  Initialize new view
 *  @param  frame       Frame where to place the view
 *  @return New View
 */
- (id)initWithFrame:(NSRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    //  nothing to do... add something if you need to
    pipeline_init = NO;
  }
  return self;
}

/*
 *  @name initWithMeshData
 *  @fn -(instancetype) initWithMeshData : (void*) mesh
 *  @brief  Constructor
 *  @param[in]  mesh  3D mesh to display (vtkPolyData*)
 */
-(instancetype) initWithMeshData : (void*) mesh {
  self = [super init];
  if (self) {
    // Init VTK Pipeline
    pipeline_init = NO;
    [self setDataMesh:mesh];
    [self initVTKPipelineSupport];
  }
  return self;
}

/*
 *  @name initVTKPipelineSupport
 *  @fn - (void) initVTKPipelineSupport
 *  @brief  Initialize VTK pipeline
 */
- (void) initVTKPipelineSupport {
  // Init view, i.e. Renderer + Window
  [self initializeVTKSupport];
  // Init mapper + actor
  self->data_mapper = vtkPolyDataMapper::New();
  self->data_actor = vtkActor::New();
  // Plug stuff together
  self->data_mapper->SetInputData(self->data_mesh);
  self->data_actor->SetMapper(self->data_mapper);
  ((vtkRenderer*)[self getRenderer])->AddActor(self->data_actor);
  // Init bar
  self->data_bar_actor = vtkScalarBarActor::New();
}

/*
 *  @name   cleanUpVTKSupport
 *  @fn - (void)cleanUpVTKSupport
 *  @brief  Clean up elements used by the view
 */
- (void)cleanUpVTKSupport {

  if (data_mapper) {
    data_mapper->Delete();
  }
  if (data_actor) {
    data_actor->Delete();
  }
  if (data_bar_actor) {
    data_bar_actor->Delete();
  }
  
  // Get object
  vtkRenderer* local_renderer = (vtkRenderer*)[self getRenderer];
  vtkRenderWindow* render_window = [self getVTKRenderWindow];
  vtkRenderWindowInteractor* render_window_interactor = [self getInteractor];

  // Start clean up
  if (local_renderer) {
    local_renderer->Delete();
  }
  if (render_window) {
    render_window->Delete();
  }
  if (render_window_interactor) {
    render_window_interactor->Delete();
  }
  // Indicate to the view that those elements are not any more present
  [self setRenderer : NULL];
  [self setVTKRenderWindow : NULL];
}

/**
 *  @name   drawRect
 *  @brief  We are going to over ride the super class here to do some last minute
 *          setups. We need to do this because if we initialize in the constructor or
 *          even later, in say an NSDocument's windowControllerDidLoadNib, then
 *          we will get a warning about "Invalid Drawable" because the OpenGL Context
 *          is trying to be set and rendered into an NSView that most likely is not
 *          on screen yet. This is a way to defer that initialization until the NSWindow
 *          that contains our NSView subclass is actually on screen and ready to be drawn.
 */
- (void)drawRect:(NSRect)theRect {
  //  Check for a valid vtkWindowInteractor and then initialize it. Technically we
  //  do not need to do this, but what happens is that the window that contains
  //  this object will not immediately render it so you end up with a big empty
  //  space in your gui where this NSView subclass should be. This may or may
  //  not be what is wanted. If you allow this code then what you end up with is the
  //  typical empty black OpenGL view which seems more 'correct' or at least is
  //  more soothing to the eye.
  vtkRenderWindowInteractor* theRenWinInt = [self getInteractor];
  if (theRenWinInt && (theRenWinInt->GetInitialized() == NO)) {
    theRenWinInt->Initialize();
  }
  // Let the vtkCocoaGLVIew do its regular drawing
  [super drawRect:theRect];
}

/*
 *  @name   initializeVTKSupport
 *  @brief  Initialize vtk windows system for rendering 3D object
 */
- (void)initializeVTKSupport {
  //  The usual vtk object creation
  vtkRenderer* local_renderer = vtkRenderer::New();
  vtkRenderWindow* render_window = vtkRenderWindow::New();
  vtkRenderWindowInteractor* render_window_interactor = vtkRenderWindowInteractor::New();

  //  The cast should never fail, but we do it anyway, as
  //  it's more correct to do so.
  vtkCocoaRenderWindow* cocoa_window = vtkCocoaRenderWindow::SafeDownCast(render_window);

  if (local_renderer && cocoa_window && render_window_interactor)
  {
    //  This is special to our usage of vtk.  To prevent vtk
    //  from creating an NSWindow and NSView automatically (its
    //  default behaviour) we tell vtk that they exist already.
    //  The APIs names are a bit misleading, due to the cross
    //  platform nature of vtk, but this usage is correct.
    cocoa_window->SetRootWindow((__bridge void*)[self window]);
    cocoa_window->SetWindowId((__bridge void*)self);

    //  The usual vtk connections
    cocoa_window->AddRenderer(local_renderer);
    render_window_interactor->SetRenderWindow(cocoa_window);
    //  This is special to our usage of vtk.  vtkCocoaGLView
    //  keeps track of the renderWindow, and has a get
    //  accessor if you ever need it.
    [self setVTKRenderWindow:cocoa_window];
    //  Likewise, BasicVTKView keeps track of the renderer
    [self setRenderer: local_renderer];
  }
}

/*
 *  @name nativeRelease
 *  @fn -(void) nativeRelease
 *  @brief  Destructor
 */
-(void) nativeRelease {
  [self cleanUpVTKSupport];
}

#pragma mark -
#pragma mark Process

/*
 *  @name update
 *  @fn -(void) update
 *  @brief  Update VTK Pipeline
 */
-(void) update {

  // Update mapper
  self->data_mapper->Modified();
  self->data_mapper->Update();
  // Refresh window
  ((vtkRenderer*)[self getRenderer])->ResetCamera();
  // Called for rendering
  [self setNeedsDisplay:YES];
}

/**
 *  @name RotateX
 *  @fn -(void) RotateX:(double)theta Y:(double) phi Z:(double) gamma
 *  @brief  Apply rotation on object
 *  @param[in]  gamma   Rotation along Z axis
 *  @param[in]  theta   Rotation along X axis
 *  @param[in]  phi     Rotation along Y axis
 */
-(void) RotateGamma:(double)gamma Theta:(double) theta Phi:(double) phi {
  self->data_actor->SetOrientation(0.0, 0.0, 0.0);
  self->data_actor->RotateX(theta);
  self->data_actor->RotateY(phi);
  self->data_actor->RotateZ(gamma);
  self->data_actor->Modified();
}

#pragma mark -
#pragma mark Accessors

/*
 *  @name setDataMesh
 *  @fn -(void) setDataMesh : (void*) data_mesh
 *  @brief  Set the mesh to render
 *  @param[in]  data_mesh 3D mesh to display (vtkPolyData*)
 */
-(void) setDataMesh : (void*) mesh {
  self->data_mesh = (vtkPolyData*) mesh;
  if (!pipeline_init) {
    [self initVTKPipelineSupport];
    pipeline_init = YES;
  }
}

-(void) setAmbientX: (double) x Y:(double) y Z:(double) z {
  if (pipeline_init) {
    self->data_renderer->SetAmbient(x, y, z);
  }
}

-(void) setLighting: (BOOL)state {
  if (state) {
    self->data_actor->GetProperty()->LightingOn();
  } else {
    self->data_actor->GetProperty()->LightingOff();
  }

}

/*
 *  @name   setRenderer
 *  @fn - (void)setRenderer:(void*)renderer
 *  @brief  Set a new renderer
 *  @param  new_renderer     New renderer
 */
- (void)setRenderer:(void*)new_renderer {
  self->data_renderer = (vtkRenderer*)new_renderer;
}

/*
 *  @name   getRenderer
 *  @fn - (void*)getRenderer
 *  @brief  Return current renderer (vtkRenderer) as void*
 *  @return (void) Pointer to current renderer
 */
- (void*)getRenderer {
  return (void*)self->data_renderer;
}

/*
 *  @name setTextureMap
 *  @fn -(void) setTextureMap : (void*) texture_map;
 *  @brief  Add a texture map
 *  @param[in]  texture_map vtk texture map
 */
-(void) setTextureMap : (void*) texture_map {
  if ((vtkTexture*)texture_map != NULL && pipeline_init) {
    self->data_actor->SetTexture((vtkTexture*)texture_map);
  }
}

-(void) setLookUpTable:(void*)table {
  vtkLookupTable* lut = (vtkLookupTable*)table;
  data_mapper->SetScalarRange(lut->GetRange());
  data_mapper->SetLookupTable(lut);
  data_mapper->UseLookupTableScalarRangeOn();
  // Init actor as well
  data_bar_actor->SetLookupTable(lut);
  data_bar_actor->SetTitle("Error");
  data_bar_actor->SetOrientationToHorizontal();
  data_bar_actor->SetPosition(0.1, 0.01);
  data_bar_actor->SetWidth(0.8);
  data_bar_actor->SetHeight(0.1);
  data_bar_actor->SetTextPositionToPrecedeScalarBar();
  // Add to renderer
  data_renderer->AddActor2D(data_bar_actor);
}


@end
