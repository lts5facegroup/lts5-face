/**
 *  @file AppDelegate.m
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 06/08/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#import "AppDelegate.h"
#import "face_synthesis_pipeline_wrapper.h"
#import "pipeline_view_controller.h"

@interface AppDelegate () {
  FaceSynthesisPipelineWrapper* face_pipeline;
}

/** Main window */
@property (weak) IBOutlet NSWindow *window;
/** Pipeline view controller */
@property (assign) IBOutlet PipelineViewController* pipeline_controller;

@end

@implementation AppDelegate

/**
 *  @name awakeFromNib
 *  @fn - (void)awakeFromNib
 *  @brief  Entry point of GUI instantiation
 */
- (void)awakeFromNib {
}



/*- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
  // Insert code here to initialize your application
  NSLog(@"applicationDidFinishLaunching");
}

- (void)applicationDidUnhide:(NSNotification *)notification {
  NSLog(@"applicationDidUnhide");
}

- (void)applicationDidResignActive:(NSNotification *)notification {
  NSLog(@"applicationDidResignActive");
}*/

- (void)applicationWillTerminate:(NSNotification *)aNotification {
  // Insert code here to tear down your application
}

@end
