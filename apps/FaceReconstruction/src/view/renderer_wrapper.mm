
/**
 *  @file   renderer_wrapper.mm
 *  @brief  Wrapper from OpenGL Renderer
 *          HAs one LTS5::Mesh as input and holds OGLCallback instance for 
 *          rendering
 *
 *  @author Christophe Ecabert
 *  @date   30/08/16
 *  Copyright © 2016 Christophe Ecabert. All rights reserved.
 */
//ObjC
#import "renderer_wrapper.h"
#import "callback_wrapper.h"
// C++
#include "lts5/ogl/callbacks.hpp"
#include "lts5/ogl/program.hpp"
#include "lts5/ogl/renderer.hpp"
#include "lts5/ogl/camera.hpp"

using Mesh = LTS5::Mesh<float>;

/**
 *  @struct CustomCallback
 *  @brief  Custom OpenGL Callback implementation
 */
struct CustomCallback : LTS5::OGLCallback {
  /** Reference to renderer */
  LTS5::OGLRenderer<float>* renderer;
  /** Reference to camera */
  LTS5::OGLCamera* cam;
  /** Reference to mesh */
  Mesh* mesh;
  
  /**
   * @name  OGLRenderCb
   * @fn  virtual void OGLRenderCb(void)
   * @brief Callback invoked when scene need to be rendered
   */
  void OGLRenderCb(void) {
    if (renderer && mesh) {
      renderer->Render();
    }
  }
  
  /**
   * @name  OGLKeyboardCb
   * @fn  virtual void OGLKeyboardCb(const OGLKey& key,
   *                                 const OGLKeyState& state)
   * @brief Callback for keyboard event
   * @param[in] key     Key that trigger the callback
   * @param[in] state   Key state at that time
   */
  void OGLKeyboardCb(const LTS5::OGLKey& key, const LTS5::OGLKeyState& state) {
    std::cout << key << std::endl;
  }
};

@interface OGLRendererWrapper() {
  /** Native Renderer */
  LTS5::OGLRenderer<float>* renderer_;
  /** Reference to external Mesh */
  Mesh* ext_mesh_;
  /** Program */
  LTS5::OGLProgram* program_;
  /** Camera */
  LTS5::OGLCamera* cam_;
  /** Callback implementation */
  CustomCallback callback_;
  /** Callback wrapper */
  OGLCallbacksWrapper* callback_wrapper_;
}
@end

@implementation OGLRendererWrapper

/**
 *  @name init
 *  @brief  Constructor
 */
-(id) initWithWidth:(NSInteger) width andHeight:(NSInteger) height; {
  self = [super init];
  if (self) {
    using Attributes = LTS5::OGLProgram::Attributes;
    using AttType = LTS5::OGLProgram::AttributeType;
    
    // Init shader"
    std::string vertex_shdr_str = "#version 330\n"
                              "layout (location = 0) in vec3 position;\n"
                              "layout (location = 1) in vec3 normal;\n"
                              "layout (location = 2) in vec2 tex_coord;\n"
                              "uniform mat4 camera;\n"
                              "out vec2 tex_coord0;\n"
                              "out vec3 normal0;\n"
                              "void main() {\n"
                              "  gl_Position = camera * vec4(position, 1);\n"
                              "  normal0 = normal;\n"
                              "  tex_coord0 = tex_coord;\n"
                              "}";
    std::string frag_shdr_str = "#version 330\n"
                  "in vec2 tex_coord0;\n"
                  "in vec3 normal0;\n"
                  "out vec4 frag_color;\n"
                  "void main() {\n"
                  "  vec3 light_dir = vec3(0.0, 0.0, 1.0);\n"
                  "  float cos_theta = clamp( dot(normal0, light_dir), 0, 1);\n"
                  "  frag_color = vec4(vec3(1.0, 1.0, 1.0) * cos_theta, 1.0);\n"
                  "}";
    
    std::vector<LTS5::OGLShader*> shaders;
    shaders.push_back(new LTS5::OGLShader(vertex_shdr_str, GL_VERTEX_SHADER));
    shaders.push_back(new LTS5::OGLShader(frag_shdr_str, GL_FRAGMENT_SHADER));
    
    // Attributes
    std::vector<Attributes> attributes;
    attributes.push_back(Attributes(AttType::kPoint, 0, "position"));
    attributes.push_back(Attributes(AttType::kNormal, 1, "normal"));
    attributes.push_back(Attributes(AttType::kTexCoord, 2, "tex_coord"));
    
    // init program
    program_ = new LTS5::OGLProgram(shaders, attributes);
    for (auto& shader : shaders) {
      delete shader;
    }
    // init camera
    cam_ = new LTS5::OGLCamera((int)width, (int)height);
    // init renderer
    renderer_ = new LTS5::OGLRenderer<float>();
    renderer_->AddProgram(program_);
    
    // Init callbacks
    callback_.renderer = renderer_;
    callback_.cam = cam_;
    callback_wrapper_ = [[OGLCallbacksWrapper alloc] initWithPointer:&callback_];
  }
  return self;
}

#pragma mark -
#pragma mark Usage

/**
 *  @name
 *  @brief  Destructor
 */
-(void) nativeRelease {
  // Unlink callback
  callback_.renderer = nullptr;
  callback_.cam = nullptr;
  // Release stuff
  if (cam_) {
    delete cam_;
    cam_ = nullptr;
  }
  if (program_) {
    delete program_;
    program_ = nullptr;
  }
  if (renderer_) {
    delete renderer_;
    renderer_ = nullptr;
  }
}

/**
 *  @name updateMesh
 *  @brief  Update mesh, push data onto GPU
 */
-(void) updateMesh:(void*)mesh {
  if (ext_mesh_ == nullptr) {
    // First update, need to allocate all OGL buffer
    ext_mesh_ = (Mesh*)mesh;
    callback_.mesh = ext_mesh_;
    renderer_->AddMesh(ext_mesh_);
    renderer_->BindToOpenGL();
  } else {
    // Update each component
    renderer_->UpdateVertex();
    //renderer_->UpdateNormal();
  }
  // Update camera
  ext_mesh_->ComputeBoundingBox();
  const LTS5::AABB<float>& bbox = ext_mesh_->bbox();
  auto dt = bbox.max_ - bbox.center_;
  float r = std::sqrt((dt.x_ * dt.x_) + (dt.y_ * dt.y_) + (dt.z_ * dt.z_));
  // Define displacement in Z direction
  float dz = r / std::sin(cam_->get_field_of_view() * (float)M_PI * 0.002777778f);
  cam_->set_position(glm::vec3(bbox.center_.x_,
                               bbox.center_.y_,
                               bbox.center_.z_ + dz));
  program_->Use();
  program_->SetUniform("camera", cam_->matrix());
  program_->StopUsing();
}

#pragma mark -
#pragma mark Accessors

/**
 *  @name get_callback
 *  @brief  Provide callback to invoke when event occurs
 */
-(OGLCallbacksWrapper*) get_callbacks {
  return callback_wrapper_;
}

@end

