/**
 *  @file PipelineViewController.m
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 11/08/15.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#import "pipeline_view_controller.h"
#import "ogl_view.h"
#import "pipeline_type_wrapper.h"

#import "face_synthesis_pipeline_wrapper.h"

@interface PipelineViewController () {
  /** Pipeline parameters */
  PipelineParameters* pipeline_parameters;
  /** Face reconstruction pipeline */
  FaceSynthesisPipelineWrapper* face_pipeline;
  BOOL need_processing;
}

@end

@implementation PipelineViewController


#pragma mark -
#pragma mark Initialization

-(void) awakeFromNib {
}

/**
 *  @name initPipelineWithRootPath
 *  @fn -(void) initPipelineWithRootPath : (NSString*) path
 *  @brief  Instantiate reconstruction pipeline
 *  @param[in]  path
 */
-(void) initPipelineWithRootPath : (NSString*) path {
  // Pipeline
  if (!pipeline_parameters) {
    pipeline_parameters = [[PipelineParameters alloc] init];
  }
  // Extract working directory
  NSRange pos = [path rangeOfString:@"/" options:NSBackwardsSearch];
  if(pos.length > 0) {
    // Root found
    pipeline_parameters->working_directory = [path substringWithRange:NSMakeRange(0, pos.location)];
  } else {
    // No root
    pipeline_parameters->working_directory = @"";
  }
  pipeline_parameters->pipeline_xml_config_file = path;
  pipeline_parameters->error_vertex_file = [pipeline_parameters->working_directory stringByAppendingString:@"/BilinearFaceModel/v1.3/ErrVertexList.cfg"];
  
  @try {
    // Instantiate pipeline
    if (!face_pipeline) {
      // Init pipeline
      face_pipeline = [[FaceSynthesisPipelineWrapper alloc] initWithParameters:pipeline_parameters];
    } else {
      [face_pipeline nativeRelease];
      face_pipeline = nil;
      face_pipeline = [[FaceSynthesisPipelineWrapper alloc]initWithParameters:pipeline_parameters];
    }
  } @catch(NSException *exception) {
    NSLog(@"Error, with message : %@", [exception reason]);
    [self displayAlertViewWithMessage:@"Exception" hint: [exception reason]];
  }
}

/**
 *  @name displayDialogBoxSelector
 *  @fn - (void) displayDialogBoxSelector
 *  @brief  Display dialog box for the user to be able to select working a directory
 *  @return Working path
 *  @see  http://stackoverflow.com/questions/1640419/open-file-dialog-box
 */
- (NSString*) displayDialogBoxSelector {
  // Create the File Open Dialog class.
  NSOpenPanel* panel = [NSOpenPanel openPanel];
  // Can't select file in the dialog.
  [panel setCanChooseFiles:YES];
  // Can select a directory
  [panel setCanChooseDirectories:NO];
  // Multiple files not allowed
  [panel setAllowsMultipleSelection:NO];
  // Selection
  NSString* path_to_config;
  if ( [panel runModal] == NSModalResponseOK )
  {
    if (!pipeline_parameters) {
      pipeline_parameters = [[PipelineParameters alloc]init];
    }
    
    // Get an array containing the full filenames of all
    // files and directories selected.
    NSArray* urls = [panel URLs];
    // Process paths
    for (int i = 0; i < [urls count]; ++i) {
      path_to_config = [[urls objectAtIndex:i] path];
    }
    if ([urls count] == 1) {
      pipeline_parameters->pipeline_xml_config_file = path_to_config;
      // Update dictionary
      NSString* plist_path = [[NSBundle mainBundle] pathForResource:@"config" ofType:@"plist"];
      NSMutableDictionary* dic = [NSMutableDictionary dictionaryWithContentsOfFile:plist_path];
      [dic setValue:pipeline_parameters->pipeline_xml_config_file forKey:@"pipeline_config"];
      // Write into file
      [dic writeToFile:plist_path atomically:YES];
    }
  }
  return path_to_config;
}

/**
 *  @name displayAlertViewWithMessage
 *  @fn - (void) displayAlertViewWithMessage: (NSString*) message
 *  @brief  Display alert view with a given message
 *  @param[in]  message Alert message
 *  @param[in]  info    Information about the alert
 *  @see  https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/Dialog/Tasks/UsingAlerts.html
 */
- (void) displayAlertViewWithMessage: (NSString*) message hint : (NSString*) info {
  // Create Alert
  NSAlert* alert = [[NSAlert alloc] init];
  // Add button + message
  [alert addButtonWithTitle:@"OK"];
  [alert setMessageText:message];
  [alert setInformativeText:info];
  // Define style
  [alert setAlertStyle:NSAlertStyleWarning];
  // Pop up + catch event
  [alert runModal];
}

/**
 *  @name viewDidLoad
 *  @fn -(void) viewDidLoad
 *  @brief  Called when view's loaded
 */
-(void) viewDidLoad {
  [super viewDidLoad];
  
  // Read working directoy location
  NSString* plist_path = [[NSBundle mainBundle] pathForResource:@"config" ofType:@"plist"];
  NSMutableDictionary* dic = [NSMutableDictionary dictionaryWithContentsOfFile:plist_path];
  NSString* config_path = [dic valueForKey:@"pipeline_config"];
  if ([config_path isEqual:@""]) {
    // Ask user
    NSLog(@"Ask user for path");
    config_path = [self displayDialogBoxSelector];
    if (config_path != nil) {
      [self initPipelineWithRootPath:config_path];
    } else {
      NSLog(@"Error while selection working directory ! ");
      [self displayAlertViewWithMessage:@"Error !" hint:@"while selecting working directory"];
    }
  } else {
    // Working directory already set.
    [self initPipelineWithRootPath:config_path];
  }
  
  // Setup shape selector
  // ---------------------
  [shape_container addSubview:shape_selector];
  [self setupShapeControl];
  [self setupRealShapeControl];

  // Transformation
  // ---------------------
  [transformation_container addSubview:synthetic_static_transform_selector];
  [self setupStaticTransformationControl];
  [self setupRealTransformationControl];
  
  // Recovered data
  // ---------------------
  [self setupRecoverParameterControl];
}

-(void) viewDidDisappear {
  // Release OpenGL View
  [synthetic_view nativeRelease];
  [model_view nativeRelease];
  // Release pipeline
  [face_pipeline nativeRelease];
  
  [super viewDidDisappear];
}

/**
 *  @name setupShapeControl
 *  @fn -(void) setupShapeControl
 *  @brief  Setup synthetic shape control view
 */
-(void) setupShapeControl {
  // Text field
  [identity_selector setIntegerValue:face_pipeline.getSyntheticIdentity];
  [expression_selected_viewer setIntegerValue:face_pipeline.getSyntheticExpression];
  [camera_selector setIntegerValue:face_pipeline.getNumberCamera];
  // Slider - expression
  [expression_selector setMinValue : 0.0];
  [expression_selector setMaxValue : 46.0];
  [expression_selector setNumberOfTickMarks : 47];
  [expression_selector setAllowsTickMarkValuesOnly : YES];
  [expression_selector setIntegerValue:face_pipeline.getSyntheticExpression];
}

/**
 *  @name setupStaticTransformationControl
 *  @fn -(void) setupStaticTransformationControl
 *  @brief  Setup static rigid transformation control view
 */
-(void) setupStaticTransformationControl {
  // Formatter
  NSNumberFormatter* format = [[NSNumberFormatter alloc] init];
  [format setFormat:@"##0.1"];
  NSNumberFormatter* format_e2 = [[NSNumberFormatter alloc] init];
  [format_e2 setFormat:@"##0.01"];
  NSNumberFormatter* format_e4 = [[NSNumberFormatter alloc] init];
  [format_e4 setFormat:@"##0.0001"];

  // Regulariation parameters
  [etaId_selector setFloatValue:[face_pipeline getIdentityRegularization]];
  [etaExpr_selector setFloatValue:[face_pipeline getExpressionRegularization]];
  [etaId_selector setFormatter:format_e4];
  [etaExpr_selector setFormatter:format_e4];
  // Slider gamma
  [gamma_slider setMinValue:-30.0];
  [gamma_slider setMaxValue:30.0];
  [gamma_slider setNumberOfTickMarks:5];
  [gamma_slider setAllowsTickMarkValuesOnly:NO];
  [gamma_slider setFloatValue : [face_pipeline getSyntheticGamma]];
  // Slider theta
  [theta_slider setMinValue:-30.0];
  [theta_slider setMaxValue:30.0];
  [theta_slider setNumberOfTickMarks:5];
  [theta_slider setAllowsTickMarkValuesOnly:NO];
  [theta_slider setFloatValue : [face_pipeline getSyntheticTheta]];
  // Slider phi
  [phi_slider setMinValue:-30.0];
  [phi_slider setMaxValue:30.0];
  [phi_slider setNumberOfTickMarks:5];
  [phi_slider setAllowsTickMarkValuesOnly:NO];
  [phi_slider setFloatValue : [face_pipeline getSyntheticPhi]];
  // Gamma viewer
  [gamma_viewer setFloatValue:[face_pipeline getSyntheticGamma]];
  [gamma_viewer setFormatter:format];
  // Theta viewer
  [theta_viewer setFloatValue:[face_pipeline getSyntheticTheta]];
  [theta_viewer setFormatter:format];
  // Phi viewer
  [phi_viewer setFloatValue:[face_pipeline getSyntheticPhi]];
  [phi_viewer setFormatter:format];
  // Delta_{X,Y,Z}
  [tx_viewer setFloatValue : [face_pipeline getSyntheticDeltaX]];
  [tx_viewer setFormatter:format];
  [ty_viewer setFloatValue : [face_pipeline getSyntheticDeltaY]];
  [ty_viewer setFormatter:format];
  [tz_viewer setFloatValue : [face_pipeline getSyntheticDeltaZ]];
  [tz_viewer setFormatter:format];
  // Noise
  [noise_selector setFloatValue:[face_pipeline getSyntheticNoiseSTD]];
  [noise_selector setFormatter:format_e2];
  // Scale
  [s_selector setFloatValue:[face_pipeline getSyntheticScale]];
  [s_selector setFormatter:format];
}

/**
 *  @name setupRecoverParameterControl
 *  @fn -(void) setupRecoverParameterControl
 *  @brief  Initialize controle corresponding to the recovered parameters
 */
-(void) setupRecoverParameterControl {
  // Formatter
  NSNumberFormatter* format = [[NSNumberFormatter alloc] init];
  [format setFormat:@"##0.1"];
  // Angle
  [computed_gamma_viewer setFloatValue:[face_pipeline getGamma]];
  [computed_gamma_viewer setFormatter:format];
  [computed_theta_viewer setFloatValue:[face_pipeline getTheta]];
  [computed_theta_viewer setFormatter:format];
  [computed_phi_viewer setFloatValue:[face_pipeline getPhi]];
  [computed_phi_viewer setFormatter:format];
  // Recovered translation
  [computed_delta_x_viewer setFloatValue:[face_pipeline getDeltaX]];
  [computed_delta_x_viewer setFormatter:format];
  [computed_delta_y_viewer setFloatValue:[face_pipeline getDeltaY]];
  [computed_delta_y_viewer setFormatter:format];
  [computed_delta_z_viewer setFloatValue:[face_pipeline getDeltaZ]];
  [computed_delta_z_viewer setFormatter:format];
  // Recovered scale
  [computed_scale_viewer setFloatValue: [face_pipeline getScale]];
  [computed_scale_viewer setFormatter: format];
}

/**
 *  @name   setupRealTransformationControl
 *  @fn -(void) setupRealTransformationControl
 *  @brief  Initialize subview for real sequence transform
 */
-(void) setupRealTransformationControl {
  // Format
  NSNumberFormatter* format_e4 = [[NSNumberFormatter alloc]init];
  [format_e4 setFormat:@"##0.0001"];

  // Eta
  [eta_real_id_selector setFloatValue:[face_pipeline getIdentityRegularization]];
  [eta_real_expr_selector setFloatValue:[face_pipeline getExpressionRegularization]];
  [eta_real_id_selector setFormatter:format_e4];
  [eta_real_expr_selector setFormatter:format_e4];
}

/**
 *  @name   setupRealShapeControl
 *  @fn -(void) setupRealShapeControl
 *  @brief  Initialize subview for real data input
 */
-(void) setupRealShapeControl {
  // Text field
  [frame_to_process_selector setIntValue:0];
  [total_frame_viewer setIntegerValue:[face_pipeline getTotalNumberOfFrame]];
  // Checkbox
  [process_all_selector setState:NSOffState];
}

#pragma mark -
#pragma mark Update GUI / Data

/**
 *  @name updateSyntheticDataAndRefreshGui
 *  @fn -(void) updateSyntheticDataAndRefreshGui
 *  @brief  Generate new synthetic data and refresh GUI
 */
-(void) updateSyntheticDataAndRefreshGui {
  // Generate new data
  [face_pipeline GenerateSyntheticData];
  // Update OpenGL buffer
  [synthetic_view updateMesh:[face_pipeline getMesh:kSynthetic]];
  // Refresh UI
  [synthetic_view setNeedsDisplay:YES];
}

/**
 *  @name updateModelDataAndRefreshGui
 *  @fn -(void) updateModelDataAndRefreshGui
 *  @brief  Generate new reconstruction and update GUI
 */
-(void) updateModelDataAndRefreshGui {
  // Call pipeline process
  [face_pipeline Process];
  // Update OpenGL buffer
  [model_view updateMesh:[face_pipeline getMesh:kModel]];
  // Display recovered information
  [computed_gamma_viewer setFloatValue:[face_pipeline getGamma]];
  [computed_theta_viewer setFloatValue:[face_pipeline getTheta]];
  [computed_phi_viewer setFloatValue:[face_pipeline getPhi]];
  [computed_delta_x_viewer setFloatValue:[face_pipeline getDeltaX]];
  [computed_delta_y_viewer setFloatValue:[face_pipeline getDeltaY]];
  [computed_delta_z_viewer setFloatValue:[face_pipeline getDeltaZ]];
  [computed_scale_viewer setFloatValue:[face_pipeline getScale]];
  [model_view setNeedsDisplay:YES];
}


#pragma mark -
#pragma mark IBAction / Delegate

/**
 *  @name controlTextDidEndEditing
 *  @fn - (void) controlTextDidEndEditing:(NSNotification *)notification
 *  @brief  Callback invoked when TextField are editted
 *  @param[in]  notification  Event notifaction
 */
- (void) controlTextDidEndEditing:(NSNotification *)notification {
  // Get input
  NSTextField* input = (NSTextField*)[notification object];
  // Act based on input
  if (input == identity_selector) {
    // New identity selected
    [face_pipeline setSyntheticIdentity:[input integerValue]];
    // Refresh data
    [self updateSyntheticDataAndRefreshGui];
  }  else if (input == tx_viewer) {
    // Delta X input changed
    [face_pipeline setSyntheticDeltaX:[input floatValue]];
    [self updateSyntheticDataAndRefreshGui];
  } else if (input == ty_viewer) {
    // Delta Y input changed
    [face_pipeline setSyntheticDeltaY:[input floatValue]];
    [self updateSyntheticDataAndRefreshGui];
  } else if (input == tz_viewer) {
    // Delta Z input changed
    [face_pipeline setSyntheticDeltaZ:[input floatValue]];
    [self updateSyntheticDataAndRefreshGui];
  } else if (input == s_selector) {
    // Scale changed
    [face_pipeline setSyntheticScale:[input floatValue]];
    [self updateSyntheticDataAndRefreshGui];
  } else if (input == etaId_selector || input == eta_real_id_selector) {
    // Id regularization changed
    [face_pipeline setIdentityRegularization:[input floatValue]];
  } else if (input == etaExpr_selector || input == eta_real_expr_selector) {
    // Expr regularization changed
    [face_pipeline setExpressionRegularization:[input floatValue]];
  } else if (input == noise_selector) {
    // Noise std changed
    [face_pipeline setSyntheticNoiseSTD:[input floatValue]];
    [self updateSyntheticDataAndRefreshGui];
  } else if (input == frame_to_process_selector) {
    [face_pipeline setFrameToProcess:[input integerValue]];
  }
}

- (IBAction)slider_changed:(NSSlider*)sender {
  if (sender == expression_selector) {
    // Change of expression
    [expression_selected_viewer setIntegerValue:[sender integerValue]];
    [face_pipeline setSyntheticExpression:[sender integerValue]];
  } else if (sender == gamma_slider) {
    // Change of gamma
    [gamma_viewer setIntegerValue:[sender integerValue]];
    [face_pipeline setSyntheticGamma:[sender integerValue]];
  } else if (sender == theta_slider) {
    // Change of gamma
    [theta_viewer setIntegerValue:[sender integerValue]];
    [face_pipeline setSyntheticTheta:[sender integerValue]];
  } else if (sender == phi_slider) {
    // Change of gamma
    [phi_viewer setIntegerValue:[sender integerValue]];
    [face_pipeline setSyntheticPhi:[sender integerValue]];
  }
  // Update data
  [self updateSyntheticDataAndRefreshGui];
}

- (IBAction)swicth_synthetic_real_data:(NSButton *)sender {
  // Action depend on state
  if ([sender state] == NSOnState) {
    // Synthetic data
    [face_pipeline setIsSyntheticData:YES];
    // Remove old GUI
    [real_data_transform_selector removeFromSuperviewWithoutNeedingDisplay];
    [real_shape_control removeFromSuperviewWithoutNeedingDisplay];
    // Add new stuff
    [shape_container addSubview:shape_selector];
    [transformation_container addSubview:synthetic_static_transform_selector];

  } else {
    // Real data
    [face_pipeline setIsSyntheticData:NO];
    // Remove old GUI
    [shape_selector removeFromSuperviewWithoutNeedingDisplay];
    [synthetic_static_transform_selector removeFromSuperviewWithoutNeedingDisplay];
    // Add new stuff
    [shape_container addSubview:real_shape_control];
    [transformation_container addSubview:real_data_transform_selector];
  }
}

/*
 *  @name clear_transformation
 *  @fn - (IBAction)clear_transformation:(NSButton *)sender
 *  @brief  Callback where transformation is initialiazed to zero
 *  @param[in]  sender
 */
- (IBAction)clear_transformation:(NSButton *)sender {
  // New value
  [face_pipeline setSyntheticGamma:0.f];
  [face_pipeline setSyntheticTheta:0.f];
  [face_pipeline setSyntheticPhi:0.f];
  [face_pipeline setSyntheticDeltaX:0.f];
  [face_pipeline setSyntheticDeltaY:0.f];
  [face_pipeline setSyntheticDeltaZ:-300.f];
  [face_pipeline setSyntheticScale:100.f];
  // Set gui
  [gamma_slider setFloatValue:[face_pipeline getSyntheticGamma]];
  [gamma_viewer setFloatValue:[face_pipeline getSyntheticGamma]];
  [theta_slider setFloatValue:[face_pipeline getSyntheticTheta]];
  [theta_viewer setFloatValue:[face_pipeline getSyntheticTheta]];
  [phi_slider setFloatValue:[face_pipeline getSyntheticPhi]];
  [phi_viewer setFloatValue:[face_pipeline getSyntheticPhi]];
  [tx_viewer setFloatValue:[face_pipeline getSyntheticDeltaX]];
  [ty_viewer setFloatValue:[face_pipeline getSyntheticDeltaY]];
  [tz_viewer setFloatValue:[face_pipeline getSyntheticDeltaZ]];
  [s_selector setFloatValue:[face_pipeline getSyntheticScale]];

  // Generate new data
  [self updateSyntheticDataAndRefreshGui];
  
  // Reset tracking
  [face_pipeline ResetTrackingState];
}

/*
 *  @name process
 *  @fn - (IBAction)process:(NSButton *)sender
 *  @brief  Callback where processing is launched
 *  @param[in]  sender
 */
- (IBAction)process:(NSButton *)sender {
  // Real/Synth data
  if ([face_pipeline isSyntheticData] == YES) {
    // Fake images, call fit
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^(void){
                     // Call reconstruction off UI thread
                     [self updateModelDataAndRefreshGui];
                   });
  } else {
    // Real images
    if (need_processing == YES) {
      // Start/stop processing
      need_processing = NO;
    } else {
      // Call processing for whole frames
      need_processing = YES;
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                     ^(void){
                       // Disable tracking
                       //[face_pipeline setTrackingState:NO];
                       // Single frame/ multiframe ?
                       if ([process_all_selector integerValue] == NSOnState) {
                         // Multiframe
                         NSInteger max_frame_idx = [face_pipeline getTotalNumberOfFrame];
                         NSInteger counter = [face_pipeline getFrameToProcess];
                         while (counter < max_frame_idx && need_processing == YES) {
                           [self updateModelDataAndRefreshGui];
                           counter++;
                         }
                       } else {
                         [self updateModelDataAndRefreshGui];
                       }
                       // Processing is done
                       need_processing = NO;
                     });
    }
  }
}

/*
 *  @name select_config_file
 *  @fn - (IBAction)select_config_file:(NSMenuItem *)sender
 *  @brief  Callback to select configuration file from menu
 *  @param[in]  sender  Entry being clicked
 */
- (IBAction)select_config_file:(NSMenuItem *)sender {
  NSString* config_path = [self displayDialogBoxSelector];
  if (config_path != nil) {
    [self initPipelineWithRootPath:config_path];
  } else {
    NSLog(@"Error while selection working directory ! ");
    [self displayAlertViewWithMessage:@"Error !" hint:@"while selecting working directory"];
  }
}


#pragma mark -
#pragma mark Accessors

@end
