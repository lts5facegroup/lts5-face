//
//  main.m
//  FaceReconstruction
//
//  Created by Christophe Ecabert on 05/08/15.
//  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
  return NSApplicationMain(argc, argv);
}
