//
//  DemoViewController.m
//  LTS5-Apps
//
//  Created by Christophe Ecabert on 20/09/16.
//  Copyright © 2016 Ecabert Christophe. All rights reserved.
//

#import "DemoViewController.h"
#import "OGLCameraView.h"
#import "OglView.h"
#import "ExpressionKitWrapper.h"
#import "PlotView.h"
#import "ExpressionKitPlot.h"


@interface DemoViewController () {
  /** Processing interface */
  ExpressionKitWrapper* expression_kit;
  /** Processing parameters */
  ExpressionKitParameter* params;
  /** Init */
  BOOL is_kit_allocated;
  /** Indicate processing state */
  BOOL is_processing;
  /** Maximum frame number */
  NSInteger max_frame_number;
  /** Current frame index */
  NSInteger current_frame_number;
  
  /** Expression score */
  NSMutableArray* expression_score;
  /** ExpressionKit plot data */
  ExpressionKitPlot* plotData;
}


/** Display view for left camera */
@property (weak) IBOutlet OGLCameraView *streamView;
/** 3D Reconstructed */
@property (weak) IBOutlet OGLView *reconstructedView;
/** Expression Plot */
@property (weak) IBOutlet PlotView *plotView;

/** Processing */
-(void) Process;

@end

@implementation DemoViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  // Alloc params
  params = [[ExpressionKitParameter alloc] init];
  if([self displayFileSelector] != nil) {
    is_kit_allocated = YES;
    // Create expression kit
    expression_kit = [ExpressionKitWrapper alloc];
    // Expression score
    expression_score = [[NSMutableArray alloc] initWithCapacity:7];
    // PlotData
    plotData = [[ExpressionKitPlot alloc] init];
  }
}

-(void) viewDidAppear {
  [super viewDidAppear];
  
  @try {
    if (expression_kit) {
      // Init expression kit
      expression_kit = [expression_kit initWithParameters:params];
      // Init input view
      NSInteger n_cam = [expression_kit getNumberCamera];
      [_streamView setNumberOfCamera:n_cam withAspectRation:1.25f];
      // Get maximum frame
      max_frame_number = [expression_kit getTotalNumberOfFrame];
    }
  } @catch (NSException *exception) {
    // Release native ressource
    [expression_kit nativeRelease];
  } @finally {
    [plotData renderInView:self.plotView animated:NO];
  }
}

-(void)viewDidDisappear {
  [super viewDidDisappear];
  // Clean up
  if (expression_kit) {
    [expression_kit nativeRelease];
  }
  
}


#pragma mark -
#pragma mark Dialog box

-(NSString*) displayFileSelector {
  // Crate panel
  NSOpenPanel* panel = [NSOpenPanel openPanel];
  // Set option
  [panel setCanChooseFiles:YES];
  [panel setCanChooseDirectories:NO];
  [panel setAllowsMultipleSelection:NO];
  // Selection
  NSString* selection;
  if ([panel runModal] == NSModalResponseOK) {
    // Get an array containing the full filenames of all
    // files and directories selected.
    NSArray* urls = [panel URLs];
    if ([urls count] == 1) {
      selection = [[urls objectAtIndex:0] path];
      // Define path to config file
      params.configuration_file = selection;
      // Working dir
      NSRange pos = [selection rangeOfString:@"/" options:NSBackwardsSearch];
      if (pos.length > 0) {
        params.working_directory = [selection substringWithRange:NSMakeRange(0, pos.location + 1)];
      } else {
        params.working_directory = @"";
      }
      // Set tracker path
      params.face_tracker_model = [params.working_directory
                                   stringByAppendingString:@"../models/face-model-v1.bin"];
      params.f_detector_model = [params.working_directory
                                 stringByAppendingString:@"../models/haarcascade_frontalface_alt2.xml"];
      // Set expression path
      params.expression_model = [params.working_directory
                                 stringByAppendingString:@"../models/expression_model.bin"];
    }
  }
  return selection;
}

#pragma mark -
#pragma mark Process

-(void) Process {
  NSLog(@"Process start");
  
  NSInteger err = -1;
  max_frame_number = 450;
  while (is_processing && current_frame_number < max_frame_number) {
    NSLog(@"Process frame %ld", (long)current_frame_number);
    // Reconstruct
    current_frame_number = [expression_kit getCurrentFrameIndex];
    err = [expression_kit generateFrontalImage];
    if(!err) {
      [expression_kit updateWithInputImage:_streamView.views];
      [expression_kit updateWithReconstruction:_reconstructedView];
      
      // Detect expression
      [expression_kit detectExpression:expression_score];
      
      
    }
    // Update
    dispatch_sync(dispatch_get_main_queue(), ^(void){
      [plotData addDataEntry:(void*)expression_score];
    });
  }
  if (current_frame_number == max_frame_number) {
    is_processing = NO;
  }
}

#pragma mark -
#pragma mark Callbacks

- (IBAction)TapProcess:(id)sender {
  if (is_kit_allocated) {
    if (is_processing) {
      is_processing = NO;
      NSLog(@"Process stop");
    } else {
      // Start
      is_processing = YES;
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                     ^(void){
                       [self Process];
                     });
    }
  }
}
@end
