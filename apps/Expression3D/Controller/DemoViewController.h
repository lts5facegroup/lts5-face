//
//  DemoViewController.h
//  LTS5-Apps
//
//  Created by Christophe Ecabert on 20/09/16.
//  Copyright © 2016 Ecabert Christophe. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DemoViewController : NSViewController

/**
 *  @name   TapProcess
 *  @brief  Method invoked when processing is needed
 */
- (IBAction)TapProcess:(id)sender;

@end
