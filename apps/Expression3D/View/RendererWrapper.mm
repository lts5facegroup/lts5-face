/**
 *  @file   renderer_wrapper.mm
 *  @brief  Wrapper from OpenGL Renderer
 *          HAs one LTS5::Mesh as input and holds OGLCallback instance for 
 *          rendering
 *
 *  @author Christophe Ecabert
 *  @date   30/08/16
 *  Copyright © 2016 Christophe Ecabert. All rights reserved.
 */

//ObjC
#import "RendererWrapper.h"
#import "OGLCallbackWrapper.h"
// C++
#import "lts5/ogl/callbacks.hpp"
#import "lts5/ogl/image_renderer.hpp"
#import "lts5/ogl/camera.hpp"

/**
 *  @struct CustomCallback
 *  @brief  Custom OpenGL Callback implementation
 */
struct CustomCallback : LTS5::OGLCallback {
  /** Reference to renderer */
  LTS5::OGLImageRenderer* renderer;
  /** Reference to camera */
  LTS5::OGLCamera* cam;
  
  /**
   * @name  OGLRenderCb
   * @fn  virtual void OGLRenderCb(void)
   * @brief Callback invoked when scene need to be rendered
   */
  void OGLRenderCb(void) {
    if (renderer) {
      renderer->Render();
    }
  }
};

@interface OGLRendererWrapper() {
  /** Native Renderer */
  LTS5::OGLImageRenderer* renderer_;
  /** Camera */
  LTS5::OGLCamera* cam_;
  /** Texture */
  LTS5::OGLTexture* tex_;
  /** Callback implementation */
  CustomCallback callback_;
  /** Callback wrapper */
  OGLCallbackWrapper* callback_wrapper_;
}
@end

@implementation OGLRendererWrapper

/**
 *  @name init
 *  @brief  Constructor
 */
-(id) initWithWidth:(NSInteger) width andHeight:(NSInteger) height; {
  self = [super init];
  if (self) {
    renderer_ = new LTS5::OGLImageRenderer();
    renderer_->BindToOpenGL();
    renderer_->SetupView(0, 0, (int)width, (int)height);
    renderer_->SetWindowDimension((int)width, (int)height);
    // Init callbacks
    callback_.renderer = renderer_;
    callback_.cam = cam_;
    callback_wrapper_ = [[OGLCallbackWrapper alloc] initWithPointer:&callback_];
  }
  return self;
}

#pragma mark -
#pragma mark Usage

/**
 *  @name
 *  @brief  Destructor
 */
-(void) nativeRelease {
  // Unlink callback
  callback_.renderer = nullptr;
  callback_.cam = nullptr;
  // Release stuff
  if (renderer_) {
    delete renderer_;
    renderer_ = nullptr;
  }
  if (tex_) {
    delete tex_;
    tex_ = nullptr;
  }
}

/**
 *  @name updateTexture
 *  @brief  Update texture, push data onto GPU
 */
-(void) updateTexture:(void*) tex {
  cv::Mat image = *((cv::Mat*)tex);
  if (!tex_) {
    tex_ = new LTS5::OGLTexture(image, GL_LINEAR, GL_CLAMP_TO_EDGE, false);
    renderer_->AddTexture(tex_);
  }
  tex_->Upload(image);
}

#pragma mark -
#pragma mark Accessors

/**
 *  @name get_callback
 *  @brief  Provide callback to invoke when event occurs
 */
-(OGLCallbackWrapper*) get_callbacks {
  return callback_wrapper_;
}

@end

