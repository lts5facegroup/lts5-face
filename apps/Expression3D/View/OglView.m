/**
 *  @file   ogl_view.mm
 *  @brief  Custom Cocoa OpenGL view
 *  @see    https://developer.apple.com/library/mac/qa/qa1385/_index.html
 *
 *  @author Christophe Ecabert
 *  @date   31/07/16
 *  Copyright © 2016 Christophe Ecabert. All rights reserved.
 */

#import <OpenGL/gl3.h>

//ObjC
#import "OglView.h"
#import "OGLCallbackWrapper.h"
//ObjC++
#import "RendererWrapper.h"

@interface OGLView() {
  /** OGL Callback wrapper */
  OGLCallbackWrapper* callback_;
  /** OGL Renderer */
  OGLRendererWrapper* renderer_;
}
@end

@implementation OGLView

#pragma mark -
#pragma mark OpenGL Event

/**
 *  @name awakeFromNib
 *  @fn -(void) awakeFromNib
 *  @brief  Mehtod invoked when Nib file is loaded
 */
-(void) awakeFromNib {
  // Init tracking area
  NSTrackingAreaOptions opt = (NSTrackingActiveAlways |
                               NSTrackingInVisibleRect |
                               NSTrackingMouseMoved);
  NSTrackingArea* area = [[NSTrackingArea alloc] initWithRect:[self bounds]
                                                      options:opt owner:self
                                                     userInfo:nil];
  [self addTrackingArea:area];
  // Create OGL pixel format attribute
  NSOpenGLPixelFormatAttribute attribs[] = {
    NSOpenGLPFADoubleBuffer,
    NSOpenGLPFAColorSize, 24,
    NSOpenGLPFAAlphaSize, 8,
    NSOpenGLPFADepthSize, 32,
    NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core,
    NSOpenGLPFAAccelerated,
    NSOpenGLPFANoRecovery,
    0
  };
  // Create pixel format
  NSOpenGLPixelFormat* frmt = [[NSOpenGLPixelFormat alloc]
                               initWithAttributes:attribs];
  if (frmt) {
    // Set pixel format for the view
    [self setPixelFormat:frmt];
    // Create context
    NSOpenGLContext* ctx = [[NSOpenGLContext alloc]
                            initWithFormat:frmt shareContext:nil];
    [self setOpenGLContext:ctx];
    // Set context as current
    [[self openGLContext] makeCurrentContext];
    // Set bounds
    NSRect b = [self bounds];
    glViewport(0, 0, b.size.width, b.size.height);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glDisable(GL_CULL_FACE);
    //glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
  }
  
  // Init renderer
  CGSize view_dim = [self bounds].size;
  renderer_ = [[OGLRendererWrapper alloc] initWithWidth:(NSInteger)view_dim.width
                                              andHeight:(NSInteger)view_dim.height];
  if (renderer_) {
    callback_ = [renderer_ get_callbacks];
  }
}

-(id)initWithFrame:(NSRect)frameRect {
  self = [super initWithFrame:frameRect];
  
  NSLog(@"frame w=%f, h=%f (%f,%f)",
        frameRect.size.width,
        frameRect.size.height,
        frameRect.origin.x,
        frameRect.origin.y);
  
  if (self) {
    // Init tracking area
    NSTrackingAreaOptions opt = (NSTrackingActiveAlways |
                                 NSTrackingInVisibleRect |
                                 NSTrackingMouseMoved);
    NSTrackingArea* area = [[NSTrackingArea alloc] initWithRect:[self bounds]
                                                        options:opt
                                                          owner:self
                                                       userInfo:nil];
    [self addTrackingArea:area];
    // Create OGL pixel format attribute
    NSOpenGLPixelFormatAttribute attribs[] = {
      NSOpenGLPFADoubleBuffer,
      NSOpenGLPFAColorSize, 24,
      NSOpenGLPFAAlphaSize, 8,
      NSOpenGLPFADepthSize, 32,
      NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core,
      NSOpenGLPFAAccelerated,
      NSOpenGLPFANoRecovery,
      0
    };
    // Create pixel format
    NSOpenGLPixelFormat* frmt = [[NSOpenGLPixelFormat alloc]
                                 initWithAttributes:attribs];
    if (frmt) {
      // Set pixel format for the view
      [self setPixelFormat:frmt];
      // Create context
      NSOpenGLContext* ctx = [[NSOpenGLContext alloc]
                              initWithFormat:frmt shareContext:nil];
      [self setOpenGLContext:ctx];
      // Set context as current
      [[self openGLContext] makeCurrentContext];
      // Set bounds
      NSRect b = [self bounds];
      glViewport(0, 0, b.size.width, b.size.height);
      
      glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
      glFrontFace(GL_CCW);
      glCullFace(GL_BACK);
      glDisable(GL_CULL_FACE);
      //glEnable(GL_CULL_FACE);
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glEnable(GL_DEPTH_TEST);
    }
    
    // Init renderer
    CGSize view_dim = [self bounds].size;
    if (!renderer_) {
      renderer_ = [[OGLRendererWrapper alloc] init];
    }
    renderer_ = [renderer_ initWithWidth:(NSInteger)view_dim.width
                               andHeight:(NSInteger)view_dim.height];
    if (renderer_) {
      callback_ = [renderer_ get_callbacks];
    }
  }
  return self;
}

/**
 *  @name nativeRelease
 *  @brief  Release native renderer
 */
-(void) nativeRelease {
  if (renderer_) {
    [renderer_ nativeRelease];
  }
}

-(void) drawRect:(NSRect)dirtyRect {
  // Called during resize operations
  // Avoid flickering during resize by drawiing
  [self drawView];
}

-(void) drawView {
  [[self openGLContext] makeCurrentContext];
  if (callback_) {
    [callback_ onRender];
  } else {
    glClearColor(0.f, 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT); 
  }
  // Swap buffer
  [[self openGLContext] flushBuffer];
}

- (void) windowWillClose:(NSNotification*)notification {
}

- (void)reshape {
  [super reshape];
  // Get the view size in Points
  NSRect viewRectPoints = [self bounds];
  // Set the new dimensions in our renderer
  glViewport(0, 0, viewRectPoints.size.width, viewRectPoints.size.height);
  
  //TODO Camera aspect ratio should be updated
  
}

#pragma mark -
#pragma mark Mouse Event

-(BOOL) acceptsFirstResponder {
  return YES;
}

/**
 *  @name mouseDown
 *  @brief  Function invoked when left mouse is hit
 *  @param[in]  theEvent  Event that trigger callback
 */
-(void) mouseDown:(NSEvent *)theEvent {
  if ([theEvent type] == NSEventTypeLeftMouseDown && callback_) {
    NSPoint pos = [self convertPoint: [theEvent locationInWindow] fromView:nil];
    [callback_ onMouseClick: kOGLLeftButton withState:kOGLKeyPressed at:&pos];
  } else {
    [super mouseDown:theEvent];
  }
}

/**
 *  @name mouseDown
 *  @brief  Function invoked when left mouse is hit
 *  @param[in]  theEvent  Event that trigger callback
 */
-(void) mouseUp:(NSEvent *)theEvent {
  if ([theEvent type] == NSEventTypeLeftMouseUp && callback_) {
    NSPoint pos = [self convertPoint: [theEvent locationInWindow] fromView:nil];
    [callback_ onMouseClick: kOGLLeftButton withState:kOGLKeyReleased at:&pos];
  } else {
    [super mouseUp:theEvent];
  }
}

/**
 *  @name rightMouseDown
 *  @brief  Function invoked when right mouse is hit
 *  @param[in]  theEvent  Event that trigger callback
 */
-(void) rightMouseDown:(NSEvent *)theEvent {
  if ([theEvent type] == NSEventTypeRightMouseDown && callback_) {
    NSPoint pos = [self convertPoint: [theEvent locationInWindow] fromView:nil];
    [callback_ onMouseClick: kOGLRightButton withState:kOGLKeyPressed at:&pos];
  } else {
    [super rightMouseDown:theEvent];
  }
}

/**
 *  @name rightMouseDown
 *  @brief  Function invoked when right mouse is hit
 *  @param[in]  theEvent  Event that trigger callback
 */
-(void) rightMouseUp:(NSEvent *)theEvent {
  if ([theEvent type] == NSEventTypeRightMouseUp && callback_) {
    NSPoint pos = [self convertPoint: [theEvent locationInWindow] fromView:nil];
    [callback_ onMouseClick: kOGLRightButton withState:kOGLKeyReleased at:&pos];
  } else {
    [super rightMouseUp:theEvent];
  }
}

/**
 *  @name mouseMoved
 *  @brief  Function invoked when left mouse moved
 *  @param[in]  theEvent  Event that trigger callback
 */
-(void)mouseMoved:(NSEvent *)theEvent {
  if ([theEvent type] == NSEventTypeMouseMoved && callback_) {
    NSPoint pos = [self convertPoint: [theEvent locationInWindow] fromView:nil];
    [callback_ onMouseMove:&pos];
  } else {
    [super mouseMoved:theEvent];
  }
}

/**
 *  @name mouseDragged
 *  @brief  Update position when mouse left is dragged
 *  @param[in]  theEvent  Event that trigger callback
 */
-(void) mouseDragged:(NSEvent *)theEvent {
  if ([theEvent type] == NSEventTypeLeftMouseDragged && callback_) {
    NSPoint pos = [self convertPoint: [theEvent locationInWindow] fromView:nil];
    [callback_ onMouseClick: kOGLLeftButton withState:kOGLKeyPressed at:&pos];
  } else {
    [super mouseDragged:theEvent];
  }
}

/**
 *  @name mouseDragged
 *  @brief  Update position when mouse left is dragged
 *  @param[in]  theEvent  Event that trigger callback
 */
-(void) rightMouseDragged:(NSEvent *)theEvent {
  if ([theEvent type] == NSEventTypeRightMouseDragged && callback_) {
    NSPoint pos = [self convertPoint: [theEvent locationInWindow] fromView:nil];
    [callback_ onMouseClick: kOGLRightButton withState:kOGLKeyPressed at:&pos];
  } else {
    [super rightMouseDragged:theEvent];
  }
}

#pragma mark -
#pragma mark Keyboard Event

/**
 *  @name keyDown
 *  @brief  Callback invoked when a key is pressed
 *  @param[in]  theEvent  Event that trigger callback
 */
-(void) keyDown:(NSEvent *)theEvent {
  if ([theEvent type] == NSEventTypeKeyDown && callback_) {
    char ch = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];
    [callback_ onKeyboard:ch withState:kOGLKeyPressed];
  } else {
    [super keyDown:theEvent];
  }
}

/**
 *  @name keyDown
 *  @brief  Callback invoked when a key is pressed
 *  @param[in]  theEvent  Event that trigger callback
 */
-(void) keyUp:(NSEvent *)theEvent {
  if ([theEvent type] == NSEventTypeKeyUp && callback_) {
    char ch = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];
    [callback_ onKeyboard:ch withState:kOGLKeyReleased];
  } else {
    [super keyDown:theEvent];
  }
}

#pragma mark -
#pragma mark Accessors

/**
 *  @name updateTexture
 *  @brief  Update texture, push data onto GPU
 */
-(void) updateTexture:(void*)texture {
  [[self openGLContext] makeCurrentContext];
  [renderer_ updateTexture:texture];
}

/**
 *  @name addCallbacks
 *  @brief  Add callback for this view
 *  @param[in]  callback  Wrapper for OGLCallbacks interface
 */
-(void) addCallbacks:(OGLCallbackWrapper*) callback {
  callback_ = callback;
}


@end
