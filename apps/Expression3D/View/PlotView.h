/**
 *  @file   PlotView.h
 *  @brief  View where CorePlot's plot is rendered
 *  @see    Examples : https://github.com/core-plot/core-plot
 *
 *  @author Christophe Ecabert
 *  @date   23/09/16
 *  Copyright © 2016 Christophe Ecabert. All rights reserved.
 */

#import <Cocoa/Cocoa.h>

@protocol PlotViewDelegate <NSObject>

/** Define new view's dimenstion */
-(void) setFrameSize:(NSSize)newSize;

@end

@interface PlotView : NSView {
  @private
  id<PlotViewDelegate> delegate;
}

@property (nonatomic, retain) id<PlotViewDelegate> delegate;

@end
