/**
 *  @file   PlotView.m
 *  @brief  View where CorePlot's plot is rendered
 *  @see    Examples : https://github.com/core-plot/core-plot
 *
 *  @author Christophe Ecabert
 *  @date   23/09/16
 *  Copyright © 2016 Christophe Ecabert. All rights reserved.
 */

#import "PlotView.h"

@implementation PlotView

@synthesize delegate;

#pragma mark -
#pragma mark Initialization

/**
 *  @name initWithFrame
 *  @brief  Constructor
 */
-(id)initWithFrame:(NSRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    
  }
  return self;
}

#pragma mark -
#pragma mark Draw

-(void) drawRect:(NSRect)rect {
}

#pragma mark -
#pragma mark Protocol

-(void)setFrameSize:(NSSize)newSize {
  [super setFrameSize:newSize];
  if (delegate && [delegate respondsToSelector:@selector(setFrameSize:)]) {
    [delegate setFrameSize:newSize];
  }
}

@end
