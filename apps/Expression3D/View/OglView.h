/**
 *  @file   OglView.h
 *  @brief  Custom Cocoa OpenGL view
 *
 *  @author Christophe Ecabert
 *  @date   31/07/16
 *  Copyright © 2016 Christophe Ecabert. All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import "OGLCallbackWrapper.h"

@interface OGLView : NSOpenGLView

#pragma mark -
#pragma mark Initialization

/**
 *  @name nativeRelease
 *  @brief  Release native renderer
 */
-(void) nativeRelease;

#pragma mark -
#pragma mark Usage

/**
 *  @name updateTexture
 *  @brief  Update texture, push data onto GPU
 */
-(void) updateTexture:(void*)texture;

/**
 *  @name addCallbacks
 *  @brief  Add callback for this view
 *  @param[in]  callback  Wrapper for OGLCallbacks interface
 */
-(void) addCallbacks:(OGLCallbackWrapper*) callback;

@end
