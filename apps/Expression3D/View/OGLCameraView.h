//
//  OGLCameraView.h
//  LTS5-Apps
//
//  Created by Christophe Ecabert on 21/09/16.
//  Copyright © 2016 Ecabert Christophe. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface OGLCameraView : NSBox

#pragma mark Property

/** List of NSImageView */
@property NSMutableArray* views;


#pragma mark Initialization

/** 
 *  @name setNumberOfCamera
 *  @brief  Initialiaze for a given number of camera
 *  @param[in]  n_camera      Number of camera to initialize
 *  @param[in]  image_ratio   Image aspect ratio
 */
-(void) setNumberOfCamera:(NSInteger) n_camera withAspectRation:(float) image_ratio;

@end
