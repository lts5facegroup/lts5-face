//
//  CameraView.m
//  LTS5-Apps
//
//  Created by Christophe Ecabert on 21/09/16.
//  Copyright © 2016 Ecabert Christophe. All rights reserved.
//

#import "CameraView.h"

@interface CameraView() {
  /** Number of camera */
  CGFloat n_cam;
  /** Aspect ratio */
  CGFloat ratio;
}

@end



@implementation CameraView

#pragma mark Synthesize

@synthesize views;

#pragma mark Initialization

/**
 *  @name setNumberOfCamera
 *  @brief  Initialiaze for a given number of camera
 *  @param[in]  n_camera  Number of camera to initialize
 */
-(void) setNumberOfCamera:(NSInteger) n_camera
         withAspectRation:(float)image_ratio {
  // Number cam
  n_cam = (float)n_camera;
  // Aspect ratio
  self->ratio = image_ratio;
  // Init array
  self.views = [[NSMutableArray alloc] initWithCapacity:n_camera];
  // Color
  self.borderType = NSNoBorder;
  // Allocate NSImageView
  for (int i = 0; i < n_camera; ++i) {
    // Allocate NSImageView
    NSImageView* view = [[NSImageView alloc] init];
    [view setImageScaling:NSImageScaleProportionallyUpOrDown];
    [view setImageAlignment:NSImageAlignCenter];
    // Add to the view (NSBox)
    [self addSubview:view];
    // Add to the list of view
    [self.views addObject:view];
  }
  // Update
  [self updateViewLocation];
}

-(void) updateViewLocation {
  // Current dim
  NSRect frame = [self frame];
  // ImageView height
  CGFloat view_height = frame.size.height;
  // ImageView width
  CGFloat view_width = ratio * view_height;
  // Define padding
  CGFloat padding = 0.f;
  // Check if dim can fit into current width
  CGFloat n_view = (frame.size.width) / view_width;
  CGPoint view_origin;
  CGFloat space_org_w = 0.f;
  if (n_view < n_cam) {
    // Not enough space, update height, widht
    view_width = 0.9f * frame.size.width / n_cam;
    view_height = view_width / ratio;
    // Define padding
    padding = (frame.size.width - (n_cam * view_width)) / (n_cam - 1.f);
    // Define origin
    view_origin = CGPointMake(0.f,
                              (frame.size.height - view_height)/2.f);
  } else {
    // Define padding
    padding = (frame.size.width - (n_cam * view_width)) / (n_cam + 1.f);
    // Define origin
    view_origin = CGPointMake(padding,
                              (frame.size.height - view_height)/2.f);
  }
  // Define origin spacing
  space_org_w = view_width + padding;
  // Update
  for (int v = 0; v < (int)n_cam; ++v) {
    // View location
    CGRect rect = CGRectMake(view_origin.x + (float)v * space_org_w ,
                             view_origin.y,
                             view_width,
                             view_height);
    // Get view
    NSImageView* view = [self.views objectAtIndex:v];
    view.frame = rect;
    // DEBUG
    [view setWantsLayer:YES];
    [view.layer setBackgroundColor:[NSColor greenColor].CGColor];
    
    // Refresh view
    [view setNeedsDisplay:YES];
  }
}

/** 
 *  @name   viewDidMoveToWindow
 *  @brief  Invoked when view is added to window
 */
-(void)viewDidMoveToWindow {
  // Add notification for resizing
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(viewResized)
                                               name:NSWindowDidResizeNotification
                                             object:nil];
}

/**
 *  @name viewResized
 *  @brief  Callback invoked when window is resized
 */
- (void)viewResized {
  [self updateViewLocation];
  
}


#pragma mark Drawing 

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

@end
