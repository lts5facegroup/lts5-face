//
//  main.m
//  PSADemo
//
//  Created by Christophe Ecabert on 20/09/16.
//  Copyright © 2016 Ecabert Christophe. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
  return NSApplicationMain(argc, argv);
}
