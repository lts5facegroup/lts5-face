//
//  AppDelegate.m
//  PSADemo
//
//  Created by Christophe Ecabert on 20/09/16.
//  Copyright © 2016 Ecabert Christophe. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
  // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
  // Insert code here to tear down your application
}


@end
