/**
 *  @file ExpressionKitType.h
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 22/09/16.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#import <Foundation/Foundation.h>

#pragma mark -
#pragma mark Initialization parameters

/**
 *  @name
 *  @brief  ExpressionKit initialization parameters
 */
@interface ExpressionKitParameter : NSObject

/** Pipeline configuration file path */
@property NSString* configuration_file;
/** Working directory */
@property NSString* working_directory;
/** Tracker path */
@property NSString* face_tracker_model;
/** Detector path */
@property NSString* f_detector_model;
/** Expression model path */
@property NSString* expression_model;

@end
