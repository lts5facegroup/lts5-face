/**
 *  @file   PlotItem.m
 *  @brief  Data source for plot
 *  @see    Examples : https://github.com/core-plot/core-plot
 *
 *  @author Christophe Ecabert
 *  @date   23/09/16
 *  Copyright © 2016 Christophe Ecabert. All rights reserved.
 */

#import "PlotItem.h"

@implementation PlotItem

@synthesize defaultLayerHostingView;
@synthesize graphs;
@synthesize section;
@synthesize title;
@dynamic titleSize;

-(nonnull instancetype)init {
  self = [super init];
  if (self) {
    defaultLayerHostingView = nil;
    graphs  = [[NSMutableArray alloc] init];
    section = @"";
    title   = @"";
  }
  return self;
}

/**
 *  @name
 *  @brief  Add graph to host view
 */
-(void)addGraph:(nonnull CPTGraph *)graph
  toHostingView:(nullable CPTGraphHostingView *)hostingView {
  // Add graph to the list
  [self.graphs addObject:graph];
  if (hostingView) {
    hostingView.hostedGraph = graph;
  }
}

/**
 *  @name
 *  @brief  Add graph
 */
-(void)addGraph:(nonnull CPTGraph *)graph {
  [self addGraph:graph toHostingView:nil];
}

/**
 *  @name
 *  @brief  Release graph
 */
-(void)killGraph {
  // Remove aniation
  [[CPTAnimation sharedInstance] removeAllAnimationOperations];
  // Remove the CPTLayerHostingView
  CPTGraphHostingView *hostingView = self.defaultLayerHostingView;
  if ( hostingView ) {
    [hostingView removeFromSuperview];
    hostingView.hostedGraph      = nil;
    self.defaultLayerHostingView = nil;
  }
  [self.graphs removeAllObjects];
}

/**
 *  @@name
 *  @brief  Add new entry point
 *          Override to add new entry
 */
-(void)addDataEntry:(void* _Nonnull)entry {
}

/**
 *  @name
 *  @brief  Provide title size
 */
-(CGFloat)titleSize {
  return 24.0;
}

/**
 *  @name
 *  @brief  Default padding
 */
-(void)setPaddingDefaultsForGraph:(nonnull CPTGraph *)graph {
  CGFloat boundsPadding = self.titleSize;
  graph.paddingLeft = boundsPadding;
  if ( graph.titleDisplacement.y > CPTFloat(0.0) ) {
    graph.paddingTop = graph.titleTextStyle.fontSize * CPTFloat(2.0);
  } else {
    graph.paddingTop = boundsPadding;
  }
  graph.paddingRight  = boundsPadding;
  graph.paddingBottom = boundsPadding;
}

/**
 *  @name
 *  @brief  Format graphs
 */
-(void)formatAllGraphs {
  CGFloat graphTitleSize = self.titleSize;
  for ( CPTGraph *graph in self.graphs ) {
    // Title
    CPTMutableTextStyle *textStyle = [CPTMutableTextStyle textStyle];
    textStyle.color    = [CPTColor grayColor];
    textStyle.fontName = @"Helvetica-Bold";
    textStyle.fontSize = graphTitleSize;
    graph.title                    = (self.graphs.count == 1 ? self.title : nil);
    graph.titleTextStyle           = textStyle;
    graph.titleDisplacement        = CPTPointMake( 0.0, textStyle.fontSize * CPTFloat(1.5) );
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    
    // Padding
    CGFloat boundsPadding = graphTitleSize;
    graph.paddingLeft = boundsPadding;
    if ( graph.title.length > 0 ) {
      graph.paddingTop = MAX(graph.titleTextStyle.fontSize * CPTFloat(2.0), boundsPadding);
    } else {
      graph.paddingTop = boundsPadding;
    }
    graph.paddingRight  = boundsPadding;
    graph.paddingBottom = boundsPadding;
    
    // Axis labels
    CGFloat axisTitleSize = graphTitleSize * CPTFloat(0.75);
    CGFloat labelSize     = graphTitleSize * CPTFloat(0.5);
    for ( CPTAxis *axis in graph.axisSet.axes ) {
      // Axis title
      textStyle          = [axis.titleTextStyle mutableCopy];
      textStyle.fontSize = axisTitleSize;
      axis.titleTextStyle = textStyle;
      
      // Axis labels
      textStyle          = [axis.labelTextStyle mutableCopy];
      textStyle.fontSize = labelSize;
      axis.labelTextStyle = textStyle;
      
      textStyle          = [axis.minorTickLabelTextStyle mutableCopy];
      textStyle.fontSize = labelSize;
      axis.minorTickLabelTextStyle = textStyle;
    }
    
    // Plot labels
    for ( CPTPlot *plot in graph.allPlots ) {
      textStyle          = [plot.labelTextStyle mutableCopy];
      textStyle.fontSize = labelSize;
      plot.labelTextStyle = textStyle;
    }
    
    // Legend
    CPTLegend *theLegend = graph.legend;
    textStyle          = [theLegend.textStyle mutableCopy];
    textStyle.fontSize = labelSize;
    theLegend.textStyle  = textStyle;
    theLegend.swatchSize = CGSizeMake( labelSize * CPTFloat(1.5), labelSize * CPTFloat(1.5) );
    theLegend.rowMargin    = labelSize * CPTFloat(0.75);
    theLegend.columnMargin = labelSize * CPTFloat(0.75);
    theLegend.paddingLeft   = labelSize * CPTFloat(0.375);
    theLegend.paddingTop    = labelSize * CPTFloat(0.375);
    theLegend.paddingRight  = labelSize * CPTFloat(0.375);
    theLegend.paddingBottom = labelSize * CPTFloat(0.375);
  }
}

-(void)setFrameSize:(NSSize)size {
}

/**
 *  @name
 *  @brief  Render in view
 */
-(void)renderInView:(nonnull NSView*)inView animated:(BOOL)animated {
  // Clear graph
  [self killGraph];
  // Create hosting view
  CPTGraphHostingView* hostingView = [[CPTGraphHostingView alloc]
                                      initWithFrame:inView.bounds];
  [inView addSubview:hostingView];
  hostingView.autoresizingMask = NSViewWidthSizable | NSViewHeightSizable;
  [hostingView setAutoresizesSubviews:YES];
  // Clear data
  [self clearDataEntry];
  // Render
  [self renderInGraphHostingView:hostingView animated:animated];
  // Format
  [self formatAllGraphs];
  self.defaultLayerHostingView = hostingView;
}

/**
 *  @name
 *  @brief  Render graph
 */
-(void)renderInGraphHostingView:(nonnull CPTGraphHostingView *)hostingView
                       animated:(BOOL)animated {
  NSLog(@"PlotItem:renderInLayer: Override me");
}

/**
 *  @name
 *  @brief  Reload data into graph
 */
-(void)reloadData {
  for ( CPTGraph *graph in self.graphs ) {
    [graph reloadData];
  }
}

/**
 *  @@name
 *  @brief  clear all entry point
 *          Override
 */
-(void)clearDataEntry {
  
}



@end
