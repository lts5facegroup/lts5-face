/**
 *  @file   ExpressionKitPlot.m
 *  @brief  Data source for expression plot
 *  @see    Examples : https://github.com/core-plot/core-plot
 *
 *  @author Christophe Ecabert
 *  @date   23/09/16
 *  Copyright © 2016 Christophe Ecabert. All rights reserved.
 */

#import <Foundation/Foundation.h>

#import "ExpressionKitPlot.h"

static const NSUInteger kMaxDataPoints = 302;

@interface ExpressionKitPlot()

//@property (nonatomic, readwrite, strong, nonnull) CPTMutableNumberArray *plotData;
@property (nonatomic, readwrite, strong) NSMutableArray<NSArray*> *plotData;
@property (nonatomic, readwrite, assign) NSUInteger currentIndex;
@property (nonatomic) NSArray* colors;
@property (nonatomic) NSArray* labels;

@end

@implementation ExpressionKitPlot

@synthesize plotData;
@synthesize currentIndex;

#pragma mark -
#pragma mark Initialization

/**
 *  @name
 *  @brief  Init
 */
-(nonnull instancetype) init {
  self = [super init];
  if (self) {
    plotData = [[NSMutableArray alloc] initWithCapacity:kMaxDataPoints];
    //self.title = @"ExpressionKit Plot";
    self.section = @"Line Plots";
    
    // Init colors
    self.colors = @[[CPTColor colorWithComponentRed:0.043f
                                              green:0.141f
                                               blue:0.984f
                                              alpha:1.f],   // Anger
                    [CPTColor colorWithComponentRed:0.243f
                                              green:0.604f
                                               blue:0.247f
                                              alpha:1.f],   // Disgust
                    [CPTColor colorWithComponentRed:0.992f
                                              green:0.502f
                                               blue:0.509f
                                              alpha:1.f],   // Fear
                    [CPTColor colorWithComponentRed:0.141f
                                              green:0.757f
                                               blue:0.757f
                                              alpha:1.f],   // Happy
                    [CPTColor colorWithComponentRed:0.741f
                                              green:0.106f
                                               blue:0.741f
                                              alpha:1.f],   // Neutral
                    [CPTColor colorWithComponentRed:0.745f
                                              green:0.744f
                                               blue:0.157f
                                              alpha:1.f],   // Sad
                    [CPTColor colorWithComponentRed:0.098f
                                              green:0.098f
                                               blue:0.098f
                                              alpha:1.f]];  // Surprise
    // Init labels
    self.labels = @[@"Anger",
                    @"Disgust",
                    @"Fear",
                    @"Happy",
                    @"Neutral",
                    @"Sad",
                    @"Surprise"];
  }
  return self;
}

#pragma mark -
#pragma mark Display

/**
 *  @name
 *  @brief  Render graph
 */
-(void)renderInGraphHostingView:(nonnull CPTGraphHostingView *)hostingView
                       animated:(BOOL)animated {
  // Get dim
  CGRect bounds = hostingView.bounds;
  
  // Add graph
  CPTGraph* graph = [[CPTXYGraph alloc]initWithFrame:bounds];
  [self addGraph:graph toHostingView:hostingView];
  
  // Set padding
  graph.plotAreaFrame.paddingTop = self.titleSize * CPTFloat(0.1);
  graph.plotAreaFrame.paddingRight = self.titleSize * CPTFloat(2.25);
  graph.plotAreaFrame.paddingBottom = self.titleSize * CPTFloat(0.1);
  graph.plotAreaFrame.paddingLeft = self.titleSize * CPTFloat(0.2);
  graph.plotAreaFrame.masksToBorder = NO;
  
  // Gridline add here if needed
  CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
  majorGridLineStyle.lineWidth = 0.75;
  majorGridLineStyle.lineColor = [[CPTColor colorWithGenericGray:CPTFloat(0.2)] colorWithAlphaComponent:CPTFloat(0.75)];
  
  // Axes
  // X axis
  CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
  CPTXYAxis *x          = axisSet.xAxis;
  x.labelingPolicy        = CPTAxisLabelingPolicyAutomatic;
  x.orthogonalPosition    = @0.0;
  //x.majorGridLineStyle    = majorGridLineStyle;
  //x.minorGridLineStyle    = minorGridLineStyle;
  x.minorTicksPerInterval = 9;
  x.labelOffset           = self.titleSize * CPTFloat(0.005);
  x.title                 = @"Time";
  x.titleOffset           = self.titleSize * CPTFloat(0.1);
  
  NSNumberFormatter *labelFormatter = [[NSNumberFormatter alloc] init];
  labelFormatter.numberStyle = NSNumberFormatterNoStyle;
  x.labelFormatter           = labelFormatter;
  
  CPTXYAxis *y = axisSet.yAxis;
  y.labelingPolicy        = CPTAxisLabelingPolicyAutomatic;
  y.labelTextStyle = nil;
  y.orthogonalPosition    = @0.0;
  y.majorGridLineStyle    = majorGridLineStyle;
  y.preferredNumberOfMajorTicks = 7;
  //y.minorGridLineStyle    = minorGridLineStyle;
  y.minorTicksPerInterval = 1;
  y.majorTickLineStyle = nil;   // Hide major ticks
  y.minorTickLineStyle = nil;
  y.labelOffset           = self.titleSize * CPTFloat(0.005);
  y.title                 = @"Expression";
  y.titleOffset           = self.titleSize * CPTFloat(0.2);
  y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:0.0];
  
  // Rotate the labels by 45 degrees, just to show it can be done.
  //x.labelRotation = CPTFloat(M_PI_4);
  
  // Create the plot
  for (NSInteger i = 0; i < [self.colors count]; ++i) {
    CPTScatterPlot *dataSourceLinePlot = [[CPTScatterPlot alloc] init];
    dataSourceLinePlot.identifier     = [NSString stringWithFormat:@"%ld", (long)i];
    dataSourceLinePlot.cachePrecision = CPTPlotCachePrecisionDouble;
    
    CPTMutableLineStyle *lineStyle = [dataSourceLinePlot.dataLineStyle mutableCopy];
    lineStyle.lineWidth              = 2.0;
    lineStyle.lineColor              = self.colors[i];
    dataSourceLinePlot.dataLineStyle = lineStyle;
    
    dataSourceLinePlot.identifier = self.labels[i];
    
    dataSourceLinePlot.dataSource = self;
    [graph addPlot:dataSourceLinePlot];
  }
  
  // Plot space
  CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
  plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:@0.0 length:@(kMaxDataPoints - 2)];
  plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:@0.0 length:@7.0];
  
  // Add legend
  graph.legend                 = [CPTLegend legendWithGraph:graph];
  //graph.legend.fill            = [CPTFill fillWithColor:[CPTColor lightGrayColor]];
  //graph.legend.borderLineStyle = x.axisLineStyle;
  graph.legend.cornerRadius    = 1.0;
  graph.legend.numberOfRows    = 7;
  graph.legendAnchor           = CPTRectAnchorRight;
  //graph.legendDisplacement     = CGPointMake( self.titleSize * CPTFloat(0.25), 0.f );

  CPTMutableTextStyle *textStyle = [CPTMutableTextStyle textStyle];
  textStyle.fontSize = CPTFloat(40.f);
  graph.legend.textStyle = textStyle;
  
  
}


#pragma mark -
#pragma mark Add data

/**
 *  @@name
 *  @brief  Add new entry point
 *          Override to add new entry
 */
-(void)addDataEntry:(void* _Nonnull)entry {
  // Get value
  NSMutableArray* array = (__bridge NSMutableArray*)entry;
  
  NSArray* val = [NSArray arrayWithArray:array];
  
  // Add to plot
  CPTGraph* theGraph = self.graphs[0];
  CPTPlotArray* plots = [theGraph allPlots];
  if (plots) {
    if ( self.plotData.count >= kMaxDataPoints ) {
      [self.plotData removeObjectAtIndex:0];
      for(CPTPlot* p in plots) {
        [p deleteDataInIndexRange:NSMakeRange(0, 1)];
      }
    }
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)theGraph.defaultPlotSpace;
    NSUInteger location       = (self.currentIndex >= kMaxDataPoints ?
                                 self.currentIndex - kMaxDataPoints + 2 :
                                 0);
    
    CPTPlotRange *oldRange = [CPTPlotRange
                              plotRangeWithLocation:@((location > 0) ?
                              (location - 1) :
                              0)
                              length:@(kMaxDataPoints - 2)];
    CPTPlotRange *newRange = [CPTPlotRange plotRangeWithLocation:@(location)
                                                          length:@(kMaxDataPoints - 2)];
    
    [CPTAnimation animate:plotSpace
                 property:@"xRange"
            fromPlotRange:oldRange
              toPlotRange:newRange
                 duration:CPTFloat(0.1)];
    self.currentIndex++;
    
    // Add value
    [self.plotData addObject:val];
    for (CPTPlot* p in plots) {
      [p insertDataAtIndex:self.plotData.count - 1 numberOfRecords:1];
    }
  }
}

/**
 *  @@name
 *  @brief  clear all entry point
 *          Override
 */
-(void)clearDataEntry {
  [self.plotData removeAllObjects];
  self.currentIndex = 0;
}

#pragma mark -
#pragma mark Plot Data Source Methods

-(NSUInteger)numberOfRecordsForPlot:(nonnull CPTPlot *)plot {
  return self.plotData.count;
}

-(NSNumber*)numberForPlot:(nonnull CPTPlot *)plot
                      field:(NSUInteger)fieldEnum
                recordIndex:(NSUInteger)index {
  NSNumber *num = nil;
  NSInteger serie_idx = [self.labels indexOfObject:(NSString*)plot.identifier];
  switch ( fieldEnum ) {
    case CPTScatterPlotFieldX:
      num = @(index + self.currentIndex - self.plotData.count);
      break;
      
    case CPTScatterPlotFieldY:
      num = [self.plotData[index] objectAtIndex:serie_idx];
      break;
      
    default:
      break;
  }
  return num;
}

#pragma mark - 
#pragma mark Legend delegate

-(void)legend:(nonnull CPTLegend *)legend
legendEntryForPlot:(nonnull CPTPlot *)plot
wasSelectedAtIndex:(NSUInteger)idx {
  NSLog(@"legend");
}

@end
