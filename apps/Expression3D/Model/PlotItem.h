/**
 *  @file   PlotItem.h
 *  @brief  Data source for plot
 *  @see    Examples : https://github.com/core-plot/core-plot
 *
 *  @author Christophe Ecabert
 *  @date   23/09/16
 *  Copyright © 2016 Christophe Ecabert. All rights reserved.
 */

#import <Foundation/Foundation.h>
#import <CorePlot.h>

@interface PlotItem : NSObject

@property (nonatomic,readwrite,strong,nullable)CPTGraphHostingView *defaultLayerHostingView;
@property (nonatomic,readwrite,strong,nonnull)NSMutableArray<__kindof CPTGraph *> *graphs;
@property (nonatomic,readwrite,strong,nonnull)NSString *section;
@property (nonatomic,readwrite,strong,nonnull)NSString *title;
@property (nonatomic, readonly) CGFloat titleSize;

#pragma mark -
#pragma mark Initialization

/** 
 *  @name setFrameSize
 *  @brief  update dimension
 */
-(void)setFrameSize:(NSSize)size;

/**
 *  @name
 *  @brief  Release graph
 */
-(void)killGraph;

/**
 *  @name
 *  @brief  Add graph to host view
 */
-(void)addGraph:(nonnull CPTGraph *)graph toHostingView:(nullable CPTGraphHostingView *)hostingView;

/**
 *  @name
 *  @brief  Add graph
 */
-(void)addGraph:(nonnull CPTGraph *)graph;

#pragma mark -
#pragma mark Display

/**
 *  @name
 *  @brief  Render graph
 */
-(void)renderInView:(nonnull NSView*)inView animated:(BOOL)animated;

/**
 *  @name
 *  @brief  Render graph
 */
-(void)renderInGraphHostingView:(nonnull CPTGraphHostingView *)hostingView
                       animated:(BOOL)animated;

/**
 *  @name
 *  @brief  Format graphs
 */
-(void)formatAllGraphs;

/**
 *  @name
 *  @brief  Reload data into graph
 */
-(void)reloadData;

#pragma mark -
#pragma mark Add data

/**
 *  @@name
 *  @brief  Add new entry point
 *          Override to add new entry
 */
-(void)addDataEntry:(void* _Nonnull)entry;

/**
 *  @@name
 *  @brief  clear all entry point
 *          Override
 */
-(void)clearDataEntry;



@end
