/**
 *  @file   ExpressionKitPlot.h
 *  @brief  Data source for expression plot
 *  @see    Examples : https://github.com/core-plot/core-plot
 *
 *  @author Christophe Ecabert
 *  @date   23/09/16
 *  Copyright © 2016 Christophe Ecabert. All rights reserved.
 */

#import "PlotItem.h"

@interface ExpressionKitPlot : PlotItem<CPTPlotDataSource>

@end
