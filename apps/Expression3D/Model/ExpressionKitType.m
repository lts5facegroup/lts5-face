/**
 *  @file ExpressionKitType.m
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 22/09/16.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#import "ExpressionKitType.h"

@implementation ExpressionKitParameter

/* Pipeline configuration file path */
@synthesize configuration_file;
/* Working directory */
@synthesize working_directory;
/* Tracker path */
@synthesize face_tracker_model;
/* Detector path */
@synthesize f_detector_model;
/* Expression model path */
@synthesize expression_model;

@end
