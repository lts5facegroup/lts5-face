/**
 *  @file ExpressionKitWrapper.mm
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 22/09/16.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#import "ExpressionKitWrapper.h"
#import "OglView.h"

#import "lts5/face_reconstruction/face_synthesis_pipeline.hpp"
#import "lts5/face_tracker/sdm_tracker.hpp"
#import "lts5/facial_analysis/expression_analysis.hpp"

@interface ExpressionKitWrapper() {
  /** Reconstruction pipeline */
  LTS5::FaceSynthesisPipeline* pipeline;
  /** Reconstruction parameters */
  LTS5::FaceSynthesisPipeline::FaceSynthesisPipelineParameters params;
  /** Processing configuration */
  LTS5::FaceSynthesisPipeline::PipelineProcessConfiguration process_configuration;
  
  /** Face tracker */
  LTS5::SdmTracker* tracker;
  /** Tracked shape */
  cv::Mat shape;
  
  /** Expression detector */
  LTS5::ExpressionAnalysis* exp_analyzer;
  /** Expression score */
  std::vector<double> score;
  /** Expression labels */
  std::vector<std::string> labels;
}

/**
 *  @name NSImageFromMat
 *  @brief  Convert cv::Mat into NSImage*
 */
NSImage* NSImageFromMat(const cv::Mat& image);

@end

@implementation ExpressionKitWrapper

#pragma mark -
#pragma mark Initialization

/**
 *  @name initWithParameters
 *  @fn +(void) FaceSynthesisPipelineWrapper
 *  @brief  Constructor
 *  @param[in]  parameters  Configuration of the pipeline
 */
-(instancetype) initWithParameters : (ExpressionKitParameter*) parameters {
  self = [super init];
  if (self) {
    // Init data struct
    params.pipeline_xml_config_file = std::string([parameters.configuration_file UTF8String]);
    params.working_directory = std::string([parameters.working_directory UTF8String]);
    // Create 3D pipeline
    try {
      // 3D
      pipeline = new LTS5::FaceSynthesisPipeline(params);
      process_configuration.has_synthetic_data = false;
      
      
      // Face tracker
      tracker = new LTS5::SdmTracker();
      std::string model([parameters.face_tracker_model UTF8String]);
      std::string f_model([parameters.f_detector_model UTF8String]);
      if(tracker->Load(model, f_model)) {
        throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErr,
                                 "Unable to load face tracker",
                                 __PRETTY_FUNCTION__ );
      }
      tracker->set_score_threshold(-0.9);
      
      // Expression modules
      exp_analyzer = new LTS5::ExpressionAnalysis();
      std::string exp_model([parameters.expression_model UTF8String]);
      if (exp_analyzer->Load(exp_model)) {
        throw LTS5::ProcessError(LTS5::ProcessError::ProcessErrorEnum::kErr,
                                 "Unable to load expression detector",
                                 __PRETTY_FUNCTION__ );
      }
    } catch (const LTS5::ProcessError& e) {
      // Rethrow
      NSString* msg = [[NSString alloc] initWithUTF8String:e.what()];
      @throw [NSException exceptionWithName:@"Unable to create 3D pipeline"
                                     reason:msg
                                   userInfo:nil];
    }
  }
  return self;
}

/**
 *  @name nativeRelease
 *  @fn -(void) nativeRelease
 *  @brief  Destructor
 */
-(void) nativeRelease {
  // Clean up
  if (pipeline) {
    delete pipeline;
    pipeline = nullptr;
  }
  if (tracker) {
    delete tracker;
    tracker = nullptr;
  }
  if (exp_analyzer) {
    delete exp_analyzer;
    exp_analyzer = nullptr;
  }
}

#pragma mark -
#pragma mark Process

/**
 *  @name generateFrontalImage
 *  @fn -(int) generateFrontalImage
 *  @brief  Launch pipeline to reconstruct 3D face
 */
-(int) generateFrontalImage {
  return pipeline->Process(process_configuration);
}

/**
 *  @name updateWithInputImage
 *  @brief  Update UI view image from input
 *  @param[in]  views List of view to update
 */
-(void) updateWithInputImage:(NSMutableArray*)views {
  const std::vector<cv::Mat> cam_input = pipeline->get_frame();
  // This will wait to finish
  dispatch_sync(dispatch_get_main_queue(), ^{
    for (int i = 0; i < [views count]; ++i) {
      @autoreleasepool {
        // Get view
        OGLView* v = [views objectAtIndex:i];
        // Convert
        [v updateTexture:(void*)&cam_input[i]];        
        [v setNeedsDisplay:YES];
      }
    }
  });
}

/**
 *  @name updateWithReconstruction
 *  @brief  Update UI view image from reconstructed image
 *  @param[in]  view  Where to draw reconstructed image
 */
-(void) updateWithReconstruction:(OGLView*) view {
  const cv::Mat& recon = pipeline->get_frontal_image();
  // This will wait to finish
  dispatch_sync(dispatch_get_main_queue(), ^{
    @autoreleasepool {
      // Convert
      [view updateTexture:(void*)&recon];
      [view setNeedsDisplay:YES];
    }
  });
}

/**
 *  @name detectExpression
 *  @fn -(NSInteger) detectExpression
 *  @brief  Use frontal image to detect expression
 */
-(int) detectExpression:(NSMutableArray*)expression_score {
  int err = -1;
  const cv::Mat& recon = pipeline->get_frontal_image();
  err = tracker->Track(recon, &shape);
  if (!err && !shape.empty()) {
    exp_analyzer->Predict(recon, shape, &score);
    
    double max_val = *std::max_element(score.begin(), score.end());
    for (int i = 0; i < score.size(); ++i) {
      double v = score[i];
      if (v < 0.0 && v != max_val) {
        v = 0.0;
      }
      v += (double)i;
      [expression_score insertObject:[NSNumber numberWithDouble:v]
                             atIndex:i];
    }
  } else {
    std::cout << "No tracking on frontal image" << std::endl;
    if (labels.empty()) {
      exp_analyzer->get_expression_labels(&labels);
    }
    for (int i = 0; i < labels.size(); ++i) {
      [expression_score insertObject:[NSNumber numberWithInt:i]
                             atIndex:i];
    }
  }
  return err;
}

#pragma mark -
#pragma mark Converter

/**
 *  @name NSImageFromMat
 *  @brief  Convert cv::Mat into NSImage*
 */
NSImage* NSImageFromMat(const cv::Mat& image) {
  // Access data
  NSData* data = [NSData dataWithBytes:image.data
                                length:image.elemSize() * image.total()];
  // Create color space
  CGColorSpaceRef colorSpace;
  if (image.channels() == 1) {
    colorSpace = CGColorSpaceCreateDeviceGray();
  } else {
    colorSpace = CGColorSpaceCreateDeviceRGB();
  }
  // Data provider
  CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
  // Creating CGImage from cv::Mat
  CGImageRef imageRef = CGImageCreate(image.cols,
                                      image.rows,
                                      8,
                                      8 * image.elemSize(),
                                      image.step.p[0],
                                      colorSpace,
                                      kCGImageAlphaNone|
                                      kCGBitmapByteOrderDefault,
                                      provider,
                                      NULL,
                                      false,
                                      kCGRenderingIntentDefault
                                      );
  // Getting NSImage from CGImage
  NSImage *finalImage = [[NSImage alloc] initWithCGImage:imageRef size:NSZeroSize];
  CGImageRelease(imageRef);
  CGDataProviderRelease(provider);
  CGColorSpaceRelease(colorSpace);
  return finalImage;
}

#pragma mark -
#pragma mark Accessors

/**
 *  @name   getNumberCamera
 *  @fn -(NSInteger) getNumberCamera
 *  @brief  Indicate how many camera are used to reconstruct
 *  @@return Number of camera
 */
-(NSInteger) getNumberCamera {
  return pipeline->get_number_of_view();
}

/**
 *  @name   getCurrentFrameIndex
 *  @fn -(NSInteger) getCurrentFrameIndex
 *  @brief  Provide the index of the current frame being processed
 *  @return Frame index
 */
-(NSInteger) getCurrentFrameIndex {
  return pipeline->get_current_frame_index();
}

/**
 *  @name   getTotalNumberOfFrame
 *  @fn -(NSInteger) getTotalNumberOfFrame
 *  @brief  Gives the number of image to process
 *  @return Number of frame
 */
-(NSInteger) getTotalNumberOfFrame {
  return pipeline->get_total_number_of_frame();
}


@end
