/**
 *  @file ExpressionKitWrapper.h
 *  LTS5-Apps
 *
 *  Created by Christophe Ecabert on 22/09/16.
 *  Copyright (c) 2014 Ecabert Christophe. All rights reserved.
 */

#import <Foundation/Foundation.h>
#import "ExpressionKitType.h"
#import "OglView.h"

@interface ExpressionKitWrapper : NSObject

#pragma mark -
#pragma mark Initialization

/**
 *  @name initWithParameters
 *  @fn +(void) FaceSynthesisPipelineWrapper
 *  @brief  Constructor
 *  @param[in]  parameters  Configuration of the pipeline
 */
-(instancetype) initWithParameters : (ExpressionKitParameter*) parameters;

/**
 *  @name nativeRelease
 *  @fn -(void) nativeRelease
 *  @brief  Destructor
 */
-(void) nativeRelease;

#pragma mark -
#pragma mark Process

/**
 *  @name generateFrontalImage
 *  @fn -(int) generateFrontalImage
 *  @brief  Launch pipeline to reconstruct 3D face
 */
-(int) generateFrontalImage;

/**
 *  @name updateWithInputImage
 *  @brief  Update UI view image from input
 *  @param[in]  views List of view to update
 */
-(void) updateWithInputImage:(NSMutableArray*)views;

/**
 *  @name updateWithReconstruction
 *  @brief  Update UI view image from reconstructed image
 *  @param[in]  view  Where to draw reconstructed image
 */
-(void) updateWithReconstruction:(OGLView*) view;

/**
 *  @name detectExpression
 *  @fn -(NSInteger) detectExpression
 *  @brief  Use frontal image to detect expression
 */
-(int) detectExpression:(NSMutableArray*)expression_score;

#pragma mark -
#pragma mark Accessors

/**
 *  @name   getNumberCamera
 *  @fn -(NSInteger) getNumberCamera
 *  @brief  Indicate how many camera are used to reconstruct
 *  @@return Number of camera
 */
-(NSInteger) getNumberCamera;

/**
 *  @name   getCurrentFrameIndex
 *  @fn -(NSInteger) getCurrentFrameIndex
 *  @brief  Provide the index of the current frame being processed
 *  @return Frame index
 */
-(NSInteger) getCurrentFrameIndex;

/**
 *  @name   getTotalNumberOfFrame
 *  @fn -(NSInteger) getTotalNumberOfFrame
 *  @brief  Gives the number of image to process
 *  @return Number of frame
 */
-(NSInteger) getTotalNumberOfFrame;

@end
