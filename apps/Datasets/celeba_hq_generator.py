"""
Generate HQ images from CelebA dataset.

See:
  - https://github.com/willylulu/celeba-hq-modified
"""
from os import makedirs as _makedirs
import os.path as _path
import argparse as _argparse
from collections import defaultdict as _defaultdict
import numpy as _np
from zipfile import ZipFile as _ZipFile
import bz2 as _bz2
from PIL import Image as _Image
from cryptography.hazmat.primitives.hashes import SHA256 as _SHA256
from cryptography.hazmat.backends import default_backend as _def_backend
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC as _PBKDF2HMAC
from cryptography.fernet import Fernet as _Fernet
from base64 import urlsafe_b64encode as _urlsafe
from scipy.ndimage import gaussian_filter as _gaussian_filter
from lts5.utils import search_folder as _search_folder
from lts5.utils import init_logger as _init_logger

# Logger
_logger = _init_logger()


def _rot90(v):
  """ Rotate by 90 degree """
  return _np.array([-v[1], v[0]])


def _generator_fn(pargs, fields, idx):
  """
  Generate one samples HQ samples at various resolition
  """

  # Load image
  im_path = _path.join(pargs.celeba_dir, fields['orig_file'][idx])
  img = _Image.open(im_path)

  # Choose oriented crop rectangle.
  lms = fields['lms'][idx]
  eye_avg = (lms[0] + lms[1]) * 0.5 + 0.5
  mouth_avg = (lms[3] + lms[4]) * 0.5 + 0.5
  eye_to_eye = lms[1] - lms[0]
  eye_to_mouth = mouth_avg - eye_avg
  x = eye_to_eye - _rot90(eye_to_mouth)
  x /= _np.hypot(*x)
  x *= max(_np.hypot(*eye_to_eye) * 2.0, _np.hypot(*eye_to_mouth) * 1.8)
  y = _rot90(x)
  c = eye_avg + eye_to_mouth * 0.1
  quad = _np.stack([c - x - y, c - x + y, c + x + y, c + x - y])
  zoom = 1024 / (_np.hypot(*x) * 2)

  # Shrink.
  shrink = int(_np.floor(0.5 / zoom))
  if shrink > 1:
    size = (int(_np.round(float(img.size[0]) / shrink)),
            int(_np.round(float(img.size[1]) / shrink)))
    img = img.resize(size, _Image.ANTIALIAS)
    quad /= shrink
    zoom *= shrink

  # Crop.
  border = max(int(_np.round(1024 * 0.1 / zoom)), 3)
  crop = (int(_np.floor(min(quad[:, 0]))), int(_np.floor(min(quad[:, 1]))),
          int(_np.ceil(max(quad[:, 0]))), int(_np.ceil(max(quad[:, 1]))))
  crop = (max(crop[0] - border, 0), max(crop[1] - border, 0),
          min(crop[2] + border, img.size[0]),
          min(crop[3] + border, img.size[1]))
  if crop[2] - crop[0] < img.size[0] or crop[3] - crop[1] < img.size[1]:
    img = img.crop(crop)
    quad -= crop[0:2]

  # Simulate super-resolution.
  superres = int(_np.exp2(_np.ceil(_np.log2(zoom))))
  if superres > 1:
    img = img.resize((img.size[0] * superres,
                      img.size[1] * superres),
                     _Image.ANTIALIAS)
    quad *= superres
    zoom /= superres

  # Pad.
  pad = (int(_np.floor(min(quad[:, 0]))),
         int(_np.floor(min(quad[:, 1]))),
         int(_np.ceil(max(quad[:, 0]))),
         int(_np.ceil(max(quad[:, 1]))))
  pad = (max(-pad[0] + border, 0),
         max(-pad[1] + border, 0),
         max(pad[2] - img.size[0] + border, 0),
         max(pad[3] - img.size[1] + border, 0))
  if max(pad) > border - 4:
    pad = _np.maximum(pad, int(_np.round(1024 * 0.3 / zoom)))
    img = _np.pad(_np.float32(img),
                  ((pad[1], pad[3]), (pad[0], pad[2]), (0, 0)),
                  'reflect')
    h, w, _ = img.shape
    y, x, _ = _np.mgrid[:h, :w, :1]
    mask = 1.0 - _np.minimum(
      _np.minimum(_np.float32(x) / pad[0], _np.float32(y) / pad[1]),
      _np.minimum(_np.float32(w - 1 - x) / pad[2],
                  _np.float32(h - 1 - y) / pad[3]))
    blur = 1024 * 0.02 / zoom
    img += ((_gaussian_filter(img, [blur, blur, 0]) - img) *
            _np.clip(mask * 3.0 + 1.0, 0.0, 1.0))
    img += (_np.median(img, axis=(0, 1)) - img) * _np.clip(mask, 0.0, 1.0)
    img = _Image.fromarray(_np.uint8(_np.clip(_np.round(img), 0, 255)), 'RGB')
    quad += pad[0:2]

  # Transform.
  img = img.transform((4096, 4096),
                      _Image.QUAD,
                      (quad + 0.5).flatten(),
                      _Image.BILINEAR)
  img = img.resize((1024, 1024), _Image.ANTIALIAS)
  img = _np.asarray(img).transpose(2, 0, 1) # CHW

  # Load delta image and original JPG.
  zname = 'deltas{:05d}.zip'.format(idx - idx%1000)
  fname = 'delta{:05d}.dat'.format(idx)
  with _ZipFile(_path.join(pargs.delta_dir, zname), 'r') as fzip:
    delta_bytes = fzip.read(fname)
  with open(im_path, 'rb') as f:
    orig_bytes = f.read()

  # Decrypt delta image, using original JPG data as decryption key.
  algo = _SHA256()
  backend = _def_backend()
  kdf = _PBKDF2HMAC(algorithm=algo,
                    length=32,
                    salt=bytes(fields['orig_file'][idx], 'ascii'),
                    iterations=100000,
                    backend=backend)
  key = _urlsafe(kdf.derive(orig_bytes))
  delta = _np.frombuffer(_bz2.decompress(_Fernet(key).decrypt(delta_bytes)),
                         dtype=_np.uint8).reshape(3, 1024, 1024)

  # Apply delta image.
  img = img + delta
  img = _np.asarray(img).transpose(1, 2, 0)
  img = _Image.fromarray(img, mode='RGB')
  # Done
  return img


def _generate_samples(pargs):
  """
  Generate multi-resolution samples from CelebA dataset
  :param pargs: Parsed arguments
  """

  if not _path.exists('cache.npy'):

    # Parse image_list.txt
    # ---------------------------------------------------------------------------
    _logger.info('Loading CelebA-HQ deltas from: {}'.format(pargs.delta_dir))
    fname = _path.join(pargs.delta_dir, 'image_list.txt')
    with open(fname, 'r') as f:
      # header
      entries_hdr = f.readline().split()
      entries_type = [int if x.endswith('idx') else str for x in entries_hdr]
      # Parse lines
      fields = _defaultdict(list)
      for line in f:
        entries = line.split()
        for k, entry in enumerate(entries):
          fields[entries_hdr[k]].append(entries_type[k](entry))
    # Scan for deltas zip file
    deltas_zip = _search_folder(pargs.delta_dir, '.zip')
    if len(deltas_zip) != 30:
      msg = 'Expected to find {} zips in {} but found only {}' \
            ' files'.format(30,
                            pargs.delta_dir,
                            len(deltas_zip))
      _logger.error(msg)
      return -1

    # Parse list_landmarks_celeba.txt
    # ---------------------------------------------------------------------------
    _logger.info('Loading CelebA landmarks from: {}'.format(pargs.label_dir))
    fname = _path.join(pargs.label_dir, 'list_landmarks_celeba.txt')
    with open(fname, 'r') as f:
      _ = f.readline() # Drop header
      _ = f.readline() # Drop header
      # Process each line, pick only the entries of interest
      fields['lms'] = [None] * len(fields['idx'])
      cnt = 0
      for line in f:
        entries = line.split()
        im_name = entries[0]
        if im_name in fields['orig_file']:
          # Extract landmarks
          k = fields['orig_file'].index(im_name)
          lms = [float(v) for v in entries[1:]]
          lms = _np.reshape(lms, [5, 2])
          fields['lms'][k] = _np.array(lms, dtype=_np.float32)
          cnt += 1
          if cnt == len(fields['lms']):
            break
  else:
    # process data
    fields = _np.load('cache.npy').item()

  # Create output structure
  if not _path.exists(pargs.output_dir):
    _makedirs(pargs.output_dir)
  if not _path.exists(_path.join(pargs.output_dir, 'res1024')):
    #_makedirs(_path.join(pargs.output_dir, 'res64'))
    #_makedirs(_path.join(pargs.output_dir, 'res128'))
    #_makedirs(_path.join(pargs.output_dir, 'res256'))
    #_makedirs(_path.join(pargs.output_dir, 'res512'))
    _makedirs(_path.join(pargs.output_dir, 'res1024'))

  # Generate all samples
  _logger.info('Generate multi-resolution samples')
  for idx in fields['idx']:
    if idx % 500 == 0:
      _logger.info('\tGenerate {}th sample'.format(idx))
    im_name = _path.join(pargs.output_dir, 'res1024', '{:05d}.jpg'.format(idx))
    if not _path.exists(im_name):
      samples = _generator_fn(pargs=pargs, fields=fields, idx=idx)
      #aidx, aimg64, aimg128, aimg256, aimg512, aimg1024 = samples
      samples.save(im_name)


  a = 0


if __name__ == '__main__':
  # Create parser
  p = _argparse.ArgumentParser(description='CelebA-HQ generator')

  # Input
  p.add_argument('--celeba_dir',
                 type=str,
                 required=True,
                 help='Location where the unaligned CelebA dataset is located')
  # Delta location
  p.add_argument('--delta_dir',
                 type=str,
                 required=True,
                 help='Location where CelebA deltas are located')
  # Labels location
  p.add_argument('--label_dir',
                 type=str,
                 required=True,
                 help='Location where CelebA labels are stored')
  # Output
  p.add_argument('--output_dir',
                 type=str,
                 required=True,
                 help='Location where to dump the generated images')
  # Parse input
  args = p.parse_args()
  # Process
  err_code = _generate_samples(pargs=args)
  exit(err_code)
