/**
 *  @file   face_module.cpp
 *  @brief  Interface for LTS5 processing module
 *
 *  @author Christophe Ecabert / Hua Gao
 *  @date   01/05/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#include "face_module.hpp"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5
{

/**
 *  @name   ReadNote
 *  @brief  Search a key in a given node and output its value if present
 *  @param[in]  node    Node to search in
 *  @param[in]  key     Key to search for
 *  @param[out] value   Value associated for the given key
 */
template<class T>
void ReadNote(const cv::FileNode& node, const std::string& key, T* value);

#pragma mark -
#pragma mark Initialization
/**
 *  @name   FaceModule
 *  @brief  Constructor
 */
FaceModule::FaceModule(void) {
}

/**
 *  @name   ~FaceModule
 *  @brief  Destructor
 */
FaceModule::~FaceModule(void) {
  //Release subpart
  if (face_tracker_) {
    delete face_tracker_;
  }
  if (pose_estimator_) {
    delete pose_estimator_;
  }
  if (emotion_analyzer_) {
    delete emotion_analyzer_;
  }
}

/**
 *  @name Load
 *  @brief  Initialize LTS5 processing module
 *  @param[in]  config_filename   Configuration filename
 *  @return -1 if error, 0 otherwise.
 */
int FaceModule::Load(const std::string& config_filename) {
  //Parse config file
  int error = this->ParseConfiguration(config_filename);

  //Init subpart - SDM
  face_tracker_ = new SdmTracker();
  const bool is_binary = (sdm_model_path_.find(".bin") != std::string::npos);
  std::ifstream::openmode open_flag = (is_binary ?
                                       std::ios_base::in |
                                       std::ios_base::binary :
                                       std::ios_base::in);
  std::ifstream sdm_stream(sdm_model_path_.c_str(),open_flag);
  if (sdm_stream.is_open()) {
    // Ok
    int status = LTS5::ScanStreamForObject(sdm_stream, HeaderObjectType::kSdm);
    if (status == 0) {
      error = face_tracker_->Load(sdm_stream, is_binary,face_detector_path_);
      LTS5::SdmTracker::SdmTrackerParameters tracker_params;
      tracker_params.starting_pass = 1;
      face_tracker_->set_tracker_parameters(tracker_params);
    }

    // Pose estimator
    pose_estimator_ = new PointDistributionModel();
    status = LTS5::ScanStreamForObject(sdm_stream,
                                       HeaderObjectType::kPointDistributionModel);
    error |= (status == 0 ?
              pose_estimator_->Load(sdm_stream, is_binary) :
              -1);

    // Emotion module
    emotion_analyzer_ = new ExpressionAnalysis();
    status = LTS5::ScanStreamForObject(sdm_stream,
                                       LTS5::HeaderObjectType::kExpressionAnalysis);
    error |= (status == 0 ?
              emotion_analyzer_->Load(sdm_stream) :
              -1);
    emotion_analyzer_->get_expression_labels(&emotion_label_);
  } else {
    error = -1;
  }
  // Done
  sdm_stream.close();
  return error;
}

/**
 *  @name   ReadNote
 *  @brief  Search a key in a given node and output its value if present
 *  @param[in]  node    Node to search in
 *  @param[in]  key     Key to search for
 *  @param[out] value   Value associated for the given key
 */
template<class T>
void ReadNode(const cv::FileNode& node, const std::string& key, T* value) {
  if (!node[key].empty()) {
    node[key] >> *value;
  }
}

/**
 *  @name   ParseConfig
 *  @brief  Read configuration file
 *  @param[in]  config_filename   Path to configuration file
 *  @return -1 if error, 0 otherwise
 */
int FaceModule::ParseConfiguration(const std::string& config_filename) {
  int error = -1;
  cv::FileStorage storage(config_filename, cv::FileStorage::READ);
  if (storage.isOpened()) {
    //Ok - Get root node
    cv::FileNode root = storage.getFirstTopLevelNode();
    //Read config
    LTS5::ReadNode(root, "model_path", &sdm_model_path_);
    LTS5::ReadNode(root, "face_detector_path", &face_detector_path_);
    //Done
    error = 0;
  }
  return error;
}

#pragma mark -
#pragma mark Processing

/**
 *  @name   Process
 *  @brief  Run face processing on image
 *  @param[in]  image           Input image
 */
bool FaceModule::Process(const cv::Mat& image)
{
  //Color image ?
  search_region_.x = 0;
  search_region_.y = 0;
  search_region_.width = image.cols;
  search_region_.height = image.rows;
  if (image.channels() != 1)
  {
    cv::cvtColor(image, working_img_, CV_BGR2GRAY);
  } else {
    working_img_ = image;
  }

  //Call tracker
  face_tracker_->Track(working_img_, &face_landmarks_);
  if (!face_tracker_->is_tracking()) {
    //Ko
    face_landmarks_ = cv::Mat();
    head_pose_[0] = -1.0;
    head_pose_[1] = -1.0;
    head_pose_[2] = -1.0;
    return false;
  } else {
    //Estimate 3D pose
    pose_estimator_->CalcPoseAngle(face_landmarks_,
                                   &head_pose_[0],
                                   &head_pose_[1],
                                   &head_pose_[2]);
    return true;
  }
}

/**
 *  @name   Process
 *  @brief  Run face processing on image
 *  @param[in]  image           Input image
 *  @param[in]  search_region   Region of interest
 */
bool FaceModule::Process(const cv::Mat& image,
                         const cv::Rect& search_region) {
  //Color image ?
  search_region_ = search_region;
  if (image.channels() != 1)
  {
    cv::cvtColor(image(search_region), working_img_, CV_BGR2GRAY);
  } else {
    working_img_ = image(search_region);
  }

  // Move shape into image reference
  int n_point = face_landmarks_.rows / 2;
  for (int i = 0; i < n_point; ++i) {
    face_landmarks_.at<double>(i) -= search_region.x;
    face_landmarks_.at<double>(i + n_point) -= search_region.y;
  }

  //Call tracker
  face_tracker_->Track(working_img_, &face_landmarks_);

  // Goes back into image referential
  n_point = face_landmarks_.rows / 2;
  for (int i = 0; i < n_point; ++i) {
    face_landmarks_.at<double>(i) += search_region.x;
    face_landmarks_.at<double>(i + n_point) += search_region.y;
  }

  if (!face_tracker_->is_tracking())
  {
    //Ko
    face_landmarks_ = cv::Mat();
    head_pose_[0] = -1.0;
    head_pose_[1] = -1.0;
    head_pose_[2] = -1.0;
    return false;
  } else {
    //Estimate 3D pose
    pose_estimator_->CalcPoseAngle(face_landmarks_,
                                   &head_pose_[0],
                                   &head_pose_[1],
                                   &head_pose_[2]);
    return true;
  }
}

/**
 *  @name   Process
 *  @brief  Run face processing on image
 *  @param[in]  image           Input image
 *  @param[in]  search_region   Region of interest
 *  @param[in]  head_roll       Head orientation in radian (in plane)
 */
bool FaceModule::Process(const cv::Mat& image,
                         const cv::Rect& search_region,
                         const double& head_roll)
{
  //Color image ?
  search_region_.x = 0;
  search_region_.y = 0;
  search_region_.width = image.cols;
  search_region_.height = image.rows;
  if (image.channels() != 1) {
    cv::cvtColor(image, working_img_, CV_BGR2GRAY);
  } else {
    working_img_ = image.clone();
  }

  if (!face_tracker_->is_tracking()) {
    //Compensate for in plane rotation by applying inverse transformation on
    //input image in order to have vertical head pose.
    cv::Mat rotation_mat = (cv::Mat_<double>(2,3) <<
                            std::cos(head_roll),
                            -std::sin(head_roll),
                            search_region.x + search_region.width/2,
                            std::sin(head_roll),
                            std::cos(head_roll),
                            search_region.y + search_region.height/2);


    rotation_mat.at<double>(0,2) -= rotation_mat.at<double>(0,0) *
    search_region.width/2 +
    rotation_mat.at<double>(0,1) *
    search_region.height/2;
    rotation_mat.at<double>(1,2) -= rotation_mat.at<double>(1,0) *
    search_region.width/2 +
    rotation_mat.at<double>(1,1) *
    search_region.height/2;

    cv::Mat inverse_transformation;
    cv::invertAffineTransform(rotation_mat, inverse_transformation);
    cv::Mat cropped_image;
    cv::warpAffine(working_img_,
                   cropped_image,
                   inverse_transformation,
                   cv::Size(search_region.width,search_region.height));

    face_tracker_->Track(cropped_image, &face_landmarks_);
    if (!face_landmarks_.empty()) {
      face_landmarks_ = LTS5::TransformShape(rotation_mat, face_landmarks_);
    }
  } else {
    //Call tracker
    face_tracker_->Track(working_img_, &face_landmarks_);
  }

  if (!face_tracker_->is_tracking())
  {
    //Ko
    face_landmarks_ = cv::Mat();
    head_pose_[0] = -1.0;
    head_pose_[1] = -1.0;
    head_pose_[2] = -1.0;
    return false;
  } else {
    //Estimate 3D pose
    pose_estimator_->CalcPoseAngle(face_landmarks_,
                                   &head_pose_[0],
                                   &head_pose_[1],
                                   &head_pose_[2]);
    return true;
  }
}

/*
 *  @name   ProcessEmotion
 *  @brief  Run face processing on image
 *  @param[out] emotion_label Detected emotion
 */
void FaceModule::ProcessEmotion(std::string* emotion_label) {
  if (face_tracker_->is_tracking()) {
    // Adjust reference if needed
    cv::Mat landmarks = face_landmarks_.clone();
    if (!(search_region_.x == 0 && search_region_.y == 0 && search_region_.width == working_img_.cols && search_region_.height == working_img_.rows)) {
      int n_point = face_landmarks_.rows / 2;
      for (int i = 0; i < n_point; ++i) {
        landmarks.at<double>(i) -= search_region_.x;
        landmarks.at<double>(i + n_point) -= search_region_.y;
      }
    };
    // Run detection
    emotion_analyzer_->Predict(working_img_, landmarks, &emotion_score_);
    // Provide corresponding label
    int idx = static_cast<int>(std::distance(emotion_score_.begin(),
                                             std::max_element(emotion_score_.begin(),
                                                              emotion_score_.end())));
    *emotion_label = emotion_label_[idx];
  } else {
    *emotion_label = "N/A";
  }
}

/**
 *  @name   DrawShape
 *  @brief  Draw landmarks
 *  @param[in]  image    Input image
 *  @param[in]  shape    Input landmarks
 *  @param[in]  color    BGR color
 *  @return
 */
cv::Mat FaceModule::DrawShape(const cv::Mat& image,
                              const cv::Mat& shape,
                              const cv::Scalar& color) {
  return LTS5::SdmTracker::draw_shape(image, shape, color);
}

/**
 *  @name   DrawPose
 *  @brief  Draw pose orientation
 *  @param[in]  image    Input image
 *  @return
 */
cv::Mat FaceModule::DrawPose(const cv::Mat& image) {
  cv::Mat ret = image.clone();
  double alpha = head_pose_[0];
  double beta = head_pose_[1];
  double gamma = head_pose_[2];

  cv::Mat points = (cv::Mat_<double>(4, 3) <<
                    0,  0,  0,
                    1,  0,  0,
                    0, -1,  0,
                    0,  0,  1);

  cv::Mat rot_mat = cv::Mat(3, 3, CV_64FC1);

  rot_mat.at<double>(0, 0) = cos(alpha)*cos(beta);
  rot_mat.at<double>(0, 1) = cos(alpha)*sin(beta)*sin(gamma) - sin(alpha)*cos(gamma);
  rot_mat.at<double>(0, 2) = cos(alpha)*sin(beta)*cos(gamma) + sin(alpha)*sin(gamma);
  rot_mat.at<double>(1, 0) = sin(alpha)*cos(beta);
  rot_mat.at<double>(1, 1) = sin(alpha)*sin(beta)*sin(gamma) + cos(alpha)*cos(gamma);
  rot_mat.at<double>(1, 2) = sin(alpha)*sin(beta)*cos(gamma) - cos(alpha)*sin(gamma);
  rot_mat.at<double>(2, 0) = -sin(beta);
  rot_mat.at<double>(2, 1) = cos(beta)*sin(gamma);
  rot_mat.at<double>(2, 2) = cos(beta)*cos(gamma);

  cv::Mat _pose_points = points * rot_mat;

  //adaptive
  float rate = image.rows / 480.0;
  float offset_x = image.cols - 150 * rate;
  float offset_y = image.rows - 100 * rate;
  float scale = 100 * rate;

  cv::Point2f p0(_pose_points.at<double>(0, 0) * scale + offset_x,
                 _pose_points.at<double>(0, 1) * scale + offset_y);
  cv::Point2f p1(_pose_points.at<double>(1, 0) * scale + offset_x,
                 _pose_points.at<double>(1, 1) * scale + offset_y);
  cv::Point2f p2(_pose_points.at<double>(2, 0) * scale + offset_x,
                 _pose_points.at<double>(2, 1) * scale + offset_y);
  cv::Point2f p3(_pose_points.at<double>(3, 0) * scale + offset_x,
                 _pose_points.at<double>(3, 1) * scale + offset_y);
  cv::line(ret, p0, p1, cv::Scalar(255, 0, 0), 4);
  cv::line(ret, p0, p2, cv::Scalar(0, 255, 0), 4);
  cv::line(ret, p0, p3, cv::Scalar(0, 0, 255), 4);
  return ret;
}
}
