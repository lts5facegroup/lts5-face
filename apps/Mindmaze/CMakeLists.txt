CMAKE_MINIMUM_REQUIRED(VERSION 2.8 FATAL_ERROR)
PROJECT(LTS5FaceTracker)

INCLUDE_DIRECTORIES(".")
set( CMAKE_BUILD_TYPE Release CACHE STRING "Choose the type of build, options are: None Debug Release RelWithDebInfo
MinSizeRel." FORCE)
SET(LIBRARY_OUTPUT_PATH ${LTS5FaceTracker_BINARY_DIR}/lib CACHE PATH "Output directory for the libraries")
SET(EXECUTABLE_OUTPUT_PATH ${LTS5FaceTracker_BINARY_DIR}/bin CACHE PATH "Output directory for the executables")
  
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -msse3 -fpermissive -fPIC")

FIND_PACKAGE(OpenCV REQUIRED)
INCLUDE_DIRECTORIES( ${OpenCV_INCLUDE_DIRS} )

#Find LTS5 library
FIND_PACKAGE(LTS5 REQUIRED)

#FLAG
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

# Interface
SET(lib_src
    src/face_module.cpp)
SET(lib_inc
    include/face_module.hpp)

ADD_LIBRARY(LTS5FaceModule SHARED ${lib_src} ${lib_inc})
TARGET_INCLUDE_DIRECTORIES(LTS5FaceModule
        PUBLIC
          $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)
TARGET_LINK_LIBRARIES(LTS5FaceModule
        PUBLIC
          ${OpenCV_LIBS}
          LTS5::lts5_face_tracker
          LTS5::lts5_facial_analysis)

ADD_EXECUTABLE(test_LTS5_FaceModule test_face_module.cpp)
TARGET_LINK_LIBRARIES(test_LTS5_FaceModule
        LTS5FaceModule)