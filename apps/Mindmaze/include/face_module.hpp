/**
 *  @file   face_module.hpp
 *  @brief  Interface for LTS5 processing module
 *
 *  @author Christophe Ecabert
 *  @date   01/05/15
 *  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
 */

#ifndef __LTS5_face_module_hpp__
#define __LTS5_face_module_hpp__

#include <string>

#include "opencv2/core/core.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/facial_analysis/facial_analysis.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
  
/**
 *  @class  FaceModule
 *  @brief  Interface for LTS5 processing module
 *  @author Christophe Ecabert
 *  @date   01/05/2015
 */
class FaceModule {
 private :
  /** Sdm model path */
  std::string sdm_model_path_;
  /** Tracking assessment model path */
  std::string tracking_model_path_;
  /** Face detector configuration path */
  std::string face_detector_path_;
  /** Face tracker */
  SdmTracker* face_tracker_;
  /** Face Landmarks provided by tracker */
  cv::Mat face_landmarks_;
  /** Pose estimation */
  PointDistributionModel* pose_estimator_;
  /** Head pose : Pitch - Yaw - Roll */
  double head_pose_[3] = {0.0, 0.0, 0.0};
  /** Emotion analysis module */
  ExpressionAnalysis* emotion_analyzer_;
  /** Classification scores */
  std::vector<double> emotion_score_;
  /** Label of detected emotion */
  std::vector<std::string> emotion_label_;
  /** Working image */
  cv::Mat working_img_;
  /** Region of interest */
  cv::Rect search_region_;

  /**
   *  @name   ParseConfig
   *  @brief  Read configuration file
   *  @param[in]  config_filename   Path to configuration file
   *  @return -1 if error, 0 otherwise
   */
  int ParseConfiguration(const std::string& config_filename);
  
 public :
  
#pragma mark -
#pragma mark Initialization
  /**
   *  @name   FaceModule
   *  @brief  Constructor
   */
  FaceModule(void);
  
  /**
   *  @name   ~FaceModule
   *  @brief  Destructor
   */
  ~FaceModule(void);

  /**
   *  @name Load
   *  @brief  Initialize LTS5 processing module
   *  @param[in]  config_filename   Configuration filename
   *  @return -1 if error, 0 otherwise.
   */
  int Load(const std::string& config_filename);

#pragma mark -
#pragma mark Processing

  /**
   *  @name   Process
   *  @brief  Run face processing on image
   *  @param[in]  image           Input image
   *  @return True on success
   */
  bool Process(const cv::Mat& image);

  /**
   *  @name   Process
   *  @brief  Run face processing on image
   *  @param[in]  image           Input image
   *  @param[in]  search_region   Region of interest
   *  @return True on success
   */
  bool Process(const cv::Mat& image,const cv::Rect& search_region);

  /**
   *  @name   Process
   *  @brief  Run face processing on image
   *  @param[in]  image           Input image
   *  @param[in]  search_region   Region of interest
   *  @param[in]  head_roll       Head orientation in radian (in plane)
   *  @return True on success
   */
  bool Process(const cv::Mat& image,
               const cv::Rect& search_region,
               const double& head_roll);
  /**
   *  @name   ProcessEmotion
   *  @brief  Run face processing on image
   *  @param[out] emotion_label Detected emotion
   */
  void ProcessEmotion(std::string* emotion_label);

  /**
   *  @name   DrawShape
   *  @brief  Draw landmarks
   *  @param[in]  image    Input image
   *  @param[in]  shape    Input landmarks
   *  @param[in]  color    BGR color
   *  @return      
   */
  static cv::Mat DrawShape(const cv::Mat& image, const cv::Mat& shape, const cv::Scalar& color);
  
  /**
   *  @name   DrawPose
   *  @brief  Draw pose orientation
   *  @param[in]  image    Input image
   *  @return
   */
  cv::Mat DrawPose(const cv::Mat& image);

#pragma mark -
#pragma mark Accessors

  /**
   *  @name   get_face_landmarks
   *  @brief  Give set of landmarks
   *  @return Face landmarks
   */
  const cv::Mat& get_face_landmarks(void) {return face_landmarks_;}

  /**
   *  @name   get_pitch_angle
   *  @brief  Give the estimated pitch
   *  @return pitch angle
   */
  double get_pitch_angle(void) const {return head_pose_[0];}
  
  /**
   *  @name   get_yaw_angle
   *  @brief  Give the estimated yaw
   *  @return yaw angle
   */
  double get_yaw_angle(void) const {return head_pose_[1];}
  
  /**
   *  @name   get_roll_angle
   *  @brief  Give the estimated roll
   *  @return roll angle
   */
  double get_roll_angle(void) const {return head_pose_[2];}

  /**
   *  @name   get_head_pose
   *  @brief  Give the estimated head pose
   *  @return Pose angle
   */
  const double* get_head_pose(void) const { return &head_pose_[0];}

  /**
   *  @name   get_emotion_label(void
   *  @brief  Provide a list of all possible emotion label
   *  @return List of possible emotion detection
   */
  const std::vector<std::string>& get_emotion_label(void) const {
    return emotion_label_;
  }

  /**
   *  @name   get_emotion_score
   *  @brief  Provide the result of all classifier
   *  @return List of individual classification score
   */
  const std::vector<double>& get_emotion_score(void) const {
    return emotion_score_;
  }
};
}  // namepsace LTS5

#endif /* defined(__LTS5_face_module_hpp__) */
