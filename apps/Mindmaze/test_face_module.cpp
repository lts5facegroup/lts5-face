//
//  main.cpp
//  Mindmaze
//
//  Created by Christophe Ecabert on 01/05/15.
//  Copyright (c) 2015 Ecabert Christophe. All rights reserved.
//

#include <iostream>
#include <map>

#include "opencv2/highgui.hpp"

#include "face_module.hpp"

const int face_roi_size = 400;

std::map<int, cv::Mat> ReadMapNeck(const std::string& filename) {
  std::cout << "Read head location data..." << std::endl;
  std::map<int, cv::Mat> ret;
  std::ifstream ifs(filename.c_str());

  if(ifs.is_open()) {
    std::string line;
    while(std::getline(ifs, line)) {
      if(line.length()>1) {
        std::istringstream iss(line);

        int fid;
        double x, y, z;
        int cx, cy;
        double nx, ny, nz;
        int ncx, ncy;
        iss >> fid >> x >> y >> z >> cx >> cy >> nx >> ny >> nz >> ncx >> ncy;
        cv::Mat range_data = cv::Mat(10, 1, CV_64FC1);


        range_data.at<double>(0, 0) = x;
        range_data.at<double>(1, 0) = y;
        range_data.at<double>(2, 0) = z;
        range_data.at<double>(3, 0) = cx;
        range_data.at<double>(4, 0) = cy;

        range_data.at<double>(5, 0) = nx;
        range_data.at<double>(6, 0) = ny;
        range_data.at<double>(7, 0) = nz;
        range_data.at<double>(8, 0) = ncx;
        range_data.at<double>(9, 0) = ncy;

        //std::cout << range_data << std::endl;

        ret[fid] = range_data;
      }
    }
  }else {
    std::cerr << "can not open file " << filename << std::endl;
  }

  ifs.close();
  return ret;
}

int main(int argc, const char * argv[]) {
  if (argc < 3) {
    std::cout << "Usage : " << argv[0] << " config_file input_video head_file"  << std::endl;
    return -1;
  }
  //Face Module initialisation
  auto* face_processor = new LTS5::FaceModule();
  int error = face_processor->Load(argv[1]);

  //Init input capture
  cv::VideoCapture input_video(argv[2]);
  if (!input_video.isOpened()) {
    std::cout  << "Could not open the input video stream: " << argv[1] << std::endl;
    return -1;
  }

  std::string output_fn = "outnew.avi";
  cv::Size S = cv::Size(  (int) input_video.get(CV_CAP_PROP_FRAME_WIDTH), (int) input_video.get(CV_CAP_PROP_FRAME_HEIGHT));
  int fps = (int) input_video.get(CV_CAP_PROP_FPS);

  cv::VideoWriter output_video;
  output_video.open(output_fn, CV_FOURCC('X','V','I','D'), fps, S, true);
  if (!output_video.isOpened()) {
    std::cout  << "Could not open the output video for write: " << output_fn << std::endl;
    return -1;
  }

  //Search region
  cv::Rect search_region  (150, 50, face_roi_size, face_roi_size);

  //Load depth info
  std::map<int, cv::Mat> hmap;
  if (argc > 3) {
    hmap= ReadMapNeck(argv[3]);
  }
  cv::namedWindow("LST5-Face-Module", cv::WINDOW_NORMAL);
  cv::Mat image;
  //tracking
  cv::Point head_location(-1, -1);
  cv::Point neck_location(-1, -1);
  int frame_index = 0;

  //Process video
  while (true) {
    input_video >> image;
    if(image.empty())
      break;

    double t0 = cvGetTickCount();
    double roll = 0;
    bool track_ok = false;
    if(hmap.find(frame_index)!=hmap.end()) {
      cv::Mat data_row = hmap[frame_index];

      head_location.x = data_row.at<double>(3, 0);
      head_location.y = data_row.at<double>(4, 0);

      double ncx = data_row.at<double>(8, 0);
      double ncy = data_row.at<double>(9, 0);

      double dx = head_location.y - ncy;
      double dy = head_location.x - ncx;

      neck_location.x = ncx;
      neck_location.y = ncy;

      if(dx==0)
        dx = 0.0000001;

      roll = -std::atan(dy/dx);

      search_region.x = head_location.x - face_roi_size/2;
      search_region.y = head_location.y - face_roi_size/2;

      search_region.x = std::max(search_region.x, 0);
      search_region.y = std::max(search_region.y, 0);
      search_region.width = std::min(search_region.width,
                                     image.cols - search_region.x);
      search_region.height = std::min(search_region.height,
                                      image.rows - search_region.y);
      track_ok = face_processor->Process(image, search_region, roll);
      head_location.x = -1;
      head_location.y = -1;
      neck_location.x = -1;
      neck_location.y = -1;

    } else {
      track_ok = face_processor->Process(image, search_region);
    }
    if (track_ok) {
      std::string label;
      face_processor->ProcessEmotion(&label);
      std::cout << label << std::endl;
    }

    double t1 = cvGetTickCount();
    double t = (double(t1-t0)/cvGetTickFrequency()) / 1e+3;
    std::cout << "processing time:" << t << std::endl;

    if(track_ok) {
        cv::Mat landmarks = face_processor->get_face_landmarks();
        cv::Mat canvas = LTS5::FaceModule::DrawShape(image, landmarks, CV_RGB(0, 255, 0));
        canvas = face_processor->DrawPose(canvas);
        cv::imshow("LST5-Face-Module", canvas);
    } else {
        cv::imshow("LST5-Face-Module", image);
    }
    cv::waitKey(5);
    frame_index++;
  }

  //Release
  delete face_processor;

  return error;
}
