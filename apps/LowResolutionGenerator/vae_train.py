# coding=utf-8
from os.path import join as _join
from argparse import ArgumentParser
from multiprocessing import cpu_count
import tensorflow as _tf
from tensorflow.keras.callbacks import ModelCheckpoint
from utils.data_loader import VAEDataLoader
from lts5.utils.tools import init_logger
from lts5.tensorflow_op.utils.device import initialize_device_memory
from lts5.tensorflow_op.callbacks import AugmentedTensorBoard
from lts5.tensorflow_op.network.vae import VariationalAutoEncoder
from lts5.tensorflow_op.utils.summary import SummaryWriterManager

# Init LTS5 logger
logger = init_logger()
# Limit memory usage
initialize_device_memory()


def input_fn(filepath,
             img_size,
             training,
             batch_size):
  """
  Load data from a text file `filepath`
  :param filepath:    Path to the file storing the input data
  :param img_size:    Image dimensions, (width, height)
  :param training:    Boolean flag indicating training phase
  :param batch_size:  Batch size
  :return:  Dataset's iterator
  """
  # Define number of possible parallel call
  n_call = cpu_count()
  # Create loader
  loader = (VAEDataLoader(file_pattern=filepath,
                          image_size=img_size,
                          training=training,
                          n_parallel_call=n_call)
            .shuffle(buffer_size=(batch_size * 50))
            .decode()
            .preprocess()
            .batch(batch_size=batch_size, drop_remainder=True)
            .prefetch(1))
  # Get iterator
  return loader.one_shot_iterator()


def train_vae(pargs):
  """
  Train VAE
  :param pargs: Parsed arguments
  """

  # Summary manager
  # ------------------------------------------------
  logdir = _join(pargs.model_dir, 'logs')
  manager = SummaryWriterManager(log_dir=logdir, log_freqency=pargs.log_freq)

  # Define inputs dataset
  # ------------------------------------------------
  train_dataset = input_fn(pargs.train_set,
                           img_size=(256, 256, 3),
                           training=True,
                           batch_size=pargs.batch_size)
  validation_dataset = input_fn(pargs.validation_set,
                                img_size=(256, 256, 3),
                                training=True,
                                batch_size=pargs.batch_size)

  # Model
  # ------------------------------------------------
  shp = (256, 256, 3)
  image_in = _tf.keras.Input(shape=shp,
                             dtype=_tf.float32,
                             name='image')
  model = VariationalAutoEncoder(encoder_cfg=None,
                                 decoder_cfg=None,
                                 manager=manager,
                                 name='VAE')
  image_rec = model(image_in, training=True)
  # Losses
  # ------------------------------------------------
  losses = model.get_losses()
  # Metrics
  # ------------------------------------------------
  metrics = model.get_metrics(input_shape=shp)
  # Optimizer
  # ------------------------------------------------
  optimizer = model.get_optimizer(learning_rate=pargs.lr)

  # Compile
  # ------------------------------------------------
  model.compile(optimizer=optimizer,
                loss=losses,
                metrics=metrics)

  # Callbacks
  # ------------------------------------------------
  model_name = _join(pargs.model_dir, 'residual_vae.ckpt')
  tensorboad_cb = AugmentedTensorBoard(log_dir=logdir,
                                       histogram_freq=1,
                                       write_graph=True,
                                       write_images=False,
                                       update_freq=pargs.log_freq * pargs.batch_size,
                                       profile_batch=0,
                                       embeddings_freq=0)
  # Save model based on performance of validation loss
  checkpoint_cb = ModelCheckpoint(filepath=model_name,
                                  save_best_only=True,
                                  monitor='val_loss',
                                  mode='min',
                                  save_weights_only=True,
                                  verbose=1)
  callbacks = [tensorboad_cb,
               checkpoint_cb]

  # Reload
  latest = _tf.train.latest_checkpoint(pargs.model_dir)
  if latest is not None:
    model.load_weights(latest)

  # Train
  #model.summary(120)
  model.fit(x=train_dataset,
            callbacks=callbacks,
            epochs=pargs.n_epoch,
            validation_data=validation_dataset,
            validation_steps=None)




if __name__ == '__main__':
  # Command line input
  p = ArgumentParser()

  # Input - Encoder
  p.add_argument('--train_set',
                 type=str,
                 required=True,
                 help='Glob pattern during training phase for '
                      'model (*.tfrecords)')
  p.add_argument('--validation_set',
                 type=str,
                 required=True,
                 help='Glob pattern during validation phase for '
                      'model (*.tfrecords)')
  p.add_argument('--model_dir',
                 type=str,
                 required=True,
                 help='Location where to store the model')
  # Learning rate
  p.add_argument('--lr',
                 type=float,
                 default=1e-5,
                 help='Learning rate')
  # Number of epoch
  p.add_argument('--n_epoch',
                 type=int,
                 default=10,
                 help='Number of epoch to train for')
  # Batch size
  p.add_argument('--batch_size',
                 type=int,
                 default=16,
                 help='Number of samples in a single batch')
  # Logging frequency
  p.add_argument('--log_freq',
                 type=int,
                 default=500,
                 help='Logging frequency')

  # Decode
  args = p.parse_args()
  # Train procedure
  train_vae(pargs=args)
