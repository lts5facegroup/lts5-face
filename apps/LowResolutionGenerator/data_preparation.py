# coding=utf-8
"""
This script creates two datasets LR / HR from a corpus of face datasets:

Low resolution (LR):
  - WIDERFace
High resolution (HR):
  - CelebA
  - AFLW
  - VGGFace2

Run face detector (i.e. S3FD) and select image where face can be properly
detected and fulfill some requirements.
Finally the images are packed into *.tfrecords files for training
"""
from os.path import join as _join
from os.path import exists as _exists
from os import makedirs as _makedirs
import argparse
import numpy as np
import matplotlib.pyplot as plt
from imageio import imread
import tensorflow as tf
from lts5.utils import init_logger
from lts5.utils import WIDERFace, CelebA, VGGFace2, AFLW
from utils.detector import S3FD
from utils.data import IdentityCriterion, SquareBBoxTransformer
from utils.data import ImageFileExporter, BBoxExporter, ImageResolutionSelector
from lts5.utils.tools import search_folder
from lts5.tensorflow_op.utils.tfrecords import to_feature


__author__ = 'Christophe Ecabert'

logger = init_logger()


def run_face_detector(detector,
                      datasets,
                      exporters,
                      partition='train',
                      criterion=IdentityCriterion(),
                      transformer=SquareBBoxTransformer()):
  """
  Run a given face detector on a list of `utils.dataset.base.Dataset` instances
  :param detector:  Face detector (i.e. `DetectorAdapter`)
  :param datasets:  List of datasets
  :param exporters: List of `RegionExporter` to apply to the region
  :param partition: Type of partition (i.e. 'train', 'validation')
  :param criterion: Object checking if a given region should be selected
  :param transformer: Transform detected bounding box
  """
  # Iterate over every dataset
  for dset in datasets:
    logger.info('Process data from: %s', dset.infos.name())
    for image in dset.samples(partition=partition):
      # Run detection
      dets = detector.process(image=image)
      for bbox in dets:
        bb = transformer(bbox, image.shape[1], image.shape[0])
        if bb is not None and criterion(image, bb):
          # Export image
          for exp in exporters:
            exp(image, bb)


def run_face_region_selection(image_path, bboxes, selectors):
  """
  Run all `selectors` on a list of image/bboxes
  :param image_path:  List of images
  :param bboxes:      Corresponding list of bbox
  :param selectors:   Selectors to run (i.e. callable(image, bbox))
  """
  for im_path, bbox in zip(image_path, bboxes):
    # Load image
    img = imread(im_path)
    # Run selector
    for sel in selectors:
      sel(image=img, bbox=bbox)


def export_to_records(samples, out_folder, val_size, test_size, shard_size):
  """
  Export a list of entries into tfrecords files
  :param samples:     List of images to export to tfrecords files
  :param out_folder:  Location where to place the records
  :param val_size:    Validation set size (i.e. in percent)
  :param test_size:   Test set size (i.e. in percent)
  :param shard_size:  Shard size
  """

  def _create_example_from_image(im_path):
    """
    Create a tf.train.Example for a given image
    :param im_path:  File location
    :return:  tf.train.Example
    """
    with open(im_path, 'rb') as f:
      raw_bytes = f.read()
    # Create example
    feat = {'image': to_feature(raw_bytes)}
    return tf.train.Example(features=tf.train.Features(feature=feat))

  # Check output
  if not _exists(out_folder):
    _makedirs(out_folder)
  # Define each sets dimensions
  n_sample = len(samples)
  n_val = int(val_size * n_sample)
  n_test = int(test_size * n_sample)
  n_train = n_sample - (n_val + n_test)

  # Shuffle
  rand_samples = np.random.permutation(samples)

  # Create partitions
  n_tot = 0
  for ns, set_name in zip([n_train, n_val, n_test],
                          ['train', 'val', 'test']):
    # Define samples belonging to the current partition
    from_idx = n_tot
    to_idx = from_idx + ns
    n_tot = to_idx
    samples_set = rand_samples[from_idx:to_idx]
    partition_sz = len(samples_set)
    # Split into shards
    n_shards = (partition_sz // shard_size) + 1
    for k in range(n_shards):
      # Starting/stopping index of the shard for the current partition
      start = k * shard_size
      stop = (k + 1) * shard_size
      stop = stop if stop < partition_sz else partition_sz
      # Create shard writer
      sh_name = _join(out_folder, '{}_{:03d}.tfrecords'.format(set_name, k))
      logger.info('Process %s',
                  '{}/{}_{:03d}.tfrecords'.format(out_folder,
                                                  set_name,
                                                  k))
      with tf.io.TFRecordWriter(path=sh_name) as writer:
        # Iterate over samples
        for s in samples_set[start:stop]:
          # Create tf.train.Example
          example = _create_example_from_image(im_path=s)
          # Dump into writer
          writer.write(example.SerializeToString())


if __name__ == '__main__':
  # Parse command line
  parser = argparse.ArgumentParser(description='Data preparation')

  # Face detector
  parser.add_argument('--face_detector_model',
                      type=str,
                      required=True,
                      dest='fdet',
                      help='Face detector model (*.npy)')
  # Low-resolution dataset (i.e. WIDERFace)
  parser.add_argument('--widerface_path',
                      type=str,
                      required=True,
                      dest='wider_folder',
                      help='Location where `WIDERFace` is stored')
  parser.add_argument('--aflw_path',
                      type=str,
                      required=True,
                      dest='aflw_folder',
                      help='Location where `AFLW` is stored')
  parser.add_argument('--celeba_path',
                      type=str,
                      required=True,
                      dest='celeba_folder',
                      help='Location where `CalebA` is stored')
  parser.add_argument('--vggface_path',
                      type=str,
                      required=True,
                      dest='vggface_folder',
                      help='Location where `VGGFace2` is stored')
  # Maximum face bounding box size
  parser.add_argument('--max_face_size',
                      type=int,
                      default=320,
                      dest='max_face_size',
                      help='Maximum face bounding box size')
  # What is low resolution
  parser.add_argument('--lr_size',
                      type=int,
                      default=64,
                      help='Face region dimension consider as low resolution')
  parser.add_argument('--lr_margin',
                      type=float,
                      default=0.1,
                      help='Low resolution margin')
  # What is high resolution
  parser.add_argument('--hr_size',
                      type=int,
                      default=256,
                      help='Face region dimension consider as high resolution')
  parser.add_argument('--hr_margin',
                      type=float,
                      default=0.1,
                      help='High resolution margin')
  # Partition size
  parser.add_argument('--val_size',
                      type=float,
                      default=0.1,
                      help='Amount of data to put into validation set')
  parser.add_argument('--test_size',
                      type=float,
                      default=0.15,
                      help='Amount of data to put into test set')
  parser.add_argument('--lr_shard_size',
                      type=int,
                      default=100000,
                      help='Number of samples for each LR shards')
  parser.add_argument('--hr_shard_size',
                      type=int,
                      default=25000,
                      help='Number of samples for each HR shards')
  # Output location
  parser.add_argument('--output',
                      type=str,
                      required=True,
                      help='Root folder where data will be placed')
  # Parse
  args = parser.parse_args()

  # Already process dataset ?
  if not _exists(_join(args.output, 'bboxes.csv')):
    # Detector
    fdet = S3FD(model_path=args.fdet)

    # Process images -> get face bounding boxes
    exporters = [ImageFileExporter(folder=_join(args.output, 'images')),
                 BBoxExporter(filename=_join(args.output, 'bboxes.csv'),
                              pattern='images/face_region_{:08d}.jpg')]
    datasets = [WIDERFace(folder=args.wider_folder),
                AFLW(folder=args.aflw_folder),
                CelebA(folder=args.celeba_folder),
                VGGFace2(folder=args.vggface_folder)]

    run_face_detector(detector=fdet,
                      datasets=datasets,
                      exporters=exporters)

  # Reload
  im_pathes = []
  bboxes = []
  sizes = []
  with open(_join(args.output, 'bboxes.csv')) as bb_f:
    hdr = bb_f.readline().strip()
    for line in bb_f:
      parts = line.strip().split(',')
      path = parts[0]
      xmin, ymin, xmax, ymax = map(int, parts[1:])

      # Drop image too large
      size = xmax - xmin
      if size <= args.max_face_size:
        im_pathes.append(_join(args.output, path))
        bboxes.append(np.asarray([xmin, ymin, xmax, ymax], dtype=np.int32))
        sizes.append(size)

  # compute histogram
  nbr, bins, patch = plt.hist(sizes, 100)
  plt.title('Face bounding box distribution')
  plt.xlabel('Bounding box size')
  plt.ylabel('#Occurence')
  plt.show()

  # Process each bboxes to defines what is LR and HR
  if not _exists(_join(args.output, 'lr_patches')):
    lr_select = ImageResolutionSelector(_join(args.output, 'lr_patches'),
                                         size=args.lr_size,
                                         margin=args.lr_margin,
                                         pattern='lr_{:08}.jpg')
    hr_select = ImageResolutionSelector(_join(args.output, 'hr_patches'),
                                         size=args.hr_size,
                                         margin=args.hr_margin,
                                         pattern='hr_{:08}.jpg')
    run_face_region_selection(image_path=im_pathes,
                              bboxes=bboxes,
                              selectors=[lr_select, hr_select])

    logger.info('Low resolution samples: {}'.format(lr_select.count))
    logger.info('High resolution samples: {}'.format(hr_select.count))

  # Create *.tfrecords shards
  for entries in zip(['lr_patches', 'hr_patches'],
                     ['lr', 'hr'],
                     [args.lr_shard_size, args.hr_shard_size]):
    in_folder, out_folder, shard_sz = entries
    # Get images
    patches = search_folder(folder=_join(args.output, in_folder),
                            ext=['.jpg'],
                            relative=False)
    # Create records
    export_to_records(samples=patches,
                      out_folder=_join(args.output, 'records', out_folder),
                      val_size=args.val_size,
                      test_size=args.test_size,
                      shard_size=shard_sz)
  # Done
  logger.info('Done')
