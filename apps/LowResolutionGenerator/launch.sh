#!/bin/bash

# Check if a mod is given
if [ "$#" -ne 1 ]; then
    echo "Wrong number of parameters"
    echo "$0 <mod>"
    echo "Possible mod: <Prepare> "
    exit
fi

MODEL_DIR="/home/christophe/Documents/LTS5/Data/models"
DATASET_DIR="/media/christophe/Data/Datasets"
OUTPUT_DATA_DIR="${DATASET_DIR}/LowResolutionGen"

case "$1" in
    "Prepare")
        # Datasets
        WIDERFACE_DIR="${DATASET_DIR}/widerface"
        AFLW_DIR="${DATASET_DIR}/AFLW"
        CELEBA_DIR="${DATASET_DIR}/CelebA"
        VGGFACE2_DIR="${DATASET_DIR}/VGGFace2"
        # Detector
        FDET="${MODEL_DIR}/sfd.npy"

        echo "Run data preparation ... "
        python data_preparation.py --face_detector_model ${FDET} --widerface_path ${WIDERFACE_DIR} --aflw_path ${AFLW_DIR} --celeba_path ${CELEBA_DIR} --vggface_path ${VGGFACE2_DIR} --output ${OUTPUT_DATA_DIR}
        ;;

    "Train")
        python h2l_train.py --dis_train_pattern "${OUTPUT_DATA_DIR}/records/lr/train_*.tfrecords" --gen_train_pattern "${OUTPUT_DATA_DIR}/records/hr/train_*.tfrecords" --model_dir ${MODEL_DIR}/h2l_test
        ;;

    "TrainVae")
        python vae_train.py --train_set "${OUTPUT_DATA_DIR}/records/hr/train_*.tfrecords" --validation_set "${OUTPUT_DATA_DIR}/records/hr/val_*.tfrecords" --model_dir ${MODEL_DIR}/ResidualVAE --lr 1e-4 --batch_size 16 --n_epoch 50
        ;;
    *)
		echo "Unknown mod"
		;;
esac