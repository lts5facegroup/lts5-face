# coding=utf-8
"""
Realistic low resolution image generation training script

See:
 - https://www.adrianbulat.com/downloads/ECCV18/image-super-resolution.pdf
"""
import argparse
from utils.data_loader import DiscriminatorDataset, GeneratorDataset
from lts5.tensorflow_op.network.high2low import DiscriminatorConfig
from lts5.tensorflow_op.network.high2low import GeneratorConfig
from lts5.tensorflow_op.network.high2low import H2LSupervisor
from lts5.tensorflow_op.utils import initialize_device_memory

__author__ = 'Christophe Ecabert'
initialize_device_memory()


def create_dataset(pattern,
                   dataset_cls,
                   training=False,
                   batch_size=32,
                   n_epoch=1):
  """
  :param pattern:       Glob pattern to select tfrecords files
  :param dataset_cls:   Class of dataset to create
  :param training:      If `True` indicates training phase
  :param batch_size:    Batch size
  :param n_epoch:       Number of epoch to perform
  :return:  `DataProvider` instance
  """
  d = dataset_cls(file_pattern=pattern, training=training)
  if training:
    d = d.shuffle(batch_size * 100)
  d = (d.repeat(n_epoch)
       .decode()
       .preprocess()
       .batch(batch_size, True)
       .prefetch(1))
  return d


def train_gan(pargs):
  """
  Training process for High-2-low gan
  :param pargs: Parsed arguments
  """

  # Create loader
  g_data = create_dataset(pattern=pargs.gen_train_pattern,
                          dataset_cls=GeneratorDataset,
                          training=True,
                          n_epoch=1)
  d_data = create_dataset(pattern=pargs.dis_train_pattern,
                          dataset_cls=DiscriminatorDataset,
                          training=True,
                          n_epoch=-1)

  # Create configuration
  d_cfg = DiscriminatorConfig(name='h2l_dis',
                              step=2,
                              optimizer_config={'name': 'Adam',
                                                'config': {'learning_rate': 1e-4,
                                                           'beta_1': 0.0,
                                                           'beta_2': 0.9,
                                                           'name': 'dis_optimizer'}})
  g_cfg = GeneratorConfig(name='h2l_gen',
                          optimizer_config={'name': 'Adam',
                                            'config': {'learning_rate': 5e-5,
                                                       'beta_1': 0.0,
                                                       'beta_2': 0.9,
                                                       'name': 'gen_optimizer'}})

  # Create supervisor for training
  supervisor = H2LSupervisor(model_dir=pargs.model_dir,
                             dis_config=d_cfg,
                             gen_config=g_cfg,
                             weight=0.1,
                             log_freq=100)

  # Start training
  # supervisor.train(gen_dataset=g_data, dis_dataset=d_data)
  supervisor.train_discriminator_only(gen_dataset=g_data, dis_dataset=d_data)
  # supervisor.train_generator_only(gen_dataset=g_data)


if __name__ == '__main__':
  # Command line input
  parser = argparse.ArgumentParser()

  # Input - discriminator
  parser.add_argument('--dis_train_pattern',
                      type=str,
                      required=True,
                      help='Glob pattern during training phase for '
                           'discriminator (*.tfrecords)')
  parser.add_argument('--gen_train_pattern',
                      type=str,
                      required=True,
                      help='Glob pattern during training phase for '
                           'generator (*.tfrecords)')
  parser.add_argument('--model_dir',
                      type=str,
                      required=True,
                      help='Location where to store the model')

  # Decode
  args = parser.parse_args()

  # Train procedure
  train_gan(pargs=args)



