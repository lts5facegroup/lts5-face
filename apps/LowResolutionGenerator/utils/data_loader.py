# coding=utf-8
""" Data loader for H2L generator and discriminator """
from multiprocessing import cpu_count as _cpu_count
from random import random as _random
import tensorflow as _tf
import tensorflow_addons as _tfa
from lts5.tensorflow_op.data.data_io import DataProvider

__author__ = 'Christophe Ecabert'


class DiscriminatorDataset(DataProvider):
  """ Load real low-resolution images and provide its label """

  def __init__(self,
               file_pattern,
               image_size=(64, 64, 3),
               training=False,
               n_parallel_call=None):
    """
    Constructor
    :param file_pattern:      Glob pattern to gather tfrecords files
    :param image_size:        Low-resolution image size (H, W, C)
    :param training:          If `True` indicates training, add augmentation to
                              the process
    :param n_parallel_call:   Number of parallel call allowed
    """
    super(DiscriminatorDataset, self).__init__()
    self._n_call = _cpu_count() if n_parallel_call is None else n_parallel_call
    self._img_sz = image_size
    self._training = training
    self._source = _tf.data.Dataset.list_files(file_pattern, shuffle=training)
    auto = _tf.data.experimental.AUTOTUNE
    self._source = self._source.interleave(_tf.data.TFRecordDataset,
                                           cycle_length=self._n_call,
                                           num_parallel_calls=auto)

  def decode(self):
    """
    Decode an entry from tfrecords files
    :return:  self
    """

    def _parse_fn(example):
      """
      convert an tf.train.Example into tensors
      :param example: Dataset example
      :return:  tuple: image, label
      """
      # Parse TFExample records and perform simple data augmentation.
      fmt = {'image': _tf.io.FixedLenFeature([], _tf.string, "")}
      parsed = _tf.io.parse_single_example(example, fmt)
      # Decode image
      image = _tf.io.decode_jpeg(parsed['image'])
      image.set_shape(self._img_sz)
      image = _tf.cast(image, _tf.float32)
      # Done
      return image, _tf.constant(1.0, dtype=_tf.float32)

    # Add to pipeline
    self._source = self._source.map(_parse_fn)
    return self

  def preprocess(self):
    """
    Add pre-processing to the pipeline
    :return: self
    """

    def _preprocess(image, label):
      """
      Apply pre-processing
      :param image: Image
      :param label: Label
      :return:  Transformed image
      """
      # image = image / 255.0
      image = (image - 127.5) / 127.5
      return image, label

    def _random_flip(image, label):
      """
      Randomly add left-right flip
      :param image: Image to be transformed
      :param label: Label
      :return:  Random flip
      """
      return _tf.image.random_flip_left_right(image), label

    if self._training:
      self._source = self._source.map(_random_flip)
    self._source = self._source.map(_preprocess)
    return self


class GeneratorDataset(DataProvider):
  """
  Generate pairs of "real" high resolution images + noise and the corresponding
  label
  """

  def __init__(self,
               file_pattern,
               image_size=(256, 256, 3),
               factor=4,
               training=False,
               n_parallel_call=None):
    """
    Constructor
    :param file_pattern:    Glob pattern to gather tfrecords files
    :param image_size:      High-resolution image size (H, W, C)
    :param factor:          Downsampling factor
    :param n_parallel_call: Number of parallel call allowed
    """
    super(GeneratorDataset, self).__init__()
    self._n_call = _cpu_count() if n_parallel_call is None else n_parallel_call
    self._scale = factor
    self._hr_img_sz = image_size
    self._lr_img_sz = [x // self._scale for x in self._hr_img_sz[:-1]]
    self._training = training
    self._source = _tf.data.Dataset.list_files(file_pattern, shuffle=training)
    auto = _tf.data.experimental.AUTOTUNE
    self._source = self._source.interleave(_tf.data.TFRecordDataset,
                                           cycle_length=self._n_call,
                                           num_parallel_calls=auto)

  def decode(self):
    """
    Decode tfrecords
    :return: self
    """

    def _parse_fn(example):
      # Parse TFExample records and perform simple data augmentation.
      fmt = {'image': _tf.io.FixedLenFeature([], _tf.string, "")}
      parsed = _tf.io.parse_single_example(example, fmt)
      # Decode image
      image = _tf.io.decode_jpeg(parsed['image'])
      image.set_shape(self._hr_img_sz)
      image = _tf.cast(image, _tf.float32)
      return image

    self._source = self._source.map(_parse_fn)
    return self

  def preprocess(self):
    """
    Apply pre-processing
    :return:  self
    """

    def _generate_proxy(image):
      """
      Generate low resolution proxy image
      :param image: Image
      :return:  Down-sampled proxy image
      """
      # Image: [0, 255]
      proxy = _tf.image.resize(images=image,
                               size=self._lr_img_sz,
                               method=_tf.image.ResizeMethod.NEAREST_NEIGHBOR)
      return image, proxy

    def _normalize_image(data, label):
      """
      Pre-process data. Convert image
      :param data:    Dict of tensor to be processed
      :param label:   Corresponding label
      :return:  tuple: data, label
      """
      # normalize image
      image = (data - 127.5) / 127.5
      lr_proxy = (label - 127.5) / 127.5
      # image [-1, 1], proxy [-1, 1]
      return image, lr_proxy

    if self._training:

      def _flip_data(data):
        """ flip image left/right """
        return _tf.image.flip_left_right(data)

      def _rot_data(data, maxangle=30.0):
        """ Rotated image """
        m_ang = maxangle * 0.017453292519943295
        ang = _tf.random.uniform(shape=[],
                                 minval=-m_ang,
                                 maxval=m_ang)
        return _tfa.image.rotate(data, angles=ang)

      def _scale_data(data, minval=0.8):
        """ Scaling image """
        s = _tf.random.uniform(shape=[], minval=minval, maxval=1.0)
        # Define box
        x1 = y1 = 0.5 - (0.5 * s)
        x2 = y2 = 0.5 + (0.5 * s)
        box = _tf.expand_dims(_tf.stack([x1, y1, x2, y2]), 0)
        # Image needs to be 4D tensor
        if _tf.rank(data) == 3:
          data = _tf.expand_dims(data, 0)
        # Get inputs H, W
        dims = _tf.shape(data)[1:3]
        scaled = _tf.image.crop_and_resize(data,
                                           boxes=box,
                                           box_indices=[0],
                                           crop_size=dims)
        return _tf.squeeze(scaled, axis=0)

      def _augment_data(image):
        """ Apply random augmentaiton """
        c_flip = _tf.random.uniform(shape=[], maxval=1.0, dtype=_tf.float32)
        c_rot = _tf.random.uniform(shape=[], maxval=1.0, dtype=_tf.float32)
        c_scale = _tf.random.uniform(shape=[], maxval=1.0, dtype=_tf.float32)
        # Apply flip ?
        im_flip = _tf.cond(c_flip < 0.5,
                           lambda: image,
                           lambda: _flip_data(image))
        # # Apply rotation ?
        # im_rot = _tf.cond(c_rot < 0.5,
        #                   lambda: im_flip,
        #                   lambda: _rot_data(im_flip))
        # Apply scaling ?
        im_scale = _tf.cond(c_scale < 0.5,
                            lambda: im_flip,
                            lambda: _scale_data(im_flip))
        return im_scale

      self._source = self._source.map(_augment_data)
    self._source = self._source.map(_generate_proxy)
    self._source = self._source.map(_normalize_image)
    return self


class VAEDataLoader(DataProvider):
  """ Dataset for VAE training """

  def __init__(self,
               file_pattern,
               image_size=(256, 256, 3),
               training=False,
               n_parallel_call=None):
    """
    Constructor
    :param file_pattern:    Glob pattern to gather tfrecords files
    :param image_size:      High-resolution image size (H, W, C)
    :param n_parallel_call: Number of parallel call allowed
    """
    super(VAEDataLoader, self).__init__()
    self._n_call = _cpu_count() if n_parallel_call is None else n_parallel_call
    self._img_sz = image_size
    self._training = training
    self._source = _tf.data.Dataset.list_files(file_pattern, shuffle=training)
    auto = _tf.data.experimental.AUTOTUNE
    self._source = self._source.interleave(_tf.data.TFRecordDataset,
                                           cycle_length=self._n_call,
                                           num_parallel_calls=auto)

  def decode(self):
    """
    Decode tfrecords
    :return: self
    """

    def _parse_fn(example):
      # Parse TFExample records and perform simple data augmentation.
      fmt = {'image': _tf.io.FixedLenFeature([], _tf.string, "")}
      parsed = _tf.io.parse_single_example(example, fmt)
      # Decode image
      image = _tf.io.decode_jpeg(parsed['image'])
      image.set_shape(self._img_sz)
      image = _tf.cast(image, _tf.float32)
      # Done
      return image

    self._source = self._source.map(_parse_fn)
    return self

  def preprocess(self):
    """
    Apply pre-processing
    :return:  self
    """

    def _augment_data(image):
      return _tf.image.random_flip_left_right(image)

    def _preprocess(image):
      inputs = (image - 127.5) / 127.5
      labels = image / 255.0
      return {'image': inputs}, labels

    if self._training:
      self._source = self._source.map(_augment_data)
    self._source = self._source.map(_preprocess)
    return self
