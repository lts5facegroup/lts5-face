# coding=utf-8
from os.path import exists as _exists
from os.path import join as _join
from os.path import dirname as _dirname
from os import makedirs as _makedirs
import numpy as _np
from imageio import imwrite as _imwrite
from skimage.transform import resize
from .base import Criterion
from .base import BBoxTransformer
from .base import RegionExporter

__author__ = 'Christophe Ecabert'


class IdentityCriterion(Criterion):
  """ Every region is selected """

  def __call__(self, image, bbox):
    return True


class SquareBBoxTransformer(BBoxTransformer):
  """ Transform bounding box into square bounding box """

  def __init__(self, margin=0.1):
    """
    Constructor
    :param margin:  Extra margin to add
    """
    self._margin = margin

  def __call__(self, bbox, width, height):
    """
    Transform a given bounding box
    :param bbox:   Original bounding box
    :param width:  Image width
    :param height: Image height
    :return:  Transform bounding box
    """
    # convert bbox to square
    xmin, ymin, xmax, ymax = bbox
    cx = (xmin + xmax) / 2.0
    cy = (ymin + ymax) / 2.0
    hside = max(xmax - xmin, ymax - ymin) * (1.0 + self._margin)
    hside /= 2.0
    xmin = cx - hside
    xmax = cx + hside + 1
    ymin = cy - hside
    ymax = cy + hside + 1
    # Check if outside image range
    if hside < height / 2.0 and hside < width / 2.0:
      if xmin < 0:
        shift = abs(xmin)
        xmin += shift
        xmax += shift
      if xmax > width:
        shift = xmax - width
        xmax -= shift
        xmin -= shift
      if ymin < 0:
        shift = abs(ymin)
        ymin += shift
        ymax += shift
      if ymax > height:
        shift = ymax - height
        ymax -= shift
        ymin -= shift
      # Extract region + dump
      dx = int(xmax - xmin)
      dy = int(ymax - ymin)
      xmin = int(xmin)
      xmax = xmin + dx
      ymin = int(ymin)
      ymax = ymin + dy
      return _np.asarray([xmin, ymin, xmax, ymax])
    else:
      # Region larger than image
      return None


class ImageFileExporter(RegionExporter):
  """ Crop and export face bounding box into file """

  def __init__(self, folder, pattern='face_region_{:08d}.jpg'):
    """
    Constructor
    :param folder:  Location where to dump the data
    :param pattern: File naming pattern
    """
    self._folder = folder
    if not _exists(self._folder):
      _makedirs(self._folder)
    self._pattern = pattern
    self._cnt = 0

  def __call__(self, image, bbox):
    """
    Export a given face region
    :param image:   Image
    :param bbox:    Face region
    """
    xmin, ymin, xmax, ymax = bbox
    crop = image[ymin:ymax, xmin:xmax, :3]
    fname = self._pattern.format(self._cnt)
    _imwrite(_join(self._folder, fname), crop)
    self._cnt += 1


class BBoxExporter(RegionExporter):
  """ Dump bounding box into a csv file where each rows are formatted as:
    BBOX_ID; x_min, y_min, x_max, y_max
  """

  def __init__(self, filename, pattern='face_region_{:08d}.jpg'):
    """
    Constructor
    :param filename:  Name of the csv file
    :param pattern:   BBox ID pattern (should be index based
    """
    folder = _dirname(filename)
    if not _exists(folder):
      _makedirs(folder)
    self._file = open(filename, 'wt')
    self._file.write('{},{},{},{},{}\n'.format('BBOX_ID',
                                               'Xmin',
                                               'Ymin',
                                               'Xmax',
                                               'Ymax'))
    self._pattern = pattern
    self._cnt = 0

  def __call__(self, image, bbox):
    """ Dump bbox into csv file """
    bbox_id = self._pattern.format(self._cnt)
    xmin, ymin, xmax, ymax = bbox
    entry = '{},{},{},{},{}'.format(bbox_id, xmin, ymin, xmax, ymax)
    self._file.write('{}\n'.format(entry))
    self._file.flush()
    self._cnt += 1


class ImageResolutionSelector(Criterion):
  """ Select face region matching the low/high resolution definition """

  def __init__(self,
               output_location,
               size,
               margin=0.1,
               resize=True,
               pattern='{:08}.jpg'):
    """
    Constructor.

    Region will be selected if the actual bounding box size is in range of:
      size - margin < bbox_size < size + margin

    :param output_location: Where to dump the region that fulfill the
                            requirements
    :param size:            Required image size
    :param margin:          Margin in [0, 1]
    :param resize:          If `True` the patches will be resized to the
                            required `size`
    :param pattern:         Naming convention for output location
    """
    dw = int(size * margin)
    self._size = size
    self._lower_sz = size - dw
    self._upper_sz = size + dw
    self._resize = resize
    self._pattern = _join(output_location, pattern)
    # Check if output location exist
    if not _exists(output_location):
      _makedirs(output_location)
    self._cnt = 0

  def __call__(self, image, bbox):
    """
    Run region check + export if requirements are fulfilled
    :param image: Image corresponding to the bounding box
    :param bbox:  Bounding box to check
    """
    # Get size
    xmin, ymin, xmax, ymax = bbox
    sz = xmax - xmin
    if self._lower_sz < sz < self._upper_sz:
      patch = image
      # Resize ?
      if self._resize:
        patch = resize(patch.astype(_np.float32),
                       output_shape=(self._size, self._size)).astype(_np.uint8)
      # Dump
      _imwrite(self._pattern.format(self._cnt), patch)
      self._cnt += 1

  @property
  def count(self):
    """ Number of samples selected """
    return self._cnt