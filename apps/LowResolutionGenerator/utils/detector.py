# coding=utf-8
import numpy as _np
from .base import DetectorAdapter
from lts5.tensorflow_op import S3FaceDetector as _S3FaceDetector

__author__ = 'Christophe Ecabert'


class S3FD(DetectorAdapter):
  """ Adapter for S3FD face detector """

  def __init__(self, model_path):
    """
    Constructor
    :param model_path:  Path to the face detector model
    """
    self._detector = _S3FaceDetector(model_path=model_path)

  def process(self, image):
    """
    Run detection
    :param image: Image to be processed, RGB formatted
    :return:  Detected bounding boxes
    """
    detection = self._detector.process(image_or_path=image)
    return detection[:, 3:].astype(_np.int32)
