# coding=utf-8
""" Interfaces definition """
from abc import ABC, abstractmethod

__author__ = 'Christophe Ecabert'


class DetectorAdapter(ABC):
  """ Thin wrapper around face detector """

  @abstractmethod
  def process(self, image):
    """
    Run detection
    :param image: Image to be processed, RGB formatted
    :return:  Detected bounding boxes
    """
    pass


class Criterion(ABC):
  """ Interface for face region assessment """

  @abstractmethod
  def __call__(self, image, bbox):
    """
    Run assessment. If face bounding box need to be selected, return `True`
    otherwise return `False`
    :param image: Full size image {numpy.ndarray}
    :param bbox:  Face region {numpy.ndarray}
    :return:  Boolean
    """
    pass


class BBoxTransformer(ABC):
  """ Apply transformation to a bounding box """

  @abstractmethod
  def __call__(self, bbox, width, height):
    """
    Transform a given bounding box
    :param bbox:   Original bounding box
    :param width:  Image width
    :param height: Image height
    :return:  Transform bounding box
    """
    pass


class RegionExporter(ABC):
  """ Interface for face region exportation """

  @abstractmethod
  def __call__(self, image, bbox):
    """
    Export a given face region
    :param image:   Image
    :param bbox:    Face region
    :return:        exported name
    """
    pass
