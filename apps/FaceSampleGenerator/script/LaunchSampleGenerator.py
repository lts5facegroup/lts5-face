# coding=utf-8
import argparse
import colorlog
import logging
import os
import itertools
import shlex
import subprocess
import re
__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
# Define Logger
# See : https://github.com/borntyping/python-colorlog
formatter = colorlog.ColoredFormatter(
    "%(log_color)s%(levelname)-8s%(reset)s | %(message)s",
    datefmt=None,
    reset=True,
    log_colors={'DEBUG': 'white',
                'INFO': 'green',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red'},
    secondary_log_colors={},
    style='%'
)
handler = colorlog.StreamHandler()
handler.setFormatter(formatter)
script_name = os.path.basename(__file__).strip().split('.')[0]
logger = colorlog.getLogger(script_name + 'Logger')
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


def scan_file(folder, ext, filt=None):
    r"""
        Scan recursively oa given folder for a specific extension. Filter can be 
        provided to only select a subset of the matching element
        :param folder:  Root folder to scan
        :param ext:     Extension to look for
        :param filt:    Optional filtering key
        :return:        List of files detected
        """
    pick = []
    for root, dirs, files in os.walk(folder):
        for f in files:
            if f.endswith(ext):
                if filt is None or filt in f:
                    pick.append(os.path.join(root[len(folder):], f))
    pick.sort()
    return pick


# ----------------------------------------------------------------------------
class GeneratorLauncher:

    def __init__(self, args):
        """
        Constructor
    
        :param args:    Structure with the parameters needed by the launcher
        """
        # Data folder location
        self._input = args.root if args.root[-1] == '/' else args.root + '/'
        # Angles
        self._angles = args.angles
        # Increments
        self._incs = args.incs
        # Steps
        self._steps = args.steps
        # Tracker
        self._tracker = args.tracker
        # Generator executable
        self._exec = args.exe
        # Destination
        self._out = args.output if args.output[-1] == '/' else args.output + '/'

    def run(self):
        r"""
        Generate a bunch of samples from BU3DFE dataset
        """
        # Scan for meshes + textures
        meshes = scan_file(self._input, 'ply')
        textures = scan_file(self._input, 'bmp')
        if len(meshes) == len(textures):
            # Define cmd line template
            cmd_t = self._exec + ' -m MESH'
            cmd_t += ' -t TEX'
            cmd_t += ' -f \"%s\"' % self._tracker
            cmd_t += ' -a \"%s\"' % self._angles
            cmd_t += ' -i \"%s\"' % self._incs
            cmd_t += ' -n %s' % self._steps
            cmd_t += ' -o OUT'



            # Iterate over
            for mesh, tex in itertools.izip(meshes, textures):
                # update mesh + tex
                cmd = cmd_t.replace('MESH', self._input + mesh)
                cmd = cmd.replace('TEX', self._input + tex)
                cmd = cmd.replace('OUT',
                                  self._out + os.path.dirname(mesh) + '/')

                # Define cmdline
                cmdline = shlex.split(cmd)
                s = subprocess.Popen(cmdline,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                cmd_out, cmd_err = s.communicate()
                if s.returncode != 0:
                    logger.error('Generator fail to run correctly '
                                 + str(s.returncode))
                    logger.error('stdout : ' + cmd_out)
                    logger.error('stderr : ' + cmd_err)
                # Clear
                del s
        else:
            logger.error('Number of mesh/texture does not match')
            exit(-1)


# ----------------------------------------------------------------------------
# Entry point
if __name__ == '__main__':
    # Parser
    parser = argparse.ArgumentParser(description='Automatic face sample '
                                                 'generator launcher')
    # Input
    parser.add_argument('-f',
                        type=str,
                        required=True,
                        dest='root',
                        help='BU3D-FE Root folder')
    # Camera position
    parser.add_argument('-a',
                        type=str,
                        required=True,
                        dest='angles',
                        help='List of camera angles around Y axis : '
                             '\"a0;a1;..\"')
    # Increment step
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='incs',
                        help='List of increment step around Y axis : '
                             '\"i0;i1;..\"')
    # Number of steps
    parser.add_argument('-n',
                        type=str,
                        required=True,
                        dest='steps',
                        help='Number of steps to performs')
    # Tracker models
    parser.add_argument('-t',
                        type=str,
                        required=True,
                        dest='tracker',
                        help='Path to tracker model files "tracker;fdetect"')
    # Exectuble location
    parser.add_argument('-e',
                        type=str,
                        required=True,
                        dest='exe',
                        help='Path to generator executable : '
                             '"./sample_generator"')
    # Output location
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='output',
                        help='Location where to place the generated data')
    args = parser.parse_args()

    # Create Launcher
    launcher = GeneratorLauncher(args)
    # Run
    launcher.run()

