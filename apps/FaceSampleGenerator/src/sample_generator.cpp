/**
 *  @file   sample_generator.cpp
 *  @brief  Generate Images from 3D Mesh given a set of camera parameters. Can
 *          be used to generate training samples (i.e. with BU3D-FE dataset)
 *  @ingroup    app
 *
 *  @author Christophe Ecabert
 *  @date   21/08/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <vector>

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/math/quaternion.hpp"
#include "lts5/utils/math/constant.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/sys/file_system.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/octree.hpp"
#include "lts5/geometry/aabb_tree.hpp"
#include "lts5/geometry/intersection_utils.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/ogl/offscreen_renderer.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"

#define IMAGE_SIZE 512
#define CAM_FOV 40.f
#define CAM_FAR 10000.f
#define CAM_NEAR 0.001f
#define CAM_RANGE_SCALE 0.8

/**
 * @struct  Params
 * @brief   Input parameters
 * @tparam T    Data type
 */
template<typename T>
struct Params {
  /** Mesh */
  std::string mesh_file;
  /** Mesh name */
  std::string mesh_name;
  /** Texture */
  std::string texture_file;
  /** Tracker */
  std::string tracker_file;
  /** Face Detector */
  std::string fdetect_file;
  /** Angles */
  std::vector<T> angle;
  /** Increment */
  std::vector<T> increment;
  /** number of step */
  size_t n_step;
  /** Output */
  std::string output_folder;
};

/**
 * @struct  OGLComponent
 * @brief   OpenGL container
 * @tparam T    Data type
 */
template<typename T>
struct OGLComponent {
  /** Renderer */
  LTS5::OGLOffscreenRenderer<T>* renderer;
  /** Program */
  LTS5::OGLProgram* program;
  /** Texture */
  LTS5::OGLTexture* texture;

  /**
   * @name  OGLComponent
   * @brief Constructor
   */
  OGLComponent(void) : renderer(nullptr), program(nullptr), texture(nullptr) {
  }

  /**
   * @name  ~OGLComponent
   * @brief Destructor
   */
  ~OGLComponent(void) {
    if (texture) {
      delete texture;
      texture = nullptr;
    }
    if (program) {
      delete program;
      program = nullptr;
    }
    if (renderer) {
      delete renderer;
      renderer = nullptr;
    }
  }
};

/**
 * @struct  String
 * @brief   Helper class for string conversion: str->num
 * @tparam T    Data type
 */
template<typename T>
struct String {
  static T Convert(const std::string& number);
};

template<typename T>
T String<T>::Convert(const std::string& number) {
  return T(-1.0);
}
template<>
float String<float>::Convert(const std::string& number) {
  return std::stof(number);
}
template<>
double String<double>::Convert(const std::string& number) {
  return std::stod(number);
}
template<>
size_t String<size_t>::Convert(const std::string& number) {
  size_t v;
  sscanf(number.c_str(), "%zu", &v);
  return v;
}

/**
 * @name deg2rad
 * @brief Convert angle in degrees into radians
 * @tparam T Data type
 * @param degree Angle in degree to convert to rad
 * @return  Angle in radians
 */
template<typename T>
inline T deg2rad(const T degree) {
  static const T pi_on_180 = T(4.0) * std::atan (T(1.0)) / T(180.0);
  return degree * pi_on_180;
}

/**
 * @name    ParseInput
 * @brief   Populate parameters struct from parsed input
 * @tparam T    Data type
 * @param[in] parser Input parser
 * @param[out] params Populated parameters
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
int ParseInput(const LTS5::CmdLineParser& parser, Params<T>* params) {
  // Input
  parser.HasArgument("-m", &params->mesh_file);
  std::string dir, ext;
  LTS5::ExtractDirectory(params->mesh_file, &dir, &params->mesh_name, &ext);
  // Texture
  parser.HasArgument("-t", &params->texture_file);
  // Tracker
  std::string buff;
  std::vector<std::string> parts;
  parser.HasArgument("-f", &buff);
  LTS5::SplitString(buff, ";", &parts);
  params->tracker_file = parts[0];
  params->fdetect_file = parts[1];
  // Position
  LTS5::Vector3<T> axis(T(0.0), T(1.0), T(0.0));
  parser.HasArgument("-a", &buff);
  parts.clear();
  LTS5::SplitString(buff, ";", &parts);
  for (auto& p : parts) {
    T ang = String<T>::Convert(p);
    params->angle.push_back(deg2rad(ang));
  }
  // Increments
  parts.clear();
  parser.HasArgument("-i", &buff);
  LTS5::SplitString(buff, ";", &parts);
  for (auto& p : parts) {
    T inc = String<T>::Convert(p);
    params->increment.push_back(deg2rad(inc));
  }
  // #step
  parser.HasArgument("-n", &buff);
  params->n_step = String<size_t>::Convert(buff);
  // Output
  parser.HasArgument("-o", &params->output_folder);
  // Check
  int err = params->angle.size() == params->increment.size() ? 0 : -1;
  return err;
}

/**
 * @name    LoadMesh
 * @brief   Load Mesh and texture
 * @tparam T Data type
 * @param[in] params    Input parameters
 * @param[out] mesh     Loaded mesh
 * @param[out] texture  Loaded texture, can be empty if no texture provided
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int LoadMesh(const Params<T>& params, LTS5::Mesh<T>* mesh, cv::Mat* texture) {
  int err = mesh->Load(params.mesh_file);
  if (err == 0) {
    auto& tcoords = mesh->get_tex_coord();
    if (!tcoords.empty()) {
      for (auto& tc : tcoords) {
        tc.y_ = T(1.0) - tc.y_;
      }
      if (!params.texture_file.empty()) {
        *texture = cv::imread(params.texture_file);
        err = texture->empty() ? -1 : 0;
      }
    } else {
      err = -1;
      LTS5_LOG_ERROR("Missing texture coordinates !");
    }
  }
  return err;
}

/**
 * @name    InitCamera
 * @brief   Initialize cameras
 * @tparam T    Data type
 * @param[in] params    List of input parameters
 * @param[in] mesh      Mesh to display
 * @param[out] cams     Initialized cameras
 */
template<typename T>
void InitCamera(const Params<T>& params,
                const LTS5::Mesh<T>& mesh,
                const size_t idx,
                std::vector<LTS5::OGLCamera>* cams) {
  cams->clear();
  // Define radius
  const auto& bbox = mesh.bbox();
  const auto dt = bbox.max_ - bbox.center_;
  const T r = std::sqrt(dt.x_ * dt.x_ + dt.z_ + dt.z_); //dt.Norm();
  // Loop over all cameras
  LTS5::Matrix3x3<T> rmat;
  LTS5::Vector3<T> pos, pcam, axis(T(0.0), T(1.0), T(0.0));
  for (size_t i = 0; i < params.angle.size(); ++i) {
    // Camera
    LTS5::OGLCamera cam(IMAGE_SIZE, IMAGE_SIZE);
    cam.set_field_of_view(CAM_FOV);
    cam.set_planes(CAM_NEAR, CAM_FAR);
    T dz = r / std::sin(cam.get_field_of_view()  *
                        LTS5::Constants<float>::PI *
                        0.002777778f);
    dz *= T(CAM_RANGE_SCALE);
    // Rotation angles
    const T ang = params.angle[i] + T(idx) * params.increment[i];
    LTS5::Quaternion<T> q(axis, ang);
    // Define position
    pos.x_ = T(0.0);
    pos.y_ = T(0.0);
    pos.z_ = dz;
    q.ToRotationMatrix(&rmat);
    pcam = rmat * pos;
    // Init cam
    cam.LookAt(glm::vec3(pcam.x_, pcam.y_, pcam.z_),
               glm::vec3(0.0, 0.0, 0.0));
    // Add to list
    cams->push_back(cam);
  }
}

/**
 * @name    InitOpenGL
 * @brief   Initialize OpenGL component
 * @tparam T    Data type
 * @param[in] mesh  Mesh to render
 * @param[in] texture   Texture
 * @param[out] ogl_component    Ogl component initialized
 * @return  -1 if error, 0 otherwise
 */
template<typename T>
int InitOpenGL(const LTS5::Mesh<T>& mesh,
               cv::Mat& texture,
               OGLComponent<T>* ogl_component) {
  using Renderer = LTS5::OGLOffscreenRenderer<T>;
  using AttributeType = LTS5::OGLProgram::AttributeType;
  // Init renderer (init OpenGL context)
  ogl_component->renderer = new Renderer();
  int err = ogl_component->renderer->Init(IMAGE_SIZE,
                                          IMAGE_SIZE,
                                          false,
                                          Renderer::ImageType::kRGBA);
  // Create shader + program
  std::string vertex_shader_str = "#version 330\n"
          "layout (location = 0) in vec3 position;\n"
          "layout (location = 1) in vec3 normal;\n"
          "layout (location = 2) in vec2 tex_coord;\n"
          "uniform mat4 camera;\n"
          "out vec2 tex_coord0;\n"
          "out vec3 normal0;\n"
          "void main() {\n"
          "  gl_Position = camera * vec4(position, 1);\n"
          "  normal0 = normal;\n"
          "  tex_coord0 = tex_coord;\n"
          "}";
  /*std::string vertex_shader_str = "#version 330\n"
          "layout (location = 0) in vec3 position;\n"
          "layout (location = 1) in vec3 normal;\n"
          "layout (location = 2) in vec4 color;\n"
          "uniform mat4 camera;\n"
          "out vec4 color0;\n"
          "out vec3 normal0;\n"
          "void main() {\n"
          "  gl_Position = camera * vec4(position, 1);\n"
          "  normal0 = normal;\n"
          "  color0 = color;\n"
          "}";*/
  std::string frag_shader_str = "#version 330\n"
          "in vec2 tex_coord0;\n"
          "in vec3 normal0;\n"
          "uniform sampler2D tex_sampler;\n"
          "out vec4 frag_color;\n"
          "void main() {\n"
          "  vec3 t = texture(tex_sampler, tex_coord0).rgb;\n"
          "  frag_color = vec4(t, 1.f);\n"
          "}";
  using ShaderType = LTS5::OGLShader::ShaderType;
  std::vector<LTS5::OGLShader> shaders;
  shaders.emplace_back(LTS5::OGLShader(vertex_shader_str, ShaderType::kVertex));
  shaders.emplace_back(LTS5::OGLShader(frag_shader_str, ShaderType::kFragment));
  std::vector<LTS5::OGLProgram::Attributes> attrib;
  attrib.emplace_back(LTS5::OGLProgram::Attributes(AttributeType::kPoint,
                                                   0,
                                                   "position"));
  attrib.emplace_back(LTS5::OGLProgram::Attributes(AttributeType::kNormal,
                                                   1,
                                                   "normal"));
  attrib.emplace_back(LTS5::OGLProgram::Attributes(AttributeType::kTexCoord,
                                                   2,
                                                   "tex_coord"));
  ogl_component->program = new LTS5::OGLProgram(shaders, attrib);
  // Texture
  if (!texture.empty()) {
    ogl_component->texture = new LTS5::OGLTexture(texture,
                                                  GL_LINEAR,
                                                  GL_CLAMP_TO_EDGE,
                                                  false);
    err |= ogl_component->texture->Upload(texture);
  }
  // Add item to renderer
  ogl_component->renderer->AddMesh(mesh);
  ogl_component->renderer->AddProgram(*ogl_component->program);
  if (ogl_component->texture) {
    ogl_component->renderer->AddTexture(*ogl_component->texture);
  }
  // Bind to opengl
  err |= ogl_component->renderer->BindToOpenGL();
  return err;
}

/**
 * @name    AddLeadingZero
 * @brief Convert a number into string and add some leading zero
 * @tparam T    Data type
 * @param[in] value Number to convert
 * @param[in] N     Number total of number in the final string
 * @return  Converted number
 */
template<typename T>
std::string AddLeadingZero(const T value, const size_t N) {
  std::string num = std::to_string(value);
  return std::string(N - num.length(), '0') + num;
}

/**
 * @name    LoadLandmarkIndex
 * @brief Load landmark indexes
 * @param[in] filename  Path to 3D points
 * @param[in] mesh      Corresponding mesh
 * @param[out] index    List of indexes
 * @return -1 if error, 0 otherwise
 */
template<typename T>
int LoadLandmarkIndex(const std::string& filename,
                      const LTS5::Mesh<T>& mesh,
                      std::vector<size_t>* index) {
  int err = -1;
  std::ifstream stream(filename.c_str());
  std::vector<LTS5::Vector3<T>> pts;
  if (stream.is_open()) {
    index->clear();
    std::stringstream sstream;
    int dummy;
    LTS5::Vector3<T> p;
    while(!stream.eof()) {
      std::string line;
      std::getline(stream, line);
      if (!line.empty()) {
        sstream.str(line);
        sstream >> dummy >> p.x_ >> p.y_>> p.z_;
        pts.push_back(p);
        sstream.clear();
      }
    }
    // Find correspondance
    using PrimitiveType = typename LTS5::Mesh<T>::PrimitiveType;
    const auto& vertex = mesh.get_vertex();
    LTS5::OCTree<T> tree;
    tree.Insert(mesh, PrimitiveType::kPoint);
    tree.Build();
    for (size_t i = 0; i < pts.size(); ++i) {
      LTS5::Sphere<T> query(pts[i], 0.5);
      std::vector<int> candidates;
      tree.RadiusNearestNeighbor(mesh, query, &candidates);
      // Find closest candidate

      auto cmp = [&vertex, &query](int k, int l) {
        const T dk = (vertex[k] - query.center_).Norm();
        const T dl = (vertex[k] - query.center_).Norm();
        return dk < dl;
      };

      auto it = std::min_element(candidates.begin(), candidates.end(), cmp);
      if (it != candidates.end()) {
        std::cout << "Query : " << pts[i] << std::endl;
        std::cout << "\tClosest : " << vertex[*it] << std::endl;

        index->push_back(static_cast<size_t>(*it));
      }
    }
    err = index->size() >= 68 ? 0 : -1;
  }
  return err;
}

template<typename T>
int FindLandmarkCorrespondance(const Params<T>& params,
                               const OGLComponent<T>& ogl,
                               const LTS5::Mesh<T>& mesh,
                               std::vector<size_t>* index) {
  // Load face tracker
  LTS5::SdmTracker tracker;
  int err = tracker.Load(params.tracker_file, params.fdetect_file);
  if (err == 0) {
    // CAMERA
    // ---------------------------------------------------
    LTS5::OGLCamera cam(IMAGE_SIZE, IMAGE_SIZE);
    const auto& bbox = mesh.bbox();
    const auto dt = bbox.max_ - bbox.center_;
    const T r = dt.Norm();
    cam.set_field_of_view(CAM_FOV);
    cam.set_planes(CAM_NEAR, CAM_FAR);
    T dz = r / std::sin(cam.get_field_of_view()  *
                        LTS5::Constants<float>::PI *
                        0.002777778f);
    dz *= T(CAM_RANGE_SCALE);
    // Init cam
    cam.LookAt(glm::vec3(0.0, 0.0, dz),
               glm::vec3(0.0, 0.0, 0.0));
    glm::mat4 trsfrm = cam.projection() * cam.view(false);
    glm::mat4 projection = cam.projection();
    glm::mat4 view = cam.view(false);

    cv::Mat Kcam(4, 4, cv::DataType<T>::type);
    cv::Mat Mview(4, 4, cv::DataType<T>::type);
    for (int row = 0; row < 4; ++row) {
      for (int col = 0; col < 4; ++col) {
        Kcam.at<T>(row, col) = projection[col][row];
        Mview.at<T>(row, col) = view[col][row];
      }
    }
    // RENDER
    // ---------------------------------------------------
    ogl.program->Use();
    ogl.program->SetUniform("camera", trsfrm);
    if (ogl.texture) {
      ogl.program->SetUniform("tex_sampler", 0);
    }
    ogl.program->StopUsing();
    // Render
    ogl.renderer->ActiveContext();
    ogl.renderer->Render();
    // Get image
    cv::Mat image, vflip_image;
    ogl.renderer->GetImage(&image);
    cv::flip(image, vflip_image, 0);
    // TRACK
    // ---------------------------------------------------
    cv::Mat shape;
    tracker.Detect(vflip_image, &shape);
    if (!shape.empty()) {
      cv::Mat canvas = vflip_image.clone();
      for (int i = 0; i < 68; ++i) {
        cv::circle(canvas, cv::Point(shape.at<double>(i), shape.at<double>(i+68)), 2, CV_RGB(0, 255, 0), -1);
      }
      //cv::imshow("detect", canvas);
      //cv::waitKey(0);
      // Back-Projection
      // ---------------------------------------------------
      // Move mesh into camera coordinate system
      LTS5::Mesh<T> cam_mesh;
      cam_mesh.set_triangle(mesh.get_triangle());
      cam_mesh.set_vertex(mesh.get_vertex());
      cam_mesh.Transform(Mview);
      // Convert to NDC
      int n_pts = std::max(shape.cols, shape.rows) / 2;
      cv::Mat Pwin = cv::Mat(4, n_pts, cv::DataType<T>::type);
      for (int p = 0; p < n_pts; ++p) {
        Pwin.at<T>(0, p) = static_cast<T>(shape.at<double>(p));  // x
        Pwin.at<T>(1, p) = static_cast<T>(IMAGE_SIZE - shape.at<double>(p + n_pts));  // y
        Pwin.at<T>(2, p) = T(0.0);
        Pwin.at<T>(3, p) = T(1.0);
      }
      cv::Mat WinToNDC = cv::Mat::zeros(4, 4, cv::DataType<T>::type);
      WinToNDC.at<T>(0, 0) = T(2.0) / IMAGE_SIZE;
      WinToNDC.at<T>(1, 1) = T(2.0) / IMAGE_SIZE;
      WinToNDC.at<T>(0, 3) = T(-1.0);
      WinToNDC.at<T>(1, 3) = T(-1.0);
      WinToNDC.at<T>(2, 3) = T(-1.0);
      WinToNDC.at<T>(3, 3) = T(1.0);
      cv::Mat Pndc = WinToNDC * Pwin;
      // Transform from NDC to world + normalize homogenous coordinate
      cv::Mat Pclip = Kcam.inv() * Pndc;
      for (int i = 0; i < n_pts; ++i) {
        Pclip.at<T>(0, i) /= Pclip.at<T>(3, i);
        Pclip.at<T>(1, i) /= Pclip.at<T>(3, i);
        Pclip.at<T>(2, i) /= Pclip.at<T>(3, i);
      }
      // Search tree
      LTS5::AABBTree<T> tree;
      tree.Insert(cam_mesh);
      tree.Build();
      LTS5::Vector3<T> Ocam;
      const auto& tri = cam_mesh.get_triangle();
      const auto& vertex = cam_mesh.get_vertex();
      for (int p = 0; p < n_pts; ++p) {
        // Define query ray
        LTS5::Vector3<T> ray(Pclip.at<T>(0, p),
                             Pclip.at<T>(1, p),
                             Pclip.at<T>(2, p));
        ray.Normalize();
        auto ray_far = ray * T(cam.get_far_plane());

        // Seek for intersection candidate
        std::vector<int> idx;
        std::vector<size_t> idx_candidate;
        tree.DoIntersect(Ocam, ray_far, &idx);
        T min_lambda = std::numeric_limits<T>::max();
        for (size_t id = 0; id < idx.size(); ++id) {
          T lambda;
          const auto& t = tri[idx[id]];
          const auto& A = vertex[t.x_];
          const auto& B = vertex[t.y_];
          const auto& C = vertex[t.z_];
          if (LTS5::IntersectTriWithLine(Ocam, ray, A, B, C, &lambda)) {
            // Intersect, find closest one
            if (lambda < min_lambda) {
              // update lambda
              min_lambda = lambda;
              // Find closest point
              auto Q = Ocam + lambda * ray;
              const T ra = (A - Q).Norm();
              const T rb = (B - Q).Norm();
              const T rc = (C - Q).Norm();
              if (ra < rb && ra < rc) {
                idx_candidate.push_back(t.x_);
              } else if (rb < ra && rb < rc) {
                idx_candidate.push_back(t.y_);
              } else {
                idx_candidate.push_back(t.z_);
              }
            }
          }
        }
        if (idx_candidate.empty()) {
          // Search for closest points to the ray
          size_t close_idx = 0;
          LTS5::ClosestPointToRay(Ocam, ray, vertex, &close_idx);
          index->push_back(close_idx);
          //std::cout << close_idx << std::endl;
        } else {
          index->push_back(idx_candidate.back());
          //std::cout << idx_candidate.back() << std::endl;
        }
      }
      err = index->size() == 68 ? 0 : -1;
    } else {
      err = -1;
    }
  }
  return err;
}

template<typename T>
void ProjectLandmark(const LTS5::Mesh<T>& mesh,
                     const std::vector<size_t>& idx,
                     const glm::mat4& mat,
                     cv::Mat* pts) {
  const auto& vertex = mesh.get_vertex();
  int N = static_cast<int>(idx.size());
  pts->create(N * 2, 1, cv::DataType<T>::type);
  LTS5::Vector4<T> vc;
  LTS5::Vector3<T> vn;
  for (int i = 0; i < N; ++i) {
    size_t vi = idx[i];
    const auto& v = vertex[vi];
    // See: http://www.songho.ca/opengl/gl_projectionmatrix.html
    // Vclip [col][row]
    vc.x_ = mat[0][0] * v.x_ + mat[1][0] * v.y_ + mat[2][0] * v.z_ + mat[3][0];
    vc.y_ = mat[0][1] * v.x_ + mat[1][1] * v.y_ + mat[2][1] * v.z_ + mat[3][1];
    vc.z_ = mat[0][2] * v.x_ + mat[1][2] * v.y_ + mat[2][2] * v.z_ + mat[3][2];
    vc.w_ = mat[0][3] * v.x_ + mat[1][3] * v.y_ + mat[2][3] * v.z_ + mat[3][3];
    // Vndc
    vn.x_ = vc.x_ / vc.w_;
    vn.y_ = vc.y_ / vc.w_;
    vn.z_ = vc.z_ / vc.w_;
    // Move to screen
    const T x = (vn.x_ + T(1.0)) * (T(IMAGE_SIZE) / T(2.0));
    const T y = (vn.y_ + T(1.0)) * (T(IMAGE_SIZE) / T(2.0));
    // Put to shape
    pts->at<T>(i) = x;
    pts->at<T>(i + N) = y;
  }
}

int main(int argc, const char** argv) {
  using Parser = LTS5::CmdLineParser;
  using T = double;
  // Parser
  Parser parser;
  // Mesh
  parser.AddArgument("-m",
                     Parser::ArgState::kNeeded,
                     "Path to mesh file");
  // Texture
  parser.AddArgument("-t",
                     Parser::ArgState::kOptional,
                     "Path to texture file");
  // Tracker
  parser.AddArgument("-f",
                     Parser::ArgState::kNeeded,
                     "Path to tracker file \"tracker;detector\"");
  // Camera angles
  parser.AddArgument("-a",
                     Parser::ArgState::kNeeded,
                     "Camera angles around Y axis. Can be a list \"a0;a1;..\"");
  // Camera angles increments
  parser.AddArgument("-i",
                     Parser::ArgState::kNeeded,
                     "Camera angles increment. Can be a list \"a0;a1;..\"");
  // Number of increment
  parser.AddArgument("-n",
                     Parser::ArgState::kNeeded,
                     "Number of increment");
  // Output
  parser.AddArgument("-o",
                     Parser::ArgState::kNeeded,
                     "Output location");
  int err = parser.ParseCmdLine(argc, argv);
  if (err == 0) {
    // Check input
    Params<T> params;
    err = ParseInput(parser, &params);
    if (err == 0) {
      // Load mesh
      LTS5::Mesh<T> mesh;
      cv::Mat texture;
      err = LoadMesh(params, &mesh, &texture);
      //err |= LoadLandmarkIndex(params.landmark_file, mesh, &indexes);
      if (err == 0) {
        // Mesh normalized
        mesh.NormalizeMesh();
        // Init opengl
        OGLComponent<T> ogl;
        err = InitOpenGL(mesh, texture, &ogl);
        if (err == 0) {
          std::vector<size_t> indexes;
          err = FindLandmarkCorrespondance(params, ogl, mesh, &indexes);
          if (err == 0 /*&& indexes.size() == 68*/) {
            // Cams
            std::vector<LTS5::OGLCamera> cameras;
            auto* fs = LTS5::GetDefaultFileSystem();
            for (size_t i = 0; i < params.n_step; ++i) {
              // Init cams
              InitCamera(params, mesh, i, &cameras);
              // Generate all sample
              cv::Mat image;
              for (size_t c = 0; c < cameras.size(); ++c) {
                auto &cam = cameras[c];
                glm::mat4 proj = cam.projection();
                glm::mat4 view = cam.view(true);
                glm::mat4 trsfrm = proj * view;
                ogl.program->Use();
                ogl.program->SetUniform("camera", trsfrm);
                if (ogl.texture) {
                  ogl.program->SetUniform("tex_sampler", 0);
                }
                ogl.program->StopUsing();
                // Render
                ogl.renderer->Render();
                // Get image
                ogl.renderer->GetImage(&image);
                // Project landmarks
                cv::Mat shape;
                ProjectLandmark(mesh, indexes, trsfrm, &shape);
                // Save ouput
                // -----------------------------------------------
                // Output folder, exist ?
                std::string path =
                        params.output_folder + params.mesh_name + "/";
                path += "C" + AddLeadingZero(c, 2) + "/";
                if (!fs->FileExist(path).Good()) {
                  fs->CreateDirRecursively(path);
                }
                // Filename
                T angle = params.angle[c] + T(i) * params.increment[c];
                angle *= T(180.0) / LTS5::Constants<T>::PI;
                angle = std::round(angle);
                std::string file = params.mesh_name;
                file += "_V" + AddLeadingZero(i, 2);
                file += "_C" + AddLeadingZero(c, 2);
                file += "_A" + std::to_string(int(angle));
                // Finally save outputs
                std::string img_path = path + "/" + file + ".jpg";
                cv::imwrite(img_path, image);
                std::string shp_path = path + "/" + file + ".pts";
                LTS5::SavePts(shp_path, shape);
              }
            }
          } else {
            LTS5_LOG_ERROR("Error, can not find vertex correspondences");
          }
        } else {
          LTS5_LOG_ERROR("Error, can not init OpenGL context");
        }
      } else {
        LTS5_LOG_ERROR("Unable to load mesh / texture");
      }
    } else {
      LTS5_LOG_ERROR("Input parameters not valid");
    }
  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
  return err;
}
