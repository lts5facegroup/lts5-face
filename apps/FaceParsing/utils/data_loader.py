# coding=utf-8
"""
Load image data for SkinNet experiments
"""
from random import random
import os.path as _path
import tensorflow as _tf
from lts5.tensorflow_op.data import DataProvider

__author__ = 'Christophe Ecabert'


class RandomScale:
  """ Randomly apply scaling on input data. Scale are chosen from a given set
  of values (i.e. categorical sampling) with equal probabilities"""

  def __init__(self, scale=(1.0,)):
    """
    Constructor
    :param scale: List of scaling factor to randomly sample from
    """
    self.scale = _tf.convert_to_tensor(scale, dtype=_tf.float32)

  def run(self, image, label):
    # Get current shape
    H, W = image.shape[:2]
    # Select scaling + define dims
    idx = _tf.random.uniform([],
                             minval=0,
                             maxval=len(self.scale),
                             dtype=_tf.int32)
    s = self.scale[idx]
    _h = _tf.cast(H * s, dtype=_tf.int32)
    _w = _tf.cast(W * s, dtype=_tf.int32)
    # Resize
    _im = _tf.image.resize(image, size=[_h, _w], method='bilinear')
    _lbl = _tf.image.resize(label, size=[_h, _w], method='nearest')
    # Crop or pad
    _im = _tf.image.resize_with_crop_or_pad(_im, H, W)
    _lbl = _tf.image.resize_with_crop_or_pad(_lbl, H, W)
    return _im, _lbl


class ColorJitter:
  """ Randomly apply color transformation on input data. Type of color
   transformations are change of:
      - Brightness
      - Contrast
      - Saturation
  """

  def __init__(self, brightness, saturation, contrast):
    """
    Constructor
    :param brightness:  Max brightness adjustment, must be in [0, 1) or None to
                        disable
    :param saturation:  Max brightness saturation, must be in [0, 1) or None to
                        disable
    :param contrast:    Max contrast adjustment, must be in [0, 1) or None to
                        disable
    """
    self.brightness = None
    self.saturation = None
    self.contrast = None
    if brightness is not None:
      self.brightness = max(min(brightness, 1.0), 0.0)
    if saturation is not None:
      self.saturation = max(min(saturation, 1.0), 0.0)
    if contrast is not None:
      self.contrast = max(min(contrast, 1.0), 0.0)

  def run(self, image, labels):
    """
    Perform color augmentation
    :param image:   Image to transform
    :param labels:  Labels, left untouched
    :return:  Transform images + labels
    """
    _im = image
    if self.brightness:
      _im = _tf.image.random_brightness(_im, self.brightness)
    if self.contrast:
      _im = _tf.image.random_contrast(_im,
                                      lower=1.0 - self.contrast,
                                      upper=1.0 + self.contrast)
    if self.saturation:
      _im = _tf.image.random_saturation(_im,
                                        lower=1.0 - self.saturation,
                                        upper=1.0 + self.saturation)
    return _im, labels


class HorizonalFlip:
  """ Apply random horizontal flip with a given probability """

  def __init__(self, prob, dataset='CelebA'):
    """
    Constructor
    :param prob:     Probability of flip [0, 1]
    :param dataset:  Type of dataset, must be one of ['Helen', 'CelebA']
    """
    self.p = max(min(prob, 1.0), 0.0)
    if not dataset.lower() in ['helen', 'celeba']:
      raise ValueError('Unknown type of dataset: {}'.format(dataset))

    # Build lookup table
    if dataset.lower() == 'helen':
      # Helen labels
      # Label 00: background
      # Label 01: face skin (excluding ears and neck)
      # Label 02: left eyebrow
      # Label 03: right eyebrow
      # Label 04: left eye
      # Label 05: right eye
      # Label 06: nose
      # Label 07: upper lip
      # Label 08: inner mouth
      # Label 09: lower lip
      # Label 10: hair
      keys = _tf.convert_to_tensor([2,
                                    3,
                                    4,
                                    5], dtype=_tf.int32)
      values = _tf.convert_to_tensor([3.0,
                                      2.0,
                                      5.0,
                                      4.0], dtype=_tf.float32)
      t_init = _tf.lookup.KeyValueTensorInitializer(keys=keys,
                                                    values=values)
    else:
      # CelebA
      # # Labels = [1 'skin',
      #             2 'l_brow',
      #             3 'r_brow',
      #             4 'l_eye',
      #             5 'r_eye',
      #             6 'eye_g',
      #             7 'l_ear',
      #             8 'r_ear',
      #             9 'ear_r',
      #             10 'nose',
      #             11 'mouth',
      #             12 'u_lip',
      #             13 'l_lip',
      #             14 'neck',
      #             15 'neck_l',
      #             16 'cloth',
      #             17 'hair',
      #             18 'hat']
      keys = _tf.convert_to_tensor([2,
                                    3,
                                    4,
                                    5,
                                    7,
                                    8],
                                   dtype=_tf.int32)
      values = _tf.convert_to_tensor([3.0,
                                      2.0,
                                      5.0,
                                      4.0,
                                      8.0,
                                      7.0],
                                     dtype=_tf.float32)
      t_init = _tf.lookup.KeyValueTensorInitializer(keys=keys,
                                                    values=values)
    # Lookup table
    self.lut = _tf.lookup.StaticHashTable(t_init, default_value=0.0)

  def run(self, image, label):
    """
    Do augmentation
    :param image: Image on which to apply transformation
    :param label: Labels on which to apply transformation
    :return:  Image + labels
    """
    _im = image
    _lbl = label
    p = _tf.random.uniform(shape=[], maxval=1.0, dtype=_tf.float32)
    if p > self.p:
      # Do flip
      _im = _tf.image.flip_left_right(_im)
      _lbl = _tf.image.flip_left_right(_lbl)
      # Change left/right labels
      _lbl = self.lut.lookup(_tf.cast(_lbl, _tf.int32))
    return _im, _lbl


def _corrupt_brightness(image, mask):
  """
  Randomly corrupt image brightness
  :param image:   Image
  :param mask:    Mask
  :return:  Corrupted image, or identical image
  """
  # 50% of chance to apply corruption
  cond = _tf.cast(_tf.random.uniform(shape=[],
                                     maxval=2,
                                     dtype=_tf.int32),
                  _tf.bool)
  # Apply correction or forward the image
  image = _tf.cond(cond,
                   lambda: _tf.image.random_brightness(image, 0.5),
                   lambda: _tf.identity(image))
  return image, mask


def _corrupt_contrast(image, mask):
  """
  Randomly applies a random contrast change.

  :param image:   Image
  :param mask:    Mask
  :return:  Corrupted image, or identical image
  """
  # 50% of chance to apply corruption
  cond = _tf.cast(_tf.random.uniform(shape=[],
                                     maxval=2,
                                     dtype=_tf.int32),
                  _tf.bool)
  # Apply transform
  image = _tf.cond(cond,
                   lambda: _tf.image.random_contrast(image, 0.5, 1.5),
                   lambda: _tf.identity(image))
  return image, mask


def _corrupt_saturation(image, mask):
  """
  Randomly applies a random saturation change.

  :param image:   Image
  :param mask:    Mask
  :return:  Corrupted image, or identical image
  """
  # 50% of chance to apply corruption
  cond = _tf.cast(_tf.random.uniform(shape=[],
                                     maxval=2,
                                     dtype=_tf.int32),
                  _tf.bool)
  # Apply transform
  image = _tf.cond(cond,
                   lambda: _tf.image.random_saturation(image, 0.5, 1.5),
                   lambda: _tf.identity(image))
  return image, mask


def _random_flip(image, mask):
  """
  Perform randomly image flip
  :param image: Image
  :param mask:  Label
  :return:  Transformed image + label
  """
  seed = random()
  image = _tf.image.random_flip_left_right(image, seed=seed)
  mask = _tf.image.random_flip_left_right(mask, seed=seed)
  return image, mask


class DataLoader(DataProvider):
  """ Load image + label into TF framework using `tf.data` API """

  def __init__(self,
               filename,
               img_size,
               training,
               with_auxiliary_loss=False,
               n_parallel_call=None):
    """
    Constructor
    :param filename:        Glob pattern
    :param img_size:        Tuple of integers, (Width, Height)
    :param training:        If `True` indicates training phase
    :param with_auxiliary_loss: If `True` indicate auxiliary loss
    :param n_parallel_call: Number of parallel call the pipeline is allowed to
                            do
    """
    super(DataLoader, self).__init__()
    self._n_call = n_parallel_call
    self._training = training
    self._img_sz = img_size
    self._with_auxiliary_loss = with_auxiliary_loss
    self._source = _tf.data.Dataset.list_files(filename,
                                               shuffle=training)
    self._source = self._source.interleave(_tf.data.TFRecordDataset,
                             cycle_length=self._n_call,
                             num_parallel_calls=_tf.data.experimental.AUTOTUNE)

  def decode(self):
    """
    Decode one line of a sample text file. Output two tensors, the image and the
    corresponding label/mask
    :param line:  Single line from a sample file (i.e. csv)
    :return:  Image, mask
    """

    def _parse_fn(example):
      """
            convert an tf.train.Example into tensors
            :param example: Dataset example
            :return:  dict
            """
      # Parse TFExample records and perform simple data augmentation.
      fmt = {
        'image': _tf.io.FixedLenFeature([], _tf.string, ""),
        'mask': _tf.io.FixedLenFeature([], _tf.string, ""),
      }
      parsed = _tf.io.parse_single_example(example, fmt)
      # Image
      image = _tf.io.decode_jpeg(parsed['image'])
      image.set_shape((self._img_sz[0], self._img_sz[1], 3))
      image = _tf.cast(image, _tf.float32)
      # Label
      label = _tf.io.decode_png(parsed['mask'], channels=1)
      label.set_shape((self._img_sz[0], self._img_sz[1], 1))
      label = _tf.cast(label, _tf.float32)
      return image, label

    # Decode samples
    self._source = self._source.map(_parse_fn, _tf.data.experimental.AUTOTUNE)
    return self

  def preprocess(self):
    """
    Add pre-processing to the input pipeline
    :return: `self`
    """

    def _normalize(image, label):
      if self._with_auxiliary_loss:
        return image, (label, label)
      else:
        return image, label

    # Apply pre-processing
    AUTO = _tf.data.experimental.AUTOTUNE
    if self._training:
      # self._source = self._source.map(_corrupt_brightness, self._n_call)
      # self._source = self._source.map(_corrupt_contrast, self._n_call)
      # self._source = self._source.map(_corrupt_saturation, self._n_call)

      rnd_color = ColorJitter(brightness=0.5, contrast=0.5, saturation=0.5)
      rnd_flip = HorizonalFlip(prob=0.5, dataset='CelebA')
      rnd_scale = RandomScale((0.75, 1.0, 1.25, 1.5, 1.75, 2.0))
      self._source = self._source.map(rnd_color.run, AUTO)
      self._source = self._source.map(rnd_flip.run, AUTO)
      self._source = self._source.map(rnd_scale.run, AUTO)
    self._source = self._source.map(_normalize, AUTO)
    return self
