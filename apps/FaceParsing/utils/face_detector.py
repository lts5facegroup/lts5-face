# coding=utf-8
""" Common interface for various face detector used when preparing data """
from math import floor
from abc import ABC, abstractmethod
import cv2 as _cv
import numpy as _np
import tensorflow as _tf
from lts5.tensorflow_op.network.sfd import S3FaceDetector

# from lts5.tensorflow_op.network.mtcnn import MTCNN as _MTCNN

__author__ = 'Christophe Ecabert'


class BBox:
  """ Face bounding box """

  def __init__(self, xmin, ymin, xmax, ymax, score=1.0):
    """
    Constructor
    :param xmin:  Min x
    :param ymin:  Min y
    :param xmax:  Max x
    :param ymax:  Max y
    """
    self.xmin = int(xmin)
    self.ymin = int(ymin)
    self.xmax = int(xmax)
    self.ymax = int(ymax)
    self.score = score

  def width(self):
    return self.xmax - self.xmin + 1

  def height(self):
    return self.ymax - self.ymin + 1

  def center(self):
    return (self.xmin + self.xmax) / 2.0, (self.ymin + self.ymax) / 2.0,

  def area(self):
    """
    Compute bbox area
    :return:  Area
    """
    w = self.width()
    h = self.height()
    return w * h

  def overlap(self, other):
    """
    Check if the boxes overlaps
    :param other: Other bbox to check against
    :return: True if overlap, False otherwise
    """
    # See: "Realtime Collision Detection: p79
    if self.xmax < other.xmin or self.xmin > other.xmax:
      return False
    if self.ymax < other.ymin or self.ymin > other.ymax:
      return False
    return True

  def iou(self, other):
    """
    Compute intersection over union between `this` and `other` bbox
    :param other: BBox to check against
    :return:  IoU
    """
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(self.xmin, other.xmin)
    yA = max(self.ymin, other.ymin)
    xB = min(self.xmax, other.xmax)
    yB = min(self.ymax, other.ymax)
    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = self.area()
    boxBArea = other.area()
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the intersection area
    iou = interArea / float(boxAArea + boxBArea - interArea)
    # return the intersection over union value
    return iou

  def inside(self, x, y):
    """
    Check if a given point (x, y) is inside the box
    :param x: X point coordinate
    :param y: Y point coordinate
    :return:  True, False
    """
    if self.xmin <= x <= self.xmax and self.ymin <= y <= self.ymax:
      return True
    return False

  def add_margin(self, margin):
    """
    Add extra margin to the bbox
    :param margin:  Margin percentage to add
    """
    dx = int(float(self.width()) * margin * 0.5)
    dy = int(float(self.height()) * margin * 0.5)
    self.xmin -= dx
    self.xmin += dx
    self.ymin -= dy
    self.ymax += dy
    # NOTE: Only apply margin toward the chin. Doing so gives results more
    # close to the original paper than apply dy in both direction.

  def clip_to_border(self, width, height):
    """
    Ensure the box is inside the given dimensions
    :param width:   Maximum possible x value
    :param height:  Maximum possible y value
    """
    # Box larger than image
    if self.width() > width:
      self.xmin = 0
      self.xmax = width - 1
    if self.height() > height:
      self.ymin = 0
      self.ymax = height - 1
    # Left
    if self.xmin < 0:
      self.xmax -= self.xmin
      self.xmin -= self.xmin
    # Right
    if self.xmax > width:
      dx = self.xmax - width
      self.xmin -= dx
      self.xmax -= dx
    # Top
    if self.ymin < 0:
      self.ymax -= self.ymin
      self.ymin -= self.ymin
    # Bottom
    if self.ymax > height:
      dy = self.ymax - height
      self.ymin -= dy
      self.ymax -= dy

  def square(self, clip_h=None, clip_w=None):
    """
    Convert bbox to a square box
    :param clip_h:  Clipping value for box's height
    :param clip_w:  Clipping value for box's width
    :return:  New squared bounding box
    """
    w = self.width()
    if clip_w is not None:
      w = w if w < clip_w else clip_w
    h = self.height()
    if clip_h is not None:
      h = h if h < clip_h else clip_h
    s = max(w, h) - 1
    cx, cy = self.center()
    xmin = int(round(cx - (s / 2.0)))
    xmax = xmin + int(s)
    if xmin < 0:
      xmax -= xmin  # Shift everything to the right
      xmin = 0
    ymin = int(round(cy - (s / 2.0)))
    ymax = ymin + int(s)
    if ymin < 0:
      ymax -= ymin  # Shift everything to the bottom
      ymin = 0
    return BBox(xmin, ymin, xmax, ymax, self.score)

  def __repr__(self):
    return '({}, {}, {}, {})'.format(self.xmin,
                                     self.ymin,
                                     self.xmax,
                                     self.ymax)


class AbstractDetector(ABC):
  """ Face detector abstraction """
  def __init__(self, name):
    self._name = name

  @abstractmethod
  def empty(self):
    """ Indicate if detector is loaded """
    return True

  @abstractmethod
  def __call__(self, image):
    """ Call operator """
    pass


# class DLibDetector(AbstractDetector):
#   """ Dlib face detector class """
#
#   def __init__(self, name='DLIB'):
#     """ Constructor """
#     super(DLibDetector, self).__init__(name=name)
#     # Create dlib detector
#     self._fd = _dlib.get_frontal_face_detector()
#
#   def empty(self):
#     """ Indicate if detector is loaded """
#     return self._fd is None
#
#   def __call__(self, image):
#     """
#     Run face detection on a given image
#     :param image: Image on which to run the detection
#     :return: Numpy array with detected rectangles by row
#     """
#     rects = self._fd(image)
#     rects = _np.array([[r.left(), r.top(),
#                         r.width(), r.height()] for r in rects])
#     if len(rects):
#       bbox = []
#       for r in rects:
#         r[2:] += r[:2]
#         bbox.append(BBox(xmin=r[0], ymin=r[1], xmax=r[2], ymax=r[2]))
#       return bbox
#     return None


class ViolaJonesDetector(AbstractDetector):
  """ OpenCV face detector """
  def __init__(self, model, name='ViolaJones'):
    """
    Constructor
    :param model: Path to detection model (*.xml)
    :param name:  Detector name
    """
    super(ViolaJonesDetector, self).__init__(name=name)
    self._fd = _cv.CascadeClassifier()
    self._fd.load(model)

  def empty(self):
    """ Indicate if detector is loaded """
    return self._fd.empty()

  def __call__(self, image):
    """
    Run face detection on a given image
    :param image: Image on which to run the detection
    :return: List of bbox
    """
    rects = self._fd.detectMultiScale(image=image,
                                      scaleFactor=1.05,
                                      minNeighbors=3,
                                      minSize=(100, 100),
                                      flags=_cv.CASCADE_SCALE_IMAGE)
    if len(rects):
      bbox = []
      for r in rects:
        r[2:] += r[:2]
        bbox.append(BBox(xmin=r[0], ymin=r[1], xmax=r[2], ymax=r[2]))
      return bbox
    return None


class SFDDetector(AbstractDetector):
  """ SFD face detector """

  def __init__(self, model, max_image_size=1280.0, name='SFD'):
    super(SFDDetector, self).__init__(name=name)
    self.fd = S3FaceDetector(model_path=model)
    self.image_sz = max_image_size

  def empty(self):
    """ Indicate if detector is loaded """
    return False

  def __call__(self, image):
    """
    Run face detection on a given image
    :param image: Image on which to run the detection
    :return: List of bbox
    """
    image_sz = float(max(image.shape[0], image.shape[1]))
    image_sz = self.image_sz if image_sz > self.image_sz else image_sz
    res = self.fd.process(image_or_path=image, image_size=image_sz)
    if len(res):
      bbox = []
      for r in res:
        bbox.append(BBox(xmin=r[3], ymin=r[4],
                         xmax=r[5], ymax=r[6],
                         score=r[2]))
      return bbox
    return None


# class MTCnnDetector(AbstractDetector):
#   """ MTCNN face detector """
#
#   def __init__(self, model, name='MTCNN'):
#     super(MTCnnDetector, self).__init__(name=name)
#     # Create detector instance
#     self._graph = _tf.Graph()
#     with self._graph.as_default():
#       self._mtcnn = _MTCNN(input_is_bgr=True)
#       # Create session + restore network
#       self._session = _tf.Session()
#       self._mtcnn.restore(session=self._session, path=model)
#
#   def __del__(self):
#     """ Destructor """
#     self._session.close()
#
#   def empty(self):
#     return False
#
#   def __call__(self, image):
#     """ Run detection """
#     with self._graph.as_default():
#       im = _np.expand_dims(image, 0)
#       res = self._mtcnn.inference(x=im, session=self._session)
#       if len(res) > 0:
#         bbox = []
#         for r in enumerate(res):
#           r = r['box']
#           r[2] += r[0]
#           r[3] += r[1]
#           bbox.append(BBox(xmin=r[0], ymin=r[1], xmax=r[2], ymax=r[2]))
#         return bbox
#       return None

