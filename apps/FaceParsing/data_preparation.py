# coding=utf-8
""" Prepare tfrecord to train Face-Parsing segmentation network """
from os.path import join as _join
from os.path import exists as _exists
from os import makedirs as _mkdir
from argparse import ArgumentParser
import cv2 as cv
from imageio import imwrite
import tensorflow as _tf
from utils.face_detector import SFDDetector, BBox
from lts5.utils import init_logger, search_folder
from lts5.tensorflow_op.utils import initialize_device_memory
from lts5.tensorflow_op.utils.tfrecords import to_feature

logger = init_logger()
initialize_device_memory()


def _create_datasets(pargs):
  """
  Instantiate list of datasets
  :param pargs: Parsed arguments
  :return: List of datasets
  """
  dsets = []
  if pargs.helen_face_parts_path is not None:
    from lts5.utils import HelenFaceParts
    dsets.append(HelenFaceParts(pargs.helen_face_parts_path))

  if pargs.celeba_mask_path is not None:
    from lts5.utils import CelebAMaskHq
    dsets.append(CelebAMaskHq(pargs.celeba_mask_path))
  # Add other datasets if needed
  return dsets


def process(pargs):
  """
  Run data preparation
  :param pargs: Pared command line
  """
  # Ensure output location exists
  if not _exists(pargs.output):
    _mkdir(pargs.output)
  # Setup datasets
  datasets = _create_datasets(pargs)
  # Load face detector
  fdet = SFDDetector(model=pargs.sfd)
  # Iterate over every images
  frame_cnt = 0
  for dset in datasets:
    for part in dset.infos.partitions():
      logger.info('Process: {} - {}'.format(dset.infos.name(), part))
      # Destination folders
      folder = _join(pargs.output, 'images', part)
      if not _exists(folder):
        _mkdir(folder)
      # Image + Labels
      for img, lbl, name in zip(dset.samples(part),
                                dset.annotations(part),
                                dset.names(part)):
        _, image = img
        _, label = lbl
        ratio = int(image.shape[0] // label.shape[0])
        logger.info('Process: {}'.format(name))
        # Run face detection
        bboxes = fdet(image)
        if bboxes is not None:
          for bbox in bboxes:
            if bbox.score > 0.75:
              # Add margin + square + inside
              bbox.add_margin(margin=pargs.margin)
              bbox.clip_to_border(width=image.shape[1], height=image.shape[0])
              r = bbox.square(clip_h=image.shape[0], clip_w=image.shape[1])
              # Crop
              x_1, y_1, x_2, y_2 = list(
                map(int, (r.xmin, r.ymin, r.xmax, r.ymax)))
              im_patch = image[y_1:y_2, x_1:x_2, :]
              if ratio == 1:
                lbl_patch = label[y_1:y_2, x_1:x_2]
              else:
                y_1r = y_1 // ratio
                y_2r = y_2 // ratio
                x_1r = x_1 // ratio
                x_2r = x_2 // ratio
                lbl_patch = label[y_1r:y_2r, x_1r:x_2r]
              # Check if annotation are present
              if (lbl_patch != 0).sum() > 100:
                im_patch_sz = cv.resize(im_patch,
                                        dsize=(pargs.img_size, pargs.img_size),
                                        interpolation=cv.INTER_LINEAR)
                lbl_patch_sz = cv.resize(lbl_patch,
                                         dsize=(pargs.img_size, pargs.img_size),
                                         interpolation=cv.INTER_NEAREST )
                imwrite(_join(folder, 'patch{:06d}.jpg'.format(frame_cnt)),
                        im_patch_sz)
                imwrite(_join(folder, 'patch{:06d}.png'.format(frame_cnt)),
                        lbl_patch_sz)
                frame_cnt += 1
              else:
                logger.warning('No annotation found in bbox')
            else:
              logger.warning('Drop bounding box, score too low'
                             ' {:.2f}'.format(bbox.score))
        else:
          logger.warning('No face detected')


def create_tfrecords(pargs):
  """
  Create tfrecords
  :param pargs: PArsed arguments
  """

  def _read_raw(path):
    """
    Load a given file into memory as a raw array
    :param path:  Path to the file
    :return:  Bytes array as a string
    """
    with open(path, 'rb') as f:
      return f.read()

  def _create_single_example(image_path, label_path):
    """
    Convert image+label pair into tf.train.Example object
    :param image_path:  Path to the image
    :param label_path:  Path to the label
    :return:  tf.train.Example
    """
    im_str = _read_raw(image_path)
    lbl_str = _read_raw(label_path)
    # Create example
    feat = {'image': to_feature(im_str),
            'mask': to_feature(lbl_str)}
    return _tf.train.Example(features=_tf.train.Features(feature=feat))

  def _convert_to_records(pargs, partition):
    """
    Convert a specific partitions to tfrecords
    :param pargs:     Parsed arguments
    :param partition: Partition to convert
    """
    # Load samples
    folder = _join(pargs.output, 'images', partition)
    images = search_folder(folder, ext=['.jpg'], relative=False)
    labels = search_folder(folder, ext=['.png'], relative=False)
    # Train, split it into various 'shards' for "faster" data processing with
    # tf.data pipeline
    folder = _join(pargs.output, 'records')
    if not _exists(folder):
      _mkdir(folder)
    n_split = (len(images) // pargs.train_shard_size) + 1
    for k in range(n_split):
      start = k * pargs.train_shard_size
      stop = (k + 1) * pargs.train_shard_size
      stop = stop if stop < len(images) else len(images)
      # Create tfrecords writer for the given shards
      wname = _join(pargs.output,
                    'records',
                    '{}_{:03d}.tfrecords'.format(partition, k))
      logger.info('Process %s',
                  'records/{}_{:02d}.tfrecords'.format(partition, k))
      with _tf.io.TFRecordWriter(path=wname) as writer:
        # Iterate over samples
        for im, lbl in  zip(images[start:stop], labels[start:stop]):
          example = _create_single_example(image_path=im, label_path=lbl)
          # Dump it
          writer.write(example.SerializeToString())

  _convert_to_records(pargs, 'train')
  _convert_to_records(pargs, 'validation')
  _convert_to_records(pargs, 'test')


if __name__ == '__main__':
  # Parser
  p = ArgumentParser()

  # Helen face parts
  p.add_argument('--helen_face_parts_path',
                 type=str,
                 help='Location where `Helen Face Parts` dataset is stored')
  p.add_argument('--celeba_mask_path',
                 type=str,
                 help='Location where `CelebA Mask` dataset is stored')
  # Face detector model (S3FD)
  p.add_argument('--sfd',
                 type=str,
                 required=True,
                 dest='sfd',
                 help='S3FD model (*.npy)')
  # BBox margin
  p.add_argument('--margin',
                 type=float,
                 required=False,
                 default=0.4,
                 dest='margin',
                 help='Extra margin for the face bbox (percentage)')
  # Image size
  p.add_argument('--img_size',
                 type=int,
                 required=False,
                 default=224,
                 dest='img_size',
                 help='Dimension of the cropped face')
  # Shard
  p.add_argument('--train_shard_size',
                 type=int,
                 default=2000,
                 help='Number of samples in a shard')
  # Output
  p.add_argument('--output',
                 required=True,
                 type=str,
                 help='Location where to place the data')

  args = p.parse_args()
  process(pargs=args)
  create_tfrecords(pargs=args)
