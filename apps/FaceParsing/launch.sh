#!/bin/bash
DATASETS_DIR="/media/christophe/Data/Datasets"
DATA_DIR="${DATASETS_DIR}/FaceParsingCelebARecords"
MODEL_DIR="/home/christophe/Documents/LTS5/Data/models"
OUTPUT_DIR="${MODEL_DIR}/face_parsing/bisenet_celeba_18_radam_ohem"
LEARNING_RATE=1e-4
WEIGHT_DECAY=1e-6
BATCH_SIZE=16
N_EPOCH=200
LOG_FREQ=1000

# Check if a mod is given
if [ "$#" -ne 1 ]; then
    echo "Wrong number of parameters"
    echo "$0 <mod>"
    echo "Possible mod: <Prepare> <Clear> <Train>"
    exit
fi

case "$1" in

    "Prepare")
        echo "Prepare data for training/validation/testing ..."
        python data_preparation.py --celeba_mask_path ${DATASETS_DIR}/CelebAMask-HQ --sfd ${MODEL_DIR}/sfd.npy --output ${DATA_DIR}
        ;;

    "Clear")
        read -p "Clear previous model in: ${OUTPUT_DIR}. Continue [y/n] ?" CHOICE
		case "$CHOICE" in
			y|Y )
				rm -rf ${OUTPUT_DIR}
			;;
			# Deal with other letter
			* ) echo "Stop current action";;
		esac
		;;

	"Train")
		echo "Train .."
		#python face_parsing_train.py --train_set "${DATA_DIR}/records/train_*.tfrecords" --validation_set "${DATA_DIR}/records/validation_*.tfrecords" --test_set "${DATA_DIR}/records/test_*.tfrecords" --model_dir ${OUTPUT_DIR} --lr ${LEARNING_RATE} --n_epoch ${N_EPOCH} --batch_size ${BATCH_SIZE} --weighted_class yes
		python face_parsing_bisenet_train.py --train_set "${DATA_DIR}/records/train_*.tfrecords" --validation_set "${DATA_DIR}/records/validation_*.tfrecords" --test_set "${DATA_DIR}/records/test_*.tfrecords" --model_dir ${OUTPUT_DIR} --log_freq ${LOG_FREQ} --lr ${LEARNING_RATE} --wd ${WEIGHT_DECAY} --n_epoch ${N_EPOCH} --batch_size ${BATCH_SIZE} --weighted_class no
		;;


	*)
		echo "Unknown mod"
		;;
esac
