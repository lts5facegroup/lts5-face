# coding=utf-8
"""
Train SkinNet model using TF Estimator framework
"""
from argparse import ArgumentParser
from os.path import join as _join
from distutils.version import LooseVersion
from multiprocessing import cpu_count
from matplotlib.colors import ListedColormap
import numpy as np
import tensorflow as _tf
from tensorflow.keras.callbacks import ModelCheckpoint
from utils.data_loader import DataLoader
from lts5.utils.tools import init_logger, str2bool
from lts5.tensorflow_op.network.bisenet import BiSeNet
from lts5.tensorflow_op.utils.device import initialize_device_memory
from lts5.tensorflow_op.callbacks import AugmentedTensorBoard
from lts5.tensorflow_op.callbacks import CosineAnnealingLearningRateSchedule


__author__ = 'Christophe Ecabert'

# Init LTS5 logger
logger = init_logger()
# Limit memory usage
initialize_device_memory()


# Ensure TF 2.0
tf_version = _tf.__version__
logger.info('Tensorflow version: %s', tf_version)
assert LooseVersion(tf_version) >= LooseVersion('2.0'),\
  "TensorFlow r2.0 or later is needed"


def input_fn(filepath,
             img_size,
             training,
             batch_size,
             with_auxiliary_loss=False,
             summary=None):
  """
  Load data from a text file `filepath`
  :param filepath:    Path to the file storing the input data
  :param img_size:    Image dimensions, (width, height)
  :param training:    Boolean flag indicating training phase
  :param batch_size:  Batch size
  :param drop_remainder: Indicate if not complete needs to be dropped
  :return:  Dataset's iterator
  """
  # Define number of possible parallel call
  n_call = cpu_count()
  # Create loader
  loader = (DataLoader(filename=filepath,
                       img_size=img_size,
                       training=training,
                       with_auxiliary_loss=with_auxiliary_loss,
                       n_parallel_call=n_call)
            .shuffle(buffer_size=(batch_size * 50))
            .decode()
            .preprocess()
            .batch(batch_size=batch_size, drop_remainder=True)
            .prefetch(_tf.data.experimental.AUTOTUNE))
  if summary:
    loader = loader.map(summary, None)
  # Get iterator
  return loader.one_shot_iterator()


def train(pargs):
  """
  Train skin segmentation network
  :param pargs: Parsed arguments
  """

  # Summary manager
  # ------------------------------------------------
  logdir = _join(pargs.model_dir, 'logs')
  # Define inputs dataset
  # ------------------------------------------------
  train_dataset = input_fn(pargs.train_set,
                           img_size=(224, 224),
                           training=True,
                           batch_size=pargs.batch_size,
                           with_auxiliary_loss=True)
  valid_dataset = input_fn(pargs.validation_set,
                           img_size=(224, 224),
                           training=False,
                           batch_size=pargs.batch_size,
                           with_auxiliary_loss=True)

  # Model
  # ------------------------------------------------
  n_classes = 19  #11
  # Set BN momentum to 0.9 for all BNs
  layer_params = {_tf.keras.layers.BatchNormalization.__name__: {'all': {'momentum': 0.9}}}
  model = BiSeNet(n_classes=n_classes,
                  type='resnet18',
                  with_auxiliary_head=True,
                  name='BiSeNet',
                  layer_params=layer_params)
  # Losses
  # ------------------------------------------------
  losses = model.get_losses()
  # Metrics
  # ------------------------------------------------
  # Define colormap for segmentation mask metrics
  colors = np.asarray([[0, 0, 0],
                       [204, 0, 0],
                       [76, 153, 0],
                       [204, 204, 0],
                       [51, 51, 255],
                       [204, 0, 204],
                       [0, 255, 255],
                       [255, 204, 204],
                       [102, 51, 0],
                       [255, 0, 0],
                       [102, 204, 0],
                       [255, 255, 0],
                       [0, 0, 153],
                       [0, 0, 204],
                       [255, 51, 153],
                       [0, 204, 204],
                       [0, 51, 0],
                       [255, 153, 51],
                       [0, 204, 0]]) / 255.0
  cmap = ListedColormap(colors)
  metrics = model.get_metrics(cmap=cmap)
  # Optimizer
  # ------------------------------------------------
  optimizer = model.get_optimizer(type='RectifiedAdam',
                                  learning_rate=pargs.lr,
                                  weight_decay=pargs.wd,
                                  total_steps=1000,
                                  warmup_proportion=0.25,
                                  min_lr=1e-2 * pargs.lr
                                  )
  # Weighted class
  # ------------------------------------------------
  class_weights = None
  if pargs.weighted_class:
    # See:
    # https://www.tensorflow.org/tutorials/structured_data/imbalanced_data#calculate_class_weights
    pos = dict(zip(range(n_classes),
                   [0] * n_classes))
    total = 0
    for _, label in train_dataset:
      shp = label[0].get_shape()
      lbl = _tf.cast(label, _tf.int32).numpy()
      for c in pos.keys():
        n_pixel = float((lbl == c).sum())
        pos[c] += n_pixel
      total += (shp[0] * shp[1] * shp[2])

    w = [total / (2 * pos[n]) for n in range(len(pos))]
    class_weights = dict(zip(range(n_classes), w))

    # class_weight can not be used with multi-output model (i.e. Main
    # prediction branch + auxiliary prediction branch). Therefore use the
    # sample_weights property instead. The weights are generated in the input
    # pipeline using a lookup table in form of `label` -> weight
    keys = []
    values = []
    for k, v in class_weights.items():
      keys.append(k)
      values.append(v)
    keys = _tf.convert_to_tensor(keys, dtype=_tf.int32)
    values = _tf.convert_to_tensor(values, dtype=_tf.float32)
    table_init = _tf.lookup.KeyValueTensorInitializer(keys=keys, values=values)
    table = _tf.lookup.StaticHashTable(table_init, default_value=1.0)

    def _class_weighting_fn(image, labels):
      cls_weight = table.lookup(_tf.cast(labels[0], _tf.int32))
      return image, (labels[0], labels[0], labels[0]), (cls_weight, cls_weight, cls_weight)

    train_dataset = train_dataset.map(_class_weighting_fn)
    valid_dataset = valid_dataset.map(_class_weighting_fn)

  else:
    def _dispatch_sample(image, labels):
      return image, (labels[0], labels[0], labels[0])

    train_dataset = train_dataset.map(_dispatch_sample)
    valid_dataset = valid_dataset.map(_dispatch_sample)

  # Compile
  # ------------------------------------------------
  model.compile(optimizer=optimizer,
                loss=losses,
                loss_weights=[1.0, 1.0, 1.0],
                metrics=metrics,
                run_eagerly=False)  # Set to `True` for debug
  # Callbacks
  # ------------------------------------------------
  helen_labels = ['Background',
                  'Face skin',
                  'Left Eyebrow',
                  'Right Eyebrow',
                  'Left Eye',
                  'Right Eye',
                  'Nose',
                  'Upper Lip',
                  'Inner Mouth',
                  'Lower Lip',
                  'Hair']
  celeba_labels = ['Background',
                   'Skin',
                   'Nose',
                   'EyeGlasses',
                   'Left Eye',
                   'Right Eye',
                   'Left Eyebrow',
                   'Right Eyebrow',
                   'Left Ear',
                   'Right Ear',
                   'Mouth',
                   'Upper Lip',
                   'Lower Lip',
                   'Hair',
                   'Hat',
                   'Earrings',
                   'Necklace',
                   'Neck',
                   'Cloth']
  names = helen_labels if n_classes == 11 else celeba_labels
  model_name = _join(pargs.model_dir, 'face_parsing.ckpt')
  tensorboad_cb = AugmentedTensorBoard(log_dir=logdir,
                                       histogram_freq=1,
                                       write_graph=True,
                                       write_images=False,
                                       update_freq=pargs.log_freq,
                                       profile_batch=0,
                                       embeddings_freq=0,
                                       labels=names,
                                       figsize=(6, 6))
  # Save model based on performance of validation loss
  checkpoint_cb = ModelCheckpoint(filepath=model_name,
                                  save_best_only=True,
                                  monitor='val_loss',
                                  mode='min',
                                  save_weights_only=True,
                                  verbose=1)

  # Learning rate scheduling
  # lr_scheduler = ReduceLROnPlateau(monitor='val_loss',
  #                                  factor=0.5,
  #                                  patience=10,
  #                                  cooldown=5,
  #                                  verbose=1,
  #                                  min_delta=1e-3,
  #                                  min_lr=1e-7,
  #                                  mode='min')
  lr_scheduler = CosineAnnealingLearningRateSchedule(n_epochs=pargs.n_epoch,
                                                     n_cycles=10,
                                                     lr=pargs.lr)
  callbacks = [checkpoint_cb,
               lr_scheduler,
               tensorboad_cb]

  # Reload
  latest = _tf.train.latest_checkpoint(pargs.model_dir)
  latest = latest if latest is not None else 'imagenet'
  model.load_weights(latest)

  # Train
  model.fit(x=train_dataset,
            callbacks=callbacks,
            epochs=pargs.n_epoch,
            validation_data=valid_dataset,
            validation_steps=None,
            class_weight=None)


if __name__ == '__main__':

  p = ArgumentParser(description='Skin segmentation network training script')

  # Train set location
  p.add_argument('--train_set',
                 type=str,
                 help='Location of training records is located (*.tfrecords)')
  # Validation set location
  p.add_argument('--validation_set',
                 type=str,
                 help='Location of validation records is located (*.tfrecords)')
  # Test set location
  p.add_argument('--test_set',
                 type=str,
                 help='Location of test records is located (*.tfrecords)')
  # Folder where to store model
  p.add_argument('--model_dir',
                 type=str,
                 help='Folder where the checkpoints are stored (*.ckpt)')
  # Learning rate
  p.add_argument('--lr',
                 type=float,
                 default=1e-5,
                 help='Learning rate')
  # Weight decay at optimizer level
  p.add_argument('--wd',
                 type=float,
                 default=1e-4,
                 help='Weight decay')
  # Number of epoch
  p.add_argument('--n_epoch',
                 type=int,
                 default=10,
                 help='Number of epoch to train for')
  # Batch size
  p.add_argument('--batch_size',
                 type=int,
                 default=16,
                 help='Number of samples in a single batch')
  # Logging frequency
  p.add_argument('--log_freq',
                 type=int,
                 default=100,
                 help='Logging frequency')
  # Weighted class
  p.add_argument('--weighted_class',
                 type=str2bool,
                 default=True,
                 help='Class weighting for imbalanced data')
  args = p.parse_args()
  # Train + Evaluate
  train(args)



