/**
 *  @file   main.cpp
 *  @brief  Example on how to use LTS5 face processing unit with multithreading
 *
 *  @authors Christophe Ecabert, Marco Lourenço
 *  @date   21/06/2016
 *  Copyright (c) 2016 Christophe Ecabert, Marco Lourenço. All rights reserved.
 */

#include <iostream>
#include <fstream>
#include <thread>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"

#include "face_processing_unit.hpp"

//------------------------------
int main(int argc, const char* argv[]) {
  //Enable, filter logger output
  LTS5::Logger::Instance().set_log_level(LTS5::Logger::Level::kWarning);
  // Parse cmd line
  LTS5::CmdLineParser parser;
  parser.AddArgument("-c",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face Unit configuration file (.yaml)");
  parser.AddArgument("-f",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Folder containing video input");
  parser.AddArgument("-n",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Number of frame to load");
  parser.AddArgument("-threads",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Number of threads to use");
  parser.AddArgument("-csv",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Path to CSV output file (profiling)");
  parser.AddArgument("-s",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Show output frames in a window");
  parser.AddArgument("-save",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Save frames to specified folder");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Retrieve args
    std::string config_file;
    std::string video_folder;
    std::string csv_file;
    std::string show_frames;
    std::string output_img_folder;
    std::string frame_number_str;
    std::string thread_number_str;
    parser.HasArgument("-c", &config_file);
    parser.HasArgument("-f", &video_folder);
    parser.HasArgument("-n", &frame_number_str);
    parser.HasArgument("-threads", &thread_number_str);
    int n_frame = std::stoi(frame_number_str);
    int n_thread = std::stoi(thread_number_str);

    printf("\nStarting test with %d threads and %d frames...\n\n",n_thread, n_frame);

    Timer timer;
    double init_time, compute_time;

    timer.Start(); //---------------------------------------------- Start Timer

    int fifo_size = 10;
    LTS5::VideoDataLoader* vdl = new LTS5::VideoDataLoader(n_thread, fifo_size);
    err = vdl->Init(video_folder);
    if (err) {
      std::cout << "Error occured while init VideoDataLoader" << std::endl;
      exit(1);
    }

    // Creation of the worker
    LTS5::FaceProcessingUnit* the_worker;
    the_worker = new LTS5::FaceProcessingUnit(n_thread, config_file);
    the_worker->set_loader(vdl);

    // Get time to create the worker
    init_time = timer.Elapsed();  // ------------------------------- Initialisation Time

    // Start data acquisition BEFORE starting processing
    vdl->Start(n_frame);
    the_worker->Process();

    the_worker->WaitProcessEnded();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    LTS5::ResultData* rd = nullptr;
    int count = 0;
    while( ((rd = the_worker->GetNextFrame()) != NULL) || the_worker->is_processing() ) {
      if(rd != NULL) {
        LTS5::TimeStamp<> t = rd->get_timestamp();
        std::string s;
        t.ConvertToString(&s);
        std::cout << "Timestamp : " << s;
        auto tmp = rd->get_exp_score();
        if (!tmp.empty()) {
          std::cout << tmp[4] << std::endl;
        }
        cv::Mat frame = rd->get_front_image();
        // please add anything after -s (like ON, 1, ...) so that it works
        if(parser.HasArgument("-s", &show_frames)) {
          if(!show_frames.empty()) {
            // Display reconstruction
            cv::imshow("Frontal", frame);
            cv::waitKey(0);
          }
        }
        // please add trailing / to path, code doesn't check for this for now
        if(parser.HasArgument("-save", &output_img_folder)) {
          if(!output_img_folder.empty()) {
            output_img_folder += std::to_string(count++);
            output_img_folder += "_frame.jpg";
            std::cout << "Saving frame to " << output_img_folder << std::endl;
            cv::imwrite(output_img_folder, frame);
          }
        }
        // Clean up
        delete rd;
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    // Get time to send all data to workers and launch computation
    compute_time = timer.Elapsed() - init_time;  // -------------------- Computing Time
    timer.Stop();

    printf("Times (init and computing) = %.3g, %.3g sec\n\n", init_time,  compute_time);
    printf("...End of test\n\n");

    if(parser.HasArgument("-csv", &csv_file))
    {
      if(!csv_file.empty())
      {
        std::cout << "Saving to CSV file..." << std::endl;
        std::ofstream file;
        file.open(csv_file, std::fstream::app | std::fstream::out);
        file << init_time << "," << compute_time << "\n";
        file.close();
      }
    }
  } else {
    std::cout << "Unable to parse command line" << std::endl;
  }
  return err;

}
