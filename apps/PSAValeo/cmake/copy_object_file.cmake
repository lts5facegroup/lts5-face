# - copy_object_file
# Copy object file for a given TARGET into a given DEST
# The following parameters need to be set
#   _TARGET_NAME         - Name of the target
#   _OUTPUT_DESTINATION  - Where to copy object file(s)
#

MESSAGE("Run script")

# Define the location of the object file
SET(OFILE_OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${_TARGET_NAME}.dir/src)
# Get all object file
FILE(GLOB OFILE_LIST "${OFILE_OUTPUT_DIR}/*.o")
# Copy to destination
FILE(MAKE_DIRECTORY ${_OUTPUT_DESTINATION}/ofile)
FILE(COPY ${OFILE_LIST} DESTINATION ${_OUTPUT_DESTINATION}/ofile/.)