# - Find Pylon
# Find the native Pylon includes and library
#
#   PYLON_FOUND       - True if Pylon found.
#   PYLON_INCLUDE_DIR - where to find PylonIncludes.h, etc.
#   PYLON_LIBRARIES   - List of libraries when using FlycAPTure.
#
#	For windows set ENV{PYLON_SDK}

IF( PYLON_INCLUDE_DIR )
    # Already in cache, be silent
    SET( PYLON_FIND_QUIETLY TRUE)
ENDIF(PYLON_INCLUDE_DIR)
# ---[ Header search locations
SET(PYLON_HEADER_SEARCH_PATHS
        /usr/include
        /usr/local/include
        /opt/pylon5
        /opt/pylon5/include
        ENV{PYLON_SDK}
        ENV{PYLON_SDK}/include)
# ---[ Library search locations
SET(PYLON_LIBRARY_SEARCH_PATHS
        /usr/lib
        /usr/local/lib
        /opt/pylon5
        /opt/pylon5/lib64
        ENV{PYLON_SDK}
        ENV{PYLON_SDK}/lib)

# ---[ Header folder path
FIND_PATH(PYLON_INCLUDE_DIR "pylon/PylonIncludes.h"
          HINTS ${PYLON_HEADER_SEARCH_PATHS})

# ---[ Pylon libraries
# PylonGC
FIND_LIBRARY(PYLON_GC
        NAMES GCBase_gcc_v3_0_Basler_pylon_v5_0
        HINTS ${PYLON_LIBRARY_SEARCH_PATHS})
# PylonBase
FIND_LIBRARY(PYLON_BASE
            NAMES pylonbase
            HINTS ${PYLON_LIBRARY_SEARCH_PATHS})
# PylonUtility
FIND_LIBRARY(PYLON_UTILITY
        NAMES pylonutility
        HINTS ${PYLON_LIBRARY_SEARCH_PATHS})

# handle the QUIETLY and REQUIRED arguments and set PYLON_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE( "FindPackageHandleStandardArgs" )
FIND_PACKAGE_HANDLE_STANDARD_ARGS( "Pylon" DEFAULT_MSG PYLON_INCLUDE_DIR PYLON_GC PYLON_BASE PYLON_UTILITY)
IF(PYLON_FOUND)
    SET(PYLON_LIBRARIES ${PYLON_GC} ${PYLON_BASE} ${PYLON_UTILITY})
ENDIF(PYLON_FOUND)
MARK_AS_ADVANCED(PYLON_INCLUDE_DIR PYLON_LIBRARIES)