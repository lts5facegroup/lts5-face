# coding=utf-8
import argparse
import colorlog
import logging
import os
import shlex
import subprocess
import re
__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
# Define Logger
# See : https://github.com/borntyping/python-colorlog
formatter = colorlog.ColoredFormatter(
    "%(log_color)s%(levelname)-8s%(reset)s | %(message)s",
    datefmt=None,
    reset=True,
    log_colors={'DEBUG': 'white',
                'INFO': 'green',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red'},
    secondary_log_colors={},
    style='%'
)
handler = colorlog.StreamHandler()
handler.setFormatter(formatter)
script_name = os.path.basename(__file__).strip().split('.')[0]
logger = colorlog.getLogger(script_name + 'Logger')
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

exp_lut = {'Undef': 0x00,
           'Anger': 0x01,
           'Disgust': 0x02,
           'Fear': 0x04,
           'Happy': 0x08,
           'Neutral': 0x10,
           'Sadness': 0x20,
           'Surprise': 0x40}

exp_map = {0x00: 'Undef',
           0x01: 'Anger',
           0x02: 'Disgust',
           0x04: 'Fear',
           0x08: 'Happy',
           0x10: 'Neutral',
           0x20: 'Sadness',
           0x40: 'Surprise'}


# ----------------------------------------------------------------------------
class Label:
    """
    Represent ground truth label
    """
    def __init__(self, data, separator):
        parts = data.strip().split(separator)
        self.folder = parts[0]
        self.expression = exp_lut[parts[1]]
        self.valence = int(parts[2])
        self.intensity = int(parts[3])
        self.uncertainty = int(parts[4])

    def __str__(self):
        msg = '%s, exp=%s, valence=%d, intensity=%d, uncertainty=%d' % \
              (self.folder,
               exp_map[self.expression],
               self.valence,
               self.intensity,
               self.uncertainty)
        return msg


class HeadposeDetectionLauncher:

    def __init__(self, arguments):
        """
        Constructor
        :param arguments:   Command line arguments 
        """
        # Label file
        self._labels = args.label
        # Generate detection
        self._detection = []
        # Data folder location
        self._input = args.data if args.data[-1] == '/' else args.data + '/'
        # subfolder
        self._subfolder = arguments.subfolder
        if self._subfolder is not None:
            self._subfolder = self._subfolder if self._subfolder[-1] == '/' \
                else self._subfolder + '/'
        # Working directory for reconstruction
        self._work_dir = (args.word_dir if args.word_dir[-1] == '/'
                          else args.word_dir + '/')
        # Reconstruction executable
        self._exec = args.executable
        # Destination
        self._out = args.output if args.output[-1] == '/' else args.output + '/'
        # model
        parts = arguments.models.strip().split(';')
        tracker = self._work_dir + parts[0]
        fdetect = self._work_dir + parts[1]
        pose = self._work_dir + parts[2]
        # Command line
        self._cmd_line = '%s -i %s -t %s -f %s -p %s -o %s -th -1.5' % \
                         (self._exec, '<in>', tracker, fdetect, pose,
                          '<out>')

    def run(self):
        """
        Start expression detection
        """
        # Open
        with open(self._labels, 'r') as f:
            # Read header
            headers = f.readline().strip()
            # Loop
            for line in f:
                # Create label
                label = Label(line, ';')
                logger.info('Process : ' + str(label))
                # folder to process
                input_folder = self._input + label.folder
                # output
                output_folder = self._out + label.folder
                # Need to add subfolder ?
                if self._subfolder is not None:
                    input_folder += self._subfolder
                    output_folder += self._subfolder
                # Check if exist
                if not os.path.exists(output_folder):
                    os.makedirs(output_folder)
                output_folder += 'headpose.csv'
                self._detection.append(output_folder)
                # Finish command line
                srch = r'<in>'
                repl = r'%s' % input_folder
                cmd = re.sub(srch, repl, self._cmd_line)
                srch = r'<out>'
                repl = r'%s' % output_folder
                cmd = re.sub(srch, repl, cmd)
                # execute
                cmd_exec = shlex.split(cmd)
                p = subprocess.Popen(cmd_exec,
                                     cwd=self._work_dir,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                cmd_out, cmd_err = p.communicate()
                if p.returncode != 0:
                    logger.error('Headpose detectior fail to run correctly : ' +
                                 str(p.returncode))
                    logger.debug('stdout : ' + cmd_out)
                    logger.debug('stderr : ' + cmd_err)
                # Clear p
                del p

    def fuse_detection(self):
        """
        Fuse all headpose detection into a single file
        """
        out_filename = self._out
        if self._subfolder is not None:
            out_filename += self._subfolder.replace('/','_') + 'headpose.csv'
        else:
            out_filename += 'headpose.csv'
        logger.info('Fuse all detection into : ' + out_filename)
        with open(out_filename, 'w') as res:
            for i, dfile in enumerate(self._detection):
                with open(dfile, 'r') as f:
                    header = f.readline()
                    if i == 0:
                        res.write(header)
                    # Copy content of f into results
                    res.write(f.read())

# ----------------------------------------------------------------------------
if __name__ == '__main__':
    # Parser
    parser = argparse.ArgumentParser(description='Automatic Head Pose '
                                                 'detector launcher')
    # Label file
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='label',
                        help='File storing ground truth labels')
    # Data folder
    parser.add_argument('-d',
                        type=str,
                        required=True,
                        dest='data',
                        help='Root folder where image data are stored')
    # Extra folder
    parser.add_argument('-s',
                        type=str,
                        required=False,
                        dest='subfolder',
                        help='Subfolder to add at the end')
    # Working directory
    parser.add_argument('-w',
                        type=str,
                        required=True,
                        dest='word_dir',
                        help='Working directory  for C++ executable')
    parser.add_argument('-e',
                        type=str,
                        required=True,
                        dest='executable',
                        help='C++ executable name')
    parser.add_argument('-m',
                        type=str,
                        required=True,
                        dest='models',
                        help='Detector models : "Tracker;FDetect;Pose"')
    # Output
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='output',
                        help='Location where to place the detection')
    # Parse
    args = parser.parse_args()
    # Launcher
    launcher = HeadposeDetectionLauncher(arguments=args)
    # Run
    launcher.run()
    # Finalize
    launcher.fuse_detection()
