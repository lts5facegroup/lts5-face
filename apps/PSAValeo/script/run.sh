#!/bin/bash

# C++ 
# Trakcer models
TRACKER_MODEL="models/face-model-v1.bin;models/haarcascade_frontalface_alt2.xml;models/expression_model.bin"
# Working directory
WDIR="/home/christophe/Documents/LTS5/Data/"
# Reconstruction exec
EXEC="../build_cmd/bin/lts5_face_reconstruction_pipeline"
# Xml template
XML_TEMPLATE="3DRec/template.xml"
# Camera to use
CAMS="1;2"
# Image sequence input
IMAGE_INTPUT="/media/christophe/MyBook/SpontaneousExpressions/PSAValeoImage/"
# Output
IMAGE_OUTPUT="/media/christophe/MyBook/SpontaneousExpressions/PSAValeoImageFrontal/"

echo "Run 3D Reconstruction"
python LaunchReconstruction.py -i $IMAGE_INTPUT -w $WDIR -e $EXEC -t $XML_TEMPLATE -c "$CAMS" -o $IMAGE_OUTPUT

echo "Done"
