# coding=utf-8
import argparse
import colorlog
import logging
import os
import random
import shutil
__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
# Define Logger
# See : https://github.com/borntyping/python-colorlog
formatter = colorlog.ColoredFormatter(
    "%(log_color)s%(levelname)-8s%(reset)s | %(message)s",
    datefmt=None,
    reset=True,
    log_colors={'DEBUG': 'white',
                'INFO': 'green',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red'},
    secondary_log_colors={},
    style='%'
)
handler = colorlog.StreamHandler()
handler.setFormatter(formatter)
script_name = os.path.basename(__file__).strip().split('.')[0]
script_folder = os.path.split(__file__)[0] + '/'
logger = colorlog.getLogger(script_name + 'Logger')
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

exp_lut = {1: 'Anger',
           2: 'Disgust',
           4: 'Fear',
           8: 'Happy',
           16: ' Neutral',
           32: ' Sad',
           64: ' Surprise'}

class Sample:
    r"""
    Classification results
    """

    def __init__(self, line, sep):
        parts = line.strip().split(sep)
        if 'NaN' not in parts:
            self.true = int(parts[0])
            self.pred = int(parts[1])
            self.path = parts[2]
        else:
            raise ValueError('Corrupted label entry')


def load_result(filename):
    lbl_map = dict()
    samples = []
    with open(filename, 'r') as f:
        # read label
        nlabel = int(f.readline().strip())
        for i in range(0, nlabel):
            parts = f.readline().strip().split(' ')
            key = int(parts[0])
            value = parts[1]
            lbl_map[key] = value
        # read entries
        for line in f:
            try:
                s = Sample(line.strip(), ' ')
                samples.append(s)
            except ValueError:
                logger.warning('Drop sample : %s' % line.strip())
    samples.sort(key=lambda x: x.path)
    return lbl_map, samples


def pick_sample(samples, exp, type, number, output):
    r"""
    Randomly select some miss-classified samples
    :param samples:     List of classified samples
    :param exp:         Expression index
    :param type:        0 false positive, 1 false negative
    :param number:      Number of examples to pick
    :param output:      Location where to place the selection
    :return:
    """
    selection = []
    typename = 'sample_'
    if type == 0:
        typename += 'false_pos'
        for sample in samples:
            if sample.true != exp and sample.pred == exp:
                selection.append(sample)
    else:
        typename += 'false_neg'
        for sample in samples:
            if sample.true == exp and sample.pred != exp:
                selection.append(sample)
    # Selection
    nsample = number if len(selection) > number else len(selection)
    perm = random.sample(range(0, len(selection)), nsample)
    # Clear output
    if os.path.exists(output + typename):
        shutil.rmtree(output + typename + '/')
    os.makedirs(output + typename)
    reports = []
    for p in perm:
        img_name = selection[p].path
        filename = os.path.basename(img_name)
        # Save for reporting
        truestr = exp_lut[selection[p].true]
        predstr = exp_lut[selection[p].pred]
        reports.append((filename, truestr, predstr))
        # copy
        logger.info('Copy %s' % img_name)
        shutil.copy2(img_name, output + typename + '/' + filename)

    # Reporting
    reports.sort(key=lambda x: x[0])
    with open(output + typename + '.txt', 'w') as f:
        f.write('ImgName;True;Pred\n')
        for r in reports:
            msg = '%s;%s;%s\n' % (r[0], r[1], r[2])
            f.write(msg)


if __name__ == '__main__':
    # Parser
    parser = argparse.ArgumentParser(description='Search for miss-classified '
                                                 'samples')
    # Classification results (benchmark)
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='input',
                        help='Classification test results')
    # Label to sick
    parser.add_argument('-l',
                        type=str,
                        required=True,
                        dest='label',
                        help='Label of interest')
    # Location where to export images
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='output',
                        help='Location where to place the samples')
    args = parser.parse_args()

    # Load data
    labels, samples = load_result(args.input)
    # Sanity check
    if args.label in labels.values():
        # Pick samples
        exp_idx = labels.keys()[labels.values().index(args.label)]
        pick_sample(samples,
                    exp_idx,
                    0,
                    40,
                    args.output)
        pick_sample(samples,
                    exp_idx,
                    1,
                    40,
                    args.output)
    else:
        logger.error('Unknown label : %s' % args.label)
        lbl = sorted(labels.values())
        logger.info('Supported labels are : %s' % ', '.join(lbl))
