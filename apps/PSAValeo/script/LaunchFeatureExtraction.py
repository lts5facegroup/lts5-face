# coding=utf-8
import argparse
import colorlog
import logging
import os
import shlex
import subprocess
import re
__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
# Define Logger
# See : https://github.com/borntyping/python-colorlog
formatter = colorlog.ColoredFormatter(
    "%(log_color)s%(levelname)-8s%(reset)s | %(message)s",
    datefmt=None,
    reset=True,
    log_colors={'DEBUG': 'white',
                'INFO': 'green',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red'},
    secondary_log_colors={},
    style='%'
)
handler = colorlog.StreamHandler()
handler.setFormatter(formatter)
script_name = os.path.basename(__file__).strip().split('.')[0]
logger = colorlog.getLogger(script_name + 'Logger')
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


# ----------------------------------------------------------------------------
# Automatic launcher
class FeatureExtractionLauncher:
    """
    Automatically generate all reconstruction of the PSA/Valeo database
    """
    def __init__(self, args):
        """
        Constructor
            
        :param args:    Structure with the parameters needed by the launcher
        """
        # Data folder location
        self._input = args.root if args.root[-1] == '/' else args.root + '/'
        # Working directory for reconstruction
        self._work_dir = (args.word_dir if args.word_dir[-1] == '/'
                          else args.word_dir + '/')
        # Extractor executable
        self._exec = args.executable
        # Exec parameters
        p_parts = args.exec_params.strip().split(';')
        self._params = ' -i <IN> -t "%s;%s" -e %s -thr -3.5 -o <OUT>' % (
            p_parts[0],
            p_parts[1],
            p_parts[2])
        # Camera index
        self._cams = map(int,args.cam.strip().split(';'))
        # Destination
        self._out = args.output if args.output[-1] == '/' else args.output + '/'

    def _gather_file(self, filename='label.csv', sep=';'):
        """
        Read all entry in the label file
            
        :param filename:    Name if the file where labels are stored
        :param sep:         Label separator
        :return:            List of folder to process, or an empty list in 
        case of something went wrong.
        """
        # Open labels
        folders = []
        with open(self._input + filename, 'r') as f:
            # Skip headers
            f.readline()
            for line in f:
                folders.append(line.strip().split(sep)[0])
        return folders

    def run(self):
        # gather folders
        folders = self._gather_file()
        if folders:
            # Run extraction for every entry in the label files
            for folder in folders:
                # Loop over all cams
                for cam in self._cams:
                    cam_str = 'C%02d/' % cam
                    # User info
                    logger.info('Process folder : ' + folder + cam_str)
                    # check output exists
                    output = self._out + folder + cam_str
                    if not os.path.exists(output):
                        os.makedirs(output)
                    # run extraction only if folder not empty
                    if len(os.listdir(output)) == 0:
                        # Update parameters
                        param = self._params.replace('<IN>',
                                                     self._input + folder +
                                                     cam_str)
                        param = param.replace('<OUT>', output)
                        # Cmd line
                        cmd = self._exec + param
                        cmd_exec = shlex.split(cmd)
                        s = subprocess.Popen(cmd_exec,
                                             cwd=self._work_dir,
                                             stdout=subprocess.PIPE,
                                             stderr=subprocess.PIPE)
                        cmd_out, cmd_err = s.communicate()
                        if s.returncode != 0:
                            logger.error('Extraction fail to run correctly '
                                         + str(s.returncode))
                            logger.error('stdout : ' + cmd_out)
                            logger.error('stderr : ' + cmd_err)
                        # Clear
                        del s
                    else:
                        logger.info('Skip extraction, already done !')
        else:
            logger.error('No labels founded !')


if __name__ == '__main__':
    # Parser
    parser = argparse.ArgumentParser(description='Automatic feature '
                                                 'extraction launcher launcher')
    # Input folder
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='root',
                        help='Input folder where labels are stored')
    # Working directory
    parser.add_argument('-w',
                        type=str,
                        required=True,
                        dest='word_dir',
                        help='Working directory  for C++ executable')
    parser.add_argument('-e',
                        type=str,
                        required=True,
                        dest='executable',
                        help='C++ executable name')
    parser.add_argument('-p',
                        type=str,
                        required=True,
                        dest='exec_params',
                        help='C++ executable parameters '
                             '"tracker;fdetector;expression"')
    # Camera
    parser.add_argument('-c',
                        type=str,
                        required=True,
                        dest='cam',
                        help='Camera index to use, format : "i;j;k"')
    # Output
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='output',
                        help='Location where to place the reconstrction')
    p = parser.parse_args()
    # Launch every reconstruction
    launcher = FeatureExtractionLauncher(args=p)
    # Run
    launcher.run()
