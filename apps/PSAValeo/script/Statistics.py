# coding=utf-8
import argparse
import pandas
import matplotlib.pyplot as plt
import numpy as np
import os
__author__ = 'Christophe Ecabert'


def parse_cmd_line():
    """s
    Parse command line and return queried parameters
    :return:    Script's arguments
    """
    # Create parser
    parser = argparse.ArgumentParser()
    # Label file
    parser.add_argument('-f',
                        type=str,
                        required=True,
                        dest='filename',
                        help='Path to label file (*.csv)')
    # Location where to output statistics
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='output_folder',
                        help='Folder where to save generated plots')
    # Parse
    return parser.parse_args()


def load_label(argument, separator=';'):
    """
    Load labels into panda's Data format
    :param argument:    Argument from command line
    :param separator:   Label's separator, default=';'
    :return: Labels
    """
    print 'Load data from : ' + argument.filename
    with open(argument.filename, 'r') as f:
        # Read header
        header = f.readline().strip().split(separator)
        # Read labels
        d = pandas.read_csv(f, sep=separator, names=header, usecols=header)
        d.sort_values(by=header[0], inplace=True)
        return d


def autolabel(axis, rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        axis.text(rect.get_x() + rect.get_width()/2., 0.1 + height,
                '%d' % int(height), fontsize='smaller',
                ha='center', va='bottom')


def generate_statistics(argument, dataset):
    # Define output location
    folder = argument.output_folder if argument.output_folder[-1] == '/' else \
        argument.output_folder + '/'
    if not os.path.exists(folder):
        os.makedirs(folder)
    # Expression distribution
    # ------------------------------------------------------------
    exp = list(dataset['Expression'].unique())
    exp.sort()
    dist_exp = []
    dist_frame = []
    n_frame = dataset['#frame'].sum()
    n_frame -= dataset['#Speak'].sum()
    n_frame -= dataset['#Sun'].sum()
    n_frame -= dataset['#Fatigue'].sum()
    for e in exp:
        idx = dataset['Expression'] == e
        sample = dataset[idx]
        n = len(sample)
        nf = sample['#frame'].sum()
        nf -= sample['#Speak'].sum()
        nf -= sample['#Sun'].sum()
        nf -= sample['#Fatigue'].sum()
        v = (float(n) * 100.0) / float(len(idx))
        v = round(v, 2)
        dist_exp.append(v)
        v = (float(nf) * 100.0) / float(n_frame)
        v = round(v, 2)
        dist_frame.append(v)
    # Chart
    fig = plt.figure(1)
    ind = np.arange(len(exp))
    width = 0.4
    plt.bar(left=ind, height=dist_exp, width=width, color='steelblue')
    plt.bar(left=ind+width, height=dist_frame, width=width,
           color='darkseagreen')
    ax = plt.gca()
    # Set title
    ax.set_title('Expression distribution in recordings')
    # Set x label
    ax.set_xticks(ind + width / 2)
    ax.set_xticklabels(exp)
    # Set y label
    ax.set_ylabel('Percentage of dataset')
    ax.set_xlabel('Expression')
    # Grid
    ax.set_axisbelow(True)
    ax.grid(b=True, which='major', color='lightsteelblue', linestyle='--')
    # Legend
    ax.legend(['#Occurence', '#Frame'])
    # Save
    plt.savefig(folder + 'expression_distribution.eps')
    plt.savefig(folder + 'expression_distribution.png')
    # Intensity/Uncertainty distribution
    # ------------------------------------------------------------
    ind = np.arange(-2, 3, 1)
    width = 0.1
    color = ['lightcoral', 'burlywood', 'darkgray', 'darkseagreen', 'steelblue',
             'mediumslateblue', 'thistle']
    for k, e in enumerate(exp):
        sample = dataset[dataset['Expression'] == e]
        # Intensity
        count = sample['Intensity'].value_counts()
        n_count = count.sum()
        idx = count.index.tolist()
        dist_intensity = []
        for i in ind:
            if i in idx:
                v = float(count.get(i)) * 100.0 / float(n_count)
                v = round(v, 2)
                dist_intensity.append(v)
            else:
                dist_intensity.append(0.0)

        # Uncertainty
        count = sample['Uncertainty'].value_counts()
        n_count = count.sum()
        idx = count.index.tolist()
        dist_uncertainty = []
        for i in ind:
            if i in idx:
                v = float(count.get(i)) * 100.0 / float(n_count)
                v = round(v, 2)
                dist_uncertainty.append(v)
            else:
                dist_uncertainty.append(0.0)
        # plot - intensity
        plt.figure(2)
        ax = plt.gca()
        r1 = ax.bar(left=ind + (k * width),
                    height=dist_intensity,
                    width=width,
                    color=color[k])
        # Add value
        autolabel(axis=ax, rects=r1)
        # plot - uncertainty
        plt.figure(3)
        ax = plt.gca()
        r1 = ax.bar(left=ind + (k * width),
                    height=dist_uncertainty,
                    width=width,
                    color=color[k])
        # Add value
        autolabel(axis=ax, rects=r1)

    plt.figure(2)
    ax = plt.gca()
    # Set title
    ax.set_title('Intensity distribution')
    # Set x label
    ax.set_xticks(ind + ((len(exp)-1) * width / 2))
    ax.set_xticklabels(ind)
    # Set y label
    ax.set_ylabel('Percentage of segments')
    ax.set_xlabel('Level of intensity')
    # Grid
    ax.set_axisbelow(True)
    ax.grid(b=True, which='major', color='lightsteelblue', linestyle='--')
    # Legend
    ax.legend(exp)
    # Save
    plt.savefig(folder + 'intensity_distribution.eps')
    plt.savefig(folder + 'intensity_distribution.png')

    # Set title
    plt.figure(3)
    ax = plt.gca()
    ax.set_title('Level of certitude distribution')
    # Set x label
    ax.set_xticks(ind + ((len(exp)-1) * width / 2))
    ax.set_xticklabels(ind)
    # Set y label
    ax.set_ylabel('Percentage of segments')
    ax.set_xlabel('Level of certitude')
    # Grid
    ax.set_axisbelow(True)
    ax.grid(b=True, which='major', color='lightsteelblue', linestyle='--')
    # Legend
    ax.legend(exp)
    # Save
    plt.savefig(folder + 'uncertainty_distribution.eps')
    plt.savefig(folder + 'uncertainty_distribution.png')


if __name__ == '__main__':
    # Parser
    args = parse_cmd_line()
    # Load data into panda's data format
    data = load_label(args)
    # Generate statistics
    generate_statistics(argument=args, dataset=data)


