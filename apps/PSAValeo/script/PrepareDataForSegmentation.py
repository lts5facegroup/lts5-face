# -*- coding: utf-8 -*-
import argparse
import os, shutil
__author__ = 'Christophe Ecabert'


def scan_folder(folder, expr):
    """
        List all files in a given folder with specific expression
    :param folder:      Folder to scan
    :param expr:        List of expression to look for
    :return:            List of folder
    """
    picked_file = []
    for root, dirs, files in os.walk(folder):
        # Loop over files
        p = len(folder) + 1 if folder[-1] != '/' else len(folder)
        for f in files:
            # has expr in it ?
            for e in expr:
                if e in f:
                    filename = root + '/' + f
                    filename = filename[p:]
                    picked_file.append(filename)
    picked_file.sort()
    return picked_file


def copy_file_into_structure(export_folder, root_input, list_files):
    # Loop over all
    out = export_folder if export_folder[-1] == '/' else export_folder + '/'
    input = root_input if root_input[-1] == '/' else root_input + '/'
    for f in list_files:
        # define in/out filename
        part = f.split('/')
        in_filename = input + f
        if 'PC3/' in f:
            out_filename = out + part[0] + '/' + part[2]
        else:
            out_filename = out + f
        # part[0] == subject folder, check if exist
        if not os.path.exists(out + part[0]):
            os.makedirs(out + part[0])
        # Copy file
        if not os.path.exists(out_filename):
            print 'Copy : ' + in_filename
            shutil.copy2(in_filename, out_filename)
        else:
            print '\tSkip : ' + in_filename

if __name__ == '__main__':
    # Cmd line parser
    parser = argparse.ArgumentParser(description='Prepare dataset for '
                                                 'segmentation')
    # Root folder
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='root',
                        help='Root folder')
    # Export
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='export',
                        help='Export folder location')
    # Parse
    args = parser.parse_args()
    # Recover files needed
    expr_needed = ['metadata',
                   'camLeft',
                   'camCenter',
                   'camRight',
                   'oFrameInfo',
                   'offsets']
    list_file = scan_folder(args.root, expr=expr_needed)
    # Copy stuff
    copy_file_into_structure(args.export, args.root, list_file)



