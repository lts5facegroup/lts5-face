# coding=utf-8
import argparse
import math
import colorlog
import logging
import os
import re
import random
__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
# Define Logger
# See : https://github.com/borntyping/python-colorlog
formatter = colorlog.ColoredFormatter(
    "%(log_color)s%(levelname)-8s%(reset)s | %(message)s",
    datefmt=None,
    reset=True,
    log_colors={'DEBUG': 'white',
                'INFO': 'green',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red'},
    secondary_log_colors={},
    style='%'
)
handler = colorlog.StreamHandler()
handler.setFormatter(formatter)
script_name = os.path.basename(__file__).strip().split('.')[0]
logger = colorlog.getLogger(script_name + 'Logger')
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

exp_lut = {'Undef': 0x00,
           'Anger': 0x01,
           'Disgust': 0x02,
           'Fear': 0x04,
           'Happy': 0x08,
           'Neutral': 0x10,
           'Sadness': 0x20,
           'Surprise': 0x40}

exp_str = {0x00: 'Undef',
           0x01: 'Anger',
           0x02: 'Disgust',
           0x04: 'Fear',
           0x08: 'Happy',
           0x10: 'Neutral',
           0x20: 'Sadness',
           0x40: 'Surprise'}


def unique(seq, key=None):
    r"""
    Return a list of unique elements contained in a given sequence. Optional 
    selector function can be provided
    :param seq: List to scan for unique elements
    :param key: Selector function
    :return: Unique items
    """
    # Define selector if none is provided
    if key is None:
        def key(x):
            return x
    # Get unique items
    seen = {}
    result = []
    for item in seq:
        i = key(item)
        if i in seen:
            continue
        seen[i] = 1
        result.append(item)
    return result


def load_pose(path, sep, thresh):
    r"""
    Load data from a given pose files. 
    :param path:    File holding head pose
    :param sep:     Separator (csv)
    :param thresh:  Head pose treshold in Yaw
    :return: List of samples matching criterion
    """
    paths = []
    with open(path, 'r') as f:
        headers = f.readline()
        for line in f:
            parts = line.strip().split(sep)
            # Pose angle
            pose = float(parts[2])
            if abs(pose) < thresh:
                # Ground truth
                paths.append(parts[0])
    paths.sort()
    return paths


def print_info(set, name, idx):
    logger.info('%d - Statistics for %s : ' % (idx, name))
    for key, value in set.iteritems():
        msg = '\t#Sample in %s : %d' % (exp_str[key], len(value))
        logger.info(msg)


def generate_sets(paths, nsample):
    r"""
    Generate datasets for training testing
    :param paths:   List of samples
    :param nsample: Number of maximum of samples for training
    :return: Train and test sets
    """
    # Generate ground truth
    gt = []
    ids = set()
    for p in paths:
        # Extract labels
        m = re.search(r'S([0-9]*)/([^0-9]*)/T[0-9]]*', p).groups()
        id = int(m[0])
        label = m[1]
        label = exp_lut[label]
        gt.append((id, label, p))
        # Create list of unique id
        if id not in ids:
            ids.add(id)
    # Train, test
    trainset = dict()
    testset = dict()
    cnt = 0
    while len(trainset) != 7 or len(testset) != 7:
        # Reset
        if len(trainset) > 0:
            trainset = dict()
        if len(testset) > 0:
            testset = dict()
        # Split train/test id
        select_id = random.sample(range(0, len(ids)), len(ids) / 2)
        for item in gt:
            # Selected for train ?
            if item[0] in select_id:
                # Add
                if trainset.get(item[1]) is None:
                    trainset[item[1]] = [(item[0], item[2])]
                else:
                    trainset[item[1]].append((item[0], item[2]))
            else:
                # Add ot test
                if testset.get(item[1]) is None:
                    testset[item[1]] = [(item[0], item[2])]
                else:
                    testset[item[1]].append((item[0], item[2]))
        cnt += 1
        if cnt > 20:
            logger.error('Can not generate train/test set with proper amount '
                         'of samples')
            trainset = None
            testset = None
            return trainset, testset
    sets = [trainset, testset]
    for d in sets:
        for key, value in d.iteritems():
            if len(value) > nsample:
                # Sample
                select = random.sample(range(0, len(value)), nsample)
                items = [value[x] for x in select]
                d[key] = items
    print_info(trainset, 'Train', cnt)
    print_info(testset, 'Test', cnt)
    return trainset, testset


def save_in_file(train, test, folder):
    # Dest
    dst = folder if folder[-1] == '/' else folder + '/'
    if not os.path.exists(dst):
        os.makedirs(dst)
    # Training
    path = dst + 'trainset.txt'
    with open(path, 'w') as f:
        # Write data
        for key, value in train.iteritems():
            value.sort()
            for v in value:
                # Write data
                data = '%d %s %d\n' % (v[0], v[1], key)
                f.write(data)
        f.close()
    # Testing
    path = dst + 'testset.txt'
    with open(path, 'w') as f:
        # Write data
        for key, value in test.iteritems():
            value.sort()
            for v in value:
                # Write data
                data = '%s %d\n' % (v[1], key)
                f.write(data)
        f.close()


if __name__ == '__main__':
    # Parser
    parser = argparse.ArgumentParser(description='Generate training/testing '
                                                 'configuration file for '
                                                 'facial analysis system')
    # Input
    parser.add_argument('-p',
                        type=str,
                        required=True,
                        dest='pose',
                        help='File storing head pose (*.csv)')
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='output',
                        help='Folder where to store config file')
    # Parse
    args = parser.parse_args()
    # Load pose
    paths = load_pose(args.pose, ',', math.radians(15.0))
    # Generate train / test
    trainset, testset = generate_sets(paths, 1000)
    # Dump if data are ok
    if trainset is not None and testset is not None:
        save_in_file(trainset, testset, args.output)

