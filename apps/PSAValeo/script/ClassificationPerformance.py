# coding=utf-8
import argparse
import colorlog
import logging
import os
import re
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
import numpy as np
import matplotlib.pyplot as plt
import itertools
import bisect
__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
# Define Logger
# See : https://github.com/borntyping/python-colorlog
formatter = colorlog.ColoredFormatter(
    "%(log_color)s%(levelname)-8s%(reset)s | %(message)s",
    datefmt=None,
    reset=True,
    log_colors={'DEBUG': 'white',
                'INFO': 'green',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red'},
    secondary_log_colors={},
    style='%'
)
handler = colorlog.StreamHandler()
handler.setFormatter(formatter)
script_name = os.path.basename(__file__).strip().split('.')[0]
logger = colorlog.getLogger(script_name + 'Logger')
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

exp_lut = {'Undef': 0x00,
           'Anger': 0x01,
           'Disgust': 0x02,
           'Fear': 0x04,
           'Happy': 0x08,
           'Neutral': 0x10,
           'Sadness': 0x20,
           'Surprise': 0x40}

exp_map = {0x00: 'Undef',
           0x01: 'Anger',
           0x02: 'Disgust',
           0x04: 'Fear',
           0x08: 'Happy',
           0x10: 'Neutral',
           0x20: 'Sadness',
           0x40: 'Surprise'}


# ----------------------------------------------------------------------------
class Label:
    """
    Represent ground truth label
    """
    def __init__(self, data, separator):
        parts = data.strip().split(separator)
        self.folder = parts[0]
        self.expression = exp_lut[parts[1]]
        self.valence = int(parts[2])
        self.intensity = int(parts[3])
        self.uncertainty = int(parts[4])
        self.n_frame = int(parts[5])

    def __str__(self):
        msg = '%s, valence=%d, intensity=%d, uncertainty=%d' % \
              (exp_map[self.expression],
               self.valence,
               self.intensity,
               self.uncertainty)
        return msg


class Prediction:

    def __init__(self, data, separator):
        parts = data.strip().split(separator)
        # Find folder
        self.folder = re.search(r'S[0-9]*/[^0-9]*/T[0-9]*/', parts[0]).group()
        # Find file
        m = re.search(r'F[0-9]*(_?[a-z]*)?.jpg', parts[0])
        if m.groups()[0] == '':
            self.file = m.group()
        else:
            self.file = m.group().replace(m.groups()[0], '')
        # Complete path
        self.path = self.folder + self.file
        # Find label
        if len(parts) > 2 and parts[1] != 'nan':
            score = map(float, parts[1:8])
            idx, max_value = max(enumerate(score), key=lambda p: p[1])
            self.exp = 0x01 << idx
        else:
            self.exp = 0


class HeadPose:

    def __init__(self, data, separator):
        parts = data.strip().split(separator)
        # Find folder
        self.folder = re.search(r'S[0-9]*/[^0-9]*/T[0-9]*/', parts[0]).group()
        # Find file
        m = re.search(r'F[0-9]*(_?[a-z]*)?.jpg', parts[0])
        if m.groups()[0] == '':
            self.file = m.group()
        else:
            self.file = m.group().replace(m.groups()[0], '')
        # Complete path
        self.path = self.folder + self.file
        # Head pose
        if parts[1] != 'nan':
            self.pitch = float(parts[1])
            self.yaw = float(parts[2])
            self.roll = float(parts[3])
        else:
            self.pitch = None
            self.yaw = None
            self.roll = None

    def __str__(self):
        msg = 'Pitch : %.3f Yaw : %.3f Roll : %.3f' % (self.pitch,
                                                       self.yaw,
                                                       self.roll)
        return msg


class Filter:
    """
    Filter class to select subset of ground truth annotation
    """
    def __init__(self, thresh):
        """
        Constructor
        :param arguments:    Arguments from command line 
        """
        if thresh is None:
            self.min_intensity = -2
            self.min_uncertainty = -2
        else:
            part = thresh.replace('\\','').split(';')
            self.min_intensity = int(part[0])
            self.min_uncertainty = int(part[1])

    def __le__(self, other):
        if isinstance(other, Label):
            return (self.min_intensity <= other.intensity) and \
                   (self.min_uncertainty <= other.uncertainty)
        else:
            msg = "unsupported operand type(s) for +: '%s' and '%s'" % \
                  (self.__class__, type(other))
            raise TypeError(msg)


class Metric:
    # Confusion matrix
    ConfusionMatrix = 0x01
    # F1 Score
    F1Score = 0x02
    # Coverage
    Coverage = 0x04


class PerfType:
    # Single camera
    Single = 0x01
    # Virtual camera
    Virtual = 0x02
    # By angle
    Angle = 0x03

    def __init__(self, type):
        self.type = 0
        if type == 'Single':
            self.type = PerfType.Single
        elif type == 'Virtual':
            self.type = PerfType.Virtual
        elif type == 'Angle':
            self.type = PerfType.Angle

    def __str__(self):
        if self.type == 1:
            return 'Single'
        elif self.type == 2:
            return 'Virtual'
        elif self.type == 3:
            return 'Angle'
        else:
            return 'Unknown'


def load_ground_truth(path, separator, lfilter=None):
    """
    Load ground truth
    :param path:            Path to label file (*.csv)
    :param separator:       Separator used in file
    :param lfilter:         Filter to apply on expression
    :return:                Number total of frame, list of ground truth label
    """
    # Open file
    labels = []
    nframe = 0
    with open(path, 'r') as f:
        headers = f.readline()
        # Iterate till the end
        for line in f:
            # Create label
            lbl = Label(data=line, separator=separator)
            if lfilter <= lbl:
                labels.append(lbl)
                nframe += lbl.n_frame
            else:
                logger.warning('Label filtered out : ' + str(lbl))
    labels = sorted(labels, key=lambda f: f.folder)
    return nframe, labels


def load_detection(path, separator, labels):
    # Load
    detections = []
    gt_lut = dict()
    for lbl in labels:
        gt_lut[lbl.folder] = lbl.expression
    with open(path, 'r') as f:
        classes = f.readline().strip().split(separator)[1:8]
        for line in f:
            pred = Prediction(data=line, separator=separator)
            if pred.exp != 0:
                i = gt_lut.get(pred.folder)
                if i is not None:
                    detections.append(pred)
    # Sort by path
    detections.sort(key=lambda p: p.path)
    return classes, detections


def load_headpose(path, separator, labels):
    # Load
    poses = []
    gt_lut = dict()
    for lbl in labels:
        gt_lut[lbl.folder] = lbl.expression
    with open(path, 'r') as f:
        header = f.readline()
        for line in f:
            p = HeadPose(data=line, separator=separator)
            if p.yaw is not None:
                i = gt_lut.get(p.folder)
                if i is not None:
                    poses.append(p)
    # Sort by path
    poses.sort(key=lambda p: p.path)
    return poses


def finalize_data(gtruth, single_pred, virt_pred, headp):
    """
    Merge data into common array : Truth, SinglePred, VirtualPred, Pose1, 
    Pose2 Pose3
    
    :param gtruth:      List of true label 
    :param single_pred: List of prediction from single camera
    :param virt_pred:   List of prediction from vitual view
    :param headp:       List of head pose
    :return:    list of files, Harmonized array with label if available (-1 
                otherwise)
    """
    # Init lookup table
    label_lut = dict()
    single_label = dict()
    virtual_label = dict()
    head_pose = dict()
    files = set()
    for gt in gtruth:
        label_lut[gt.folder] = gt.expression
    # Fill
    for s, v, h in itertools.izip_longest(single_pred,
                                          virt_pred,
                                          headp,
                                          fillvalue=None):
        if s is not None:
            if s.path not in files:
                files.add(s.path)
            single_label[s.path] = s.exp
        if v is not None:
            if v.path not in files:
                files.add(v.path)
            virtual_label[v.path] = v.exp
        if h is not None:
            if h.path not in files:
                files.add(h.path)
            head_pose[h.path] = abs(h.yaw)
    files = sorted(files)
    # Populate array
    data = np.empty((len(files), 4), dtype=np.float)
    for i, f in enumerate(files):
        # Extract folder
        folder = f[0:f.rfind('/')+1]
        # Ground truth
        lbl = label_lut.get(folder)
        if lbl is not None:
            data[i, 0] = lbl
            # Single
            s = single_label.get(f)
            data[i, 1] = s if s is not None else -1.0
            # Virtual
            v = virtual_label.get(f)
            data[i, 2] = v if v is not None else -1.0
            # Pose
            p = head_pose.get(f)
            data[i, 3] = p if p is not None else -1.0
        else:
            data[i,:] = -1.0
    return files, data


class DetectionPerformance:

    def __init__(self,arg):
        # Create filter
        exp_filter = Filter(arg.filter)
        self._intensity = exp_filter.min_intensity
        self._uncertainty = exp_filter.min_uncertainty
        # Load ground truth
        logger.debug('Load ground truth')
        self._nframe, labels = load_ground_truth(path=arg.gtruth,
                                                 separator=';',
                                                 lfilter=exp_filter)
        # Load detection
        logger.debug('Load single camera detection')
        self._classes, detections = load_detection(path=arg.detection,
                                                   separator=',',
                                                   labels=labels)
        logger.debug('Load multicamera detection')
        _, vdetections = load_detection(path=arg.v_detection,
                                        separator=',',
                                        labels=labels)
        # Load head pose
        logger.debug('Load head pose')
        hpose = load_headpose(path=arg.pose,
                              separator=',',
                              labels=labels)
        # Generate true/pred array
        logger.debug('Finalize data')
        self._files, self._data = finalize_data(gtruth=labels,
                                                single_pred=detections,
                                                virt_pred=vdetections,
                                                headp=hpose)
        # Define output
        self._out = arg.output if arg.output[-1] == '/' \
            else arg.output + '/'
        if not os.path.exists(self._out):
            os.makedirs(self._out)
        # Define angle split
        self.angles = [0.0, 15.0, 30.0, 90.0]
        self.angles = map(lambda x: x * np.pi / 180.0, self.angles)
        self.angles_deg = [0, 15, 30, 90]
        # Init metrics
        self._f1score = []
        self._cm = []
        self._coverage = []

    def generate_metrics(self,
                         type,
                         metric):
        """
        Generate metrics for a given set of labels (ground truth + prediction)
        :param metric:  Type of performance metric
        """
        # Generate proper data
        ytrue, ypred = self._extract_data(type=type)
        # Compute metrics
        if (metric & Metric.ConfusionMatrix) == Metric.ConfusionMatrix:
            self._compute_confusion_matrix(self._classes,
                                           truth=ytrue,
                                           prediction=ypred,
                                           type=type)
        if (metric & Metric.F1Score) == Metric.F1Score:
            self._compute_f1_score(truth=ytrue,
                                   prediction=ypred,
                                   type=type)
        if (metric & Metric.Coverage) == Metric.Coverage:
            self._compute_coverage(truth=ytrue, prediction=ypred, type=type)

    def _extract_data(self, type):
        ypred = []
        ytrue = []
        if type.type == PerfType.Single:
            idx = self._data[:, 1] > 0
            ypred.append([self._data[idx, 1]])
            ytrue.append([self._data[idx, 0]])
        elif type.type == PerfType.Virtual:
            idx = self._data[:, 2] > 0
            ypred.append([self._data[idx, 2]])
            ytrue.append([self._data[idx, 0]])
        else:
            s = 1
            for i in range(len(self.angles) - 1):
                min_a = self.angles[i]
                max_a = self.angles[i + 1]
                yypred = []
                yytrue = []
                for s in range(1, 3):
                    idx = (self._data[:, s] > 0) & \
                          (min_a <= self._data[:,3]) & \
                          (self._data[:,3] < max_a)
                    yypred.append(self._data[idx, s])
                    yytrue.append(self._data[idx, 0])
                ypred.append(yypred)
                ytrue.append(yytrue)
        return ytrue, ypred

    def _compute_confusion_matrix(self,
                                  classes,
                                  truth,
                                  prediction,
                                  type,
                                  color=plt.cm.Blues):
        # Compute confusion matrix
        self._cm = []
        for k, (ytrue, ypred) in enumerate(itertools.izip(truth, prediction)):
            nview = len(ytrue) if type.type == PerfType.Angle else 1
            fig = plt.figure(figsize=(nview * 6, 5))
            if nview > 1:
                ang = ' [%d, %d]' % (self.angles_deg[k],
                                     self.angles_deg[k + 1])
                plt.suptitle('Confustion Matrix : ' + str(type) + ang)
            cms = []
            cm_max = 0.0
            for m, (yytrue, yypred) in enumerate(itertools.izip(ytrue, ypred)):
                cm = confusion_matrix(yytrue, yypred)
                # Normalize + add to list
                cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
                np.set_printoptions(precision=2)
                cms.append(cm)
                cm_max = max(np.nanmax(cm), cm_max)

            for m, cm in enumerate(cms):
                # Print confusion matrix
                if type.type == PerfType.Angle:
                    title = 'Single' if m == 0 else 'Virtual'
                else:
                    title = 'Confusion Matrix : %s' % (str(type))
                plt.subplot(1, len(ytrue), m + 1)
                plt.imshow(cm,
                           interpolation='nearest',
                           aspect='equal',
                           cmap=color)
                plt.title(title)
                plt.colorbar()
                plt.clim(0.0, cm_max)
                tick_marks = np.arange(len(classes))
                plt.xticks(tick_marks, classes, rotation=45)
                plt.yticks(tick_marks, classes)
                # Draw value onto plot
                thresh = cm_max / 2.
                for i, j in itertools.product(range(cm.shape[0]),
                                              range(cm.shape[1])):
                    plt.text(j,
                             i,
                             round(cm[i, j], 2),
                             horizontalalignment="center",
                             verticalalignment="center",
                             color="white" if round(cm[i, j],2) > thresh else
                             "black")
                plt.tight_layout(rect=[0.03, 0.03, 0.97, 0.97])
                plt.ylabel('True label')
                plt.xlabel('Predicted label')

                # Dump into console
                logger.info(title)
                logger.info('\n' + str(cm))
            self._cm.append(cms)
            # Save
            tp = str(type).lower()
            filename = self._out + tp +'_confusion_matrix_%d_%d_%d' % (
                self._intensity,
                self._uncertainty,
                k)
            #plt.savefig(filename + '.eps')
            plt.savefig(filename + '.png')

    def _compute_f1_score(self, truth, prediction, type):
        self._f1score = []
        for i, (ytrue, ypred) in enumerate(itertools.izip(truth, prediction)):
            f1s = []
            for k, (yytrue, yypred) in enumerate(itertools.izip(ytrue, ypred)):
                f1w = f1_score(yytrue, yypred, average='weighted')
                f1 = f1_score(yytrue, yypred, average=None)
                f1s.append((f1w, f1))
                if type.type != PerfType.Angle:
                    msg = 'Overall F1-score, %s : %.2f' % (str(type), f1s[k][0])
                else:
                    msg = 'Overall F1-score, %s [%d, %d]: %.2f (%s)' % \
                          (str(type),
                           self.angles_deg[i],
                           self.angles_deg[i+1],
                           f1s[k][0],
                           'Single' if k == 0 else 'Virtual')
                logger.info(msg)
            self._f1score.append(f1s)

    def _compute_coverage(self,truth, prediction, type):
        self._coverage = []
        if type.type != PerfType.Angle:

            for i, (ytrue, ypred) in enumerate(itertools.izip(truth,
                                                              prediction)):
                cov = []
                for k, (yytrue, yypred) in enumerate(itertools.izip(ytrue,
                                                                    ypred)):
                    c = float(yypred.shape[0]) / float(self._nframe) * 100.0
                    cov.append(c)
                    msg = 'Coverage, %s : %.2f' % (str(type), c)
                    msg += '%'
                    logger.info(msg)
                self._coverage.append(cov)

    def save(self, type):
        tp = str(type).lower()
        filename = self._out + tp + '_metrics.txt'
        logger.info('Dump metrics into : ' + filename)
        with open(filename, 'w') as f:
            # Filter info
            f.write('*** Metrics ***\n')
            msg = 'Min intensity : %d\n' % self._intensity
            f.write(msg)
            msg = 'Min uncertainty : %d\n' % self._uncertainty
            f.write(msg)
            # Number total of frame analyzed
            msg = 'Total #Frame : %d\n' % self._nframe
            f.write(msg)
            # Coverage if any
            for i, covs in enumerate(self._coverage):
                # Coverage if any
                for k, cov in enumerate(covs):
                    msg = 'Coverage : %.2f\n' % (cov)
                    f.write(msg)

            # F1 if any
            for i, f1s in enumerate(self._f1score):
                for k, f1 in enumerate(f1s):
                    if type.type == PerfType.Angle:
                        msg = 'Overall F1 %s [%d, %d]: %.2f,\t %s\n' % \
                              ('Single' if k == 0 else 'Virtual',
                               self.angles_deg[i],
                               self.angles_deg[i + 1],
                               f1[0],
                               str(f1[1]))
                    else:
                        msg = 'Overall F1 %s : %.2f,\t %s\n' % (str(type),
                                                                f1[0],
                                                                str(f1[1]))
                    f.write(msg)
            # CM if any
            for i, cms in enumerate(self._cm):
                for k, cm in enumerate(cms):
                    if type.type == PerfType.Angle:
                        msg = 'Confusion Matrix : %s [%d, %d]\n' % \
                              ('Single' if k == 0 else 'Virtual',
                               self.angles_deg[i],
                               self.angles_deg[i+1])
                    else:
                        msg = 'Confusion Matrix : %s\n' % (str(type))
                    f.write(msg)
                    f.write(str(cm) + '\n')

if __name__ == '__main__':


    # Parser
    parser = argparse.ArgumentParser(description='Generate confusion matrix '
                                                 'for expression detection')
    # Ground truth
    parser.add_argument('-g',
                        type=str,
                        required=True,
                        dest='gtruth',
                        help='File storing ground truth labels')
    # Single camera - Expression detection
    parser.add_argument('-d',
                        type=str,
                        required=True,
                        dest='detection',
                        help='File storing expression detections (single)')
    # Virtual view - Expression detection
    parser.add_argument('-v',
                        type=str,
                        required=True,
                        dest='v_detection',
                        help='File storing expression detections (virtual)')
    # Head pose file
    # Virtual view - Expression detection
    parser.add_argument('-p',
                        type=str,
                        required=True,
                        dest='pose',
                        help='File storing head pose')
    # Expression filter
    parser.add_argument('-f',
                        type=str,
                        required=False,
                        dest='filter',
                        help='Annotation filter : min level wanted '
                             '\"intensity;certitude\", i.e. \"1;-1\"')
    # Output
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='output',
                        help='Location where to place the metrics')
    args = parser.parse_args()
    # Init
    perf = DetectionPerformance(arg=args)
    # Generate metric
    types = ['Single', 'Virtual', 'Angle']
    for t in types:
        m = Metric.ConfusionMatrix + Metric.F1Score + Metric.Coverage
        tp = PerfType(t)
        perf.generate_metrics(type=tp, metric=m)
        # Save
        perf.save(type=tp)
