# coding=utf-8
import argparse
import os
import numpy as np
import re
__author__ = 'Christophe Ecabert'


def scan_folder(folder, ext):
    """
    Scan a given folder for files with a specific extension in a
    recursive way.

    :param folder:  Root folder to scan
    :param ext:     Extension to filter
    :return:    List of file with the desired extension
    """
    picked_file = []
    if folder[-1] != '/':
        folder += '/'
    for root, dirs, files in os.walk(folder):
        # Loop over files
        for f in files:
            # Correct extension ?
            if f.endswith(ext):
                picked_file.append(root + '/'+ f)
    picked_file.sort()
    return picked_file


def load_detection(filename):
    r"""
    Load classification result from a given file
    :param filename: Path to classification result file
    :return:         Tuple with detection score array and ground truth label
    """
    # Load detection
    detect = np.loadtxt(filename,
                        dtype=np.float64,
                        delimiter=',',
                        usecols=range(1,8),
                        skiprows=1)
    # Get label
    m = re.search(r'S[0-9]*/([a-zA-Z]*)/T', filename)
    return detect, m.groups()[0]


def load_data(folder):
    r"""
    Load all classification results into a list of tuple
    :param folder:  Root folder where results are stored
    :return:        List of tuples with score and ground truth
    """
    samples = []
    # Check folder trailing '/'
    out = folder if folder[-1] == '/' else folder + '/'
    files = scan_folder(out, 'csv')
    # Loop over
    for f in files:
        detect, label = load_detection(f)
        samples.append((label, detect))
    return samples


if __name__ == '__main__':
    # parser
    parser = argparse.ArgumentParser()
    # Root Folder
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='root',
                        help='Root folder')
    args = parser.parse_args()
    # load data
    data = load_data(args.root)

    # Process data
    for d in data:
        score, label = d[0], d[1]
        # Do processing here
        # ....
        print 'Do some magic here ...'