# coding=utf-8
import argparse
import os
import shutil
import errno
__author__ = 'Christophe Ecabert'


def load_label(path, sep, exp_list, min_f, max_f):
    r"""
    Load data from label file. Extract only sample with label matching the 
    one given in exp_list
    :param path:        Path to label file
    :param sep:         File separator
    :param exp_list:    Expression of interest
    :return:            Samples
    """
    with open(path, 'r') as f:
        samples = []
        headers = f.readline().strip().split(sep)
        for line in f:
            parts = line.strip().split(sep)
            # Expression match ?
            if parts[1] in exp_list:
                # Check if segement is complete
                num = map(int, parts[5:])
                s = sum(num[1:])
                if s == 0 and max_f > num[0] > min_f :
                    samples.append((parts[0], num[0], line))

        samples.sort(key=lambda x: x[1])
        return samples, headers


def copy_image(src, dst, ext):
    # Scan fodler
    imgs = [f for f in os.listdir(src) if f.endswith(ext)]
    # Dest exist ?
    if not os.path.exists(dst):
        os.makedirs(dst)
    for img in imgs:
        shutil.copy2(src + img, dst)


def export_data(header, samples, nsample, folder, img_root, detect_root,
                output):
    if not os.path.exists(output):
        os.makedirs(output)
    if not os.path.exists(output + 'in_car_expression/'):
        os.makedirs(output + 'in_car_expression/')
    with open(output + 'in_car_expression/dynamic_label.csv', 'w') as f:
        # Dump head
        f.write(''.join(header))
        # Dump each entries
        for i, sample in enumerate(samples):
            print 'Process sample : ' + sample[0]
            if i < nsample:
                # Dump entry
                f.write(sample[2])
                # Copy detection
                src = detect_root + sample[0] + folder + '/detection.csv'
                dst = (output + 'in_car_expression_detection/' + sample[0] +
                       folder)
                if not os.path.exists(dst):
                    os.makedirs(dst)
                shutil.copy2(src, dst + '/detection.csv')
                # Copy image
                src = img_root + sample[0] + folder + '/'
                dst = output + 'in_car_expression/' + sample[0] + 'C00'
                copy_image(src, dst, 'jpg')


if __name__ == '__main__':
    # parser
    parser = argparse.ArgumentParser()
    # Root
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='root',
                        help='Root folder')
    # Label
    parser.add_argument('-l',
                        type=str,
                        required=True,
                        dest='label',
                        help='File storing labels (*.csv)')
    # output
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='output',
                        help='Export location')
    # Parse
    args = parser.parse_args()

    # Load data
    root = args.root if args.root[-1] == '/' else args.root + '/'
    path = root + args.label
    out = args.output if args.output[-1] == '/' else args.output + '/'

    # Export data, segment of size [1, 10] secs
    exp_list = ['Neutral', 'Happy']
    data, hdr = load_label(path, ';', exp_list, 30, 300)
    export_data(hdr,
                data,
                50,
                'C00',
                root + 'in_car_expression/',
                root + 'in_car_expression_detection/',
                out)


