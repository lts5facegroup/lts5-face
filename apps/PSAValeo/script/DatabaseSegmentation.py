# -*- coding: utf-8 -*-
import argparse
import colorlog
import logging
import unicodedata
import os
import io
import re
import pandas
import itertools
import cv2
import operator
__author__ = 'Christophe Ecabert'

# ----------------------------------------------------------------------------
# Define Logger
# See : https://github.com/borntyping/python-colorlog
formatter = colorlog.ColoredFormatter(
    "%(log_color)s%(levelname)-8s%(reset)s | %(message)s",
    datefmt=None,
    reset=True,
    log_colors={'DEBUG': 'white',
                'INFO': 'green',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red'},
    secondary_log_colors={},
    style='%'
)
handler = colorlog.StreamHandler()
handler.setFormatter(formatter)
logger = colorlog.getLogger('SegmentationLogger')
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


# ----------------------------------------------------------------------------
# Look up table
lut_expression = {0x00: 'Undef',
                  0x01: 'Anger',
                  0x02: 'Disgust',
                  0x04: 'Fear',
                  0x08: 'Happy',
                  0x10: 'Neutral',
                  0x20: 'Sadness',
                  0x40: 'Surprise'}

event_map = {'parler': 0x01,
             'soleil': 0x02,
             'fatigue': 0x04,
             'occlusionfrontale':0x08,
             'occlusiondroite': 0x10}

expression_map = {'indef': 0x00,
                  'colere': 0x01,
                  'degout': 0x02,
                  'peur': 0x04,
                  'joie': 0x08,
                  'neutre': 0x10,
                  'tristesse': 0x20,
                  'surprise': 0x40}

valence_map = {'indef': -100,
               'positive': 1,
               'neutre': 0,
               'negative': -1}

intensity_map = {'indef': -100,
                 '-2trespeu': -2,
                 '-1peu': -1,
                 '0neutre': 0,
                 '+1beaucoup': 1,
                 '+2extrememet': 2,
                 '+2extremement': 2}

uncertainty_map = {'indef': -100,
                   '-2pasdutoutsur': -2,
                   '-2pasdetoutsur': -2,
                   '-1peusur': -1,
                   '0moyennementsur': 0,
                   '+1plutotsur': 1,
                   '+2totalementsur': 2,
                   '+2totalemementsur': 2}


# ----------------------------------------------------------------------------
class OverlapType:
    """
        Class listing all possible overlapping
    """
    NONE = 0
    BEGINNING = 1
    END = 2
    MIDDLE = 3
    OVERALL = 4


# ----------------------------------------------------------------------------
# String normalization
# http://stackoverflow.com/questions/1382998
def norm_str(string):
    """
    Take an arbitrary string and convert it to lower case and remove 
    all spaces
        
    http://stackoverflow.com/questions/1382998
        
    :param string:  String to be normalized 
    :return:        Normalized string
    """
    ascii_str = unicodedata.normalize('NFKD',
                                      string.decode('utf-8')).encode('ASCII',
                                                                     'ignore')
    return "".join(ascii_str.split()).lower()


# ----------------------------------------------------------------------------
# Timestamp normalization
def norm_timestamp(ts):
    """
    Normalize a given timestamp with 4 significant digits
        
    :param ts:  Timestamp to normalize
    :return:    Normalized timestamp
    """
    return round(ts, 4)


# ----------------------------------------------------------------------------
# Scan folder recursively, pick file that match a given extension
def scan_folder(folder, ext):
    """
    Scan a given folder for files with a specific extension in a
    recursive way.
        
    :param folder:  Root folder to scan
    :param ext:     Extension to filter
    :return:    List of file with the desired extension
    """
    picked_file = []
    if folder[-1] != '/':
        folder += '/'
    for root, dirs, files in os.walk(folder):
        # Loop over files
        for f in files:
            # Correct extension ?
            if f.endswith(ext):
                picked_file.append(root + f)
    picked_file.sort()
    return picked_file


# ----------------------------------------------------------------------------
# Create destination folders
def init_folder_structure(root, subject_id):
    """
    Initialiaze folder structure for a specific subect
    
    :param root:        Root destination where data will be placed
    :param subject_id:  ID of the subject (Unique number)
    """
    # Check if destination exist
    d_folder = root if root[-1] == '/' else root + '/'
    d_folder += 'S%0.3d' % subject_id
    if not os.path.exists(d_folder):
        # Create subject folder
        os.makedirs(d_folder)
        # Create expression folder
        for key, e in lut_expression.iteritems():
            if e != 'Undef':
                os.makedirs(d_folder + '/' + e)


def convert_timestamp(time):
    """
    Convert string timestamp with the format : HH:MM:ss.xxx to sec
    
    :param time:    String with time formatted
    :return:        Time in seconds
    """
    t_part = time.strip().split(':')
    t_sec = float(t_part[2])
    t_sec += float(t_part[1]) * 60.0
    t_sec += float(t_part[0]) * 3600.0
    return round(t_sec, 4)


def overlap_fn(label, event):
    """
    Check if a given label overlap with the extracted events
    
    :param label:   Label of interest
    :param event:   Event to compare against
    :return:    True if overlapping, false otherwise
    """
    if event.start <= label.start < event.end:
        return True
    elif label.end > event.start >= label.start and (
            event.end <= label.end or event.end >= label.end):
        return True
    else:
        return False


def skip_this_frame(frame_idx, camera_idx, skip_list):
    """
    Check if a given frame index has to be skipped because it falls into 
    a skiped region
    
    :param frame_idx:   Frame index to check
    :param camera_idx:  Current view index
    :param skip_list:   List of region to skip
    :return:    True if index need to be skipped, False otherwise    
    """
    for r in skip_list:
        start = r[0][camera_idx]
        stop = r[1][camera_idx]
        if start <= frame_idx <= stop:
            return True
    return False


# ----------------------------------------------------------------------------
class Subject:
    """
        Hold information relevant to a Subject
    """
    def __init__(self):
        # Subect ID
        self.id = ''
        # Path to folder with recording
        self.data_path = ''
        # Path to folder with annotation
        self.annotation_path = ''
        # Path to list of timestamps -> For video croping
        self.timestamp_path = ''
        # offset
        self.offset_path = ''

    def is_valid(self):
        """
            Check validity of the subject
        :return:    True if Ok, false otherwise
        """
        if (len(self.id) > 0 and len(self.data_path) == 1 and len(
                self.annotation_path) > 1 and len(self.timestamp_path)
                == 4) and len(self.offset_path) == 1:
            return True
        else:
            return False


class Event:
    """
        Event object storing the information
    """
    def __init__(self, data):
        """
        Create an event
        
        :param data:    One row of DataFrame.Series object
        """
        self.type = event_map[data[1]]
        self.start = round(data[0], 4)
        self.end = self.start + data[3]


# ----------------------------------------------------------------------------
class VideoLoader:
    """
    Wrapper around opencv video loader
    """
    def __init__(self, filename):
        """
        Create video loader
        
        :param filename:    Path to video file 
        """
        self.capture = cv2.VideoCapture(filename)
        self.counter = 0

    def is_opened(self):
        """
        Indicate if the video is open
        :return:    True if open, false otherwise
        """
        return self.capture.isOpened()

    def cleanup(self):
        """
        Free ressources
        :return: 
        """
        if self.capture.isOpened():
            self.capture.release()

    def read(self, frame_id):
        """
        Read a specific frame number
        
        :param frame_id:    Frame number wanted
        :return:    Image corresponding, or empty matrix if fail
        """
        if self.counter > frame_id:
            msg = 'Error, asked for frame already loaded (' + str(self.counter)\
                  + ' / ' + str(frame_id)
            logger.error(msg)
            return False, None
        else:
            while self.counter <= frame_id:
                self.capture.grab()
                self.counter += 1
            return self.capture.retrieve()


# ----------------------------------------------------------------------------
class Label:
    """
    Object containing information about a manual annotation
    """
    def __init__(self, data):
        """
        Create a label from manual annotation extracted in csv file
        
        :param data:    DataFrame object
        """
        class_index = pandas.Index(data.get('Classe'))
        observable = data.get('Observable')
        # Time
        self.skip = []
        self.skip_type = []
        self.unify_skip = []
        self.start = data.iloc[0, 0]
        self.end = self.start + data.iloc[0, 3]
        self.overlap_type = OverlapType.NONE
        # Frame index
        self.start_f = None
        self.end_f = None
        # Expression
        test_val = ['emotions', 'emotion']
        cnt = 0
        for e in test_val:
            try:
                i = class_index.get_loc(e)
                self.exp = expression_map[observable.iloc[i]]
                break
            except KeyError:
                cnt += 1
                self.exp = 0x00
        if cnt == len(test_val):
            logger.warning('Emotion label not consistent')
            logger.debug(data)
        # Valence
        test_val = ['valence']
        cnt = 0
        for v in test_val:
            try:
                i = class_index.get_loc(v)
                self.valence = valence_map[observable.iloc[i]]
                break
            except KeyError:
                cnt += 1
                self.valence = valence_map['neutre']
        if cnt == len(test_val):
            logger.warning('Valence label not consistent')
            logger.debug(data)
        # Intensity
        test_val = ['intensite']
        cnt = 0
        for v in test_val:
            try:
                i = class_index.get_loc(v)
                self.intensity = intensity_map[observable.iloc[i]]
                break
            except KeyError:
                cnt += 1
                self.intensity = intensity_map['0neutre']
        if cnt == len(test_val):
            logger.warning('Intensity label not consistent')
            logger.debug(data)
        # Uncertainty
        test_val = ['degredecertitude']
        cnt = 0
        for u in test_val:
            try:
                i = class_index.get_loc(u)
                self.uncertainty = uncertainty_map[observable.iloc[i]]
                break
            except KeyError:
                cnt += 1
                if self.exp == expression_map['neutre']:
                    self.uncertainty = uncertainty_map['+2totalementsur']
                else:
                    self.uncertainty = uncertainty_map['0moyennementsur']
        if cnt == len(test_val):
            logger.warning('Uncertainty label not consistent')
            logger.debug(data)

    def unify_skiplist(self):
        if len(self.skip) > 1:
            # Sort
            self.unify_skip = sorted(self.skip, key=lambda t: (t[0][0], t[1][0]))
            # Union of overlapping element
            i = 0
            while i < len(self.unify_skip) - 1:
                c_elem = self.unify_skip[i]
                n_elem = self.unify_skip[i + 1]
                if n_elem[0] <= c_elem[1]:
                    s = min(c_elem[0], n_elem[0])
                    e = max(c_elem[1], n_elem[1])
                    self.unify_skip[i] = (s, e)
                    del self.unify_skip[i + 1]
                else:
                    i += 1
            # Check if distance between two overlap is bigger than one
            i = 0
            while i < len(self.unify_skip) - 1:
                c_elem = self.unify_skip[i]
                n_elem = self.unify_skip[i + 1]
                if n_elem[0][0] - c_elem[1][0] <= 1:
                    self.unify_skip[i] = (c_elem[0], n_elem[1])
                    del self.unify_skip[i + 1]
                else:
                    i += 1

        else:
            self.unify_skip = self.skip
        # Check if overlap totally the segment
        if len(self.unify_skip) == 1 and \
                self.overlap_type != OverlapType.OVERALL:
            d_start = map(abs,
                          map(operator.sub,
                              self.unify_skip[0][0],
                              self.start_f))
            d_end = map(abs,
                        map(operator.sub,
                            self.unify_skip[0][1],
                            self.end_f))
            if d_start <= [1, 1, 1] and d_end <= [1, 1, 1]:
                self.overlap_type = OverlapType.OVERALL
            if self.unify_skip[0][0] == self.start_f and \
                            self.unify_skip[0][1] == self.end_f:
                self.overlap_type = OverlapType.OVERALL

    def __str__(self):
        """
            Dump Label object into string
        :return:    Formatted string
        """
        value = lut_expression[self.exp]
        value += '  valence=' + str(self.valence)
        value += '  intensity=' + str(self.intensity)
        value += '  uncertainty=' + str(self.uncertainty)
        return value


class SubjectSegmentation:
    """
    Processing Helper class for data segmentation of expression recordings

    Attributes:
        labels      Expert annotation in a csv format loaded with pandas.

    """
    def __init__(self, subject, output_path, subject_id, separator, log_label):
        """
        Constructor
        
        :param subject:         Subject for image extraction
        :param output_path:     Where to write data
        :param subject_id:      ID of the subject
        :param log_label:       Logger for annotations
        """
        # Load label from csv
        self._headers = []
        self._load_label(subject.annotation_path, separator=separator)
        # Remove unnecessary data :
        #   Classe == Commentaire
        #   Observable == null
        self._anno = self._anno[self._anno.Classe != 'Commentaire']
        self._anno = self._anno[self._anno.Observable != 'null']
        self._anno.reset_index(drop=True, inplace=True)
        # Normalize string classe + observable
        self._anno.Classe = self._anno.Classe.map(norm_str)
        self._anno.Observable = self._anno.Observable.map(norm_str)
        self._anno.iloc[:, 3] = self._anno.iloc[:, 3].map(convert_timestamp)
        self._anno.Date = self._anno.Date.map(norm_timestamp)
        # Parse data
        self._parse_annotation()
        # Load timestamp
        self.expert_view_ts = None
        self.camera_ts = []
        self.camera_name = []
        self._load_timestamps(subject.timestamp_path)
        # Load offset
        self.expert_view_offset = 0
        self.offset = self._load_offset(subject.offset_path[0])
        # Get video loader
        self.video = []
        self._open_video(subject.data_path[0])
        # output location
        self.output_path = output_path
        # subject counter
        self.subject_id = subject_id
        # Labels
        self._log_label = log_label
        # Can be used ?
        self.init = True if len(self.camera_ts) == len(self.video) else False

    def cleanup(self):
        """
            Clean up internal register such as :
                Annotation log
                Video readers
        """
        for v in self.video:
            v.cleanup()

    def _load_label(self, annotation_file, separator):
        """
        Load annotation in panda's data structure
        
        :param annotation_file: Annotation file corresponding to recordingsS
        :return:                Csv array stored in panda's data structure
        """
        # Open csv with proper encoding
        with io.open(annotation_file, 'r', encoding='utf-16-le') as f:
            # skip beginning for the moment
            line = ''
            while line[0:3] != 'Dur':
                line = f.readline()
            # Read header + remove duplicate
            f.readline()
            self._headers = f.readline().strip().split(separator)
            # Read begining of data
            read = True
            while read:
                pos = f.tell()
                line = f.readline().strip().split(separator)
                if line[0] != '' and line[0] == '0':
                    read = False
                    f.seek(pos)
            # load with pandas
            self._anno = pandas.read_csv(f,
                                         sep=separator,
                                         names=self._headers,
                                         usecols=self._headers)

    def _load_timestamps(self, timestamp_files):
        """
        Load timestamp data from file
            
        :param timestamp_files:     Path to file
        """
        # Loop over timestamps
        for timestamp in timestamp_files:
            # Open csv with proper encoding
            sep = ';'
            with open(timestamp, 'r') as f:
                header = f.readline().strip().split(sep)
                if 'expert' in timestamp:
                    # Load expert view
                    self.expert_view_ts = pandas.read_csv(f,
                                                          sep=sep,
                                                          names=header)
                else:
                    # load data
                    self.camera_ts.append(pandas.read_csv(f,
                                                          sep=sep,
                                                          names=header))
                    # get camera name
                    self.camera_name.append(f.name.split('_')[-2])

    def _open_video(self, video_path):
        """
        Open video reader
        
        :param video_path:  Path to video file
        """
        # Scan folder for video
        videos = scan_folder(video_path, 'avi')
        for v in videos:
            cap = VideoLoader(v)
            if cap.is_opened():
                self.video.append(cap)
            else:
                logger.warning('Unable to open : ' + v)

    def _load_offset(self, offset_path):
        """
        Load offset for video synchronization
            
        :param offset_path:     Path to offset file
        """
        offsets = {}
        with open(offset_path, 'r') as f:
            for line in f:
                if line[0:3] == 'cam':
                    offset_value = re.findall('\d+', line.strip())[0]
                    camera_name = re.findall('[a-zA-Z_]+', line.strip())[0]
                    offsets[camera_name] = offset_value
                if line[0:6] == 'expert':
                    self.expert_view_offset = re.findall('\d+', line.strip())[0]
        return offsets

    def _convert_ts_to_frame_id(self, timestamp, ts_type):
        """
        Convert a given timestamp into frame ID.
            
        :param timestamp: Timestamp
        :param ts_type:   Starting or ending (start = 0, end > 0)
        :return:    list of frame_id
        """
        # Loop
        converted_ts = []
        ts_ms = timestamp * 1000.0
        ts_ms += float(self.expert_view_offset)
        for (ts, cam) in itertools.izip(self.camera_ts, self.camera_name):
            # Convert
            if ts_type == 0:
                idx = (ts['ts_ms'] <= ts_ms).idxmin()
            else:
                idx = (ts['ts_ms'] >= ts_ms).idxmax() - 1
            converted_ts.append(ts.ix[idx, 'frame_id'])
        return converted_ts

    def _parse_annotation(self):
        """
        Extract Labels + Events and store them into dedicated list
        """
        self._events = []
        self._labels = []
        # Extract event
        ev_idx = self._anno.Observable == 'oui'
        ev = self._anno[ev_idx]
        for idx, r in ev.iterrows():
            self._events.append(Event(r))
        # Extract annotation
        a_idx = self._anno.Observable != 'oui'
        ann = self._anno[a_idx]
        i = 0
        while i < len(ann):
            # Seek for 4 consecutive equal timestamp
            curr_t = round(ann.iloc[i, 0], 4)
            k = 0
            while i + k < len(ann) and round(ann.iloc[i + k, 0], 4) == curr_t:
                k += 1
            if k == 4:
                # Found a block of valid annotation
                lbl = Label(data=ann.iloc[i:i+k, :])
                if lbl.end - lbl.start > 0.033:
                    self._labels.append(lbl)
                else:
                    logger.warning('Drop label due to too short duration ('
                                   'less 33ms' + str(lbl))
            else:
                logger.error('Unconsistant annotation, find only ' + str(k) +
                             ' consecutive similar timestamps')
                logger.debug(ann.iloc[i:i+k, :])
            i += k

        logger.debug('Extracted ' + str(len(self._labels)) + ' annotations')

    def process(self):
        """
        Process parsed annotation in order to generate the final 
        starting/ending point to extract taking into account the extra 
        events
        """
        err = -1
        if self.init:
            # Finalize labels
            self._finalize_labels()
            # Dump
            self._extract_image()
            err = 0
        return err

    def _finalize_labels(self):
        """
        Define region where frames have to be skipped due to event like :
        speak, sun, fatigue
        """
        for i, lbl in enumerate(self._labels):
            # Convert time to frame number
            lbl.start_f = self._convert_ts_to_frame_id(lbl.start, 0)
            lbl.end_f = self._convert_ts_to_frame_id(lbl.end, 1)

            diff = map(operator.sub, lbl.end_f, lbl.start_f)
            if diff[1] != diff[2]:
                logger.warning('Left/Right do not have the same number of '
                               'images : %d/%d' % (diff[1], diff[2]))
            # Check if overlap with any events
            lbl.skip = []
            lbl.skip_type = []
            lbl.overlap_type = OverlapType.NONE
            overlaps = [ev for ev in self._events if overlap_fn(lbl, ev)]
            if len(overlaps) != 0:
                # Something overlap, update skip list
                for ov in overlaps:
                    # find type of overlap
                    if ov.start <= lbl.start and ov.end < lbl.end:
                        # Beginning
                        strt = lbl.start_f
                        stop = self._convert_ts_to_frame_id(ov.end, 1)
                        lbl.skip.append((strt, stop))
                        lbl.skip_type.append(ov.type)
                        if lbl.overlap_type != OverlapType.OVERALL:
                            lbl.overlap_type = OverlapType.BEGINNING
                    elif ov.start > lbl.start and ov.end >= lbl.end:
                        # End
                        strt = self._convert_ts_to_frame_id(ov.start, 0)
                        stop = lbl.end_f
                        lbl.skip.append((strt, stop))
                        lbl.skip_type.append(ov.type)
                        if lbl.overlap_type != OverlapType.OVERALL:
                            lbl.overlap_type = OverlapType.END
                    elif ov.start > lbl.start and ov.end < lbl.end:
                        # Middle
                        strt = self._convert_ts_to_frame_id(ov.start, 0)
                        stop = self._convert_ts_to_frame_id(ov.end, 1)
                        lbl.skip.append((strt, stop))
                        lbl.skip_type.append(ov.type)
                        if lbl.overlap_type != OverlapType.OVERALL:
                            lbl.overlap_type = OverlapType.MIDDLE
                    else:
                        # Overall
                        strt = lbl.start_f
                        stop = lbl.end_f
                        lbl.skip.append((strt, stop))
                        lbl.skip_type.append(ov.type)
                        lbl.overlap_type = OverlapType.OVERALL
                # Union
                lbl.unify_skiplist()

    def _extract_image(self):
        """
        Do the actual extraction, read video and export images to final 
        destination
        """
        label_counter = {0x01: 0,
                         0x02: 0,
                         0x04: 0,
                         0x08: 0,
                         0x10: 0,
                         0x20: 0,
                         0x40: 0}
        for lbl in self._labels:
            # Discard label due to event completely overlapping
            if lbl.exp != 0x00 and lbl.overlap_type != OverlapType.OVERALL:
                # Create segment folder
                cnt = label_counter[lbl.exp]
                label_counter[lbl.exp] += 1
                folder = self.output_path + 'S%0.3d' % self.subject_id + '/'
                folder += lut_expression[lbl.exp] + '/T%0.3d' % cnt + '/'
                if not os.path.exists(folder):
                    os.makedirs(folder)
                # Extract for each camera
                n_frame = 0
                for idx, cam in enumerate(self.video):
                    # Camera index
                    cam_folder = 'C%0.2d/' % idx
                    if not os.path.exists(folder + cam_folder):
                        os.makedirs(folder + cam_folder)
                    # set start index
                    start = lbl.start_f[idx]
                    stop = lbl.end_f[idx]
                    assert stop - start > 1
                    n = 0
                    i = start
                    cnt = 0
                    while i < stop:
                        # Check if current index fall into a skipped section
                        if not skip_this_frame(frame_idx=i,
                                               camera_idx=idx,
                                               skip_list=lbl.unify_skip):
                            # Extract only if image not already present
                            img_name = 'F%0.8d' % n + '.jpg'
                            f = folder + cam_folder + img_name
                            cnt += 1
                            if idx == 0:
                                n_frame += 1
                            if not os.path.exists(f):
                                success, img = cam.read(i)
                                # save
                                if success:
                                    cv2.imwrite(f, img)
                        # inc
                        i += 1
                        n += 1
                    assert cnt > 0
                # Log for all images at once (each view as well)
                label = folder[len(self.output_path):] + ';'
                label += lut_expression[lbl.exp] + ';'
                label += str(lbl.valence) + ';'
                label += str(lbl.intensity) + ';'
                label += str(lbl.uncertainty) + ';'
                # Event log
                cnt = {0x01: 0,
                       0x02: 0,
                       0x04: 0,
                       0x08: 0,
                       0x10: 0}
                for idx,t in enumerate(lbl.skip_type):
                    dt = abs(lbl.skip[idx][1][0] - lbl.skip[idx][0][0])
                    cnt[t] += dt
                label += '%d;%d;%d;%d;%d;%d\n' % (n_frame, cnt[1], cnt[2],
                                                  cnt[4], cnt[8], cnt[16])
                self._log_label.write(label)
                self._log_label.flush()
            else:
                logger.info('Skip label due to event overlapping')
        return 0


class SegmentationProcessor:
    """
    Dataset segmentation helper object
    """
    def __init__(self, root, annotation, timestamps, destination):
        """
        Initialization helper class
            
        :param root:        Location where recorded data are stored
        :param annotation:  Location where annotations are stored
        :param timestamps:  Location where timestamps are stored
        :param destination: Where to write data
        """
        # Data root folder
        self.root = root if root[-1] == '/' else root + '/'
        # Folder where annotation are stored
        self.annotation_location = (annotation if annotation[-1] == '/'
                                    else annotation + '/')
        # Folder where timestamp are stored
        self.timestamp_location = (timestamps if timestamps[-1] == '/'
                                   else timestamps + '/')
        # Destination folder
        self.dest = destination if destination[-1] == '/' else destination + '/'
        if not os.path.exists(self.dest):
            os.makedirs(self.dest)
        # List of subect to treat
        self.subjects = []
        # Label log
        logf = self.dest + 'label.csv'
        self._log_label = open(logf, 'w')
        header = 'ImagePath;Expression;Valence;Intensity;Uncertainty;'
        header += '#frame;#Speak;#Sun;#Fatigue;#FrontalOcclusion;' \
                  '#RightOcclusion\n'
        self._log_label.write(header)

    def gather_files(self):
        # Scan each folder to collect data needed by the helper
        annotation = scan_folder(self.annotation_location, 'csv')
        timestamps = scan_folder(self.timestamp_location, 'csv')
        dirs = os.listdir(self.root)
        # Loop over all annotation
        for ann in annotation:
            # Create empty subect
            subject = Subject()
            # Define annotation path
            subject.annotation_path = ann
            # Define subect id
            f = ann[len(self.root):].split('/')[-1]
            start_id = len('Participant ')
            subject.id = f[start_id:start_id+4]
            # Define data folder
            subject.data_path = [self.root + x for x in dirs if subject.id in x]
            # Define timestamps
            subject.timestamp_path = [x for x in timestamps if subject.id in x]
            # Define offset_path
            subject.offset_path = scan_folder(subject.data_path[0], 'txt')
            # Check if valid
            if subject.is_valid():
                self.subjects.append(subject)
            else:
                logger.error('Entry : ' + f + ' is not valid !')

    def segment_dataset(self):
        error = -1
        if len(self.subjects) > 0:
            # Start processing
            for idx, s in enumerate(self.subjects):
                # Create subect structure if not already set
                init_folder_structure(self.dest, idx)
                # Create processor for this subject
                logger.info('Subject : ' + s.id)
                logger.info('-------------------------------------------------')
                subject_p = SubjectSegmentation(subject=s,
                                                output_path=self.dest,
                                                subject_id=idx,
                                                separator='\t',
                                                log_label=self._log_label)
                # Process
                if subject_p.process():
                    logger.warning('Can not process subject : ' + s.id)
                # Cleanup
                subject_p.cleanup()
        else:
            logger.warning('No subject available')
        return error

    def release(self):
        """
            Close label log file
        """
        if not self._log_label.closed:
            self._log_label.close()


if __name__ == '__main__':
    # Cmd line parser
    parser = argparse.ArgumentParser(description='Expression segmentation tool')
    # Input
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='root',
                        help='Root folder')
    # Annotation
    parser.add_argument('-a',
                        type=str,
                        required=True,
                        dest='annotation_folder',
                        help='Annotation folder')
    # Timestamps
    parser.add_argument('-t',
                        type=str,
                        required=True,
                        dest='timestamp_folder',
                        help='Timestamp folder')
    # Export
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='export',
                        help='Export folder location')
    # Parse
    args = parser.parse_args()
    # Create SegmentationProcessor
    p = SegmentationProcessor(root=args.root,
                              annotation=args.annotation_folder,
                              timestamps=args.timestamp_folder,
                              destination=args.export)
    p.gather_files()
    p.segment_dataset()
    p.release()
