# coding=utf-8
import argparse
import colorlog
import logging
import os
import shlex
import subprocess
import re
import lts5.utils as utils
__author__ = 'Christophe Ecabert'

# Sanity check
utils.check_py_version(3, 0)
# Init logger
logger = utils.init_logger()


# ----------------------------------------------------------------------------
# Automatic launcher
class ReconstructionLauncher:
    """
    Automatically generate all reconstruction of the PSA/Valeo database
    """
    def __init__(self, args):
        """
        Constructor
            
        :param args:    Structure with the parameters needed by the launcher
        """
        # Data folder location
        self._input = args.root if args.root[-1] == '/' else args.root + '/'
        # Working directory for reconstruction
        self._work_dir = (args.word_dir if args.word_dir[-1] == '/'
                          else args.word_dir + '/')
        # Reconstruction executable
        self._exec = args.executable
        # Template
        path = self._work_dir + args.xml_template if self._work_dir \
            else args.xml_template
        with open(path, 'r') as f:
            self._xml = f.read()
        # Camera index
        self._cams = list(map(int, args.cam.strip().split(';')))
        # Destination
        self._out = args.output if args.output[-1] == '/' else args.output + '/'

    def _gather_file(self, filename='label.csv', sep=';'):
        """
        Read all entry in the label file
            
        :param filename:    Name if the file where labels are stored
        :param sep:         Label separator
        :return:            List of folder to process, or an empty list in 
        case of something went wrong.
        """
        # Open labels
        folders = []
        with open(self._input + filename, 'r') as f:
            # Skip headers
            f.readline()
            for line in f:
                folders.append(line.strip().split(sep)[0])
        return folders

    def run(self):
        # gather folders
        folders = self._gather_file()
        if folders:
            # Run reconstruction for every entry in the label files
            for folder in folders:
                # User info
                logger.info('Process folder : ' + folder)
                # Generate configuration file
                self._generate_template(input=folder, ext='.jpg')
                # make output
                output = self._out + folder
                if not os.path.exists(output):
                    os.makedirs(output)
                # run reconstruction only if folder not empty
                if os.listdir(output) == []:
                    cmd = self._exec + ' -c cache/config.xml -o ' + output
                    cmd_exec = shlex.split(cmd)
                    s = subprocess.Popen(cmd_exec,
                                         cwd=self._work_dir,
                                         stdout=subprocess.PIPE,
                                         stderr=subprocess.PIPE)
                    cmd_out, cmd_err = s.communicate()
                    if s.returncode or len(cmd_err) != 0:
                        logger.error('Reconstruction fail to run correctly '
                                     + str(s.returncode))
                        logger.error('stdout : ' + cmd_out.decode('utf-8'))
                        logger.error('stderr : ' + cmd_err.decode('utf-8'))
                    # Clear
                    del s
                else:
                    logger.info('Skip reconstruction, already done !')
        else:
            logger.error('No labels founded !')

    def _generate_template(self, input, ext):
        # Set input location
        root = input if input[-1] == '/' else input + '/'
        xml = re.sub(r'root=\"\"', r'root="' + self._input + root + '"',
                     self._xml)
        xml = re.sub(r'ext=\"\"', r'ext="' + ext + '"', xml)
        # Set camera input
        for i, cam_idx in enumerate(self._cams):
            f = 'C' + str(cam_idx) + '/'
            srch = r'Cam%d CalibFile=\"\" ImgFolder=\"\"' % i
            repl = r'Cam%d CalibFile="" ImgFolder="C%02d/"' % (i, cam_idx)
            xml = re.sub(srch, repl, xml)
        # save
        f_name = '%s/cache/config.xml' % self._work_dir
        if not os.path.exists(os.path.dirname(f_name)):
            os.makedirs(os.path.dirname(f_name))
        with open(f_name, 'w') as f:
            f.write(xml)


if __name__ == '__main__':
    # Parser
    parser = argparse.ArgumentParser(description='Automatic 3D face '
                                                 'reconstruction launcher')
    # Input folder
    parser.add_argument('-i',
                        type=str,
                        required=True,
                        dest='root',
                        help='Input folder where labels are stored')
    # Working directory
    parser.add_argument('-w',
                        type=str,
                        required=True,
                        dest='word_dir',
                        help='Working directory  for C++ executable')
    parser.add_argument('-e',
                        type=str,
                        required=True,
                        dest='executable',
                        help='C++ executable name')
    # XML template
    parser.add_argument('-t',
                        type=str,
                        required=True,
                        dest='xml_template',
                        help='XML file template for reconstruction '
                             'configuration')
    # Camera
    parser.add_argument('-c',
                        type=str,
                        required=True,
                        dest='cam',
                        help='Camera index to use, format : "i;j;k"')
    # Output
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        dest='output',
                        help='Location where to place the reconstrction')
    p = parser.parse_args()
    # Launch every reconstruction
    launcher = ReconstructionLauncher(args=p)
    # Run
    launcher.run()
