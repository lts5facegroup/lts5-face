/**
 *  @file   face_processing_unit.hpp
 *  @brief  Processing unit for the following tasks :
 *            - 3D Face reconstruction
 *            - Action Unit detection
 *            - Expression detection
 *            - Cognitive distraction
 *
 *  @author Marco Lourenço
 *  @date   01/09/2016
 *  Copyright (c) 2016 Marco Lourenço. All rights reserved.
 */
#ifndef __FACE_PROCESSING_UNIT__
#define __FACE_PROCESSING_UNIT__

#include <string>
#include <vector>
#include <string>
#include <queue>
#include <deque>
#include <mutex>
#include <thread>

#include "lts5/utils/library_export.hpp"

#include "frame_buffer.hpp"
#include "video_data_loader.hpp"
#include "timer.hpp"
#include "result_data.hpp"
#include "base_data_loader.hpp"
#include "result_comparator.hpp"


namespace LTS5 {

/**
 *  @class  FaceProcessingUnit
 *  @brief  Call many Face Processing Units in a multithreaded way
 *
 *  @author Marco Lourenço
 *  @date   01/09/2016
 */
class LTS5_EXPORTS FaceProcessingUnit {

 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  FaceProcessingUnit
   * @fn    FaceProcessingUnit(int nb_cores,
                               const std::string video_folder,
                               const std::string config_file)
   * @brief Constructor
   * @param[in] nb_cores    Number of core use to run the process
   * @param[in] config_file Path to process configuration (.yaml)
   */
  FaceProcessingUnit(int nb_cores,
                     const std::string& config_file);

  /**
   * @name  ~FaceProcessingUnit
   * @fn  ~FaceProcessingUnit()
   * @brief Destructor
   */
  ~FaceProcessingUnit();

#pragma mark -
#pragma mark Usage

  /**
   * @name  Process
   * @fn  void Process()
   * @brief Launch the computation on all cores specified by this.nb_cores
   */
  void Process();

  /**
   * @name  WaitProcessEnded
   * @fn  void WaitProcessEnded()
   * @brief Wait until all processing has been done. This is useful when there
   * is a max number of frames.
   */
  void WaitProcessEnded();

#pragma mark -
#pragma mark Accessors

  /**
   * @name    set_loader
   * @fn  void set_loader(BaseDataLoader *loader)
   * @brief   Setup where pipeline need to query frames
   * @param loader    Data loader to use
   */
  void set_loader(BaseDataLoader *loader) {
      loader_ = loader;
  }

  /**
   * @name  GetNextFrame
   * @fn  LTS5::ResultData* GetNextFrame()
   * @brief Return the next frame
   * @result the next image contained in a ResultData container or NULL if empty
   */
  LTS5::ResultData *GetNextFrame();

  /**
   * @name  is_processing
   * @fn  bool is_processing()
   * @brief Return the status of processing
   * @result a boolean telling if something is still processing
   */
  bool is_processing();

#pragma mark -
#pragma mark Private

 private:

  /**
   * @name  SolveCore
   * @fn  void SolveCore(const int my_id)
   * @brief Do the computation for one core
   * @param[in] my_id the ID of the core
   */
  void SolveCore(const int my_id);

  /** Actual implementation class (using PIMPL idiom) */
  class FaceProcessingUnitImpl;

  /** List of Processing units */
  std::vector<LTS5::FaceProcessingUnit::FaceProcessingUnitImpl *> unit_;
  /** Number of cores */
  int nb_cores_;
  /** Path to configuration file */
  std::string config_file_;
  /** to acquire images from camera or video */
  LTS5::BaseDataLoader *loader_;
  /** thread group used to launch processing */
  std::vector<std::thread> th_grp_;
  /** priority queue containing the output results */
  std::priority_queue<LTS5::ResultData *,
          std::deque<LTS5::ResultData *>,
          LTS5::ResultComparator> priority_queue_;
  /** Frame ID to release */
  unsigned int expected_frame_id_;
  /** Mutex for priority queue */
  std::mutex pqueue_mutex_;
  /** keep track of processing state of threads */
  std::vector<bool> is_processing_;
};
}

#endif
