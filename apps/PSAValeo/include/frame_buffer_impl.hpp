/**
 *  @file   frame_buffer_impl.hpp
 *  @brief  Implementation of FIFO queue to store data to process
 *
 *  @author Marco Lourenço
 *  @date   01/09/2016
 *  Copyright (c) 2016 Marco Lourenço. All rights reserved.
 */

#include <chrono>
#include <thread>

#include "frame_buffer.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name	FrameBuffer
 * @fn	FrameBuffer(void)
 * @brief Constructor
 */
template <typename T>
FrameBuffer<T>::FrameBuffer(void) : my_fifo_(nullptr),
                                    to_read_(0),
                                    to_write_(0),
                                    crt_size_(0),
                                    max_size_(0) {
}

/*
 * @name	FrameBuffer
 * @fn	explicit FrameBuffer(const int size)
 * @brief Constructor
 * @param[in]	size	Queue size
 */
template <typename T>
FrameBuffer<T>::FrameBuffer(int size) : to_read_(0),
                                        to_write_(0),
                                        crt_size_(0),
                                        max_size_(size) {
  my_fifo_ = (T**) malloc(sizeof(T*) * max_size_);
}

/*
 * @name	~FrameBuffer
 * @fn	~FrameBuffer(void)
 * @brief	Destructor
 */
template <typename T>
FrameBuffer<T>::~FrameBuffer() {
  if (my_fifo_ != nullptr) {
    free(my_fifo_);
  }
}

/*
 * @name	Put
 * @fn	void Put(T* element)
 * @brief	Add new element into the queue
 * @param[in] element	Object to add to the queue
 */
template <typename T>
void FrameBuffer<T>::Put(T* element) {
  // Wait for some space
  while (crt_size_ == max_size_) {
    std::this_thread::sleep_for(std::chrono::microseconds(1));
  }
  // Lock queue to avoid data race
  pool_lock_.lock();
  // Add element
  my_fifo_[to_write_] = element;
  // Update index
  to_write_ = (to_write_ + 1) % max_size_;
  ++crt_size_;
  // Release lock
  pool_lock_.unlock();
}

/*
 * @name	Get
 * @fn	T* Get(void)
 * @brief	Get first element in the queue
 * @return first element in the queue
 */
template <typename T>
T* FrameBuffer<T>::Get(void) {
  // Wait for data
  while (crt_size_==0) {
    std::this_thread::sleep_for(std::chrono::microseconds(1));
  }
  // Lock queue to avoid data race
  pool_lock_.lock();
  // Get element
  T* v = my_fifo_[to_read_];
  // Update index
  to_read_ = (to_read_+1) % max_size_;
  --crt_size_;
  // Release lock
  pool_lock_.unlock();
  return v;
}

/*
 * @name	is_empty
 * @fn	bool is_empty(bool) const
 * @brief	Indicate if the queue is empty
 * @return	True if empty, false otherwise
 */
template <typename T>
bool FrameBuffer<T>::is_empty(void) const {
  return (crt_size_ == 0);
}

/*
 * @name	is_full
 * @fn	bool is_full() const
 * @brief	Indicate if the queue is full
 * @return	True if full, false otherwise
 */
template <typename T> bool FrameBuffer<T>::is_full(void) const {
  return (crt_size_ == max_size_);
}

/*
 * @name	get_size
 * @fn	int get_size() const
 * @brief	Give the actual size of the queue
 * @return	Size of the queue
 */
template <typename T>
int FrameBuffer<T>::get_size(void) const {
  return crt_size_;
}
}  // namepsace LTS5
