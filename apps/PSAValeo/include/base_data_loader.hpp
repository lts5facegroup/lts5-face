/**
 *  @file   base_data_loader.hpp
 *  @brief  Interface definition for pipeline data loader.
 *
 *  @author Marco Lourenço
 *  @date   28/09/2016
 *  Copyright (c) 2016 Marco Lourenço. All rights reserved.
 */

#ifndef __LTS5_BASE_DATA_LOADER__
#define __LTS5_BASE_DATA_LOADER__

#include "frame_buffer.hpp"

#define  EOD NULL        // End Of Data

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  BaseDataLoader
 *  @brief  Interface definition for pipeline data loader.
 *  @author Marco Lourenço
 *  @date   28/09/2016
 */
class BaseDataLoader {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  BaseDataLoader
   * @fn    BaseDataLoader(int nb_queues, int fifo_size)
   * @brief Constructor
   * @param nb_queues   Number of queue needed
   * @param fifo_size   Size of each queue
   */
  BaseDataLoader(int nb_queues, int fifo_size) : nb_queues_(nb_queues),
                                                 fifo_size_(fifo_size),
                                                 frame_id_(0) {
    // create an empty pool of frames
    frame_buffer_ = (FrameBuffer<LTS5::StreamData>**) malloc(sizeof(FrameBuffer<LTS5::StreamData>*) *
                                                                     nb_queues_);
    for (int i = 0; i < nb_queues_; i++) {
      frame_buffer_[i] = new FrameBuffer<LTS5::StreamData>(fifo_size_);
    }
  }

  /**
   * @name  ~BaseDataLoader
   * @fn    virtual ~BaseDataLoader(void)
   * @brief Destructor
   */
  virtual ~BaseDataLoader(void) {
  }

  /**
   * @name  Init
   * @fn    virtual int Init(const std::string& folder) = 0
   * @brief Initialize data loader
   *        Need to be implemented by the user
   * @param folder  Path to configuration/data location
   * @return    -1 if error, 0 otherwise
   */
  virtual int Init(const std::string& folder) = 0;

#pragma mark -
#pragma mark Usage

  /**
   * @name  GetData
   * @fn    virtual LTS5::StreamData* GetData(void) = 0
   * @brief Load data from a given support and return a StreamData object.
   *        Need to be implemented by the user
   * @return
   */
  //virtual LTS5::StreamData* GetData(void) = 0;

  /**
   * @name  Start
   * @fn    virtual void Start(void) = 0
   * @brief Start to load data
   *        Need to be implemented by the user
   */
  virtual void Start(void) = 0;

  /**
   * @name  Stop
   * @fn    virtual void Stop(void) = 0
   * @brief Stop loading data
   *        Need to be implemented by the user
   */
  virtual void Stop(void) = 0;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_frame_buffer_
   * @fn    FrameBuffer<StreamData> **get_frame_buffer_() const
   * @brief Return list of buffer
   * @return    List of queue where frames are stored
   */
  FrameBuffer<StreamData>** get_frame_buffer_() const {
    return frame_buffer_;
  }

#pragma mark -
#pragma mark Protected

 protected:
  /** the pool of StreamData used as input */
  FrameBuffer<LTS5::StreamData> **frame_buffer_;
  /** number of queues */
  int nb_queues_;
  /** size of the FIFO */
  int fifo_size_;
  /** Frame ID */
  unsigned int frame_id_;
  /** start or stop data input */
  bool is_data_loading_;
};

}  // namepsace LTS5
#endif //__LTS5_BASE_DATA_LOADER__
