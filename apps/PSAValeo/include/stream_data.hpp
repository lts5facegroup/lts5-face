/**
 *  @file   stream_data.hpp
 *  @brief  Container holding all information needed to process one "frame"
 *
 *  @author Christophe Ecabert
 *  @date   02/08/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_STREAM_DATA__
#define __LTS5_STREAM_DATA__

#include <vector>

#include "opencv2/core/core.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/timestamp.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   StreamData
 * @brief   Container holding all information needed to process one "frame"
 * @author  Christophe Ecabert
 * @date    02/08/2016
 */
class LTS5_EXPORTS StreamData {

 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  StreamData
   * @fn    StreamData(const std::vector<cv::Mat>& buffer,
                       const unsigned int frame_id)
   * @brief Constructor
   * @param[in] buffer      Image data buffer
   * @param[in] frame_id    Image frame ID
   */
  StreamData(const std::vector<cv::Mat>& buffer, const unsigned int frame_id);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_buffer
   * @fn    const std::vector<cv::Mat>& get_buffer(void) const
   * @brief Provide access to image buffer
   * @return    Reference to the image buffer
   */
  const std::vector<cv::Mat>& get_buffer(void) const {
    return buffer_;
  }

  /**
   * @name  get_buffer
   * @fn    std::vector<cv::Mat>& get_buffer(void)
   * @brief Provide access to image buffer
   * @return    Reference to the image buffer
   */
  std::vector<cv::Mat>& get_buffer(void) {
    return buffer_;
  }

  /**
   * @name  get_timestamp
   * @fn    const LTS5::TimeStamp<>& get_timestamp(void) const
   * @brief Provide access to data's timestamp
   * @return    Reference to the data's timestamp
   */
  const LTS5::TimeStamp<>& get_timestamp(void) const {
    return time_stamp_;
  }

  /**
   * @name  get_frame_id
   * @fn    unsigned int get_frame_id(void) const
   * @brief Provide a copy of the data frame ID.
   * @return    Data frame ID.
   */
  unsigned int get_frame_id(void) const {
    return frame_id_;
  }

#pragma mark -
#pragma mark Private

 private:
  /** Image data */
  std::vector<cv::Mat> buffer_;
  /** Timestamp */
  LTS5::TimeStamp<> time_stamp_;
  /** Frame ID */
  unsigned int frame_id_;
};

}  // namepsace LTS5

#endif //__LTS5_STREAM_DATA__
