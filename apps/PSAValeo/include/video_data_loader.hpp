/**
 *  @file   video_data_loader.hpp
 *  @brief  Load image into StreamData object from video file
 *
 *  @author Christophe Ecabert
 *  @date   02/08/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */
#ifndef __LTS5_VIDEO_DATA_LOADER__
#define __LTS5_VIDEO_DATA_LOADER__

#include <vector>

#include "opencv2/highgui/highgui.hpp"

#include "lts5/utils/library_export.hpp"

#include "stream_data.hpp"
#include "base_data_loader.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   VideoDataLoader
 * @brief   Load image into StreamData object from video file
 * @author  Christophe Ecabert
 * @date    02/08/2016
 */
class LTS5_EXPORTS VideoDataLoader : public BaseDataLoader {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  VideoDataLoader
   * @fn    VideoDataLoader(int nb_queues, int fifo_size)
   * @brief Constructor
   * @param[in] nb_queues the number of queues to create (depends of number of threads)
   * @param[in] buffer_size size of the buffer
   */
  VideoDataLoader(int nb_queues, int buffer_size);

  /**
   * @name  ~VideoDataLoader
   * @fn    ~VideoDataLoader() override
   * @brief Destructor
   */
  ~VideoDataLoader() override;

  /**
   * @name  Init
   * @fn    int Init(const std::string& folder) override
   * @brief Initialize data loader
   * @param folder  Folder where videos are stored
   * @return -1 if error, 0 otherwise
   */
  int Init(const std::string &folder) override;

#pragma mark -
#pragma mark Usage

  /**
   * @name  Start
   * @fn    void Start()
   * @brief Start to load data
   */
  void Start() override;

  /**
   * @name  Start
   * @fn    void Start(const int max_frames)
   * @brief Start to load data
   * @param[in] max_frames  Number of frame to load.
   */
  void Start(const int max_frames);

  /**
   * @name  Stop
   * @fn    void Stop() override
   * @brief Stop loading data
   */
  void Stop() override;

#pragma mark -
#pragma mark Private

 private:

  /**
   * @name  GetData
   * @fn    LTS5::StreamData* GetData()
   * @brief Load one frame from multiple videos into one StreamData object
   * @return    Pointer to image data
   */
  LTS5::StreamData *GetData();

  /**
   * @name  LoadingLoop
   * @fn    void LoadingLoop()
   * @brief Load data into queue
   */
  void LoadingLoop();

  /** Thread where data are loaded */
  std::thread thread_;
  /** Video loader */
  std::vector<cv::VideoCapture*> loader_;
  /** allow to limit the maximum number of treated frames */
  int max_frames_;
};
}  // namepsace LTS5

#endif //__LTS5_VIDEO_DATA_LOADER__
