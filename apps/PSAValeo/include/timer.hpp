/**
 *  @file   timer.hpp
 *  @brief  Measure time
 *
 *  @author Pierre Kuonen
 *  @date   01/09/2016
 *  Copyright (c) 2016 Pierre Kuonen. All rights reserved.
 */

#ifndef PAROCTIMER_H
#define PAROCTIMER_H

#include "lts5/utils/library_export.hpp"

/**
 *  @class  Timer
 *  @brief  Measure time
 *
 *  @author Pierre Kuonen
 *  @date   01/09/2016
 */
class LTS5_EXPORTS Timer
{
public:
  /**
   * @name  Timer
   * @fn    Timer(void)
   * @brief Default Constructor
   */
	Timer();
  /**
   * @name  Start
   * @fn    Start(void)
   * @brief Start the timer
   */
	void Start();
  /**
   * @name  Stop
   * @fn    Stop(void)
   * @brief Stop the timer
   */
	void Stop();
  /**
   * @name  Reset
   * @fn    Reset(void)
   * @brief Reset the timer
   */
	void Reset();
  /**
   * @name  Elapsed
   * @fn    double Elapsed(void)
   * @brief Get the elapsed time since the very first Start() was called
   */
	double Elapsed();
protected:
  /**
   * @name  GetCurrentTime
   * @fn    double GetCurrentTime(void)
   * @brief Get the current time in seconds
   */
	virtual double GetCurrentTime();

  /** Stores the time when start is called*/
	double start_point_;
  /** Stores the elapsed time.*/
	double elapsed_;
  /** Check if Start() was called before Stop() or Elapsed() */
	bool is_running_;
};

#endif


