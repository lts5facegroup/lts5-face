/**
 *  @file   result_data.hpp
 *  @brief  Container class for processing reuslt
 *
 *  @author Marco Lourenço
 *  @date   21/09/2016
 *  Copyright (c) 2016 Marco Lourenço. All rights reserved.
 */

#ifndef __LTS5_RESULTDATA__
#define __LTS5_RESULTDATA__

#include <vector>

#include "opencv2/core/core.hpp"
#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @class  ResultData
 *  @brief  Container class for processing reuslt
 *  @author Marco Lourenço
 *  @date   21/09/2016
 */
class LTS5_EXPORTS ResultData {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name  ResultData
   * @fn    ResultData(void)
   * @brief Constructor
   */
  ResultData(void) {}

  /**
   * @name  ResultData
   * @fn    ResultData(const ResultData& other) = delete
   * @brief Copy Constructor
   * @param[in] other   Object to copy from
   */
  ResultData(const ResultData& other) = delete;

  /**
   * @name  operator=
   * @fn    ResultData& operator=(const ResultData& rhs) = delete
   * @brief Assigment operator
   * @param[in] rhs Object to assign from
   * @return Newly assigned object
   */
  ResultData& operator=(const ResultData& rhs) = delete;

  /**
   * @name  ~ResultData
   * @fn    ~ResultData(void)
   * @brief Destructor
   */
  ~ResultData(void) {}

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_front_image
   * @fn    const cv::Mat &get_front_image(void) const
   * @brief Provide synthetic image
   * @return    Frontal image
   */
  const cv::Mat &get_front_image(void) const {
    return front_image_;
  }

  /**
   * @name  set_front_image
   * @fn    void set_front_image(const cv::Mat &front_image)
   * @brief Add synthetic image
   * @return
   */
  void set_front_image(const cv::Mat &front_image) {
    this->front_image_ = front_image;
  }

  /**
   * @name  get_exp_score
   * @fn    const std::vector<double> &get_exp_score(void) const
   * @brief Provide expression score
   * @return    Expression score
   */
  const std::vector<double>& get_exp_score(void) const {
    return exp_score_;
  }

  /**
   * @name  set_exp_score
   * @fn    void set_exp_score(const std::vector<double> &exp_score)
   * @brief Define expression score
   * @param[in] exp_score   Expression score
   */
  void set_exp_score(const std::vector<double> &exp_score) {
    this->exp_score_ = exp_score;
  }

  /**
   * @name  get_frontal_landmark
   * @fn    const cv::Mat& get_frontal_landmark(void) const
   * @brief Provide frontal landmarks
   * @return    Landmarks
   */
  const cv::Mat& get_frontal_landmark(void) const {
    return frontal_landmark_;
  }

  /**
   * @name  set_frontal_landmark
   * @fn    void set_frontal_landmark(const cv::Mat& landmarks)
   * @brief Set frontal landmarks
   * @param[in] landmarks   Frontal landmarks
   */
  void set_frontal_landmark(const cv::Mat& landmarks) {
    this->frontal_landmark_ = landmarks;
  }

  /**
   * @name  get_timestamp
   * @fn    const LTS5::TimeStamp<> &get_timestamp(void) const
   * @brief Provide timestamp
   * @return    Data timestamp
   */
  const LTS5::TimeStamp<> &get_timestamp(void) const {
    return timestamp_;
  }

  /**
   * @name  set_timestamp
   * @fn    void set_timestamp(const LTS5::TimeStamp<> &timestamp)
   * @brief Set timestamp
   * @param[in]    Timestamp
   */
  void set_timestamp(const LTS5::TimeStamp<> &timestamp) {
    this->timestamp_ = timestamp;
  }

  /**
   * @name  get_error_code
   * @fn    const int& get_error_code(void) const
   * @brief Provide error code
   * @return    Error code
   */
  const int& get_error_code(void) const {
    return error_code_;
  }

  /**
   * @name  get_error_code
   * @fn    int& get_error_code(void)
   * @brief Provide error code
   * @return    Error code
   */
  int& get_error_code(void) {
    return error_code_;
  }

  /**
   * @name  set_error_code
   * @fn    void set_error_code(const int error_code)
   * @brief Set error code
   * @param[in] error_code Error code
   */
  void set_error_code(const int error_code) {
    error_code_ = error_code;
  }

  /**
   * @name  get_frame_id
   * @fn    unsigned int get_frame_id(void) const
   * @brief Provide a copy of the data frame ID.
   * @return    Data frame ID.
   */
  unsigned int get_frame_id(void) const {
    return frame_id_;
  }

  /**
   * @name  set_frame_id
   * @fn    void set_frame_id(const unsigned int frame_id)
   * @brief Set the frame ID
   * @param[in] frame_id    Result frame ID
   */
  void set_frame_id(const unsigned int frame_id) {
    frame_id_ = frame_id;
  }

#pragma mark -
#pragma mark Private
 private:
  /** Error code */
  int error_code_;
  /** Synthetic frontal image */
  cv::Mat front_image_;
  /** Expression scores */
  std::vector<double> exp_score_;
  /** Frontal landmark */
  cv::Mat frontal_landmark_;
  /** Data timestamp */
  LTS5::TimeStamp<> timestamp_;
  /** Data frame ID */
  unsigned int frame_id_;
};

}

#endif //__LTS5_RESULTDATA__
