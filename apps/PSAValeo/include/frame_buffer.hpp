/**
 *  @file   frame_buffer.hpp
 *  @brief  FIFO queue to store data to process
 *
 *  @author Marco Lourenço
 *  @date   01/09/2016
 *  Copyright (c) 2016 Marco Lourenço. All rights reserved.
 */


#ifndef __LTS5_FRAME_BUFFER__
#define __LTS5_FRAME_BUFFER__

#include <mutex>

#include "lts5/utils/library_export.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class	FrameBuffer
 * @brief	Queue to store data to be processed
 * @author	Marco Lourenço
 * @date	01/09/2016
 */
template <typename T>
class LTS5_EXPORTS FrameBuffer {
 public:

#pragma mark -
#pragma mark Initialization

  /**
   * @name	FrameBuffer
   * @fn	FrameBuffer()
   * @brief Constructor
   */
  FrameBuffer();

  /**
   * @name	FrameBuffer
   * @fn	explicit FrameBuffer(const int size)
   * @brief Constructor
   * @param[in]	size	Queue size
   */
  explicit FrameBuffer(int size);

  /**
   * @name	FrameBuffer
   * @fn	FrameBuffer(const FrameBuffer& other) = delete
   * @brief	Copy contructor
   * @param[in] other Object to copy from
   */
  FrameBuffer(const FrameBuffer& other) = delete;

  /**
   * @name	operator=
   * @fn	FrameBuffer& operator=(const FrameBuffer& rhs) = delete
   * @brief	Assignement operator
   * @param[in] rhs	Object to assign from
   * @return	Newly assigned object
   */
  FrameBuffer& operator=(const FrameBuffer& rhs) = delete;

  /**
   * @name	~FrameBuffer
   * @fn	~FrameBuffer()
   * @brief	Destructor
   */
  ~FrameBuffer();

#pragma mark -
#pragma mark Usage

  /**
   * @name	Put
   * @fn	void Put(T* element)
   * @brief	Add new element into the queue
   * @param[in] element	Object to add to the queue
   */
  void Put(T* element);

  /**
   * @name	Get
   * @fn	T* Get()
   * @brief	Get first element in the queue
   * @return first element in the queue
   */
  T* Get();

#pragma mark -
#pragma mark Accessors

  /**
   * @name	is_empty
   * @fn	bool is_empty() const
   * @brief	Indicate if the queue is empty
   * @return	True if empty, false otherwise
   */
  bool is_empty() const;

  /**
   * @name	is_full
   * @fn	bool is_full() const
   * @brief	Indicate if the queue is full
   * @return	True if full, false otherwise
   */
  bool is_full() const;

  /**
   * @name	get_size
   * @fn	int get_size() const
   * @brief	Give the actual size of the queue
   * @return	Size of the queue
   */
  int get_size() const;

#pragma mark -
#pragma mark Private

 private:
  /** queue */
  T** my_fifo_;
  /** Index where to read in the circular queue */
  int to_read_;
  /** Index where to write in the circular queue */
  int to_write_;
  /** Current queue size */
  int crt_size_;
  /** Maximum queue size */
  int max_size_;
  /** Queue mutex for thread safety */
  std::mutex pool_lock_;
};

}  // namepsace LTS5

#include "frame_buffer_impl.hpp"
#endif	// __LTS5_FRAME_BUFFER__
