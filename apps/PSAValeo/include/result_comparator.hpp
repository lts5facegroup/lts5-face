/**
 *  @file   result_comparator.hpp
 *  @brief  Processed data comparator to order them by increasing timestamp
 *
 *  @author Marco Lourenço
 *  @date   9/30/16
 *  Copyright (c) 2016 Marco Lourenço. All rights reserved.
 */

#ifndef __LTS5_RESULT_COMPARATOR__
#define __LTS5_RESULT_COMPARATOR__

#include "lts5/utils/library_export.hpp"
#include "result_data.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 * @class   ResultComparator
 * @brief   Compare two timestamp in ResultData container
 * @author  Marco Lourenço
 * @date    9/30/16
 */
class LTS5_EXPORTS ResultComparator {
public:
  /**
   * @name  operator()
   * @fn    bool operator()(LTS5::ResultData* rd1, LTS5::ResultData* rd2)
   * @brief Compare timestamp
   * @param rd1 First timestamp
   * @param rd2 Second timestamp
   * @return    true if /p rd1 timestamp bigger than \p rd2 timestamp
   */
  bool operator()(LTS5::ResultData* rd1, LTS5::ResultData* rd2) {
    return rd1->get_timestamp() > rd2->get_timestamp();
  }
};
}  // namepsace LTS5
#endif //__LTS5_RESULT_COMPARARTOR__
