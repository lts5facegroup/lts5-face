/**
 *  @file   stream_data.cpp
 *  @brief  Container holding all information needed to process one "frame"
 *
 *  @author Christophe Ecabert
 *  @date   02/08/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include "stream_data.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 * @name  StreamData
 * @fn    StreamData(const std::vector<cv::Mat>& buffer,
                     const unsigned int frame_id)
 * @brief Constructor
 * @param[in] buffer      Image data buffer
 * @param[in] frame_id    Image frame ID
 */
StreamData::StreamData(const std::vector<cv::Mat> &buffer,
                       const unsigned int frame_id) : buffer_(buffer),
                                                      frame_id_(frame_id) {
}

}  // namepsace LTS5
