/**
 *  @file   face_processing_unit.cpp
 *  @brief  Call many Face Processing Units in a multithreaded way
 *
 *  @author Marco Lourenço
 *  @date   01/09/2016
 *  Copyright (c) 2016 Marco Lourenço. All rights reserved.
 */

#include <string>
#include <vector>

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/face_reconstruction/utilities.hpp"
#include "lts5/face_reconstruction/face_synthesis_pipeline.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/facial_analysis/expression_analysis.hpp"

#include "stream_data.hpp"
#include "face_processing_unit.hpp"

/*
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Type definition

/**
 *  @class  FaceProcessingUnitImpl
 *  @brief  Implementation of the FaceProcessingUnit class
 *
 *  @author Marco Lourenço
 *  @date   01/10/2016
 */
class FaceProcessingUnit::FaceProcessingUnitImpl : public ExternalVideoStream::ICallbacks {
 public:

#pragma mark -
#pragma mark Type declaration

  /**
   * @enum  ExpressionType
   * @brief List of possible expression
   */
  enum ExpressionType {
    /** Undefined */
    kUndefined = -1,
    /** Anger */
    kAnger = 0,
    /** Disgust */
    kDisgust = 1,
    /** Fear */
    kFear = 2,
    /** Happy */
    kHappy = 3,
    /** Neutral */
    kNeutral = 4,
    /** Sad */
    kSad = 5,
    /** Surprise */
    kSurprise = 6
  };

  /**
   * @struct  HeadPose
   * @brief Head pose information
   */
  struct HeadPose {
    /** Gamma - */
    float gamma;
    /** Theta - */
    float theta;
    /** Phi - */
    float phi;
  };

#pragma mark -
#pragma mark Initialization

  /**
   * @name  FaceProcessingUnitImpl
   * @fn    FaceProcessingUnitImpl()
   * @brief Constructor
   */
  FaceProcessingUnitImpl();

  /**
   * @name  ~FaceProcessingUnitImpl
   * @fn  ~FaceProcessingUnitImpl()
   * @brief Destructor
   */
  ~FaceProcessingUnitImpl();

  /**
   * @name  FaceProcessingUnitImpl
   * @fn  FaceProcessingUnitImpl(const FaceProcessingUnit& other) = delete
   * @brief Copy constructor
   * @param[in] other Object to copy from
   */
  FaceProcessingUnitImpl(const FaceProcessingUnitImpl &other) = delete;

  /**
   * @name  operator=
   * @fn  FaceProcessingUnitImpl& operator=(const FaceProcessingUnit& rhs) = delete
   * @brief Assignment operator
   * @param[in] rhs Object to assign from
   * @return  Assigned object
   */
  FaceProcessingUnitImpl &operator=(const FaceProcessingUnitImpl &rhs) = delete;

  /**
   * @name  Load
   * @fn  int Load(const std::string& config_file)
   * @brief Load processing modules
   * @param[in] config_file Path to the configuration file
   * @return -1 if error, 0 otherwise
   */
  int Load(const std::string &config_file);

#pragma mark -
#pragma mark Usage

  /**
   * @name  Process
   * @fn  int Process(const LTS5::StreamData *data)
   * @brief Run all algorithms. Start with acquisition then call each process.
   * @param[in] data    Image data for this frame
   * @return -1 if error, 0 otherwise
   */
  int Process(const LTS5::StreamData *data);

#pragma mark -
#pragma mark Callbacks

  /**
   * @name  Read
   * @fn  int Read(std::vector<cv::Mat>* buffer);
   * @brief Load image into a given \p buffer
   * @param[out]  buffer  Buffer where image are loaded
   * @return -1 if error, 0 otherwise
   */
  int Read(std::vector<cv::Mat> *buffer);

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_frontal_face_landmark
   * @fn  const cv::Mat& get_frontal_face_landmark() const
   * @brief Provide the output of the face tracker for the frontal image
   * @return  Landmarks on frontal image
   */
  const cv::Mat &get_frontal_face_landmark() const {
    return frontal_shape_;
  }

  /**
   * @name  get_face_landmarks
   * @fn  const std::vector<cv::Mat>& get_face_landmarks() const
   * @brief Provide the output of the face tracker for each image
   * @return  Sets of Landmarks on each images, can be empty
   */
  const std::vector<cv::Mat> &get_face_landmarks() const {
    return face_reconstruction_->get_landmarks();
  }

  /**
   * @name  get_head_pose
   * @fn  const std::vector<HeadPose>& get_head_pose()
   * @brief Provide the estimation of the head pose in 3D
   * @return  3D Head pose for each view
   */
  const std::vector<HeadPose> &get_head_pose();

  /**
   * @name  get_frontal_image
   * @fn  const cv::Mat& get_frontal_image() const
   * @brief Provide the normalized frontal image
   * @return  Synthetic frontal image
   */
  const cv::Mat &get_frontal_image() const {
    return frontal_image_;
  }

  /**
   * @name  get_au_prediction
   * @fn  const std::vector<double>& get_au_prediction() const;
   * @brief Provide action unit detection results
   * @return  Vector of classifier's output.
   */
  const std::vector<double> &get_au_prediction() const;

  /**
   * @name  get_expression_prediction
   * @fn  const std::vector<double>& get_expression_prediction() const;
   * @brief Provide exppression detection results
   * @return  Vector of classifier's output. Higher score determine the finale
   *          expression
   */
  const std::vector<double> &get_expression_prediction() const {
    return exp_score_;
  }

  /**
   * @name  get_expression_type
   * @fn  const ExpressionType& get_expression_type() const
   * @brief Provide label for detected expression
   * @return  Detected expression
   */
  const ExpressionType &get_expression_type() const {
    return exp_;
  }

  /**
   * @name  get_expression_label
   * @fn  void get_expression_label(std::string* label) const
   * @brief Provide detected expression's label
   * @param[out]  label   Last detected expression label
   */
  void get_expression_label(std::string *label) const {
    if (exp_ == ExpressionType::kUndefined) {
      *label = "Undefined";
    } else {
      *label = exp_label_[exp_];
    }
  }

#pragma mark -
#pragma mark Private
private:

  /** Pipeline ready flag */
  bool is_ready_;
  /** Image data */
  const LTS5::StreamData* image_data_;

  /** 3D Configuration file */
  std::string face_reconstruction_config_;
  /** 3D Reconstruction */
  LTS5::FaceSynthesisPipeline* face_reconstruction_;
  /** 3D Pipeline configuration */
  LTS5::FaceSynthesisPipeline::FaceSynthesisPipelineParameters pipeline_param_;
  /** 3D Processing configuration */
  LTS5::FaceSynthesisPipeline::PipelineProcessConfiguration process_pipeline_config_;
  /** Frontal image */
  cv::Mat frontal_image_;
  /** Head pose */
  std::vector<HeadPose> head_pose_;

  /** Face Tracker */
  LTS5::SdmTracker *face_tracker_;
  /** Landmarks on frontal image */
  cv::Mat frontal_shape_;
  /** Frontal working images */
  cv::Mat frontal_working_image_;

  /** Expression detection */
  LTS5::ExpressionAnalysis *exp_analyzer_;
  /** Expression label */
  std::vector<std::string> exp_label_;
  /** Expression score */
  std::vector<double> exp_score_;
  /** Expression detected */
  ExpressionType exp_;
};

#pragma mark -
#pragma mark Initialization

/*
 * @name  FaceProcessingUnitMtImpl
 * @fn    FaceProcessingUnitMtImpl()
 * @brief Constructor
 */
FaceProcessingUnit::FaceProcessingUnitImpl::FaceProcessingUnitImpl() : is_ready_(false),
                                                                           face_reconstruction_(nullptr),
                                                                           face_tracker_(nullptr),
                                                                           exp_analyzer_(nullptr) {
}

/*
 * @name  FaceProcessingUnitMtImpl
 * @fn  FaceProcessingUnitMtImpl()
 * @brief Destructor
 */
FaceProcessingUnit::FaceProcessingUnitImpl::~FaceProcessingUnitImpl() {
  // 3D
  if (face_reconstruction_) {
    delete face_reconstruction_;
    face_reconstruction_ = nullptr;
  }
  // Face tracker
  if (face_tracker_) {
    delete face_tracker_;
    face_tracker_ = nullptr;
  }
  // Expression analysis
  if (exp_analyzer_) {
    delete exp_analyzer_;
    exp_analyzer_ = nullptr;
  }
}

/*
 * @name  Load
 * @fn  int Load(const std::string& config_file)
 * @brief Load processing modules
 * @param[in] config_file Path to the configuration file
 * @return -1 if error, 0 otherwise
 */
int FaceProcessingUnit::FaceProcessingUnitImpl::Load(const std::string &config_file) {
  int err = -1;
  // Open config file
  cv::FileStorage storage(config_file, cv::FileStorage::READ);
  if (storage.isOpened()) {
    std::string sdm_model_path;
    std::string f_detect_path;
    std::string exp_model_path;
    double tracker_thresh = -0.3;
    int start_pass = 0;
    cv::FileNode root = storage.getFirstTopLevelNode();
    // Parse config
    std::string path;
    LTS5::ReadNode(root,
                   "rec_config_path",
                   &pipeline_param_.pipeline_xml_config_file);
    LTS5::ReadNode(root, "sdm_model_path", &sdm_model_path);
    LTS5::ReadNode(root, "face_detect_model_path", &f_detect_path);
    LTS5::ReadNode(root, "expression_model_path", &exp_model_path);
    LTS5::ReadNode(root, "tracker_thresh", &tracker_thresh);
    LTS5::ReadNode(root, "tracker_start_pass", &start_pass);
    // Instantiate element
    process_pipeline_config_.has_synthetic_data = false;
    process_pipeline_config_.draw_landmarks = false;
    try {
      // Init face reconstruction
      if (!face_reconstruction_) {
        face_reconstruction_ = new LTS5::FaceSynthesisPipeline(pipeline_param_);
      }
      // Head pose struct
      int n_view = face_reconstruction_->get_number_of_view();
      head_pose_.resize(static_cast<size_t>(n_view));
      // Data loading
      face_reconstruction_->set_external_input_callback(this);
    } catch (LTS5::ProcessError &e) {
      LTS5_LOG_ERROR(e.what());
      delete face_reconstruction_;
      return -1;
    }
    // Face tracker
    if (!face_tracker_) {
      face_tracker_ = new LTS5::SdmTracker();
    }
    err = face_tracker_->Load(sdm_model_path, f_detect_path);
    face_tracker_->set_score_threshold(tracker_thresh);
    auto tracker_param = face_tracker_->get_tracker_parameters();
    tracker_param.starting_pass = start_pass;
    face_tracker_->set_tracker_parameters(tracker_param);
    if (!err) {
      // Expression analysis
      exp_analyzer_ = new LTS5::ExpressionAnalysis();
      err = exp_analyzer_->Load(exp_model_path);
      is_ready_ = (err == 0);
      if (!err) {
        // Grab labels
        exp_analyzer_->get_expression_labels(&exp_label_);
      } else {
        LTS5_LOG_ERROR("Can not load expression analyzer");
      }
    } else {
      LTS5_LOG_ERROR("Can not load face tracker");
    }
  } else {
    LTS5_LOG_ERROR("Can not open file : " << config_file);
  }
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Process
 * @fn  void Process(const LTS5::StreamData* data)
 * @brief Run all algorithms. Start with acquisition then call each process.
 * @param[in] image_grabber Object to collect image
 * @return -1 if error, 0 otherwise
 */
int FaceProcessingUnit::FaceProcessingUnitImpl::Process(const LTS5::StreamData* data) {
  // Set image data for this frame
  image_data_ = data;
  // Call processing flow
  int err = -1;
  if (is_ready_) {
    // Call reconstruction
    err = face_reconstruction_->Process(process_pipeline_config_);
    if (!err) {
      // Grab + convert
      frontal_image_ = face_reconstruction_->get_frontal_image();
      if (frontal_image_.channels() != 1) {
        cv::cvtColor(frontal_image_, frontal_working_image_, CV_BGR2GRAY);
      } else {
        frontal_working_image_ = frontal_image_;
      }
      // Track
      face_tracker_->Track(frontal_working_image_, &frontal_shape_);
      if (!frontal_shape_.empty()) {
        // Process expression
        exp_analyzer_->Predict(frontal_working_image_,
                               frontal_shape_,
                               &exp_score_);
        // Find maximum
        exp_ = static_cast<ExpressionType>(std::distance(exp_score_.begin(),
                                                         std::max_element(exp_score_.begin(),
                                                                          exp_score_.end())));
        err = 0;
      } else {
        err = -1;
        exp_ = ExpressionType::kUndefined;
        LTS5_LOG_WARNING("can not track on frontal image");
      }
    } else {
      err = -2;
      exp_ = ExpressionType::kUndefined;
      LTS5_LOG_WARNING("Can not synthesize frontal image");
    }
  }
  return err;
}

#pragma mark -
#pragma mark Callbacks

/*
 * @name  Read
 * @fn  int Read(std::vector<cv::Mat>* buffer);
 * @brief Load image into a given \p buffer
 * @param[out]  buffer  Buffer where image are loaded
 * @return -1 if error, 0 otherwise
 */
int FaceProcessingUnit::FaceProcessingUnitImpl::Read(std::vector<cv::Mat> *buffer) {
  *buffer = image_data_->get_buffer();
  return 0;
}

#pragma mark -
#pragma mark Accessors

/*
 * @name  get_head_pose
 * @fn  const std::vector<HeadPose>& get_head_pose() const
 * @brief Provide the estimation of the head pose in 3D
 * @return  3D Head pose for each view
 */
const std::vector<FaceProcessingUnit::FaceProcessingUnitImpl::HeadPose> &
FaceProcessingUnit::FaceProcessingUnitImpl::get_head_pose() {
  auto &trsfrm = face_reconstruction_->get_pose();
  int n_view = trsfrm.number_of_view;
  for (int i = 0; i < n_view; ++i) {
    cv::Mat rot = trsfrm.rotation[i];
    HeadPose &pose = head_pose_[i];
    if (trsfrm.translation[i].at<float>(2) != -1.f) {
      LTS5::Utilities::GetRotationAngle(rot, &pose.gamma, &pose.theta, &pose.phi);
    } else {
      pose.gamma = -1.f;
      pose.theta = -1.f;
      pose.phi = -1.f;
    }
  }
  return head_pose_;
}

#pragma mark -
#pragma mark Initialization


/*
 * @name  FaceProcessingUnitMt
 * @fn    FaceProcessingUnitMt(int nb_cores, const std::string config_file)
 * @brief Constructor
 */
FaceProcessingUnit::FaceProcessingUnit(int nb_cores,
                                       const std::string& config_file) {
  nb_cores_ = nb_cores;         // nb of available cores
  config_file_ = config_file;
  expected_frame_id_ = 0;
  // Allocate container of correct size
  unit_.resize(nb_cores);
  is_processing_.resize(nb_cores);
  th_grp_.resize(nb_cores);
  // Init
  for (int i = 0; i < nb_cores; ++i) {
    unit_[i] = new FaceProcessingUnitImpl();
    unit_[i]->Load(config_file_);
  }
}

/*
 * @name  FaceProcessingUnit
 * @fn  FaceProcessingUnit()
 * @brief Destructor
 */
FaceProcessingUnit::~FaceProcessingUnit() {
  // Release unit
  for (int i = 0; i < nb_cores_; ++i) {
    // Wait till thread is done
    th_grp_[i].join();
    // dtor
    delete unit_[i];
  }
}

#pragma mark -
#pragma mark Usage

/*
 * @name  SolveCore
 * @fn  void SolveCore(int my_id)
 * @brief Do the computation for one core
 * @param[in] myID the ID of the core
 */
void FaceProcessingUnit::SolveCore(const int my_id) {
  is_processing_[my_id] = true;
  LTS5::StreamData *current_data = (loader_->get_frame_buffer_())[my_id]->Get();
  while (current_data != nullptr) {
    int err = unit_[my_id]->Process(current_data);
    auto *result_data = new ResultData();
    result_data->set_frame_id(current_data->get_frame_id());
    result_data->set_timestamp(current_data->get_timestamp());
    result_data->set_error_code(err);
    // Set data to ResultData output container
    switch(err) {
      case 0 :
        result_data->set_exp_score(unit_[my_id]->get_expression_prediction());
        result_data->set_frontal_landmark(unit_[my_id]->get_frontal_face_landmark().clone());
      case -1:
        result_data->set_front_image(unit_[my_id]->get_frontal_image().clone());
        break;

      default:
        break;
    }
    // Push result to queue.
    {
      std::lock_guard<std::mutex> lock(this->pqueue_mutex_);
      priority_queue_.push(result_data);
    }
    current_data = (loader_->get_frame_buffer_())[my_id]->Get();
  }
  is_processing_[my_id] = false;
  //printf("Core:%2d terminates (size=%d)\n", my_id, (loader_->get_frame_buffer_())[my_id]->get_size());
}

/*
* @name  Process
* @fn  void Process()
* @brief Launch the computation on all cores specified by this.nb_cores
* @param[in] nb_frames the ID of the core
*/
void FaceProcessingUnit::Process() {
  for (int i = 0; i < nb_cores_; i++) {
    th_grp_[i]= std::thread(std::bind(&FaceProcessingUnit::SolveCore, this, i));
  }
}

/*
 * @name  GetNextFrame
 * @fn  LTS5::ResultData* GetNextFrame()
 * @brief Return the next frame
 * @result the next image contained in a ResultData container or NULL if empty
 */
LTS5::ResultData *FaceProcessingUnit::GetNextFrame() {
  LTS5::ResultData *result_data = nullptr;
  std::lock_guard<std::mutex> lock(this->pqueue_mutex_);
  if (!priority_queue_.empty()) {
    auto top_result = priority_queue_.top();
    if (top_result->get_frame_id() == expected_frame_id_) {
      priority_queue_.pop();
      ++expected_frame_id_;
      result_data = top_result;
    }
  }
  return result_data;
}

/*
 * @name  WaitProcessEnded
 * @fn  void WaitProcessEnded()
 * @brief Wait until all processing has been done. This is useful when there
 * is a max number of frames.
 */
void FaceProcessingUnit::WaitProcessEnded() {
  for (auto& thd : th_grp_) {
    thd.join();
  }
}

/*
 * @name  is_processing
 * @fn  bool is_processing()
 * @brief Return the status of processing
 * @result a boolean telling if something is still processing
 */
bool FaceProcessingUnit::is_processing() {
  bool status = false;
  for (int i = 0; i < nb_cores_; i++) {
    status |= is_processing_[i];
  }
  return status;
}
}  // namespace LTS5
