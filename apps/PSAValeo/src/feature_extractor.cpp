/**
 *  @file   feature_extractor.cpp
 *  @brief  Extract features from a given stream (Images / video)
 *  @ingroup    apps
 *
 *  @author Christophe Ecabert
 *  @date   20/11/2017
 *  Copyright (c) 2017 Christophe Ecabert. All rights reserved.
 */

#include <string>

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/logger.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/facial_analysis/expression_analysis.hpp"

int main(int argc, const char** argv) {
  LTS5::CmdLineParser parser;
  // Input
  parser.AddArgument("-i",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Input stream (Image folder, video)");
  // Tracker
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Face tracker \"tracker;fdetector\"");
  // Image normalization
  parser.AddArgument("-e",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Expression model");
  // Output location
  parser.AddArgument("-o",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Output folder");
  // Threshold
  parser.AddArgument("-thr",
                     LTS5::CmdLineParser::ArgState::kOptional,
                     "Face tracker threshold");

  int err = parser.ParseCmdLine(argc, argv);
  if (err == 0) {
    std::string input_path, tracker_path, expression_path, output, threshold;
    parser.HasArgument("-i", &input_path);
    parser.HasArgument("-t", &tracker_path);
    parser.HasArgument("-e", &expression_path);
    parser.HasArgument("-o", &output);
    parser.HasArgument("-thr", &threshold);

    // Scan for images
    std::vector<std::string> images =  LTS5::ScanImagesInFolder(input_path,
                                                                true);
    if (!images.empty()) {
      std::vector<std::string> tracker_model;
      LTS5::SplitString(tracker_path, ";", &tracker_model);

      auto* tracker = new LTS5::SdmTracker();
      err = tracker->Load(tracker_model[0], tracker_model[1]);
      if (!threshold.empty()) {
        tracker->set_score_threshold(std::atof(threshold.c_str()));
      }
      auto* exp_detector = new LTS5::ExpressionAnalysis();
      err |= exp_detector->Load(expression_path);

      if (err == 0) {
        // Open files for landmarks and features
        std::ofstream feat_stream(output + "features.bin",
                                  std::ios_base::binary);
        std::ofstream lms_stream(output + "landmarks.bin",
                                 std::ios_base::binary);
        if (feat_stream.is_open() && lms_stream.is_open()) {
          LTS5_LOG_INFO("Start feature extraction, may take a while ...");
          // Iterate overall images
          cv::Mat img, gray, shape, shape32f;
          std::vector<double> score;
          std::string n_img = "/" + std::to_string(images.size() + 1);
          for(size_t i = 0; i < images.size(); ++i) {
            // "Show" progress
            if ((i % 100) == 0) {
              std::string msg = std::to_string(i + 1) + n_img;
              msg += " features extracted ...";
              LTS5_LOG_INFO(msg);
            }
            // open images
            img = cv::imread(images[i]);
            // Convert to gray
            if (img.channels() != 1) {
              cv::cvtColor(img, gray, CV_BGR2GRAY);
            } else {
              gray = img;
            }
            // Track
            tracker->Track(gray, &shape);
            int e = 0;
            if (!shape.empty()) {
              // Process - expression
              exp_detector->Predict(gray, shape, &score);
              const cv::Mat& features = exp_detector->get_features();
              // Dump stuff
              shape.convertTo(shape32f, CV_32FC1);
              e = LTS5::WriteMatToBin(feat_stream, features);
              e |= LTS5::WriteMatToBin(lms_stream, shape32f);
            } else {
              // No landmarks
              cv::Mat dummy(1, 1, CV_32FC1);
              e = LTS5::WriteMatToBin(feat_stream, dummy);
              e |= LTS5::WriteMatToBin(lms_stream, dummy);
            }
            if (e != 0) {
              LTS5_LOG_WARNING("Error while dumpy features/landmarks");
            }
          }
          err = 0;
        } else {
          LTS5_LOG_ERROR("Can not create destination files");
          err = -1;
        }
      } else {
        LTS5_LOG_ERROR("Can not load tracker/expression detector");
      }
      // Clean up
      delete tracker;
      delete exp_detector;
    } else {
      LTS5_LOG_ERROR("No images found in folder: " << input_path);
      err = -1;
    }
  } else {
    LTS5_LOG_ERROR("Unable to parse command line");
  }
  return err;
}
