/**
 *  @file   video_data_loader.cpp
 *  @brief  Load image into StreamData object from video file
 *
 *  @author Christophe Ecabert
 *  @date   02/08/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <thread>

#include "opencv2/imgproc.hpp"

#include "lts5/utils/process_error.hpp"
#include "lts5/utils/file_io.hpp"

#include "video_data_loader.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/*
 * @name  VideoDataLoader
 * @fn    VideoDataLoader(int nb_queues, int fifo_size)
 * @brief Constructor
 * @param[in] nb_queues the number of queues to create (depends of number of threads)
 * @param[in] buffer_size size of the buffer
 */
VideoDataLoader::VideoDataLoader(int nb_queues,
                                 int buffer_size) : BaseDataLoader(nb_queues,
                                                                   buffer_size){
}

/*
 * @name  ~VideoDataLoader
 * @fn    ~VideoDataLoader()
 * @brief Destructor
 */
VideoDataLoader::~VideoDataLoader() {
  // Stop
  this->Stop();
  thread_.join();
  // Release video loader
  if (loader_.size() > 0) {
    for (auto &loader : loader_) {
      loader->release();
      delete loader;
    }
  }
  // destroy the frames pool
  for (int i = 0; i < nb_queues_; i++) {
    if (frame_buffer_[i] != nullptr) {
      delete frame_buffer_[i];
    }
  }
}

/*
 * @name  Init
 * @fn    int Init(const std::string& folder)
 * @brief Initialize data loader
 * @param folder  Folder where videos are stored
 * @return -1 if error, 0 otherwise
 */
int VideoDataLoader::Init(const std::string &folder) {
  int err = -1;
  // Scan given folder
  std::vector<std::string> files;
  LTS5::GetFileListing(files, folder, "", ".avi");
  if (!files.empty()) {
    // Open video(s)
    err = 0;
    for (int i = 0; i < files.size(); ++i) {
      loader_.push_back(new cv::VideoCapture(files[i]));
      err |= loader_[i]->isOpened() ? 0 : -1;
    }
  }
  return err;
}

#pragma mark -
#pragma mark Usage

/*
 * @name  Start
 * @fn    void Start(int max_frames)
 * @brief Start the data acquisition and automatically stop after
 *        max_frames frames
 * @param[in] max_frames the max number of frames to acquire
 */
void VideoDataLoader::Start(int max_frames) {
  max_frames_ = max_frames;
  this->Start();
}

/*
 * @name  Start
 * @fn    void Start()
 * @brief Start the data acquisition
 */
void VideoDataLoader::Start() {
  // Move operator
  thread_ = std::thread(std::bind(&VideoDataLoader::LoadingLoop, this));
}

/*
 * @name  Stop
 * @fn    void Stop()
 * @brief Stop the data acquisition
 */
void VideoDataLoader::Stop() {
  is_data_loading_ = false;
  for (int i = 0; i < nb_queues_; i++) {
    frame_buffer_[i]->Put(nullptr);
  }
}

#pragma mark -
#pragma mark Usage

/*
 * @name  GetData
 * @fn    LTS5::StreamData* GetData()
 * @brief Load one frame from multiple videos into one StreamData object
 * @return    Pointer to image data
 */
StreamData* VideoDataLoader::GetData() {
  // Loop over all videos
  std::vector<cv::Mat> data(loader_.size());
  for (int i = 0; i < loader_.size(); ++i) {
    cv::Mat frame;
    loader_[i]->read(frame);
    if(frame.channels() != 1) {
      cv::cvtColor(frame, data[i], CV_BGR2GRAY);
    } else {
      frame.copyTo(data[i]);
    }
  }
  return new LTS5::StreamData(data, this->frame_id_++);
}

/*
 * @name  LoadingLoop
 * @fn    void LoadingLoop()
 * @brief The loop doing the data acquisition and storing it in the buffer
 */
void VideoDataLoader::LoadingLoop() {
  is_data_loading_ = true;
  if(max_frames_ > 0) {
    int frames_processed = 0;
    // we only treat max_frames or stop if Stop() called before
    while (is_data_loading_ && frames_processed < max_frames_) {
      for (int j = 0; j < nb_queues_; j++) {
        frame_buffer_[j]->Put(this->GetData());
      }
      frames_processed += nb_queues_;
    }
    // max_frames processed, tell compute process to stop
    this->Stop();
  } else {
    // treat input frames until Stop() called
    while (is_data_loading_) {
      for (int j = 0; j < nb_queues_; j++) {
        frame_buffer_[j]->Put(this->GetData());
      }
    }
  }
}


}  // namespace LTS5