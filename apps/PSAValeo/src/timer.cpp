/**
 *  @file   timer.cpp
 *  @brief  Measure time
 *
 *  @author Pierre Kuonen
 *  @date   01/09/2016
 *  Copyright (c) 2016 Pierre Kuonen. All rights reserved.
 */

#include "timer.hpp"
#include <sys/time.h>
#include <stdio.h>

int debuglevel=2;

/*
 * @name  Timer
 * @fn    Timer(void)
 * @brief Default Constructor
 */
Timer::Timer()
{
  is_running_=false;
  start_point_=0;
  elapsed_=0;
}

/*
 * @name  Start
 * @fn    Start(void)
 * @brief Start the timer
 */
void Timer::Start()
{
  if (!is_running_)
    {
      is_running_=true;
      start_point_=GetCurrentTime();
    }
}

/*
 * @name  Stop
 * @fn    Stop(void)
 * @brief Stop the timer
 */
void Timer::Stop()
{
  if (is_running_)
    {
      elapsed_+=GetCurrentTime()-start_point_;
      is_running_=false;
    }
}

/*
 * @name  Reset
 * @fn    Reset(void)
 * @brief Reset the timer
 */
void Timer::Reset()
{
  start_point_=GetCurrentTime();
  elapsed_=0;
}

/*
 * @name  Elapsed
 * @fn    double Elapsed(void)
 * @brief Get the elapsed_ time since the very first Start() was called
 */
double Timer::Elapsed()
{
  if (is_running_)
    {
      return elapsed_+GetCurrentTime()-start_point_;
    }
  return elapsed_;
}

/*
 * @name  GetCurrentTime
 * @fn    double GetCurrentTime(void)
 * @brief Get the current time in seconds
 */
double Timer::GetCurrentTime()
{
  timeval tp;
  gettimeofday(&tp,NULL);
  double time_seconds = tp.tv_sec+double(tp.tv_usec)*1.0E-6;
  return time_seconds;
}
