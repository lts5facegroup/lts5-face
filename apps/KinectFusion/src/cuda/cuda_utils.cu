/**
 *  @file   cuda_utils.cu
 *  @brief
 *
 *  @author Gabriel Cuendet
 *  @date   22/07/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <kfusion/cuda/kernel_containers.hpp>
#include <kfusion/types.hpp>
#include <vector_types.h>
#include <cstdio>
#include "cuda.h"
#include "cuda_utils.hpp"

namespace kfdevice {
struct Converter {
  const float4* input_data;
  int n_pts;

  __device__ __forceinline__
  void operator()(kfusion::cuda::PtrSz<float3> output) const {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx < n_pts) {
      float4 p4 = *(const float4 *) (input_data + idx);
      float3 p3 = make_float3(p4.x, p4.y, p4.z);

      float3 *out_data = output.data;
      out_data[idx] = p3;
    }
  }
};

__global__ void convertFloat_kernel (const Converter converter, kfusion::device::PtrSz<float3> output) {
  converter(output);
};
}  // namespace kfdevice

void kfdevice::ConvertFloat4to3(const kfusion::cuda::PtrSz<float4>& input, kfusion::cuda::PtrSz<float3>& output) {
  const int block = 256;

  kfdevice::Converter cf;
  cf.n_pts = input.size;
  cf.input_data = input.data;

  int divUp = (input.size + block - 1) / block;
  convertFloat_kernel<<<divUp, block>>>(cf, output);
  cudaSafeCall ( cudaGetLastError () );
  cudaSafeCall (cudaDeviceSynchronize ());
}
/*
namespace kfdevice {

__device__ __forceinline__ float dot(const float3& v1, const float3& v2) {
  return __fmaf_rn(v1.x, v2.x, __fmaf_rn(v1.y, v2.y, v1.z*v2.z));
}

__device__ __forceinline__ float3 operator*(const float3& v1, const float3& v2) {
  return make_float3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}

__device__ __forceinline__ float3 operator*(const float3& v1, const float& v) {
  return make_float3(v1.x * v, v1.y * v, v1.z * v);
}

__device__ __forceinline__ float3 operator-(const float3& v1, const float3& v2) {
  return make_float3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

struct Aligner {
  const ushort* input_data;
  int n_pts;
  int2 im_size;
  Mat3f K_ir_R_ir_Inv;
  Mat3f K_rgb;
  float3 C_ir;

  __device__ __forceinline__
  void operator()(kfusion::cuda::PtrStepSz<ushort> output) const {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx < n_pts) {

      // For each input pixel
      float u_ir = idx % im_size.x;
      float v_ir = idx / im_size.x;

      float3 uvw_ir = make_float3(u_ir, v_ir, 1.f);

      // Backproject to world
      //cv::Mat xyz = K_ir_R_ir_Inv * uvw_ir;
      float a = dot(K_ir_R_ir_Inv.data[0], uvw_ir);
      float b = dot(K_ir_R_ir_Inv.data[1], uvw_ir);
      float c = dot(K_ir_R_ir_Inv.data[2], uvw_ir);
      float3 xyz = make_float3(a, b, c);

      // Reproject on RGB camera
      //cv::Mat uvw_rgb = K_rgb * (xyz * static_cast<float>(input_data[idx]) - C_ir);
      float3 uvw_rgb = make_float3(dot(K_rgb.data[0], xyz * static_cast<float>(input_data[idx]) - C_ir),
                                   dot(K_rgb.data[1], xyz * static_cast<float>(input_data[idx]) - C_ir),
                                   dot(K_rgb.data[2], xyz * static_cast<float>(input_data[idx]) - C_ir));

      // Fill in the new depth frame
      int u_rgb = (int)round(uvw_rgb.x / uvw_rgb.z);
      int v_rgb = (int)round(uvw_rgb.y / uvw_rgb.z);
      if (u_rgb < 0 || u_rgb > im_size.x - 1) {
        return;
      }
      if (v_rgb < 0 || v_rgb > im_size.y - 1) {
        return;
      }
      unsigned short new_depth_val = static_cast<unsigned short>(round(uvw_rgb.z));

      // STORE: CAUTION!!!
      if (new_depth_val > 0) {
        // Check that new depth is smaller than current depth (not masked)
        if (output(v_rgb, u_rgb) > 0) {
          if (new_depth_val < output(v_rgb, u_rgb))
            output(v_rgb, u_rgb) = new_depth_val;
        } else {
          output(v_rgb, u_rgb) = new_depth_val;
        }
      }
    }
  }
};

__global__ void alignDepthRgb_kernel (const Aligner align, kfusion::device::PtrStepSz<ushort> output) {
  align(output);
};
} // namespace kfdevice

void kfdevice::AlignDepthRgb(const kfusion::cuda::PtrStepSz<ushort>& input, const Mat3f& K_ir_R_ir_Inv, const Mat3f& K_rgb, const float3& C_ir, kfusion::cuda::PtrStepSz<ushort>& output) {
  const int block = 256;

  kfdevice::Aligner al;
  al.n_pts = input.cols * input.rows;
  al.input_data = input.data;
  al.im_size = make_int2(input.cols, input.rows);
  al.K_ir_R_ir_Inv = K_ir_R_ir_Inv;
  al.K_rgb = K_rgb;
  al.C_ir = C_ir;

  // DEBUG PURPOSE
  //printf("C_ir = \n %f, %f, %f \n", C_ir.x, C_ir.y, C_ir.z);

  // DEBUG PURPOSE
  //printf("K_ir_R_ir_Inv = \n %f, %f, %f \n", K_ir_R_ir_Inv.data[0].x, K_ir_R_ir_Inv.data[0].y, K_ir_R_ir_Inv.data[0].z);
  //printf("%f, %f, %f \n", K_ir_R_ir_Inv.data[1].x, K_ir_R_ir_Inv.data[1].y, K_ir_R_ir_Inv.data[1].z);
  //printf("%f, %f, %f \n", K_ir_R_ir_Inv.data[2].x, K_ir_R_ir_Inv.data[2].y, K_ir_R_ir_Inv.data[2].z);

  // DEBUG PURPOSE
  //printf("K_rgb = \n %f, %f, %f \n", K_rgb.data[0].x, K_rgb.data[0].y, K_rgb.data[0].z);
  //printf("%f, %f, %f \n", K_rgb.data[1].x, K_rgb.data[1].y, K_rgb.data[1].z);
  //printf("%f, %f, %f \n", K_rgb.data[2].x, K_rgb.data[2].y, K_rgb.data[2].z);

  int divUp = (input.cols * input.rows + block - 1) / block;
  alignDepthRgb_kernel<<<divUp, block>>>(al, output);
  cudaSafeCall ( cudaGetLastError () );
  cudaSafeCall (cudaDeviceSynchronize ());

}
 */