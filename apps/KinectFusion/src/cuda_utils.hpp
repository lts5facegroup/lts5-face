/**
 *  @file   cuda_utils.hpp
 *  @brief
 *
 *  @author Gabriel Cuendet
 *  @date   22/07/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef __CUDA_UTILS_HPP__
#define __CUDA_UTILS_HPP__

#include "cuda.h"
#include "vector_types.h"
#include "cuda_runtime.h"
#include <iostream>
#include "kfusion/cuda/kernel_containers.hpp"


#if defined(__GNUC__)
#define cudaSafeCall(expr)  ___cudaSafeCall(expr, __FILE__, __LINE__, __func__)
#else /* defined(__CUDACC__) || defined(__MSVC__) */
#define cudaSafeCall(expr)  ___cudaSafeCall(expr, __FILE__, __LINE__)
#endif

static inline void ___cudaSafeCall(cudaError_t err, const char *file, const int line, const char *func = "") {
  if (cudaSuccess != err) {
    std::cout << "Cuda error: " << cudaGetErrorString(err) << "\t" << file << ":" << line << std::endl;
    exit(0);
  }
}

typedef int3   Vec3i;
typedef float3 Vec3f;
struct Mat3f { float3 data[3]; };
struct Aff3f { Mat3f R; Vec3f t; };

namespace kfdevice {
void ConvertFloat4to3(const kfusion::cuda::PtrSz<float4> &input, kfusion::cuda::PtrSz<float3> &output);

/*
void AlignDepthRgb(const kfusion::cuda::PtrStepSz<ushort>& input, const Mat3f& K_ir_R_ir_Inv, const Mat3f& K_rgb, const float3& C_ir, kfusion::cuda::PtrStepSz<ushort>& output);
 */
}

#endif // __CUDA_UTILS_HPP__
