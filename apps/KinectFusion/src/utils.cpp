//
// Created by gabriel on 7/27/16.
//

#include <iostream>
#include "utils.hpp"
#include "cuda_utils.hpp"
#include "opencv2/core.hpp"

/*
 * @name ConvertFloat4to3
 * @fn void ConvertFloat4to3(const kfusion::cuda::DeviceArray<kfusion::Point>& input,
 *                           kfusion::cuda::DeviceArray<float3>& output)
 * @brief Convert a device array of float4 to a device array of float3 (typically for color conversion from RGBA to RGB)
 * @param input the float4 device array to convert from
 * @param output the float3 device array to convert to
 */
void ConvertFloat4to3(const kfusion::cuda::DeviceArray<kfusion::Point>& input,
                      kfusion::cuda::DeviceArray<float3>& output) {
  if (input.size() != output.size() || output.size() == 0)
    output.create(input.size());

  kfusion::cuda::DeviceArray<float4>& in_ = (kfusion::cuda::DeviceArray<float4>&) input;

  //kfusion::cuda::PtrSz<kfusion::Point> in_(input.ptr(), input.size());
  kfusion::cuda::PtrSz<float3> out_(output.ptr(), output.size());

  kfdevice::ConvertFloat4to3(in_, out_);
}

/*
 * @name AlignDepthRgb
 * @fn void AlignDepthRgb(kfusion::cuda::Depth* depth_device
 * @brief Align the depth map onto the color image on GPU in order to integrate colors in the reconstruction, in case
 *        the data from the Kinect are not already aligned (which should be the case:
 *        Use GetAlternativeViewPointCap().SetViewPoint())
 * @param K_rgb Intrinsics of the RGB camera
 * @param K_ir Intrinsics of the depth camera
 * @param transform_ir Affine transformation [R|t] between the RGB and the depth camera
 * @param depth_device the deivce array containing depth map to align
 *
void AlignDepthRgb(const kfusion::Intr& K_rgb, const kfusion::Intr& K_ir, const kfusion::Affine3f& transform_ir, kfusion::cuda::Depth* depth_device) {
  // Create and allocate storage for the temporary aligned version of depth_device!
  kfusion::cuda::Depth aligned_device;
  aligned_device.create(depth_device->rows(), depth_device->cols());

  // Process
  //kfusion::cuda::DeviceArray2D<ushort>& in_ = (kfusion::cuda::DeviceArray2D<ushort>&) *depth_device;
  kfusion::cuda::PtrStepSz<ushort> out_(aligned_device.rows(), aligned_device.cols(), reinterpret_cast<ushort*>(aligned_device.ptr()), 2);

  float3 C_ir = *reinterpret_cast<const float3*>(transform_ir.translation().val);

  Mat3f K_ir_R_ir_Inv_device; // = *reinterpret_cast<const Mat3f*>(&K_ir_R_ir_Inv);
  Mat3f K_rgb_device; // = *reinterpret_cast<const Mat3f*>((cv::Mat*)&K_rgb_mat);

  K_ir_R_ir_Inv_device.data[0] = make_float3(1/K_ir.fx, 0.f, 0.f);
  K_ir_R_ir_Inv_device.data[1] = make_float3(0.f, 1/K_ir.fy, 0.f);
  K_ir_R_ir_Inv_device.data[2] = make_float3(-K_ir.cx/K_ir.fx, -K_ir.cy/K_ir.fy, 1.f);

  K_rgb_device.data[0] = make_float3(K_rgb.fx, 0.f, K_rgb.cx);
  K_rgb_device.data[1] = make_float3(0.f, K_rgb.fy, K_rgb.cy);
  K_rgb_device.data[2] = make_float3(0.f, 0.f, 1.f);

  kfdevice::AlignDepthRgb(*depth_device, K_ir_R_ir_Inv_device, K_rgb_device, C_ir, out_);

  // And then swap!
  aligned_device.copyTo(*depth_device);
  //depth_device->swap(aligned_device);
}
*/