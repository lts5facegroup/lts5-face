/**
 *  @file   kinect_fusion.cpp
 *  @brief  KinectFusion app implementation
 *
 *  @author Gabriel Cuendet
 *  @date   14/07/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <iostream>
#include <sstream>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/lts5_config.h"
#include "kinect_fusion.hpp"

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)


int WriteConfig(const std::string& config_path,
                const kfusion::KinFuParams& kf_params) {
  int err = 0;

  cv::FileStorage storage(config_path, cv::FileStorage::WRITE);
  if (!storage.isOpened()) {
    std::cout << "[WriteConfig] ERROR: Unable to open configuration file" <<
    std::endl;
    return -1;
  }

  storage << "kfusion_params" << "{";
  storage << "cols" << kf_params.cols;
  storage << "rows" << kf_params.rows;
  storage << "integrate_color" << (int)kf_params.integrate_color;

  storage << "intr" << "{";
  storage << "fx" << kf_params.intr.fx;
  storage << "fy" << kf_params.intr.fy;
  storage << "cx" << kf_params.intr.cx;
  storage << "cy" << kf_params.intr.cy;
  storage << "}";

  storage << "tsdf_volume_dims" << kf_params.tsdf_volume_dims;
  storage << "color_volume_dims" << kf_params.color_volume_dims;
  storage << "volume_size" << kf_params.volume_size;
  storage << "volume_pose" << cv::Mat(kf_params.volume_pose.matrix);

  storage << "bilateral_sigma_depth" << kf_params.bilateral_sigma_depth;
  storage << "bilateral_sigma_spatial" << kf_params.bilateral_sigma_spatial;
  storage << "bilateral_kernel_size" << kf_params.bilateral_kernel_size;
  storage << "icp_truncate_depth_dist" << kf_params.icp_truncate_depth_dist;
  storage << "icp_dist_thres" << kf_params.icp_dist_thres;
  storage << "icp_angle_thres" << kf_params.icp_angle_thres;


  storage << "icp_iter_num" << kf_params.icp_iter_num;
  storage << "tsdf_min_camera_movement" << kf_params.tsdf_min_camera_movement;
  storage << "tsdf_trunc_dist" << kf_params.tsdf_trunc_dist;
  storage << "tsdf_max_weight" << kf_params.tsdf_max_weight;
  storage << "color_max_weight" << kf_params.color_max_weight;

  storage << "raycast_step_factor" << kf_params.raycast_step_factor;
  storage << "gradient_delta_factor" << kf_params.gradient_delta_factor;
  storage << "light_pose" << kf_params.light_pose;
  storage << "}";

  storage.release();
  return err;
}

/**
 * @name  ParseConfig
 * @fn  int ParseConfig(const std::string& config_path,
 *                      kfusion::KinFuParams* kf_params)
 * @brief Parse the config file and sets the kfusion parameters kf_params
 * @param[in]  config_path, path to the config file to parse
 * @param[out] kf_params
 * @return 0 if success, -1 otherwise
 */
int ParseConfig(const std::string& config_path,
                kfusion::KinFuParams* kf_params) {
  int err = 0;

  // http://docs.opencv.org/2.4/doc/tutorials/core/file_input_output_with_xml_yml/file_input_output_with_xml_yml.html
  cv::FileStorage storage(config_path, cv::FileStorage::READ);
  if (storage.isOpened()) {
    // read config file
    // Go root
    int integrate_color_int = 0;
    cv::FileNode root = storage.getFirstTopLevelNode();

    root["cols"] >> kf_params->cols;
    root["rows"] >> kf_params->rows;
    root["integrate_color"] >> integrate_color_int;
    kf_params->integrate_color = static_cast<bool>(integrate_color_int);

    root["intr"]["fx"] >> kf_params->intr.fx;
    root["intr"]["fy"] >> kf_params->intr.fy;
    root["intr"]["cx"] >> kf_params->intr.cx;
    root["intr"]["cy"] >> kf_params->intr.cy;

    root["tsdf_volume_dims"] >> kf_params->tsdf_volume_dims;
    root["color_volume_dims"] >> kf_params->color_volume_dims;
    root["volume_size"] >> kf_params->volume_size;

    cv::Mat pose_temp(4, 4, CV_32FC1);
    root["volume_pose"] >> pose_temp;
    kf_params->volume_pose.matrix = pose_temp.clone();

    root["bilateral_sigma_depth"] >> kf_params->bilateral_sigma_depth;
    root["bilateral_sigma_spatial"] >> kf_params->bilateral_sigma_spatial;
    root["bilateral_kernel_size"] >> kf_params->bilateral_kernel_size;
    root["icp_truncate_depth_dist"] >> kf_params->icp_truncate_depth_dist;
    root["icp_dist_thres"] >> kf_params->icp_dist_thres;
    root["icp_angle_thres"] >> kf_params->icp_angle_thres;

    root["icp_iter_num"] >> kf_params->icp_iter_num;
    root["tsdf_min_camera_movement"] >> kf_params->tsdf_min_camera_movement;
    root["tsdf_trunc_dist"] >> kf_params->tsdf_trunc_dist;
    root["tsdf_max_weight"] >> kf_params->tsdf_max_weight;
    root["color_max_weight"] >> kf_params->color_max_weight;

    root["raycast_step_factor"] >> kf_params->raycast_step_factor;
    root["gradient_delta_factor"] >> kf_params->gradient_delta_factor;
    root["light_pose"] >> kf_params->light_pose;

  } else {
    err = -1;
    std::cout << "[ParseConfig] ERROR: Unable to open configuration file" <<
    std::endl;
  }

  return err;
}

/**
 * @name SerializeParameters
 * @fn int SerializeParameters(const kfusion::KinFuParams& parameters)
 * @param[in] parameters structure to serialize
 * @param[out] serial_params serialized parameters into a vector of string
 * @return 0 if success, -1 otherwise
 */
int SerializeParameters(const kfusion::KinFuParams& parameters, std::vector<std::string>* serial_params) {
  serial_params->clear();

  serial_params->push_back(std::string("kfusion_params:"));
  serial_params->push_back(std::string("    cols: ") + std::to_string(parameters.cols));
  serial_params->push_back(std::string("    rows: ") + std::to_string(parameters.rows));
  serial_params->push_back(std::string("    integrate_color: ") + std::to_string(parameters.integrate_color));

  serial_params->push_back(std::string("    intr:"));
  serial_params->push_back(std::string("        fx: ") + std::to_string(parameters.intr.fx));
  serial_params->push_back(std::string("        fy: ") + std::to_string(parameters.intr.fy));
  serial_params->push_back(std::string("        cx: ") + std::to_string(parameters.intr.cx));
  serial_params->push_back(std::string("        cy: ") + std::to_string(parameters.intr.cy));

  serial_params->push_back(std::string("    tsdf_volume_dims: [ ")
                                       + std::to_string(parameters.tsdf_volume_dims[0]) + std::string(" ")
                                       + std::to_string(parameters.tsdf_volume_dims[1]) + std::string(" ")
                                       + std::to_string(parameters.tsdf_volume_dims[2]) + " ]");
  serial_params->push_back(std::string("    color_volume_dims: [")
                                       + std::to_string(parameters.color_volume_dims[0]) + std::string(" ")
                                       + std::to_string(parameters.color_volume_dims[1]) + std::string(" ")
                                       + std::to_string(parameters.color_volume_dims[2]) + " ]");
  serial_params->push_back(std::string("    volume_size: [")
                                       + std::to_string(parameters.volume_size[0]) + std::string(" ")
                                       + std::to_string(parameters.volume_size[1]) + std::string(" ")
                                       + std::to_string(parameters.volume_size[2]) + " ]");

  serial_params->push_back(std::string("    volume_pose: [ ") + std::to_string(parameters.volume_pose.matrix.val[0]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[1]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[2]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[3]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[4]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[5]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[6]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[7]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[8]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[9]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[10]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[11]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[12]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[13]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[14]) + std::string(" ")
                                                             + std::to_string(parameters.volume_pose.matrix.val[15]) + " ]");

  serial_params->push_back(std::string("    bilateral_sigma_depth: ") + std::to_string(parameters.bilateral_sigma_depth));
  serial_params->push_back(std::string("    bilateral_sigma_spatial: ") + std::to_string(parameters.bilateral_sigma_spatial));
  serial_params->push_back(std::string("    bilateral_kernel_size: ") + std::to_string(parameters.bilateral_kernel_size));
  serial_params->push_back(std::string("    icp_truncate_depth_dist: ") + std::to_string(parameters.icp_truncate_depth_dist));
  serial_params->push_back(std::string("    icp_dist_thres: ") + std::to_string(parameters.icp_dist_thres));
  serial_params->push_back(std::string("    icp_angle_thres: ") + std::to_string(parameters.icp_angle_thres));

  serial_params->push_back(std::string("    icp_iter_num: [ ")
                                       + std::to_string(parameters.icp_iter_num[0]) + std::string(" ")
                                       + std::to_string(parameters.icp_iter_num[1]) + std::string(" ")
                                       + std::to_string(parameters.icp_iter_num[2]) + " ]");

  serial_params->push_back(std::string("    tsdf_min_camera_movement: ") + std::to_string(parameters.tsdf_min_camera_movement));
  serial_params->push_back(std::string("    tsdf_trunc_dist: ") + std::to_string(parameters.tsdf_trunc_dist));
  serial_params->push_back(std::string("    tsdf_max_weight: ") + std::to_string(parameters.tsdf_max_weight));
  serial_params->push_back(std::string("    color_max_weight: ") + std::to_string(parameters.color_max_weight));
  serial_params->push_back(std::string("    raycast_step_factor: ") + std::to_string(parameters.raycast_step_factor));
  serial_params->push_back(std::string("    gradient_delta_factor: ") + std::to_string(parameters.gradient_delta_factor));

  serial_params->push_back(std::string("    light_pose: [ ")
                                       + std::to_string(parameters.light_pose[0]) + std::string(" ")
                                       + std::to_string(parameters.light_pose[1]) + std::string(" ")
                                       + std::to_string(parameters.light_pose[2]) + " ]");
  return 0;
}

int main(int argc, const char* argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("-d", LTS5::CmdLineParser::kNeeded,
                     "Depth bin file containing raw depth sequence (.bin)");
  parser.AddArgument("-r", LTS5::CmdLineParser::kNeeded,
                     "RGB bin file containing raw color sequence (.bin)");
  parser.AddArgument("-n", LTS5::CmdLineParser::kOptional,
                     "Number of frames to use for the reconstruction");
  parser.AddArgument("-c", LTS5::CmdLineParser::kNeeded,
                     "KFusion configuration file (.yaml/.xml)");
  parser.AddArgument("-o", LTS5::CmdLineParser::kNeeded,
                     "Output mesh file (.ply)");

  // Process inputs
  int err = parser.ParseCmdLine(argc, argv);
  if (err != 0) {
    std::cout << "Unable to parse command line" << std::endl;
    return err;
  }

  int device = 0;
  kfusion::cuda::setDevice (device);
  kfusion::cuda::printShortCudaDeviceInfo (device);

  if(kfusion::cuda::checkIfPreFermiGPU(device))
    return std::cout << std::endl << "Kinfu is not supported for pre-Fermi GPU architectures, and not built for them by default. Exiting..." << std::endl, 1;

  std::string depth_path;
  parser.HasArgument("-d", &depth_path);
  std::string rgb_path;
  parser.HasArgument("-r", &rgb_path);
  int n_frames = -1;
  std::string n_frames_str;
  parser.HasArgument("-n", &n_frames_str);

  std::istringstream is(n_frames_str);
  is >> n_frames;

  kfusion::BinSource capture(depth_path, rgb_path, n_frames);
  kfusion::KinFuParams kf_params = kfusion::KinFuParams::default_params();

  // Open config file
  std::string config_path;
  parser.HasArgument("-c", &config_path);

  // Write the defaults values in config file
  //err = WriteConfig(config_path, kf_params);

  err = ParseConfig(config_path, &kf_params);
  if (err != 0)
    return err;

  KinFuApp app (capture, kf_params);
  // executing
  try {
    app.execute ();
  } catch (const std::bad_alloc& /*e*/) {
    std::cout << "Bad alloc" << std::endl;
  } catch (const std::exception& /*e*/) {
    std::cout << "Exception" << std::endl;
  }

  std::cout << "Reconstruction finished!" << std::endl;

  LTS5::Mesh<float> mesh;
  app.take_mesh(&mesh);

  std::string output_path;
  parser.HasArgument("-o", &output_path);
  // Generate "meta-data" to be saved in the ply file of the mesh
  std::vector<std::string>& comments = mesh.get_comments();
  comments.push_back("Using KinectFusion app, based on kinfu_remake (https://github.com/Nerei/kinfu_remake)");
  comments.push_back(std::string("raw_depth_file: ") + depth_path);
  comments.push_back(std::string("raw_color_file: ") + rgb_path);
  comments.push_back(std::string("n_frames_used: ") + std::to_string(n_frames));
  std::vector<std::string> parameters_strings;
  SerializeParameters(kf_params, &parameters_strings);
  comments.insert(comments.end(), parameters_strings.begin(), parameters_strings.end());
  comments.push_back(std::string("lts5_git_commit_hash: ") + std::string(TOSTRING(LTS5_GIT_COMMIT_HASH)));
  // Finally, save the mesh!
  mesh.Save(output_path);

  return err;
}
