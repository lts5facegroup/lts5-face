# -*- coding: utf-8 -*-
"""
Created on Wed Aug  3 13:29:48 2016

@author: gabriel
"""

import argparse
import sys
import csv
import subprocess
import os


def parse_args():
    """ Arguments parser configuration """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-f', '--folder', metavar='PATH', required=False,
                        help='Path to the root folder containing the raw data '
                        '(.bin)')
    parser.add_argument('-i', '--csvfile', metavar='PATH', required=False,
                        help='Path to the csv file containing path to the raw '
                        'data')
    parser.add_argument('-c', '--config', metavar='PATH', required=True,
                        help='Path to the KinectFusion config file (.yaml) '
                        'in case the input mode is a folder')
    parser.add_argument('-e', '--executable', metavar='PATH',
                        required=True, default=None,
                        help='Path to the Kinect Fusion executable')
    parser.add_argument('-o', '--output-folder', metavar='PATH',
                        required=False, default=None,
                        help='Path to the output folder (where to save the '
                             'reconstruction')
    parser.add_argument('--dry-run', required=False, action='store_true')
    # Add more arguments here...
    args = parser.parse_args()

    if not (args.folder or args.csvfile):
        parser.error('Provide path to the data, either using -f <root/folder/>'
                     ' or -i <csvfile>.csv')
    if (args.folder and not args.config):
        parser.error('Provide a config file when parsing a folder. Use '
                     '-c <config>.yaml')

    return args

    
def get_csv_headers():
    return ['depth', 'color', 'mesh_path', 'last_frame',
            'wrong_subject', 'wrong_expression', 'quality', 'reconstruct']
    
            
def get_dict_row(row):
    d = {}
    for h in get_csv_headers():
        d[h] = row[h]
    return d

    
def get_dict(root, f_depth, f_color, out_path):
    d = {}
    d['depth'] = os.path.join(root, f_depth)
    d['color'] = os.path.join(root, f_color)
    d['mesh_path'] = out_path
    d['last_frame'] = -1
    d['wrong_subject'] = 1
    d['wrong_expression'] = 1
    d['quality'] = 1
    d['reconstruct'] = 1
    return d

    
def parse_csv(csv_file):
    """ Given a CSV file, parse it

    The CSV file should contain at least the following headers:
    depth, color, config, mesh_path

    Parameters
    ----------
    csv_file: str
        Path to the csv file to parse

    Returns
    -------
    rec_list: list of dict containing the infos for each reconstructions
    """
    rec_list = []
    with open(csv_file) as f:
        reader = csv.DictReader(f, get_csv_headers())

        try:
            reader.next()  # Skip headers
            for row in reader:
                    rec_list.append(get_dict_row(row))
        except csv.Error as e:
            sys.exit('file %s, line %d: %s' % (csv_file, reader.line_num, e))

    return rec_list


def parse_folder(folder, config_file, output_folder):
    """ Given a folder, parse it

    Parameters
    ----------
    folder: str
        Path to the root folder to parse

    config_file: str
        Path to the kinect fusion config file, to append to rec_list

    output_folder: str
        Path to the output_folder, to append to rec_list

    Returns
    -------
    rec_list: list of dict containing the infos for each reconstructions
    """
    rec_list = []
    for root, dirs, files in os.walk(folder):
        for f in files:
            if f.endswith('RawDepth.bin') or f.endswith('RawDepth.bin.gz'):
                color_f = str.replace(f, 'RawDepth', 'RawColor')
                out_f = f[18:32] + '.ply'
                if (output_folder):
                    out_path = os.path.join(output_folder, out_f)
                else:
                    out_path = os.path.join(root, out_f)

                rec_list.append(get_dict(root, f, color_f, config_file, out_path))

    # and save a csv while you are at it...
    with open('rec_list.csv', 'w') as f:
        print 'Saving ' + 'rec_list.csv' + ' with the result of the folder parsing...'
        writer = csv.DictWriter(f, fieldnames=get_csv_headers())
        writer.writeheader()
        writer.writerows(rec_list)

    return rec_list


def check_file_extension(f):
    """ Check that the file extension is either .bin or .bin.gz"""
    if f.endswith('.bin') or (f.endswith('.bin.gz')):
        return True
    return False


def unzip_file(f, dest):
    """ Unzip a .gz file f in dest"""
    res = os.path.join(dest, os.path.basename(f)[:-3])

    # First check that the file does not exist already...
    if not os.path.exists(res):
        cmd = ["cp", f, dest]
        subprocess.call(cmd)
        cmd = ["pigz", "-dk", "-f", res + '.gz']
        subprocess.call(cmd)
        #cmd = ["mv", f[:-3], dest]
        #subprocess.call(cmd)

    return res


def reconstruct(rec_list, config_file, exec_path, csvfile):
    """ Given a list of dict containing Kinect Fusion parameters, launch it """

    for i, rec in enumerate(rec_list):
        if rec['reconstruct'] == '0':
            continue

        temp_depth = False
        temp_color = False
        if rec['depth'].endswith('gz'):
            temp_depth = True
            temp_dir = os.path.dirname(rec['mesh_path']) + '/'
            unzip_file(rec['depth'], temp_dir)
            rec['depth'] = os.path.join(temp_dir,
                                        os.path.basename(rec['depth'])[:-3])
        if rec['color'].endswith('gz'):
            temp_color = True
            temp_dir = os.path.dirname(rec['mesh_path']) + '/'
            unzip_file(rec['color'], temp_dir)
            rec['color'] = os.path.join(temp_dir,
                                        os.path.basename(rec['color'])[:-3])

        cmd = [exec_path,
               '-d', rec['depth'],
               '-r', rec['color'],
               '-n', rec['last_frame'],
               '-c', config_file,
               '-o', rec['mesh_path']]

        # DEBUG PURPOSE
        success = True
        print rec['mesh_path']
        try:
            subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            success = False
            print e.output

        if success:
            rec['reconstruct'] = 0
            with open(csvfile, 'w') as f:
                writer = csv.DictWriter(f, fieldnames=get_csv_headers())
                writer.writeheader()
                writer.writerows(rec_list)

        # Clean the temp stuff
        if temp_depth:
            cmd = ["rm", rec['depth']]
            subprocess.call(cmd)
            cmd = ["rm", rec['depth'] + '.gz']
            subprocess.call(cmd)
        if temp_color:
            cmd = ["rm", rec['color']]
            subprocess.call(cmd)
            cmd = ["rm", rec['color'] + '.gz']
            subprocess.call(cmd)


def main():
    """

    """
    args = parse_args()
    if args.csvfile:
        rec_list = parse_csv(args.csvfile)
    elif args.folder:
        rec_list = parse_folder(args.folder, args.config, args.output_folder)
    else:
        print 'ERROR: Provide a CSV file or a folder to parse'
    if not args.dry_run:
        reconstruct(rec_list, args.config, args.executable, args.csvfile)
    else:
        print 'Dry run'


if __name__ == "__main__":
    main()
