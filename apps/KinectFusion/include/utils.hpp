/**
 *  @file   utils.hpp
 *  @brief  Utils for KinectFusion app
 *
 *  @author Gabriel Cuendet
 *  @date   27/07/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef __UTILS_HPP__
#define __UTILS_HPP__

#include "vector_types.h"
#include "kfusion/cuda/device_array.hpp"
#include "kfusion/types.hpp"

/**
 * @name ConvertFloat4to3
 * @fn void ConvertFloat4to3(const kfusion::cuda::DeviceArray<kfusion::Point>& input,
 *                           kfusion::cuda::DeviceArray<float3>& output)
 * @brief Convert a device array of float4 to a device array of float3 (typically for color conversion from RGBA to RGB)
 * @param input the float4 device array to convert from
 * @param output the float3 device array to convert to
 */
void ConvertFloat4to3(const kfusion::cuda::DeviceArray<kfusion::Point>& input,
                      kfusion::cuda::DeviceArray<float3>& output);

/**
 * @name AlignDepthRgb
 * @fn void AlignDepthRgb(kfusion::cuda::Depth* depth_device
 * @brief Align the depth map onto the color image on GPU in order to integrate colors in the reconstruction, in case
 *        the data from the Kinect are not already aligned (which should be the case:
 *        Use GetAlternativeViewPointCap().SetViewPoint())
 * @param K_rgb Intrinsics of the RGB camera
 * @param K_ir Intrinsics of the depth camera
 * @param transform_ir Affine transformation [R|t] between the RGB and the depth camera
 * @param depth_device the deivce array containing depth map to align
 *
void AlignDepthRgb(const kfusion::Intr& K_rgb, const kfusion::Intr& K_ir,
                   const kfusion::Affine3f& transform_ir,
                   kfusion::cuda::Depth* depth_device);
 */
#endif // __UTILS_HPP__
