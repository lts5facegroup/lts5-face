//
//  kinect_fusion.hpp
//  LTS5-Dev
//
//  Created by Gabriel Cuendet on 14.07.16.
//  Copyright © 2016 Cuendet Gabriel. All rights reserved.
//

#ifndef __KINECT_FUSION_HPP__
#define __KINECT_FUSION_HPP__

#include <stdio.h>

#include "kfusion/kinfu.hpp"
#include "kfusion/cuda/marching_cubes.hpp"
#include "io/bin_grabber.hpp"

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/geometry/mesh.hpp"
#include "utils.hpp"

struct KinFuApp {
    /** Vertex type */
    using Vertex = typename LTS5::Mesh<float>::Vertex;
    /** Triangle type */
    using Triangle = typename LTS5::Mesh<float>::Triangle;
    /** Vertex color */
    using Color = typename LTS5::Mesh<float>::Color;

    /**
   * @name KinFuApp
   * @fn KinFuApp(BinSource& source)
   * @brief Constructor of the struct, only taking a BinSource data source
   * @param[in] source, a BinSource from which the frames are grabbed
   */
  explicit KinFuApp(kfusion::BinSource& source) : exit_ (false), capture_ (source),
  pause_(false) {
    kfusion::KinFuParams params = kfusion::KinFuParams::default_params();
    kinfu_ = kfusion::KinFu::Ptr(new kfusion::KinFu(params));

    capture_.setRegistration(true);
  }

  /**
   * @name KinFuApp
   * @fn KinFuApp(BinSource& source, const KinFuParams& params)
   * @brief Constructor of the struct, allowing to initialize KinFu to different parameters
   * @param[in] source, a BinSource from which the frames are grabbed
   * @param[in] params, a KinFuParams struct containing the parameters
   */
  KinFuApp(kfusion::BinSource& source, const kfusion::KinFuParams& params)
  : exit_ (false), capture_ (source), pause_(false) {
    kinfu_ = kfusion::KinFu::Ptr(new kfusion::KinFu(params));

    capture_.setRegistration(true);
  }

  /**
   * @name take_cloud
   * @fn void take_cloud(KinFu& kinfu)
   * @brief Fetch cloud and display it
   * @param[in] kinfu instance
   */
  void take_cloud(kfusion::KinFu& kinfu) {
    kfusion::cuda::DeviceArray<kfusion::Point> cloud = kinfu.tsdf().fetchCloud(cloud_buffer_);

    cv::Mat cloud_host(1, (int)cloud.size(), CV_32FC4);

    if (kinfu.params().integrate_color) {
      kinfu.color_volume()->fetchColors(cloud, color_buffer_);
      cv::Mat color_host(1, (int)cloud.size(), CV_8UC4);
      cloud.download(cloud_host.ptr<kfusion::Point>());
      color_buffer_.download(color_host.ptr<kfusion::RGB>());
      //viz.showWidget("cloud", cv::viz::WCloud(cloud_host, color_host));
    } else {
      cloud.download(cloud_host.ptr<kfusion::Point>());
      //viz.showWidget("cloud", cv::viz::WCloud(cloud_host));
      //viz.showWidget("cloud", cv::viz::WPaintedCloud(cloud_host));
    }
  }

  /**
   * @name take_mesh
   * @fn void take_mesh(KinFu& kinfu)
   * @brief Run marching cubes on the volume and construct the mesh
   * @param[in] kinfu instance
   * @param[out] mesh
   */
  void take_mesh(LTS5::Mesh<float>* mesh) {
    std::cout << "Grabbing mesh..." << std::flush;

    if (!marching_cubes_)
      marching_cubes_ = cv::Ptr<kfusion::cuda::MarchingCubes>(new kfusion::cuda::MarchingCubes());

    kfusion::cuda::DeviceArray<kfusion::Point> vertices = marching_cubes_->run(kinfu_->tsdf(), triangles_buffer_);
    int n_vert = vertices.size();

    std::vector<Vertex>& mesh_vertices = mesh->get_vertex();
    mesh_vertices.resize(n_vert);
    std::vector<Triangle>& mesh_triangles = mesh->get_triangle();
    mesh_triangles.resize(n_vert/3);
    std::vector<Color>& mesh_colors = mesh->get_vertex_color();

    for (int i = 0; i < n_vert/3; ++i) {
      mesh_triangles[i] = Triangle(3*i, 3*i+1, 3*i+2);
    }

    if (kinfu_->params().integrate_color) {
      mesh_colors.resize(n_vert);
      kinfu_->color_volume()->fetchColors(vertices, color_buffer_);
      color_buffer_.download(reinterpret_cast<kfusion::RGB*>(mesh_colors.data()));
      // Fix color order, there might be a better way of doing that...
      cv::Mat mat_colors(mesh_colors.size(), 1, CV_8UC4, mesh_colors.data());
      cv::cvtColor(mat_colors, mat_colors, CV_BGRA2RGBA);
    }

    kfusion::cuda::DeviceArray<float3> vertices_converted;
    ConvertFloat4to3(vertices, vertices_converted);
    vertices_converted.download(reinterpret_cast<float3*>(mesh_vertices.data()));
    //cudaSafeCall(cudaMemcpy(mesh_vertices.data(), triangles.ptr(), triangles.sizeBytes(), cudaMemcpyDeviceToHost));
    std::cout << "  OK (#vertices = " << n_vert << ")" << std::endl;
  }

  /**
   * @name execute
   * @fn bool execute()
   * @brief Run the main loop of the app
   * @return true if no error, false otherwise
   */
  bool execute() {
    kfusion::KinFu& kinfu = *kinfu_;
    cv::Mat depth, image;
    double time_ms = 0;
    bool has_image = false;

    int n_frames = capture_.getTotalFrames();

    for (int i = 0; !exit_ && i < n_frames; ++i)
    {
      kfusion::SampledScopeTime fps(time_ms); (void)fps;

      bool has_frame = capture_.grab(depth, image);
      if (!has_frame) {
        return std::cout << "Can't grab: i = " << i << "; #frames = " << n_frames << std::endl, false;
      }
      depth_device_.upload(depth.data, depth.step, depth.rows, depth.cols);
      color_device_.upload(image.data, image.step, image.rows, image.cols);

      /* GPU alignment
      kfusion::Intr K_rgb(520.89, 520.23, 324.54, 237.55);
      kfusion::Intr K_ir(589.9, 589.1, 328.4, 236.89);
      cv::Mat R_ir = cv::Mat::eye(3, 3, CV_32FC1);
      cv::Vec3f t_ir = {-25.4, -0.13, -2.18};
      cv::Affine3f transform_ir(R_ir, t_ir);
      AlignDepthRgb(K_rgb, K_ir, transform_ir, &depth_device_);
       */

      if (kinfu.params().integrate_color)
        has_image = kinfu(depth_device_, color_device_);
      else
        has_image = kinfu(depth_device_);
    }

    return true;
  }

  /**< */
  bool pause_; // = false
  /**< Stop the execution when set to true */
  bool exit_;
  /**< Reference to a source of data BinSource */
  kfusion::BinSource& capture_;
  /**< Pointer to the instance of kinfu */
  kfusion::KinFu::Ptr kinfu_;

  /**< Depth frame on the GPU */
  kfusion::cuda::Depth depth_device_;
  /**< Color frame on the GPU */
  kfusion::cuda::Image color_device_;
  /**< point buffer used when fetching the point cloud from the tsdf volume */
  kfusion::cuda::DeviceArray<kfusion::Point> cloud_buffer_;
  /**< color buffer used when fetching the colors from the color volume */
  kfusion::cuda::DeviceArray<kfusion::RGB> color_buffer_;

  /**< Marching cubes instance (to generate a MESH) */
  cv::Ptr<kfusion::cuda::MarchingCubes> marching_cubes_;
  /**< triangles buffer used in marching cubes */
  kfusion::cuda::DeviceArray<kfusion::Point> triangles_buffer_;
};


#endif /* __KINECT_FUSION_HPP__ */
