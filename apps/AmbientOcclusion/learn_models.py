# coding=utf-8
"""
Learn Ambient Occlusion statistical model and joint model (shp + ao)
"""
from os.path import join as _join
from os.path import exists as _exists
from os.path import isdir as _isdir
from os import makedirs as _mkdir
from argparse import ArgumentParser
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from lts5.utils.tools import str2bool, init_logger, search_folder
from lts5.utils import load_matrix
from lts5.geometry import MeshFloat as Mesh
from lts5.model import ColorModelFloat as CModel
from lts5.model import ColorMorphableModelFloat as CMModel


logger = init_logger()


# https://stackoverflow.com/a/5929165
def cache_result(filename='cache.npy'):
  """
  Function decorator to cache function results into a file
  :param filename:  Name of the file where to cache the results
  :return:  Decorated function
  """
  def cached_function(func):
    def wrapper(*args, **kwargs):
      if not _exists(filename):
        res = func(*args, **kwargs)
        np.save(filename, res, allow_pickle=False)
      else:
        logger.debug('Reload output from cache file: {}'.format(filename))
        res = np.load(filename)
      return res
    return wrapper
  return cached_function


def _validate_ao_model_params(pargs):
  """
  Check if required parameters are present for training AO model
  :param pargs:   Parsed arguments
  :return:  `True` if valid, `False` otherwise
  """
  return pargs.ao_folder is not None and pargs.ao_model_folder is not None


def _validate_joint_model_params(pargs):
  """
  Check if required parameters are present for training joint shape+ao model
  :param pargs:   Parsed arguments
  :return:  `True` if valid, `False` otherwise
  """
  return (pargs.shape_model is not None and
          pargs.ao_model_folder is not None and
          pargs.joint_model_folder is not None and
          pargs.shape_folder is not None)


def _validate_test_model_params(pargs):
  """
  Check if required parameters for test are present
  :param pargs: Parsed arguments
  :return:  `True` if valid, `False` otherwise
  """
  return ((((pargs.test_shape_folder is not None and
             pargs.test_ao_folder is not None) or
            (pargs.shape_folder is not None and
             pargs.ao_folder is not None))) and
          pargs.joint_model_folder is not None and
          pargs.shape_model is not None)


def _validate_export_params(pargs):
  """
    Check if required parameters for exportation are present
    :param pargs: Parsed arguments
    :return:  `True` if valid, `False` otherwise
    """


  return True


def _select_number_basis(samples, thresh, center=False):
  """
  Compute eigenvalues of covariance matrix for a given set of samples. Define
  number of basis to select to have energy higher than a given threshold
  :param samples:   Array of samples, stored by row
  :param thresh:    Amount of energy to keep i.e. value in [0, 1.0]
  :return: int, number of basis to keep
  """
  # Get eigenvalue using SVD
  x = samples
  if not center:
    x = x - x.mean(axis=0)
  sv = np.linalg.svd(x, compute_uv=False)
  # Convert singular value to eigenvalue
  # See: https://stats.stackexchange.com/a/134283
  eigval = sv ** 2.0 / (x.shape[0] - 1)
  # Compute cumulative energy
  energy = np.cumsum(eigval)
  energy /= energy[-1]
  # Select basis
  return np.where(energy > thresh)[0].min()


def _load_shape_model(filename):
  """
  Load shape model from 3DMM file
  :param filename:  Filename
  :return:  Statistical shape model instance or None if something went wrong
  """
  from lts5.model import ColorMorphableModelFloat as CMModel
  model = CMModel()
  err = model.load(filename)
  if err:
    logger.error('Can not load model "{}"'.format(filename))
    return None
  return model.shape


def _load_ao_model(folder_or_file):
  """
  Load ambient occlusion model
  :param folder_or_file:  Folder or file to the model. If folder is provided,
                          load with default name
  :return: Model instance or None if something went wrong
  """
  fname = folder_or_file
  if _isdir(fname):
    fname = _join(fname, 'ao_stat_model.bin')
  model = CModel()
  err = model.load(fname)
  if err:
    logger.error('Can not load model "{}"'.format(fname))
    return None
  return model


@cache_result(filename='ao_samples.npy')
def _load_ao_samples(folder):
  """
  Load AO samples into an array. Result will be cached into file
  :return:  AO samples
  """
  files = search_folder(folder, ext=['.ao'], relative=False)
  ao_samples = []
  for f in files:
    _, ao = load_matrix(f)
    ao_samples.append(ao.T)
  ao_samples = np.vstack(ao_samples)
  return ao_samples


@cache_result(filename='samples.npy')
def _load_samples(pargs, shp_model, ao_model):
  """
  Load samples. The following steps are performed
    - Load shape + ao
    - Project into respective subspace
    - Concatenate them

  :param pargs: Parsed arguments
  :param shp_model: Shape model
  :param ao_model:  Ambient occlusion model
  :return:  Sample matrix
  """

  def _project(sample, ibasis, mean):
    return ibasis @ (sample - mean)

  shp_mean = shp_model.mean
  shp_ibasis = shp_model.variation @ np.diagflat(shp_model.prior)
  shp_ibasis = np.linalg.pinv(shp_ibasis)

  ao_mean = ao_model.mean
  ao_ibasis = ao_model.variation @ np.diagflat(ao_model.prior)
  ao_ibasis = np.linalg.pinv(ao_ibasis)

  samples = []
  surfaces = search_folder(pargs.shape_folder, ext=['.obj'])
  for k, path in enumerate(surfaces):
    # Log
    if k % 1000 == 0:
      logger.info('Process surface: {}'.format(path))
    # Does corresponding ao file exists ?
    ao_file = _join(pargs.ao_folder, path.replace('.obj', '.ao'))
    if _exists(ao_file):
      # Load them
      mesh = Mesh(_join(pargs.shape_folder, path))
      _, ao = load_matrix(ao_file)
      # Project into respective basis
      theta_shp = _project(mesh.vertex.reshape(-1, 1), shp_ibasis, shp_mean)
      theta_ao = _project(ao, ao_ibasis, ao_mean)
      # Concatenate
      samples.append(np.vstack((theta_shp, theta_ao)).T)
    else:
      logger.warning('No AO found for {}'.format(path))
  return np.vstack(samples)


def _learn_ao_model(pargs):
  """
  Learn Ambient Occlusion statistical model
  :param pargs: PArsed arguments
  """
  if pargs.learn_ao_model:
    if _validate_ao_model_params(pargs):
      logger.info('Train ambient occlusion model...')

      # Load AO files
      ao_samples = _load_ao_samples(folder=pargs.ao_folder)
      # Compute eigenvalue to select amount of variance to keep
      logger.debug('Select number of components to have to {:.2f}%'
                   ' variance'.format(pargs.ao_variance))
      n_component = 160 #_select_number_basis(ao_samples, pargs.ao_variance)
      logger.debug('#Components: {}'.format(n_component))
      # Do PCA
      pca = PCA(n_components=n_component, copy=False)
      pca.fit(ao_samples)

      var = np.cumsum(pca.explained_variance_ratio_)
      logger.info('Explained var: {:.3f}'.format(var[-1]))

      # Extract meaning full information
      eigvec = pca.components_.T
      eigval = (pca.singular_values_ ** 2.0) / (ao_samples.shape[0] - 1)
      eigval = eigval.reshape(-1, 1)
      mean = pca.mean_.reshape(-1, 1)
      # Save model
      if not _exists(pargs.ao_model_folder):
        _mkdir(pargs.ao_model_folder)
      fname = _join(pargs.ao_model_folder, 'ao_stat_model.bin')
      model = CModel(mean=mean, variation=eigvec, prior=eigval, n_channels=1)
      model.save(fname)
      logger.info('Ambient occlusion model trained')
    else:
      msg = 'Missing parameters for ambient occlusion model training. Requir' \
            'ed entries: `--ao_folder`, `--ao_model_folder`'
      logger.error(msg)
  else:
    logger.warning('Skip AO model training')


def _learn_joint_model(pargs):
  """
  Learn joint model, Shape + Ambient occlusion
  :param pargs:   Parsed arguments
  """
  if pargs.learn_joint_model:
    if _validate_joint_model_params(pargs):
      logger.info('Train joint shape+ao model...')
      # Load models: Shape + AO
      shp_model = _load_shape_model(pargs.shape_model)
      ao_model = _load_ao_model(pargs.ao_model_folder)
      # Get samples
      samples = _load_samples(pargs, shp_model, ao_model)
      # Build design matrix
      mu_ml = samples.mean(axis=0)
      x = samples - mu_ml
      dmat = (x.T @ x) / (x.shape[0] - 1)

      eval, evec = np.linalg.eigh(dmat)

      # Compute PCA
      u, s, vt = np.linalg.svd(dmat)
      energy = np.cumsum(s)
      energy /= energy[-1]
      n_comp = np.where(energy > pargs.joint_variance)[0].min()
      # Compute maximum likelihood basis
      Un = u[:, :n_comp]
      Sigma_n = s[:n_comp]
      Sigma_ml = s[n_comp:].mean()
      Wml = Un @ (np.diagflat(Sigma_n) - np.eye(Sigma_n.size) * Sigma_ml) ** 0.5
      # Save vars
      data = {'mean': mu_ml.reshape(-1, 1).astype(np.float32),
              'eigval': Sigma_n.astype(np.float32),
              'eigvec': Wml.astype(np.float32),
              'noise_sq': Sigma_ml,
              'dims': [shp_model.variation.shape[1],
                       ao_model.variation.shape[1]]}
      if not _exists(pargs.joint_model_folder):
        _mkdir(pargs.joint_model_folder)
      fname = _join(pargs.joint_model_folder, 'joint_shape_ao_model.npy')
      np.save(fname, data)
      logger.info('Joint shape+ao model saved in {}'.format(fname))
    else:
      msg = 'Missing parameters for joint shape+ao model training. Required: ' \
            '`--shp_model`, `--joint_model_folder`'
      logger.error(msg)
  else:
    logger.warning('Skip Joint Shape+Ao model training')


class JointShapeAoModel:
  """ Joint Shape+Ao statistical model """
  def __init__(self,
               joint_filename,
               shape_model,
               ao_model):
    """
    Constructor
    :param joint_filename: Location where model is stored (i.e. *.npy)
    :param shape_model: Shape model instance
    :param ao_model:    Ambient occlusion model instance
    """
    # Shape
    self._shp_mean = shape_model.mean
    self._shp_basis = shape_model.variation @ np.diagflat(shape_model.prior)
    self._ishp_basis = np.linalg.pinv(self._shp_basis)
    # AO
    self._ao_mean = ao_model.mean
    self._ao_basis = ao_model.variation @ np.diagflat(ao_model.prior)
    # Joint
    data = np.load(joint_filename, allow_pickle=True).item()
    self._dims = data['dims']
    self._j_mean = data['mean']
    self._j_basis = data['eigvec'] @ np.diagflat(data['eigval'])
    self._sigma_ml = data['noise_sq']

    # Init subbasis
    self._init_internal()

  @classmethod
  def FromFiles(cls,
                joint_filename,
                shape_filename,
                ao_filename):
    """
    Construct Joint model instance from files
    :param joint_filename:  Location where model is stored (i.e. *.npy)
    :param shape_filename:  Location where morphable model is stored
    :param ao_filename:     Location where ao model is stored
    :return:  Joint model instance
    """
    shp_model = CMModel()
    err = shp_model.load(shape_filename)
    if err:
      raise ValueError('Can not open shape model: {}'.format(shape_filename))
    shp_model = shp_model.shape
    ao_model = CModel()
    err = ao_model.load(ao_filename)
    if err:
      raise ValueError('Can not open AO model: {}'.format(ao_filename))
    return cls(joint_filename, shp_model, ao_model)

  def _init_internal(self):
    """ Initialize internal component: w_shp, w_ao, mu_shp, mu_ao, M"""
    # Shape
    n_shp = self._dims[0]
    self._w_shp = self._j_basis[:n_shp, :]
    self._iw_shp = np.linalg.pinv(self._w_shp)
    self._mu_shp = self._j_mean[:n_shp, :]
    # AO
    self._w_ao = self._j_basis[n_shp:, :]
    self._mu_ao = self._j_mean[n_shp:, :]
    # (Wml' Wml)^-1
    self._iWtW = np.linalg.inv(self._w_ao.T @ self._w_ao).astype(np.float32)

  def generate(self, shape, is_param=False):
    """
    Estimate Ambient occlusion from a given shape/shape parameters
    :param shape:     Array, shape or shape parameters
    :param is_param:  Bool, If `True` indicates this is already shape parameters
                      otherwise project it first into shape parameter space
    :return:  Corresponding ambient occlusion
    """
    param = shape
    if not is_param:
      # Project into shape basis
      param = self._ishp_basis @ (param - self._shp_mean)
    # Reconstruction
    # Probabilistic Principal Component Analysis.
    # Michael E. Tipping and Christopher M. Bishop, page 615
    # < xn|tn > = M^-1 Wml' (tn - mu)
    # where M = Wml' Wml + sigma^2 I
    # Wml (Wml' Wml)^-1 M < xn|tn>
    p_xn_tn = (self._w_shp.T @ (param - self._mu_shp))
    theta_ao = ((self._w_ao @ self._iWtW) @ p_xn_tn) + self._mu_ao
    ao = self._ao_mean + (self._ao_basis @ theta_ao)
    return ao

  def export_to_tensorflow(self, folder):
    """
    Pack everything into numpy file ready to use in tensorflow layer.
    :param folder:  Location where to place the file
    """

    data = {'ishp_basis': self._ishp_basis,
            'shp_mean': self._shp_mean,
            'ao_basis': self._ao_basis,
            'ao_mean': self._ao_mean,
            'w_ao': self._w_ao,
            'mu_ao': self._mu_ao,
            'w_shp': self._w_shp,
            'mu_shp': self._mu_shp}
    if not _exists(folder):
      _mkdir(folder)
    fname = _join(folder, 'tf_joint_shape_ao_model.npy')
    np.save(fname, data, allow_pickle=True)
    logger.info('TF model saved into {}'.format(fname))


def _test_joint_model(pargs):
  """
  Test learned joint model
  :param pargs: Parsed command line arguments
  """
  if pargs.test_joint_model:
    if _validate_test_model_params(pargs):

      # Load model
      jname = _join(pargs.joint_model_folder, 'joint_shape_ao_model.npy')
      shp_model = _load_shape_model(pargs.shape_model)
      ao_model = _load_ao_model(pargs.ao_model_folder)
      model = JointShapeAoModel(jname, shp_model, ao_model)

      def _run_test(shape_folder, ao_folder, partition):
        if shape_folder and ao_folder:
          # Load data
          shp_path = search_folder(shape_folder, ext=['.obj'], relative=False)
          ao_path = search_folder(ao_folder, ext=['.ao'], relative=False)
          true_shp = []
          true_ao = []
          for shp_p, ao_p in zip(shp_path, ao_path):
            shape = Mesh(shp_p).vertex.reshape(-1, 1)
            _, ao = load_matrix(ao_p)
            true_shp.append(shape)
            true_ao.append(ao)
          # Ground truth
          true_shp = np.hstack(true_shp)
          true_ao = np.hstack(true_ao)
          # Predict
          pred_ao = model.generate(true_shp, is_param=False)
          # Compute error L2 norm
          diff = (((true_ao - pred_ao) ** 2.0).sum(axis=0) ** 0.5)
          logger.info('Mean L2 error: {:.3f}'.format(diff.mean()))
          plt.plot(diff)
          plt.xlabel('Sample index')
          plt.ylabel('L2 norm')
          plt.title('AO Reconstruction error for {} partition'.format(partition))
          plt.show(block=False)
        else:
          logger.warning('Skip test on {} set'.format(partition))

      _run_test(shape_folder=pargs.shape_folder,
                ao_folder=pargs.ao_folder,
                partition='training')
      _run_test(shape_folder=pargs.test_shape_folder,
                ao_folder=pargs.test_ao_folder,
                partition='testing')
    else:
      msg = 'Missing parameters for testing joint model. Required: ' \
            '`--test_shape_folder` && `--test_ao_folder` OR ' \
            '`--shape_folder` && `--ao_folder`'
      logger.error(msg)
  else:
    logger.warning('Skip Joint Shape+Ao model testing')


def _export_tf_model(pargs):
  """
  Pack model to use in tensorflow layer
  :param pargs: Parsed command line arguments
  """
  if pargs.export_to_tensorflow:
    if _validate_export_params(pargs):
      # Load models
      jname = _join(pargs.joint_model_folder, 'joint_shape_ao_model.npy')
      shp_model = _load_shape_model(pargs.shape_model)
      ao_model = _load_ao_model(pargs.ao_model_folder)
      model = JointShapeAoModel(jname, shp_model, ao_model)
      # Export
      model.export_to_tensorflow(pargs.tf_model_folder)
    else:
      msg = 'Missing parameters for testing joint model. Required: ' \
            '`--shape_model`, `--ao_model_folder`, `--joint_model_folder`, ' \
            '`--tf_model_folder`'
      logger.error(msg)
  else:
    logger.warning('Skip joint Shape+Ao model export to Tensorflow')


if __name__ == '__main__':
  # Parser
  p = ArgumentParser()

  # Lear AO model
  p.add_argument('--learn_ao_model',
                 type=str2bool,
                 required=True,
                 help='Indicates if AO statistical model needs to be learned')
  # Location where AO are stored
  p.add_argument('--ao_folder',
                 type=str,
                 required=True,
                 help='Location where AO files are stored (i.e. *.ao)')
  # Amount of variance to keep ao models
  p.add_argument('--ao_variance',
                 type=float,
                 default=0.98,
                 help='Amount of variance to keep in AO model')
  # Location where to place the AO models
  p.add_argument('--ao_model_folder',
                 type=str,
                 help='Path were to save/load AO model')
  # Joint model
  p.add_argument('--learn_joint_model',
                 type=str2bool,
                 required=True,
                 help='Indicates if joint statistical model needs to be learn'
                      'ed')
  p.add_argument('--shape_folder',
                 type=str,
                 help='Location where shapes are stored')
  p.add_argument('--shape_model',
                 type=str,
                 help='Statistical morphable model file location')
  p.add_argument('--joint_variance',
                 type=float,
                 default=0.98,
                 help='Amount of variance to keep in joint shape+ao model')
  p.add_argument('--joint_model_folder',
                 type=str,
                 help='Location where to store the learned model')
  # Test
  p.add_argument('--test_joint_model',
                 required=True,
                 type=str2bool,
                 help='Indicate if joint model needs to be tested')
  p.add_argument('--test_shape_folder',
                 type=str,
                 help='Location where test surfaces are located')
  p.add_argument('--test_ao_folder',
                 type=str,
                 help='Location where test ao are located')
  # Export
  p.add_argument('--export_to_tensorflow',
                 type=str2bool,
                 required=True,
                 help='Indicates if model needs to be export for tensorflow')
  p.add_argument('--tf_model_folder',
                 type=str,
                 help='Location where to place tensorflow model')
  args = p.parse_args()

  # Do stuff
  _learn_ao_model(pargs=args)
  _learn_joint_model(pargs=args)
  _test_joint_model(pargs=args)
  _export_tf_model(pargs=args)
