# coding=utf-8
"""
This script generate the required data in order learn statistical model for
Ambient Occlusions (AO) and a Joint model between the mesh surface and its
corresponding AO.

Two steps are performed:
  1) Generate a set of surface from a statistical shape model (i.e BFM)
  2) Estimate Ambient Occlusion coefficients using Monte Carlo estimation.
"""
from os.path import join as _join
from os.path import exists as _exists
from os.path import basename as _basename
from os import makedirs as _mkdir
from subprocess import Popen, PIPE
from argparse import ArgumentParser
import numpy as np
from lts5.utils.tools import str2bool, init_logger, search_folder
from lts5.utils import save_matrix
from lts5.geometry import MeshFloat as Mesh
from lts5.geometry import AOGeneratorFloat as AOGenerator
from lts5.model import ColorMorphableModelFloat as Model
from utils.plyfile import PlyData, PlyElement

logger = init_logger()


def _validate_gen_surf_params(pargs):
  """
  Check if required parameters are present
  :param pargs:   Parsed arguments
  :return:  True if valid, False otherwise
  """
  return pargs.morphable_model is not None and pargs.surface_folder is not None


def _validate_ao_estm_params(pargs):
  """
  Check if required parameters are present for AO estimation
  :param pargs: Parsed arguments
  :return:  True if valid, False otherwise
  """
  return pargs.surface_folder is not None and pargs.ao_folder is not None


def _validate_ao_estm_meshlab_params(pargs):
  """
  Check if all required parameters are present for AO estimation using meshlab
  :param pargs: Parsed arguments
  :return: True if valid, False otherwise
  """
  return (pargs.surface_folder is not None and
          pargs.ao_folder is not None and
          pargs.meshlabserver is not None and
          pargs.meshlab_project_file is not None)


def _generate_surface(pargs):
  """
  Generate surface from statistical shape model
  :param pargs: Parsed arguments
  """
  if pargs.generate_surface:
    # Validate arguments
    if _validate_gen_surf_params(pargs):
      logger.info('Sample surfaces ...')
      # Setup output
      if not _exists(pargs.surface_folder):
        _mkdir(pargs.surface_folder)
      # Load model
      model = Model()
      model.load(pargs.morphable_model)
      shp_model = model.shape
      n_id, n_exp = shp_model.dims
      # Sample border first
      cnt = 100
      p = np.zeros((n_id + n_id, 1), dtype=np.float32)
      for ik in range(n_id):
        # Each identity will be sampled at +/- 5 sigma
        for sigma_id in [5.0, -5.0]:
          p[ik, 0] = sigma_id
          for k, ek in enumerate(range(n_id, n_id + n_exp)):
            range_exp = [0.0, 5.0, -5.0] if k == 0 else [5.0, -5.0]
            for sigma_exp in range_exp:
              p[ek, 0] = sigma_exp
              # Generate sample
              surface = shp_model.generate(p, scaled=True)
              sname = 'surface{:06d}.obj'.format(cnt)
              surface.save(_join(pargs.surface_folder, sname))
              cnt += 1
              p[ek, 0] = 0.0
          p[ik, 0] = 0.0
      logger.info('Sampled {} surface on distribution\'s "border"'.format(cnt))
      # Sample randomly the same amount of surface
      for k in range(cnt):
        p = np.random.normal(size=(n_id + n_exp, 1)).astype(np.float32)
        surface = shp_model.generate(p, scaled=True)
        sname = 'surface{:06d}.obj'.format(cnt)
        surface.save(_join(pargs.surface_folder, sname))
        cnt += 1
      logger.info('Total number of sampled surface: {}'.format(cnt))
    else:
      logger.error('Missing parameters for surface generation. Required entries'
                   ': `--morphable_model`, `--surface_folder`')
  else:
    logger.warning('Skip shape generation...')


def _estimate_ao(pargs):
  """
  Compute Ambient Occlusion for a given set of surface
  :param pargs: Parsed command line arguments
  """
  if args.estimate_ao:
    if _validate_ao_estm_params(pargs):
      logger.info('Estimating ambient occlusions, this will take a while...')
      if not _exists(pargs.ao_folder):
        _mkdir(pargs.ao_folder)
      # Scan for meshes
      meshes = search_folder(pargs.surface_folder,
                             ext=['.obj', '.ply'],
                             relative=False)
      # Estimates AO
      for path in meshes:
        mesh = Mesh()
        if not mesh.load(path):
          gen = AOGenerator(mesh)
          gen.process(n_sample=pargs.ao_sample)
          # Save AO
          fname = _basename(path).replace('obj', 'ao')
          fname = _join(pargs.ao_folder, fname)
          gen.save(path=fname)
        else:
          logger.error('Can not open: {}'.format(path))
    else:
      msg = 'Missing parameters for Ambient Occlusion estimation. Required ' \
            'entries: `--surface_folder`, `--ao_folder`'
      logger.error(msg)
  else:
    logger.warning('Skip ambient occlusion estimation')


def _estimate_ao_meshlab(pargs):
  """
  Estimate AO using meshlabserver (i.e. SSAO)
  :param pargs:   Parsed arguments
  """
  if args.estimate_ao_meshlab:
    if _validate_ao_estm_meshlab_params(pargs):
      logger.info('Estimating ambient occlusions, this will take a while...')
      if not _exists(pargs.ao_folder):
        _mkdir(pargs.ao_folder)
      # Scan for meshes
      meshes = search_folder(pargs.surface_folder,
                             ext=['.obj', '.ply'],
                             relative=False)
      # Estimates AO
      output_mesh = _join(pargs.ao_folder, 'mesh.ply')
      for k, path in enumerate(meshes):
        if k % 1000 == 0:
          logger.info('Process: {}'.format(path))
        # Already computed ?
        ao_name = _join(pargs.ao_folder, _basename(path).replace('obj', 'ao'))
        if not _exists(ao_name):
          # Create command
          # cmd: meshlabserver -i <Mesh> -o <OMesh> -m vq -s <ProjectFile>
          cmd = '{} -i {} -o {} -m vq -s {}'.format(pargs.meshlabserver,
                                                    path,
                                                    output_mesh,
                                                    pargs.meshlab_project_file)
          cmd = cmd.split(' ')
          # Call meshlab
          p = Popen(cmd, stdout=PIPE, stderr=PIPE, universal_newlines=True)
          output, errors = p.communicate()
          if p.returncode == 0:
            # Load ply file
            pdata = PlyData.read(output_mesh)
            # Extra AO
            ao = np.asarray(pdata.elements[0]['quality'], dtype=np.float32)
            ao = ao.reshape(-1, 1)
            ao /= ao.max()  # normalize entry

            save_matrix(ao_name, ao)
          else:
            logger.error('Error while computing AO: {}'.format(errors))
    else:
      msg = 'Missing parameters for Ambient Occlusion estimation. Required ' \
            'entries: `--surface_folder`, `--ao_folder`, `--meshlabserver`,' \
            '`--meshlab_project_file`'
      logger.error(msg)
  else:
    logger.warning('Skip ambient occlusion estimation (meshlab)')


if __name__ == '__main__':
  # Parser
  p = ArgumentParser()

  # Generate surface
  p.add_argument('--generate_surface',
                 type=str2bool,
                 required=True,
                 help='Indicates if surfaces need to be generated')
  # Location where model are stored
  p.add_argument('--morphable_model',
                 type=str,
                 help='Statistical model for shape generation')
  # Where to place data
  p.add_argument('--surface_folder',
                 type=str,
                 help='Location where to place the generated surface')
  # Compute AO
  p.add_argument('--estimate_ao',
                 type=str2bool,
                 required=True,
                 help='Indicate if Ambient Occlusion needs to be computed')
  p.add_argument('--ao_folder',
                 type=str,
                 help='Location where to place the estimated AOs')
  p.add_argument('--ao_sample',
                 type=int,
                 default=5000,
                 help='Number of samples to draw when estimating AOs')
  # Meshlab AO
  p.add_argument('--estimate_ao_meshlab',
                 type=str2bool,
                 required=True,
                 help='Indicate if AO have to be estimated using meshlab')
  p.add_argument('--meshlabserver',
                 type=str,
                 help='Meshlabserver executable location')
  p.add_argument('--meshlab_project_file',
                 type=str,
                 help='Location where the AO project file is located '
                      '(i.e. *mlx)')
  # Parse
  args = p.parse_args()

  # Do stuff
  _generate_surface(pargs=args)
  _estimate_ao(pargs=args)
  _estimate_ao_meshlab(pargs=args)
