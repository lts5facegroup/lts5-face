#!/bin/bash
set -e

MODEL_DIR="/home/christophe/Documents/LTS5/Data/models"
DATA_DIR="/media/christophe/Data/Datasets"
DATA_FOLDER="MorphNet_224_cleaned_with_gt_lms"
DATE=$(date '+%d_%m_%Y')
OUTPUT_DIR="${MODEL_DIR}/morphnet_keras_joint_${DATE}"
CONFIG="config_joint.json"
MARGIN=0.3
IMG_SIZE=224

# Check if a mod is given
if [ "$#" -lt 1 ]; then
    echo "Wrong number of parameters"
    echo "$0 <mod>"
    echo "Possible mod:"
    echo "<PrepareTrain>: Prepare dataset for trainintg"
    echo "<PrepareEvaluation>: Prepare dataset for evaluation"
    echo "<Train>: Run training "
    echo "<TrainAndEvaluate>: Run training and evaluate model at the end"
    echo "<Evaluate>: Run evaluation (MICC, FaceWarehouse, LFW)"
    echo "<Clear>: Clear folder where model will be dumped"
    exit
fi

case "$1" in 
	"PrepareTrain")
    SFD="${MODEL_DIR}/sfd.npy"
    # Tracker for ibug landmarks, adrian bulat FAN
    TRACKER="${MODEL_DIR}/fan2d.pb"
    # Tracker for projected 3D landmarks, adrian bulat FAN
    TRACKER_PROJECTION="${MODEL_DIR}/fan2halfd.pb"

		echo "Start data preparation..."
		# Face detection
		# FaceWarehouse --fw_path ${DATA_DIR}/3D/FaceWarehouse
		# CelebA --celeba_path ${DATA_DIR}/CelebA
		# 300-VW --ibugvw ${DATA_DIR}/300VW
		# 300W-LP --ibugw_lp_path ${DATA_DIR}/300W_LP
		python prepare/detection_alignment.py       \
		  --sfd ${SFD}                              \
		  --fan ${TRACKER_PROJECTION}               \
		  --celeba_path ${DATA_DIR}/CelebA          \
		  --ibugvw ${DATA_DIR}/300VW                \
		  --ibugw_lp_path ${DATA_DIR}/300W_LP       \
		  --output ${DATA_DIR}/${DATA_FOLDER}       \
		  --size ${IMG_SIZE}                        \
		  --margin ${MARGIN}                        \
		  --use_landmarks_label Yes
		# Face segmentation
		python prepare/segmentation.py                  \
		  --input ${DATA_DIR}/${DATA_FOLDER}            \
		  --seg_bisenet ${MODEL_DIR}/face_parsing/bisenet_celeba_18_radam_ohem  \
		  --seg_cnn ${MODEL_DIR}/PSPNet/PSPNet_res18    \
		  --size ${IMG_SIZE}
		# Discard samples that have too bad reconstruction
		python prepare/false_positive_mining.py                     \
		  --data_dir ${DATA_DIR}/${DATA_FOLDER}                     \
		  --model_dir ${MODEL_DIR}/morphnet_keras_joint_23_10_2020  \
		  --output_dir ${DATA_DIR}/${DATA_FOLDER}                   \
		  --photo_threshold 0.2                                     \
		  --landmarks_threshold 4.5
		# Split
		python prepare/data_split.py                \
		  --input ${DATA_DIR}/${DATA_FOLDER}        \
		  --val_size 0.2                            \
		  --shard_size 2000                         \
		  --discard_file discard_rec.txt
		;;

  "PrepareEvaluation")
    SFD="${MODEL_DIR}/sfd.npy"
    MICC_DATASET="${DATA_DIR}/MICC"
    LFW_DATASET="${DATA_DIR}/LFW"
    FW_DATASET="${DATA_DIR}/3D/FaceWarehouse"
    MESHLAB_FILE="${DATA_DIR}/Deng_et_al/metadata/MoFA_sub/sub.mlx"
    echo "Start data preparation for MICC shape / LFW appearance  evaluation..."
    python prepare/evaluation.py                  \
      --micc_path ${MICC_DATASET}                 \
      --fw_path ${FW_DATASET}                     \
      --lfw_path ${LFW_DATASET}                   \
      --sfd_path ${SFD}                           \
      --meshlab_file ${MESHLAB_FILE}              \
      --meshlabserver /usr/bin/meshlabserver      \
      --margin ${MARGIN}                          \
      --img_size ${IMG_SIZE}                      \
      --output ${DATA_DIR}/MorphNetBenchmark224
    ;;

  "Evaluate")
    echo "Run shape evaluation protocol..."
    EVALUATION_FOLDER="/home/christophe/Documents/LTS5/Data/evaluation_morphnet"
    python evaluate/shape.py                                                \
      --micc_records ${DATA_DIR}/MoprhNetBenchmark_v2/micc_shape/records    \
      --facewarehouse_records ${DATA_DIR}/MorphNetBenchmark224_v2/fw_shape  \
      --micc_folder ${DATA_DIR}/MICC                                        \
      --morphnet_model_dir ${OUTPUT_DIR}                                    \
      --bfm_nose_idx 8191                                                   \
      --bfm_vertex_map ${DATA_DIR}/Deng_et_al/metadata/idx_vertex_map.txt   \
      --uniform_bfm_correspondence ${DATA_DIR}/Deng_et_al/metadata/mofa_to_out_topo.txt \
      --facewarehouse_mask ${DATA_DIR}/Deng_et_al/metadata/fw_evaluation_region.ply \
      --output ${EVALUATION_FOLDER}

    echo "Run appearance eevaluation protocol..."
    python evaluate/appearance.py                                           \
      --lfw_record ${DATA_DIR}/MorphNetBenchmark224_v2/appearance/records/image.tfrecords \
      --lfw_pairs ${DATA_DIR}/LFW/pairs.txt                                 \
      --morphnet_model_dir ${OUTPUT_DIR}                                    \
      --vgg_face_model ${MODEL_DIR}/vgg-face.npy                            \
      --output ${EVALUATION_FOLDER}
    ;;

	"Train")
		echo "Train .."
		LOCAL_DIR="/home/christophe/Documents/LTS5/Data/MorphNet_224"
		python train/train.py                                                 \
		  --config ${CONFIG}                                                  \
		  --train_input "${LOCAL_DIR}/records/train_*.tfrecords"              \
		  --validation_input "${LOCAL_DIR}/records/validation_*.tfrecords"    \
		  --model_dir ${OUTPUT_DIR}
		;;

  "TrainAndEvaluate")
    echo "Train .."
		LOCAL_DIR="/home/christophe/Documents/LTS5/Data/MorphNet_224"
		python train/train.py                                                 \
		  --config ${CONFIG}                                                  \
		  --train_input "${LOCAL_DIR}/records/train_*.tfrecords"              \
		  --validation_input "${LOCAL_DIR}/records/validation_*.tfrecords"    \
		  --model_dir ${OUTPUT_DIR}

    echo "Run shape evaluation protocol..."
    EVALUATION_FOLDER="/home/christophe/Documents/LTS5/Data/evaluation_morphnet"
    python evaluate/shape.py                                                \
      --micc_records ${DATA_DIR}/MoprhNetBenchmark_v2/micc_shape/records    \
      --facewarehouse_records ${DATA_DIR}/MorphNetBenchmark224_v2/fw_shape  \
      --micc_folder ${DATA_DIR}/MICC                                        \
      --morphnet_model_dir ${OUTPUT_DIR}                                    \
      --bfm_nose_idx 8191                                                   \
      --bfm_vertex_map ${DATA_DIR}/Deng_et_al/metadata/idx_vertex_map.txt   \
      --uniform_bfm_correspondence ${DATA_DIR}/Deng_et_al/metadata/mofa_to_out_topo.txt \
      --facewarehouse_mask ${DATA_DIR}/Deng_et_al/metadata/fw_evaluation_region.ply \
      --output ${EVALUATION_FOLDER}

    echo "Run appearance eevaluation protocol..."
    python evaluate/appearance.py                                           \
      --lfw_record ${DATA_DIR}/MorphNetBenchmark224_v2/appearance/records/image.tfrecords \
      --lfw_pairs ${DATA_DIR}/LFW/pairs.txt                                 \
      --morphnet_model_dir ${OUTPUT_DIR}                                    \
      --vgg_face_model ${MODEL_DIR}/vgg-face.npy                            \
      --output ${EVALUATION_FOLDER}

    ;;

  "GenerateResults")
    echo "Generate results..."
    MM_MODEL_PATH="$MODEL_DIR/basel_face_exp_model_deng_v3.bin"
    python viz_reconstruction.py --sample_folder $2 --mm_model_path $MM_MODEL_PATH --output $3 --selected_index 746 764 765 3286 3301 3302 3308 11833 14592 14597 17881 17900 19760 20994 21001 23031 23054 26550
    ;;

  "Clear")
      read -p "Clear previous model in: ${OUTPUT_DIR}. Continue [y/n] ?" CHOICE
      case "$CHOICE" in
        y|Y )
          rm -rf ${OUTPUT_DIR}
          ;;
        # Deal with other letter
        * ) echo "Stop current action"
          ;;
      esac
    ;;

	*)
		echo "Unknown mod"
		;;
esac
