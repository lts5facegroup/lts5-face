# coding=utf-8
"""
Reconstruction visualization tools for MorphNet
"""
from os.path import join, exists
from os import makedirs
from argparse import ArgumentParser
from imageio import imread, imsave, mimsave
import numpy as np
import tensorflow as _tf
from tensorflow.keras.layers import Input
from lts5.utils.tools import init_logger
from lts5.utils.tools import search_folder
from lts5.tensorflow_op.utils.device import initialize_device_memory
from lts5.tensorflow_op.layers import MorphableModel
from lts5.tensorflow_op.layers import SurfaceNormal
from lts5.tensorflow_op.layers import RigidTransform
from lts5.tensorflow_op.layers import ShLighting
from lts5.tensorflow_op.layers import ColorTransform
from lts5.tensorflow_op.layers import Renderer
from lts5.ogl import SHViewer
from lts5.utils import RodriguesFloat as Rodrigues

import matplotlib.pyplot as plt

__author__ = 'Christophe Ecabert'

logger = init_logger()
initialize_device_memory()


def _gather_samples(pargs):
  """
  Search for images + parameters
  :param pargs: Parsed arguments
  :return:  list of images
  """
  images = []
  params = []
  # Explicit samples ?
  if pargs.selected_index is not None:
    # Yes
    for idx in pargs.selected_index:
      im_name = pargs.file_pattern.format('org', idx, 'jpg')
      np_name = pargs.file_pattern.format('out', idx, 'npy')
      images.append(join(pargs.sample_folder, im_name))
      params.append(join(pargs.sample_folder, np_name))
  else:
    # Select all samples
    images = search_folder(pargs.sample_folder,
                           ext=['.jpg'],
                           skip=['out'],
                           relative=False)
    params = search_folder(pargs.sample_folder,
                          ext=['.npy'],
                          relative=False)
  return images, params


def _build_generator(filename, n_params):
  """
  Build generator
  :param filename:  Path to the parametric model
  :param n_params:  Number of input parameters
  :return:  Model
  """
  in_params = Input(shape=(n_params,),
                    dtype=_tf.float32,
                    name='MMParameters')
  # Split parameters
  w_shp = in_params[:, :124]
  w_tex = in_params[:, 124:204]
  w_illu = in_params[:, 204:231]
  w_ct = in_params[:, 231:238]
  rvec = in_params[:, 238:241]
  tvec = in_params[:, 241:]

  # Morphable model
  mm_model = MorphableModel(path=filename)
  vertex, aldebo, tri = mm_model(inputs=[w_shp, w_tex])
  # Normal
  surf_n = SurfaceNormal()
  normals, _ = surf_n(inputs=[vertex, tri])
  # Rigid
  rigid_t = RigidTransform()
  vertex = rigid_t(inputs=[vertex, rvec, tvec])
  normals = rigid_t(inputs=[normals, rvec])
  # Lighting
  light = ShLighting(name='SHLighting')
  sh_color = light(inputs=[aldebo, normals, w_illu])
  # Color transform
  color_trsfrm = ColorTransform(reg_cfg=None)
  f_color = color_trsfrm([sh_color, w_ct])
  gen = _tf.keras.Model(inputs=in_params, outputs=[vertex,
                                                   normals,
                                                   aldebo,
                                                   f_color,
                                                   tri])
  # Done
  return gen


def _build_decoder(n_vertex=53490,
                   n_tri=106466,
                   size=227,
                   focal=525.0,
                   near=1e2,
                   far=1e6):
  """
  Build renderer
  :param n_vertex: Number of vertex
  :param n_tri: Number of triangle
  :param size:  Image size
  :param focal: Focal length
  :param near:  Near plane
  :param far:   Far plane
  :return:  Decoder model
  """
  vertex = Input(shape=(3 * n_vertex,), dtype=_tf.float32)
  normal = Input(shape=(3 * n_vertex,), dtype=_tf.float32)
  color = Input(shape=(3 * n_vertex,), dtype=_tf.float32)
  tri = Input(shape=(n_tri,), dtype=_tf.int32)
  bg = Input(shape=(size, size, 3), dtype=_tf.float32)
  renderer = Renderer(width=size,
                      height=size,
                      focal=focal,
                      near=near,
                      far=far,
                      n_face=68620,
                      visible=False,
                      extra_output=False)
  img = renderer([vertex, normal, color, tri, bg])
  # Model
  dec = _tf.keras.Model(inputs=[vertex, normal, color, tri, bg],
                        outputs=img,
                        name='Renderer')
  return dec


def _build_viewer(pargs):
  """
  Build
  :param pargs: Parse arguments
  :return:  ShViewer instance
  """
  viewer = SHViewer(width=pargs.size, height=pargs.size, step=50)
  viewer.background_color = (1.0, 1.0, 1.0)
  return viewer


def _render_geometry(vertex, normal, tri, background, renderer):
  """

  :param vertex:  Vertex
  :param normal:  Normals
  :param tri: Triangles
  :param background: Background
  :param renderer: Renderer instance
  :return:  Image
  """
  # Define color
  nv = _tf.shape(vertex)[1] / 3
  chanel = _tf.ones((1, nv))
  red = chanel * 1.0
  green = chanel * 0.921568627
  blue = chanel * 0.819607843
  color = _tf.concat([red, green, blue], axis=0)
  color = _tf.transpose(color)
  n = _tf.reshape(normal, (-1, 3))
  light_dir = _tf.convert_to_tensor([0.0, 0.0, 1.0], dtype=_tf.float32)
  cos_theta = _tf.reduce_sum(n * light_dir, axis=-1)
  cos_theta = _tf.clip_by_value(cos_theta, 0.0, 1.0)
  cos_theta = _tf.reshape(cos_theta, (-1, 1))
  color = color * cos_theta
  color = _tf.reshape(color, (1, -1))
  return renderer([vertex, normal, color, tri, background])


def _render_aldebo(vertex, normal, tri, color, background, renderer):
  """
  Render albedo
  :param vertex:  Vertex
  :param normal:  Normals
  :param tri:     Triangles
  :param background:  Background
  :param renderer:  Render instance
  :return:  Image
  """
  return renderer([vertex, normal, color, tri, background])


def _render_light(params, color, viewer):
  """
  Render lighting
  :param params:
  :param color:
  :param viewer:
  :return:
  """
  w_illu = params[:, 204:231].reshape(3, -1)
  bcolor = np.tile(color, (viewer.dims, 1))
  _, sh_image = viewer.generate(aldebo=bcolor, sh_coef=w_illu)
  return sh_image


def _render_fused(vertex, normal, color, tri, background, renderer):
  """
  Render fused image
  :param vertex:      Vertex
  :param normal:      Normals
  :param color:       Color
  :param tri:         Triangle
  :param background:  Background image
  :param renderer:    Renderer instance
  :return:  Fused image
  """
  return renderer([vertex, normal, color, tri, background])


def _render_gif(params, background, gen, dec, step=30, angular_range=30.0):
  """
  Generate GIF
  :param params:        Model parameters
  :param background:    Backgrounds
  :param gen:           Generator
  :param dec:           Decoder
  :param step:          Number of step to take
  :param angular_range: Rotation range
  :return:  List of images
  """
  p = np.copy(params)
  ang_step = angular_range / step
  images = []
  n_step = int(2 * step)
  for s in range(n_step):
    s -= step
    angle = s * ang_step
    r = Rodrigues(axis=(0.0, 1.0, 0.0), angle=np.deg2rad(angle))
    # Define new parameters
    p[:, -6] = r.x
    p[:, -5] = r.y
    p[:, -4] = r.z
    p[:, -3] = 0.0        # tx
    p[:, -2] = -5.0       # ty
    p[:, -1] = -525.0     # tz
    # Generate + Render
    vertex, normals, aldebo, sh_color, tri = gen(p)
    img = dec([vertex, normals, sh_color, tri, background])
    images.append(_tf.cast(img * 255.0, _tf.uint8).numpy()[0, ...])
  # Replicate with reverse motion
  images = images + [x for x in reversed(images[:-2])]
  return images


def generate_viz(pargs):
  """
  Generate visualization outputs
  :param pargs: Parsed arguments
  """

  # Scan for data
  images, params = _gather_samples(pargs=pargs)
  # Check output
  if not exists(pargs.output):
    makedirs(pargs.output)
  # Process them
  gen = _build_generator(filename=pargs.mm_model_path, n_params=pargs.n_params)
  dec = _build_decoder(size=pargs.size)
  sh_viewer = _build_viewer(pargs=pargs)
  bg = _tf.convert_to_tensor([[[1.0, 1.0, 1.0]]], dtype=_tf.float32)
  bg = _tf.tile(bg, multiples=[pargs.size, pargs.size, 1])
  bg = _tf.expand_dims(bg, 0)

  # Go through all samples and generate viz
  for k, (im_path, p_path) in enumerate(zip(images, params)):
    logger.info('Process: %s', im_path)
    # Load image + params
    image_org = np.asarray(imread(im_path), dtype=np.uint8)
    model_p = np.load(p_path).reshape(1, -1)
    # Define component
    # -----------------------------------------------------------
    vertex, normal, aldebo, color, tri = gen(model_p)
    # Geometry
    # -----------------------------------------------------------
    image_geo = _render_geometry(vertex=vertex,
                                 normal=normal,
                                 tri=tri,
                                 background=bg,
                                 renderer=dec)
    # Aldebo
    # -----------------------------------------------------------
    image_aldebo = _render_aldebo(vertex=vertex,
                                  normal=normal,
                                  tri=tri,
                                  color=aldebo,
                                  background=bg,
                                  renderer=dec)
    # Lighting
    # -----------------------------------------------------------
    bfm_median_skin_color = np.asarray([0.7176471, 0.5137255, 0.4], np.float32)
    image_light = _render_light(params=model_p,
                                color=bfm_median_skin_color,
                                viewer=sh_viewer)
    # All
    # -----------------------------------------------------------
    img_bg = np.expand_dims(image_org, 0).astype(np.float32) / 255.0
    img_bg = _tf.convert_to_tensor(img_bg)
    image_fused = _render_fused(vertex=vertex,
                                  normal=normal,
                                  tri=tri,
                                  color=color,
                                  background=img_bg,
                                  renderer=dec)
    # Gif
    # -----------------------------------------------------------
    image_gifs = _render_gif(params=model_p,
                             background=bg,
                             gen=gen,
                             dec=dec,
                             step=30,
                             angular_range=30.0)

    # Dump images
    # -----------------------------------------------------------
    # Input
    im = image_org
    bname = 'input_{:06d}.jpg'.format(k)
    fname = join(pargs.output, bname)
    imsave(fname, im)
    # Geometry - float [0,1]
    im = _tf.cast(image_geo * 255.0, _tf.uint8).numpy()[0, ...]
    bname = 'geometry_{:06d}.jpg'.format(k)
    fname = join(pargs.output, bname)
    imsave(fname, im)
    # Aldebo - float [0,1]
    im = _tf.cast(image_aldebo * 255.0, _tf.uint8).numpy()[0, ...]
    bname = 'aldebo_{:06d}.jpg'.format(k)
    fname = join(pargs.output, bname)
    imsave(fname, im)
    # Lighting
    im = (image_light[..., :3] * 255.0).astype(np.uint8)
    bname = 'lighting_{:06d}.jpg'.format(k)
    fname = join(pargs.output, bname)
    imsave(fname, im)
    # Fused
    im = _tf.cast(image_fused * 255.0, _tf.uint8).numpy()[0, ...]
    bname = 'fused_{:06d}.jpg'.format(k)
    fname = join(pargs.output, bname)
    imsave(fname, im)
    # GIF
    im = image_gifs
    bname = 'animation_{:06d}.gif'.format(k)
    fname = join(pargs.output, bname)
    mimsave(fname, im, duration=0.01)



if __name__ == '__main__':
  # Parser
  p = ArgumentParser('Reconstruction visualization tools')

  # Sample folder
  p.add_argument('--sample_folder',
                 type=str,
                 required=True,
                 help='Location where samples are stored')
  p.add_argument('--file_pattern',
                 type=str,
                 default='sample_{}_{:06d}.{}',
                 help='Glob pattern for gathering samples (i.e. original + '
                      'numpy file')
  p.add_argument('--selected_index',
                 type=int,
                 nargs='*',
                 default=None,
                 help='Explicit selection of samples')
  # Morphable model
  p.add_argument('--mm_model_path',
                 type=str,
                 required=True,
                 help='Location where morphable model is stored')
  p.add_argument('--n_params',
                 type=int,
                 default=240,
                 help='Total number of parameters for the decoder')
  p.add_argument('--size',
                 type=int,
                 default=227,
                 help='Image size')
  # Output location
  p.add_argument('--output',
                 type=str,
                 required=True,
                 help='Location where to place the generated images')
  # Parse
  args = p.parse_args()
  # Run Viz
  generate_viz(pargs=args)