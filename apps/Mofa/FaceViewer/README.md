# Installation

Dependencies can be installed using 

```bash
$ pip install -r requirements.txt
```

from the root folder.

# Usage

Once everything has been set up, launch the application with

```bash
$ python image_viewer.py -- -f <Path>
```

where `<Path>` refer to the location where face patches have been extracted.

