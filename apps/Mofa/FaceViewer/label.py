""" Label object """
from enum import Enum as _enum, unique as _unique

@_unique
class Status(_enum):
  """ Label attributed """
  GOOD = 1
  OCCLUSION = 2
  GLASSES = 4
  BEARD = 8
  BAD_POSE = 16
  BAD_COLOR = 32
  RESOLUTION = 64
  NO_FACE = 128


class LabelItem:
  """ Object representing the outcome of the annotation process """

  def __init__(self, item_id):
    """
    Constructor

    :param item_id: Id of the object
    """
    self._id = item_id
    self._filename = ''
    self._status = Status.GOOD

  @property
  def id(self):
    return self._id

  @property
  def filename(self):
    return self._filename

  @filename.setter
  def filename(self, fname):
    self._filename = fname

  @property
  def status(self):
    return self._status

  @status.setter
  def status(self, s):
    self._status = s