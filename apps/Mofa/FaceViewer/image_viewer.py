""" Interactive Image viewer for manual annotation """
import argparse as _argparse
from os.path import exists as _fexists
import pickle as _pickle
# Set fixed windows size, disable resizing
from kivy.config import Config as _Config
_Config.set('graphics', 'resizable', 0)
_Config.set('graphics', 'width', '640')
_Config.set('graphics', 'height', '320')


from kivy.app import App as _App
from kivy.uix.relativelayout import RelativeLayout as _RelativeLayout
from kivy.properties import StringProperty as _StringProperty
from kivy.properties import NumericProperty as _NumericProperty
from kivy.core.window import Window as _Window

from lts5.utils import search_folder
from lts5.utils import init_logger
from .label import LabelItem

# Logger
#_Logger.disabled=True
logger = init_logger()
logger.warning('App should be called as: {} -- -f <folder>'.format(__file__))


def _find_widget(root, name):
  """
  Search for widget with a given name
  :param root:  Root widget where to start to look for
  :param name:  Widget's name of interest
  :return:  List of widget matching the given name or empty list
  """

  def _check_w_name(w, name):
    if hasattr(w, 'name'):
      # Does name match ?
      return name in w.name
    return False

  w = []
  # List of element to check
  stack = [root]
  # process till reach end
  while len(stack):
    # Get first item
    top_w = stack.pop()
    if _check_w_name(top_w, name):
      # Name match
      w.append(top_w)
    # Push children
    stack.extend(top_w.children)
  # Done, stack empty
  return w


class BoxSelector(_RelativeLayout):
  """ Object holding a checkbox + label for convenience """
  label = _StringProperty('Box')     # Text to display next to the checkbox
  value = _NumericProperty(0)        # Label value
  pass


class ImageViewerApp(_App):
  """ Container for application based on kivy framework """

  def __init__(self, folder):
    """
    Constructor

    :param: folder  Location where images are stored
    """
    # Call constructor
    super(ImageViewerApp, self).__init__()
    # Scan for images
    self._folder = folder.replace('/', '')
    self._images = search_folder(folder=folder, ext='.jpg', relative=False)
    # Annotation exist? else create empty list
    a_name = self._folder + '_ann.pkl'
    self._labels = _pickle.load(open(a_name, 'rb')) if _fexists(a_name) else []
    # Sort label by ID
    self._labels.sort(key=lambda x: x.id)
    self._idx = 0 if not len(self._labels) else len(self._labels) - 1
    # Widgets
    self._w_img = None
    self._w_boxes = []
    self._w_btn = None

  def build(self):
    # Build interface
    root = self.root
    # Setup btn
    self._w_btn = _find_widget(root=root, name='btn')[0]
    self._w_btn.bind(on_press=self._save_callback)
    # Setup box
    self._w_boxes = _find_widget(root=root, name='box_')
    self._w_boxes.sort(key=lambda x: x.value)
    # Setup image
    self._w_img = _find_widget(root=root, name='image')[0]
    # Restore or start new ?
    if len(self._labels):
      # restore
      self._restore_annotation(idx=self._idx)
    else:
      # Start new, load first image
      if self._images:
        self._w_img.source = self._images[0]
        self._w_img.reload()
    # Add keybindings
    _Window.bind(on_keyboard=self._on_keyboard)

  def on_pause(self):
    """ Fired when the application is paused by the OS, Dump annotation """
    self._save_label()

  def on_stop(self):
    """ Fired when the application close. Dump annotation """
    self._save_label()

  def _on_keyboard(self, instance, key, scancode, codepoint, modifiers):
    # key `s` trigger the button callback
    if codepoint == 's':
      a = 0
      self._save_callback(instance=self._w_btn)

  def _save_label(self):
    """ Dump labels into pickle file """
    a_name = self._folder + '_ann.pkl'
    with open(a_name, 'wb') as w:
      logger.info('Dump labels in {}'.format(a_name))
      _pickle.dump(self._labels, w)

  def _save_callback(self, instance):
    # Check entry
    if self._entry_is_valid():
      # Create new annotation
      if self._idx < len(self._images):
        # Not reach the last entry
        lbl = LabelItem(self._idx)
        lbl.filename = self._images[self._idx]
        lbl.status = sum([x.value for x in self._w_boxes if x.ids['box'].active])
        if self._idx < len(self._labels):
          self._labels[self._idx] = lbl
        else:
          self._labels.append(lbl)
        # Increase index
        self._idx += 1
        # Load next image
        self._load_next_entry()
        # Dump label every 100 annotations
        if self._idx % 100 == 0:
          self._save_label()
      else:
        logger.info('Reach end of samples')

  def _restore_annotation(self, idx):
    """
    Update GUI elements with a given annotation
    :param idx: Index of the annotation
    """
    lbl = self._labels[idx]
    # Set image
    self._w_img.source = lbl.filename
    self._w_img.reload()
    # Set box
    for i, box in enumerate(self._w_boxes):
      # Checked ?
      if lbl.status & int(2 ** i):
        # Yes
        box.ids['box'].active = True

  def _entry_is_valid(self):
    """
    Check if at least one entry has been filled
    :return: True if valid, False otherwise
    """
    for box in self._w_boxes:
      if box.ids['box'].active:
        return True
    return False

  def _load_next_entry(self):
    """
    Load the next image to annotate if any
    """
    if self._idx < len(self._labels):
      self._restore_annotation(idx=self._idx)
    elif self._idx < len(self._images):
      # Next image
      self._w_img.source = self._images[self._idx]
      self._w_img.reload()
      # Clear checkbox
      for box in self._w_boxes:
        box.ids['box'].active = False


if __name__ == '__main__':
  # Arg parser
  p = _argparse.ArgumentParser(description='Annotation tool')
  # Location where raw images are stored
  p.add_argument('-f',
                 type=str,
                 dest='folder',
                 required=True,
                 help='Location where samples are stored')
  args = p.parse_args()
  # Create app
  ImageViewerApp(folder=args.folder).run()
