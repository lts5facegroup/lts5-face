# coding=utf-8
""" Code for evaluating the quality of the shape reconstruction """
from os import makedirs as _mkdir

from os.path import exists as _exists
from os.path import join as _join
from os.path import dirname, basename, sep
from subprocess import Popen, PIPE
from argparse import ArgumentParser
import numpy as np
import tensorflow as tf
from pickle import dump, load
from imageio import imsave
from data_loader import MICCShapeLoader
from lts5.utils.tools import search_folder, init_logger, str2bool
from lts5.geometry import MeshFloat as Mesh
from lts5.tensorflow_op.utils import initialize_device_memory
from eval.model import ModelJointFeat, ModelSeparateFeat
from eval.metrics import Metric, PartitionMetrics
from eval.alignment import AlignmentError, ICPAlignmentError

logger = init_logger()
initialize_device_memory()










class MICCEvaluator(ShapeEvaluator):
  """ Shape evaluator for MICC dataset """

  def __init__(self, pargs, logger, verbose=False):
    """
    Constructor
    :param pargs: Parsed command line arguments
    :param logger:  Logger instance
    :param verbose:   If True add extra output
    """
    self.pargs = pargs
    self.log = logger
    self.verbose = verbose
    self.micc_nose_idx = _micc_nose_idx





  def run_evaluation(self, output, icp_iter=20):
    """
    Compute surface evaluation metrics. Apply first rigid ICP with isotropic
    scaling
    :param output:    Location where to dump the data
    :param icp_iter:  Maximum number of iteration to run for ICP
    """
    self.log.info('Run shape evaluation')
    # setup output
    fname = _join(output,
                  basename(self.pargs.morphnet_model_dir),
                  'result.pkl')
    # if not _exists(fname):
    if _exists(fname):
      results = []
      for part in ['cooperative', 'indoor', 'outdoor']:
        logger.info('Evaluate partition: {}'.format(part))
        part_results = PartitionMetrics()
        for s_id in range(1, 54):
          if (s_id - 1) % 10 == 0:
            logger.info('Process subject {}'.format(s_id))
          # Load ground truth
          gt_path = _join(output,
                          'cropped_true',
                          'subject{:02d}.obj'.format(s_id))
          # Predicted mesh
          pred_path = _join(output,
                            basename(self.pargs.morphnet_model_dir),
                            part,
                            'subject_{:02d}'.format(s_id),
                            'full_shape.ply')
                            # 'cropped_shape.ply')
          if part == 'cooperative':
            pred_path = '/home/christophe/Documents/LTS5/Data/dbg/optim_mesh0.ply'
          else:
            pred_path = '/home/christophe/Documents/LTS5/Data/dbg/optim_mesh1.ply'
          # Compute error
          register = ICPAlignmentError(pred_path, gt_path)
          register.align(n_iter=25)
          res = register.error(nose_idx=self.pargs.bfm_nose_idx,
                               distance=self.pargs.crop_distance)
          part_results.add(s_id, res)
          # Color map
          #register.source.save(pred_path.replace('cropped_shape', 'error_map'))

          break

        results.append(part_results)
      # Dump result for backup
      dump(results, open(fname, 'wb'))

    else:
      logger.warning('Use cached results')
      results = load(open(fname, 'rb'))
    # Print stuff
    self.print_results(results, ['cooperative', 'indoor', 'outdoor'])


class FaceWarehouseEvaluator(ShapeEvaluator):
  """ Shape evaluator for FaceWarehouse dataset """

  def __init__(self,
               pargs,
               logger,
               image_sz,
               verbose=False):
    """
    Constructor
    :param pargs: Parsed command line arguments
    :param logger:  Logger instance
    :param image_sz: Image dimension
    :param verbose:   If True add extra output
    """
    self.pargs = pargs
    self.log = logger
    self.verbose = verbose
    self.img_sz = image_sz

  def run_reconstruction(self, model, output):
    """
    Run face reconstruction for the underlying dataset
    :param model:     Model to use for reconstructing the surface, type of
                      `EvaluationModel` instance
    :param output:    Root location where to place the data

    """
    self.log.info('Run FaceWarehouse shape reconstruction...')

    # Destination folder
    dest_folder = _join(output,
                        basename(self.pargs.morphnet_model_dir),
                        'reconstruction')
    if not _exists(dest_folder):
      _mkdir(dest_folder)

    # Create dataset
    # ------------------------------------------------
    dset = self._create_dataset(self.img_sz)
    for image, filename in dset:
      # Get latent code
      embedding, w_shp, w_tex, rendering = model.predict(image)
      embedding = embedding.numpy()
      w_shp = w_shp.numpy()
      rendering = rendering.numpy()
      filename = filename.numpy()[0].decode()
      # Dump mesh, rendering, w_shp
      m = _create_mesh(model,
                       w_shp,
                       model.cfg.model.ns,
                       model.cfg.model.nt,
                       with_exp=True)
      mesh_name = filename.replace('.jpg', '.ply')
      m.save(_join(dest_folder, 'rec_{}'.format(mesh_name)))
      # Dump image
      img = rendering[0, ..., :3]
      img = (img * 255.0).astype(np.uint8)
      imsave(_join(dest_folder, 'rec_{}'.format(filename)), img)
      emb_name = _join(dest_folder,
                       'rec_{}'.format(mesh_name.replace('.ply', '.npy')))
      np.save(emb_name, w_shp, allow_pickle=True)

  def _create_dataset(self,
                      img_sz,
                      batch_size=1):
    """
    Create tensorflow dataset for a given image folder
    :param img_sz: Image size
    :param batch_size: Batch size
    :return: tf.data.Dataset instance ready to be used
    """

    def _load_image(path):
      # Define filename
      filename = tf.strings.split(path, sep)[-1]
      img_str = tf.io.read_file(path)  # Raw image string
      img = tf.io.decode_jpeg(img_str)
      img.set_shape([img_sz, img_sz, 3])
      img = tf.cast(img, tf.float32)
      return img, filename

    pattern = _join(self.pargs.facewarehouse_folder, 'images', '*.jpg')
    # pattern = '/home/christophe/Documents/LTS5/apps/Mofa/periodic_inference/*.jpg'
    dset = tf.data.Dataset.list_files(pattern, shuffle=False)
    dset = (dset.map(_load_image, tf.data.experimental.AUTOTUNE)
            .batch(batch_size, False)
            .prefetch(tf.data.experimental.AUTOTUNE))
    return dset

  def run_cropping(self, output, dist):
    """
    Run mesh cropping in order to extract a region of `dist` mm from the nose
    tip
    :param output:  Location where to place the data
    :param dist:    Cropping distance
    """
    self.log.info('Run FaceWarehouse shape cropping/alignment ...')
    # Destination folder
    dest_folder = _join(output,
                        basename(self.pargs.morphnet_model_dir),
                        'gt_aligned')
    rec_folder = _join(output,
                       basename(self.pargs.morphnet_model_dir),
                       'reconstruction')
    if not _exists(dest_folder):
      _mkdir(dest_folder)
    # Align ground truth to reconstruction
    gt_path = _join(self.pargs.facewarehouse_folder, 'meshes')
    gt_meshes = search_folder(folder=gt_path, ext=['.obj'], relative=False)
    for k, path in enumerate(gt_meshes):
      filename = basename(path)
      if k % 10 == 0:
        self.log.info('Process mesh: {}'.format(filename))
      # Target == reconstruction
      # Source == ground truth
      fname = 'rec_' + filename.replace('.obj', '.ply')
      target_path = _join(rec_folder, fname)
      source_path = path
      # Destination
      fname = fname.replace('rec_', 'aligned_')
      aligned_path = _join(dest_folder, fname)
      if not _exists(aligned_path):
        # Aligned both
        register = ICPAlignmentError(source=source_path, target=target_path)
        register.align(n_iter=25)
        aligned_source = register.transform()
        aligned_source.normal = np.asarray([], dtype=np.float32)
        aligned_source.save(aligned_path)


def _evaluate(pargs, verbose=False):
  """
  Run shape evaluation
  :param pargs:   Parsed arguments
  :param verbose: If `True` add extra output
  """

  # Model
  # ------------------------------------------------
  model = ModelSeparateFeat(pargs.morphnet_model_dir)

  # MICC EValuation
  # ------------------------------------------------
  micc_output = _join(pargs.output, 'micc')
  micc_eval = MICCEvaluator(pargs=pargs,
                            logger=logger,
                            verbose=verbose)
  # micc_eval.run_reconstruction(model, micc_output)
  # micc_eval.run_cropping(micc_output, pargs.crop_distance)
  micc_eval.run_evaluation(micc_output)

  # # FaceWarehouse EValuation
  # # ------------------------------------------------
  fw_output = _join(pargs.output, 'facewarehouse')
  # fw_eval = FaceWarehouseEvaluator(pargs=pargs,
  #                                  logger=logger,
  #                                  image_sz=model.cfg.rendering.width,
  #                                  verbose=verbose)
  # fw_eval.run_reconstruction(model, fw_output)
  # fw_eval.run_cropping(fw_output, pargs.crop_distance)
  # fw_eval.run_evaluation(fw_output)


if __name__ == '__main__':

  p = ArgumentParser()
  # Dataset
  p.add_argument('--micc_records',
                 type=str,
                 required=True,
                 help='Location where MICC tfrecords are stored')
  p.add_argument('--micc_folder',
                 type=str,
                 required=True,
                 help='Location where MICC dataset is stored')
  p.add_argument('--facewarehouse_folder',
                 type=str,
                 required=True,
                 help='Root location where FaceWarehouse dataset is stored')
  # Network configuration
  p.add_argument('--morphnet_model_dir',
                 type=str,
                 required=True,
                 help='Model location')
  # BFM nose tip index
  p.add_argument('--bfm_nose_idx',
                 type=int,
                 default=8320,
                 help='Vertex index of the nose tip')
  p.add_argument('--facewarehouse_nose_idx',
                 type=int,
                 default=8972,
                 help='Vertex index of the nose tip')
  p.add_argument('--crop_distance',
                 type=float,
                 default=95,
                 help='Cropping distance from the nose tip')
  p.add_argument('--do_reconstruction',
                 type=str2bool,
                 nargs='?',
                 default=True,
                 help='Need to run face reconstruction first.')
  # Meshlab_server path
  p.add_argument('--meshlabserver',
                 type=str,
                 required=True,
                 help='Path to the meshlab server executable')
  p.add_argument('--micc_meshlab_script',
                 type=str,
                 required=True,
                 help='Path to the densification script for MICC samples'
                      ' (*.mlx)')
  # Output
  p.add_argument('--output',
                 type=str,
                 required=True,
                 help='Location where to place the results')

  args = p.parse_args()
  _evaluate(pargs=args, verbose=True)

