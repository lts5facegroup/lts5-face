# coding=utf-8
"""
Build background prior (histogram) from patches. Patches do not include face
region.

Inputs:
    - Folders containing background patches
    - Number of bins per channel

Output:
    - Background prior as a histogram
"""

import argparse
from lts5.utils.tools import check_py_version
from lts5.utils.tools import init_logger
from lts5.tensorflow_op.probability.pixel_evaluator import HistogramPixelEvaluator

__author__ = 'Christophe Ecabert'

check_py_version(3, 0)
logger = init_logger()

if __name__ == '__main__':
  # Parser
  desc = 'Build data prior'
  parser = argparse.ArgumentParser(description=desc)

  # Folder where patches are stored
  parser.add_argument('-f',
                      type=str,
                      required=True,
                      dest='folder',
                      help='Folder where background patches are stored')
  # Number of bin per channel
  parser.add_argument('-n',
                      type=int,
                      required=False,
                      default=25,
                      dest='n_bins',
                      help='Number of bins per channels')
  # Output location
  parser.add_argument('-o',
                      type=str,
                      required=True,
                      dest='output',
                      help='Folder where to place the learned prior')

  # parse
  args = parser.parse_args()
  # Compute histogram
  hist, bins = HistogramPixelEvaluator.build_from_data(folder=args.folder,
                                                       bins_per_channel=25,
                                                       ofile=args.output)




