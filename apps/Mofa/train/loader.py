#coding=utf-8
""" Data loader for training step """
import tensorflow as tf
import tensorflow.keras.layers as kl
import tensorflow_addons as tfa
from tensorflow_addons.image import rotate
from lts5.tensorflow_op.data.data_io import DataProvider
from lts5.tensorflow_op.layers.image import GaussianBlur

__author__ = 'Christophe Ecabert'

# Landmark index flip when applying horizontal flip
LANDMARKS_FLIP_INDEXES = (16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1,
                          0, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 27, 28, 29,
                          30, 35, 34, 33, 32, 31, 45, 44, 43, 42, 47, 46, 39,
                          38, 37, 36, 41, 40, 54, 53, 52, 51, 50, 49, 48, 59,
                          58, 57, 56, 55, 64, 63, 62, 61, 60, 67, 66, 65)

AUTOTUNE = tf.data.experimental.AUTOTUNE


class ColorJitter:
  """ Randomly apply color transformation on input data. Type of color
   transformations are change of:
      - Brightness
      - Contrast
      - Saturation
  """

  def __init__(self, brightness, saturation, contrast):
    """
    Constructor
    :param brightness:  Max brightness adjustment, must be in [0, 1) or None to
                        disable
    :param saturation:  float, Max brightness saturation, must be in [0, 1)OR
                        tuple, (upper, lower) range OR
                        None to disable
    :param contrast:    Max contrast adjustment, must be in [0, 1) OR
                        tuple, (upper, lower) range OR
                        None to disable
    """
    self.brightness = None
    self.saturation = None
    self.contrast = None
    if brightness is not None:
      self.brightness = max(min(brightness, 1.0), 0.0)
    if saturation is not None:
      if isinstance(saturation, tuple):
        self.saturation = (saturation[0], saturation[1])
      else:
        value = max(min(saturation, 1.0), 0.0)
        self.saturation = (1.0 - value, 1.0 + value)
    if contrast is not None:
      if isinstance(contrast, tuple):
        self.contrast = (contrast[0], contrast[1])
      else:
        value = max(min(contrast, 1.0), 0.0)
        self.contrast = (1.0 - value, 1.0 + value)

  def run(self, image, mask, landmarks, label):
    """
    Perform color augmentation
    :param image:     Image to transform
    :param mask:      Segmentation mask
    :param landmarks: Face landmarks
    :param label:     True image
    :return:  Transform images, mask, landmarks, label
    """
    with tf.xla.experimental.jit_scope():
      _im = image
      if self.brightness:
        _im = tf.image.random_brightness(_im, self.brightness)
      if self.contrast:
        _im = tf.image.random_contrast(_im,
                                       lower=self.contrast[0],
                                       upper=self.contrast[1])
      if self.saturation:
        _im = tf.image.random_saturation(_im,
                                         lower=self.saturation[0],
                                         upper=self.saturation[1])
      return _im, mask, landmarks, label


def _gauss_kernel(channels, kernel_size, sigma):
  """
  Create a gaussian kernel
  :param channels:      Number of channel in the image
  :param kernel_size:   Kernel dimensions
  :param sigma:         Standard deviation
  :return:  Filter kernel
  """
  ax = tf.range(-kernel_size // 2 + 1.0, kernel_size // 2 + 1.0)
  xx, yy = tf.meshgrid(ax, ax)
  kernel = tf.exp(-(xx ** 2 + yy ** 2) / (2.0 * sigma ** 2))
  kernel = kernel / tf.reduce_sum(kernel)
  kernel = tf.tile(kernel[..., tf.newaxis], [1, 1, channels])
  return kernel


class RandomBlur:

  def __init__(self,
               upper_stddev=1.0,
               ksize=3,
               ):
    """
    Constructor
    :param sigmas: Maximum possible sigma
    :param ksize: int, kernel dimension
    """
    self.stddev = upper_stddev
    self.ksize = ksize

  def run(self, image, mask, landmarks, label):
    """
    Perform blur augmentation
    :param image:     Image to transform
    :param mask:      Segmentation mask
    :param landmarks: Face landmarks
    :param label:     True image
    :return:  Transformed images, mask, landmarks, label
    """
    with tf.xla.experimental.jit_scope():
      # Select sigma
      std = tf.random.uniform([],
                              minval=0,
                              maxval=self.stddev,
                              dtype=tf.float32)
      # Convert to rank4 tensor
      in_rank = image.shape.rank  # Conv2D needs 4D Tensor
      if in_rank == 3:
        image = tf.expand_dims(image, 0)
      # Apply blur
      _img = self._blur(image=image, ksize=self.ksize, sigma=std)
      # Convert back to rank3 if needed
      if in_rank == 3:
        _img = tf.squeeze(_img, 0)
      # Done
      return _img, mask, landmarks, label

  @staticmethod
  def _blur(image, ksize, sigma):
    """
    Blur a given image
    :param image:   Image to be blurred
    :param ksize:   Kernel size
    :param sigma:   Standard deviation
    :return:  Blurred image
    """
    kernel = _gauss_kernel(image.shape[-1], ksize, sigma)
    kernel = kernel[..., tf.newaxis]
    return tf.nn.depthwise_conv2d(image, kernel, [1, 1, 1, 1], padding='SAME',
                                  data_format='NHWC')


class RandomGaussianNoise:

  def __init__(self, sigmas=(0.0, 0.01, 0.05, 0.1)):
    """
    Constructor
    :param sigmas: List of blur sigmas to select from (i.e. variance)
    """
    self.filters = []
    for sigma in sigmas:
      self.filters.append(kl.GaussianNoise(stddev=sigma**0.5))

  def run(self, image, mask, landmarks, label):
    """
    Perform gaussian noise augmentation (additive)
    :param image:     Image to transform
    :param mask:      Segmentation mask
    :param landmarks: Face landmarks
    :param label:     True image
    :return:  Transformed images, mask, landmarks, label
    """
    with tf.xla.experimental.jit_scope():
      # Select scaling + define dims
      idx = tf.random.uniform([],
                              minval=0,
                              maxval=len(self.filters),
                              dtype=tf.int32)
      # Build graph selector with switch-case approach
      branch_fn = [lambda: f(image, training=True) for f in self.filters]
      _img = tf.switch_case(idx, branch_fns=branch_fn)
      # Done
      return _img, mask, landmarks, label


class HorizontalFlip:
  """ Apply random horizontal flip with a given probability """

  def __init__(self, prob):
    """
    Constructor
    :param prob:     Probability of flip [0, 1]
    """
    self.p = max(min(prob, 1.0), 0.0)

  def run(self, image, mask, landmarks, label):
    """
    Do augmentation
    :param image:     Image to transform
    :param mask:      Segmentation mask
    :param landmarks: Face landmarks
    :param label:     True image
    :return:  Transform images, mask, landmarks, label
    """
    with tf.xla.experimental.jit_scope():
      _im = image
      _msk = mask
      _lms = landmarks
      _lbl = label
      p = tf.random.uniform(shape=[], maxval=1.0, dtype=tf.float32)
      if p > self.p:
        # Do flip
        _im = tf.image.flip_left_right(_im)
        _msk = tf.image.flip_left_right(_msk)
        _lbl = tf.image.flip_left_right(_lbl)
        # Apply landmarks flips
        w = tf.shape(_im)[1]
        _lms = self._flip_landmarks(_lms, w, LANDMARKS_FLIP_INDEXES)
      return _im, _msk, _lms, _lbl

  @staticmethod
  def _flip_landmarks(lms, width, indexes):
    """
    Flip landmarks and permute them
    :param lms:     Landmarks
    :param width:   Image width
    :param indexes: List of indexes to swap
    :return:  Flipped landmards
    """
    idx = tf.convert_to_tensor(indexes, tf.int32)
    w = tf.cast(width - 1, lms.dtype)
    lms = tf.stack([(w - lms[:, 0]), lms[:, 1]], axis=1)
    lms = tf.gather(lms, idx)  # Back to [68, 2]
    return lms


class RandomRotation:

  def __init__(self, angle, interpolation='BILINEAR'):
    """
    Constructor
    :param angle: Maximum rotation angle in degree, actual rotation will be
                  drawn from [-angle, angle] range
    :param interpolation: Type of interpolation: 'BILINEAR' or 'NEAREST'
    """
    self.angle = angle * 0.0174533      # Convert to radian
    self.interpolation = interpolation

  def run(self, image, mask, landmarks, label):
    """
    Do augmentation
    :param image:     Image to rotate
    :param mask:      Segmentation mask
    :param landmarks: Face landmarks
    :param label:     True image
    :return:  Transformed data
    """
    with tf.xla.experimental.jit_scope():
      # Select rotation angle
      ang = tf.random.uniform([], minval=-self.angle, maxval=self.angle)
      # Apply rotation to images
      img = rotate(image, angles=ang, interpolation=self.interpolation)
      msk = rotate(mask, angles=ang, interpolation=self.interpolation)
      lbl = rotate(label, angles=ang, interpolation=self.interpolation)
      # Rotate landmarks
      h, w, _ = image.shape
      lms = self._rotate_lms(landmarks, angle=ang, height=h, width=w)
      return img, msk, lms, lbl

  @staticmethod
  def _rotate_lms(lms, angle, width, height):
    # Build rotation matrix
    h2 = height / 2.0
    w2 = width / 2.0
    offset = tf.convert_to_tensor([[w2, h2]])
    rmat = tf.convert_to_tensor([[tf.math.cos(angle), tf.math.sin(angle)],
                                 [-tf.math.sin(angle), tf.math.cos(angle)]])
    pts = (lms - offset)
    pts = tf.matmul(pts, rmat, transpose_b=True) + offset
    return pts


class RandomScale:
  """ Randomly apply scaling on input data. Scale are chosen from a given set
  of values (i.e. categorical sampling) with equal probabilities"""

  def __init__(self, scale=(1.0,)):
    """
    Constructor
    :param scale: List of scaling factor to randomly sample from
    """
    self.scale = tf.convert_to_tensor(scale, dtype=tf.float32)

  def run(self, image, mask, landmarks, label):
    with tf.xla.experimental.jit_scope():
      # Get current shape
      h, w = image.shape[:2]
      # Select scaling + define dims
      idx = tf.random.uniform([],
                              minval=0,
                              maxval=len(self.scale),
                              dtype=tf.int32)
      s = self.scale[idx]
      _h = tf.cast(h * s, dtype=tf.int32)
      _w = tf.cast(w * s, dtype=tf.int32)
      # Resize
      _im = tf.image.resize(image, size=[_h, _w], method='bilinear')
      _msk = tf.image.resize(mask, size=[_h, _w], method='bilinear')
      _lbl = tf.image.resize(label, size=[_h, _w], method='bilinear')
      # Crop or pad
      _im = tf.image.resize_with_crop_or_pad(_im, h, w)
      _msk = tf.image.resize_with_crop_or_pad(_msk, h, w)
      _lbl = tf.image.resize_with_crop_or_pad(_lbl, h, w)
      # Landmarks
      _lms = self._scale_lms(landmarks, scale=s, width=w, height=h)
      return _im, _msk, _lms, _lbl

  def _scale_lms(self, lms, scale, width, height):
    # Build rotation matrix
    h2 = height / 2.0
    w2 = width / 2.0
    offset = tf.convert_to_tensor([[w2, h2]])
    trsfrm = tf.convert_to_tensor([[scale, 0.0],
                                   [0.0, scale]])
    pts = (lms - offset)
    pts = tf.matmul(pts, trsfrm) + offset
    return pts


class TrainLoader(DataProvider):
  """ DataLoader for MorphNet training """

  def __init__(self,
               pattern,
               img_size,
               n_pts=68,
               shuffle_files=True,
               augmentation=False,
               with_alb=False,
               n_parallel_call=None):
    """
    Constructor
    :param pattern:         Glob pattern for tfrecors shards
    :param img_size:        Size of the image input [H, W, C]
    :param n_pts:   Total number of landmarks
    :param shuffle_files: If `True` shuffle all shards
    :param augmentation: If `True` indicates that image needs to be randomly
                          augmented
    :param with_alb:  If True, add label for albedo cost
    :param n_parallel_call: Number of parallel call allowed
    """
    super(TrainLoader, self).__init__()
    self._n_call = n_parallel_call
    self._img_sz = img_size
    self._n_pts = n_pts
    self._augmentation = augmentation
    self.with_alb = with_alb
    self._source = tf.data.Dataset.list_files(pattern, shuffle=shuffle_files)
    self._source = self._source.interleave(tf.data.TFRecordDataset,
                                           block_length=1,
                                           cycle_length=self._n_call,
                                           num_parallel_calls=AUTOTUNE)

  def decode(self):
    """
    Decode single file entry and load data into memory
    :return:  `self`
    """

    def _parse_fn(example):
      """
      convert an tf.train.Example into tensors
      :param example: Dataset example
      :return:  image, mask, landmarks, id
      """
      # Parse TFExample records and perform simple data augmentation.
      fmt = {
        'face_id': tf.io.FixedLenFeature([], tf.int64),
        'shape':   tf.io.FixedLenFeature([2], tf.int64),
        'image':        tf.io.FixedLenFeature([], tf.string),
        'landmark':     tf.io.FixedLenFeature([], tf.string),
        'segmentation': tf.io.FixedLenFeature([], tf.string)
      }
      parsed = tf.io.parse_single_example(example, fmt)
      # FaceID
      face_id = parsed['face_id']
      # Image
      image = tf.io.decode_jpeg(parsed['image'])
      image = tf.reshape(image, self._img_sz)
      image = tf.cast(image, tf.float32)
      # Label
      label = image / 255.0
      # Landmark
      landmark = tf.io.decode_raw(parsed['landmark'], tf.float32)
      landmark = tf.reshape(landmark, shape=[self._n_pts, 2])
      # Mask
      decoded = tf.io.decode_compressed(parsed['segmentation'],
                                        compression_type='ZLIB')
      seg_mask = tf.io.decode_raw(decoded, out_type=tf.float32)
      seg_mask = tf.reshape(seg_mask,
                            shape=[self._img_sz[0], self._img_sz[1], 1])
      # Set value small than epsilon to zero
      # mask = tf.cast(tf.greater_equal(seg_mask, 0.25), seg_mask.dtype)
      # seg_mask = seg_mask * mask
      return image, seg_mask, landmark, label

    self._source = self._source.map(_parse_fn, AUTOTUNE)
    # https://www.tensorflow.org/api_docs/python/tf/data/experimental/ignore_errors
    # self._source = self._source.apply(tf.data.experimental.ignore_errors())
    return self

  def preprocess(self):
    """
    Add pre-processing to the loading chain
    :return:  `self`
    """

    def _dispatch_data(image, mask, landmarks, label):
      """
      Dispatch data for network
      :param image:  Image feed to the network
      :param mask:  Segmentation mask where to compute loss (i.e. Attention)
      :param landmarks: Facial landmarks
      :param label:     True image
      :return:  Batch, labels
      """
      # Batch inputs: Image + Background
      # Labels: Add segmentation as alpha channel
      # background = label
      label = tf.concat([label, mask], -1)  # [H, W, 4]
      # Network inputs
      # Ensure image is in proper range for network [0, 255]
      image = tf.clip_by_value(image, 0.0, 255.0)
      batch = image
      # Labels -> Loss functions
      if self.with_alb:
        labels = (label, label, landmarks)
      else:
        labels = (label, landmarks)
      # Done
      return batch, labels

    if self._augmentation:
      # To define which one ... should be contrast
      rnd_color = ColorJitter(brightness=None,
                              contrast=(0.75, 1.0),
                              saturation=None)
      rnd_blur = RandomBlur(upper_stddev=1.5, ksize=5)
      rnd_flip = HorizontalFlip(prob=0.5)
      rnd_scale = RandomScale(scale=[1.0, 1.1, 1.2])
      rnd_rot = RandomRotation(angle=30.0)
      self._source = self._source.map(rnd_color.run, AUTOTUNE)
      # self._source = self._source.map(rnd_blur.run, AUTOTUNE)
      self._source = self._source.map(rnd_flip.run, AUTOTUNE)
      self._source = self._source.map(rnd_scale.run, AUTOTUNE)
      self._source = self._source.map(rnd_rot.run, AUTOTUNE)
    self._source = self._source.map(_dispatch_data, AUTOTUNE)
    return self

  def multi_resolution(self, resolutions: list=None):
    """
    Add multi-resolution generator (random)
    :param resolutions: List of resolution level to sample from. Each level is
     represented by a list/tuple of size two. First value is the lower bound (inclusive)
     and second one is the upper bound (exclusive)
    :return:
    """
    if resolutions is not None:
      if not isinstance(resolutions, (list, tuple)):
        raise ValueError('Input must be list/tuple')
      # Check entries
      for res in resolutions:
        # Check size
        if len(res) != 2:
          raise ValueError('Each level of resolution must be a `list`/`tuple`'
                           ' of size 2, got: {}'.format(len(res)))
        # Check range, i.e. res[0] < res[1]
        if res[0] > res[1]:
          raise ValueError('First entry must be smaller than second entry for a'
                           ' given level, instead got {} > {}'.format(res[0],
                                                                      res[1]))

      def _generate_low_res(batch, labels):
        """
        Generate low resolution image for a given batches
        :param batch:   Tuple of Tensor: Image + batckground
        :param labels:  Tuple of Tensor: labels (not touched)
        :return:
        """
        hres_img = batch
        # Init output with original scale
        image_res = [hres_img]

        # Generate entries for each level
        in_shp = tf.shape(hres_img)
        for res in resolutions:
          # Randomly select image size
          new_sz = tf.random.uniform(shape=[1],
                                     minval=res[0],
                                     maxval=res[1],
                                     dtype=tf.int32)
          # Down-sample
          shp = tf.concat([new_sz, new_sz], axis=0)
          lres_img = tf.image.resize(hres_img,
                                     size=shp,
                                     method=tf.image.ResizeMethod.BICUBIC)
          # Up-sample
          lres_img = tf.image.resize(lres_img,
                                     size=in_shp[1:3],
                                     method=tf.image.ResizeMethod.BICUBIC)
          lres_img = tf.clip_by_value(lres_img, 0.0, 255.0)
          # Stack
          image_res.append(lres_img)
        return image_res, labels

      # Add to input pipeline
      self._source = self._source.map(_generate_low_res, AUTOTUNE)
    return self
