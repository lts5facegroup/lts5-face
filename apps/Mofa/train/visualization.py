# coding=utf-8
""" Implementation of paper:

  MoFA: Model-based Deep Convolutional Face Autoencoder for Unsupervised
  Monocular Reconstruction
  Tewari et al.

Link:
  - https://arxiv.org/abs/1703.10580
"""
from os.path import join as _join
from os.path import exists as _exists
from os import makedirs as _makedirs
import argparse
import tensorflow as _tf
# _tf.config.experimental_run_functions_eagerly(True)
import numpy as _np
import cv2 as _cv
from imageio import imwrite
from lts5.geometry import MeshFloat as Mesh
from lts5.tensorflow_op.network.morphnet import MorphNet, MorphNetParameter
from lts5.tensorflow_op import initialize_device_memory
from loader import TrainLoader

__author__ = 'Christophe Ecabert'


def _create_tfrecords_loader(pattern, params):
  """
  Load data from a given text file `filepath`
  :param pattern:     Glob pattern for tfrecord shards
  :param params:      Network parameters
  :return:    tf.data.Dataset object
  """
  from multiprocessing import cpu_count

  # Define number of possible parallel call
  n_call = cpu_count()
  # Create loader
  loader = (TrainLoader(pattern=pattern,
                        img_size=params.img_dims,
                        shuffle_files=False,
                        n_parallel_call=n_call)
            .decode()
            .preprocess()
            .batch(batch_size=params.optimizer.batch_sz, drop_remainder=True)
            .prefetch(1))
  return loader.one_shot_iterator()


def dump_mesh(vertex, color, tri, normal, name):
  m = Mesh()
  m.export_binary_ply = True
  m.vertex = vertex.reshape(-1, 3)
  m.normal = normal.reshape(-1, 3)
  m.vertex_color = color.reshape(-1, 3)
  m.tri = tri
  m.save(name)


def _process(pargs):
  """
  Run processing
  :param pargs: Parsed arguments
  """

  # Config
  # ------------------------------------------------
  initialize_device_memory()

  cfg = MorphNetParameter.from_json(filename=pargs.config)
  cfg.regularizer.albedo_ratio = 0.0

  # Data input
  # ------------------------------------------------
  loader = None
  if pargs.input is not None:
    # TF Records
    pattern = _join(pargs.folder, pargs.input)
    loader = _create_tfrecords_loader(pattern=pattern, params=cfg)
  else:
    raise NotImplementedError('Can process folder alone at the moment')

  # Output
  if not _exists(pargs.output):
    _makedirs(pargs.output)

  if not _exists(_join(pargs.output, 'input')):
    _makedirs(_join(pargs.output, 'input'))
  if not _exists(_join(pargs.output, 'reconstruction')):
    _makedirs(_join(pargs.output, 'reconstruction'))
  if not _exists(_join(pargs.output, 'mesh')):
    _makedirs(_join(pargs.output, 'mesh'))

  # Network
  # ------------------------------------------------
  img = _tf.keras.layers.Input(shape=cfg.img_dims)
  bg = _tf.keras.layers.Input(shape=cfg.img_dims)
  net = MorphNet(config=cfg)
  renderings = net([img, bg], training=False)
  synth = renderings[0][..., :3]
  mask = renderings[0][..., 3:]
  lms = renderings[-1]
  (shape, albedo, lights, color, normal, tri), _ = net.face_model.output
  # Create new model in order to output model parameters as well
  outputs = [synth, mask, lms, shape, color, normal, tri]
  model = _tf.keras.Model(inputs=[img, bg], outputs=outputs)
  # Restore checkpoint
  path = _tf.train.latest_checkpoint(pargs.model_dir)
  net.load_weights(filepath=path)

  # Run through validation set
  # ------------------------------------------------
  canvas = _np.zeros(shape=(cfg.rendering.height,
                            cfg.rendering.width * 3,
                            3),
                     dtype=_np.uint8)

  k = 0
  for samples, labels in loader:
    gt_image = labels[0][..., :3].numpy()
    gt_mask = labels[0][..., 3:].numpy()
    gt_lms = labels[-1].numpy()
    # Forward prediction
    outputs = model.predict(samples)
    mm_image, mm_mask, mm_lms, mm_shp, mm_color, mm_normal, mm_tri = outputs
    # Stitch + dump
    for values in zip(gt_image,
                      gt_mask,
                      gt_lms,
                      mm_image,
                      mm_lms,
                      mm_shp,
                      mm_color,
                      mm_normal):
      gt, msk, true_lms, recon, pred_lms, pred_s, pred_c, pred_n = values
      true_lms = true_lms.reshape(-1, 2)
      pred_lms = pred_lms.reshape(-1, 2)

      gt_u = (gt * 255.0).astype(_np.uint8)
      for pt in true_lms:
        x, y = pt
        x = max(0, int(x))
        y = max(0, int(y))
        gt_u = _cv.circle(gt_u,
                          center=(x, y),
                          radius=1,
                          color=(0, 255, 0),
                          thickness=-1,
                          lineType=_cv.LINE_AA)

      recon_u = (recon * 255.0).astype(_np.uint8)
      # for pt in p_lms:
      #   x, y = pt
      #   x = max(0, int(x))
      #   y = max(0, int(y))
      #   rec_u = _cv.circle(rec_u,
      #                      center=(x, y),
      #                      radius=1,
      #                      color=(0, 255, 0),
      #                      thickness=-1,
      #                      lineType=_cv.LINE_AA)


      mask_u = (msk * 255.0).astype(_np.uint8)
      canvas[:, :gt.shape[1], :] = gt_u
      canvas[:, gt.shape[1]:(2*gt.shape[1]), :] = recon_u
      canvas[:, (2*gt.shape[1]):, :] = mask_u

      # Dump image
      im_name = _join(pargs.output,
                      'reconstruction',
                      'sample_{:06d}.jpg'.format(k))
      imwrite(im_name, canvas)
      # Dump mesh only every 500 image to avoid too large amount of data
      if k % 500 == 0:
        mesh_name = _join(pargs.output, 'mesh', 'sample_{:06d}.ply'.format(k))
        dump_mesh(vertex=pred_s,
                  color=pred_c,
                  normal=pred_n,
                  tri=mm_tri,
                  name=mesh_name)

      # Dump original inpput
      im_name = _join(pargs.output,'input', 'sample_{:06d}.jpg'.format(k))
      img = (gt * 255.0).astype(_np.uint8)
      imwrite(im_name, img)
      k += 1


if __name__ == '__main__':
  # Parser
  parser = argparse.ArgumentParser(description='Mofa forward pass')

  # Input folder
  parser.add_argument('--folder',
                      type=str,
                      required=True,
                      help='Location where data are stored')
  # TFRecords pattern
  parser.add_argument('--tfrecords',
                      type=str,
                      dest='input',
                      help='Glob pattern for tfrecords file(s)')
  # Network config
  parser.add_argument('--config',
                      type=str,
                      required=True,
                      help='File holding network configuration (*.json)')
  # Model dir
  parser.add_argument('--model_dir',
                      type=str,
                      dest='model_dir',
                      help='Location where network is stored')
  # Output
  parser.add_argument('--output',
                      type=str,
                      required=True)

  pargs = parser.parse_args()

  # Run processing
  _process(pargs=pargs)