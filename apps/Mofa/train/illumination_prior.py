# coding=utf-8
"""
Build illumination prior to use when doing 3D face reconstruction.

See:
  - Occlusion-aware 3D Morphable Models and an Illumination Prior for Face
    Image Analysis, Egger et al.
  - https://gravis.dmi.unibas.ch/PMM/data/bip/
"""
from os.path import join, exists, basename
from os import makedirs as mkdir
from argparse import ArgumentParser
import numpy as np
from imageio import imread
import json
import matplotlib.pyplot as plt
from lts5.utils.tools import search_folder, init_logger
from lts5.ogl import SHViewer

__author__ = 'Christophe Ecabert'

logger = init_logger()


def _convert_sh_coef(c):
  """
  Convert sphericial harmonics from scalismo format to LTS format
  :param c: SH coefficient to convert, `numpy.ndarray` of dimensions [9, 3]
  :return:  Converted coefficient
  """
  assert tuple(c.shape) == (9, 3), 'SH coefficient must have 9x3 dimensions'
  sh = np.zeros_like(c)
  # Values of the Lambertian reflectance kernel expressed in SH
  # https://github.com/unibas-gravis/scalismo-faces/blob/master/src/main/scala/scalismo/faces/parameters/Illumination.scala
  lbk = [3.141593,
         2.094395,
         2.094395,
         2.094395,
         0.785398,
         0.785398,
         0.785398,
         0.785398,
         0.785398]

  # First band -> rescale
  s0 = np.sqrt(1.0 / np.pi) * 0.5
  sh[0, :] = c[0, :] * s0 * lbk[0]
  # Second band, scale + reverse
  s0 = np.sqrt(3.0 / np.pi) * 0.5
  sh[1, :] = c[3, :] * s0 * lbk[3]  # x
  sh[2, :] = c[1, :] * s0 * lbk[1]  # y
  sh[3, :] = c[2, :] * s0 * lbk[2]  # z
  # Third band
  s0 = np.sqrt(15.0 / np.pi) * 0.5
  s1 = np.sqrt(5.0 / np.pi) * 0.25
  s2 = np.sqrt(15.0 / np.pi) * 0.25
  sh[4, :] = c[4, :] * s0 * lbk[4]  # nx * ny
  sh[5, :] = c[7, :] * s0 * lbk[7]  # nx * nz
  sh[6, :] = c[5, :] * s0 * lbk[5]  # ny * nz
  sh[7, :] = c[8, :] * s2 * lbk[8]  # nx * nx - ny * ny
  sh[8, :] = c[6, :] * s1 * lbk[6]  # 3 * nz * nz - 1
  return sh.T


def process(pargs):
  """
  Learn Illumination prior
  :param pargs: Parsed arguments from command line
  """
  # Output location exists ?
  if not exists(pargs.output):
    mkdir(pargs.output)

  if not exists(join(pargs.output, 'converted_coefficients.npy')):
    logger.info('Convert raw SH coefficients...')
    # Need to build viewer ?
    sh_viewer = None
    albedo = None
    axes = None
    if pargs.n_visualization:
      sh_viewer = SHViewer(128, 128, 50)
      sh_viewer.background_color = (1.0, 1.0, 1.0)
      n_vertex = sh_viewer.dims
      # Skin color provided by BIP readme
      color = np.asarray([[194.0, 142.0, 114.0]], np.float32) / 255.0
      albedo = np.repeat(color, n_vertex, axis=0)
      if not exists(join(pargs.output, 'visualization')):
        mkdir(join(pargs.output, 'visualization'))
      # Create canvas
      axes = (plt.subplot(121), plt.subplot(122))

    # Scan for SH Coefficient files
    pfiles = search_folder(folder=join(pargs.input, 'data', 'parameters'),
                           ext=['.rps'],
                           relative=False)
    imfiles = search_folder(folder=join(pargs.input, 'data', 'renderings'),
                            ext=['.png'],
                            relative=False)
    sh_coeffs = []
    # Convert every entries
    for k, (pfile, imfile) in enumerate(zip(pfiles, imfiles)):
      if k % 1000 == 0:
        logger.info('Convert file: {}'.format(pfile))

      rparam = json.load(open(pfile))
      # Raw coeff + Conversion
      raw_coef = np.asarray(rparam['environmentMap']['coefficients'])
      sh_coef = _convert_sh_coef(raw_coef)
      # Viz ?
      if k < pargs.n_visualization:
        # Draw converted sh coef
        _, img = sh_viewer.generate(albedo=albedo,
                                    sh_coef=sh_coef.astype(np.float32))
        # Load gt
        im_true = np.asarray(imread(imfile)).astype(np.float32) / 255.0

        ax = axes[0]
        ax.imshow(img)
        ax.axis('off')
        ax.set_title('Converted')
        ax = axes[1]
        ax.imshow(im_true)
        ax.axis('off')
        ax.set_title('Ground truth')
        plt.tight_layout()
        fname = join(pargs.output,
                     'visualization',
                     basename(imfile))
        plt.savefig(fname)
      # Add to list
      sh_coeffs.append(sh_coef.reshape(-1))

    # Learn multivariate gaussian model
    sh_coeffs = np.asarray(sh_coeffs)
    np.save(join(pargs.output, 'converted_coefficients.npy'), sh_coeffs)
  else:
    logger.warning('Reload pre-computed SH coefficients...')
    sh_coeffs = np.load(join(pargs.output, 'converted_coefficients.npy'))

  # Multivariate Gaussian components
  mu = np.mean(sh_coeffs, axis=0)
  cov = np.cov(sh_coeffs, rowvar=False)
  # Save
  model = {'mean': mu,
           'cov': cov}
  np.save(join(pargs.output, 'aflw_illumination_prior.npy'), model)


if __name__ == '__main__':
  # Parser
  p = ArgumentParser()

  # Input
  p.add_argument('--input',
                 type=str,
                 required=True,
                 help='Folder where the illumination coefficient of the AFLW'
                      ' dataset are stored')
  # Location where to place model
  p.add_argument('--output',
                 type=str,
                 required=True,
                 help='Where to place the learned prior and intermediate data')
  # Conversion visualization
  p.add_argument('--n_visualization',
                 type=int,
                 required=False,
                 default=0,
                 help='Number of sample to dump for visual insepection')

  # Parse
  args = p.parse_args()
  process(pargs=args)
