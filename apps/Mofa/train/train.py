# coding=utf-8
""" Implementation of paper:

  MoFA: Model-based Deep Convolutional Face Autoencoder for Unsupervised
  Monocular Reconstruction
  Tewari et al.

Link:
  - https://arxiv.org/abs/1703.10580
"""
from os.path import join as _join
from os.path import exists as _exists
from os import makedirs as _mkdir
from datetime import datetime
from copy import deepcopy
import argparse
from multiprocessing import cpu_count
from distutils.version import LooseVersion
from json import dump
from imageio import imread, imwrite
import numpy as np
import tensorflow as tf
import tensorflow.keras.layers as kl
import wandb
from wandb.keras import WandbCallback
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import LearningRateScheduler
from lts5.tensorflow_op.network.morphnet import MorphNet, MorphNetParameter
from lts5.utils.tools import init_logger, str2bool
from lts5.tensorflow_op.utils.device import initialize_device_memory
from lts5.tensorflow_op.utils.summary import SummaryWriterManager
from lts5.tensorflow_op.callbacks import AugmentedTensorBoard
from lts5.tensorflow_op.callbacks import PeriodicModelInference
from lts5.tensorflow_op.callbacks import CosineAnnealingLearningRateSchedule
from loader import TrainLoader

# Init LTS5 logger
logger = init_logger()
# Limit memory usage
initialize_device_memory()

# Ensure TF 2.0
tf_version = tf.__version__
logger.info('Tensorflow version: %s', tf_version)
assert LooseVersion(tf_version) >= LooseVersion('2.0'),\
  "TensorFlow r2.0 or later is needed"


def flatten_dict(dictionary,
                 accumulator=None):
  if accumulator is None:
    accumulator = {}
  for k, v in dictionary.items():
    if isinstance(v, dict):
      flatten_dict(dictionary=v, accumulator=accumulator)
      continue
    accumulator[k] = v
  return accumulator


def input_fn(pattern,
             config,
             n_repeat=1,
             shuffle=True,
             augmentation=False):
  """
  Load data from a given text file `filepath`
  :param pattern:     Glob pattern for tfrecord shards
  :param config:      Network parameters
  :param n_repeat:    Number of time to repeat the whole dataset
  :param augmentation: If `True` apply random image augmentation
  :return:    tf.data.Dataset object
  """
  # Define number of possible parallel call
  n_call = cpu_count()
  # Create loader
  loader = TrainLoader(pattern=pattern,
                       img_size=config.img_dims,
                       shuffle_files=shuffle,
                       augmentation=augmentation,
                       with_alb=config.regularizer.w_alb > 0.0,
                       n_parallel_call=n_call)
  if shuffle:
    loader = loader.shuffle(buffer_size=100 * config.optimizer.batch_sz)
  loader = (loader.repeat(count=n_repeat)
            .decode()
            .preprocess()
            .batch(batch_size=config.optimizer.batch_sz,
                   drop_remainder=True))
  return loader.prefetch(tf.data.experimental.AUTOTUNE).one_shot_iterator()


def train_model(cfg,
                model_dir,
                train_pattern,
                validation_pattern,
                log_freq=1000,
                n_step=None,
                starting_epoch=0,
                with_summary=True,
                with_checkpoint=True,
                with_lr_scheduler=True,
                with_periodic_inference=True,
                with_xla=False):
  """
  Train network for a given configuration
  :param cfg:                 Configuration
  :param model_dir:           Location where to place the model
  :param train_pattern:       Glob pattern for training tfrecords
  :param validation_pattern:  Glob pattern for validation tfrecords
  :param log_freq:            Logging frequency
  :param n_step:              Number of steps to train for
  :param starting_epoch:      Starting epoch
  :param with_summary:        If true, display model summary
  :param with_checkpoint:     If True, add checkpoint
  :param with_lr_scheduler:   If True add lr schduling
  :param with_periodic_inference:   If True run periodic inference
  :param with_xla: If `True` enable xla optimization
  """

  # Save copy to output folder just in case
  if not _exists(model_dir):
    _mkdir(model_dir)
  fname = _join(model_dir, 'config.json')
  with open(fname, 'w') as fs:
    dump(cfg, fs)

  # Tensorflow config
  # ------------------------------------------------
  # Enable XLA
  tf.config.optimizer.set_jit(with_xla)

  # Experiment tracking
  # ------------------------------------------------
  params = flatten_dict(cfg)
  wandb.init(name='Run-{}'.format(datetime.today().strftime('%d-%m-%Y')),
             project='morphnet',
             config=params,
             dir=model_dir,
             sync_tensorboard=False)

  # Summary manager
  # ------------------------------------------------
  logdir = _join(model_dir, 'logs')
  manager = SummaryWriterManager(log_dir=logdir,
                                 log_freqency=log_freq,
                                 use_v2=True)


  # Model
  # ------------------------------------------------
  # Number of identity in training set:  v2 8456, v3 12143
  # Set to zero to disable
  image = tf.keras.Input(shape=cfg.img_dims,
                         dtype=tf.float32,
                         name='image')
  # background = tf.keras.Input(shape=cfg.img_dims,
  #                             dtype=tf.float32,
  #                             name='background')
  # labels = tf.keras.Input(shape=(1,),
  #                         dtype=tf.int64,
  #                         name='id_labels')

  # Set BN moment to 0.9 for all layers
  params = {kl.BatchNormalization.__name__: {'all': {'momentum': 0.9}}}
  model = MorphNet.ForTraining(config=cfg,
                               manager=manager,
                               layer_params=params)
  # renderings = model([image, background, labels], training=True)
  renderings = model(image, training=True)

  # Dataset
  # ------------------------------------------------
  train_dataset = input_fn(train_pattern,
                           config=cfg,
                           n_repeat=1,
                           shuffle=True,
                           augmentation=True)
  valid_dataset = None
  if validation_pattern is not None:
    valid_dataset = input_fn(validation_pattern,
                             config=cfg,
                             shuffle=True,
                             n_repeat=1,
                             augmentation=False)
  # Losses
  # ------------------------------------------------
  losses = model.get_losses(tye='l2', normalized=False)
  # Metrics
  # ------------------------------------------------
  metrics = model.get_metrics()
  # Optimizer
  # ------------------------------------------------
  optimizer = model.get_optimizer()
  # Compile
  # ------------------------------------------------
  model.compile(optimizer=optimizer,
                loss=losses,
                metrics=metrics,
                run_eagerly=False)

  # Callbacks
  # ------------------------------------------------
  model_cb = []
  if with_checkpoint:
    # Save model based on performance of validation loss
    model_name = _join(model_dir, 'morphnet.ckpt')
    checkpoint_cb = ModelCheckpoint(filepath=model_name,
                                    save_best_only=True,
                                    monitor='val_loss',
                                    mode='min',
                                    save_weights_only=True,
                                    verbose=1)
    model_cb.append(checkpoint_cb)

  if with_lr_scheduler:
    from tensorflow.keras.optimizers.schedules import PiecewiseConstantDecay
    # base_lr = cfg.optimizer.params['learning_rate']
    # lr_scheduler_cb = CosineAnnealingLearningRateSchedule(n_epochs=cfg.optimizer.n_epoch,
    #                                                       n_cycles=10,
    #                                                       lr=[base_lr, 0.1 * base_lr],
    #                                                       boundaries=[50])

    base_lr = cfg.optimizer.params['learning_rate']
    # Decrease by a factor 10 for each plateau
    bnd = [20, 80]
    v = [base_lr, 0.5 * base_lr, 0.1 * base_lr]
    _lr_scheduler = PiecewiseConstantDecay(boundaries=bnd, values=v)
    lr_scheduler_cb = LearningRateScheduler(_lr_scheduler, verbose=1)
    model_cb.append(lr_scheduler_cb)

  if with_periodic_inference:
    # Periodic inference
    def _input_fn():
      """ Load images for periodic inference """
      pathes = ['periodic_inference/00001002.jpg',
                'periodic_inference/00012466.jpg',
                'periodic_inference/00015176.jpg',
                'periodic_inference/00020639.jpg',
                'periodic_inference/00032805.jpg']
      images = []
      for k, path in enumerate(pathes):
        im = np.asarray(imread(path)).astype(np.float32)
        images.append(im)
      images = tf.convert_to_tensor(np.stack(images))
      return images

    def _output_fn(outputs, epoch, logdir):
      """
      Dump reconstructed image
      :param outputs: Model's output
      :param epoch:   Epoch number
      """
      images = outputs[0]
      for k, img in enumerate(images):
        im = (img[..., :3] * 255.0).astype(np.uint8)
        fname = _join(logdir,
                      'reconstruction_{:02d}@{:03d}.jpg'.format(k, epoch))
        imwrite(fname, im)

    # Periodic inference call back
    inference_cb = PeriodicModelInference(_input_fn,
                                          _output_fn,
                                          logdir=_join(model_dir, 'inference'))
    model_cb.append(inference_cb)

  # Add tensorboard as last callback
  tensorboad_cb = AugmentedTensorBoard(log_dir=logdir,
                                       histogram_freq=1,
                                       write_graph=True,
                                       write_images=False,
                                       update_freq=log_freq,
                                       profile_batch='10,20',
                                       embeddings_freq=0)
  model_cb.append(tensorboad_cb)
  # Add experiment tracking
  model_cb.append(WandbCallback(monitor='val_loss',
                                mode='min',
                                save_model=False,
                                log_weights=True,
                                log_batch_frequency=log_freq))

  # Reload
  # ---------------------------------------------------------
  latest = tf.train.latest_checkpoint(model_dir)
  latest = "imagenet" if latest is None else latest
  model.load_weights(latest)  # Train from scratch

  # Train
  if with_summary:
    model.summary(120)
  # Train full model
  logger.info('Start end-to-end training...')
  model.fit(x=train_dataset,
            steps_per_epoch=n_step,
            validation_steps=n_step,
            callbacks=model_cb,
            epochs=cfg.optimizer.n_epoch,
            initial_epoch=starting_epoch,
            validation_data=valid_dataset)


def pretrain(pargs):
  """
  Train network, without model refinement
  :param pargs: Parsed command line arguments
  """

  # Get config
  cfg = MorphNetParameter.from_json(pargs.config)
  cfg.model.n_es = 0.0  # Disable model refinement
  cfg.model.n_et = 0.0  # Disable model refinement

  # Train
  train_model(cfg,
              pargs.model_dir,
              pargs.train_input,
              pargs.validation_input,
              pargs.log_freq,
              starting_epoch=pargs.starting_epoch,
              with_summary=True,
              with_checkpoint=True,
              with_lr_scheduler=True,
              with_periodic_inference=True,
              with_xla=pargs.enable_xla)


def parameter_search(pargs, n_run=10):
  """
  Run hparams search
  :param pargs:   Parsed command line
  :param n_run:   Number of run to perform
  """
  from random import uniform

  # Get config
  cfg = MorphNetParameter.from_json(pargs.config)
  cfg.model.n_es = 0.0  # Disable model refinement
  cfg.model.n_et = 0.0  # Disable model refinement
  # Single epoch
  cfg.optimizer.n_epoch = 1

  w_id_min = 5e-2
  w_id_max = 1e-1

  def _update_cfg(cfg):
    """ Update model configuration """
    w_id = uniform(w_id_min, w_id_max)
    cfg.regularizer.w_id = w_id
    # Summary
    logger.info('Try configuration: w_id={:.3f}'.format(w_id))
    return cfg

  for k in range(n_run):
    # Define parameters
    model_dir = _join(pargs.model_dir,
                      '{:.4f}-{:.4f}'.format(w_id_min, w_id_max),
                      'run{:02d}'.format(k))
    # Select random parameters
    new_cfg = _update_cfg(cfg)
    train_model(new_cfg,
                model_dir,
                pargs.train_input,
                None,
                log_freq=100,
                n_step=2000,
                with_summary=False,
                with_checkpoint=False,
                with_lr_scheduler=False,
                with_periodic_inference=False)


def loopback_validation(pargs):

  def _update_cfg(config, updates: dict):
    u_cfg = deepcopy(config)
    for p_name, value in updates.items():
      u_cfg.regularizer[p_name] = value
    return u_cfg

  # Get config
  cfg = MorphNetParameter.from_json(pargs.config)
  cfg.optimizer.n_epoch = 5

  # No loopback
  model_dir = _join(pargs.model_dir, 'Exp0')
  new_cfg = _update_cfg(cfg, {'w_contrast_id': -1.0,
                              'w_contrast_tex': -1.0,
                              'w_loop_exp': -1.0})
  train_model(new_cfg, model_dir, pargs.train_input, pargs.validation_input,
              pargs.log_freq, starting_epoch=0,
              with_lr_scheduler=False,
              with_periodic_inference=False)

  # Only ID
  tf.keras.backend.clear_session()
  model_dir = _join(pargs.model_dir, 'Exp1')
  new_cfg = _update_cfg(cfg, {'w_contrast_tex': -1.0,
                              'w_loop_exp': -1.0})
  train_model(new_cfg, model_dir, pargs.train_input, pargs.validation_input,
              pargs.log_freq, starting_epoch=0,
              with_lr_scheduler=False,
              with_periodic_inference=False)

  # Only Tex
  tf.keras.backend.clear_session()
  model_dir = _join(pargs.model_dir, 'Exp2')
  new_cfg = _update_cfg(cfg, {'w_contrast_id': -1.0,
                              'w_loop_exp': -1.0})
  train_model(new_cfg, model_dir, pargs.train_input, pargs.validation_input,
              pargs.log_freq, starting_epoch=0,
              with_lr_scheduler=False,
              with_periodic_inference=False)

  # ID + Tex
  tf.keras.backend.clear_session()
  model_dir = _join(pargs.model_dir, 'Exp3')
  new_cfg = _update_cfg(cfg, {'w_loop_exp': -1.0})
  train_model(new_cfg, model_dir, pargs.train_input, pargs.validation_input,
              pargs.log_freq, starting_epoch=0,
              with_lr_scheduler=False,
              with_periodic_inference=False)

  # ID + Tex + No exp
  tf.keras.backend.clear_session()
  model_dir = _join(pargs.model_dir, 'Exp4')
  new_cfg = cfg
  train_model(new_cfg, model_dir, pargs.train_input, pargs.validation_input,
              pargs.log_freq, starting_epoch=0,
              with_lr_scheduler=False,
              with_periodic_inference=False)


if __name__ == '__main__':
  # Parser
  parser = argparse.ArgumentParser(description='Training process')

  parser.add_argument('--config',
                      required=True,
                      type=str,
                      help='Network configuration file')
  parser.add_argument('--train_input',
                      required=True,
                      type=str,
                      help='Glob pattern for training tfrecords')
  parser.add_argument('--validation_input',
                      required=True,
                      type=str,
                      help='Glob pattern for validation tfrecords')
  parser.add_argument('--model_dir',
                      required=True,
                      type=str,
                      help='Location where to dump checkpoints')
  parser.add_argument('--starting_epoch',
                      type=int,
                      default=0,
                      help='Starting epoch, for this session')
  parser.add_argument('--log_freq',
                      type=int,
                      default=1000,
                      help='Frequency at which losses and metrics are logged in'
                           ' tensorboard')
  parser.add_argument('--enable_xla',
                      type=str2bool,
                      required=False,
                      default=False,
                      help='Enable XLA optimization')
  # Get inputs
  args = parser.parse_args()
  # Train
  pretrain(pargs=args)
  # parameter_search(pargs=args, n_run=10)
  # loopback_validation(pargs=args)
