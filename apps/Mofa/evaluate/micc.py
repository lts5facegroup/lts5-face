#coding=utf-8
""" MICC Shape evaluator interface """
from os.path import exists as _exists
from os import makedirs as _mkdir
from os import listdir as _ls
from os.path import join as _join
from os.path import basename
import numpy as np
import tensorflow as tf
from imageio import imsave
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import seaborn as sns
from lts5.utils.tools import search_folder
from lts5.utils import RodriguesFloat as Rodrigues
from lts5.utils import ColorMapDouble as CMap
from lts5.geometry import MeshFloat as Mesh
from lts5.geometry.metric import ICPPointToPlaneMetric
from lts5.geometry.registration import RegistrationEvaluator

from evaluator import ShapeEvaluator, create_mesh
from loader import MICCShapeLoader
from alignment import ICPAlignment


_micc_nose_idx = {1: 5291,
                  2: 20035,
                  3: 14773,
                  4: 5925,
                  5: 6607,
                  6: 20383,
                  7: 17270,
                  8: 2753,
                  9: 4194,
                  10: 9855,
                  11: 6329,
                  12: 5597,
                  13: 13087,
                  14: 14459,
                  15: 23829,
                  16: 5504,
                  17: 7290,
                  18: 6574,
                  19: 16589,
                  20: 8591,
                  21: 8156,
                  22: 6679,
                  23: 14046,
                  24: 7738,
                  25: 11455,
                  26: 18883,
                  27: 10011,
                  28: 5865,
                  29: 3361,
                  30: 14542,
                  31: 11487,
                  32: 12792,
                  33: 8610,
                  34: 15473,
                  35: 23231,
                  36: 9270,
                  37: 21097,
                  38: 13000,
                  39: 8770,
                  40: 8066,
                  41: 17228,
                  42: 2618,
                  43: 18246,
                  44: 14685,
                  45: 6774,
                  46: 2751,
                  47: 26768,
                  48: 14954,
                  49: 7361,
                  50: 2884,
                  51: 2897,
                  52: 10764,
                  53: 6839}


nose_f_idx = [13860, 13861, 13862, 13863, 13864, 13865, 13866, 13867, 14096,
              14097, 14098, 14099, 14100, 14101, 14102, 14103, 14332, 14333,
              14334, 14335, 14336, 14337, 14338, 14339, 14568, 14569, 14570,
              14571, 14572, 14573, 14574, 14575, 14804, 14805, 14806, 14807,
              14808, 14809, 14810, 14811, 15040, 15041, 15042, 15043, 15044,
              15045, 15046, 15047, 15276, 15278, 15279, 15280, 15281, 15282,
              17164, 17165, 17166, 17167, 17168, 17169, 17170, 17171, 17400,
              17401, 17402, 17403, 17404, 17405, 17406, 17407, 17636, 17637,
              17638, 17639, 17640, 17641, 17642, 17643, 17872, 17873, 17874,
              17875, 17876, 17877, 17878, 17879, 18108, 18109, 18110, 18111,
              18112, 18113, 18114, 18115, 18344, 18345, 18346, 18347, 18348,
              18349, 18350, 18351, 18580, 18582, 18583, 18584, 18585, 18586]


def _remove_nostrils(mesh):
  """
  Remove nostrils from a given BFM mesh
  :param mesh:  BFM mesh instance
  """
  # Remove tri
  tri = np.delete(mesh.tri, nose_f_idx, axis=0)
  # Set new value
  mesh.tri = tri
  mesh.build_connectivity()
  mesh.remesh_for_triangle(mesh.tri)


def add_colormap(mesh, error):
  cm = CMap(type=CMap.kJet, min=0.0, max=error.max())
  e_color = np.zeros(shape=(error.size, 3), dtype=np.float32)
  for k, e in enumerate(error):
    e_color[k, :] = cm.pick_color(e)
  mesh.vertex_color = e_color


def plot_error_distribution(data,
                            names,
                            title,
                            xlabel,
                            ylabel,
                            filename):
  plt.figure()
  for serie, name in zip(data, names):
    sns.distplot(serie, hist=False, rug=True, label=name,
                 rug_kws={'height': 0.025})
  plt.title(title)
  plt.xlabel(xlabel)
  plt.ylabel(ylabel)
  plt.savefig(filename)
  plt.show(block=False)
  plt.close()


class MICCEvaluator(ShapeEvaluator):
  """ Shape evaluator for MICC dataset """

  def __init__(self, pargs, logger):
    """
    Constructor
    :param pargs: Parsed command line arguments
    :param logger:  Logger instance
    """
    super(MICCEvaluator, self).__init__(args=pargs, logger=logger)
    self.micc_nose_idx = _micc_nose_idx
    self.evaluator = RegistrationEvaluator(matcher='closest_point',
                                           metric=ICPPointToPlaneMetric(True))

  def run_reconstruction(self, model, output):
    """
    Run face reconstruction for the underlying dataset
    :param model:     Model to use for reconstructing the surface, type of
                      `EvaluationModel` instance
    :param output:    Root location where to place the data

    """
    self.logger.info('Run MICC shape reconstruction...')

    # Search for tfrecords
    # ------------------------------------------------
    for part in ['cooperative', 'indoor', 'outdoor']:
      records = search_folder(folder=_join(self.pargs.micc_records, part),
                              ext=['.tfrecords'],
                              relative=False)
      for s_id, subj_record in enumerate(records):
        # Create dataset
        dataset = MICCShapeLoader.from_file(path=subj_record,
                                            batch=32,
                                            image_dim=model.cfg.img_dims[0])
        # Output location
        subj_folder = _join(output,
                            basename(self.pargs.morphnet_model_dir),
                            part,
                            'subject_{:02d}'.format(s_id + 1))
        if not _exists(subj_folder):
          _mkdir(subj_folder)
        self.logger.info('Process: %s',
                         _join(part, 'subject_{:02d}'.format(s_id + 1)))

        emb_name = _join(subj_folder, 'shape_p.npy')
        if not _exists(emb_name):
          w_shp_avg = None
          n_frame = 0
          for k, frame in enumerate(dataset):
            # Get latent code for a batch of images
            preds = model.predict(frame)
            w_shp = preds[0]
            w_tex = preds[1]
            rendering = preds[-1]
            shp_p = tf.reduce_sum(w_shp, axis=0).numpy()
            w_shp_avg = (shp_p if w_shp_avg is None else w_shp_avg + shp_p)
            n_frame += w_shp.shape[0]
            # Reconstruct shape
            if k % 1 == 0:
              # Dump mesh
              m = create_mesh(model, w_shp[0:1, ...].numpy(),
                              model.cfg.model.ns,
                              model.cfg.model.nt)
              m.save(_join(subj_folder, 'step{:04d}.obj'.format(k)))
              # Dump image
              img = rendering[0, ..., :3].numpy()
              img = (img * 255.0).astype(np.uint8)
              imsave(_join(subj_folder, 'step{:04d}.jpg'.format(k)), img)
          # Average embeddings, create mesh
          w_shp_avg /= float(n_frame)
          w_shp_avg[:, model.cfg.model.ns:] = 0.0
          np.save(emb_name, w_shp_avg, allow_pickle=True)
        else:
          # reuse
          self.logger.warning('Reuse previous reconstruction')
          w_shp_avg = np.load(emb_name, allow_pickle=True)
        # Create mesh
        m = create_mesh(model,
                        np.expand_dims(w_shp_avg, 0),
                        model.cfg.model.ns,
                        model.cfg.model.nt)
        m.save(_join(subj_folder, 'full_shape.obj'))

        # # TODO(christophe): Debug purpose
        # break

  def run_alignment(self, output, iter=200):
    """
    Run surface alignment between ground truth and reconstruction
    tip
    :param output:  Location where to place the data
    :param iter:    Maximum number of iteration to run for ICP
    """

    # Prepare ground truth
    self._prepare_ground_truth(output)
    # Do rigid alignment
    self._align_reconstructions(output, iter, rmv_nostril=True)

  def _prepare_ground_truth(self, output):
    """
    Prepare ground truth, i.e. crop surface from nose tip
    :param output:  Location where to place data
    """
    self.logger.info('Crop ground truth')
    root_true = _join(output, 'cropped_true')
    if not _exists(root_true):
      _mkdir(root_true)
    # Iterate over every subjects
    for s_id in range(1, 54):
      # Ground truth
      fname = _join(root_true, 'subject{:02d}.obj'.format(s_id))
      if not _exists(fname):
        gt_path = _join(self.pargs.micc_folder,
                        'subject_{:02d}'.format(s_id),
                        'Model',
                        'frontal1',
                        'obj')
        if not _exists(gt_path) or _ls(gt_path) == ['.listing']:
          # Subject 27 has no obj mesh in 'frontal1', but 2 in 'frontal2'
          gt_path = gt_path.replace('frontal1', 'frontal2')
        gt_path = search_folder(folder=gt_path, ext=['.obj'], relative=False)
        # 1. Load ground truth
        # 2. Crop
        # 3. Place it at the origin
        gt_mesh = Mesh(gt_path[-1])  # Pick last
        gt_nose_tip_idx = self.micc_nose_idx[s_id]
        gt_mesh.crop_around_vertex(index=gt_nose_tip_idx,
                                   distance=self.pargs.crop_distance)
        gt_mesh.remesh_for_triangle(subset=gt_mesh.tri)
        gt_mesh.vertex -= gt_mesh.vertex.mean(axis=0)

        # Align mesh with system coordinate system using PCA
        if s_id == 15:
          # For some reason PCA approach does not work on subject 15
          r = Rodrigues(axis=(1.0, 0.0, 0.0), angle=np.deg2rad(-40.0))
          trsfrm = r.to_rotation_matrix()[:3, :3]
          gt_mesh.vertex = gt_mesh.vertex @ trsfrm.T
        else:
          pca = PCA()
          pca.fit(X=gt_mesh.vertex)
          Rmat = np.zeros(shape=(3, 3), dtype=np.float32)
          Rmat[0, :] = pca.components_[1, :]
          Rmat[1, :] = pca.components_[0, :]
          e0 = np.asarray([1.0, 0.0, 0.0], dtype=np.float32)
          e1 = np.asarray([0.0, 1.0, 0.0], dtype=np.float32)
          if np.dot(Rmat[0, :], e0) < 0.0:
            Rmat[0, :] *= -1.0
          if np.dot(Rmat[1, :], e1) < 0.0:
            Rmat[1, :] *= -1.0
          Rmat[2, :] = np.cross(Rmat[0, :], Rmat[1, :])  # Avoid reflection
          gt_mesh.vertex = gt_mesh.vertex @ Rmat.T
        gt_mesh.save(fname)

  def _align_reconstructions(self, output, iter, rmv_nostril):
    """
    Align reconstruction with ground truth
    :param output:  Location where data are stored
    :param iter:  Maximum number of iteration for ICP
    :param rmv_nostril: If `True` remove vertex from nostrils
    """
    for part in ['cooperative', 'indoor', 'outdoor']:
      self.logger.info('Align partition: {}'.format(part))
      for s_id in range(1, 54):
        if (s_id - 1) % 10 == 0:
          self.logger.info('Process subject {}'.format(s_id))
        # Load ground truth
        gt_path = _join(output,
                        'cropped_true',
                        'subject{:02d}.obj'.format(s_id))
        # Predicted mesh
        pred_path = _join(output,
                          basename(self.pargs.morphnet_model_dir),
                          part,
                          'subject_{:02d}'.format(s_id),
                          'full_shape.obj')
        pred_mesh = Mesh(pred_path)
        if rmv_nostril:
          _remove_nostrils(pred_mesh)
        # Align
        icp = ICPAlignment(pred_mesh, gt_path)
        aligned_m, dist = icp.align(n_iter=iter)
        # Crop
        idx = 8173 if rmv_nostril else self.pargs.bfm_nose_idx
        aligned_m.crop_around_vertex(index=idx,
                                     distance=self.pargs.crop_distance)
        aligned_m.remesh_for_triangle(subset=aligned_m.tri)
        # Save aligned mesh
        fname = _join(output,
                      basename(self.pargs.morphnet_model_dir),
                      part,
                      'subject_{:02d}'.format(s_id),
                      'aligned_shape.obj')
        aligned_m.save(fname)

        # # TODO(christophe): Debug purpose
        # break

  def run_evaluation(self, output):
    """
    Compute surface evaluation metrics. Apply first rigid ICP with isotropic
    scaling
    :param output:    Location where to dump the data
    """

    results = np.zeros(shape=(3, 53, 2), dtype=np.float32)
    for k, part in enumerate(['cooperative', 'indoor', 'outdoor']):
      self.logger.info('Evaluate partition: {}'.format(part))
      for s_id in range(1, 54):
        if (s_id - 1) % 10 == 0:
          self.logger.info('Process subject {}'.format(s_id))

        # Load ground truth
        gt_path = _join(output,
                        'cropped_true',
                        'subject{:02d}.obj'.format(s_id))
        gt_mesh = Mesh(gt_path)
        gt_mesh.compute_vertex_normal()
        # Aligned reconstruction
        rec_path = _join(output,
                         basename(self.pargs.morphnet_model_dir),
                         part,
                         'subject_{:02d}'.format(s_id),
                         'aligned_shape.obj')
        rec_mesh = Mesh(rec_path)
        rec_mesh.compute_vertex_normal()

        # Error
        avg_dist, error_map = self.evaluator.evaluate(x=rec_mesh.vertex,
                                                      y=gt_mesh.vertex,
                                                      nx=rec_mesh.normal,
                                                      ny=gt_mesh.normal)
        rmse_pt2pl = (self.evaluator.per_pts_distance ** 2.0).mean() ** 0.5

        # Add Point-to-plane error
        l = s_id - 1
        results[k, l, 0] = avg_dist
        # Add Point-to-plane RMSE
        results[k, l, 1] = rmse_pt2pl

        # Error map
        add_colormap(rec_mesh, error_map)
        err_path = rec_path.replace('aligned_shape.obj', 'error_map.ply')
        rec_mesh.save(err_path)
    # Plot error distribution
    fname = _join(output,
                  basename(self.pargs.morphnet_model_dir),
                  'micc_error_distribution.pdf')
    plot_error_distribution(results[..., 0],
                            ['Cooperative', 'Indoor', 'Outdoor'],
                            title='Error distribution',
                            xlabel='Point-to-plane error [mm]',
                            ylabel='Density',
                            filename=fname)
    fname = _join(output,
                  basename(self.pargs.morphnet_model_dir),
                  'micc_rmse_distribution.pdf')
    plot_error_distribution(results[..., 1],
                            ['Cooperative', 'Indoor', 'Outdoor'],
                            title='RMSE distribution',
                            xlabel='Point-to-plane RMSE [mm]',
                            ylabel='Density',
                            filename=fname)
    # Save results
    fname = _join(output,
                  basename(self.pargs.morphnet_model_dir),
                  'alignment_result.npy')
    np.save(fname, results)
    return results
