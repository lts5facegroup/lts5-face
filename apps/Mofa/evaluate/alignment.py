# coding=utf-8
""" Run mesh alignment """
import numpy as np
from sklearn.neighbors import KDTree
from lts5.utils import ColorMapFloat as CMap
from lts5.geometry import MeshFloat as Mesh
from lts5.geometry import SICPRegistration
from lts5.geometry import IterativeClosestPoint, RegistrationEvaluator
from lts5.geometry import ICPNormalSpaceSampling, ICPGlobalSelector
from lts5.geometry import ICPNormalShootingMatcher
from lts5.geometry import ICPMedianDistanceFilter
from lts5.geometry import ICPPointToPlaneMetric
__author__ = 'Christophe Ecabert'


def _generate_color_map(err, cmap, m):
  """
  Convert error to color map
  :param err:   Vector of error
  :param cmap:  Colormap instance to use for mapping
  :param m:     Mesh instance
  """
  colors = np.ones_like(m.vertex) * 0.5  # Gray
  for k, e in enumerate(err):
    if e >= 0.0:
      colors[k, :] = cmap.pick_color(e)
  m.vertex_color = colors


class ICPAlignment:
  """ Perform ICP with isotropic scale """

  def __init__(self,
               source,
               target,
               pre_scaling=True):
    """
    Constructor
    :param source:  Source surface (mesh or str)
    :param target:  Target surface (mesh or str)
    :param pre_scaling: If `True` pre scale source surface in order to have
                        similar scale with target, otherwise no.
    """
    # Load mesh if needed
    src = Mesh(source) if isinstance(source, str) else source
    tgt = Mesh(target) if isinstance(target, str) else target
    # Define mesh
    self.source = src
    self.target = tgt
    self.source.compute_vertex_normal()
    self.target.compute_vertex_normal()
    # Place both mesh at origin
    self.source.vertex -= self.source.vertex.mean(axis=0)
    self.target.vertex -= self.target.vertex.mean(axis=0)
    # Pre scaling
    if pre_scaling:
      pre_scale = (np.linalg.norm(self.target.vertex, axis=1).mean() /
                   np.linalg.norm(self.source.vertex, axis=1).mean())
      self.source.vertex *= pre_scale

    # Initialize ICP
    # selector = ICPNormalSpaceSampling(ratio=0.2,
    #                                   normals=self.source.normal,
    #                                   bin_per_channel=8)
    selector = ICPGlobalSelector()
    matcher = ICPNormalShootingMatcher(y=self.target.vertex)
    filter = ICPMedianDistanceFilter(factor=1.5)
    metric = ICPPointToPlaneMetric(isotropic_scale=True)
    self.icp = IterativeClosestPoint(selector=selector,
                                     matcher=matcher,
                                     filter=filter,
                                     metric=metric)

  def align(self, n_iter=20):
    """
    Run alignment
    :param n_iter:  Number of iteration to do
    :return: Aligned mesh, distance
    """
    distance = self.icp.align(source=self.source,
                              target=self.target,
                              n_iter=n_iter)
    aligned_m = self.icp.transform(self.source)
    return aligned_m, distance
