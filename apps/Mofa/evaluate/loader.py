#coding=utf-8
""" Data loader for evalutation """
from multiprocessing import cpu_count
import tensorflow as tf
from lts5.tensorflow_op.data import DataProvider


class MICCShapeLoader(DataProvider):
  """ Data loader for MICC shape evaluation """

  def __init__(self,
               path,
               image_dim=227,
               n_parallel_call=None):
    """
    Constructor
    :param path:  Path to a tfrecord file
    :param n_parallel_call: Number of parallel call to run
    """
    super(MICCShapeLoader, self).__init__()
    # Setup
    self._n_call = n_parallel_call
    if self._n_call is None:
      self._n_call = cpu_count()
    self._image_sz = [image_dim, image_dim, 3]
    # Reader
    self._source = tf.data.TFRecordDataset(path)

  @classmethod
  def from_file(cls,
                path,
                batch=1,
                image_dim=227):
    """
    Create an iterator for a given tfrecords file
    :param path:      Path to the tfrecord file
    :param batch:     Batch size
    :param image_dim: Image dimension
    :return:  Dataset iterator
    """
    dset = (cls(path=path, image_dim=image_dim)
            .decode()
            .preprocess()
            .batch(batch, drop_remainder=False)
            .prefetch(1))
    return dset.one_shot_iterator()

  def decode(self):
    """
    Function parsing input dataset and generate tensors (i.e. image + labels)
    :return: A `DataProvider`
    """

    def _parse_fn(example):
      fmt = {'image': tf.io.FixedLenFeature([], tf.string, "")}
      parsed = tf.io.parse_single_example(example, fmt)
      # Image
      img = tf.io.decode_jpeg(parsed['image'])
      img.set_shape(self._image_sz)
      img = tf.cast(img, tf.float32)
      return img

    self._source = self._source.map(_parse_fn)
    return self

  def preprocess(self):
    """
    Apply all the pre-processing / augmentation steps on the dataset
    :return: A `DataProvider`
    """

    # def _preprocess(example):
    #   return example
    #
    # # Only convert image into BGR format
    # self._source = self._source.map(_preprocess)
    return self


class LFWAppearanceLoader(DataProvider):
  """ Data loader for LFW appearance evaluation """

  def __init__(self,
               path,
               image_dim=227,
               n_parallel_call=None):
    """
    Constructor
    :param path:  Path to a tfrecord file
    :param image_dim: Image dimensions
    :param n_parallel_call: Number of parallel call to run
    """
    super(LFWAppearanceLoader, self).__init__()
    # Setup
    self._n_call = n_parallel_call
    if self._n_call is None:
      self._n_call = cpu_count()
    self._image_sz = [image_dim, image_dim, 3]
    # Reader
    self._source = tf.data.TFRecordDataset(path)

  @classmethod
  def from_file(cls,
                path,
                batch=1,
                image_dim=224):
    """
    Create an iterator for a given tfrecords file
    :param path:      Path to the tfrecord file
    :param batch:     Batch size
    :param image_dim: Image dimension
    :return:  Dataset iterator
    """
    dset = (cls(path=path, image_dim=image_dim)
            .decode()
            .preprocess()
            .batch(batch, drop_remainder=False)
            .prefetch(1))
    return dset.one_shot_iterator()

  def decode(self):
    """
    Function parsing input dataset and generate tensors (i.e. image + labels)
    :return: A `DataProvider`
    """

    def _parse_fn(example):
      fmt = {'image': tf.io.FixedLenFeature([], tf.string, ""),
             'label': tf.io.FixedLenFeature([], tf.string, "")}
      parsed = tf.io.parse_single_example(example, fmt)
      # Image
      img = tf.io.decode_jpeg(parsed['image'])
      img.set_shape(self._image_sz)
      img = tf.cast(img, tf.float32)
      return img, parsed['label']

    self._source = self._source.map(_parse_fn)
    return self

  def preprocess(self):
    """
    Apply all the pre-processing / augmentation steps on the dataset
    :return: A `DataProvider`
    """

    def _preprocess(image, label):
      img = image
      bg = image / 255.0
      return img, bg, label  # [0, 255]

    # Preprocess
    # self._source = self._source.map(_preprocess)
    return self