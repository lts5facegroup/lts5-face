#coding=utf-8
""" model abstracion for evaluation """
from os.path import join as _join
import tensorflow as tf
import tensorflow.keras.layers as kl
from lts5.tensorflow_op.network import MorphNetParameter
from lts5.tensorflow_op.network import MorphNet
from lts5.tensorflow_op.layers.statistical_model import MorphableModel
__author__ = 'Christophe Ecabert'


class ShapeModel:

  def __init__(self,
               config: str,
               model_dir: str,
               full_model=False):
    """
    Constructor
    :param config:      Path to config file (i.e. json)
    :param model_dir:   Location where model is stored
    :param full_model: If `True`, the whole face model will be used instead of
      the cropped version.
    """
    # Configuration
    self.cfg = MorphNetParameter.from_json(config)
    # Predictor
    self.pred = self._initialize_predictor(self.cfg, model_dir)
    # Morphable Model
    fname = self.cfg.model.path
    if full_model:
      fname = fname.replace('basel_face_exp_',
                            'basel_face_exp_full_')
    self.mmodel = MorphableModel(fname)

  def _initialize_predictor(self, config, model_dir):
    """
    Initialize model predictor that map image to model coefficients. The model
    must have the following properties
      inputs:   Image
      outputs:  Latent code, Wshp, Wtex, Reconstructed image
    :param config:      Model configurations (Dict)
    :param model_dir:   Folder storing model's weight
    :return:  Predictor
    """
    raise NotImplementedError('Must be implemented by subclass')

  def predict(self, image):
    """
    Run inference for model prediction
    :param image: Image to process
    :return:  Latent code, Wshp, Wtex, Reconstruction
    """
    return self.pred(image, training=False)

  def generate_surface(self, w_shp, w_tex):
    """
    Generate mesh from parameters (without illumination)
    :param w_shp: Shape parameters (id + exp)
    :param w_tex: Texture parameters
    :return:  Vertex, Color, Triangles
    """
    model, inner_loss = self.mmodel([w_shp, w_tex])
    # model == Vertex, Color, Triangles
    return model


class ModelWithDisentangledFeatureSpace(ShapeModel):
  """
  Build model for evaluation based on disentangled feature space
  """

  def __init__(self,
               model_dir: str,
               full_model: bool):
    """
    Constructor
    :param model_dir:   Location where model is stored
    :param full_model: If `True`, the whole face model will be used instead of
      the cropped version.
    """
    config = _join(model_dir, 'config.json')
    super(ModelWithDisentangledFeatureSpace, self).__init__(config,
                                                            model_dir,
                                                            full_model)

  def _initialize_predictor(self, config, model_dir):
    """
    Initialize model predictor that map image to model coefficients. The model
    must have the following properties
      inputs:   Image
      outputs:  Latent code, Wshp, Wtex
    :param config:      Model configurations (Dict)
    :param model_dir:   Folder storing model's weight
    :return:  Predictor
    """
    img_ph = kl.Input(shape=config.img_dims, name='Image')
    model = MorphNet(config=config)
    renderings = model(img_ph, training=False)
    # Restore model
    latest = tf.train.latest_checkpoint(model_dir)
    s = model.load_weights(latest)#.assert_existing_objects_matched()
    # Coefficients predictor
    params = model.get_layer('MultiRegressionHead').output
    pred_output = params + [renderings[0]]
    pred_input = [img_ph]
    predictor = tf.keras.Model(inputs=pred_input,
                               outputs=pred_output, name='Predictor')
    return predictor


