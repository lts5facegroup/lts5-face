#coding=utf-8
""" Shape evaluation """
from os.path import exists as _exists
from os.path import join as _join
from argparse import ArgumentParser
import numpy as np
from lts5.utils.tools import init_logger
from lts5.tensorflow_op.utils import initialize_device_memory

from model import ModelWithDisentangledFeatureSpace as ShapeModel
from micc import MICCEvaluator
from facewarehouse import FaceWarehouseEvaluator


__author__ = 'Christophe Ecabert'

logger = init_logger()
initialize_device_memory()


def _micc_evaluation(pargs):
  """
  Evaluation protocal for MICC dataset
  :param pargs: Parsed argumentss
  """
  # Model
  model = ShapeModel(pargs.morphnet_model_dir, full_model=False)
  # MICC EValuation
  micc_output = _join(pargs.output, 'micc')
  micc_eval = MICCEvaluator(pargs=pargs,
                            logger=logger)
  micc_eval.run_reconstruction(model, micc_output)
  micc_eval.run_alignment(micc_output, iter=25)
  res_micc = micc_eval.run_evaluation(micc_output)
  micc_eval.print(results=res_micc,
                  partitions=['cooperative', 'indoor', 'outdoor'],
                  metrics=['Pt2Pl', 'Pt2PlRmse'],
                  folder=micc_output)
  # Clean up
  del model, micc_eval


def _facewarehouse_evaluation(pargs):
  """
  Evaluation protocal for FaceWarehouse dataset
  :param pargs: Parsed argumentss
  """
  # Model
  model = ShapeModel(pargs.morphnet_model_dir, full_model=True)
  # FW Evaluation
  fw_output = _join(pargs.output, 'facewarehouse')
  fw_eval = FaceWarehouseEvaluator(pargs=pargs,
                                   logger=logger,
                                   image_sz=model.cfg.rendering.width)
  fw_eval.run_reconstruction(model, fw_output)
  fw_eval.run_alignment(fw_output, iter=25)
  res_fw = fw_eval.run_evaluation(fw_output)
  fw_eval.print(results=res_fw,
                partitions=['small', 'larger'],
                metrics=['Pt2Pt'],
                folder=fw_output)
  # Clean up
  del model, fw_eval


def _evaluate(pargs):
  """
  Run shape evaluation
  :param pargs:   Parsed arguments
  :param verbose: If `True` add extra output
  """

  # MICC
  _micc_evaluation(pargs=pargs)
  # FaceWarehouse
  _facewarehouse_evaluation(pargs=pargs)


if __name__ == '__main__':

  p = ArgumentParser()
  # Dataset
  p.add_argument('--micc_records',
                 type=str,
                 required=True,
                 help='Location where MICC tfrecords are stored')
  p.add_argument('--micc_folder',
                 type=str,
                 required=True,
                 help='Location where MICC dataset is stored')
  p.add_argument('--facewarehouse_folder',
                 type=str,
                 required=True,
                 help='Root location where FaceWarehouse dataset is stored')
  # Network configuration
  p.add_argument('--morphnet_model_dir',
                 type=str,
                 required=True,
                 help='Model location')
  # BFM nose tip index
  p.add_argument('--bfm_nose_idx',
                 type=int,
                 default=8191,
                 help='Vertex index of the nose tip')
  p.add_argument('--facewarehouse_nose_idx',
                 type=int,
                 default=8972,
                 help='Vertex index of the nose tip')
  p.add_argument('--crop_distance',
                 type=float,
                 default=95,
                 help='Cropping distance from the nose tip')
  p.add_argument('--bfm_vertex_map',
                 type=str,
                 default='idx_vertex_map.txt',
                 help='Vertex correspondance between redistributed BFM topology'
                      ' and dense ground truth facewarehouse topology')
  p.add_argument('--uniform_bfm_correspondence',
                 type=str,
                 default='mofa_to_out_topo.txt',
                 help='Vertex correspondance between original BFM topology and'
                      ' redistributed BFM topology')
  p.add_argument('--facewarehouse_mask',
                 type=str,
                 default='mask.obj',
                 help='Face region for error evaluation')
  # Output
  p.add_argument('--output',
                 type=str,
                 required=True,
                 help='Location where to place the results')

  args = p.parse_args()
  _evaluate(pargs=args)