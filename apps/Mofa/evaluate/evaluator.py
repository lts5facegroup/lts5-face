#coding=utf-8
""" Shape evaluator interface """
from os.path import join as _join
from os.path import basename
import numpy as np
from lts5.geometry import MeshFloat as Mesh


def create_mesh(model, w_shp, n_id, n_tex, w_tex=None, with_exp=False):
  if w_tex is None:
    if not with_exp:
      w_shp[..., n_id:] = 0.0  # Rmv expression
    t_shp = (1, n_tex) if len(w_shp) == 2 else (1, w_shp.shape[1], n_tex)
    w_tex = np.zeros(t_shp, dtype=np.float32)
  shp, color, tri = model.generate_surface(w_shp, w_tex)
  # Create mesh
  m = Mesh()
  m.vertex = shp.numpy().reshape(-1, 3)
  # m.vertex_color = color.numpy().reshape(-1, 3)
  m.tri = tri.numpy().astype(np.int32)
  return m


class ShapeEvaluator:
  """ High level class abstraction for shape evaluation """

  def __init__(self, args, logger):
    """
    Constructor
    :param args:    Parsed arguments from cmd line
    :param logger:  Logger instance
    """
    self.pargs = args
    self.logger = logger

  def run_reconstruction(self,
                         model,
                         output):
    """
    Run face reconstruction for the underlying dataset
    :param model:     Model to use for reconstructing the surface, type of
                      `EvaluationModel` instance
    :param output:    Root location where to place the data
    :param verbose:   If True add extra output
    """
    raise NotImplementedError('Must be implemented by derived class')

  def run_alignment(self, output, iter=200):
    """
    Run surface alignment between ground truth and reconstruction
    tip
    :param output:  Location where to place the data
    :param iter:    Maximum number of iteration to run for ICP
    """
    raise NotImplementedError('Must be implemented by derived class')

  def run_evaluation(self, output):
    """
    Compute surface evaluation metrics. Apply first rigid ICP with isotropic
    scaling
    :param output:    Location where to dump the data
    """
    raise NotImplementedError('Must be implemented by derived class')

  def print(self, results, partitions, metrics, folder=None):
    """
    Dump metrics statistics into console
    :param results: Numpy array of shape: #Partitions, #Subject, #Metric
    :param partitions: List of partitions
    :param metrics: List of metrics name
    :param folder: Location where to dump results text file
    """
    # Aggregate results
    n_part, n_subj, n_metric = results.shape
    # Statistics holder
    mean_metric = np.zeros(shape=(n_metric, n_part), dtype=np.float32)
    std_metric = np.zeros_like(mean_metric)
    min_max_metric = np.zeros(shape=(n_metric, n_part, 2), dtype=np.int32)
    for k, res in enumerate(results):
      mean_metric[:, k] = res.mean(axis=0)
      std_metric[:, k] = res.std(axis=0)
      min_max_metric[:, k, 0] = np.argmin(res, axis=0)
      min_max_metric[:, k, 1] = np.argmax(res, axis=0)

    # Plot
    m_name_width = max([len(x) for x in metrics])
    p_name_width = max(max(len(x) for x in partitions), 13)
    col_header = []
    for part in partitions:
      part_sz = len(part)
      front_space = 1
      back_space = 1
      if part_sz < p_name_width:
        front_space = (p_name_width + 2 - part_sz) // 2
        back_space = p_name_width + 2 - front_space - part_sz
      line = '{}{}{}'.format(' ' * front_space,
                             part,
                             ' ' * back_space)
      col_header.append(line)
    # Header
    msg = []
    hdr = ' ' * (m_name_width + 1) + '|' + '|'.join(col_header)
    msg.append(hdr)
    self.logger.info(hdr)
    spaces = '=' * len(hdr)
    self.logger.info(spaces)
    msg.append(spaces)
    zip_args = (metrics, mean_metric, std_metric, min_max_metric)
    for r_hdr, r_avg, r_std, r_range in zip(*zip_args):
      # Mean/std metrics
      n_space = m_name_width - len(r_hdr)
      line = '{}{} '.format(r_hdr, ' ' * n_space)
      for k, part in enumerate(col_header):
        sz = len(part)
        front_space = (sz - 11) // 2
        back_space = sz - front_space - 11
        line += '|{}{:.2f} \u00B1 {:.2f}{}'.format(' ' * front_space,
                                                   r_avg[k],
                                                   r_std[k],
                                                   ' ' * back_space)
      self.logger.info(line)
      msg.append(line)
      # Add min/max index
      n_space = m_name_width
      line = '{} '.format(' ' * n_space)
      for k, part in enumerate(col_header):
        sz = len(part)
        front_space = (sz - 7) // 2
        back_space = sz - front_space - 7
        line += '|{}{:02d} / {:02d}{}'.format(' ' * front_space,
                                              r_range[k, 0],
                                              r_range[k, 1],
                                              ' ' * back_space)
      self.logger.info(line)
      msg.append(line)
    # Dump sample ranking as well
    msg.append('')
    sample_rank = np.argsort(results, axis=1)  # [#Part, #Subject, #Metric]
    for p, part in enumerate(partitions):
      msg.append('Ranking: {}'.format(part))
      for m, metric in enumerate(metrics):
        line = '  {}: {}'.format(metric, sample_rank[p, :, m])
        msg.append(line)

    # Dump results into text file
    if folder is not None:
      fname = _join(folder,
                    basename(self.pargs.morphnet_model_dir),
                    'results.txt')
      with open(fname, 'wt') as f:
        for m in msg:
          f.write('{}\n'.format(m))
