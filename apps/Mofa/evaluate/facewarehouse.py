# coding=utf-8
""" Evaluation protocol for face warehouse dataset """
from os.path import exists as _exists
from os import makedirs as _mkdir
from os.path import join as _join
from os.path import basename
import numpy as np
from imageio import imsave
import matplotlib.pyplot as plt
import seaborn as sns
import tensorflow as tf
from sklearn.neighbors import NearestNeighbors
from lts5.geometry import MeshFloat as Mesh

from evaluator import ShapeEvaluator, create_mesh


def get_closest_point(s, gt, nbrs):
  """
  Find closest point of s on gt using KD tree
  :param s:
  :param gt:
  :param nbrs:
  :return:
  """
  dist, idx = nbrs.kneighbors(s)
  idx = np.reshape(idx, [s.shape[0]])
  return gt[idx, :], idx, dist


def get_transformation(source, target, scale=False):
  """
  Calculate scale + rigid transformation using matched closest points
  :param source:  Source point
  :param target:  Target points
  :param scale:   If `True` estimate scaling factor as well
  :return:  Rotation, Translation, Scale
  """
  tar = target.copy()
  sou = source.copy()
  center_tar = tar - np.mean(tar, 0)
  # Rotation
  center_sou = sou - np.mean(sou, 0)
  W = np.matmul(center_tar.transpose(), center_sou)
  U, S, V = np.linalg.svd(W)
  R = np.matmul(np.matmul(V.transpose(),
                          np.diag([1,
                                   1,
                                   np.linalg.det(np.matmul(V.transpose(),
                                                           U.transpose()))])),
                U.transpose())
  # Scaling
  if scale:
    R_sou = np.matmul(center_sou, R)
    s = np.sum(R_sou * center_tar) / np.sum(R_sou * R_sou)
  else:
    s = 1.0
  # Translation
  t = np.mean(tar, 0) - s * np.matmul(np.expand_dims(np.mean(sou, 0), 0), R)
  return R, t, s


def icp(source, target, landmark_s, landmark_t, nbrs, max_iter=10):
  sou = source.copy()
  tar = target.copy()

  i = 0
  tt = np.zeros([1, 3]).astype(np.float32)
  RR = np.eye(3)
  ss = 1
  while i < max_iter:
    # using landmarks as initialization
    if i < 1:
      s_match = landmark_s
      t_match = landmark_t
    else:
      s_match = sou
      t_match, _, _ = get_closest_point(sou, tar, nbrs)

    R, t, s = get_transformation(s_match, t_match, scale=True)
    RR = np.matmul(RR, R)
    tt = t + s * np.matmul(tt, R)
    ss = ss * s
    sou = s * np.matmul(sou, R) + t
    i += 1

  return sou, RR, tt, ss


def point_to_point_distance(source, target, nbrs):

  _, idx, dist = get_closest_point(source, target, nbrs)
  return dist


def reconstruct_distance(source, target, landmark_s, landmark_t):
  """
  Compute distance between reconstruction and ground truth
  :param source:      Source surface
  :param target:      Target surface
  :param landmark_s:  Landmarks on source surface
  :param landmark_t:  Landmarks on target surface
  :return:
  """
  nbrs = NearestNeighbors(n_neighbors=1, algorithm='kd_tree').fit(target)
  sou, RR, tt, ss = icp(source,
                        target,
                        landmark_s,
                        landmark_t,
                        nbrs,
                        max_iter=1)
  error = point_to_point_distance(sou, target, nbrs)
  return error, RR, tt, ss


class FaceWarehouseEvaluator(ShapeEvaluator):
  """ FaceWarehouse evaluator """

  def __init__(self,
               pargs,
               logger,
               image_sz):
    """
        Constructor
        :param pargs: Parsed command line arguments
        :param logger:  Logger instance
        :param image_sz: Image dimension
        :param verbose:   If True add extra output
        """
    super(FaceWarehouseEvaluator, self).__init__(args=pargs,
                                                 logger=logger)
    self.img_sz = image_sz
    # Vertex correspondence between redistributed BFM topo and dense ground
    # truth facewarehouse topo
    self.corres = np.loadtxt(pargs.bfm_vertex_map).astype(np.int32)
    # Vertex correspondence between original BFM topo and redistributed BFM topo
    self.vertex_map = np.loadtxt(pargs.uniform_bfm_correspondence)
    m = Mesh(pargs.facewarehouse_mask)
    vc = m.vertex_color
    self.converted_tri = m.tri.copy()
    self.mask = {}
    # Small region == Red channel
    self.mask['small'] = (vc[:, 0] == 1.0).astype(np.int32)
    # Larger region == Blue channel
    self.mask['larger'] = ((vc[:, 2] == 1.0).astype(np.int32) +
                           self.mask['small'])
    self.results = None

  def run_reconstruction(self, model, output):
    """
    Run face reconstruction for the underlying dataset
    :param model:     Model to use for reconstructing the surface, type of
                      `EvaluationModel` instance
    :param output:    Root location where to place the data

    """
    self.logger.info('Run FaceWarehouse shape reconstruction...')

    # Destination folder
    dest_folder = _join(output,
                        basename(self.pargs.morphnet_model_dir),
                        'reconstruction')
    if not _exists(dest_folder):
      _mkdir(dest_folder)

    # Reconstruction
    # ------------------------------------------------
    dset = self._create_dataset(self.img_sz)
    for image, filename in dset:
      # Get latent code
      preds = model.predict(image)
      w_shp = preds[0].numpy()
      rendering = preds[-1].numpy()
      filename = filename.numpy()[0].decode()
      # Dump mesh, rendering, w_shp
      m = create_mesh(model,
                      w_shp,
                      model.cfg.model.ns,
                      model.cfg.model.nt,
                      with_exp=True)
      mesh_name = filename.replace('.jpg', '.obj')
      m.save(_join(dest_folder, 'rec_{}'.format(mesh_name)))
      # Dump image
      img = rendering[0, ..., :3]
      img = (img * 255.0).astype(np.uint8)
      imsave(_join(dest_folder, 'rec_{}'.format(filename)), img)
      emb_name = _join(dest_folder,
                       'rec_{}'.format(mesh_name.replace('.ply', '.npy')))
      np.save(emb_name, w_shp, allow_pickle=True)

  def _create_dataset(self,
                      img_sz,
                      batch_size=1):
    """
    Create tensorflow dataset for a given image folder
    :param img_sz: Image size
    :param batch_size: Batch size
    :return: tf.data.Dataset instance ready to be used
    """

    def _load_image(path):
      # Define filename
      filename = tf.strings.split(path, '/')[-1]
      img_str = tf.io.read_file(path)  # Raw image string
      img = tf.io.decode_jpeg(img_str)
      img.set_shape([img_sz, img_sz, 3])
      img = tf.cast(img, tf.float32)
      return img, filename

    pattern = _join(self.pargs.facewarehouse_folder, 'images', '*.jpg')
    dset = tf.data.Dataset.list_files(pattern, shuffle=False)
    dset = (dset.map(_load_image, tf.data.experimental.AUTOTUNE)
            .batch(batch_size, False)
            .prefetch(tf.data.experimental.AUTOTUNE))
    return dset

  def run_alignment(self, output, iter=200):
    """
    Run surface alignment between ground truth and reconstruction
    tip
    :param output:  Location where to place the data
    :param iter:    Maximum number of iteration to run for ICP
    """
    self.logger.info('Run FaceWarehouse shape cropping/alignment ...')
    # Reconstruction folder
    rec_folder = _join(output,
                       basename(self.pargs.morphnet_model_dir),
                       'reconstruction')
    # Goes through all
    self.results = np.zeros(shape=(2, 180, 1), dtype=np.float32)
    for p, part in enumerate(['small', 'larger']):
      n = 0
      for i in [4, 136, 138, 148, 150, 145, 146, 147, 149]:
        for k in range(20):
          # Get current reconstruction result
          rec_path = _join(rec_folder,
                           'rec_subj{:03d}_pose{:02d}.obj'.format(i, k))
          m = Mesh(rec_path)
          rec_shape = m.vertex

          # For each reconstruction result, transfer its topo from BFM to a
          # re-distributed one(60,000 vertex evenly-distributed) created by
          # Tewari et al.(ICCV 17)
          conv_shape = self._transfer_topology(rec_shape, m.tri)

          # Select proper region for error evaluation
          recon_frontmask = self.mask[part]
          reconstruct_shape_ = conv_shape[recon_frontmask == 1, :]
          # Load ground truth
          gt_path = _join(self.pargs.facewarehouse_folder,
                          'meshes',
                          'subj{:03d}_pose{:02d}.obj'.format(i, k))
          gt_mesh = Mesh(gt_path)
          # ICP, calculate error
          shape_src = reconstruct_shape_
          shape_tgt = gt_mesh.vertex
          lms_src = conv_shape[self.corres[recon_frontmask == 1, 0], :]
          lms_tgt = gt_mesh.vertex[self.corres[recon_frontmask == 1, 1], :]
          error, RR, tt, ss = reconstruct_distance(shape_src,
                                                   shape_tgt,
                                                   lms_src,
                                                   lms_tgt)
          # Store error
          self.results[p, n, 0] = error.mean() * 100.0
          n += 1
    # Plot and save error distribution for each subject
    plt.figure()
    sns.distplot(self.results[0, ...], hist=False, rug=True, label='Small',
                 rug_kws={'height': 0.025})
    sns.distplot(self.results[1, ...], hist=False, rug=True, label='Large',
                 rug_kws={'height': 0.025})
    plt.xlabel('Point-to-plane error [mm]')
    plt.ylabel('Density')
    plt.title('Error distribution')
    plt.savefig(_join(output,
                      basename(self.pargs.morphnet_model_dir),
                      'facewarehouse_error_distribution.pdf'))
    plt.show(block=False)
    plt.close()

  def _transfer_topology(self, shape, tri):
    """
    Convert mesh
    :param shape: Vertices to convert
    :return:  Converted vertices
    """
    shape_new = np.zeros([len(self.vertex_map), 3], dtype=np.float32)
    for k in range(len(shape_new)):
      mapping_index = self.vertex_map[k, 0].astype(np.int32)
      mapping = np.reshape(self.vertex_map[k, 1:], [1, 3])
      tri_vertex = shape[tri[mapping_index, :], :]
      shape_new[k, :] = np.matmul(mapping, tri_vertex)
    return shape_new

  def run_evaluation(self, output):
    """
    Compute surface evaluation metrics. Apply first rigid ICP with isotropic
    scaling
    :param output:    Location where to dump the data
    """
    return self.results

