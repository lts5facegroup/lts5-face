#coding=utf-8
""" Appearance benchmark on LFW dataset """
from os.path import exists as _exists
from os.path import join as _join
from os import makedirs as _mkdir
from os.path import basename
from collections import defaultdict
from argparse import ArgumentParser
import numpy as np
from scipy.stats import wasserstein_distance
import tensorflow as tf
from tensorflow.keras.layers import Input, Lambda
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.integrate import cumtrapz
from lts5.utils import init_logger, search_folder
from lts5.tensorflow_op import initialize_device_memory
from lts5.tensorflow_op.network import MorphNet, MorphNetParameter
from lts5.tensorflow_op.network import VGG
from loader import LFWAppearanceLoader

__author__ = 'Christophe Ecabert'

logger = init_logger()
initialize_device_memory()


def _extract_features(pargs):
  """
  Do face reconstruction + extract features
  :param pargs: Parse arguments
  """

  # Check if destination exists
  fout = _join(pargs.output,
               'appearance',
               basename(pargs.morphnet_model_dir),
               'features')
  if not _exists(fout):
    _mkdir(fout)

  # VGG preprocessing
  def _vgg_preprocessing(x):
    """
    Convert image with range [0, 1] into VGG format
    :param x: Image to convert
    :return:  Converted image to VGG format
    """
    # Rescale + convert to BGR
    img = x * 255.0
    img = tf.reverse(img, axis=[-1])
    # Remove mean
    img -= tf.convert_to_tensor([103.939, 116.779, 123.68], dtype=img.dtype)
    # Done
    return img

  def _init_vgg_feat_extractor(pargs, cfg):
    """
    Initialize VGGFace feature extractor
    :param pargs: Parse arguments
    """
    vgg_ph = Input(shape=cfg.img_dims)
    img = Lambda(_vgg_preprocessing, name='vgg_preprocess')(vgg_ph)
    vgg_model = VGG.VGGFace(input_tensor=img, include_top=True)
    embbeding = vgg_model.get_layer('Predictor/relu7').output
    vgg_model.load_weights(pargs.vgg_face_model)
    return tf.keras.Model(inputs=vgg_ph, outputs=embbeding)

  def _init_model(pargs):
    """
    Initialize complete model
    :param pargs: Parsed arguments
    :return:  Model
    """
    # Create reconstruction model
    path = _join(pargs.morphnet_model_dir, 'config.json')
    cfg = MorphNetParameter.from_json(path)
    cfg.regularizer.w_alb = 0.0
    # Reconstruction
    img_ph = Input(shape=cfg.img_dims)
    rec_model = MorphNet(config=cfg)
    renderings = rec_model(img_ph, training=False)
    # Remove alpha channel (i.e. binary mask)
    img = renderings[0][..., :3]
    rec_model.load_weights(tf.train.latest_checkpoint(pargs.morphnet_model_dir))
    # Features
    feat_extractor = _init_vgg_feat_extractor(pargs, cfg=cfg)
    y_true_feat = feat_extractor(img_ph / 255.0)
    y_pred_feat = feat_extractor(img)
    # Complete models
    # Inputs: [Image, Background]:
    #   Image in [0, 255.0]
    #   Background in [0, 1]
    # Outputs: [rec, y_true, y_pred]
    #   rec:  Reconstructed image in [0, 1]
    #   y_true: Face embedding of ground truth
    #   y_pred: Face embedding of rendered image (i.e. reconstruction)
    model = tf.keras.Model(inputs=img_ph,
                           outputs=[img, y_true_feat, y_pred_feat])
    return model, cfg

  # Full model
  model, cfg = _init_model(pargs)

  # Identity map
  id_map = defaultdict(list)
  # Create dataset loader
  loader = LFWAppearanceLoader.from_file(path=pargs.lfw_record)
  for cnt, (img, label) in enumerate(loader):
    # Extract features
    rec, y_true, y_pred = model(img, training=False)
    # Get data on cpu
    id_name = label.numpy()[0].decode()
    p = id_name.rfind('_')
    id_name = id_name[:p]
    y_true = y_true.numpy()
    y_pred = y_pred.numpy()
    # Save features
    np.save(_join(fout, 'embedding_{:06d}.npy'.format(cnt)), y_pred)
    np.save(_join(fout, 'gt_embedding_{:06d}.npy'.format(cnt)), y_true)
    # Save id correspondence
    id_map[id_name].append(cnt)

    if cnt % 1000 == 0:
      logger.info('Process image for %s', id_name)
  # Save map
  np.save(_join(pargs.output,
                'appearance',
                basename(pargs.morphnet_model_dir),
                'id_map.npy'),
          id_map)


def _cosine_similarity(x, y):
  """
  Compute similarity between to vector
  :param x: First vector
  :param y: Second vector
  :return:  Similarity
  """
  x = np.squeeze(x)
  y = np.squeeze(y)
  return np.dot(x, y) / (np.linalg.norm(x) * np.linalg.norm(y))


def _add_median_value(hist:list, axes, type='median'):
  """
  Compute median value and report it on set of `axes`
  See: https://stackoverflow.com/a/28958609
  :param hist:  List of hist
  :param axes:  Axes object from matplotlib
  """

  # Get mean value:
  value = []
  for h in hist:
    for line in h.get_lines():
      x, y = line.get_data()
      # care with the order, it is first y
      # initial fills a 0 so the result has same length than x
      if type == 'median':
        cdf = cumtrapz(y, x, initial=0)
        nearest_05 = np.abs(cdf - 0.5).argmin()
        x_median = x[nearest_05]
        y_median = y[nearest_05]
        value.append(x_median)
      else:
        x_mean = x.mean()
        value.append(x_mean)
  # Add it to the axes
  axes.set_xticks(value)
  axes.set_xticklabels(['{:0.2f}'.format(x) for x in value],
                       rotation=45,
                       rotation_mode='anchor')


def _compute_metrics(pargs):
  """
  Compute appearance metrics
  :param pargs: Parsed arguments
  """
  # Reload id map
  id_map = np.load(_join(pargs.output,
                         'appearance',
                         basename(pargs.morphnet_model_dir),
                         'id_map.npy'),
                   allow_pickle=True).item()

  # Compute similarity upper bound for LFW
  # ----------------------------------------------------------------------------
  ph_to_id_map = {}
  for key, value in id_map.items():
    for v in value:
      ph_to_id_map[v] = key

  match_pairs = []
  missmatch_pairs = []
  with open(pargs.lfw_pairs, 'r') as f:
    n_fold, n_pairs = list(map(int, f.readline().strip().split('\t')))
    for k in range(n_fold):
      # Matched pairs
      for p in range(n_pairs):
        # parts: name n1 n2
        parts = f.readline().strip().split('\t')
        name = parts[0].lower()
        n1 = int(parts[1]) - 1
        n2 = int(parts[2]) - 1
        candidates = id_map[name]
        match_pairs.append([candidates[n1], candidates[n2]])
      # Missmatched pairs
      for p in range(n_pairs):
        # parts: name1   n1   name2   n2
        parts = f.readline().strip().split('\t')
        name1 = parts[0].lower()
        name2 = parts[2].lower()
        n1 = int(parts[1]) - 1
        n2 = int(parts[3]) - 1
        candidates1 = id_map[name1]
        candidates2 = id_map[name2]
        missmatch_pairs.append([candidates1[n1], candidates2[n2]])

  same_id_similarity = []
  diff_id_similarity = []
  n_id = len(ph_to_id_map)
  # Load every features
  gt_features = []
  features = []
  base_folder = _join(pargs.output,
                      'appearance',
                      basename(pargs.morphnet_model_dir),
                      'features')
  for i in range(n_id):
    path = _join(base_folder, 'gt_embedding_{:06d}.npy'.format(i))
    feat = np.load(path, allow_pickle=True)
    gt_features.append(feat)
    path = _join(base_folder, 'embedding_{:06d}.npy'.format(i))
    feat = np.load(path, allow_pickle=True)
    features.append(feat)
  gt_features = np.vstack(gt_features)
  features = np.vstack(features)


  # Loop over every pairs
  for n1, n2 in match_pairs:
    feat_i = gt_features[n1, :]
    feat_j = gt_features[n2, :]
    pair_similarity = _cosine_similarity(feat_i, feat_j)
    same_id_similarity.append(pair_similarity)
  for n1, n2 in missmatch_pairs:
    feat_i = gt_features[n1, :]
    feat_j = gt_features[n2, :]
    pair_similarity = _cosine_similarity(feat_i, feat_j)
    diff_id_similarity.append(pair_similarity)
  # Compute rendering-to-photo similarity
  # ----------------------------------------------------------------------------
  ren_to_ph_similarity = []
  ren_missmatch_similarity = []
  for k in range(features.shape[0]):
    # Load features
    ren_feat = features[k, :]
    gt_feat = gt_features[k, :]
    s = _cosine_similarity(ren_feat, gt_feat)
    ren_to_ph_similarity.append(s)
  for n1, n2 in missmatch_pairs:
    feat_i = features[n1, :]
    feat_j = features[n2, :]
    pair_similarity = _cosine_similarity(feat_i, feat_j)
    ren_missmatch_similarity.append(pair_similarity)
  # Measure distance between distribution
  # ----------------------------------------------------------------------------
  # Earth mover distance
  hist_same_id, _ = np.histogram(same_id_similarity,
                                 bins=100,
                                 range=(0.0, 1.0),
                                 density=True)
  hist_diff_id, _ = np.histogram(diff_id_similarity,
                                 bins=100,
                                 range=(0.0, 1.0),
                                 density=True)
  hist_ren_to_photo, _ = np.histogram(ren_to_ph_similarity,
                                      bins=100,
                                      range=(0.0, 1.0),
                                      density=True)
  hist_ren_missmatch_similarity, _ = np.histogram(ren_missmatch_similarity,
                                                  bins=100,
                                                  range=(0.0, 1.0),
                                                  density=True)
  emd_same = wasserstein_distance(hist_same_id, ren_to_ph_similarity)
  emd_diff = wasserstein_distance(hist_diff_id, ren_missmatch_similarity)
  emd_ren_miss = wasserstein_distance(ren_to_ph_similarity,
                                      ren_missmatch_similarity)
  # Save results
  msg = ['Earth Mover Distance (EMD): {:.3f} [SAME - Photo / Rendering]'
         ''.format(emd_same)]
  msg += ['Earth Mover Distance (EMD): {:.3f} [DIFF - Photo / Rendering]'
          ''.format(emd_diff)]
  msg += ['Earth Mover Distance (EMD): {:.3f} [Rendering / Rendering]'
          ''.format(emd_ren_miss)]
  fname = _join(pargs.output,
                'appearance',
                basename(pargs.morphnet_model_dir),
                'distance.txt')
  with open(fname, 'wt') as f:
    for m in msg:
      logger.info(m)
      f.write('{}\n'.format(m))

  # Plots
  # ----------------------------------------------------------------------------
  fig, axes = plt.subplots(2, 1, figsize=(10, 4), sharex=True, sharey=True)
  h1 = sns.distplot(same_id_similarity,
                    bins=100,
                    hist_kws={'range': (0.0, 1.0)},
                    kde_kws={'label': 'Same'},
                    ax=axes[0])
  sns.distplot(diff_id_similarity,
               bins=100,
               hist_kws={'range': (0.0, 1.0)},
               kde_kws={'label': 'Different'},
               ax=axes[0])
  axes[0].set_title('VGG-Face similarity on LFW')
  axes[0].set_xticks([])
  axes[0].set_yticks([])
  h2 = sns.distplot(ren_to_ph_similarity,
                    bins=100,
                    hist_kws={'range': (0.0, 1.0)},
                    kde_kws={'label': 'Same'},
                    ax=axes[1])
  sns.distplot(ren_missmatch_similarity,
               bins=100,
               hist_kws={'range': (0.0, 1.0)},
               kde_kws={'label': 'Different'},
               ax=axes[1])
  axes[1].set_title('Rendering-to-photo similarity on LFW')
  axes[1].set_xticks([])
  axes[1].set_yticks([])
  _add_median_value([h1, h2], axes=axes[1])
  # _add_median_value([h2], axes=axes[1])
  fig.savefig(_join(pargs.output,
                    'appearance',
                    basename(pargs.morphnet_model_dir),
                    'separated.pdf'))
  plt.show(block=False)

  fig, axes = plt.subplots(1, 1, figsize=(10, 4))
  h1 = sns.distplot(same_id_similarity,
                    bins=100,
                    hist_kws={'range': (0.0, 1.0)},
                    kde_kws={'label': 'Same'},
                    ax=axes)
  sns.distplot(diff_id_similarity,
               bins=100,
               hist_kws={'range': (0.0, 1.0)},
               kde_kws={'label': 'Different'},
               ax=axes)
  sns.distplot(ren_to_ph_similarity,
               bins=100,
               hist_kws={'range': (0.0, 1.0)},
               kde_kws={'label': 'Rendering'},
               ax=axes)
  sns.distplot(ren_missmatch_similarity,
               bins=100,
               hist_kws={'range': (0.0, 1.0)},
               kde_kws={'label': 'Rendering diff'},
               ax=axes)
  axes.set_title('VGG-Face similarity on LFW')
  axes.set_xticks([])
  axes.set_yticks([])
  # Add median value
  _add_median_value(hist=[h1], axes=axes)
  fig.savefig(_join(pargs.output,
                    'appearance',
                    basename(pargs.morphnet_model_dir),
                    'fused.pdf'))
  plt.show(block=False)


if __name__ == '__main__':
  # Command line
  p = ArgumentParser('Appearance benchmark on LFW dataset')

  # Record file
  p.add_argument('--lfw_record',
                 type=str,
                 required=True,
                 help='Path to the tfrecords with LFW image')
  p.add_argument('--lfw_pairs',
                 type=str,
                 required=True,
                 help='Path to the LFW pairs.txt file storing benchmark'
                      ' configuration')
  # Network configuration
  p.add_argument('--morphnet_model_dir',
                 type=str,
                 required=True,
                 help='Model location')
  # VGGFace model
  p.add_argument('--vgg_face_model',
                 type=str,
                 required=True,
                 help='Location where vgg face model is stored')
  # Output
  p.add_argument('--output',
                 type=str,
                 required=True,
                 help='Location where to place the results')

  args = p.parse_args()
  # Reconstruction
  _extract_features(pargs=args)
  # Metrics
  _compute_metrics(pargs=args)

