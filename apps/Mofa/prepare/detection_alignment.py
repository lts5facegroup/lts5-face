# coding=utf-8
"""
Data preparation script for MorphNet training.

Do:
  Face detection + Face alignment
"""

__author__ = 'Christophe Ecabert'

import argparse
from os import makedirs
from os.path import exists, join, splitext, basename

import cv2 as cv
import numpy as np

# Setup logger to also dump log in text file
from sys import stdout
from lts5.utils import TeeStream, Logger
stream = TeeStream()    # Link to default sys.cout + file
stream.link_stream(stdout)
script_name = splitext(basename(__file__))[0]
stream.link_stream(open('{}.log'.format(script_name), 'wt'))
# Create logger with TeeStream as ouput
logger = Logger(stream)

from lts5.utils import LFW, FaceWarehouse, CelebA, IBugVW, IBug, IBugW_LP
from lts5.utils.tools import str2bool

from utils import SFDDetector, BBox
from utils import FAN2DWrapper


def create_dataset(pargs):
  """
  Create dataset object based its type
  :param pargs:   Parsed arguments
  :return:    List of Dataset object
  """
  datasets = []
  # Add dataset
  if pargs.lfw_path is not None:
    datasets.append(LFW(folder=pargs.lfw_path))
  if pargs.fw_path is not None:
    datasets.append(FaceWarehouse(folder=pargs.fw_path))
  if pargs.celeba_path is not None:
    datasets.append(CelebA(folder=pargs.celeba_path))
  if pargs.ibugvw_path is not None:
    datasets.append(IBugVW(folder=pargs.ibugvw_path))
  if pargs.ibug_path is not None:
    datasets.append(IBug(folder=pargs.ibug_path))
  if pargs.ibugw_lp_path is not None:
    datasets.append(IBugW_LP(folder=pargs.ibugw_lp_path, lms_type='3d'))
  return datasets


def select_bbox_for_landmarks(bboxes, landmarks):
  """
  Select a box encapsulating the landmarks
  :param bboxes:      List of bbox
  :param landmarks:   Landmarks to match
  :return:  A bbox or None if not match
  """
  lms = landmarks.reshape(-1, 2)
  cog = lms.mean(axis=0)
  x, y = cog
  candidates = []
  distances = []
  if bboxes is not None:
    for bbox in bboxes:
      if bbox.inside(x, y):
        candidates.append(bbox)
        d = np.linalg.norm((np.asarray(bbox.center()) - cog))
        distances.append(d)
    if candidates:
      idx = np.argmin(distances)
      return candidates[idx]
  return None


def select_bbox_with_highest_score(bboxes):
  """
  Select box with hight score
  :param bboxes:  List of detection
  :return:  Selected bbox or None
  """
  if bboxes is not None:
    return max(bboxes, key=lambda x: x.score)
  return bboxes


def process(pargs):
  """
  Run processing
  :param pargs: Parsed arguments
  """

  def _draw_landmarks(image, landmarks):
    canvas = np.copy(image[..., ::-1])
    for pt in landmarks:
      canvas = cv.circle(canvas,
                         center=(int(pt[0]), int(pt[1])),
                         color=(0, 255, 0),
                         radius=1,
                         thickness=-1,
                         lineType=cv.LINE_AA)
    return canvas

  def _convert_landmarks(landmark, bbox, resolution):
    """
    Convert landmarks to a new referential
    :param landmark:    Landmarks to convert
    :param bbox:        Bounding box enclosing the landmarks
    :param resolution:  Final image size
    :return:  Converted landmarks
    """
    lms = landmark.reshape(-1, 2)
    conv_lms = np.zeros(lms.shape, lms.dtype)
    for k, (x, y) in enumerate(lms):
      # Shift
      xn = x - float(bbox.xmin)
      yn = y - float(bbox.ymin)
      # Scale
      xn = (xn / float(bbox.width())) * resolution
      yn = (yn / float(bbox.height())) * resolution
      # Set
      conv_lms[k, :] = (xn, yn)
    return conv_lms

  def _dump_landmarks(path, landmarks):
    with open(path, 'wb') as f_lms:
      f_lms.write(landmarks.astype(np.float32).tobytes())

  def _is_grayscale(image, n_sample=100):
    """
    Check if a given image is grascale (i.e. R == G == B). Instead of checking
    the whole image, only sample a few numby of pixels
    :param image:     Image to check
    :param n_sample:  Number of pixels to sample
    :return:  `True` if grayscale, otherwise `False`
    """
    if len(image.shape) == 3:
      wh = np.prod(image.shape[:2])
      idx = np.random.randint(0, wh, size=n_sample, dtype=np.int32)
      ry, rx = np.unravel_index(idx, image.shape[:2])
      pixels = image[ry, rx, :]
      return np.all(pixels[:, 0] == pixels[:, 1]) and \
             np.all(pixels[:, 0] == pixels[:, 2])
    return True

  # Load models
  model_sfd = SFDDetector(model=pargs.sfd)
  tracker = FAN2DWrapper(model_path=pargs.fan)

  # Define output
  root_f = pargs.output
  image_f = join(root_f, 'images')
  landmarks_f = join(root_f, 'landmarks')
  if not exists(root_f):
    makedirs(root_f)
  if not exists(image_f):
    makedirs(image_f)
  if not exists(landmarks_f):
    makedirs(landmarks_f)

  # Create output files
  with open(join(root_f, 'samples.txt'), 'wt') as f:
    # Dump header
    hdr = 'FaceID,Image,Label,Landmark'
    f.write('{}\n'.format(hdr))
    f.write('{0},{0}\n'.format(pargs.img_size))
    f.flush()
    # Create dataset
    datasets = create_dataset(pargs=pargs)
    # Goes through all
    cnt = 0
    image_cnt = 0
    n_identity = 0
    ibugw_lp_map = {}
    sample_counter = {}
    for dset in datasets:
      # iterate over all images in the dataset
      part = 'train'
      dset_ids = {}
      sample_counter[dset] = sample_counter.get(dset, 0)  # init sample counter
      for k, (img, label, im_name) in enumerate(zip(dset.samples(part),
                                                    dset.annotations(part),
                                                    dset.names(part))):
        # Get data
        _, img = img
        label_id, label_value = label

        if isinstance(dset, (CelebA, FaceWarehouse)):
          face_id = label_value
        elif isinstance(dset, IBugVW):
          face_id = int(label_id // 1e6)
        elif isinstance(dset, IBugW_LP):
          im_basename, ext = splitext(im_name)
          # im_basename formatted as XXX_k, need to extract XXX
          im_basename = '_'.join(im_basename.split('_')[:-1])
          face_id = ibugw_lp_map.get(im_basename, None)
          if face_id is None:
            n_item = len(ibugw_lp_map)
            ibugw_lp_map[im_basename] = n_item
            face_id = n_item
        else:
          raise ValueError('Unknown dataset type')

        # Get global face ID
        if not face_id in dset_ids.keys():
          dset_ids[face_id] = n_identity
          face_id = n_identity
          n_identity += 1
        else:
          face_id = dset_ids[face_id]

        # If video dataset, skip frames to avoid sample being close
        if isinstance(dset, IBugVW):
          if k % pargs.video_sample_multiple != 0:
            continue    # Skip index

        # Image loaded
        logger.info('Process image: %s', im_name)
        image_cnt += 1

        # Is it grayscale image ?
        if _is_grayscale(img, n_sample=1024):
          continue  # Skip grayscale image

        # Run face detection + select bbox
        bboxes = model_sfd(image=img)   # Convert to BGR internally

        # Landmarks annotation available ?
        if pargs.use_landmarks_label and dset.infos.name() in ('ibugw_lp',):
          lms = label_value
          sel_box = select_bbox_for_landmarks(bboxes, lms)
        else:
          # - Run SFD
          # - Match output with Viola-jones
          # - Do alignment
          sel_box = select_bbox_with_highest_score(bboxes=bboxes)
          if sel_box is not None:
            r = np.asarray([sel_box.xmin, sel_box.ymin,
                            sel_box.xmax, sel_box.ymax])
            lms = tracker.process(image=img, region=r)

        if sel_box is not None:
          # We have a match, add margin
          sel_box.add_margin(margin=pargs.margin)
          sel_box.clip_to_border(width=img.shape[1], height=img.shape[0])
          r = sel_box.square(clip_h=img.shape[0], clip_w=img.shape[1])
          # Crop
          x_1, y_1, x_2, y_2 = list(map(int, (r.xmin, r.ymin, r.xmax, r.ymax)))
          if (x_2 - x_1) != (y_2 - y_1):
            d = min((x_2 - x_1), (y_2 - y_1))
            x_2 = x_1 + d
            y_2 = y_1 + d
          patch = img[y_1:y_2, x_1:x_2, :]
          if patch.shape[0] != patch.shape[1]:
            ar = patch.shape[0] / patch.shape[1]
            logger.error('Aspect ratio not correct: {:2f}'.format(ar))
          # Down sample patch
          patch_sized = cv.resize(patch[..., ::-1],     # bgr for ocv
                                  dsize=(pargs.img_size, pargs.img_size),
                                  interpolation=cv.INTER_LINEAR)
          # Rescale, remove samples with upscaling factor larger than 2x.
          ratio = pargs.img_size / patch.shape[0]
          if ratio > 1.75:
            msg = 'Patch will be upscaled by factor {:.2f}'.format(ratio)
            logger.warning(msg)
            continue

          # Convert landmarks to new referential
          lms = _convert_landmarks(landmark=lms,
                                   bbox=BBox(int(r.xmin),
                                             int(r.ymin),
                                             int(r.xmin) + patch.shape[1],
                                             int(r.ymin) + patch.shape[0]),
                                   resolution=pargs.img_size)

          # canvas = _draw_landmarks(image=patch_sized, landmarks=lms)
          # plt.imshow(canvas / 255.0)
          # plt.show()

          # Export Image + landmarks
          f_name = join(image_f, '{:08}.jpg'.format(cnt))
          cv.imwrite(f_name, patch_sized)
          lms_name = join(landmarks_f, '{:08}.lms'.format(cnt))
          _dump_landmarks(lms_name, lms)
          # Log to file
          im_path = f_name[len(root_f) + 1:]
          lms_path = lms_name[len(root_f) + 1:]
          msg = '{},{},{},{}\n'.format(face_id, im_path, im_path, lms_path)
          cnt += 1
          sample_counter[dset] += 1
          # Log
          f.write(msg)
          f.flush()
        else:
          logger.warning('No face detected')

    # Done, summary
    logger.info('Founded %s images', image_cnt)
    logger.info('Selected %s bbox', cnt)
    # Dump sample counter
    with open(join(root_f, 'datasets_distribution.txt'), 'wt') as f:
      for key, value in sample_counter.items():
        f.write('{}, {}\n'.format(key.infos.name(), value))


if __name__ == '__main__':
  # Parser
  desc = 'Data preparation for 3D face reconstruction algorithm'
  parser = argparse.ArgumentParser(description=desc)

  # Models
  parser.add_argument('--sfd',
                      type=str,
                      required=True,
                      dest='sfd',
                      help='S3FD model (*.npy)')
  parser.add_argument('--fan',
                      type=str,
                      required=True,
                      dest='fan',
                      help='FAN2D face alignment model (*.pb)')
  # Image datasets
  parser.add_argument('--lfw_path',
                      type=str,
                      help='Folder where LFW dataset is stored')
  parser.add_argument('--fw_path',
                      type=str,
                      help='Folder where FaceWarehouse dataset is stored')
  parser.add_argument('--celeba_path',
                      type=str,
                      help='Folder where CelebA dataset is stored')
  parser.add_argument('--ibugvw_path',
                      type=str,
                      help='Folder where 300VW dataset is stored')
  parser.add_argument('--ibug_path',
                      type=str,
                      help='Folder where 300W dataset is stored')
  parser.add_argument('--ibugw_lp_path',
                      type=str,
                      help='Location where 3002-LP dataset is stored')
  # Use landmarks annotation when available
  parser.add_argument('--use_landmarks_label',
                      type=str2bool,
                      default=True,
                      help='If True, use landmarks annoation when available')
  # Output location
  parser.add_argument('--output',
                      type=str,
                      required=True,
                      dest='output',
                      help='Location where the generated data will be placed')
  # Image size
  parser.add_argument('--size',
                      type=int,
                      required=False,
                      default=224,
                      dest='img_size',
                      help='Dimension of the cropped face')
  # Face bbox margin
  parser.add_argument('--margin',
                      type=float,
                      required=False,
                      default=0.2,
                      dest='margin',
                      help='Extra margin for the face bbox (percentage)')
  # Drop sample from video dataset
  parser.add_argument('--video_sample_multiple',
                      type=int,
                      default=90,
                      help='Pick one frame every nth frames, default 5sec')
  # parse
  args = parser.parse_args()
  process(pargs=args)
