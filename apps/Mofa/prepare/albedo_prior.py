# coding=utf-8
"""
Learn a smooth albedo prior in the image space (i.e. gs(R)).

See:
  - https://arxiv.org/pdf/2010.03592.pdf
"""
from os.path import join, exists
from os import makedirs as mkdir
from os import listdir
from argparse import ArgumentParser
import numpy as np
from imageio import imsave, imread
from scipy.optimize import minimize
import pickle
import tensorflow as tf
import matplotlib.pyplot as plt
from lts5.utils.tools import search_folder, init_logger
from lts5.tensorflow_op.layers.face import ParametricFaceModel
from lts5.tensorflow_op.layers.rendering import DeferredRendererV2
from lts5.tensorflow_op.layers.image import PatchPixelDifference
from lts5.utils import HistogramFloat as Histogram
from lts5.utils import weighted_histogram_sampling

__author__ = 'Christophe Ecabert'
logger = init_logger()


@tf.function
def _shrink_mask(mask, f):
  """
  Remove half of the patch size from the binary mask to avoid taking difference
  with background pixel later on
  :param mask:  Mask to shrinks, [H, W]
  :param f: filter
  :return:  Shrinked mask, [H, W]
  """
  mask = 1.0 - tf.expand_dims(tf.expand_dims(mask, 0), -1)
  mk = tf.nn.depthwise_conv2d(mask, f, strides=[1, 1, 1, 1], padding='SAME')
  mk = tf.math.divide_no_nan(mk, tf.reduce_max(mk, axis=-1, keepdims=True))
  return tf.squeeze(1.0 - mk)


def _save_image(image, filename):
  """
  Save image + mask into file
  :param image:     Tensor to save
  :param filename:  Image filename
  """
  if isinstance(image, tf.Tensor):
    image = image.numpy()
  img = image[0, ..., :3]
  mask = image[0, ..., 3]
  img = (img * 255.0).astype(np.uint8)
  imsave(filename, img)
  # mask
  fname = filename.replace('png', 'npy')
  np.save(fname, mask)


def generate_image(model_path,
                   folder,
                   n_tex=80,
                   im_size=224,
                   near=1e0,
                   far=1e3,
                   focal=1015.0):
  """
  Generate random image from 3D Morphable model.
  :param model_path:  Path to morphable model
  :param folder:  Location where to place the generated data
  :param n_tex: Number of texture component
  :param im_size: Image dimensions
  :param near:  Near plane
  :param far:   Far plane
  :param focal: Focal length
  """
  im_folder = join(folder, 'images')
  if not exists(im_folder):
    mkdir(im_folder)

  if not listdir(im_folder):
    logger.info('Generate synthetic images...')

    # Load model + renderer
    model = ParametricFaceModel(model_path, None)
    renderer = DeferredRendererV2(width=im_size,
                                  height=im_size,
                                  near=near,
                                  far=far)
    # Default params
    w_shp = tf.zeros(shape=(1, 1, 80 + 64), dtype=tf.float32)
    w_illu = np.zeros(shape=(1, 1, 27), dtype=np.float32)
    w_illu[:, :, ::9] = 1.0
    rvec = tf.zeros(shape=(1, 1, 3), dtype=tf.float32)
    tvec = np.zeros(shape=(1, 1, 3), dtype=np.float32)
    tvec[:, :, 1] = -0.1
    tvec[:, :, 2] = -0.0095 * focal
    bg = tf.zeros(shape=(1, im_size, im_size, 3), dtype=tf.float32)
    w_illu = tf.convert_to_tensor(w_illu)
    rvec = tf.convert_to_tensor(rvec)
    tvec = tf.convert_to_tensor(tvec)
    focal = tf.ones(shape=(1, 1), dtype=tf.float32) * focal

    @tf.function
    def _generate(wtex):
      attr, _ = model([w_shp, wtex, w_illu, rvec, tvec], False)
      shp = attr[0]
      albedo = attr[1]
      tri = attr[5]
      attr = [(albedo, lambda x: tf.clip_by_value(x, 0.0, 1.0), bg, 'lin')]
      renderings = renderer([shp, focal, tri], attributes=attr)
      return renderings[0]

    cnt = 0
    for k in range(n_tex):
      w_tex = np.zeros(shape=(1, 1, n_tex), dtype=np.float32)
      # +3 sigma
      w_tex[..., k] = 3.0
      img = _generate(tf.convert_to_tensor(w_tex))
      _save_image(img, join(im_folder, 'image_{:04d}.png'.format(cnt)))
      cnt += 1
      # -3 sigma
      w_tex[..., k] = -3.0
      img = _generate(tf.convert_to_tensor(w_tex))
      _save_image(img, join(im_folder, 'image_{:04d}.png'.format(cnt)))
      # Reset component
      cnt += 1

    for k in range(cnt):
      w_tex = np.random.normal(size=(1, 1, n_tex)).astype(np.float32)
      img = _generate(tf.convert_to_tensor(w_tex))
      _save_image(img, join(im_folder, 'image_{:04d}.png'.format(cnt)))
      cnt += 1
  else:
    logger.warning('Skip image generation, folder not empty!')


def patch_pixel_difference(folder, size, sampling=None):
  """
  Extract pixel difference for patches
  :param folder:   Location where data are placed
  :param size:  Patch size
  :param sampling: Fraction or Number of the observation to samples
  :return:  List of pixel difference
  """

  # Use cache data
  data_fname = join(folder, 'pixel_difference.npy')
  if not exists(data_fname):
    logger.info('Compute pixel difference')
    im_folder = join(folder, 'images')
    # Scan
    im_list = search_folder(im_folder, ext=['.png'], relative=False)

    # Layer
    patch_diff = PatchPixelDifference(size=size)
    hsize = int(size // 2)
    f_shrink = np.ones(shape=(size, size, 1, 1), dtype=np.float32)
    f_shrink[hsize, hsize, ...] = 0.0
    f_shrink = tf.convert_to_tensor(f_shrink)

    @tf.function
    def _extract(image):
      # Log scale
      im = tf.maximum(image / 255.0, 1e-3)
      l_im = tf.expand_dims(tf.math.log(im), 0)
      # l_im = tf.expand_dims(image / 255.0, 0)
      return tf.squeeze(patch_diff(l_im), 0)

    data = []
    for im_path in im_list:
      # Load data
      mk_path = im_path.replace('.png', '.npy')
      img = np.asarray(imread(im_path)).astype(np.float32)
      mask = np.load(mk_path)
      # Shrink mask by half of patch size
      mask_s = _shrink_mask(tf.convert_to_tensor(mask), f_shrink).numpy()
      # Extract pixel difference
      px_diff = _extract(tf.convert_to_tensor(img)).numpy()  # [H, W, #Pair, C]
      px_sel = px_diff[mask_s > 0.5, ...].reshape(-1, 3)   # [N, 3]
      data.append(px_sel)

    # Samples
    if sampling is not None:
      # Build histogram
      logger.info('Start data sampling, will take a while...')
      for k, sample in enumerate(data):
        if k % 100 == 0:
          logger.info('Process image: {}'.format(k))

        hist = Histogram()
        ranges = np.column_stack((sample.min(axis=0), sample.max(axis=0)))
        hist.build(sample, ranges, 50)
        d2 = np.asarray(weighted_histogram_sampling(hist, 10000))
        data[k] = sample[d2, :]
    # Combine everything
    data = np.concatenate(data, axis=0)
    np.save(data_fname, data)
  else:
    logger.warning('Reload cached data!')
    data = np.load(data_fname)
  return data


def _psd2vec(m):
  """
  Convert PSD matrix into lower triangular matrix and serialize it
  :param m: PSD Matrix
  :return:  Serialize lower triangular matrix
  """
  l_chol = np.linalg.cholesky(m)
  log_l_diag = np.log(np.diag(l_chol))
  l_tril_idx = np.tril_indices(m.shape[0], -1)
  l_tril = l_chol[l_tril_idx]
  return np.concatenate([log_l_diag, l_tril])


def _vec2psd(v):
  """
  Convert serialized vector back to PSD matrix
  :param v: Vector
  :return:  PSD matrix
  """
  d = int((0.25 + (2 * v.size)) ** 0.5 - 0.5)
  l_diag = np.exp(v[:d])
  l_tril = v[d:]
  l_tri = np.diagflat(l_diag)
  l_tril_idx = np.tril_indices(d, -1)
  l_tri[l_tril_idx] = l_tril
  return l_tri @ l_tri.T


def _gsm_log_lik(data, mean, covariance, scale, mix):
  """
  Compute data log-likelihood
  :param data:  Sample, [N, D] where N is number of samples and D is the feature
   dimensions
  :param mean:  Mean value, [D,]
  :param covariance:  Covariance matrix [D, D]
  :param scale: Covariance scaling factor, [K,] one for each component
  :param mix: Mixing probability, [K,]
  :return:  Likelihood of each samples [N,]
  """
  n_sample, feat_dims = data.shape    # N, D

  # Remove mean
  data = data - mean.reshape(1, -1)
  # Compute maha distance
  icov = np.linalg.inv(covariance)
  maha_dist = 0.5 * ((data @ icov) * data).sum(axis=1)   # [N, ]

  # Likelihood
  gsm_sigma_r = np.expand_dims(np.linalg.cholesky(covariance), -1)  # [D, D, 1]
  gsm_scale = gsm_sigma_r * (scale ** 0.5)  # [D, D, K]

  # Constant in front of exp for each gaussian in the mixture
  log_mult = (np.log(mix) -
              ((feat_dims / 2.0) * np.log(2.0 * np.pi)) -
              np.sum(np.log(np.einsum('iij->ji', gsm_scale)), 1))  # [K, ]

  # Term inside exp
  log_data = -(np.expand_dims(maha_dist, -1) / np.expand_dims(scale, 0))

  # Likelihood
  likelihood = np.exp(log_mult + log_data).sum(axis=1)
  log_likelihood = np.log(likelihood)
  log_likelihood[log_likelihood < -1000000] = -1000000
  return log_likelihood


def _gsm_loss(x, data, gsm_scale, n_cov, with_mean):
  """
  Compute negative log-likelihood for GSM
  :param x: Current estimation of the parameters [gsm_mix, gsm_cov, (gsm_mean)]
  :param data:  Data [N, D]
  :param gsm_scale: Scaling factor of the covariance for the mixture, [K,]
  :param n_cov: Size of the covariance vector
  :param with_mean: bool, If `True` optimize mean as well
  :return:  Negative log-likelihood
  """
  n_sample, feat_dims = data.shape
  n_component = gsm_scale.size
  # Recover parameters
  gsm_log_mix = x[:n_component]
  gsm_cov = x[n_component:(n_component + n_cov)]
  gsm_mean = x[-feat_dims:] if with_mean else np.zeros((feat_dims,))

  # Convert mixing prob
  gsm_mix = np.exp(gsm_log_mix * 20.0)  # Magic scaling factor
  gsm_mix /= np.sum(gsm_mix)
  # Covariance
  gsm_cov = _vec2psd(gsm_cov)
  gsm_cov /= np.linalg.det(gsm_cov) ** (1.0 / gsm_cov.shape[0])

  # Compute negative log-likelihood
  neg_ll = -np.sum(_gsm_log_lik(data,
                                mean=gsm_mean,
                                covariance=gsm_cov,
                                scale=gsm_scale,
                                mix=gsm_mix))
  return neg_ll


class MultivariateGaussianScaleMixture:
  """ Multivariate Gaussian scale mixture (GSM)
  Mixture of K multivariate gaussians with shared mean/covariance matrix

  """

  def __init__(self,
               mix_prob,
               mean,
               covariance,
               scales):
    """
    Constructor
    :param mix_prob:  Mixing probability, one for each gaussian [K,]
    :param mean:  Gaussian mean [D,]
    :param scales:  Covariance scaling factor, one for each gaussian [K,]
    :param covariance:  Shared covariance matrix [D, D]
    """
    self._mix_prob = mix_prob
    self._mean = mean
    # Covariance
    self._inv_cov = np.linalg.inv(covariance)
    self._chol_cov = np.expand_dims(np.linalg.cholesky(covariance), -1)
    # Scalings
    self._scales = scales

  @staticmethod
  def fit(data, n_component, with_mean=False, n_iter=5000):
    """
      Fit Multivariate normal Gaussian Scale Mixture (GSM). The code is based on
      released code from "Shape, Illumination, and Reflectance from Shading",
      Jonathan T. Barron and Jitendra Malik, TPAMI 2015.
        http://www.cs.berkeley.edu/~barron/
        https://github.com/bhushan23/SIRFS
      :param data:  Array of shape [n_sample, n_feature]
      :param n_component: Number of component in the mixture
      :param with_mean: bool, If `True` estimate mean as well, otherwise assume
        zero mean.
      :param n_iter: Maximum number of iteration to do
      :return:  Model
      """
    n_sample, feat_dims = data.shape
    # Estimate mean
    gsm_mean = np.zeros(shape=(feat_dims,))
    if with_mean:
      gsm_mean = np.mean(data, axis=0)
    # Estimate data covariance
    data_cov = np.cov(data, rowvar=False)
    data_cov /= np.linalg.det(data_cov) ** (1.0 / data_cov.shape[0])
    gsm_cov = _psd2vec(data_cov)  # Convert PSD matrix to vector

    # Compute mahalanobis distance, with transformed covariance ..why I don't
    # know
    data_icov = np.linalg.inv(data_cov)
    maha_dist = 0.5 * ((data @ data_icov) * data).sum(axis=1)

    # Define scale of each gaussian based on mahalanobis distance, will not be
    # optimized later on!
    v_range = [max(0.00001, np.percentile(maha_dist, 0.01)),
               maha_dist.max()]
    log_range = [np.log2(v_range[0] / 8), np.log2(8 * v_range[1])]
    gsm_scale = 2.0 ** np.linspace(log_range[0], log_range[1], n_component)

    # Mixing probability -> will be optimized
    gsm_log_mix = np.zeros((n_component,))

    # Minimize negative log-likelihood of the mixture
    vars_opt = [gsm_log_mix, gsm_cov]
    if with_mean:
      vars_opt.append(gsm_mean)

    # Initial value for parameters to optimize
    x0 = np.concatenate(vars_opt)
    opt = {'maxiter': n_iter,  # Maximum number of iteration
           'disp': True,  # Show convergence message: debug
           # --- Solver specific ---
           'maxfun': 3 * n_iter,  # Maximum number of function evaluations.
           'maxcor': 500,  # The maximum number of variable metric
           # corrections used to define the limited memory
           # matrix
           }
    res = minimize(_gsm_loss,
                   x0,
                   args=(data, gsm_scale, gsm_cov.size, with_mean),
                   method='l-bfgs-b',
                   options=opt)
    if res.success:
      xp = res.x

      estm_log_mix = xp[:n_component]
      estm_cov = xp[n_component:(n_component + gsm_cov.size)]
      estm_mean = xp[-feat_dims:] if with_mean else np.zeros((feat_dims,))

      # Post-process data
      # Mixing prob
      opt_mix = np.exp(estm_log_mix * 20.0)
      opt_mix /= np.sum(opt_mix)
      # Mean
      opt_mean = estm_mean
      # Shared covariance matrix
      opt_sigma = _vec2psd(estm_cov)
      opt_sigma /= np.linalg.det(opt_sigma) ** (1.0 / opt_sigma.shape[0])
      # Variance scaling
      opt_scale = gsm_scale

      # Sanity check
      gsm = MultivariateGaussianScaleMixture(opt_mix,
                                             opt_mean,
                                             opt_sigma,
                                             opt_scale)
      neg_ll = -np.sum(gsm.log_likelihood(data))
      np.testing.assert_allclose(res.fun,
                                 neg_ll,
                                 err_msg='Final cost does not match the one '
                                         'estimated with GSM class')
      return gsm
    else:
      raise ValueError('Optimization did not converged!')

  def log_likelihood(self, data):
    """
    Compute log-likelihood for every sample in `data`
    :param data:  Samples, [N, D] `N` observation of dimension `D`
    :return:  Log-likelihood, [N]
    """
    n_sample, feat_dims = data.shape  # N, D
    # Center
    data = data - self._mean
    # Compute mahalanobis distance
    maha_dist = 0.5 * ((data @ self._inv_cov) * data).sum(axis=1)  # [N, ]

    # Likelihood
    gsm_scale = self._chol_cov * (self._scales ** 0.5)  # [D, D, K]
    # Constant in front of exp for each gaussian in the mixture
    # [K, ]
    log_mult = (np.log(self._mix_prob) -
                ((feat_dims / 2.0) * np.log(2.0 * np.pi)) -
                np.sum(np.log(np.einsum('iij->ji', gsm_scale)), 1))
    # Term inside exp
    log_data = -(np.expand_dims(maha_dist, -1) /
                 np.expand_dims(self._scales, 0))

    # Likelihood
    likelihood = np.exp(log_mult + log_data).sum(axis=1)
    log_likelihood = np.log(likelihood)
    log_likelihood[log_likelihood < -1000000] = -1000000
    return log_likelihood


def train_mixture(data, folder, n_component):
  """
  Train gaussian mixture model
  :param data:  Array, observations [N, 3]
  :param folder:  Location where to place the model
  :param n_component: Number of gaussian in the mixture
  """
  # Train GMM
  fname = join(folder, 'bfm_log_albedo_prior.pkl')
  if not exists(fname):
    # Train mixture
    gmm = MultivariateGaussianScaleMixture.fit(data,
                                               n_component,
                                               with_mean=False)
    # Save model
    pickle.dump(gmm, open(fname, 'wb'))
  else:
    gmm = pickle.load(open(fname, 'rb'))


def process(pargs):
  """
  Run experiments
  :param pargs:  Parsed command line arugments
  """
  if not exists(pargs.output):
    mkdir(pargs.output)

  # Generate images
  generate_image(model_path=pargs.model_path,
                 folder=pargs.output)
  # Pixel difference
  px_diff = patch_pixel_difference(folder=pargs.output,
                                   size=pargs.patch_size,
                                   sampling=None)
  # Train mixture
  train_mixture(px_diff,
                folder=pargs.output,
                n_component=pargs.n_components)


if __name__ == '__main__':
  p = ArgumentParser()

  p.add_argument('--model_path',
                 required=True,
                 type=str,
                 help='Path to 3D Morphable model')
  p.add_argument('--patch_size',
                 type=int,
                 default=3,
                 required=False,
                 help='Patch dimensions')
  p.add_argument('--n_components',
                 type=int,
                 required=False,
                 default=40,
                 help='Number of gaussian in the mixture model')
  p.add_argument('--output',
                 required=True,
                 type=str,
                 help='Location where to place the generated data')

  args = p.parse_args()
  process(pargs=args)
