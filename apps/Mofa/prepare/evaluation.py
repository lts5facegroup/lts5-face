# coding=utf-8
"""
Prepare tfrecords for dataset for evaluation. The following datasets are
handled:
  - MICC
  - FaceWarehouse
  - LFW
"""
from os import makedirs as _mkdir
import os.path as _path
import re
from argparse import ArgumentParser
from subprocess import Popen, PIPE
import cv2 as cv
import numpy as np
import tensorflow as tf
from imageio import imread, imwrite

# Setup logger to also dump log in text file
from sys import stdout
from lts5.utils import TeeStream, Logger
stream = TeeStream()    # Link to default sys.cout + file
stream.link_stream(stdout)
script_name = _path.splitext(_path.basename(__file__))[0]
stream.link_stream(open('{}.log'.format(script_name), 'wt'))
# Create logger with TeeStream as ouput
logger = Logger(stream)

from utils import SFDDetector
from lts5.utils import MICC, LFW
from lts5.utils.tools import search_folder
from lts5.tensorflow_op.utils.tfrecords import to_feature
from lts5.tensorflow_op.utils.device import initialize_device_memory

import matplotlib.pyplot as plt

initialize_device_memory()


class RunningAverage(object):

  def __init__(self):
    self._cma = 0.0   # Cumulative average
    self._step = 0

  def __call__(self, value):
    """
    Add new value into running average
    :param value: New value to add
    :return:  New average
    """
    avg = float(self._step) * self._cma + value
    self._step += 1
    self._cma = avg / float(self._step)
    return self._cma

  def clear(self):
    """ Reset running average """
    self._cma = 0.0
    self._step = 0
    
  @property
  def mean(self):
    return self._cma


def _dump_to_tfrecords(image, writer, label=None):
  """
  Dump an image into a given writer
  :param image:   Numpy array, image to dump
  :param writer:  TFRecordWriter instance
  """
  # Convert to jpg
  img_str = tf.io.encode_jpeg(image=tf.convert_to_tensor(image)).numpy()
  # Create example
  feat = {'image': to_feature(img_str)}
  if label is not None:
    feat['label'] = to_feature(label)
  example = tf.train.Example(features=tf.train.Features(feature=feat))
  # Dump it
  writer.write(example.SerializeToString())
  writer.flush()


def _setup_output(pargs, *args):
  # Setup destination folder
  path = _path.join(pargs.output, *args)
  if not _path.exists(path):
    _mkdir(path)
  return path


def _run_fdet(pargs, image_or_path, fdet):
  """
  Run face detection
  :param pargs:           Parsed arguments
  :param image_or_path:   Image (np array) or path to an image
  :param fdet: Face detector
  :return:  Image patch or None if failed
  """
  # Load image ?
  if isinstance(image_or_path, str):
    # Load image
    img = np.asarray(imread(image_or_path)).astype(np.float32)
  else:
    img = image_or_path.astype(np.float32)
  # Detect
  bboxes = fdet(img)
  if bboxes:
    # Select bbox
    # Make all the detection square
    bbox = []
    for bb in bboxes:
      # Consider only bbox with score large enough
      if bb.score > pargs.score_threshold:
        bb.add_margin(margin=pargs.margin)
        bb.clip_to_border(width=img.shape[1], height=img.shape[0])
        bbox.append(bb.square(clip_h=img.shape[0], clip_w=img.shape[1]))
    # Sort by:
    #  - bbox width, largest first
    bbox.sort(key=lambda x: x.width(), reverse=True)
    if len(bbox):
      # Pick first + Crop
      r = bbox[0]
      x_1, y_1, x_2, y_2 = list(map(int, (r.xmin, r.ymin,
                                          r.xmax, r.ymax)))
      if (x_2 - x_1) != (y_2 - y_1):
        d = min((x_2 - x_1), (y_2 - y_1))
        x_2 = x_1 + d
        y_2 = y_1 + d
      patch = img[y_1:y_2, x_1:x_2, :]
      # Down sample patch
      patch_sized = cv.resize(patch.astype(np.float32),
                              dsize=(pargs.img_size, pargs.img_size),
                              interpolation=cv.INTER_LINEAR)
      patch_sized = patch_sized.astype(patch.dtype)
      return patch_sized
    return None
  return bboxes


def _densify_mesh(pargs, input, output):
  """
  Run meshlab mesh densification
  :param pargs: Parsed arguments
  :param input: Input mesh
  :param output:  Output destination
  """
  # Create command
  # cmd: meshlabserver -i <Mesh> -o <OMesh> -s <ProjectFile>
  cmd = '{} -i {} -o {} -s {}'.format(pargs.meshlabserver,
                                      input,
                                      output,
                                      pargs.meshlab_file)
  cmd = cmd.split(' ')
  # Call meshlab
  p = Popen(cmd, stdout=PIPE, stderr=PIPE, universal_newlines=True)
  output, errors = p.communicate()
  if p.returncode != 0:
    logger.error('Error while running mesh densification: {}'.format(errors))


def _detect_face_micc(pargs, det, folder):
  """
  Run face detection for MICC dataset
  :param pargs: Parsed arguments
  :param det:   Face detector
  :param folder: Location where to dump data
  """
  # Create dataset
  dset = MICC(pargs.micc_path)
  partitions = dset.infos.partitions()
  # Loop over each partition
  for part in partitions:
    subject_id = 0
    prev_frame_idx = -2
    image_per_subject = 0
    match_id = re.compile(r"\D*([0-9]*)")
    for (_, img), name in zip(dset.samples(partition=part),
                              dset.names(partition=part)):
      # Define subject ID
      frame_idx = int(match_id.match(name).groups()[0])
      if frame_idx != prev_frame_idx + 1:
        # New subject
        subject_id += 1
        image_per_subject = 0
        subj_folder = _path.join(folder,
                                 part,
                                 'subject_{:02d}'.format(subject_id))
        logger.info('Folder: {}'.format(subj_folder))
        if not _path.exists(subj_folder):
          _mkdir(subj_folder)

      prev_frame_idx = frame_idx
      # Sample video frame, only a subset is selected
      if frame_idx == 0 or frame_idx % pargs.video_sampling_rate != 0:
        continue

      if subject_id == 47 and frame_idx < 100:
        # Drop 100 first frames for this subject since it can not detect face
        # reliably
        continue

      if frame_idx % 1000 == 0:
        logger.info('Process: %s', name)
      # Run face detection + select bbox
      bboxes = det(image=img)
      if bboxes is None:
        logger.warning('No face detected on frame {}'.format(frame_idx))
        continue

      # Select the closest bbox from previous detection
      bbox = None
      max_dist = 0.0
      for bb in bboxes:
        if bb.score > pargs.score_threshold:
          # Pick largest detection
          sz = max(bb.width(), bb.height())
          if sz > max_dist:
            bbox = bb
            max_dist = sz
      # Check:
      # 1. Is there a bbox
      # 2. Is the biggest one close to the previous detection ?
      if bbox is None:
        logger.warning('No bbox detected at frame {}'.format(frame_idx))
        continue

      if bbox.height() > 710.0:
        # Patch reach border of image, drop it
        continue

      # Add margin + Clip + Square + Crop
      bbox.add_margin(margin=pargs.margin)
      bbox.clip_to_border(width=img.shape[1], height=img.shape[0])
      r = bbox.square(clip_h=img.shape[0], clip_w=img.shape[1])
      x_1, y_1, x_2, y_2 = list(map(int, (r.xmin, r.ymin,
                                          r.xmax, r.ymax)))
      if (x_2 - x_1) != (y_2 - y_1):
        d = min((x_2 - x_1), (y_2 - y_1))
        x_2 = x_1 + d
        y_2 = y_1 + d
      patch = img[y_1:y_2, x_1:x_2, :]

      # Down sample patch
      patch_sized = cv.resize(patch.astype(np.float32),
                              dsize=(pargs.img_size, pargs.img_size),
                              interpolation=cv.INTER_LINEAR)
      patch_sized = patch_sized.astype(patch.dtype)
      # Export Image
      fname = _path.join(subj_folder, '{:08}.jpg'.format(frame_idx))
      cv.imwrite(fname, patch_sized[..., ::-1])
      image_per_subject += 1


def _create_micc_tf_records(folder, image_folder):
  """
  Create TF Records
  :param pargs:
  :return:
  """
  for parts in ['cooperative', 'indoor', 'outdoor']:
    # Image for partition
    im_folder = _path.join(image_folder, parts)
    # Records for partition
    rec_folder = _path.join(folder, parts)
    if not _path.exists(rec_folder):
      _mkdir(rec_folder)
    # Goes through all subjects (i.e. there is 53 subjects)
    for s_id in range(1, 54):
      # Location where images are stored for a given subject
      im_folder_subj = _path.join(im_folder, 'subject_{:02d}'.format(s_id))
      # Scan for image
      images = search_folder(im_folder_subj, ext=['.jpg'], relative=False)
      # Create TF Records
      w_name = _path.join(rec_folder, 'subject_{:02d}.tfrecords'.format(s_id))
      with tf.io.TFRecordWriter(w_name) as writer:
        for im_path in images:
          # Load + Dump
          img = imread(im_path)
          _dump_to_tfrecords(img, writer)


def _prepare_micc_dataset(pargs, det):
  """
  Prepare MICC dataset for geometry benchmarking
  :param pargs: Parsed arguments
  :param det:   Face detector
  """
  # Create output folder if needed
  img_folder = _setup_output(pargs, 'micc_shape', 'images')
  rec_folder = _setup_output(pargs, 'micc_shape', 'records')
  # Run face detection
  # _detect_face_micc(pargs=pargs, det=det, folder=img_folder)
  _create_micc_tf_records(folder=rec_folder, image_folder=img_folder)


def _prepare_facewarehouse_dataset(pargs, det):
  """
  Prepare FaceWarehouse for shape benchmarking
  :param pargs: Parsed arguments
  :param det:   Face detectector
  """
  # Create output folder if needed
  img_folder = _setup_output(pargs, 'fw_shape', 'images')
  mesh_folder = _setup_output(pargs, 'fw_shape', 'meshes')
  # Pose index matcher
  p_matcher = re.compile(r"pose_([0-9]*).")
  # Go through selected ids
  for subj in pargs.fw_subjects:
    logger.info('Process subject {:03d}'.format(subj))
    subj_folder = _path.join(pargs.fw_path,
                             'Tester_{:03d}'.format(subj),
                             'TrainingPose')
    images = search_folder(subj_folder, ext=['.png'], relative=False)
    meshes = search_folder(subj_folder, ext=['.obj'], relative=False)
    if len(images) != len(meshes):
      logger.error('Can not find all data for subject'
                   ' {}, {} != {}'.format(subj, len(images), len(meshes)))
    # Iterate over each pairs
    for im_path, m_path in zip(images, meshes):
      # Extract pose
      k = int(p_matcher.search(im_path).groups()[0])
      # Run face detection
      im_patch = _run_fdet(pargs, im_path, det)
      if im_patch is not None:
        # Save image
        im_name = _path.join(img_folder,
                             'subj{:03d}_pose{:02d}.jpg'.format(subj, k))
        imwrite(im_name, im_patch.astype(np.uint8))
        # Densify mesh
        me_name = _path.join(mesh_folder,
                             'subj{:03d}_pose{:02d}.obj'.format(subj, k))
        _densify_mesh(pargs, input=m_path, output=me_name)
      else:
        logger.error('Can not detect face for: {}'.format(im_path))


def _prepare_lfw_dataset(pargs, det):
  """
  Prepare LFW dataset for appearance benchmarking
  :param pargs: Parsed arguments
  :param det:   Face detector
  """

  # Create output folder if needed
  img_folder = _setup_output(pargs, 'appearance', 'images')
  rec_folder = _setup_output(pargs, 'appearance', 'records')
  # Create writer
  wname = _path.join(rec_folder, 'image.tfrecords')
  with tf.io.TFRecordWriter(wname) as writer:
    # Create dataset
    dset = LFW(pargs.lfw_path)
    part = 'train'
    for cnt, ((_, img), name) in enumerate(zip(dset.samples(partition=part),
                                               dset.names(partition=part))):
      # Log progress
      if cnt % 1000 == 0:
        logger.info('Process: %s', name)
      # Run face detection
      bboxes = det(image=img)
      if bboxes:
        # Select bbox
        # Make all the detection square
        bbox = []
        for bb in bboxes:
          # Consider only bbox with score large enough
          if bb.score > pargs.score_threshold:
            bb.add_margin(margin=pargs.margin)
            bb.clip_to_border(width=img.shape[1], height=img.shape[0])
            bbox.append(bb.square(clip_h=img.shape[0], clip_w=img.shape[1]))
        # Sort by:
        #  - bbox width, largest first
        bbox.sort(key=lambda x: x.width(), reverse=True)
        if len(bbox):
          # Pick first + Crop
          r = bbox[0]
          x_1, y_1, x_2, y_2 = list(map(int, (r.xmin, r.ymin,
                                              r.xmax, r.ymax)))
          if (x_2 - x_1) != (y_2 - y_1):
            d = min((x_2 - x_1), (y_2 - y_1))
            x_2 = x_1 + d
            y_2 = y_1 + d
          patch = img[y_1:y_2, x_1:x_2, :]
          # Down sample patch
          patch_sized = cv.resize(patch.astype(np.float32),
                                  dsize=(pargs.img_size, pargs.img_size),
                                  interpolation=cv.INTER_LINEAR)
          patch_sized = patch_sized.astype(patch.dtype)
          # Export Image
          fname = _path.join(img_folder, '{:08}.jpg'.format(cnt))
          cv.imwrite(fname, patch_sized[..., ::-1])
          # Dump to tf records ?
          label = _path.splitext(name)[0].lower().encode()
          _dump_to_tfrecords(patch_sized, writer, label=label)
        else:
          logger.warning('Bounding box dropped')
      else:
        logger.warning('No face detected: %s', name)


def _process(pargs):
  """
  Run preparation process
  :param pargs: Parsed arguments
  """
  # Destination folder
  if not _path.exists(pargs.output):
    _mkdir(pargs.output)

  # Load models
  fdet = SFDDetector(model=pargs.sfd_path, max_image_size=640.0)
  if pargs.micc_path is not None:
    logger.info('Prepare MICC dataset')
    _prepare_micc_dataset(pargs=pargs, det=fdet)
  if pargs.fw_path is not None:
    logger.info('Prepare FaceWarehouse dataset')
    _prepare_facewarehouse_dataset(pargs=pargs, det=fdet)
  if pargs.lfw_path is not None:
    logger.info('Prepare LFW dataset')
    _prepare_lfw_dataset(pargs=pargs, det=fdet)
  logger.info('Done')


if __name__ == '__main__':
  # Parser
  p = ArgumentParser('Test set preparation')

  # Micc path -> Geometry
  p.add_argument('--micc_path',
                 type=str,
                 help='Location where MICC dataset is stored')
  # FaceWarehouse
  p.add_argument('--fw_path',
                 type=str,
                 help='Location of the FaceWarehouse dataset')
  p.add_argument('--fw_subjects',
                 type=list,
                 default=[4, 136, 138, 145, 146, 147, 148, 149, 150],
                 help='List of subject to select from the FaceWarehouse '
                      'dataset')
  p.add_argument('--meshlab_file',
                 type=str,
                 help='Meshlab script performing densification for '
                      'FaceWarehouse dataset')
  p.add_argument('--meshlabserver',
                 type=str,
                 help='Path to meshlabserver executable')
  # LFM path -> Appearance
  p.add_argument('--lfw_path',
                 type=str,
                 help='Location where LFW dataset is stored')
  # SFD detector
  p.add_argument('--sfd_path',
                 type=str,
                 required=True,
                 help='Path to the SFD model (i.e. *.npy)')
  # Micc video frame sampling
  # Based on Deng et al, they used every 10 frames
  p.add_argument('--video_sampling_rate',
                 type=int,
                 required=False,
                 default=10,
                 help='Sampling rate for MICC videos, default 10')
  # Image dimension
  p.add_argument('--img_size',
                 type=int,
                 default=227,
                 help='Image dimension')
  # Extra margin
  p.add_argument('--margin',
                 type=float,
                 default=0.3,
                 help='Extra margin for the face bbox (percentage)')
  p.add_argument('--bbox_distance',
                 type=float,
                 default=100.0,
                 help='Maximum distance the bbox is allowed to move between two'
                      ' consecutive frames. If distance is larger, the '
                      'detection will be assumed to be invalid')
  p.add_argument('--score_threshold',
                 type=float,
                 default=0.90,
                 help='Minimum detection score.')
  p.add_argument('--bbox_image_ratio',
                 type=float,
                 default=1.25,
                 help='Minimum ratio between image size (i.e. min(H, W)) and '
                      'bbox size')
  p.add_argument('--max_zoom_level',
                 type=int,
                 default=2,
                 help='Maximum zoom level to consider (zero indexed), '
                      'default=2')
  # Output
  p.add_argument('--output',
                 type=str,
                 required=True,
                 help='Location where to place the data')
  args = p.parse_args()
  # Run processing
  _process(pargs=args)

