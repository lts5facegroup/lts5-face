# coding=utf-8
"""
This script is part of the data preparation steps for 3DMM fitting network.
Given a bunch of images (i.e. face region) it will generate `training` and
`validation` sets.
"""
from os.path import join as _join
from os.path import exists as _exists
from os.path import splitext, basename
from os import makedirs as _makedirs
from math import ceil as _ceil
import re
import multiprocessing as mp
from functools import partial
import argparse
import numpy as _np
import tensorflow as _tf

# Setup logger to also dump log in text file
from sys import stdout
from lts5.utils import TeeStream, Logger
stream = TeeStream()    # Link to default sys.cout + file
stream.link_stream(stdout)
script_name = splitext(basename(__file__))[0]
stream.link_stream(open('{}_log.txt'.format(script_name), 'wt'))
# Create logger with TeeStream as ouput
logger = Logger(stream)

from lts5.tensorflow_op.utils import initialize_device_memory
from lts5.tensorflow_op.utils.tfrecords import to_feature

__author__ = 'Christophe Ecabert'


initialize_device_memory()


def _create_example_for_line(folder: str,
                             line: str,
                             dims: tuple,
                             mask_folder=None):
  """
  Create a tf.train.Example from a given string
  :param folder:      Root folder where data are stored
  :param line:        String to parse
  :param dims         Image dimensions, tuple(H, W)
  :param mask_folder: If set, replace default mask folder with this one.
  :return:  tf.train.Example
  """

  def _read_raw(path):
    """
    Load a given file into memory as a raw array
    :param path:  Path to the file
    :return:  Bytes array as a string
    """
    with open(path, 'rb') as f:
      return f.read()

  # Split
  face_id, im_path, label_path, lms_path, seg_path = line.strip().split(',')
  if mask_folder is not None:
    seg_path = seg_path.replace('masks', mask_folder)
  # Load img, landmark, segmentation
  face_id_int = int(face_id)
  im_str = _read_raw(_join(folder, im_path))
  lms_str = _read_raw(_join(folder, lms_path))
  seg_str = _read_raw(_join(folder, seg_path))
  # Create example
  feat = {'face_id':      to_feature(face_id_int),
          'shape':        to_feature(dims),
          'image':        to_feature(im_str),
          'landmark':     to_feature(lms_str),
          'segmentation': to_feature(seg_str)}
  example = _tf.train.Example(features=_tf.train.Features(feature=feat))
  return example


def write_single_shard(shd_index,
                       pargs,
                       samples,
                       dims,
                       type,
                       mask_folder=None):
  """
  Dump a single shard into a tfrecord file
  :param shd_index: int, Shard index
  :param pargs: Parsed command line parameters
  :param samples: list of samples
  :param dims: Image dimensions
  :param type: Partition type
  :param mask_folder: If set, replace default mask folder with this one.
  """
  start = shd_index * pargs.train_shard_size
  stop = (shd_index + 1) * pargs.train_shard_size
  stop = stop if stop < len(samples) else len(samples)
  # Create tfrecords writer for the given shards
  record_f = '' if mask_folder is None else '_' + mask_folder
  wname = _join(pargs.root,
                'records{}'.format(record_f),
                '{}_{:03d}.tfrecords'.format(type.lower(),
                                             shd_index))
  logger.info('Process %s',
              'records{}/{}_{:02d}.tfrecords'.format(record_f,
                                                     type.lower(),
                                                     shd_index))
  with _tf.io.TFRecordWriter(path=wname) as writer:
    # Iterate over samples
    for s in samples[start:stop]:
      # Create an tf.train.Example from a given line
      example = _create_example_for_line(folder=pargs.root,
                                         line=s,
                                         dims=dims,
                                         mask_folder=mask_folder)
      # Dump it
      writer.write(example.SerializeToString())


def convert_to_records(pargs, samples, dims, type='train', mask_folder=None):
  """
  Convert samples into tfrecors file
  :param pargs:   Parse arguments
  :param samples: List of samples to dump
  :param dims:    Image dimensions, tuple(H, W)
  :param type:    Partition: 'train', 'validation', 'test'
  :param mask_folder: If set, replace default mask folder with this one.
  """
  pool = mp.Pool(mp.cpu_count())  # Create threaded pool
  n_split = (len(samples) // pargs.train_shard_size) + 1
  pool.map(partial(write_single_shard,
                   pargs=pargs,
                   samples=samples,
                   dims=dims,
                   type=type,
                   mask_folder=mask_folder),
           range(n_split))


def _load_index(path):
  """ Read file """
  index = []
  with open(path, 'rt') as f:
    for line in f:
      line = line.strip()
      if len(line):
        index.append(int(line))
  return index


def _filter_list(arrays, indexes):
  """
  Remove entry from a given array based on a selection
  :param arrays:     List to prune of list
  :param indexes:   List of indexes to remove
  :return:  Pruned list
  """
  sample_nb = re.compile(r"[0-9]{8}")
  for i, array in enumerate(arrays):
    values = []
    for value in array:
      # Extract sample number
      k = int(sample_nb.search(value).group())
      if k not in indexes:
        values.append(value)
    arrays[i] = values
  return arrays


def _load_dataset_distribution(path):
  dist = []
  with open(path, 'rt') as f:
    for line in f:
      line = line.strip()
      if len(line) > 0:
        dataset_name, counter = line.split(',')
        dist.append(int(counter))
  return dist


def _split_data(pargs):
  """
  Generate training/validation set
  :param pargs: Parsed arguments
  """

  # Pick proper source of samples
  if _exists(_join(pargs.root, 'segmented_samples.txt')):
    fname = _join(pargs.root, 'segmented_samples.txt')
  elif _exists(_join(pargs.root, 'aligned_samples.txt')):
    fname = _join(pargs.root, 'aligned_samples.txt')
  else:
    fname = _join(pargs.root, 'samples.txt')

  # Read dataset distribution
  dataset_dist = _load_dataset_distribution(_join(pargs.root,
                                                  'datasets_distribution.txt'))
  # Read all selected images
  samples = [[] for _ in dataset_dist]
  dims = None
  with open(fname, 'r') as f:
    hdr = f.readline().strip()
    dims = f.readline().strip().split(',')
    dims = list(map(int, dims))
    dataset_idx = 0
    sample_counter = 0
    for line in f:
      samples[dataset_idx].append(line)
      sample_counter += 1
      if sample_counter == dataset_dist[dataset_idx]:
        sample_counter = 0
        dataset_idx += 1

  if samples[0]:

    # Sample to discard ?
    if pargs.discard_file is not None:
      discard_idx = _load_index(_join(pargs.root, pargs.discard_file))
      logger.debug('Sample set size BEFORE pruning: {}'.format([len(s) for s in samples]))
      samples = _filter_list(samples, discard_idx)
      logger.debug('Sample set size AFTER pruning: {}'.format([len(s) for s in samples]))

    # Create output folder
    if not _exists(_join(pargs.root, 'records')):
      _makedirs(_join(pargs.root, 'records'))
      _makedirs(_join(pargs.root, 'records_face_parts'))
    # random permutations
    train_samples = []
    val_samples = []
    for sample in samples:
      n_valid = int(_ceil(pargs.validation_set_size * len(sample)))
      rand_sample = _np.random.permutation(sample)
      val_samples.extend(rand_sample[:n_valid])
      train_samples.extend(rand_sample[n_valid:])
    # Dump into tfrecords files with combined segmentation mask
    # (i.e. Face parts + Skin segmentation)
    convert_to_records(pargs=pargs,
                       samples=train_samples,
                       dims=dims,
                       type='train')
    convert_to_records(pargs=pargs,
                       samples=val_samples,
                       dims=dims,
                       type='validation')
    # Dump into tfrecords files with face parts only
    convert_to_records(pargs=pargs,
                       samples=train_samples,
                       dims=dims,
                       type='train',
                       mask_folder='face_parts')
    convert_to_records(pargs=pargs,
                       samples=val_samples,
                       dims=dims,
                       type='validation',
                       mask_folder='face_parts')
    # Done
    return 0
  logger.error('No samples was found in: %s', fname)
  return -1


if __name__ == '__main__':
  # Parser
  parser = argparse.ArgumentParser(description='Data split')

  # Input folder
  parser.add_argument('--input',
                      type=str,
                      required=True,
                      dest='root',
                      help='Root folder storing face region')
  # Validation set size
  parser.add_argument('--val_size',
                      type=float,
                      required=False,
                      default=0.1,
                      dest='validation_set_size',
                      help='Percentage of samples to put in the validation set')
  # Training set shard size
  parser.add_argument('--shard_size',
                      type=int,
                      default=2000,
                      dest='train_shard_size',
                      help='Number of training samples at most per files')
  # List of images to discard
  parser.add_argument('--discard_file',
                      type=str,
                      default=None,
                      help='Text file containing index of images to discard')
  # parse
  args = parser.parse_args()
  err_code = _split_data(pargs=args)
  exit(err_code)
