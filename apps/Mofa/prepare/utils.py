#coding=utf-8
from abc import ABC, abstractmethod
import numpy as _np
import cv2 as _cv
import tensorflow as tf

from lts5.classifier import SkinClassifier
from lts5.tensorflow_op.network.pspnet import PSPNet, PSPNetV2
from lts5.tensorflow_op.network.bisenet import BiSeNet
from lts5.tensorflow_op.network.sfd import S3FaceDetector

__author__ = 'Christophe Ecabert'


def _check_delta_range(center, delta, max_value):
  """
  Check if a given delta value will fall outside
  :param center:      Center
  :param delta:       Distance to add
  :param max_value:   Upper bound
  :return:  Correct delta if necessary
  """
  # upper / lower bound
  valid_delta = delta
  if center + delta > max_value - 1.0:
    # Overshoot upper limit
    valid_delta = max_value - 1.0 - center
  if center - delta < 0.0:
    # overshoot lower limit
    valid_delta = min(valid_delta, center)
  return valid_delta


class BBox:
  """ Face bounding box """

  def __init__(self, xmin, ymin, xmax, ymax, score=1.0):
    """
    Constructor
    :param xmin:  Min x
    :param ymin:  Min y
    :param xmax:  Max x
    :param ymax:  Max y
    """
    self.xmin = xmin
    self.ymin = ymin
    self.xmax = xmax
    self.ymax = ymax
    self.score = score

  @classmethod
  def from_position(cls, cx, cy, width, height, score=1.0):
    """
    Constructor a bbox at given position
    :param cx:        Center - x
    :param cy:        Center - y
    :param width:     Box width
    :param height:    Box height
    :param score:     Box score, default to 1.0
    :return:  BBox
    """
    xmin = cx - (width / 2.0)
    xmax = xmin + width
    ymin = cy - (height / 2.0)
    ymax = ymin + height
    return cls(xmin, ymin, xmax, ymax, score)

  def width(self):
    return self.xmax - self.xmin

  def height(self):
    return self.ymax - self.ymin

  def center(self):
    return (self.xmin + self.xmax) / 2.0, (self.ymin + self.ymax) / 2.0,

  def area(self):
    """
    Compute bbox area
    :return:  Area
    """
    w = self.width()
    h = self.height()
    return w * h

  def overlap(self, other):
    """
    Check if the boxes overlaps
    :param other: Other bbox to check against
    :return: True if overlap, False otherwise
    """
    # See: "Realtime Collision Detection: p79
    if self.xmax < other.xmin or self.xmin > other.xmax:
      return False
    if self.ymax < other.ymin or self.ymin > other.ymax:
      return False
    return True

  def iou(self, other):
    """
    Compute intersection over union between `this` and `other` bbox
    :param other: BBox to check against
    :return:  IoU
    """
    # determine the (x, y)-coordinates of the intersection rectangle
    xa = max(self.xmin, other.xmin)
    ya = max(self.ymin, other.ymin)
    xb = min(self.xmax, other.xmax)
    yb = min(self.ymax, other.ymax)
    # compute the area of intersection rectangle
    inter_area = max(0, xb - xa + 1) * max(0, yb - ya + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    box_a_area = self.area()
    box_b_area = other.area()
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the intersection area
    iou = inter_area / float(box_a_area + box_b_area - inter_area)
    # return the intersection over union value
    return iou

  def inside(self, x, y):
    """
    Check if a given point (x, y) is inside the box
    :param x: X point coordinate
    :param y: Y point coordinate
    :return:  True, False
    """
    if self.xmin <= x <= self.xmax and self.ymin <= y <= self.ymax:
      return True
    return False

  def add_margin(self, margin):
    """
    Add extra margin to the bbox
    :param margin:  Margin percentage to add
    """
    dx = round(self.width() * margin * 0.5)
    dy = round(self.height() * margin * 0.5)
    self.xmin -= dx
    self.xmin += dx
    self.ymin -= dy
    self.ymax += dy
    # NOTE: Only apply margin toward the chin. Doing so gives results more
    # close to the original paper than apply dy in both direction.

  def clip_to_border(self, width, height):
    """
    Ensure the box is inside the given dimensions
    :param width:   Maximum possible x value
    :param height:  Maximum possible y value
    """
    # Box larger than image
    if self.width() > width:
      self.xmin = 0.0
      self.xmax = width - 1.0
    if self.height() > height:
      self.ymin = 0.0
      self.ymax = height - 1.0
    # Left
    if self.xmin < 0.0:
      self.xmax -= self.xmin
      self.xmin -= self.xmin
    # Right
    if self.xmax > width:
      dx = self.xmax - width
      self.xmin -= dx
      self.xmax -= dx
    # Top
    if self.ymin < 0.0:
      self.ymax -= self.ymin
      self.ymin -= self.ymin
    # Bottom
    if self.ymax > height:
      dy = self.ymax - height
      self.ymin -= dy
      self.ymax -= dy

  def square(self, clip_h=None, clip_w=None):
    """
    Convert bbox to a square box
    :param clip_h:  Clipping value for box's height
    :param clip_w:  Clipping value for box's width
    :return:  New squared bounding box
    """
    w = self.width()
    h = self.height()
    half_s = max(w, h) / 2.0
    cx, cy = self.center()
    dx = half_s if clip_w is None else _check_delta_range(cx, half_s, clip_w)
    dy = half_s if clip_h is None else _check_delta_range(cy, half_s, clip_h)
    return BBox(xmin=cx - dx,
                ymin=cy - dy,
                xmax=cx + dx,
                ymax=cy + dy,
                score=self.score)

  def __repr__(self):
    return '({}, {}, {}, {})'.format(self.xmin,
                                     self.ymin,
                                     self.xmax,
                                     self.ymax)


class AbstractDetector(ABC):
  """ Face detector abstraction """
  def __init__(self, name):
    self._name = name

  @abstractmethod
  def empty(self):
    """ Indicate if detector is loaded """
    return True

  @abstractmethod
  def __call__(self, image):
    """ Call operator """
    pass


class ViolaJonesDetector(AbstractDetector):
  """ OpenCV face detector """
  def __init__(self, model, name='ViolaJones'):
    """
    Constructor
    :param model: Path to detection model (*.xml)
    :param name:  Detector name
    """
    super(ViolaJonesDetector, self).__init__(name=name)
    self._fd = _cv.CascadeClassifier()
    self._fd.load(model)

  def empty(self):
    """ Indicate if detector is loaded """
    return self._fd.empty()

  def __call__(self, image):
    """
    Run face detection on a given image
    :param image: Image on which to run the detection
    :return: List of bbox
    """
    rects = self._fd.detectMultiScale(image=image,
                                      scaleFactor=1.05,
                                      minNeighbors=3,
                                      minSize=(100, 100),
                                      flags=_cv.CASCADE_SCALE_IMAGE)
    if len(rects):
      bbox = []
      for r in rects:
        r[2:] += r[:2]
        bbox.append(BBox(xmin=r[0], ymin=r[1], xmax=r[2], ymax=r[2]))
      return bbox
    return None


class SFDDetector(AbstractDetector):
  """ SFD face detector """

  def __init__(self, model, max_image_size=1280.0, name='SFD'):
    super(SFDDetector, self).__init__(name=name)
    self.fd = S3FaceDetector(model_path=model)
    self.image_sz = max_image_size

  def empty(self):
    """ Indicate if detector is loaded """
    return False

  def __call__(self, image):
    """
    Run face detection on a given image
    :param image: Image on which to run the detection
    :return: List of bbox
    """
    image_sz = float(max(image.shape[0], image.shape[1]))
    image_sz = self.image_sz if image_sz > self.image_sz else image_sz
    res = self.fd.process(image_or_path=image, image_size=image_sz)
    if len(res):
      bbox = []
      for r in res:
        bbox.append(BBox(xmin=r[3], ymin=r[4],
                         xmax=r[5], ymax=r[6],
                         score=r[2]))
      return bbox
    return None


class TrackerWrapper(object):
  """ Thin wrapper around face tracker algorithm """

  def process(self, image, region):
    """
    Run face tracking algorithm on a given image
    :param image: Image - BGR (numpy array)
    :param region:  Face region to process
    :return:  Landmarks or none
    """
    pass

  @property
  def score(self):
    """ Detection confidence """
    return 1.0


class SdmTrackerWrapper(TrackerWrapper):
  """ SDM interface """

  def __init__(self, model_path):
    """
    Constructor
    :param model_path: Location where model is stored
    """
    from lts5.face_tracker import SDMTracker as Sdm
    # Create tracker + load
    self._tracker = Sdm()
    if self._tracker.load(tracker=model_path):
      raise RuntimeError('Unable to load face tracker: {}\n'.format(model_path))

  def process(self, image, region):
    """
    Run face alignment
    :param image:   Image - BGR (numpy array)
    :param region:  Face region to process
    :return:  Landmarks or none
    """
    lms = self._tracker.detect(image=image, region=region)
    if lms is not None:
      # Reshape landmarks
      lms = (lms.reshape(2, -1)
             .transpose()
             .reshape(1, -1)
             .astype(_np.float32))
    return lms

  @property
  def score(self):
    """ Detection confidence """
    return self._tracker.score


class FAN2DWrapper(TrackerWrapper):
  """ FAN2D alignment network """

  def __init__(self, model_path):
    """
    Constructor
    :param model_path: Location where model is stored
    """
    from lts5.tensorflow_op.network.fan import FAN2D
    self._net = FAN2D()
    self._net.load_weights(filepath=model_path)

  def process(self, image, region):
    """
    Run face alignment
    :param image:   Image - RGB (numpy array)
    :param region:  Face region to process
    :return:  Landmarks or none
    """
    img = image[..., ::-1].copy().astype(_np.float32)
    roi = _np.asarray(region, dtype=_np.int32).reshape(1, -1)
    lms = self._net.get_landmark_from_image(image_or_path=img, face_bboxes=roi)
    if lms is not None:
      lms = lms[0].reshape(1, -1).astype(_np.float32)
    return lms

  @property
  def score(self):
    """ Confidence, return default value: 1.0 """
    return 1.0


class SegmentationWrapper(object):
  """ Thin wrapper around skin segmentation algorithm """

  def process(self, image):
    """ Run segmentation """
    pass


class NeuralWrapper(SegmentationWrapper):

  def __init__(self, model_path, img_size=227):
    """
    Constructor
    :param model_path: Location of the segmentation model
    :param img_size: Image size, default=227
    """
    # Instantiate segmentation network
    img_sz = (1, img_size, img_size, 3)
    self._model = PSPNet(config='resnet18')
    self._model.build(img_sz)
    path = tf.train.latest_checkpoint(model_path)
    self._model.load_weights(path).expect_partial()

  def process(self, image):
    """
    Run segmentation
    :param image: Image to be processed (BGR formatted)
    :return:  Segmentation mask
    """
    # Do segmentation
    im = _np.expand_dims(image[..., ::-1], axis=0).astype(_np.float32)
    prob = self._model(im, training=False).numpy()
    prob = _np.squeeze(prob, axis=(0, 3))
    return prob


class CelebAParsingWrapper(SegmentationWrapper):
  """ Face parsing using CelebA mask (i.e. 19 classes) """

  def __init__(self,
               model_path,
               type='resnet18',
               img_size=227):
    """
    Constructor
    :param model_path: Location of the segmentation model
    :param type: Backbone type
    :param img_size: Image size, default=227
    """
    # Instantiate segmentation network
    img_sz = (1, img_size, img_size, 3)
    self._model = BiSeNet.CelebA(type=type)
    self._model.build(img_sz)
    path = tf.train.latest_checkpoint(model_path)
    self._model.load_weights(path).expect_partial()

  def process(self, image):
    """
    Run segmentation
    :param image: Image to be processed (BGR formatted)
    :return:  Segmentation mask
    """
    # Convert back to RGB
    im = _np.expand_dims(image[..., ::-1], axis=0).astype(_np.float32)
    pred = self._model(im, training=False).numpy()
    mask = _np.squeeze(pred.argmax(axis=-1), 0)
    # Create binary mask by combining category
    # 1: 'Skin',
    # 2: 'Nose',
    # 4: 'Left Eye',
    # 5: 'Right Eye',
    # 6: 'Left Eyebrow',
    # 7: 'Right Eyebrow',
    # 10: 'Mouth',
    # 11: 'Upper Lip',
    # 12: 'Lower Lip',
    fused_mask = _np.zeros_like(mask, dtype=_np.float32)
    fused_mask[(((mask > 0) & (mask < 8) & (mask != 3)) |
                ((mask > 9) & (mask < 13)))] = 1.0
    # Mask has shape 224, 227 -> resize to input dims
    fused_mask = _cv.resize(fused_mask,
                            image.shape[:2],
                            interpolation=_cv.INTER_NEAREST)
    return fused_mask


class CombinedWrapper(SegmentationWrapper):
  """
  Combine skin segmentation and face parsing into a single model
  """

  def __init__(self,
               skin_model_path,
               parsing_model_path,
               skin_type='resnet18',
               parsing_type='resnet18'):
    """
    Constructor
    :param skin_model_path: Location of the segmentation model
    :param parsing_model_path: Location of the face parsing model
    :param skin_type: Backbone type for skin segmentation
    :param parsing_type: Backbone type for face parsing
    :param img_size: Image size, default=227
    """
    # Instantiate segmentation network
    img_sz = (1, 224, 224, 3)
    self._skin_model = PSPNetV2(config=skin_type)
    self._skin_model.build(img_sz)
    path = tf.train.latest_checkpoint(skin_model_path)
    self._skin_model.load_weights(path).expect_partial()
    # Instantiate Parsing network
    img_sz = (1, 224, 224, 3)
    self._parse_model = BiSeNet.CelebA(type=parsing_type)
    self._parse_model.build(img_sz)
    path = tf.train.latest_checkpoint(parsing_model_path)
    self._parse_model.load_weights(path).expect_partial()

  def process(self, image):
    """
    Run segmentation
    :param image: Image to be processed (BGR formatted)
    :return:  Segmentation mask
    """
    # Convert back to RGB + resize to 224
    im = image[..., ::-1].astype(_np.float32)
    im = _cv.resize(im, (224, 224))
    im = _np.expand_dims(im, axis=0)
    # Skin segmentation
    seg = self._skin_model(im, training=False).numpy()
    seg = _np.squeeze(seg, (0, -1))
    # Face parsing
    pred = self._parse_model(im, training=False).numpy()
    mask = _np.squeeze(pred.argmax(axis=-1), 0)
    # Create binary mask by combining category
    # 1: 'Skin',
    # 2: 'Nose',
    # 4: 'Left Eye',
    # 5: 'Right Eye',
    # 6: 'Left Eyebrow',
    # 7: 'Right Eyebrow',
    # 10: 'Mouth',
    # 11: 'Upper Lip',
    # 12: 'Lower Lip',
    fused_mask = _np.zeros_like(mask, dtype=_np.float32)
    fused_mask[(((mask > 0) & (mask < 8) & (mask != 3)) |
                ((mask > 9) & (mask < 13)))] = 1.0
    part_mask = fused_mask.copy()
    # Set skin region to skin segmentation value
    fused_mask[mask == 1] = seg[mask == 1]
    # Mask has shape 224, 227 -> resize to input dims
    fused_mask = _cv.resize(fused_mask,
                            image.shape[:2],
                            interpolation=_cv.INTER_NEAREST)
    part_mask = _cv.resize(part_mask,
                           image.shape[:2],
                           interpolation=_cv.INTER_NEAREST)
    return fused_mask, part_mask


class GMMWrapper(SegmentationWrapper):
  """ Naive bayse classifier based on Gaussian Mixture for classes modeling"""

  def __init__(self, thresh=0.5):
    """ Constructor """
    self._model = SkinClassifier(use_equal_prior=True)
    self._thr = thresh

  def process(self, image):
    """
    Run segmentation
    :param image: Image to be processed (BGR formatted)
    :return:  Segmentation mask
    """
    # Do segmentation
    im = image[:, :, ::-1].astype(_np.float64)
    prob = self._model.predict_proba(im)
    # Extract prob of being `skin`
    prob = prob[:, :, 1].astype(_np.float32)
    # Set prob > 0.5 to 1 in a similar way of "Deng et al, 2019"
    prob[prob > self._thr] = 1.0
    return prob