# coding=utf-8
"""
This script is part of the data preparation steps for 3DMM fitting network.
Given a bunch of images (i.e. face region) it will perform skin segmentation to
define which region can be used during training
"""
from os.path import join as _join
from os.path import exists as _exists
from os.path import splitext, basename
from os import makedirs as _makedirs
import zlib
import argparse
import cv2 as _cv


# Setup logger to also dump log in text file
from sys import stdout
from lts5.utils import TeeStream, Logger
stream = TeeStream()    # Link to default sys.cout + file
stream.link_stream(stdout)
script_name = splitext(basename(__file__))[0]
stream.link_stream(open('{}.log'.format(script_name), 'wt'))
# Create logger with TeeStream as ouput
logger = Logger(stream)

from utils import CombinedWrapper, NeuralWrapper
from utils import GMMWrapper, CelebAParsingWrapper

__author__ = 'Christophe Ecabert'


def init_segmentation(pargs, img_size=227):
  """
  Initialize segmentation algorithm
  :param pargs:  Parsed arguments
  :param img_size:    Image dimension, default=227
  """
  if pargs.seg_bisenet is not None and pargs.seg_cnn is not None:
    return CombinedWrapper(skin_model_path=pargs.seg_cnn,
                           skin_type='resnet18',
                           parsing_model_path=pargs.seg_bisenet,
                           parsing_type='resnet18')
  elif pargs.seg_gmm is not None:
    return GMMWrapper()
  elif pargs.seg_cnn is not None:
    return NeuralWrapper(model_path=pargs.seg_cnn,
                         img_size=img_size)
  elif pargs.seg_bisenet is not None:
    return CelebAParsingWrapper(model_path=pargs.seg_bisenet,
                                img_size=img_size)
  else:
    raise RuntimeError('No segmentation model provided')


def _run_segmentation(pargs):
  """
  Run skin segmentation
  :param pargs:   Parsed argument
  :return:  Errno
  """

  # Check output directory
  odir = _join(pargs.root, 'masks')
  if not _exists(odir):
    _makedirs(odir)

  # Instantiate segmentation network
  algo = init_segmentation(pargs=pargs, img_size=pargs.img_size)

  if isinstance(algo, CombinedWrapper):
    odir = _join(pargs.root, 'face_parts')
    if not _exists(odir):
      _makedirs(odir)

  # Load, check if alignment as been performed already
  if _exists(_join(pargs.root, 'aligned_samples.txt')):
    samples_file = _join(pargs.root, 'aligned_samples.txt')
  else:
    samples_file = _join(pargs.root, 'samples.txt')

  with open(samples_file, 'rt') as samples, \
    open(_join(pargs.root, 'segmented_samples.txt'), 'w') as f_seg:

    def _dump_mask(pargs, prob, im_name, mask_folder='masks'):
      """
      Dump segmentation mask into binary file
      :param pargs:     Parsed arguments
      :param prob:      Numpy array of probability
      :param im_name:   Image name
      :param mask_folder: Name of folder where to dump data
      """
      mask_name = im_name.replace('images', mask_folder)    # Folder
      mask_name = mask_name.replace('jpg', 'mk')            # Extension
      with open(_join(pargs.root, mask_name), 'wb') as f:
        data = zlib.compress(prob.tobytes())
        f.write(data)
      return mask_name

    # Read header + image dims
    hdr = samples.readline().strip()
    dims = samples.readline().strip()
    # Generate hdr
    f_seg.write('{},Mask\n'.format(hdr))
    f_seg.write('{}\n'.format(dims))
    # Goes through all samples
    for im_path in samples:
      path = im_path.strip().split(',')[1]
      img = _cv.imread(_join(pargs.root, path))
      logger.info('Processing: %s', path)
      # Run segmentation
      prob = algo.process(image=img)
      if isinstance(prob, tuple):
        # Got multiple mask
        prob, face_part = prob
        # Dump face parts
        _dump_mask(pargs=pargs, prob=face_part,
                   im_name=path, mask_folder='face_parts')
      # Dump + log
      mask_name = _dump_mask(pargs=pargs, prob=prob, im_name=path)
      f_seg.write('{},{}\n'.format(im_path.strip(), mask_name))
  return 0


if __name__ == '__main__':
  # Parser
  parser = argparse.ArgumentParser(description='Skin segmentation')

  # Input folder
  parser.add_argument('--input',
                      type=str,
                      required=True,
                      dest='root',
                      help='Root folder storing face region')
  # Segmentation network
  parser.add_argument('--seg_gmm',
                      type=str,
                      help='GMM segmentation model')
  parser.add_argument('--seg_cnn',
                      type=str,
                      help='Folder where SkinNet model is stored')
  parser.add_argument('--seg_bisenet',
                      type=str,
                      help='Folder where BiSeNet model is stored (Face '
                           'parsing)')
  # Image size
  parser.add_argument('--size',
                      type=int,
                      required=False,
                      default=227,
                      dest='img_size',
                      help='Dimension of the cropped face')

  # parse
  args = parser.parse_args()

  # Do stuff
  err_code = _run_segmentation(pargs=args)
  exit(err_code)
