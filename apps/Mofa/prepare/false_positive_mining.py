# coding=utf-8
"""
Perform 3d reconstruction on every sample of MorphNet training set and detect
the one that perform badly for further analysis
"""
from os.path import join as _join
from os.path import exists as _exists
from os.path import splitext, basename
from os import listdir as _ls
from os import makedirs as _mkdir
from collections import OrderedDict
from argparse import ArgumentParser
from zlib import decompress
import tensorflow as tf
import numpy as np
from imageio import imread, imwrite
import pickle as pkl
import matplotlib.pyplot as plt
import seaborn as sns
import cv2

# Setup logger to also dump log in text file
from sys import stdout
from lts5.utils import TeeStream, Logger
stream = TeeStream()    # Link to default sys.cout + file
stream.link_stream(stdout)
script_name = splitext(basename(__file__))[0]
stream.link_stream(open('{}.log'.format(script_name), 'wt'))
# Create logger with TeeStream as ouput
logger = Logger(stream)


from lts5.tensorflow_op.utils import initialize_device_memory
from lts5.tensorflow_op.network import MorphNetParameter, MorphNet

__author__ = 'Christophe Ecabert'


initialize_device_memory()


def _load_image(path):
  """
  Load an image from a given path
  :param path:  Location of the image to open
  :return:  numpy array of Image + background
  """
  # Load image
  image = np.asarray(imread(path)).astype(np.float32)
  bg = image / 255.0
  return image, bg


def _load_landmarks(path):
  """
  Load a set of landmarks from a given path
  :param path:  Location of landmark's file
  :return:  numpy array
  """
  with open(path, 'rb') as f:
    bytes = f.read()
  return np.frombuffer(bytes, dtype=np.float32).reshape(-1, 2)


def _load_mask(path, dims):
  """
  Load segmentation mask from a given path
  :param path:  Location where mask is stored
  :param dims:  int, Mask dimensions
  :return:  numpy array
  """
  with open(path, 'rb') as f:
    bytes = f.read()
    uncompress_bytes = decompress(bytes)
  return np.frombuffer(uncompress_bytes, dtype=np.float32).reshape(dims,
                                                                   dims,
                                                                   1)


def load_data(folder, index):
  """
  Load data from a given folder for a given sample index
  :param folder:  Root folder where data are stored
  :param index:   Sample index
  :return:  Image, background, mask, landmarks
  """
  # Image
  im_path = _join(folder, 'images', '{:08d}.jpg'.format(index))
  image, bg = _load_image(im_path)
  # Mask
  seg_path = _join(folder, 'masks', '{:08d}.mk'.format(index))
  mask = _load_mask(seg_path, 224)
  # Landmarks
  lms_path = _join(folder, 'landmarks', '{:08d}.lms'.format(index))
  lms = _load_landmarks(lms_path)
  return image, bg, mask, lms


def _run_reconstruction(pargs):
  """
  Run face reconstruction
  :param pargs: Parsed argument
  :return:  Evaluation metrics
  """

  # Use cache ?
  if not _exists(pargs.output_dir):
    _mkdir(pargs.output_dir)
  if not _exists(_join(pargs.output_dir, 'reconstructions')):
    _mkdir(_join(pargs.output_dir, 'reconstructions'))

  if not _exists(_join(pargs.output_dir, 'result_eval.pkl')):
    # Build network
    cfg_file = _join(pargs.model_dir, 'config.json')
    cfg = MorphNetParameter.from_json(cfg_file)
    cfg.model.n_es = 0.0  # Disable model refinement
    cfg.model.n_et = 0.0  # Disable model refinement

    model = MorphNet(config=cfg)
    image = tf.keras.Input(shape=cfg.img_dims,
                           dtype=tf.float32,
                           name='image')
    background = tf.keras.Input(shape=cfg.img_dims,
                                dtype=tf.float32,
                                name='background')
    renderings = model([image, background], training=False)
    # Evaluation metrics
    metrics = model.get_metrics()
    eval_ph = metrics[0][0]
    eval_lms = metrics[-1]
    if not isinstance(eval_lms, tf.keras.metrics.Metric):
      # Got a list of metric, pick first
      eval_lms = eval_lms[0]
    # Reload model
    path = tf.train.latest_checkpoint(pargs.model_dir)
    model.load_weights(path).assert_existing_objects_matched()

    @tf.function
    def _predict_fn(image, bg):
      return model([image, bg], training=False)

    # Process all data
    n_sample = len(_ls(_join(pargs.data_dir, 'images')))
    res = OrderedDict()
    for k in range(n_sample):
      if (k % 1000) == 0:
        logger.info('Process sample index: {:08d}'.format(k))
      # Load data
      image, bg, mask, lms = load_data(pargs.data_dir, k)
      tf_image = tf.expand_dims(tf.convert_to_tensor(image), 0)
      tf_bg = tf.expand_dims(tf.convert_to_tensor(bg), 0)
      tf_mask = tf.expand_dims(tf.convert_to_tensor(mask), 0)
      tf_lms = tf.expand_dims(tf.convert_to_tensor(lms), 0)
      tf_label = tf.concat([tf_bg, tf_mask], -1)
      # Run reconstruction
      renderings = _predict_fn(tf_image, tf_bg)
      image_rec = renderings[0]
      lms_rec = renderings[-1]
      # Run evaluation
      eval_ph.reset_states()
      eval_lms.reset_states()
      error_ph = eval_ph(tf_label, image_rec).numpy()
      error_lms = eval_lms(tf_lms, lms_rec).numpy()
      # Save values
      res[k] = (error_ph, error_lms)
      fname = _join(pargs.output_dir, 'reconstructions', '{:08d}.jpg'.format(k))
      im = (image_rec[0, ..., :3].numpy() * 255.0).astype(np.uint8)
      imwrite(fname, im)
    # Dump evaluation results
    with open(_join(pargs.output_dir, 'result_eval.pkl'),'wb') as f:
      pkl.dump(res, f)
  else:
    logger.warning('Use cached evaluation...')
    with open(_join(pargs.output_dir, 'result_eval.pkl'), 'rb') as f:
      res = pkl.load(f)
  # Done
  return res


def _draw_landmarks(image, lms):
  """
  Draw landmarks on a given image
  :param image:   Image to draw on
  :param lms:     Set of landmarks
  :return:        Image
  """
  img = image.copy()
  for pt in lms:
    img = cv2.circle(img,
                     (int(pt[0]), int(pt[1])),
                     1,
                     (0, 1.0, 0),
                     -1,
                     cv2.LINE_AA)
  return img


def _dump_inputs(pargs, index):
  # Load data
  img, bg, mask, lms = load_data(pargs.data_dir, index)
  # Fuse them
  # Input + Landmarks
  fig = plt.figure(figsize=(12, 4.8))
  fig.add_subplot(1, 3, 1)
  im = _draw_landmarks(bg, lms)
  plt.imshow(im)
  plt.axis('off')
  plt.title('Input')
  # Segmentation mask
  fig.add_subplot(1, 3, 2)
  plt.imshow(bg)
  plt.imshow(mask[..., 0], alpha=0.5)
  plt.axis('off')
  plt.title('Mask')
  # Reconstruction
  fname = _join(pargs.output_dir, 'reconstructions', '{:08d}.jpg'.format(index))
  img_rec = np.asarray(imread(fname)).astype(np.float32) / 255.0
  fig.add_subplot(1, 3, 3)
  plt.imshow(img_rec)
  plt.axis('off')
  plt.title('Reconstruction')
  # Dump image
  plt.savefig(_join(pargs.output_dir, 'failures', '{:08d}.jpg'.format(index)),
              bbox_inches="tight")
  plt.close(fig)


def _filter_reconstruction(pargs, results):
  """
  Filter reconstruction
  :param pargs:     Parsed command line option
  :param results:   Evaluation metrics
  """
  # Show error distribution
  error_ph = []
  error_lms = []
  for k, metrics in results.items():
    eval_ph, eval_lms = metrics
    error_ph.append(eval_ph)
    error_lms.append(eval_lms)
  error_ph = np.asarray(error_ph)
  error_lms = np.asarray(error_lms)

  # Photometric error distribution
  # Remove sample with error too large for visibility
  plt.subplot(1, 2, 1)
  e_ph = error_ph[error_ph < pargs.photo_threshold * 3]
  sns.distplot(e_ph,
               bins=100,
               kde_kws={'label': 'Photometric L2,1'},
               norm_hist=False)
  plt.axvline(x=pargs.photo_threshold, color='r', ls='--')

  # Landmarks errors distribution
  # Remove sample with error too large for visibility
  upper_bnd = 3 * pargs.landmarks_threshold
  e_lms = error_lms[error_lms < upper_bnd]
  plt.subplot(1, 2, 2)
  sns.distplot(e_lms,
               bins=100,
               kde_kws={'label': 'Landmarks L2'},
               norm_hist=False)
  plt.axvline(x=pargs.landmarks_threshold, color='r', ls='--')
  # Dump
  plt.savefig(_join(pargs.output_dir, 'error_distribution.jpg'),
              bbox_inches="tight")

  # Filter samples
  if not _exists(_join(pargs.output_dir, 'failures')):
    _mkdir(_join(pargs.output_dir, 'failures'))

  discard_idx = []
  for k, (v_lms, v_ph) in enumerate(zip(error_lms, error_ph)):
    if (k % 1000) == 0:
      logger.info('Check sample index: {:08d}'.format(k))
    if v_lms > pargs.landmarks_threshold:
      _dump_inputs(pargs, k)
      discard_idx.append(k)

    if k not in discard_idx and v_ph > pargs.photo_threshold:
      _dump_inputs(pargs, k)
      discard_idx.append(k)

  with open(_join(pargs.output_dir, 'discard_rec.txt'), 'wt') as f:
    for idx in discard_idx:
      f.write('{}\n'.format(idx))


if __name__ == '__main__':

  # Parser
  p = ArgumentParser()

  # Root
  p.add_argument('--data_dir',
                 required=True,
                 type=str,
                 help='Location where training set is stored')
  # Model
  p.add_argument('--model_dir',
                 required=True,
                 type=str,
                 help='Location where model is stored')
  # Output dir
  p.add_argument('--output_dir',
                 required=True,
                 type=str,
                 help='Location where to place the results')
  # Photometric threshold
  p.add_argument('--photo_threshold',
                 type=float,
                 default=0.30,
                 help='Photometric error threshold for sample selection')
  p.add_argument('--landmarks_threshold',
                 type=float,
                 default=7.5,
                 help='Landmarks error threshold for sample selection')
  args = p.parse_args()
  res = _run_reconstruction(pargs=args)
  _filter_reconstruction(pargs=args, results=res)


