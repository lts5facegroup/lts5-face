# coding=utf-8
from argparse import ArgumentParser
import numpy as np
from os.path import join, exists
from os import makedirs as mkdir
from lts5.utils.tools import search_folder, init_logger
from lts5.geometry import MeshFloat as Mesh

logger = init_logger()

region_labels = {'skin': 1,
                 'nose': 2,
                 'lefteye': 4,
                 'righteye': 5,
                 'leftbrow': 6,
                 'rightbrow': 7,
                 'upperlip': 11,
                 'lowerlip': 12,
                 'hair': 13}

region_colors = {'skin': (204.0, 0.0, 0.0),
                 'nose': (76.0, 153.0, 0.0),
                 'lefteye': (51.0, 51.0, 255.0),
                 'righteye': (204.0, 0.0, 204.0),
                 'leftbrow': (0.0, 255.0, 255.0),
                 'rightbrow': (255.0, 204.0, 204.0),
                 'upperlip': (255.0, 255.0, 0.0),
                 'lowerlip': (0.0, 0.0, 153.0),
                 'hair': (0.0, 0.0, 204.0)}


def _parse_annotation_file(filename):
  """
  Parse annotation file
  :param filename:  Path to file
  :return:  str: Segment name
            list: List of index
  """
  with open(filename, 'rt') as f:
    # Get segment name: <text>: <segment_name>
    line = f.readline().strip()
    seg_name = line.split(':')[1].lower().strip()
    index = []
    for line in f:
      line = line.strip()
      if line:
        index.append(int(line))
    return seg_name, index


def load_annotation(folder):
  """
  Load manual annotation
  :param folder:  Location were annotation are stored
  :return:  dict: {segment_name, list index}
            int:  Largest index
  """
  segments = {}

  ann_files = search_folder(folder, ext=['.txt'], relative=False)
  for path in ann_files:
    # Load annotation
    seg_name, index = _parse_annotation_file(path)
    segments[seg_name] = index

  # Vertices on the border of each segment is allocated to both segment.
  # Therefore it needs to be changed. A vertex can only belong to a single
  # segment/region
  max_index = 0
  skin_segment = set(segments['skin'])
  for name, index in segments.items():
    # Avoid self intersection
    if name != 'skin':
      inter = set(index).intersection(skin_segment)
      skin_segment -= inter
      max_index += len(index)
  segments['skin'] = list(skin_segment)

  # Upper / Lower lip also share some common boundaries therefore do the same
  up_lip = set(segments['upperlip'])
  inter = set(segments['lowerlip']).intersection(up_lip)
  up_lip -= inter
  segments['upperlip'] = list(up_lip)
  # Number of points
  max_index = sum([len(v) for v in segments.values()])
  return segments, max_index


def process(pargs):
  """
  Combine annotations
  :param pargs: Parse arguments from command line
  """
  # Post-prcocess annotations
  annotations, n_index = load_annotation(pargs.annotation_folder)

  # Define attributes mask
  semantic_attr = np.zeros(shape=(n_index, 1), dtype=np.int32)
  color_attr = None
  if pargs.mesh:
    color_attr = np.zeros(shape=(n_index, 3), dtype=np.float32)
  for name, index in annotations.items():
    semantic_attr[index, :] = region_labels[name]
    if color_attr is not None:
      color_attr[index, :] = region_colors[name]

  # Save attribute mask
  if not exists(pargs.output):
    mkdir(pargs.output)
  fname = join(pargs.output, 'bfm_semantic_mask.npy')
  logger.info('Save semantic segmentation mask in: {}'.format(fname))
  np.save(fname, semantic_attr)

  # Save mesh
  if color_attr is not None:
    m = Mesh(pargs.mesh)
    m.vertex_color = color_attr
    fname = join(pargs.output, 'semantic_attribute.ply')
    logger.info('Save semantic mesh attributes in: {}'.format(fname))
    m.save(fname)


if __name__ == '__main__':
  p = ArgumentParser()

  # Input
  p.add_argument('--annotation_folder',
                 required=True,
                 type=str,
                 help='Location where annotation file is stored')
  p.add_argument('--output',
                 required=True,
                 type=str,
                 help='Location where to save the data')

  p.add_argument('--mesh',
                 required=False,
                 type=str,
                 help='Corresponding 3D surface to use for visualization')
  # Parse
  args = p.parse_args()
  # Process
  process(pargs=args)
