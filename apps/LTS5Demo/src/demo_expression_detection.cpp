/**
 *  @file   demo_expression_detection.cpp
 *  @brief  Expression detection 2.0
 *
 *  @author Christophe Ecabert
 *  @date   26/05/2016
 *  Copyright (c) 2016 Christophe Ecabert. All rights reserved.
 */

#include <iostream>
#include <string>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/circular_buffer.hpp"
#include "lts5/pointgrey/rig.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/facial_analysis/expression_analysis.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/ogl/glfw_backend.hpp"
#include "lts5/ogl/texture.hpp"
#include "lts5/ogl/image_renderer.hpp"
#include "lts5/ogl/bargraph.hpp"

#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1200


using Color = typename LTS5::Mesh<float>::Color;

struct CustomCallback : LTS5::OGLCallback {

  /** Shutdown flag */
  bool quit_;
  /** Show landmarks */
  bool print_landmark_;

  /**
   * @name  CustomCallback
   * @fn  CustomCallback(void)
   * @brief Constructor
   */
  CustomCallback(void) : quit_(false), print_landmark_(false) {}

  /**
   * @name  OGLKeyboardCb
   * @fn  void OGLKeyboardCb(const OGLKey& key,
   *                                 const OGLKeyState& state)
   * @brief Callback for keyboard event
   * @param[in] key     Key that trigger the callback
   * @param[in] state   Key state at that time
   */
  void OGLKeyboardCb(const LTS5::OGLKey& key, const LTS5::OGLKeyState& state) {
    // Quit
    if ((key == LTS5::OGLKey::kESCAPE) &&
        (state == LTS5::OGLKeyState::kPress)) {
      // Leave
      quit_ = true;
    }
    if ((key == LTS5::OGLKey::kL) &&
        (state == LTS5::OGLKeyState::kPress)) {
      // Leave
      print_landmark_ = !print_landmark_;
    }
  }
};

/**
 * @name  UpdateBarGraph
 * @fn  void UpdateBarGraph(const LTS5::CircularBuffer<long>& buff)
 * @brief Update bargraph value
 * @param[in] buff      Circular buffer holding expression result
 */
void UpdateBarGraph(const LTS5::CircularBuffer<long>& buff);

static LTS5::PGCameraRig* rig_ = nullptr;
static LTS5::SdmTracker* face_tracker_ = nullptr;
static LTS5::ExpressionAnalysis* exp_analysis_ = nullptr;
static CustomCallback callback_;
static LTS5::OGLGlfwBackend* backend_ = nullptr;
static LTS5::OGLTexture* tex_ = nullptr;
static LTS5::OGLImageRenderer* img_renderer_ = nullptr;
static LTS5::OGLBarGraph* bar_graph_ = nullptr;

int main(int argc, const char * argv[]) {

  typedef LTS5::CmdLineParser::ArgState State;
  LTS5::CmdLineParser parser;
  parser.AddArgument("-c",
                     State::kNeeded,
                     "Demo configuration file (.xml)");
  // Process inputs
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Open config file
    std::string config_path;
    parser.HasArgument("-c", &config_path);
    cv::FileStorage storage(config_path, cv::FileStorage::READ);
    if (storage.isOpened()) {
      // Path var
      std::string sdm_model_path;
      std::string f_detector_path;
      std::string expression_model_path;
      std::string logo_path;
      int img_width;
      int img_height;
      int win_width = SCREEN_WIDTH;
      int win_height = SCREEN_HEIGHT;
      int exp_win_size;
      // Go root
      cv::FileNode root = storage.getFirstTopLevelNode();
      LTS5::ReadNode(root, "sdm_model_path", &sdm_model_path);
      LTS5::ReadNode(root, "face_detect_model_path", &f_detector_path);
      LTS5::ReadNode(root, "expression_model_path", &expression_model_path);
      LTS5::ReadNode(root, "image_width", &img_width);
      LTS5::ReadNode(root, "image_height", &img_height);
      LTS5::ReadNode(root, "exp_win_size", &exp_win_size);
      // Open cam
      rig_ = new LTS5::PGCameraRig();
      err = rig_->Init(1, true);
      if (!err) {
        // Analysis tool
        face_tracker_ = new LTS5::SdmTracker();
        err = face_tracker_->Load(sdm_model_path, f_detector_path);
        face_tracker_->set_score_threshold(-1.5);
        if (!err) {
          // Expression analyis
          exp_analysis_ = new LTS5::ExpressionAnalysis();
          err = exp_analysis_->Load(expression_model_path);
          if (!err) {
            // Get label
            std::vector<std::string> expression_label; // = {"Anger", "Disgust", "Fear", "Happy", "Neutral", "Sad", "Surprise"};
            exp_analysis_->get_expression_labels(&expression_label);
            // Init backend, create context
            backend_ = new LTS5::OGLGlfwBackend();
            backend_->Init(argc, argv, false, false, true);
            if (backend_->CreateWindows(win_width,
                                        win_height,
                                        true,
                                        "Expression Detection Demo")) {
              // Init some opengl
              backend_->EnableBlending();
              backend_->SetCallbacks(&callback_);
              // Create texture
              cv::Mat dummy(img_height, img_width, CV_8UC1);
              tex_ = new LTS5::OGLTexture(dummy,
                                          GL_LINEAR,
                                          GL_CLAMP_TO_EDGE,
                                          false);
              // Create image renderer
              img_renderer_ =new LTS5::OGLImageRenderer();
              img_renderer_->AddTexture(tex_);
              img_renderer_->SetWindowDimension(win_width, win_height);
              int x = (win_width - img_width) / 2;
              int y = (win_height - img_height) / 2;
              img_renderer_->SetupView(x, y, img_width, img_height);
              err = img_renderer_->BindToOpenGL();
              // Create bargraph
              bar_graph_ = new LTS5::OGLBarGraph();
              bar_graph_->SetWindowDimension(win_width, win_height);
              bar_graph_->SetupView(x + 20,
                                    y + 800,
                                    200,
                                    200,
                                    static_cast<int>(expression_label.size()));
              bar_graph_->SetLabel(expression_label);
              bar_graph_->SetBarOrientation(LTS5::OGLBarGraph::BarOrientationType::kHorizontal);
              bar_graph_->SetColor(Color(77, 179, 230, 255));
              err |= bar_graph_->BindToOpenGL();
              // Create circular buffer
              size_t sz = static_cast<size_t>(2 * exp_win_size);
              LTS5::CircularBuffer<long> buffer(sz);
              // Start capture
              err |= rig_->Start();
              if (!err) {
                // Can process
                std::vector<cv::Mat> img;
                cv::Mat shape;
                std::vector<double> exp_score;
                int err_track;
                while (!callback_.quit_) {
                  // Read
                  if (!rig_->Read(true, &img)) {
                    // Clear opengl
                    backend_->ClearBuffer();
                    // Process
                    // ----------------------------------------------------
                    // Tracker
                    err_track = face_tracker_->Track(img[0], &shape);
                    if(!err_track && !shape.empty()) {
                      // Expression detection
                      exp_analysis_->Predict(img[0], shape, &exp_score);
                      // Get max index
                      long idx = std::distance(exp_score.begin(),
                                               std::max_element(exp_score.begin(),
                                                                exp_score.end()));
                      buffer.push_back(idx);
                      if (buffer.size() > exp_win_size) {
                        buffer.pop_back();
                      }
                      UpdateBarGraph(buffer);

                      if (callback_.print_landmark_) {
                        int n_pts = shape.rows / 2;
                        for (int i = 0; i < n_pts; ++i) {
                          cv::circle(img[0],
                                     cv::Point(shape.at<double>(i),
                                               shape.at<double>(i + n_pts)),
                                     1,
                                     cv::Scalar(255),
                                     -1);
                        }
                      }
                    }
                    // Display
                    // ----------------------------------------------------
                    // Upload
                    int ret = tex_->Upload(img[0]);
                    // Render
                    img_renderer_->Render();
                    // Bargraph
                    bar_graph_->Render();
                    // Swap
                    backend_->SwapBuffer();
                    // Poll
                    backend_->PollEvents();
                  }
                }
              } else {
                std::cout << "Error while initializing OpenGL" << std::endl;
              }
              // Stop image feed
              rig_->Stop();
              // Clean up
              delete img_renderer_;
              img_renderer_ = nullptr;
              delete tex_;
              tex_ = nullptr;
              delete bar_graph_;
              bar_graph_ = nullptr;
            } else {
              err = -1;
              std::cout << "Error while creating OpenGL Window" << std::endl;
            }
            delete backend_;
            backend_ = nullptr;
          } else {
            std::cout << "Error : Can not load expression detector" << std::endl;
          }
          delete exp_analysis_;
          exp_analysis_ = nullptr;
        } else {
          std::cout << "Error : Can not load face tracker" << std::endl;
        }
        // Clean up
        delete face_tracker_;
        face_tracker_ = nullptr;
      } else {
        std::cout << "Error : Can not initialize camera" << std::endl;
      }
      delete rig_;
      rig_ = nullptr;
    } else {
      err = -1;
      std::cout << "Unable to open configuration file" << std::endl;
    }
  } else {
    std::cout << "Unable to parse command line" << std::endl;
  }
  return err;
}

/**
 * @name  UpdateBarGraph
 * @fn  void UpdateBarGraph(const LTS5::CircularBuffer<long>& buff)
 * @brief Update bargraph value
 * @param[in] buff      Circular buffer holding expression result
 */
void UpdateBarGraph(const LTS5::CircularBuffer<long>& buff) {
  std::vector<int> exp_count(7);
  // Count expression occurence inside the window
  auto it = buff.begin();
  auto last = buff.end();
  for (; it != last ; ++it) {
    exp_count[*it]++;
  }
  // normalize + update graph
  std::vector<float> exp_val;
  float sum = static_cast<float>(std::accumulate(exp_count.begin(),
                                                 exp_count.end(),
                                                 0));
  for (int i = 0; i < exp_count.size(); ++i) {
    if (exp_count[i] > 0) {
      float v = exp_count[i] / sum;
      exp_val.push_back(v);
    } else {
      exp_val.push_back(0.f);
    }
  }
  // Update
  bar_graph_->SetValue(exp_val);
}
