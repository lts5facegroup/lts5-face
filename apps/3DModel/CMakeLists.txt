CMAKE_MINIMUM_REQUIRED(VERSION 2.8 FATAL_ERROR)

### ---[ Define build type
SET(CMAKE_CONFIGURATION_TYPES "Debug;Release" CACHE STRING "possible configurations" FORCE)
IF(NOT CMAKE_BUILD_TYPE)
SET(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING
"Choose the type of build. Options are: None, Debug, Release, RelWithDebInfo, MinSizeRel"
FORCE)
ENDIF(NOT CMAKE_BUILD_TYPE)
### *************************************************
### ---[ PROJECT
### *************************************************
PROJECT(AlignMesh)

INCLUDE_DIRECTORIES(".")
### ---[ Find universal dependencies
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/" ${CMAKE_MODULE_PATH})
### ---[ Define output folder
SET(LIBRARY_OUTPUT_PATH ${AlignMesh_BINARY_DIR}/lib CACHE PATH "Output directory for the libraries")
SET(EXECUTABLE_OUTPUT_PATH ${AlignMesh_BINARY_DIR}/bin CACHE PATH "Output directory for the executables")

set(NANOGUI_BUILD_EXAMPLE OFF CACHE BOOL "Build nanogui examples")
set(NANOGUI_BUILD_SHARED OFF CACHE BOOL "Build nanogui shared lib")
set(NANOGUI_BUILD_PYTHON OFF CACHE BOOL "Build nanogui python lib")
set(NANOGUI_INSTALL OFF CACHE BOOL "Install nanogui lib")
ADD_SUBDIRECTORY(${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/nanogui ${AlignMesh_BINARY_DIR}/nanogui)

# Find OpenCV library
FIND_PACKAGE(OpenCV REQUIRED)
#Find LTS5 library
FIND_PACKAGE(LTS5 REQUIRED)

#FLAG
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(align_srcs
    src/rigid_alignment.cpp
    src/nonrigid_alignment.cpp)

set(check_srcs
  src/app_view.cpp
  src/app_model.cpp
)

if (APPLE)
  find_library(cocoa_library Cocoa)
  list(APPEND check_srcs src/dir_select.mm)
endif()

## Add executable if needed
ADD_EXECUTABLE(AlignMesh align_mesh.cpp ${align_srcs})
TARGET_INCLUDE_DIRECTORIES(AlignMesh
        PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)
TARGET_LINK_LIBRARIES(AlignMesh
        ${OpenCV_LIBRARIES}
        LTS5::lts5_utils
        LTS5::lts5_face_tracker
        LTS5::lts5_geometry
        LTS5::lts5_ogl
        LTS5::lts5_optimization)

add_definitions(${NANOGUI_EXTRA_DEFS})
include_directories(
  ${NANOGUI_EIGEN_INCLUDE_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/nanogui/ext/glfw/include
  ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/nanogui/ext/nanovg/src
  ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/nanogui/include
  ${NANOGUI_EXTRA_INCS}
  ${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/fast-cpp-csv-parser
)

#GLEW
IF(NOT ${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    FIND_PACKAGE(GLEW)
    INCLUDE_DIRECTORIES(${GLEW_INCLUDE_DIRS})
ENDIF(NOT ${CMAKE_SYSTEM_NAME} MATCHES "Darwin")

# DEBUG PURPOSE
#message("Extra-libs: ${NANOGUI_EXTRA_LIBS}")

ADD_EXECUTABLE(CheckReconstructions check_reconstructions.cpp ${check_srcs})
TARGET_INCLUDE_DIRECTORIES(CheckReconstructions
        PUBLIC
          $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)
TARGET_LINK_LIBRARIES(CheckReconstructions
        ${OpenCV_LIBS}
        nanogui
        ${GLEW_LIBRARIES}
        ${NANOGUI_EXTRA_LIBS}
        LTS5::lts5_utils
        LTS5::lts5_ogl)
