/**
 *  @file   check_reconstructions.cpp
 *  @brief
 *
 *  @author Gabriel Cuendet
 *  @date   23.10.16
 *  Copyright © 2016 Cuendet Gabriel. All rights reserved.
 */

#include <iostream>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/string_util.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/ogl/renderer.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/ogl/glfw_backend.hpp"

#include "nanogui/nanogui.h"

#include "app_view.hpp"
#include "app_model.hpp"


int main(int argc, const char * argv[]) {
  std::string mesh_path;
  std::string mesh_dir;
  std::string records_dir;
  std::string temp_dir;
  std::string csv_path;

  using CmdState = LTS5::CmdLineParser::ArgState;
  LTS5::CmdLineParser parser;
  parser.AddArgument("-r", CmdState::kNeeded, "recordings (.bin files) directory");
  parser.AddArgument("-d", CmdState::kOptional, "reconstruction meshes (.ply) directory");
  parser.AddArgument("-m", CmdState::kOptional, "mesh filename");
  parser.AddArgument("-t", CmdState::kNeeded, "temp dir path");
  parser.AddArgument("-o", CmdState::kNeeded, "output CSV file path");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    parser.HasArgument("-r", &records_dir);
    parser.HasArgument("-m", &mesh_path);
    parser.HasArgument("-d", &mesh_dir);
    parser.HasArgument("-t", &temp_dir);
    parser.HasArgument("-o", &csv_path);

    AppModel* app_model = new AppModel(records_dir, csv_path);
    if (!mesh_path.empty())
      app_model->LoadMesh(mesh_path);
    if (!mesh_dir.empty())
      app_model->set_reconstructions_dir(mesh_dir);

    nanogui::init();
    AppView* app_view = new AppView(temp_dir, app_model);
    // deleted when nanogui shuts down

    err = app_view->Run();

    delete app_model;
  } else {
    std::cout << "Incomplete arguments!" << std::endl;
  }

  return err;
}
