function [ Mat ] = ReadBinToMat( filename,dataType )
%READBINTOMAT Summary of this function goes here
%   load matrix stored in filename into Mat

fileId = fopen(filename,'rb');
if fileId ~= -1
    %ok
    %Read type and dimension
    aType = fread(fileId,1,'int32');
    aRow = fread(fileId,1,'int32');
    aCol = fread(fileId,1,'int32');
    
    %Data
    data = fread(fileId,aRow*aCol,dataType);
    
    if (strcmp(dataType, 'float') == 0) 
        aData = single(data);
    else
        aData = data;
    end
    %Reshape
    Mat = reshape(aData,[aCol aRow])';
    
    %Close
    fclose(fileId);
else
   error(['Can not open file : ' filename]); 
end

end

