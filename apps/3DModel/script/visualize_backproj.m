close all; clear all;

root = '/Users/gabrielcuendet/Documents/professionnel/EPFL/source/cpp/lts5-face/data/';
mesh = ReadBinToMat([root 'vertex_mat.bin'], 'double');
landmarks_idx = ReadBinToMat([root 'idx_mat.bin'], 'int32') + 1;
rays = ReadBinToMat([root 'rays.bin'],'float');
all_intersections = ReadBinToMat([root 'intersections.bin'],'double');

missing_landmarks = find(landmarks_idx==0);
disp(['Missing landmarks: '  mat2str(missing_landmarks)]);

landmarks_idx(missing_landmarks) = [];

mesh = reshape(mesh, [3, size(mesh, 1) / 3]);
all_intersections = reshape(all_intersections, [3, size(all_intersections, 1) / 3]);

line_v = []
for k = 1:size(rays, 2)
    alpha = 120.0; % Length of the line
    line_v = [line_v zeros(3,1) alpha(1) * rays(1:3, k)];
end

plot3(mesh(1,1:10:end), mesh(2,1:10:end), mesh(3,1:10:end), '.');
hold all;

plot3(mesh(1, landmarks_idx), mesh(2, landmarks_idx), mesh(3, landmarks_idx), 'x', 'MarkerSize', 10);
axis equal;

plot3(all_intersections(1,:), all_intersections(2,:), all_intersections(3,:), 'o', 'MarkerSize', 10);

line(line_v(1,:),line_v(2,:),line_v(3,:), 'color', 'r');
%hold off;
