clear all; close all;

root = '/Users/gabrielcuendet/Documents/professionnel/EPFL/source/cpp/lts5-face/data/';
target_mesh = ReadBinToMat([root 'target_mesh.bin'], 'double');
target_mesh = reshape(target_mesh, [3, size(target_mesh, 1) / 3]);

aligned_mesh = ReadBinToMat([root 'aligned_mesh.bin'], 'double');
aligned_mesh = reshape(aligned_mesh, [3, size(aligned_mesh, 1) / 3]);

target_landmarks_idx = ReadBinToMat([root 'target_landmarks.bin'], 'int32') + 1;

weights = ReadBinToMat([root 'weights.bin'], 'double');

source_landmarks_idx = ReadBinToMat([root 'source_landmarks.bin'], 'int32') + 1;

missing_landmarks = find(target_landmarks_idx==0);
disp(['Missing landmarks on target: '  mat2str(missing_landmarks)]);

% Plot target mesh and landmarks
figure(1);
plot3(target_mesh(1,1:5:end), target_mesh(2,1:5:end), target_mesh(3,1:5:end), '.');
hold all;

valid_landmarks = setdiff([1:68],missing_landmarks)
plot3(target_mesh(1,target_landmarks_idx(valid_landmarks)), target_mesh(2, target_landmarks_idx(valid_landmarks)), target_mesh(3, target_landmarks_idx(valid_landmarks)), 'x', 'MarkerSize', 10);
axis equal;


% Plot source mesh and landmarks
figure(2);
plot3(aligned_mesh(1,1:end), aligned_mesh(2,1:end), aligned_mesh(3,1:end), '.', 'Color', 'r');
hold all;

plot3(aligned_mesh(1,source_landmarks_idx(valid_landmarks)), aligned_mesh(2,source_landmarks_idx(valid_landmarks)), aligned_mesh(3,source_landmarks_idx(valid_landmarks)), 'x', 'MarkerSize', 10, 'Color', 'k');
axis equal;


% Plot source and target landmarks
figure(3)
plot3(target_mesh(1,target_landmarks_idx(valid_landmarks)), target_mesh(2, target_landmarks_idx(valid_landmarks)), target_mesh(3, target_landmarks_idx(valid_landmarks)), 'x', 'MarkerSize', 10);
hold all;
plot3(aligned_mesh(1,source_landmarks_idx(valid_landmarks)), aligned_mesh(2,source_landmarks_idx(valid_landmarks)), aligned_mesh(3,source_landmarks_idx(valid_landmarks)), 'x', 'MarkerSize', 10, 'Color', 'k');
