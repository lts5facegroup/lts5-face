/**
 *  @file   example_mpu.cpp
 *  @brief
 *
 *  @author Gabriel Cuendet
 *  @date   14.12.16
 *  Copyright © 2016 Cuendet Gabriel. All rights reserved.
 */

#include <stdio.h>
#include <string>
#include <memory>

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/ogl/wired_renderer.hpp"
#include "lts5/ogl/renderer.hpp"
#include "lts5/ogl/glfw_backend.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/geometry/aabb_tree.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/mesh_processor.hpp"
#include "lts5/geometry/multiscale_partition_unity.hpp"
#include "lts5/geometry/marching_cube.hpp"

static LTS5::OGLGlfwBackend* ogl_backend;

struct App : public LTS5::OGLCallback {

  std::vector<LTS5::OGLShader*> shaders_;
  LTS5::OGLProgram* program_;
  LTS5::OGLCamera* cam_;
  LTS5::Mesh<float>* mesh_;
  LTS5::OGLRenderer<float>* renderer_;
  LTS5::AABBTree<float>* tree_;
  LTS5::OGLWiredRenderer<float>* tree_renderer_;

  App(void) : program_(nullptr),
              cam_(nullptr),
              mesh_(nullptr),
              tree_(nullptr),
              tree_renderer_(nullptr) {
  }

  ~App() {
    // Clean up
    for (int i = 0; i < shaders_.size(); ++i) {
      delete shaders_[i];
    }
    delete program_;
    delete cam_;
    delete mesh_;
    delete renderer_;
    //delete tree_;
    delete tree_renderer_;

  }

  void Init(LTS5::Mesh<float>* mesh, LTS5::AABBTree<float>* tree) {
    // Create shader + program
    std::string vertex_shader_str = "#version 330\n"
        "layout (location = 0) in vec3 position;\n"
        "layout (location = 1) in vec3 normal;\n"
        "layout (location = 2) in vec4 vertex_color;\n"
        "uniform mat4 camera;\n"
        "out vec3 normal0;\n"
        "out vec4 color0;\n"
        "void main() {\n"
        "  gl_Position = camera * vec4(position, 1);\n"
        "  normal0 = normal;\n"
        "  color0 = vertex_color;\n"
        "}";
    std::string frag_shader_str = "#version 330\n"
        "in vec3 normal0;\n"
        "in vec4 color0;\n"
        "out vec4 frag_color;\n"
        "void main() {\n"
        "  vec3 light_dir = vec3(0.f, 0.f, -1.f);\n"
        "  float cos_theta = clamp( dot(normal0, light_dir), 0.f, 1.f);\n"
        "  float out_color = 0.2126*color0.b + 0.7152*color0.g + 0.0722*color0.r;\n"
        "  frag_color = vec4(vec3(color0.r, color0.g, color0.b) * cos_theta, 1.f);\n"
        "}";
    shaders_.push_back(new LTS5::OGLShader(vertex_shader_str, GL_VERTEX_SHADER));
    shaders_.push_back(new LTS5::OGLShader(frag_shader_str, GL_FRAGMENT_SHADER));
    // Create program
    std::vector<LTS5::OGLProgram::Attributes> attrib_list;
    attrib_list.push_back(
        LTS5::OGLProgram::Attributes(LTS5::OGLProgram::AttributeType ::kPoint,
                                     0,
                                     "position"));
    attrib_list.push_back(
        LTS5::OGLProgram::Attributes(LTS5::OGLProgram::AttributeType::kNormal,
                                     1,
                                     "normal"));
    attrib_list.push_back(
        LTS5::OGLProgram::Attributes(LTS5::OGLProgram::AttributeType::kColor,
                                     2,
                                     "vertex_color"));
    program_ = new LTS5::OGLProgram(shaders_, attrib_list);
    // Create mesh
    mesh_ = new LTS5::Mesh<float>();
    mesh->Clone(mesh_);
    LTS5::MeshProcessor<float>::NormalizeMesh(mesh_);
    mesh_->BuildConnectivity();
    mesh_->ComputeVertexNormal();

    // Create Cam
    cam_ = new LTS5::OGLCamera(1280, 1024);
    const auto& center = mesh_->bbox().center_;
    cam_->LookAt(glm::vec3(center.x_, center.y_, center.z_ + 1.3f),
                 glm::vec3(center.x_, center.y_, center.z_));

    // Create renderer
    renderer_ = new LTS5::OGLRenderer<float>();
    renderer_->AddMesh(mesh_);
    renderer_->AddProgram(program_);
    int err = renderer_->BindToOpenGL();
    // Create tree renderer
    tree_renderer_ = new LTS5::OGLWiredRenderer<float>();

    if (!err) {
      program_->Use();
      program_->SetUniform("camera", cam_->projection() * cam_->view(true));
      program_->StopUsing();

      // Create AABB_tree
      //tree_ = new LTS5::AABBTree<float>();
      //tree_->Insert(*mesh_);
      //tree_->Build();
      tree_ = tree;
      // Populate wired renderer
      LTS5::OGLWiredRenderer<float>::FillFromAABBTree(*tree_,
                                                      8,
                                                      tree_renderer_);
    }
  }

  void Run(void) {
    ogl_backend->Run(this);
  }

  void OGLKeyboardCb(const LTS5::OGLKey& key, const LTS5::OGLKeyState& state) {
    // Quit
    if ((key == LTS5::OGLKey::kESCAPE) &&
        (state == LTS5::OGLKeyState::kPress)) {
      // Leave
      ogl_backend->LeaveMainLoop();
    }
    // Cam displacement
    cam_->OnKeyboard(key, state);
  }

  void OGLRenderCb(void) {
    // draw one frame
    program_->Use();
    program_->SetUniform("camera", cam_->matrix());
    program_->StopUsing();

    glClearColor(0, 0, 0, 1); // black
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    renderer_->Render();
    tree_renderer_->Render(*cam_, glm::mat4());
  }

  void OGLPassiveMouseCb(const int x, const int y) {
    cam_->OnMouseMove(x, y);
  }

  void OGLMouseCb(const LTS5::OGLMouse& button,
                  const LTS5::OGLKeyState& state,
                  const int x,
                  const int y) {
    cam_->OnMouseClick(button, state, x, y);
  }
};

int main(int argc, const char * argv[]) {
  // -t /home/gabriel/src/cpp/lts5-face/modules/geometry/test/sine_xy_plane.ply
  // -t /home/gabriel/src/cpp/lts5-face/apps/3DModel/resource/target_mesh_simple_clean.ply
  LTS5::CmdLineParser parser;
  parser.AddArgument("-t",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "target mesh path");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    std::string target_path;
    parser.HasArgument("-t", &target_path);

    // Instantiate the target mesh, normalize it and compute the normals
    std::unique_ptr<LTS5::Mesh<float> > target_mesh(new LTS5::Mesh<float>(target_path));
    LTS5::MeshProcessor<float>::FixTriangleSoup(std::numeric_limits<float>::epsilon(),
                                                target_mesh.get());
    // CAUTION: Need to reverse triangle order (back culling problem)
    for (auto& tri : target_mesh->get_triangle()) {
      std::swap(tri.x_, tri.y_);
    }
    cv::Mat target_transform = cv::Mat::eye(4, 4, CV_32FC1);
    target_transform.at<float>(1,1) = -1.0;
    target_transform.at<float>(2,2) = -1.0;
    LTS5::MeshProcessor<float>::Transform(target_transform, target_mesh.get());

    // DEBUG PURPOSE
    LTS5::MeshProcessor<float>::NormalizeMesh(target_mesh.get());
    target_mesh->BuildConnectivity();
    target_mesh->ComputeVertexNormal();
    target_mesh->Save("output/target_fixed_soup.ply");

    LTS5::AABC<float> domain = target_mesh->bcube();

    // DEBUG PURPOSE
    std::cout << "bcube = " << domain << std::endl;

    // Instantiate the multilevel partition of unity
    double max_error = 1e-4;
    int max_depth = 4;
    double alpha = 0.75;
    std::unique_ptr<LTS5::MPU<float> > mpu(new LTS5::MPU<float>(max_error, max_depth, alpha));
    mpu->set_n_min(500);

    mpu->DisableQuadricApproximant();
//    mpu->set_signed_distance(false);

    mpu->Build(*target_mesh, domain);
    mpu->Save("output/mpu_example.bin");

    /// Marching cubes
    int max_mc_level = 6; // >6 it becomes extremely slow!
    std::unique_ptr<LTS5::MarchingCube<float> > march_cube(new LTS5::MarchingCube<float>(max_mc_level));
    std::unique_ptr<LTS5::Mesh<float> > result(new LTS5::Mesh<float>());
    march_cube->Init(*mpu);
    march_cube->ComputeMesh(result.get());
    result->set_vertex_color(std::vector<LTS5::Vector4<unsigned char> >(result->size(), LTS5::Vector4<unsigned char>(255,155,255,255)));
    result->Save("output/marching_cube_ex.ply");

    LTS5::AABBTree<float>* mpu_aab_tree(nullptr);
/*    mpu_aab_tree = &mpu->get_tree();
    App* app = new App();
    ogl_backend = new LTS5::OGLGlfwBackend();

    try {

      ogl_backend->Init(argc, argv, true, false, false);
      if (!ogl_backend->CreateWindows(1280,
                                      1024,
                                      false,
                                      "AABB Tree")) {
        ogl_backend->Terminate();
      } else {
        app->Init(result.get(), mpu_aab_tree);
        app->Run();
      }
    } catch (const std::exception& e){
      std::cerr << "ERROR: " << e.what() << std::endl;
      delete app;
      ogl_backend->Terminate();
      return -1;
    }

    delete app;
    delete ogl_backend;
 */



  } else {
    std::cout << "Unable to parse cmd line" << std::endl;
  }

  return err;
}
