/**
 *  @file   test_shape_model
 *  @brief  Test class for shape model
 *
 *  @author Gabriel Cuendet
 *  @date   2016-12-05
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Eigen/Core"
#include "lts5/geometry/mesh_processor.hpp"
#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/math/vector.hpp"

#include "shape_transformation_model.hpp"

std::string mesh_path;

class ShapeModelTest : public ::testing::Test {
 public:

  ShapeModelTest () {

  }

 protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

};

/** Define unit test, RefShape */
TEST_F(ShapeModelTest, RefShape) {
  LTS5::Mesh<double> ref_mesh(mesh_path);
  ref_mesh.ComputeHalfedges();
  ShapeModel<double> shape_model(ref_mesh);
  shape_model.ComputeBasis(0.0, 15);

  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> shape;
  shape_model.GetShape(&shape);

  const std::vector<LTS5::Vector3<double> >& ref_vertices = ref_mesh.get_vertex();
  LTS5::Vector3<double>* begin = reinterpret_cast<LTS5::Vector3<double>*>(shape.data());
  std::vector<LTS5::Vector3<double> > mod_vertices(begin, begin + shape.cols());

  EXPECT_TRUE(mod_vertices == ref_vertices);
}

/** Define unit test, Plate01 */
TEST_F(ShapeModelTest, Plate01) {
  LTS5::Mesh<double> ref_mesh("resource/mesh_ref.obj");
  ref_mesh.ComputeHalfedges();
  ShapeModel<double> shape_model(ref_mesh);
  shape_model.ComputeBasis(0.0, 15);

  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>& coeffs = shape_model.get_coeffs();
  coeffs(2, 6) = 0.3;
  shape_model.SaveCurrentMesh("output/mesh_mode_1_reconstruct.ply");

}

/** Define unit test, ManualCoeff  ENABLE to tune manually the coeffs used for
 * the reconstruction */
TEST_F(ShapeModelTest, ManualCoeff) {
  LTS5::Mesh<double> ref_mesh(mesh_path);
  LTS5::MeshProcessor<double>::NormalizeMesh(&ref_mesh);

  ref_mesh.ComputeHalfedges();
  ShapeModel<double> shape_model(ref_mesh);
  shape_model.ComputeBasis(0.0, 200);

  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>& coeffs = shape_model.get_coeffs();
  coeffs(0, 49) = 0.01;
  coeffs(1, 49) = 0.01;
  coeffs(2, 49) = 0.01;

  shape_model.SaveCurrentMesh("output/test_current_mesh.ply");
}

int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("-m",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Path to the reference mesh");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    parser.HasArgument("-m", &mesh_path);

    // Init test
    ::testing::GTEST_FLAG(filter) = "ShapeModelTest.ManualCoeff";
    ::testing::InitGoogleTest(&argc, const_cast<char **>(argv));
    // Run test
    return RUN_ALL_TESTS();
  } else {
    std::cout << "ERROR: Cannot parse the cmd line!" << std::endl;
  }
  return -1;
}
