/**
 *  @file   test_rigid_alignment
 *  @brief  Test class for rigid alignment methods
 *
 *  @author Gabriel Cuendet
 *  @date   2016-10-04
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <limits>
#include <iostream>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "opencv2/core.hpp"

#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/mesh_processor.hpp"
#include "lts5/utils/cmd_parser.hpp"
#include "rigid_alignment.hpp"

std::string mesh_path;
std::string landmarks_path;

class RigidAlignTest : public ::testing::Test {
 public:
  RigidAlignTest() : source_(mesh_path), target_(mesh_path) {
    LTS5::MeshProcessor<double>::NormalizeMesh(&source_);
    LTS5::MeshProcessor<double>::NormalizeMesh(&target_);
  }

 protected:

  /**
   * @name
   * @fn
   * @brief
   */
  void SetUp(void) {
    // code here will execute just before the test ensues
  }

  /**
   * @name
   * @fn
   * @brief
   */
  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

  LTS5::Mesh<double> source_;
  LTS5::Mesh<double> target_;

};

/** Define unit test, RigidTransScale */
TEST_F(RigidAlignTest, RigidTransScale) {
  double scale = 1.25;
  LTS5::MeshProcessor<double>::Scale(scale, &target_);

  //DEBUG PURPOSE
  std::cout << source_.Cog() << std::endl;
  std::cout << target_.Cog() << std::endl;

  AlignConfig config;
  RigidAlignment<double> align(target_, &source_, config);
  align.LoadSrcLandmarks(landmarks_path);
  align.set_trg_landmarks(align.src_landmarks());
  align.ComputeAlignment();

  EXPECT_NEAR(align.Error(), 0.0, std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Scale(), scale,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Translation().x_, 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().y_, 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().z_, 0.0,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR((align.Rotation()).at<double>(0,0), 1.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,1), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,2), 0.0,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(1,0), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,1), 1.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,2), 0.0,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(2,0), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,1), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,2), 1.0,
              std::numeric_limits<float>::epsilon());

}

/** Define unit test, RigidTransTranslation */
TEST_F(RigidAlignTest, RigidTransTranslation) {
  LTS5::Vector3<double> translation(1.25,-0.1,-0.25);
  LTS5::MeshProcessor<double>::Translate(translation, &target_);

  std::cout << source_.Cog() << std::endl;
  std::cout << target_.Cog() << std::endl;

  AlignConfig config;
  RigidAlignment<double> align(target_, &source_, config);
  align.LoadSrcLandmarks(landmarks_path);
  align.set_trg_landmarks(align.src_landmarks());
  align.ComputeAlignment();

  EXPECT_NEAR(align.Error(), 0.0, std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Scale(), 1.0,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Translation().x_, translation.x_,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().y_, translation.y_,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().z_, translation.z_,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(0,0), 1.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,1), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,2), 0.0,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(1,0), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,1), 1.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,2), 0.0,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(2,0), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,1), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,2), 1.0,
              std::numeric_limits<float>::epsilon());

}

/** Define unit test, RigidTransTransScale */
TEST_F(RigidAlignTest, RigidTransTransScale) {
  LTS5::Vector3<double> translation(1.25,-0.1,-0.25);
  double scale = 1.25;
  LTS5::MeshProcessor<double>::Translate(translation, &target_);
  LTS5::MeshProcessor<double>::Scale(scale, &target_);

  std::cout << source_.Cog() << std::endl;
  std::cout << target_.Cog() << std::endl;

  AlignConfig config;
  RigidAlignment<double> align(target_, &source_, config);
  align.LoadSrcLandmarks(landmarks_path);
  align.set_trg_landmarks(align.src_landmarks());
  align.ComputeAlignment();

  EXPECT_NEAR(align.Error(), 0.0, std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Scale(), scale,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Translation().x_, translation.x_,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().y_, translation.y_,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().z_, translation.z_,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(0,0), 1.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,1), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,2), 0.0,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(1,0), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,1), 1.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,2), 0.0,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(2,0), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,1), 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,2), 1.0,
              std::numeric_limits<float>::epsilon());
  
}

/** Define unit test, RigidTransRotation */
TEST_F(RigidAlignTest, RigidTransRotation) {
  cv::Mat rot = cv::Mat::eye(4, 4, CV_64F);
  rot.at<double>(0, 0) = std::cos(-M_PI/6.0);
  rot.at<double>(0, 1) = -std::sin(-M_PI/6.0);
  rot.at<double>(1, 0) = std::sin(-M_PI/6.0);
  rot.at<double>(1, 1) = std::cos(-M_PI/6.0);
  LTS5::MeshProcessor<double>::Transform(rot, &target_);

  std::cout << source_.Cog() << std::endl;
  std::cout << target_.Cog() << std::endl;

  AlignConfig config;
  RigidAlignment<double> align(target_, &source_, config);
  align.LoadSrcLandmarks(landmarks_path);
  align.set_trg_landmarks(align.src_landmarks());
  align.ComputeAlignment();

  EXPECT_NEAR(align.Error(), 0.0, std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Scale(), 1.0,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Translation().x_, 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().y_, 0.0,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().z_, 0.0,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(0,0), rot.at<double>(0, 0),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,1), rot.at<double>(0, 1),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,2), rot.at<double>(0, 2),
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(1,0), rot.at<double>(1, 0),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,1), rot.at<double>(1, 1),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,2), rot.at<double>(1, 2),
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(2,0), rot.at<double>(2, 0),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,1), rot.at<double>(2, 1),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,2), rot.at<double>(2, 2),
              std::numeric_limits<float>::epsilon());

}

/** Define unit test, RigidTransTransRot */
TEST_F(RigidAlignTest, RigidTransTransRot) {
  cv::Mat rot = cv::Mat::eye(4, 4, CV_64F);
  rot.at<double>(0, 0) = std::cos(-M_PI/6.0);
  rot.at<double>(0, 1) = -std::sin(-M_PI/6.0);
  rot.at<double>(1, 0) = std::sin(-M_PI/6.0);
  rot.at<double>(1, 1) = std::cos(-M_PI/6.0);

  std::cout << "COG (before rot) = " << source_.Cog()<< std::endl;
  LTS5::MeshProcessor<double>::Transform(rot, &target_);
  std::cout << "COG (after rot) = " << source_.Cog()<< std::endl;

  LTS5::Vector3<double> translation(0.15,-0.1,-0.25);
  LTS5::MeshProcessor<double>::Translate(translation,&target_);
  std::cout << "COG (after translate) = " << source_.Cog()<< std::endl;

  AlignConfig config;
  RigidAlignment<double> align(target_, &source_, config);
  align.LoadSrcLandmarks(landmarks_path);
  align.set_trg_landmarks(align.src_landmarks());
  align.ComputeAlignment();

  EXPECT_NEAR(align.Error(), 0.0, std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Scale(), 1.0,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Translation().x_, translation.x_,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().y_, translation.y_,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().z_, translation.z_,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(0,0), rot.at<double>(0, 0),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,1), rot.at<double>(0, 1),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,2), rot.at<double>(0, 2),
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(1,0), rot.at<double>(1, 0),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,1), rot.at<double>(1, 1),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,2), rot.at<double>(1, 2),
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(2,0), rot.at<double>(2, 0),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,1), rot.at<double>(2, 1),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,2), rot.at<double>(2, 2),
              std::numeric_limits<float>::epsilon());
}

/** Define unit test, RigidTransAll */
TEST_F(RigidAlignTest, RigidTransAll) {
  cv::Mat rot = cv::Mat::eye(4, 4, CV_64F);
  rot.at<double>(0, 0) = std::cos(-M_PI/6.0);
  rot.at<double>(0, 1) = -std::sin(-M_PI/6.0);
  rot.at<double>(1, 0) = std::sin(-M_PI/6.0);
  rot.at<double>(1, 1) = std::cos(-M_PI/6.0);
  LTS5::MeshProcessor<double>::Transform(rot, &target_);

  LTS5::Vector3<double> translation(0.15,-0.1,-0.25);
  LTS5::MeshProcessor<double>::Translate(translation, &target_);

  double scale = 1.25;
  LTS5::MeshProcessor<double>::Scale(scale, &target_);

  AlignConfig config;
  RigidAlignment<double> align(target_, &source_, config);
  align.LoadSrcLandmarks(landmarks_path);
  align.set_trg_landmarks(align.src_landmarks());
  align.ComputeAlignment();

  EXPECT_NEAR(align.Error(), 0.0, std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Scale(), scale, std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Translation().x_, translation.x_,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().y_, translation.y_,
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Translation().z_, translation.z_,
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(0,0), rot.at<double>(0, 0),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,1), rot.at<double>(0, 1),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(0,2), rot.at<double>(0, 2),
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(1,0), rot.at<double>(1, 0),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,1), rot.at<double>(1, 1),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(1,2), rot.at<double>(1, 2),
              std::numeric_limits<float>::epsilon());

  EXPECT_NEAR(align.Rotation().at<double>(2,0), rot.at<double>(2, 0),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,1), rot.at<double>(2, 1),
              std::numeric_limits<float>::epsilon());
  EXPECT_NEAR(align.Rotation().at<double>(2,2), rot.at<double>(2, 2),
              std::numeric_limits<float>::epsilon());
}

/** Define unit test, ApplyTransScale */
TEST_F(RigidAlignTest, ApplyTransScale) {
  double scale = 1.25;
  LTS5::MeshProcessor<double>::Scale(scale, &target_);

  AlignConfig config;
  RigidAlignment<double> align(target_, &source_, config);
  align.LoadSrcLandmarks(landmarks_path);
  align.set_trg_landmarks(align.src_landmarks());
  align.AlignSource();

  const std::vector<LTS5::Vector3<double> >& src_v = source_.get_vertex();
  const std::vector<LTS5::Vector3<double> >& trg_v = target_.get_vertex();

  for (int i = 0; i < src_v.size(); ++i) {
    EXPECT_NEAR((src_v[i] - trg_v[i]).Norm(), 0, std::numeric_limits<float>::epsilon());
  }

  EXPECT_NEAR(align.Error(), 0.0, std::numeric_limits<float>::epsilon());
}

/** Define unit test, ApplyTransTranslation */
TEST_F(RigidAlignTest, ApplyTransTranslation) {
  LTS5::Vector3<double> translation(1.25,-0.1,-0.25);
  LTS5::MeshProcessor<double>::Translate(translation, &target_);

  AlignConfig config;
  RigidAlignment<double> align(target_, &source_, config);
  align.LoadSrcLandmarks(landmarks_path);
  align.set_trg_landmarks(align.src_landmarks());
  align.AlignSource();

  const std::vector<LTS5::Vector3<double> >& src_v = source_.get_vertex();
  const std::vector<LTS5::Vector3<double> >& trg_v = target_.get_vertex();

  for (int i = 0; i < src_v.size(); ++i) {
    EXPECT_NEAR((src_v[i] - trg_v[i]).Norm(), 0, std::numeric_limits<float>::epsilon());
  }

  EXPECT_NEAR(align.Error(), 0.0, std::numeric_limits<float>::epsilon());
}

/** Define unit test, ApplyTransRot */
TEST_F(RigidAlignTest, ApplyTransRot) {
  cv::Mat rot = cv::Mat::eye(4, 4, CV_64F);
  rot.at<double>(0, 0) = std::cos(-M_PI/6.0);
  rot.at<double>(0, 1) = -std::sin(-M_PI/6.0);
  rot.at<double>(1, 0) = std::sin(-M_PI/6.0);
  rot.at<double>(1, 1) = std::cos(-M_PI/6.0);
  LTS5::MeshProcessor<double>::Transform(rot, &target_);

  AlignConfig config;
  RigidAlignment<double> align(target_, &source_, config);
  align.LoadSrcLandmarks(landmarks_path);
  align.set_trg_landmarks(align.src_landmarks());
  align.AlignSource();

  const std::vector<LTS5::Vector3<double> >& src_v = source_.get_vertex();
  const std::vector<LTS5::Vector3<double> >& trg_v = target_.get_vertex();

  for (int i = 0; i < src_v.size(); ++i) {
    EXPECT_NEAR((src_v[i] - trg_v[i]).Norm(), 0, std::numeric_limits<float>::epsilon());
  }

  EXPECT_NEAR(align.Error(), 0.0, std::numeric_limits<float>::epsilon());
}

int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser parser;
  parser.AddArgument("-m",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Path to the test mesh");
  parser.AddArgument("-l",
                     LTS5::CmdLineParser::ArgState::kNeeded,
                     "Path to the landmarks annotation file");

  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    parser.HasArgument("-m", &mesh_path);
    parser.HasArgument("-l", &landmarks_path);

    // Init test
    ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));
    // Run test
    return RUN_ALL_TESTS();
  } else {
    std::cout << "Unable to parse cmd line!" << std::endl;
  }

  return err;
}
