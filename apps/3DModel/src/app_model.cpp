/**
 *  @file   AppModel implementation
 *  @brief  Stores and manage data used by the app
 *
 *  @author Gabriel Cuendet
 *  @date   25/10/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <iostream>
#include <memory>
#include <sys/stat.h>
#include <cstring>

#include "app_model.hpp"
#include "csv.h"

const std::vector<std::string> AppModel::expressions{
  "NEUTR_AU00",
  "NEUTR_AU43",
  "MOUTO_AU25",
  "SMILE_AU12",
  "BROWL_AU04",
  "BROWR_AU01",
  "ANGER_EXPR",
  "JAW-L_AU30",
  "JAW-R_AU30",
  "JAWFW_AU29",
  "MOUTL_AUXX",
  "MOUTR_AUXX",
  "DIMPL_AU14",
  "CHINR_AU17",
  "LPUCK_AU18",
  "LFUNN_AU22",
  "SADNE_EXPR",
  "LROLL_AU28",
  "GRIN-_AU13",
  "CBLOW_AU33",
  "ANGE2_EXPR",
  "SURPR_EXPR",
  "SURP2_EXPR",
  "FEAR-_EXPR",
  "DISGU_EXPR",
  "VISEM_oooo",
  "VISEM_ouou",
  "VISEM_aaaa",
  "VISEM_iiii",
  "VISEM_llll",
  "VISEM_mmmm",
  "VISEM_nnnn",
  "VISEM_ffff",
  "VISEM_eeee",
  "LBYTE_AU32",
  "TGOUT_AU19"};

/*
 * @name  AppModel
 * @fn  AppModel()
 * @brief Class constructor
 * @param[in] csv_path  Path to the csv file where to save the review
 */
AppModel::AppModel(const std::string& records_dir, const std::string& csv_path)
    : mesh_(nullptr), current_frame_(-1), total_frames_(-1),
      output_csv_path_(csv_path), recordings_dir_(records_dir), subject_id_(0),
      expression_id_(0) {}

/*
 * @name  LoadMesh
 * @fn  void LoadMesh(const std::string& mesh_path)
 * @brief Load a mesh
 * @param[in]  mesh_path  Path to the mesh to load
 */
int AppModel::LoadMesh(const std::string& mesh_path) {
  if (mesh_) {
    mesh_.reset();
  }

  mesh_ = std::make_shared<Mesh>();
  int err = mesh_->Load(mesh_path);

  if (err) {
    mesh_.reset();
  } else {
    mesh_->NormalizeMesh();
    mesh_->ComputeVertexNormal();
  }

  return err;
}

/*
 * @name  LoadNewSubject
 * @fn  int LoadNewSubject(void)
 * @brief Load a mesh and all infos, from the reconstructions dir, with the
 *        name composed with subject id and expression.
 *        Typically called from GUI
 * @return 0 if success, -1 otherwise
 */
int AppModel::LoadNewSubject(void) {
  std::string filename = mesh_filename();
  int error = LoadMesh(filename);

  current_frame_ = -1;
  total_frames_ = -1;
  review_ = DataReview();
  raw_color_.reset();
  raw_color_ = std::make_shared<RawFile>();
  raw_depth_.reset();
  raw_depth_= std::make_shared<RawFile>();
  unzip_color_.reset();
  kfusion_params_.reset(new KinFuParams());

  if (error) {
    std::cout << "[AppModel::LoadMesh] Warning: " <<
    "invalid path: " << filename << std::endl;
  } else {
    std::cout << "[AppModel::LoadMesh] Mesh loaded from: " <<
    filename << std::endl;

    DataReview* csv_review = nullptr;
    ReadReviewFromCsv(filename, &csv_review);

    // Parse the comments of the ply file
    const std::vector<std::string>& comments = mesh_->get_comments();
    kfusion_params_->DeserializeParameters(comments);

    auto pos_it = std::find_if(comments.begin(), comments.end(),
                               [](const std::string& s) {
                                 return s.compare(0, 16, "raw_depth_file: ") == 0;
                               });
    if (pos_it != comments.end()) {
      this->raw_depth_->file_bin_name_ = pos_it->substr(pos_it->find_last_of("/") + 1);
      this->raw_depth_->file_found_ = false;
    }

    pos_it = std::find_if(comments.begin(), comments.end(),
                          [](const std::string& s) {
                            return s.compare(0, 16, "raw_color_file: ") == 0;
                          });
    if (pos_it != comments.end()) {
      this->raw_color_->file_bin_name_ = pos_it->substr(pos_it->find_last_of("/") + 1);
      this->raw_color_->file_found_ = FindBinaryFiles();
      if (this->raw_color_->file_found_ && !(this->raw_color_->file_compressed_)) {
        // Check the total number fo frames
        std::ifstream ifs(this->raw_color_->path());
        ifs.read((char*)&total_frames_, sizeof(int));
        ifs.close();
      }
    }

    pos_it = std::find_if(comments.begin(), comments.end(),
                          [](const std::string& s) {
                            return s.compare(0, 15, "n_frames_used: ") == 0;
                          });
    if (pos_it != comments.end()) {
      this->review_.last_frame = std::stoi(pos_it->substr(15));
    }

    this->current_frame_ = this->review_.last_frame == -1 ? total_frames_ : this->review_.last_frame;

    if (csv_review) {
      if (this->review_.last_frame > -1) {
        // Apparently, ply file indicates a last frame, and review as well
        std::cout << "[AppModel::LoadNewSubject] Warning: " <<
        "last frame in .ply is " << this->review_.last_frame <<
        " and last frame in .csv is " << csv_review->last_frame << std::endl;
      }
      this->review_ = *csv_review;
    }

    delete csv_review;
  }

  return error;
}

/**
   * @name ResetSubject
   * @fn void ResetSubject(void)
   * @brief Reset parameters linked to a subject in particular
   */
void AppModel::ResetSubject(void) {
  mesh_.reset();
  current_frame_ = -1;
  total_frames_ = -1;
  review_ = DataReview();

  subject_id_ = -1;
  expression_id_ = -1;

  raw_color_.reset();
  raw_depth_.reset();

  kfusion_params_.reset();
}

/*
 * @name ReadReviewFromCsv
 * @fn int ReadReviewFromCsv(const std::string& mesh_path, DataReview* review) const
 * @param[in]  mesh_path  Path to the reconstruction mesh
 * @param[out] review     Review loaded from the csv or nullptr
 * @return 0 if success, -1 otherwise
 */
int AppModel::ReadReviewFromCsv(const std::string& mesh_path,
                                AppModel::DataReview** review) const {
  int error = -1;
  if (*review) {
    delete *review;
    *review = nullptr;
  }

  std::string temp_mesh_name = "";

  io::CSVReader<8>csv_in(output_csv_path_);
  csv_in.read_header(io::ignore_no_column, "depth", "color", "mesh_path",
                     "last_frame", "wrong_subject", "wrong_expression",
                     "quality", "reconstruct");

  bool has_depth= csv_in.has_column("depth");
  bool has_color = csv_in.has_column("color");
  bool has_mesh = csv_in.has_column("mesh_path");
  bool has_frame = csv_in.has_column("last_frame");
  bool has_subj = csv_in.has_column("wrong_subject");
  bool has_expr = csv_in.has_column("wrong_expression");
  bool has_qual = csv_in.has_column("quality");
  bool has_reconst = csv_in.has_column("reconstruct");

  std::string temp_depth, temp_color;
  int reconstruct_int = 0;
  int subj, expr, qual, last_f = 0;

  if (!has_depth || !has_color || !has_mesh || ! has_frame || !has_subj ||
      !has_expr || !has_qual || !has_reconst) {
    std::cerr << "[AppModel::ReadReviewFromCsv] Error: " <<
    "Cannot read csv, headers are missing!" << std::endl;
    return error;
  }

  while (csv_in.read_row(temp_depth, temp_color, temp_mesh_name, last_f, subj,
                         expr, qual, reconstruct_int)) {
    // Only compare the filename, not the whole path!
    if (temp_mesh_name.substr(temp_mesh_name.find_last_of("/") + 1).compare(mesh_path.substr(mesh_path.find_last_of("/") + 1)) == 0) {
      *review = new DataReview(last_f,
                               static_cast<AppModel::AssessValue>(subj),
                               static_cast<AppModel::AssessValue>(expr),
                               static_cast<AppModel::AssessValue>(qual),
                               reconstruct_int == 1);
      error = 0;
      break;
    }
  }

  return error;
}

/*
 * @name WriteReviewToCsv
 * @fn int WriteReviewToCsv(const std::string& mesh_path,
 *                          const DataReview& review) const
 * @return 0 if success, -1 otherwise
 */
int AppModel::WriteReviewToCsv() {
  int error = -1;

  struct csv_row {
    std::string depth;
    std::string color;
    std::string mesh_path;
    DataReview review;

    csv_row(const std::string& dep, const std::string& col, const std::string& path,
    const DataReview& rev) : depth(dep), color(col), mesh_path(path), review(rev) {}
  };

  std::vector<csv_row> csv_content;

  io::CSVReader<8>csv_in(output_csv_path_);
  csv_in.read_header(io::ignore_no_column, "depth", "color", "mesh_path", "last_frame",
                     "wrong_subject", "wrong_expression", "quality",
                     "reconstruct");

  bool has_mesh = csv_in.has_column("mesh_path");
  bool has_frame = csv_in.has_column("last_frame");
  bool has_subj = csv_in.has_column("wrong_subject");
  bool has_expr = csv_in.has_column("wrong_expression");
  bool has_qual = csv_in.has_column("quality");
  bool has_reconst = csv_in.has_column("reconstruct");

  if (!has_mesh || ! has_frame || !has_subj || !has_expr || !has_qual || !has_reconst) {
    std::cerr << "[AppModel::ReadReviewFromCsv] Error: " <<
              "Cannot read csv, headers are missing!" << std::endl;
    return error;
  }

  std::string temp_depth, temp_color, temp_mesh_name = "";
  int reconstruct_int = 0;
  int subj, expr, qual, last_f = 0;
  bool mesh_found = false;

  while (csv_in.read_row(temp_depth, temp_color, temp_mesh_name, last_f, subj,
                         expr, qual,reconstruct_int)) {
    DataReview review(last_f, static_cast<AppModel::AssessValue>(subj),
                      static_cast<AppModel::AssessValue>(expr),
                      static_cast<AppModel::AssessValue>(qual),
                      reconstruct_int == 1);

    if (temp_mesh_name.compare(this->mesh_filename()) == 0) {
      review_.set_reconstruct();
      csv_content.push_back(csv_row(this->raw_depth_->path(), this->raw_color_->path(),
                            this->mesh_filename(), this->review_));
      mesh_found = true;
    } else {
      csv_content.push_back(csv_row(temp_depth, temp_color, temp_mesh_name,
                                    review));
    }
  }

  if (!mesh_found) {
    review_.set_reconstruct();
    csv_content.push_back(csv_row(this->raw_depth_->path(), this->raw_color_->path(),
                                  this->mesh_filename(), this->review_));
  }

  // open file
  std::ofstream csv_file;
  csv_file.open(output_csv_path_);
  if (csv_file.is_open()) {
    error = 0;

    csv_file << "depth,color,mesh_path,last_frame,wrong_subject,wrong_expression,quality,"
        "reconstruct" << std::endl;

    for (int i = 0; i < csv_content.size(); ++i) {
      // write line
      csv_file << csv_content[i].depth
               << "," << csv_content[i].color
               << "," << csv_content[i].mesh_path
               << "," << csv_content[i].review.last_frame
               << "," << static_cast<int>(csv_content[i].review.subject)
               << "," << static_cast<int>(csv_content[i].review.expression)
               << "," << static_cast<int>(csv_content[i].review.quality)
               << "," << csv_content[i].review.reconstruct
               << std::endl;
    }
    // close file
    csv_file.close();
  } else {
    std::cout << "Could not write to file " << output_csv_path_ << std::endl;
  }

  return error;
}

#pragma mark -
#pragma mark Private

/*
 * @name FindBinaryFiles
 * @fn int FindBinaryFiles()
 * @brief Compute full path of the current recording bin files
 * @return true if found, false otherwise
 */
bool AppModel::FindBinaryFiles() {
  // check for recordings_dir_ trailing slash
  if (recordings_dir_.compare(recordings_dir_.size() - 1, 1, "/") != 0) {
    recordings_dir_ += "/";
  }

  std::ostringstream rec_root_dir_stream;
  rec_root_dir_stream << recordings_dir_ << raw_color_->file_bin_name_.substr(0, 10) <<
  "/";

  // Still need to find session dir
  std::vector<std::string> sessions_list = {"session01/", "session02/",
                                            "session03/", "session04/",
                                            "session05/"};
  bool found_session = false;
  int i;
  for (i = 0; i < sessions_list.size(); ++i) {
    std::string root_dir = rec_root_dir_stream.str() + sessions_list[i];
    std::string path =  root_dir + raw_color_->file_bin_name_;
    struct stat buffer;
    if (stat(path.c_str(), &buffer) == 0) {
      found_session = true;

      raw_color_->dir_path_ = root_dir;
      raw_color_->file_found_ = true;
      raw_color_->file_compressed_ = false;

      path = root_dir + raw_depth_->file_bin_name_;
      if (stat(path.c_str(), &buffer) == 0) {
        raw_depth_->dir_path_ = root_dir;
        raw_depth_->file_found_ = true;
        raw_depth_->file_compressed_ = false;
      }
      path = root_dir + raw_depth_->file_bin_name_ + ".gz";
      if (stat(path.c_str(), &buffer) == 0) {
        raw_depth_->dir_path_ = root_dir;
        raw_depth_->file_found_ = true;
        raw_depth_->file_compressed_ = true;
      }

      break;
    }

    path = root_dir + raw_color_->file_bin_name_ + ".gz";
    if (stat(path.c_str(), &buffer) == 0) {
      found_session = true;

      raw_color_->dir_path_ = root_dir;
      raw_color_->file_found_ = true;
      raw_color_->file_compressed_ = true;

      path = root_dir + raw_depth_->file_bin_name_;
      if (stat(path.c_str(), &buffer) == 0) {
        raw_depth_->dir_path_ = root_dir;
        raw_depth_->file_found_ = true;
        raw_depth_->file_compressed_ = false;
      }
      path = root_dir + raw_depth_->file_bin_name_ + ".gz";
      if (stat(path.c_str(), &buffer) == 0) {
        raw_depth_->dir_path_ = root_dir;
        raw_depth_->file_found_ = true;
        raw_depth_->file_compressed_ = true;
      }
      break;
    }
  }

  return found_session;
}
