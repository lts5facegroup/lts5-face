/**
 *  @file   dir_select.cpp
 *  @brief
 *
 *  @author Gabriel Cuendet
 *  @date   26.10.16
 *  Copyright © 2016 Ecabert Christophe. All rights reserved.
 */

#include <Cocoa/Cocoa.h>
#include <string>

std::string dir_dialog() {
  std::string path = "";
  NSOpenPanel *openDlg = [NSOpenPanel openPanel];

  [openDlg setCanChooseFiles:NO];
  [openDlg setCanChooseDirectories:YES];
  [openDlg setAllowsMultipleSelection:NO];

  if ([openDlg runModal] == NSModalResponseOK) {
    for (NSURL* url in [openDlg URLs]) {
      path = std::string((char*) [[url path] UTF8String]);
      break;
    }
  }

  return path;
}
