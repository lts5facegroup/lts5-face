/**
 *  @file   app_view.cpp
 *  @brief  Wrapper for nanogui, View implementation
 *
 *  @author Gabriel Cuendet
 *  @date   25/10/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifdef __APPLE__
#include <OpenGL/gl3.h>
#else
#include <GL/glew.h>
#endif

#include "lts5/ogl/key_types.hpp"
#include <chrono>
#include <sys/stat.h>

#include "app_model.hpp"
#include "app_view.hpp"

std::string dir_dialog();

#if !defined(__APPLE__)
std::string dir_dialog() {
#define FILE_DIALOG_MAX_BUFFER 1024
  char buffer[FILE_DIALOG_MAX_BUFFER];
  std::string cmd = "/usr/bin/zenity --directory";
  FILE *output = popen(cmd.c_str(), "r");
  if (output == nullptr)
    throw std::runtime_error("popen() failed -- could not launch zenity!");
  while (fgets(buffer, FILE_DIALOG_MAX_BUFFER, output) != NULL)
    ;
  pclose(output);
  std::string result(buffer);
  result.erase(std::remove(result.begin(), result.end(), '\n'), result.end());
  return result;
}
#endif

/*
 * @name  AppView()
 * @fn  AppView()
 * @brief Constructor of the class
 * @param[in] temp_dir  Temp dir path
 * @param[in/out] model  AppModel
 */
AppView::AppView(const std::string& temp_dir, AppModel* model)
: nanogui::Screen(Eigen::Vector2i(1280, 1024), "ReconstructionsChecker"),
app_model_(model), n_tri_(0), texture_("frame_texture"), load_image_(false),
  display_image_(false), texture_data_(nullptr, free),
  ready_to_load_(false), loader_is_alive_(false), temp_dir_path_(temp_dir) {

#ifndef __APPLE__
  // GLEW INITIALIZATION!
  glewExperimental = GL_TRUE;
  GLenum res = glewInit();
  if (res != GLEW_OK) {
    std::cout << glewGetErrorString(res) << std::endl;
  }
  // GLEW throws some errors, so discard all the errors so far
  while(glGetError() != GL_NO_ERROR) {}
#endif

  //------
  // GUI |
  //------
  int menu_width = 200;

  //---------------------
  // RECONSTRUCTION BOX |
  //---------------------
  nanogui::Window *window = new nanogui::Window(this, "Reconstruction");
  window->setPosition(Eigen::Vector2i(15, 15));
  window->setLayout(new nanogui::GroupLayout());
  window->setFixedWidth(menu_width);

  new nanogui::Label(window, "Reconstructions dir", "sans-bold");
  nanogui::Widget* widget = new nanogui::Widget(window);
  widget->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal,
                                           nanogui::Alignment::Middle, 0, 5));

  reconstrDir_txtBox_ = new nanogui::TextBox(widget);
  reconstrDir_txtBox_->setEditable(false);
  reconstrDir_txtBox_->setFixedSize(Eigen::Vector2i(110, 20));
  reconstrDir_txtBox_->setValue(app_model_->reconstructions_dir());
  reconstrDir_txtBox_->setFontSize(16);

  nanogui::Button* b = new nanogui::Button(widget, "", ENTYPO_ICON_FOLDER);
  b->setFixedSize(Eigen::Vector2i(25, 25));
  b->setCallback([&] {
    std::string new_val = dir_dialog();
    app_model_->set_reconstructions_dir(new_val);
    reconstrDir_txtBox_->setValue(new_val);
    this->SubjectChangeEvent();
  });

  {
    new nanogui::Label(window, "Subject ID", "sans-bold");
    auto intBox = new nanogui::IntBox<int>(window);
    intBox->setEditable(true);
    intBox->setFixedSize(Eigen::Vector2i(110, 20));
    intBox->setValue(0);
    //intBox->setUnits("Mhz");
    intBox->setDefaultValue("0");
    intBox->setCallback([&](int val){
      app_model_->set_subject_id(val);
      this->SubjectChangeEvent();
    });
    intBox->setFontSize(16);
    //intBox->setFormat("[0-9][0-9]*");
    intBox->setSpinnable(true);
    intBox->setMinValue(0);
    //intBox->setMaxValue(119);
    intBox->setValueIncrement(1);
  }

  new nanogui::Label(window, "Expression ID", "sans-bold");
  widget = new nanogui::Widget(window);
  widget->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal,
                                           nanogui::Alignment::Middle, 0, 5));

  exprId_txtBox_ = new nanogui::TextBox(widget);
  exprId_txtBox_->setEditable(false);
  exprId_txtBox_->setFixedSize(Eigen::Vector2i(85, 20));
  exprId_txtBox_->setValue(app_model_->expression());
  exprId_txtBox_->setDefaultValue("");
  exprId_txtBox_->setFontSize(16);

  auto intBox = new nanogui::IntBox<int>(widget);
  intBox->setEditable(true);
  intBox->setFixedSize(Eigen::Vector2i(50, 20));
  intBox->setValue(0);
  intBox->setCallback(std::function<void(int)>([&](int val){
    if (val < 0 || val >= app_model_->expressions.size())
      return;
    app_model_->set_expression_id(val);
    exprId_txtBox_->setValue(app_model_->expression());
    this->SubjectChangeEvent();
  }));
  //intBox->setUnits("Mhz");
  intBox->setDefaultValue("0");
  intBox->setFontSize(16);
  //intBox->setFormat("[0-9][0-9]*");
  intBox->setSpinnable(true);
  intBox->setMinValue(0);
  intBox->setMaxValue(app_model_->expressions.size() - 1);
  intBox->setValueIncrement(1);


  //-------------
  // FRAME VIEW |
  //-------------
  window = new nanogui::Window(this, "Current frame");
  window->setPosition(Eigen::Vector2i(905, 15));
  window->setLayout(new nanogui::GroupLayout());

  // Set the first texture
  image_view_ = new nanogui::ImageView(window, 0);
  image_view_->setFixedSize(Eigen::Vector2i(320, 240));
  image_view_->setGridThreshold(20);
  image_view_->setPixelInfoThreshold(20);
  image_view_->setPixelInfoCallback(
      [this](const Eigen::Vector2i& index) -> std::pair<std::string, nanogui::Color> {
        auto& imageData = texture_data_;
        auto& textureSize = this->image_view_->imageSize();
        std::string stringData;
        uint16_t channelSum = 0;
        for (int i = 0; i != 4; ++i) {
          auto& channelData = imageData[4*index.y()*textureSize.x() + 4*index.x() + i];
          channelSum += channelData;
          stringData += (std::to_string(static_cast<int>(channelData)) + "\n");
        }
        float intensity = static_cast<float>(255 - (channelSum / 4)) / 255.0f;
        float colorScale = intensity > 0.5f ? (intensity + 1) / 2 : intensity / 2;
        nanogui::Color textColor = nanogui::Color(colorScale, 1.0f);
        return { stringData, textColor };
      });

  //new nanogui::Label(window, "Frame id", "sans-bold");
  frameId_IntBox_ = new nanogui::IntBox<int>(window);
  frameId_IntBox_->setEditable(true);
  frameId_IntBox_->setFixedSize(Eigen::Vector2i(320, 20));
  frameId_IntBox_->setValue(app_model_->current_frame());
  frameId_IntBox_->setDefaultValue("0");
  frameId_IntBox_->setFontSize(16);
  //frameId_IntBox_->setFormat("[0-9][0-9]*");
  frameId_IntBox_->setSpinnable(true);
  frameId_IntBox_->setMinValue(0);
  frameId_IntBox_->setValueIncrement(1);
  frameId_IntBox_->setCallback([&](int val) {
    app_model_->set_current_frame(val);
    slider_frame_->setValue(app_model_->current_frame() / (float)app_model_->total_frames());
    load_image_ = true;
  });

  slider_frame_ = new nanogui::Slider(window);
  slider_frame_->setValue(app_model_->current_frame() / (float)app_model_->total_frames());
  slider_frame_->setFixedWidth(320);
  slider_frame_->setCallback([this](float value) {
    int frame = (int) (value * app_model_->total_frames());
    frameId_IntBox_->setValue(frame);
    app_model_->set_current_frame(frame);
    load_image_ = true;
  });


  //---------
  // REVIEW |
  //---------
  window = new nanogui::Window(this, "Review");
  window->setPosition(Eigen::Vector2i(15, 240));
  window->setLayout(new nanogui::GroupLayout());
  window->setFixedWidth(menu_width);

  new nanogui::Label(window, "Errors", "sans-bold");
  wsubject_cb_ = new nanogui::CheckBox(window, "Wrong subject");
  wsubject_cb_->setChecked(app_model_->review().subject == AppModel::AssessValue::WRONG);
  wsubject_cb_->setCallback([&] (bool val){
    app_model_->review().set_subject(val ? AppModel::AssessValue::WRONG :
                                     AppModel::AssessValue::CORRECT);
  });

  wexpr_cb_ = new nanogui::CheckBox(window, "Wrong expression");
  wexpr_cb_->setChecked(app_model_->review().expression == AppModel::AssessValue::WRONG);
  wexpr_cb_->setCallback([&] (bool val) {
    app_model_->review().set_expression(val ? AppModel::AssessValue::WRONG :
                                        AppModel::AssessValue::CORRECT);
  });

  new nanogui::Label(window, "Last frame", "sans-bold");
  widget = new nanogui::Widget(window);
  widget->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal,
                                           nanogui::Alignment::Middle, 0, 5));

  lastFrame_IntBox_ = new nanogui::IntBox<int>(widget);
  lastFrame_IntBox_->setEditable(true);
  lastFrame_IntBox_->setFixedSize(Eigen::Vector2i(110, 20));
  lastFrame_IntBox_->setValue(app_model_->review().last_frame);
  lastFrame_IntBox_->setDefaultValue("0");
  lastFrame_IntBox_->setFontSize(16);
  //lastFrame_IntBox_->setFormat("[0-9][0-9]*");
  lastFrame_IntBox_->setSpinnable(true);
  lastFrame_IntBox_->setMinValue(-1);
  lastFrame_IntBox_->setValueIncrement(1);
  lastFrame_IntBox_->setCallback([&](int val) {
    app_model_->review().last_frame = val;
  });

  update_button_ = new nanogui::Button(widget, "", ENTYPO_ICON_CYCLE);
  update_button_->setFixedSize(Eigen::Vector2i(25, 25));
  update_button_->setCallback([&] {
    int val = frameId_IntBox_->value();
    lastFrame_IntBox_->setValue(val);
    app_model_->review().last_frame = val;
  });

  new nanogui::Label(window, "Quality", "sans-bold");
  widget = new nanogui::Widget(window);
  widget->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal));
  good_Btn_ = new nanogui::Button(widget, "Good");
  good_Btn_->setFlags(nanogui::Button::RadioButton);
  good_Btn_->setFixedSize(Eigen::Vector2i(60, 25));
  good_Btn_->setPushed(app_model_->review().quality == AppModel::AssessValue::CORRECT);
  good_Btn_->setCallback([&]{
    app_model_->review().set_quality(AppModel::AssessValue::CORRECT);
    this->ReviewUpdatedEvent();
  });
  bad_Btn_ = new nanogui::Button(widget, "Bad");
  bad_Btn_->setFlags(nanogui::Button::RadioButton);
  bad_Btn_->setFixedSize(Eigen::Vector2i(60, 25));
  bad_Btn_->setPushed(app_model_->review().quality == AppModel::AssessValue::WRONG);
  bad_Btn_->setCallback([&]{
    app_model_->review().set_quality(AppModel::AssessValue::WRONG);
    this->ReviewUpdatedEvent();
  });

  performLayout();


  //shader_.initFromFiles("a_simple_shader", v_shader_path, f_shader_path);
  shader_.init("simple_color_shader",
               // content of resource/vertex-shader-color.vs
               "#version 330\n"
               "// Input data\n"
               "layout (location = 0) in vec3 position;\n"
               "layout (location = 1) in vec3 normal;\n"
               "layout (location = 2) in vec4 vertex_color;\n"
               "// uniform input -> matrix\n"
               "uniform mat4 camera;\n"
               "uniform mat4 model;\n"
               "// output\n"
               "out vec3 normal0;\n"
               "out vec4 color0;\n"
               "void main() {\n"
               "  // Apply transformation onto points\n"
               "  gl_Position = camera * model * vec4(position, 1);\n"
               "  // Transfer normal\n"
               "  normal0 = (model * vec4(normal, 0.0)).xyz;\n"
               "  // transfert TCoord\n"
               "  color0 = vertex_color;\n"
               "}",
               // content of resource/fragment-shader-color.vs
               "#version 330\n"
               "// input\n"
               "in vec3 normal0;\n"
               "in vec4 color0;\n"
               "// output\n"
               "out vec4 fragColor;\n"
               "void main() {\n"
               "  //note: the texture function was called texture2D in older versions of GLSL\n"
               "  //fragColor = texture(gSampler, texCoord0);\n"
               "  vec3 light_dir = vec3(0.0, 0.0, 1.0);\n"
               "  float cos_theta = clamp( dot(normal0, light_dir), 0, 1);\n"
               "  fragColor = color0;\n"
               "}");

  camera_ = new LTS5::OGLCamera(1280, 1024);
  camera_->LookAt(glm::vec3(0.f, 0.f, 1.f),
                  glm::vec3(0.f, 0.f, 0.f));

  // Loader loop initialization
  loader_is_alive_ = true;
  loader_thread_.reset(new std::thread(std::bind(&AppView::LoaderLoop, this)));
}

/*
 * @name  ~AppView
 * @fn  ~AppView()
 * @brief Destructor of the class
 */
AppView::~AppView() {
  shader_.free();

  if (camera_) {
    delete camera_;
  }

  loader_is_alive_ = false;
  loader_thread_->join();
}

/*
 * @name  Run
 * @fn  int Run()
 * @brief Run the main loop of the app
 * @return 0 if success, -1 otherwise
 */
int AppView::Run() {
  try {
    {
      nanogui::ref<AppView> app = this;

      std::shared_ptr<AppModel::Mesh> mesh;
      if (app_model_->mesh(mesh)) {
        this->UploadMesh(*mesh);
      }

      app->drawAll();
      app->setVisible(true);
      nanogui::mainloop();
    }

    nanogui::shutdown();

  } catch (const std::runtime_error &e) {
    std::string error_msg = std::string("Caught a fatal error: ") +
      std::string(e.what());
#if defined(_WIN32)
    nanogui::MessageBoxA(nullptr, error_msg.c_str(), NULL, MB_ICONERROR | MB_OK);
#else
    std::cerr << error_msg << std::endl;
#endif
    return -1;
  }
  return 0;
}

#pragma mark -
#pragma mark Callbacks

/*
 * @name  SubjectEvent
 * @fn  void SubjectChangeEvent(void)
 * @brief Notify a change of subject (either subject id or expression id)
 */
void AppView::SubjectChangeEvent() {
  int err = app_model_->LoadNewSubject();
  std::shared_ptr<AppModel::Mesh> mesh;

  if (app_model_->raw_color().file_found_) {
    ready_to_load_ = true;
  }

  if (err == 0) {
    app_model_->mesh(mesh);
    this->UploadMesh(*mesh);
  } else {
    ClearView();
  }

  this->ResetSubject();
}

/*
 * @name ReviewUpdatedEvent
 * @fn void ReviewUpdatedEvent(void)
 * @brief Notify an update in the review
 */
void AppView::ReviewUpdatedEvent(void) {
  bool subj_val = wsubject_cb_->checked();
  app_model_->review().set_subject(subj_val ? AppModel::AssessValue::WRONG :
                                   AppModel::AssessValue::CORRECT);
  bool expr_val = wexpr_cb_->checked();
  app_model_->review().set_expression(expr_val ? AppModel::AssessValue::WRONG:
                                      AppModel::AssessValue::CORRECT);

  app_model_->WriteReviewToCsv();
  std::cout << "Review updated!" << std::endl;
}

/*
 * @name  keyboardEvent
 * @fn  virtual bool keyboardEvent(int key, int scancode, int action,
 *                                 int modifiers)
 * @brief Handle a keyboard event
 * @param[in]  key
 * @param[in]  scancode
 * @param[in]  action
 * @param[in]  modifiers
 * @return true if callback is defined, false otherwise
 */
bool AppView::keyboardEvent(int key, int scancode, int action,
                                    int modifiers) {
  if (nanogui::Screen::keyboardEvent(key, scancode, action, modifiers))
    return true;
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    setVisible(false);
    return true;
  }

  if (camera_->OnKeyboard(static_cast<LTS5::OGLKey>(key),
                          static_cast<LTS5::OGLKeyState>(action))) {
    return true;
  }
  return false;
}

/*
 * @name  mouseButtonEvent
 * @fn  virtual bool mouseButtonEvent(const Eigen::Vector2i& pos, int button,
 *                                    bool down, int modifier)
 * @brief Handle a mouse button event
 * @param[in]  pos
 * @param[in]  button
 * @param[in]  down
 * @param[in]  modifier
 * @return true if callback is defined, false otherwise
 */
bool AppView::mouseButtonEvent(const Eigen::Vector2i& pos, int button,
                               bool down, int modifier) {
  if (nanogui::Screen::mouseButtonEvent(pos, button, down, modifier))
    return true;

  if (camera_->OnMouseClick(static_cast<LTS5::OGLMouse>(button),
                            static_cast<LTS5::OGLKeyState>(!down),
                            pos[0], pos[1])) {
    return true;
  }
  return false;
}

/*
 * @name  mouseMotionEvent
 * @fn  virtual bool mouseMotionEvent(const Eigen::Vector2i& pos,
 *                                    const Eigen::Vector2i& rel, int button,
 *                                    int modifier)
 * @brief Handle a mouse Motion event
 * @param[in]  pos
 * @param[in]  rel
 * @param[in]  button
 * @param[in]  modifier
 * @return true if callback is defined, false otherwise
 */
bool AppView::mouseMotionEvent(const Eigen::Vector2i& p,
                               const Eigen::Vector2i& rel,
                               int button, int modifier) {
  if (nanogui::Screen::mouseMotionEvent(p, rel, button, modifier))
    return true;

  if (camera_->OnMouseMove(p[0], p[1])) {
    return true;
  }
  return false;
}

/*
 * @name  draw
 * @fn  virtual void draw(NVGcontext *ctx)
 * @brief draw
 * @param[in]  ctx  pointer to NVG context
 */
void AppView::draw(NVGcontext *ctx) {
  // Draw the user interface
  nanogui::Screen::draw(ctx);
}

/*
 * @name  drawContents
 * @fn  virtual void drawContents()
 * @brief draw contents
 */
void AppView::drawContents() {
  // Draw the window contents using OpenGL
  shader_.bind();

  //glFrontFace(GL_CCW);
  //glCullFace(GL_BACK);
  //glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);

  Eigen::Matrix4f model;
  model.setIdentity();
  model(1, 1) = -1.f;
  model(2, 2) = -1.f;

  cam_mat_ = Eigen::Map<Eigen::Matrix<float, 4, 4> >(&(camera_->matrix()[0][0]));

  shader_.setUniform("camera", cam_mat_);
  shader_.setUniform("model", model);
  shader_.drawIndexed(GL_TRIANGLES, 0, n_tri_);

  // Update the image
  if (load_image_) {
    int total_frames;
    texture_data_ = texture_.load(app_model_->color_bin_path(),
                                  app_model_->current_frame_ptr(),
                                  &total_frames);
    frameId_IntBox_->setValue(app_model_->current_frame());
    display_image_ = true;
    load_image_ = false;

    app_model_->set_total_frames(total_frames);
  }
  if (display_image_) {
    image_view_->bindImage(texture_.texture());
  }
}

#pragma mark -
#pragma mark Private

void AppView::ResetSubject() {
  // Review part
  lastFrame_IntBox_->setValue(app_model_->review().last_frame);
  wsubject_cb_->setChecked(app_model_->review().subject == AppModel::AssessValue::WRONG);
  wexpr_cb_->setChecked(app_model_->review().expression == AppModel::AssessValue::WRONG);
  good_Btn_->setPushed(app_model_->review().quality == AppModel::AssessValue::CORRECT);
  bad_Btn_->setPushed(app_model_->review().quality == AppModel::AssessValue::WRONG);

  frameId_IntBox_->setValue(app_model_->current_frame());
  float slider_value = app_model_->current_frame() / (float) app_model_->total_frames();
  slider_frame_->setValue(slider_value > 1.0 ? 1.0 : slider_value);

}

/*
 * @name  UploadMesh
 * @fn  void UploadMesh(const LTS5::Mesh<double>& mesh)
 * @brief Upload the mesh to the GPU
 * @param[in]  mesh
 */
void AppView::UploadMesh(const AppModel::Mesh& mesh) {
  const auto& vertices = mesh.get_vertex();
  int n_vert = vertices.size();
  const auto& colors = mesh.get_vertex_color();
  const auto& triangles = mesh.get_triangle();
  n_tri_ = triangles.size();
  const auto& normals = mesh.get_normal();

  Eigen::MatrixXi indices(3, n_tri_);
  for (int i = 0; i < n_tri_; ++i) {
    indices.col(i) << triangles[i].x_, triangles[i].y_, triangles[i].z_;
  }

  // In the future: do it like that, if types agree!
  //Eigen::Map<Eigen::MatrixXf> vert_pos(&vertices[0], 3, n_vert);

  Eigen::MatrixXf positions(3, n_vert);
  Eigen::MatrixXf norms(3, n_vert);
  Eigen::MatrixXf vert_colors(4, n_vert);

  for (int i = 0; i < n_vert; ++i) {
    positions.col(i) << vertices[i].x_, vertices[i].y_, vertices[i].z_;
    norms.col(i) << normals[i].x_, normals[i].y_, normals[i].z_;
  }

  for (int i = 0; i < colors.size(); ++i) {
    vert_colors.col(i) << colors[i].r_/255.f, colors[i].g_/255.f,
      colors[i].b_/255.f, 1.f;
  }

  shader_.bind();
  shader_.uploadIndices(indices);
  shader_.uploadAttrib("position", positions);
  shader_.uploadAttrib("normal", norms);
  shader_.uploadAttrib("vertex_color", vert_colors);
}

/*
 * @name  ClearView
 * @fn  void ClearView(void)
 * @brief Clear the display from any mesh
 */
void AppView::ClearView(void) {
  Eigen::MatrixXi indices(3, 1);

  Eigen::MatrixXf positions(3, 1);
  Eigen::MatrixXf norms(3, 1);
  Eigen::MatrixXf vert_colors(4, 1);

  shader_.bind();
  shader_.uploadIndices(indices);
  shader_.uploadAttrib("position", positions);
  shader_.uploadAttrib("normal", norms);
  shader_.uploadAttrib("vertex_color", vert_colors);
}

/*
 * @name  LoaderLoop
 * @fn  void LoaderLoop(void)
 * @brief Loader loop to execute in sepatate thread
 */
void AppView::LoaderLoop(void) {
  while (loader_is_alive_) {
    if (ready_to_load_) {
      display_image_ = false;

      // loop
      const AppModel::RawFile& color = app_model_->raw_color();
      std::cout << "[AppView::LoaderLoop] Loading " << color.path() << std::endl;

      if (color.file_found_ && !color.file_compressed_) {

      } else if (color.file_found_ && color.file_compressed_) {
        std::string path = temp_dir_path_ + color.file_bin_name_;
        struct stat buffer;
        if (stat(path.c_str(), &buffer) != 0) {
          // copy in temp/, decompress, use and delete
          std::string origin = color.path();
          std::string dest = temp_dir_path_ + color.file_bin_name_ + ".gz";
          std::string cmd = "rsync -u " + origin + " " + dest;
          std::system (cmd.c_str());
          // Decompress
          cmd = "/usr/local/bin/pigz -kd " + dest ;
          std::system(cmd.c_str());
          // Remove the compressed file
          cmd = "rm " + dest;
        }
        app_model_->set_unzip_color(std::make_shared<AppModel::RawFile>(temp_dir_path_, color.file_bin_name_, false));
      }

      load_image_ = true;

      std::cout << "[AppView::LoaderLoop] Done!" << std::endl;
      ready_to_load_ = false;
    } else {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
  }
}
