/**
 *  @file   nonrigid_alignment.cpp
 *  @brief  Non rigid alignment optimisation problem implementation
 *
 *  @author Gabriel Cuendet
 *  @date   06/10/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <fstream>
#include "nonrigid_alignment.hpp"

#pragma mark -
#pragma mark Initialization

/*
 * @name  NonrigidAlignmentProblem
 * @fn  NonrigidAlignmentProblem(void)
 * @brief Class constructor
 */
template<typename T>
NonrigidAlignmentProblem<T>::NonrigidAlignmentProblem(void) {
  mht_ = new LTS5::MHT<T>();
  mpu_ = new LTS5::MPU<T>();
}

/*
 * @name  NonrigidAlignmentProblem
 * @fn  void ~NonrigidAlignmentProblem(void) {}
 * @brief Destructor of the class
 */
template<typename T>
NonrigidAlignmentProblem<T>::~NonrigidAlignmentProblem(void) {
  if (mpu_) {
    delete mpu_;
  }
  if (mht_) {
    delete mht_;
  }
}

/*
 * @name  InitMHT
 * @fn  void Init(const Eigen::SparseMatrix<T>& Q,
 *                const Eigen::SparseMatrix<T>& D, const int& n_basis)
 * @brief Initialize the MHT transform by setting important params and
 *        computing the basis
 * @param[in] Q       sparse stifness matrix
 * @param[in] D       diagonal lumped mass matrix
 * @param[in] n_basis number of basis vector to compute
 */
template<typename T>
void NonrigidAlignmentProblem<T>::InitMHT(const Eigen::SparseMatrix<T>& Q,
                                          const Eigen::SparseMatrix<T>& D,
                                          const int& n_basis) {
  mht_->Init(Q, D, n_basis);
}

/**
 * @name  LoadMHTFromFile
 * @fn  int LoadMHTFromFile(const std::string filepath)
 * @brief Load a precomputed MHT from file
 * @param[in]  filepath  Path of the file containing the MHT
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int NonrigidAlignmentProblem<T>::LoadMHTFromFile(const std::string& filepath) {
  return mht_->Read(filepath);
}

/*
 * @name  SaveMHTToFile
 * @fn  int SaveMHTToFile(const std::string& filepath) const
 * @brief Save the basis to binary file
 * @param[in]  filepath  Path to the file
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int NonrigidAlignmentProblem<T>::SaveMHTToFile(const std::string& filepath) const {
  return mht_->Write(filepath);
}

/**
 * @name  ComputeTheta0
 * @fn  void ComputeTheta0(LTS5::Mesh<T>* source)
 * @brief Project the source onto the MHT in order to get intital params theta0
 * @param[in,out]  source  Source mesh
 */
template<typename T>
void NonrigidAlignmentProblem<T>::ComputeTheta0(LTS5::Mesh<T>* source) {
  // Compute theta0 (initial spectral coefficients) by projecting rigidly
  // aligned source mesh onto the MHT
  const std::vector<LTS5::Vector3<T> >& vertices = source->get_vertex();
  mht_->Transform(vertices, &theta0_);
  theta0_.resize(theta0_.size(), 1);
}

/*
 * @name  InitMPU
 * @fn  void InitMPU(double max_error, int max_depth, double alpha_radius,
 *                   const LTS5::Mesh<T>& target)
 * @brief Initialize target representation
 * @param[in]  max_error     Maximum tolerated approximation error
 * @param[in]  max_depth     Maximum depth of the tree
 * @param[in]  alpha_radius  Radius
 * @param[in]  target        Target mesh
 */
template<typename T>
void NonrigidAlignmentProblem<T>::InitMPU(double max_error, int max_depth,
                                          double alpha_radius,
                                          const LTS5::Mesh<T>& target,
                                          const LTS5::AABC<T>& domain) {
  mpu_->set_max_error(max_error);
  mpu_->set_max_depth(max_depth);
  mpu_->set_alpha(alpha_radius);

  mpu_->Build(target, domain);
}

/*
 * @name  LoadSrcMaskFromFile
 * @fn  void LoadSrcMaskFromFile(const std::string& filepath)
 * @brief Load the source's mask from a file containing the indices of the mask
 * @param[in]  filepath  Path to the file containing the indices of the mask
 */
template<typename T>
void NonrigidAlignmentProblem<T>::LoadSrcMaskFromFile(const std::string& filepath) {
  src_mask_.clear();
  std::ifstream f(filepath);
  int id;
  while(f >> id) {
    src_mask_.push_back(id);
  }
  f.close();
}


#pragma mark -
#pragma mark Process

/*
 *  @name EvaluateObjectiveFunction
 *  @fn void EvaluateObjectiveFunction(const Matrix& theta, T* cost) const
 *  @brief  Evaluate the objective function with parameters \p theta
 *  @param[in]  theta     Parameters to use to evaluate objective function
 *  @param[out] cost      Objective function cost
 */
template<typename T>
void NonrigidAlignmentProblem<T>::EvaluateObjectiveFunction(const Matrix& theta,
                                                            T* cost) const {
  // Reshape theta into a mx3 matrix
  size_t theta_sz = theta.size();
  Matrix& freq_vec = const_cast<Matrix&>(theta);
  freq_vec.resize(3, theta_sz/3);

  // MPU Evaluation
  Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> spat_vec;
  mht_->InvTransform(freq_vec, &spat_vec);

  *cost = T(0.0);
  if (!src_mask_.empty()) {
    for (auto mask_it = src_mask_.begin(); mask_it != src_mask_.end(); ++mask_it) {
      LTS5::Vector3<T>* vert_ptr = reinterpret_cast<LTS5::Vector3<T>* >(spat_vec.col(*mask_it).data());
      *cost += mpu_->EvaluateSquare(*vert_ptr);
    }
  } else {
    for (int i = 0; i < spat_vec.outerSize(); ++i) {
      LTS5::Vector3<T>* vert_ptr = reinterpret_cast<LTS5::Vector3<T>* >(spat_vec.col(i).data());
      *cost += mpu_->EvaluateSquare(*vert_ptr);
    }
  }
  freq_vec.resize(theta_sz, 1);
}

/**
 *  @name EvaluateObjectiveFunction
 *  @fn void EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost) const
 *  @brief  Evaluate the objective function with parameters \p theta
 *  @param[in]  theta     Parameters to use to evaluate objective function
 *  @param[out] cost      Objective function cost vector
 */
template<typename T>
void NonrigidAlignmentProblem<T>::EvaluateObjectiveFunction(const Matrix& theta,
                                                            Matrix* cost) const {
  std::cout << "Not implemented" << std::endl;
}

/**
 *  @name EvaluateGradient
 *  @fn void EvaluateGradient(const Matrix& theta, Matrix* jacob)
 *  @brief  Evaluate the function's gradient with parameters \p theta
 *  @param[in]  theta     Position where to evaluate objective function
 *  @param[out] jacob     Jacobian matrix for parameters theta
 */
template<typename T>
void NonrigidAlignmentProblem<T>::EvaluateGradient(const Matrix& theta,
                                                   Matrix* jacob) const {
  // Reshape theta into a mx3 matrix
  size_t theta_sz = theta.size();
  Matrix& freq_vec = const_cast<Matrix&>(theta);
  freq_vec.resize(3, theta_sz/3);

  // MPU Evaluation
  Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> spat_vec;
  mht_->InvTransform(freq_vec, &spat_vec);

  const Matrix& E_t = mht_->get_basis_t();
  int n = static_cast<int>(E_t.innerSize());
  Matrix GradQ2(3, n);
  GradQ2.setZero();

  if (!src_mask_.empty()) {
    for (auto mask_it = src_mask_.begin(); mask_it != src_mask_.end(); ++mask_it) {
      LTS5::Vector3<T>* vert_ptr = reinterpret_cast<LTS5::Vector3<T>* >(spat_vec.col(*mask_it).data()) ;
      LTS5::Vector3<T>* grad_ptr = reinterpret_cast<LTS5::Vector3<T>* >(GradQ2.col(*mask_it).data());
      mpu_->EvaluateGradientSquare(*vert_ptr, grad_ptr);  // Store as a row of GradQ2
    }
  } else {
    for (int i = 0; i < spat_vec.innerSize(); ++i) {
      LTS5::Vector3<T>* vert_ptr = reinterpret_cast<LTS5::Vector3<T>* >(spat_vec.col(i).data()) ;
      LTS5::Vector3<T>* grad_ptr = reinterpret_cast<LTS5::Vector3<T>* >(GradQ2.col(i).data());
      mpu_->EvaluateGradientSquare(*vert_ptr, grad_ptr);  // Store as a row of GradQ2
    }
  }

  (*jacob) = GradQ2 * E_t;
  jacob->resize(theta_sz, 1);

  // Resize theta back!
  freq_vec.resize(theta_sz, 1);
}

/**
 *  @name EvaluateHessian
 *  @fn void EvaluateHessian(const Matrix& x, Matrix* hessian)
 *  @brief  Evaluate the function's hessian matrix with parameters \p theta
 *  @param[in]  theta   Parameters used to evalute hessian matrix
 *  @param[out] hessian Hessian value for parameters theta
 */
template<typename T>
void NonrigidAlignmentProblem<T>::EvaluateHessian(const Matrix& theta,
                                                  Matrix* hessian) const {
  std::cout << "Not implemented" << std::endl;
}

/**
 * @name  SaveMesh
 * @fn  void SaveMesh(const Matrix& theta, const std::string& filename) const
 * @brief Save the mesh corresponding to the vector of parameters theta
 * @param[in]  theta     Vector of parameters to reconstruct from
 * @param[in]  filename  Path of the file to write
 */
template<typename T>
void NonrigidAlignmentProblem<T>::SaveMesh(const Matrix& theta, const std::string& filename) const {

  // Reshape theta into a mx3 matrix
  size_t theta_sz = theta.size();
  Matrix& freq_vec = const_cast<Matrix&>(theta);
  freq_vec.resize(3, theta_sz/3);

  // MPU Evaluation
  Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> spat_vec;
  mht_->InvTransform(freq_vec, &spat_vec);

  LTS5::Mesh<T> regenerated_mesh;
  std::vector<LTS5::Vector3<T> >& vertices = regenerated_mesh.get_vertex();
  vertices.clear();
  for (int i = 0; i < spat_vec.outerSize(); ++i) {
    LTS5::Vector3<T>* vert_ptr = reinterpret_cast<LTS5::Vector3<T>* >(spat_vec.col(i).data());
    vertices.push_back(*vert_ptr);
  }

  regenerated_mesh.Save(filename);

  // Resize theta back!
  freq_vec.resize(theta_sz, 1);
}

#pragma mark -
#pragma mark Accessors

/**
 *  @name get_dimensions
 *  @fn void get_dimensions(int* n_row, int* n_col) const
 *  @brief  Provide problem dimensions
 *  @param[out] n_row   Number of row in the system (Observables)
 *  @param[out] n_col   Number of column in the system (Variables)
 */
template<typename T>
void NonrigidAlignmentProblem<T>::get_dimensions(int* n_row, int* n_col) const {

}

/**
 *  @name get_A
 *  @fn Matrix& get_A(void)
 *  @brief  Provide access to system matrix ()
 *  @return System Matrix
 */
template<typename T>
const typename NonrigidAlignmentProblem<T>::Matrix& NonrigidAlignmentProblem<T>::get_A(void) const {
  return Matrix(0,0);
}

/**
 *  @name get_AtA
 *  @fn Matrix& get_AtA(void)
 *  @brief  Provide access to system matrix ()
 *  @return At * A
 */
template<typename T>
const typename NonrigidAlignmentProblem<T>::Matrix& NonrigidAlignmentProblem<T>::get_AtA(void) const {
 return Matrix(0,0);
}

/**
 *  @name get_y
 *  @fn Matrix& get_y(void)
 *  @brief Provide access to y
 *  @return y
 */
template<typename T>
const typename NonrigidAlignmentProblem<T>::Matrix& NonrigidAlignmentProblem<T>::get_y(void) const {
  return Matrix(0,0);
}

/**
 *  @name get_Aty
 *  @fn Matrix& get_Aty(void)
 *  @brief Provide access to At * y
 *  @return At * y
 */
template<typename T>
const typename NonrigidAlignmentProblem<T>::Matrix& NonrigidAlignmentProblem<T>::get_Aty(void) const {
  return Matrix(0,0);
}

#pragma mark -
#pragma mark Declaration

/** Float NonrigidAlignmentProblem */
template class NonrigidAlignmentProblem<float>;
/** Double NonrigidAlignmentProblem */
template class NonrigidAlignmentProblem<double>;

