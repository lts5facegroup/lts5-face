/**
 *  @file   rigid_alignment.cpp
 *  @brief  Rigid Alignement implementation
 *
 *  @author Gabriel Cuendet
 *  @date   2016-09-29
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#define GLM_FORCE_RADIANS
#include "glm/gtx/rotate_vector.hpp"
#include "glm/gtx/string_cast.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"

#include "lts5/geometry/intersection_utils.hpp"
#include "lts5/geometry/octree.hpp"
#include "lts5/geometry/aabb_tree.hpp"
#include "lts5/utils/math/vector.hpp"
#include "lts5/utils/math/linear_algebra_wrapper.hpp"
#include "lts5/utils/file_io.hpp"
#include "rigid_alignment.hpp"

#pragma mark -
#pragma mark Initialization

/*
 * @name  RigidAlignment
 * @fn  RigidAlignment(const LTS5::Mesh<T>& target)
 * @brief Constructor of the class
 * @param[in]  target         Target mesh
 * @param[in]  source         Source mesh (template)
 */
template<typename T>
RigidAlignment<T>::RigidAlignment(const LTS5::Mesh<T>& target,
                                  LTS5::Mesh<T>* source) :
source_(source), target_(&target), im_width_(512),
im_height_(512) {
  face_tracker_ = nullptr;
  offscreen_renderer_ = nullptr;
  program_ = nullptr;
  camera_ = nullptr;
}

/*
 * @name  ~RigidAlignment
 * @fn  ~RigidAlignment()
 * @brief Destructor of the class
 */
template<typename T>
RigidAlignment<T>::~RigidAlignment() {
  if (face_tracker_) {
    delete face_tracker_;
  }
  if (offscreen_renderer_) {
    delete offscreen_renderer_;
  }
  if (program_) {
    delete program_;
  }
  if (camera_) {
    delete camera_;
  }
}

/*
 * @name  LoadSrcLandmarks
 * @fn  int LoadSrcLandmarks(const std::string& filename)
 * @brief Load the source landmarks from a file
 * @param[in]  filename  File path containing the annotations
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int RigidAlignment<T>::LoadSrcLandmarks(const std::string& filename) {
  int error = 0;
  src_landmarks_.clear();
  std::ifstream f(filename);
  int id;
  while(f >> id) {
    src_landmarks_.push_back(id);
  }
  std::cout << "Loaded " << src_landmarks_.size()<< " landmarks!" << std::endl;

  // Initialize weights_
  weights_.clear();
  weights_.resize(src_landmarks_.size(), 1.0);

  return error;
}


/*
 * @name  InitFaceTracker
 * @fn  int InitFaceTracker(const std::string& model_file,
 *                          const std::string& face_det_file);
 * @brief Initialize the face tracker with given model files
 * @param[in]  model_file    Bin file containing face tracker model
 * @param[in]  face_det_file File containing the face detector config
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int RigidAlignment<T>::InitFaceTracker(const std::string& model_file,
                                       const std::string& face_det_file) {
  if (face_tracker_ != nullptr) {
    delete face_tracker_;
  }

  face_tracker_ = new LTS5::SdmTracker();
  int error = face_tracker_->Load(model_file, face_det_file);

  return error;
}

/*
 * @name  InitOffscreenRenderer
 * @fn  int InitOffscreenRenderer(const std::string& vertex_shader_path,
 *                                const std::string& fragment_shader_path);
 * @brief Initialize the offscreen renderer with the correct shaders and all
 * @param[in]  vertex_shader_path    Path to the shader file
 * @param[in]  fragment_shader_path  Path to the shader file
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int RigidAlignment<T>::InitOffscreenRenderer() {
  int error = 0;
  // Create renderer in order to have an OpenGL ctx !!!
  using ImageType = typename LTS5::OGLOffscreenRenderer<T>::ImageType;
  if (!offscreen_renderer_) {
    offscreen_renderer_ = new LTS5::OGLOffscreenRenderer<T>();
  }
  offscreen_renderer_->Init(im_width_, im_height_, false, ImageType::kGrayscale);

  // Create shader + program
  std::string vertex_shader_str = "#version 330\n"
    "layout (location = 0) in vec3 position;\n"
    "layout (location = 1) in vec3 normal;\n"
    "layout (location = 2) in vec4 vertex_color;\n"
    "uniform mat4 camera;\n"
    "out vec3 normal0;\n"
    "out vec4 color0;\n"
    "void main() {\n"
    "  gl_Position = camera * vec4(position, 1);\n"
    "  normal0 = normal;\n"
    "  color0 = vertex_color;\n"
    "}";
  std::string frag_shader_str = "#version 330\n"
    "in vec3 normal0;\n"
    "in vec4 color0;\n"
    "out float frag_color;\n"
    "void main() {\n"
    "  float out_color = 0.2126*color0.b + 0.7152*color0.g + 0.0722*color0.r;\n"
    "  frag_color = out_color * 255.f;\n"
    "}";
  using ShaderType = LTS5::OGLShader::ShaderType;
  std::vector<LTS5::OGLShader> shaders;
  shaders.emplace_back(LTS5::OGLShader(vertex_shader_str, ShaderType::kVertex));
  shaders.emplace_back(LTS5::OGLShader(frag_shader_str, ShaderType::kFragment));
  std::vector<LTS5::OGLProgram::Attributes> attrib;
  attrib.emplace_back(
          LTS5::OGLProgram::Attributes(LTS5::OGLProgram::AttributeType::kPoint,
                                       0,
                                       "position"));
  attrib.emplace_back(
          LTS5::OGLProgram::Attributes(LTS5::OGLProgram::AttributeType::kNormal,
                                       1,
                                       "normal"));
  attrib.emplace_back(
          LTS5::OGLProgram::Attributes(LTS5::OGLProgram::AttributeType::kColor,
                                       2,
                                       "vertex_color"));
  if (program_) {
    delete program_;
  }
  program_ = new LTS5::OGLProgram(shaders, attrib);
  // Camera
  if (camera_) {
    delete camera_;
  }
  camera_ = new LTS5::OGLCamera(im_width_, im_height_);
  camera_->set_position(glm::vec3(0, 0, 1.0));
//  glm::mat4 transform;
//  transform *= glm::rotate(glm::mat4(), glm::radians(0.f),
//                           glm::vec3(0, 1, 0));
  program_->Use();
  program_->SetUniform("camera", camera_->projection() * camera_->view(true));
//  program_->SetUniform("model", transform);
  program_->StopUsing();

  offscreen_renderer_->AddMesh(*target_);
  offscreen_renderer_->AddProgram(*program_);
  // Bind to opengl
  error = offscreen_renderer_->BindToOpenGL();
  // Done
  //is_initialized_ = (error == 0);

  return error;
}

/*
 * @name  get_trg_landmarks_vertices
 * @fn  void get_trg_landmarks_vertices(std::vector<LTS5::Vector3<T> >* vertices) const
 * @brief Getter for the vertices corresponding to the target landmarks
 * @param[out] vertices  List of vertices corresponding to the target landmarks
 */
template<typename T>
void RigidAlignment<T>::get_trg_landmarks_vertices(std::vector<LTS5::Vector3<T> >* vertices) const {
  vertices->clear();
  const std::vector<LTS5::Vector3<T> >& trg_vertices = target_->get_vertex();

  int i = 0;
  for (auto v_id = trg_landmarks_.begin(); v_id != trg_landmarks_.end(); ++v_id, ++i) {
    if (*v_id != -1 && weights_[i] != 0) {
      vertices->push_back(trg_vertices[*v_id]);
    }
  }
}

/*
 * @name  get_trg_landmarks_mat
 * @fn  void get_trg_landmarks_mat(cv::Mat* trg_landmarks) const
 * @brief Get target landmarks as an OpenCV Mat
 * @param[out] trg_landmarks  OpenCV Mat containing trg landmarks in the rows
 */
template<typename T>
void RigidAlignment<T>::get_trg_landmarks_mat(cv::Mat* trg_landmarks) const {
  int n_rows = static_cast<int>(trg_landmarks_.size());
  unsigned char cv_type = cv::DataType<T>::type;
  trg_landmarks->create(n_rows, 3, cv_type);
  const std::vector<LTS5::Vector3<T> >& vertices = target_->get_vertex();
  int i =0;
  for (auto id_it = trg_landmarks_.begin(); id_it != trg_landmarks_.end();
       ++id_it, ++i) {
    if (*id_it != -1) {
      trg_landmarks->at<T>(i, 0) = vertices[*id_it].x_;
      trg_landmarks->at<T>(i, 1) = vertices[*id_it].y_;
      trg_landmarks->at<T>(i, 2) = vertices[*id_it].z_;
    } else {
      trg_landmarks->at<T>(i, 0) = 0.0;
      trg_landmarks->at<T>(i, 1) = 0.0;
      trg_landmarks->at<T>(i, 2) = 0.0;
    }
  }
}

/*
 * @name  get_src_landmarks_vertices
 * @fn  void get_src_landmarks_vertices(std::vector<LTS5::Vector3<T> >* vertices) const
 * @brief Getter for the vertices corresponding to the source landmarks
 * @param[out] vertices  List of vertices corresponding to the source landmarks
 */
template<typename T>
void RigidAlignment<T>::get_src_landmarks_vertices(std::vector<LTS5::Vector3<T> >* vertices) const {
  vertices->clear();
  const std::vector<LTS5::Vector3<T> >& src_vertices = source_->get_vertex();

  int i = 0;
  for (auto v_id = src_landmarks_.begin(); v_id != src_landmarks_.end(); ++v_id, ++i) {
    if (*v_id != -1 && weights_[i] != 0) {
      vertices->push_back(src_vertices[*v_id]);
    }
  }
}

/*
 * @name  get_src_landmarks_mat
 * @fn  void get_src_landmarks_mat(cv::Mat* src_landmarks) const
 * @brief Get source landmarks as an OpenCV Mat
 * @param[out] src_landmarks  OpenCV Mat containing src landmarks in the rows
 */
template<typename T>
void RigidAlignment<T>::get_src_landmarks_mat(cv::Mat* src_landmarks) const {
  int n_rows = static_cast<int>(src_landmarks_.size());
  unsigned char cv_type = cv::DataType<T>::type;
  src_landmarks->create(n_rows, 3, cv_type);
  const std::vector<LTS5::Vector3<T> >& vertices = source_->get_vertex();
  int i = 0;
  for (auto id_it = src_landmarks_.begin(); id_it != src_landmarks_.end();
       ++id_it, ++i) {
    src_landmarks->at<T>(i, 0) = vertices[*id_it].x_;
    src_landmarks->at<T>(i, 1) = vertices[*id_it].y_;
    src_landmarks->at<T>(i, 2) = vertices[*id_it].z_;
  }
}

#pragma mark -
#pragma mark Usage

/**
 * @name  ComputeTargetLandmarks
 * @fn    int ComputeTargetLandmarks();
 * @brief Given the source mesh, a face tracker and an offscreen renderer,
 *        project the mesh, detect the landmarks and backproject the landmarks
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int RigidAlignment<T>::ComputeTargetLandmarks() {
  int error = 0;
  // @TODO: (Gabriel) For now, everything is in float (OpenGL is float), maybe
  // check with T in the future

  // Render
  offscreen_renderer_->Render();
  // Get image
  cv::Mat img;
  offscreen_renderer_->GetImage(&img);
  // Convert to char (from float)
  img.convertTo(img, CV_8UC1);

  cv::Mat landmarks_set;
  face_tracker_->Detect(img, &landmarks_set);

  // DEBUG PURPOSE
  cv::Mat canvas = face_tracker_->draw_shape(img, landmarks_set,
                                             CV_RGB(0, 0, 255));
  cv::imwrite("data/detected_2dlandmarks.jpg", canvas);

  // Backproject!
  // Transformation matrix from screen to NDC
  cv::Mat Tscreen_ndc = cv::Mat::zeros(4, 4, CV_32F);
  Tscreen_ndc.at<float>(0, 0) = 2.0/im_width_;
  Tscreen_ndc.at<float>(1, 1) = 2.0/im_height_;
  Tscreen_ndc.at<float>(0, 3) = -1.0;
  Tscreen_ndc.at<float>(1, 3) = -1.0;
  Tscreen_ndc.at<float>(2, 3) = -1.0;
  Tscreen_ndc.at<float>(3, 3) = 1.0;

  landmarks_set = landmarks_set.reshape(1,2);
  landmarks_set.convertTo(landmarks_set, CV_32F);
  // Invert y coordinates of the landmarks!
  cv::Mat h_mat = im_height_ * cv::Mat::ones(1, landmarks_set.cols,
                                             landmarks_set.type());
  landmarks_set.row(1) = h_mat - landmarks_set.row(1);
  // And add a row of zeros and a row of ones
  cv::Mat zeros_row = cv::Mat::zeros(1, landmarks_set.cols,
                                     landmarks_set.type());
  cv::Mat ones_row = cv::Mat::ones(1, landmarks_set.cols,
                                   landmarks_set.type());
  landmarks_set.push_back(zeros_row);
  landmarks_set.push_back(ones_row);
  /* landmarks_set is now 4xn
   --- x ---
   --- y ---
   --- 0 ---
   --- 1 ---
   */

  if (weights_.size() == 0) {
    weights_.resize(landmarks_set.cols, 1.0);
  } else {
    assert(weights_.size() == landmarks_set.cols);
  }

  glm::mat4 KcamGL = camera_->projection();
  cv::Mat Kcam(4, 4, CV_32F, glm::value_ptr(KcamGL));
  Kcam = Kcam.t();
  // As we inverted the y coordinates of the landmarks already, we don't need
  // to flip the object again
  glm::mat4 RtcamGL = camera_->view(false);
  cv::Mat Rtcam(4, 4, CV_32F, glm::value_ptr(RtcamGL));
  Rtcam = Rtcam.t();

  // Transform from screen to NDC
  cv::Mat Pi_ndc = Tscreen_ndc * landmarks_set;

  // Transform from NDC to world
  cv::Mat Pi_clip = Kcam.inv() * Pi_ndc;

  // Normalize homogeneous coordinates
  for (int i = 0; i < Pi_clip.cols; ++i) {
    cv::Mat col = Pi_clip.col(i);
    col /=  col.at<float>(3);
  }

  // DEBUG PURPOSE
  //std::cout << Pi_clip << std::endl;
  LTS5::SaveMatToBin("data/rays.bin", Pi_clip);

  // Rays are starting at (0,0,0) and crossing Pi_clip[0:2, :]
  // Transform the mesh in the referential of the camera before backprojecting!
  LTS5::Mesh<T> transformed_mesh;
  transformed_mesh.set_vertex(target_->get_vertex());
  transformed_mesh.set_normal(target_->get_normal());
  transformed_mesh.set_vertex_color(target_->get_vertex_color());
  transformed_mesh.set_triangle(target_->get_triangle());
  transformed_mesh.NormalizeMesh();
  transformed_mesh.Transform(Rtcam);
  transformed_mesh.ComputeBoundingBox();

  // Compute OCTree of the target and intersect with the rays!
//  LTS5::OCTree<T> tree;
//  tree.Insert(transformed_mesh, LTS5::Mesh<T>::PrimitiveType::kTriangle);
//  tree.Build();
  LTS5::AABBTree<T> tree;
  tree.Insert(transformed_mesh);
  tree.Build();

  LTS5::Vector3<T> cam_origin(0.0, 0.0, 0.0);
  trg_landmarks_.clear();
  trg_landmarks_.resize(Pi_clip.cols, -1);

  //DEBUG PURPOSE
  std::vector<LTS5::Vector3<T> > all_intersections;

  // For each landmarks, define ray and test for intersection with the mesh
  for (int landmark_i = 0; landmark_i < Pi_clip.cols; ++landmark_i) {
    bool found_correspondence = false;
    LTS5::Vector3<T> d(Pi_clip.at<float>(0, landmark_i) - cam_origin.x_,
                       Pi_clip.at<float>(1, landmark_i) - cam_origin.y_,
                       Pi_clip.at<float>(2, landmark_i) - cam_origin.z_);
    d.Normalize();
    std::vector<int> idx;
    tree.DoIntersect(cam_origin, d, &idx);
    // DEBUG PURPOSE
    //std::cout << idx.size() << std::endl;

    const std::vector<Triangle>& tri_list = transformed_mesh.get_triangle();
    const std::vector<Vertex>& vertex = transformed_mesh.get_vertex();

    T min_lambda = std::numeric_limits<float>::max();

    for (int id = 0; id < idx.size(); ++id) {
      T lambda;
      const Triangle& tri = tri_list[idx[id]];
      const Vertex& A = vertex[tri.x_];
      const Vertex& B = vertex[tri.y_];
      const Vertex& C = vertex[tri.z_];

      if (LTS5::IntersectTriWithLine(cam_origin, d, A, B, C, &lambda)) {
        // Current lambda is smaller than previous one, triangle is closer to
        // the camera
        LTS5::Vector3<T> intersection = cam_origin + lambda * d;

        // DEBUG PURPOSE
        all_intersections.push_back(intersection);

        if (lambda < min_lambda) {
          found_correspondence = true;
          
          T dist_A = (A-intersection).Norm();
          T dist_B = (B-intersection).Norm();
          T dist_C = (C-intersection).Norm();
          // Check which vertex of the tri is closer to the intersection
          if (dist_A < dist_B && dist_A < dist_C) {
            trg_landmarks_[landmark_i] = tri.x_;
          } else if (dist_B < dist_A && dist_B < dist_C) {
            trg_landmarks_[landmark_i] = tri.y_;
          } else {
            trg_landmarks_[landmark_i] = tri.z_;
          }
          min_lambda = lambda;
        }
      }
    }

    if (!found_correspondence) {
      weights_[landmark_i] = T(0.0);
    }
  }

  // DEBUG PURPOSE
  cv::Mat int_mat((int)all_intersections.size() * 3, 1, cv::DataType<T>::type,
                  (void*)all_intersections.data());
  LTS5::SaveMatToBin("data/intersections.bin", int_mat);

  const auto& v = transformed_mesh.get_vertex();
  cv::Mat vertex_mat((int)v.size() * 3, 1, cv::DataType<T>::type,
                     (void*)v.data());
  LTS5::SaveMatToBin("data/vertex_mat.bin",vertex_mat);

  cv::Mat idx_mat((int)trg_landmarks_.size() , 1, CV_32SC1,
                  (void*)trg_landmarks_.data());
  LTS5::SaveMatToBin("data/idx_mat.bin",idx_mat);
   //*/

  return error;
}

/**
 * @name  ComputeAlignment
 * @fn  int ComputeAlignment()
 * @brief Compute rigid transformation from source to target using landmarks
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int RigidAlignment<T>::ComputeAlignment() {
  int error = 0;
  int d = 3;

  std::vector<LTS5::Vector3<T> > trg_q;
  get_trg_landmarks_vertices(&trg_q); // Only landmarks found (W!=0)
  // Get size (non zero landmarks!)
  int n = static_cast<int>(trg_q.size());
  cv::Mat q(n, d, cv::DataType<T>::type, (void*) trg_q.data());
  std::vector<LTS5::Vector3<T> > src_p;
  get_src_landmarks_vertices(&src_p); // Only landmarks found (W!=0)
  assert(src_p.size() == trg_q.size());
  cv::Mat p(n, d, cv::DataType<T>::type, (void*) src_p.data());


  //--- DEBUG PURPOSE
  cv::Mat target_landmarks_mat((int) trg_landmarks_.size(), 1, CV_32SC1,
                               (void*)trg_landmarks_.data());
  LTS5::SaveMatToBin("data/target_landmarks.bin", target_landmarks_mat);
  cv::Mat source_landmarks_mat((int) src_landmarks_.size(), 1, CV_32SC1,
                               (void*)src_landmarks_.data());
  LTS5::SaveMatToBin("data/source_landmarks.bin", source_landmarks_mat);
  // Save weights
  cv::Mat weights_mat((int) weights_.size(), 1, cv::DataType<T>::type,
                      (void*)weights_.data());
  LTS5::SaveMatToBin("data/weights.bin", weights_mat);
  // Save target
  const auto& trg_v = target_->get_vertex();
  cv::Mat trg_v_mat((int)trg_v.size() * 3, 1, cv::DataType<T>::type,
                    (void*)trg_v.data());
  LTS5::SaveMatToBin("data/target_mesh.bin",trg_v_mat);
  // Save source
  const auto& src_v = source_->get_vertex();
  cv::Mat src_v_mat((int)src_v.size() * 3, 1, cv::DataType<T>::type,
                     (void*)src_v.data());
  LTS5::SaveMatToBin("data/source_mesh.bin",src_v_mat);
  //---


  cv::Mat W = cv::Mat::eye(n, n, p.type());
//  if (this->weights_.size() != 0) {
//    W = cv::Mat::diag(cv::Mat(weights_, true));
//  }

  // 1) Compute centroids q_centered and p_centered
  // Take the mean along the columns (first dimension)
  cv::Mat p_cog, q_cog;
  cv::reduce(p, p_cog, 0, CV_REDUCE_AVG);
  cv::reduce(q, q_cog, 0, CV_REDUCE_AVG);

  // 2) Center the vectors X = p - p_cog and Y = q - q_cog
  cv::Mat X(n, d, p.type());
  cv::Mat Y(n, d, q.type());
  for (int i = 0; i < n; ++i) {
    X.row(i) = p.row(i) - p_cog;
    Y.row(i) = q.row(i) - q_cog;
  }

  // 3) Compute covariance matrix S = X W Y^T   (d x d)
  cv::Mat Xt = X.t();
  cv::Mat covS = Xt * W * Y;
  //      dxd  =dxn  nxn nxd

  // 4) SVD of S = U Sig V^T
  //    R = V diag(1 1 det(VU^T)) U^T
  //    s = Sum y^T R x / Sum x^T x
  cv::Mat sig_mat, u_mat, vt_mat;
  cv::SVD::compute(covS, sig_mat, u_mat, vt_mat);

  cv::Mat Rotation = vt_mat.t() * u_mat.t();
  double det = cv::determinant(Rotation);

  if (det < 0) {
    Rotation.row(2) = det * Rotation.row(2);
  }

  // Get scale
  T num = 0.0;
  T denum = 0.0;
  for (int i = 0; i < n; ++i) {
    cv::Mat res = Y.row(i) * Rotation * Xt.col(i);
    num += res.at<T>(0);
    denum += X.row(i).dot(X.row(i));
  }

  R_ = Rotation;
  if (denum > 1e3 * std::numeric_limits<T>::epsilon()) {
    scale_ = num/denum;
  } else {
    scale_ = 1;
    error = -1;
  }

  // For the translation: center, rescale, compute translation
  // 5) t = q_centered - s * R * p_centered
  cv::Mat translation = q_cog.t() - scale_ * Rotation * p_cog.t();
  t_.x_ = translation.at<T>(0);
  t_.y_ = translation.at<T>(1);
  t_.z_ = translation.at<T>(2);

  // Compute error (difference between 2 sets of landmarks)
  cv::Mat big_translation;
  cv::repeat(translation, 1, n, big_translation);
  cv::Mat diff = scale_ * R_ * p.t() + big_translation - q.t();
  cv::Mat metric = (diff.t()*diff).diag(0);
  cv::sqrt(metric, metric);
  error_ = cv::sum(metric)[0];

  // DEBUG PURPOSE
  std::cout << "[RigidAlignment::ComputeAlignment] Resuls: " << std::endl;
  std::cout << "                                   R = " << std::endl <<
  R_ << std::endl;
  std::cout << "                                   t = " << t_ << std::endl;
  std::cout << "                                   s = " << scale_ << std::endl;
  std::cout << "                                   Sum of || x - y || = " <<
  error_ << std::endl;
  std::cout << "                                   Mean   || x - y || = " <<
  error_ / n << std::endl;

  return error;
}

/*
 * @name  AlignSource
 * @fn  int AlignSource()
 * @brief Compute the rigid transformation from source to target and apply it
 * @return 0 if success, -1 otherwise
 */
template<typename T>
int RigidAlignment<T>::AlignSource() {
  int error = ComputeAlignment();

  LTS5::Vector3<T> source_pos = source_->Cog();
  LTS5::Vector3<T> minus_cog(-source_pos.x_, -source_pos.y_, -source_pos.z_);

  source_->Translate(minus_cog);

  // Rotation in homogeneous coordinates
  cv::Mat rot = cv::Mat::eye(4, 4, CV_32F);
  R_.copyTo(rot(cv::Rect(0, 0, 3, 3)));

  source_->Transform(rot);
  source_->Scale(scale_);

  source_->Translate(source_pos);
  source_->Translate(t_);

  // DEBUG PURPOSE
  // Save aligned source
//  const auto& algn_v = source_->get_vertex();
//  cv::Mat algn_v_mat((int)algn_v.size() * 3, 1, cv::DataType<T>::type,
//                    (void*)algn_v.data());
//  LTS5::SaveMatToBin("data/aligned_mesh.bin",algn_v_mat);

  return error;
}

/*
 * @name  AlignSourceOpenCV
 * @fn  int AlignSourceOpenCV()
 * @brief Use OpenCV algo to compute alignment
 * @return 0 if sucess, -1 otherwise
 */
template<typename T>
int RigidAlignment<T>::AlignSourceOpenCV() {
  int error = 0;

  std::vector<LTS5::Vector3<T> > src_landmarks;
  this->get_src_landmarks_vertices(&src_landmarks);
  cv::Mat src_landmarks_mat((int)src_landmarks.size(), 3, cv::DataType<T>::type,
                            (void*) src_landmarks.data());

  std::vector<LTS5::Vector3<T> > trg_landmarks;
  this->get_trg_landmarks_vertices(&trg_landmarks);
  cv::Mat trg_landmarks_mat((int)trg_landmarks.size(), 3, cv::DataType<T>::type,
                            (void*) trg_landmarks.data());

  cv::Mat affine(3, 4, CV_32FC1);
  cv::Mat inliers;
  double threshold = 0.15;
  cv::estimateAffine3D(src_landmarks_mat, trg_landmarks_mat, affine, inliers, threshold);

  std::cout << inliers << std::endl;

  cv::Mat transform = cv::Mat::eye(4, 4, CV_32FC1);
  affine.copyTo(transform(cv::Rect(0,0,4,3)));

  std::cout << transform << std::endl;

  source_->Transform(transform);

  // DEBUG PURPOSE
  // Save aligned source
  const auto& algn_v = source_->get_vertex();
  cv::Mat algn_v_mat((int)algn_v.size() * 3, 1, cv::DataType<T>::type,
                     (void*)algn_v.data());
  LTS5::SaveMatToBin("data/aligned_mesh.bin",algn_v_mat);

  return error;
}


#pragma mark -
#pragma mark Declaration

/** Float TemplateAlignment */
template class RigidAlignment<float>;
/** Double TemplateAlignment */
template class RigidAlignment<double>;
