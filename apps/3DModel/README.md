---
title: Review documentation
author: Gabriel Cuendet
date: 2016-11-07
---
This document describes the procedure that must be followed when reviewing the
3d reconstructions computed from the data recorded during the summer of 2014. 

# Goal of the review process
The goal of this review is to make sure that each and every reconstruction:
	
- contains the correct expression (the expression corresponding to the name of
the file) 

- contains the correct subject (same subject across all reconstructions with 
the same subject number)

- is of sufficient quality

## Tool
In order to navigate quickly through the 3d reconstructions, as well as through
the raw data that have been used to generate that reconstruction, a graphical
tool has been designed (cf. fig.1).

![Screenshot of the GUI](resource/screenshot.jpg)

That tool is ```CheckReconstructions``` and can be
compiled from the lts5-face git repository:

```
lts5-face/apps/3DModel/
```

### Dependencies
#### Pigz
In order to accelerate the process of unzipping large archive (such as the raw
binary files), **pigz** is used. It is a parallel implementation of gzip for 
modern multi-processor, multi-core machines. It can be installed via apt-get:

```
sudo apt-get install pigz
```

Or by downloading the source [here][pigz_sources] and compiling.

### Usage
It takes the following arguments:

```
-r (REQUIRED) recordings (.bin files) directory
-d (OPTIONAL) reconstruction meshes (.ply) directory
-m (OPTIONAL) mesh filename
-t (REQUIRED) temp dir path
-o (REQUIRED) output CSV file path
```

By default, the following arguments are recommended: 

- ```-r``` followed by the path to a directory containing the raw binary color
and depth streams recorded by the kinect

- ```-d``` followed by the path to a directory containing the 3d reconstructions.
This path can also be set from within the application but that is not
recommended.

- ```-o``` followed by the path to a CSV file which will contain the results of
the review. An empty CSV file with the correct headers is available to use in 
```lts5-face/apps/3DModel/output/review.csv```.

- ```-t``` followed by the path to a temporary folder which will be used to
unzip raw binary files. These temporary files are not deleted automatically and
can take a large amount of disk space (several GB). It is recommended to
manually empty that folder every once in a while or when disk space becomes 
insufficient.

### Workflow and user interface
When first launched, the tool does not load any reconstruction. The user needs 
to manually specified a _user ID_ and an _expression ID_ in the palette entitled
**Recontruction**.

The 3d reconstruction should load and be displayed in the window. In parallel, 
the raw color stream is unzipped in the temp folder and the last frame used for
the reconstruction is displayed in the palette entitled **Current frame**. This 
process can take some time (~10 seconds) and a message will be displayed in the 
command line when the frame is loaded:

```
[AppView::LoaderLoop] Loading XXX_RawColor.bin
[AppView::LoaderLoop] Done!
```

The reviewer should then take the following steps:

1) Select the last valid frame with the slider and text box in the palette
**Current frame**. Do it even if the 3d reconstruction looks good!
_Tips: there might be a small gap between frames in the stream, in that case,
the last valid frame is the last frame between that gap._

2) Turn to the palette **Review**. Update the last frame (use the update symbol
or renter the frame number in the text box) according to what you found at the 
previous step. 

3) If the expression and/or the subject are not correct, check the corresponding
check box(es).

4) Finally, validate your review by selecting the quality: either ```good```,
or ```bad```.
**Important: If you updated the last frame, select ```bad```, irrespectively of
the actual quality of the reconstruction.** 


## Data
The data are available on the shared disk ```Data/``` on the machine
```lts5pc4``` in the following subfolders:

- 3d reconstructions: ```Data/3d_reconstructions/2016-11-01/``` in a flat
folder structure.

- Raw binary recordings: ```Data/3d/``` organized by dates and sessions.


<!-- LINKS -->
[pigz_sources]: http://zlib.net/pigz/pigz-2.3.4.tar.gz