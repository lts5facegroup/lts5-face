//
//  kfusion_parameters.hpp
//  LTS5-Dev
//
//  Created by Gabriel Cuendet on 28.10.16.
//  Copyright © 2016 Ecabert Christophe. All rights reserved.
//

#ifndef kfusion_parameters_h
#define kfusion_parameters_h

#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>

#include "opencv2/core/core.hpp"
#include <opencv2/core/affine.hpp>

struct KinFuParams {
  struct Intr {
    float fx, fy, cx, cy;

    Intr () {}

    Intr (float fx_, float fy_, float cx_, float cy_) : fx(fx_), fy(fy_),
    cx(cx_), cy(cy_) {}

    Intr operator()(int level_index) const {
      int div = 1 << level_index;
      return (Intr (fx / div, fy / div, cx / div, cy / div));
    }
  };

  int cols;  //pixels
  int rows;  //pixels

  /** Color integration */
  bool integrate_color;
  /** Camera parameters */
  Intr intr;

  /** number of voxels for the TSDF volume */
  cv::Vec3i tsdf_volume_dims;
  /** number of voxels for the color volume (typically <= TSDF volume) */
  cv::Vec3i color_volume_dims;
  cv::Vec3f volume_size; //meters
  /** inital pose */
  cv::Affine3f volume_pose; //meters

  float bilateral_sigma_depth;    //meters
  float bilateral_sigma_spatial;  //pixels
  int   bilateral_kernel_size;    //pixels

  float icp_truncate_depth_dist;  //meters
  float icp_dist_thres;           //meters
  float icp_angle_thres;          //radians
  /** iterations for level index 0,1,..,3 */
  std::vector<int> icp_iter_num;

  /** integrate only if exceedes tsdf_trunc_dist*/
  float tsdf_min_camera_movement; //meters
  float tsdf_trunc_dist;          //meters
  int tsdf_max_weight;            //frames

  int color_max_weight;           //frames

  float raycast_step_factor;   // in voxel sizes
  float gradient_delta_factor; // in voxel sizes

  cv::Vec3f light_pose; //meters

  /**
   * @name  KinFuParams
   * @fn  KinFuParams()
   * @brief Default constructor with default values
   */
  KinFuParams() {
    //const int iters[] = {10, 5, 4, 0};
    //const int levels = sizeof(iters)/sizeof(iters[0]);

    this->cols = 640;  //pixels
    this->rows = 480;  //pixels
    this->integrate_color = false;
    this->intr = Intr(525.f, 525.f, this->cols/2 - 0.5f, this->rows/2 - 0.5f);

    this->tsdf_volume_dims = cv::Vec3i::all(512);  //number of voxels
    this->color_volume_dims = cv::Vec3i::all(256);  //number of voxels
    this->volume_size = cv::Vec3f::all(3.f);  //meters
    this->volume_pose = cv::Affine3f().translate(cv::Vec3f(-this->volume_size[0]/2,
                                                           -this->volume_size[1]/2,
                                                           0.5f));

    this->bilateral_sigma_depth = 0.04f;  //meter
    this->bilateral_sigma_spatial = 4.5; //pixels
    this->bilateral_kernel_size = 7;     //pixels

    this->icp_truncate_depth_dist = 0.f;        //meters, disabled
    this->icp_dist_thres = 0.1f;                //meters
    this->icp_angle_thres = 0.017453293f * 30.f; //radians
    this->icp_iter_num.assign({10, 5, 4, 0});

    this->tsdf_min_camera_movement = 0.f; //meters, disabled
    this->tsdf_trunc_dist = 0.04f; //meters;
    this->tsdf_max_weight = 64;   //frames

    this->color_max_weight = 64;   //frames

    this->raycast_step_factor = 0.75f;  //in voxel sizes
    this->gradient_delta_factor = 0.5f; //in voxel sizes

    //this->light_pose = p.volume_pose.translation()/4; //meters
    this->light_pose = cv::Vec3f::all(0.f); //meters
  }

  /**
   * @name  ~KinFuParams
   * @fn  ~KinFuParams()
   * @brief Default destructor
   */
  ~KinFuParams() {}

  /**
   * @name  WriteConfig
   * @fn  int WriteConfig(const std::string& config_path) const
   * @brief Parse the config file and sets the kfusion parameters
   * @param[in]  config_path  path to the config file to parse
   * @return 0 if success, -1 otherwise
   */
  int WriteConfig(const std::string& config_path) const {
    int err = 0;

    cv::FileStorage storage(config_path, cv::FileStorage::WRITE);
    if (!storage.isOpened()) {
      std::cout << "[WriteConfig] ERROR: Unable to open configuration file" <<
      std::endl;
      return -1;
    }

    storage << "kfusion_params" << "{";
    storage << "cols" <<                this->cols;
    storage << "rows" <<                this->rows;
    storage << "integrate_color" <<(int)this->integrate_color;

    storage << "intr" << "{";
    storage << "fx" <<                  this->intr.fx;
    storage << "fy" <<                  this->intr.fy;
    storage << "cx" <<                  this->intr.cx;
    storage << "cy" <<                  this->intr.cy;
    storage << "}";

    storage << "tsdf_volume_dims" <<    this->tsdf_volume_dims;
    storage << "color_volume_dims" <<   this->color_volume_dims;
    storage << "volume_size" <<         this->volume_size;
    storage << "volume_pose" << cv::Mat(this->volume_pose.matrix);

    storage << "bilateral_sigma_depth" <<   this->bilateral_sigma_depth;
    storage << "bilateral_sigma_spatial" << this->bilateral_sigma_spatial;
    storage << "bilateral_kernel_size" <<   this->bilateral_kernel_size;
    storage << "icp_truncate_depth_dist" << this->icp_truncate_depth_dist;
    storage << "icp_dist_thres" <<          this->icp_dist_thres;
    storage << "icp_angle_thres" <<         this->icp_angle_thres;


    storage << "icp_iter_num" <<            this->icp_iter_num;
    storage << "tsdf_min_camera_movement" <<this->tsdf_min_camera_movement;
    storage << "tsdf_trunc_dist" <<         this->tsdf_trunc_dist;
    storage << "tsdf_max_weight" <<         this->tsdf_max_weight;
    storage << "color_max_weight" <<        this->color_max_weight;

    storage << "raycast_step_factor" <<     this->raycast_step_factor;
    storage << "gradient_delta_factor" <<   this->gradient_delta_factor;
    storage << "light_pose" <<              this->light_pose;
    storage << "}";

    storage.release();
    return err;
  }

  /**
   * @name  ParseConfig
   * @fn  int ParseConfig(const std::string& config_path)
   * @brief Parse the config file and sets the kfusion parameters
   * @param[in]  config_path  path to the config file to parse
   * @return 0 if success, -1 otherwise
   */
  int ParseConfig(const std::string& config_path) {
    int err = 0;

    // http://docs.opencv.org/2.4/doc/tutorials/core/file_input_output_with_xml_yml/file_input_output_with_xml_yml.html
    cv::FileStorage storage(config_path, cv::FileStorage::READ);
    if (storage.isOpened()) {
      // read config file
      // Go root
      int integrate_color_int = 0;
      cv::FileNode root = storage.getFirstTopLevelNode();

      root["cols"] >> this->cols;
      root["rows"] >> this->rows;
      root["integrate_color"] >> integrate_color_int;
      this->integrate_color = static_cast<bool>(integrate_color_int);

      root["intr"]["fx"] >> this->intr.fx;
      root["intr"]["fy"] >> this->intr.fy;
      root["intr"]["cx"] >> this->intr.cx;
      root["intr"]["cy"] >> this->intr.cy;

      root["tsdf_volume_dims"] >> this->tsdf_volume_dims;
      root["color_volume_dims"] >> this->color_volume_dims;
      root["volume_size"] >> this->volume_size;

      cv::Mat pose_temp(4, 4, CV_32FC1);
      root["volume_pose"] >> pose_temp;
      this->volume_pose.matrix = pose_temp.clone();

      root["bilateral_sigma_depth"] >> this->bilateral_sigma_depth;
      root["bilateral_sigma_spatial"] >> this->bilateral_sigma_spatial;
      root["bilateral_kernel_size"] >> this->bilateral_kernel_size;
      root["icp_truncate_depth_dist"] >> this->icp_truncate_depth_dist;
      root["icp_dist_thres"] >> this->icp_dist_thres;
      root["icp_angle_thres"] >> this->icp_angle_thres;

      root["icp_iter_num"] >> this->icp_iter_num;
      root["tsdf_min_camera_movement"] >> this->tsdf_min_camera_movement;
      root["tsdf_trunc_dist"] >> this->tsdf_trunc_dist;
      root["tsdf_max_weight"] >> this->tsdf_max_weight;
      root["color_max_weight"] >> this->color_max_weight;

      root["raycast_step_factor"] >> this->raycast_step_factor;
      root["gradient_delta_factor"] >> this->gradient_delta_factor;
      root["light_pose"] >> this->light_pose;

    } else {
      err = -1;
      std::cout << "[ParseConfig] ERROR: Unable to open configuration file" <<
      std::endl;
    }

    return err;
  }

  /**
   * @name SerializeParameters
   * @fn int SerializeParameters(std::vector<std::string>* serial_params)
   * @brief Write the parameters values to a vector of string (typically
   *        useful to dump parameters as comments embedded in a .ply file)
   * @param[out] serial_params  Serialized parameters into a vector of string
   * @return 0 if success, -1 otherwise
   */
  int SerializeParameters(std::vector<std::string>* serial_params) const {
    serial_params->clear();

    serial_params->push_back(std::string("kfusion_params:"));
    serial_params->push_back(std::string("    cols: ") + std::to_string(this->cols));
    serial_params->push_back(std::string("    rows: ") + std::to_string(this->rows));
    serial_params->push_back(std::string("    integrate_color: ") + std::to_string(this->integrate_color));

    serial_params->push_back(std::string("    intr:"));
    serial_params->push_back(std::string("        fx: ") + std::to_string(this->intr.fx));
    serial_params->push_back(std::string("        fy: ") + std::to_string(this->intr.fy));
    serial_params->push_back(std::string("        cx: ") + std::to_string(this->intr.cx));
    serial_params->push_back(std::string("        cy: ") + std::to_string(this->intr.cy));

    serial_params->push_back(std::string("    tsdf_volume_dims: [ ")
                             + std::to_string(this->tsdf_volume_dims[0]) + std::string(" ")
                             + std::to_string(this->tsdf_volume_dims[1]) + std::string(" ")
                             + std::to_string(this->tsdf_volume_dims[2]) + " ]");
    serial_params->push_back(std::string("    color_volume_dims: [")
                             + std::to_string(this->color_volume_dims[0]) + std::string(" ")
                             + std::to_string(this->color_volume_dims[1]) + std::string(" ")
                             + std::to_string(this->color_volume_dims[2]) + " ]");
    serial_params->push_back(std::string("    volume_size: [")
                             + std::to_string(this->volume_size[0]) + std::string(" ")
                             + std::to_string(this->volume_size[1]) + std::string(" ")
                             + std::to_string(this->volume_size[2]) + " ]");

    serial_params->push_back(std::string("    volume_pose: [ ") + std::to_string(this->volume_pose.matrix.val[0]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[1]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[2]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[3]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[4]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[5]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[6]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[7]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[8]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[9]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[10]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[11]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[12]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[13]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[14]) + std::string(" ")
                             + std::to_string(this->volume_pose.matrix.val[15]) + " ]");

    serial_params->push_back(std::string("    bilateral_sigma_depth: ") + std::to_string(this->bilateral_sigma_depth));
    serial_params->push_back(std::string("    bilateral_sigma_spatial: ") + std::to_string(this->bilateral_sigma_spatial));
    serial_params->push_back(std::string("    bilateral_kernel_size: ") + std::to_string(this->bilateral_kernel_size));
    serial_params->push_back(std::string("    icp_truncate_depth_dist: ") + std::to_string(this->icp_truncate_depth_dist));
    serial_params->push_back(std::string("    icp_dist_thres: ") + std::to_string(this->icp_dist_thres));
    serial_params->push_back(std::string("    icp_angle_thres: ") + std::to_string(this->icp_angle_thres));

    serial_params->push_back(std::string("    icp_iter_num: [ ")
                             + std::to_string(this->icp_iter_num[0]) + std::string(" ")
                             + std::to_string(this->icp_iter_num[1]) + std::string(" ")
                             + std::to_string(this->icp_iter_num[2]) + " ]");

    serial_params->push_back(std::string("    tsdf_min_camera_movement: ") + std::to_string(this->tsdf_min_camera_movement));
    serial_params->push_back(std::string("    tsdf_trunc_dist: ") + std::to_string(this->tsdf_trunc_dist));
    serial_params->push_back(std::string("    tsdf_max_weight: ") + std::to_string(this->tsdf_max_weight));
    serial_params->push_back(std::string("    color_max_weight: ") + std::to_string(this->color_max_weight));
    serial_params->push_back(std::string("    raycast_step_factor: ") + std::to_string(this->raycast_step_factor));
    serial_params->push_back(std::string("    gradient_delta_factor: ") + std::to_string(this->gradient_delta_factor));

    serial_params->push_back(std::string("    light_pose: [ ")
                             + std::to_string(this->light_pose[0]) + std::string(" ")
                             + std::to_string(this->light_pose[1]) + std::string(" ")
                             + std::to_string(this->light_pose[2]) + " ]");
    return 0;
  }

  /**
   * @name DeserializeParameters
   * @fn int DeserializeParameters(const std::vector<std::string>& serial_params)
   * @brief Read the parameters values from a vector of string (typically
   *        useful to load parameters from comments in .ply file)
   * @param[in] serial_params  Serialized parameters into a vector of string
   * @return 0 if success, -1 otherwise
   */
  int DeserializeParameters(const std::vector<std::string>& serial_params) {
    int error = -1;

    auto pos_it = std::find_if(serial_params.begin(), serial_params.end(),
                               [](const std::string& s) {
                                 return s.compare("kfusion_params:") == 0;
                               });

    if (pos_it == serial_params.end()) {
      return error;
    } else {
      pos_it++;
      this->cols = std::stoi(pos_it->substr(6, pos_it->length() - 6));
      pos_it++;
      this->rows = std::stoi(pos_it->substr(6, pos_it->length() - 6));
      pos_it++;
      this->integrate_color = std::stoi(pos_it->substr(17, pos_it->length() - 17)) == 1;

      pos_it += 2;
      this->intr.fx = std::stof(pos_it->substr(4, pos_it->length() - 4));
      pos_it++;
      this->intr.fy = std::stof(pos_it->substr(4, pos_it->length() - 4));
      pos_it++;
      this->intr.cx = std::stof(pos_it->substr(4, pos_it->length() - 4));
      pos_it++;
      this->intr.cy = std::stof(pos_it->substr(4, pos_it->length() - 4));

      pos_it++;
      std::vector<int> tsdf_vol_d = this->DeserializeArray<int, 3>(pos_it->substr(20, pos_it->length() - 20));
      this->tsdf_volume_dims[0] = tsdf_vol_d[0];
      this->tsdf_volume_dims[1] = tsdf_vol_d[1];
      this->tsdf_volume_dims[2] = tsdf_vol_d[2];
      pos_it++;
      std::vector<int> col_vol_d = this->DeserializeArray<int, 3>(pos_it->substr(20, pos_it->length() - 20));
      this->color_volume_dims[0] = col_vol_d[0];
      this->color_volume_dims[1] = col_vol_d[1];
      this->color_volume_dims[2] = col_vol_d[2];
      pos_it++;
      std::vector<int> vol_s = this->DeserializeArray<int, 3>(pos_it->substr(14, pos_it->length() - 14));
      this->volume_size[0] = vol_s[0];
      this->volume_size[1] = vol_s[1];
      this->volume_size[2] = vol_s[2];
      pos_it++;
      std::vector<float> vol_p = this->DeserializeArray<float, 16>(pos_it->substr(15, pos_it->length() - 15));
      this->volume_pose.matrix.val[0] = vol_p[0];
      this->volume_pose.matrix.val[1] = vol_p[1];
      this->volume_pose.matrix.val[2] = vol_p[2];
      this->volume_pose.matrix.val[3] = vol_p[3];
      this->volume_pose.matrix.val[4] = vol_p[4];
      this->volume_pose.matrix.val[5] = vol_p[5];
      this->volume_pose.matrix.val[6] = vol_p[6];
      this->volume_pose.matrix.val[7] = vol_p[7];
      this->volume_pose.matrix.val[8] = vol_p[8];
      this->volume_pose.matrix.val[9] = vol_p[9];
      this->volume_pose.matrix.val[10] = vol_p[10];
      this->volume_pose.matrix.val[11] = vol_p[11];
      this->volume_pose.matrix.val[12] = vol_p[12];
      this->volume_pose.matrix.val[13] = vol_p[13];
      this->volume_pose.matrix.val[14] = vol_p[14];
      this->volume_pose.matrix.val[15] = vol_p[15];

      pos_it++;
      this->bilateral_sigma_depth = std::stof(pos_it->substr(23, pos_it->length() - 23));
      pos_it++;
      this->bilateral_sigma_spatial = std::stof(pos_it->substr(25, pos_it->length() - 25));
      pos_it++;
      this->bilateral_kernel_size = std::stoi(pos_it->substr(23, pos_it->length() - 23));
      pos_it++;
      this->icp_truncate_depth_dist = std::stof(pos_it->substr(25, pos_it->length() - 25));
      pos_it++;
      this->icp_dist_thres = std::stof(pos_it->substr(16, pos_it->length() - 16));
      pos_it++;
      this->icp_angle_thres = std::stof(pos_it->substr(17, pos_it->length() - 17));

      pos_it++;
      std::vector<int> icp_iter = this->DeserializeArray<int, 3>(pos_it->substr(16, pos_it->length() - 16));
      this->icp_iter_num[0] = icp_iter[0];
      this->icp_iter_num[1] = icp_iter[1];
      this->icp_iter_num[2] = icp_iter[2];

      //tsdf_min_camera_movement
      pos_it++;
      this->tsdf_min_camera_movement = std::stof(pos_it->substr(26, pos_it->length() - 26));
      pos_it++;
      this->tsdf_trunc_dist = std::stof(pos_it->substr(17, pos_it->length() - 17));
      pos_it++;
      this->tsdf_max_weight = std::stoi(pos_it->substr(17, pos_it->length() - 17));
      pos_it++;
      this->color_max_weight = std::stoi(pos_it->substr(18, pos_it->length() - 18));
      pos_it++;
      this->raycast_step_factor = std::stof(pos_it->substr(21, pos_it->length() - 21));
      pos_it++;
      this->gradient_delta_factor = std::stof(pos_it->substr(23, pos_it->length() - 23));

      pos_it++;
      std::vector<float> light_p = this->DeserializeArray<float, 3>(pos_it->substr(14, pos_it->length() - 14));
      this->light_pose[0] = light_p[0];
      this->light_pose[1] = light_p[1];
      this->light_pose[2] = light_p[2];

      return 0;
    }
  }

  template<typename T, int N>
  std::vector<T> DeserializeArray(const std::string& string) {
    std::stringstream ss(string);
    std::vector<T> arr(N);
    for (int i = 0; i < arr.size(); ++i) {
      ss >> arr[i];
    }

    return arr;
  }
};


#endif /* kfusion_parameters_h */
