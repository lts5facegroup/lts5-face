/**
 *  @file   alignment_config.hpp
 *  @brief  Structure containing the configuration information for the alignment
 *
 *  @author Gabriel Cuendet
 *  @date   09.11.16
 *  Copyright © 2016 Cuendet Gabriel. All rights reserved.
 */

#ifndef __LTS5_alignment_config__
#define __LTS5_alignment_config__

#include "opencv2/core/core.hpp"

#include "lts5/utils/file_io.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
struct AlignConfig {

  /** Path to the file containing the 68 landmarks indices annotations */
  std::string annotations_path;
  /** Path to the file containing the indices of vertices to consider in src */
  std::string src_mask_path;
  /** Path to the file containing the sdm model*/
  std::string sdm_model_path;
  /** Path to the file containing the face detector model */
  std::string face_detector_path;
  /** Central frquency of the MHT*/
  double centre_freq;
  /** Number of frequencies in the MHT*/
  int no_freqs;
  /** Vector of weights for the 68 landmarks */
  std::vector<double> landmark_weights;
  /** MPU max error */
  double mpu_max_error;
  /** MPU max depth*/
  int mpu_max_depth;
  /** MPU alpha factor for the radius */
  double mpu_alpha_radius;

  /**
   * @name  AlignConfig
   * @fn  AlignConfig(void)
   * @brief Default constructor of the class
   */
  AlignConfig(void) : annotations_path(""), src_mask_path(""), sdm_model_path(""),
  face_detector_path(""), centre_freq(0.0), no_freqs(0), landmark_weights(),
  mpu_max_error(1e-6), mpu_max_depth(8), mpu_alpha_radius(0.75) {}

  /**
   * @name  AlignConfig
   * @fn  AlignConfig(const AlignConfig& other)
   * @brief Default copy constructor
   * @param[in]  other  Object to copy from
   */
  AlignConfig(const AlignConfig& other) = default;

  /**
   * @name  operator=
   * @fn  AlignConfig& operator=(const AlignConfig& other)
   * @brief Default copy assignment operator
   * @param[in]  other  Object to assign from
   * @return Ref to this
   */
  AlignConfig& operator=(const AlignConfig& other) = default;

  /**
   * @name  Read
   * @fn  int Read(const cv::FileStorage& storage)
   * @brief Read parameters of the struct from a cv::FileStorage
   * @param[in]  storage  Already opened cv::FileStorage
   * @return 0 if success, -1 otherwise
   */
  int Read(const cv::FileStorage& storage) {
    if (!storage.isOpened()) {
      std::cout << "[AlignConfig::Read] ERROR: " <<
      "Opening config file failed!" << std::endl;
      return -1;
    }

    // Go root
    cv::FileNode root = storage.getFirstTopLevelNode();
    LTS5::ReadNode(root, "annotations_path", &annotations_path);
    LTS5::ReadNode(root, "src_mask_path", &src_mask_path);
    LTS5::ReadNode(root, "sdm_model_path", &sdm_model_path);
    LTS5::ReadNode(root, "face_detector_path", &face_detector_path);
    LTS5::ReadNode(root, "centre_freq", &centre_freq);
    LTS5::ReadNode(root, "no_freqs", &no_freqs);
    LTS5::ReadNode(root, "weights", &landmark_weights);
    LTS5::ReadNode(root, "mpu_max_error", &mpu_max_error);
    LTS5::ReadNode(root, "mpu_max_depth", &mpu_max_depth);
    LTS5::ReadNode(root, "mpu_alpha_radius", &mpu_alpha_radius);

    return 0;
  }

  /**
   * @name  Load
   * @fn  int Load(const std::string& filepath)
   * @brief Load the struct parameters from a file
   * @param[in]  filepath  Path to the file to load the config from
   * @return 0 if success, -1 otherwise
   */
  int Load(const std::string& filepath) {
    cv::FileStorage storage(filepath, cv::FileStorage::READ);
    int error = Read(storage);
    storage.release();

    return error;
  }

};
}  // namespace LTS5

#endif /* _LTS5_alignment_config__ */
