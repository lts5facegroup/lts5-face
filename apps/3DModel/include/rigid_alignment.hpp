/**
 *  @file   rigid_alignment.hpp
 *  @brief  Rigid alignment declaration
 *
 *  @author Gabriel Cuendet
 *  @date   2016-09-29
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef __rigid_alignment_hpp__
#define __rigid_alignment_hpp__

#include <stdio.h>
#include <vector>

#include "opencv2/core.hpp"

#include "lts5/geometry/mesh.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/ogl/offscreen_renderer.hpp"
#include "lts5/ogl/camera.hpp"
#include "lts5/utils/library_export.hpp"
#include "lts5/utils/math/vector.hpp"

/**
 *  @class  RigidAlignment
 *  @brief  Rigid alignement of two face meshes using detected landmarks
 *  @author Gabriel Cuendet
 *  @date   2016-09-29
 */
template<typename T>
class LTS5_EXPORTS RigidAlignment {
 public :

  /** Vertex type definition */
  using Vertex = typename LTS5::Mesh<T>::Vertex;
  /** Triangle type definition */
  using Triangle = typename LTS5::Mesh<T>::Triangle;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  RigidAlignment
   * @fn  RigidAlignment(const LTS5::Mesh<T>& target)
   * @brief Constructor of the class
   * @param[in]  target         Target mesh
   * @param[in]  source         Source mesh (template)
   */
  RigidAlignment(const LTS5::Mesh<T>& target, LTS5::Mesh<T>* source);

  /**
   * @name  ~RigidAlignment
   * @fn  ~RigidAlignment()
   * @brief Destructor of the class
   */
  ~RigidAlignment();

  /**
   * @name  LoadSrcLandmarks
   * @fn  int LoadSrcLandmarks(const std::string& filename)
   * @brief Load the source landmarks from a file
   * @param[in]  filename  File path containing the annotations
   * @return 0 if success, -1 otherwise
   */
  int LoadSrcLandmarks(const std::string& filename);

  /**
   * @name  InitFaceTracker
   * @fn  int InitFaceTracker(const std::string& model_file,
   *                          const std::string& face_det_file);
   * @brief Initialize the face tracker with given model files
   * @param[in]  model_file    Bin file containing face tracker model
   * @param[in]  face_det_file File containing the face detector config
   * @return 0 if success, -1 otherwise
   */
  int InitFaceTracker(const std::string& model_file,
                      const std::string& face_det_file);

  /**
   * @name  InitOffscreenRenderer
   * @fn  int InitOffscreenRenderer();
   * @brief Initialize the offscreen renderer with the correct shaders and all
   * @return 0 if success, -1 otherwise
   */
  int InitOffscreenRenderer();

#pragma mark -
#pragma mark Getters and Setters

  /**
   * @name  src_landmarks
   * @fn  const std::vector<int>& src_landmarks(void) const
   * @brief Getter for the source landmarks
   * @return const ref to the source landmarks
   */
  const std::vector<int>& src_landmarks(void) const {
    return src_landmarks_;
  }

  /**
   * @name  trg_landmarks
   * @fn  const std::vector<int>& trg_landmarks(void) const
   * @brief Getter for trg_landmarks
   * @return const ref to target landmarks
   */
  const std::vector<int>& trg_landmarks(void) const {
    return trg_landmarks_;
  }

  /**
   * @name  set_trg_landmarks
   * @fn  void set_trg_landmarks(const std::vector<int>& landmarks)
   * @brief Set the target landmarks (should be detected!)
   * @param[in]  landmarks  Landmarks to set
   */
  void set_trg_landmarks(const std::vector<int>& landmarks) {
    trg_landmarks_ = landmarks;
  }

  /**
   * @name  get_trg_landmarks_vertices
   * @fn  void get_trg_landmarks_vertices(std::vector<LTS5::Vector3<T> >* vertices) const
   * @brief Getter for the vertices corresponding to the target landmarks
   * @param[out] vertices  List of vertices corresponding to the target landmarks
   */
  void get_trg_landmarks_vertices(std::vector<LTS5::Vector3<T> >* vertices) const;

  /**
   * @name  get_trg_landmarks_mat
   * @fn  void get_trg_landmarks_mat(cv::Mat* trg_landmarks) const
   * @brief Get target landmarks as an OpenCV Mat
   * @param[out] trg_landmarks  OpenCV Mat containing trg landmarks in the rows
   */
  void get_trg_landmarks_mat(cv::Mat* trg_landmarks) const;

  /**
   * @name  get_src_landmarks_vertices
   * @fn  void get_src_landmarks_vertices(std::vector<LTS5::Vector3<T> >* vertices) const
   * @brief Getter for the vertices corresponding to the source landmarks
   * @param[out] vertices  List of vertices corresponding to the source landmarks
   */
  void get_src_landmarks_vertices(std::vector<LTS5::Vector3<T> >* vertices) const;

  /**
   * @name  get_src_landmarks_mat
   * @fn  void get_src_landmarks_mat(cv::Mat* src_landmarks) const
   * @brief Get source landmarks as an OpenCV Mat
   * @param[out] src_landmarks  OpenCV Mat containing src landmarks in the rows
   */
  void get_src_landmarks_mat(cv::Mat* src_landmarks) const;

  /**
   * @name  weights
   * @fn  std::vector<T>& weights(void)
   * @brief Getter for the weights
   * @return Reference to the weights vector
   */
  std::vector<T>& weights(void) {
    return weights_;
  }

  /**
   * @name  weights
   * @fn  const std::vector<T>& weights(void) const
   * @brief Getter for the weights
   * @return Const reference to the weights vector
   */
  const std::vector<T>& weights(void) const {
    return weights_;
  }

  /**
   * @name  set_weights
   * @fn  void set_weights(const std::vector<T>& w)
   * @brief Setter for the weights
   */
  void set_weights(const std::vector<T>& w) {
    weights_ = w;
  }

  /**
   * @name  Rotation
   * @fn  const cv::Mat& Rotation(void) const
   * @brief Getter for the rotation
   * @return Const ref to the rotation matrix
   */
  const cv::Mat& Rotation(void) const {
    return R_;
  }

  /**
   * @name  Translation
   * @fn  const LTS5::Vector3<T>& Translation(void) const
   * @brief Getter for the translation
   * @return Const ref to the LTS5::Vector3 containing the translation
   */
  const LTS5::Vector3<T>& Translation(void) const {
    return t_;
  }

  /**
   * @name  Scale
   * @fn  T Scale(void) const
   * @brief Getter for the scale
   * @return Scale
   */
  T Scale(void) const {
    return scale_;
  }

  /**
   * @name  Error
   * @fn  T Error(void) const
   * @brief Getter for the error
   * @return Error
   */
  T Error(void) const {
    return error_;
  }

#pragma mark -
#pragma mark Usage

  /**
   * @name  ComputeTargetLandmarks
   * @fn    int ComputeTargetLandmarks();
   * @brief Given the source mesh, a face tracker and an offscreen renderer,
   *        project the mesh, detect the landmarks and backproject the landmarks
   * @return 0 if success, -1 otherwise
   */
  int ComputeTargetLandmarks();

  /**
   * @name  ComputeAlignment
   * @fn  int ComputeAlignment()
   * @brief Compute rigid transformation from source to target using landmarks
   * @return 0 if success, -1 otherwise
   */
  int ComputeAlignment();

  /**
   * @name  AlignSource
   * @fn  int AlignSource()
   * @brief Compute the rigid transformation from source to target and apply it
   * @return 0 if success, -1 otherwise
   */
  int AlignSource();

  /**
   * @name  AlignSourceOpenCV
   * @fn  int AlignSourceOpenCV()
   * @brief Use OpenCV algo to compute alignment
   * @return 0 if sucess, -1 otherwise
   */
  int AlignSourceOpenCV();

#pragma mark -
#pragma mark Private members and methods

 private:
  /**< Source mesh (to be rigidly aligned to the target) */
  LTS5::Mesh<T>* source_;
  /**< Target mesh */
  const LTS5::Mesh<T>* target_;
  /**< Indices of the landmarks in the source mesh vertex list */
  std::vector<int> src_landmarks_;
  /**< Indices of the landmarks in the target mesh vertex list */
  std::vector<int> trg_landmarks_;
  /**< Weights of the landmarks **/
  std::vector<T> weights_;
  /**< Face tracker to detect the landmarks on the target */
  LTS5::SdmTracker* face_tracker_;
  /**< Offscreen renderer to render the target and detect the landmarks */
  LTS5::OGLOffscreenRenderer<T>* offscreen_renderer_;
  /**< OGL program needed by the offscreen renderer */
  LTS5::OGLProgram* program_;
  /**< OGL Camera needed by the offscreen renderer */
  LTS5::OGLCamera* camera_;
  /**< Width of the offscreen image */
  int im_width_;
  /**< Height of the offscreen image */
  int im_height_;
  /**< resulting 3x3 rotation matrix */
  cv::Mat R_;
  /**< resulting 3x1 translation vector */
  LTS5::Vector3<T> t_;
  /**< resulting scale factor */
  T scale_;
  /**< Alignment error */
  T error_;
};
#endif /* __rigid_alignment_hpp__ */
