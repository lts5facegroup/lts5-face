/**
 *  @file   nonrigid_alignment.hpp
 *  @brief  Non rigid alignment optimisation problem definition
 *
 *  @author Gabriel Cuendet
 *  @date   06/10/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef nonrigid_alignment_hpp
#define nonrigid_alignment_hpp

#include "lts5/geometry/manifold_harmonic_transform.hpp"
#include "lts5/geometry/mesh.hpp"
#include "lts5/geometry/aabc.hpp"
#include "lts5/geometry/multiscale_partition_unity.hpp"
#include "lts5/optimization/base_optimization_problem.hpp"

/**
 *  @class  NonrigidAlignmentProblem
 *  @brief  Non rigid alignment optimization problem class
 *  @author Gabriel Cuendet
 *  @date   06/10/2016
 */
template<class T>
class NonrigidAlignmentProblem : public LTS5::BaseProblem<T> {
 public :
  using Matrix = typename LTS5::BaseProblem<T>::Matrix;

#pragma mark -
#pragma mark Initialization

  /**
   * @name  NonrigidAlignmentProblem
   * @fn  NonrigidAlignmentProblem(void)
   * @brief Class constructor
   */
  NonrigidAlignmentProblem(void);

  /**
   * @name  NonrigidAlignmentProblem
   * @fn  void ~NonrigidAlignmentProblem(void) {}
   * @brief Destructor of the class
   */
  ~NonrigidAlignmentProblem(void);

  /**
   * @name  InitMHT
   * @fn  void Init(const Eigen::SparseMatrix<T>& Q,
   *                const Eigen::SparseMatrix<T>& D, const int& n_basis)
   * @brief Initialize the MHT transform by setting important params and
   *        computing the basis
   * @param[in] Q           sparse stifness matrix
   * @param[in] D           diagonal lumped mass matrix
   * @param[in] n_basis     number of basis vector to compute
   */
  void InitMHT(const Eigen::SparseMatrix<T>& Q,
               const Eigen::SparseMatrix<T>& D,
               const int& n_basis);

  /**
   * @name  LoadMHTFromFile
   * @fn  int LoadMHTFromFile(const std::string filepath)
   * @brief Load a precomputed MHT from file
   * @param[in]  filepath  Path of the file containing the MHT
   * @return 0 if success, -1 otherwise
   */
  int LoadMHTFromFile(const std::string& filepath);

  /**
   * @name  SaveMHTToFile
   * @fn  int SaveMHTToFile(const std::string& filepath) const
   * @brief Save the basis to binary file
   * @param[in]  filepath  Path to the file
   * @return 0 if success, -1 otherwise
   */
  int SaveMHTToFile(const std::string& filepath) const;

  /**
   * @name  ComputeTheta0
   * @fn  void ComputeTheta0(LTS5::Mesh<T>* source)
   * @brief Project the source onto the MHT in order to get intital params theta0
   * @param[in,out]  source  Source mesh
   */
  void ComputeTheta0(LTS5::Mesh<T>* source);

  /**
   * @name  InitMPU
   * @fn  void InitMPU(double max_error, int max_depth, double alpha_radius,
   *                   const LTS5::Mesh<T>& target)
   * @brief Initialize target representation
   * @param[in]  max_error     Maximum tolerated approximation error
   * @param[in]  max_depth     Maximum depth of the tree
   * @param[in]  alpha_radius  Radius
   * @param[in]  target        Target mesh
   */
  void InitMPU(double max_error, int max_depth, double alpha_radius,
               const LTS5::Mesh<T>& target, const LTS5::AABC<T>& domain);

  /**
   * @name  LoadSrcMaskFromFile
   * @fn  void LoadSrcMaskFromFile(const std::string& filepath)
   * @brief Load the source's mask from a file containing the indices of the mask
   * @param[in]  filepath  Path to the file containing the indices of the mask
   */
  void LoadSrcMaskFromFile(const std::string& filepath);

#pragma mark -
#pragma mark Process

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn void EvaluateObjectiveFunction(const Matrix& theta, T* cost) const
   *  @brief  Evaluate the objective function with parameters \p theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost
   */
  void EvaluateObjectiveFunction(const Matrix& theta, T* cost) const;

  /**
   *  @name EvaluateObjectiveFunction
   *  @fn void EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost) const
   *  @brief  Evaluate the objective function with parameters \p theta
   *  @param[in]  theta     Parameters to use to evaluate objective function
   *  @param[out] cost      Objective function cost vector
   */
  void EvaluateObjectiveFunction(const Matrix& theta, Matrix* cost) const;

  /**
   *  @name EvaluateGradient
   *  @fn void EvaluateGradient(const Matrix& theta, Matrix* jacob)
   *  @brief  Evaluate the function's gradient with parameters \p theta
   *  @param[in]  theta     Position where to evaluate objective function
   *  @param[out] jacob     Jacobian matrix for parameters theta
   */
  void EvaluateGradient(const Matrix& theta, Matrix* jacob) const;

  /**
   *  @name EvaluateHessian
   *  @fn void EvaluateHessian(const Matrix& x, Matrix* hessian)
   *  @brief  Evaluate the function's hessian matrix with parameters \p theta
   *  @param[in]  theta   Parameters used to evalute hessian matrix
   *  @param[out] hessian Hessian value for parameters theta
   */
  void EvaluateHessian(const Matrix& theta, Matrix* hessian) const;

  /**
   * @name  SaveMesh
   * @fn  void SaveMesh(const Matrix& theta, const std::string& filename) const
   * @brief Save the mesh corresponding to the vector of parameters theta
   * @param[in]  theta     Vector of parameters to reconstruct from
   * @param[in]  filename  Path of the file to write
   */
  void SaveMesh(const Matrix& theta, const std::string& filename) const;

#pragma mark -
#pragma mark Accessors

  /**
   * @name  get_mpu
   * @fn  LTS5::MPU<T>& get_mpu(void)
   * @brief Getter for mpu_
   * @return reference to mpu
   */
  LTS5::MPU<T>& get_mpu(void) {
    return *mpu_;
  }

  /**
   * @name  get_mpu
   * @fn  const LTS5::MPU<T>& get_mpu(void) const
   * @brief Getter for mpu_
   * @return consst reference to mpu
   */
  const LTS5::MPU<T>& get_mpu(void) const {
    return *mpu_;
  }

  /**
   * @name  get_mht
   * @fn  LTS5::MHT<T>& get_mht(void)
   * @brief Getter for MHT
   * @return Reference to mht_
   */
  LTS5::MHT<T>& get_mht(void) {
    return *mht_;
  }

  /**
   * @name  get_mht
   * @fn  LTS5::MHT<T>& get_mht(void)
   * @brief Getter for MHT
   * @return Const reference to mht_
   */
  const LTS5::MHT<T>& get_mht(void) const {
    return *mht_;
  }

  /**
   * @name  get_theta0
   * @fn  Matrix& get_theta0(void)
   * @brief Getter for theta0 parameter
   * @return Ref to Eigen::Matrix
   */
  Matrix& get_theta0(void) {
    return theta0_;
  }

  /**
   * @name  get_theta0
   * @fn  Matrix& get_theta0(void)
   * @brief Getter for theta0 parameter
   * @return const ref to Eigen::Matrix
   */
  const Matrix& get_theta0(void) const {
    return theta0_;
  }

  /**
   *  @name get_dimensions
   *  @fn void get_dimensions(int* n_row, int* n_col) const
   *  @brief  Provide problem dimensions
   *  @param[out] n_row   Number of row in the system (Observables)
   *  @param[out] n_col   Number of column in the system (Variables)
   */
  void get_dimensions(int* n_row, int* n_col) const;

  /**
   *  @name get_A
   *  @fn Matrix& get_A(void)
   *  @brief  Provide access to system matrix ()
   *  @return System Matrix
   */
  const Matrix& get_A(void) const;

  /**
   *  @name get_AtA
   *  @fn Matrix& get_AtA(void)
   *  @brief  Provide access to system matrix ()
   *  @return At * A
   */
  const Matrix& get_AtA(void) const;

  /**
   *  @name get_y
   *  @fn Matrix& get_y(void)
   *  @brief Provide access to y
   *  @return y
   */
  const Matrix& get_y(void) const;

  /**
   *  @name get_Aty
   *  @fn Matrix& get_Aty(void)
   *  @brief Provide access to At * y
   *  @return At * y
   */
  const Matrix& get_Aty(void) const;

 private:
  /**< MPU of the target surface */
  LTS5::MPU<T>* mpu_;
  /**< Manifold harmonics transform */
  LTS5::MHT<T>* mht_;
  /**< theta0, initial parameters */
  Matrix theta0_;
  /**< Source's mask */
  std::vector<int> src_mask_;
};

#endif /* nonrigid_alignment_hpp */
