/**
 *  @file   AppModel declaration
 *  @brief  Stores and manage data used by the app
 *
 *  @author Gabriel Cuendet
 *  @date   25/10/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef app_model_hpp
#define app_model_hpp

#include <string>
#include <memory>
#include <fstream>
#include <iomanip>

#include "kfusion_parameters.hpp"
#include "lts5/geometry/mesh.hpp"

class AppModel {
public:
  using Mesh = LTS5::Mesh<double>;

  static const std::vector<std::string> expressions;

  enum struct AssessValue {
    UNDEFINED = -1,
    WRONG,
    CORRECT
  };

  struct DataReview {
    /** Index of the last frame of the .bin files to use for reconstruction */
    int last_frame;
    /** Wrong subject flag */
    AssessValue subject;
    /** Wrong expression flag */
    AssessValue expression;
    /** Wether or not the review is passe (visual quality is good) or not */
    AssessValue quality;
    /** Wether or not to rerun the reconstruction */
    bool reconstruct;

    /**
     * @name  DataReview
     * @brief Default constructor of the struct
     */
    DataReview() : last_frame(-1), subject(AssessValue::UNDEFINED),
    expression(AssessValue::UNDEFINED), quality(AssessValue::UNDEFINED),
    reconstruct(false) {}

    DataReview(const int& last, const AssessValue& subj, const AssessValue& expr,
    const AssessValue& qual, const bool& reconstr)
    : last_frame(last)
    , subject(subj)
    , expression(expr)
    , quality(qual)
    , reconstruct(reconstr) {}

    DataReview(const DataReview& other) = default;

    DataReview& operator=(const DataReview& other) = default;

    void set_quality(const AssessValue& quality) {
      this->quality = quality;
      set_reconstruct();
    }

    void set_subject(const AssessValue& val) {
      subject = val;
      set_reconstruct();
    }

    void set_expression(const AssessValue& val) {
      expression = val;
      set_reconstruct();
    }

    void set_reconstruct(void) {
      if (subject == AssessValue::CORRECT &&
          expression == AssessValue::CORRECT) {
        if (quality == AssessValue::CORRECT) {
          reconstruct = false;
        } else {
          // Reconstruct only if subject and expression are correct but quality
          // is bad
          reconstruct = true;
        }
      } else {
        reconstruct = false;
      }
    }
  };

  struct RawFile {
    /** Raw binary file name */
    std::string file_bin_name_;
    /** Path to the file */
    std::string dir_path_;
    /** Wether the raw color bin file was found or not */
    bool file_found_;
    /** Wether the raw color bin file is compressed or not */
    bool file_compressed_;

    /**
     * @name  RawFile
     * @fn  RawFile()
     * @brief Constructor
     */
    RawFile() : file_bin_name_(""), file_found_(false), file_compressed_(false) {}

    RawFile(const std::string& dir, const std::string& file, bool compressed)
        : file_bin_name_(file), dir_path_(dir), file_found_(true),
          file_compressed_(compressed) {}

    /**
     * @name path
     * @fn std::string path(void) const
     * @brief Return complete path to the file
     * @return complete path to the file
     */
    std::string path(void) const {
      std::string extension = file_compressed_ ? ".gz" : "";
      return dir_path_ + file_bin_name_ + extension;
    }
  };

  /**
   * @name  AppModel
   * @fn  AppModel()
   * @brief Class constructor
   * @param[in] records_dir  Path to the root directory of the recordings (.bin)
   * @param[in] csv_path  Path to the csv file where to save the review
   */
  AppModel(const std::string& records_dir, const std::string& csv_path);

  /**
   * @name  LoadMesh
   * @fn  int LoadMesh(const std::string& mesh_path)
   * @brief Load a mesh
   * @param[in]  mesh_path  Path to the mesh to load
   * @return 0 if success, -1 otherwise
   */
  int LoadMesh(const std::string& mesh_path);

  /**
   * @name  LoadNewSubject
   * @fn  int LoadNewSubject(void)
   * @brief Load a mesh and all infos, from the reconstructions dir, with the
   *        name composed with subject id and expression.
   *        Typically called from GUI
   * @return 0 if success, -1 otherwise
   */
  int LoadNewSubject(void);

  /**
   * @name ResetSubject
   * @fn void ResetSubject(void)
   * @brief Reset parameters linked to a subject in particular
   */
  void ResetSubject(void);

  /**
   * @name ReadReviewFromCsv
   * @fn int ReadReviewFromCsv(const std::string& mesh_path, DataReview** review) const
   * @param[in]  mesh_path  Path to the reconstruction mesh
   * @param[out] review     Review loaded from the csv or nullptr
   * @return 0 if success, -1 otherwise
   */
  int ReadReviewFromCsv(const std::string& mesh_path, DataReview** review) const;

  /**
   * @name WriteReviewToCsv
   * @fn int WriteReviewToCsv(const std::string& mesh_path,
   *                          const DataReview& review) const
   * @return 0 if success, -1 otherwise
   */
int WriteReviewToCsv();

#pragma mark -
#pragma mark Getters and Setters

  /**
   * @name mesh_filename
   * @fn std::string mesh_filename(void) const
   * @brief Getter for the mesh filename
   * @return  Mesh filename
   */
  std::string mesh_filename(void) const {
    std::ostringstream subj_id_stream;
    subj_id_stream << std::setw(3) << std::setfill('0') << this->subject_id_;
    std::string subj_id = subj_id_stream.str();

    return this->reconstructions_dir() + "/" + subj_id + "_" +
        this->expression() + ".ply";
  }

  /**
   * @name is_mesh_loaded
   * @fn bool is_mesh_loaded(void) const
   * @return Wether or not a valid mesh is loaded
   */
  bool is_mesh_loaded(void) const {
    return bool(mesh_);
  }

  // @TODO: (Gabriel) Fix this daube!
  /**
   * @name  mesh
   * @fn  const LTS5::Mesh<double>& mesh(void)
   * @brief Getter for the mesh pointer
   * @return const ref to mesh
   */
  bool mesh(std::shared_ptr<Mesh>& mesh) {
    if (mesh_) {
      mesh = mesh_;
      return true;
    } else {
      return false;
    }
  }

  /**
   * @name  current_frame
   * @fn  int current_frame(void) const
   * @brief Getter for the current frame
   * @return current_frame
   */
  int current_frame(void) const {
    return current_frame_;
  }

  /**
   * @name  current_frame_ptr
   * @fn int* current_frame_ptr(void)
   * @brief Getter of a  pointer to current_frame
   * @return Pointer to current_frame
   */
  int* current_frame_ptr(void) {
    return &current_frame_;
  }

  /**
   * @name  set_current_frame
   * @fn  void set_current_frame(int frame)
   * @brief Setter for the current frame
   */
  void set_current_frame(int frame) {
    current_frame_ = frame;
  }

  /**
   * @name  total_frames
   * @fn  int total_frames(void) const
   * @brief Getter for the total number of frames
   * @return total_frames
   */
  int total_frames(void) const {
    return total_frames_;
  }

  /**
   * @name set_total_frames
   * @fn void set_total_frames(int n)
   * @brief Setter for total frames
   * @param n  Total number of frames
   */
  void set_total_frames(int n) {
    total_frames_ = n;
  }

  /**
   * @name  reconstructions_dir
   * @fn  std::string reconstructions_dir(void) const
   * @brief Getter for the recontructions dir
   * @return Path to the reconstructions dir
   */
  std::string reconstructions_dir(void) const {
    return reconstructions_dir_;
  }

  /**
   * @name  set_reconstructions_dir
   * @fn  void set_reconstructions_dir(const std::string& path)
   * @brief Setter for the reconstructions dir
   * @param[in]  path  Path to the reconstructions dir
   */
  void set_reconstructions_dir(const std::string& path) {
    reconstructions_dir_ = path;
  }

  /**
   * @name  subject_id
   * @fn  int subject_id(void) const
   * @brief Getter for subject_id
   * @return subject_id
   */
  int subject_id(void) const {
    return subject_id_;
  }

  /**
   * @name  set_subject_id
   * @fn  void set_subject_id(int id)
   * @brief Setter for subject_id
   * @param[in]  id  Id of the subject
   */
  void set_subject_id(int id) {
    subject_id_ = id;
  }

  /**
   * @name  expression_id
   * @fn  int expression_id(void) const
   * @brief Getter for expression_id
   * @return expression_id
   */
  int expression_id(void) const {
    return expression_id_;
  }

  /**
 * @name  set_expression_id
 * @fn  void set_expression_id(int id)
 * @brief Setter for the expression id
 * @param[in]  id  Id of the expression
 */
  void set_expression_id(int id) {
    expression_id_ = id;
  }

  /**
   * @name  expression
   * @fn  std::string expression(void) const
   * @brief Getter for the expression string
   * @return Expression
   */
  std::string expression(void) const {
    return expressions[expression_id_];
  }

  /**
   * @name  review
   * @fn  const DataReview& review(void) const
   * @brief Getter for the review data struct
   * @return const ref to DataReview
   */
  const DataReview& review(void) const {
    return review_;
  }

  /**
   * @name  review
   * @fn  DataReview& review(void)
   * @brief Getter for the review data struct
   * @return Ref to DataReview
   */
  DataReview& review(void) {
    return review_;
  }

  /**
   * @name  raw_color
   * @fn  const RawFile& raw_color(void) const
   * @brief Getter for the raw color bin file
   * @return Const ref to a RawFile struct
   */
  const RawFile& raw_color(void) const {
    return *raw_color_;
  }

  /**
   * @name  raw_depth
   * @fn  const RawFile& raw_depth(void) const
   * @brief Getter for the raw depth bin file
   * @return Const ref to a RawFile struct
   */
  const RawFile& raw_depth(void) const {
    return *raw_depth_;
  }

  /**
   * @name set_unzip_color
   * @param unz_clr  Smart pointer to the unzip color file instance
   */
  void set_unzip_color(std::shared_ptr<RawFile> unz_clr) {
    this->unzip_color_ = unz_clr;
  }

  bool unzipped_color(void) {
    return bool(unzip_color_);
  }

  std::string color_bin_path(void) {
    if (unzipped_color()) {
      return unzip_color_->path();
    } else if (raw_color_) {
      return raw_color_->path();
    } else {
      return "";
    }
  }

#pragma mark -
#pragma mark Private

private:

  /**
   * @name FindBinaryFiles
   * @fn int FindBinaryFiles()
   * @brief Compute full path of the current recording bin files
   * @return true if found, false otherwise
   */
  bool FindBinaryFiles();

  /** Mesh */
  std::shared_ptr<Mesh> mesh_;
  /** Index of the current frame in the stream */
  int current_frame_;
  /** Number of frames in the stream */
  int total_frames_;
  /** review of the reconstruction */
  DataReview review_;
  /** Path to the output CSV file */
  std::string output_csv_path_;
  /** Path to the reconstructions directory */
  std::string reconstructions_dir_;
  /** Path to the recordings directory */
  std::string recordings_dir_;
  /** Subject ID */
  int subject_id_;
  /** Expression ID */
  int expression_id_;
  /** Struct containing informations about the raw color bin file*/
  std::shared_ptr<RawFile> raw_color_;
  /** */
  std::shared_ptr<RawFile> unzip_color_;
  /** Struct containing informations about the raw color bin file*/
  std::shared_ptr<RawFile> raw_depth_;
  /** kinect fusion parameters struc*/
  std::unique_ptr<KinFuParams> kfusion_params_;
};

#endif /* app_model_hpp */
