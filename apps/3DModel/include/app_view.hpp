/**
 *  @file   app_view.hpp
 *  @brief  Wrapper for nanogui, View declaration
 *
 *  @author Gabriel Cuendet
 *  @date   25/10/2016
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#ifndef app_view_hpp
#define app_view_hpp

#ifdef __APPLE__
#include <OpenGL/gl3.h>
#else
#include <GL/glew.h>
#endif

#include <GLFW/glfw3.h>
#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"
#include "glm/ext.hpp"

#include "Eigen/Core"
#include "nanogui/nanogui.h"
#include <memory>
#include <thread>
#include <fstream>

#include "lts5/geometry/mesh.hpp"
#include "lts5/ogl/camera.hpp"

class AppModel;

class GLTexture {
 public:
  using handleType = std::unique_ptr<uint8_t[], void(*)(void*)>;

  GLTexture() = default;

  GLTexture(const std::string& textureName)
  : mTextureName(textureName), mTextureId(0) {}

  GLTexture(const std::string& textureName, GLint textureId)
  : mTextureName(textureName), mTextureId(textureId) {}

  GLTexture(const GLTexture& other) = delete;

  GLTexture(GLTexture&& other) noexcept
  : mTextureName(std::move(other.mTextureName)),
  mTextureId(other.mTextureId) {
    other.mTextureId = 0;
  }

  GLTexture& operator=(const GLTexture& other) = delete;

  GLTexture& operator=(GLTexture&& other) noexcept {
    mTextureName = std::move(other.mTextureName);
    std::swap(mTextureId, other.mTextureId);
    return *this;
  }

  ~GLTexture() noexcept {
    if (mTextureId)
      glDeleteTextures(1, &mTextureId);
  }

  GLuint texture() const {return mTextureId;}

  const std::string& textureName() const {return mTextureName;}

  /**
   *  Load a file in memory and create an OpenGL texture.
   *  Returns a handle type (an std::unique_ptr) to the loaded pixels.
   */
  handleType load(const std::string& fileName, int* frame, int* total_frames) {
    if (mTextureId) {
      glDeleteTextures(1, &mTextureId);
      mTextureId = 0;
    }
    int force_channels = 0;
    int w, h, n;
    //handleType textureData(stbi_load(fileName.c_str(), &w, &h, &n, force_channels), stbi_image_free);
    handleType textureData(load_image_bytes(fileName, frame, total_frames, &w, &h, &n) , free);
    if (!textureData)
      throw std::invalid_argument("Could not load texture data from file " + fileName);
    glGenTextures(1, &mTextureId);
    glBindTexture(GL_TEXTURE_2D, mTextureId);
    GLint internalFormat;
    GLint format;
    switch (n) {
      case 1: internalFormat = GL_R8; format = GL_RED; break;
      case 2: internalFormat = GL_RG8; format = GL_RG; break;
      case 3: internalFormat = GL_RGB8; format = GL_RGB; break;
      case 4: internalFormat = GL_RGBA8; format = GL_BGRA; break;
      default: internalFormat = 0; format = 0; break;
    }
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0, format, GL_UNSIGNED_BYTE, textureData.get());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    return textureData;
  }

private:

  uint8_t* load_image_bytes(const std::string& filename, int* index, int* total, int* w, int* h, int* n) {
    // I'll need a malloc somewhere and a free
    std::ifstream color_image_stream(filename.c_str(), std::ios::in | std::ios::binary);

    int color_num_frames, color_block_size;

    color_image_stream.read((char*)&color_num_frames, sizeof(int));
    color_image_stream.read((char*)&color_block_size, sizeof(int)); // size in bytes for 1 frame
    color_image_stream.read((char*)w, sizeof(int));
    color_image_stream.read((char*)h, sizeof(int));

    *total = color_num_frames;
    *n = color_block_size / ((*w) * (*h)); // Number of channels (one channel being 1 byte)

    // Read the COLOR frame "index"
    //unsigned short color_buffer[color_block_size];
    uint8_t* array_ptr = static_cast<uint8_t*>(malloc(color_block_size));

    if (*index > color_num_frames) {
      *index = color_num_frames;
    }

    int ind_frame = (*index) == -1 ? color_num_frames : (*index);

    (*index) = ind_frame;
    int color_pos = 16 + ind_frame * color_block_size; // header (4 bytes) + index * block_size
    color_image_stream.seekg(color_pos);

    if (! color_image_stream.read( (char*)(array_ptr), color_block_size) ) {
      std::cerr << "Cannot read from color image stream!" << std::endl;
    }

    color_image_stream.close();

    return array_ptr;
  }

  std::string mTextureName;
  GLuint mTextureId;
};

/**
 *  @class  AppView
 *  @brief  nanogui AppView
 *  @author Gabriel Cuendet
 *  @date   24/10/2016
 */
class AppView : public nanogui::Screen {
 public:

  /**
   * @name  AppView()
   * @fn  AppView()
   * @brief Constructor of the class
   * @param[in] temp_dir  Temp dir path
   * @param[in/out] model  AppModel
   */
  AppView(const std::string& temp_dir, AppModel* model);

  /**
   * @name  ~AppView
   * @fn  ~AppView()
   * @brief Destructor of the class
   */
  ~AppView();

  /**
   * @name  Run
   * @fn  int Run()
   * @brief Run the main loop of the app
   * @return 0 if success, -1 otherwise
   */
  int Run();

#pragma mark -
#pragma mark Callbacks

/**
   * @name  SubjectChangeEvent
   * @fn  void SubjectChangeEvent(void)
   * @brief Notify a change of subject (either subject id or expression id)
   */
  void SubjectChangeEvent(void);

  /**
   * @name ReviewUpdatedEvent
   * @fn void ReviewUpdatedEvent(void)
   * @brief Notify an update in the review
   */
  void ReviewUpdatedEvent(void);

  /**
   * @name  keyboardEvent
   * @fn  virtual bool keyboardEvent(int key, int scancode, int action,
   *                                 int modifiers)
   * @brief Handle a keyboard event
   * @param[in]  key
   * @param[in]  scancode
   * @param[in]  action
   * @param[in]  modifiers
   * @return true if callback is defined, false otherwise
   */
  virtual bool keyboardEvent(int key, int scancode, int action, int modifiers);

  /**
   * @name  mouseButtonEvent
   * @fn  virtual bool mouseButtonEvent(const Eigen::Vector2i& pos, int button,
   *                                    bool down, int modifier)
   * @brief Handle a mouse button event
   * @param[in]  pos
   * @param[in]  button
   * @param[in]  down
   * @param[in]  modifier
   * @return true if callback is defined, false otherwise
   */
  virtual bool mouseButtonEvent(const Eigen::Vector2i& pos, int button,
                                bool down, int modifier);

  /**
   * @name  mouseMotionEvent
   * @fn  virtual bool mouseMotionEvent(const Eigen::Vector2i& pos,
   *                                    const Eigen::Vector2i& rel, int button,
   *                                    int modifier)
   * @brief Handle a mouse Motion event
   * @param[in]  pos
   * @param[in]  rel
   * @param[in]  button
   * @param[in]  modifier
   * @return true if callback is defined, false otherwise
   */
  virtual bool mouseMotionEvent(const Eigen::Vector2i& pos,
                                const Eigen::Vector2i& rel, int button,
                                int modifier);

  /**
   * @name  draw
   * @fn  virtual void draw(NVGcontext *ctx)
   * @brief draw
   * @param[in]  ctx  pointer to NVG context
   */
  virtual void draw(NVGcontext *ctx);

  /**
   * @name  drawContents
   * @fn  virtual void drawContents()
   * @brief draw contents
   */
  virtual void drawContents();

#pragma mark -
#pragma mark Private

 private:

  void ResetSubject();

  /**
   * @name  UploadMesh
   * @fn  void UploadMesh(const LTS5::Mesh<double>& mesh)
   * @brief Upload the mesh to the GPU
   * @param[in]  mesh
   */
  void UploadMesh(const LTS5::Mesh<double>& mesh);

  /**
   * @name  ClearView
   * @fn  void ClearView(void)
   * @brief Clear the display from any mesh
   */
  void ClearView(void);

  /**
   * @name  LoaderLoop
   * @fn  void LoaderLoop(void)
   * @brief Loader loop to execute in sepatate thread
   */
  void LoaderLoop(void);

  /** AppModel in which to store the data from the GUI */
  AppModel* app_model_;
  /** OpenGL part */
  nanogui::GLShader shader_;
  /** Camera object */
  LTS5::OGLCamera* camera_;
  /** Camera matrix to pass to the shader */
  Eigen::Matrix<float, 4, 4> cam_mat_;
  /** Number of triagnels to render */
  int n_tri_;
  /** Gui elements*/
  nanogui::TextBox* exprId_txtBox_;
  nanogui::TextBox* reconstrDir_txtBox_;
  nanogui::IntBox<int>* frameId_IntBox_;
  nanogui::CheckBox* wsubject_cb_;
  nanogui::CheckBox* wexpr_cb_;
  nanogui::IntBox<int>* lastFrame_IntBox_;
  nanogui::Button* good_Btn_;
  nanogui::Button* bad_Btn_;
  nanogui::ImageView* image_view_;
  nanogui::Slider* slider_frame_;
  nanogui::Button* update_button_;
  GLTexture texture_;
  GLTexture::handleType texture_data_;
  bool load_image_;
  bool display_image_;
  /** Pointer to the thread to use to load heavy files */
  std::unique_ptr<std::thread> loader_thread_;
  /** Wether or not there is something ready to be loaded */
  bool ready_to_load_;
  /** Wether or not to keep the thread alive (only usefull when destructing it)*/
  bool loader_is_alive_;
  /** Temporary folder path */
  std::string temp_dir_path_;
};

#endif /* app_view_hpp */
