/**
 *  @file   align_mesh.cpp
 *  @brief  App to perform Non-rigid alignment between a source and a target mesh
 *
 *  @author Gabriel Cuendet
 *  @date   2016-09-28
 *  Copyright (c) 2016 Gabriel Cuendet. All rights reserved.
 */

#include <iostream>
#include <fstream>

#include "Eigen/Sparse"
#include "Eigen/Dense"

#include "opencv2/core/core.hpp"
#include "opencv2/core/eigen.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/geometry/manifold_harmonic_transform.hpp"
#include "lts5/geometry/multiscale_partition_unity.hpp"
#include "lts5/optimization/gradient_descent_solver.hpp"

#include "rigid_alignment.hpp"
#include "nonrigid_alignment.hpp"
#include "alignment_config.hpp"


int main(int argc, const char * argv[]) {
  LTS5::CmdLineParser args;
  args.AddArgument("-c", LTS5::CmdLineParser::kNeeded, "Path to 3D model configuration file (.xml)");
  args.AddArgument("-s", LTS5::CmdLineParser::kNeeded, "Path to the SOURCE mesh input file");
  args.AddArgument("-b", LTS5::CmdLineParser::kOptional, "Path to a binary file containing MHT");
  args.AddArgument("-t", LTS5::CmdLineParser::kNeeded, "Path to the TARGET mesh input file");
  args.AddArgument("-o", LTS5::CmdLineParser::kNeeded, "Path to the OUTPUT mesh file");
  int error = args.ParseCmdLine(argc, argv);

  std::string config_path;
  std::string source_path;
  std::string mht_path;
  std::string target_path;
  std::string output_path;
  if (error) {
    std::cout << "ERROR: Parsing arguments failed!" << std::endl;
    return -1;
  }
  args.HasArgument("-c", &config_path);
  args.HasArgument("-s", &source_path);
  args.HasArgument("-t", &target_path);
  args.HasArgument("-o", &output_path);

  LTS5::AlignConfig config;
  config.Load(config_path);

//  cv::FileStorage storage(config_path, cv::FileStorage::READ);
//  if (!storage.isOpened()) {
//    std::cout << "Opening config file failed" << std::endl;
//    return -1;
//  }
//  // Path var
//  std::string annotations;
//  std::string sdm_model_path;
//  std::string f_detector_path;
//  double centre_freq;
//  int no_freqs;
//  std::vector<double> config_weights;
//  double max_error = 1e-6;
//  int max_depth = 8;
//  double alpha_radius = 0.75;
//  // Go root
//  cv::FileNode root = storage.getFirstTopLevelNode();
//  LTS5::ReadNode(root, "annotations", &annotations);
//  LTS5::ReadNode(root, "sdm_model_path", &sdm_model_path);
//  LTS5::ReadNode(root, "face_detect_model_path", &f_detector_path);
//  LTS5::ReadNode(root, "centre_freq", &centre_freq);
//  LTS5::ReadNode(root, "no_freqs", &no_freqs);
//  LTS5::ReadNode(root, "weights", &config_weights);
//  LTS5::ReadNode(root, "mpu_max_error", &max_error);
//  LTS5::ReadNode(root, "mpu_max_depth", &max_depth);
//  LTS5::ReadNode(root, "mpu_alpha_radius", &alpha_radius);

  LTS5::Mesh<double>* source = new LTS5::Mesh<double>(source_path);

  source->NormalizeMesh();
  // DEBUG PURPOSE
  source->Save("output/source_mesh.ply");

  // Pass 1: RIGID ALIGNMENT
  // Detect the landmarks on the target! (included)
  LTS5::Mesh<double>* target = new LTS5::Mesh<double>(target_path);
  target->NormalizeMesh();

  RigidAlignment<double> rigid_align(*target, source);
  rigid_align.LoadSrcLandmarks(config.annotations_path);
  rigid_align.InitOffscreenRenderer();
  rigid_align.InitFaceTracker(config.sdm_model_path,
                              config.face_detector_path);
  rigid_align.ComputeTargetLandmarks();
  std::vector<double> &weights = rigid_align.weights();
  for (int i = 0; i < weights.size(); ++i) {
    weights[i] *= config.landmark_weights[i];
  }

  rigid_align.AlignSource();

  // DEBUG PURPOSE
  source->Save("output/aligned_mesh.ply");
  target->Save("output/target_mesh.ply");

  source->ComputeVertexNormal();
  target->ComputeVertexNormal();

  // Pass 2: NON-RIGID ALIGNMENT
  NonrigidAlignmentProblem<double> nonrigid_align;
  args.HasArgument("-b", &mht_path);
  if (nonrigid_align.LoadMHTFromFile(mht_path) != 0) {
    std::cout << "Warning: Recomputing spectral basis (might be inefficient)" <<
    std::endl;

    int n_basis = source->size() - 1; // Compute all basis
    int n = static_cast<int>(source->size());
    Eigen::SparseMatrix<double>* Q = new Eigen::SparseMatrix<double>(n, n);
    Eigen::SparseMatrix<double>* D = new Eigen::SparseMatrix<double>(n, n);
    source->ComputeCotanLaplacian(D, Q);


    // @TODO: DEBUG PURPOSE
    Eigen::DiagonalMatrix<double, Eigen::Dynamic> DInv(D->diagonal());
    DInv = DInv.inverse();
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> A = -1.0 * DInv * (*Q);

    cv::Mat A_cv;
    cv::eigen2cv(A, A_cv);
    LTS5::SaveMatToBin("output/DQ_mat.bin", A_cv);
    //---


    nonrigid_align.InitMHT(*Q, *D, n_basis);
    nonrigid_align.LoadSrcMaskFromFile(config.src_mask_path);

    // @TODO: DEBUG PURPOSE
    cv::Mat basis_mat;
    cv::eigen2cv(nonrigid_align.get_mht().get_basis_t(), basis_mat);
    LTS5::SaveMatToBin("output/Basis_mat.bin", basis_mat);
    //---


    if (!mht_path.empty()) {
      nonrigid_align.SaveMHTToFile(mht_path);
    }

    delete Q;
    delete D;
  }

  nonrigid_align.ComputeTheta0(source);
  LTS5::AABC<double> domain = source->bcube() + target->bcube();
  // Scale a bit the whole domain, just to take some margin (it´s not costly)
  domain.Scale(1.5);
  nonrigid_align.InitMPU(config.mpu_max_error, config.mpu_max_depth, config.mpu_alpha_radius, *target, domain);

  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> theta(nonrigid_align.get_theta0());
  LTS5::GradientDescentSolver<double> solver;
  LTS5::BaseSolver<double>::SolverOption solver_options;
  solver_options.learning_rate = 0.0000005;
  solver_options.max_iter = 1;
  solver.set_option(solver_options);
  solver.Minimize(nonrigid_align, &theta);

  nonrigid_align.SaveMesh(theta, "output/aligned_iter01.ply");

  std::cout << "done" << std::endl;

  delete source;
  delete target;
  return 0;
}
