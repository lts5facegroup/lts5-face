# coding=utf-8
"""
Train SkinNet model using TF Estimator framework
"""
from argparse import ArgumentParser
from os.path import join as _join
from distutils.version import LooseVersion
from multiprocessing import cpu_count
import tensorflow as _tf
from tensorflow.keras.callbacks import ModelCheckpoint
from data_loader import DataLoader
from lts5.utils.tools import init_logger
from lts5.tensorflow_op.network.pspnet import PSPNet
from lts5.tensorflow_op.utils.device import initialize_device_memory
from lts5.tensorflow_op.callbacks import AugmentedTensorBoard


__author__ = 'Christophe Ecabert'

# Init LTS5 logger
logger = init_logger()
# Limit memory usage
initialize_device_memory()


# Ensure TF 2.0
tf_version = _tf.__version__
logger.info('Tensorflow version: %s', tf_version)
assert LooseVersion(tf_version) >= LooseVersion('2.0'),\
  "TensorFlow r2.0 or later is needed"


def input_fn(filepath,
             img_size,
             training,
             n_repeat,
             batch_size,
             with_auxiliary_loss=False,
             summary=None):
  """
  Load data from a text file `filepath`
  :param filepath:    Path to the file storing the input data
  :param img_size:    Image dimensions, (width, height)
  :param training:    Boolean flag indicating training phase
  :param n_repeat:    Number of time to repeat the dataset (i.e. epoch)
  :param batch_size:  Batch size
  :param drop_remainder: Indicate if not complete needs to be dropped
  :return:  Dataset's iterator
  """
  # Define number of possible parallel call
  n_call = cpu_count()
  # Create loader
  loader = (DataLoader(filename=filepath,
                       img_size=img_size,
                       training=training,
                       with_auxiliary_loss=with_auxiliary_loss,
                       n_parallel_call=n_call)
            .shuffle(buffer_size=(batch_size * 50))
            .decode()
            .preprocess()
            .repeat(count=n_repeat)
            .batch(batch_size=batch_size, drop_remainder=True)
            .prefetch(1))
  if summary:
    loader = loader.map(summary, None)
  # Get iterator
  return loader.one_shot_iterator()


def train(pargs):
  """
  Train skin segmentation network
  :param pargs: Parsed arguments
  """

  # Summary manager
  # ------------------------------------------------
  logdir = _join(pargs.model_dir, 'logs')

  # Define inputs dataset
  # ------------------------------------------------
  train_dataset = input_fn(pargs.train_set,
                           img_size=(200, 200),
                           training=True,
                           n_repeat=1,
                           batch_size=pargs.batch_size,
                           with_auxiliary_loss=True,
                           summary=None)
  valid_dataset = input_fn(pargs.validation_set,
                           img_size=(200, 200),
                           training=False,
                           n_repeat=1,
                           batch_size=pargs.batch_size,
                           with_auxiliary_loss=True,
                           summary=None)

  # Weighted class
  # ------------------------------------------------
  class_weights = None
  if pargs.weighted_class:
    # See:
    # https://www.tensorflow.org/tutorials/structured_data/imbalanced_data#calculate_class_weights
    pos = 0
    total = 0
    for _, label in train_dataset:
      shp = label[0].get_shape()
      pixel_skin = _tf.reduce_sum(_tf.cast(label, _tf.int32))
      total += (shp[0] * shp[1] * shp[2])
      pos += pixel_skin.numpy()
    neg = total - pos

    w0 = total / (2 * neg)
    w1 = total / (2 * pos)
    class_weights = {0: w0, 1: w1}
    # class_weights = {0: 0.6998090423748841, 1: 1.7511946257714757}

  # Model
  # ------------------------------------------------
  shp = (pargs.batch_size, 200, 200, 3)
  model = PSPNet(config='resnet18',
                 with_auxiliary_head=True,
                 name='PSPNet')
  model.build(shp)
  # Losses
  # ------------------------------------------------
  losses = model.get_losses()
  # Metrics
  # ------------------------------------------------
  metrics = model.get_metrics(input_shape=(200, 200, 3))
  # Optimizer
  # ------------------------------------------------
  optimizer = model.get_optimizer(learning_rate=pargs.lr)

  # Compile
  # ------------------------------------------------
  model.compile(optimizer=optimizer,
                loss=losses,
                loss_weights=[1.0, 0.4],
                metrics=metrics)
  # Callbacks
  # ------------------------------------------------
  model_name = _join(pargs.model_dir, 'pspnet.ckpt')
  tensorboad_cb = AugmentedTensorBoard(log_dir=logdir,
                                       histogram_freq=1,
                                       write_graph=True,
                                       write_images=False,
                                       update_freq=pargs.log_freq * pargs.batch_size,
                                       profile_batch=0,
                                       embeddings_freq=0,
                                       labels=['Non-Skin', 'Skin'],
                                       figsize=(4, 4))
  # Save model based on performance of validation loss
  checkpoint_cb = ModelCheckpoint(filepath=model_name,
                                  save_best_only=True,
                                  monitor='val_loss',
                                  mode='min',
                                  save_weights_only=True,
                                  verbose=1)
  callbacks = [tensorboad_cb,
               checkpoint_cb]

  # Reload
  latest = _tf.train.latest_checkpoint(pargs.model_dir)
  latest = latest if latest is not None else 'imagenet'
  model.load_weights(latest)

  # Train
  model.fit(x=train_dataset,
            callbacks=callbacks,
            epochs=pargs.n_epoch,
            validation_data=valid_dataset,
            validation_steps=None,
            class_weight=class_weights)


if __name__ == '__main__':

  p = ArgumentParser(description='Skin segmentation network training script')

  # Train set location
  p.add_argument('--train_set',
                 type=str,
                 help='Location where training samples list is located (*.csv)')
  # Validation set location
  p.add_argument('--validation_set',
                 type=str,
                 help='Location where validation samples list is located'
                      ' (*.csv)')
  # Test set location
  p.add_argument('--test_set',
                 type=str,
                 help='Location where test samples list is located'
                      ' (*.csv)')
  # Folder where to store model
  p.add_argument('--model_dir',
                 type=str,
                 help='Folder where the checkpoints are stored (*.ckpt)')

  # Learning rate
  p.add_argument('--lr',
                 type=float,
                 default=1e-5,
                 help='Learning rate')
  # Number of epoch
  p.add_argument('--n_epoch',
                 type=int,
                 default=10,
                 help='Number of epoch to train for')
  # Batch size
  p.add_argument('--batch_size',
                 type=int,
                 default=16,
                 help='Number of samples in a single batch')
  # Logging frequency
  p.add_argument('--log_freq',
                 type=int,
                 default=500,
                 help='Logging frequency')
  # Weighted class
  p.add_argument('--weighted_class',
                 type=bool,
                 default=True,
                 help='Class weighting for imbalanced data')
  args = p.parse_args()
  # Train + Evaluate
  train(args)



