# coding=utf-8
"""
Data preparation for skin segmentation network
"""
from itertools import chain
import os
from os.path import join
from math import ceil
from imageio import imwrite
import numpy as np
import tensorflow as _tf
from skimage.morphology import disk
from skimage.morphology import binary_closing
from lts5.utils import init_logger
from lts5.tensorflow_op.utils.tfrecords import to_feature
from lts5.tensorflow_op.utils.tfrecords import convert_csv_to_tfrecords


__author__ = 'Christophe Ecabert'


# Logger
logger = init_logger()


def _create_dataset(pargs):
  """
  Create datasets from the parsed arguments
  :param pargs: Parsed arguemtns
  :return:  List of dataset's iterator
  """
  from lts5.utils import Pratheepan, SFA, FSD, PLD

  datasets = []
  if pargs.pratheepan_path is not None:
    d = Pratheepan(folder=pargs.pratheepan_path)
    it = zip(d.samples(partition='train'), d.annotations(partition='train'))
    datasets.append(it)
    logger.info('Pratheepan dataset added')
  if pargs.sfa_path is not None:
    d = SFA(folder=pargs.sfa_path)
    it = zip(d.samples(partition='train'), d.annotations(partition='train'))
    datasets.append(it)
    logger.info('SFA dataset added')
  if pargs.fsd_path is not None:
    d = FSD(folder=pargs.fsd_path, dtype='skin')
    it = zip(d.samples(partition='train'), d.annotations(partition='train'))
    datasets.append(it)
    logger.info('FSD dataset added')
  if pargs.pld_path is not None:
    d = PLD(folder=pargs.pld_path, with_hair=False)
    it = zip(d.samples(partition='train'), d.annotations(partition='train'))
    datasets.append(it)
    logger.info('PLD dataset added')
  return datasets


def extract_image_patch(pargs):
  """
  Extract patches
  :param pargs:     Parsed arguments
  """

  # Create output folder tree
  def _create_dir_if_needed(path):
    if not os.path.exists(path):
      os.makedirs(path)

  # Output location
  _create_dir_if_needed(path=pargs.output)
  _create_dir_if_needed(path=join(pargs.output, 'images'))
  _create_dir_if_needed(path=join(pargs.output, 'labels'))
  # Create dataset iterator
  dset_it = _create_dataset(pargs=pargs)
  # Combine iterators
  it = chain.from_iterable(dset_it)
  # Create sample file
  fname = join(pargs.output, 'samples.txt')
  samples = []
  with open(fname, 'wt') as f:
    # Write header
    f.write('ImagePath,LabelPath\n')
    # Loop over all samples
    patch_cnt = 0
    logger.info('Start extracting patches of dimension %dx%d',
                pargs.crop_h,
                pargs.crop_h)
    for img, mask in it:
      # Sanity check
      assert img.shape[:2] == mask.shape, 'Image/Mask dimensions does not match'
      # Apply morphological operator (closing) to remove small holes in the mask
      mask = binary_closing(image=mask, selem=disk(radius=7)).astype(np.uint8)
      # Crop
      h = img.shape[0]
      w = img.shape[1]
      crop_h = pargs.crop_h
      crop_w = pargs.crop_w
      if h >= crop_h and w >= crop_w:
        ni = int(ceil(h / crop_h))
        nj = int(ceil(w / crop_w))
        for i in range(ni):
          for j in range(nj):
            # Define start and stop for row
            start_i = i * crop_h
            stop_i = (i+1) * crop_h
            start_i = start_i if stop_i <= h else h - crop_h
            stop_i = stop_i if stop_i <= h else h
            # Define start and stop for cols
            start_j = j * crop_w
            stop_j = (j + 1) * crop_w
            start_j = start_j if stop_j <= w else w - crop_w
            stop_j = stop_j if stop_j <= w else w
            # Crop image & mask
            im = img[start_i:stop_i, start_j:stop_j, :]
            mk = mask[start_i:stop_i, start_j:stop_j]
            # Count number of skin pixels in the patch
            n_skin_px = mk.sum()
            if n_skin_px != 0:
              n_skin_px /= (crop_w * crop_h)
              # Dump
              im_name = join(pargs.output,
                             'images',
                             '{:09d}.jpg'.format(patch_cnt))
              lbl_name = join(pargs.output,
                              'labels',
                              '{:09d}.png'.format(patch_cnt))
              samples.append(('images/{:09d}.jpg'.format(patch_cnt), n_skin_px))
              patch_cnt += 1
              imwrite(im_name, im)
              imwrite(lbl_name, mk)
              # Write entry into samples file
              n_dir = len(pargs.output) + 1
              f.write('{},{}\n'.format(im_name[n_dir:], lbl_name[n_dir:]))
              # Log
              if (patch_cnt - 1) % 1000 == 0:
                logger.info('%d patches have been extracted ...', patch_cnt - 1)
      else:
        logger.warning('Drop image/mask, %dx%d < %dx%d', h, w, crop_h, crop_w)


def shuffle_patch(pargs):
  """
  Split patches into `train`, `validation` and `test` set
  :param pargs: Parsed arguemtns
  """
  logger.info('Split data into train/val/test set')
  # Reopen sample files
  entries = []
  header = ''
  with open(join(pargs.output, 'samples.txt'), 'rt') as f:
    header = f.readline().strip()
    for line in f:
      entries.append(line.strip())
  # shuffle entries
  entries = np.asarray(entries, dtype=np.object)
  np.random.shuffle(entries)

  # Split
  n = entries.shape[0]
  train_idx = int(round(pargs.train_size * n))
  val_idx = int(round((pargs.train_size + pargs.validation_size) * n))

  train = entries[:train_idx]
  validation = entries[train_idx:val_idx]
  test = entries[val_idx:]

  # Dump
  def _dump(path, header, data):
    """
    Dump entries into a text files
    :param path:    Path to the files where to store the data
    :param header:  File's header
    :param data:    List of entry to be dumped
    """
    with open(path, 'wt') as df:
      df.write('{}\n'.format(header))
      df.write('{},{}\n'.format(pargs.crop_h, pargs.crop_w))
      for entry in data:
        df.write('{}\n'.format(entry))

  _dump(path=join(pargs.output, 'train_set.txt'),
        header=header,
        data=train)
  _dump(path=join(pargs.output, 'validation_set.txt'),
        header=header,
        data=validation)
  _dump(path=join(pargs.output, 'test_set.txt'),
        header=header,
        data=test)


def shuffle_patch_records(pargs):
  """
  Split patches into `train`, `validation` and `test` set
  :param pargs: Parsed arguments
  """

  def _create_example_for_line(line: str):
    """
    Create a tf.train.Example from a given string
    :param folder:  Root folder where samples are stored
    :param line:    String to parse
    :return:  tf.train.Example
    """

    def _read_raw(path):
      """
      Load a given file into memory as a raw array
      :param path:  Path to the file
      :return:  Bytes array as a string
      """
      with open(path, 'rb') as f:
        return f.read()

    # Split entry
    im_path, label_path = line.strip().split(',')
    # Load images
    im_str = _read_raw(join(pargs.output, im_path))
    label_str = _read_raw(join(pargs.output, label_path))
    # Create example
    feat = {'image': to_feature(im_str),
            'label': to_feature(label_str)}
    return _tf.train.Example(features=_tf.train.Features(feature=feat))

  # Read all selected samples
  samples = []
  fname = join(pargs.output, 'samples.txt')
  with open(fname, 'r') as f:
    hdr = f.readline().strip()
    for line in f:
      samples.append(line)

  if samples:
    # Create output folder
    if not os.path.exists(join(pargs.output, 'records')):
      os.makedirs(join(pargs.output, 'records'))

    # random permutations
    n_valid = int(ceil(pargs.validation_size * len(samples)))
    n_test = int(ceil(pargs.test_size * len(samples)))
    n_train = len(samples) - n_valid - n_test

    rand_samples = np.random.permutation(samples)
    train_samples = rand_samples[:n_train]
    validation_samples = rand_samples[n_train:(n_train + n_valid)]
    test_samples = rand_samples[(n_train + n_valid):]
    # Dump into tfrecords files
    convert_csv_to_tfrecords(samples=train_samples,
                             parse_fn=_create_example_for_line,
                             shard_size=pargs.shard_size,
                             folder=pargs.output,
                             type='train')
    convert_csv_to_tfrecords(samples=validation_samples,
                             parse_fn=_create_example_for_line,
                             shard_size=pargs.shard_size,
                             folder=pargs.output,
                             type='validation')
    convert_csv_to_tfrecords(samples=test_samples,
                             parse_fn=_create_example_for_line,
                             shard_size=pargs.shard_size,
                             folder=pargs.output,
                             type='test')
    logger.info('Done')
  else:
    logger.error('No samples was found in: %s', fname)


if __name__ == '__main__':
  import argparse
  # Parser
  parser = argparse.ArgumentParser(description=__doc__)

  # Input
  parser.add_argument('--pratheepan_path',
                      type=str,
                      required=False,
                      help='Location where the pathreepan dataset is stored')
  parser.add_argument('--sfa_path',
                      type=str,
                      required=False,
                      help='Location where the SFA dataset is stored')
  parser.add_argument('--fsd_path',
                      type=str,
                      required=False,
                      help='Location where the FSD dataset is stored')
  parser.add_argument('--pld_path',
                      type=str,
                      required=False,
                      help='Location where the PLD dataset is stored')
  # Output
  parser.add_argument('--output',
                      type=str,
                      required=True,
                      help='Where to output the data')
  # Option
  parser.add_argument('--crop_w',
                      type=int,
                      default=200,
                      help='Width of the cropping box')
  parser.add_argument('--crop_h',
                      type=int,
                      default=200,
                      help='Width of the cropping box')
  parser.add_argument('--train_size',
                      type=float,
                      default=0.8,
                      help='Amount of data to put in the training set')
  parser.add_argument('--validation_size',
                      type=float,
                      default=0.1,
                      help='Amount of data to put in the validation set')
  parser.add_argument('--test_size',
                      type=float,
                      default=0.1,
                      help='Amount of data to put in the test set')
  parser.add_argument('--shard_size',
                      type=int,
                      default=20000,
                      help='Number maximum of samples per shard')
  # Parse
  args = parser.parse_args()
  # Generate data
  extract_image_patch(pargs=args)
  # Generate splits
  shuffle_patch_records(pargs=args)
