# coding=utf-8
"""
Load image data for SkinNet experiments
"""
import os.path as _path
import tensorflow as _tf
from lts5.tensorflow_op.data import DataProvider

__author__ = 'Christophe Ecabert'


def _corrupt_brightness(image, mask):
  """
  Randomly corrupt image brightness
  :param image:   Image
  :param mask:    Mask
  :return:  Corrupted image, or identical image
  """
  # 50% of chance to apply corruption
  cond = _tf.cast(_tf.random.uniform(shape=[],
                                     maxval=2,
                                     dtype=_tf.int32),
                  _tf.bool)
  # Apply correction or forward the image
  image = _tf.cond(cond,
                   lambda: _tf.image.random_hue(image, 0.1),
                   lambda: _tf.identity(image))
  return image, mask


def _corrupt_contrast(image, mask):
  """
  Randomly applies a random contrast change.

  :param image:   Image
  :param mask:    Mask
  :return:  Corrupted image, or identical image
  """
  # 50% of chance to apply corruption
  cond = _tf.cast(_tf.random.uniform(shape=[],
                                     maxval=2,
                                     dtype=_tf.int32),
                  _tf.bool)
  # Apply transform
  image = _tf.cond(cond,
                   lambda: _tf.image.random_contrast(image, 0.1, 1.1),
                   lambda: _tf.identity(image))
  return image, mask


def _corrupt_saturation(image, mask):
  """
  Randomly applies a random saturation change.

  :param image:   Image
  :param mask:    Mask
  :return:  Corrupted image, or identical image
  """
  # 50% of chance to apply corruption
  cond = _tf.cast(_tf.random.uniform(shape=[],
                                     maxval=2,
                                     dtype=_tf.int32),
                  _tf.bool)
  # Apply transform
  image = _tf.cond(cond,
                   lambda: _tf.image.random_saturation(image, 0.1, 1.1),
                   lambda: _tf.identity(image))
  return image, mask


def _random_flip(image, mask):
  """
  Perform randomly image flip
  :param image: Image
  :param mask:  Label
  :return:  Transformed image + label
  """
  from random import random
  seed = random()
  image = _tf.image.random_flip_left_right(image, seed=seed)
  mask = _tf.image.random_flip_left_right(mask, seed=seed)
  return image, mask


class DataLoader(DataProvider):
  """ Load image + label into TF framework using `tf.data` API """

  def __init__(self,
               filename,
               img_size,
               training,
               with_auxiliary_loss=False,
               n_parallel_call=None):
    """
    Constructor
    :param filename:        Glob pattern
    :param img_size:        Tuple of integers, (Width, Height)
    :param training:        If `True` indicates training phase
    :param with_auxiliary_loss: If `True` indicate auxiliary loss
    :param n_parallel_call: Number of parallel call the pipeline is allowed to
                            do
    """
    super(DataLoader, self).__init__()
    self._n_call = n_parallel_call
    self._training = training
    self._img_sz = img_size
    self._with_auxiliary_loss = with_auxiliary_loss
    self._source = _tf.data.Dataset.list_files(filename,
                                               shuffle=True)
    self._source = self._source.interleave(_tf.data.TFRecordDataset,
                             cycle_length=self._n_call,
                             num_parallel_calls=_tf.data.experimental.AUTOTUNE)

  def decode(self):
    """
    Decode one line of a sample text file. Output two tensors, the image and the
    corresponding label/mask
    :param line:  Single line from a sample file (i.e. csv)
    :return:  Image, mask
    """

    def _decode_fn(line):
      # Decode the line into its fields
      fields = _tf.io.decode_csv(records=line,
                                 record_defaults=[[''], ['']],
                                 field_delim=',')
      # Build absolute path
      im_path = _tf.strings.join(inputs=[self._root_dir, fields[0]])
      lbl_path = _tf.strings.join(inputs=[self._root_dir, fields[1]])
      # Load images
      img = _tf.image.decode_jpeg(_tf.io.read_file(im_path))
      img.set_shape([self._height, self._width, 3])
      label = _tf.image.decode_png(_tf.io.read_file(lbl_path), channels=1)
      label.set_shape([self._height, self._width, 1])
      return _tf.cast(img, _tf.float32), _tf.cast(label, _tf.float32)

    def _parse_fn(example):
      """
            convert an tf.train.Example into tensors
            :param example: Dataset example
            :return:  dict
            """
      # Parse TFExample records and perform simple data augmentation.
      fmt = {
        'image': _tf.io.FixedLenFeature([], _tf.string, ""),
        'label': _tf.io.FixedLenFeature([], _tf.string, ""),
      }
      parsed = _tf.io.parse_single_example(example, fmt)
      # Image
      image = _tf.io.decode_jpeg(parsed['image'])
      image.set_shape((self._img_sz[0], self._img_sz[1], 3))
      image = _tf.cast(image, _tf.float32)
      # Label
      label = _tf.io.decode_png(parsed['label'], channels=1)
      label.set_shape((self._img_sz[0], self._img_sz[1], 1))
      label = _tf.cast(label, _tf.float32)
      return image, label

    # Decode samples
    self._source = self._source.map(_parse_fn, _tf.data.experimental.AUTOTUNE)
    return self

  def preprocess(self):
    """
    Add pre-processing to the input pipeline
    :return: `self`
    """

    def _normalize(image, label):
      if self._with_auxiliary_loss:
        return image, (label, label)
      else:
        return image, label

    # Apply pre-processing
    if self._training:
      self._source = self._source.map(_corrupt_brightness, self._n_call)
      self._source = self._source.map(_corrupt_contrast, self._n_call)
      self._source = self._source.map(_corrupt_saturation, self._n_call)
      self._source = self._source.map(_random_flip, self._n_call)
    self._source = self._source.map(_normalize, self._n_call)
    return self
