#!/bin/bash
DATASETS_DIR="/media/christophe/Data/Datasets/Skin"
DATA_DIR="${DATASETS_DIR}/skinnet"
MODEL_DIR="/home/christophe/Documents/LTS5/Data/models"
OUTPUT_DIR="${MODEL_DIR}/PSPNet_res18"
LEARNING_RATE=1e-2
BATCH_SIZE=16
N_EPOCH=100

# Check if a mod is given
if [ "$#" -ne 1 ]; then
    echo "Wrong number of parameters"
    echo "$0 <mod>"
    echo "Possible mod: <Prepare> <Clear> <Train>"
    exit
fi

case "$1" in

    "Prepare")
        echo "Prepare data for training/validation/testing ..."
        python data_preparation.py --pratheepan_path ${DATASETS_DIR}/pratheepan --sfa_path ${DATASETS_DIR}/sfa --fsd_path ${DATASETS_DIR}/fsd --pld_path ${DATASETS_DIR}/pld --output ${DATA_DIR}
        ;;

    "Clear")
        read -p "Clear previous model in: ${OUTPUT_DIR}. Continue [y/n] ?" CHOICE
		case "$CHOICE" in
			y|Y )
				rm -rf ${OUTPUT_DIR}
			;;
			# Deal with other letter
			* ) echo "Stop current action";;
		esac
		;;

	"Train")
		echo "Train .."
		#python skinnet_train.py --train_set "${DATA_DIR}/train_set.txt" --validation_set "${DATA_DIR}/validation_set.txt" --test_set "${DATA_DIR}/test_set.txt" --model_dir ${OUTPUT_DIR}/nf32bck4ja --lr ${LEARNING_RATE} --n_epoch ${N_EPOCH} --batch_size ${BATCH_SIZE} --n_filter 32 --n_block 4 --cost_func jaccard
		#python skinnet_train.py --train_set "${DATA_DIR}/train_set.txt" --validation_set "${DATA_DIR}/validation_set.txt" --test_set "${DATA_DIR}/test_set.txt" --model_dir ${OUTPUT_DIR}/nf32bck5ja --lr ${LEARNING_RATE} --n_epoch ${N_EPOCH} --batch_size ${BATCH_SIZE} --n_filter 32 --n_block 5 --cost_func jaccard
		#python skinnet_train.py --train_set "${DATA_DIR}/train_set.txt" --validation_set "${DATA_DIR}/validation_set.txt" --test_set "${DATA_DIR}/test_set.txt" --model_dir ${OUTPUT_DIR}/nf64bck4ja --lr ${LEARNING_RATE} --n_epoch ${N_EPOCH} --batch_size ${BATCH_SIZE} --n_filter 64 --n_block 4 --cost_func jaccard
		python pspnet_train.py --train_set "${DATA_DIR}/records/train_*.tfrecords" --validation_set "${DATA_DIR}/records/validation_*.tfrecords" --test_set "${DATA_DIR}/records/test_*.tfrecords" --model_dir ${OUTPUT_DIR} --lr ${LEARNING_RATE} --n_epoch ${N_EPOCH} --batch_size ${BATCH_SIZE}
		;;


	*)
		echo "Unknown mod"
		;;
esac
