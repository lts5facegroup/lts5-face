/**
 *  @file   Noise_fct.cpp
 *  @brief  Noise functions
 *
 *  @author Valentine Santarelli
 *  @date   28/07/2016
 *  Copyright (c) 2016 Valentine Santarelli. All rights reserved.
 */

#include <vector>
#include <iostream>

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "lts5/utils/file_io.hpp"
#include "lts5/utils/sys/file_system.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/intraface_tracker.hpp"


#include "Noise_fcts.hpp"
#include "FT_errors.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name   Awgn
 *  @brief Add Gaussian noise
 *  @param[in]  img, image to add gaussian noise to
 *  @param[in]  intensity, intensity of the noise to add
 */
void Awng(cv::Mat& img , int intensity) {
  cv::Mat noise = cv::Mat(img.size(), img.type());
  cv::randn(noise, 0, intensity * 10);
  img = img + noise;

}

/*
 *  @name   Jpeg
 *  @brief Change the jpeg quality
 *  @param[in]  img , image on which the jpeg quality changes
 *  @param[in]  intensity, intensity of the jpeg quality drop
 *  @param[in]  CV_IMWRITE_JPEG_QUALITY, parameter when encoding a jpeg image , gives the quality of the jpeg image to encode, default value=95.
 */
void Jpeg(cv::Mat& img, int intensity, int CV_IMWRITE_JPEG_QUALITY){
  std::vector<int> p;
  p.push_back(CV_IMWRITE_JPEG_QUALITY);
  p.push_back(100 - intensity * 10);
  std::vector<unsigned char> buf;
  cv::imencode(".jpg", img, buf, p);
  img =  cv::imdecode(buf, 1);

}

/*
 *  @name   Blur
 *  @brief Add blur to the image
 *  @param[in]  img , image to add  blur to
 *  @param[in]  intensity, intensity of the blur to add
 *  @param[in]  BORDER_DEFAULT,border mode used to extrapolate pixels outside of the image
 */
void Blur(cv::Mat& img, int intensity, int BORDER_DEFAULT) {
  cv::Mat frame=img.clone();
  blur(frame, frame, cv::Size(21,21), cv::Point(-1,-1), BORDER_DEFAULT);
  GaussianBlur(img, img, cv::Size(51, 51), intensity, intensity, BORDER_DEFAULT);
}

/*
 *  @name   OvExp
 *  @brief Simulate an overexposure on the image
 *  @param[in]  img , image to add  overexposure to
 *  @param[in]  intensity, intensity of the overexposure
 */
void OvExp(cv::Mat& img, int intensity) {
  img = (1 + 0.25 * intensity) * img;
}

/*  @name   Oc
 *  @brief Simulate an occlusion of the mouth
 *  @param[in]  img , image to add  an occlusion to
 *  @param[in]  position, position of the occlusion on the mouth (cover half the mouth, the middle ,the left corner)
 *  @param[in]  annotations , annotations or landmarks of the face.
 */
void Oc(cv::Mat& img, int position, cv::Mat& annotation) {
    /*The drawn rectangles that simulate the occlusion of the mouth  are computed from the mouth landmarks the facetracker computes
  or the mouth annotations if they are available.*/

  //draw a black rectangle on the middle of the mouth
  if (position == 2) {

    cv::Point Pt1 (int(annotation.at<double>(51)),
                   int(annotation.at<double>(51 + annotation.rows/2) - 20));
    cv::Point Pt2 (int(annotation.at<double>(53)),
                   int(annotation.at<double>(58 + annotation.rows/2) + 20));
    cv::rectangle(img, Pt1, Pt2, cv::Scalar(0, 0, 0), -1);
  }
  //draw a black rectangle on the left corner of the mouth
  else if (position == 1) {
    cv::Point Pt1(int(annotation.at<double>(49) - 20),
                  int(annotation.at<double>(51 + annotation.rows/2) - 20));
    cv::Point Pt2(int(annotation.at<double>(62) - 10),
                  int(annotation.at<double>(58 + annotation.rows/2) + 20));
    cv::rectangle(img, Pt1, Pt2, cv::Scalar(0, 0, 0), -1);
    std::cout<< "Position 1"<< std::endl;

  }
  //draw a black rectangle on half of the mouth
  else if (position == 0) {
    cv::Point Pt1(int(annotation.at<double>(49) - 20),
                  int(annotation.at<double>(51 + annotation.rows/2) - 20));
    cv::Point Pt2(int(annotation.at<double>(52)),
                  int(annotation.at<double>(58 + annotation.rows/2) + 20));
    cv::rectangle(img, Pt1, Pt2, cv::Scalar(0, 0, 0), -1);
    std::cout<< "Position 0"<< std::endl;
  }
  else {
    std::cout << "Error while Occlusion: position must be 0, 1 or 2." << std::endl;
  }
}


/*  @name   AddNoise
 *  @brief Add the different noise at different intensities on all the images in the input directory
 *  @param[in]  input_dir, path to the directory where all the images to add noise to are stored
 *  @param[in]  output_dir, path to the directory  to store the noisy images
 *  @param[in]  face_detector_, reference to the face detector (used when the occlusion is added to the image)
 *  @param[in]  face_tracker_, reference to the face tracker (used when the occlusion is added to the image)
 *
 */
void AddNoise(const std::string& input_dir,
              const std::string& output_dir,
              cv::CascadeClassifier& face_detector_,
              LTS5::SdmTracker& face_tracker_) {
  std::vector<std::string> images_files;
  images_files = LTS5::ScanImagesInFolder(input_dir);
  std::sort(images_files.begin(), images_files.end());
  const int k_total_images = static_cast<int>(images_files.size());
  std::cout << k_total_images << " images found in the folder" << std::endl;
  std::string image_filename;

  //Apply the different noise
  std::vector<std::string> noise = {"awgn", "jpeg", "blur", "over", "occl"};

  std::vector<int> intensity;

  //For loop on the noise
  auto* fs = GetDefaultFileSystem();
  for (int b=0; b < 5; b++) {

    std::string noise_path(output_dir + noise[b]);
    fs->CreateDirRecursively(noise_path);

    if(noise[b] == "awgn") {
      //Apply the noise at different intensities
      intensity = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  13, 14, 15};

      //For loop on the intensities
      for (int j = 0; j < intensity.size(); j++) {
        std::string intensity_path (output_dir  + noise[b] + "/" + std::to_string(intensity[j]));
        fs->CreateDirRecursively(intensity_path);

        //For loop on every images from the input_dir directory
        for (int i = 0; i<k_total_images; i++) {
          image_filename = images_files[i];
          cv::Mat working_img_ = cv::imread(image_filename); //read and store the image in a matrix

          //Convert image to B&W
          cv::cvtColor(working_img_, working_img_, CV_BGR2GRAY);

          //Add noise
          cv::Mat im_noise;
          Awng(working_img_, intensity[j]);
          im_noise= working_img_;

          //Recover the noisy image
          std::string img_filename;
          ImgName(image_filename, img_filename);
          std::cout << "Saving image : "<< output_dir  + noise[b] + "/" +
              std::to_string(intensity[j]) + "/" +  img_filename << std::endl;
          cv::imwrite(output_dir  + noise[b] + "/" + std::to_string(intensity[j])
                      + "/" +  img_filename, im_noise);
        }
      }
    }

    if (noise[b] == "jpeg") {
      intensity = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

      //For loop on the intensities
      for (int j = 0; j < intensity.size(); j++) {
        std::string intensity_path (output_dir  + noise[b] + "/" +
                                                    std::to_string(intensity[j]));
        fs->CreateDirRecursively(intensity_path);

        //For loop on every images from the input_dir directory
        for (int i = 0; i<k_total_images; i++) {
          image_filename = images_files[i];
          cv::Mat working_img_ = cv::imread(image_filename);

          //Convert image to B&W
          cv::cvtColor(working_img_, working_img_, CV_BGR2GRAY);

          //Add noise
          cv::Mat im_noise;
          Jpeg(working_img_, intensity[j], CV_IMWRITE_JPEG_QUALITY);
          im_noise=working_img_;

          //Recover the noisy image
          std::string img_filename;
          ImgName(image_filename, img_filename);
          std::cout << "Saving image : "<< output_dir  + noise[b] + "/" + std::to_string(intensity[j]) + "/" +  img_filename<< std::endl;
          cv::imwrite(output_dir  + noise[b] + "/" + std::to_string(intensity[j]) + "/" +  img_filename, im_noise);
        }
      }
    }

    if (noise[b] == "blur") {
      intensity = {1, 2, 3, 4, 5, 6, 7, 15, 25, 50};

      //For loop on the intensities
      for (int j = 0; j < intensity.size(); j++) {
        std::string intensity_path(
                output_dir + noise[b] + "/" + std::to_string(intensity[j]));
        fs->CreateDirRecursively(intensity_path);

        //For loop on every images from the input_dir directory
        for (int i = 0; i < k_total_images; i++) {
          image_filename = images_files[i];
          cv::Mat working_img_ = cv::imread(image_filename);

          //Convert image to B&W
          cv::cvtColor(working_img_, working_img_, CV_BGR2GRAY);

          //Add noise
          cv::Mat im_noise;
          Blur(working_img_, intensity[j], cv::BORDER_DEFAULT);
          im_noise = working_img_;

          //Recover the noisy image
          std::string img_filename;
          ImgName(image_filename, img_filename);
          std::cout << "Saving image : " <<
          output_dir + noise[b] + "/" + std::to_string(intensity[j]) + "/" +
          img_filename << std::endl;
          cv::imwrite(
                  output_dir + noise[b] + "/" + std::to_string(intensity[j]) +
                  "/" + img_filename, im_noise);

        }
      }
    }
      std::cout <<noise[b]<<std::endl;

    if (noise[b] == "over"){
      intensity ={1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};

      //For loop on the intensities
      for (int j = 0; j < intensity.size(); j++) {
        std::string intensity_path (output_dir  + noise[b] + "/" + std::to_string(intensity[j]));
        fs->CreateDirRecursively(intensity_path);

        //For loop on every images from the input_dir directory
        for (int i = 0; i < k_total_images; i++) {
          image_filename = images_files[i];
          cv::Mat working_img_ = cv::imread(image_filename);

          //Convert image to B&W
          cv::cvtColor(working_img_, working_img_, CV_BGR2GRAY);

          //Add noise
          cv::Mat im_noise;
          OvExp(working_img_, intensity[j]);
          im_noise=working_img_;

          //Recover the noisy image
          std::string img_filename;
          ImgName(image_filename, img_filename);
          std::cout << "Saving image : "<< output_dir  + noise[b] + "/" + std::to_string(intensity[j]) + "/" +  img_filename<< std::endl;
          cv::imwrite(output_dir  + noise[b] + "/" + std::to_string(intensity[j]) + "/" +  img_filename, im_noise);
        }
      }
    }


    if (noise[b] == "occl") {

      intensity = {0, 1, 2};

      //For loop on the intensities
      for (int j = 0; j < intensity.size(); j++) {
        std::string intensity_path (output_dir  + noise[b] + "/" + std::to_string(intensity[j]));
        fs->CreateDirRecursively(intensity_path);

        //For loop on every images from the input_dir directory
        for (int i = 0; i < k_total_images; i++) {
          image_filename = images_files[i];
          cv::Mat working_img_ = cv::imread(image_filename);
          std::string img_filename;
          ImgName(image_filename, img_filename);

          //Get the annotations
          cv::Mat annotation;
          GetAnnotations( input_dir, img_filename, annotation);

          //If the annotations are not available, get the landmarks
          if (annotation.empty()) {
            cv::Mat landmarks;
            cv::Mat face_landmarks;
            bool detect = true;
            GetLandmarks(working_img_, img_filename,  landmarks,
                         face_landmarks,annotation,  face_detector_,  face_tracker_, detect);
            annotation= face_landmarks;
          }

          //Convert image to B&W
          cv::cvtColor(working_img_, working_img_, CV_BGR2GRAY);

          //Add noise
          cv::Mat im_noise;
          Oc(working_img_, intensity[j], annotation);
          im_noise = working_img_;

          //Recover the noisy image
          std::cout << "Saving image : "<< output_dir  + noise[b] + "/" + std::to_string(intensity[j]) + "/" +  img_filename<< std::endl;
          cv::imwrite(output_dir + noise[b] + "/" + std::to_string(intensity[j]) + "/" +  img_filename, im_noise);

        }
      }
    }
  }
}


/*
* @name ImgName
* @brief recover the image name from the path to the image
* @param[in] img_filename,path to the image
* @param[in] img_name, image name
*/
void ImgName(std::string& img_filename, std::string& img_name) {
  size_t pos = img_filename.rfind('/');
  std::string name = img_filename.substr(pos+1, img_filename.length());
  img_name = name;
}

}  // namespace LTS5