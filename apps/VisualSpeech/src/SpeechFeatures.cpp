/**
 *  @file   SpeechFeatures.cpp
 *  @brief  SpeechFeatures class
 *
 *  @author Marina Zimmermann
 *  @date   28/07/2016
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */


#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#ifdef HAS_INTRAFACE
#include "XXDescriptor.h"
#endif

#include "lts5/utils/file_io.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "SpeechFeatures.hpp"

//#define USE_MEAN_NORMALIZATION
#define USE_HIST_EQUA_NORMALIZATION

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

#pragma mark -
#pragma mark Initialization

/**
 *  @name SpeechFeatures
 *  @fn SpeechFeatures(void)
 *  @brief  Constructor
 */
SpeechFeatures::SpeechFeatures(void) : pose_normalization_(nullptr),
                                       do_feature_normalization_(true) {
  cumulative_mouth_width_ = 0;
  cumulative_mouth_height_ = 0;
  mouth_roi_width_ = 0;
  mouth_roi_height_ = 0;
  frame_counter_ = 0;
  frame_count_1_ = 0;
  frame_count_2_ = 0;
  run_ = 1;
}

/*
 *  @name   ~SpeechFeatures
 *  @brief  Destructor
 */
SpeechFeatures::~SpeechFeatures(void) {
  // Release memory
  this->ReleaseMemory();
}

/**
  *  @name Load_PDM
  *  @fn int Load_PDM(const std::string& filename)
  *  @brief  Load point distribution model from binary file :
  *            .hf
  *  @param[in]  filename  Path to the binary file
  *  @return -1 if error, 0 otherwise
*/
int SpeechFeatures::Load_PDM(const std::string& filename) {
  int error = -1;
  std::string name;
  size_t pos = filename.rfind(".");
  if (pos != std::string::npos) {
    name = filename.substr(0,pos) + ".bin";
  } else {
    name = filename + ".bin";
  }
  // Open stream
  std::ifstream in_stream(name.c_str(),
                          std::ios_base::in | std::ios_base::binary);
  if (in_stream.is_open()) {
    // Ok, read configuration
    // Load PDM
    if (pose_normalization_ == nullptr) {
      pose_normalization_ = new LTS5::PointDistributionModel();
    }
    int status = LTS5::ScanStreamForObject(in_stream,
                                           HeaderObjectType::kPointDistributionModel);
    if (status == 0) {
      error |= pose_normalization_->Load(in_stream, true);
    }
    in_stream.close();
  }
  return error;
}

#pragma mark -
#pragma mark Extract ROI

/*
 *  @name   ExtractMouthImg
 *  @brief  Extract mouth image and separate mouth shape
 *  @param[in]  image   Source image
 *  @param[in]  shape   Facial landmarks
 *  @param[in]  bounding_box   Bool stating whether to work on BB
 *  @param[out] mouth   Extracted mouth image, normalised
 *  @param[out] mouth_shape   Extracted mouth shape, normalised
 */
int SpeechFeatures::ExtractMouthImg(const cv::Mat& image,
                                     const cv::Mat& shape,
                                     const bool bounding_box,
                                     cv::Mat* mouth,
                                     cv::Mat* mouth_shape,
                                     cv::Mat* normalized_image,
                                     cv::Mat* normalized_shape) {
  if (image.channels() > 1) {
    cv::cvtColor(image, working_image_, CV_BGR2GRAY);
  } else {
    image.copyTo(working_image_);
  }
  // Normalize input image
  int n_pts = std::max(shape.rows,
                       shape.cols) / 2;
  if (n_pts != 68) {
    pose_normalization_->NormSDMImage_intra(working_image_,
                                            shape,
                                            normalized_image,
                                            normalized_shape);
  } else {
    pose_normalization_->NormSDMImage(working_image_,
                                      shape,
                                      true,
                                      normalized_image,
                                      normalized_shape);
  }

  if (normalized_image == nullptr || normalized_shape == nullptr) {
    return 1;
  }

  // Illumination normalization
#if defined(USE_MEAN_NORMALIZATION)
  cv::Mat img32;
  normalized_image->convertTo(img32, CV_32FC1);
  const int n_elem_img = static_cast<int>(img32.total());
  float* img_ptr = reinterpret_cast<float*>(img32.data);
  float sum = 0;
  for (int i = 0; i < n_elem_img; ++i) {
    sum += *img_ptr++;
  }
  const float img_mean = sum/static_cast<float>(n_elem_img);
  img_ptr = reinterpret_cast<float*>(img32.data);
  for (int i = 0; i < n_elem_img; ++i) {
    *img_ptr++ /= img_mean;
  }
  // TODO:(Christophe) Check if it can be avoid in future
  // Copy back into source Image
  img32.copyTo(*normalized_image);
#elif defined(USE_HIST_EQUA_NORMALIZATION)
  // Apply histogram equalization first
  cv::equalizeHist(*normalized_image, *normalized_image);
#endif

  cv::Mat mouth_shape_temp;
  std::vector<cv::Point> mouth_shape_vector;

  if (bounding_box) {
    // Extract image exactly according to bounding box
    for (std::size_t i = 0; i < sizeof(mouth_indices_)/sizeof(*mouth_indices_) - 2; i++) {
      double x = normalized_shape->at<double>(mouth_indices_[i] - 17);
      double y = normalized_shape->at<double>(
          mouth_indices_[i] + n_inner_points_ - 17 - 17 - 2); // check offset
      mouth_shape_vector.push_back(cv::Point(int(x), int(y)));
    }
    mouth_shape_temp = cv::Mat(mouth_shape_vector);
    cv::Rect mouth_rect = cv::boundingRect(mouth_shape_temp);
    (*mouth_shape) = mouth_shape_temp.reshape(1, 2 * (sizeof(mouth_indices_)/sizeof(*mouth_indices_) - 2)).t();
    (*normalized_image)(mouth_rect).copyTo(*mouth);
  }
  else {
    // Extract image according to mean size + margin
    for (std::size_t i = 0; i < sizeof(mouth_indices_)/sizeof(*mouth_indices_) - 2; i++) {
      double x = normalized_shape->at<double>(mouth_indices_[i] - 17);
      double y = normalized_shape->at<double>(
          mouth_indices_[i] + n_inner_points_ - 17 - 17 - 2); // check offset
      mouth_shape_vector.push_back(cv::Point(int(x), int(y)));
    }
    mouth_shape_temp = cv::Mat(mouth_shape_vector);
    cv::Rect mouth_rect = cv::boundingRect(mouth_shape_temp);
    (*mouth_shape) = mouth_shape_temp.reshape(1, 2 * (sizeof(mouth_indices_)/sizeof(*mouth_indices_) - 2)).t();
    if (run_ == 1) {
      cumulative_mouth_width_ += mouth_rect.width;
      cumulative_mouth_height_ += mouth_rect.height;
      frame_counter_++;
      frame_count_1_++;
    }
    else if (run_ == 2) {
      mouth_rect.x -= (mouth_roi_width_ - mouth_rect.width) / 2;
      mouth_rect.width = mouth_roi_width_;
      mouth_rect.y -= (mouth_roi_height_ - mouth_rect.height) / 2;
      mouth_rect.height = mouth_roi_height_;

      if (mouth_rect.x < 0) {
        mouth_rect.x = 0;
      }
      if ((mouth_rect.x + mouth_rect.width) > (*normalized_image).size().width) {
        mouth_rect.x = (*normalized_image).size().width - mouth_rect.width;
      }
      if (mouth_rect.y < 0) {
        mouth_rect.y = 0;
      }
      if ((mouth_rect.y + mouth_rect.height) > (*normalized_image).size().height) {
        mouth_rect.y = (*normalized_image).size().height - mouth_rect.height;
      }

      (*normalized_image)(mouth_rect).copyTo(*mouth);
      frame_counter_++;
      frame_count_2_++;
    }
  }
  if (mouth == nullptr) {
    return 1;
  }

  return 0;
}

#pragma mark -
#pragma mark Private

/*
 *  @name   ReleaseMemory
 *  @brief  Release classifer and stuff
 */
void SpeechFeatures::ReleaseMemory(void) {
  // Clear pdm
  if (pose_normalization_ != nullptr) {
    delete pose_normalization_;
    pose_normalization_ = nullptr;
  }
}

}  // namespace LTS5
