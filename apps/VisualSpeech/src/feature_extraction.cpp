/**
 *  @file   feature_extraction.cpp
 *  @brief  Speech feature extraction
 *
 *  @author Marina Zimmermann
 *  @date   28/07/2016
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */

#include <iostream>

#include "opencv2/opencv.hpp"

#include "SpeechFeatures.hpp"
#include "Noise_fcts.hpp"
#include "lts5/utils/cmd_parser.hpp"
#include "lts5/ogl/glfw_backend.hpp"
#include "lts5/ogl/texture.hpp"
#include "lts5/ogl/image_renderer.hpp"

#define USE_MEAN_NORMALIZATION
//#define USE_HIST_EQUA_NORMALIZATION

struct CustomCallback : LTS5::OGLCallback {

  /** Shutdown flag */
  bool quit_;
  /** Show landmarks */
  bool print_landmark_;

  /**
   * @name  CustomCallback
   * @fn  CustomCallback(void)
   * @brief Constructor
   */
  CustomCallback(void) : quit_(false), print_landmark_(false) {}
};

static LTS5::SdmTracker* face_tracker_ = nullptr;
static CustomCallback callback_;
static LTS5::OGLGlfwBackend* backend_ = nullptr;
static LTS5::OGLTexture* tex_ = nullptr;
static LTS5::OGLImageRenderer* img_renderer_ = nullptr;

int create_video_display(const int argc, const char** argv, const int img_width, const int img_height) {
  int err = -1;
  // Init backend, create context
  backend_ = new LTS5::OGLGlfwBackend();
  backend_->Init(argc, argv, false, false, true);
  if (backend_->CreateWindows(img_width,
                              img_height,
                              false,
                              "Feature extraction")) {
    // Init some opengl
    backend_->EnableBlending();
    backend_->SetCallbacks(&callback_);
    // Create texture
    //cv::Mat dummy(img_height, img_width, CV_8UC3);
    cv::Mat dummy(img_height, img_width, CV_32FC1);
    tex_ = new LTS5::OGLTexture(dummy,
                                GL_LINEAR,
                                GL_CLAMP_TO_EDGE,
                                false);
    // Create image renderer
    img_renderer_ = new LTS5::OGLImageRenderer();
    img_renderer_->AddTexture(tex_);
    img_renderer_->SetWindowDimension(img_width, img_height);
    img_renderer_->SetupView(0, 0, img_width, img_height);
    err = img_renderer_->BindToOpenGL();
  }
  return err;
}

void display_video(cv::Mat& img, cv::Mat& shape) {
  // Clear opengl
  backend_->ClearBuffer();
  int n_pts = shape.rows / 2;
  for (int i = 0; i < n_pts; ++i) {
    cv::circle(img,
               cv::Point(int(shape.at<double>(i)),
                         int(shape.at<double>(i + n_pts))),
               1,
               cv::Scalar(255),
               -1);
  }

  // Display
  // ----------------------------------------------------
  // Upload
  //int ret =
  tex_->Upload(img);
  // Render
  img_renderer_->Render();
  // Swap
  backend_->SwapBuffer();
  // Poll
  backend_->PollEvents();
}

void close_video_display() {
  // Clean up
  delete img_renderer_;
  img_renderer_ = nullptr;
  delete tex_;
  tex_ = nullptr;
}


int main(int argc, const char * argv[]) {

  typedef LTS5::CmdLineParser::ArgState State;
  LTS5::CmdLineParser parser;
  parser.AddArgument("-c",
                     State::kNeeded,
                     "Extractor configuration file (.xml)");
  // Process inputs
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Open config file
    std::string config_path;
    parser.HasArgument("-c", &config_path);
    cv::FileStorage storage(config_path, cv::FileStorage::READ);
    if (storage.isOpened()) {
      // Path var
      std::string sdm_model_path;
      std::string f_detector_path;
      std::string video_path;
      int dct_width;
      int dct_height;
      int feature_length;
      int dct_bool;
      int dct_on_bounding_box;
      int all_feats;
      int save_images;
      std::string log_path;
      std::string feature_storage_path;
      int show_videos;
      int use_noises;
      // Go root
      cv::FileNode root = storage.getFirstTopLevelNode();
      LTS5::ReadNode(root, "sdm_model_path", &sdm_model_path);
      LTS5::ReadNode(root, "face_detect_model_path", &f_detector_path);
      LTS5::ReadNode(root, "video_path", &video_path);
      LTS5::ReadNode(root, "dct_width", &dct_width);
      LTS5::ReadNode(root, "dct_height", &dct_height);
      LTS5::ReadNode(root, "feature_length", &feature_length);
      LTS5::ReadNode(root, "dct_bool", &dct_bool);
      LTS5::ReadNode(root, "dct_on_bounding_box", &dct_on_bounding_box);
      LTS5::ReadNode(root, "all_feats", &all_feats);
      LTS5::ReadNode(root, "save_images", &save_images);
      LTS5::ReadNode(root, "log_path", &log_path);
      LTS5::ReadNode(root, "feature_storage_path", &feature_storage_path);
      LTS5::ReadNode(root, "show_videos", &show_videos);
      LTS5::ReadNode(root, "use_noises", &use_noises);

      std::vector<std::string> video_files;
      int k_total_videos;

      // Open video
      err = !video_path.empty();
      if (!err) {
        std::cerr
            << "The video directory is not set... Please specify a video directory"
            << std::endl;
        return err;
      }
        // Check if it is a single video file
      else if (video_path.find(".mp4") != std::string::npos ||
               video_path.find(".avi") != std::string::npos) {
        video_files.push_back(video_path);
        k_total_videos = 1;
      }
        // Go through video folder
      else {
        // parse the video folder!
        video_files = LTS5::ScanVideosInFolder(video_path);
        std::sort(video_files.begin(), video_files.end());
        k_total_videos = static_cast<int>(video_files.size());
        std::cout << k_total_videos << " videos found in the folder"
                  << std::endl;
      }

      std::ifstream sdm_stream;
      sdm_stream.open(sdm_model_path);
      LTS5::PrintStreamContent(sdm_stream);

      if (show_videos) {
        create_video_display(argc, argv, 200, 200);
      }

      for (int i = 0; i < k_total_videos; i++) {
        // Create the feature and log file names and create dirs
        size_t name_start = video_files[i].find_last_of("\\/") + 1;
        size_t name_len = video_files[i].find_last_of(".") - name_start;
        std::string filename = video_files[i].substr(name_start, name_len);
        // DCT with boundaries
        std::string feature_filename_full =
            feature_storage_path + "/full_dct/" + filename + ".hf";
        std::string feature_filename_odd =
            feature_storage_path + "/odd_dct/" + filename + ".hf";
        std::string feature_filename_even_odd =
            feature_storage_path + "/even_odd_dct/" + filename + ".hf";
        // DCT on bounding box
        std::string feature_filename_full_bb =
            feature_storage_path + "/full_dct_bb/" + filename + ".hf";
        std::string feature_filename_odd_bb =
            feature_storage_path + "/odd_dct_bb/" + filename + ".hf";
        std::string feature_filename_even_odd_bb =
            feature_storage_path + "/even_odd_dct_bb/" + filename + ".hf";
        // Shapes
        std::string feature_filename_mouth =
            feature_storage_path + "/mouth/" + filename + ".hf";
        std::string feature_filename_shape =
            feature_storage_path + "/shape/" + filename + ".hf";
        // Log file
        std::string log_filename =
            feature_storage_path + "/" + filename + ".log";
        // Image dirs
        std::string image_path =
            feature_storage_path + "/images/" + filename + "/";
        std::string mkdir_command = "mkdir " + image_path;
        if (save_images) {
          system(mkdir_command.c_str());
        }

        // DCT with boundaries
        std::ofstream feat_file_full;
        std::ofstream feat_file_odd;
        std::ofstream feat_file_even_odd;
        // DCT on bounding box
        std::ofstream feat_file_full_bb;
        std::ofstream feat_file_odd_bb;
        std::ofstream feat_file_even_odd_bb;
        // Shapes
        std::ofstream feat_file_mouth;
        std::ofstream feat_file_shape;
        //Log file
        std::ofstream log_file;

        // DCT with boundaries
        feat_file_full.open(feature_filename_full);
        feat_file_odd.open(feature_filename_odd);
        feat_file_even_odd.open(feature_filename_even_odd);
        // DCT on bounding box
        feat_file_full_bb.open(feature_filename_full_bb);
        feat_file_odd_bb.open(feature_filename_odd_bb);
        feat_file_even_odd_bb.open(feature_filename_even_odd_bb);
        // Shapes
        feat_file_mouth.open(feature_filename_mouth);
        feat_file_shape.open(feature_filename_shape);
        // Log file
        log_file.open(log_filename);

        // Create matrices
        // DCT with boundaries
        cv::Mat mat_full;
        cv::Mat mat_odd;
        cv::Mat mat_even_odd;
        // DCT on bounding box
        cv::Mat mat_full_bb;
        cv::Mat mat_odd_bb;
        cv::Mat mat_even_odd_bb;
        // Shapes
        cv::Mat mat_mouth;
        cv::Mat mat_shape;

        // Create matrices to read to iteratively
        cv::Mat frame;
        cv::Mat mouth;
        cv::Mat shape;
        cv::Mat mouth_shape;
        cv::Mat normalized_image;
        cv::Mat normalized_shape;

        // Create feature extractor
        LTS5::SpeechFeatures *feature_extractor_ = new LTS5::SpeechFeatures();
        feature_extractor_->Load_PDM(sdm_model_path);

        if (feature_extractor_ != nullptr) {
          if ((dct_bool && !dct_on_bounding_box) || all_feats) {
            // Start reading the video
            cv::VideoCapture video_capt(video_files[i]);
            err = !video_capt.isOpened();

            if (!err) {
              // Create tracker
              face_tracker_ = new LTS5::SdmTracker();
              err = face_tracker_->Load(sdm_model_path, f_detector_path);
              face_tracker_->set_score_threshold(-1.5);

              if (!err) {
                // Read frames
                while (1) {
                  err = !video_capt.read(frame);
                  if (!err) {
                    // Tracker
                    int err_track = face_tracker_->Track(frame, &shape);
                    if (!err_track && !shape.empty()) {
                      // Feature extraction
                      err = feature_extractor_->ExtractMouthImg(frame, shape, false,
                                                          &mouth, &mouth_shape,
                                                          &normalized_image, &normalized_shape);
                      if (!err) {
                        cv::Mat subframe = normalized_image(cv::Rect(0, 0, 200, 200));
                        if (show_videos) {
                          display_video(subframe, normalized_shape);
                        }
                      } else {
                        log_file << "Error extracting features for frame no. "
                                 << feature_extractor_->get_frame_counter()
                                 << std::endl;
                      }
                    } else {
                      log_file << "Error tracking in frame no. "
                               << feature_extractor_->get_frame_counter() + 1
                               << std::endl;
                    }
                  } else {
                    log_file << "End of video run 1 at frame "
                             << feature_extractor_->get_frame_counter()
                             << std::endl;
                    break;
                  }
                }
                // Clean up
                delete face_tracker_;
                face_tracker_ = nullptr;
              } else {
                log_file << "Error creating face tracker" << std::endl;
              }
            } else {
              log_file << "Error opening video" << std::endl;
            }
            log_file << "Number of frames in run 1 "
                     << feature_extractor_->get_frame_count_1() << std::endl;
          }


          feature_extractor_->set_run(2);

          // Start reading the video
          cv::VideoCapture video_capt(video_files[i]);
          err = !video_capt.isOpened();
          if (!err) {
            // Create tracker
            face_tracker_ = new LTS5::SdmTracker();
            err = face_tracker_->Load(sdm_model_path, f_detector_path);
            face_tracker_->set_score_threshold(-1.5);
            if (!err) {
              // Read frames
              while (1) {
                cv::Mat dct_image;
                cv::Mat full_dct;
                cv::Mat odd_dct;
                cv::Mat even_odd_dct;

                cv::Mat mouth_bb;
                cv::Mat dct_image_bb;
                cv::Mat full_dct_bb;
                cv::Mat odd_dct_bb;
                cv::Mat even_odd_dct_bb;
                cv::Mat mouth_shape_bb;

                err = !video_capt.read(frame);
                if (!err) {
                  // Tracker
                  int err_track = face_tracker_->Track(frame, &shape);
                  if (!err_track && !shape.empty()) {
                    // Feature extraction
                    if ((dct_bool && !dct_on_bounding_box) || !dct_bool ||
                        all_feats) {
                      feature_extractor_->ExtractMouthImg(frame, shape, false,
                                                          &mouth, &mouth_shape,
                                                          &normalized_image,
                                                          &normalized_shape);
                      if (use_noises) {
                        std::string noise = "jpeg";
                        int intensity = 1;
                        LTS5::Jpeg(mouth, intensity, CV_IMWRITE_JPEG_QUALITY);
                      }
                      LTS5::ComputeDct(mouth, cv::Size(64, 64), &dct_image,
                                       &full_dct, &odd_dct, &even_odd_dct);
                      if ((dct_bool && !dct_on_bounding_box) || all_feats) {
                        mat_full.push_back(full_dct);
                        mat_odd.push_back(odd_dct);
                        mat_even_odd.push_back(even_odd_dct);
                      }
                      if (!dct_bool || all_feats) {
                        mat_mouth.push_back(mouth_shape);
                        cv::Mat norm_shape;
                        norm_shape = normalized_shape.t();
                        mat_shape.push_back(norm_shape);
                      }
                      if (save_images) {
                        std::string image_name = image_path + std::to_string(feature_extractor_->get_frame_counter()) + ".jpg";
                        cv::imwrite(image_name, mouth);
                        //image_name = image_path + "face_" + std::to_string(feature_extractor_->get_frame_counter()) + ".jpg";
                        //cv::imwrite(image_name, normalized_image);
                      }
                    }
                    if ((dct_bool && dct_on_bounding_box) || all_feats) {
                      err = feature_extractor_->ExtractMouthImg(frame, shape, true,
                                                          &mouth_bb,
                                                          &mouth_shape_bb,
                                                          &normalized_image,
                                                          &normalized_shape);
                      LTS5::ComputeDct(mouth_bb, cv::Size(64, 64), &dct_image_bb,
                                       &full_dct_bb, &odd_dct_bb,
                                       &even_odd_dct_bb);
                      mat_full_bb.push_back(full_dct_bb);
                      mat_odd_bb.push_back(odd_dct_bb);
                      mat_even_odd_bb.push_back(even_odd_dct_bb);
                      if (save_images) {
                        std::string image_name = image_path + "bb_" +
                            std::to_string(feature_extractor_->get_frame_counter()) + ".jpg";
                        cv::imwrite(image_name, mouth_bb);
                      }
                    }
                    if (!err) {
                      cv::Mat subframe = frame(cv::Rect(5, 5, 100, 75));
                      if (show_videos) {
                        display_video(normalized_image, normalized_shape);
                      }
                    } else {
                      log_file << "Error extracting features for frame no. "
                               << feature_extractor_->get_frame_counter()
                               << std::endl;
                    }
                  } else {
                    log_file << "Error tracking in frame no. "
                             << feature_extractor_->get_frame_counter() + 1
                             << std::endl;
                  }
                } else {
                  log_file << "End of video run 2 at frame "
                           << feature_extractor_->get_frame_counter()
                           << std::endl;
                  break;
                }
              }
              // Clean up
              delete face_tracker_;
              face_tracker_ = nullptr;
            } else {
              log_file << "Error creating face tracker" << std::endl;
            }
          } else {
            log_file << "Error opening video" << std::endl;
          }
          log_file << "Number of frames in run 2 "
                   << feature_extractor_->get_frame_count_2() << std::endl;
        }

        // Clean up
        delete feature_extractor_;
        feature_extractor_ = nullptr;


        // WRITE TO FILE
        LTS5::WriteMatToHtkBin(feat_file_full, mat_full);
        LTS5::WriteMatToHtkBin(feat_file_odd, mat_odd);
        LTS5::WriteMatToHtkBin(feat_file_even_odd, mat_even_odd);
        LTS5::WriteMatToHtkBin(feat_file_full_bb, mat_full_bb);
        LTS5::WriteMatToHtkBin(feat_file_odd_bb, mat_odd_bb);
        LTS5::WriteMatToHtkBin(feat_file_even_odd_bb, mat_even_odd_bb);
        LTS5::WriteMatToHtkBin(feat_file_mouth, mat_mouth);
        LTS5::WriteMatToHtkBin(feat_file_shape, mat_shape);

        feat_file_full.close();
        feat_file_odd.close();
        feat_file_even_odd.close();
        feat_file_full_bb.close();
        feat_file_odd_bb.close();
        feat_file_even_odd_bb.close();
        feat_file_mouth.close();
        feat_file_shape.close();
        log_file.close();
      }
      if (show_videos) {
        close_video_display();
      }
    }
  }
}