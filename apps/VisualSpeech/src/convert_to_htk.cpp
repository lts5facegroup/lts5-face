/**
 *  @file   feature_extraction.cpp
 *  @brief  Speech feature extraction
 *
 *  @author Marina Zimmermann
 *  @date   26/08/2016
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */

#include <iostream>
#include <iterator>

#include "opencv2/opencv.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"


int main(int argc, const char * argv[]) {

  typedef LTS5::CmdLineParser::ArgState State;
  LTS5::CmdLineParser parser;
  parser.AddArgument("-c",
                     State::kNeeded,
                     "Processor configuration file (.xml)");
  // Process inputs
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Open config file
    std::string config_path;
    parser.HasArgument("-c", &config_path);
    cv::FileStorage storage(config_path, cv::FileStorage::READ);
    if (storage.isOpened()) {
      // Path var
      std::string feature_file_path;
      std::string file_listing_path;
      std::string feature_storage_path;
      // Go root
      cv::FileNode root = storage.getFirstTopLevelNode();
      LTS5::ReadNode(root, "feature_file_path", &feature_file_path);
      LTS5::ReadNode(root, "file_listing_path", &file_listing_path);
      LTS5::ReadNode(root, "feature_storage_path", &feature_storage_path);

      std::ifstream input_feat_stream;
      std::ifstream input_file_stream;

      input_feat_stream.open(feature_file_path);
      input_file_stream.open(file_listing_path);

      std::vector<std::string> file_list;

      copy(std::istream_iterator<std::string>(input_file_stream),
           std::istream_iterator<std::string>(),
           back_inserter(file_list));

      int mat_rows = file_list.size();
      int mat_cols = 28;
      int mat_type = CV_32FC1;

      cv::Mat matrix;
      int inner_row = 0;
      int row = 0;

      matrix.create(mat_rows, mat_cols, mat_type);
      float value = 0.0;
      //Fill
      for (row = 0; row < file_list.size(); ++row) {
        if (row > 0 && file_list[row] != file_list[row - 1]) {
          std::string feature_filename = feature_storage_path + "/" +
              file_list[row - 1] + ".hf";
          std::ofstream feat_file;
          feat_file.open(feature_filename);
          LTS5::WriteMatToHtkBin(feat_file, matrix(
              cv::Rect(0, inner_row, mat_cols, row - inner_row)));
          inner_row = row;
        }

        for (int col = 0; col < mat_cols; ++col) {
          if (!(input_feat_stream >> value)) {
            break;
          }
          matrix.at<float>(row, col) = log(value);
        }
      }
      // last one
      std::string feature_filename = feature_storage_path + "/" +
                                     file_list[row - 1] + ".hf";
      std::ofstream feat_file;
      feat_file.open(feature_filename);
      LTS5::WriteMatToHtkBin(feat_file, matrix(
          cv::Rect(0, inner_row, mat_cols, row - inner_row)));
    } else {
      std::cout << "Cannot read command line arguments" << std::endl;
    }
  }
}