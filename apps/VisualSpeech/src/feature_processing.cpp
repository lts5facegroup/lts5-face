/**
 *  @file   feature_processing.cpp
 *  @brief  Speech feature processing
 *
 *  @author Marina Zimmermann
 *  @date   25/08/2016
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */

#include <iostream>
#include <iterator>

#include "opencv2/opencv.hpp"

#include "lts5/utils/cmd_parser.hpp"
#include "lts5/utils/file_io.hpp"

int main(int argc, const char * argv[]) {

  typedef LTS5::CmdLineParser::ArgState State;
  LTS5::CmdLineParser parser;
  parser.AddArgument("-c",
                     State::kNeeded,
                     "Processor configuration file (.xml)");
  // Process inputs
  int err = parser.ParseCmdLine(argc, argv);
  if (!err) {
    // Open config file
    std::string config_path;
    parser.HasArgument("-c", &config_path);
    cv::FileStorage storage(config_path, cv::FileStorage::READ);
    if (storage.isOpened()) {
      // Path var
      std::string feature_path;
      std::string train_files_list;
      std::string test_files_list;
      std::string train_feature_data;
      int feature_length;
      std::string log_path;
      std::string feature_storage_path;
      int apply_pca;
      int apply_lda;
      int global_normalisation;
      // Go root
      cv::FileNode root = storage.getFirstTopLevelNode();
      LTS5::ReadNode(root, "feature_path", &feature_path);
      LTS5::ReadNode(root, "train_file_list", &train_files_list);
      LTS5::ReadNode(root, "test_file_list", &test_files_list);
      LTS5::ReadNode(root, "train_feature_data", &train_feature_data);
      LTS5::ReadNode(root, "feature_length", &feature_length);
      LTS5::ReadNode(root, "log_path", &log_path);
      LTS5::ReadNode(root, "feature_storage_path", &feature_storage_path);
      LTS5::ReadNode(root, "apply_pca", &apply_pca);
      LTS5::ReadNode(root, "apply_lda", &apply_lda);
      LTS5::ReadNode(root, "global_normalisation", &global_normalisation);

      std::vector<std::string> video_files;
      int k_total_videos;

      // Open video
      err = !feature_path.empty();
      if (!err) {
        std::cerr
            << "The video directory is not set... Please specify a video directory"
            << std::endl;
        return err;
      }
        // Check if it is a single video file
      else if (feature_path.find(".mp4") != std::string::npos ||
          feature_path.find(".avi") != std::string::npos) {
        video_files.push_back(feature_path);
        k_total_videos = 1;
      }
        // Go through video folder
      else {
        // parse the video folder!
        LTS5::GetFileListing(video_files, feature_path, "", ".hf");
        //video_files = LTS5::ScanVideosInFolder(video_path);
        std::sort(video_files.begin(), video_files.end());
        k_total_videos = static_cast<int>(video_files.size());
        std::cout << k_total_videos << " videos found in the folder"
                  << std::endl;
      }

      cv::Mat train_mean;
      cv::Mat train_stddev;
      cv::Mat global_train_mat;
      cv::Mat temp_mat;
      std::ifstream feat_file_in;
      std::ofstream feat_file_out;
      cv::Mat out_mat;
      cv::PCA pca;

      std::vector<std::string> train_files;
      int total_trainfiles;

      if (!train_files_list.empty()) {
        std::ifstream train_files_in;
        train_files_in.open(train_files_list);
        copy(std::istream_iterator<std::string>(train_files_in),
             std::istream_iterator<std::string>(),
             back_inserter(train_files));

        for (int i = 0; i < static_cast<int>(train_files.size()); i++) {
          train_files[i] = feature_path + train_files[i];
        }
        total_trainfiles = static_cast<int>(train_files.size());
      }
      else if (global_normalisation) {
        train_files = video_files;
        total_trainfiles = static_cast<int>(train_files.size());
      }



      if (!train_files_list.empty() || global_normalisation) {
        for (int k = 0; k < total_trainfiles; k++) {
          //std::string feature_filename = feature_path + video_files[k];
          feat_file_in.open(train_files[k]);
          LTS5::ReadMatFromHtkBin(feat_file_in, &temp_mat);
          feat_file_in.close();

          global_train_mat.push_back(temp_mat);

          /*if (global_normalisation) {
            global_train_mat.push_back(temp_mat);
          }
          else {
            for (int i = 0; i < static_cast<int>(train_files.size()); i++) {
              if (video_files[k].find(train_files[i])) {
                global_train_mat.push_back(temp_mat);
                break;
              }
            }
          }*/
        }
        cv::meanStdDev(global_train_mat, train_mean, train_stddev);
        if (apply_pca) {
          cv::subtract(global_train_mat, train_mean, global_train_mat);
          //cv::divide(global_train_mat, train_stddev, global_train_mat);
          pca(global_train_mat, cv::noArray(), CV_PCA_DATA_AS_ROW, 0.10); // TODO careful!!!
        }
        else if (apply_lda) {
        }
      }

      for (int k = 0; k < k_total_videos; k++) {
        size_t name_start = video_files[k].find_last_of("\\/") + 1;
        size_t name_len = video_files[k].find_last_of(".") - name_start;
        std::string filename = video_files[k].substr(name_start, name_len);
        std::string feature_filename = feature_path + filename + ".hf";
        feat_file_in.open(feature_filename);
        LTS5::ReadMatFromHtkBin(feat_file_in, &temp_mat);
        feat_file_in.close();
        if (!train_files_list.empty() || !test_files_list.empty() || global_normalisation) {
          if ((train_mean.empty() || train_stddev.empty()) && !train_feature_data.empty()) {
            // TODO Read data
          }
          // Normalise
          //cv::subtract(temp_mat, train_mean, temp_mat);
          //cv::divide(temp_mat, train_stddev, temp_mat);
        }
        else {
          // compute mean and variance on a single file
          cv::meanStdDev(temp_mat, train_mean, train_stddev);
        }
        // Normalise
        cv::subtract(temp_mat, train_mean, temp_mat);
        //cv::divide(temp_mat, train_stddev, temp_mat);
        if (apply_pca) {
          pca.project(temp_mat);
        }
        else if (apply_lda) {
        }
        else {
          // reduce matrix size
          out_mat = temp_mat(cv::Rect(0, 0, feature_length, temp_mat.rows));
        }
        // Write processed features to file
        feature_filename = feature_storage_path + filename + ".hf";
        feat_file_out.open(feature_filename);
        LTS5::WriteMatToHtkBin(feat_file_out, temp_mat);
        feat_file_out.close();
      }
    }
  }
}