/**
 *  @file   FT_errors.cpp
 *  @brief  Face tracker errors
 *
 *  @author Valentine Santarelli
 *  @date   28/07/2016
 *  Copyright (c) 2016 Valentine Santarelli. All rights reserved.
 */


#include <algorithm>

#include "opencv2/imgproc.hpp"

#include "FT_errors.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/utils/shape_transforms.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/**
 *  @name       PruneFaceDetections
 *  @brief      Amongst (potentially) several detections in an image, returns
 *              the one which center is closest to the annotation center
 *  @param[in]  annotation      Annotation
 *  @param[in]  detections      Vector of Rect returned by the face detector
 *  @return     Detection which center is closest to the annotation center or an
 *              empty Rect
 * @author Gabriel Cuendet
*/
cv::Rect PruneFaceDetections(const cv::Mat& annotation,
                             const std::vector<cv::Rect>&
                             detections) {
  cv::Rect return_rect;
  std::vector<cv::Rect> candidates_rect;// Rects that have shape center inside
  for (int j = 0; j < detections.size(); j++) {
    cv::Point2d center = LTS5::GetCenterPoint(annotation);
    if (center.x > detections[j].x &&
        center.x < detections[j].x + detections[j].width &&
        center.y > detections[j].y &&
        center.y < detections[j].y + detections[j].height) {
      candidates_rect.push_back(detections[j]);
    }
  }

  if (candidates_rect.size() == 1) {
    return_rect = candidates_rect[0];
  }
  else if (candidates_rect.size() > 1) {
    int minIdx = 0;
    double minDist = DBL_MAX;
    for (int j = 0; j < candidates_rect.size(); j++) {
      cv::Point2d rectangle_center(candidates_rect[j].x +
                                   candidates_rect[j].width / 2.0,
                                   candidates_rect[j].y +
                                   candidates_rect[j].height / 2.0);
      cv::Point2d pt1 = LTS5::GetCenterPoint(annotation);

      double dist = sqrt((rectangle_center.x - pt1.x) * (rectangle_center.x -
                                                         pt1.x) + (rectangle_center.y - pt1.y) * (rectangle_center.y - pt1.y));

      if (dist < minDist) {
        minIdx = j;
        minDist = dist;
      }
    }
    return_rect = candidates_rect[minIdx];
  }
  return return_rect;
};

/*
 *  @name   Iod
 *  @brief  Returns the inter-occular distance, for normalization
 *  @return Inter occular distance
 */
double Iod(const cv::Mat& shape) {
  const int nPoints = shape.rows / 2;
  std::vector<int> indices(4);
  switch (nPoints) {
    case 11: // Self selected 11 points for all datasets
      // use c++11 std::initializer_list<T>
      indices.assign({0, 1, 2, 3});
      break;
    case 16: // Idiap
      indices.assign({2, 3, 4, 5});
      break;
    case 20: // BioID
      indices.assign({9, 10, 11, 12});
      break;
    case 29: // LFPW29
      indices.assign({8, 10, 11, 9});
      break;
    case 49: // CMU68_inner_shape
      indices.assign({19, 22, 25, 28});
      break;
    case 58: // CVHCI
      indices.assign({21, 25, 17, 13});
      break;
    case 66: // CMU66
      indices.assign({36, 39, 42, 45});
      break;
    case 68: // CMU68
      indices.assign({36, 39, 42, 45});
      break;
    case 76: // MUCT
      indices.assign({27, 29, 34, 32});
      break;
    default:
      std::stringstream ss;
      ss << "IOD not defined for " << nPoints << " points annotation";
      throw std::runtime_error(ss.str());
  }
  double dx1, dx2, dy1, dy2;
  dx1 = shape.at<double>(indices[2], 0) - shape.at<double>(indices[0], 0);
  dx2 = shape.at<double>(indices[3], 0) - shape.at<double>(indices[1], 0);
  dy1 = shape.at<double>(indices[2] + nPoints, 0) - shape.at<double>(indices[0] + nPoints, 0);
  dy2 = shape.at<double>(indices[3] + nPoints, 0) - shape.at<double>(indices[1] + nPoints, 0);
  return sqrt(dx1 * dx1 + dy1 * dy1) / 2 + sqrt(dx2 * dx2 + dy2 * dy2) / 2;
};



/*
* @name GetErrors
* @author Valentine Santarelli
* @brief Compute the errors the face tracker makes on an image
* @date July 2016
* @param[in] face_landmarks, the landmarks the face tracker computes on the image
* @param[in] annotation, the annotations are the correct points identifying the face on the image
*/
void GetErrors(const cv::Mat& face_landmarks, const cv::Mat& annotation,std::vector<double>& err,double d){

  double error;

  int nPoints = annotation.rows / 2;
  std::vector<int> points;
  for (int j = 0; j < nPoints; j++) {
    points.push_back(j);
  }
  for (int j = 0; j < points.size(); j++) {
    double x1, x2, y1, y2;
    x1 = face_landmarks.at<double>(points[j], 0);
    y1 = face_landmarks.at<double>(points[j] + nPoints, 0);
    x2 = annotation.at<double>(points[j], 0);
    y2 = annotation.at<double>(points[j] + nPoints, 0);
    error = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
    err.push_back(sqrt(abs(error))/d);
  }

}


/**
 *  @name   Rmse
 *  @brief  Returns the root mean square error between landmarks and annotation
 *  @return RMS Error
 *  @author Gabriel Cuendet
 */
double Rmse(const cv::Mat& face_landmarks, const cv::Mat& annotation) {
  double error = 0.0;
  int nPoints = annotation.rows / 2;
  std::vector<int> points;
  /*if (nPoints == 29) { // Evaluation is only on 17 points for LFPW29
    points.assign({0, 1, 2, 3, 8, 9, 10, 11, 16, 17, 18, 19, 20, 22, 23, 24, 27});
  }
  else {
    for (int j = 0; j < nPoints; j++) {
      points.push_back(j);
    }
  } */
  for (int j = 0; j < nPoints; j++) {
    points.push_back(j);
  }
  for (int j = 0; j < points.size(); j++) {
    double x1, x2, y1, y2;
    x1 = face_landmarks.at<double>(points[j], 0);
    y1 = face_landmarks.at<double>(points[j] + nPoints, 0);
    x2 = annotation.at<double>(points[j], 0);
    y2 = annotation.at<double>(points[j] + nPoints, 0);
    error += (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
  }
  error /= points.size();
  // Interocular distance
  double d = Iod(annotation);
  // Normalized RMSE
  error /= (d * d);
  return sqrt(error);
};



/*
* @name GetAnnotations
* @author Valentine Santarelli
* @brief recover the annotations of each image of the data set
* @date July 2016
* @param[in] input_dir, path to the directory of the data set of images
* @param[in] image_name,path to the image
* @param[in] annotation, the annotations are the correct points identifying the face on the image
*
*/
void GetAnnotations(const std::string& input_dir,  const std::string& image_name, cv::Mat& annotation) {

  std::string img_filename = image_name;
  size_t pos = img_filename.rfind('.');

  if (pos == std::string::npos) {
    annotation.data = NULL;
    std::cout << "No annotations found " << std::endl;
  }
  else {

  std::string pts_filename = img_filename.substr(0, pos) + ".pts";

  std::cout << "Loading annotations : " << input_dir + "/" + pts_filename <<
  std::endl;

  LTS5::LoadPts(input_dir + "/" + pts_filename, annotation);
}

  }


/*
* @name GetSpecificPts
* @author Valentine Santarelli
* @brief recover specific points specified by their coordinates in a larger vector of points,, ex: the landmarks of the mouth in the landmarks of the face
* @date July 2016
* @param[in] pts, vector of points
* @param[in] mouth_pts, vector of specific points
* @param[in] number, coordinates of the specific points in the larger vector of points
*
*/
void GetSpecificPts(cv::Mat& pts, cv::Mat& mouth_pts, std::vector<int>& number){

  for (int i=0; i<number.size(); i++){


      int j = number[i];

      mouth_pts.push_back( pts.at<double>(j - 1));

  }


  for (int i=0; i<number.size(); i++){

    int j = number[i];
    mouth_pts.push_back( pts.at<double>(j + pts.rows/2 - 1));

  }
}


/*
* @name GetLandmarks
* @author Valentine Santarelli
* @brief recover the alandmarks the face trackers computed
* @date July 2016
* @param[in] working_img,
* @param[in] img_filename, path to the image
* @param[in] landmark, all the landmarks the face tracker computes on the image
* @param[in] face_landmarks, only the face landmarks the face tracker computes on the image
* @param[in] annotation, the annotations are the correct points identifying the face on the image
* @param[in] face_detector_, face detector
* @param[in] face_tracker_, face tracker
* @param[in] detect_ , is true when a face is detected on the image
*/
void GetLandmarks(const cv::Mat& working_img, const std::string& img_filename, cv::Mat& landmark,
                  cv::Mat& face_landmarks,cv::Mat& annotation, cv::CascadeClassifier& face_detector_, LTS5::SdmTracker& face_tracker_,bool& detect){

  //cv::cvtColor(working_img, working_img, CV_BGR2GRAY);
  if ( working_img.empty()) { std::cout<< "Image empty" << std::endl;}

  std::vector<cv::Rect> face_detections;
  face_detector_.detectMultiScale(working_img, face_detections, 1.1, 3,
                                   /*CV_HAAR_SCALE_IMAGE*/ 0,
                                  cv::Size(30,30));
  cv::Rect face_rect;
  cv::Mat zero ;
  zero = cv::Mat::zeros(1, 1,CV_8UC1);

  if(!annotation.empty()) {
     face_rect = PruneFaceDetections(annotation, face_detections);
    }
  else{ face_rect = face_detections[0];}

  if (face_rect.width == 0){
    std::cerr << "Face detector failed on image " << img_filename << std::endl;
    detect=false;
  }
  else{detect=true;}


  if (face_tracker_.Detect(working_img, face_rect, &landmark) != 0){
    std::cerr << "Face tracker failed on image " << img_filename << std::endl;
  }


  /*if (landmark.rows > 98) {
    LTS5::ExtractInnerShape(landmark, &face_landmarks);
  }
  else { face_landmarks = landmark.clone();
  }*/
  face_landmarks = landmark.clone();

}


/*
 *  @name   SaveVect
 *  @brief save the vector  in the file which path is the string filename
 *  @param[in]  filename, path to where the vector id saved
 *  @param[in]  data, vector to save
 *
 */
void SaveVect(const std::string& filename, std::vector<double> data) {
  std::ofstream os(filename.c_str());
  if (!os.good()) {
    return;
  }
  int nPoints = data.size();

  for (int i = 0; i < nPoints; i++) {
    os << data[i]<< " " << std::endl;

  }
  os.close();
}


/*
 *  @name   DrawPts
 *  @brief Draw points at the given coordinates on the image
 *  @param[in]  img_filename, path to the image to draw on
 *  @param[in]  pts, coordinates of the points to draw
 *  @param[in]  color, color of the points to draw,  cv::Scalar(B, G, R)  with B,G or R from 0 to 255
 *
 */
void DrawPts(const std::string& img_filename,cv::Mat& pts, cv::Scalar color){
  cv::Mat img = cv::imread(img_filename);
  for(int i=0; i<pts.rows/2; i++) {
    cv::Point Pt1(pts.at<double>(i), pts.at<double>(i + pts.rows / 2));
    cv::Point Pt2(pts.at<double>(i) + 2, pts.at<double>(i + pts.rows / 2) + 2);
    cv::rectangle(img, Pt1, Pt2, color, -1);
  }
  cv::imwrite(img_filename, img);


}

}  // namespace LTS5







