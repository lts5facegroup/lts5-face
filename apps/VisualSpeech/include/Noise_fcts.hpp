/**
 *  @file   Noise_fct.hpp
 *  @brief  Noise functions
 *
 *  @author Valentine Santarelli
 *  @date   28/07/2016
 *  Copyright (c) 2016 Valentine Santarelli. All rights reserved.
 */

#ifndef __VISUALSPEECH_NOISE_FCTS_HPP__
#define __VISUALSPEECH_NOISE_FCTS_HPP__

#include "opencv2/core/core.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

//---Functions to apply various noises on the video OR image input

/*
 *  @name   Awgn
 *  @brief Add Gaussian noise
 *  @param[in]  img , image to add  gaussian noise to
 *  @param[in]  intensity, intensity of the noise to add
 */
void Awng(cv::Mat& mouth, int intensity);

/*
 *  @name   Jpeg
 *  @brief Change the jpeg quality
 *  @param[in]  img , image on which the jpeg quality changes
 *  @param[in]  intensity, intensity of the jpeg quality drop
 *  @param[in]  CV_IMWRITE_JPEG_QUALITY, parameter when encoding a jpeg image , gives the quality of the jpeg image to encode, default value=95.
 */
void Jpeg(cv::Mat& mouth, int intensity,int  CV_IMWRITE_JPEG_QUALITY);

/*
 *  @name   Blur
 *  @brief Add blur to the image
 *  @param[in]  img , image to add  blur to
 *  @param[in]  intensity, intensity of the blur to add
 *  @param[in]  BORDER_DEFAULT,border mode used to extrapolate pixels outside of the image
 */
void Blur(cv::Mat& mouth, int intensity,int BORDER_DEFAULT); // type de BORDER_DEFAULT ??

/*
 *  @name   OvExp
 *  @brief Simulate an overexposure on the image
 *  @param[in]  img , image to add  overexposure to
 *  @param[in]  intensity, intensity of the overexposure
 */
void OvExp(cv::Mat& mouth, int intensity);

/*  @name   Oc
 *  @brief Simulate an occlusion of the mouth
 *  @param[in]  img , image to add  an occlusion to
 *  @param[in]  position, position of the occlusion on the mouth (cover half the mouth, the middle ,the left corner)
 *  @param[in]  annotations , annotations or landmarks of the face.
 */
void Oc(cv::Mat& mouth, int position,cv::Mat& annotation); // position = 0,1 or 2 Vérifier lesquelles correspondent /!\ /


/*  @name   AddNoise
 *  @brief Add the different noise at different intensities on all the images in the input directory
 *  @param[in]  input_dir, path to the directory where all the images to add noise to are stored
 *  @param[in]  output_dir, path to the directory  to store the noisy images
 *  @param[in]  face_detector_, reference to the face detector (used when the occlusion is added to the image
 *  @param[in]  face_tracker_, reference to the face tracker (used when the occlusion is added to the image)
 */
void AddNoise(const std::string&  input_dir, const std::string&  output_dir,cv::CascadeClassifier& face_detector_, LTS5::SdmTracker& face_tracker_);

/*
* @name ImgName
* @brief recover the image name from the path to the image
* @param[in] img_filename,path to the image
* @param[in] img_name, image name
*/
void ImgName(std::string& img_filename,std::string& img_name);

}  // namespace LTS5

#endif //__VISUALSPEECH_NOISE_FCTS_HPP__