/**
 *  @file   SpeechFeatures.hpp
 *  @brief  SpeechFeatures class
 *
 *  @author Marina Zimmermann
 *  @date   28/07/2016
 *  Copyright (c) 2016 Marina Zimmermann. All rights reserved.
 */

#ifndef __LTS5_SPEECHFEATURES_HPP__
#define __LTS5_SPEECHFEATURES_HPP__

#include <stdio.h>
#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "lts5/utils/library_export.hpp"
#include "lts5/utils/file_io.hpp"
#include "lts5/face_tracker/sdm_tracker.hpp"
#include "lts5/face_tracker/point_distribution_model.hpp"
#include "lts5/utils/dct.hpp"


/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {
/**
 *  @class  SpeechFeatures
 *  @brief  Extractor of visual speech features
 *  @author Marina Zimmermann
 *  @date   18/07/16
 */
class LTS5_EXPORTS SpeechFeatures {

 public :

#pragma mark -
#pragma mark Type definition
  /**
   *  @struct SpeechFeaturesParameters
   *  @brief  Structure encapsulating configuration for SpeechFeatures class
   */
  struct SpeechFeaturesParameters {
    /** Feature vector size */
    int feat_vector_size;
    /** Image normalization */
    LTS5::PointDistributionModel::PDMParameters pose_normalization_config;

    /**
     *  @name   ExpressionAnalysisParameters
     *  @fn ExpressionAnalysisParameters(void)
     *  @brief  Constructor
     */
    SpeechFeaturesParameters(void) {
      feat_vector_size = 64;
    }
  };


#pragma mark -
#pragma mark Initialization

 /**
  *  @name SpeechFeatures
  *  @fn SpeechFeatures(void)
  *  @brief  Constructor
  */
 SpeechFeatures(void);

 /**
  *  @name SpeechFeatures
  *  @fn SpeechFeatures(const std::string& filename)
  *  @brief  Load speech features from supported file :
  *            .hf
  *  @param[in]  filename  Path to the speech features file
  */
 SpeechFeatures(const std::string& filename);

 /**
   *  @name SpeechFeatures
   *  @fn SpeechFeatures(const SpeechFeatures<T>& other) = delete
   *  @brief  Copy constructor
   *  @param[in]  other speech features to copy from
   */
 //SpeechFeatures(const SpeechFeatures<T>& other) = delete;

 /**
   *  @name operator=
   *  @fn SpeechFeatures& operator=(const SpeechFeatures& rhs) = delete
   *  @brief  Assignment operator
   *  @param[in]  rhs   SpeechFeatures to assign from
   */
 //SpeechFeatures& operator=(const SpeechFeatures<T>& rhs) = delete;

 /**
   *  @name ~SpeechFeatures
   *  @fn ~SpeechFeatures(void)
   *  @brief  Destructor
   */
 ~SpeechFeatures(void);

 /**
   *  @name Load_PDM
   *  @fn int Load_PDM(const std::string& filename)
   *  @brief  Load point distribution model from binary file :
   *            .hf
   *  @param[in]  filename  Path to the binary file
   *  @return -1 if error, 0 otherwise
 */
 int Load_PDM(const std::string& filename);

  /**
  *  @name Load
  *  @fn int Load(const std::string& filename)
  *  @brief  Load speech features from supported file :
  *            .hf
  *  @param[in]  filename  Path to the speech features file
  *  @return -1 if error, 0 otherwise
*/
//  int Load(const std::string& filename);

 /**
   *  @name Save
   *  @fn int Save(const std::string& filename)
   *  @brief  Save speech features to supported file format:
   *            .hf
   *  @return -1 if error, 0 otherwise
   */
// int Save(const std::string& filename);

#pragma mark -
#pragma mark Extract ROI

/*
 *  @name   ExtractMouthImg
 *  @brief  Extract mouth image and separate mouth shape
 *  @param[in]  image   Source image
 *  @param[in]  shape   Facial landmarks
 *  @param[in]  bounding_box   Bool stating whether to work on BB
 *  @param[out] mouth   Extracted mouth image, normalised
 *  @param[out] mouth_shape   Extracted mouth shape, normalised
 */
 int ExtractMouthImg(const cv::Mat& image,
                      const cv::Mat& shape,
                      const bool bounding_box,
                      cv::Mat* mouth,
                      cv::Mat* mouth_shape,
                      cv::Mat* normalized_image,
                      cv::Mat* normalized_shape);

#pragma mark -
#pragma mark Accessors

/**
 *  @name   get_configuration
 *  @fn void set_configuration(const ExpressionAnalysisParameters& configuration)
 *  @brief  Provide access to the module configuration
 *  @param[in] configuration    Module configuration
 */
 void set_configuration(const SpeechFeaturesParameters& configuration) {
   configuration_ = configuration;
 }

 /**
  *  @name   get_configuration
  *  @fn const ExpressionAnalysisParameters& get_configuration(void) const
  *  @brief  Provide access to the module configuration
  *  @return Configuration
  */
  const SpeechFeaturesParameters& get_configuration(void) const {
    return configuration_;
  }

  /**
 *  @name get_frame_counter
 *  @fn const int get_frame_counter(void) const
 *  @brief Give current frame count.
 *  @return int frame_counter
 */
  const int get_frame_counter(void) const {
    return frame_counter_;
  }

  /**
*  @name get_frame_count_1
*  @fn const int get_frame_count_1(void) const
*  @brief Give total frame count from run 1.
*  @return int frame_count_1
*/
  const int get_frame_count_1(void) const {
    return frame_count_1_;
  }

  /**
*  @name get_frame_count_2
*  @fn const int get_frame_count_2(void) const
*  @brief Give total frame count from run 2.
*  @return int frame_count_2
*/
  const int get_frame_count_2(void) const {
    return frame_count_2_;
  }

  /**
*  @name set_run
*  @fn const int set_run(void) const
*  @brief Set run number.
*  @return void
*/
  void set_run(const int run_no) {
    run_ = run_no;
    frame_counter_ = 0;
    if (run_no == 2) {
      mouth_roi_width_ = cumulative_mouth_width_ / frame_count_1_;
      mouth_roi_height_ = cumulative_mouth_height_ / frame_count_1_;
      mouth_roi_width_ *= 5/3;

      if (mouth_roi_width_ > 200) {
        mouth_roi_width_ = 200;
      }
      mouth_roi_height_ = mouth_roi_width_ / 3 * 2;
    }
  }

#pragma mark -
#pragma mark Private

 private :

  /** Configuration */
  SpeechFeaturesParameters configuration_;
  /** Pose normalization */
  LTS5::PointDistributionModel* pose_normalization_;
  /** Working image */
  cv::Mat working_image_;
  /** Feature normalization */
  bool do_feature_normalization_;
  /** Normalization, mean */
  cv::Mat feature_normalization_mean_;
  /** Normalization, std */
  cv::Mat feature_normalization_std_;
  /** Array of mouth indices */
  int mouth_indices_[20] = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67 };
  /** Number of points in tracker */
  int n_inner_points_ = 68;
  /** Cumulative mouth width */
  int cumulative_mouth_width_;
  /** Cumulative mouth height */
  int cumulative_mouth_height_;
  /** Mouth width of ROI */
  int mouth_roi_width_;
  /** Mouth height of ROI */
  int mouth_roi_height_;
  /** Frame counter of current run */
  int frame_counter_;
  /** Frame count of run 1 */
  int frame_count_1_;
  /** Frame count of run 2 */
  int frame_count_2_;
  /** Run number of face tracker */
  int run_;


/*
 *  @name   ReleaseMemory
 *  @brief  Release classifer and stuff
 */
  void ReleaseMemory(void);

};


}  // namespace LTS5

#endif //__LTS5_SPEECHFEATURES_HPP__
