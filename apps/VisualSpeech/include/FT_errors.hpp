/**
 *  @file   FT_errors.hpp
 *  @brief  Face tracker errors
 *
 *  @author Valentine Santarelli
 *  @date   28/07/2016
 *  Copyright (c) 2016 Valentine Santarelli. All rights reserved.
 */

#ifndef __VISUALSPEECH_FT_ERRORS_HPP__
#define __VISUALSPEECH_FT_ERRORS_HPP__


#include <iostream>
#include <string>

#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/ml.hpp"

#include "lts5/face_tracker/sdm_tracker.hpp"

/**
 *  @namespace  LTS5
 *  @brief      LTS5 laboratory dev space
 */
namespace LTS5 {

/*
 *  @name       PruneFaceDetections
 *  @brief      Amongst (potentially) several detections in an image, returns
 *              the one which center is closest to the annotation center
 *  @param[in]  annotation      Annotation
 *  @param[in]  detections      Vector of Rect returned by the face detector
 *  @return     Detection which center is closest to the annotation center or an
 *              empty Rect
 * @author Gabriel Cuendet
*/
cv::Rect PruneFaceDetections(const cv::Mat& annotation,
                             const std::vector<cv::Rect>&
                             detections);

/**
 *  @name   Iod
 *  @brief  Returns the inter-occular distance, for normalization
 *  @return Inter occular distance
 *  @author Gabriel Cuendet
 */
double Iod(const cv::Mat& shape);

/*
* @name GetErrors
* @author Valentine Santarelli
* @brief Compute the errors the face tracker makes on an image
* @date July 2016
* @param[in] face_landmarks, the landmarks the face tracker computes on the image
* @param[in] annotation, the annotations are the correct points identifying the face on the image
*/
void GetErrors(const cv::Mat& face_landmarks, const cv::Mat& annotation,std::vector<double>& err, double d);

/**
 *  @name   Rmse
 *  @brief  Returns the root mean square error between landmarks and annotation
 *  @return RMS Error
 *  @author Gabriel CuendeZ
 */
double Rmse(const cv::Mat& face_landmarks, const cv::Mat& annotation);



/*
* @name GetAnnotations
* @author Valentine Santarelli
* @brief recover the annotations of each image of the data set
* @date July 2016
* @param[in] input_dir, path to the directory of the data set of images
* @param[in] image_name,path to the image
* @param[in] annotation, the annotations are the correct points identifying the face on the image
*
*/
void GetAnnotations(const std::string& input_dir, const std::string& image_name, cv::Mat& annotation);

/*
* @name GetSpecificPts
* @author Valentine Santarelli
* @brief recover specific points specified by their coordinates in a larger vector of points,, ex: the landmarks of the mouth in the landmarks of the face
* @date July 2016
* @param[in] pts, vector of points
* @param[in] mouth_pts, vector of specific points
* @param[in] number, coordinates of the specific points in the larger vector of points
*
*/
void GetSpecificPts(cv::Mat& pts, cv::Mat& mouth_pts, std::vector<int>& number);


/*
* @name GetLandmarks
* @author Valentine Santarelli
* @brief recover the landmarks the face trackers computed
* @date July 2016
* @param[in] working_img,
* @param[in] img_filename, path to the image
* @param[in] landmark, all the landmarks the face tracker computes on the image
* @param[in] face_landmarks, only the face landmarks the face tracker computes on the image
* @param[in] annotation, the annotations are the correct points identifying the face on the image
* @param[in] face_detector_, face detector
* @param[in] face_tracker_, face tracker
* @param[in] detect_ , is true when a face is detected on the image
*/
void GetLandmarks(const cv::Mat& working_img, const std::string& image_name, cv::Mat& landmarks,
                  cv::Mat& face_landmark,cv::Mat& annotation, cv::CascadeClassifier& face_detector_, LTS5::SdmTracker& face_tracker_, bool& detect);

/*
 *  @name   SaveVect
 *  @brief save the vector  in the file which path is the string filename
 *  @param[in]  filename, path to where the vector id saved
 *  @param[in]  data, vetor to save
 *
 */
void SaveVect(const std::string& filename, std::vector<double> data);


/*
 *  @name   DrawPts
 *  @brief Draw points at the given coordinates on the image
 *  @param[in]  img_filename, path to the image to draw on
 *  @param[in]  pts, coordinates of the points to draw
 *  @param[in]  color, color of the points to draw,  cv::Scalar(B, G, R)  with B,G or R from 0 to 255
 *
 */
void DrawPts(const std::string& img_filename,cv::Mat& pts,cv::Scalar color);

}  // namespace LTS5

#endif //__VISUALSPEECH_FT_ERRORS_HPP__