# LTS5 Face Group Library

C++ Library developped by LTS5 face group.

## Table of Content

* Getting Started
* Installing
  * *LTS5 Library Installation*
  * *Application Installation*
* [Documentation](https://lts5facegroup.bitbucket.io/LTS5Doc/index.html)
* Team Members

## Getting Started

These instructions will get you a copy of the **LTS5** library and applications up and running on your local machine for development and testing purposes. See Installation section for detailed notes on how to deploy the library and applications on a live system.

### Prerequisities

The project depends on several well known libraries you need to install before going further. Here is the exhaustive list :

* [CMake](https://github.com/Kitware/CMake)
* [Cuda (*optionnal*)](https://developer.nvidia.com/cuda-downloads)
* [Doxygen (*optionnal*)](https://github.com/doxygen/doxygen.git)
* [Eigen](https://github.com/OPM/eigen3)
* [GLEW](https://github.com/nigels-com/glew)
* [GLFW](https://github.com/glfw/glfw)
* [OpenBLAS](https://github.com/xianyi/OpenBLAS)
* [OpenCV](https://github.com/Itseez/opencv)

#### Submodules

Once the git has been cloned, submodules need to be initialized. To do so run the command:

```
$ git submodule update --init --recursive
```

#### CMake

The build script needs a version of CMake **3.8** or bigger. Install it directly from source, for more details check [here](https://cmake.org/install/). For linux, ensure `libssl-dev` and `libopenssl` are installed, otherwise you will miss some required options. 
Optionally you can install `libncurses5-dev` to build `ccmake`.

#### Cuda
Cuda framework is *optional* as no GPU code is needed. However if CUDA support is enabled the following dependencies will be needed:

- CUDA Framework
- CUDNN (*Version should be carefully selected to match the supported CUDA version*)

#### Doxygen

Doxygen is not mandartory. However if the user needs to build locally the documentation, *Doxygen* is required. Otherwise documentation can be found online [here](https://lts5facegroup.bitbucket.io/LTS5Doc/index.html).

#### Eigen

The Eigen's version needs to be **3.x**. Install it directly from source or using the command :

```
$ sudo apt-get install libeigen3-dev
```
or using Macport :

```
$ sudo port install eigen3
```

#### GLEW

Install it directly from source or using the command :

```
$ sudo apt-get install libglew-dev
```
On OS X, this is **not** required. The `OpenGL Framework` will be used instead.


#### GLFW

The library GLFW needs to be built from the source using the command :

```
$ cmake -DBUILD_SHARED_LIBS=ON ..
$ make
$ make install
```

For *Linux* user, the package `xorg-dev`is required and can be installed with the standard package manager.

#### OpenBLAS

To build OpenBLAS with lapack support you will need to install first the `gfortran` compiler. Once installed build single threaded OpenBLAS using :

```
$ make USE_THREAD=0 USE_OPENMP=0 -jX
$ make PREFIX=/path/to/your/installation install
```

It is also possible to build it with multi threading support as follow:

```
$ make USE_OPENMP=1
$ make PREFIX=/path/to/your/installation install
```

On OS X, this is **not** required. The `Accelerate Framework` will be used instead.

#### OpenCV
The library does support only the last version of OpenCV (3.0+). After you have cloned the sources, build OpenCV from source as listed below :

```
$ ccmake ..
$ make
$ make install
```

## Installing

Installation process take place into two steps listed below :

* Install **LTS5** library
* Install dedicated applications

### LTS5 Library installation

Once all the dependencies listed above have been installed, the **LTS5** library can be built and install using the following commands from the root folder.

```
$ mkdir build
$ cd build
$ ccmake ..
 [press 'c' to configure]
 [You might want to set CMAKE_BUILD_TYPE to "Debug"]
 [press 'c' again]
 [press 'g' to generate Makefiles and exit]
$ make
$ make install
```

### Application installation

Once LTS5 Library is installed, application can be build using the commands listed below :

```
$ cd <LTS5_root>/apps/XXX/
$ cd mkdir build
$ cd build
$ ccmake ..
 [press 'c' to configure]
 [You might want to set CMAKE_BUILD_TYPE to "Debug"]
 [press 'c' again]
 [press 'g' to generate Makefiles and exit]
$ make
$ make install
```

## Documentation

Basic online documentation can be found [here](https://lts5facegroup.bitbucket.io/LTS5Doc/index.html)

## Team Members

* H. Gao [hua.gao@epfl.ch](mailto:hua.gao@epfl.ch)
* G. Cuendet [gabriel.cuendet@epfl.ch](mailto:gabriel.cuendet@epfl.ch)
* M. Zimmerman [marina.zimmermann@epfl.ch](mailto:marina.zimmermann@epfl.ch)
* C. Ecabert [christophe.ecabert@epfl.ch](mailto:christophe.ecabert@epfl.ch)
