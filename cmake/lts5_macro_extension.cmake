
###############################################################################
#

# ------------------------------------------------------------------------------
# CUDA RELATED
# ------------------------------------------------------------------------------

# Shamelessly copied from :
# https://github.com/Kitware/CMake/blob/master/Modules/FindCUDA.cmake
macro(cuda_find_library_local_first_with_path_ext _var _names _doc _path_ext )
  if(CMAKE_SIZEOF_VOID_P EQUAL 8)
    # CUDA 3.2+ on Windows moved the library directories, so we need the new
    # and old paths.
    set(_cuda_64bit_lib_dir "${_path_ext}lib/x64" "${_path_ext}lib64" "${_path_ext}libx64" )
  endif()
  # CUDA 3.2+ on Windows moved the library directories, so we need to new
  # (lib/Win32) and the old path (lib).
  find_library(${_var}
    NAMES ${_names}
    PATHS "${CUDA_TOOLKIT_TARGET_DIR}"
    ENV CUDA_PATH
    ENV CUDA_LIB_PATH
    PATH_SUFFIXES ${_cuda_64bit_lib_dir} "${_path_ext}lib/Win32" "${_path_ext}lib" "${_path_ext}libWin32"
    DOC ${_doc}
    NO_DEFAULT_PATH
    )
  if (NOT CMAKE_CROSSCOMPILING)
    # Search default search paths, after we search our own set of paths.
    find_library(${_var}
      NAMES ${_names}
      PATHS "/usr/lib/nvidia-current"
      DOC ${_doc}
      )
  endif()
endmacro()

# Shamelessly copied from :
# https://github.com/Kitware/CMake/blob/master/Modules/FindCUDA.cmake
macro(cuda_find_library_local_first _var _names _doc)
  cuda_find_library_local_first_with_path_ext( "${_var}" "${_names}" "${_doc}" "" )
endmacro()

###############################################################################
# Find cudadevrt library
MACRO(CUDA_FIND_CUDADEVRT_LIBRARY)
	cuda_find_library_local_first(CUDA_cudadevrt_LIBRARY cudadevrt "\"cudadevrt\" library")
	mark_as_advanced(CUDA_cudadevrt_LIBRARY)
	IF(CUDA_cudadevrt_LIBRARY)
		SET(CUDA_CUDADEVRT_LIBRARIES ${CUDA_cudadevrt_LIBRARY})
	ENDIF(CUDA_cudadevrt_LIBRARY)
ENDMACRO()

###############################################################################
# Find cublas
MACRO(CUDA_FIND_CUBLAS_LIBRARY)
  cuda_find_library_local_first(CUDA_cublas_LIBRARY cublas "\"cublas\" library")
  mark_as_advanced(CUDA_cublas_LIBRARY)
  IF(CUDA_cublas_LIBRARY)
    MESSAGE(STATUS "cublas libs: ${CUDA_cublas_LIBRARY}")
    SET(CUDA_CUBLAS_LIBRARIES ${CUDA_cublas_LIBRARY})
  ENDIF(CUDA_cublas_LIBRARY)
ENDMACRO()

###############################################################################
# Find cuda nvtools library
MACRO(CUDA_FIND_CUDANVTOOLS_LIBRARY)
  cuda_find_library_local_first(CUDA_cudanvtools_LIBRARY nvToolsExt "\"cnvToolsExt\" library")
  mark_as_advanced(CUDA_cudanvtools_LIBRARY)
  IF(CUDA_cudanvtools_LIBRARY)
    MESSAGE(STATUS "nvtools libs: ${CUDA_cudanvtools_LIBRARY}")
    SET(CUDA_NVTOOLS_LIBRARIES ${CUDA_cudanvtools_LIBRARY})
  ENDIF(CUDA_cudanvtools_LIBRARY)
ENDMACRO()

###############################################################################
# Find cudnn libary
MACRO(CUDA_FIND_CUDNN_LIBRARY)
  cuda_find_library_local_first(CUDA_cudnn_LIBRARY cudnn "\"cudnn\" library")
  mark_as_advanced(CUDA_cudnn_LIBRARY)
  IF(CUDA_cudnn_LIBRARY)
    MESSAGE(STATUS "cudnn libs: ${CUDA_cudnn_LIBRARY}")
    SET(CUDA_CUDNN_LIBRARIES ${CUDA_cudnn_LIBRARY})
  ELSE(CUDA_cudnn_LIBRARY)
    MESSAGE(STATUS "Cudnn not found")
  ENDIF(CUDA_cudnn_LIBRARY)
ENDMACRO()

# ------------------------------------------------------------------------------
# Python
# ------------------------------------------------------------------------------

###############################################################################
# Initialize python wrapper, check if dependencies are met (numpy)
MACRO(INIT_PYTHON_WRAPPER)
  # Check if python is availab
  IF(PYTHON_FOUND)
    # Generate __init__.py for the main package (it will be empty)
    SET(PyInit ${LTS5_BINARY_DIR}/python/__init__.py)
    FILE(WRITE ${PyInit} "# Empty")
    INSTALL(FILES ${PyInit} DESTINATION ${PYTHON_SITE_PACKAGES_INSTALL_DIR}/lts5/)
  ELSE(PYTHON_FOUND)
    # No python available, disable wrpper option
    set(OPT_PYTHON_WRAPPER OFF CACHE BOOL "Build python wrapper" FORCE)
  ENDIF(PYTHON_FOUND)
ENDMACRO()

###############################################################################
# Tensorflow
# Search tensorflow package location using python interface as mention in the
# doc
# See: https://www.tensorflow.org/extend/adding_an_op
# _version Minimum version required
MACRO(FIND_PYTHON_TENSORFLOW _version)
  # Check if python is available
  SET(TENSORFLOW_FOUND FALSE)
  IF(PYTHON_FOUND)
    # We have a python interpreter, find tensorflow includes/libs
    EXECUTE_PROCESS(COMMAND ${PYTHON_EXECUTABLE} -c "import tensorflow as tf; print(tf.sysconfig.get_include())"
            OUTPUT_VARIABLE TF_INCLUDE_DIRS
            OUTPUT_STRIP_TRAILING_WHITESPACE
            ERROR_QUIET)
    EXECUTE_PROCESS(COMMAND ${PYTHON_EXECUTABLE} -c "import tensorflow as tf; print(tf.sysconfig.get_lib())"
            OUTPUT_VARIABLE TF_LIB_DIRS
            OUTPUT_STRIP_TRAILING_WHITESPACE
            ERROR_QUIET)
    EXECUTE_PROCESS(COMMAND ${PYTHON_EXECUTABLE} -c "import tensorflow as tf; print([f[3:] if f.startswith('-l:') else f[2:] for f in tf.sysconfig.get_link_flags() if '_framework' in f][0])"
            OUTPUT_VARIABLE TF_LIB_NAME
            OUTPUT_STRIP_TRAILING_WHITESPACE
            ERROR_QUIET)
    EXECUTE_PROCESS(COMMAND ${PYTHON_EXECUTABLE} -c "import tensorflow as tf; print(tf.__version__)"
            OUTPUT_VARIABLE TF_VERSION
            OUTPUT_STRIP_TRAILING_WHITESPACE
            ERROR_QUIET)
    IF(TF_INCLUDE_DIRS AND TF_LIB_DIRS)
      # Check version
      EXTRACT_VERSION(${TF_VERSION} TF)
      SET(TENSORFLOW_VERSION "${TF_MAJOR_VERSION}.${TF_MINOR_VERSION}.${TF_REVISION_VERSION}")
      IF(NOT TENSORFLOW_VERSION  VERSION_LESS ${_version})
        # Include folders founded
        SET(TENSORFLOW_INCLUDE_DIRS ${TF_INCLUDE_DIRS} ${TF_INCLUDE_DIRS}/external/nsync/public)
        # Search for library files using library folder
        UNSET(TENSORFLOW_LIBRARIES CACHE)
        FIND_LIBRARY(TENSORFLOW_LIBRARIES NAMES ${TF_LIB_NAME} PATHS ${TF_LIB_DIRS} NO_DEFAULT_PATH)
        # Check if lib as been founded
        IF(TENSORFLOW_LIBRARIES AND NOT TARGET tensorflow::python)
          ADD_LIBRARY(tensorflow::python SHARED IMPORTED GLOBAL)
          SET_TARGET_PROPERTIES(tensorflow::python PROPERTIES IMPORTED_LOCATION ${TENSORFLOW_LIBRARIES})
          SET_TARGET_PROPERTIES(tensorflow::python PROPERTIES INCLUDE_DIRECTORIES "${TENSORFLOW_INCLUDE_DIRS}")
          SET(TENSORFLOW_FOUND TRUE)
          MESSAGE(STATUS "Found Tensorflow include: ${TF_INCLUDE_DIRS}")
          MESSAGE(STATUS "Found Tensorflow libraries: ${TENSORFLOW_LIBRARIES}")
        ENDIF(TENSORFLOW_LIBRARIES AND NOT TARGET tensorflow::python)
      ELSE(NOT TENSORFLOW_VERSION  VERSION_LESS ${_version})
        MESSAGE("Found TF Version: ${TENSORFLOW_VERSION}, required ${_version}")
      ENDIF(NOT TENSORFLOW_VERSION  VERSION_LESS ${_version})
    ELSE(TF_INCLUDE_DIRS AND TF_LIB_DIRS)
      MESSAGE(STATUS "Can not find tensorflow package")
    ENDIF (TF_INCLUDE_DIRS AND TF_LIB_DIRS)
  ELSE(PYTHON_FOUND)
    # No python available
    SET(TENSFORFLOW_FOUND FALSE)
    MESSAGE(STATUS "Can not find tensorflow package, missing python interpreter")
  ENDIF(PYTHON_FOUND)
ENDMACRO()

###############################################################################
# Clear variables related to pyling
MACRO(PYLINT_CLEAR_VARIABLES)
  # Loop over all pylint target id and clear the corresponding variables
  FOREACH(_id ${PYLINT_TARGET_ID})
    CLEAR_VARIABLES(PYLINT_TARGET_${_ID}_FILE
                    PYLINT_TARGET_${_ID}_RCFILE
                    PYLINT_TARGET_${_ID}_OPTIONS)
  ENDFOREACH()
  # Clear also the list of id file to be processed
  CLEAR_VARIABLES(PYLINT_TARGET_ID)
ENDMACRO()

###############################################################################
# Collect files within a given `_folder` and pick the one matching the pattern.
# With matching file, initialise the corresponding pylint variables:
#     PYLINT_TARGET_${ID}_FILE
#     PYLINT_TARGET_${ID}_FILE
#     PYLINT_TARGET_${ID}_OPTIONS
# _folder         Root folder where to look for
# _glob_pattern   Pattern of the files of interest
# ARGN
# RCFILE    Path to the rcfile to use with pylint
# OPTIONS   Pylint options
MACRO(PYLINT_GATHER_FILE _folder _glob_pattern)
  # Parse arguments
  SET(options)
  SET(oneValueArgs)
  SET(multiValueArgs RCFILE OPTIONS)
  CMAKE_PARSE_ARGUMENTS(PYLINT "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
  # Collect python files
  FILE(GLOB_RECURSE PY_FILES
          ${_folder}/${_glob_pattern})
  FOREACH(_file ${PY_FILES})
    # Add new ID and cache it
    LIST(LENGTH PYLINT_TARGET_ID _id)
    LIST(APPEND PYLINT_TARGET_ID ${_id})
    SET(PYLINT_TARGET_ID "${PYLINT_TARGET_ID}" CACHE INTERNAL "Pylint list of target")
    # Create variables
    SET(PYLINT_TARGET_${_id}_FILE ${_file} CACHE INTERNAL "")
    SET(PYLINT_TARGET_${_id}_RCFILE ${PYLINT_RCFILE} CACHE INTERNAL "")
    SET(PYLINT_TARGET_${_id}_OPTIONS ${PYLINT_OPTIONS} CACHE INTERNAL "")
  ENDFOREACH()
ENDMACRO()

################################################################################
# Finalize pylint target
# See: https://github.com/opencv/opencv/blob/master/cmake/OpenCVPylint.cmake
# _template CMake script template for pylint target
# _output   Location where to place the generated script
MACRO(PYLINT_FINALIZE _template _output)

  # Create config header
  SET(PYLINT_CONFIG_SCRIPT "")
  SET(_SOURCES "")
  # List all id
  CMAKE_SCRIPT_APPEND_VAR(PYLINT_CONFIG_SCRIPT
          PYLINT_EXECUTABLE
          PYLINT_TARGET_ID)
  # Create individual variables for each id
  FOREACH(_id ${PYLINT_TARGET_ID})
    CMAKE_SCRIPT_APPEND_VAR(PYLINT_CONFIG_SCRIPT
            PYLINT_TARGET_${_id}_FILE
            PYLINT_TARGET_${_id}_RCFILE
            PYLINT_TARGET_${_id}_OPTIONS)
    LIST(APPEND _SOURCES ${PYLINT_TARGET_${_id}_FILE} ${PYLINT_TARGET_${_id}_RCFILE})
  ENDFOREACH()
  list(REMOVE_DUPLICATES _SOURCES)

  # Summary
  LIST(LENGTH PYLINT_TARGET_ID _total)
  SET(PYLINT_TOTAL_TARGETS "${_total}" CACHE INTERNAL "")
  MESSAGE(STATUS "Pylint: registered ${__total} targets. Build 'pylint' target to run checks")

  # Create script + target
  CONFIGURE_FILE("${_template}" "${_output}/lts5_pylint.cmake" @ONLY)
  ADD_CUSTOM_TARGET(pylint
          COMMAND ${CMAKE_COMMAND} -P "${_output}/lts5_pylint.cmake"
          COMMENT Check python API with Pylint
          DEPENDS ${_SOURCES}
          SOURCES ${_SOURCES})
ENDMACRO()
