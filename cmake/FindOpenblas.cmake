# - Finds Openblas and all dependencies
# Once done this will define
#  OPENBLAS_FOUND- System has Openblas
#  OPENBLAS_INCLUDE_DIR- The Openblas include directories
#  OPENBLAS_LIB- The libraries needed to use Openblas

MESSAGE(STATUS "Look for openblas")

FILE(TO_CMAKE_PATH "$ENV{OpenBLAS_HOME}" OpenBLAS_WIN_ROOT)
SET(Open_BLAS_INCLUDE_SEARCH_PATHS
  /usr/include
  /usr/include/openblas-base
  /usr/local/include
  /usr/local/include/openblas-base
  /opt/OpenBLAS/include
  ${OpenBLAS_WIN_ROOT}
  ${OpenBLAS_WIN_ROOT}/include
)

SET(Open_BLAS_LIB_SEARCH_PATHS
        /lib/
        /lib/openblas-base
        /lib64/
        /usr/lib
        /usr/lib/openblas-base
        /usr/lib64
        /usr/local/lib
        /usr/local/lib64
        /opt/OpenBLAS/lib
        $ENV{OpenBLAS}
        ${OpenBLAS_WIN_ROOT}/lib
        ${OpenBLAS_WIN_ROOT}
        ${OpenBLAS_WIN_ROOT}/lib
 )

IF (NOT OPENBLAS_FOUND)

FIND_PATH(OPENBLAS_INCLUDE_DIR NAMES cblas.h HINTS ${Open_BLAS_INCLUDE_SEARCH_PATHS})
FIND_LIBRARY(OPENBLAS_LIB NAMES libopenblas.a libopenblas.so openblas HINTS ${Open_BLAS_LIB_SEARCH_PATHS})

# OpenBLAS needs gfortran to be build, therefore try to findit
# Query gfortran to get the libgfortran.so path
FIND_PROGRAM(_GFORTRAN_EXECUTABLE NAMES gfortran x86_64-w64-mingw32-gfortran gfortran-mp-4.9 HINTS ${TOOLCHAIN_DIR}/bin)
IF(_GFORTRAN_EXECUTABLE)
  EXECUTE_PROCESS(COMMAND ${_GFORTRAN_EXECUTABLE} -print-file-name=libgfortran.a
        OUTPUT_VARIABLE _libgfortran_path
        OUTPUT_STRIP_TRAILING_WHITESPACE)
        GET_FILENAME_COMPONENT(GFORTRAN_PATH ${_libgfortran_path} PATH)
        FIND_LIBRARY(GFORTRAN_LIB gfortran PATHS ${GFORTRAN_PATH})
ENDIF()
unset(_GFORTRAN_EXECUTABLE CACHE)

IF(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
    find_file(OPENBLAS_LIB libopenblas.dll.a ${Open_BLAS_LIB_SEARCH_PATHS})
ENDIF(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
#MESSAGE("Lib file : ${OPENBLAS_LIB}")

SET(OPENBLAS_FOUND TRUE)
#    Check include files
IF(NOT OPENBLAS_INCLUDE_DIR)
    SET(OPENBLAS_FOUND FALSE)
    MESSAGE(STATUS "Could not find OPENBLAS include. Turning OpenBLAS_FOUND off")
ENDIF()
#    Check libraries
IF(NOT OPENBLAS_LIB AND NOT GFORTRAN_LIB)
    SET(OPENBLAS_FOUND FALSE)
    MESSAGE(STATUS "Could not find OpenBLAS lib. Turning OpenBLAS_FOUND off")
ENDIF()

IF (OPENBLAS_FOUND)
  IF (NOT OPENBLAS_FIND_QUIETLY)
    MESSAGE(STATUS "Found OpenBLAS libraries: ${OPENBLAS_LIB}")
    MESSAGE(STATUS "Found OpenBLAS include: ${OPENBLAS_INCLUDE_DIR}")
  ENDIF (NOT OPENBLAS_FIND_QUIETLY)
  # Add gfortran as well since needed by static libs
  SET(OPENBLAS_LIB ${OPENBLAS_LIB} ${GFORTRAN_LIB})
ELSE (OPENBLAS_FOUND)
  IF (OPENBLAS_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "Could not find OpenBLAS")
  ENDIF (OPENBLAS_FIND_REQUIRED)
ENDIF (OPENBLAS_FOUND)

MARK_AS_ADVANCED(
    OPENBLAS_INCLUDE_DIR
    OPENBLAS_LIB
    OPENBLAS_FOUND
)
ENDIF (NOT OPENBLAS_FOUND)