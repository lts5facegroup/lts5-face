# Set LTS5 default verbosity level from cmake

# User input default info
set (LTS5_VERBOSITY_LEVEL Info CACHE STRING "Set LTS5 verbosity level. Available options are: Always Error Warn Info Debug Verbose")

if(${LTS5_VERBOSITY_LEVEL} STREQUAL Info)
  set(VERBOSITY_LEVEL_INFO 1)
elseif(${LTS5_VERBOSITY_LEVEL} STREQUAL Always)
  set(VERBOSITY_LEVEL_ALWAYS 1)
elseif(${LTS5_VERBOSITY_LEVEL} STREQUAL Error)
  set(VERBOSITY_LEVEL_ERROR 1)
elseif(${LTS5_VERBOSITY_LEVEL} STREQUAL Warn)
  set(VERBOSITY_LEVEL_WARN 1)
elseif(${LTS5_VERBOSITY_LEVEL} STREQUAL Debug)
  set(VERBOSITY_LEVEL_DEBUG 1)
elseif(${LTS5_VERBOSITY_LEVEL} STREQUAL Verbose)
  set(VERBOSITY_LEVEL_VERBOSE 1)
else()
#  message(WARNING "Unknown verbosity level ${LTS5_VERBOSITY_LEVEL}. Set to Info!")
  set(VERBOSITY_LEVEL_INFO)
endif(${LTS5_VERBOSITY_LEVEL} STREQUAL Info)
