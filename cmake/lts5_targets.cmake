include(${PROJECT_SOURCE_DIR}/cmake/lts5_utils.cmake)

###############################################################################
# Add an option to build a subsystem or not.
# _var The name of the variable to store the option in.
# _name The name of the option's target subsystem.
# _desc The description of the subsystem.
# _default The default value (TRUE or FALSE)
# ARGV5 The reason for disabling if the default is FALSE.
macro(LTS5_SUBSYS_OPTION _var _name _desc _default)
  set(_opt_name "BUILD_${_name}")
  LTS5_GET_SUBSYS_HYPERSTATUS(subsys_status ${_name})
  if(NOT ("${subsys_status}" STREQUAL "AUTO_OFF"))
    option(${_opt_name} ${_desc} ${_default})
    if((NOT ${_default} AND NOT ${_opt_name}) OR ("${_default}" STREQUAL "AUTO_OFF"))
      set(${_var} FALSE)
      if(${ARGC} GREATER 4)
        set(_reason ${ARGV4})
      else(${ARGC} GREATER 4)
        set(_reason "Disabled by default.")
      endif(${ARGC} GREATER 4)
      LTS5_SET_SUBSYS_STATUS(${_name} FALSE ${_reason})
      LTS5_DISABLE_DEPENDIES(${_name})
    elseif(NOT ${_opt_name})
      set(${_var} FALSE)
      LTS5_SET_SUBSYS_STATUS(${_name} FALSE "Disabled manually.")
      LTS5_DISABLE_DEPENDIES(${_name})
    else(NOT ${_default} AND NOT ${_opt_name})
      set(${_var} TRUE)
      LTS5_SET_SUBSYS_STATUS(${_name} TRUE)
      LTS5_ENABLE_DEPENDIES(${_name})
    endif((NOT ${_default} AND NOT ${_opt_name}) OR ("${_default}" STREQUAL "AUTO_OFF"))
  endif(NOT ("${subsys_status}" STREQUAL "AUTO_OFF"))
  LTS5_ADD_SUBSYSTEM(${_name} ${_desc})
endmacro(LTS5_SUBSYS_OPTION)

###############################################################################
# Add an option to build a subsystem or not.
# _var The name of the variable to store the option in.
# _parent The name of the parent subsystem
# _name The name of the option's target subsubsystem.
# _desc The description of the subsubsystem.
# _default The default value (TRUE or FALSE)
# ARGV5 The reason for disabling if the default is FALSE.
macro(LTS5_SUBSUBSYS_OPTION _var _parent _name _desc _default)
  set(_opt_name "BUILD_${_parent}_${_name}")
  LTS5_GET_SUBSYS_HYPERSTATUS(parent_status ${_parent})
  if(NOT ("${parent_status}" STREQUAL "AUTO_OFF") AND NOT ("${parent_status}" STREQUAL "OFF"))
    LTS5_GET_SUBSYS_HYPERSTATUS(subsys_status ${_parent}_${_name})
    if(NOT ("${subsys_status}" STREQUAL "AUTO_OFF"))
      option(${_opt_name} ${_desc} ${_default})
      if((NOT ${_default} AND NOT ${_opt_name}) OR ("${_default}" STREQUAL "AUTO_OFF"))
        set(${_var} FALSE)
        if(${ARGC} GREATER 5)
          set(_reason ${ARGV5})
        else(${ARGC} GREATER 5)
          set(_reason "Disabled by default.")
        endif(${ARGC} GREATER 5)
        LTS5_SET_SUBSYS_STATUS(${_parent}_${_name} FALSE ${_reason})
        LTS5_DISABLE_DEPENDIES(${_parent}_${_name})
      elseif(NOT ${_opt_name})
        set(${_var} FALSE)
        LTS5_SET_SUBSYS_STATUS(${_parent}_${_name} FALSE "Disabled manually.")
        LTS5_DISABLE_DEPENDIES(${_parent}_${_name})
      else(NOT ${_default} AND NOT ${_opt_name})
        set(${_var} TRUE)
        LTS5_SET_SUBSYS_STATUS(${_parent}_${_name} TRUE)
        LTS5_ENABLE_DEPENDIES(${_parent}_${_name})
      endif((NOT ${_default} AND NOT ${_opt_name}) OR ("${_default}" STREQUAL "AUTO_OFF"))
    endif(NOT ("${subsys_status}" STREQUAL "AUTO_OFF"))
  endif(NOT ("${parent_status}" STREQUAL "AUTO_OFF") AND NOT ("${parent_status}" STREQUAL "OFF"))
  LTS5_ADD_SUBSUBSYSTEM(${_parent} ${_name} ${_desc})
endmacro(LTS5_SUBSUBSYS_OPTION)

###############################################################################
# Make one subsystem depend on one or more other subsystems, and disable it if
# they are not being built.
# _var The cumulative build variable. This will be set to FALSE if the
#   dependencies are not met.
# _name The name of the subsystem.
# _deps_inc List of include folder from the target dependencies
# ARGN The subsystems and external libraries to depend on.
macro(LTS5_SUBSYS_DEPEND _var _name _deps_inc)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs DEPS EXT_DEPS OPT_DEPS)
  cmake_parse_arguments(SUBSYS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  if(SUBSYS_DEPS)
    SET_IN_GLOBAL_MAP(LTS5_SUBSYS_DEPS ${_name} "${SUBSYS_DEPS}")
  endif(SUBSYS_DEPS)
  if(SUBSYS_EXT_DEPS)
    SET_IN_GLOBAL_MAP(LTS5_SUBSYS_EXT_DEPS ${_name} "${SUBSYS_EXT_DEPS}")
  endif(SUBSYS_EXT_DEPS)
  if(SUBSYS_OPT_DEPS)
    SET_IN_GLOBAL_MAP(LTS5_SUBSYS_OPT_DEPS ${_name} "${SUBSYS_OPT_DEPS}")
  endif(SUBSYS_OPT_DEPS)
  GET_IN_MAP(subsys_status LTS5_SUBSYS_HYPERSTATUS ${_name})
  if(${_var} AND (NOT ("${subsys_status}" STREQUAL "AUTO_OFF")))
    if(SUBSYS_DEPS)
      foreach(_dep ${SUBSYS_DEPS})
        LTS5_GET_SUBSYS_STATUS(_status ${_dep})
        if(NOT _status)
          set(${_var} FALSE)
          LTS5_SET_SUBSYS_STATUS(${_name} FALSE "Requires ${_dep}.")
        else(NOT _status)
          LTS5_GET_SUBSYS_INCLUDE_DIR(_include_dir ${_dep})
          STRING(REGEX MATCH "(.?cuda_)" match ${_include_dir})
          IF(match)
            STRING(REGEX REPLACE "^([^_]*)\\_(.*)" "\\1/\\2" _cuda_include_dir ${_include_dir})
            LIST(APPEND ${_deps_inc} ${PROJECT_SOURCE_DIR}/modules/${_cuda_include_dir}/include)
          ELSE(match)
            LIST(APPEND ${_deps_inc} ${PROJECT_SOURCE_DIR}/modules/${_include_dir}/include)
          ENDIF(match)
        endif(NOT _status)
      endforeach(_dep)
    endif(SUBSYS_DEPS)
    if(SUBSYS_EXT_DEPS)
      foreach(_dep ${SUBSYS_EXT_DEPS})
        string(TOUPPER "${_dep}_found" EXT_DEP_FOUND)
        if(NOT ${EXT_DEP_FOUND} OR (NOT (${EXT_DEP_FOUND} STREQUAL "TRUE")))
          set(${_var} FALSE)
          LTS5_SET_SUBSYS_STATUS(${_name} FALSE "Requires external library ${_dep}.")
        endif(NOT ${EXT_DEP_FOUND} OR (NOT (${EXT_DEP_FOUND} STREQUAL "TRUE")))
      endforeach(_dep)
    endif(SUBSYS_EXT_DEPS)
  endif(${_var} AND (NOT ("${subsys_status}" STREQUAL "AUTO_OFF")))
endmacro(LTS5_SUBSYS_DEPEND)

###############################################################################
# Make one subsystem depend on one or more other subsystems, and disable it if
# they are not being built.
# _var The cumulative build variable. This will be set to FALSE if the
#   dependencies are not met.
# _parent The parent subsystem name.
# _name The name of the subsubsystem.
# ARGN The subsystems and external libraries to depend on.
# macro(LTS5_SUBSUBSYS_DEPEND _var _parent _name)
#   set(options)
#   set(parentArg)
#   set(nameArg)
#   set(multiValueArgs DEPS EXT_DEPS OPT_DEPS)
#   cmake_parse_arguments(SUBSYS "${options}" "${parentArg}" "${nameArg}" "${multiValueArgs}" ${ARGN} )
#   if(SUBSUBSYS_DEPS)
#     SET_IN_GLOBAL_MAP(LTS5_SUBSYS_DEPS ${_parent}_${_name} "${SUBSUBSYS_DEPS}")
#   endif(SUBSUBSYS_DEPS)
#   if(SUBSUBSYS_EXT_DEPS)
#     SET_IN_GLOBAL_MAP(LTS5_SUBSYS_EXT_DEPS ${_parent}_${_name} "${SUBSUBSYS_EXT_DEPS}")
#   endif(SUBSUBSYS_EXT_DEPS)
#   if(SUBSUBSYS_OPT_DEPS)
#     SET_IN_GLOBAL_MAP(LTS5_SUBSYS_OPT_DEPS ${_parent}_${_name} "${SUBSUBSYS_OPT_DEPS}")
#   endif(SUBSUBSYS_OPT_DEPS)
#   GET_IN_MAP(subsys_status LTS5_SUBSYS_HYPERSTATUS ${_parent}_${_name})
#   if(${_var} AND (NOT ("${subsys_status}" STREQUAL "AUTO_OFF")))
#     if(SUBSUBSYS_DEPS)
#       foreach(_dep ${SUBSUBSYS_DEPS})
#         LTS5_GET_SUBSYS_STATUS(_status ${_dep})
#         if(NOT _status)
#           set(${_var} FALSE)
#           LTS5_SET_SUBSYS_STATUS(${_parent}_${_name} FALSE "Requires ${_dep}.")
#         else(NOT _status)
#           LTS5_GET_SUBSYS_INCLUDE_DIR(_include_dir ${_dep})
#           include_directories(${PROJECT_SOURCE_DIR}/${_include_dir}/include)
#         endif(NOT _status)
#       endforeach(_dep)
#     endif(SUBSUBSYS_DEPS)
#     if(SUBSUBSYS_EXT_DEPS)
#       foreach(_dep ${SUBSUBSYS_EXT_DEPS})
#         string(TOUPPER "${_dep}_found" EXT_DEP_FOUND)
#         if(NOT ${EXT_DEP_FOUND} OR (NOT ("${EXT_DEP_FOUND}" STREQUAL "TRUE")))
#           set(${_var} FALSE)
#           LTS5_SET_SUBSYS_STATUS(${_parent}_${_name} FALSE "Requires external library ${_dep}.")
#         endif(NOT ${EXT_DEP_FOUND} OR (NOT ("${EXT_DEP_FOUND}" STREQUAL "TRUE")))
#       endforeach(_dep)
#     endif(SUBSUBSYS_EXT_DEPS)
#   endif(${_var} AND (NOT ("${subsys_status}" STREQUAL "AUTO_OFF")))
# endmacro(LTS5_SUBSUBSYS_DEPEND)

###############################################################################
# Add a set of include files to install.
# _component The part of LTS5 that the install files belong to.
# _subdir The sub-directory for these include files.
# ARGN The include files.
macro(LTS5_ADD_INCLUDES _component _subdir)
  install(FILES ${ARGN} DESTINATION ${INCLUDE_INSTALL_DIR}/${_subdir}
          COMPONENT lts5_${_component})
endmacro(LTS5_ADD_INCLUDES)

###############################################################################
# Add a set of include files directory to install.
# _component The part of LTS5 that the install files belong to.
# _subdir The sub-directory for these include files.
# ARGN The include files.
macro(LTS5_ADD_INCLUDE_DIRS _component _subdir)
  install(DIRECTORY ${ARGN} DESTINATION ${INCLUDE_INSTALL_DIR}/${_subdir}
          COMPONENT lts5_${_component})
endmacro(LTS5_ADD_INCLUDE_DIRS)


###############################################################################
# Add a library target.
# _name The library name.
# _component The part of LTS5 that this library belongs to.
#
# LTS5_ADD_LIBRARY(<name> <component> [SHARED | STATIC] ARGN)
#
# ARGN The source files for the library.
macro(LTS5_ADD_LIBRARY _name _component)
  SET(options SHARED STATIC)
  CMAKE_PARSE_ARGUMENTS(ARG "${options}" "" "" ${ARGN})

  if(ARG_SHARED AND ARG_STATIC)
    message(FATAL_ERROR "Can't be both SHARED and STATIC")
  elseif(ARG_STATIC)
    set(_type STATIC)
  else()
    set(_type SHARED)
  endif()

  ADD_LIBRARY(${_name} ${_type} ${ARG_UNPARSED_ARGUMENTS})
  SET_TARGET_PROPERTIES(${_name} PROPERTIES POSITION_INDEPENDENT_CODE ON)

  # Add gperftools if enalbed
  IF(OPT_ADD_TIME_PROFILING)
    TARGET_LINK_LIBRARIES(${_name} PRIVATE -Wl,--no-as-needed profiler -Wl,--as-needed)
  ENDIF(OPT_ADD_TIME_PROFILING)

  if((UNIX AND NOT ANDROID) OR MINGW)
    TARGET_LINK_LIBRARIES(${_name} PRIVATE m)
  endif()

  if (MINGW)
    TARGET_LINK_LIBRARIES(${_name} PRIVATE gomp)
  endif()

  if(MSVC90 OR MSVC10)
    TARGET_LINK_LIBRARIES(${_name} PRIVATE delayimp.lib)  # because delay load is enabled for openmp.dll
  endif()
  #
  # Only link if needed
  if(WIN32 AND MSVC)
    set_target_properties(${_name} PROPERTIES LINK_FLAGS_RELEASE /OPT:REF)
  elseif(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
      set_target_properties(${_name} PROPERTIES LINK_FLAGS -Wl)
    endif()
  elseif(__COMPILER_PATHSCALE)
    set_target_properties(${_name} PROPERTIES LINK_FLAGS -mp)
  elseif(CMAKE_COMPILER_IS_GNUCXX AND MINGW)
    set_target_properties(${_name} PROPERTIES LINK_FLAGS "-Wl,--allow-multiple-definition -Wl,--as-needed")
  else()
    set_target_properties(${_name} PROPERTIES LINK_FLAGS -Wl,--as-needed)
  endif()
  #
  set_target_properties(${_name}
          PROPERTIES
          VERSION ${LTS5_VERSION}
          SOVERSION ${LTS5_VERSION_MAJOR}.${LTS5_VERSION_MINOR}
          DEFINE_SYMBOL "LTS5API_EXPORTS")
  install(TARGETS ${_name}
          EXPORT lts5-export
          RUNTIME DESTINATION ${BIN_INSTALL_DIR} COMPONENT lts5_${_component}
          LIBRARY DESTINATION ${LIB_INSTALL_DIR} COMPONENT lts5_${_component}
          ARCHIVE DESTINATION ${LIB_INSTALL_DIR} COMPONENT lts5_${_component})
endmacro(LTS5_ADD_LIBRARY)

###############################################################################
# Add a cuda library target.
# _name The library name.
# _component The part of LTS5 that this library belongs to.
# ARGN The source files for the library.
macro(LTS5_ADD_CUDA_LIBRARY _name _component)
  SET(options SHARED STATIC)
  CMAKE_PARSE_ARGUMENTS(ARG "${options}" "" "" ${ARGN})

  if(ARG_SHARED AND ARG_STATIC)
    message(FATAL_ERROR "Can't be both SHARED and STATIC")
  elseif(ARG_STATIC)
    set(_type STATIC)
  else()
    set(_type SHARED)
  endif()

  ADD_LIBRARY(${_name} ${_type} ${ARG_UNPARSED_ARGUMENTS})
  # Add some option
  SET_TARGET_PROPERTIES(${_name} PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
  SET_TARGET_PROPERTIES(${_name} PROPERTIES POSITION_INDEPENDENT_CODE ON)
  # Set include path for the target
  TARGET_INCLUDE_DIRECTORIES(${_name} PRIVATE ${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES})
  IF(APPLE)
    # We need to add the path to the driver (libcuda.dylib) as an rpath,
    # so that the static cuda runtime can find it at runtime.
    SET_PROPERTY(TARGET ${_name}
                 PROPERTY
                 BUILD_RPATH ${CMAKE_CUDA_IMPLICIT_LINK_DIRECTORIES})
  ENDIF()
  # Only link if needed
  IF(WIN32 AND MSVC)
    SET_TARGET_PROPERTIES(${_name} PROPERTIES LINK_FLAGS_RELEASE /OPT:REF)
  ELSEIF(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
      SET_TARGET_PROPERTIES(${_name} PROPERTIES LINK_FLAGS -Wl)
    endif()
  ELSE()
    SET_TARGET_PROPERTIES(${_name} PROPERTIES LINK_FLAGS -Wl,--as-needed)
  ENDIF()
  SET_TARGET_PROPERTIES(${_name} PROPERTIES
          VERSION ${LTS5_VERSION}
          SOVERSION ${LTS5_VERSION_MAJOR}
          DEFINE_SYMBOL "LTS5API_EXPORTS")
  install(TARGETS ${_name}
          EXPORT lts5-export
          RUNTIME DESTINATION ${BIN_INSTALL_DIR} COMPONENT lts5_${_component}
          LIBRARY DESTINATION ${LIB_INSTALL_DIR} COMPONENT lts5_${_component}
          ARCHIVE DESTINATION ${LIB_INSTALL_DIR} COMPONENT lts5_${_component})
endmacro(LTS5_ADD_CUDA_LIBRARY)


###############################################################################
# Add custom tensorflow (CUDA) op to python distribution
# Create object file for the GPU implementation of some custom Ops.
# _name       The library name.
# _component  The part of LTS5 that this library belongs to.
# ARGN
# FILES     The source files for the library.
# LINK_WITH Libraries to link with
macro(LTS5_ADD_CUDA_TENSORFLOW_OP _name _component)
  # Get parameters
  set(options)
  set(oneValueArgs)
  set(multiValueArgs FILES LINK_WITH)
  cmake_parse_arguments(LTS5_ADD_CUDA_TENSORFLOW_OP "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
  # Add library
  SET(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} --expt-relaxed-constexpr")
#  ADD_LIBRARY(${_name} OBJECT ${LTS5_ADD_CUDA_TENSORFLOW_OP_FILES})
  ADD_LIBRARY(${_name} SHARED ${LTS5_ADD_CUDA_TENSORFLOW_OP_FILES})
  TARGET_INCLUDE_DIRECTORIES(${_name} PRIVATE ${CUDA_TOOLKIT_INCLUDE})
  SET_TARGET_PROPERTIES(${_name} PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
  SET_TARGET_PROPERTIES(${_name} PROPERTIES POSITION_INDEPENDENT_CODE ON)
  TARGET_COMPILE_FEATURES(${_name} PUBLIC cxx_std_11)
  # Install
  SET_TARGET_PROPERTIES(${_name} PROPERTIES
          VERSION ${LTS5_VERSION}
          SOVERSION ${LTS5_VERSION_MAJOR}
          DEFINE_SYMBOL "LTS5API_EXPORTS")
  install(TARGETS ${_name}
          EXPORT lts5-export
          RUNTIME DESTINATION ${BIN_INSTALL_DIR} COMPONENT lts5_${_component}
          LIBRARY DESTINATION ${LIB_INSTALL_DIR} COMPONENT lts5_${_component}
          ARCHIVE DESTINATION ${LIB_INSTALL_DIR} COMPONENT lts5_${_component})
endmacro(LTS5_ADD_CUDA_TENSORFLOW_OP)

###############################################################################
# Add an executable target.
# _name The executable name.
# _component The part of LTS5 that this library belongs to.
# ARGN the source files for the library.
macro(LTS5_ADD_EXECUTABLE _name _component)
  add_executable(${_name} ${ARGN})
  # Add gperftools if enalbed
  IF(OPT_ADD_TIME_PROFILING)
    TARGET_LINK_LIBRARIES(${_name} PRIVATE -Wl,--no-as-needed profiler -Wl,--as-needed)
  ENDIF(OPT_ADD_TIME_PROFILING)

  # must link explicitly against boost.
  if(UNIX AND NOT ANDROID)
    target_link_libraries(${_name} PRIVATE pthread m ${CLANG_LIBRARIES})
  endif()
  #
  # Only link if needed
  if(WIN32 AND MSVC)
    set_target_properties(${_name} PROPERTIES LINK_FLAGS_RELEASE /OPT:REF
            DEBUG_OUTPUT_NAME ${_name}${CMAKE_DEBUG_POSTFIX}
            RELEASE_OUTPUT_NAME ${_name}${CMAKE_RELEASE_POSTFIX})
  elseif(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
      set_target_properties(${_name} PROPERTIES LINK_FLAGS -Wl)
    endif()
  elseif(__COMPILER_PATHSCALE)
    set_target_properties(${_name} PROPERTIES LINK_FLAGS -mp)
  elseif(CMAKE_COMPILER_IS_GNUCXX AND MINGW)
    set_target_properties(${_name} PROPERTIES LINK_FLAGS "-Wl,--allow-multiple-definition -Wl,--as-needed")
  else()
    set_target_properties(${_name} PROPERTIES LINK_FLAGS -Wl,--as-needed)
  endif()
  set(LTS5_EXECUTABLES ${LTS5_EXECUTABLES} ${_name})
  install(TARGETS ${_name}
          RUNTIME DESTINATION ${BIN_INSTALL_DIR} COMPONENT lts5_${_component})
endmacro(LTS5_ADD_EXECUTABLE)

###############################################################################
# Add an executable target as a bundle when available and required
# _name The executable name.
# _component The part of LTS5 that this library belongs to.
# _bundle
# ARGN the source files for the library.
macro(LTS5_ADD_EXECUTABLE_OPT_BUNDLE _name _component)
  if(APPLE AND VTK_USE_COCOA)
    add_executable(${_name} MACOSX_BUNDLE ${ARGN})
  else(APPLE AND VTK_USE_COCOA)
    add_executable(${_name} ${ARGN})
  endif(APPLE AND VTK_USE_COCOA)

  # must link explicitly against boost.
  if(UNIX AND NOT ANDROID)
    target_link_libraries(${_name} PRIVATE pthread)
  endif()
  #
  # Only link if needed
  if(WIN32 AND MSVC)
    set_target_properties(${_name} PROPERTIES LINK_FLAGS_RELEASE /OPT:REF
            DEBUG_OUTPUT_NAME ${_name}${CMAKE_DEBUG_POSTFIX}
            RELEASE_OUTPUT_NAME ${_name}${CMAKE_RELEASE_POSTFIX})
  elseif(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
      set_target_properties(${_name} PROPERTIES LINK_FLAGS -Wl)
    endif()
  elseif(__COMPILER_PATHSCALE)
    set_target_properties(${_name} PROPERTIES LINK_FLAGS -mp)
  elseif(CMAKE_COMPILER_IS_GNUCXX AND MINGW)
    set_target_properties(${_name} PROPERTIES LINK_FLAGS "-Wl,--allow-multiple-definition -Wl,--as-needed")
  else()
    set_target_properties(${_name} PROPERTIES LINK_FLAGS -Wl,--as-needed)
  endif()
  #
  if(USE_PROJECT_FOLDERS)
    set_target_properties(${_name} PROPERTIES FOLDER "Tools and demos")
  endif(USE_PROJECT_FOLDERS)

  set(LTS5_EXECUTABLES ${LTS5_EXECUTABLES} ${_name})
  #    message(STATUS "COMMAND ${CMAKE_COMMAND} -E create_symlink \"${_name}.app/Contents/MacOS/${_name}\" \"${_name}\"")

  install(TARGETS ${_name} RUNTIME DESTINATION ${BIN_INSTALL_DIR} COMPONENT lts5_${_component})

endmacro(LTS5_ADD_EXECUTABLE_OPT_BUNDLE)


###############################################################################
# Add an executable target.
# _name The executable name.
# _component The part of LTS5 that this library belongs to.
# ARGN the source files for the library.
macro(LTS5_CUDA_ADD_EXECUTABLE _name _component)
  add_executable(${_name} ${ARGN})
  # must link explicitly against boost.
  #target_link_libraries(${_name} ${Boost_LIBRARIES})
  #
  # Only link if needed
  if(WIN32 AND MSVC)
    set_target_properties(${_name} PROPERTIES LINK_FLAGS_RELEASE /OPT:REF
            DEBUG_OUTPUT_NAME ${_name}${CMAKE_DEBUG_POSTFIX}
            RELEASE_OUTPUT_NAME ${_name}${CMAKE_RELEASE_POSTFIX})
  elseif(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
      set_target_properties(${_name} PROPERTIES LINK_FLAGS -Wl)
    endif()
  else()
    set_target_properties(${_name} PROPERTIES LINK_FLAGS -Wl,--as-needed)
  endif()
  #
  if(USE_PROJECT_FOLDERS)
    set_target_properties(${_name} PROPERTIES FOLDER "Tools and demos")
  endif(USE_PROJECT_FOLDERS)

  set(LTS5_EXECUTABLES ${LTS5_EXECUTABLES} ${_name})
  install(TARGETS ${_name} RUNTIME DESTINATION ${BIN_INSTALL_DIR}
          COMPONENT lts5_${_component})
endmacro(LTS5_CUDA_ADD_EXECUTABLE)

###############################################################################
# Add a test target.
# _name The test name.
# _exename The exe name.
# ARGN :
#    FILES the source files for the test
#    ARGUMENTS Arguments for test executable
#    WORKING_DIR Working directory for test executable
#    LINK_WITH link test executable with libraries
#    INC_FOLDER extra include folder
macro(LTS5_ADD_TEST _name _exename)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs FILES ARGUMENTS WORKING_DIR LINK_WITH INC_FOLDER)
  cmake_parse_arguments(LTS5_ADD_TEST "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  add_executable(${_exename} ${LTS5_ADD_TEST_FILES})
  # Add google test framework if not already build
  IF(NOT TARGET gtest)
    ADD_SUBDIRECTORY(${LTS5_SOURCE_DIR}/3rdparty/googletest ${LTS5_OUTPUT_3RDPARTY_LIB_DIR}/googletest EXCLUDE_FROM_ALL)
    MARK_AS_ADVANCED(BUILD_GMOCK BUILD_GTEST BUILD_SHARED_LIBS gmock_build_tests
            gtest_build_samples gtest_build_tests gtest_disable_pthreads
            gtest_force_shared_crt gtest_hide_internal_symbols)
  ENDIF(NOT TARGET gtest)
  # Add include location for testing framework
  TARGET_INCLUDE_DIRECTORIES(${_exename}
          PRIVATE
            ${LTS5_SOURCE_DIR}/3rdparty/googletest/googletest/include
            ${LTS5_SOURCE_DIR}/3rdparty/googletest/googlemock/include
            ${LTS5_ADD_TEST_INC_FOLDER})
  # Add gperftools if enalbed
  #IF(OPT_ADD_TIME_PROFILING)
  #    TARGET_LINK_LIBRARIES(${_name} -Wl,--no-as-needed profiler -Wl,--as-needed)
  #ENDIF(OPT_ADD_TIME_PROFILING)
  if(NOT WIN32)
    set_target_properties(${_exename} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${LTS5_BINARY_DIR}/${BIN_INSTALL_DIR})
  endif(NOT WIN32)
  target_link_libraries(${_exename} PRIVATE ${LTS5_ADD_TEST_LINK_WITH} gtest gmock ${CLANG_LIBRARIES})
  # Only link if needed
  if(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
      set_target_properties(${_exename} PROPERTIES LINK_FLAGS -Wl)
    endif()
    target_link_libraries(${_exename} PRIVATE pthread)
  elseif(UNIX AND NOT ANDROID)
    set_target_properties(${_exename} PROPERTIES LINK_FLAGS -Wl,--as-needed)
    # GTest >= 1.5 requires pthread and CMake's 2.8.4 FindGTest is broken
    target_link_libraries(${_exename} PRIVATE pthread)
  elseif(CMAKE_COMPILER_IS_GNUCXX AND MINGW)
    set_target_properties(${_exename} PROPERTIES LINK_FLAGS "-Wl,--allow-multiple-definition -Wl,--as-needed")
  elseif(WIN32)
    set_target_properties(${_exename} PROPERTIES LINK_FLAGS_RELEASE /OPT:REF)
  endif()
  #
  if(USE_PROJECT_FOLDERS)
    set_target_properties(${_exename} PROPERTIES FOLDER "Tests")
  endif(USE_PROJECT_FOLDERS)
  # Register test
  add_test(NAME ${_name} COMMAND ${_exename} ${LTS5_ADD_TEST_ARGUMENTS} WORKING_DIRECTORY ${LTS5_ADD_TEST_WORKING_DIR})
endmacro(LTS5_ADD_TEST)

###############################################################################
# Generate python unit test from templates *.py.in
# ARGN :
#    FOLDER The folder where to look for templates
#    OUTPUT Location where to output the generated unit test
macro(LTS5_GENERATE_PYTHON_TEST)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs FOLDER OUTPUT)
  cmake_parse_arguments(PYGEN "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  # Scan folders for templates + loop
  FILE(GLOB _py_templates "${PYGEN_FOLDER}/test*.py.in")
  FOREACH(_py_file ${_py_templates})
    # Extract filename
    GET_FILENAME_COMPONENT(_fname ${_py_file} NAME_WE)
    # Generate test
    CONFIGURE_FILE(${_py_file} ${PYGEN_OUTPUT}/${_fname}.py)
  ENDFOREACH()
endmacro(LTS5_GENERATE_PYTHON_TEST)

###############################################################################
# Add a test unit test for python.
# _name The test name.
# _exename The exe name.
# ARGN :
#    FILE the source files for the test
#    ARGUMENTS Arguments for test executable
#    WORKING_DIR Working directory for test executable
macro(LTS5_ADD_PYTHON_TEST _name)
  set(options)
  set(oneValueArgs FILE)
  set(multiValueArgs ARGUMENTS WORKING_DIR)
  cmake_parse_arguments(LTS5_ADD_PYTHON_TEST "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  # Register test
  ADD_TEST(NAME ${_name}
           COMMAND ${PYTHON_EXECUTABLE} ${LTS5_ADD_PYTHON_TEST_FILE} ${LTS5_ADD_PYTHON_TEST_ARGUMENTS}
           WORKING_DIRECTORY ${LTS5_ADD_PYTHON_TEST_WORKING_DIR})
endmacro(LTS5_ADD_PYTHON_TEST)

###############################################################################
# Add an example target.
# _name The example name.
# ARGN :
#    FILES the source files for the example
#    LINK_WITH link example executable with libraries
#    INC_FOLDER extra include folder
macro(LTS5_ADD_EXAMPLE _name)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs FILES LINK_WITH INC_FOLDER)
  cmake_parse_arguments(LTS5_ADD_EXAMPLE "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  add_executable(${_name} ${LTS5_ADD_EXAMPLE_FILES})
  target_link_libraries(${_name} PRIVATE ${LTS5_ADD_EXAMPLE_LINK_WITH} ${CLANG_LIBRARIES})
  target_include_directories(${_name} PRIVATE ${LTS5_ADD_EXAMPLE_INC_FOLDER})
  # Add gperftools if enalbed
  IF(OPT_ADD_TIME_PROFILING)
    TARGET_LINK_LIBRARIES(${_name} PRIVATE -Wl,--no-as-needed profiler -Wl,--as-needed)
  ENDIF(OPT_ADD_TIME_PROFILING)
  if(WIN32 AND MSVC)
    set_target_properties(${_name} PROPERTIES DEBUG_OUTPUT_NAME ${_name}${CMAKE_DEBUG_POSTFIX}
            RELEASE_OUTPUT_NAME ${_name}${CMAKE_RELEASE_POSTFIX})
  endif(WIN32 AND MSVC)
  if(USE_PROJECT_FOLDERS)
    set_target_properties(${_name} PROPERTIES FOLDER "Examples")
  endif(USE_PROJECT_FOLDERS)
endmacro(LTS5_ADD_EXAMPLE)

###############################################################################
# Add a python wrapper
# _name       The library name.
# _component  The part of LTS5 that this library belongs to.
# ARGN
# FILES       The source files for the library.
# INCS        Extra include folder
# LINK_WITH   Libraries to link with
# CPP_CLASSES List of classes to be exported, if empty just add them all in the
#             __init__ file otherwise add only listed one.
# PYFILES     Python files to install
# SKIP_PYINIT List of python filename to not include in the __init__ file
macro(LTS5_ADD_PYTHON_WRAPPER _name _component)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs FILES INCS LINK_WITH CPP_CLASSES PYFILES SKIP_PYINIT)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
  IF(ARG_FILES)
    # CPP Wrapper required
    # Add library
    IF (MINGW AND WIN32)
      add_definitions(-DMS_WIN64)
      add_definitions(-DPy_BUILD_CORE_BUILTIN=1)
      pybind11_add_module(py${_name} SHARED ${ARG_FILES} NO_EXTRAS) # Disable LT0
    ELSE (MINGW AND WIN32)
      pybind11_add_module(py${_name} SHARED ${ARG_FILES}) # Disable LT0
    ENDIF(MINGW AND WIN32)
    target_link_libraries(py${_name} PUBLIC ${ARG_LINK_WITH})
    target_include_directories(py${_name}
            PUBLIC
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/python/include>
            ${ARG_INCS}
            ${PYTHON_INCLUDE_DIRS}
            ${NUMPY_INCLUDE_DIRS})
    # The dynamic lib must have the exact same name as the python package
    set_target_properties(py${_name} PROPERTIES PREFIX "")
    if( WIN32 )
      set_target_properties(py${_name} PROPERTIES SUFFIX ".pyd")
    elseif( APPLE OR ${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
      # on mac osx, python cannot import libraries with .dylib extension
      set_target_properties(py${_name} PROPERTIES SUFFIX ".so")
    endif(WIN32)
    # Define output location
    set_target_properties(py${_name} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${LTS5_BINARY_DIR}/python)
    install(TARGETS py${_name} DESTINATION ${PYTHON_SITE_PACKAGES_INSTALL_DIR}/lts5/${_component})
  ENDIF(ARG_FILES)

  # Create python init file
  SET(PyInit "${LTS5_BINARY_DIR}/python/${_component}/__init__.py")
  install(FILES ${PyInit} DESTINATION ${PYTHON_SITE_PACKAGES_INSTALL_DIR}/lts5/${_component})

  IF(NOT ARG_CPP_CLASSES AND ARG_FILES)
    # Add them all
    FILE(WRITE  ${PyInit} "from .py${_name} import *\n")
  ELSEIF(ARG_CPP_CLASSES AND ARG_FILES)
    # Add only the selected one
    FILE(WRITE  ${PyInit} "")
    FOREACH(_cls ${ARG_CPP_CLASSES})
      FILE(APPEND  ${PyInit} "from .py${_name} import ${_cls}\n")
    ENDFOREACH()
  ELSE()
    # No cpp, write empty file (will be filled later
    FILE(WRITE  ${PyInit} "")
  ENDIF()

  # Generate version file if utils module
  IF(${_component} MATCHES "utils")
    # Create version.py file + install
    SET(_py_version "${LTS5_BINARY_DIR}/python/${_component}/version.py")
    CONFIGURE_FILE("${CMAKE_CURRENT_SOURCE_DIR}/python/version.py.in"
            ${_py_version})
    INSTALL(FILES ${_py_version} DESTINATION ${PYTHON_SITE_PACKAGES_INSTALL_DIR}/lts5/${_component})
    # Update __init__.py to expose version
    FILE(APPEND ${PyInit} "from .version import version as __version__\n")
  ENDIF(${_component} MATCHES "utils")
  # Add custom python files
  SET(_py_submodules "") # List of all python submodule
  FOREACH(_pyfile ${ARG_PYFILES})
    GET_FILENAME_COMPONENT(_fname ${_pyfile} NAME_WE)
    GET_FILENAME_COMPONENT(_fdir ${_pyfile} DIRECTORY)
    STRING(SUBSTRING ${_fdir} 6 -1 _subdir) # remove "script" folder
    # Install python package
    INSTALL(FILES ${_pyfile} DESTINATION ${PYTHON_SITE_PACKAGES_INSTALL_DIR}/lts5/${_component}/${_subdir})
    IF(_subdir)
      # Replace only the first forward slash, might have multiple subfolders.
      STRING(REGEX REPLACE "^/" "" _subdir ${_subdir})
      # Has subfolder already an __init__ file ?
      SET(_py_submodule_init "${LTS5_BINARY_DIR}/python/${_component}/${_subdir}/__init__.py")
      IF (NOT ${_subdir} IN_LIST _py_submodules)
        SET(_py_submodules ${_py_submodules} ${_subdir})
        FILE(WRITE ${_py_submodule_init} "# Automatically generated for ${_subdir} module\n")
        INSTALL(FILES ${_py_submodule_init} DESTINATION ${PYTHON_SITE_PACKAGES_INSTALL_DIR}/lts5/${_component}/${_subdir})
      ENDIF (NOT ${_subdir} IN_LIST _py_submodules)
      # Import into main modules
      FILE(APPEND ${_py_submodule_init} "from .${_fname} import *\n")
      # Update subdir
      SET(_subdir "${_subdir}.")
      # Replace all slash into "." for proper import in __init__.py file
      STRING(REGEX REPLACE "/" "." _subdir ${_subdir})
    ENDIF(_subdir)
    # Global package init if not skipped
    IF (NOT ARG_SKIP_PYINIT OR NOT ${_fname} IN_LIST ARG_SKIP_PYINIT)
      FILE(APPEND ${PyInit} "from lts5.${_component}.${_subdir}${_fname} import *\n")
    ENDIF ()
  ENDFOREACH()
endmacro(LTS5_ADD_PYTHON_WRAPPER)


###############################################################################
# Add custom tensorflow op to python distribution
# Create utility script to load custom op
# _name       The library name.
# _component  The part of LTS5 that this library belongs to.
# ARGN
# FILES     The source files for the library.
# LINK_WITH Libraries to link with
# PYFILES   Python files to install
# PYINIT    Python __init__ file template location
macro(LTS5_ADD_TENSORFLOW_OP _name _component)
  # Get parameters
  set(options)
  set(oneValueArgs)
  set(multiValueArgs FILES LINK_WITH PYFILES PYINIT)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
  # Add library
  LTS5_ADD_LIBRARY(${_name} ${_component} ${ARG_FILES})
  TARGET_LINK_LIBRARIES(${_name} PUBLIC ${ARG_LINK_WITH})
  IF(APPLE AND WITH_CUDA)
    # We need to add the path to the driver (libcuda.dylib) as an rpath,
    # so that the static cuda runtime can find it at runtime.
    SET_PROPERTY(TARGET ${_name} PROPERTY BUILD_RPATH ${CMAKE_CUDA_IMPLICIT_LINK_DIRECTORIES})
  ENDIF()
  # Create python init file, from template
  SET(_PyLibName ${CMAKE_SHARED_LIBRARY_PREFIX}${_name}${CMAKE_SHARED_LIBRARY_SUFFIX})
  SET(_PyInstall ${CMAKE_INSTALL_PREFIX}/lib/${_PyLibName})
  SET(_PyModule ${_name})
  SET(_PyInit "${LTS5_BINARY_DIR}/python/${_component}/__init__.py")
  # Generate configure file + declare custom Ops with gradient inclueded
  CONFIGURE_FILE(${ARG_PYINIT} ${_PyInit})
  # Add custom python package
  SET(_py_submodules "") # List of all python submodule
  FOREACH(_pyfile ${ARG_PYFILES})
    GET_FILENAME_COMPONENT(_fname ${_pyfile} NAME_WE)
    GET_FILENAME_COMPONENT(_fdir ${_pyfile} DIRECTORY)
    STRING(SUBSTRING ${_fdir} 6 -1 _subdir) # remove "script" folder
    # Install python package
    INSTALL(FILES ${_pyfile} DESTINATION ${PYTHON_SITE_PACKAGES_INSTALL_DIR}/lts5/${_component}/${_subdir})
    IF(_subdir)
      # Replace only the first forward slash, might have multiple subfolders.
      STRING(REGEX REPLACE "^/" "" _subdir ${_subdir})
      # Has subfolder already an __init__ file ?
      SET(_py_submodule_init "${LTS5_BINARY_DIR}/python/${_component}/${_subdir}/__init__.py")
      IF (NOT ${_subdir} IN_LIST _py_submodules)
        SET(_py_submodules ${_py_submodules} ${_subdir})
        FILE(WRITE ${_py_submodule_init} "# Automatically generated for ${_subdir} module\n")
        INSTALL(FILES ${_py_submodule_init} DESTINATION ${PYTHON_SITE_PACKAGES_INSTALL_DIR}/lts5/${_component}/${_subdir})
      ENDIF (NOT ${_subdir} IN_LIST _py_submodules)
      # Import into main modules
      FILE(APPEND ${_py_submodule_init} "from .${_fname} import *\n")
      # Update subdir
      SET(_subdir "${_subdir}.")
      # Replace all slash into "." for proper import in __init__.py file
      STRING(REGEX REPLACE "/" "." _subdir ${_subdir})
    ENDIF(_subdir)
    # Global package init
    FILE(APPEND ${_PyInit} "from lts5.${_component}.${_subdir}${_fname} import *\n")
  ENDFOREACH()
  # Install destination
  install(FILES ${_PyInit} DESTINATION ${PYTHON_SITE_PACKAGES_INSTALL_DIR}/lts5/${_component})
endmacro(LTS5_ADD_TENSORFLOW_OP)

###############################################################################
# Add compile flags to a target (because CMake doesn't provide something so
# common itself).
# _name The target name.
# _flags The new compile flags to be added, as a string.
macro(LTS5_ADD_CFLAGS _name _flags)
  get_target_property(_current_flags ${_name} COMPILE_FLAGS)
  if(NOT _current_flags)
    set_target_properties(${_name} PROPERTIES COMPILE_FLAGS ${_flags})
  else(NOT _current_flags)
    set_target_properties(${_name} PROPERTIES
            COMPILE_FLAGS "${_current_flags} ${_flags}")
  endif(NOT _current_flags)
endmacro(LTS5_ADD_CFLAGS)


###############################################################################
# Add link flags to a target (because CMake doesn't provide something so
# common itself).
# _name The target name.
# _flags The new link flags to be added, as a string.
macro(LTS5_ADD_LINKFLAGS _name _flags)
  get_target_property(_current_flags ${_name} LINK_FLAGS)
  if(NOT _current_flags)
    set_target_properties(${_name} PROPERTIES LINK_FLAGS ${_flags})
  else(NOT _current_flags)
    set_target_properties(${_name} PROPERTIES
            LINK_FLAGS "${_current_flags} ${_flags}")
  endif(NOT _current_flags)
endmacro(LTS5_ADD_LINKFLAGS)


###############################################################################
# Make a pkg-config file for a library. Do not include general LTS5 stuff in the
# arguments; they will be added automaticaly.
# _name The library name. "lts5_" will be preprended to this.
# _component The part of LTS5 that this pkg-config file belongs to.
# _desc Description of the library.
# _lts5_deps External dependencies to lts5 libs, as a list. (will get mangled to external pkg-config name)
# _ext_deps External dependencies, as a list.
# _int_deps Internal dependencies, as a list.
# _cflags Compiler flags necessary to build with the library.
# _lib_flags Linker flags necessary to link to the library.
macro(LTS5_MAKE_PKGCONFIG _name _component _desc _lts5_deps _ext_deps _int_deps _cflags
        _lib_flags)
  set(PKG_NAME ${_name})
  set(PKG_DESC ${_desc})
  set(PKG_CFLAGS ${_cflags})
  set(PKG_LIBFLAGS ${_lib_flags})
  LIST_TO_STRING(_ext_deps_str "${_ext_deps}")
  set(PKG_EXTERNAL_DEPS ${_ext_deps_str})
  foreach(_dep ${_lts5_deps})
    set(PKG_EXTERNAL_DEPS "${PKG_EXTERNAL_DEPS} lts5_${_dep}-${LTS5_VERSION_MAJOR}.${LTS5_VERSION_MINOR}")
  endforeach(_dep)
  set(PKG_INTERNAL_DEPS "")
  foreach(_dep ${_int_deps})
    set(PKG_INTERNAL_DEPS "${PKG_INTERNAL_DEPS} -l${_dep}")
  endforeach(_dep)

  set(_pc_file ${CMAKE_CURRENT_BINARY_DIR}/${_name}-${LTS5_VERSION_MAJOR}.${LTS5_VERSION_MINOR}.pc)
  configure_file(${PROJECT_SOURCE_DIR}/cmake/pkgconfig.cmake.in ${_pc_file}
          @ONLY)
  install(FILES ${_pc_file} DESTINATION ${PKGCFG_INSTALL_DIR}
          COMPONENT lts5_${_component})
endmacro(LTS5_MAKE_PKGCONFIG)

###############################################################################
# Make a pkg-config file for a header-only library.
# Essentially a duplicate of LTS5_MAKE_PKGCONFIG, but
# ensures that no -L or l flags will be created
# Do not include general LTS5 stuff in the
# arguments; they will be added automaticaly.
# _name The library name. "lts5_" will be preprended to this.
# _component The part of LTS5 that this pkg-config file belongs to.
# _desc Description of the library.
# _lts5_deps External dependencies to lts5 libs, as a list. (will get mangled to external pkg-config name)
# _ext_deps External dependencies, as a list.
# _int_deps Internal dependencies, as a list.
# _cflags Compiler flags necessary to build with the library.
macro(LTS5_MAKE_PKGCONFIG_HEADER_ONLY _name _component _desc _lts5_deps _ext_deps _int_deps _cflags)
  set(PKG_NAME ${_name})
  set(PKG_DESC ${_desc})
  set(PKG_CFLAGS ${_cflags})
  #set(PKG_LIBFLAGS ${_lib_flags})
  LIST_TO_STRING(_ext_deps_str "${_ext_deps}")
  set(PKG_EXTERNAL_DEPS ${_ext_deps_str})
  foreach(_dep ${_lts5_deps})
    set(PKG_EXTERNAL_DEPS "${PKG_EXTERNAL_DEPS} lts5_${_dep}-${LTS5_VERSION_MAJOR}.${LTS5_VERSION_MINOR}")
  endforeach(_dep)
  set(PKG_INTERNAL_DEPS "")
  foreach(_dep ${_int_deps})
    set(PKG_INTERNAL_DEPS "${PKG_INTERNAL_DEPS} -l${_dep}")
  endforeach(_dep)
  set(_pc_file ${CMAKE_CURRENT_BINARY_DIR}/${_name}-${LTS5_VERSION_MAJOR}.${LTS5_VERSION_MINOR}.pc)
  configure_file(${PROJECT_SOURCE_DIR}/cmake/pkgconfig-headeronly.cmake.in ${_pc_file} @ONLY)
  install(FILES ${_pc_file} DESTINATION ${PKGCFG_INSTALL_DIR}
          COMPONENT lts5_${_component})
endmacro(LTS5_MAKE_PKGCONFIG_HEADER_ONLY)


###############################################################################
# PRIVATE

###############################################################################
# Reset the subsystem status map.
macro(LTS5_RESET_MAPS)
  foreach(_ss ${LTS5_SUBSYSTEMS})
    string(TOUPPER "LTS5_${_ss}_SUBSYS" LTS5_SUBSYS_SUBSYS)
    if (${LTS5_SUBSYS_SUBSYS})
      string(TOUPPER "LTS5_${_ss}_SUBSYS_DESC" LTS5_PARENT_SUBSYS_DESC)
      set(${LTS5_SUBSYS_SUBSYS_DESC} "" CACHE INTERNAL "" FORCE)
      set(${LTS5_SUBSYS_SUBSYS} "" CACHE INTERNAL "" FORCE)
    endif (${LTS5_SUBSYS_SUBSYS})
  endforeach(_ss)

  set(LTS5_SUBSYS_HYPERSTATUS "" CACHE INTERNAL
          "To Build Or Not To Build, That Is The Question." FORCE)
  set(LTS5_SUBSYS_STATUS "" CACHE INTERNAL
          "To build or not to build, that is the question." FORCE)
  set(LTS5_SUBSYS_REASONS "" CACHE INTERNAL
          "But why?" FORCE)
  set(LTS5_SUBSYS_DEPS "" CACHE INTERNAL "A depends on B and C." FORCE)
  set(LTS5_SUBSYS_EXT_DEPS "" CACHE INTERNAL "A depends on B and C." FORCE)
  set(LTS5_SUBSYS_OPT_DEPS "" CACHE INTERNAL "A depends on B and C." FORCE)
  set(LTS5_SUBSYSTEMS "" CACHE INTERNAL "Internal list of subsystems" FORCE)
  set(LTS5_SUBSYS_DESC "" CACHE INTERNAL "Subsystem descriptions" FORCE)
endmacro(LTS5_RESET_MAPS)


###############################################################################
# Register a subsystem.
# _name Subsystem name.
# _desc Description of the subsystem
macro(LTS5_ADD_SUBSYSTEM _name _desc)
  set(_temp ${LTS5_SUBSYSTEMS})
  list(APPEND _temp ${_name})
  set(LTS5_SUBSYSTEMS ${_temp} CACHE INTERNAL "Internal list of subsystems"
          FORCE)
  SET_IN_GLOBAL_MAP(LTS5_SUBSYS_DESC ${_name} ${_desc})
endmacro(LTS5_ADD_SUBSYSTEM)

###############################################################################
# Register a subsubsystem.
# _name Subsystem name.
# _desc Description of the subsystem
macro(LTS5_ADD_SUBSUBSYSTEM _parent _name _desc)
  string(TOUPPER "LTS5_${_parent}_SUBSYS" LTS5_PARENT_SUBSYS)
  string(TOUPPER "LTS5_${_parent}_SUBSYS_DESC" LTS5_PARENT_SUBSYS_DESC)
  set(_temp ${${LTS5_PARENT_SUBSYS}})
  list(APPEND _temp ${_name})
  set(${LTS5_PARENT_SUBSYS} ${_temp} CACHE INTERNAL "Internal list of ${_parenr} subsystems"
          FORCE)
  set_in_global_map(${LTS5_PARENT_SUBSYS_DESC} ${_name} ${_desc})
endmacro(LTS5_ADD_SUBSUBSYSTEM)


###############################################################################
# Set the status of a subsystem.
# _name Subsystem name.
# _status TRUE if being built, FALSE otherwise.
# ARGN[0] Reason for not building.
macro(LTS5_SET_SUBSYS_STATUS _name _status)
  if(${ARGC} EQUAL 3)
    set(_reason ${ARGV2})
  else(${ARGC} EQUAL 3)
    set(_reason "No reason")
  endif(${ARGC} EQUAL 3)
  SET_IN_GLOBAL_MAP(LTS5_SUBSYS_STATUS ${_name} ${_status})
  SET_IN_GLOBAL_MAP(LTS5_SUBSYS_REASONS ${_name} ${_reason})
endmacro(LTS5_SET_SUBSYS_STATUS)

###############################################################################
# Set the status of a subsystem.
# _name Subsystem name.
# _status TRUE if being built, FALSE otherwise.
# ARGN[0] Reason for not building.
macro(LTS5_SET_SUBSUBSYS_STATUS _parent _name _status)
  if(${ARGC} EQUAL 4)
    set(_reason ${ARGV2})
  else(${ARGC} EQUAL 4)
    set(_reason "No reason")
  endif(${ARGC} EQUAL 4)
  SET_IN_GLOBAL_MAP(LTS5_SUBSYS_STATUS ${_parent}_${_name} ${_status})
  SET_IN_GLOBAL_MAP(LTS5_SUBSYS_REASONS ${_parent}_${_name} ${_reason})
endmacro(LTS5_SET_SUBSUBSYS_STATUS)


###############################################################################
# Get the status of a subsystem
# _var Destination variable.
# _name Name of the subsystem.
macro(LTS5_GET_SUBSYS_STATUS _var _name)
  GET_IN_MAP(${_var} LTS5_SUBSYS_STATUS ${_name})
endmacro(LTS5_GET_SUBSYS_STATUS)

###############################################################################
# Get the status of a subsystem
# _var Destination variable.
# _name Name of the subsystem.
macro(LTS5_GET_SUBSUBSYS_STATUS _var _parent _name)
  GET_IN_MAP(${_var} LTS5_SUBSYS_STATUS ${_parent}_${_name})
endmacro(LTS5_GET_SUBSUBSYS_STATUS)


###############################################################################
# Set the hyperstatus of a subsystem and its dependee
# _name Subsystem name.
# _dependee Dependant subsystem.
# _status AUTO_OFF to disable AUTO_ON to enable
# ARGN[0] Reason for not building.
macro(LTS5_SET_SUBSYS_HYPERSTATUS _name _dependee _status)
  SET_IN_GLOBAL_MAP(LTS5_SUBSYS_HYPERSTATUS ${_name}_${_dependee} ${_status})
  if(${ARGC} EQUAL 4)
    SET_IN_GLOBAL_MAP(LTS5_SUBSYS_REASONS ${_dependee} ${ARGV3})
  endif(${ARGC} EQUAL 4)
endmacro(LTS5_SET_SUBSYS_HYPERSTATUS)

###############################################################################
# Get the hyperstatus of a subsystem and its dependee
# _name IN subsystem name.
# _dependee IN dependant subsystem.
# _var OUT hyperstatus
# ARGN[0] Reason for not building.
macro(LTS5_GET_SUBSYS_HYPERSTATUS _var _name)
  set(${_var} "AUTO_ON")
  if(${ARGC} EQUAL 3)
    GET_IN_MAP(${_var} LTS5_SUBSYS_HYPERSTATUS ${_name}_${ARGV2})
  else(${ARGC} EQUAL 3)
    foreach(subsys ${LTS5_SUBSYS_DEPS_${_name}})
      if("${LTS5_SUBSYS_HYPERSTATUS_${subsys}_${_name}}" STREQUAL "AUTO_OFF")
        set(${_var} "AUTO_OFF")
        break()
      endif("${LTS5_SUBSYS_HYPERSTATUS_${subsys}_${_name}}" STREQUAL "AUTO_OFF")
    endforeach(subsys)
  endif(${ARGC} EQUAL 3)
endmacro(LTS5_GET_SUBSYS_HYPERSTATUS)

###############################################################################
# Set the hyperstatus of a subsystem and its dependee
macro(LTS5_UNSET_SUBSYS_HYPERSTATUS _name _dependee)
  unset(LTS5_SUBSYS_HYPERSTATUS_${_name}_${dependee})
endmacro(LTS5_UNSET_SUBSYS_HYPERSTATUS)

###############################################################################
# Set the include directory name of a subsystem.
# _name Subsystem name.
# _includedir Name of subdirectory for includes
# ARGN[0] Reason for not building.
macro(LTS5_SET_SUBSYS_INCLUDE_DIR _name _includedir)
  SET_IN_GLOBAL_MAP(LTS5_SUBSYS_INCLUDE ${_name} ${_includedir})
endmacro(LTS5_SET_SUBSYS_INCLUDE_DIR)


###############################################################################
# Get the include directory name of a subsystem - return _name if not set
# _var Destination variable.
# _name Name of the subsystem.
macro(LTS5_GET_SUBSYS_INCLUDE_DIR _var _name)
  GET_IN_MAP(${_var} LTS5_SUBSYS_INCLUDE ${_name})
  if(NOT ${_var})
    set (${_var} ${_name})
  endif(NOT ${_var})
endmacro(LTS5_GET_SUBSYS_INCLUDE_DIR)


###############################################################################
# Write a report on the build/not-build status of the subsystems
macro(LTS5_WRITE_STATUS_REPORT)
  message(STATUS "The following subsystems will be built:")
  foreach(_ss ${LTS5_SUBSYSTEMS})
    LTS5_GET_SUBSYS_STATUS(_status ${_ss})
    if(_status)
      set(message_text "  ${_ss}")
      string(TOUPPER "LTS5_${_ss}_SUBSYS" LTS5_SUBSYS_SUBSYS)
      if (${LTS5_SUBSYS_SUBSYS})
        set(will_build)
        foreach(_sub ${${LTS5_SUBSYS_SUBSYS}})
          LTS5_GET_SUBSYS_STATUS(_sub_status ${_ss}_${_sub})
          if (_sub_status)
            set(will_build "${will_build}\n       |_ ${_sub}")
          endif (_sub_status)
        endforeach(_sub)
        if (NOT ("${will_build}" STREQUAL ""))
          set(message_text  "${message_text}\n       building: ${will_build}")
        endif (NOT ("${will_build}" STREQUAL ""))
        set(wont_build)
        foreach(_sub ${${LTS5_SUBSYS_SUBSYS}})
          LTS5_GET_SUBSYS_STATUS(_sub_status ${_ss}_${_sub})
          LTS5_GET_SUBSYS_HYPERSTATUS(_sub_hyper_status ${_ss}_${sub})
          if (NOT _sub_status OR ("${_sub_hyper_status}" STREQUAL "AUTO_OFF"))
            GET_IN_MAP(_reason LTS5_SUBSYS_REASONS ${_ss}_${_sub})
            set(wont_build "${wont_build}\n       |_ ${_sub}: ${_reason}")
          endif (NOT _sub_status OR ("${_sub_hyper_status}" STREQUAL "AUTO_OFF"))
        endforeach(_sub)
        if (NOT ("${wont_build}" STREQUAL ""))
          set(message_text  "${message_text}\n       not building: ${wont_build}")
        endif (NOT ("${wont_build}" STREQUAL ""))
      endif (${LTS5_SUBSYS_SUBSYS})
      message(STATUS "${message_text}")
    endif(_status)
  endforeach(_ss)

  message(STATUS "The following subsystems will not be built:")
  foreach(_ss ${LTS5_SUBSYSTEMS})
    LTS5_GET_SUBSYS_STATUS(_status ${_ss})
    LTS5_GET_SUBSYS_HYPERSTATUS(_hyper_status ${_ss})
    if(NOT _status OR ("${_hyper_status}" STREQUAL "AUTO_OFF"))
      GET_IN_MAP(_reason LTS5_SUBSYS_REASONS ${_ss})
      message(STATUS "  ${_ss}: ${_reason}")
    endif(NOT _status OR ("${_hyper_status}" STREQUAL "AUTO_OFF"))
  endforeach(_ss)
endmacro(LTS5_WRITE_STATUS_REPORT)

##############################################################################
# Collect subdirectories from dirname that contains filename and store them in
#  varname.
# WARNING If extra arguments are given then they are considered as exception
# list and varname will contain subdirectories of dirname that contains
# fielename but doesn't belong to exception list.
# dirname IN parent directory
# filename IN file name to look for in each subdirectory of parent directory
# varname OUT list of subdirectories containing filename
# exception_list OPTIONAL and contains list of subdirectories not to account
macro(collect_subproject_directory_names dirname filename names dirs)
  file(GLOB globbed RELATIVE "${dirname}" "${dirname}/*/${filename}")
  if(${ARGC} GREATER 4)
    set(exclusion_list ${ARGN})
    foreach(file ${globbed})
      get_filename_component(dir ${file} PATH)
      list(FIND exclusion_list ${dir} excluded)
      if(excluded EQUAL -1)
        set(${dirs} ${${dirs}} ${dir})
      endif(excluded EQUAL -1)
    endforeach()
  else(${ARGC} GREATER 4)
    foreach(file ${globbed})
      get_filename_component(dir ${file} PATH)
      set(${dirs} ${${dirs}} ${dir})
    endforeach(file)
  endif(${ARGC} GREATER 4)
  foreach(subdir ${${dirs}})
    file(STRINGS ${dirname}/${subdir}/CMakeLists.txt name REGEX "[setSET ]+\\(.*SUBSYS_NAME .*\\)$")
    string(REGEX REPLACE "[setSET ]+\\(.*SUBSYS_NAME[ ]+([A-Za-z0-9_]+)[ ]*\\)" "\\1" name "${name}")
    set(${names} ${${names}} ${name})
    file(STRINGS ${dirname}/${subdir}/CMakeLists.txt DEPENDENCIES REGEX "set.*SUBSYS_DEPS .*\\)")
    string(REGEX REPLACE "set.*SUBSYS_DEPS" "" DEPENDENCIES "${DEPENDENCIES}")
    string(REPLACE ")" "" DEPENDENCIES "${DEPENDENCIES}")
    string(STRIP "${DEPENDENCIES}" DEPENDENCIES)
    string(REPLACE " " ";" DEPENDENCIES "${DEPENDENCIES}")
    # Check CUDA dependencies
    IF(CUDA_FOUND AND BUILD_CUDA)
      file(STRINGS ${dirname}/${subdir}/CMakeLists.txt CUDA_DEPENDENCIES REGEX "set.*CUDA_DEPS .*\\)")
      string(REGEX REPLACE "set.*CUDA_DEPS" "" CUDA_DEPENDENCIES "${CUDA_DEPENDENCIES}")
      string(REPLACE ")" "" CUDA_DEPENDENCIES "${CUDA_DEPENDENCIES}")
      string(STRIP "${CUDA_DEPENDENCIES}" CUDA_DEPENDENCIES)
      string(REPLACE " " ";" CUDA_DEPENDENCIES "${CUDA_DEPENDENCIES}")
      list(APPEND DEPENDENCIES ${CUDA_DEPENDENCIES})
    ENDIF(CUDA_FOUND AND BUILD_CUDA)
    # Check python dependencies
    IF(OPT_PYTHON_WRAPPER)
      file(STRINGS ${dirname}/${subdir}/CMakeLists.txt PYTHON_DEPENDENCIES REGEX "set.*PYTHON_DEPS .*\\)")
      string(REGEX REPLACE "set.*PYTHON_DEPS" "" PYTHON_DEPENDENCIES "${PYTHON_DEPENDENCIES}")
      string(REPLACE ")" "" PYTHON_DEPENDENCIES "${PYTHON_DEPENDENCIES}")
      string(STRIP "${PYTHON_DEPENDENCIES}" PYTHON_DEPENDENCIES)
      string(REPLACE " " ";" PYTHON_DEPENDENCIES "${PYTHON_DEPENDENCIES}")
      list(APPEND DEPENDENCIES ${PYTHON_DEPENDENCIES})
    ENDIF(OPT_PYTHON_WRAPPER)
    if(NOT("${DEPENDENCIES}" STREQUAL ""))
      list(REMOVE_ITEM DEPENDENCIES "#")
      string(TOUPPER "LTS5_${name}_DEPENDS" SUBSYS_DEPENDS)
      set(${SUBSYS_DEPENDS} ${DEPENDENCIES})
      foreach(dependee ${DEPENDENCIES})
        string(TOUPPER "LTS5_${dependee}_DEPENDIES" SUBSYS_DEPENDIES)
        set(${SUBSYS_DEPENDIES} ${${SUBSYS_DEPENDIES}} ${name})
      endforeach(dependee)
    endif(NOT("${DEPENDENCIES}" STREQUAL ""))
  endforeach(subdir)
endmacro()

########################################################################################
# Macro to disable subsystem dependies
# _subsys IN subsystem name
macro(LTS5_DISABLE_DEPENDIES _subsys)
  string(TOUPPER "lts5_${_subsys}_dependies" LTS5_SUBSYS_DEPENDIES)
  if(NOT ("${${LTS5_SUBSYS_DEPENDIES}}" STREQUAL ""))
    foreach(dep ${${LTS5_SUBSYS_DEPENDIES}})
      LTS5_SET_SUBSYS_HYPERSTATUS(${_subsys} ${dep} AUTO_OFF "Disabled: ${_subsys} missing.")
      set(BUILD_${dep} OFF CACHE BOOL "Disabled: ${_subsys} missing." FORCE)
    endforeach(dep)
  endif(NOT ("${${LTS5_SUBSYS_DEPENDIES}}" STREQUAL ""))
endmacro(LTS5_DISABLE_DEPENDIES subsys)

########################################################################################
# Macro to enable subsystem dependies
# _subsys IN subsystem name
macro(LTS5_ENABLE_DEPENDIES _subsys)
  string(TOUPPER "lts5_${_subsys}_dependies" LTS5_SUBSYS_DEPENDIES)
  if(NOT ("${${LTS5_SUBSYS_DEPENDIES}}" STREQUAL ""))
    foreach(dep ${${LTS5_SUBSYS_DEPENDIES}})
      LTS5_GET_SUBSYS_HYPERSTATUS(dependee_status ${_subsys} ${dep})
      if("${dependee_status}" STREQUAL "AUTO_OFF")
        LTS5_SET_SUBSYS_HYPERSTATUS(${_subsys} ${dep} AUTO_ON)
        GET_IN_MAP(desc LTS5_SUBSYS_DESC ${dep})
        set(BUILD_${dep} ON CACHE BOOL "${desc}" FORCE)
      endif("${dependee_status}" STREQUAL "AUTO_OFF")
    endforeach(dep)
  endif(NOT ("${${LTS5_SUBSYS_DEPENDIES}}" STREQUAL ""))
endmacro(LTS5_ENABLE_DEPENDIES subsys)

########################################################################################
# Macro to build subsystem centric documentation
# _subsys IN the name of the subsystem to generate documentation for
macro (LTS5_ADD_DOC _subsys)
  string(TOUPPER "${_subsys}" SUBSYS)
  set(doc_subsys "doc_${_subsys}")
  GET_IN_MAP(dependencies LTS5_SUBSYS_DEPS ${_subsys})
  if(DOXYGEN_FOUND)
    if(HTML_HELP_COMPILER)
      set(DOCUMENTATION_HTML_HELP YES)
    else(HTML_HELP_COMPILER)
      set(DOCUMENTATION_HTML_HELP NO)
    endif(HTML_HELP_COMPILER)
    if(DOXYGEN_DOT_EXECUTABLE)
      set(HAVE_DOT YES)
    else(DOXYGEN_DOT_EXECUTABLE)
      set(HAVE_DOT NO)
    endif(DOXYGEN_DOT_EXECUTABLE)
    if(NOT "${dependencies}" STREQUAL "")
      set(STRIPPED_HEADERS "${LTS5_SOURCE_DIR}/${dependencies}/include")
      string(REPLACE ";" "/include \\\n\t\t\t\t\t\t\t\t\t\t\t\t ${LTS5_SOURCE_DIR}/"
              STRIPPED_HEADERS "${STRIPPED_HEADERS}")
    endif(NOT "${dependencies}" STREQUAL "")
    set(DOC_SOURCE_DIR "\"${CMAKE_CURRENT_SOURCE_DIR}\"\\")
    foreach(dep ${dependencies})
      set(DOC_SOURCE_DIR
              "${DOC_SOURCE_DIR}\n\t\t\t\t\t\t\t\t\t\t\t\t \"${LTS5_SOURCE_DIR}/${dep}\"\\")
    endforeach(dep)
    file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/html")
    set(doxyfile "${CMAKE_CURRENT_BINARY_DIR}/doxyfile")
    configure_file("${LTS5_SOURCE_DIR}/doc/lts5_doxyfile.in" ${doxyfile})
    add_custom_target(${doc_subsys} ${DOXYGEN_EXECUTABLE} ${doxyfile})
    if(USE_PROJECT_FOLDERS)
      set_target_properties(${doc_subsys} PROPERTIES FOLDER "Documentation")
    endif(USE_PROJECT_FOLDERS)
  endif(DOXYGEN_FOUND)
endmacro(LTS5_ADD_DOC)
