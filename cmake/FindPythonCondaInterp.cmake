# FindPythonCondaInterp
# ----------------
#
# Find anaconda python interpreter
#
# This module finds if Python interpreter (Anaconda) is installed and determines
# where the executables are.  This code sets the following variables:
#
# ::
#
#   PYTHONINTERP_FOUND         - Was the Python executable found
#   PYTHON_EXECUTABLE          - path to the Python interpreter
#
# If no anaconda interpreter is found, fall back to standard python distribution
#
# Based on:
# https://github.com/jkhoogland/FindPythonAnaconda/blob/master/FindPythonAnaconda.cmake

# Default
SET(PYTHONINTERP_FOUND FALSE)
# Search for python executable if none are specified
IF(NOT PYTHON_EXECUTABLE)

  MESSAGE(STATUS "Call conda")

  # Search for anaconda
  SET(_cmd conda info --root)
  EXECUTE_PROCESS(COMMAND ${_cmd}
                  RESULT_VARIABLE _r
                  OUTPUT_VARIABLE _o
                  ERROR_VARIABLE _e
                  OUTPUT_STRIP_TRAILING_WHITESPACE
                  ERROR_STRIP_TRAILING_WHITESPACE)

  IF(ANACONDA_PYTHON_VERBOSE)
    MESSAGE("Executing conda info --root")
    MESSAGE("_r = ${_r}")
    MESSAGE("_o = ${_o}")
    MESSAGE("_e = ${_e}")
  endif()

  IF(IS_DIRECTORY ${_o})
    SET(ANACONDA_FOUND TRUE)
  ENDIF(IS_DIRECTORY ${_o})

  IF(ANACONDA_FOUND)
    # Found anaconda interpreter
    SET(ANACONDA_DIR ${_o} )
    MESSAGE(STATUS "Found anaconda root directory ${ANACONDA_DIR}" )
    # Find python interpreter
    FIND_PROGRAM(ANACONDA_EXECUTABLE NAMES "python" HINTS ${ANACONDA_DIR}/bin NO_DEFAULT_PATH)

      MESSAGE(STATUS "exec ${ANACONDA_EXECUTABLE}")

    IF(ANACONDA_EXECUTABLE)
      MESSAGE(STATUS "Found python anaconda")
      # Set appropriate vars
      SET(PYTHONINTERP_FOUND TRUE)
      SET(PYTHON_EXECUTABLE ${ANACONDA_EXECUTABLE})
    ENDIF(ANACONDA_EXECUTABLE)
  ELSE(ANACONDA_FOUND)
    # Fall back into standard package
    MESSAGE(STATUS "Fall back")
    FIND_PACKAGE(PythonInterp)
  ENDIF(ANACONDA_FOUND)
ELSE(NOT PYTHON_EXECUTABLE)
  MESSAGE(STATUS "Skip exec detection")
ENDIF(NOT PYTHON_EXECUTABLE)