#
# Try to find Pylint executable
#
# Variable to be set:
#
#   PYLINT_FOUND  - Flag indicating that pyling executable have be found
#
# Create an imported executable

IF(NOT PYLINT_FOUND)
  # Find pylint
  FIND_PROGRAM(PYLINT_EXECUTABLE pylint)
  IF(PYLINT_EXECUTABLE)
    # Extract version
    EXECUTE_PROCESS(COMMAND ${PYLINT_EXECUTABLE} --version
            OUTPUT_VARIABLE PYLINT_OUTPUT
            RESULT_VARIABLE PYLINT_RESULT
            ERROR_VARIABLE PYLINT_ERROR)
    # Extract version
    IF(NOT PYLINT_RESULT)
      STRING(REGEX REPLACE
              "pylint ([0-9]+\\.[0-9]+\\.[0-9]+).*"
              "\\1"
              PYLINT_VERSION
              ${PYLINT_OUTPUT})
    ENDIF(NOT PYLINT_RESULT)
  ENDIF(PYLINT_EXECUTABLE)
ENDIF(NOT PYLINT_FOUND)

# Handle package
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Pylint DEFAULT_MSG PYLINT_VERSION PYLINT_EXECUTABLE)
MARK_AS_ADVANCED(PYLINT_VERSION PYLINT_EXECUTABLE)