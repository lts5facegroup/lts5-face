#.rst:
# FindDoxyrest
# -----------
#
# This module looks for Doxyrest and highly inspired by FindDoxygen.cmake 
# file. 
# See: https://github.com/Kitware/CMake/blob/master/Modules/FindDoxygen.cmake
#
#]=======================================================================]

cmake_policy(PUSH)
cmake_policy(SET CMP0057 NEW) # if IN_LIST
  
include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)

# Helper functions

function(_Doxyrest_get_version doxy_version result_var doxy_path)
  # doxyrest v<MAJOR>.<MINOR>.<PATCH> (<ARCH>)
  execute_process(COMMAND "${doxy_path}" --version
                  OUTPUT_VARIABLE full_doxyrest_version
                  OUTPUT_STRIP_TRAILING_WHITESPACE
                  RESULT_VARIABLE version_result)
  # Ignore any commit hashes, etc.
  string(REGEX MATCH [[^[0-9]+\.[0-9]+\.[0-9]+]] sem_doxyrest_version "${result_var}")

  set(${result_var} ${version_result} PARENT_SCOPE)
  set(${doxy_version} ${sem_doxyrest_version} PARENT_SCOPE)
endfunction()

function(_Doxyrest_version_validator version_match doxy_path)
  if(NOT DEFINED Doxyrest_FIND_VERSION)
    # Not looking for specific version
    set(${is_valid_version} TRUE PARENT_SCOPE)
  else()
    _Doxyrest_get_version(candidate_version version_result "${doxy_path}")
    
    if(version_result)
      message(DEBUG "Unable to determine candidate doxygen version at ${doxy_path}: ${version_result}")
    endif()

    find_package_check_version("${candidate_version}" valid_doxy_version HANDLE_VERSION_RANGE)
    set(${version_match} "${valid_doxy_version}" PARENT_SCOPE)
  endif()
endfunction()

function(_Doxyrest_get_install_folder result_var doxy_path)
  get_filename_component(doxy_folder "${doxy_path}" DIRECTORY)
  set(${result_var} ${doxy_folder} PARENT_SCOPE)
endfunction()
#
# Find Doxyrest...
#

macro(_Doxyrest_find_doxyrest)
  find_program(
          DOXYREST_EXECUTABLE
          NAMES doxyrest
          DOC "Doxyrest documentation generation tool (http://www.doxygen.org)")
  mark_as_advanced(DOXYREST_EXECUTABLE)
  # Find exectuable?
  if(DOXYREST_EXECUTABLE)
    _Doxyrest_get_version(DOXYREST_VERSION _Doxyrest_version_result "${DOXYREST_EXECUTABLE}")
    _Doxyrest_get_install_folder(_Doxyrest_folder_result "${DOXYREST_EXECUTABLE}")
    if(_Doxyrest_version_result)
      message(WARNING "Unable to determine doxyrest version: ${_Doxyrest_version_result}")
    endif()
    if(NOT _Doxyrest_folder_result)
      message(WARNING "Unable to determine doxyrest installation folder: ${_Doxyrest_folder_result}")
    endif()
    # Create an imported target for Doxygen
    if(NOT TARGET Doxyrest::doxyrest)
      add_executable(Doxyrest::doxyrest IMPORTED GLOBAL)
      set_target_properties(Doxyrest::doxyrest PROPERTIES IMPORTED_LOCATION "${DOXYREST_EXECUTABLE}")
      set_target_properties(Doxyrest::doxyrest PROPERTIES FOLDER "${_Doxyrest_folder_result}")
    endif()
  endif()
endmacro()

_Doxyrest_find_doxyrest()
if(TARGET Doxyrest::doxyrest)
  set(Doxyrest_FOUND TRUE)
else()
  set(Doxyrest_FOUND FALSE)
endif()

# Verify find results
find_package_handle_standard_args(
  Doxyrest
  REQUIRED_VARS DOXYREST_EXECUTABLE
  VERSION_VAR DOXYREST_VERSION)

# Maintain the _FOUND variables as "YES" or "NO" for backwards
# compatibility.
if(DOXYGEN_FOUND)
    set(DOXYGEN_FOUND "YES")
else()
    set(DOXYGEN_FOUND "NO")
endif()

cmake_policy(POP)