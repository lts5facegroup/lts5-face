# Based on: https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/
include(GNUInstallDirs)

# ---[ Version
CONFIGURE_FILE("${LTS5_SOURCE_DIR}/cmake/LTS5ConfigVersion.cmake.in"
        "${LTS5_BINARY_DIR}/LTS5ConfigVersion.cmake" @ONLY)
INSTALL(FILES ${LTS5_BINARY_DIR}/LTS5ConfigVersion.cmake
        DESTINATION ${LTS5CONFIG_INSTALL_DIR})
# ---[ Configuration
# Basically export all targets (i.e. library) with their proper include folder
# automatically. All targets are placed into a namespace LTS5
INSTALL(EXPORT lts5-export
        FILE LTS5Config.cmake
        NAMESPACE LTS5::
        DESTINATION ${LTS5CONFIG_INSTALL_DIR})