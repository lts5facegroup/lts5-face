#
# Try to find GLFW library and include path.
# Once done this will define
#
# GLFW_FOUND
# GLFW_INCLUDE_DIR
# GLFW_LIBRARIES
#

if(NOT GLFW_FOUND)
FIND_PATH(GLFW_INCLUDE_DIR GLFW/glfw3.h
  PATH
    ${GLFW_LOCATION}/include
    ${PROJECT_SOURCE_DIR}/../../external/glfw/include
    ${PROJECT_SOURCE_DIR}/../external/glfw/include
    ${PROJECT_SOURCE_DIR}/external/glfw/include
    ${PROJECT_SOURCE_DIR}/../../libigl/external/glfw/include
    ${PROJECT_SOURCE_DIR}/../libigl/external/glfw/include
    ${PROJECT_SOURCE_DIR}/libigl/external/glfw/include
    ${PROJECT_SOURCE_DIR}/../../libigl/external/nanogui/ext/glfw/include
    ${PROJECT_SOURCE_DIR}/../libigl/external/nanogui/ext/glfw/include
    ${PROJECT_SOURCE_DIR}/libigl/external/nanogui/ext/glfw/include
    /usr/local/include
    /usr/X11/include
    /usr/include
    /opt/local/include
    )
FIND_LIBRARY( GLFW_LIBRARIES NAMES glfw glfw3 glfw3dll
  PATH
    ${GLFW_LOCATION}/lib
    ${PROJECT_SOURCE_DIR}/../../external/glfw/src
    ${PROJECT_SOURCE_DIR}/../external/glfw/src
    ${PROJECT_SOURCE_DIR}/external/glfw/src
    ${PROJECT_SOURCE_DIR}/../../libigl/external/glfw/src
    ${PROJECT_SOURCE_DIR}/../libigl/external/glfw/src
    ${PROJECT_SOURCE_DIR}/libigl/external/glfw/src
    ${PROJECT_SOURCE_DIR}/../../external/glfw/lib/x64
    ${PROJECT_SOURCE_DIR}/../external/glfw/lib/x64
    ${PROJECT_SOURCE_DIR}/external/glfw/lib/x64
    ${PROJECT_SOURCE_DIR}/../../libigl/external/glfw/lib/x64
    ${PROJECT_SOURCE_DIR}/../libigl/external/glfw/lib/x64
    ${PROJECT_SOURCE_DIR}/libigl/external/glfw/lib/x64
    /usr/local
    /usr/X11
    /usr
    PATH_SUFFIXES
    lib64
    lib
)
SET(GLFW_FOUND "FALSE")
IF (GLFW_INCLUDE_DIR AND GLFW_LIBRARIES)
  SET(GLFW_FOUND "TRUE")
ENDIF (GLFW_INCLUDE_DIR AND GLFW_LIBRARIES)

if(GLFW_FOUND)
  message(STATUS "Found GLFW: ${GLFW_INCLUDE_DIR}")
else(GLFW_FOUND)
  message(STATUS "could NOT find GLFW")
endif(GLFW_FOUND)

endif(NOT GLFW_FOUND)