# Options for building LTS5.

# Export symbols if WIN32
if(WIN32)
  set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_IMPORT_LIBRARY_SUFFIX})
  # Export symbols
  ADD_DEFINITIONS(-DLTS5_API_EXPORTS)
endif(WIN32)

### ---[ CUDA
OPTION(BUILD_CUDA "Build the CUDA-related subsystems" OFF)

### ---[  BUILD
OPTION(OPT_ADD_TRAINING "Build training classes" OFF)
OPTION(OPT_ADD_TIME_PROFILING "Time profiling enabling option" OFF)
IF(OPT_ADD_TIME_PROFILING AND (${CMAKE_SYSTEM_NAME} MATCHES "Darwin" OR ${CMAKE_SYSTEM_NAME} MATCHES "Window"))
  # Available only on linux
  SET(OPT_ADD_TIME_PROFILING OFF CACHE BOOL "Only available on Linux")
ENDIF(OPT_ADD_TIME_PROFILING AND (${CMAKE_SYSTEM_NAME} MATCHES "Darwin" OR ${CMAKE_SYSTEM_NAME} MATCHES "Window"))

### ---[ Documentation
OPTION(OPT_GENERATE_DOC "Automatically generate library documentation" OFF)

### ---[ Tools / Pylint
OPTION(OPT_CHECK_PYLINT "Check python guideline" OFF)

### ---[ Tools / Cpplint
OPTION(OPT_CHECK_CPPLINT "Check C++ guideline" OFF)

### ---[ Examples
OPTION(OPT_BUILD_EXAMPLE "Build example target" OFF)

### ---[ Tests
OPTION(OPT_BUILD_TEST "Build unit test" ON)

### ---[ Python wrapper
OPTION(OPT_PYTHON_WRAPPER "Build python wrapper" ON)

# Display timing information for each compiler instance on screen
OPTION(CMAKE_TIMING_VERBOSE "Enable the display of timing information for each compiler instance." OFF)
mark_as_advanced(CMAKE_TIMING_VERBOSE)

# MSVC extra optimization options. Might lead to increasingly larger compile/link times.
OPTION(CMAKE_MSVC_CODE_LINK_OPTIMIZATION "Enable the /GL and /LTCG code and link optimization options for MSVC. Enabled by default." ON)
mark_as_advanced(CMAKE_MSVC_CODE_LINK_OPTIMIZATION)
