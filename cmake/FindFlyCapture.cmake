# - Find FlyCapture
# Find the native FlyCapture includes and library
#
#   FLYCAPTURE_FOUND       - True if FlyCapture found.
#   FLYCAPTURE_INCLUDE_DIR - where to find flycapture2.h, etc.
#   FLYCAPTURE_LIBRARIES   - List of libraries when using FlycAPTure.
#
#	For windows set ENV{FLY_CAPTURE_SDK}

IF(FLYCAPTURE_INCLUDE_DIR)
    # Already in cache, be silent
    SET( FLYCAPTURE_FIND_QUIETLY TRUE)
ENDIF(FLYCAPTURE_INCLUDE_DIR)

SET(FLYCAPUTRE_HEADER_SEARCH_PATHS
    /usr/include
    /usr/local/include
    ENV{FLY_CAPTURE_SDK}
    ENV{FLY_CAPTURE_SDK}/include)

SET(FLYCAPUTRE_LIBRARY_SEARCH_PATHS
    /usr/lib
    /usr/local/lib
    ENV{FLY_CAPTURE_SDK}
    ENV{FLY_CAPTURE_SDK}/lib)
SET(FLYCAPUTRE_LIBRARY_SEARCH_NAMES
    flycapture
    FlyCapture2)
SET(FLYCAPUTRE_C_LIBRARY_SEARCH_NAMES
    flycapture-c
    FlyCapture2_C)

FIND_PATH(FLYCAPTURE_INCLUDE_DIR "FlyCapture2.h"
          HINTS ${FLYCAPUTRE_HEADER_SEARCH_PATHS}
          PATH_SUFFIXES "flycapture")
MESSAGE(STATUS "Path founded : ${FLYCAPTURE_INCLUDE_DIR}")
FIND_LIBRARY(FLYCAPTURE_C_LIB
             NAMES ${FLYCAPUTRE_C_LIBRARY_SEARCH_NAMES})

# handle the QUIETLY and REQUIRED arguments and set FlyCapture_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE( "FindPackageHandleStandardArgs" )
FIND_PACKAGE_HANDLE_STANDARD_ARGS( "FlyCapture" DEFAULT_MSG FLYCAPTURE_INCLUDE_DIR FLYCAPTURE_C_LIB)
MARK_AS_ADVANCED(FLYCAPTURE_INCLUDE_DIR)
IF(FlyCapture_FOUND AND NOT TARGET ext::flycapture)
  # Define target
  ADD_LIBRARY(ext::flycapture SHARED IMPORTED)
  SET_TARGET_PROPERTIES(ext::flycapture PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FLYCAPTURE_INCLUDE_DIR}")
  SET_TARGET_PROPERTIES(ext::flycapture PROPERTIES IMPORTED_LOCATION "${FLYCAPTURE_C_LIB}")
ENDIF(FlyCapture_FOUND AND NOT TARGET ext::flycapture)